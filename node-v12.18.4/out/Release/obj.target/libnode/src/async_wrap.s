	.file	"async_wrap.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4858:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4858:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4859:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4859:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7098:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7678:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7678:
	.size	_ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15AsyncWrapObject8SelfSizeEv,"axG",@progbits,_ZNK4node15AsyncWrapObject8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15AsyncWrapObject8SelfSizeEv
	.type	_ZNK4node15AsyncWrapObject8SelfSizeEv, @function
_ZNK4node15AsyncWrapObject8SelfSizeEv:
.LFB7680:
	.cfi_startproc
	endbr64
	movl	$56, %eax
	ret
	.cfi_endproc
.LFE7680:
	.size	_ZNK4node15AsyncWrapObject8SelfSizeEv, .-_ZNK4node15AsyncWrapObject8SelfSizeEv
	.section	.text._ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7700:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7700:
	.size	_ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node11PromiseWrap8SelfSizeEv,"axG",@progbits,_ZNK4node11PromiseWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11PromiseWrap8SelfSizeEv
	.type	_ZNK4node11PromiseWrap8SelfSizeEv, @function
_ZNK4node11PromiseWrap8SelfSizeEv:
.LFB7702:
	.cfi_startproc
	endbr64
	movl	$56, %eax
	ret
	.cfi_endproc
.LFE7702:
	.size	_ZNK4node11PromiseWrap8SelfSizeEv, .-_ZNK4node11PromiseWrap8SelfSizeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.type	_ZNK4node9AsyncWrap18IsDoneInitializingEv, @function
_ZNK4node9AsyncWrap18IsDoneInitializingEv:
.LFB7740:
	.cfi_startproc
	endbr64
	movzbl	36(%rdi), %eax
	ret
	.cfi_endproc
.LFE7740:
	.size	_ZNK4node9AsyncWrap18IsDoneInitializingEv, .-_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED2Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED2Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED2Ev:
.LFB9891:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L10
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE9891:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED2Ev
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED1Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED2Ev
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_:
.LFB10275:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	jmp	*24(%r8)
	.cfi_endproc
.LFE10275:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_
	.section	.text._ZN4node7tracing11TracedValueD0Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD0Ev
	.type	_ZN4node7tracing11TracedValueD0Ev, @function
_ZN4node7tracing11TracedValueD0Ev:
.LFB9046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9046:
	.size	_ZN4node7tracing11TracedValueD0Ev, .-_ZN4node7tracing11TracedValueD0Ev
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED0Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED0Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED0Ev:
.LFB9893:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L17
	movq	(%rdi), %rax
	call	*8(%rax)
.L17:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9893:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11PromiseWrap19getIsChainedPromiseEN2v85LocalINS1_6StringEEERKNS1_20PropertyCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11PromiseWrap19getIsChainedPromiseEN2v85LocalINS1_6StringEEERKNS1_20PropertyCallbackInfoINS1_5ValueEEE, @function
_ZN4node11PromiseWrap19getIsChainedPromiseEN2v85LocalINS1_6StringEEERKNS1_20PropertyCallbackInfoINS1_5ValueEEE:
.LFB7704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rbx
	movq	8(%rbx), %rdi
	movq	-1(%rdi), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %edx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L28
	cmpw	$1040, %dx
	je	.L28
	leaq	8(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v86Object20SlowGetInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L30
.L26:
	movq	(%rax), %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
	movq	31(%rdi), %r12
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	jne	.L26
.L30:
	movq	24(%rbx), %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7704:
	.size	_ZN4node11PromiseWrap19getIsChainedPromiseEN2v85LocalINS1_6StringEEERKNS1_20PropertyCallbackInfoINS1_5ValueEEE, .-_ZN4node11PromiseWrap19getIsChainedPromiseEN2v85LocalINS1_6StringEEERKNS1_20PropertyCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL18extractPromiseWrapEN2v85LocalINS0_7PromiseEEE, @function
_ZN4nodeL18extractPromiseWrapEN2v85LocalINS0_7PromiseEEE:
.LFB7705:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r8
	movq	-1(%r8), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %edx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L41
	cmpw	$1040, %dx
	je	.L41
	xorl	%esi, %esi
	call	_ZN2v86Object20SlowGetInternalFieldEi@PLT
	movq	%rax, %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L40
.L45:
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L44
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L42
	cmpw	$1040, %cx
	jne	.L37
.L42:
	movq	23(%rdx), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	23(%r8), %r12
	movq	%r8, %rdi
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L45
.L40:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	.cfi_endproc
.LFE7705:
	.size	_ZN4nodeL18extractPromiseWrapEN2v85LocalINS0_7PromiseEEE, .-_ZN4nodeL18extractPromiseWrapEN2v85LocalINS0_7PromiseEEE
	.p2align 4
	.type	_ZN4nodeL18DisablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL18DisablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7709:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	xorl	%esi, %esi
	movq	8(%rax), %rdi
	jmp	_ZN2v87Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS2_INS_5ValueEEEE@PLT
	.cfi_endproc
.LFE7709:
	.size	_ZN4nodeL18DisablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL18DisablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL17EnablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL17EnablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7708:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	leaq	_ZN4nodeL11PromiseHookEN2v815PromiseHookTypeENS0_5LocalINS0_7PromiseEEENS2_INS0_5ValueEEE(%rip), %rsi
	movq	8(%rax), %rdi
	jmp	_ZN2v87Isolate14SetPromiseHookEPFvNS_15PromiseHookTypeENS_5LocalINS_7PromiseEEENS2_INS_5ValueEEEE@PLT
	.cfi_endproc
.LFE7708:
	.size	_ZN4nodeL17EnablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL17EnablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10AsyncHooks17pop_async_contextEd.part.0, @function
_ZN4node10AsyncHooks17pop_async_contextEd.part.0:
.LFB10329:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	fputc@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	call	abort@PLT
	.cfi_endproc
.LFE10329:
	.size	_ZN4node10AsyncHooks17pop_async_contextEd.part.0, .-_ZN4node10AsyncHooks17pop_async_contextEd.part.0
	.align 2
	.p2align 4
	.type	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0, @function
_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0:
.LFB10411:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r8
	movq	8(%rdi), %rcx
	movq	(%rdi), %r13
	movq	%r8, %rax
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r10
	testq	%r10, %r10
	je	.L62
	movq	%rdi, %rbx
	movq	(%r10), %rdi
	movq	%rdx, %r12
	movq	%r10, %r11
	movq	32(%rdi), %r9
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L52:
	movq	(%rdi), %r15
	testq	%r15, %r15
	je	.L62
	movq	32(%r15), %r9
	xorl	%edx, %edx
	movq	%rdi, %r11
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r12
	jne	.L62
	movq	%r15, %rdi
.L54:
	cmpq	%r9, %r8
	jne	.L52
	movq	8(%rdi), %rax
	cmpq	%rax, (%rsi)
	jne	.L52
	cmpq	16(%rdi), %r8
	jne	.L52
	movq	(%rdi), %rsi
	cmpq	%r11, %r10
	je	.L69
	testq	%rsi, %rsi
	je	.L56
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r12
	je	.L56
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L56:
	movq	%rsi, (%r11)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L63
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r12
	je	.L56
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L55:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L70
.L57:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r11, %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rsi, 16(%rbx)
	jmp	.L57
	.cfi_endproc
.LFE10411:
	.size	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0, .-_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev:
.LFB7754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movslq	32(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	_ZN4nodeL14provider_namesE(%rip), %rax
	movq	%r14, (%rdi)
	movq	(%rax,%rdx,8), %r15
	testq	%r15, %r15
	je	.L82
	movq	%rdi, %r12
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L83
	cmpq	$1, %rax
	jne	.L75
	movzbl	(%r15), %edx
	movb	%dl, 16(%r12)
.L76:
	movq	%rax, 8(%r12)
	movb	$0, (%r14,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L76
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L74:
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %rax
	movq	(%r12), %r14
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L84:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7754:
	.size	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB10414:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L86
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L86:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L88
.L99:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L99
.L88:
	subq	$48, %rsp
	orq	$0, 40(%rsp)
	movl	$1, %edx
	movl	$32, %esi
	leaq	15(%rsp), %r14
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movl	$32, %ecx
	andq	$-16, %r14
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	leaq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$32, -240(%rbp)
	movl	$48, -236(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L100
	cmpq	$1, %r13
	jne	.L92
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L93:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L93
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L91:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L93
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10414:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.p2align 4
	.type	_ZN4nodeL23DestroyParamCleanupHookEPv, @function
_ZN4nodeL23DestroyParamCleanupHookEPv:
.LFB7710:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L102
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L104
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L104:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L105
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L105:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	ret
	.cfi_endproc
.LFE7710:
	.size	_ZN4nodeL23DestroyParamCleanupHookEPv, .-_ZN4nodeL23DestroyParamCleanupHookEPv
	.section	.text._ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev:
.LFB7701:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6297566550592287312, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7701:
	.size	_ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev:
.LFB7679:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1784827760, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7021770853590856513, %rcx
	movq	%rdx, (%rdi)
	movl	$25445, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$116, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE7679:
	.size	_ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE
	.type	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE, @function
_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE:
.LFB7681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	352(%rdi), %rsi
	movq	2720(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	1416(%r12), %rax
	movq	%r12, -80(%rbp)
	movl	$1, -72(%rbp)
	movq	%rax, -200(%rbp)
	movq	1424(%r12), %rax
	movq	%rax, -192(%rbp)
	leaq	1930(%r12), %rax
	movq	%rax, -208(%rbp)
.L128:
	movq	$0, 1432(%r12)
	pxor	%xmm1, %xmm1
	movups	%xmm1, 1416(%r12)
	movq	-208(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L140
.L123:
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	je	.L127
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L127:
	movq	-216(%rbp), %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L141
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movzbl	2664(%r12), %eax
	testb	%al, %al
	jne	.L123
	movq	-200(%rbp), %rax
	leaq	-160(%rbp), %r15
	leaq	-168(%rbp), %r14
	movq	%rax, %rbx
	cmpq	%rax, -192(%rbp)
	je	.L125
	.p2align 4,,10
	.p2align 3
.L124:
	movsd	(%rbx), %xmm0
	movq	352(%r12), %rsi
	movq	%r15, %rdi
	movsd	%xmm0, -184(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r12), %rdi
	movsd	-184(%rbp), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r13, %rdi
	movq	%r14, %r8
	movl	$1, %ecx
	movq	%rax, -168(%rbp)
	movq	352(%r12), %rax
	movq	3280(%r12), %rsi
	leaq	88(%rax), %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r15, %rdi
	testq	%rax, %rax
	je	.L142
	call	_ZN2v811HandleScopeD1Ev@PLT
	addq	$8, %rbx
	cmpq	%rbx, -192(%rbp)
	jne	.L124
.L125:
	cmpq	$0, -200(%rbp)
	je	.L138
	movq	-200(%rbp), %rdi
	call	_ZdlPv@PLT
.L138:
	movq	1424(%r12), %rax
	movq	1416(%r12), %rcx
	movq	%rax, -192(%rbp)
	movq	%rcx, -200(%rbp)
	cmpq	%rcx, %rax
	jne	.L128
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L142:
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L123
.L141:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7681:
	.size	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE, .-_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE
	.align 2
	.p2align 4
	.type	_ZN4node9AsyncWrap11EmitDestroyEb.part.0, @function
_ZN4node9AsyncWrap11EmitDestroyEb.part.0:
.LFB10401:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	8(%rbx), %rcx
	movq	352(%rax), %rdi
	testq	%rcx, %rcx
	je	.L147
	movzbl	11(%rcx), %edx
	movq	%rcx, %r13
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L153
	movzbl	11(%rcx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L154
.L144:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	%r13, %rdi
	movq	168(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L155
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	movq	(%rcx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rcx
	movq	%rax, %r13
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	testq	%rcx, %rcx
	je	.L144
	movzbl	11(%rcx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	jne	.L144
.L154:
	movq	(%rcx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L147:
	xorl	%r13d, %r13d
	jmp	.L144
.L155:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10401:
	.size	_ZN4node9AsyncWrap11EmitDestroyEb.part.0, .-_ZN4node9AsyncWrap11EmitDestroyEb.part.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%ld"
.LC2:
	.string	"%lu"
.LC3:
	.string	"basic_string::append"
.LC4:
	.string	" ("
.LC5:
	.string	":"
.LC6:
	.string	")"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.type	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev, @function
_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev:
.LFB7755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rcx
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-224(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	cvttsd2siq	40(%rsi), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, %rsi
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	16(%r14), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	leaq	.LC2(%rip), %rcx
	movl	$32, %edx
	leaq	-288(%rbp), %rbx
	movq	1936(%rax), %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	(%r14), %rax
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	*24(%rax)
	movabsq	$4611686018427387903, %rax
	subq	-280(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L166
	movl	$2, %edx
	movq	%rbx, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-240(%rbp), %rbx
	movq	%rbx, -256(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L190
	movq	%rcx, -256(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -240(%rbp)
.L159:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -248(%rbp)
	movq	%rdx, (%rax)
	movq	-256(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-248(%rbp), %r8
	movq	-216(%rbp), %rdx
	cmpq	%rbx, %r9
	movq	%rax, %rdi
	cmovne	-240(%rbp), %rdi
	movq	-224(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L161
	leaq	-208(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-208(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L191
.L161:
	leaq	-256(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L163:
	leaq	-176(%rbp), %r14
	leaq	16(%rax), %rdx
	movq	%r14, -192(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L192
	movq	%rcx, -192(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -176(%rbp)
.L165:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -184(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -184(%rbp)
	je	.L166
	movl	$1, %edx
	leaq	-192(%rbp), %rdi
	leaq	.LC5(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	-144(%rbp), %r13
	movq	%r13, -160(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L193
	movq	%rcx, -160(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -144(%rbp)
.L168:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -152(%rbp)
	movq	%rdx, (%rax)
	movq	-160(%rbp), %r9
	movq	$0, 8(%rax)
	movl	$15, %eax
	movq	-120(%rbp), %rdx
	movq	-152(%rbp), %r8
	cmpq	%r13, %r9
	movq	%rax, %rdi
	movq	-128(%rbp), %rsi
	cmovne	-144(%rbp), %rdi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L170
	leaq	-112(%rbp), %rdi
	cmpq	%rdi, %rsi
	cmovne	-112(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L194
.L170:
	leaq	-160(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L172:
	leaq	-80(%rbp), %r15
	leaq	16(%rax), %rdx
	movq	%r15, -96(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L195
	movq	%rcx, -96(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
.L174:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -88(%rbp)
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -88(%rbp)
	je	.L166
	movl	$1, %edx
	leaq	-96(%rbp), %rdi
	leaq	.LC6(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L196
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L176:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%r15, %rdi
	je	.L177
	call	_ZdlPv@PLT
.L177:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L178
	call	_ZdlPv@PLT
.L178:
	movq	-192(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L179
	call	_ZdlPv@PLT
.L179:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	-288(%rbp), %rdi
	leaq	-272(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	-224(%rbp), %rdi
	leaq	-208(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L182
	call	_ZdlPv@PLT
.L182:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%r9, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L192:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -176(%rbp)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L190:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -240(%rbp)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L195:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -80(%rbp)
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L193:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -144(%rbp)
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L196:
	movdqu	16(%rax), %xmm4
	movups	%xmm4, 16(%r12)
	jmp	.L176
.L166:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L197:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7755:
	.size	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev, .-_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap15GetProviderTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node9AsyncWrap15GetProviderTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node9AsyncWrap15GetProviderTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	$0, 24(%r12)
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L209
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L204
	cmpw	$1040, %cx
	jne	.L200
.L204:
	movq	23(%rdx), %rax
.L202:
	testq	%rax, %rax
	je	.L198
	movslq	32(%rax), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L198:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7725:
	.size	_ZN4node9AsyncWrap15GetProviderTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node9AsyncWrap15GetProviderTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L221
	cmpw	$1040, %cx
	jne	.L211
.L221:
	movq	23(%rdx), %r12
.L213:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L214
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L229
.L216:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L230
	movq	8(%rbx), %r13
.L218:
	movq	2704(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L219
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2704(%r12)
.L219:
	testq	%r13, %r13
	je	.L210
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2704(%r12)
.L210:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L214:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L216
.L229:
	leaq	_ZZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L213
	.cfi_endproc
.LFE7728:
	.size	_ZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap10GetAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node9AsyncWrap10GetAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node9AsyncWrap10GetAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	.LC7(%rip), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	8(%r12), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L245
	movq	(%rax), %rax
.L233:
	movq	%rax, 24(%r12)
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L246
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L240
	cmpw	$1040, %cx
	jne	.L235
.L240:
	movq	23(%rdx), %rax
.L237:
	testq	%rax, %rax
	je	.L231
	movq	(%rbx), %rbx
	movsd	40(%rax), %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L247
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L231:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L245:
	movq	16(%r12), %rax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L231
	.cfi_endproc
.LFE7721:
	.size	_ZN4node9AsyncWrap10GetAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node9AsyncWrap10GetAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L249:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L259
.L250:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L251
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L250
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.type	_ZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jg	.L261
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L336
.L263:
	cmpl	$1, 16(%rbx)
	jle	.L337
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L265:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L338
	cmpl	$2, 16(%rbx)
	jg	.L267
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L268:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L339
	movq	(%rbx), %rax
	movl	$32, %edi
	movq	8(%rax), %r14
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	cmpl	$1, 16(%rbx)
	movq	$0x000000000, (%rax)
	movq	%rax, %r12
	movq	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	jg	.L270
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L271:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdi
	movsd	%xmm0, (%r12)
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L303
	cmpw	$1040, %cx
	jne	.L272
.L303:
	movq	23(%rdx), %rax
.L274:
	movq	%rax, 8(%r12)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L275
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L276:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L277
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 16(%r12)
.L277:
	testq	%r13, %r13
	je	.L278
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 16(%r12)
.L278:
	cmpl	$2, 16(%rbx)
	movq	24(%r12), %rdi
	jg	.L279
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	testq	%rdi, %rdi
	je	.L280
.L281:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%r12)
.L280:
	testq	%r13, %r13
	je	.L283
.L282:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%r12)
.L283:
	movq	16(%r12), %rdi
	leaq	_ZN4node9AsyncWrap12WeakCallbackERKN2v816WeakCallbackInfoINS_12DestroyParamEEE(%rip), %rdx
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	8(%r12), %rbx
	movl	$40, %edi
	movq	2640(%rbx), %r14
	leaq	1(%r14), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	leaq	_ZN4nodeL23DestroyParamCleanupHookEPv(%rip), %r10
	xorl	%edx, %edx
	movq	%r10, 8(%rax)
	movq	%rax, %r13
	movq	%r12, 16(%rax)
	movq	%r14, 24(%rax)
	movq	2592(%rbx), %rsi
	movq	$0, (%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%r8, %r8
	je	.L284
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L285:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L284
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L284
	movq	%rdi, %rax
.L287:
	cmpq	%rcx, %r12
	jne	.L285
	cmpq	%r10, 8(%rax)
	jne	.L285
	cmpq	16(%rax), %r12
	jne	.L285
	cmpq	$0, (%r8)
	je	.L284
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L261:
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L263
.L336:
	leaq	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L337:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L284:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L288
	movq	2584(%rbx), %r8
	movq	%r12, 32(%r13)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L298
.L342:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r15), %rax
	movq	%r13, (%rax)
.L299:
	addq	$1, 2608(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L270:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L279:
	movq	8(%rbx), %r13
	subq	$16, %r13
	testq	%rdi, %rdi
	jne	.L281
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L275:
	movq	8(%rbx), %r13
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L288:
	cmpq	$1, %rdx
	je	.L340
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L341
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%rbx), %r10
	movq	%rax, %r8
.L291:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L293
	xorl	%edi, %edi
	leaq	2600(%rbx), %r9
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L295:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L296:
	testq	%rsi, %rsi
	je	.L293
.L294:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L295
	movq	2600(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L302
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L294
	.p2align 4,,10
	.p2align 3
.L293:
	movq	2584(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L297
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L297:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r14, 2592(%rbx)
	divq	%r14
	movq	%r8, 2584(%rbx)
	movq	%r12, 32(%r13)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L342
.L298:
	movq	2600(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 2600(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L300
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r8,%rdx,8)
.L300:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L302:
	movq	%rdx, %rdi
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L339:
	leaq	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L272:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L340:
	leaq	2632(%rbx), %r8
	movq	$0, 2632(%rbx)
	movq	%r8, %r10
	jmp	.L291
.L341:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7717:
	.size	_ZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap16PushAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node9AsyncWrap16PushAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node9AsyncWrap16PushAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L360
	cmpw	$1040, %cx
	jne	.L344
.L360:
	movq	23(%rdx), %r12
.L346:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L347
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L348:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value11NumberValueENS_5LocalINS_7ContextEEE@PLT
	movsd	%xmm0, -88(%rbp)
	testb	%al, %al
	je	.L362
.L349:
	cmpl	$1, 16(%rbx)
	jg	.L350
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L351:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value11NumberValueENS_5LocalINS_7ContextEEE@PLT
	movsd	%xmm0, -96(%rbp)
	testb	%al, %al
	je	.L363
.L352:
	cmpl	$2, 16(%rbx)
	leaq	1144(%r12), %r10
	jg	.L353
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L354:
	leaq	-80(%rbp), %r13
	movq	-792(%r10), %rsi
	leaq	-1144(%r10), %rbx
	movq	%r10, -104(%rbp)
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1216(%r12), %rax
	movq	-104(%rbp), %r10
	movl	24(%rax), %edx
	testl	%edx, %edx
	je	.L355
	movsd	.LC7(%rip), %xmm0
	movsd	-88(%rbp), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L364
	movsd	-96(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L365
.L355:
	movl	28(%rax), %r15d
	leal	(%r15,%r15), %r9d
	movl	%r15d, %edx
	movq	%r9, %r8
	cmpq	1160(%r12), %r9
	jnb	.L366
.L357:
	movq	1256(%r12), %rcx
	movq	1176(%r12), %rsi
	leal	1(%r8), %edi
	addl	$1, %edx
	movsd	(%rcx), %xmm0
	movsd	%xmm0, (%rsi,%r9,8)
	movsd	8(%rcx), %xmm0
	movsd	%xmm0, (%rsi,%rdi,8)
	movsd	-88(%rbp), %xmm0
	movq	1272(%r12), %rdi
	movl	%edx, 28(%rax)
	movq	3280(%rbx), %rsi
	movl	%r15d, %edx
	movhpd	-96(%rbp), %xmm0
	movups	%xmm0, (%rcx)
	movq	%r14, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L367
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L350:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L353:
	movq	8(%rbx), %r14
	subq	$16, %r14
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%r10, %rdi
	movq	%r9, -112(%rbp)
	movl	%r9d, -104(%rbp)
	call	_ZN4node10AsyncHooks20grow_async_ids_stackEv@PLT
	movq	-112(%rbp), %r9
	movl	-104(%rbp), %r8d
	movq	1216(%r12), %rax
	movl	28(%rax), %edx
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L344:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L364:
	leaq	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L363:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L362:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L365:
	leaq	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L367:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7722:
	.size	_ZN4node9AsyncWrap16PushAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node9AsyncWrap16PushAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"Error: async hook stack has become corrupted (actual: %.f, expected: %.f)\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap15PopAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node9AsyncWrap15PopAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node9AsyncWrap15PopAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L382
	cmpw	$1040, %cx
	jne	.L369
.L382:
	movq	23(%rdx), %r12
.L371:
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jg	.L372
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L373:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value11NumberValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L389
.L374:
	movq	1216(%r12), %rax
	movq	(%rbx), %rbx
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L375
	movl	24(%rax), %esi
	movq	1256(%r12), %rcx
	testl	%esi, %esi
	je	.L376
	movsd	(%rcx), %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L383
	jne	.L383
.L376:
	movq	1176(%r12), %rsi
	subl	$1, %edx
	leal	(%rdx,%rdx), %r8d
	movsd	(%rsi,%r8,8), %xmm0
	movq	%r8, %rdi
	addl	$1, %edi
	movsd	%xmm0, (%rcx)
	movsd	(%rsi,%rdi,8), %xmm0
	movq	1272(%r12), %rdi
	movq	3280(%r12), %rsi
	movsd	%xmm0, 8(%rcx)
	movl	%edx, 28(%rax)
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEEj@PLT
	movq	1216(%r12), %rdx
	movl	$56, %eax
	movl	28(%rdx), %edx
	testl	%edx, %edx
	je	.L375
.L380:
	movq	8(%rbx), %rdx
	movq	56(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movl	$64, %eax
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L372:
	movq	8(%rbx), %rdi
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L383:
	movq	stderr(%rip), %rdi
	movapd	%xmm0, %xmm1
	movl	$1, %esi
	movapd	%xmm2, %xmm0
	leaq	.LC9(%rip), %rdx
	movl	$2, %eax
	call	__fprintf_chk@PLT
	movq	stderr(%rip), %rdi
	call	_ZN4node13DumpBacktraceEP8_IO_FILE@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movq	1640(%r12), %rax
	cmpb	$0, 8(%rax)
	jne	.L390
	movl	$1, %edi
	call	exit@PLT
	.p2align 4,,10
	.p2align 3
.L369:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L389:
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movsd	-24(%rbp), %xmm0
	jmp	.L374
.L390:
	call	_ZN4node10AsyncHooks17pop_async_contextEd.part.0
	.cfi_endproc
.LFE7723:
	.size	_ZN4node9AsyncWrap15PopAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node9AsyncWrap15PopAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1, @function
_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1:
.LFB10412:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movq	%rdi, -88(%rbp)
	movq	%rsi, -96(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$8, (%r8)
	movaps	%xmm0, -80(%rbp)
	jne	.L392
	movq	(%r9), %rax
	movq	%rax, -80(%rbp)
.L392:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L399
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r12
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L406
.L393:
	leaq	-64(%rbp), %r14
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rbx
.L397:
	movq	-8(%r14), %r13
	subq	$8, %r14
	testq	%r13, %r13
	je	.L394
	movq	0(%r13), %rdx
	movq	8(%rdx), %rdx
	cmpq	%rbx, %rdx
	jne	.L395
	movq	8(%r13), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r13), %rdx
	movq	%rax, 0(%r13)
	cmpq	%rdx, %rdi
	je	.L396
	call	_ZdlPv@PLT
.L396:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L394:
	cmpq	%r12, %r14
	jne	.L397
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L407
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rdx
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L399:
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %r12
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L406:
	subq	$8, %rsp
	movq	-96(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%r14, %r9
	pushq	$2
	xorl	%r8d, %r8d
	movl	$98, %esi
	pushq	%r12
	pushq	%rbx
	pushq	%r13
	pushq	-104(%rbp)
	pushq	$1
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L393
.L407:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10412:
	.size	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1, .-_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	.p2align 4
	.type	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0, @function
_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0:
.LFB10413:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%edi, -84(%rbp)
	movl	%r8d, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	testq	%rax, %rax
	je	.L415
	movq	%rax, %rdi
	movq	(%rax), %rax
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %rbx
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L422
.L409:
	leaq	-64(%rbp), %r14
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r13
.L413:
	movq	-8(%r14), %r12
	subq	$8, %r14
	testq	%r12, %r12
	je	.L410
	movq	(%r12), %rdx
	movq	8(%rdx), %rdx
	cmpq	%r13, %rdx
	jne	.L411
	movq	8(%r12), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L412
	call	_ZdlPv@PLT
.L412:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L410:
	cmpq	%rbx, %r14
	jne	.L413
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L423
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L415:
	xorl	%r15d, %r15d
	leaq	-80(%rbp), %rbx
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L422:
	movl	-88(%rbp), %ecx
	subq	$8, %rsp
	movsbl	-84(%rbp), %esi
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	%rcx
	movq	%r13, %rcx
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L409
.L423:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10413:
	.size	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0, .-_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	.section	.rodata.str1.1
.LC10:
	.string	"init"
.LC11:
	.string	"before"
.LC12:
	.string	"after"
.LC13:
	.string	"destroy"
.LC14:
	.string	"promise_resolve"
.LC15:
	.string	"PromiseWrap"
.LC16:
	.string	"isChainedPromise"
	.text
	.p2align 4
	.type	_ZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L463
	cmpw	$1040, %cx
	jne	.L425
.L463:
	movq	23(%rdx), %rbx
.L427:
	movl	16(%r12), %esi
	testl	%esi, %esi
	jg	.L428
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L501
.L430:
	cmpq	$0, 2728(%rbx)
	jne	.L502
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L503
	movq	8(%r12), %r13
.L433:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4, %ecx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L504
.L434:
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L505
.L435:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L506
	movq	2728(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L437
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2728(%rbx)
.L437:
	testq	%r12, %r12
	je	.L438
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2728(%rbx)
.L438:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L507
.L439:
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L508
.L440:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L509
	movq	2696(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L442
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2696(%rbx)
.L442:
	testq	%r12, %r12
	je	.L443
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2696(%rbx)
.L443:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$5, %ecx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L510
.L444:
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L511
.L445:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L512
	movq	2688(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L447
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2688(%rbx)
.L447:
	testq	%r12, %r12
	je	.L448
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2688(%rbx)
.L448:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L513
.L449:
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L514
.L450:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L515
	movq	2720(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L452
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2720(%rbx)
.L452:
	testq	%r12, %r12
	je	.L453
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2720(%rbx)
.L453:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$15, %ecx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L516
.L454:
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L517
.L455:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L518
	movq	2736(%rbx), %rdi
	movq	352(%rbx), %r13
	testq	%rdi, %rdi
	je	.L457
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2736(%rbx)
.L457:
	testq	%r12, %r12
	je	.L458
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2736(%rbx)
.L458:
	subq	$8, %rsp
	movq	352(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rsi
	movl	$11, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L519
.L459:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$2, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	leaq	.LC16(%rip), %rsi
	xorl	%edx, %edx
	movl	$16, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L520
.L460:
	pushq	$0
	movq	%r12, %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node11PromiseWrap19getIsChainedPromiseEN2v85LocalINS1_6StringEEERKNS1_20PropertyCallbackInfoINS1_5ValueEEE(%rip), %rdx
	pushq	$0
	pushq	$0
	call	_ZN2v814ObjectTemplate11SetAccessorENS_5LocalINS_6StringEEEPFvS3_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS3_NS1_IS5_EERKNS4_IvEEESB_NS_13AccessControlENS_17PropertyAttributeENS1_INS_17AccessorSignatureEEENS_14SideEffectTypeESL_@PLT
	movq	3200(%rbx), %rdi
	movq	352(%rbx), %r13
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L461
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3200(%rbx)
.L461:
	testq	%r12, %r12
	je	.L424
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3200(%rbx)
.L424:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L428:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L430
.L501:
	leaq	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L425:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L505:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L506:
	leaq	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L507:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L509:
	leaq	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L508:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L440
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L511:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L513:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L515:
	leaq	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L514:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L517:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L516:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L520:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rsi
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L519:
	movq	%rsi, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rsi
	jmp	.L459
	.cfi_endproc
.LFE7707:
	.size	_ZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	subq	$1, 2656(%rdi)
	addq	$2584, %rdi
	movq	%rax, -48(%rbp)
	movq	%rbx, -40(%rbp)
	movq	$0, -32(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L537
.L523:
	cmpq	$0, 8(%rbx)
	je	.L521
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L527
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L538
.L527:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L521
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L521:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L537:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L540
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L523
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L523
.L540:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7084:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7086:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.section	.text._ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc,comdat
	.p2align 4
	.weak	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	.type	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc, @function
_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc:
.LFB7329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	testq	%rax, %rax
	je	.L545
	movq	%rax, %rdi
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r8
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L548
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L548:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r8
	movq	%r8, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7329:
	.size	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc, .-_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	.text
	.p2align 4
	.globl	_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE
	.type	_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE, @function
_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE:
.LFB7691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1216(%rdi), %rax
	movl	(%rax,%rsi,4), %eax
	testl	%eax, %eax
	jne	.L557
.L549:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L558
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L557:
	.cfi_restore_state
	movq	%rdi, %rbx
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	je	.L549
	movsd	%xmm0, -168(%rbp)
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L549
	movq	352(%rdi), %rsi
	leaq	-144(%rbp), %r13
	movq	%rdx, %r12
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movsd	-168(%rbp), %xmm0
	movq	352(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	352(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	352(%rbx), %rax
	movl	$1, %ecx
	movq	3280(%rbx), %rsi
	leaq	-152(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	leaq	88(%rax), %rdx
	movl	$1, -56(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r14, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L549
.L558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7691:
	.size	_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE, .-_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap18EmitPromiseResolveEPNS_11EnvironmentEd
	.type	_ZN4node9AsyncWrap18EmitPromiseResolveEPNS_11EnvironmentEd, @function
_ZN4node9AsyncWrap18EmitPromiseResolveEPNS_11EnvironmentEd:
.LFB7692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1216(%rdi), %rax
	movl	16(%rax), %eax
	testl	%eax, %eax
	jne	.L567
.L559:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L568
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movq	2736(%rdi), %r12
	movq	%rdi, %rbx
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	je	.L559
	movsd	%xmm0, -168(%rbp)
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L559
	movq	352(%rdi), %rsi
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movsd	-168(%rbp), %xmm0
	movq	352(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	352(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	352(%rbx), %rax
	movl	$1, %ecx
	movq	3280(%rbx), %rsi
	leaq	-152(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	leaq	88(%rax), %rdx
	movl	$1, -56(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r14, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L559
.L568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7692:
	.size	_ZN4node9AsyncWrap18EmitPromiseResolveEPNS_11EnvironmentEd, .-_ZN4node9AsyncWrap18EmitPromiseResolveEPNS_11EnvironmentEd
	.section	.rodata.str1.1
.LC17:
	.string	"node,node.async_hooks"
.LC18:
	.string	"NONE_CALLBACK"
.LC19:
	.string	"DIRHANDLE_CALLBACK"
.LC20:
	.string	"DNSCHANNEL_CALLBACK"
.LC21:
	.string	"ELDHISTOGRAM_CALLBACK"
.LC22:
	.string	"FILEHANDLE_CALLBACK"
.LC23:
	.string	"FILEHANDLECLOSEREQ_CALLBACK"
.LC24:
	.string	"FSEVENTWRAP_CALLBACK"
.LC25:
	.string	"FSREQCALLBACK_CALLBACK"
.LC26:
	.string	"FSREQPROMISE_CALLBACK"
.LC27:
	.string	"GETADDRINFOREQWRAP_CALLBACK"
.LC28:
	.string	"GETNAMEINFOREQWRAP_CALLBACK"
.LC29:
	.string	"HEAPSNAPSHOT_CALLBACK"
.LC30:
	.string	"HTTP2SESSION_CALLBACK"
.LC31:
	.string	"HTTP2STREAM_CALLBACK"
.LC32:
	.string	"HTTP2PING_CALLBACK"
.LC33:
	.string	"HTTP2SETTINGS_CALLBACK"
.LC34:
	.string	"HTTPINCOMINGMESSAGE_CALLBACK"
.LC35:
	.string	"HTTPCLIENTREQUEST_CALLBACK"
.LC36:
	.string	"JSSTREAM_CALLBACK"
.LC37:
	.string	"MESSAGEPORT_CALLBACK"
.LC38:
	.string	"PIPECONNECTWRAP_CALLBACK"
.LC39:
	.string	"PIPESERVERWRAP_CALLBACK"
.LC40:
	.string	"PIPEWRAP_CALLBACK"
.LC41:
	.string	"PROCESSWRAP_CALLBACK"
.LC42:
	.string	"PROMISE_CALLBACK"
.LC43:
	.string	"QUERYWRAP_CALLBACK"
.LC44:
	.string	"SHUTDOWNWRAP_CALLBACK"
.LC45:
	.string	"SIGNALWRAP_CALLBACK"
.LC46:
	.string	"STATWATCHER_CALLBACK"
.LC47:
	.string	"STREAMPIPE_CALLBACK"
.LC48:
	.string	"TCPCONNECTWRAP_CALLBACK"
.LC49:
	.string	"TCPSERVERWRAP_CALLBACK"
.LC50:
	.string	"TCPWRAP_CALLBACK"
.LC51:
	.string	"TTYWRAP_CALLBACK"
.LC52:
	.string	"UDPSENDWRAP_CALLBACK"
.LC53:
	.string	"UDPWRAP_CALLBACK"
.LC54:
	.string	"SIGINTWATCHDOG_CALLBACK"
.LC55:
	.string	"WORKER_CALLBACK"
.LC56:
	.string	"WORKERHEAPSNAPSHOT_CALLBACK"
.LC57:
	.string	"WRITEWRAP_CALLBACK"
.LC58:
	.string	"ZLIB_CALLBACK"
.LC59:
	.string	"PBKDF2REQUEST_CALLBACK"
.LC60:
	.string	"KEYPAIRGENREQUEST_CALLBACK"
.LC61:
	.string	"RANDOMBYTESREQUEST_CALLBACK"
.LC62:
	.string	"SCRYPTREQUEST_CALLBACK"
.LC63:
	.string	"TLSWRAP_CALLBACK"
.LC64:
	.string	"INSPECTORJSBINDING_CALLBACK"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap20EmitTraceEventBeforeEv
	.type	_ZN4node9AsyncWrap20EmitTraceEventBeforeEv, @function
_ZN4node9AsyncWrap20EmitTraceEventBeforeEv:
.LFB7693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$46, 32(%rdi)
	ja	.L570
	movl	32(%rdi), %eax
	leaq	.L572(%rip), %rdx
	movq	%rdi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L572:
	.long	.L618-.L572
	.long	.L617-.L572
	.long	.L616-.L572
	.long	.L615-.L572
	.long	.L614-.L572
	.long	.L613-.L572
	.long	.L612-.L572
	.long	.L611-.L572
	.long	.L610-.L572
	.long	.L609-.L572
	.long	.L608-.L572
	.long	.L607-.L572
	.long	.L606-.L572
	.long	.L605-.L572
	.long	.L604-.L572
	.long	.L603-.L572
	.long	.L602-.L572
	.long	.L601-.L572
	.long	.L600-.L572
	.long	.L599-.L572
	.long	.L598-.L572
	.long	.L597-.L572
	.long	.L596-.L572
	.long	.L595-.L572
	.long	.L594-.L572
	.long	.L593-.L572
	.long	.L592-.L572
	.long	.L591-.L572
	.long	.L590-.L572
	.long	.L589-.L572
	.long	.L588-.L572
	.long	.L587-.L572
	.long	.L586-.L572
	.long	.L585-.L572
	.long	.L584-.L572
	.long	.L583-.L572
	.long	.L582-.L572
	.long	.L581-.L572
	.long	.L580-.L572
	.long	.L579-.L572
	.long	.L578-.L572
	.long	.L577-.L572
	.long	.L576-.L572
	.long	.L575-.L572
	.long	.L574-.L572
	.long	.L573-.L572
	.long	.L571-.L572
	.text
	.p2align 4,,10
	.p2align 3
.L573:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__44_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1045
.L756:
	testb	$5, (%rsi)
	jne	.L1046
.L569:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L574:
	.cfi_restore_state
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__43_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1047
.L753:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC62(%rip), %rdx
.L1044:
	addq	$8, %rsp
	movl	$98, %edi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	.p2align 4,,10
	.p2align 3
.L575:
	.cfi_restore_state
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__42_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1048
.L750:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC61(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L580:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__37_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1049
.L735:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC56(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L576:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__41_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1050
.L747:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC60(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L584:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__33_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1051
.L723:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC52(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L578:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__39_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1052
.L741:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC58(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L582:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__35_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1053
.L729:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC54(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L577:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__40_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1054
.L744:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC59(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L586:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__31_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1055
.L717:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC50(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L579:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__38_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1056
.L738:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC57(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L581:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__36_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1057
.L732:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC55(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L585:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__32_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1058
.L720:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC51(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L583:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__34_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1059
.L726:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC53(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L587:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__30_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1060
.L714:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC49(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L588:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__29_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1061
.L711:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC48(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L604:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__13_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1062
.L663:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC32(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L596:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__21_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1063
.L687:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC40(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L612:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_5(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1064
.L639:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC24(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L592:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__25_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1065
.L699:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC44(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L608:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_9(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1066
.L651:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC28(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L600:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__17_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1067
.L675:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC36(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L616:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_1(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1068
.L627:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC20(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L590:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__27_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1069
.L705:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC46(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L606:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__11_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1070
.L657:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC30(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L598:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__19_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1071
.L681:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC38(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L614:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_3(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1072
.L633:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC22(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L594:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__23_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1073
.L693:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC42(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L610:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_7(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1074
.L645:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC26(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L602:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__15_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1075
.L669:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC34(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L618:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1076
.L620:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC18(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L589:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__28_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1077
.L708:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC47(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L605:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__12_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1078
.L660:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC31(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L597:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__20_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1079
.L684:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC39(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L613:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_4(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1080
.L636:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC23(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L593:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__24_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1081
.L696:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC43(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L609:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_8(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1082
.L648:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC27(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L601:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__16_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1083
.L672:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC35(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L617:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_0(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1084
.L624:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC19(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L591:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__26_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1085
.L702:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC45(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L607:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__10_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1086
.L654:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC29(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L599:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__18_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1087
.L678:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC37(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L615:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_2(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1088
.L630:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC21(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L595:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__22_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1089
.L690:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC41(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L611:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_6(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1090
.L642:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC25(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L603:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__14_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1091
.L666:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC33(%rip), %rdx
	jmp	.L1044
	.p2align 4,,10
	.p2align 3
.L571:
	movq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__45_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1092
.L759:
	testb	$5, (%rsi)
	je	.L569
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC64(%rip), %rdx
	jmp	.L1044
.L1046:
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC63(%rip), %rdx
	jmp	.L1044
.L1048:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L751
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1093
.L751:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__42_(%rip)
	mfence
	jmp	.L750
.L1049:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L736
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1094
.L736:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__37_(%rip)
	mfence
	jmp	.L735
.L1050:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L748
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1095
.L748:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__41_(%rip)
	mfence
	jmp	.L747
.L1047:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L754
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1096
.L754:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__43_(%rip)
	mfence
	jmp	.L753
.L1052:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L742
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1097
.L742:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__39_(%rip)
	mfence
	jmp	.L741
.L1051:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L724
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1098
.L724:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__33_(%rip)
	mfence
	jmp	.L723
.L1054:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L745
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1099
.L745:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__40_(%rip)
	mfence
	jmp	.L744
.L1045:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L757
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1100
.L757:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__44_(%rip)
	mfence
	jmp	.L756
.L1056:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L739
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1101
.L739:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__38_(%rip)
	mfence
	jmp	.L738
.L1053:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L730
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1102
.L730:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__35_(%rip)
	mfence
	jmp	.L729
.L1057:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L733
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1103
.L733:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__36_(%rip)
	mfence
	jmp	.L732
.L1092:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L760
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1104
.L760:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__45_(%rip)
	mfence
	jmp	.L759
.L1058:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L721
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1105
.L721:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__32_(%rip)
	mfence
	jmp	.L720
.L1055:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L718
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1106
.L718:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__31_(%rip)
	mfence
	jmp	.L717
.L1059:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L727
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1107
.L727:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__34_(%rip)
	mfence
	jmp	.L726
.L1060:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L715
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1108
.L715:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__30_(%rip)
	mfence
	jmp	.L714
.L1062:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L664
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1109
.L664:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__13_(%rip)
	mfence
	jmp	.L663
.L1064:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L640
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1110
.L640:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_5(%rip)
	mfence
	jmp	.L639
.L1066:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L652
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1111
.L652:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_9(%rip)
	mfence
	jmp	.L651
.L1068:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L628
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1112
.L628:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_1(%rip)
	mfence
	jmp	.L627
.L1070:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L658
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1113
.L658:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__11_(%rip)
	mfence
	jmp	.L657
.L1072:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L634
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1114
.L634:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_3(%rip)
	mfence
	jmp	.L633
.L1074:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L646
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1115
.L646:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_7(%rip)
	mfence
	jmp	.L645
.L1076:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L621
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1116
.L621:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153(%rip)
	mfence
	jmp	.L620
.L1078:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L661
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1117
.L661:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__12_(%rip)
	mfence
	jmp	.L660
.L1080:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L637
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1118
.L637:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_4(%rip)
	mfence
	jmp	.L636
.L1082:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L649
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1119
.L649:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_8(%rip)
	mfence
	jmp	.L648
.L1084:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L625
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1120
.L625:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_0(%rip)
	mfence
	jmp	.L624
.L1086:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L655
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1121
.L655:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__10_(%rip)
	mfence
	jmp	.L654
.L1088:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L631
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1122
.L631:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_2(%rip)
	mfence
	jmp	.L630
.L1090:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L643
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1123
.L643:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_6(%rip)
	mfence
	jmp	.L642
.L1061:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L712
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1124
.L712:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__29_(%rip)
	mfence
	jmp	.L711
.L1063:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L688
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1125
.L688:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__21_(%rip)
	mfence
	jmp	.L687
.L1065:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L700
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1126
.L700:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__25_(%rip)
	mfence
	jmp	.L699
.L1067:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L676
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1127
.L676:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__17_(%rip)
	mfence
	jmp	.L675
.L1069:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L706
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1128
.L706:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__27_(%rip)
	mfence
	jmp	.L705
.L1071:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L682
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1129
.L682:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__19_(%rip)
	mfence
	jmp	.L681
.L1073:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L694
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1130
.L694:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__23_(%rip)
	mfence
	jmp	.L693
.L1075:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L670
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1131
.L670:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__15_(%rip)
	mfence
	jmp	.L669
.L1077:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L709
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1132
.L709:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__28_(%rip)
	mfence
	jmp	.L708
.L1079:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L685
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1133
.L685:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__20_(%rip)
	mfence
	jmp	.L684
.L1081:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L697
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1134
.L697:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__24_(%rip)
	mfence
	jmp	.L696
.L1083:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L673
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1135
.L673:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__16_(%rip)
	mfence
	jmp	.L672
.L1085:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L703
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1136
.L703:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__26_(%rip)
	mfence
	jmp	.L702
.L1087:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L679
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1137
.L679:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__18_(%rip)
	mfence
	jmp	.L678
.L1089:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L691
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1138
.L691:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__22_(%rip)
	mfence
	jmp	.L690
.L1091:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L667
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1139
.L667:
	movq	%rsi, _ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__14_(%rip)
	mfence
	jmp	.L666
.L1124:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L712
.L1132:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L709
.L1128:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L706
.L1136:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L703
.L1126:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L700
.L1134:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L697
.L1130:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L694
.L1138:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L691
.L1125:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L688
.L1133:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L685
.L1129:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L682
.L1137:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L679
.L1127:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L676
.L1135:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L673
.L1131:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L670
.L1139:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L667
.L1108:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L715
.L1116:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L621
.L1112:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L628
.L1120:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L625
.L1110:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L640
.L1118:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L637
.L1114:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L634
.L1122:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L631
.L1109:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L664
.L1117:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L661
.L1113:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L658
.L1121:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L655
.L1111:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L652
.L1119:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L649
.L1115:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L646
.L1123:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L643
.L1096:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L754
.L1100:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L757
.L1104:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L760
.L1094:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L736
.L1102:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L730
.L1098:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L724
.L1106:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L718
.L1093:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L751
.L1101:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L739
.L1097:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L742
.L1105:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L721
.L1095:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L748
.L1103:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L733
.L1099:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L745
.L1107:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L727
.L570:
	leaq	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7693:
	.size	_ZN4node9AsyncWrap20EmitTraceEventBeforeEv, .-_ZN4node9AsyncWrap20EmitTraceEventBeforeEv
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap10EmitBeforeEPNS_11EnvironmentEd
	.type	_ZN4node9AsyncWrap10EmitBeforeEPNS_11EnvironmentEd, @function
_ZN4node9AsyncWrap10EmitBeforeEPNS_11EnvironmentEd:
.LFB7694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1216(%rdi), %rax
	movl	4(%rax), %eax
	testl	%eax, %eax
	jne	.L1148
.L1140:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1149
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	movq	2696(%rdi), %r12
	movq	%rdi, %rbx
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	je	.L1140
	movsd	%xmm0, -168(%rbp)
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L1140
	movq	352(%rdi), %rsi
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movsd	-168(%rbp), %xmm0
	movq	352(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	352(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	352(%rbx), %rax
	movl	$1, %ecx
	movq	3280(%rbx), %rsi
	leaq	-152(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	leaq	88(%rax), %rdx
	movl	$1, -56(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r14, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1140
.L1149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7694:
	.size	_ZN4node9AsyncWrap10EmitBeforeEPNS_11EnvironmentEd, .-_ZN4node9AsyncWrap10EmitBeforeEPNS_11EnvironmentEd
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEd
	.type	_ZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEd, @function
_ZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEd:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movsd	%xmm0, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$46, %edi
	ja	.L1151
	leaq	.L1153(%rip), %rdx
	movl	%edi, %edi
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1153:
	.long	.L1199-.L1153
	.long	.L1198-.L1153
	.long	.L1197-.L1153
	.long	.L1196-.L1153
	.long	.L1195-.L1153
	.long	.L1194-.L1153
	.long	.L1193-.L1153
	.long	.L1192-.L1153
	.long	.L1191-.L1153
	.long	.L1190-.L1153
	.long	.L1189-.L1153
	.long	.L1188-.L1153
	.long	.L1187-.L1153
	.long	.L1186-.L1153
	.long	.L1185-.L1153
	.long	.L1184-.L1153
	.long	.L1183-.L1153
	.long	.L1182-.L1153
	.long	.L1181-.L1153
	.long	.L1180-.L1153
	.long	.L1179-.L1153
	.long	.L1178-.L1153
	.long	.L1177-.L1153
	.long	.L1176-.L1153
	.long	.L1175-.L1153
	.long	.L1174-.L1153
	.long	.L1173-.L1153
	.long	.L1172-.L1153
	.long	.L1171-.L1153
	.long	.L1170-.L1153
	.long	.L1169-.L1153
	.long	.L1168-.L1153
	.long	.L1167-.L1153
	.long	.L1166-.L1153
	.long	.L1165-.L1153
	.long	.L1164-.L1153
	.long	.L1163-.L1153
	.long	.L1162-.L1153
	.long	.L1161-.L1153
	.long	.L1160-.L1153
	.long	.L1159-.L1153
	.long	.L1158-.L1153
	.long	.L1157-.L1153
	.long	.L1156-.L1153
	.long	.L1155-.L1153
	.long	.L1154-.L1153
	.long	.L1152-.L1153
	.text
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__44_(%rip), %r12
	testq	%r12, %r12
	je	.L2069
.L1498:
	testb	$5, (%r12)
	jne	.L2070
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2071
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1155:
	.cfi_restore_state
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__43_(%rip), %r12
	testq	%r12, %r12
	je	.L2072
.L1490:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1492
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2073
.L1492:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1496:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1493
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1494
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1495
	call	_ZdlPv@PLT
.L1495:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1493:
	cmpq	%rbx, %r13
	jne	.L1496
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__42_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2074
.L1487:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC61(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__41_(%rip), %r12
	testq	%r12, %r12
	je	.L2075
.L1479:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1481
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2076
.L1481:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1485:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1482
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1483
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1484
	call	_ZdlPv@PLT
.L1484:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1482:
	cmpq	%rbx, %r13
	jne	.L1485
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__37_(%rip), %r12
	testq	%r12, %r12
	je	.L2077
.L1447:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1449
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2078
.L1449:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1453:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1450
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1451
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1452
	call	_ZdlPv@PLT
.L1452:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1450:
	cmpq	%rbx, %r13
	jne	.L1453
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__36_(%rip), %r12
	testq	%r12, %r12
	je	.L2079
.L1439:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1441
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2080
.L1441:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1445:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1442
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1443
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1444
	call	_ZdlPv@PLT
.L1444:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1442:
	cmpq	%rbx, %r13
	jne	.L1445
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__35_(%rip), %r12
	testq	%r12, %r12
	je	.L2081
.L1431:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1433
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2082
.L1433:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1437:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1434
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1435
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1436
	call	_ZdlPv@PLT
.L1436:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1434:
	cmpq	%rbx, %r13
	jne	.L1437
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__34_(%rip), %r12
	testq	%r12, %r12
	je	.L2083
.L1423:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1425
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2084
.L1425:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1429:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1426
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1427
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1428
	call	_ZdlPv@PLT
.L1428:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1426:
	cmpq	%rbx, %r13
	jne	.L1429
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__33_(%rip), %r12
	testq	%r12, %r12
	je	.L2085
.L1415:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1417
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2086
.L1417:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1421:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1418
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1419
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1420
	call	_ZdlPv@PLT
.L1420:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1418:
	cmpq	%rbx, %r13
	jne	.L1421
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__32_(%rip), %r12
	testq	%r12, %r12
	je	.L2087
.L1407:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1409
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2088
.L1409:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1413:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1410
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1411
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1412
	call	_ZdlPv@PLT
.L1412:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1410:
	cmpq	%rbx, %r13
	jne	.L1413
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1169:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__29_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2089
.L1393:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC48(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__28_(%rip), %r12
	testq	%r12, %r12
	je	.L2090
.L1385:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1387
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2091
.L1387:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1391:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1388
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1389
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1390
	call	_ZdlPv@PLT
.L1390:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1388:
	cmpq	%rbx, %r13
	jne	.L1391
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1171:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__27_(%rip), %r12
	testq	%r12, %r12
	je	.L2092
.L1377:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1379
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2093
.L1379:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1383:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1380
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1381
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1382
	call	_ZdlPv@PLT
.L1382:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1380:
	cmpq	%rbx, %r13
	jne	.L1383
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1172:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__26_(%rip), %r12
	testq	%r12, %r12
	je	.L2094
.L1369:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1371
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2095
.L1371:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1375:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1372
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1373
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1374
	call	_ZdlPv@PLT
.L1374:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1372:
	cmpq	%rbx, %r13
	jne	.L1375
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__25_(%rip), %r12
	testq	%r12, %r12
	je	.L2096
.L1361:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1363
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2097
.L1363:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1367:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1364
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1365
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1366
	call	_ZdlPv@PLT
.L1366:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1364:
	cmpq	%rbx, %r13
	jne	.L1367
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__23_(%rip), %r12
	testq	%r12, %r12
	je	.L2098
.L1350:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1352
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2099
.L1352:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1356:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1353
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1354
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1355
	call	_ZdlPv@PLT
.L1355:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1353:
	cmpq	%rbx, %r13
	jne	.L1356
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__22_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2100
.L1347:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC41(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__24_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2101
.L1358:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC43(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1177:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__21_(%rip), %r12
	testq	%r12, %r12
	je	.L2102
.L1339:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1341
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2103
.L1341:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1345:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1342
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1343
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1344
	call	_ZdlPv@PLT
.L1344:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1342:
	cmpq	%rbx, %r13
	jne	.L1345
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__31_(%rip), %r12
	testq	%r12, %r12
	je	.L2104
.L1399:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1401
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2105
.L1401:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1405:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1402
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1403
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1404
	call	_ZdlPv@PLT
.L1404:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1402:
	cmpq	%rbx, %r13
	jne	.L1405
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1168:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__30_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2106
.L1396:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC49(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1159:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__39_(%rip), %r12
	testq	%r12, %r12
	je	.L2107
.L1463:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1465
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2108
.L1465:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1469:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1466
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1467
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1468
	call	_ZdlPv@PLT
.L1468:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1466:
	cmpq	%rbx, %r13
	jne	.L1469
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__13_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2109
.L1280:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC32(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__12_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2110
.L1277:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC31(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1187:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__11_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2111
.L1274:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC30(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__40_(%rip), %r12
	testq	%r12, %r12
	je	.L2112
.L1471:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1473
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2113
.L1473:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1477:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1474
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1475
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1476
	call	_ZdlPv@PLT
.L1476:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1474:
	cmpq	%rbx, %r13
	jne	.L1477
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__17_(%rip), %r12
	testq	%r12, %r12
	je	.L2114
.L1307:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1309
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2115
.L1309:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1313:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1310
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1311
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1312
	call	_ZdlPv@PLT
.L1312:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1310:
	cmpq	%rbx, %r13
	jne	.L1313
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__15_(%rip), %r12
	testq	%r12, %r12
	je	.L2116
.L1291:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1293
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2117
.L1293:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1297:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1294
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1295
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1296
	call	_ZdlPv@PLT
.L1296:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1294:
	cmpq	%rbx, %r13
	jne	.L1297
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1184:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__14_(%rip), %r12
	testq	%r12, %r12
	je	.L2118
.L1283:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1285
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2119
.L1285:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1289:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1286
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1287
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1288
	call	_ZdlPv@PLT
.L1288:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1286:
	cmpq	%rbx, %r13
	jne	.L1289
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__16_(%rip), %r12
	testq	%r12, %r12
	je	.L2120
.L1299:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1301
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2121
.L1301:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1305:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1302
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1303
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1304
	call	_ZdlPv@PLT
.L1304:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1302:
	cmpq	%rbx, %r13
	jne	.L1305
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1179:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__19_(%rip), %r12
	testq	%r12, %r12
	je	.L2122
.L1323:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1325
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2123
.L1325:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1329:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1326
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1327
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1328
	call	_ZdlPv@PLT
.L1328:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1326:
	cmpq	%rbx, %r13
	jne	.L1329
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1178:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__20_(%rip), %r12
	testq	%r12, %r12
	je	.L2124
.L1331:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1333
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2125
.L1333:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1337:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1334
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1335
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1336
	call	_ZdlPv@PLT
.L1336:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1334:
	cmpq	%rbx, %r13
	jne	.L1337
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1180:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__18_(%rip), %r12
	testq	%r12, %r12
	je	.L2126
.L1315:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1317
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2127
.L1317:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1321:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1318
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1319
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1320
	call	_ZdlPv@PLT
.L1320:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1318:
	cmpq	%rbx, %r13
	jne	.L1321
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1193:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_5(%rip), %r12
	testq	%r12, %r12
	je	.L2128
.L1246:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1248
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2129
.L1248:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1252:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1249
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1250
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1251
	call	_ZdlPv@PLT
.L1251:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1249:
	cmpq	%rbx, %r13
	jne	.L1252
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1194:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_4(%rip), %r12
	testq	%r12, %r12
	je	.L2130
.L1238:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1240
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2131
.L1240:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1244:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1241
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1242
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1243
	call	_ZdlPv@PLT
.L1243:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1241:
	cmpq	%rbx, %r13
	jne	.L1244
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_3(%rip), %r12
	testq	%r12, %r12
	je	.L2132
.L1230:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1232
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2133
.L1232:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1236:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1233
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1234
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1235
	call	_ZdlPv@PLT
.L1235:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1233:
	cmpq	%rbx, %r13
	jne	.L1236
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_2(%rip), %r12
	testq	%r12, %r12
	je	.L2134
.L1222:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1224
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2135
.L1224:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1228:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1225
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1226
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1227
	call	_ZdlPv@PLT
.L1227:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1225:
	cmpq	%rbx, %r13
	jne	.L1228
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_1(%rip), %r12
	testq	%r12, %r12
	je	.L2136
.L1214:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1216
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2137
.L1216:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1220:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1217
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1218
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1219
	call	_ZdlPv@PLT
.L1219:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1217:
	cmpq	%rbx, %r13
	jne	.L1220
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_0(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2138
.L1211:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC19(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175(%rip), %r12
	testq	%r12, %r12
	je	.L2139
.L1201:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1204
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2140
.L1204:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1208:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1205
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1206
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1207
	call	_ZdlPv@PLT
.L1207:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1205:
	cmpq	%rbx, %r13
	jne	.L1208
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__38_(%rip), %r12
	testq	%r12, %r12
	je	.L2141
.L1455:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1457
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2142
.L1457:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1461:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1458
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1459
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1460
	call	_ZdlPv@PLT
.L1460:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1458:
	cmpq	%rbx, %r13
	jne	.L1461
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_9(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2143
.L1268:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC28(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_8(%rip), %r12
	testq	%r12, %r12
	je	.L2144
.L1260:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1262
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2145
.L1262:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1266:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1263
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1264
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1265
	call	_ZdlPv@PLT
.L1265:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1263:
	cmpq	%rbx, %r13
	jne	.L1266
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__45_(%rip), %r12
	testq	%r12, %r12
	je	.L2146
.L1506:
	testb	$5, (%r12)
	je	.L1150
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1508
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2147
.L1508:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1512:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1509
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1510
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1511
	call	_ZdlPv@PLT
.L1511:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1509:
	cmpq	%rbx, %r13
	jne	.L1512
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__10_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2148
.L1271:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC29(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_7(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2149
.L1257:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC26(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_6(%rip), %rsi
	testq	%rsi, %rsi
	je	.L2150
.L1254:
	testb	$5, (%rsi)
	je	.L1150
	cvttsd2siq	-88(%rbp), %rcx
	movl	$2, %r8d
	movl	$101, %edi
	leaq	.LC25(%rip), %rdx
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	jmp	.L1150
.L2110:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1278
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2151
.L1278:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__12_(%rip)
	mfence
	jmp	.L1277
.L2107:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1464
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2152
.L1464:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__39_(%rip)
	mfence
	jmp	.L1463
.L2150:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1255
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2153
.L1255:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_6(%rip)
	mfence
	jmp	.L1254
.L2144:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1261
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2154
.L1261:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_8(%rip)
	mfence
	jmp	.L1260
.L2134:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1223
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2155
.L1223:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_2(%rip)
	mfence
	jmp	.L1222
.L2130:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1239
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2156
.L1239:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_4(%rip)
	mfence
	jmp	.L1238
.L2126:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1316
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2157
.L1316:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__18_(%rip)
	mfence
	jmp	.L1315
.L2146:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1507
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2158
.L1507:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__45_(%rip)
	mfence
	jmp	.L1506
.L2138:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1212
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2159
.L1212:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_0(%rip)
	mfence
	jmp	.L1211
.L2143:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1269
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2160
.L1269:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_9(%rip)
	mfence
	jmp	.L1268
.L2148:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1272
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2161
.L1272:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__10_(%rip)
	mfence
	jmp	.L1271
.L2149:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1258
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2162
.L1258:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_7(%rip)
	mfence
	jmp	.L1257
.L2090:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1386
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2163
.L1386:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__28_(%rip)
	mfence
	jmp	.L1385
.L2089:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1394
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2164
.L1394:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__29_(%rip)
	mfence
	jmp	.L1393
.L2096:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1362
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2165
.L1362:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__25_(%rip)
	mfence
	jmp	.L1361
.L2100:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1348
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2166
.L1348:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__22_(%rip)
	mfence
	jmp	.L1347
.L2098:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1351
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2167
.L1351:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__23_(%rip)
	mfence
	jmp	.L1350
.L2104:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1400
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2168
.L1400:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__31_(%rip)
	mfence
	jmp	.L1399
.L2118:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1284
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2169
.L1284:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__14_(%rip)
	mfence
	jmp	.L1283
.L2114:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1308
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2170
.L1308:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__17_(%rip)
	mfence
	jmp	.L1307
.L2116:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1292
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2171
.L1292:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__15_(%rip)
	mfence
	jmp	.L1291
.L2124:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1332
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2172
.L1332:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__20_(%rip)
	mfence
	jmp	.L1331
.L2101:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1359
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2173
.L1359:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__24_(%rip)
	mfence
	jmp	.L1358
.L2122:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1324
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2174
.L1324:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__19_(%rip)
	mfence
	jmp	.L1323
.L2141:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1456
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2175
.L1456:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__38_(%rip)
	mfence
	jmp	.L1455
.L2106:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1397
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2176
.L1397:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__30_(%rip)
	mfence
	jmp	.L1396
.L2136:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1215
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2177
.L1215:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_1(%rip)
	mfence
	jmp	.L1214
.L2128:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1247
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2178
.L1247:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_5(%rip)
	mfence
	jmp	.L1246
.L2132:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1231
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2179
.L1231:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_3(%rip)
	mfence
	jmp	.L1230
.L2120:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1300
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2180
.L1300:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__16_(%rip)
	mfence
	jmp	.L1299
.L2112:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1472
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2181
.L1472:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__40_(%rip)
	mfence
	jmp	.L1471
.L2109:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1281
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2182
.L1281:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__13_(%rip)
	mfence
	jmp	.L1280
.L2094:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1370
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2183
.L1370:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__26_(%rip)
	mfence
	jmp	.L1369
.L2092:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1378
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2184
.L1378:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__27_(%rip)
	mfence
	jmp	.L1377
.L2087:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1408
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2185
.L1408:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__32_(%rip)
	mfence
	jmp	.L1407
.L2139:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1202
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2186
.L1202:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175(%rip)
	mfence
	jmp	.L1201
.L2074:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1488
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2187
.L1488:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__42_(%rip)
	mfence
	jmp	.L1487
.L2111:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1275
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2188
.L1275:
	movq	%rsi, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__11_(%rip)
	mfence
	jmp	.L1274
.L2072:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1491
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2189
.L1491:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__43_(%rip)
	mfence
	jmp	.L1490
.L2102:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1340
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2190
.L1340:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__21_(%rip)
	mfence
	jmp	.L1339
.L2083:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1424
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2191
.L1424:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__34_(%rip)
	mfence
	jmp	.L1423
.L2079:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1440
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2192
.L1440:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__36_(%rip)
	mfence
	jmp	.L1439
.L2075:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1480
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2193
.L1480:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__41_(%rip)
	mfence
	jmp	.L1479
.L2070:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1500
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %rbx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2194
.L1500:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L1504:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L1501
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1502
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L1503
	call	_ZdlPv@PLT
.L1503:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1501:
	cmpq	%rbx, %r13
	jne	.L1504
	jmp	.L1150
.L2069:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1499
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2195
.L1499:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__44_(%rip)
	mfence
	jmp	.L1498
.L2077:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1448
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2196
.L1448:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__37_(%rip)
	mfence
	jmp	.L1447
.L2081:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1432
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2197
.L1432:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__35_(%rip)
	mfence
	jmp	.L1431
.L2085:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2198
.L1416:
	movq	%r12, _ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__33_(%rip)
	mfence
	jmp	.L1415
.L1502:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1501
.L1510:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1509
.L1242:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1241
.L1319:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1318
.L1264:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1263
.L1226:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1225
.L1467:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1466
.L1335:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1334
.L1295:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1294
.L1287:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1286
.L1311:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1310
.L1403:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1402
.L1354:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1353
.L1365:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1364
.L1389:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1388
.L1419:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1418
.L1435:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1434
.L1451:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1450
.L1443:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1442
.L1483:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1482
.L1343:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1342
.L1427:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1426
.L1494:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1493
.L1206:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1205
.L1381:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1380
.L1411:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1410
.L1373:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1372
.L1475:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1474
.L1303:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1302
.L1234:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1233
.L1250:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1249
.L1218:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1217
.L1459:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1458
.L1327:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L1326
.L2174:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1324
.L2195:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1499
.L2192:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1440
.L2168:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1400
.L2167:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1351
.L2197:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1432
.L2196:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1448
.L2166:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1348
.L2181:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1472
.L2180:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1300
.L2177:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1215
.L2175:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1456
.L2176:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1397
.L2179:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1231
.L2178:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1247
.L2191:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1424
.L2189:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1491
.L2193:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1480
.L2190:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1340
.L2187:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1488
.L2188:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1275
.L2186:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1202
.L2184:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1378
.L2182:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1281
.L2185:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1408
.L2183:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1370
.L2165:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1362
.L2163:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1386
.L2164:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1394
.L2160:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1269
.L2161:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1272
.L2162:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1258
.L2159:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1212
.L2158:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1507
.L2156:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1239
.L2198:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1416
.L2169:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1284
.L2170:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1308
.L2155:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1223
.L2152:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1464
.L2151:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1278
.L2153:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1255
.L2172:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1332
.L2171:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1292
.L2157:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1316
.L2154:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1261
.L2173:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1359
.L2194:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC63(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1500
.L2125:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC39(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1333
.L2117:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC34(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1293
.L2119:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC33(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1285
.L2073:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC62(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1492
.L2093:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC46(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1379
.L2095:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC45(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1371
.L2086:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC52(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1417
.L2088:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC51(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1409
.L2115:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC36(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1309
.L2121:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC35(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1301
.L2113:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC59(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1473
.L2091:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC47(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1387
.L2145:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC27(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1262
.L2147:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC64(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1508
.L2076:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC60(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1481
.L2105:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC50(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1401
.L2108:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC58(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1465
.L2097:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC44(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1363
.L2142:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC57(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1457
.L2129:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC24(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1248
.L2131:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC23(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1240
.L2133:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC22(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1232
.L2135:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC21(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1224
.L2137:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC20(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1216
.L2140:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC18(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1204
.L2123:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC38(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1325
.L2127:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC37(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1317
.L2078:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC56(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1449
.L2080:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC55(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1441
.L2099:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC42(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1352
.L2103:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC40(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1341
.L2082:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC54(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1433
.L2084:
	subq	$8, %rsp
	cvttsd2siq	-88(%rbp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$2
	leaq	.LC53(%rip), %rcx
	movl	$101, %esi
	pushq	%rbx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1425
.L1151:
	leaq	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2071:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7695:
	.size	_ZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEd, .-_ZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEd
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap9EmitAfterEPNS_11EnvironmentEd
	.type	_ZN4node9AsyncWrap9EmitAfterEPNS_11EnvironmentEd, @function
_ZN4node9AsyncWrap9EmitAfterEPNS_11EnvironmentEd:
.LFB7696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$144, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1216(%rdi), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L2207
.L2199:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2208
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2207:
	.cfi_restore_state
	movq	2688(%rdi), %r12
	movq	%rdi, %rbx
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	je	.L2199
	movsd	%xmm0, -168(%rbp)
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L2199
	movq	352(%rdi), %rsi
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movsd	-168(%rbp), %xmm0
	movq	352(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	352(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, -152(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	352(%rbx), %rax
	movl	$1, %ecx
	movq	3280(%rbx), %rsi
	leaq	-152(%rbp), %r8
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	leaq	88(%rax), %rdx
	movl	$1, -56(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r14, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2199
.L2208:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7696:
	.size	_ZN4node9AsyncWrap9EmitAfterEPNS_11EnvironmentEd, .-_ZN4node9AsyncWrap9EmitAfterEPNS_11EnvironmentEd
	.section	.rodata.str1.1
.LC65:
	.string	"AsyncWrap"
.LC66:
	.string	"getAsyncId"
.LC67:
	.string	"asyncReset"
.LC68:
	.string	"getProviderType"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE
	.type	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE, @function
_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE:
.LFB7729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	3064(%rdi), %r12
	testq	%r12, %r12
	je	.L2224
.L2210:
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2224:
	.cfi_restore_state
	subq	$8, %rsp
	movq	2680(%rdi), %rdx
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	movq	352(%rdi), %rdi
	pushq	$0
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	leaq	.LC65(%rip), %rsi
	xorl	%edx, %edx
	movl	$9, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2225
.L2211:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node9AsyncWrap10GetAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC66(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2226
.L2212:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC67(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2227
.L2213:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node9AsyncWrap15GetProviderTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC68(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L2228
.L2214:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3064(%rbx), %rdi
	movq	352(%rbx), %r13
	testq	%rdi, %rdi
	je	.L2215
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3064(%rbx)
.L2215:
	testq	%r12, %r12
	je	.L2210
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3064(%rbx)
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2225:
	.cfi_restore_state
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rsi
	jmp	.L2211
	.p2align 4,,10
	.p2align 3
.L2228:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2214
	.p2align 4,,10
	.p2align 3
.L2227:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2213
	.p2align 4,,10
	.p2align 3
.L2226:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2212
	.cfi_endproc
.LFE7729:
	.size	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE, .-_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE
	.section	.rodata.str1.1
.LC69:
	.string	"setupHooks"
.LC70:
	.string	"setCallbackTrampoline"
.LC71:
	.string	"pushAsyncContext"
.LC72:
	.string	"popAsyncContext"
.LC73:
	.string	"queueDestroyAsyncId"
.LC74:
	.string	"enablePromiseHook"
.LC75:
	.string	"disablePromiseHook"
.LC76:
	.string	"registerDestroyHook"
.LC77:
	.string	"async_hook_fields"
.LC78:
	.string	"async_id_fields"
.LC79:
	.string	"execution_async_resources"
.LC80:
	.string	"kInit"
.LC81:
	.string	"kBefore"
.LC82:
	.string	"kAfter"
.LC83:
	.string	"kDestroy"
.LC84:
	.string	"kPromiseResolve"
.LC85:
	.string	"kTotals"
.LC86:
	.string	"kCheck"
.LC87:
	.string	"kExecutionAsyncId"
.LC88:
	.string	"kTriggerAsyncId"
.LC89:
	.string	"kAsyncIdCounter"
.LC90:
	.string	"kDefaultTriggerAsyncId"
.LC91:
	.string	"kStackLength"
.LC92:
	.string	"constants"
.LC93:
	.string	"NONE"
.LC94:
	.string	"DIRHANDLE"
.LC95:
	.string	"DNSCHANNEL"
.LC96:
	.string	"ELDHISTOGRAM"
.LC97:
	.string	"FILEHANDLE"
.LC98:
	.string	"FILEHANDLECLOSEREQ"
.LC99:
	.string	"FSEVENTWRAP"
.LC100:
	.string	"FSREQCALLBACK"
.LC101:
	.string	"FSREQPROMISE"
.LC102:
	.string	"GETADDRINFOREQWRAP"
.LC103:
	.string	"GETNAMEINFOREQWRAP"
.LC104:
	.string	"HEAPSNAPSHOT"
.LC105:
	.string	"HTTP2SESSION"
.LC106:
	.string	"HTTP2STREAM"
.LC107:
	.string	"HTTP2PING"
.LC108:
	.string	"HTTP2SETTINGS"
.LC109:
	.string	"HTTPINCOMINGMESSAGE"
.LC110:
	.string	"HTTPCLIENTREQUEST"
.LC111:
	.string	"JSSTREAM"
.LC112:
	.string	"MESSAGEPORT"
.LC113:
	.string	"PIPECONNECTWRAP"
.LC114:
	.string	"PIPESERVERWRAP"
.LC115:
	.string	"PIPEWRAP"
.LC116:
	.string	"PROCESSWRAP"
.LC117:
	.string	"PROMISE"
.LC118:
	.string	"QUERYWRAP"
.LC119:
	.string	"SHUTDOWNWRAP"
.LC120:
	.string	"SIGNALWRAP"
.LC121:
	.string	"STATWATCHER"
.LC122:
	.string	"STREAMPIPE"
.LC123:
	.string	"TCPCONNECTWRAP"
.LC124:
	.string	"TCPSERVERWRAP"
.LC125:
	.string	"TCPWRAP"
.LC126:
	.string	"TTYWRAP"
.LC127:
	.string	"UDPSENDWRAP"
.LC128:
	.string	"UDPWRAP"
.LC129:
	.string	"SIGINTWATCHDOG"
.LC130:
	.string	"WORKER"
.LC131:
	.string	"WORKERHEAPSNAPSHOT"
.LC132:
	.string	"WRITEWRAP"
.LC133:
	.string	"ZLIB"
.LC134:
	.string	"PBKDF2REQUEST"
.LC135:
	.string	"KEYPAIRGENREQUEST"
.LC136:
	.string	"RANDOMBYTESREQUEST"
.LC137:
	.string	"SCRYPTREQUEST"
.LC138:
	.string	"TLSWRAP"
.LC139:
	.string	"INSPECTORJSBINDING"
.LC140:
	.string	"Providers"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB141:
	.text
.LHOTB141:
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L2230
	movq	%rdi, %r15
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2230
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L2230
	movq	271(%rax), %rbx
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	movq	352(%rbx), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	%rax, -96(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2443
.L2231:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC69(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L2444
.L2232:
	movq	-96(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-88(%rbp), %r8
	testb	%al, %al
	je	.L2445
.L2233:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	%rax, -88(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2446
.L2234:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC70(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2447
.L2235:
	movq	-88(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-96(%rbp), %r9
	testb	%al, %al
	je	.L2448
.L2236:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node9AsyncWrap16PushAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2449
.L2237:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC71(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2450
.L2238:
	movq	-88(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-96(%rbp), %r9
	testb	%al, %al
	je	.L2451
.L2239:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	%rax, -88(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node9AsyncWrap15PopAsyncContextERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L2452
.L2240:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC72(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2453
.L2241:
	movq	-88(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-96(%rbp), %r9
	testb	%al, %al
	je	.L2454
.L2242:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	%rax, -88(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	movq	%rax, %r14
	popq	%rax
	testq	%r14, %r14
	je	.L2455
.L2243:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC73(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2456
.L2244:
	movq	-88(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-96(%rbp), %r9
	testb	%al, %al
	je	.L2457
.L2245:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	%rax, -88(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4nodeL17EnablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2458
.L2246:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC74(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2459
.L2247:
	movq	-88(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-96(%rbp), %r9
	testb	%al, %al
	je	.L2460
.L2248:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	%rax, -88(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4nodeL18DisablePromiseHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2461
.L2249:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC75(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2462
.L2250:
	movq	-88(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-96(%rbp), %r9
	testb	%al, %al
	je	.L2463
.L2251:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2464
.L2252:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC76(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L2465
.L2253:
	movq	-88(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	%r9, -96(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-96(%rbp), %r9
	testb	%al, %al
	je	.L2466
.L2254:
	movq	%r14, %rdi
	movq	%r9, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	1224(%rbx), %r14
	testq	%r14, %r14
	je	.L2255
	movq	(%r14), %rsi
	movq	1192(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
.L2255:
	xorl	%edx, %edx
	movl	$17, %ecx
	leaq	.LC77(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2467
.L2256:
	movl	$5, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2468
.L2257:
	movq	1264(%rbx), %r14
	testq	%r14, %r14
	je	.L2258
	movq	(%r14), %rsi
	movq	1232(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
.L2258:
	xorl	%edx, %edx
	movl	$15, %ecx
	leaq	.LC78(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2469
.L2259:
	movl	$5, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2470
.L2260:
	xorl	%edx, %edx
	movl	$25, %ecx
	movq	%r12, %rdi
	movq	1272(%rbx), %r14
	leaq	.LC79(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2471
.L2261:
	movl	$5, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2472
.L2262:
	movq	1184(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L2263
	movq	(%rcx), %rsi
	movq	1152(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
.L2263:
	movq	360(%rbx), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movq	216(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2473
.L2264:
	movq	%r12, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$5, %ecx
	movq	%r12, %rdi
	leaq	.LC80(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2474
.L2265:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2475
.L2266:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	.LC81(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2476
.L2267:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2477
.L2268:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$6, %ecx
	movq	%r12, %rdi
	leaq	.LC82(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2478
.L2269:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2479
.L2270:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	leaq	.LC83(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2480
.L2271:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2481
.L2272:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$15, %ecx
	movq	%r12, %rdi
	leaq	.LC84(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2482
.L2273:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2483
.L2274:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	.LC85(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2484
.L2275:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2485
.L2276:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$6, %ecx
	movq	%r12, %rdi
	leaq	.LC86(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2486
.L2277:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2487
.L2278:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$17, %ecx
	movq	%r12, %rdi
	leaq	.LC87(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2488
.L2279:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2489
.L2280:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$15, %ecx
	movq	%r12, %rdi
	leaq	.LC88(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2490
.L2281:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2491
.L2282:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$15, %ecx
	movq	%r12, %rdi
	leaq	.LC89(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2492
.L2283:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2493
.L2284:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$22, %ecx
	movq	%r12, %rdi
	leaq	.LC90(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2494
.L2285:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2495
.L2286:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%r12, %rdi
	leaq	.LC91(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2496
.L2287:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2497
.L2288:
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC92(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2498
.L2289:
	movl	$5, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2499
.L2290:
	movq	%r12, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r12, %rdi
	leaq	.LC93(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2500
.L2291:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2501
.L2292:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$9, %ecx
	movq	%r12, %rdi
	leaq	.LC94(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2502
.L2293:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2503
.L2294:
	movl	$2, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r12, %rdi
	leaq	.LC95(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2504
.L2295:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2505
.L2296:
	movl	$3, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%r12, %rdi
	leaq	.LC96(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2506
.L2297:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2507
.L2298:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r12, %rdi
	leaq	.LC97(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2508
.L2299:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2509
.L2300:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%r12, %rdi
	leaq	.LC98(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2510
.L2301:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2511
.L2302:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r12, %rdi
	leaq	.LC99(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2512
.L2303:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2513
.L2304:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$13, %ecx
	movq	%r12, %rdi
	leaq	.LC100(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2514
.L2305:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2515
.L2306:
	movl	$8, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%r12, %rdi
	leaq	.LC101(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2516
.L2307:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2517
.L2308:
	movl	$9, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%r12, %rdi
	leaq	.LC102(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2518
.L2309:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2519
.L2310:
	movl	$10, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%r12, %rdi
	leaq	.LC103(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2520
.L2311:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2521
.L2312:
	movl	$11, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%r12, %rdi
	leaq	.LC104(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2522
.L2313:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2523
.L2314:
	movl	$12, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%r12, %rdi
	leaq	.LC105(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2524
.L2315:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2525
.L2316:
	movl	$13, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r12, %rdi
	leaq	.LC106(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2526
.L2317:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2527
.L2318:
	movl	$14, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$9, %ecx
	movq	%r12, %rdi
	leaq	.LC107(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2528
.L2319:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2529
.L2320:
	movl	$15, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$13, %ecx
	movq	%r12, %rdi
	leaq	.LC108(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2530
.L2321:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2531
.L2322:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$19, %ecx
	movq	%r12, %rdi
	leaq	.LC109(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2532
.L2323:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2533
.L2324:
	movl	$17, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$17, %ecx
	movq	%r12, %rdi
	leaq	.LC110(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2534
.L2325:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2535
.L2326:
	movl	$18, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	leaq	.LC111(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2536
.L2327:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2537
.L2328:
	movl	$19, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r12, %rdi
	leaq	.LC112(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2538
.L2329:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2539
.L2330:
	movl	$20, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$15, %ecx
	movq	%r12, %rdi
	leaq	.LC113(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2540
.L2331:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2541
.L2332:
	movl	$21, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	leaq	.LC114(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2542
.L2333:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2543
.L2334:
	movl	$22, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$8, %ecx
	movq	%r12, %rdi
	leaq	.LC115(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2544
.L2335:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2545
.L2336:
	movl	$23, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r12, %rdi
	leaq	.LC116(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2546
.L2337:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2547
.L2338:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	.LC117(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2548
.L2339:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2549
.L2340:
	movl	$25, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$9, %ecx
	movq	%r12, %rdi
	leaq	.LC118(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2550
.L2341:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2551
.L2342:
	movl	$26, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$12, %ecx
	movq	%r12, %rdi
	leaq	.LC119(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2552
.L2343:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2553
.L2344:
	movl	$27, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r12, %rdi
	leaq	.LC120(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2554
.L2345:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2555
.L2346:
	movl	$28, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r12, %rdi
	leaq	.LC121(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2556
.L2347:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2557
.L2348:
	movl	$29, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r12, %rdi
	leaq	.LC122(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2558
.L2349:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2559
.L2350:
	movl	$30, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	leaq	.LC123(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2560
.L2351:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2561
.L2352:
	movl	$31, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$13, %ecx
	movq	%r12, %rdi
	leaq	.LC124(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2562
.L2353:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2563
.L2354:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	.LC125(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2564
.L2355:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2565
.L2356:
	movl	$33, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	.LC126(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2566
.L2357:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2567
.L2358:
	movl	$34, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$11, %ecx
	movq	%r12, %rdi
	leaq	.LC127(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2568
.L2359:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2569
.L2360:
	movl	$35, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	.LC128(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2570
.L2361:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2571
.L2362:
	movl	$36, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	leaq	.LC129(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2572
.L2363:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2573
.L2364:
	movl	$37, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$6, %ecx
	movq	%r12, %rdi
	leaq	.LC130(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2574
.L2365:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2575
.L2366:
	movl	$38, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%r12, %rdi
	leaq	.LC131(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2576
.L2367:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2577
.L2368:
	movl	$39, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$9, %ecx
	movq	%r12, %rdi
	leaq	.LC132(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2578
.L2369:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2579
.L2370:
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r12, %rdi
	leaq	.LC133(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2580
.L2371:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2581
.L2372:
	movl	$41, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$13, %ecx
	movq	%r12, %rdi
	leaq	.LC134(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2582
.L2373:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2583
.L2374:
	movl	$42, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$17, %ecx
	movq	%r12, %rdi
	leaq	.LC135(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2584
.L2375:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2585
.L2376:
	movl	$43, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%r12, %rdi
	leaq	.LC136(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2586
.L2377:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2587
.L2378:
	movl	$44, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$13, %ecx
	movq	%r12, %rdi
	leaq	.LC137(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2588
.L2379:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2589
.L2380:
	movl	$45, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%r12, %rdi
	leaq	.LC138(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2590
.L2381:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2591
.L2382:
	movl	$46, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%r12, %rdi
	leaq	.LC139(%rip), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2592
.L2383:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2593
.L2384:
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC140(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2594
.L2385:
	movl	$5, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L2595
.L2386:
	movq	2728(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2387
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2728(%rbx)
.L2387:
	movq	2696(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2388
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2696(%rbx)
.L2388:
	movq	2688(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2389
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2688(%rbx)
.L2389:
	movq	2720(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2390
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2720(%rbx)
.L2390:
	movq	2736(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2391
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2736(%rbx)
.L2391:
	movq	2712(%rbx), %rdi
	movq	352(%rbx), %r12
	testq	%rdi, %rdi
	je	.L2392
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2712(%rbx)
.L2392:
	testq	%r15, %r15
	je	.L2393
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2712(%rbx)
.L2393:
	movq	3072(%rbx), %r12
	testq	%r12, %r12
	je	.L2596
.L2394:
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2597
.L2398:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC65(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2598
.L2399:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2599
.L2400:
	movq	-104(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2600
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2596:
	.cfi_restore_state
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC65(%rip), %rsi
	movl	$9, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L2601
.L2395:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3072(%rbx), %rdi
	movq	352(%rbx), %r13
	testq	%rdi, %rdi
	je	.L2396
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3072(%rbx)
.L2396:
	testq	%r12, %r12
	je	.L2394
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3072(%rbx)
	jmp	.L2394
	.p2align 4,,10
	.p2align 3
.L2597:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2398
	.p2align 4,,10
	.p2align 3
.L2598:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L2399
	.p2align 4,,10
	.p2align 3
.L2599:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2400
	.p2align 4,,10
	.p2align 3
.L2443:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2231
	.p2align 4,,10
	.p2align 3
.L2444:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %r8
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2445:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r8
	jmp	.L2233
	.p2align 4,,10
	.p2align 3
.L2446:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2234
	.p2align 4,,10
	.p2align 3
.L2447:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2448:
	movq	%r9, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r9
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2449:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2237
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	jmp	.L2238
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	%r9, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r9
	jmp	.L2239
	.p2align 4,,10
	.p2align 3
.L2452:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2453:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2454:
	movq	%r9, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r9
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2455:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2243
	.p2align 4,,10
	.p2align 3
.L2456:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2457:
	movq	%r9, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r9
	jmp	.L2245
	.p2align 4,,10
	.p2align 3
.L2458:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2246
	.p2align 4,,10
	.p2align 3
.L2459:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	jmp	.L2247
	.p2align 4,,10
	.p2align 3
.L2460:
	movq	%r9, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r9
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2461:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2249
	.p2align 4,,10
	.p2align 3
.L2462:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	jmp	.L2250
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	%r9, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r9
	jmp	.L2251
	.p2align 4,,10
	.p2align 3
.L2464:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	jmp	.L2253
	.p2align 4,,10
	.p2align 3
.L2466:
	movq	%r9, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %r9
	jmp	.L2254
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2468:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2257
	.p2align 4,,10
	.p2align 3
.L2469:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L2259
	.p2align 4,,10
	.p2align 3
.L2470:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2260
	.p2align 4,,10
	.p2align 3
.L2471:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L2261
	.p2align 4,,10
	.p2align 3
.L2472:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2262
	.p2align 4,,10
	.p2align 3
.L2473:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2264
	.p2align 4,,10
	.p2align 3
.L2474:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2265
	.p2align 4,,10
	.p2align 3
.L2475:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2476:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2267
	.p2align 4,,10
	.p2align 3
.L2477:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2478:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2269
	.p2align 4,,10
	.p2align 3
.L2479:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2270
	.p2align 4,,10
	.p2align 3
.L2480:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2271
	.p2align 4,,10
	.p2align 3
.L2481:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2272
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2483:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2484:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2275
	.p2align 4,,10
	.p2align 3
.L2485:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2276
	.p2align 4,,10
	.p2align 3
.L2486:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2277
	.p2align 4,,10
	.p2align 3
.L2487:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2278
	.p2align 4,,10
	.p2align 3
.L2488:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2279
	.p2align 4,,10
	.p2align 3
.L2489:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2280
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2281
	.p2align 4,,10
	.p2align 3
.L2491:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2282
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2283
	.p2align 4,,10
	.p2align 3
.L2493:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2494:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2285
	.p2align 4,,10
	.p2align 3
.L2495:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2496:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2287
	.p2align 4,,10
	.p2align 3
.L2497:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2288
	.p2align 4,,10
	.p2align 3
.L2498:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L2289
	.p2align 4,,10
	.p2align 3
.L2499:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2500:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2291
	.p2align 4,,10
	.p2align 3
.L2501:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2293
	.p2align 4,,10
	.p2align 3
.L2503:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2294
	.p2align 4,,10
	.p2align 3
.L2504:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2295
	.p2align 4,,10
	.p2align 3
.L2505:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2296
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2297
	.p2align 4,,10
	.p2align 3
.L2507:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2298
	.p2align 4,,10
	.p2align 3
.L2508:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2299
	.p2align 4,,10
	.p2align 3
.L2509:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2510:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2511:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2302
	.p2align 4,,10
	.p2align 3
.L2512:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2303
	.p2align 4,,10
	.p2align 3
.L2513:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2514:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2305
	.p2align 4,,10
	.p2align 3
.L2515:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2516:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2307
	.p2align 4,,10
	.p2align 3
.L2517:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2518:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2519:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2520:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2521:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2312
	.p2align 4,,10
	.p2align 3
.L2522:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2523:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2314
	.p2align 4,,10
	.p2align 3
.L2524:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2525:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2316
	.p2align 4,,10
	.p2align 3
.L2526:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2317
	.p2align 4,,10
	.p2align 3
.L2527:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2528:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2319
	.p2align 4,,10
	.p2align 3
.L2529:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2530:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2321
	.p2align 4,,10
	.p2align 3
.L2531:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2532:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2323
	.p2align 4,,10
	.p2align 3
.L2533:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2534:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L2535:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2536:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2537:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2538:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2329
	.p2align 4,,10
	.p2align 3
.L2539:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2540:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2331
	.p2align 4,,10
	.p2align 3
.L2541:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2332
	.p2align 4,,10
	.p2align 3
.L2542:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2333
	.p2align 4,,10
	.p2align 3
.L2543:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2334
	.p2align 4,,10
	.p2align 3
.L2544:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2335
	.p2align 4,,10
	.p2align 3
.L2545:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2336
	.p2align 4,,10
	.p2align 3
.L2546:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2337
	.p2align 4,,10
	.p2align 3
.L2547:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2338
	.p2align 4,,10
	.p2align 3
.L2548:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2339
	.p2align 4,,10
	.p2align 3
.L2549:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2340
	.p2align 4,,10
	.p2align 3
.L2550:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2341
	.p2align 4,,10
	.p2align 3
.L2551:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2342
	.p2align 4,,10
	.p2align 3
.L2552:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2553:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2554:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2345
	.p2align 4,,10
	.p2align 3
.L2555:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2346
	.p2align 4,,10
	.p2align 3
.L2556:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2347
	.p2align 4,,10
	.p2align 3
.L2557:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2558:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2349
	.p2align 4,,10
	.p2align 3
.L2559:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2350
	.p2align 4,,10
	.p2align 3
.L2560:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2351
	.p2align 4,,10
	.p2align 3
.L2561:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2352
	.p2align 4,,10
	.p2align 3
.L2562:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2563:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2354
	.p2align 4,,10
	.p2align 3
.L2564:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2355
	.p2align 4,,10
	.p2align 3
.L2565:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2566:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2357
	.p2align 4,,10
	.p2align 3
.L2567:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2358
	.p2align 4,,10
	.p2align 3
.L2568:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2359
	.p2align 4,,10
	.p2align 3
.L2569:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2570:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2361
	.p2align 4,,10
	.p2align 3
.L2571:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2572:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2363
	.p2align 4,,10
	.p2align 3
.L2573:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2574:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2575:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2366
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2367
	.p2align 4,,10
	.p2align 3
.L2577:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2368
	.p2align 4,,10
	.p2align 3
.L2578:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2579:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2370
	.p2align 4,,10
	.p2align 3
.L2580:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2371
	.p2align 4,,10
	.p2align 3
.L2581:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2582:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2373
	.p2align 4,,10
	.p2align 3
.L2583:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2374
	.p2align 4,,10
	.p2align 3
.L2584:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2585:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2376
	.p2align 4,,10
	.p2align 3
.L2586:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2587:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2378
	.p2align 4,,10
	.p2align 3
.L2588:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2379
	.p2align 4,,10
	.p2align 3
.L2589:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2590:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2381
	.p2align 4,,10
	.p2align 3
.L2591:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2592:
	movq	%r9, -96(%rbp)
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rdx
	jmp	.L2383
	.p2align 4,,10
	.p2align 3
.L2593:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2384
	.p2align 4,,10
	.p2align 3
.L2594:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2595:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2386
	.p2align 4,,10
	.p2align 3
.L2601:
	movq	%rsi, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L2395
.L2600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7730:
.L2230:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7730:
	.text
	.size	_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE141:
	.text
.LHOTE141:
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L2603
	movq	%rdx, %rsi
	movq	%rdx, %r13
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2641
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %r14
	movl	$40, %edi
	leaq	1(%r14), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r10
	movq	2592(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r10, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r14, 24(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%r8, %r8
	je	.L2605
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L2606:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2605
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L2605
	movq	%rdi, %rax
.L2608:
	cmpq	%rcx, %r12
	jne	.L2606
	cmpq	%r10, 8(%rax)
	jne	.L2606
	cmpq	16(%rax), %r12
	jne	.L2606
	cmpq	$0, (%r8)
	je	.L2605
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2605:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L2609
	movq	2584(%rbx), %r8
	movq	%r12, 32(%r13)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L2619
.L2644:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r15), %rax
	movq	%r13, (%rax)
.L2620:
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	addq	$1, 2656(%rbx)
	movq	%rax, (%r12)
	movq	.LC7(%rip), %rax
	addq	$1, 2608(%rbx)
	movl	$0, 32(%r12)
	movb	$0, 36(%r12)
	movq	%rax, 40(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2609:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L2642
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2643
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%rbx), %r10
	movq	%rax, %r8
.L2612:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L2614
	xorl	%edi, %edi
	leaq	2600(%rbx), %r9
	jmp	.L2615
	.p2align 4,,10
	.p2align 3
.L2616:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2617:
	testq	%rsi, %rsi
	je	.L2614
.L2615:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L2616
	movq	2600(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L2623
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2615
	.p2align 4,,10
	.p2align 3
.L2614:
	movq	2584(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L2618
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L2618:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r13)
	divq	%r14
	movq	%r14, 2592(%rbx)
	movq	%r8, 2584(%rbx)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L2644
.L2619:
	movq	2600(%rbx), %rax
	movq	%r13, 2600(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L2621
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r8,%rdx,8)
.L2621:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2623:
	movq	%rdx, %rdi
	jmp	.L2617
	.p2align 4,,10
	.p2align 3
.L2603:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2641:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2642:
	leaq	2632(%rbx), %r8
	movq	$0, 2632(%rbx)
	movq	%r8, %r10
	jmp	.L2612
.L2643:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7738:
	.size	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.globl	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.set	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE,_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	.type	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv, @function
_ZN4node9AsyncWrap21EmitTraceEventDestroyEv:
.LFB7745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	cmpl	$46, 32(%rdi)
	ja	.L2646
	movl	32(%rdi), %eax
	leaq	.L2648(%rip), %rdx
	movq	%rdi, %rbx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2648:
	.long	.L2694-.L2648
	.long	.L2693-.L2648
	.long	.L2692-.L2648
	.long	.L2691-.L2648
	.long	.L2690-.L2648
	.long	.L2689-.L2648
	.long	.L2688-.L2648
	.long	.L2687-.L2648
	.long	.L2686-.L2648
	.long	.L2685-.L2648
	.long	.L2684-.L2648
	.long	.L2683-.L2648
	.long	.L2682-.L2648
	.long	.L2681-.L2648
	.long	.L2680-.L2648
	.long	.L2679-.L2648
	.long	.L2678-.L2648
	.long	.L2677-.L2648
	.long	.L2676-.L2648
	.long	.L2675-.L2648
	.long	.L2674-.L2648
	.long	.L2673-.L2648
	.long	.L2672-.L2648
	.long	.L2671-.L2648
	.long	.L2670-.L2648
	.long	.L2669-.L2648
	.long	.L2668-.L2648
	.long	.L2667-.L2648
	.long	.L2666-.L2648
	.long	.L2665-.L2648
	.long	.L2664-.L2648
	.long	.L2663-.L2648
	.long	.L2662-.L2648
	.long	.L2661-.L2648
	.long	.L2660-.L2648
	.long	.L2659-.L2648
	.long	.L2658-.L2648
	.long	.L2657-.L2648
	.long	.L2656-.L2648
	.long	.L2655-.L2648
	.long	.L2654-.L2648
	.long	.L2653-.L2648
	.long	.L2652-.L2648
	.long	.L2651-.L2648
	.long	.L2650-.L2648
	.long	.L2649-.L2648
	.long	.L2647-.L2648
	.text
	.p2align 4,,10
	.p2align 3
.L2649:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__44_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3121
.L2832:
	testb	$5, (%rsi)
	jne	.L3122
.L2645:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2650:
	.cfi_restore_state
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__43_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3123
.L2829:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC137(%rip), %rdx
.L3120:
	addq	$8, %rsp
	movl	$101, %edi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	.p2align 4,,10
	.p2align 3
.L2651:
	.cfi_restore_state
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__42_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3124
.L2826:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC136(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2656:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__37_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3125
.L2811:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC131(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2652:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__41_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3126
.L2823:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC135(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2660:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__33_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3127
.L2799:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC127(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2654:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__39_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3128
.L2817:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC133(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2658:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__35_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3129
.L2805:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC129(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2653:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__40_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3130
.L2820:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC134(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2662:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__31_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3131
.L2793:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC125(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2655:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__38_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3132
.L2814:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC132(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2657:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__36_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3133
.L2808:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC130(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2661:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__32_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3134
.L2796:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC126(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__34_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3135
.L2802:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC128(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2663:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__30_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3136
.L2790:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC124(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2664:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__29_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3137
.L2787:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC123(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2680:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__13_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3138
.L2739:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC107(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__21_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3139
.L2763:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC115(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2688:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_5(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3140
.L2715:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC99(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2668:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__25_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3141
.L2775:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC119(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2684:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_9(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3142
.L2727:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC103(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2676:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__17_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3143
.L2751:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC111(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2692:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_1(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3144
.L2703:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC95(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2666:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__27_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3145
.L2781:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC121(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2682:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__11_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3146
.L2733:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC105(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2674:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__19_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3147
.L2757:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC113(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2690:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_3(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3148
.L2709:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC97(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2670:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__23_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3149
.L2769:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC117(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2686:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_7(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3150
.L2721:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC101(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__15_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3151
.L2745:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC109(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2694:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3152
.L2696:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC93(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2665:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__28_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3153
.L2784:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC122(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2681:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__12_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3154
.L2736:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC106(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2673:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__20_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3155
.L2760:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC114(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2689:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_4(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3156
.L2712:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC98(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2669:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__24_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3157
.L2772:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC118(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_8(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3158
.L2724:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC102(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2677:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__16_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3159
.L2748:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC110(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2693:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_0(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3160
.L2700:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC94(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2667:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__26_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3161
.L2778:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC120(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2683:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__10_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3162
.L2730:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC104(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__18_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3163
.L2754:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC112(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_2(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3164
.L2706:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC96(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__22_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3165
.L2766:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC116(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2687:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_6(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3166
.L2718:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC100(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__14_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3167
.L2742:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC108(%rip), %rdx
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L2647:
	movq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__45_(%rip), %rsi
	testq	%rsi, %rsi
	je	.L3168
.L2835:
	testb	$5, (%rsi)
	je	.L2645
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC139(%rip), %rdx
	jmp	.L3120
.L3122:
	cvttsd2siq	40(%rbx), %rcx
	movl	$2, %r8d
	leaq	.LC138(%rip), %rdx
	jmp	.L3120
.L3124:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2827
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3169
.L2827:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__42_(%rip)
	mfence
	jmp	.L2826
.L3125:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2812
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3170
.L2812:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__37_(%rip)
	mfence
	jmp	.L2811
.L3126:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2824
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3171
.L2824:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__41_(%rip)
	mfence
	jmp	.L2823
.L3123:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2830
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3172
.L2830:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__43_(%rip)
	mfence
	jmp	.L2829
.L3128:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2818
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3173
.L2818:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__39_(%rip)
	mfence
	jmp	.L2817
.L3127:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2800
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3174
.L2800:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__33_(%rip)
	mfence
	jmp	.L2799
.L3130:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2821
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3175
.L2821:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__40_(%rip)
	mfence
	jmp	.L2820
.L3121:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2833
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3176
.L2833:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__44_(%rip)
	mfence
	jmp	.L2832
.L3132:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2815
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3177
.L2815:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__38_(%rip)
	mfence
	jmp	.L2814
.L3129:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2806
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3178
.L2806:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__35_(%rip)
	mfence
	jmp	.L2805
.L3133:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2809
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3179
.L2809:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__36_(%rip)
	mfence
	jmp	.L2808
.L3168:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2836
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3180
.L2836:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__45_(%rip)
	mfence
	jmp	.L2835
.L3134:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2797
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3181
.L2797:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__32_(%rip)
	mfence
	jmp	.L2796
.L3131:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2794
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3182
.L2794:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__31_(%rip)
	mfence
	jmp	.L2793
.L3135:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2803
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3183
.L2803:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__34_(%rip)
	mfence
	jmp	.L2802
.L3136:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2791
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3184
.L2791:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__30_(%rip)
	mfence
	jmp	.L2790
.L3138:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2740
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3185
.L2740:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__13_(%rip)
	mfence
	jmp	.L2739
.L3140:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2716
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3186
.L2716:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_5(%rip)
	mfence
	jmp	.L2715
.L3142:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2728
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3187
.L2728:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_9(%rip)
	mfence
	jmp	.L2727
.L3144:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2704
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3188
.L2704:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_1(%rip)
	mfence
	jmp	.L2703
.L3146:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2734
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3189
.L2734:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__11_(%rip)
	mfence
	jmp	.L2733
.L3148:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2710
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3190
.L2710:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_3(%rip)
	mfence
	jmp	.L2709
.L3150:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2722
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3191
.L2722:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_7(%rip)
	mfence
	jmp	.L2721
.L3152:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2697
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3192
.L2697:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655(%rip)
	mfence
	jmp	.L2696
.L3154:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2737
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3193
.L2737:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__12_(%rip)
	mfence
	jmp	.L2736
.L3156:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2713
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3194
.L2713:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_4(%rip)
	mfence
	jmp	.L2712
.L3158:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2725
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3195
.L2725:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_8(%rip)
	mfence
	jmp	.L2724
.L3160:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2701
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3196
.L2701:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_0(%rip)
	mfence
	jmp	.L2700
.L3162:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2731
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3197
.L2731:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__10_(%rip)
	mfence
	jmp	.L2730
.L3164:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2707
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3198
.L2707:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_2(%rip)
	mfence
	jmp	.L2706
.L3166:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2719
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3199
.L2719:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_6(%rip)
	mfence
	jmp	.L2718
.L3137:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2788
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3200
.L2788:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__29_(%rip)
	mfence
	jmp	.L2787
.L3139:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2764
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3201
.L2764:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__21_(%rip)
	mfence
	jmp	.L2763
.L3141:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2776
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3202
.L2776:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__25_(%rip)
	mfence
	jmp	.L2775
.L3143:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2752
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3203
.L2752:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__17_(%rip)
	mfence
	jmp	.L2751
.L3145:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2782
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3204
.L2782:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__27_(%rip)
	mfence
	jmp	.L2781
.L3147:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2758
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3205
.L2758:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__19_(%rip)
	mfence
	jmp	.L2757
.L3149:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2770
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3206
.L2770:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__23_(%rip)
	mfence
	jmp	.L2769
.L3151:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2746
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3207
.L2746:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__15_(%rip)
	mfence
	jmp	.L2745
.L3153:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2785
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3208
.L2785:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__28_(%rip)
	mfence
	jmp	.L2784
.L3155:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2761
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3209
.L2761:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__20_(%rip)
	mfence
	jmp	.L2760
.L3157:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2773
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3210
.L2773:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__24_(%rip)
	mfence
	jmp	.L2772
.L3159:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2749
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3211
.L2749:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__16_(%rip)
	mfence
	jmp	.L2748
.L3161:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2779
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3212
.L2779:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__26_(%rip)
	mfence
	jmp	.L2778
.L3163:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2755
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3213
.L2755:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__18_(%rip)
	mfence
	jmp	.L2754
.L3165:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2767
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3214
.L2767:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__22_(%rip)
	mfence
	jmp	.L2766
.L3167:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2743
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3215
.L2743:
	movq	%rsi, _ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__14_(%rip)
	mfence
	jmp	.L2742
.L3200:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2788
.L3208:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2785
.L3204:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2782
.L3212:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2779
.L3202:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2776
.L3210:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2773
.L3206:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2770
.L3214:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2767
.L3201:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2764
.L3209:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2761
.L3205:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2758
.L3213:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2755
.L3203:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2752
.L3211:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2749
.L3207:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2746
.L3215:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2743
.L3184:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2791
.L3192:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2697
.L3188:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2704
.L3196:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2701
.L3186:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2716
.L3194:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2713
.L3190:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2710
.L3198:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2707
.L3185:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2740
.L3193:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2737
.L3189:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2734
.L3197:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2731
.L3187:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2728
.L3195:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2725
.L3191:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2722
.L3199:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2719
.L3172:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2830
.L3176:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2833
.L3180:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2836
.L3170:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2812
.L3178:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2806
.L3174:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2800
.L3182:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2794
.L3169:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2827
.L3177:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2815
.L3173:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2818
.L3181:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2797
.L3171:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2824
.L3179:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2809
.L3175:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2821
.L3183:
	leaq	.LC17(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L2803
.L2646:
	leaq	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7745:
	.size	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv, .-_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd
	.type	_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd, @function
_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd:
.LFB7751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L3223
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L3224
	movq	1216(%rdi), %rax
	movq	%rdi, %rbx
	movsd	%xmm1, -208(%rbp)
	movsd	%xmm0, -200(%rbp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L3225
.L3216:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3226
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3225:
	.cfi_restore_state
	movq	%rsi, %r13
	leaq	-192(%rbp), %r14
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %rdi
	movsd	-200(%rbp), %xmm0
	movq	2728(%rbx), %r15
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	352(%rbx), %rdi
	movq	%r12, -88(%rbp)
	leaq	-160(%rbp), %r12
	movsd	-208(%rbp), %xmm1
	movq	%rax, -96(%rbp)
	movapd	%xmm1, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	352(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r13, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	leaq	-96(%rbp), %r8
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	3280(%rbx), %rsi
	movl	$4, %ecx
	movq	%rbx, -112(%rbp)
	movl	$1, -104(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3223:
	leaq	_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3224:
	leaq	_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7751:
	.size	_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd, .-_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE
	.type	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE, @function
_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE:
.LFB7753:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	call	_ZN4node9AsyncWrap20EmitTraceEventBeforeEv
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rsi
	movl	32(%rbx), %r13d
	movsd	40(%rbx), %xmm0
	movsd	48(%rbx), %xmm1
	movq	352(%rdi), %r8
	testq	%rsi, %rsi
	je	.L3228
	movzbl	11(%rsi), %eax
	movq	%rsi, %rdx
	andl	$7, %eax
	cmpb	$2, %al
	je	.L3237
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L3238
.L3230:
	movq	%r15, %r9
	movl	%r14d, %r8d
	movq	%r12, %rcx
	movsd	%xmm0, -56(%rbp)
	call	_ZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextE@PLT
	movsd	-56(%rbp), %xmm0
	movl	%r13d, %edi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEd
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3237:
	.cfi_restore_state
	movq	(%rsi), %rsi
	movq	%r8, %rdi
	movq	%xmm1, -64(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdi
	movsd	-56(%rbp), %xmm0
	movq	-64(%rbp), %xmm1
	movq	%rax, %rdx
	testq	%rsi, %rsi
	movq	352(%rdi), %r8
	je	.L3230
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L3230
.L3238:
	movq	(%rsi), %rsi
	movq	%r8, %rdi
	movq	%rdx, -64(%rbp)
	movq	%xmm1, -72(%rbp)
	movsd	%xmm0, -56(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdi
	movq	-72(%rbp), %xmm1
	movq	-64(%rbp), %rdx
	movsd	-56(%rbp), %xmm0
	movq	%rax, %rsi
	jmp	.L3230
	.p2align 4,,10
	.p2align 3
.L3228:
	xorl	%edx, %edx
	jmp	.L3230
	.cfi_endproc
.LFE7753:
	.size	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE, .-_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	352(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	je	.L3248
	movq	352(%r15), %rsi
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%r15, -80(%rbp)
	movl	$0, -72(%rbp)
	jmp	.L3242
	.p2align 4,,10
	.p2align 3
.L3241:
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3244
	movq	%rbx, %r12
.L3242:
	movq	360(%r15), %rax
	movq	3280(%r15), %rsi
	movq	%r12, %rdi
	movq	152(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L3241
.L3244:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3249
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3248:
	.cfi_restore_state
	leaq	_ZZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3249:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7757:
	.size	_ZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap8GetOwnerEv
	.type	_ZN4node9AsyncWrap8GetOwnerEv, @function
_ZN4node9AsyncWrap8GetOwnerEv:
.LFB7756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rbx), %rsi
	movq	16(%rdi), %rdi
	testq	%rsi, %rsi
	je	.L3251
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L3256
.L3251:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L3256:
	.cfi_restore_state
	movq	352(%rdi), %rdi
	movq	(%rsi), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	movq	%rax, %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.cfi_endproc
.LFE7756:
	.size	_ZN4node9AsyncWrap8GetOwnerEv, .-_ZN4node9AsyncWrap8GetOwnerEv
	.p2align 4
	.globl	_Z20_register_async_wrapv
	.type	_Z20_register_async_wrapv, @function
_Z20_register_async_wrapv:
.LFB7761:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7761:
	.size	_Z20_register_async_wrapv, .-_Z20_register_async_wrapv
	.section	.rodata._ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC142:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	.type	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_, @function
_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_:
.LFB9036:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L3272
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L3268
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L3273
.L3260:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L3267:
	movsd	(%r15), %xmm0
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	movsd	%xmm0, (%rbx,%rdx)
	testq	%rdx, %rdx
	jg	.L3274
	testq	%r13, %r13
	jg	.L3263
	testq	%r9, %r9
	jne	.L3266
.L3264:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3274:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L3263
.L3266:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3264
	.p2align 4,,10
	.p2align 3
.L3273:
	testq	%rsi, %rsi
	jne	.L3261
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L3267
	.p2align 4,,10
	.p2align 3
.L3263:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3264
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3268:
	movl	$8, %r14d
	jmp	.L3260
.L3272:
	leaq	.LC142(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3261:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L3260
	.cfi_endproc
.LFE9036:
	.size	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_, .-_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd
	.type	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd, @function
_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd:
.LFB7746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	1216(%rdi), %rax
	movsd	%xmm0, -24(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L3293
.L3275:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3293:
	.cfi_restore_state
	movq	%rdi, %rbx
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	je	.L3275
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L3275
	movq	1424(%rdi), %rsi
	cmpq	1416(%rdi), %rsi
	je	.L3294
.L3279:
	cmpq	%rsi, 1432(%rbx)
	je	.L3284
	movsd	-24(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3294:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L3280
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L3282
.L3292:
	movq	(%rdi), %rax
	call	*8(%rax)
.L3282:
	movq	1424(%rbx), %rsi
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3284:
	leaq	-24(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L3275
	.p2align 4,,10
	.p2align 3
.L3280:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L3292
	jmp	.L3282
	.cfi_endproc
.LFE7746:
	.size	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd, .-_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap12WeakCallbackERKN2v816WeakCallbackInfoINS_12DestroyParamEEE
	.type	_ZN4node9AsyncWrap12WeakCallbackERKN2v816WeakCallbackInfoINS_12DestroyParamEEE, @function
_ZN4node9AsyncWrap12WeakCallbackERKN2v816WeakCallbackInfoINS_12DestroyParamEEE:
.LFB7714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-96(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %r12
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L3296
	movzbl	11(%r13), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L3330
.L3296:
	movq	8(%r12), %rdi
	leaq	_ZN4nodeL23DestroyParamCleanupHookEPv(%rip), %rax
	leaq	-64(%rbp), %rsi
	movq	%r12, -56(%rbp)
	movq	%rax, -64(%rbp)
	addq	$2584, %rdi
	movq	$0, -48(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	8(%r12), %rax
	movq	%r13, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	456(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3297
	call	_ZNK2v85Value7IsFalseEv@PLT
	testb	%al, %al
	jne	.L3331
.L3297:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3303
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L3303:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L3302
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L3302:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3332
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3331:
	.cfi_restore_state
	movq	8(%r12), %rdi
	movsd	(%r12), %xmm0
	call	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd
	jmp	.L3297
	.p2align 4,,10
	.p2align 3
.L3330:
	movq	0(%r13), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
	jmp	.L3296
.L3332:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7714:
	.size	_ZN4node9AsyncWrap12WeakCallbackERKN2v816WeakCallbackInfoINS_12DestroyParamEEE, .-_ZN4node9AsyncWrap12WeakCallbackERKN2v816WeakCallbackInfoINS_12DestroyParamEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jg	.L3334
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L3344
.L3336:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L3345
	movq	8(%rbx), %rdi
.L3338:
	call	_ZNK2v86Number5ValueEv@PLT
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3342
	cmpw	$1040, %cx
	jne	.L3339
.L3342:
	movq	23(%rdx), %rdi
.L3341:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd
	.p2align 4,,10
	.p2align 3
.L3334:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L3336
.L3344:
	leaq	_ZZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3345:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3338
	.p2align 4,,10
	.p2align 3
.L3339:
	addq	$32, %rdi
	xorl	%esi, %esi
	movsd	%xmm0, -24(%rbp)
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movsd	-24(%rbp), %xmm0
	movq	%rax, %rdi
	jmp	.L3341
	.cfi_endproc
.LFE7727:
	.size	_ZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC144:
	.string	"executionAsyncId"
.LC145:
	.string	"triggerAsyncId"
.LC146:
	.string	"data"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb
	.type	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb, @function
_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb:
.LFB7747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	32(%rdi), %eax
	testl	%eax, %eax
	je	.L4131
	movapd	%xmm0, %xmm1
	movsd	40(%rdi), %xmm0
	movq	%rdi, %rbx
	movq	%rsi, %r12
	ucomisd	.LC7(%rip), %xmm0
	movq	16(%rdi), %rdi
	movl	%edx, %r13d
	jp	.L3698
	jne	.L3698
.L3348:
	ucomisd	.LC7(%rip), %xmm1
	movq	1256(%rdi), %rax
	jp	.L3351
	jne	.L3351
	movsd	.LC143(%rip), %xmm1
	addsd	16(%rax), %xmm1
	movsd	%xmm1, 16(%rax)
.L3351:
	movsd	%xmm1, 40(%rbx)
	movsd	24(%rax), %xmm0
	pxor	%xmm1, %xmm1
	comisd	%xmm0, %xmm1
	jbe	.L3353
	movsd	(%rax), %xmm0
.L3353:
	movq	352(%rdi), %rsi
	leaq	-80(%rbp), %r14
	movsd	%xmm0, 48(%rbx)
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3355
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L4132
.L3356:
	testq	%r12, %r12
	je	.L3357
	movq	(%r12), %rax
	cmpq	%rax, (%rdi)
	jne	.L3357
.L3358:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	cmpl	$46, 32(%rbx)
	ja	.L3359
	movl	32(%rbx), %eax
	leaq	.L3361(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L3361:
	.long	.L3407-.L3361
	.long	.L3406-.L3361
	.long	.L3405-.L3361
	.long	.L3404-.L3361
	.long	.L3403-.L3361
	.long	.L3402-.L3361
	.long	.L3401-.L3361
	.long	.L3400-.L3361
	.long	.L3399-.L3361
	.long	.L3398-.L3361
	.long	.L3397-.L3361
	.long	.L3396-.L3361
	.long	.L3395-.L3361
	.long	.L3394-.L3361
	.long	.L3393-.L3361
	.long	.L3392-.L3361
	.long	.L3391-.L3361
	.long	.L3390-.L3361
	.long	.L3389-.L3361
	.long	.L3388-.L3361
	.long	.L3387-.L3361
	.long	.L3386-.L3361
	.long	.L3385-.L3361
	.long	.L3384-.L3361
	.long	.L3383-.L3361
	.long	.L3382-.L3361
	.long	.L3381-.L3361
	.long	.L3380-.L3361
	.long	.L3379-.L3361
	.long	.L3378-.L3361
	.long	.L3377-.L3361
	.long	.L3376-.L3361
	.long	.L3375-.L3361
	.long	.L3374-.L3361
	.long	.L3373-.L3361
	.long	.L3372-.L3361
	.long	.L3371-.L3361
	.long	.L3370-.L3361
	.long	.L3369-.L3361
	.long	.L3368-.L3361
	.long	.L3367-.L3361
	.long	.L3366-.L3361
	.long	.L3365-.L3361
	.long	.L3364-.L3361
	.long	.L3363-.L3361
	.long	.L3362-.L3361
	.long	.L3360-.L3361
	.text
	.p2align 4,,10
	.p2align 3
.L4132:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L3356
	.p2align 4,,10
	.p2align 3
.L3355:
	leaq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3357:
	movq	16(%rbx), %rax
	movq	%r12, %rcx
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	168(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	jmp	.L3358
	.p2align 4,,10
	.p2align 3
.L3698:
	movsd	%xmm1, -120(%rbp)
	call	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd
	cmpq	$0, 8(%rbx)
	movsd	-120(%rbp), %xmm1
	movq	.LC7(%rip), %rax
	movq	%rax, 40(%rbx)
	je	.L4133
	movq	%rbx, %rdi
	movsd	%xmm1, -120(%rbp)
	call	_ZN4node9AsyncWrap11EmitDestroyEb.part.0
	movq	16(%rbx), %rdi
	movsd	-120(%rbp), %xmm1
	jmp	.L3348
	.p2align 4,,10
	.p2align 3
.L4131:
	leaq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4133:
	movq	16(%rbx), %rdi
	jmp	.L3348
	.p2align 4,,10
	.p2align 3
.L3407:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4134
.L3409:
	cmpb	$0, (%rax)
	jne	.L4135
	.p2align 4,,10
	.p2align 3
.L3416:
	testb	%r13b, %r13b
	jne	.L3346
	movq	16(%rbx), %rdi
	movslq	32(%rbx), %rdx
	movq	%r12, %rsi
	movsd	40(%rbx), %xmm0
	movsd	48(%rbx), %xmm1
	movq	360(%rdi), %rax
	movq	1976(%rax,%rdx,8), %rdx
	call	_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd
.L3346:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4136
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3372:
	.cfi_restore_state
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4137
.L3589:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__34_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4138
.L3591:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4139
.L3592:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3368:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4140
.L3609:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__38_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4141
.L3611:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4142
.L3612:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3370:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4143
.L3599:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__36_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4144
.L3601:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4145
.L3602:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3371:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4146
.L3594:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__35_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4147
.L3596:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4148
.L3597:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3376:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4149
.L3569:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__30_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4150
.L3571:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4151
.L3572:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3378:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4152
.L3559:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__28_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4153
.L3561:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4154
.L3562:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3379:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4155
.L3554:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__27_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4156
.L3556:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4157
.L3557:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3360:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4158
.L3644:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__45_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4159
.L3646:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4160
.L3647:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3374:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4161
.L3579:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__32_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4162
.L3581:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4163
.L3582:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3373:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4164
.L3584:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__33_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4165
.L3586:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4166
.L3587:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3380:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4167
.L3549:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__26_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4168
.L3551:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4169
.L3552:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3382:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4170
.L3539:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__24_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4171
.L3541:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4172
.L3542:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3363:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4173
.L3634:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__43_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4174
.L3636:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4175
.L3637:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3369:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4176
.L3604:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__37_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4177
.L3606:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4178
.L3607:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3383:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4179
.L3534:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__23_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4180
.L3536:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4181
.L3537:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3385:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4182
.L3524:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__21_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4183
.L3526:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4184
.L3527:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3386:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4185
.L3519:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__20_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4186
.L3521:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4187
.L3522:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3377:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4188
.L3564:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__29_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4189
.L3566:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4190
.L3567:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3399:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4191
.L3454:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_7(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4192
.L3456:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4193
.L3457:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3401:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4194
.L3444:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_5(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4195
.L3446:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4196
.L3447:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3402:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4197
.L3439:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_4(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4198
.L3441:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4199
.L3442:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3367:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4200
.L3614:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__39_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4201
.L3616:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4202
.L3617:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3391:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4203
.L3494:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__15_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4204
.L3496:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4205
.L3497:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3393:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4206
.L3484:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__13_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4207
.L3486:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4208
.L3487:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3394:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4209
.L3479:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__12_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4210
.L3481:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4211
.L3482:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3375:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4212
.L3574:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__31_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4213
.L3576:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4214
.L3577:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3364:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4215
.L3629:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__42_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4216
.L3631:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4217
.L3632:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3366:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4218
.L3619:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__40_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4219
.L3621:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4220
.L3622:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3362:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4221
.L3639:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__44_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4222
.L3641:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4223
.L3642:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3387:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4224
.L3514:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__19_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4225
.L3516:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4226
.L3517:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3389:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4227
.L3504:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__17_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4228
.L3506:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4229
.L3507:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3390:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4230
.L3499:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__16_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4231
.L3501:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4232
.L3502:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3403:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4233
.L3434:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_3(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4234
.L3436:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4235
.L3437:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3405:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4236
.L3424:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_1(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4237
.L3426:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4238
.L3427:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3406:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4239
.L3417:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_0(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4240
.L3419:
	movq	-96(%rbp), %r15
	testb	$5, (%rdi)
	jne	.L4241
.L3420:
	testq	%r15, %r15
	je	.L3416
	movq	(%r15), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3422
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3423
	call	_ZdlPv@PLT
.L3423:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3395:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4242
.L3474:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__11_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4243
.L3476:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4244
.L3477:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3396:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4245
.L3469:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__10_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4246
.L3471:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4247
.L3472:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3397:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4248
.L3464:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_9(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4249
.L3466:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4250
.L3467:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3398:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4251
.L3459:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_8(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4252
.L3461:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4253
.L3462:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3381:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4254
.L3544:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__25_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4255
.L3546:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4256
.L3547:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3384:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4257
.L3529:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__22_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4258
.L3531:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4259
.L3532:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3400:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4260
.L3449:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_6(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4261
.L3451:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4262
.L3452:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3392:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4263
.L3489:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__14_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4264
.L3491:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4265
.L3492:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3365:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4266
.L3624:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__41_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4267
.L3626:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4268
.L3627:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3388:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4269
.L3509:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__18_(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4270
.L3511:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4271
.L3512:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
	.p2align 4,,10
	.p2align 3
.L3404:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3416
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L4272
.L3429:
	cmpb	$0, (%rax)
	je	.L3416
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_2(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4273
.L3431:
	movq	-96(%rbp), %r8
	testb	$5, (%rdi)
	jne	.L4274
.L3432:
	testq	%r8, %r8
	je	.L3416
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L3416
.L4135:
	leaq	-96(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	16(%rbx), %rax
	movq	-96(%rbp), %rdi
	leaq	.LC144(%rip), %rsi
	movq	1256(%rax), %rax
	cvttsd2siq	(%rax), %rdx
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	cvttsd2siq	48(%rbx), %rdx
	movq	-96(%rbp), %rdi
	leaq	.LC145(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10SetIntegerEPKci@PLT
	movq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4275
.L3411:
	movq	-96(%rbp), %r15
	testb	$5, (%rdi)
	jne	.L4276
.L3412:
	testq	%r15, %r15
	je	.L3416
	movq	(%r15), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3414
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L3415
	call	_ZdlPv@PLT
.L3415:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L3416
.L4167:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3549
.L4221:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3639
.L4230:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3499
.L4176:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3604
.L4188:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3564
.L4200:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3614
.L4212:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3574
.L4143:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3599
.L4152:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3559
.L4140:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3609
.L4149:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3569
.L4161:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3579
.L4173:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3634
.L4197:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3439
.L4185:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3519
.L4209:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3479
.L4242:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3474
.L4260:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3449
.L4254:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3544
.L4266:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3624
.L4251:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3459
.L4263:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3489
.L4257:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3529
.L4269:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3509
.L4158:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3644
.L4215:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3629
.L4218:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3619
.L4227:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3504
.L4236:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3424
.L4239:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3417
.L4224:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3514
.L4233:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3434
.L4248:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3464
.L4245:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3469
.L4164:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3584
.L4155:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3554
.L4203:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3494
.L4206:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3484
.L4146:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3594
.L4191:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3454
.L4194:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3444
.L4137:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3589
.L4179:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3534
.L4182:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3524
.L4170:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3539
.L4134:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3409
.L4272:
	leaq	.LC17(%rip), %rsi
	call	*%rdx
	jmp	.L3429
.L4274:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC96(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3432
.L4273:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_2(%rip)
	mfence
	jmp	.L3431
.L4157:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC121(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3557
.L4199:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC98(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3442
.L4160:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC139(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3647
.L4211:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC106(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3482
.L4198:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_4(%rip)
	mfence
	jmp	.L3441
.L4186:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__20_(%rip)
	mfence
	jmp	.L3521
.L4148:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC129(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3597
.L4187:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC114(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3522
.L4141:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__38_(%rip)
	mfence
	jmp	.L3611
.L4150:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__30_(%rip)
	mfence
	jmp	.L3571
.L4163:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC126(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3582
.L4162:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__32_(%rip)
	mfence
	jmp	.L3581
.L4174:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__43_(%rip)
	mfence
	jmp	.L3636
.L4139:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC128(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3592
.L4175:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC137(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3637
.L4168:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__26_(%rip)
	mfence
	jmp	.L3551
.L4276:
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	-97(%rbp), %r8
	movq	%r15, -80(%rbp)
	cvttsd2siq	40(%rbx), %rdx
	leaq	.LC146(%rip), %rax
	movb	$8, -97(%rbp)
	leaq	.LC93(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r15
	jmp	.L3412
.L4275:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719(%rip)
	mfence
	jmp	.L3411
.L4177:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__37_(%rip)
	mfence
	jmp	.L3606
.L4189:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__29_(%rip)
	mfence
	jmp	.L3566
.L4222:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__44_(%rip)
	mfence
	jmp	.L3641
.L4231:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__16_(%rip)
	mfence
	jmp	.L3501
.L4144:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__36_(%rip)
	mfence
	jmp	.L3601
.L4153:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__28_(%rip)
	mfence
	jmp	.L3561
.L4151:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC124(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3572
.L4154:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC122(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3562
.L4201:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__39_(%rip)
	mfence
	jmp	.L3616
.L4213:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__31_(%rip)
	mfence
	jmp	.L3576
.L4142:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC132(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3612
.L4145:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC130(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3602
.L4138:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__34_(%rip)
	mfence
	jmp	.L3591
.L4180:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__23_(%rip)
	mfence
	jmp	.L3536
.L4181:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC117(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3537
.L4184:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC115(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3527
.L4183:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__21_(%rip)
	mfence
	jmp	.L3526
.L4171:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__24_(%rip)
	mfence
	jmp	.L3541
.L4169:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC120(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3552
.L4172:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC118(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3542
.L4241:
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	-97(%rbp), %r8
	movq	%r15, -80(%rbp)
	cvttsd2siq	40(%rbx), %rdx
	leaq	.LC146(%rip), %rax
	movb	$8, -97(%rbp)
	leaq	.LC94(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r15
	jmp	.L3420
.L4240:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_0(%rip)
	mfence
	jmp	.L3419
.L4225:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__19_(%rip)
	mfence
	jmp	.L3516
.L4216:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__42_(%rip)
	mfence
	jmp	.L3631
.L4219:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__40_(%rip)
	mfence
	jmp	.L3621
.L4217:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC136(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3632
.L4220:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC134(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3622
.L4192:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_7(%rip)
	mfence
	jmp	.L3456
.L4195:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_5(%rip)
	mfence
	jmp	.L3446
.L4193:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC101(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3457
.L4196:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC99(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3447
.L4205:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC109(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3497
.L4208:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC107(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3487
.L4207:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__13_(%rip)
	mfence
	jmp	.L3486
.L4147:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__35_(%rip)
	mfence
	jmp	.L3596
.L4234:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_3(%rip)
	mfence
	jmp	.L3436
.L4249:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_9(%rip)
	mfence
	jmp	.L3466
.L4247:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC104(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3472
.L4250:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC103(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3467
.L4156:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__27_(%rip)
	mfence
	jmp	.L3556
.L4204:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__15_(%rip)
	mfence
	jmp	.L3496
.L4246:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__10_(%rip)
	mfence
	jmp	.L3471
.L4165:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__33_(%rip)
	mfence
	jmp	.L3586
.L4214:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC125(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3577
.L4265:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC108(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3492
.L4232:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC110(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3502
.L4271:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC112(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3512
.L4264:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__14_(%rip)
	mfence
	jmp	.L3491
.L4258:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__22_(%rip)
	mfence
	jmp	.L3531
.L4190:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC123(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3567
.L4259:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC116(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3532
.L4210:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__12_(%rip)
	mfence
	jmp	.L3481
.L4243:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__11_(%rip)
	mfence
	jmp	.L3476
.L4244:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC105(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3477
.L4267:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__41_(%rip)
	mfence
	jmp	.L3626
.L4252:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_8(%rip)
	mfence
	jmp	.L3461
.L4166:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC127(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3587
.L4253:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC102(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3462
.L4228:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__17_(%rip)
	mfence
	jmp	.L3506
.L4237:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_1(%rip)
	mfence
	jmp	.L3426
.L4235:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC97(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3437
.L4238:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC95(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3427
.L4270:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__18_(%rip)
	mfence
	jmp	.L3511
.L4159:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__45_(%rip)
	mfence
	jmp	.L3646
.L4226:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC113(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3517
.L4229:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC111(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3507
.L4202:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC133(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3617
.L4262:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC100(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3452
.L4223:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC138(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3642
.L4268:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC135(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3627
.L4261:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_6(%rip)
	mfence
	jmp	.L3451
.L4255:
	leaq	.LC17(%rip), %rdi
	call	_ZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKc
	movq	%rax, %rdi
	movq	%rax, _ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__25_(%rip)
	mfence
	jmp	.L3546
.L4178:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC131(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3607
.L4256:
	cvttsd2siq	40(%rbx), %rdx
	movq	%r8, -80(%rbp)
	leaq	-88(%rbp), %rcx
	movq	%r14, %r9
	leaq	.LC146(%rip), %rax
	leaq	-97(%rbp), %r8
	movb	$8, -97(%rbp)
	leaq	.LC119(%rip), %rsi
	movq	%rax, -88(%rbp)
	movq	$0, -96(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.1
	movq	-96(%rbp), %r8
	jmp	.L3547
.L3422:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3416
.L3414:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L3416
.L3359:
	leaq	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4136:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7747:
	.size	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb, .-_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L4278
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L4299
.L4280:
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L4300
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4295
	cmpw	$1040, %cx
	jne	.L4282
.L4295:
	movq	23(%rdx), %r12
.L4284:
	testq	%r12, %r12
	je	.L4297
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L4301
	movq	8(%rbx), %r13
	cmpl	$1, %eax
	je	.L4302
	leaq	-8(%r13), %rdi
.L4290:
	call	_ZNK2v85Value8IsNumberEv@PLT
	movsd	.LC7(%rip), %xmm0
	testb	%al, %al
	je	.L4291
	cmpl	$1, 16(%rbx)
	jle	.L4303
	movq	8(%rbx), %rdi
	subq	$8, %rdi
.L4293:
	call	_ZNK2v86Number5ValueEv@PLT
.L4291:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb
	.p2align 4,,10
	.p2align 3
.L4278:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L4280
.L4299:
	leaq	_ZZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4301:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r13
	jmp	.L4290
	.p2align 4,,10
	.p2align 3
.L4303:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4293
	.p2align 4,,10
	.p2align 3
.L4282:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L4284
	.p2align 4,,10
	.p2align 3
.L4297:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4300:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4302:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4290
	.cfi_endproc
.LFE7724:
	.size	_ZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb
	.type	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb, @function
_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb:
.LFB7735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	movl	%r8d, -60(%rbp)
	movsd	%xmm0, -56(%rbp)
	testq	%rdx, %rdx
	je	.L4305
	movq	%rdx, %rsi
	movq	%rdx, %r14
	movl	%ecx, %r15d
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r14, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L4347
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %rdx
	movl	$40, %edi
	leaq	1(%rdx), %rax
	movq	%rdx, -72(%rbp)
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	2592(%rbx), %rsi
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%rdx, 24(%rax)
	xorl	%edx, %edx
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r8
	testq	%r9, %r9
	je	.L4307
	movq	(%r9), %rax
	movq	32(%rax), %rcx
	jmp	.L4310
	.p2align 4,,10
	.p2align 3
.L4308:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L4307
	movq	32(%rdi), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L4307
	movq	%rdi, %rax
.L4310:
	cmpq	%r12, %rcx
	jne	.L4308
	cmpq	%r11, 8(%rax)
	jne	.L4308
	cmpq	16(%rax), %rcx
	jne	.L4308
	cmpq	$0, (%r9)
	je	.L4307
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4307:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	movq	%r8, -72(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r9
	testb	%al, %al
	jne	.L4311
	movq	2584(%rbx), %r10
	movq	-72(%rbp), %r8
	movq	%r12, 32(%r13)
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L4321
.L4351:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r8), %rax
	movq	%r13, (%rax)
.L4322:
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	addq	$1, 2608(%rbx)
	addq	$1, 2656(%rbx)
	movq	%rax, (%r12)
	movq	.LC7(%rip), %rax
	movl	$0, 32(%r12)
	movb	$0, 36(%r12)
	movq	%rax, 40(%r12)
	testl	%r15d, %r15d
	je	.L4348
	movl	%r15d, 32(%r12)
	movzbl	-60(%rbp), %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movsd	-56(%rbp), %xmm0
	call	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb
	movb	$1, 36(%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4311:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L4349
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L4350
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -80(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-80(%rbp), %r9
	movq	%rax, %r10
	leaq	2632(%rbx), %rax
	movq	%rax, -72(%rbp)
.L4314:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L4316
	xorl	%r8d, %r8d
	leaq	2600(%rbx), %r11
	jmp	.L4317
	.p2align 4,,10
	.p2align 3
.L4318:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L4319:
	testq	%rsi, %rsi
	je	.L4316
.L4317:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r9
	leaq	(%r10,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L4318
	movq	2600(%rbx), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L4326
	movq	%rcx, (%r10,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L4317
	.p2align 4,,10
	.p2align 3
.L4316:
	movq	2584(%rbx), %rdi
	cmpq	-72(%rbp), %rdi
	je	.L4320
	movq	%r9, -80(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r10
.L4320:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r13)
	divq	%r9
	movq	%r9, 2592(%rbx)
	movq	%r10, 2584(%rbx)
	leaq	0(,%rdx,8), %r8
	addq	%r10, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L4351
.L4321:
	movq	2600(%rbx), %rax
	movq	%r13, 2600(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L4323
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r10,%rdx,8)
.L4323:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r8)
	jmp	.L4322
	.p2align 4,,10
	.p2align 3
.L4326:
	movq	%rdx, %r8
	jmp	.L4319
	.p2align 4,,10
	.p2align 3
.L4305:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4347:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4348:
	leaq	_ZZN4node9AsyncWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4349:
	leaq	2632(%rbx), %r10
	movq	$0, 2632(%rbx)
	movq	%r10, -72(%rbp)
	jmp	.L4314
.L4350:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7735:
	.size	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb, .-_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb
	.globl	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb
	.set	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb,_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd
	.type	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd, @function
_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd:
.LFB7732:
	.cfi_startproc
	endbr64
	xorl	%r8d, %r8d
	jmp	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb
	.cfi_endproc
.LFE7732:
	.size	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd, .-_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd
	.globl	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd
	.set	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd,_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd
	.section	.text._ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE,"axG",@progbits,_ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4364
	cmpw	$1040, %cx
	jne	.L4354
.L4364:
	movq	23(%rdx), %r12
.L4356:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L4357
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L4366
.L4357:
	movq	8(%rbx), %rax
	movq	3072(%r12), %rdi
	leaq	8(%rax), %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L4367
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L4368
	movq	8(%rbx), %rdi
.L4360:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4369
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L4362
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4363:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movl	$56, %edi
	movl	%eax, %r14d
	call	_Znwm@PLT
	addq	$8, %r13
	movl	%r14d, %ecx
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	xorl	%r8d, %r8d
	movq	%rax, %rbx
	movsd	.LC7(%rip), %xmm0
	call	_ZN4node9AsyncWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb
	leaq	16+_ZTVN4node15AsyncWrapObjectE(%rip), %rax
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4368:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4360
	.p2align 4,,10
	.p2align 3
.L4366:
	cmpl	$5, 43(%rax)
	jne	.L4357
	leaq	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4362:
	movq	8(%rbx), %rdi
	jmp	.L4363
	.p2align 4,,10
	.p2align 3
.L4354:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r12
	jmp	.L4356
	.p2align 4,,10
	.p2align 3
.L4367:
	leaq	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4369:
	leaq	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7673:
	.size	_ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b
	.type	_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b, @function
_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b:
.LFB7703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$8, %rsp
	movq	3200(%rdi), %rdi
	movq	3280(%r13), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L4388
	movq	%rax, %r14
	movq	352(%r13), %rax
	movl	$1, %esi
	movq	%r14, %rdi
	leaq	120(%rax), %rdx
	addq	$112, %rax
	testq	%r15, %r15
	cmovne	%rax, %rdx
	call	_ZN2v86Object16SetInternalFieldEiNS_5LocalINS_5ValueEEE@PLT
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4384
	cmpw	$1040, %cx
	jne	.L4374
.L4384:
	movq	23(%rdx), %rax
.L4376:
	testq	%rax, %rax
	jne	.L4389
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object16SetInternalFieldEiNS_5LocalINS_5ValueEEE@PLT
	movl	$56, %edi
	call	_Znwm@PLT
	movzbl	%bl, %r8d
	movq	%r14, %rdx
	movq	%r13, %rsi
	movsd	.LC7(%rip), %xmm0
	movq	%rax, %rdi
	movl	$24, %ecx
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdb
	leaq	16+_ZTVN4node11PromiseWrapE(%rip), %rax
	movq	%rax, (%r12)
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L4380
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L4370
.L4380:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L4370:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4374:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L4376
	.p2align 4,,10
	.p2align 3
.L4389:
	leaq	_ZZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_bE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4388:
	xorl	%r12d, %r12d
	jmp	.L4370
	.cfi_endproc
.LFE7703:
	.size	_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b, .-_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b
	.section	.rodata.str1.1
.LC147:
	.string	"node,node.environment"
.LC148:
	.string	"EnvPromiseHook"
	.text
	.p2align 4
	.type	_ZN4nodeL11PromiseHookEN2v815PromiseHookTypeENS0_5LocalINS0_7PromiseEEENS2_INS0_5ValueEEE, @function
_ZN4nodeL11PromiseHookEN2v815PromiseHookTypeENS0_5LocalINS0_7PromiseEEENS2_INS0_5ValueEEE:
.LFB7706:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edi, %ebx
	movq	%rsi, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v86Object15CreationContextEv@PLT
	testq	%rax, %rax
	je	.L4390
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L4390
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L4390
	movq	271(%rax), %r14
	testq	%r14, %r14
	je	.L4390
	movq	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip), %rdx
	testq	%rdx, %rdx
	je	.L4499
	testb	$5, (%rdx)
	jne	.L4500
.L4395:
	movq	%r12, %rdi
	call	_ZN4nodeL18extractPromiseWrapEN2v85LocalINS0_7PromiseEEE
	movq	%rax, %r15
	testl	%ebx, %ebx
	je	.L4443
.L4508:
	testq	%rax, %rax
	je	.L4443
.L4401:
	cmpl	$2, %ebx
	je	.L4501
	cmpl	$3, %ebx
	je	.L4502
	cmpl	$1, %ebx
	je	.L4503
.L4413:
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L4504
.L4419:
	testb	$5, (%r12)
	jne	.L4505
.L4390:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4506
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4500:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	%rdx, -120(%rbp)
	leaq	-80(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-120(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L4396
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	leaq	-80(%rbp), %r15
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4507
.L4396:
	leaq	-64(%rbp), %rax
.L4400:
	movq	-8(%rax), %r8
	subq	$8, %rax
	testq	%r8, %r8
	je	.L4397
	movq	(%r8), %rdx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rcx
	movq	8(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L4398
	movq	8(%r8), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rcx
	leaq	24(%r8), %rdx
	movq	%rcx, (%r8)
	cmpq	%rdx, %rdi
	je	.L4399
	movq	%r8, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rax
.L4399:
	movl	$48, %esi
	movq	%r8, %rdi
	movq	%rax, -120(%rbp)
	call	_ZdlPvm@PLT
	movq	-120(%rbp), %rax
.L4397:
	cmpq	%r15, %rax
	jne	.L4400
	movq	%r12, %rdi
	call	_ZN4nodeL18extractPromiseWrapEN2v85LocalINS0_7PromiseEEE
	movq	%rax, %r15
	testl	%ebx, %ebx
	jne	.L4508
	.p2align 4,,10
	.p2align 3
.L4443:
	testl	%ebx, %ebx
	movq	%r13, %rdi
	setne	%r15b
	call	_ZNK2v85Value9IsPromiseEv@PLT
	testb	%al, %al
	jne	.L4509
	movzbl	%r15b, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b
	movq	%rax, %r15
.L4407:
	testq	%r15, %r15
	jne	.L4401
.L4405:
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L4510
.L4427:
	testb	$5, (%r12)
	je	.L4390
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4429
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %r15
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4511
.L4429:
	leaq	-64(%rbp), %rbx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r13
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r14
.L4433:
	movq	-8(%rbx), %r12
	subq	$8, %rbx
	testq	%r12, %r12
	je	.L4430
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L4431
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r14, (%r12)
	cmpq	%rax, %rdi
	je	.L4432
	call	_ZdlPv@PLT
.L4432:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4430:
	cmpq	%r15, %rbx
	jne	.L4433
	jmp	.L4390
	.p2align 4,,10
	.p2align 3
.L4499:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4394
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L4512
.L4394:
	movq	%rdx, _ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip)
	mfence
	testb	$5, (%rdx)
	je	.L4395
	jmp	.L4500
	.p2align 4,,10
	.p2align 3
.L4505:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r15
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4421
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	leaq	-80(%rbp), %r15
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4513
.L4421:
	leaq	-64(%rbp), %rbx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r13
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r14
.L4425:
	movq	-8(%rbx), %r12
	subq	$8, %rbx
	testq	%r12, %r12
	je	.L4422
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r13, %rax
	jne	.L4423
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r14, (%r12)
	cmpq	%rax, %rdi
	je	.L4424
	call	_ZdlPv@PLT
.L4424:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L4422:
	cmpq	%r15, %rbx
	jne	.L4425
	jmp	.L4390
	.p2align 4,,10
	.p2align 3
.L4509:
	movq	%r13, %rdi
	call	_ZN4nodeL18extractPromiseWrapEN2v85LocalINS0_7PromiseEEE
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L4514
.L4404:
	movq	16(%rdx), %r13
	movsd	40(%rdx), %xmm0
	movq	1216(%r13), %rax
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	je	.L4406
	comisd	.LC8(%rip), %xmm0
	jb	.L4515
.L4406:
	movq	1256(%r13), %rax
	movzbl	%r15b, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -120(%rbp)
	call	_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b
	movsd	-120(%rbp), %xmm1
	movq	%rax, %r15
	movq	1256(%r13), %rax
	movsd	%xmm1, 24(%rax)
	jmp	.L4407
	.p2align 4,,10
	.p2align 3
.L4501:
	movq	8(%r15), %rbx
	leaq	1144(%r14), %rax
	movq	%rax, -120(%rbp)
	testq	%rbx, %rbx
	je	.L4409
	movzbl	11(%rbx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L4516
.L4409:
	movsd	48(%r15), %xmm3
	movsd	40(%r15), %xmm2
	leaq	-112(%rbp), %r12
	movq	%r14, %r13
	movq	352(%r14), %rsi
	movq	%r12, %rdi
	movsd	%xmm3, -128(%rbp)
	movsd	%xmm2, -120(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1216(%r14), %rax
	movl	24(%rax), %edx
	testl	%edx, %edx
	je	.L4410
	movsd	.LC7(%rip), %xmm0
	movsd	-120(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L4517
	movsd	-128(%rbp), %xmm4
	comisd	%xmm0, %xmm4
	jb	.L4518
.L4410:
	movl	28(%rax), %r8d
	leal	(%r8,%r8), %r11d
	movl	%r8d, %edx
	movq	%r11, %r10
	cmpq	1160(%r14), %r11
	jnb	.L4519
.L4412:
	movq	1256(%r14), %rcx
	movq	1176(%r14), %rsi
	leal	1(%r10), %edi
	addl	$1, %edx
	movsd	(%rcx), %xmm0
	movsd	%xmm0, (%rsi,%r11,8)
	movsd	8(%rcx), %xmm0
	movsd	%xmm0, (%rsi,%rdi,8)
	movsd	-120(%rbp), %xmm0
	movq	3280(%r13), %rsi
	movl	%edx, 28(%rax)
	movq	1272(%r14), %rdi
	movl	%r8d, %edx
	movhpd	-128(%rbp), %xmm0
	movups	%xmm0, (%rcx)
	movq	%rbx, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN4node9AsyncWrap20EmitTraceEventBeforeEv
	movq	16(%r15), %rdi
	movsd	40(%r15), %xmm0
	movl	$1, %esi
	movq	2696(%rdi), %rdx
	call	_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	jne	.L4419
	.p2align 4,,10
	.p2align 3
.L4504:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4420
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4520
.L4420:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L4419
	.p2align 4,,10
	.p2align 3
.L4502:
	movsd	40(%r15), %xmm0
	movl	32(%r15), %edi
	call	_ZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEd
	movq	16(%r15), %rdi
	movsd	40(%r15), %xmm0
	movl	$2, %esi
	movq	2688(%rdi), %rdx
	call	_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE
	movq	1256(%r14), %rax
	movsd	40(%r15), %xmm0
	ucomisd	(%rax), %xmm0
	jp	.L4413
	jne	.L4413
	movq	1216(%r14), %rcx
	movl	28(%rcx), %edx
	testl	%edx, %edx
	je	.L4413
	movq	1176(%r14), %rsi
	subl	$1, %edx
	leal	(%rdx,%rdx), %r8d
	movsd	(%rsi,%r8,8), %xmm0
	movq	%r8, %rdi
	addl	$1, %edi
	movsd	%xmm0, (%rax)
	movsd	(%rsi,%rdi,8), %xmm0
	movq	1272(%r14), %rdi
	movq	3280(%r14), %rsi
	movsd	%xmm0, 8(%rax)
	movl	%edx, 28(%rcx)
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEEj@PLT
	jmp	.L4413
	.p2align 4,,10
	.p2align 3
.L4503:
	movq	16(%r15), %rdi
	movsd	40(%r15), %xmm0
	movl	$4, %esi
	movq	2736(%rdi), %rdx
	call	_ZN4node4EmitEPNS_11EnvironmentEdNS_10AsyncHooks6FieldsEN2v85LocalINS4_8FunctionEEE
	jmp	.L4413
	.p2align 4,,10
	.p2align 3
.L4398:
	movq	%rax, -120(%rbp)
	movq	%r8, %rdi
	call	*%rdx
	movq	-120(%rbp), %rax
	jmp	.L4397
	.p2align 4,,10
	.p2align 3
.L4423:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4422
	.p2align 4,,10
	.p2align 3
.L4512:
	leaq	.LC147(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L4394
	.p2align 4,,10
	.p2align 3
.L4507:
	subq	$8, %rsp
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movl	$98, %esi
	pushq	$6
	leaq	.LC148(%rip), %rcx
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L4396
.L4515:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4518:
	leaq	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L4510:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L4428
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L4521
.L4428:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L4427
	.p2align 4,,10
	.p2align 3
.L4514:
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_b
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L4404
	jmp	.L4405
	.p2align 4,,10
	.p2align 3
.L4519:
	leaq	1144(%r14), %rdi
	movq	%r11, -144(%rbp)
	movl	%r11d, -136(%rbp)
	movl	%r8d, -132(%rbp)
	call	_ZN4node10AsyncHooks20grow_async_ids_stackEv@PLT
	movq	1216(%r14), %rax
	movq	-144(%rbp), %r11
	movl	-136(%rbp), %r10d
	movl	-132(%rbp), %r8d
	movl	28(%rax), %edx
	jmp	.L4412
	.p2align 4,,10
	.p2align 3
.L4520:
	leaq	.LC147(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L4420
	.p2align 4,,10
	.p2align 3
.L4516:
	movq	16(%r15), %rax
	movq	(%rbx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rbx
	jmp	.L4409
	.p2align 4,,10
	.p2align 3
.L4513:
	subq	$8, %rsp
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$6
	leaq	.LC148(%rip), %rcx
	movl	$101, %esi
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L4421
	.p2align 4,,10
	.p2align 3
.L4431:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L4430
.L4521:
	leaq	.LC147(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L4428
.L4511:
	subq	$8, %rsp
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	pushq	$6
	leaq	.LC148(%rip), %rcx
	movl	$101, %esi
	pushq	%r15
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L4429
.L4517:
	leaq	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7706:
	.size	_ZN4nodeL11PromiseHookEN2v815PromiseHookTypeENS0_5LocalINS0_7PromiseEEENS2_INS0_5ValueEEE, .-_ZN4nodeL11PromiseHookEN2v815PromiseHookTypeENS0_5LocalINS0_7PromiseEEENS2_INS0_5ValueEEE
	.section	.text._ZN4node11PromiseWrapD2Ev,"axG",@progbits,_ZN4node11PromiseWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11PromiseWrapD2Ev
	.type	_ZN4node11PromiseWrapD2Ev, @function
_ZN4node11PromiseWrapD2Ev:
.LFB10238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	movq	16(%r12), %rbx
	movsd	40(%r12), %xmm0
	movq	1216(%rbx), %rax
	movsd	%xmm0, -32(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L4541
.L4524:
	movq	.LC7(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	call	_ZN4node10BaseObjectD2Ev
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4542
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4541:
	.cfi_restore_state
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L4524
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L4524
	movq	1424(%rbx), %rsi
	cmpq	1416(%rbx), %rsi
	je	.L4543
.L4526:
	cmpq	%rsi, 1432(%rbx)
	je	.L4531
	movsd	-32(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	jmp	.L4524
	.p2align 4,,10
	.p2align 3
.L4543:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L4527
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L4529
.L4540:
	movq	(%rdi), %rax
	call	*8(%rax)
.L4529:
	movq	1424(%rbx), %rsi
	jmp	.L4526
	.p2align 4,,10
	.p2align 3
.L4531:
	leaq	-32(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L4524
	.p2align 4,,10
	.p2align 3
.L4527:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L4540
	jmp	.L4529
.L4542:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10238:
	.size	_ZN4node11PromiseWrapD2Ev, .-_ZN4node11PromiseWrapD2Ev
	.weak	_ZN4node11PromiseWrapD1Ev
	.set	_ZN4node11PromiseWrapD1Ev,_ZN4node11PromiseWrapD2Ev
	.section	.text._ZN4node15AsyncWrapObjectD2Ev,"axG",@progbits,_ZN4node15AsyncWrapObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15AsyncWrapObjectD2Ev
	.type	_ZN4node15AsyncWrapObjectD2Ev, @function
_ZN4node15AsyncWrapObjectD2Ev:
.LFB10242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	movq	16(%r12), %rbx
	movsd	40(%r12), %xmm0
	movq	1216(%rbx), %rax
	movsd	%xmm0, -32(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L4563
.L4546:
	movq	.LC7(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	call	_ZN4node10BaseObjectD2Ev
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4564
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4563:
	.cfi_restore_state
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L4546
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L4546
	movq	1424(%rbx), %rsi
	cmpq	1416(%rbx), %rsi
	je	.L4565
.L4548:
	cmpq	%rsi, 1432(%rbx)
	je	.L4553
	movsd	-32(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	jmp	.L4546
	.p2align 4,,10
	.p2align 3
.L4565:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L4549
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L4551
.L4562:
	movq	(%rdi), %rax
	call	*8(%rax)
.L4551:
	movq	1424(%rbx), %rsi
	jmp	.L4548
	.p2align 4,,10
	.p2align 3
.L4553:
	leaq	-32(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L4546
	.p2align 4,,10
	.p2align 3
.L4549:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L4562
	jmp	.L4551
.L4564:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10242:
	.size	_ZN4node15AsyncWrapObjectD2Ev, .-_ZN4node15AsyncWrapObjectD2Ev
	.weak	_ZN4node15AsyncWrapObjectD1Ev
	.set	_ZN4node15AsyncWrapObjectD1Ev,_ZN4node15AsyncWrapObjectD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrapD0Ev
	.type	_ZN4node9AsyncWrapD0Ev, @function
_ZN4node9AsyncWrapD0Ev:
.LFB7744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	movq	16(%r12), %rbx
	movsd	40(%r12), %xmm0
	movq	1216(%rbx), %rax
	movsd	%xmm0, -32(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L4585
.L4568:
	movq	.LC7(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	call	_ZN4node10BaseObjectD2Ev
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4586
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4585:
	.cfi_restore_state
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L4568
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L4568
	movq	1424(%rbx), %rsi
	cmpq	1416(%rbx), %rsi
	je	.L4587
.L4570:
	cmpq	1432(%rbx), %rsi
	je	.L4575
	movsd	-32(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	jmp	.L4568
	.p2align 4,,10
	.p2align 3
.L4587:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L4571
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L4573
.L4584:
	movq	(%rdi), %rax
	call	*8(%rax)
.L4573:
	movq	1424(%rbx), %rsi
	jmp	.L4570
	.p2align 4,,10
	.p2align 3
.L4575:
	leaq	-32(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L4568
	.p2align 4,,10
	.p2align 3
.L4571:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L4584
	jmp	.L4573
.L4586:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7744:
	.size	_ZN4node9AsyncWrapD0Ev, .-_ZN4node9AsyncWrapD0Ev
	.section	.text._ZN4node11PromiseWrapD0Ev,"axG",@progbits,_ZN4node11PromiseWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11PromiseWrapD0Ev
	.type	_ZN4node11PromiseWrapD0Ev, @function
_ZN4node11PromiseWrapD0Ev:
.LFB10240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	movq	16(%r12), %rbx
	movsd	40(%r12), %xmm0
	movq	1216(%rbx), %rax
	movsd	%xmm0, -32(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L4607
.L4590:
	movq	.LC7(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	call	_ZN4node10BaseObjectD2Ev
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4608
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4607:
	.cfi_restore_state
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L4590
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L4590
	movq	1424(%rbx), %rsi
	cmpq	1416(%rbx), %rsi
	je	.L4609
.L4592:
	cmpq	%rsi, 1432(%rbx)
	je	.L4597
	movsd	-32(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	jmp	.L4590
	.p2align 4,,10
	.p2align 3
.L4609:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L4593
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L4595
.L4606:
	movq	(%rdi), %rax
	call	*8(%rax)
.L4595:
	movq	1424(%rbx), %rsi
	jmp	.L4592
	.p2align 4,,10
	.p2align 3
.L4597:
	leaq	-32(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L4590
	.p2align 4,,10
	.p2align 3
.L4593:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L4606
	jmp	.L4595
.L4608:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10240:
	.size	_ZN4node11PromiseWrapD0Ev, .-_ZN4node11PromiseWrapD0Ev
	.section	.text._ZN4node15AsyncWrapObjectD0Ev,"axG",@progbits,_ZN4node15AsyncWrapObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15AsyncWrapObjectD0Ev
	.type	_ZN4node15AsyncWrapObjectD0Ev, @function
_ZN4node15AsyncWrapObjectD0Ev:
.LFB10244:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	movq	16(%r12), %rbx
	movsd	40(%r12), %xmm0
	movq	1216(%rbx), %rax
	movsd	%xmm0, -32(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L4629
.L4612:
	movq	.LC7(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	call	_ZN4node10BaseObjectD2Ev
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4630
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4629:
	.cfi_restore_state
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L4612
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L4612
	movq	1424(%rbx), %rsi
	cmpq	1416(%rbx), %rsi
	je	.L4631
.L4614:
	cmpq	%rsi, 1432(%rbx)
	je	.L4619
	movsd	-32(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	jmp	.L4612
	.p2align 4,,10
	.p2align 3
.L4631:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L4615
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L4617
.L4628:
	movq	(%rdi), %rax
	call	*8(%rax)
.L4617:
	movq	1424(%rbx), %rsi
	jmp	.L4614
	.p2align 4,,10
	.p2align 3
.L4619:
	leaq	-32(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L4612
	.p2align 4,,10
	.p2align 3
.L4615:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L4628
	jmp	.L4617
.L4630:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10244:
	.size	_ZN4node15AsyncWrapObjectD0Ev, .-_ZN4node15AsyncWrapObjectD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrapD2Ev
	.type	_ZN4node9AsyncWrapD2Ev, @function
_ZN4node9AsyncWrapD2Ev:
.LFB7742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9AsyncWrapE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv
	movq	16(%r12), %rbx
	movsd	40(%r12), %xmm0
	movq	1216(%rbx), %rax
	movsd	%xmm0, -32(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L4651
.L4634:
	movq	.LC7(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, 40(%r12)
	call	_ZN4node10BaseObjectD2Ev
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4652
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4651:
	.cfi_restore_state
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L4634
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L4634
	movq	1424(%rbx), %rsi
	cmpq	1416(%rbx), %rsi
	je	.L4653
.L4636:
	cmpq	1432(%rbx), %rsi
	je	.L4641
	movsd	-32(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	jmp	.L4634
	.p2align 4,,10
	.p2align 3
.L4653:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L4637
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L4639
.L4650:
	movq	(%rdi), %rax
	call	*8(%rax)
.L4639:
	movq	1424(%rbx), %rsi
	jmp	.L4636
	.p2align 4,,10
	.p2align 3
.L4641:
	leaq	-32(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L4634
	.p2align 4,,10
	.p2align 3
.L4637:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L4650
	jmp	.L4639
.L4652:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7742:
	.size	_ZN4node9AsyncWrapD2Ev, .-_ZN4node9AsyncWrapD2Ev
	.globl	_ZN4node9AsyncWrapD1Ev
	.set	_ZN4node9AsyncWrapD1Ev,_ZN4node9AsyncWrapD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9AsyncWrap11EmitDestroyEb
	.type	_ZN4node9AsyncWrap11EmitDestroyEb, @function
_ZN4node9AsyncWrap11EmitDestroyEb:
.LFB7726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %rbx
	movsd	40(%rdi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1216(%rbx), %rax
	movsd	%xmm0, -48(%rbp)
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L4674
.L4656:
	movq	.LC7(%rip), %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 40(%r12)
	je	.L4654
	testb	%r13b, %r13b
	je	.L4675
.L4654:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4676
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4675:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap11EmitDestroyEb.part.0
	jmp	.L4654
	.p2align 4,,10
	.p2align 3
.L4674:
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L4656
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L4656
	movq	1424(%rbx), %rsi
	cmpq	1416(%rbx), %rsi
	je	.L4677
.L4658:
	cmpq	%rsi, 1432(%rbx)
	je	.L4663
	movsd	-48(%rbp), %xmm0
	addq	$8, %rsi
	movsd	%xmm0, -8(%rsi)
	movq	%rsi, 1424(%rbx)
	jmp	.L4656
	.p2align 4,,10
	.p2align 3
.L4677:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	leaq	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L4659
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L4661
.L4673:
	movq	(%rdi), %rax
	call	*8(%rax)
.L4661:
	movq	1424(%rbx), %rsi
	jmp	.L4658
	.p2align 4,,10
	.p2align 3
.L4663:
	leaq	-48(%rbp), %rdx
	leaq	1416(%rbx), %rdi
	call	_ZNSt6vectorIdSaIdEE17_M_realloc_insertIJRKdEEEvN9__gnu_cxx17__normal_iteratorIPdS1_EEDpOT_
	jmp	.L4656
	.p2align 4,,10
	.p2align 3
.L4659:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L4673
	jmp	.L4661
.L4676:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7726:
	.size	_ZN4node9AsyncWrap11EmitDestroyEb, .-_ZN4node9AsyncWrap11EmitDestroyEb
	.section	.text._ZN4node7tracing11TracedValueD2Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD2Ev
	.type	_ZN4node7tracing11TracedValueD2Ev, @function
_ZN4node7tracing11TracedValueD2Ev:
.LFB9044:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L4678
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L4678:
	ret
	.cfi_endproc
.LFE9044:
	.size	_ZN4node7tracing11TracedValueD2Ev, .-_ZN4node7tracing11TracedValueD2Ev
	.weak	_ZN4node7tracing11TracedValueD1Ev
	.set	_ZN4node7tracing11TracedValueD1Ev,_ZN4node7tracing11TracedValueD2Ev
	.weak	_ZTVN4node15AsyncWrapObjectE
	.section	.data.rel.ro._ZTVN4node15AsyncWrapObjectE,"awG",@progbits,_ZTVN4node15AsyncWrapObjectE,comdat
	.align 8
	.type	_ZTVN4node15AsyncWrapObjectE, @object
	.size	_ZTVN4node15AsyncWrapObjectE, 96
_ZTVN4node15AsyncWrapObjectE:
	.quad	0
	.quad	0
	.quad	_ZN4node15AsyncWrapObjectD1Ev
	.quad	_ZN4node15AsyncWrapObjectD0Ev
	.quad	_ZNK4node15AsyncWrapObject10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15AsyncWrapObject14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15AsyncWrapObject8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node11PromiseWrapE
	.section	.data.rel.ro._ZTVN4node11PromiseWrapE,"awG",@progbits,_ZTVN4node11PromiseWrapE,comdat
	.align 8
	.type	_ZTVN4node11PromiseWrapE, @object
	.size	_ZTVN4node11PromiseWrapE, 96
_ZTVN4node11PromiseWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node11PromiseWrapD1Ev
	.quad	_ZN4node11PromiseWrapD0Ev
	.quad	_ZNK4node11PromiseWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node11PromiseWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node11PromiseWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node9AsyncWrapE
	.section	.data.rel.ro._ZTVN4node9AsyncWrapE,"awG",@progbits,_ZTVN4node9AsyncWrapE,comdat
	.align 8
	.type	_ZTVN4node9AsyncWrapE, @object
	.size	_ZTVN4node9AsyncWrapE, 96
_ZTVN4node9AsyncWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE
	.section	.data.rel.ro.local._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EEE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIPFvS2_EE4CallES2_
	.section	.rodata.str1.1
.LC149:
	.string	"../src/async_wrap.cc"
.LC150:
	.string	"async_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC149
	.quad	0
	.quad	_ZN4node9AsyncWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC150
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC151:
	.string	"../src/async_wrap.cc:794"
.LC152:
	.string	"!obj.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC153:
	.string	"static v8::Local<v8::Object> node::AsyncWrap::GetOwner(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @object
	.size	_ZZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node9AsyncWrap8GetOwnerEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.section	.rodata.str1.1
.LC154:
	.string	"../src/async_wrap.cc:739"
.LC155:
	.string	"!type.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC156:
	.string	"static void node::AsyncWrap::EmitAsyncInit(node::Environment*, v8::Local<v8::Object>, v8::Local<v8::String>, double, double)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args_0, @object
	.size	_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args_0, 24
_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args_0:
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.section	.rodata.str1.1
.LC157:
	.string	"../src/async_wrap.cc:738"
.LC158:
	.string	"!object.IsEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args, @object
	.size	_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args, 24
_ZZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEddE4args:
	.quad	.LC157
	.quad	.LC158
	.quad	.LC156
	.section	.rodata.str1.1
.LC159:
	.string	"../src/async_wrap.cc:722"
.LC160:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"void node::AsyncWrap::AsyncReset(v8::Local<v8::Object>, double, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_1, @object
	.size	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_1, 24
_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_1:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__45_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__45_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__44_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__44_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__43_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__43_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__42_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__42_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__41_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__41_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__40_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__40_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__39_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__39_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__38_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__38_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__37_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__37_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__36_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__36_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__35_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__35_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__34_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__34_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__33_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__33_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__32_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__32_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__31_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__31_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__30_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__30_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__29_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__29_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__28_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__28_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__27_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__27_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__26_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__26_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__25_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__25_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__24_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__24_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__23_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__23_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__22_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__22_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__21_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__21_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__20_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__20_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__19_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__19_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__18_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__18_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__17_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__17_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__16_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__16_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__15_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__15_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__14_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__14_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__13_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__13_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__12_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__12_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__11_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__11_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__10_
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719__10_,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_9
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_9,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_8
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_8,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_7
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_7,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_6
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_6,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_5
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_5,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_4
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_4,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_3
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_3,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_2
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_2,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_1
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_1,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_0
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719_0,8,8
	.local	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719
	.comm	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE28trace_event_unique_atomic719,8,8
	.section	.rodata.str1.1
.LC162:
	.string	"../src/async_wrap.cc:697"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_0, @object
	.size	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_0, 24
_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args_0:
	.quad	.LC162
	.quad	.LC152
	.quad	.LC161
	.section	.rodata.str1.1
.LC163:
	.string	"../src/async_wrap.cc:680"
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"(provider_type()) != (PROVIDER_NONE)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args, @object
	.size	_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args, 24
_ZZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdbE4args:
	.quad	.LC163
	.quad	.LC164
	.quad	.LC161
	.section	.rodata.str1.1
.LC165:
	.string	"../src/async_wrap.cc:658"
	.section	.rodata.str1.8
	.align 8
.LC166:
	.string	"void node::AsyncWrap::EmitTraceEventDestroy()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE4args, @object
	.size	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE4args, 24
_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE4args:
	.quad	.LC165
	.quad	.LC160
	.quad	.LC166
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__45_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__45_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__44_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__44_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__43_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__43_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__42_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__42_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__41_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__41_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__40_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__40_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__39_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__39_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__38_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__38_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__37_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__37_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__36_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__36_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__35_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__35_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__34_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__34_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__33_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__33_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__32_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__32_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__31_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__31_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__30_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__30_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__29_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__29_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__28_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__28_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__27_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__27_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__26_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__26_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__25_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__25_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__24_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__24_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__23_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__23_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__22_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__22_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__21_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__21_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__20_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__20_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__19_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__19_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__18_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__18_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__17_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__17_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__16_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__16_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__15_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__15_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__14_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__14_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__13_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__13_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__12_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__12_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__11_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__11_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__10_
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655__10_,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_9
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_9,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_8
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_8,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_7
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_7,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_6
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_6,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_5
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_5,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_4
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_4,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_3
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_3,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_2
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_2,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_1
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_1,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_0
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655_0,8,8
	.local	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655
	.comm	_ZZN4node9AsyncWrap21EmitTraceEventDestroyEvE28trace_event_unique_atomic655,8,8
	.section	.rodata.str1.1
.LC167:
	.string	"../src/async_wrap.cc:605"
.LC168:
	.string	"(provider) != (PROVIDER_NONE)"
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"node::AsyncWrap::AsyncWrap(node::Environment*, v8::Local<v8::Object>, node::AsyncWrap::ProviderType, double, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdbE4args, @object
	.size	_ZZN4node9AsyncWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdbE4args, 24
_ZZN4node9AsyncWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEdbE4args:
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.section	.rodata.str1.1
.LC170:
	.string	"../src/async_wrap.cc:475"
.LC171:
	.string	"args[0]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"static void node::AsyncWrap::SetCallbackTrampoline(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node9AsyncWrap21SetCallbackTrampolineERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.section	.rodata.str1.1
.LC173:
	.string	"../src/async_wrap.cc:466"
.LC174:
	.string	"args[0]->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"static void node::AsyncWrap::QueueDestroyAsyncId(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node9AsyncWrap19QueueDestroyAsyncIdERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC173
	.quad	.LC174
	.quad	.LC175
	.section	.rodata.str1.1
.LC176:
	.string	"../src/async_wrap.cc:434"
.LC177:
	.string	"args[0]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"static void node::AsyncWrap::AsyncReset(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node9AsyncWrap10AsyncResetERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.section	.rodata.str1.1
.LC179:
	.string	"../src/async_wrap.cc:396"
.LC180:
	.string	"args[2]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC181:
	.string	"void node::RegisterDestroyHook(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, @object
	.size	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, 24
_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1:
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.section	.rodata.str1.1
.LC182:
	.string	"../src/async_wrap.cc:395"
.LC183:
	.string	"args[1]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC182
	.quad	.LC183
	.quad	.LC181
	.section	.rodata.str1.1
.LC184:
	.string	"../src/async_wrap.cc:394"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL19RegisterDestroyHookERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC184
	.quad	.LC177
	.quad	.LC181
	.section	.rodata.str1.1
.LC185:
	.string	"../src/async_wrap.cc:326"
.LC186:
	.string	"v->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC187:
	.string	"void node::SetupHooks(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_5, @object
	.size	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_5, 24
_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_5:
	.quad	.LC185
	.quad	.LC186
	.quad	.LC187
	.section	.rodata.str1.1
.LC188:
	.string	"../src/async_wrap.cc:325"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_4, @object
	.size	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_4, 24
_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_4:
	.quad	.LC188
	.quad	.LC186
	.quad	.LC187
	.section	.rodata.str1.1
.LC189:
	.string	"../src/async_wrap.cc:324"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_3, @object
	.size	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_3, 24
_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_3:
	.quad	.LC189
	.quad	.LC186
	.quad	.LC187
	.section	.rodata.str1.1
.LC190:
	.string	"../src/async_wrap.cc:323"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2, @object
	.size	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2, 24
_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_2:
	.quad	.LC190
	.quad	.LC186
	.quad	.LC187
	.section	.rodata.str1.1
.LC191:
	.string	"../src/async_wrap.cc:322"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, @object
	.size	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1, 24
_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_1:
	.quad	.LC191
	.quad	.LC186
	.quad	.LC187
	.section	.rodata.str1.1
.LC192:
	.string	"../src/async_wrap.cc:308"
	.section	.rodata.str1.8
	.align 8
.LC193:
	.string	"env->async_hooks_init_function().IsEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC192
	.quad	.LC193
	.quad	.LC187
	.section	.rodata.str1.1
.LC194:
	.string	"../src/async_wrap.cc:302"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL10SetupHooksERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC194
	.quad	.LC177
	.quad	.LC187
	.section	.rodata.str1.1
.LC195:
	.string	"../src/async_wrap.cc:223"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"(promise->GetAlignedPointerFromInternalField(0)) == nullptr"
	.align 8
.LC197:
	.string	"static node::PromiseWrap* node::PromiseWrap::New(node::Environment*, v8::Local<v8::Promise>, node::PromiseWrap*, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_bE4args, @object
	.size	_ZZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_bE4args, 24
_ZZN4node11PromiseWrap3NewEPNS_11EnvironmentEN2v85LocalINS3_7PromiseEEEPS0_bE4args:
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.section	.rodata.str1.1
.LC198:
	.string	"../src/async_wrap.cc:178"
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"static void node::AsyncWrap::EmitTraceEventAfter(node::AsyncWrap::ProviderType, double)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE4args, @object
	.size	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE4args, 24
_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE4args:
	.quad	.LC198
	.quad	.LC160
	.quad	.LC199
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__45_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__45_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__44_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__44_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__43_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__43_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__42_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__42_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__41_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__41_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__40_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__40_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__39_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__39_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__38_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__38_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__37_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__37_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__36_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__36_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__35_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__35_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__34_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__34_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__33_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__33_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__32_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__32_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__31_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__31_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__30_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__30_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__29_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__29_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__28_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__28_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__27_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__27_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__26_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__26_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__25_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__25_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__24_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__24_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__23_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__23_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__22_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__22_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__21_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__21_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__20_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__20_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__19_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__19_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__18_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__18_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__17_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__17_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__16_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__16_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__15_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__15_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__14_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__14_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__13_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__13_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__12_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__12_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__11_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__11_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__10_
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175__10_,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_9
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_9,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_8
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_8,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_7
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_7,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_6
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_6,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_5
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_5,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_4
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_4,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_3
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_3,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_2
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_2,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_1
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_1,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_0
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175_0,8,8
	.local	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175
	.comm	_ZZN4node9AsyncWrap19EmitTraceEventAfterENS0_12ProviderTypeEdE28trace_event_unique_atomic175,8,8
	.section	.rodata.str1.1
.LC200:
	.string	"../src/async_wrap.cc:156"
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"void node::AsyncWrap::EmitTraceEventBefore()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE4args, @object
	.size	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE4args, 24
_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE4args:
	.quad	.LC200
	.quad	.LC160
	.quad	.LC201
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__45_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__45_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__44_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__44_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__43_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__43_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__42_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__42_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__41_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__41_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__40_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__40_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__39_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__39_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__38_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__38_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__37_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__37_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__36_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__36_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__35_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__35_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__34_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__34_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__33_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__33_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__32_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__32_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__31_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__31_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__30_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__30_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__29_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__29_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__28_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__28_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__27_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__27_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__26_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__26_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__25_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__25_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__24_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__24_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__23_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__23_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__22_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__22_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__21_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__21_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__20_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__20_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__19_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__19_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__18_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__18_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__17_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__17_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__16_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__16_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__15_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__15_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__14_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__14_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__13_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__13_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__12_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__12_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__11_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__11_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__10_
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153__10_,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_9
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_9,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_8
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_8,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_7
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_7,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_6
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_6,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_5
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_5,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_4
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_4,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_3
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_3,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_2
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_2,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_1
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_1,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_0
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153_0,8,8
	.local	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153
	.comm	_ZZN4node9AsyncWrap20EmitTraceEventBeforeEvE28trace_event_unique_atomic153,8,8
	.weak	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1
	.section	.rodata.str1.1
.LC202:
	.string	"../src/async_wrap.cc:75"
.LC203:
	.string	"args[0]->IsUint32()"
	.section	.rodata.str1.8
	.align 8
.LC204:
	.string	"static void node::AsyncWrapObject::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local._ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1,"awG",@progbits,_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1,comdat
	.align 16
	.type	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @gnu_unique_object
	.size	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC202
	.quad	.LC203
	.quad	.LC204
	.weak	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0
	.section	.rodata.str1.1
.LC205:
	.string	"../src/async_wrap.cc:74"
	.section	.rodata.str1.8
	.align 8
.LC206:
	.string	"env->async_wrap_object_ctor_template()->HasInstance(args.This())"
	.section	.data.rel.ro.local._ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC205
	.quad	.LC206
	.quad	.LC204
	.weak	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args
	.section	.rodata.str1.1
.LC207:
	.string	"../src/async_wrap.cc:73"
.LC208:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local._ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args,"awG",@progbits,_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node15AsyncWrapObject3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC207
	.quad	.LC208
	.quad	.LC204
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN4nodeL14provider_namesE, @object
	.size	_ZN4nodeL14provider_namesE, 376
_ZN4nodeL14provider_namesE:
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.weak	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381
	.section	.bss._ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381,"awG",@nobits,_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381,comdat
	.align 8
	.type	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381, @gnu_unique_object
	.size	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381, 8
_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381:
	.zero	8
	.weak	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378
	.section	.bss._ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378,"awG",@nobits,_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378,comdat
	.align 8
	.type	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378, @gnu_unique_object
	.size	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378, 8
_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378:
	.zero	8
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC209:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC210:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC211:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC212:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC213:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC214:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC215:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC216:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC217:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC218:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC219:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC220:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC221:
	.string	"../src/base_object-inl.h:44"
.LC222:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC221
	.quad	.LC222
	.quad	.LC220
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC223:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC224:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC225:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC226:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC227:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC228:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.weak	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0
	.section	.rodata.str1.1
.LC229:
	.string	"../src/env-inl.h:133"
.LC230:
	.string	"(trigger_async_id) >= (-1)"
	.section	.rodata.str1.8
	.align 8
.LC231:
	.string	"void node::AsyncHooks::push_async_context(double, double, v8::Local<v8::Value>)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0, 24
_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0:
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.weak	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args
	.section	.rodata.str1.1
.LC232:
	.string	"../src/env-inl.h:132"
.LC233:
	.string	"(async_id) >= (-1)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args,"awG",@progbits,_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args, 24
_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args:
	.quad	.LC232
	.quad	.LC233
	.quad	.LC231
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	-1074790400
	.align 8
.LC8:
	.long	0
	.long	0
	.align 8
.LC143:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
