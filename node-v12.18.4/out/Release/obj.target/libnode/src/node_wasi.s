	.file	"node_wasi.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4698:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4698:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4700:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4700:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7168:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7168:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7169:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7169:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7170:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7170:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7172:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L8
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7172:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZNK4node4wasi4WASI8SelfSizeEv,"axG",@progbits,_ZNK4node4wasi4WASI8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4wasi4WASI8SelfSizeEv
	.type	_ZNK4node4wasi4WASI8SelfSizeEv, @function
_ZNK4node4wasi4WASI8SelfSizeEv:
.LFB7741:
	.cfi_startproc
	endbr64
	movl	$168, %eax
	ret
	.cfi_endproc
.LFE7741:
	.size	_ZNK4node4wasi4WASI8SelfSizeEv, .-_ZNK4node4wasi4WASI8SelfSizeEv
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10617:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L10
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE10617:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv,"axG",@progbits,_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv
	.type	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv, @function
_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv:
.LFB10646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	-8(%rsi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	%rax, 128(%rdi)
	movq	-16(%rdi), %rax
	movq	-8(%rsi), %r14
	movq	352(%rax), %r12
	movq	32(%r12), %r13
	subq	%r14, %r13
	movq	%r13, %rax
	subq	48(%r12), %rax
	movq	%r13, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L20
.L13:
	testq	%r14, %r14
	jg	.L21
	je	.L15
	cmpq	40(%r12), %r13
	jg	.L22
.L15:
	movq	$0, -8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	40(%r12), %rax
	subq	%r14, %rax
	cmpq	$67108864, %rax
	jle	.L15
	movq	%rax, 40(%r12)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L15
	.cfi_endproc
.LFE10646:
	.size	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv, .-_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10619:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"memory"
.LC2:
	.string	"uvwasi_memory"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node4wasi4WASI10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node4wasi4WASI10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node4wasi4WASI10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L28
	movq	(%rax), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L28
	movq	8(%rbx), %r13
	leaq	-48(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L38
	cmpq	72(%rbx), %rcx
	je	.L49
.L31:
	movq	-8(%rcx), %rsi
.L30:
	leaq	.LC1(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L28:
	movq	160(%r12), %r13
	testq	%r13, %r13
	je	.L26
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$13, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC2(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 64(%r12)
	leaq	-48(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -48(%rbp)
	call	*%rax
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	(%rdi), %rax
	call	*8(%rax)
.L34:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L26
	cmpq	72(%rbx), %rax
	je	.L50
.L36:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L26
	movq	8(%rbx), %rdi
	leaq	.LC2(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L26:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L50:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%esi, %esi
	jmp	.L30
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7757:
	.size	_ZNK4node4wasi4WASI10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node4wasi4WASI10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0:
.LFB10884:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L53
	testq	%rdx, %rdx
	je	.L53
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L53:
	subq	%r13, %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L54
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L54:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L55
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L56
.L55:
	testq	%rbx, %rbx
	je	.L56
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L56:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L57
	call	__stack_chk_fail@PLT
.L57:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10884:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	.section	.text._ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_,"axG",@progbits,_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_
	.type	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_, @function
_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_:
.LFB9127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	jne	.L69
	testq	%rdi, %rdi
	je	.L88
	movq	-8(%rdi), %rbx
	leaq	-8(%rdi), %r15
	testq	%rbx, %rbx
	je	.L71
	cmpq	160(%rdx), %rbx
	jbe	.L104
.L87:
	leaq	_ZZNK4node4wasi4WASI18CheckAllocatedSizeEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	8(%rsi), %r14
	testq	%rdi, %rdi
	je	.L89
	movq	-8(%rdi), %rbx
	leaq	-8(%rdi), %r15
	testq	%rbx, %rbx
	je	.L107
	cmpq	160(%rdx), %rbx
	ja	.L87
.L73:
	testq	%r14, %r14
	je	.L104
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	realloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L108
.L81:
	movq	%r14, %rax
	subq	%rbx, %rax
	addq	%rax, 160(%r13)
	movq	%rax, %rbx
	movq	16(%r13), %rax
	movq	352(%rax), %r13
	movq	32(%r13), %r15
	addq	%rbx, %r15
	movq	%r15, %rax
	subq	48(%r13), %rax
	movq	%r15, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L109
.L83:
	testq	%rbx, %rbx
	js	.L110
	je	.L85
	cmpq	40(%r13), %r15
	jle	.L85
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%r14, (%r12)
	leaq	8(%r12), %rax
.L68:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	addq	40(%r13), %rbx
	cmpq	$67108864, %rbx
	jle	.L85
	movq	%rbx, 40(%r13)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%r12d, %r12d
.L70:
	call	free@PLT
	movq	16(%r13), %rax
	subq	%rbx, 160(%r13)
	movq	352(%rax), %r14
	movq	32(%r14), %r13
	subq	%rbx, %r13
	movq	%r13, %rax
	subq	48(%r14), %rax
	movq	%r13, 32(%r14)
	cmpq	$33554432, %rax
	jg	.L111
.L80:
	testq	%r12, %r12
	jg	.L112
	je	.L105
	cmpq	40(%r14), %r13
	jle	.L105
	movq	%r14, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	xorl	%eax, %eax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L112:
	movq	40(%r14), %rax
	subq	%rbx, %rax
	cmpq	$67108864, %rax
	jle	.L105
	movq	%rax, 40(%r14)
.L105:
	xorl	%eax, %eax
.L113:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L108:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	realloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L81
	xorl	%eax, %eax
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L107:
	testq	%r14, %r14
	je	.L71
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L114
.L77:
	addq	$8, %rax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r14, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r15, %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L77
	xorl	%eax, %eax
	jmp	.L113
.L104:
	movq	%rbx, %r12
	movq	%r15, %rdi
	jmp	.L70
	.cfi_endproc
.LFE9127:
	.size	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_, .-_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpl	$1, 16(%rdi)
	jne	.L136
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L137
	movq	8(%rbx), %r12
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L138
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L128
	cmpw	$1040, %cx
	jne	.L119
.L128:
	movq	23(%rdx), %r12
.L121:
	testq	%r12, %r12
	je	.L115
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L139
	movq	8(%rbx), %r13
.L125:
	movq	16(%r12), %rax
	movq	112(%r12), %rdi
	movq	352(%rax), %r14
	testq	%rdi, %rdi
	je	.L126
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 112(%r12)
.L126:
	testq	%r13, %r13
	je	.L115
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 112(%r12)
.L115:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7810:
	.size	_ZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev:
.LFB7740:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1230192983, 16(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	$4, 8(%rdi)
	movb	$0, 20(%rdi)
	ret
	.cfi_endproc
.LFE7740:
	.size	_ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev, .-_ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev
	.section	.text.unlikely
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB10737:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L142
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L143
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L143:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L144
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L145
.L144:
	testq	%rbx, %rbx
	je	.L145
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L145:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L146
	call	__stack_chk_fail@PLT
.L142:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L146:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10737:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata.str1.1
.LC4:
	.string	"0123456789abcdef"
	.section	.text.unlikely
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB10850:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC4(%rip), %rax
.L153:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L153
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L154
	call	__stack_chk_fail@PLT
.L154:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10850:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB10858:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC4(%rip), %rax
.L158:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L158
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L159
	call	__stack_chk_fail@PLT
.L159:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10858:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB10864:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzwl	%si, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC4(%rip), %rax
.L163:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L163
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L164
	call	__stack_chk_fail@PLT
.L164:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10864:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB10870:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC4(%rip), %rax
.L168:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L168
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L169
	call	__stack_chk_fail@PLT
.L169:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10870:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text.unlikely
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB10876:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%sil, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC4(%rip), %rax
.L173:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L173
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L174
	call	__stack_chk_fail@PLT
.L174:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10876:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.rodata.str1.1
.LC5:
	.string	"WASI"
.LC6:
	.string	"args_get"
.LC7:
	.string	"args_sizes_get"
.LC8:
	.string	"clock_res_get"
.LC9:
	.string	"clock_time_get"
.LC10:
	.string	"environ_get"
.LC11:
	.string	"environ_sizes_get"
.LC12:
	.string	"fd_advise"
.LC13:
	.string	"fd_allocate"
.LC14:
	.string	"fd_close"
.LC15:
	.string	"fd_datasync"
.LC16:
	.string	"fd_fdstat_get"
.LC17:
	.string	"fd_fdstat_set_flags"
.LC18:
	.string	"fd_fdstat_set_rights"
.LC19:
	.string	"fd_filestat_get"
.LC20:
	.string	"fd_filestat_set_size"
.LC21:
	.string	"fd_filestat_set_times"
.LC22:
	.string	"fd_pread"
.LC23:
	.string	"fd_prestat_get"
.LC24:
	.string	"fd_prestat_dir_name"
.LC25:
	.string	"fd_pwrite"
.LC26:
	.string	"fd_read"
.LC27:
	.string	"fd_readdir"
.LC28:
	.string	"fd_renumber"
.LC29:
	.string	"fd_seek"
.LC30:
	.string	"fd_sync"
.LC31:
	.string	"fd_tell"
.LC32:
	.string	"fd_write"
.LC33:
	.string	"path_create_directory"
.LC34:
	.string	"path_filestat_get"
.LC35:
	.string	"path_filestat_set_times"
.LC36:
	.string	"path_link"
.LC37:
	.string	"path_open"
.LC38:
	.string	"path_readlink"
.LC39:
	.string	"path_remove_directory"
.LC40:
	.string	"path_rename"
.LC41:
	.string	"path_symlink"
.LC42:
	.string	"path_unlink_file"
.LC43:
	.string	"poll_oneoff"
.LC44:
	.string	"proc_exit"
.LC45:
	.string	"proc_raise"
.LC46:
	.string	"random_get"
.LC47:
	.string	"sched_yield"
.LC48:
	.string	"sock_recv"
.LC49:
	.string	"sock_send"
.LC50:
	.string	"sock_shutdown"
.LC51:
	.string	"_setMemory"
	.section	.text.unlikely
.LCOLDB52:
	.text
.LHOTB52:
	.p2align 4
	.type	_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L178
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L178
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L178
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$4, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L232
.L179:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L233
.L180:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L234
.L181:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L235
.L182:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L236
.L183:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L237
.L184:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L238
.L185:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L239
.L186:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L240
.L187:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L241
.L188:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L242
.L189:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L243
.L190:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L244
.L191:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L245
.L192:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC19(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L246
.L193:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L247
.L194:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L248
.L195:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L249
.L196:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L250
.L197:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC24(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L251
.L198:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC25(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L252
.L199:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC26(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L253
.L200:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC27(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L254
.L201:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC28(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L255
.L202:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC29(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L256
.L203:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC30(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L257
.L204:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L258
.L205:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC32(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L259
.L206:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC33(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L260
.L207:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC34(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L261
.L208:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC35(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L262
.L209:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC36(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L263
.L210:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC37(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L264
.L211:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC38(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L265
.L212:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC39(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L266
.L213:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC40(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L267
.L214:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC41(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L268
.L215:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC42(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L269
.L216:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC43(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L270
.L217:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC44(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L271
.L218:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC45(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L272
.L219:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC46(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L273
.L220:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC47(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L274
.L221:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC48(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L275
.L222:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC49(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L276
.L223:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC50(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L277
.L224:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC51(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L278
.L225:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L279
.L226:
	movq	3280(%rbx), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L280
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L235:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L244:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L246:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L257:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L258:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L260:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L262:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L266:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L271:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L273:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L276:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7812:
.L178:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7812:
	.text
	.size	_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE52:
	.text
.LHOTE52:
	.section	.text._ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv,"axG",@progbits,_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv
	.type	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv, @function
_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv:
.LFB9124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	jne	.L302
.L282:
	movq	16(%r13), %rax
	movq	352(%rax), %rdi
	movq	32(%rdi), %rax
	subq	48(%rdi), %rax
	cmpq	$33554432, %rax
	jg	.L284
	xorl	%eax, %eax
.L307:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_restore_state
	movq	%rdi, %rbx
	addq	$8, %rbx
	je	.L282
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L303
.L286:
	movq	16(%r13), %rax
	addq	%rbx, 160(%r13)
	movq	352(%rax), %r13
	movq	32(%r13), %r14
	addq	%rbx, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L304
	movq	40(%r13), %rax
	testq	%rbx, %rbx
	jns	.L305
.L290:
	addq	%rbx, %rax
	cmpq	$67108864, %rax
	jg	.L306
.L289:
	movq	%rbx, (%r12)
	leaq	8(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	40(%r13), %rax
	testq	%rbx, %rbx
	js	.L290
.L305:
	cmpq	%rax, %r14
	jle	.L289
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rax, 40(%r13)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L303:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L286
	xorl	%eax, %eax
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L284:
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	xorl	%eax, %eax
	jmp	.L307
	.cfi_endproc
.LFE9124:
	.size	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv, .-_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv
	.section	.text._ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv,"axG",@progbits,_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv
	.type	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv, @function
_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv:
.LFB9126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	jne	.L332
.L309:
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	movq	32(%rdi), %rax
	subq	48(%rdi), %rax
	cmpq	$33554432, %rax
	jg	.L312
.L331:
	xorl	%r8d, %r8d
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%rdi, %r14
	xorl	%edx, %edx
	imulq	%rsi, %r14
	movq	%r14, %rax
	divq	%rdi
	cmpq	%rax, %rsi
	jne	.L333
	testq	%r14, %r14
	je	.L309
	movq	%r14, %r13
	addq	$8, %r13
	je	.L309
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L334
.L314:
	movq	16(%r12), %rax
	addq	%r13, 160(%r12)
	movq	352(%rax), %r12
	movq	32(%r12), %r15
	addq	%r13, %r15
	movq	%r15, %rax
	subq	48(%r12), %rax
	movq	%r15, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L335
	movq	40(%r12), %rax
	testq	%r13, %r13
	jns	.L336
.L318:
	addq	%r13, %rax
	cmpq	$67108864, %rax
	jg	.L337
.L317:
	movq	%r13, (%rbx)
	leaq	8(%rbx), %r8
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	call	memset@PLT
	movq	%rax, %r8
.L308:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	movq	%rax, 40(%r12)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	40(%r12), %rax
	testq	%r13, %r13
	js	.L318
.L336:
	cmpq	%r15, %rax
	jge	.L317
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L334:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L331
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L312:
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L331
	.cfi_endproc
.LFE9126:
	.size	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv, .-_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv
	.section	.text._ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_,"axG",@progbits,_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_
	.type	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_, @function
_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_:
.LFB9125:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L347
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-8(%rdi), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	-8(%rdi), %r12
	testq	%r12, %r12
	je	.L350
	movq	%rsi, %rbx
	cmpq	160(%rsi), %r12
	ja	.L351
	movq	%r8, %rdi
	call	free@PLT
	movq	16(%rbx), %rax
	subq	%r12, 160(%rbx)
	movq	352(%rax), %r13
	movq	32(%r13), %rbx
	subq	%r12, %rbx
	movq	%rbx, %rax
	subq	48(%r13), %rax
	movq	%rbx, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L352
.L343:
	movq	40(%r13), %rax
	testq	%r12, %r12
	jg	.L353
	cmpq	%rbx, %rax
	jl	.L354
.L338:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	addq	$8, %rsp
	movq	%r8, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L351:
	.cfi_restore_state
	leaq	_ZZNK4node4wasi4WASI18CheckAllocatedSizeEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L353:
	subq	%r12, %rax
	cmpq	$67108864, %rax
	jle	.L338
	movq	%rax, 40(%r13)
	jmp	.L338
.L354:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.cfi_endproc
.LFE9125:
	.size	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_, .-_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L355
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L359:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L357
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L359
.L355:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L359
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE,"axG",@progbits,_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE, @function
_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE:
.LFB6241:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L365
	cmpw	$1040, %cx
	jne	.L362
.L365:
	movq	23(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	xorl	%esi, %esi
	jmp	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	.cfi_endproc
.LFE6241:
	.size	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE, .-_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L368
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L369:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L368
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L368
	movq	%r9, %rdi
.L371:
	cmpq	%rsi, %rbx
	jne	.L369
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L369
	cmpq	16(%rdi), %rbx
	jne	.L369
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L403
	testq	%rsi, %rsi
	je	.L373
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L373
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L373:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L368:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L404
.L376:
	cmpq	$0, 8(%rbx)
	je	.L367
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L380
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L405
.L380:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L367
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L367:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L406
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L385
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L373
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L372:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L407
.L374:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L405:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%r10, %rax
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L404:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L408
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L376
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%rsi, 2600(%r12)
	jmp	.L374
.L408:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L406:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7084:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASID2Ev
	.type	_ZN4node4wasi4WASID2Ev, @function
_ZN4node4wasi4WASID2Ev:
.LFB7754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4wasi4WASIE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$88, %rax
	movq	%rax, -8(%rdi)
	call	uvwasi_destroy@PLT
	cmpq	$0, 160(%r12)
	jne	.L416
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L411
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L411:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	leaq	_ZZN4node4wasi4WASID4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7754:
	.size	_ZN4node4wasi4WASID2Ev, .-_ZN4node4wasi4WASID2Ev
	.globl	_ZN4node4wasi4WASID1Ev
	.set	_ZN4node4wasi4WASID1Ev,_ZN4node4wasi4WASID2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASID0Ev
	.type	_ZN4node4wasi4WASID0Ev, @function
_ZN4node4wasi4WASID0Ev:
.LFB7756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4wasi4WASIE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$88, %rax
	movq	%rax, -8(%rdi)
	call	uvwasi_destroy@PLT
	cmpq	$0, 160(%r12)
	jne	.L424
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L419
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L419:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	leaq	_ZZN4node4wasi4WASID4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7756:
	.size	_ZN4node4wasi4WASID0Ev, .-_ZN4node4wasi4WASID0Ev
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node4wasi4WASID0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L426
	leaq	16+_ZTVN4node4wasi4WASIE(%rip), %rax
	leaq	40(%rdi), %rdi
	movq	%rax, -40(%rdi)
	addq	$88, %rax
	movq	%rax, -8(%rdi)
	call	uvwasi_destroy@PLT
	cmpq	$0, 160(%r12)
	jne	.L433
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L428
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L428:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L433:
	.cfi_restore_state
	leaq	_ZZN4node4wasi4WASID4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7098:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L435
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L435:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L451
.L436:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L437
	movq	8(%rax), %rax
	leaq	_ZN4node4wasi4WASID0Ev(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L438
	leaq	16+_ZTVN4node4wasi4WASIE(%rip), %rax
	leaq	40(%r12), %rdi
	movq	%rax, (%r12)
	addq	$88, %rax
	movq	%rax, 32(%r12)
	call	uvwasi_destroy@PLT
	cmpq	$0, 160(%r12)
	jne	.L452
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L440
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L440:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$168, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L437:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L436
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L452:
	leaq	_ZZN4node4wasi4WASID4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7086:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB7140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L456
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L485
	cmpq	$1, %rax
	jne	.L459
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L460:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L455:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L460
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L458:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L456:
	cmpb	$37, 1(%rax)
	jne	.L487
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L488
	cmpq	$1, %r13
	jne	.L465
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L466:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L468
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L489
.L468:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L470:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L490
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L472:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L473
	call	_ZdlPv@PLT
.L473:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L455
	call	_ZdlPv@PLT
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L465:
	testq	%r13, %r13
	jne	.L491
	movq	%rbx, %rax
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L464:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L490:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L489:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L470
.L487:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L486:
	call	__stack_chk_fail@PLT
.L491:
	movq	%rbx, %rdi
	jmp	.L464
	.cfi_endproc
.LFE7140:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.section	.rodata._ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE.str1.1,"aMS",@progbits,1
.LC53:
	.string	"ERR_WASI_NOT_STARTED"
	.section	.rodata._ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE.str1.8,"aMS",@progbits,1
	.align 8
.LC54:
	.string	"wasi.start() has not been called"
	.section	.rodata._ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE.str1.1
.LC55:
	.string	"code"
	.section	.text._ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE,"axG",@progbits,_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE,comdat
	.p2align 4
	.weak	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	.type	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE, @function
_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE:
.LFB7726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$16, %rsp
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L499
.L493:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC54(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L500
.L494:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L501
.L495:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L502
.L496:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L503
.L497:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L499:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L500:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L501:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L502:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L503:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L497
	.cfi_endproc
.LFE7726:
	.size	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE, .-_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	.section	.rodata.str1.1
.LC56:
	.string	"uvwasi_init"
.LC57:
	.string	", "
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASIC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s
	.type	_ZN4node4wasi4WASIC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s, @function
_ZN4node4wasi4WASIC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s:
.LFB7751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L505
	movq	%rdx, %rsi
	movq	%rdx, %r13
	movq	%rcx, %r14
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L567
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %r15
	movl	$40, %edi
	leaq	1(%r15), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	2592(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r15, 24(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r9
	movq	%rdx, %r10
	leaq	0(,%rdx,8), %r8
	testq	%r9, %r9
	je	.L507
	movq	(%r9), %rax
	movq	32(%rax), %rcx
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L508:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L507
	movq	32(%rdi), %rcx
	movq	%rax, %r9
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L507
	movq	%rdi, %rax
.L510:
	cmpq	%rcx, %r12
	jne	.L508
	cmpq	%r11, 8(%rax)
	jne	.L508
	cmpq	16(%rax), %r12
	jne	.L508
	cmpq	$0, (%r9)
	je	.L507
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L507:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	movq	%r8, -56(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r15
	testb	%al, %al
	jne	.L511
	movq	2584(%rbx), %r9
	movq	-56(%rbp), %r8
	movq	%r12, 32(%r13)
	addq	%r9, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L521
.L575:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r8), %rax
	movq	%r13, (%rax)
.L522:
	leaq	16+_ZTVN4node4wasi4WASIE(%rip), %rax
	addq	$1, 2656(%rbx)
	movq	%rax, (%r12)
	addq	$88, %rax
	movq	%rax, 32(%r12)
	movq	24(%r12), %rax
	addq	$1, 2608(%rbx)
	movq	$0, 160(%r12)
	movq	$0, 112(%r12)
	testq	%rax, %rax
	je	.L526
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L525
.L526:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L525:
	leaq	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE8FreeImplEPvS6_(%rip), %rax
	movq	%r12, 120(%r12)
	leaq	40(%r12), %rdi
	leaq	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10MallocImplEmPv(%rip), %rcx
	movq	%rax, %xmm2
	leaq	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE11ReallocImplEPvmS6_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm3
	leaq	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE10CallocImplEmmPv(%rip), %rsi
	punpcklqdq	%xmm2, %xmm0
	leaq	120(%r12), %rax
	movups	%xmm0, 128(%r12)
	movq	%rsi, %xmm0
	movq	%r14, %rsi
	movq	%rax, 56(%r14)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 144(%r12)
	call	uvwasi_init@PLT
	movzwl	%ax, %r12d
	testl	%r12d, %r12d
	jne	.L568
.L504:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r13
	testq	%r14, %r14
	je	.L530
	movq	%r14, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L530
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L530
	movq	271(%rax), %rbx
	testq	%rbx, %rbx
	je	.L530
	movl	%r12d, %edi
	call	uvwasi_embedder_err_code_to_string@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L569
.L533:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC56(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -64(%rbp)
	testq	%rax, %rax
	je	.L570
.L534:
	xorl	%edx, %edx
	movl	$2, %ecx
	leaq	.LC57(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L571
.L535:
	movq	-56(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	-64(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%rax, %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L572
.L536:
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L504
	movq	360(%rbx), %rax
	movq	-56(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	328(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L504
	movq	360(%rbx), %rax
	movq	-64(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L504
	testq	%r15, %r15
	je	.L504
	movq	%r14, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	addq	$40, %rsp
	movq	%r15, %rsi
	popq	%rbx
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L573
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L574
	leaq	0(,%rdx,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%rbx), %r10
	movq	%rax, %r9
.L514:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L516
	xorl	%edi, %edi
	leaq	2600(%rbx), %r8
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L518:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L519:
	testq	%rsi, %rsi
	je	.L516
.L517:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r15
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L518
	movq	2600(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L541
	movq	%rcx, (%r9,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L517
	.p2align 4,,10
	.p2align 3
.L516:
	movq	2584(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L520
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r9
.L520:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r13)
	divq	%r15
	movq	%r15, 2592(%rbx)
	movq	%r9, 2584(%rbx)
	leaq	0(,%rdx,8), %r8
	addq	%r9, %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L575
.L521:
	movq	2600(%rbx), %rax
	movq	%r13, 2600(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L523
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r9,%rdx,8)
.L523:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r8)
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%rdx, %rdi
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L505:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L567:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	_ZZN4node4wasiL13WASIExceptionEN2v85LocalINS1_7ContextEEEiPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L572:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	jmp	.L535
	.p2align 4,,10
	.p2align 3
.L570:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L569:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L573:
	leaq	2632(%rbx), %r9
	movq	$0, 2632(%rbx)
	movq	%r9, %r10
	jmp	.L514
.L574:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7751:
	.size	_ZN4node4wasi4WASIC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s, .-_ZN4node4wasi4WASIC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s
	.globl	_ZN4node4wasi4WASIC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s
	.set	_ZN4node4wasi4WASIC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s,_ZN4node4wasi4WASIC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2232, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L577
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L708
.L577:
	cmpl	$4, 16(%rbx)
	je	.L709
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L709:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L710
	cmpl	$1, 16(%rbx)
	jle	.L711
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L580:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L712
	cmpl	$2, 16(%rbx)
	jg	.L582
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L583:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L713
	cmpl	$3, 16(%rbx)
	jg	.L585
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L586:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L714
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L658
	cmpw	$1040, %cx
	jne	.L588
.L658:
	movq	23(%rdx), %r15
.L590:
	movl	16(%rbx), %eax
	movq	3280(%r15), %r13
	testl	%eax, %eax
	jg	.L591
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L592:
	movq	%r14, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -2252(%rbp)
	jg	.L593
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L594:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	$3, %eax
	jne	.L715
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L716
.L596:
	movq	%r13, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L717
.L597:
	sarq	$32, %rax
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	%eax, -2184(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L718
.L598:
	movq	%r13, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L719
.L599:
	sarq	$32, %rax
	movq	%r12, %rdi
	movl	$2, %edx
	movq	%r13, %rsi
	movl	%eax, -2180(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L720
.L600:
	movq	%r13, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L721
.L601:
	movl	-2252(%rbp), %eax
	sarq	$32, %r12
	movl	$3, -2224(%rbp)
	movl	%r12d, -2176(%rbp)
	movl	%eax, -2208(%rbp)
	testl	%eax, %eax
	jne	.L722
	movq	$0, -2200(%rbp)
.L608:
	cmpl	$1, 16(%rbx)
	jle	.L723
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r14
.L612:
	movq	%r14, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	leal	1(%rax), %edi
	movl	%eax, -2264(%rbp)
	movl	%eax, %r12d
	salq	$3, %rdi
	call	_Znam@PLT
	movq	%rax, -2192(%rbp)
	testl	%r12d, %r12d
	je	.L613
	leal	-1(%r12), %eax
	xorl	%r12d, %r12d
	movq	%rax, -2248(%rbp)
	leaq	-1104(%rbp), %rax
	movq	%rax, -2240(%rbp)
.L621:
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L724
.L614:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L725
.L615:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L711:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L708:
	cmpl	$5, 43(%rax)
	jne	.L577
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L722:
	movl	%eax, %edi
	movq	%rdi, %r12
	salq	$3, %rdi
	call	_Znam@PLT
	movq	%rax, -2200(%rbp)
	leal	-1(%r12), %eax
	xorl	%r12d, %r12d
	movq	%rax, -2248(%rbp)
	leaq	-1104(%rbp), %rax
	movq	%rax, -2240(%rbp)
.L610:
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L726
.L603:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L727
.L604:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L727:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L604
	movq	352(%r15), %rsi
	movq	-2240(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2200(%rbp), %rax
	movq	-1088(%rbp), %rdi
	leaq	(%rax,%r12,8), %rdx
	movq	%rdx, -2232(%rbp)
	call	strdup@PLT
	movq	-2232(%rbp), %rdx
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L728
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L607
	testq	%rdi, %rdi
	je	.L607
	call	free@PLT
	leaq	1(%r12), %rax
	cmpq	%r12, -2248(%rbp)
	je	.L608
.L609:
	movq	%rax, %r12
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L607:
	leaq	1(%r12), %rax
	cmpq	%r12, -2248(%rbp)
	jne	.L609
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L582:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L585:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L593:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %r12
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L591:
	movq	8(%rbx), %r14
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2232(%rbp), %rdx
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L728:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L710:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L712:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L588:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L715:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L723:
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L725:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L615
	movq	352(%r15), %rsi
	movq	-2240(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2192(%rbp), %rax
	movq	-1088(%rbp), %rdi
	leaq	(%rax,%r12,8), %rdx
	movq	%rdx, -2232(%rbp)
	call	strdup@PLT
	movq	-2232(%rbp), %rdx
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L729
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L618
	testq	%rdi, %rdi
	je	.L618
	call	free@PLT
	leaq	1(%r12), %rax
	cmpq	%r12, -2248(%rbp)
	je	.L619
.L620:
	movq	%rax, %r12
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	1(%r12), %rax
	cmpq	%r12, -2248(%rbp)
	jne	.L620
.L619:
	movq	-2192(%rbp), %rax
.L613:
	movl	-2264(%rbp), %edx
	cmpl	$2, 16(%rbx)
	movq	$0, (%rax,%rdx,8)
	jg	.L622
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L623:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	testb	$1, %al
	jne	.L730
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	$1, %edi
	movl	$16, %esi
	shrl	%eax
	movl	%eax, %r14d
	movl	%eax, -2220(%rbp)
	testq	%r14, %r14
	cmovne	%r14, %rdi
	call	calloc@PLT
	testq	%r14, %r14
	je	.L651
	testq	%rax, %rax
	je	.L731
.L651:
	movq	%rax, -2216(%rbp)
	leaq	-2160(%rbp), %rax
	xorl	%r14d, %r14d
	movq	$0, -2240(%rbp)
	movq	%rax, -2248(%rbp)
.L635:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r14d, %eax
	jbe	.L624
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L732
.L625:
	leal	1(%r14), %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r8, -2232(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	-2232(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L733
.L626:
	movq	(%r8), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L734
.L627:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-2232(%rbp), %rax
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L718:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2232(%rbp), %rdi
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2232(%rbp), %rdi
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L721:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L719:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-2232(%rbp), %rax
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L720:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2232(%rbp), %rdi
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L724:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2232(%rbp), %rdx
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L729:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L734:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L627
	movq	(%r9), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L735
.L629:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L735:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L629
	movq	352(%r15), %rsi
	movq	-2248(%rbp), %rdi
	movq	%r8, %rdx
	movq	%r9, -2232(%rbp)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2232(%rbp), %r9
	movq	352(%r15), %rsi
	leaq	-1104(%rbp), %rdi
	movq	%r9, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2144(%rbp), %rdi
	movq	-2240(%rbp), %rax
	addq	-2216(%rbp), %rax
	movq	%rax, -2232(%rbp)
	call	strdup@PLT
	movq	-2232(%rbp), %rsi
	movq	%rax, (%rsi)
	testq	%rax, %rax
	je	.L736
	movq	-1088(%rbp), %rdi
	call	strdup@PLT
	movq	-2232(%rbp), %rsi
	movq	%rax, 8(%rsi)
	testq	%rax, %rax
	je	.L737
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L633
	testq	%rdi, %rdi
	je	.L633
	call	free@PLT
.L633:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L634
	testq	%rdi, %rdi
	je	.L634
	call	free@PLT
.L634:
	addq	$16, -2240(%rbp)
	addl	$2, %r14d
	jmp	.L635
.L622:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %r12
	jmp	.L623
.L733:
	movq	%r8, -2264(%rbp)
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2264(%rbp), %r8
	movq	-2232(%rbp), %r9
	jmp	.L626
.L732:
	movq	%rax, -2232(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2232(%rbp), %r8
	jmp	.L625
.L736:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__13_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L737:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__14_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L730:
	leaq	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L731:
	leaq	_ZZN4node6CallocI16uvwasi_preopen_sEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L624:
	movq	8(%rbx), %r12
	movl	$168, %edi
	call	_Znwm@PLT
	leaq	-2224(%rbp), %rcx
	movq	%r15, %rsi
	addq	$8, %r12
	movq	%rax, %rdi
	movq	%r12, %rdx
	call	_ZN4node4wasi4WASIC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP16uvwasi_options_s
	movq	-2200(%rbp), %rax
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L637
	movl	-2252(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L638
	leal	-1(%rbx), %edx
	xorl	%ebx, %ebx
	leaq	8(,%rdx,8), %r12
	jmp	.L639
.L738:
	movq	-2200(%rbp), %rax
.L639:
	movq	(%rax,%rbx), %rdi
	addq	$8, %rbx
	call	free@PLT
	cmpq	%rbx, %r12
	jne	.L738
	movq	-2200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L637
.L638:
	call	_ZdaPv@PLT
.L637:
	movq	-2192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L642
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L643
	xorl	%ebx, %ebx
.L644:
	movq	%r8, %rdi
	call	free@PLT
	movq	-2192(%rbp), %rdi
	leal	1(%rbx), %eax
	movq	%rax, %rbx
	movq	(%rdi,%rax,8), %r8
	testq	%r8, %r8
	jne	.L644
	testq	%rdi, %rdi
	je	.L642
.L643:
	call	_ZdaPv@PLT
.L642:
	cmpq	$0, -2216(%rbp)
	je	.L576
	xorl	%ebx, %ebx
	cmpl	$0, -2220(%rbp)
	je	.L648
.L647:
	movq	-2216(%rbp), %rax
	movl	%ebx, %r12d
	addl	$1, %ebx
	salq	$4, %r12
	movq	(%rax,%r12), %rdi
	call	free@PLT
	movq	-2216(%rbp), %rax
	movq	8(%rax,%r12), %rdi
	call	free@PLT
	cmpl	%ebx, -2220(%rbp)
	ja	.L647
.L648:
	movq	-2216(%rbp), %rdi
	call	free@PLT
.L576:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L739
	addq	$2232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L739:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7761:
	.size	_ZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node4wasi4WASI18CheckAllocatedSizeEm
	.type	_ZNK4node4wasi4WASI18CheckAllocatedSizeEm, @function
_ZNK4node4wasi4WASI18CheckAllocatedSizeEm:
.LFB7758:
	.cfi_startproc
	endbr64
	cmpq	%rsi, 160(%rdi)
	jb	.L745
	ret
	.p2align 4,,10
	.p2align 3
.L745:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node4wasi4WASI18CheckAllocatedSizeEmE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7758:
	.size	_ZNK4node4wasi4WASI18CheckAllocatedSizeEm, .-_ZNK4node4wasi4WASI18CheckAllocatedSizeEm
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI21IncreaseAllocatedSizeEm
	.type	_ZN4node4wasi4WASI21IncreaseAllocatedSizeEm, @function
_ZN4node4wasi4WASI21IncreaseAllocatedSizeEm:
.LFB7759:
	.cfi_startproc
	endbr64
	addq	%rsi, 160(%rdi)
	ret
	.cfi_endproc
.LFE7759:
	.size	_ZN4node4wasi4WASI21IncreaseAllocatedSizeEm, .-_ZN4node4wasi4WASI21IncreaseAllocatedSizeEm
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI21DecreaseAllocatedSizeEm
	.type	_ZN4node4wasi4WASI21DecreaseAllocatedSizeEm, @function
_ZN4node4wasi4WASI21DecreaseAllocatedSizeEm:
.LFB7760:
	.cfi_startproc
	endbr64
	subq	%rsi, 160(%rdi)
	ret
	.cfi_endproc
.LFE7760:
	.size	_ZN4node4wasi4WASI21DecreaseAllocatedSizeEm, .-_ZN4node4wasi4WASI21DecreaseAllocatedSizeEm
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI12backingStoreEPPcPm
	.type	_ZN4node4wasi4WASI12backingStoreEPPcPm, @function
_ZN4node4wasi4WASI12backingStoreEPPcPm:
.LFB7811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	112(%rdi), %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	232(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L757
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	je	.L757
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-96(%rbp), %rdx
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	movq	%rdx, (%rbx)
	testq	%rdx, %rdx
	je	.L758
.L748:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L759
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L757:
	.cfi_restore_state
	movl	$28, %eax
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L758:
	leaq	_ZZN4node4wasi4WASI12backingStoreEPPcPmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L759:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7811:
	.size	_ZN4node4wasi4WASI12backingStoreEPPcPm, .-_ZN4node4wasi4WASI12backingStoreEPPcPm
	.p2align 4
	.globl	_Z14_register_wasiv
	.type	_Z14_register_wasiv, @function
_Z14_register_wasiv:
.LFB7813:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7813:
	.size	_Z14_register_wasiv, .-_Z14_register_wasiv
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB7887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L762
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L762:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L764
.L778:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L778
.L764:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L779
.L765:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r14
	movl	$32, -240(%rbp)
	movq	%rax, -232(%rbp)
	andq	$-16, %r14
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$48, -236(%rbp)
	call	*%r10
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L780
	cmpq	$1, %r13
	jne	.L768
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L769:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L781
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L768:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L769
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L780:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L767:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L779:
	orq	$0, -8(%rsp,%rcx)
	jmp	.L765
.L781:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7887:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L789
	movq	16(%rdi), %r10
.L783:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L784
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L790
	movq	16(%r9), %r10
.L785:
	cmpq	%r10, %rax
	jbe	.L792
.L784:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L786:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L793
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L788:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L792:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L793:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L788
	.p2align 4,,10
	.p2align 3
.L789:
	movl	$15, %r10d
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L790:
	movl	$15, %r10d
	jmp	.L785
	.cfi_endproc
.LFE8322:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB9904:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L795
	testq	%r14, %r14
	je	.L811
.L795:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L812
	cmpq	$1, %r13
	jne	.L798
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L799:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L813
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L799
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L812:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L797:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L799
.L811:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L813:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9904:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB9910:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L818
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L816:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L816
	.cfi_endproc
.LFE9910:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC58:
	.string	"(null)"
.LC59:
	.string	"lz"
.LC60:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB9496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L820
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L820:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC59(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r9
.L821:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L821
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L822
	cmpb	$99, %dl
	jg	.L823
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L824
	cmpb	$88, %dl
	je	.L825
	jmp	.L822
.L823:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L822
	leaq	.L827(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L827:
	.long	.L826-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L826-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L826-.L827
	.long	.L829-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L826-.L827
	.long	.L822-.L827
	.long	.L826-.L827
	.long	.L822-.L827
	.long	.L822-.L827
	.long	.L826-.L827
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L824:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L861
	jmp	.L858
.L822:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L858
.L861:
	call	_ZdlPv@PLT
	jmp	.L858
.L826:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L841
	leaq	.LC58(%rip), %rsi
.L841:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L853
	jmp	.L838
.L825:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L843
	leaq	.LC58(%rip), %rsi
.L843:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L844
	call	_ZdlPv@PLT
.L844:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L838
.L853:
	call	_ZdlPv@PLT
	jmp	.L838
.L829:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC60(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L846
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L846:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L838:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L858:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L833
	call	_ZdlPv@PLT
.L833:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L819
	call	_ZdlPv@PLT
.L819:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L849
	call	__stack_chk_fail@PLT
.L849:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9496:
	.size	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB9061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L863
	call	__stack_chk_fail@PLT
.L863:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9061:
	.size	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_:
.LFB8473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L865
	call	_ZdlPv@PLT
.L865:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L867
	call	__stack_chk_fail@PLT
.L867:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8473:
	.size	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.section	.rodata.str1.1
.LC61:
	.string	"sched_yield()\n"
.LC62:
	.string	"%s"
	.section	.text.unlikely
	.align 2
.LCOLDB63:
	.text
.LHOTB63:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L870
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L869:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L894
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L870:
	.cfi_restore_state
	movq	8(%rdi), %r12
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L895
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L887
	cmpw	$1040, %cx
	jne	.L873
.L887:
	movq	23(%rdx), %rax
.L875:
	testq	%rax, %rax
	je	.L869
	cmpq	$0, 112(%rax)
	je	.L896
	movq	16(%rax), %rdx
	leaq	.LC61(%rip), %rcx
	movq	%rcx, -64(%rbp)
	cmpb	$0, 2259(%rdx)
	jne	.L892
.L885:
	leaq	40(%rax), %rdi
	call	uvwasi_sched_yield@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L873:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L895:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L896:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L888
	cmpw	$1040, %cx
	jne	.L877
.L888:
	movq	23(%rdx), %rax
.L879:
	movq	352(%rax), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L897
.L880:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC54(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L898
.L881:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L899
.L882:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L900
.L883:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L901
.L884:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L869
.L877:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L879
.L901:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L884
.L900:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L883
.L899:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L882
.L898:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L881
.L897:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L880
.L894:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7806:
.L892:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-64(%rbp), %rdx
	leaq	.LC62(%rip), %rsi
	movq	%rax, -72(%rbp)
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	movq	-72(%rbp), %rax
	jmp	.L885
	.cfi_endproc
.LFE7806:
	.text
	.size	_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI10SchedYieldERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE63:
	.text
.LHOTE63:
	.section	.rodata._ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC64:
	.string	"%u"
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L903
	leaq	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L903:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L904:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L904
	cmpb	$120, %dl
	jg	.L905
	cmpb	$99, %dl
	jg	.L906
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L907
	cmpb	$88, %dl
	je	.L908
	jmp	.L905
.L906:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L905
	leaq	.L910(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L910:
	.long	.L911-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L911-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L913-.L910
	.long	.L912-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L911-.L910
	.long	.L905-.L910
	.long	.L911-.L910
	.long	.L905-.L910
	.long	.L905-.L910
	.long	.L909-.L910
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L907:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L914
	call	_ZdlPv@PLT
.L914:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L934
	jmp	.L916
.L905:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L939
	call	_ZdlPv@PLT
	jmp	.L939
.L911:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L936
.L913:
	movb	$0, -57(%rbp)
	movl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L921:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L921
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L936
.L909:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L936:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L933
	jmp	.L920
.L908:
	movl	(%r8), %esi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L924
	call	_ZdlPv@PLT
.L924:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L920
.L933:
	call	_ZdlPv@PLT
	jmp	.L920
.L912:
	leaq	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L920:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L939:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L916
.L934:
	call	_ZdlPv@PLT
.L916:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L902
	call	_ZdlPv@PLT
.L902:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L928
	call	__stack_chk_fail@PLT
.L928:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10395:
	.size	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L941
	call	__stack_chk_fail@PLT
.L941:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10259:
	.size	_ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_:
.LFB9972:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L943
	call	_ZdlPv@PLT
.L943:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L945
	call	__stack_chk_fail@PLT
.L945:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9972:
	.size	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC65:
	.string	"fd_close(%d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB66:
	.text
.LHOTB66:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	je	.L948
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L947:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L976
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L948:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L977
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L978
	movq	8(%rbx), %rdi
.L952:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L979
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L969
	cmpw	$1040, %cx
	jne	.L954
.L969:
	movq	23(%rdx), %rax
.L956:
	testq	%rax, %rax
	je	.L947
	cmpq	$0, 112(%rax)
	je	.L980
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L974
.L966:
	movl	-60(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_fd_close@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L978:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L977:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L979:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L954:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L980:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L970
	cmpw	$1040, %cx
	jne	.L958
.L970:
	movq	23(%rdx), %rax
.L960:
	movq	352(%rax), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L981
.L961:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC54(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L982
.L962:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L983
.L963:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L984
.L964:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L985
.L965:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L947
.L958:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L960
.L985:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L965
.L984:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L964
.L983:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L963
.L982:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L962
.L981:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L961
.L976:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7773:
.L974:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-60(%rbp), %rdx
	leaq	.LC65(%rip), %rsi
	movq	%rax, -72(%rbp)
	call	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_
	movq	-72(%rbp), %rax
	jmp	.L966
	.cfi_endproc
.LFE7773:
	.text
	.size	_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI7FdCloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE66:
	.text
.LHOTE66:
	.section	.rodata.str1.1
.LC67:
	.string	"fd_datasync(%d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB68:
	.text
.LHOTB68:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	je	.L987
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L986:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1015
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L987:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1016
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1017
	movq	8(%rbx), %rdi
.L991:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1018
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1008
	cmpw	$1040, %cx
	jne	.L993
.L1008:
	movq	23(%rdx), %rax
.L995:
	testq	%rax, %rax
	je	.L986
	cmpq	$0, 112(%rax)
	je	.L1019
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L1013
.L1005:
	movl	-60(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_fd_datasync@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1016:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1018:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L993:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1009
	cmpw	$1040, %cx
	jne	.L997
.L1009:
	movq	23(%rdx), %rax
.L999:
	movq	352(%rax), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1020
.L1000:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC54(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1021
.L1001:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1022
.L1002:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1023
.L1003:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1024
.L1004:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L986
.L997:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L999
.L1024:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1004
.L1023:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1003
.L1022:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1002
.L1021:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L1001
.L1020:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1000
.L1015:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7774:
.L1013:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-60(%rbp), %rdx
	leaq	.LC67(%rip), %rsi
	movq	%rax, -72(%rbp)
	call	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_
	movq	-72(%rbp), %rax
	jmp	.L1005
	.cfi_endproc
.LFE7774:
	.text
	.size	_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI10FdDatasyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE68:
	.text
.LHOTE68:
	.section	.rodata.str1.1
.LC69:
	.string	"fd_sync(%d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB70:
	.text
.LHOTB70:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	je	.L1026
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L1025:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1054
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1055
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1056
	movq	8(%rbx), %rdi
.L1030:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1057
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1047
	cmpw	$1040, %cx
	jne	.L1032
.L1047:
	movq	23(%rdx), %rax
.L1034:
	testq	%rax, %rax
	je	.L1025
	cmpq	$0, 112(%rax)
	je	.L1058
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L1052
.L1044:
	movl	-60(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_fd_sync@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1055:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1057:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1032:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1048
	cmpw	$1040, %cx
	jne	.L1036
.L1048:
	movq	23(%rdx), %rax
.L1038:
	movq	352(%rax), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1059
.L1039:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC54(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1060
.L1040:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1061
.L1041:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1062
.L1042:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1063
.L1043:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L1025
.L1036:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1038
.L1063:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1043
.L1062:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1042
.L1061:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1041
.L1060:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L1040
.L1059:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1039
.L1054:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7789:
.L1052:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-60(%rbp), %rdx
	leaq	.LC69(%rip), %rsi
	movq	%rax, -72(%rbp)
	call	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_
	movq	-72(%rbp), %rax
	jmp	.L1044
	.cfi_endproc
.LFE7789:
	.text
	.size	_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI6FdSyncERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE70:
	.text
.LHOTE70:
	.section	.rodata.str1.1
.LC71:
	.string	"proc_exit(%d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB72:
	.text
.LHOTB72:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	je	.L1065
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L1064:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1093
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1065:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1094
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1095
	movq	8(%rbx), %rdi
.L1069:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1096
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1086
	cmpw	$1040, %cx
	jne	.L1071
.L1086:
	movq	23(%rdx), %rax
.L1073:
	testq	%rax, %rax
	je	.L1064
	cmpq	$0, 112(%rax)
	je	.L1097
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L1091
.L1083:
	movl	-60(%rbp), %esi
	leaq	40(%rax), %rdi
	movq	(%rbx), %rbx
	call	uvwasi_proc_exit@PLT
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1069
	.p2align 4,,10
	.p2align 3
.L1094:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1096:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1071:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1097:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1087
	cmpw	$1040, %cx
	jne	.L1075
.L1087:
	movq	23(%rdx), %rax
.L1077:
	movq	352(%rax), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC53(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1098
.L1078:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC54(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1099
.L1079:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1100
.L1080:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC55(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1101
.L1081:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1102
.L1082:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L1064
.L1075:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1077
.L1102:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1082
.L1101:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1081
.L1100:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1080
.L1099:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	jmp	.L1079
.L1098:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1078
.L1093:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7803:
.L1091:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-60(%rbp), %rdx
	leaq	.LC71(%rip), %rsi
	movq	%rax, -72(%rbp)
	call	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_
	movq	-72(%rbp), %rax
	jmp	.L1083
	.cfi_endproc
.LFE7803:
	.text
	.size	_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI8ProcExitERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE72:
	.text
.LHOTE72:
	.section	.rodata.str1.1
.LC73:
	.string	"proc_raise(%d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB74:
	.text
.LHOTB74:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rdi)
	je	.L1104
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L1103:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1127
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1104:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1128
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1129
	movq	8(%rbx), %rdi
.L1108:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -44(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1130
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1120
	cmpw	$1040, %cx
	jne	.L1110
.L1120:
	movq	23(%rdx), %rax
.L1112:
	testq	%rax, %rax
	je	.L1103
	cmpq	$0, 112(%rax)
	je	.L1131
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L1125
.L1117:
	movzbl	-44(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_proc_raise@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1128:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1130:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1110:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1121
	cmpw	$1040, %cx
	jne	.L1114
.L1121:
	movq	23(%rdx), %rax
.L1116:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1103
.L1114:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1116
.L1127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7804:
.L1125:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-44(%rbp), %rdx
	leaq	.LC73(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN4node7FPrintFIJRjEEEvP8_IO_FILEPKcDpOT_
	movq	-56(%rbp), %rax
	jmp	.L1117
	.cfi_endproc
.LFE7804:
	.text
	.size	_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI9ProcRaiseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE74:
	.text
.LHOTE74:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1133
	leaq	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1133:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1134:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1134
	cmpb	$120, %dl
	jg	.L1135
	cmpb	$99, %dl
	jg	.L1136
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L1137
	cmpb	$88, %dl
	je	.L1138
	jmp	.L1135
.L1136:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1135
	leaq	.L1140(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1140:
	.long	.L1141-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1141-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1143-.L1140
	.long	.L1142-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1141-.L1140
	.long	.L1135-.L1140
	.long	.L1141-.L1140
	.long	.L1135-.L1140
	.long	.L1135-.L1140
	.long	.L1139-.L1140
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1137:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1144
	call	_ZdlPv@PLT
.L1144:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1164
	jmp	.L1146
.L1135:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1169
	call	_ZdlPv@PLT
	jmp	.L1169
.L1141:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L1166
.L1143:
	movb	$0, -57(%rbp)
	movl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L1151:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1151
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1166
.L1139:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1166:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1163
	jmp	.L1150
.L1138:
	movl	(%r8), %esi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1154
	call	_ZdlPv@PLT
.L1154:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1150
.L1163:
	call	_ZdlPv@PLT
	jmp	.L1150
.L1142:
	leaq	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1150:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1169:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1146
.L1164:
	call	_ZdlPv@PLT
.L1146:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1132
	call	_ZdlPv@PLT
.L1132:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1158
	call	__stack_chk_fail@PLT
.L1158:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10389:
	.size	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10251:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1171
	call	__stack_chk_fail@PLT
.L1171:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10251:
	.size	_ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_:
.LFB9957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1173
	call	_ZdlPv@PLT
.L1173:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1175
	call	__stack_chk_fail@PLT
.L1175:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9957:
	.size	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC75:
	.string	"fd_renumber(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB76:
	.text
.LHOTB76:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1178
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L1177:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1207
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1178:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1206
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1208
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -48(%rbp)
	jg	.L1183
.L1211:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1184:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1206
	cmpl	$1, 16(%rbx)
	jg	.L1186
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1187:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -44(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1209
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1199
	cmpw	$1040, %cx
	jne	.L1189
.L1199:
	movq	23(%rdx), %rax
.L1191:
	testq	%rax, %rax
	je	.L1177
	cmpq	$0, 112(%rax)
	je	.L1210
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L1204
.L1196:
	movl	-44(%rbp), %edx
	movl	-48(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_fd_renumber@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1208:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -48(%rbp)
	jle	.L1211
.L1183:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1206:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1209:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1189:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1210:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1200
	cmpw	$1040, %cx
	jne	.L1193
.L1200:
	movq	23(%rdx), %rax
.L1195:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1177
.L1193:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1195
.L1207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7787:
.L1204:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-44(%rbp), %rcx
	leaq	-48(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	.LC75(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	movq	-56(%rbp), %rax
	jmp	.L1196
	.cfi_endproc
.LFE7787:
	.text
	.size	_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI10FdRenumberERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE76:
	.text
.LHOTE76:
	.section	.rodata.str1.1
.LC77:
	.string	"args_sizes_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB78:
	.text
.LHOTB78:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1213
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1212:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1247
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1213:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1246
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1248
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L1218
.L1251:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1219:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1246
	cmpl	$1, 16(%rbx)
	jg	.L1221
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1222:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -68(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1249
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1238
	cmpw	$1040, %cx
	jne	.L1224
.L1238:
	movq	23(%rdx), %r12
.L1226:
	testq	%r12, %r12
	je	.L1212
	cmpq	$0, 112(%r12)
	je	.L1250
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1243
.L1231:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1232
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1248:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jle	.L1251
.L1218:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1246:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1221:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1232:
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1245
	movl	-68(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1245
	leaq	40(%r12), %rdi
	leaq	-60(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	call	uvwasi_args_sizes_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1252
.L1235:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1245:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1249:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1224:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1239
	cmpw	$1040, %cx
	jne	.L1228
.L1239:
	movq	23(%rdx), %rax
.L1230:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1212
.L1252:
	movl	-72(%rbp), %esi
	movl	-64(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	movl	-68(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L1235
.L1228:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1230
.L1247:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7766:
.L1243:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-68(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	.LC77(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1231
	.cfi_endproc
.LFE7766:
	.text
	.size	_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI12ArgsSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE78:
	.text
.LHOTE78:
	.section	.rodata.str1.1
.LC79:
	.string	"clock_res_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB80:
	.text
.LHOTB80:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1254
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1253:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1286
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1254:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1285
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1287
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L1259
.L1290:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1260:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1285
	cmpl	$1, 16(%rbx)
	jg	.L1262
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1263:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -68(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1288
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1278
	cmpw	$1040, %cx
	jne	.L1265
.L1278:
	movq	23(%rdx), %r12
.L1267:
	testq	%r12, %r12
	je	.L1253
	cmpq	$0, 112(%r12)
	je	.L1289
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1283
.L1272:
	leaq	-56(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1273
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jle	.L1290
.L1259:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1285:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1262:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1273:
	movl	-68(%rbp), %edi
	movq	-56(%rbp), %rsi
	movl	$8, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1274
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1274:
	movl	-72(%rbp), %esi
	leaq	40(%r12), %rdi
	leaq	-48(%rbp), %rdx
	call	uvwasi_clock_res_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1291
.L1275:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1288:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1265:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1279
	cmpw	$1040, %cx
	jne	.L1269
.L1279:
	movq	23(%rdx), %rax
.L1271:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1253
.L1291:
	movl	-68(%rbp), %esi
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rdi
	call	uvwasi_serdes_write_timestamp_t@PLT
	jmp	.L1275
.L1269:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1271
.L1286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7767:
.L1283:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-68(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	.LC79(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1272
	.cfi_endproc
.LFE7767:
	.text
	.size	_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI11ClockResGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE80:
	.text
.LHOTE80:
	.section	.rodata.str1.1
.LC81:
	.string	"environ_sizes_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB82:
	.text
.LHOTB82:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1293
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1292:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1327
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1293:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1326
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1328
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L1298
.L1331:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1299:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1326
	cmpl	$1, 16(%rbx)
	jg	.L1301
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1302:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -68(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1329
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1318
	cmpw	$1040, %cx
	jne	.L1304
.L1318:
	movq	23(%rdx), %r12
.L1306:
	testq	%r12, %r12
	je	.L1292
	cmpq	$0, 112(%r12)
	je	.L1330
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1323
.L1311:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1312
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jle	.L1331
.L1298:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1326:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1312:
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1325
	movl	-68(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1325
	leaq	40(%r12), %rdi
	leaq	-60(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	call	uvwasi_environ_sizes_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1332
.L1315:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1325:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1329:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1304:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1330:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1319
	cmpw	$1040, %cx
	jne	.L1308
.L1319:
	movq	23(%rdx), %rax
.L1310:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1292
.L1332:
	movl	-72(%rbp), %esi
	movl	-64(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	movl	-68(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L1315
.L1308:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1310
.L1327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7770:
.L1323:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-68(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	.LC81(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1311
	.cfi_endproc
.LFE7770:
	.text
	.size	_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI15EnvironSizesGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE82:
	.text
.LHOTE82:
	.section	.rodata.str1.1
.LC83:
	.string	"fd_fdstat_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB84:
	.text
.LHOTB84:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1334
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1333:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1366
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1334:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1365
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1367
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -88(%rbp)
	jg	.L1339
.L1370:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1340:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1365
	cmpl	$1, 16(%rbx)
	jg	.L1342
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1343:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -84(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1368
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1358
	cmpw	$1040, %cx
	jne	.L1345
.L1358:
	movq	23(%rdx), %r12
.L1347:
	testq	%r12, %r12
	je	.L1333
	cmpq	$0, 112(%r12)
	je	.L1369
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1363
.L1352:
	leaq	-72(%rbp), %rdx
	leaq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1353
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -88(%rbp)
	jle	.L1370
.L1339:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1365:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1342:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1353:
	movl	-84(%rbp), %edi
	movq	-72(%rbp), %rsi
	movl	$24, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1354
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1354:
	movl	-88(%rbp), %esi
	leaq	-64(%rbp), %r13
	leaq	40(%r12), %rdi
	movq	%r13, %rdx
	call	uvwasi_fd_fdstat_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1371
.L1355:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1368:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1345:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1369:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1359
	cmpw	$1040, %cx
	jne	.L1349
.L1359:
	movq	23(%rdx), %rax
.L1351:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1333
.L1371:
	movl	-84(%rbp), %esi
	movq	-80(%rbp), %rdi
	movq	%r13, %rdx
	call	uvwasi_serdes_write_fdstat_t@PLT
	jmp	.L1355
.L1349:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1351
.L1366:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7775:
.L1363:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-84(%rbp), %rcx
	leaq	-88(%rbp), %rdx
	leaq	.LC83(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1352
	.cfi_endproc
.LFE7775:
	.text
	.size	_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI11FdFdstatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE84:
	.text
.LHOTE84:
	.section	.rodata.str1.1
.LC85:
	.string	"fd_filestat_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB86:
	.text
.LHOTB86:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1373
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1372:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1405
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1404
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1406
	movq	8(%rbx), %rdi
.L1377:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -136(%rbp)
	jg	.L1378
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1379:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1404
	cmpl	$1, 16(%rbx)
	jg	.L1381
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1382:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -132(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1407
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1397
	cmpw	$1040, %cx
	jne	.L1384
.L1397:
	movq	23(%rdx), %r12
.L1386:
	testq	%r12, %r12
	je	.L1372
	cmpq	$0, 112(%r12)
	je	.L1408
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1402
.L1391:
	leaq	-120(%rbp), %rdx
	leaq	-128(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1392
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1377
	.p2align 4,,10
	.p2align 3
.L1404:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1392:
	movl	-132(%rbp), %edi
	movq	-120(%rbp), %rsi
	movl	$64, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1393
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1393:
	movl	-136(%rbp), %esi
	leaq	-112(%rbp), %r13
	leaq	40(%r12), %rdi
	movq	%r13, %rdx
	call	uvwasi_fd_filestat_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1409
.L1394:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1407:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1384:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1386
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1398
	cmpw	$1040, %cx
	jne	.L1388
.L1398:
	movq	23(%rdx), %rax
.L1390:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1372
.L1409:
	movl	-132(%rbp), %esi
	movq	-128(%rbp), %rdi
	movq	%r13, %rdx
	call	uvwasi_serdes_write_filestat_t@PLT
	jmp	.L1394
.L1388:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1390
.L1405:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7778:
.L1402:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-132(%rbp), %rcx
	leaq	-136(%rbp), %rdx
	leaq	.LC85(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1391
	.cfi_endproc
.LFE7778:
	.text
	.size	_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI13FdFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE86:
	.text
.LHOTE86:
	.section	.rodata.str1.1
.LC87:
	.string	"fd_prestat_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB88:
	.text
.LHOTB88:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1411
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1410:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1443
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1411:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1442
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1444
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L1416
.L1447:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1417:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1442
	cmpl	$1, 16(%rbx)
	jg	.L1419
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1420:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -68(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1445
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1435
	cmpw	$1040, %cx
	jne	.L1422
.L1435:
	movq	23(%rdx), %r12
.L1424:
	testq	%r12, %r12
	je	.L1410
	cmpq	$0, 112(%r12)
	je	.L1446
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1440
.L1429:
	leaq	-56(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1430
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jle	.L1447
.L1416:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1417
	.p2align 4,,10
	.p2align 3
.L1442:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1420
	.p2align 4,,10
	.p2align 3
.L1430:
	movl	-68(%rbp), %edi
	movq	-56(%rbp), %rsi
	movl	$8, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1431
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1431:
	movl	-72(%rbp), %esi
	leaq	-48(%rbp), %r13
	leaq	40(%r12), %rdi
	movq	%r13, %rdx
	call	uvwasi_fd_prestat_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1448
.L1432:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1445:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1422:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1436
	cmpw	$1040, %cx
	jne	.L1426
.L1436:
	movq	23(%rdx), %rax
.L1428:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1410
.L1448:
	movl	-68(%rbp), %esi
	movq	-64(%rbp), %rdi
	movq	%r13, %rdx
	call	uvwasi_serdes_write_prestat_t@PLT
	jmp	.L1432
.L1426:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1428
.L1443:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7782:
.L1440:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-68(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	.LC87(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1429
	.cfi_endproc
.LFE7782:
	.text
	.size	_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI12FdPrestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE88:
	.text
.LHOTE88:
	.section	.rodata.str1.1
.LC89:
	.string	"fd_tell(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB90:
	.text
.LHOTB90:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1450
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1449:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1482
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1481
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1483
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L1455
.L1486:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1456:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1481
	cmpl	$1, 16(%rbx)
	jg	.L1458
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1459:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -68(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1484
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1474
	cmpw	$1040, %cx
	jne	.L1461
.L1474:
	movq	23(%rdx), %r12
.L1463:
	testq	%r12, %r12
	je	.L1449
	cmpq	$0, 112(%r12)
	je	.L1485
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1479
.L1468:
	leaq	-56(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1469
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -72(%rbp)
	jle	.L1486
.L1455:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1481:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1469:
	movl	-68(%rbp), %edi
	movq	-56(%rbp), %rsi
	movl	$8, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1470
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1470:
	movl	-72(%rbp), %esi
	leaq	40(%r12), %rdi
	leaq	-48(%rbp), %rdx
	call	uvwasi_fd_tell@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1487
.L1471:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1484:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1461:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1475
	cmpw	$1040, %cx
	jne	.L1465
.L1475:
	movq	23(%rdx), %rax
.L1467:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1449
.L1487:
	movl	-68(%rbp), %esi
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rdi
	call	uvwasi_serdes_write_filesize_t@PLT
	jmp	.L1471
.L1465:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1467
.L1482:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7790:
.L1479:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-68(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	.LC89(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1468
	.cfi_endproc
.LFE7790:
	.text
	.size	_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI6FdTellERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE90:
	.text
.LHOTE90:
	.section	.rodata.str1.1
.LC91:
	.string	"random_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB92:
	.text
.LHOTB92:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1489
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1488:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1521
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1489:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1520
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1522
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L1494
.L1525:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1495:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1520
	cmpl	$1, 16(%rbx)
	jg	.L1497
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1498:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1523
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1512
	cmpw	$1040, %cx
	jne	.L1500
.L1512:
	movq	23(%rdx), %r12
.L1502:
	testq	%r12, %r12
	je	.L1488
	cmpq	$0, 112(%r12)
	je	.L1524
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1517
.L1507:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1508
.L1519:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1522:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -64(%rbp)
	jle	.L1525
.L1494:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L1520:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1508:
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1509
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1509:
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %esi
	leaq	40(%r12), %rdi
	addq	-56(%rbp), %rsi
	call	uvwasi_random_get@PLT
	jmp	.L1519
	.p2align 4,,10
	.p2align 3
.L1523:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1500:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1513
	cmpw	$1040, %cx
	jne	.L1504
.L1513:
	movq	23(%rdx), %rax
.L1506:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1488
.L1504:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1506
.L1521:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7805:
.L1517:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	leaq	.LC91(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1507
	.cfi_endproc
.LFE7805:
	.text
	.size	_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI9RandomGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE92:
	.text
.LHOTE92:
	.section	.rodata.str1.1
.LC93:
	.string	"args_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB94:
	.text
.LHOTB94:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1527
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1526:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1573
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1571
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L1574
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L1532
.L1577:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1533:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1571
	cmpl	$1, 16(%rbx)
	jg	.L1535
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1536:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1575
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1562
	cmpw	$1040, %cx
	jne	.L1538
.L1562:
	movq	23(%rdx), %r12
.L1540:
	testq	%r12, %r12
	je	.L1526
	cmpq	$0, 112(%r12)
	je	.L1576
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1567
.L1546:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1547
.L1569:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -64(%rbp)
	jle	.L1577
.L1532:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1571:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1547:
	movl	72(%r12), %edx
	movl	-60(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1572
	movl	48(%r12), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	salq	$2, %rdx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1572
	movl	48(%r12), %eax
	testq	%rax, %rax
	je	.L1550
	leaq	0(,%rax,8), %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%rax, %r14
	movq	%rax, %rdi
	call	memset@PLT
	movl	-60(%rbp), %edx
	leaq	40(%r12), %rdi
	movq	%r14, %rsi
	addq	-56(%rbp), %rdx
	call	uvwasi_args_get@PLT
	testw	%ax, %ax
	jne	.L1578
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L1560
.L1559:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1554:
	movl	-64(%rbp), %eax
	movq	-56(%rbp), %rdi
	movq	(%r14,%r13,8), %rdx
	subq	(%r14), %rdx
	leaq	(%rax,%r13,4), %rsi
	addl	-60(%rbp), %edx
	addq	$1, %r13
	call	uvwasi_serdes_write_uint32_t@PLT
	movl	48(%r12), %eax
	cmpq	%r13, %rax
	ja	.L1554
.L1560:
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
.L1555:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1572:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1526
	.p2align 4,,10
	.p2align 3
.L1575:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1538:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1540
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1563
	cmpw	$1040, %cx
	jne	.L1543
.L1563:
	movq	23(%rdx), %rax
.L1545:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1526
.L1550:
	movl	-60(%rbp), %edx
	xorl	%esi, %esi
	addq	-56(%rbp), %rdx
	leaq	40(%r12), %rdi
	call	uvwasi_args_get@PLT
	testw	%ax, %ax
	jne	.L1569
	cmpl	$0, 48(%r12)
	je	.L1558
	xorl	%r14d, %r14d
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1543:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1545
.L1578:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1555
.L1573:
	call	__stack_chk_fail@PLT
.L1558:
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1526
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7765:
.L1567:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	stderr(%rip), %rdi
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	leaq	.LC93(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1546
	.cfi_endproc
.LFE7765:
	.text
	.size	_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI7ArgsGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE94:
	.text
.LHOTE94:
	.section	.rodata.str1.1
.LC95:
	.string	"environ_get(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB96:
	.text
.LHOTB96:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L1580
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1579:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1626
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1580:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1624
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L1627
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L1585
.L1630:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1586:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1624
	cmpl	$1, 16(%rbx)
	jg	.L1588
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1589:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1628
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1615
	cmpw	$1040, %cx
	jne	.L1591
.L1615:
	movq	23(%rdx), %r12
.L1593:
	testq	%r12, %r12
	je	.L1579
	cmpq	$0, 112(%r12)
	je	.L1629
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1620
.L1599:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1600
.L1622:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -64(%rbp)
	jle	.L1630
.L1585:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1624:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1600:
	movl	96(%r12), %edx
	movl	-60(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1625
	movl	76(%r12), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	salq	$2, %rdx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L1625
	movl	76(%r12), %eax
	testq	%rax, %rax
	je	.L1603
	leaq	0(,%rax,8), %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%r13, %rdx
	movq	%rax, %r14
	movq	%rax, %rdi
	call	memset@PLT
	movl	-60(%rbp), %edx
	leaq	40(%r12), %rdi
	movq	%r14, %rsi
	addq	-56(%rbp), %rdx
	call	uvwasi_environ_get@PLT
	testw	%ax, %ax
	jne	.L1631
	movl	76(%r12), %eax
	testl	%eax, %eax
	je	.L1613
.L1612:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1607:
	movl	-64(%rbp), %eax
	movq	-56(%rbp), %rdi
	movq	(%r14,%r13,8), %rdx
	subq	(%r14), %rdx
	leaq	(%rax,%r13,4), %rsi
	addl	-60(%rbp), %edx
	addq	$1, %r13
	call	uvwasi_serdes_write_uint32_t@PLT
	movl	76(%r12), %eax
	cmpq	%r13, %rax
	ja	.L1607
.L1613:
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
.L1608:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1625:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1579
	.p2align 4,,10
	.p2align 3
.L1628:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1591:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1616
	cmpw	$1040, %cx
	jne	.L1596
.L1616:
	movq	23(%rdx), %rax
.L1598:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1579
.L1603:
	movl	-60(%rbp), %edx
	xorl	%esi, %esi
	addq	-56(%rbp), %rdx
	leaq	40(%r12), %rdi
	call	uvwasi_environ_get@PLT
	testw	%ax, %ax
	jne	.L1622
	cmpl	$0, 76(%r12)
	je	.L1611
	xorl	%r14d, %r14d
	jmp	.L1612
	.p2align 4,,10
	.p2align 3
.L1596:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1598
.L1631:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1608
.L1626:
	call	__stack_chk_fail@PLT
.L1611:
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	jmp	.L1579
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7769:
.L1620:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	stderr(%rip), %rdi
	leaq	-60(%rbp), %rcx
	leaq	-64(%rbp), %rdx
	leaq	.LC95(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1599
	.cfi_endproc
.LFE7769:
	.text
	.size	_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI10EnvironGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE96:
	.text
.LHOTE96:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L1633
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1633:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L1634:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L1634
	cmpb	$120, %dl
	jg	.L1635
	cmpb	$99, %dl
	jg	.L1636
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L1637
	cmpb	$88, %dl
	je	.L1638
	jmp	.L1635
.L1636:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1635
	leaq	.L1640(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1640:
	.long	.L1641-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1641-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1643-.L1640
	.long	.L1642-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1641-.L1640
	.long	.L1635-.L1640
	.long	.L1641-.L1640
	.long	.L1635-.L1640
	.long	.L1635-.L1640
	.long	.L1639-.L1640
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1637:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1644
	call	_ZdlPv@PLT
.L1644:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1664
	jmp	.L1646
.L1635:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1669
	call	_ZdlPv@PLT
	jmp	.L1669
.L1641:
	movl	(%r9), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L1666
.L1643:
	movb	$0, -57(%rbp)
	movl	(%r9), %eax
	leaq	-57(%rbp), %rsi
.L1651:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1651
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1666
.L1639:
	movl	(%r9), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1666:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1663
	jmp	.L1650
.L1638:
	movl	(%r9), %esi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1654
	call	_ZdlPv@PLT
.L1654:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1650
.L1663:
	call	_ZdlPv@PLT
	jmp	.L1650
.L1642:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1650:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1669:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1646
.L1664:
	call	_ZdlPv@PLT
.L1646:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1632
	call	_ZdlPv@PLT
.L1632:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1658
	call	__stack_chk_fail@PLT
.L1658:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10402:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1671
	call	__stack_chk_fail@PLT
.L1671:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10268:
	.size	_ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_:
.LFB9988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1673
	call	_ZdlPv@PLT
.L1673:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1675
	call	__stack_chk_fail@PLT
.L1675:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9988:
	.size	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"fd_prestat_dir_name(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB98:
	.text
.LHOTB98:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7783:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 16(%rdi)
	je	.L1678
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1677:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1716
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1678:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1715
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1717
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L1683
.L1721:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1684:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1715
	cmpl	$1, 16(%rbx)
	jg	.L1686
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1687:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L1688
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1689:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1715
	cmpl	$2, 16(%rbx)
	jle	.L1718
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L1692:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1719
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1706
	cmpw	$1040, %cx
	jne	.L1694
.L1706:
	movq	23(%rdx), %r12
.L1696:
	testq	%r12, %r12
	je	.L1677
	cmpq	$0, 112(%r12)
	je	.L1720
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1711
.L1701:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L1713
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1703
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1715:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1717:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jle	.L1721
.L1683:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1688:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1689
	.p2align 4,,10
	.p2align 3
.L1718:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1692
	.p2align 4,,10
	.p2align 3
.L1703:
	movl	-60(%rbp), %ecx
	movl	-68(%rbp), %esi
	leaq	40(%r12), %rdi
	movl	-64(%rbp), %edx
	addq	-56(%rbp), %rdx
	call	uvwasi_fd_prestat_dir_name@PLT
.L1713:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1677
.L1719:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1694:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1696
.L1720:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1707
	cmpw	$1040, %si
	jne	.L1698
.L1707:
	movq	23(%rdx), %rax
.L1700:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1677
.L1698:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1700
.L1716:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7783:
.L1711:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-64(%rbp), %rcx
	leaq	-68(%rbp), %rdx
	leaq	-60(%rbp), %r8
	leaq	.LC97(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1701
	.cfi_endproc
.LFE7783:
	.text
	.size	_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI16FdPrestatDirNameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE98:
	.text
.LHOTE98:
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"path_create_directory(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB100:
	.text
.LHOTB100:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 16(%rdi)
	je	.L1723
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1722:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1761
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1723:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1760
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1762
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L1728
.L1766:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1729:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1760
	cmpl	$1, 16(%rbx)
	jg	.L1731
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1732:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L1733
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1734:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1760
	cmpl	$2, 16(%rbx)
	jle	.L1763
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L1737:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1764
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1751
	cmpw	$1040, %cx
	jne	.L1739
.L1751:
	movq	23(%rdx), %r12
.L1741:
	testq	%r12, %r12
	je	.L1722
	cmpq	$0, 112(%r12)
	je	.L1765
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1756
.L1746:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L1758
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1748
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1760:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1762:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jle	.L1766
.L1728:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1729
	.p2align 4,,10
	.p2align 3
.L1731:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1734
	.p2align 4,,10
	.p2align 3
.L1763:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1748:
	movl	-60(%rbp), %ecx
	movl	-68(%rbp), %esi
	leaq	40(%r12), %rdi
	movl	-64(%rbp), %edx
	addq	-56(%rbp), %rdx
	call	uvwasi_path_create_directory@PLT
.L1758:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1722
.L1764:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1739:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1741
.L1765:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1752
	cmpw	$1040, %si
	jne	.L1743
.L1752:
	movq	23(%rdx), %rax
.L1745:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1722
.L1743:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1745
.L1761:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7792:
.L1756:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-64(%rbp), %rcx
	leaq	-68(%rbp), %rdx
	leaq	-60(%rbp), %r8
	leaq	.LC99(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1746
	.cfi_endproc
.LFE7792:
	.text
	.size	_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI19PathCreateDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE100:
	.text
.LHOTE100:
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"path_filestat_get(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB102:
	.text
.LHOTB102:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 16(%rdi)
	je	.L1768
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L1767:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1814
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1768:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1813
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1815
	movq	8(%rbx), %rdi
.L1772:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -156(%rbp)
	jg	.L1773
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1774:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1812
	cmpl	$1, 16(%rbx)
	jg	.L1776
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1777:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, %r12d
	jg	.L1778
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1779:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1813
	cmpl	$2, 16(%rbx)
	jle	.L1816
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L1782:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -152(%rbp)
	jg	.L1783
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1784:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1812
	cmpl	$3, 16(%rbx)
	jg	.L1786
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1787:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movl	%eax, -148(%rbp)
	jg	.L1788
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1789:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1813
	cmpl	$4, 16(%rbx)
	jg	.L1791
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1792:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r14
	movl	%eax, %r13d
	leaq	8(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1817
	movq	8(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1805
	cmpw	$1040, %cx
	jne	.L1794
.L1805:
	movq	23(%rdx), %r14
.L1796:
	testq	%r14, %r14
	je	.L1767
	cmpq	$0, 112(%r14)
	je	.L1818
	movq	16(%r14), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1809
.L1798:
	leaq	-136(%rbp), %rdx
	leaq	-144(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1799
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1815:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L1813:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1773:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L1812:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1778:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1776:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L1816:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1783:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1788:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1786:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1791:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1799:
	movl	-148(%rbp), %edx
	movl	-152(%rbp), %edi
	movq	-136(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1800
	movabsq	$261993005056, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L1767
.L1800:
	movq	-136(%rbp), %rsi
	movl	$64, %edx
	movq	%r13, %rdi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1801
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1767
.L1817:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1794:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1796
.L1801:
	movl	-148(%rbp), %r8d
	movl	-156(%rbp), %esi
	leaq	-128(%rbp), %r15
	movl	%r12d, %edx
	movl	-152(%rbp), %ecx
	leaq	40(%r14), %rdi
	movq	%r15, %r9
	addq	-144(%rbp), %rcx
	call	uvwasi_path_filestat_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1819
.L1802:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L1767
.L1818:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1767
.L1819:
	movq	-144(%rbp), %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	uvwasi_serdes_write_filestat_t@PLT
	jmp	.L1802
.L1814:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7793:
.L1809:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-152(%rbp), %rcx
	leaq	-156(%rbp), %rdx
	leaq	-148(%rbp), %r8
	leaq	.LC101(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1798
	.cfi_endproc
.LFE7793:
	.text
	.size	_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI15PathFilestatGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE102:
	.text
.LHOTE102:
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"path_remove_directory(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB104:
	.text
.LHOTB104:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 16(%rdi)
	je	.L1821
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1820:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1859
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1821:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1858
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1860
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L1826
.L1864:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1827:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1858
	cmpl	$1, 16(%rbx)
	jg	.L1829
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1830:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L1831
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1832:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1858
	cmpl	$2, 16(%rbx)
	jle	.L1861
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L1835:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1862
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1849
	cmpw	$1040, %cx
	jne	.L1837
.L1849:
	movq	23(%rdx), %r12
.L1839:
	testq	%r12, %r12
	je	.L1820
	cmpq	$0, 112(%r12)
	je	.L1863
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1854
.L1844:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L1856
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1846
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1858:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jle	.L1864
.L1826:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1827
	.p2align 4,,10
	.p2align 3
.L1829:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1830
	.p2align 4,,10
	.p2align 3
.L1831:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1832
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1835
	.p2align 4,,10
	.p2align 3
.L1846:
	movl	-60(%rbp), %ecx
	movl	-68(%rbp), %esi
	leaq	40(%r12), %rdi
	movl	-64(%rbp), %edx
	addq	-56(%rbp), %rdx
	call	uvwasi_path_remove_directory@PLT
.L1856:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1820
.L1862:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1837:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1839
.L1863:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1850
	cmpw	$1040, %si
	jne	.L1841
.L1850:
	movq	23(%rdx), %rax
.L1843:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1820
.L1841:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1843
.L1859:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7798:
.L1854:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-64(%rbp), %rcx
	leaq	-68(%rbp), %rdx
	leaq	-60(%rbp), %r8
	leaq	.LC103(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1844
	.cfi_endproc
.LFE7798:
	.text
	.size	_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI19PathRemoveDirectoryERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE104:
	.text
.LHOTE104:
	.section	.rodata.str1.1
.LC105:
	.string	"path_unlink_file(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB106:
	.text
.LHOTB106:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 16(%rdi)
	je	.L1866
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1865:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1904
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1866:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1903
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1905
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L1871
.L1909:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1872:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1903
	cmpl	$1, 16(%rbx)
	jg	.L1874
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1875:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L1876
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1877:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1903
	cmpl	$2, 16(%rbx)
	jle	.L1906
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L1880:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1907
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1894
	cmpw	$1040, %cx
	jne	.L1882
.L1894:
	movq	23(%rdx), %r12
.L1884:
	testq	%r12, %r12
	je	.L1865
	cmpq	$0, 112(%r12)
	je	.L1908
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L1899
.L1889:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L1901
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L1891
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1903:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1905:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -68(%rbp)
	jle	.L1909
.L1871:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L1874:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1875
	.p2align 4,,10
	.p2align 3
.L1876:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1877
	.p2align 4,,10
	.p2align 3
.L1906:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1880
	.p2align 4,,10
	.p2align 3
.L1891:
	movl	-60(%rbp), %ecx
	movl	-68(%rbp), %esi
	leaq	40(%r12), %rdi
	movl	-64(%rbp), %edx
	addq	-56(%rbp), %rdx
	call	uvwasi_path_unlink_file@PLT
.L1901:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1865
.L1907:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1882:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1884
.L1908:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1895
	cmpw	$1040, %si
	jne	.L1886
.L1895:
	movq	23(%rdx), %rax
.L1888:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1865
.L1886:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1888
.L1904:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7801:
.L1899:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-64(%rbp), %rcx
	leaq	-68(%rbp), %rdx
	leaq	-60(%rbp), %r8
	leaq	.LC105(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1889
	.cfi_endproc
.LFE7801:
	.text
	.size	_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI14PathUnlinkFileERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE106:
	.text
.LHOTE106:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10405:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L1911
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1911:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L1912:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L1912
	cmpb	$120, %dl
	jg	.L1913
	cmpb	$99, %dl
	jg	.L1914
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L1915
	cmpb	$88, %dl
	je	.L1916
	jmp	.L1913
.L1914:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1913
	leaq	.L1918(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1918:
	.long	.L1919-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1919-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1921-.L1918
	.long	.L1920-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1919-.L1918
	.long	.L1913-.L1918
	.long	.L1919-.L1918
	.long	.L1913-.L1918
	.long	.L1913-.L1918
	.long	.L1917-.L1918
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1915:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L1922
	call	_ZdlPv@PLT
.L1922:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L1942
	jmp	.L1924
.L1913:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1947
	call	_ZdlPv@PLT
	jmp	.L1947
.L1919:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L1944
.L1921:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L1929:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1929
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1944
.L1917:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1944:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1941
	jmp	.L1928
.L1916:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L1932
	call	_ZdlPv@PLT
.L1932:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L1928
.L1941:
	call	_ZdlPv@PLT
	jmp	.L1928
.L1920:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1928:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1947:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1924
.L1942:
	call	_ZdlPv@PLT
.L1924:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1910
	call	_ZdlPv@PLT
.L1910:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1936
	call	__stack_chk_fail@PLT
.L1936:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10405:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1949
	call	__stack_chk_fail@PLT
.L1949:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10273:
	.size	_ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_:
.LFB10000:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1951
	call	_ZdlPv@PLT
.L1951:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1953
	call	__stack_chk_fail@PLT
.L1953:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10000:
	.size	_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC107:
	.string	"poll_oneoff(%d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB108:
	.text
.LHOTB108:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 16(%rdi)
	je	.L1956
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L1955:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2022
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1956:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2020
	movl	16(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L2023
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -92(%rbp)
	jg	.L1961
.L2028:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1962:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2020
	cmpl	$1, 16(%rbx)
	jg	.L1964
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1965:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -88(%rbp)
	jg	.L1966
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1967:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2020
	cmpl	$2, 16(%rbx)
	jle	.L2024
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L1970:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L1971
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1972:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2025
	cmpl	$3, 16(%rbx)
	jg	.L1974
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1975:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -80(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2026
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2001
	cmpw	$1040, %cx
	jne	.L1977
.L2001:
	movq	23(%rdx), %r12
.L1979:
	testq	%r12, %r12
	je	.L1955
	cmpq	$0, 112(%r12)
	je	.L2027
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2016
.L1982:
	leaq	-64(%rbp), %rdx
	leaq	-72(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L1983
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2020:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -92(%rbp)
	jle	.L2028
.L1961:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1962
	.p2align 4,,10
	.p2align 3
.L1964:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1965
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1967
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1970
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1972
	.p2align 4,,10
	.p2align 3
.L2025:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1974:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1975
	.p2align 4,,10
	.p2align 3
.L1983:
	movl	-84(%rbp), %eax
	movl	-92(%rbp), %edi
	movq	-64(%rbp), %rsi
	leal	(%rax,%rax,2), %edx
	sall	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2021
	movl	-84(%rbp), %edx
	movl	-88(%rbp), %edi
	movq	-64(%rbp), %rsi
	sall	$5, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2021
	movl	-80(%rbp), %edi
	movq	-64(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2021
	movl	-84(%rbp), %eax
	testq	%rax, %rax
	je	.L1987
	leaq	(%rax,%rax,2), %rdx
	salq	$4, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r14
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r13
	leaq	(%rax,%r14), %rdx
	.p2align 4,,10
	.p2align 3
.L1988:
	xorl	%edi, %edi
	movb	$0, 8(%rax)
	addq	$48, %rax
	movq	$0, -48(%rax)
	movl	$0, -32(%rax)
	movups	%xmm0, -24(%rax)
	movw	%di, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L1988
	movl	-84(%rbp), %edx
	testq	%rdx, %rdx
	je	.L1989
	salq	$5, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r15
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r15), %rdx
	.p2align 4,,10
	.p2align 3
.L1990:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	$0, (%rax)
	addq	$32, %rax
	movw	%cx, -24(%rax)
	movb	$0, -22(%rax)
	movq	$0, -16(%rax)
	movw	%si, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L1990
	movl	-84(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L1991
	movl	-92(%rbp), %esi
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1994:
	movl	%r15d, %eax
	movq	-72(%rbp), %rdi
	addl	$1, %r15d
	leaq	(%rax,%rax,2), %rdx
	salq	$4, %rdx
	addq	%r13, %rdx
	call	uvwasi_serdes_read_subscription_t@PLT
	movl	-92(%rbp), %eax
	movl	-84(%rbp), %ecx
	leal	48(%rax), %esi
	movl	%esi, -92(%rbp)
	cmpl	%r15d, %ecx
	ja	.L1994
.L1991:
	leaq	40(%r12), %rdi
	leaq	-76(%rbp), %r8
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	uvwasi_poll_oneoff@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L1995
.L1998:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	testq	%r14, %r14
	je	.L1997
	movq	%r14, %rdi
	call	_ZdlPv@PLT
.L1997:
	testq	%r13, %r13
	je	.L1955
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L2021:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L1955
.L2026:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1977:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1979
.L2027:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L1955
.L1995:
	movl	-80(%rbp), %esi
	movl	-76(%rbp), %edx
	movq	-72(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	cmpl	$0, -84(%rbp)
	je	.L1998
	movl	-88(%rbp), %eax
	xorl	%r15d, %r15d
.L1999:
	movl	%r15d, %edx
	movq	-72(%rbp), %rdi
	movl	%eax, %esi
	addl	$1, %r15d
	salq	$5, %rdx
	addq	%r14, %rdx
	call	uvwasi_serdes_write_event_t@PLT
	movl	-88(%rbp), %eax
	addl	$32, %eax
	movl	%eax, -88(%rbp)
	cmpl	%r15d, -84(%rbp)
	ja	.L1999
	jmp	.L1998
.L1987:
	xorl	%r13d, %r13d
.L1989:
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	jmp	.L1991
.L2022:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7802:
.L2016:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	stderr(%rip), %rdi
	leaq	-88(%rbp), %rcx
	leaq	-92(%rbp), %rdx
	leaq	-80(%rbp), %r9
	leaq	-84(%rbp), %r8
	leaq	.LC107(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L1982
	.cfi_endproc
.LFE7802:
	.text
	.size	_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI10PollOneoffERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE108:
	.text
.LHOTE108:
	.section	.rodata.str1.1
.LC109:
	.string	"fd_read(%d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB110:
	.text
.LHOTB110:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7785:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 16(%rdi)
	je	.L2030
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2029:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2085
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2030:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2083
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L2086
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L2035
.L2092:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2036:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2083
	cmpl	$1, 16(%rbx)
	jg	.L2038
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2039:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L2040
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2041:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2087
	cmpl	$2, 16(%rbx)
	jle	.L2088
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2044:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L2045
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2046:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2083
	cmpl	$3, 16(%rbx)
	jg	.L2048
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2049:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -64(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2089
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2070
	cmpw	$1040, %cx
	jne	.L2051
.L2070:
	movq	23(%rdx), %r12
.L2053:
	testq	%r12, %r12
	je	.L2029
	cmpq	$0, 112(%r12)
	je	.L2090
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2079
.L2059:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L2067
	movl	-68(%rbp), %eax
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	leal	0(,%rax,8), %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2084
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2084
	movl	-68(%rbp), %edx
	testq	%rdx, %rdx
	je	.L2063
	salq	$4, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L2064
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %r8d
	movq	%r14, %rcx
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_iovec_t@PLT
	testw	%ax, %ax
	jne	.L2065
.L2068:
	movl	-68(%rbp), %ecx
	movl	-76(%rbp), %esi
	leaq	40(%r12), %rdi
	leaq	-60(%rbp), %r8
	movq	%r14, %rdx
	call	uvwasi_fd_read@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L2091
.L2066:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	testq	%r14, %r14
	je	.L2029
.L2081:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2083:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2086:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jle	.L2092
.L2035:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2038:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2040:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2041
	.p2align 4,,10
	.p2align 3
.L2088:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2044
	.p2align 4,,10
	.p2align 3
.L2087:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2045:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2046
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2049
.L2063:
	movl	-72(%rbp), %edx
	movq	-48(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-56(%rbp), %rdi
	xorl	%r14d, %r14d
	call	uvwasi_serdes_readv_iovec_t@PLT
	testw	%ax, %ax
	je	.L2068
	.p2align 4,,10
	.p2align 3
.L2067:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2029
	.p2align 4,,10
	.p2align 3
.L2084:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2029
.L2089:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2051:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2053
.L2090:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2071
	cmpw	$1040, %si
	jne	.L2056
.L2071:
	movq	23(%rdx), %rax
.L2058:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2029
.L2065:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2081
.L2091:
	movl	-64(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L2066
.L2056:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2058
.L2085:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7785:
.L2079:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	stderr(%rip), %rdi
	leaq	-72(%rbp), %rcx
	leaq	-76(%rbp), %rdx
	leaq	-64(%rbp), %r9
	leaq	-68(%rbp), %r8
	leaq	.LC109(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L2059
	.cfi_endproc
.LFE7785:
	.text
	.size	_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI6FdReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE110:
	.text
.LHOTE110:
	.section	.rodata.str1.1
.LC111:
	.string	"fd_write(%d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB112:
	.text
.LHOTB112:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 16(%rdi)
	je	.L2094
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2093:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2149
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2094:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2147
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L2150
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L2099
.L2156:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2100:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2147
	cmpl	$1, 16(%rbx)
	jg	.L2102
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2103:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L2104
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2105:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2151
	cmpl	$2, 16(%rbx)
	jle	.L2152
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2108:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L2109
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2110:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2147
	cmpl	$3, 16(%rbx)
	jg	.L2112
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2113:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -64(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2153
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2134
	cmpw	$1040, %cx
	jne	.L2115
.L2134:
	movq	23(%rdx), %r12
.L2117:
	testq	%r12, %r12
	je	.L2093
	cmpq	$0, 112(%r12)
	je	.L2154
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2143
.L2123:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L2131
	movl	-68(%rbp), %eax
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	leal	0(,%rax,8), %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2148
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2148
	movl	-68(%rbp), %edx
	testq	%rdx, %rdx
	je	.L2127
	salq	$4, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L2128
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %r8d
	movq	%r14, %rcx
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_ciovec_t@PLT
	testw	%ax, %ax
	jne	.L2129
.L2132:
	movl	-68(%rbp), %ecx
	movl	-76(%rbp), %esi
	leaq	40(%r12), %rdi
	leaq	-60(%rbp), %r8
	movq	%r14, %rdx
	call	uvwasi_fd_write@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L2155
.L2130:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	testq	%r14, %r14
	je	.L2093
.L2145:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2147:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jle	.L2156
.L2099:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2100
	.p2align 4,,10
	.p2align 3
.L2102:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2103
	.p2align 4,,10
	.p2align 3
.L2104:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2105
	.p2align 4,,10
	.p2align 3
.L2152:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2151:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2109:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2110
	.p2align 4,,10
	.p2align 3
.L2112:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2113
.L2127:
	movl	-72(%rbp), %edx
	movq	-48(%rbp), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	-56(%rbp), %rdi
	xorl	%r14d, %r14d
	call	uvwasi_serdes_readv_ciovec_t@PLT
	testw	%ax, %ax
	je	.L2132
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2093
	.p2align 4,,10
	.p2align 3
.L2148:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2093
.L2153:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2115:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2117
.L2154:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2135
	cmpw	$1040, %si
	jne	.L2120
.L2135:
	movq	23(%rdx), %rax
.L2122:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2093
.L2129:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2145
.L2155:
	movl	-64(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L2130
.L2120:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2122
.L2149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7791:
.L2143:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	stderr(%rip), %rdi
	leaq	-72(%rbp), %rcx
	leaq	-76(%rbp), %rdx
	leaq	-64(%rbp), %r9
	leaq	-68(%rbp), %r8
	leaq	.LC111(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L2123
	.cfi_endproc
.LFE7791:
	.text
	.size	_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI7FdWriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE112:
	.text
.LHOTE112:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L2158
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2158:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L2159:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L2159
	cmpb	$120, %dl
	jg	.L2160
	cmpb	$99, %dl
	jg	.L2161
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L2162
	cmpb	$88, %dl
	je	.L2163
	jmp	.L2160
.L2161:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2160
	leaq	.L2165(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2165:
	.long	.L2166-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2166-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2168-.L2165
	.long	.L2167-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2166-.L2165
	.long	.L2160-.L2165
	.long	.L2166-.L2165
	.long	.L2160-.L2165
	.long	.L2160-.L2165
	.long	.L2164-.L2165
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2162:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	-200(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r13, %rdx
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r8
	movq	-144(%rbp), %rdi
	popq	%r9
	cmpq	-216(%rbp), %rdi
	jne	.L2196
	jmp	.L2194
.L2160:
	pushq	%rax
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L2194
.L2196:
	call	_ZdlPv@PLT
	jmp	.L2194
.L2166:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2191
.L2168:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L2176:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2176
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2191
.L2164:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2191:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2188
	jmp	.L2175
.L2163:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2179
	call	_ZdlPv@PLT
.L2179:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L2175
.L2188:
	call	_ZdlPv@PLT
	jmp	.L2175
.L2167:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2175:
	movq	-184(%rbp), %rsi
	leaq	-112(%rbp), %r13
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %r9
	movq	%r13, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2194:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2171
	call	_ZdlPv@PLT
.L2171:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L2157
	call	_ZdlPv@PLT
.L2157:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2183
	call	__stack_chk_fail@PLT
.L2183:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10411:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2198
	call	__stack_chk_fail@PLT
.L2198:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10279:
	.size	_ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_:
.LFB10006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L2200
	call	_ZdlPv@PLT
.L2200:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2202
	call	__stack_chk_fail@PLT
.L2202:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10006:
	.size	_ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC113:
	.string	"path_symlink(%d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB114:
	.text
.LHOTB114:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 16(%rdi)
	je	.L2205
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2204:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2252
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2205:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2251
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L2253
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L2210
.L2257:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2211:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2250
	cmpl	$1, 16(%rbx)
	jg	.L2213
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2214:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L2215
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2216:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2251
	cmpl	$2, 16(%rbx)
	jle	.L2254
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2219:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L2220
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2221:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2250
	cmpl	$3, 16(%rbx)
	jg	.L2223
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2224:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L2225
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2226:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2251
	cmpl	$4, 16(%rbx)
	jg	.L2228
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2229:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2255
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2241
	cmpw	$1040, %cx
	jne	.L2231
.L2241:
	movq	23(%rdx), %r12
.L2233:
	testq	%r12, %r12
	je	.L2204
	cmpq	$0, 112(%r12)
	je	.L2256
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2245
.L2235:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L2249
	movl	-72(%rbp), %edx
	movl	-76(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2247
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2247
	movq	-56(%rbp), %rax
	movl	-64(%rbp), %edx
	leaq	40(%r12), %rdi
	movl	-76(%rbp), %esi
	movl	-60(%rbp), %r9d
	leaq	(%rax,%rdx), %r8
	movl	-68(%rbp), %ecx
	movl	-72(%rbp), %edx
	addq	%rax, %rsi
	call	uvwasi_path_symlink@PLT
	.p2align 4,,10
	.p2align 3
.L2249:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2253:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jle	.L2257
.L2210:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2211
	.p2align 4,,10
	.p2align 3
.L2251:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2250:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2204
	.p2align 4,,10
	.p2align 3
.L2215:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2216
	.p2align 4,,10
	.p2align 3
.L2213:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2214
	.p2align 4,,10
	.p2align 3
.L2254:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2220:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2225:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2226
	.p2align 4,,10
	.p2align 3
.L2223:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2224
	.p2align 4,,10
	.p2align 3
.L2228:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2247:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2204
.L2255:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2231:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2233
.L2256:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2204
.L2252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7800:
.L2245:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	pushq	%rax
	leaq	-60(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-72(%rbp), %rcx
	pushq	%rax
	leaq	-76(%rbp), %rdx
	leaq	-64(%rbp), %r9
	leaq	-68(%rbp), %r8
	leaq	.LC113(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	popq	%rdx
	popq	%rcx
	jmp	.L2235
	.cfi_endproc
.LFE7800:
	.text
	.size	_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI11PathSymlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE114:
	.text
.LHOTE114:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L2259
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2259:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L2260:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L2260
	cmpb	$120, %dl
	jg	.L2261
	cmpb	$99, %dl
	jg	.L2262
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L2263
	cmpb	$88, %dl
	je	.L2264
	jmp	.L2261
.L2262:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2261
	leaq	.L2266(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2266:
	.long	.L2267-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2267-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2269-.L2266
	.long	.L2268-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2267-.L2266
	.long	.L2261-.L2266
	.long	.L2267-.L2266
	.long	.L2261-.L2266
	.long	.L2261-.L2266
	.long	.L2265-.L2266
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2263:
	pushq	-208(%rbp)
	movq	%rbx, %r9
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	-184(%rbp), %rsi
	pushq	-200(%rbp)
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	movq	%r11, -224(%rbp)
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r9
	movq	-144(%rbp), %rdi
	popq	%r10
	cmpq	-216(%rbp), %rdi
	jne	.L2295
	jmp	.L2273
.L2261:
	pushq	-208(%rbp)
	leaq	-112(%rbp), %r11
	movq	%r15, %r8
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rsi
	popq	%r8
	cmpq	%rax, %rdi
	je	.L2273
.L2295:
	call	_ZdlPv@PLT
.L2273:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2290
	jmp	.L2272
.L2267:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2292
.L2269:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L2277:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2277
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2292
.L2265:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2292:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2289
	jmp	.L2276
.L2264:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2280
	call	_ZdlPv@PLT
.L2280:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L2276
.L2289:
	call	_ZdlPv@PLT
	jmp	.L2276
.L2268:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2276:
	movq	-184(%rbp), %rsi
	pushq	%rax
	leaq	-112(%rbp), %r13
	movq	%r15, %rcx
	pushq	-208(%rbp)
	movq	-200(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L2272
.L2290:
	call	_ZdlPv@PLT
.L2272:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2258
	call	_ZdlPv@PLT
.L2258:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2284
	call	__stack_chk_fail@PLT
.L2284:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10410:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2297
	call	__stack_chk_fail@PLT
.L2297:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10278:
	.size	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_:
.LFB10005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L2299
	call	_ZdlPv@PLT
.L2299:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2301
	call	__stack_chk_fail@PLT
.L2301:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10005:
	.size	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC115:
	.string	"path_readlink(%d, %d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB116:
	.text
.LHOTB116:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$6, 16(%rdi)
	je	.L2304
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2303:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2358
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2304:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2356
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jle	.L2359
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L2309
.L2363:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2310:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2357
	cmpl	$1, 16(%rbx)
	jg	.L2312
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2313:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L2314
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2315:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2356
	cmpl	$2, 16(%rbx)
	jle	.L2360
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2318:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L2319
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2320:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2357
	cmpl	$3, 16(%rbx)
	jg	.L2322
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2323:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L2324
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2325:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2356
	cmpl	$4, 16(%rbx)
	jg	.L2327
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2328:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$5, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L2329
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2330:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2357
	cmpl	$5, 16(%rbx)
	jg	.L2332
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2333:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -64(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2361
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2347
	cmpw	$1040, %cx
	jne	.L2335
.L2347:
	movq	23(%rdx), %r12
.L2337:
	testq	%r12, %r12
	je	.L2303
	cmpq	$0, 112(%r12)
	je	.L2362
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2351
.L2339:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L2340
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2303
	.p2align 4,,10
	.p2align 3
.L2359:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jle	.L2363
.L2309:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2310
	.p2align 4,,10
	.p2align 3
.L2356:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2303
	.p2align 4,,10
	.p2align 3
.L2357:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2303
	.p2align 4,,10
	.p2align 3
.L2314:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2315
	.p2align 4,,10
	.p2align 3
.L2312:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2313
	.p2align 4,,10
	.p2align 3
.L2360:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2318
	.p2align 4,,10
	.p2align 3
.L2319:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2320
	.p2align 4,,10
	.p2align 3
.L2324:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2325
	.p2align 4,,10
	.p2align 3
.L2322:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2323
	.p2align 4,,10
	.p2align 3
.L2329:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L2330
	.p2align 4,,10
	.p2align 3
.L2327:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2328
	.p2align 4,,10
	.p2align 3
.L2332:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L2333
.L2340:
	movl	-76(%rbp), %edx
	movl	-80(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2355
	movl	-68(%rbp), %edx
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L2355
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L2343
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2303
.L2355:
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2303
.L2361:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2335:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2337
.L2362:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2303
.L2343:
	subq	$8, %rsp
	movq	-56(%rbp), %rax
	movl	-72(%rbp), %ecx
	leaq	-60(%rbp), %rsi
	movl	-80(%rbp), %edx
	movl	-68(%rbp), %r9d
	pushq	%rsi
	leaq	40(%r12), %rdi
	leaq	(%rax,%rcx), %r8
	movl	-84(%rbp), %esi
	movl	-76(%rbp), %ecx
	addq	%rax, %rdx
	call	uvwasi_path_readlink@PLT
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	testw	%r12w, %r12w
	je	.L2364
.L2344:
	movq	(%rbx), %rdx
	movzwl	%r12w, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2303
.L2358:
	call	__stack_chk_fail@PLT
.L2364:
	movl	-64(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L2344
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7797:
.L2351:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	-64(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-80(%rbp), %rcx
	pushq	%rax
	leaq	-68(%rbp), %rax
	leaq	.LC115(%rip), %rsi
	pushq	%rax
	leaq	-84(%rbp), %rdx
	leaq	-72(%rbp), %r9
	leaq	-76(%rbp), %r8
	call	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	popq	%rcx
	popq	%rsi
	jmp	.L2339
	.cfi_endproc
.LFE7797:
	.text
	.size	_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI12PathReadlinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE116:
	.text
.LHOTE116:
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"path_rename(%d, %d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB118:
	.text
.LHOTB118:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7799:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$6, 16(%rdi)
	je	.L2366
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2365:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2417
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2366:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2415
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jle	.L2418
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L2371
.L2422:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2372:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2416
	cmpl	$1, 16(%rbx)
	jg	.L2374
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2375:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L2376
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2377:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2415
	cmpl	$2, 16(%rbx)
	jle	.L2419
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2380:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L2381
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2382:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2416
	cmpl	$3, 16(%rbx)
	jg	.L2384
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2385:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L2386
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2387:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2415
	cmpl	$4, 16(%rbx)
	jg	.L2389
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2390:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$5, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L2391
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2392:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2416
	cmpl	$5, 16(%rbx)
	jg	.L2394
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2395:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2420
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2407
	cmpw	$1040, %cx
	jne	.L2397
.L2407:
	movq	23(%rdx), %r12
.L2399:
	testq	%r12, %r12
	je	.L2365
	cmpq	$0, 112(%r12)
	je	.L2421
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2411
.L2401:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L2402
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2418:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -80(%rbp)
	jle	.L2422
.L2371:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2372
	.p2align 4,,10
	.p2align 3
.L2415:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2416:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2365
	.p2align 4,,10
	.p2align 3
.L2376:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2377
	.p2align 4,,10
	.p2align 3
.L2374:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2375
	.p2align 4,,10
	.p2align 3
.L2419:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2380
	.p2align 4,,10
	.p2align 3
.L2381:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2382
	.p2align 4,,10
	.p2align 3
.L2386:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2387
	.p2align 4,,10
	.p2align 3
.L2384:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2385
	.p2align 4,,10
	.p2align 3
.L2391:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L2392
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2390
	.p2align 4,,10
	.p2align 3
.L2394:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L2395
.L2402:
	movl	-72(%rbp), %edx
	movl	-76(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L2403
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2365
.L2403:
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L2404
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2365
.L2420:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2397:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2399
.L2404:
	movl	-60(%rbp), %esi
	subq	$8, %rsp
	movq	-56(%rbp), %rax
	leaq	40(%r12), %rdi
	movl	-64(%rbp), %ecx
	movl	-76(%rbp), %edx
	pushq	%rsi
	movl	-68(%rbp), %r8d
	leaq	(%rax,%rcx), %r9
	movl	-80(%rbp), %esi
	movl	-72(%rbp), %ecx
	addq	%rax, %rdx
	call	uvwasi_path_rename@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	popq	%rax
	popq	%rdx
	jmp	.L2365
.L2421:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2365
.L2417:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7799:
.L2411:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	-60(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-76(%rbp), %rcx
	pushq	%rax
	leaq	-64(%rbp), %rax
	leaq	.LC117(%rip), %rsi
	pushq	%rax
	leaq	-80(%rbp), %rdx
	leaq	-68(%rbp), %r9
	leaq	-72(%rbp), %r8
	call	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	popq	%rcx
	popq	%rsi
	jmp	.L2401
	.cfi_endproc
.LFE7799:
	.text
	.size	_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI10PathRenameERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE118:
	.text
.LHOTE118:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r11, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L2424
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2424:
	movq	%rax, %r10
	leaq	-176(%rbp), %r14
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	movq	%rbx, -224(%rbp)
	movq	%r15, %rbx
	movq	%r14, -232(%rbp)
	movq	%r12, %r15
	leaq	.LC59(%rip), %rcx
	movq	%r13, %r14
	movq	%r10, %r12
.L2425:
	movq	%r12, %rax
	movq	%rcx, %rdi
	movq	%r12, -184(%rbp)
	leaq	1(%r12), %r12
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L2425
	movl	%r13d, %edx
	movq	%r12, %r10
	movq	%r14, %r13
	movq	%r15, %r12
	movq	-232(%rbp), %r14
	movq	%rbx, %r15
	movq	-224(%rbp), %rbx
	cmpb	$120, %dl
	jg	.L2426
	cmpb	$99, %dl
	jg	.L2427
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	leaq	-144(%rbp), %r11
	je	.L2428
	cmpb	$88, %dl
	je	.L2429
	jmp	.L2426
.L2427:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2426
	leaq	.L2431(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2431:
	.long	.L2432-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2432-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2434-.L2431
	.long	.L2433-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2432-.L2431
	.long	.L2426-.L2431
	.long	.L2432-.L2431
	.long	.L2426-.L2431
	.long	.L2426-.L2431
	.long	.L2430-.L2431
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2428:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%rax, %rdi
	pushq	-216(%rbp)
	movq	-192(%rbp), %rcx
	movq	%r15, %r8
	movq	%r12, %rdx
	pushq	-208(%rbp)
	addq	$2, %rsi
	pushq	-200(%rbp)
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movq	%r14, %rsi
	addq	$32, %rsp
	movl	$37, %edx
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r11, %rsi
	jmp	.L2463
.L2426:
	pushq	%rsi
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r12, %rdx
	pushq	-216(%rbp)
	movq	%r11, %rdi
	movq	%r10, %rsi
	movq	%rbx, %r9
	pushq	-208(%rbp)
	movq	%r15, %r8
	leaq	-144(%rbp), %r12
	pushq	-200(%rbp)
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r12, %rsi
	movq	%r11, %rdx
.L2463:
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2438
	call	_ZdlPv@PLT
.L2438:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2455
	jmp	.L2437
.L2432:
	movl	(%r12), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	jmp	.L2459
.L2434:
	movb	$0, -57(%rbp)
	movl	(%r12), %eax
	leaq	-57(%rbp), %rsi
.L2442:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2442
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2457
.L2430:
	movl	(%r12), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2457:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
.L2459:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2454
	jmp	.L2441
.L2429:
	movl	(%r12), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2445
	call	_ZdlPv@PLT
.L2445:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2441
.L2454:
	call	_ZdlPv@PLT
	jmp	.L2441
.L2433:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2441:
	pushq	-216(%rbp)
	leaq	-112(%rbp), %r12
	movq	%r15, %rcx
	movq	%rbx, %r8
	movq	-184(%rbp), %rsi
	pushq	-208(%rbp)
	movq	%r12, %rdi
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L2437
.L2455:
	call	_ZdlPv@PLT
.L2437:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2423
	call	_ZdlPv@PLT
.L2423:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2449
	call	__stack_chk_fail@PLT
.L2449:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10408:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10276:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2465
	call	__stack_chk_fail@PLT
.L2465:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10276:
	.size	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_:
.LFB10003:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	addq	$32, %rsp
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2467
	call	_ZdlPv@PLT
.L2467:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2469
	call	__stack_chk_fail@PLT
.L2469:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10003:
	.size	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"path_link(%d, %d, %d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB120:
	.text
.LHOTB120:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$7, 16(%rdi)
	je	.L2472
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L2471:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2529
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2472:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2526
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jle	.L2530
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L2477
.L2534:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2478:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2528
	cmpl	$1, 16(%rbx)
	jg	.L2480
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2481:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L2482
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2483:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2526
	cmpl	$2, 16(%rbx)
	jle	.L2531
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2486:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L2487
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2488:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2528
	cmpl	$3, 16(%rbx)
	jg	.L2490
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2491:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L2492
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2493:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2528
	cmpl	$4, 16(%rbx)
	jg	.L2495
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2496:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$5, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L2497
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2498:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2526
	cmpl	$5, 16(%rbx)
	jg	.L2500
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2501:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$6, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L2502
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2503:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2528
	cmpl	$6, 16(%rbx)
	jg	.L2505
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2506:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -60(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2532
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2518
	cmpw	$1040, %cx
	jne	.L2508
.L2518:
	movq	23(%rdx), %r12
.L2510:
	testq	%r12, %r12
	je	.L2471
	cmpq	$0, 112(%r12)
	je	.L2533
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2522
.L2512:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L2513
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2530:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jle	.L2534
.L2477:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2478
	.p2align 4,,10
	.p2align 3
.L2526:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2528:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2471
	.p2align 4,,10
	.p2align 3
.L2482:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2483
	.p2align 4,,10
	.p2align 3
.L2480:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2481
	.p2align 4,,10
	.p2align 3
.L2531:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2486
	.p2align 4,,10
	.p2align 3
.L2487:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2488
	.p2align 4,,10
	.p2align 3
.L2492:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2493
	.p2align 4,,10
	.p2align 3
.L2490:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L2491
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L2498
	.p2align 4,,10
	.p2align 3
.L2495:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2496
	.p2align 4,,10
	.p2align 3
.L2502:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L2503
	.p2align 4,,10
	.p2align 3
.L2500:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L2501
.L2505:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L2506
.L2513:
	movl	-72(%rbp), %edx
	movl	-76(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L2514
	movabsq	$261993005056, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L2471
.L2514:
	movl	-60(%rbp), %edx
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L2515
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2471
.L2532:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2508:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2510
.L2515:
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rax
	leaq	40(%r12), %rdi
	movl	-76(%rbp), %ecx
	movl	-68(%rbp), %r9d
	movl	-72(%rbp), %r8d
	movl	-84(%rbp), %esi
	pushq	%rdx
	movl	-64(%rbp), %edx
	addq	%rax, %rcx
	addq	%rdx, %rax
	movl	-80(%rbp), %edx
	pushq	%rax
	call	uvwasi_path_link@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	popq	%rax
	popq	%rdx
	jmp	.L2471
.L2533:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2471
.L2529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7795:
.L2522:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	-60(%rbp), %rax
	pushq	%rsi
	movq	stderr(%rip), %rdi
	leaq	-80(%rbp), %rcx
	pushq	%rax
	leaq	-64(%rbp), %rax
	leaq	-84(%rbp), %rdx
	pushq	%rax
	leaq	-68(%rbp), %rax
	leaq	-72(%rbp), %r9
	pushq	%rax
	leaq	-76(%rbp), %r8
	leaq	.LC119(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_S1_S1_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	addq	$32, %rsp
	jmp	.L2512
	.cfi_endproc
.LFE7795:
	.text
	.size	_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI8PathLinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE120:
	.text
.LHOTE120:
	.section	.rodata._ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC121:
	.string	"%lu"
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2536
	leaq	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2536:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2537:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2537
	cmpb	$120, %dl
	jg	.L2538
	cmpb	$99, %dl
	jg	.L2539
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2540
	cmpb	$88, %dl
	je	.L2541
	jmp	.L2538
.L2539:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2538
	leaq	.L2543(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2543:
	.long	.L2544-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2544-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2546-.L2543
	.long	.L2545-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2544-.L2543
	.long	.L2538-.L2543
	.long	.L2544-.L2543
	.long	.L2538-.L2543
	.long	.L2538-.L2543
	.long	.L2542-.L2543
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2540:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2547
	call	_ZdlPv@PLT
.L2547:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2567
	jmp	.L2549
.L2538:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2572
	call	_ZdlPv@PLT
	jmp	.L2572
.L2544:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2569
.L2546:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2554:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2554
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2569
.L2542:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2569:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2566
	jmp	.L2553
.L2541:
	movq	(%r8), %rsi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2557
	call	_ZdlPv@PLT
.L2557:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2553
.L2566:
	call	_ZdlPv@PLT
	jmp	.L2553
.L2545:
	leaq	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2553:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2572:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2549
.L2567:
	call	_ZdlPv@PLT
.L2549:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2535
	call	_ZdlPv@PLT
.L2535:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2561
	call	__stack_chk_fail@PLT
.L2561:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10496:
	.size	_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10392:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L2574
	leaq	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2574:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L2575:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L2575
	cmpb	$120, %dl
	jg	.L2576
	cmpb	$99, %dl
	jg	.L2577
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L2578
	cmpb	$88, %dl
	je	.L2579
	jmp	.L2576
.L2577:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2576
	leaq	.L2581(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2581:
	.long	.L2582-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2582-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2584-.L2581
	.long	.L2583-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2582-.L2581
	.long	.L2576-.L2581
	.long	.L2582-.L2581
	.long	.L2576-.L2581
	.long	.L2576-.L2581
	.long	.L2580-.L2581
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2578:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2585
	call	_ZdlPv@PLT
.L2585:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2605
	jmp	.L2587
.L2576:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2610
	call	_ZdlPv@PLT
	jmp	.L2610
.L2582:
	movl	(%r9), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2607
.L2584:
	movb	$0, -57(%rbp)
	movl	(%r9), %eax
	leaq	-57(%rbp), %rsi
.L2592:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2592
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2607
.L2580:
	movl	(%r9), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2607:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2604
	jmp	.L2591
.L2579:
	movl	(%r9), %esi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2595
	call	_ZdlPv@PLT
.L2595:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2591
.L2604:
	call	_ZdlPv@PLT
	jmp	.L2591
.L2583:
	leaq	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2591:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2610:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2587
.L2605:
	call	_ZdlPv@PLT
.L2587:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2573
	call	_ZdlPv@PLT
.L2573:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2599
	call	__stack_chk_fail@PLT
.L2599:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10392:
	.size	_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2612
	call	__stack_chk_fail@PLT
.L2612:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10256:
	.size	_ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_:
.LFB9969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2614
	call	_ZdlPv@PLT
.L2614:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2616
	call	__stack_chk_fail@PLT
.L2616:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9969:
	.size	_ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC122:
	.string	"clock_time_get(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB123:
	.text
.LHOTB123:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 16(%rdi)
	je	.L2619
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2618:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2656
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2619:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2655
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L2657
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L2624
.L2662:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2625:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L2658
	cmpl	$1, 16(%rbx)
	jg	.L2627
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2628:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, -72(%rbp)
	jg	.L2629
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2630:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2655
	cmpl	$2, 16(%rbx)
	jle	.L2659
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2633:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movl	%eax, -76(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2660
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2648
	cmpw	$1040, %cx
	jne	.L2635
.L2648:
	movq	23(%rdx), %r13
.L2637:
	testq	%r13, %r13
	je	.L2618
	cmpq	$0, 112(%r13)
	je	.L2661
	movq	16(%r13), %rax
	cmpb	$0, 2259(%rax)
	jne	.L2653
.L2642:
	leaq	-56(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L2643
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2657:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -80(%rbp)
	jle	.L2662
.L2624:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2625
	.p2align 4,,10
	.p2align 3
.L2655:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2658:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2629:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2627:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2628
	.p2align 4,,10
	.p2align 3
.L2659:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2633
	.p2align 4,,10
	.p2align 3
.L2643:
	movl	-76(%rbp), %edi
	movq	-56(%rbp), %rsi
	movl	$8, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L2644
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2618
	.p2align 4,,10
	.p2align 3
.L2644:
	movq	-72(%rbp), %rdx
	movl	-80(%rbp), %esi
	movq	%r12, %rcx
	leaq	40(%r13), %rdi
	call	uvwasi_clock_time_get@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L2663
.L2645:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L2618
.L2660:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2635:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2637
.L2661:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2649
	cmpw	$1040, %si
	jne	.L2639
.L2649:
	movq	23(%rdx), %rax
.L2641:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2618
.L2663:
	movl	-76(%rbp), %esi
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rdi
	call	uvwasi_serdes_write_timestamp_t@PLT
	jmp	.L2645
.L2639:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2641
.L2656:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7768:
.L2653:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	stderr(%rip), %rdi
	leaq	-72(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	leaq	-76(%rbp), %r8
	leaq	.LC122(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRmS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L2642
	.cfi_endproc
.LFE7768:
	.text
	.size	_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI12ClockTimeGetERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE123:
	.text
.LHOTE123:
	.section	.rodata._ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC124:
	.string	"%d"
	.section	.text.unlikely._ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L2665
	leaq	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2665:
	movq	%rax, %r9
	leaq	-160(%rbp), %r12
	leaq	-144(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %r9
.L2666:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -192(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -184(%rbp)
	movb	%sil, -176(%rbp)
	call	strchr@PLT
	movb	-176(%rbp), %dl
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	-192(%rbp), %r8
	jne	.L2666
	cmpb	$120, %dl
	jg	.L2667
	cmpb	$99, %dl
	jg	.L2668
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-96(%rbp), %r15
	movq	%rax, -176(%rbp)
	leaq	-80(%rbp), %rax
	leaq	-128(%rbp), %r14
	movq	%rax, -184(%rbp)
	je	.L2669
	cmpb	$88, %dl
	je	.L2670
	jmp	.L2667
.L2668:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2667
	leaq	.L2672(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2672:
	.long	.L2673-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2673-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2675-.L2672
	.long	.L2674-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2673-.L2672
	.long	.L2667-.L2672
	.long	.L2673-.L2672
	.long	.L2667-.L2672
	.long	.L2667-.L2672
	.long	.L2671-.L2672
	.section	.text.unlikely._ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2669:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2676
	call	_ZdlPv@PLT
.L2676:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L2696
	jmp	.L2678
.L2667:
	leaq	-96(%rbp), %r14
	movq	%r8, %rdx
	leaq	-128(%rbp), %r15
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2701
	call	_ZdlPv@PLT
	jmp	.L2701
.L2673:
	movzwl	(%r8), %r8d
	leaq	-96(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC124(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2698
.L2675:
	movb	$0, -57(%rbp)
	movzwl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L2683:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2683
	leaq	-96(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2698
.L2671:
	movw	(%r8), %si
	leaq	-96(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2698:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2695
	jmp	.L2682
.L2670:
	movw	(%r8), %si
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2686
	call	_ZdlPv@PLT
.L2686:
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L2682
.L2695:
	call	_ZdlPv@PLT
	jmp	.L2682
.L2674:
	leaq	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2682:
	leaq	-96(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2701:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2678
.L2696:
	call	_ZdlPv@PLT
.L2678:
	movq	-160(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L2664
	call	_ZdlPv@PLT
.L2664:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2690
	call	__stack_chk_fail@PLT
.L2690:
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10499:
	.size	_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2703
	leaq	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2703:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2704:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2704
	cmpb	$120, %dl
	jg	.L2705
	cmpb	$99, %dl
	jg	.L2706
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2707
	cmpb	$88, %dl
	je	.L2708
	jmp	.L2705
.L2706:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2705
	leaq	.L2710(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2710:
	.long	.L2711-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2711-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2713-.L2710
	.long	.L2712-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2711-.L2710
	.long	.L2705-.L2710
	.long	.L2711-.L2710
	.long	.L2705-.L2710
	.long	.L2705-.L2710
	.long	.L2709-.L2710
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2707:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2714
	call	_ZdlPv@PLT
.L2714:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2734
	jmp	.L2716
.L2705:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2739
	call	_ZdlPv@PLT
	jmp	.L2739
.L2711:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2736
.L2713:
	movb	$0, -57(%rbp)
	movl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L2721:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2721
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2736
.L2709:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2736:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2733
	jmp	.L2720
.L2708:
	movl	(%r8), %esi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2724
	call	_ZdlPv@PLT
.L2724:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2720
.L2733:
	call	_ZdlPv@PLT
	jmp	.L2720
.L2712:
	leaq	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2720:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2739:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2716
.L2734:
	call	_ZdlPv@PLT
.L2716:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2702
	call	_ZdlPv@PLT
.L2702:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2728
	call	__stack_chk_fail@PLT
.L2728:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10396:
	.size	_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2741
	call	__stack_chk_fail@PLT
.L2741:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10260:
	.size	_ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_:
.LFB9973:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2743
	call	_ZdlPv@PLT
.L2743:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2745
	call	__stack_chk_fail@PLT
.L2745:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9973:
	.size	_ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC125:
	.string	"fd_fdstat_set_flags(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB126:
	.text
.LHOTB126:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L2748
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L2747:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2777
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2748:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2776
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L2778
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -44(%rbp)
	jg	.L2753
.L2781:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2754:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2776
	cmpl	$1, 16(%rbx)
	jg	.L2756
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2757:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movw	%ax, -46(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2779
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2769
	cmpw	$1040, %cx
	jne	.L2759
.L2769:
	movq	23(%rdx), %rax
.L2761:
	testq	%rax, %rax
	je	.L2747
	cmpq	$0, 112(%rax)
	je	.L2780
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L2774
.L2766:
	movzwl	-46(%rbp), %edx
	movl	-44(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_fd_fdstat_set_flags@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2778:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -44(%rbp)
	jle	.L2781
.L2753:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2754
	.p2align 4,,10
	.p2align 3
.L2776:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2756:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2779:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2759:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2761
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2770
	cmpw	$1040, %cx
	jne	.L2763
.L2770:
	movq	23(%rdx), %rax
.L2765:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2747
.L2763:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2765
.L2777:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7776:
.L2774:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-46(%rbp), %rcx
	leaq	-44(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	.LC125(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRtEEEvP8_IO_FILEPKcDpOT_
	movq	-56(%rbp), %rax
	jmp	.L2766
	.cfi_endproc
.LFE7776:
	.text
	.size	_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI16FdFdstatSetFlagsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE126:
	.text
.LHOTE126:
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2783
	leaq	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2783:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2784:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2784
	cmpb	$120, %dl
	jg	.L2785
	cmpb	$99, %dl
	jg	.L2786
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L2787
	cmpb	$88, %dl
	je	.L2788
	jmp	.L2785
.L2786:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2785
	leaq	.L2790(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2790:
	.long	.L2791-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2791-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2793-.L2790
	.long	.L2792-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2791-.L2790
	.long	.L2785-.L2790
	.long	.L2791-.L2790
	.long	.L2785-.L2790
	.long	.L2785-.L2790
	.long	.L2789-.L2790
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2787:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2794
	call	_ZdlPv@PLT
.L2794:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2814
	jmp	.L2796
.L2785:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2819
	call	_ZdlPv@PLT
	jmp	.L2819
.L2791:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2816
.L2793:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2801:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2801
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2816
.L2789:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2816:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2813
	jmp	.L2800
.L2788:
	movq	(%r8), %rsi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2804
	call	_ZdlPv@PLT
.L2804:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2800
.L2813:
	call	_ZdlPv@PLT
	jmp	.L2800
.L2792:
	leaq	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2800:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2819:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2796
.L2814:
	call	_ZdlPv@PLT
.L2796:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2782
	call	_ZdlPv@PLT
.L2782:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2808
	call	__stack_chk_fail@PLT
.L2808:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10500:
	.size	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2821
	leaq	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2821:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2822:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2822
	cmpb	$120, %dl
	jg	.L2823
	cmpb	$99, %dl
	jg	.L2824
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2825
	cmpb	$88, %dl
	je	.L2826
	jmp	.L2823
.L2824:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2823
	leaq	.L2828(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2828:
	.long	.L2829-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2829-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2831-.L2828
	.long	.L2830-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2829-.L2828
	.long	.L2823-.L2828
	.long	.L2829-.L2828
	.long	.L2823-.L2828
	.long	.L2823-.L2828
	.long	.L2827-.L2828
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2825:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2832
	call	_ZdlPv@PLT
.L2832:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2852
	jmp	.L2834
.L2823:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2857
	call	_ZdlPv@PLT
	jmp	.L2857
.L2829:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2854
.L2831:
	movb	$0, -57(%rbp)
	movl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L2839:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2839
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2854
.L2827:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2854:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2851
	jmp	.L2838
.L2826:
	movl	(%r8), %esi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2842
	call	_ZdlPv@PLT
.L2842:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2838
.L2851:
	call	_ZdlPv@PLT
	jmp	.L2838
.L2830:
	leaq	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2838:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2857:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2834
.L2852:
	call	_ZdlPv@PLT
.L2834:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2820
	call	_ZdlPv@PLT
.L2820:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2846
	call	__stack_chk_fail@PLT
.L2846:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10397:
	.size	_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2859
	call	__stack_chk_fail@PLT
.L2859:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10261:
	.size	_ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_:
.LFB9974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2861
	call	_ZdlPv@PLT
.L2861:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2863
	call	__stack_chk_fail@PLT
.L2863:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9974:
	.size	_ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC127:
	.string	"fd_filestat_set_size(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB128:
	.text
.LHOTB128:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L2866
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2865:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2895
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2866:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2894
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L2896
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -52(%rbp)
	jg	.L2871
.L2899:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2872:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L2894
	cmpl	$1, 16(%rbx)
	jg	.L2874
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2875:
	leaq	-53(%rbp), %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	movq	8(%rbx), %r12
	movq	%rax, -48(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2897
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2887
	cmpw	$1040, %cx
	jne	.L2877
.L2887:
	movq	23(%rdx), %rax
.L2879:
	testq	%rax, %rax
	je	.L2865
	cmpq	$0, 112(%rax)
	je	.L2898
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L2892
.L2884:
	movq	-48(%rbp), %rdx
	movl	-52(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_fd_filestat_set_size@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2865
	.p2align 4,,10
	.p2align 3
.L2896:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -52(%rbp)
	jle	.L2899
.L2871:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2872
	.p2align 4,,10
	.p2align 3
.L2894:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2865
	.p2align 4,,10
	.p2align 3
.L2874:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2875
	.p2align 4,,10
	.p2align 3
.L2897:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2877:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2879
	.p2align 4,,10
	.p2align 3
.L2898:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2888
	cmpw	$1040, %cx
	jne	.L2881
.L2888:
	movq	23(%rdx), %rax
.L2883:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2865
.L2881:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2883
.L2895:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7779:
.L2892:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-48(%rbp), %rcx
	leaq	-52(%rbp), %rdx
	movq	%rax, -72(%rbp)
	leaq	.LC127(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRmEEEvP8_IO_FILEPKcDpOT_
	movq	-72(%rbp), %rax
	jmp	.L2884
	.cfi_endproc
.LFE7779:
	.text
	.size	_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI17FdFilestatSetSizeERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE128:
	.text
.LHOTE128:
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2901
	leaq	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2901:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2902:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2902
	cmpb	$120, %dl
	jg	.L2903
	cmpb	$99, %dl
	jg	.L2904
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2905
	cmpb	$88, %dl
	je	.L2906
	jmp	.L2903
.L2904:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2903
	leaq	.L2908(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2908:
	.long	.L2909-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2909-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2911-.L2908
	.long	.L2910-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2909-.L2908
	.long	.L2903-.L2908
	.long	.L2909-.L2908
	.long	.L2903-.L2908
	.long	.L2903-.L2908
	.long	.L2907-.L2908
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2905:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2912
	call	_ZdlPv@PLT
.L2912:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2932
	jmp	.L2914
.L2903:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2937
	call	_ZdlPv@PLT
	jmp	.L2937
.L2909:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2934
.L2911:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2919:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2919
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2934
.L2907:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2934:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2931
	jmp	.L2918
.L2906:
	movq	(%r8), %rsi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2922
	call	_ZdlPv@PLT
.L2922:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2918
.L2931:
	call	_ZdlPv@PLT
	jmp	.L2918
.L2910:
	leaq	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2918:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2937:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2914
.L2932:
	call	_ZdlPv@PLT
.L2914:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2900
	call	_ZdlPv@PLT
.L2900:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2926
	call	__stack_chk_fail@PLT
.L2926:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10498:
	.size	_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10394:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L2939
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2939:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L2940:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L2940
	cmpb	$120, %dl
	jg	.L2941
	cmpb	$99, %dl
	jg	.L2942
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L2943
	cmpb	$88, %dl
	je	.L2944
	jmp	.L2941
.L2942:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2941
	leaq	.L2946(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2946:
	.long	.L2947-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2947-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2949-.L2946
	.long	.L2948-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2947-.L2946
	.long	.L2941-.L2946
	.long	.L2947-.L2946
	.long	.L2941-.L2946
	.long	.L2941-.L2946
	.long	.L2945-.L2946
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2943:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2950
	call	_ZdlPv@PLT
.L2950:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2970
	jmp	.L2952
.L2941:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2975
	call	_ZdlPv@PLT
	jmp	.L2975
.L2947:
	movl	(%r9), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L2972
.L2949:
	movb	$0, -57(%rbp)
	movl	(%r9), %eax
	leaq	-57(%rbp), %rsi
.L2957:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2957
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2972
.L2945:
	movl	(%r9), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2972:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2969
	jmp	.L2956
.L2944:
	movl	(%r9), %esi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2960
	call	_ZdlPv@PLT
.L2960:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2956
.L2969:
	call	_ZdlPv@PLT
	jmp	.L2956
.L2948:
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2956:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2975:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2952
.L2970:
	call	_ZdlPv@PLT
.L2952:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2938
	call	_ZdlPv@PLT
.L2938:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2964
	call	__stack_chk_fail@PLT
.L2964:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10394:
	.size	_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10258:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2977
	call	__stack_chk_fail@PLT
.L2977:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10258:
	.size	_ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_:
.LFB9971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2979
	call	_ZdlPv@PLT
.L2979:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2981
	call	__stack_chk_fail@PLT
.L2981:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9971:
	.size	_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC129:
	.string	"fd_allocate(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB130:
	.text
.LHOTB130:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 16(%rdi)
	je	.L2984
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L2983:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3018
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2984:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3019
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L3020
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -60(%rbp)
	jg	.L2989
.L3024:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2990:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3017
	cmpl	$1, 16(%rbx)
	jg	.L2992
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2993:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, -56(%rbp)
	jg	.L2994
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2995:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3017
	cmpl	$2, 16(%rbx)
	jle	.L3021
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L2998:
	leaq	-61(%rbp), %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	movq	8(%rbx), %r13
	movq	%rax, -48(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3022
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3010
	cmpw	$1040, %cx
	jne	.L3000
.L3010:
	movq	23(%rdx), %rax
.L3002:
	testq	%rax, %rax
	je	.L2983
	cmpq	$0, 112(%rax)
	je	.L3023
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L3015
.L3007:
	movq	-56(%rbp), %rdx
	movq	-48(%rbp), %rcx
	leaq	40(%rax), %rdi
	movl	-60(%rbp), %esi
	call	uvwasi_fd_allocate@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -60(%rbp)
	jle	.L3024
.L2989:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2990
	.p2align 4,,10
	.p2align 3
.L3017:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L3019:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L2983
	.p2align 4,,10
	.p2align 3
.L2994:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L2995
	.p2align 4,,10
	.p2align 3
.L2992:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2993
	.p2align 4,,10
	.p2align 3
.L3021:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L2998
.L3022:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3000:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3002
.L3023:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3011
	cmpw	$1040, %si
	jne	.L3004
.L3011:
	movq	23(%rdx), %rax
.L3006:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L2983
.L3004:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3006
.L3018:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7772:
.L3015:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	-56(%rbp), %rcx
	leaq	-60(%rbp), %rdx
	movq	%r12, %r8
	movq	%rax, -72(%rbp)
	movq	stderr(%rip), %rdi
	leaq	.LC129(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_
	movq	-72(%rbp), %rax
	jmp	.L3007
	.cfi_endproc
.LFE7772:
	.text
	.size	_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI10FdAllocateERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE130:
	.text
.LHOTE130:
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"fd_fdstat_set_rights(%d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB132:
	.text
.LHOTB132:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$3, 16(%rdi)
	je	.L3026
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L3025:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3060
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3026:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3061
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L3062
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -60(%rbp)
	jg	.L3031
.L3066:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3032:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3059
	cmpl	$1, 16(%rbx)
	jg	.L3034
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3035:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, -56(%rbp)
	jg	.L3036
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3037:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3059
	cmpl	$2, 16(%rbx)
	jle	.L3063
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3040:
	leaq	-61(%rbp), %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	movq	8(%rbx), %r13
	movq	%rax, -48(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3064
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3052
	cmpw	$1040, %cx
	jne	.L3042
.L3052:
	movq	23(%rdx), %rax
.L3044:
	testq	%rax, %rax
	je	.L3025
	cmpq	$0, 112(%rax)
	je	.L3065
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L3057
.L3049:
	movq	-56(%rbp), %rdx
	movq	-48(%rbp), %rcx
	leaq	40(%rax), %rdi
	movl	-60(%rbp), %esi
	call	uvwasi_fd_fdstat_set_rights@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3062:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -60(%rbp)
	jle	.L3066
.L3031:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3032
	.p2align 4,,10
	.p2align 3
.L3059:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3061:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3025
	.p2align 4,,10
	.p2align 3
.L3036:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3037
	.p2align 4,,10
	.p2align 3
.L3034:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3035
	.p2align 4,,10
	.p2align 3
.L3063:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3040
.L3064:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3042:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3044
.L3065:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3053
	cmpw	$1040, %si
	jne	.L3046
.L3053:
	movq	23(%rdx), %rax
.L3048:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3025
.L3046:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3048
.L3060:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7777:
.L3057:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	-56(%rbp), %rcx
	leaq	-60(%rbp), %rdx
	movq	%r12, %r8
	movq	%rax, -72(%rbp)
	movq	stderr(%rip), %rdi
	leaq	.LC131(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRmS2_EEEvP8_IO_FILEPKcDpOT_
	movq	-72(%rbp), %rax
	jmp	.L3049
	.cfi_endproc
.LFE7777:
	.text
	.size	_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI17FdFdstatSetRightsERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE132:
	.text
.LHOTE132:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L3068
	leaq	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3068:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3069:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L3069
	cmpb	$120, %dl
	jg	.L3070
	cmpb	$99, %dl
	jg	.L3071
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L3072
	cmpb	$88, %dl
	je	.L3073
	jmp	.L3070
.L3071:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3070
	leaq	.L3075(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3075:
	.long	.L3076-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3076-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3078-.L3075
	.long	.L3077-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3076-.L3075
	.long	.L3070-.L3075
	.long	.L3076-.L3075
	.long	.L3070-.L3075
	.long	.L3070-.L3075
	.long	.L3074-.L3075
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3072:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3079
	call	_ZdlPv@PLT
.L3079:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L3099
	jmp	.L3081
.L3070:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3104
	call	_ZdlPv@PLT
	jmp	.L3104
.L3076:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3101
.L3078:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L3086:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3086
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3101
.L3074:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3101:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3098
	jmp	.L3085
.L3073:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L3089
	call	_ZdlPv@PLT
.L3089:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3085
.L3098:
	call	_ZdlPv@PLT
	jmp	.L3085
.L3077:
	leaq	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3085:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3104:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3081
.L3099:
	call	_ZdlPv@PLT
.L3081:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3067
	call	_ZdlPv@PLT
.L3067:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3093
	call	__stack_chk_fail@PLT
.L3093:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10502:
	.size	_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10399:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L3106
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3106:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3107:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L3107
	cmpb	$120, %dl
	jg	.L3108
	cmpb	$99, %dl
	jg	.L3109
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L3110
	cmpb	$88, %dl
	je	.L3111
	jmp	.L3108
.L3109:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3108
	leaq	.L3113(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3113:
	.long	.L3114-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3114-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3116-.L3113
	.long	.L3115-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3114-.L3113
	.long	.L3108-.L3113
	.long	.L3114-.L3113
	.long	.L3108-.L3113
	.long	.L3108-.L3113
	.long	.L3112-.L3113
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3110:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	-200(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r13, %rdx
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r8
	movq	-144(%rbp), %rdi
	popq	%r9
	cmpq	-216(%rbp), %rdi
	jne	.L3144
	jmp	.L3142
.L3108:
	pushq	%rax
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L3142
.L3144:
	call	_ZdlPv@PLT
	jmp	.L3142
.L3114:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3139
.L3116:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L3124:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3124
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3139
.L3112:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3139:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3136
	jmp	.L3123
.L3111:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3127
	call	_ZdlPv@PLT
.L3127:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L3123
.L3136:
	call	_ZdlPv@PLT
	jmp	.L3123
.L3115:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3123:
	movq	-184(%rbp), %rsi
	leaq	-112(%rbp), %r13
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %r9
	movq	%r13, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3142:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3119
	call	_ZdlPv@PLT
.L3119:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3105
	call	_ZdlPv@PLT
.L3105:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3131
	call	__stack_chk_fail@PLT
.L3131:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10399:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3146
	call	__stack_chk_fail@PLT
.L3146:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10263:
	.size	_ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_:
.LFB9976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L3148
	call	_ZdlPv@PLT
.L3148:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3150
	call	__stack_chk_fail@PLT
.L3150:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9976:
	.size	_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"uvwasi_fd_pread(%d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB134:
	.text
.LHOTB134:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 16(%rdi)
	je	.L3153
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L3152:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3210
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3153:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3208
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L3211
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L3158
.L3217:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3159:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3207
	cmpl	$1, 16(%rbx)
	jg	.L3161
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3162:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L3163
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3164:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3208
	cmpl	$2, 16(%rbx)
	jle	.L3212
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3167:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L3168
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3169:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3207
	cmpl	$3, 16(%rbx)
	jg	.L3171
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3172:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$4, 16(%rbx)
	movq	%rax, -64(%rbp)
	jg	.L3173
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3174:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3208
	cmpl	$4, 16(%rbx)
	jg	.L3176
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3177:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movl	%eax, -72(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3213
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3196
	cmpw	$1040, %cx
	jne	.L3179
.L3196:
	movq	23(%rdx), %r13
.L3181:
	testq	%r13, %r13
	je	.L3152
	cmpq	$0, 112(%r13)
	je	.L3214
	movq	16(%r13), %rax
	cmpb	$0, 2259(%rax)
	jne	.L3203
.L3184:
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L3192
	movl	-76(%rbp), %eax
	movl	-80(%rbp), %edi
	movq	-48(%rbp), %rsi
	leal	0(,%rax,8), %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L3209
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L3209
	movl	-76(%rbp), %edx
	testq	%rdx, %rdx
	je	.L3188
	salq	$4, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r12), %rdx
	.p2align 4,,10
	.p2align 3
.L3190:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L3190
	movl	-80(%rbp), %edx
	movl	-76(%rbp), %r8d
	movq	%r14, %rcx
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_iovec_t@PLT
	testw	%ax, %ax
	jne	.L3215
.L3193:
	movq	-64(%rbp), %r8
	movl	-76(%rbp), %ecx
	leaq	40(%r13), %rdi
	leaq	-68(%rbp), %r9
	movl	-84(%rbp), %esi
	movq	%r14, %rdx
	call	uvwasi_fd_pread@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L3216
.L3191:
	movq	(%rbx), %rdx
	movzwl	%r12w, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	testq	%r14, %r14
	je	.L3152
.L3205:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3211:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jle	.L3217
.L3158:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3159
	.p2align 4,,10
	.p2align 3
.L3208:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3207:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3163:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3164
	.p2align 4,,10
	.p2align 3
.L3161:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3162
	.p2align 4,,10
	.p2align 3
.L3212:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3168:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3173:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3171:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3176:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3177
.L3188:
	movl	-80(%rbp), %edx
	movl	-76(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_iovec_t@PLT
	testw	%ax, %ax
	je	.L3193
	.p2align 4,,10
	.p2align 3
.L3192:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3152
	.p2align 4,,10
	.p2align 3
.L3209:
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3152
.L3213:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3179:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L3181
.L3214:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3152
.L3215:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3205
.L3216:
	movl	-72(%rbp), %esi
	movl	-68(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L3191
.L3210:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7781:
.L3203:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	pushq	%rax
	leaq	-72(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-80(%rbp), %rcx
	pushq	%rax
	leaq	-84(%rbp), %rdx
	leaq	-64(%rbp), %r9
	leaq	-76(%rbp), %r8
	leaq	.LC133(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_
	popq	%rdx
	popq	%rcx
	jmp	.L3184
	.cfi_endproc
.LFE7781:
	.text
	.size	_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI7FdPreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE134:
	.text
.LHOTE134:
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"uvwasi_fd_readdir(%d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB136:
	.text
.LHOTB136:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7786:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 16(%rdi)
	je	.L3219
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L3218:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3266
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3219:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3265
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L3267
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L3224
.L3271:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3225:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3264
	cmpl	$1, 16(%rbx)
	jg	.L3227
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3228:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L3229
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3230:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3264
	cmpl	$2, 16(%rbx)
	jle	.L3268
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3233:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L3234
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3235:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3265
	cmpl	$3, 16(%rbx)
	jg	.L3237
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3238:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$4, 16(%rbx)
	movq	%rax, -64(%rbp)
	jg	.L3239
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3240:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3265
	cmpl	$4, 16(%rbx)
	jg	.L3242
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3243:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movl	%eax, -72(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3269
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3256
	cmpw	$1040, %cx
	jne	.L3245
.L3256:
	movq	23(%rdx), %r13
.L3247:
	testq	%r13, %r13
	je	.L3218
	cmpq	$0, 112(%r13)
	je	.L3270
	movq	16(%r13), %rax
	cmpb	$0, 2259(%rax)
	jne	.L3260
.L3249:
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L3250
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3267:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jle	.L3271
.L3224:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3225
	.p2align 4,,10
	.p2align 3
.L3265:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3264:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3229:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3230
	.p2align 4,,10
	.p2align 3
.L3227:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3228
	.p2align 4,,10
	.p2align 3
.L3268:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3233
	.p2align 4,,10
	.p2align 3
.L3234:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3235
	.p2align 4,,10
	.p2align 3
.L3239:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3240
	.p2align 4,,10
	.p2align 3
.L3237:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3238
	.p2align 4,,10
	.p2align 3
.L3242:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3250:
	movl	-76(%rbp), %edx
	movl	-80(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L3262
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L3262
	movq	-64(%rbp), %r8
	movl	-76(%rbp), %ecx
	leaq	40(%r13), %rdi
	leaq	-68(%rbp), %r9
	movl	-84(%rbp), %esi
	movl	-80(%rbp), %edx
	addq	-56(%rbp), %rdx
	call	uvwasi_fd_readdir@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L3272
.L3253:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L3218
	.p2align 4,,10
	.p2align 3
.L3262:
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3218
.L3269:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3245:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L3247
.L3270:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3218
.L3272:
	movl	-72(%rbp), %esi
	movl	-68(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L3253
.L3266:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7786:
.L3260:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	pushq	%rax
	leaq	-72(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-80(%rbp), %rcx
	pushq	%rax
	leaq	-84(%rbp), %rdx
	leaq	-64(%rbp), %r9
	leaq	-76(%rbp), %r8
	leaq	.LC135(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_
	popq	%rdx
	popq	%rcx
	jmp	.L3249
	.cfi_endproc
.LFE7786:
	.text
	.size	_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI9FdReaddirERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE136:
	.text
.LHOTE136:
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"uvwasi_fd_pwrite(%d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB138:
	.text
.LHOTB138:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 16(%rdi)
	je	.L3274
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L3273:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3331
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3274:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3329
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L3332
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L3279
.L3338:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3280:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3328
	cmpl	$1, 16(%rbx)
	jg	.L3282
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3283:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L3284
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3285:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3329
	cmpl	$2, 16(%rbx)
	jle	.L3333
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3288:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L3289
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3290:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3328
	cmpl	$3, 16(%rbx)
	jg	.L3292
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3293:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$4, 16(%rbx)
	movq	%rax, -64(%rbp)
	jg	.L3294
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3295:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3329
	cmpl	$4, 16(%rbx)
	jg	.L3297
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3298:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movl	%eax, -72(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3334
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3317
	cmpw	$1040, %cx
	jne	.L3300
.L3317:
	movq	23(%rdx), %r13
.L3302:
	testq	%r13, %r13
	je	.L3273
	cmpq	$0, 112(%r13)
	je	.L3335
	movq	16(%r13), %rax
	cmpb	$0, 2259(%rax)
	jne	.L3324
.L3305:
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L3313
	movl	-76(%rbp), %eax
	movl	-80(%rbp), %edi
	movq	-48(%rbp), %rsi
	leal	0(,%rax,8), %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L3330
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L3330
	movl	-76(%rbp), %edx
	testq	%rdx, %rdx
	je	.L3309
	salq	$4, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r12), %rdx
	.p2align 4,,10
	.p2align 3
.L3311:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L3311
	movl	-80(%rbp), %edx
	movl	-76(%rbp), %r8d
	movq	%r14, %rcx
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_ciovec_t@PLT
	testw	%ax, %ax
	jne	.L3336
.L3314:
	movq	-64(%rbp), %r8
	movl	-76(%rbp), %ecx
	leaq	40(%r13), %rdi
	leaq	-68(%rbp), %r9
	movl	-84(%rbp), %esi
	movq	%r14, %rdx
	call	uvwasi_fd_pwrite@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L3337
.L3312:
	movq	(%rbx), %rdx
	movzwl	%r12w, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	testq	%r14, %r14
	je	.L3273
.L3326:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L3273
	.p2align 4,,10
	.p2align 3
.L3332:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -84(%rbp)
	jle	.L3338
.L3279:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3280
	.p2align 4,,10
	.p2align 3
.L3329:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3273
	.p2align 4,,10
	.p2align 3
.L3328:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3273
	.p2align 4,,10
	.p2align 3
.L3284:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3285
	.p2align 4,,10
	.p2align 3
.L3282:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3333:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3289:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3290
	.p2align 4,,10
	.p2align 3
.L3294:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3295
	.p2align 4,,10
	.p2align 3
.L3292:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3293
	.p2align 4,,10
	.p2align 3
.L3297:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L3298
.L3309:
	movl	-80(%rbp), %edx
	movl	-76(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_ciovec_t@PLT
	testw	%ax, %ax
	je	.L3314
	.p2align 4,,10
	.p2align 3
.L3313:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3273
	.p2align 4,,10
	.p2align 3
.L3330:
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3273
.L3334:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3300:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L3302
.L3335:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3273
.L3336:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3326
.L3337:
	movl	-72(%rbp), %esi
	movl	-68(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L3312
.L3331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7784:
.L3324:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	pushq	%rax
	leaq	-72(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-80(%rbp), %rcx
	pushq	%rax
	leaq	-84(%rbp), %rdx
	leaq	-64(%rbp), %r9
	leaq	-76(%rbp), %r8
	leaq	.LC137(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_RmS1_EEEvP8_IO_FILEPKcDpOT_
	popq	%rdx
	popq	%rcx
	jmp	.L3305
	.cfi_endproc
.LFE7784:
	.text
	.size	_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI8FdPwriteERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE138:
	.text
.LHOTE138:
	.section	.text.unlikely._ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10520:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L3340
	leaq	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3340:
	movq	%rax, %r9
	leaq	-160(%rbp), %r12
	leaq	-144(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %r9
.L3341:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -192(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -184(%rbp)
	movb	%sil, -176(%rbp)
	call	strchr@PLT
	movb	-176(%rbp), %dl
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	-192(%rbp), %r8
	jne	.L3341
	cmpb	$120, %dl
	jg	.L3342
	cmpb	$99, %dl
	jg	.L3343
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-96(%rbp), %r15
	movq	%rax, -176(%rbp)
	leaq	-80(%rbp), %rax
	leaq	-128(%rbp), %r14
	movq	%rax, -184(%rbp)
	je	.L3344
	cmpb	$88, %dl
	je	.L3345
	jmp	.L3342
.L3343:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3342
	leaq	.L3347(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3347:
	.long	.L3348-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3348-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3350-.L3347
	.long	.L3349-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3348-.L3347
	.long	.L3342-.L3347
	.long	.L3348-.L3347
	.long	.L3342-.L3347
	.long	.L3342-.L3347
	.long	.L3346-.L3347
	.section	.text.unlikely._ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3344:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L3351
	call	_ZdlPv@PLT
.L3351:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L3371
	jmp	.L3353
.L3342:
	leaq	-96(%rbp), %r14
	movq	%r8, %rdx
	leaq	-128(%rbp), %r15
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3376
	call	_ZdlPv@PLT
	jmp	.L3376
.L3348:
	movzbl	(%r8), %r8d
	leaq	-96(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC124(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3373
.L3350:
	movb	$0, -57(%rbp)
	movzbl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L3358:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3358
	leaq	-96(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3373
.L3346:
	movb	(%r8), %sil
	leaq	-96(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3373:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3370
	jmp	.L3357
.L3345:
	movb	(%r8), %sil
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3361
	call	_ZdlPv@PLT
.L3361:
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L3357
.L3370:
	call	_ZdlPv@PLT
	jmp	.L3357
.L3349:
	leaq	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3357:
	leaq	-96(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3376:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3353
.L3371:
	call	_ZdlPv@PLT
.L3353:
	movq	-160(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L3339
	call	_ZdlPv@PLT
.L3339:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3365
	call	__stack_chk_fail@PLT
.L3365:
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10520:
	.size	_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L3378
	leaq	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3378:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L3379:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L3379
	cmpb	$120, %dl
	jg	.L3380
	cmpb	$99, %dl
	jg	.L3381
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L3382
	cmpb	$88, %dl
	je	.L3383
	jmp	.L3380
.L3381:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3380
	leaq	.L3385(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3385:
	.long	.L3386-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3386-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3388-.L3385
	.long	.L3387-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3386-.L3385
	.long	.L3380-.L3385
	.long	.L3386-.L3385
	.long	.L3380-.L3385
	.long	.L3380-.L3385
	.long	.L3384-.L3385
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3382:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3389
	call	_ZdlPv@PLT
.L3389:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L3409
	jmp	.L3391
.L3380:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3414
	call	_ZdlPv@PLT
	jmp	.L3414
.L3386:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3411
.L3388:
	movb	$0, -57(%rbp)
	movl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L3396:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3396
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3411
.L3384:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3411:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3408
	jmp	.L3395
.L3383:
	movl	(%r8), %esi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3399
	call	_ZdlPv@PLT
.L3399:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3395
.L3408:
	call	_ZdlPv@PLT
	jmp	.L3395
.L3387:
	leaq	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3395:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3414:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3391
.L3409:
	call	_ZdlPv@PLT
.L3391:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3377
	call	_ZdlPv@PLT
.L3377:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3403
	call	__stack_chk_fail@PLT
.L3403:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10418:
	.size	_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3416
	call	__stack_chk_fail@PLT
.L3416:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10290:
	.size	_ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_:
.LFB10031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3418
	call	_ZdlPv@PLT
.L3418:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3420
	call	__stack_chk_fail@PLT
.L3420:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10031:
	.size	_ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC139:
	.string	"sock_shutdown(%d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB140:
	.text
.LHOTB140:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	je	.L3423
	movabsq	$120259084288, %rsi
	movq	(%rdi), %rax
	movq	%rsi, 24(%rax)
.L3422:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3452
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3423:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3451
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L3453
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -44(%rbp)
	jg	.L3428
.L3456:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3429:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3451
	cmpl	$1, 16(%rbx)
	jg	.L3431
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3432:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movb	%al, -45(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3454
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3444
	cmpw	$1040, %cx
	jne	.L3434
.L3444:
	movq	23(%rdx), %rax
.L3436:
	testq	%rax, %rax
	je	.L3422
	cmpq	$0, 112(%rax)
	je	.L3455
	movq	16(%rax), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L3449
.L3441:
	movzbl	-45(%rbp), %edx
	movl	-44(%rbp), %esi
	leaq	40(%rax), %rdi
	call	uvwasi_sock_shutdown@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3422
	.p2align 4,,10
	.p2align 3
.L3453:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -44(%rbp)
	jle	.L3456
.L3428:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3429
	.p2align 4,,10
	.p2align 3
.L3451:
	movabsq	$120259084288, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L3422
	.p2align 4,,10
	.p2align 3
.L3431:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3432
	.p2align 4,,10
	.p2align 3
.L3454:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3434:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3436
	.p2align 4,,10
	.p2align 3
.L3455:
	movq	(%rbx), %rsi
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3445
	cmpw	$1040, %cx
	jne	.L3438
.L3445:
	movq	23(%rdx), %rax
.L3440:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3422
.L3438:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3440
.L3452:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7809:
.L3449:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	stderr(%rip), %rdi
	leaq	-45(%rbp), %rcx
	leaq	-44(%rbp), %rdx
	movq	%rax, -56(%rbp)
	leaq	.LC139(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRhEEEvP8_IO_FILEPKcDpOT_
	movq	-56(%rbp), %rax
	jmp	.L3441
	.cfi_endproc
.LFE7809:
	.text
	.size	_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI12SockShutdownERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE140:
	.text
.LHOTE140:
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10552:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L3458
	leaq	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3458:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L3459:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L3459
	cmpb	$120, %dl
	jg	.L3460
	cmpb	$99, %dl
	jg	.L3461
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L3462
	cmpb	$88, %dl
	je	.L3463
	jmp	.L3460
.L3461:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3460
	leaq	.L3465(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3465:
	.long	.L3466-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3466-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3468-.L3465
	.long	.L3467-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3466-.L3465
	.long	.L3460-.L3465
	.long	.L3466-.L3465
	.long	.L3460-.L3465
	.long	.L3460-.L3465
	.long	.L3464-.L3465
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3462:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3469
	call	_ZdlPv@PLT
.L3469:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L3489
	jmp	.L3471
.L3460:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3494
	call	_ZdlPv@PLT
	jmp	.L3494
.L3466:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3491
.L3468:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L3476:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3476
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3491
.L3464:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3491:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3488
	jmp	.L3475
.L3463:
	movq	(%r8), %rsi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3479
	call	_ZdlPv@PLT
.L3479:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3475
.L3488:
	call	_ZdlPv@PLT
	jmp	.L3475
.L3467:
	leaq	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3475:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3494:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3471
.L3489:
	call	_ZdlPv@PLT
.L3471:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3457
	call	_ZdlPv@PLT
.L3457:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3483
	call	__stack_chk_fail@PLT
.L3483:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10552:
	.size	_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L3496
	leaq	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3496:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3497:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L3497
	cmpb	$120, %dl
	jg	.L3498
	cmpb	$99, %dl
	jg	.L3499
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L3500
	cmpb	$88, %dl
	je	.L3501
	jmp	.L3498
.L3499:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3498
	leaq	.L3503(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3503:
	.long	.L3504-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3504-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3506-.L3503
	.long	.L3505-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3504-.L3503
	.long	.L3498-.L3503
	.long	.L3504-.L3503
	.long	.L3498-.L3503
	.long	.L3498-.L3503
	.long	.L3502-.L3503
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3500:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3507
	call	_ZdlPv@PLT
.L3507:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L3527
	jmp	.L3509
.L3498:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3532
	call	_ZdlPv@PLT
	jmp	.L3532
.L3504:
	movq	(%r9), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3529
.L3506:
	movb	$0, -57(%rbp)
	movq	(%r9), %rax
	leaq	-57(%rbp), %rsi
.L3514:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3514
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3529
.L3502:
	movq	(%r9), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3529:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3526
	jmp	.L3513
.L3501:
	movq	(%r9), %rsi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3517
	call	_ZdlPv@PLT
.L3517:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3513
.L3526:
	call	_ZdlPv@PLT
	jmp	.L3513
.L3505:
	leaq	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3513:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3532:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3509
.L3527:
	call	_ZdlPv@PLT
.L3509:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3495
	call	_ZdlPv@PLT
.L3495:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3521
	call	__stack_chk_fail@PLT
.L3521:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10497:
	.size	_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L3534
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3534:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3535:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L3535
	cmpb	$120, %dl
	jg	.L3536
	cmpb	$99, %dl
	jg	.L3537
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L3538
	cmpb	$88, %dl
	je	.L3539
	jmp	.L3536
.L3537:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3536
	leaq	.L3541(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3541:
	.long	.L3542-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3542-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3544-.L3541
	.long	.L3543-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3542-.L3541
	.long	.L3536-.L3541
	.long	.L3542-.L3541
	.long	.L3536-.L3541
	.long	.L3536-.L3541
	.long	.L3540-.L3541
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3538:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3545
	call	_ZdlPv@PLT
.L3545:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L3565
	jmp	.L3547
.L3536:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3570
	call	_ZdlPv@PLT
	jmp	.L3570
.L3542:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3567
.L3544:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L3552:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3552
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3567
.L3540:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3567:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3564
	jmp	.L3551
.L3539:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L3555
	call	_ZdlPv@PLT
.L3555:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3551
.L3564:
	call	_ZdlPv@PLT
	jmp	.L3551
.L3543:
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3551:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3570:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3547
.L3565:
	call	_ZdlPv@PLT
.L3547:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3533
	call	_ZdlPv@PLT
.L3533:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3559
	call	__stack_chk_fail@PLT
.L3559:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10393:
	.size	_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10257:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3572
	call	__stack_chk_fail@PLT
.L3572:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10257:
	.size	_ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_:
.LFB9970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3574
	call	_ZdlPv@PLT
.L3574:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3576
	call	__stack_chk_fail@PLT
.L3576:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9970:
	.size	_ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC141:
	.string	"fd_advise(%d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB142:
	.text
.LHOTB142:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 16(%rdi)
	je	.L3579
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L3578:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3619
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3579:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3617
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L3620
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L3584
.L3624:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3585:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3618
	cmpl	$1, 16(%rbx)
	jg	.L3587
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3588:
	leaq	-64(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, -72(%rbp)
	jg	.L3589
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3590:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3617
	cmpl	$2, 16(%rbx)
	jle	.L3621
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3593:
	leaq	-77(%rbp), %r13
	movq	%r13, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$3, 16(%rbx)
	movq	%rax, -64(%rbp)
	jg	.L3594
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3595:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3618
	cmpl	$3, 16(%rbx)
	jg	.L3597
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3598:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r14
	movb	%al, -77(%rbp)
	leaq	8(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3622
	movq	8(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3610
	cmpw	$1040, %cx
	jne	.L3600
.L3610:
	movq	23(%rdx), %r14
.L3602:
	testq	%r14, %r14
	je	.L3578
	cmpq	$0, 112(%r14)
	je	.L3623
	movq	16(%r14), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L3615
.L3607:
	movq	-72(%rbp), %rdx
	movzbl	-77(%rbp), %r8d
	leaq	40(%r14), %rdi
	movq	-64(%rbp), %rcx
	movl	-76(%rbp), %esi
	call	uvwasi_fd_advise@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3578
	.p2align 4,,10
	.p2align 3
.L3620:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jle	.L3624
.L3584:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3585
	.p2align 4,,10
	.p2align 3
.L3617:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3578
	.p2align 4,,10
	.p2align 3
.L3618:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3578
	.p2align 4,,10
	.p2align 3
.L3589:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3590
	.p2align 4,,10
	.p2align 3
.L3587:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3588
	.p2align 4,,10
	.p2align 3
.L3621:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3593
	.p2align 4,,10
	.p2align 3
.L3594:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3595
	.p2align 4,,10
	.p2align 3
.L3597:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3598
.L3622:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3600:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L3602
.L3623:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3611
	cmpw	$1040, %si
	jne	.L3604
.L3611:
	movq	23(%rdx), %rax
.L3606:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3578
.L3604:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3606
.L3619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7771:
.L3615:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-72(%rbp), %rcx
	leaq	-76(%rbp), %rdx
	movq	%r13, %r9
	movq	%r12, %r8
	movq	stderr(%rip), %rdi
	leaq	.LC141(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRmS2_RhEEEvP8_IO_FILEPKcDpOT_
	jmp	.L3607
	.cfi_endproc
.LFE7771:
	.text
	.size	_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI8FdAdviseERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE142:
	.text
.LHOTE142:
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10556:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L3626
	leaq	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3626:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L3627:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L3627
	cmpb	$120, %dl
	jg	.L3628
	cmpb	$99, %dl
	jg	.L3629
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L3630
	cmpb	$88, %dl
	je	.L3631
	jmp	.L3628
.L3629:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3628
	leaq	.L3633(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3633:
	.long	.L3634-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3634-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3636-.L3633
	.long	.L3635-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3634-.L3633
	.long	.L3628-.L3633
	.long	.L3634-.L3633
	.long	.L3628-.L3633
	.long	.L3628-.L3633
	.long	.L3632-.L3633
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3630:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3637
	call	_ZdlPv@PLT
.L3637:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L3657
	jmp	.L3639
.L3628:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3662
	call	_ZdlPv@PLT
	jmp	.L3662
.L3634:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3659
.L3636:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L3644:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3644
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3659
.L3632:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3659:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3656
	jmp	.L3643
.L3631:
	movq	(%r8), %rsi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3647
	call	_ZdlPv@PLT
.L3647:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3643
.L3656:
	call	_ZdlPv@PLT
	jmp	.L3643
.L3635:
	leaq	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3643:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3662:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3639
.L3657:
	call	_ZdlPv@PLT
.L3639:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3625
	call	_ZdlPv@PLT
.L3625:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3651
	call	__stack_chk_fail@PLT
.L3651:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10556:
	.size	_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10501:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L3664
	leaq	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3664:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3665:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L3665
	cmpb	$120, %dl
	jg	.L3666
	cmpb	$99, %dl
	jg	.L3667
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L3668
	cmpb	$88, %dl
	je	.L3669
	jmp	.L3666
.L3667:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3666
	leaq	.L3671(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3671:
	.long	.L3672-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3672-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3674-.L3671
	.long	.L3673-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3672-.L3671
	.long	.L3666-.L3671
	.long	.L3672-.L3671
	.long	.L3666-.L3671
	.long	.L3666-.L3671
	.long	.L3670-.L3671
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3668:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3675
	call	_ZdlPv@PLT
.L3675:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L3695
	jmp	.L3677
.L3666:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3700
	call	_ZdlPv@PLT
	jmp	.L3700
.L3672:
	movq	(%r9), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3697
.L3674:
	movb	$0, -57(%rbp)
	movq	(%r9), %rax
	leaq	-57(%rbp), %rsi
.L3682:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3682
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3697
.L3670:
	movq	(%r9), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3697:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3694
	jmp	.L3681
.L3669:
	movq	(%r9), %rsi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3685
	call	_ZdlPv@PLT
.L3685:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3681
.L3694:
	call	_ZdlPv@PLT
	jmp	.L3681
.L3673:
	leaq	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3681:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3700:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3677
.L3695:
	call	_ZdlPv@PLT
.L3677:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3663
	call	_ZdlPv@PLT
.L3663:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3689
	call	__stack_chk_fail@PLT
.L3689:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10501:
	.size	_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L3702
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3702:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3703:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L3703
	cmpb	$120, %dl
	jg	.L3704
	cmpb	$99, %dl
	jg	.L3705
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L3706
	cmpb	$88, %dl
	je	.L3707
	jmp	.L3704
.L3705:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3704
	leaq	.L3709(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3709:
	.long	.L3710-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3710-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3712-.L3709
	.long	.L3711-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3710-.L3709
	.long	.L3704-.L3709
	.long	.L3710-.L3709
	.long	.L3704-.L3709
	.long	.L3704-.L3709
	.long	.L3708-.L3709
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3706:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3713
	call	_ZdlPv@PLT
.L3713:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L3733
	jmp	.L3715
.L3704:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3738
	call	_ZdlPv@PLT
	jmp	.L3738
.L3710:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3735
.L3712:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L3720:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3720
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3735
.L3708:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3735:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3732
	jmp	.L3719
.L3707:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L3723
	call	_ZdlPv@PLT
.L3723:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3719
.L3732:
	call	_ZdlPv@PLT
	jmp	.L3719
.L3711:
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3719:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3738:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3715
.L3733:
	call	_ZdlPv@PLT
.L3715:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3701
	call	_ZdlPv@PLT
.L3701:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3727
	call	__stack_chk_fail@PLT
.L3727:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10398:
	.size	_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3740
	call	__stack_chk_fail@PLT
.L3740:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10262:
	.size	_ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_:
.LFB9975:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3742
	call	_ZdlPv@PLT
.L3742:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3744
	call	__stack_chk_fail@PLT
.L3744:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9975:
	.size	_ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"fd_filestat_set_times(%d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB144:
	.text
.LHOTB144:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 16(%rdi)
	je	.L3747
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L3746:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3787
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3747:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3785
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L3788
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L3752
.L3792:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3753:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3786
	cmpl	$1, 16(%rbx)
	jg	.L3755
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3756:
	leaq	-64(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, -72(%rbp)
	jg	.L3757
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3758:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3785
	cmpl	$2, 16(%rbx)
	jle	.L3789
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3761:
	leaq	-78(%rbp), %r13
	movq	%r13, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$3, 16(%rbx)
	movq	%rax, -64(%rbp)
	jg	.L3762
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3763:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3786
	cmpl	$3, 16(%rbx)
	jg	.L3765
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3766:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r14
	movw	%ax, -78(%rbp)
	leaq	8(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3790
	movq	8(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3778
	cmpw	$1040, %cx
	jne	.L3768
.L3778:
	movq	23(%rdx), %r14
.L3770:
	testq	%r14, %r14
	je	.L3746
	cmpq	$0, 112(%r14)
	je	.L3791
	movq	16(%r14), %rdx
	cmpb	$0, 2259(%rdx)
	jne	.L3783
.L3775:
	movq	-72(%rbp), %rdx
	movzwl	-78(%rbp), %r8d
	leaq	40(%r14), %rdi
	movq	-64(%rbp), %rcx
	movl	-76(%rbp), %esi
	call	uvwasi_fd_filestat_set_times@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3746
	.p2align 4,,10
	.p2align 3
.L3788:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jle	.L3792
.L3752:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3753
	.p2align 4,,10
	.p2align 3
.L3785:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3746
	.p2align 4,,10
	.p2align 3
.L3786:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3746
	.p2align 4,,10
	.p2align 3
.L3757:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3758
	.p2align 4,,10
	.p2align 3
.L3755:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3756
	.p2align 4,,10
	.p2align 3
.L3789:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3761
	.p2align 4,,10
	.p2align 3
.L3762:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3763
	.p2align 4,,10
	.p2align 3
.L3765:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3766
.L3790:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3768:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L3770
.L3791:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3779
	cmpw	$1040, %si
	jne	.L3772
.L3779:
	movq	23(%rdx), %rax
.L3774:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3746
.L3772:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3774
.L3787:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7780:
.L3783:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-72(%rbp), %rcx
	leaq	-76(%rbp), %rdx
	movq	%r13, %r9
	movq	%r12, %r8
	movq	stderr(%rip), %rdi
	leaq	.LC143(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRmS2_RtEEEvP8_IO_FILEPKcDpOT_
	jmp	.L3775
	.cfi_endproc
.LFE7780:
	.text
	.size	_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI18FdFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE144:
	.text
.LHOTE144:
	.section	.text.unlikely._ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10560:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L3794
	leaq	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3794:
	movq	%rax, %r9
	leaq	-160(%rbp), %r12
	leaq	-144(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %r9
.L3795:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -192(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -184(%rbp)
	movb	%sil, -176(%rbp)
	call	strchr@PLT
	movb	-176(%rbp), %dl
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	-192(%rbp), %r8
	jne	.L3795
	cmpb	$120, %dl
	jg	.L3796
	cmpb	$99, %dl
	jg	.L3797
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-96(%rbp), %r15
	movq	%rax, -176(%rbp)
	leaq	-80(%rbp), %rax
	leaq	-128(%rbp), %r10
	movq	%rax, -184(%rbp)
	je	.L3798
	cmpb	$88, %dl
	je	.L3799
	jmp	.L3796
.L3797:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3796
	leaq	.L3801(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3801:
	.long	.L3802-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3802-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3804-.L3801
	.long	.L3803-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3802-.L3801
	.long	.L3796-.L3801
	.long	.L3802-.L3801
	.long	.L3796-.L3801
	.long	.L3796-.L3801
	.long	.L3800-.L3801
	.section	.text.unlikely._ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3798:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L3805
	call	_ZdlPv@PLT
.L3805:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L3825
	jmp	.L3807
.L3796:
	leaq	-96(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3830
	call	_ZdlPv@PLT
	jmp	.L3830
.L3802:
	movzbl	(%r8), %r8d
	leaq	-96(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC124(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3827
.L3804:
	movb	$0, -57(%rbp)
	movzbl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L3812:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3812
	leaq	-96(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3827
.L3800:
	movb	(%r8), %sil
	leaq	-96(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3827:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3824
	jmp	.L3811
.L3799:
	movb	(%r8), %sil
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EhLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-192(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3815
	call	_ZdlPv@PLT
.L3815:
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L3811
.L3824:
	call	_ZdlPv@PLT
	jmp	.L3811
.L3803:
	leaq	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3811:
	leaq	-96(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-176(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3830:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3807
.L3825:
	call	_ZdlPv@PLT
.L3807:
	movq	-160(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L3793
	call	_ZdlPv@PLT
.L3793:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3819
	call	__stack_chk_fail@PLT
.L3819:
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10560:
	.size	_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.rodata._ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC145:
	.string	"%ld"
	.section	.text.unlikely._ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10509:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L3832
	leaq	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3832:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3833:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L3833
	cmpb	$120, %dl
	jg	.L3834
	cmpb	$99, %dl
	jg	.L3835
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L3836
	cmpb	$88, %dl
	je	.L3837
	jmp	.L3834
.L3835:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3834
	leaq	.L3839(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3839:
	.long	.L3840-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3840-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3842-.L3839
	.long	.L3841-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3840-.L3839
	.long	.L3834-.L3839
	.long	.L3840-.L3839
	.long	.L3834-.L3839
	.long	.L3834-.L3839
	.long	.L3838-.L3839
	.section	.text.unlikely._ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3836:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3843
	call	_ZdlPv@PLT
.L3843:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L3863
	jmp	.L3845
.L3834:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3868
	call	_ZdlPv@PLT
	jmp	.L3868
.L3840:
	movq	(%r9), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC145(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3865
.L3842:
	movb	$0, -57(%rbp)
	movq	(%r9), %rax
	leaq	-57(%rbp), %rsi
.L3850:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3850
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3865
.L3838:
	movq	(%r9), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3865:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3862
	jmp	.L3849
.L3837:
	movq	(%r9), %rsi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3853
	call	_ZdlPv@PLT
.L3853:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L3849
.L3862:
	call	_ZdlPv@PLT
	jmp	.L3849
.L3841:
	leaq	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3849:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3868:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3845
.L3863:
	call	_ZdlPv@PLT
.L3845:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L3831
	call	_ZdlPv@PLT
.L3831:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3857
	call	__stack_chk_fail@PLT
.L3857:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10509:
	.size	_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10406:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L3870
	leaq	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3870:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3871:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L3871
	cmpb	$120, %dl
	jg	.L3872
	cmpb	$99, %dl
	jg	.L3873
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L3874
	cmpb	$88, %dl
	je	.L3875
	jmp	.L3872
.L3873:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3872
	leaq	.L3877(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3877:
	.long	.L3878-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3878-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3880-.L3877
	.long	.L3879-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3878-.L3877
	.long	.L3872-.L3877
	.long	.L3878-.L3877
	.long	.L3872-.L3877
	.long	.L3872-.L3877
	.long	.L3876-.L3877
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3874:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3881
	call	_ZdlPv@PLT
.L3881:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L3901
	jmp	.L3883
.L3872:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3906
	call	_ZdlPv@PLT
	jmp	.L3906
.L3878:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3903
.L3880:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L3888:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3888
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3903
.L3876:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3903:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3900
	jmp	.L3887
.L3875:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L3891
	call	_ZdlPv@PLT
.L3891:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3887
.L3900:
	call	_ZdlPv@PLT
	jmp	.L3887
.L3879:
	leaq	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3887:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L3906:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3883
.L3901:
	call	_ZdlPv@PLT
.L3883:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L3869
	call	_ZdlPv@PLT
.L3869:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3895
	call	__stack_chk_fail@PLT
.L3895:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10406:
	.size	_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3908
	call	__stack_chk_fail@PLT
.L3908:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10274:
	.size	_ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_:
.LFB10001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRjRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3910
	call	_ZdlPv@PLT
.L3910:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3912
	call	__stack_chk_fail@PLT
.L3912:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10001:
	.size	_ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.1
.LC146:
	.string	"fd_seek(%d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB147:
	.text
.LHOTB147:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$4, 16(%rdi)
	je	.L3915
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L3914:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3958
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3915:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3956
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L3959
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L3920
.L3963:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3921:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L3957
	cmpl	$1, 16(%rbx)
	jg	.L3923
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3924:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt10Int64ValueEPb@PLT
	cmpl	$2, 16(%rbx)
	movq	%rax, -72(%rbp)
	jg	.L3925
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3926:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3957
	cmpl	$2, 16(%rbx)
	jle	.L3960
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L3929:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movb	%al, -81(%rbp)
	jg	.L3930
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3931:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L3956
	cmpl	$3, 16(%rbx)
	jg	.L3933
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L3934:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movl	%eax, -76(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L3961
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3949
	cmpw	$1040, %cx
	jne	.L3936
.L3949:
	movq	23(%rdx), %r13
.L3938:
	testq	%r13, %r13
	je	.L3914
	cmpq	$0, 112(%r13)
	je	.L3962
	movq	16(%r13), %rax
	cmpb	$0, 2259(%rax)
	jne	.L3954
.L3943:
	leaq	-56(%rbp), %rdx
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L3944
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3959:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -80(%rbp)
	jle	.L3963
.L3920:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3921
	.p2align 4,,10
	.p2align 3
.L3956:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3957:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L3914
	.p2align 4,,10
	.p2align 3
.L3925:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L3926
	.p2align 4,,10
	.p2align 3
.L3923:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L3924
	.p2align 4,,10
	.p2align 3
.L3960:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3930:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3931
	.p2align 4,,10
	.p2align 3
.L3933:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L3934
	.p2align 4,,10
	.p2align 3
.L3944:
	movl	-76(%rbp), %edi
	movq	-56(%rbp), %rsi
	movl	$8, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L3945
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L3914
.L3945:
	movzbl	-81(%rbp), %ecx
	movq	-72(%rbp), %rdx
	movq	%r12, %r8
	leaq	40(%r13), %rdi
	movl	-80(%rbp), %esi
	call	uvwasi_fd_seek@PLT
	movzwl	%ax, %r12d
	testw	%r12w, %r12w
	je	.L3964
.L3946:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
	jmp	.L3914
.L3961:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3936:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L3938
.L3962:
	movq	(%rbx), %rcx
	movq	32(%rcx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L3950
	cmpw	$1040, %si
	jne	.L3940
.L3950:
	movq	23(%rdx), %rax
.L3942:
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L3914
.L3964:
	movl	-76(%rbp), %esi
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rdi
	call	uvwasi_serdes_write_filesize_t@PLT
	jmp	.L3946
.L3940:
	leaq	32(%rcx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L3942
.L3958:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7788:
.L3954:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movq	stderr(%rip), %rdi
	leaq	-72(%rbp), %rcx
	leaq	-80(%rbp), %rdx
	leaq	-76(%rbp), %r9
	leaq	-81(%rbp), %r8
	leaq	.LC146(%rip), %rsi
	call	_ZN4node7FPrintFIJRjRlRhS1_EEEvP8_IO_FILEPKcDpOT_
	jmp	.L3943
	.cfi_endproc
.LFE7788:
	.text
	.size	_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI6FdSeekERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE147:
	.text
.LHOTE147:
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10561:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L3966
	leaq	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3966:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L3967:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L3967
	cmpb	$120, %dl
	jg	.L3968
	cmpb	$99, %dl
	jg	.L3969
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L3970
	cmpb	$88, %dl
	je	.L3971
	jmp	.L3968
.L3969:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L3968
	leaq	.L3973(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L3973:
	.long	.L3974-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3974-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3976-.L3973
	.long	.L3975-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3974-.L3973
	.long	.L3968-.L3973
	.long	.L3974-.L3973
	.long	.L3968-.L3973
	.long	.L3968-.L3973
	.long	.L3972-.L3973
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L3970:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	-200(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r13, %rdx
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r8
	movq	-144(%rbp), %rdi
	popq	%r9
	cmpq	-216(%rbp), %rdi
	jne	.L4004
	jmp	.L4002
.L3968:
	pushq	%rax
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4002
.L4004:
	call	_ZdlPv@PLT
	jmp	.L4002
.L3974:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L3999
.L3976:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L3984:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L3984
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L3999
.L3972:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L3999:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L3996
	jmp	.L3983
.L3971:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3987
	call	_ZdlPv@PLT
.L3987:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L3983
.L3996:
	call	_ZdlPv@PLT
	jmp	.L3983
.L3975:
	leaq	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3983:
	movq	-184(%rbp), %rsi
	leaq	-112(%rbp), %r13
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %r9
	movq	%r13, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4002:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L3979
	call	_ZdlPv@PLT
.L3979:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L3965
	call	_ZdlPv@PLT
.L3965:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L3991
	call	__stack_chk_fail@PLT
.L3991:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10561:
	.size	_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4006
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4006:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4007:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4007
	cmpb	$120, %dl
	jg	.L4008
	cmpb	$99, %dl
	jg	.L4009
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L4010
	cmpb	$88, %dl
	je	.L4011
	jmp	.L4008
.L4009:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4008
	leaq	.L4013(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4013:
	.long	.L4014-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4014-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4016-.L4013
	.long	.L4015-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4014-.L4013
	.long	.L4008-.L4013
	.long	.L4014-.L4013
	.long	.L4008-.L4013
	.long	.L4008-.L4013
	.long	.L4012-.L4013
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4010:
	pushq	-208(%rbp)
	movq	%rbx, %r9
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	-184(%rbp), %rsi
	pushq	-200(%rbp)
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	movq	%r11, -224(%rbp)
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r9
	movq	-144(%rbp), %rdi
	popq	%r10
	cmpq	-216(%rbp), %rdi
	jne	.L4042
	jmp	.L4020
.L4008:
	pushq	-208(%rbp)
	leaq	-112(%rbp), %r11
	movq	%r15, %r8
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rsi
	popq	%r8
	cmpq	%rax, %rdi
	je	.L4020
.L4042:
	call	_ZdlPv@PLT
.L4020:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4037
	jmp	.L4019
.L4014:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4039
.L4016:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4024:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4024
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4039
.L4012:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4039:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4036
	jmp	.L4023
.L4011:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4027
	call	_ZdlPv@PLT
.L4027:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4023
.L4036:
	call	_ZdlPv@PLT
	jmp	.L4023
.L4015:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4023:
	movq	-184(%rbp), %rsi
	pushq	%rax
	leaq	-112(%rbp), %r13
	movq	%r15, %rcx
	pushq	-208(%rbp)
	movq	-200(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4019
.L4037:
	call	_ZdlPv@PLT
.L4019:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4005
	call	_ZdlPv@PLT
.L4005:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4031
	call	__stack_chk_fail@PLT
.L4031:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10510:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10407:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r11, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4044
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4044:
	movq	%rax, %r10
	leaq	-176(%rbp), %r14
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	movq	%rbx, -224(%rbp)
	movq	%r15, %rbx
	movq	%r14, -232(%rbp)
	movq	%r12, %r15
	leaq	.LC59(%rip), %rcx
	movq	%r13, %r14
	movq	%r10, %r12
.L4045:
	movq	%r12, %rax
	movq	%rcx, %rdi
	movq	%r12, -184(%rbp)
	leaq	1(%r12), %r12
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4045
	movl	%r13d, %edx
	movq	%r12, %r10
	movq	%r14, %r13
	movq	%r15, %r12
	movq	-232(%rbp), %r14
	movq	%rbx, %r15
	movq	-224(%rbp), %rbx
	cmpb	$120, %dl
	jg	.L4046
	cmpb	$99, %dl
	jg	.L4047
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	leaq	-144(%rbp), %r11
	je	.L4048
	cmpb	$88, %dl
	je	.L4049
	jmp	.L4046
.L4047:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4046
	leaq	.L4051(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4051:
	.long	.L4052-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4052-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4054-.L4051
	.long	.L4053-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4052-.L4051
	.long	.L4046-.L4051
	.long	.L4052-.L4051
	.long	.L4046-.L4051
	.long	.L4046-.L4051
	.long	.L4050-.L4051
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4048:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%rax, %rdi
	pushq	-216(%rbp)
	movq	-192(%rbp), %rcx
	movq	%r15, %r8
	movq	%r12, %rdx
	pushq	-208(%rbp)
	addq	$2, %rsi
	pushq	-200(%rbp)
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movq	%r14, %rsi
	addq	$32, %rsp
	movl	$37, %edx
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r11, %rsi
	jmp	.L4083
.L4046:
	pushq	%rsi
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r12, %rdx
	pushq	-216(%rbp)
	movq	%r11, %rdi
	movq	%r10, %rsi
	movq	%rbx, %r9
	pushq	-208(%rbp)
	movq	%r15, %r8
	leaq	-144(%rbp), %r12
	pushq	-200(%rbp)
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r12, %rsi
	movq	%r11, %rdx
.L4083:
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4058
	call	_ZdlPv@PLT
.L4058:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4075
	jmp	.L4057
.L4052:
	movl	(%r12), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	jmp	.L4079
.L4054:
	movb	$0, -57(%rbp)
	movl	(%r12), %eax
	leaq	-57(%rbp), %rsi
.L4062:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4062
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4077
.L4050:
	movl	(%r12), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4077:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
.L4079:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4074
	jmp	.L4061
.L4049:
	movl	(%r12), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4065
	call	_ZdlPv@PLT
.L4065:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4061
.L4074:
	call	_ZdlPv@PLT
	jmp	.L4061
.L4053:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4061:
	pushq	-216(%rbp)
	leaq	-112(%rbp), %r12
	movq	%r15, %rcx
	movq	%rbx, %r8
	movq	-184(%rbp), %rsi
	pushq	-208(%rbp)
	movq	%r12, %rdi
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4057
.L4075:
	call	_ZdlPv@PLT
.L4057:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4043
	call	_ZdlPv@PLT
.L4043:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4069
	call	__stack_chk_fail@PLT
.L4069:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10407:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4085
	call	__stack_chk_fail@PLT
.L4085:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10275:
	.size	_ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_:
.LFB10002:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	addq	$32, %rsp
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4087
	call	_ZdlPv@PLT
.L4087:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4089
	call	__stack_chk_fail@PLT
.L4089:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10002:
	.size	_ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC148:
	.string	"path_filestat_set_times(%d, %d, %d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB149:
	.text
.LHOTB149:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7794:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$7, 16(%rdi)
	je	.L4092
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L4091:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4148
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4092:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4146
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jle	.L4149
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -88(%rbp)
	jg	.L4097
.L4153:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4098:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4147
	cmpl	$1, 16(%rbx)
	jg	.L4100
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4101:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L4102
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4103:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4147
	cmpl	$2, 16(%rbx)
	jle	.L4150
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L4106:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -80(%rbp)
	jg	.L4107
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4108:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4146
	cmpl	$3, 16(%rbx)
	jg	.L4110
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4111:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L4112
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4113:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L4147
	cmpl	$4, 16(%rbx)
	jg	.L4115
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4116:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$5, 16(%rbx)
	movq	%rax, -72(%rbp)
	jg	.L4117
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4118:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L4146
	cmpl	$5, 16(%rbx)
	jg	.L4120
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4121:
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$6, 16(%rbx)
	movq	%rax, -64(%rbp)
	jg	.L4122
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4123:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4147
	cmpl	$6, 16(%rbx)
	jg	.L4125
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4126:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movw	%ax, -90(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L4151
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4137
	cmpw	$1040, %cx
	jne	.L4128
.L4137:
	movq	23(%rdx), %r13
.L4130:
	testq	%r13, %r13
	je	.L4091
	cmpq	$0, 112(%r13)
	je	.L4152
	movq	16(%r13), %rax
	cmpb	$0, 2259(%rax)
	jne	.L4141
.L4132:
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L4133
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L4091
	.p2align 4,,10
	.p2align 3
.L4149:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -88(%rbp)
	jle	.L4153
.L4097:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4098
	.p2align 4,,10
	.p2align 3
.L4146:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L4091
	.p2align 4,,10
	.p2align 3
.L4147:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L4091
	.p2align 4,,10
	.p2align 3
.L4102:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L4103
	.p2align 4,,10
	.p2align 3
.L4100:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4101
	.p2align 4,,10
	.p2align 3
.L4150:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4106
	.p2align 4,,10
	.p2align 3
.L4107:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4108
	.p2align 4,,10
	.p2align 3
.L4112:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4113
	.p2align 4,,10
	.p2align 3
.L4110:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4111
	.p2align 4,,10
	.p2align 3
.L4117:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L4118
	.p2align 4,,10
	.p2align 3
.L4115:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4116
	.p2align 4,,10
	.p2align 3
.L4122:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L4123
	.p2align 4,,10
	.p2align 3
.L4120:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L4121
.L4125:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L4126
.L4133:
	movl	-76(%rbp), %edx
	movl	-80(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	jne	.L4134
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L4091
.L4134:
	movzwl	-90(%rbp), %eax
	movl	-84(%rbp), %edx
	leaq	40(%r13), %rdi
	movq	-72(%rbp), %r9
	movl	-76(%rbp), %r8d
	movl	-88(%rbp), %esi
	movl	-80(%rbp), %ecx
	pushq	%rax
	pushq	-64(%rbp)
	addq	-56(%rbp), %rcx
	call	uvwasi_path_filestat_set_times@PLT
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	popq	%rax
	popq	%rdx
	jmp	.L4091
.L4151:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4128:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L4130
.L4152:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L4091
.L4148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7794:
.L4141:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	-90(%rbp), %rax
	pushq	%rsi
	movq	stderr(%rip), %rdi
	leaq	-84(%rbp), %rcx
	pushq	%rax
	leaq	-64(%rbp), %rax
	leaq	-88(%rbp), %rdx
	pushq	%rax
	leaq	-72(%rbp), %rax
	leaq	-76(%rbp), %r9
	pushq	%rax
	leaq	-80(%rbp), %r8
	leaq	.LC148(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_S1_RmS2_RtEEEvP8_IO_FILEPKcDpOT_
	addq	$32, %rsp
	jmp	.L4132
	.cfi_endproc
.LFE7794:
	.text
	.size	_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI20PathFilestatSetTimesERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE149:
	.text
.LHOTE149:
	.section	.text.unlikely._ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$168, %rsp
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r9
	testq	%rax, %rax
	jne	.L4155
	leaq	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4155:
	movq	%rax, %r10
	leaq	-160(%rbp), %r12
	leaq	-144(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -184(%rbp)
	movq	%r10, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4156:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -192(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -184(%rbp)
	movb	%sil, -176(%rbp)
	call	strchr@PLT
	movb	-176(%rbp), %dl
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-192(%rbp), %r9
	jne	.L4156
	cmpb	$120, %dl
	jg	.L4157
	cmpb	$99, %dl
	jg	.L4158
	leaq	-112(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-96(%rbp), %rax
	movq	%rcx, -176(%rbp)
	leaq	-80(%rbp), %rcx
	leaq	-128(%rbp), %r11
	movq	%rcx, -184(%rbp)
	je	.L4159
	cmpb	$88, %dl
	je	.L4160
	jmp	.L4157
.L4158:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4157
	leaq	.L4162(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4162:
	.long	.L4163-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4163-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4165-.L4162
	.long	.L4164-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4163-.L4162
	.long	.L4157-.L4162
	.long	.L4163-.L4162
	.long	.L4157-.L4162
	.long	.L4157-.L4162
	.long	.L4161-.L4162
	.section	.text.unlikely._ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4159:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -200(%rbp)
	movq	%r11, -192(%rbp)
	call	_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-200(%rbp), %rax
	movq	-192(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L4166
	call	_ZdlPv@PLT
.L4166:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L4186
	jmp	.L4168
.L4157:
	leaq	-96(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4191
	call	_ZdlPv@PLT
	jmp	.L4191
.L4163:
	movzwl	(%r9), %r8d
	leaq	-96(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC124(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4188
.L4165:
	movb	$0, -57(%rbp)
	movzwl	(%r9), %eax
	leaq	-57(%rbp), %rsi
.L4173:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4173
	leaq	-96(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4188
.L4161:
	movw	(%r9), %si
	leaq	-96(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4188:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4185
	jmp	.L4172
.L4160:
	movw	(%r9), %si
	movq	%r11, %rdi
	movq	%rax, -200(%rbp)
	movq	%r11, -192(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-192(%rbp), %r11
	movq	-200(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4176
	call	_ZdlPv@PLT
.L4176:
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L4172
.L4185:
	call	_ZdlPv@PLT
	jmp	.L4172
.L4164:
	leaq	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4172:
	leaq	-96(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-176(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4191:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4168
.L4186:
	call	_ZdlPv@PLT
.L4168:
	movq	-160(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L4154
	call	_ZdlPv@PLT
.L4154:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4180
	call	__stack_chk_fail@PLT
.L4180:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10593:
	.size	_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4193
	leaq	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4193:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4194:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4194
	cmpb	$120, %dl
	jg	.L4195
	cmpb	$99, %dl
	jg	.L4196
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L4197
	cmpb	$88, %dl
	je	.L4198
	jmp	.L4195
.L4196:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4195
	leaq	.L4200(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4200:
	.long	.L4201-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4201-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4203-.L4200
	.long	.L4202-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4201-.L4200
	.long	.L4195-.L4200
	.long	.L4201-.L4200
	.long	.L4195-.L4200
	.long	.L4195-.L4200
	.long	.L4199-.L4200
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4197:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4204
	call	_ZdlPv@PLT
.L4204:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L4224
	jmp	.L4206
.L4195:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4229
	call	_ZdlPv@PLT
	jmp	.L4229
.L4201:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4226
.L4203:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4211:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4211
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4226
.L4199:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4226:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4223
	jmp	.L4210
.L4198:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4214
	call	_ZdlPv@PLT
.L4214:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4210
.L4223:
	call	_ZdlPv@PLT
	jmp	.L4210
.L4202:
	leaq	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4210:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4229:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4206
.L4224:
	call	_ZdlPv@PLT
.L4206:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L4192
	call	_ZdlPv@PLT
.L4192:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4218
	call	__stack_chk_fail@PLT
.L4218:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10563:
	.size	_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10518:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4231
	leaq	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4231:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4232:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4232
	cmpb	$120, %dl
	jg	.L4233
	cmpb	$99, %dl
	jg	.L4234
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L4235
	cmpb	$88, %dl
	je	.L4236
	jmp	.L4233
.L4234:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4233
	leaq	.L4238(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4238:
	.long	.L4239-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4239-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4241-.L4238
	.long	.L4240-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4239-.L4238
	.long	.L4233-.L4238
	.long	.L4239-.L4238
	.long	.L4233-.L4238
	.long	.L4233-.L4238
	.long	.L4237-.L4238
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4235:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	-200(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r13, %rdx
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r8
	movq	-144(%rbp), %rdi
	popq	%r9
	cmpq	-216(%rbp), %rdi
	jne	.L4269
	jmp	.L4267
.L4233:
	pushq	%rax
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4267
.L4269:
	call	_ZdlPv@PLT
	jmp	.L4267
.L4239:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4264
.L4241:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4249:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4249
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4264
.L4237:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4264:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4261
	jmp	.L4248
.L4236:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4252
	call	_ZdlPv@PLT
.L4252:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4248
.L4261:
	call	_ZdlPv@PLT
	jmp	.L4248
.L4240:
	leaq	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4248:
	movq	-184(%rbp), %rsi
	leaq	-112(%rbp), %r13
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %r9
	movq	%r13, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4267:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4244
	call	_ZdlPv@PLT
.L4244:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4230
	call	_ZdlPv@PLT
.L4230:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4256
	call	__stack_chk_fail@PLT
.L4256:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10518:
	.size	_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4271
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4271:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4272:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4272
	cmpb	$120, %dl
	jg	.L4273
	cmpb	$99, %dl
	jg	.L4274
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L4275
	cmpb	$88, %dl
	je	.L4276
	jmp	.L4273
.L4274:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4273
	leaq	.L4278(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4278:
	.long	.L4279-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4279-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4281-.L4278
	.long	.L4280-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4279-.L4278
	.long	.L4273-.L4278
	.long	.L4279-.L4278
	.long	.L4273-.L4278
	.long	.L4273-.L4278
	.long	.L4277-.L4278
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4275:
	pushq	-208(%rbp)
	movq	%rbx, %r9
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	-184(%rbp), %rsi
	pushq	-200(%rbp)
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	movq	%r11, -224(%rbp)
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r9
	movq	-144(%rbp), %rdi
	popq	%r10
	cmpq	-216(%rbp), %rdi
	jne	.L4307
	jmp	.L4285
.L4273:
	pushq	-208(%rbp)
	leaq	-112(%rbp), %r11
	movq	%r15, %r8
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rsi
	popq	%r8
	cmpq	%rax, %rdi
	je	.L4285
.L4307:
	call	_ZdlPv@PLT
.L4285:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4302
	jmp	.L4284
.L4279:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4304
.L4281:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4289:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4289
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4304
.L4277:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4304:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4301
	jmp	.L4288
.L4276:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4292
	call	_ZdlPv@PLT
.L4292:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4288
.L4301:
	call	_ZdlPv@PLT
	jmp	.L4288
.L4280:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4288:
	movq	-184(%rbp), %rsi
	pushq	%rax
	leaq	-112(%rbp), %r13
	movq	%r15, %rcx
	pushq	-208(%rbp)
	movq	-200(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4284
.L4302:
	call	_ZdlPv@PLT
.L4284:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4270
	call	_ZdlPv@PLT
.L4270:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4296
	call	__stack_chk_fail@PLT
.L4296:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10416:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4309
	call	__stack_chk_fail@PLT
.L4309:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10288:
	.size	_ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_:
.LFB10029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4311
	call	_ZdlPv@PLT
.L4311:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4313
	call	__stack_chk_fail@PLT
.L4313:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10029:
	.size	_ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"sock_recv(%d, %d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB151:
	.text
.LHOTB151:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7807:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$6, 16(%rdi)
	je	.L4316
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L4315:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4380
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4316:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4377
	movl	16(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L4381
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L4321
.L4387:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4322:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4379
	cmpl	$1, 16(%rbx)
	jg	.L4324
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4325:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L4326
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4327:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4377
	cmpl	$2, 16(%rbx)
	jle	.L4382
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L4330:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L4331
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4332:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4379
	cmpl	$3, 16(%rbx)
	jg	.L4334
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4335:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movw	%ax, -82(%rbp)
	jg	.L4336
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4337:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4377
	cmpl	$4, 16(%rbx)
	jg	.L4339
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4340:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$5, 16(%rbx)
	movl	%eax, -64(%rbp)
	jg	.L4341
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4342:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4379
	cmpl	$5, 16(%rbx)
	jg	.L4344
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4345:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movw	%ax, -80(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L4383
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4365
	cmpw	$1040, %cx
	jne	.L4347
.L4365:
	movq	23(%rdx), %r12
.L4349:
	testq	%r12, %r12
	je	.L4315
	cmpq	$0, 112(%r12)
	je	.L4384
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L4372
.L4352:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L4361
	movl	-68(%rbp), %eax
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	leal	0(,%rax,8), %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L4378
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L4378
	movzwl	-80(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L4378
	movl	-68(%rbp), %edx
	testq	%rdx, %rdx
	je	.L4357
	salq	$4, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r13), %rdx
.L4359:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L4359
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %r8d
	movq	%r14, %rcx
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_iovec_t@PLT
	testw	%ax, %ax
	jne	.L4385
.L4362:
	pushq	%rax
	leaq	-78(%rbp), %rax
	movl	-68(%rbp), %ecx
	leaq	40(%r12), %rdi
	pushq	%rax
	movzwl	-82(%rbp), %r8d
	movq	%r14, %rdx
	leaq	-60(%rbp), %r9
	movl	-76(%rbp), %esi
	call	uvwasi_sock_recv@PLT
	popq	%rdx
	popq	%rcx
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L4386
.L4360:
	movq	(%rbx), %rdx
	movzwl	%r12w, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	testq	%r14, %r14
	je	.L4315
.L4374:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L4315
	.p2align 4,,10
	.p2align 3
.L4381:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jle	.L4387
.L4321:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4322
	.p2align 4,,10
	.p2align 3
.L4377:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L4315
	.p2align 4,,10
	.p2align 3
.L4379:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L4315
	.p2align 4,,10
	.p2align 3
.L4326:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L4327
	.p2align 4,,10
	.p2align 3
.L4324:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4325
	.p2align 4,,10
	.p2align 3
.L4382:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4330
	.p2align 4,,10
	.p2align 3
.L4331:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4332
	.p2align 4,,10
	.p2align 3
.L4336:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4337
	.p2align 4,,10
	.p2align 3
.L4334:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4335
	.p2align 4,,10
	.p2align 3
.L4341:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L4342
	.p2align 4,,10
	.p2align 3
.L4339:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4340
	.p2align 4,,10
	.p2align 3
.L4344:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L4345
.L4357:
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_iovec_t@PLT
	testw	%ax, %ax
	je	.L4362
.L4361:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L4315
.L4378:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L4315
.L4383:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4347:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L4349
.L4384:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L4315
.L4380:
	call	__stack_chk_fail@PLT
.L4385:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L4374
.L4386:
	movl	-64(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	movzwl	-78(%rbp), %edx
	movzwl	-80(%rbp), %esi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_roflags_t@PLT
	jmp	.L4360
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7807:
.L4372:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	-80(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	.LC150(%rip), %rsi
	pushq	%rax
	leaq	-64(%rbp), %rax
	leaq	-72(%rbp), %rcx
	pushq	%rax
	leaq	-76(%rbp), %rdx
	leaq	-82(%rbp), %r9
	leaq	-68(%rbp), %r8
	call	_ZN4node7FPrintFIJRjS1_S1_RtS1_S2_EEEvP8_IO_FILEPKcDpOT_
	popq	%rsi
	popq	%rdi
	jmp	.L4352
	.cfi_endproc
.LFE7807:
	.text
	.size	_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI8SockRecvERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE151:
	.text
.LHOTE151:
	.section	.text.unlikely._ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-168(%rbp), %r8
	testq	%rax, %rax
	jne	.L4389
	leaq	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4389:
	movq	%rax, %r9
	leaq	-160(%rbp), %r12
	leaq	-144(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -184(%rbp)
	leaq	.LC59(%rip), %r15
	movq	%r9, -176(%rbp)
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r8
	movq	-176(%rbp), %r9
.L4390:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -192(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -184(%rbp)
	movb	%sil, -176(%rbp)
	call	strchr@PLT
	movb	-176(%rbp), %dl
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	movq	-192(%rbp), %r8
	jne	.L4390
	cmpb	$120, %dl
	jg	.L4391
	cmpb	$99, %dl
	jg	.L4392
	leaq	-112(%rbp), %rax
	cmpb	$37, %dl
	leaq	-96(%rbp), %r15
	movq	%rax, -176(%rbp)
	leaq	-80(%rbp), %rax
	leaq	-128(%rbp), %r10
	movq	%rax, -184(%rbp)
	je	.L4393
	cmpb	$88, %dl
	je	.L4394
	jmp	.L4391
.L4392:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4391
	leaq	.L4396(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4396:
	.long	.L4397-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4397-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4399-.L4396
	.long	.L4398-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4397-.L4396
	.long	.L4391-.L4396
	.long	.L4397-.L4396
	.long	.L4391-.L4396
	.long	.L4391-.L4396
	.long	.L4395-.L4396
	.section	.text.unlikely._ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4393:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L4400
	call	_ZdlPv@PLT
.L4400:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	jne	.L4420
	jmp	.L4402
.L4391:
	leaq	-96(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-128(%rbp), %r14
	call	_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4425
	call	_ZdlPv@PLT
	jmp	.L4425
.L4397:
	movzwl	(%r8), %r8d
	leaq	-96(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC124(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4422
.L4399:
	movb	$0, -57(%rbp)
	movzwl	(%r8), %eax
	leaq	-57(%rbp), %rsi
.L4407:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4407
	leaq	-96(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4422
.L4395:
	movw	(%r8), %si
	leaq	-96(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4422:
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4419
	jmp	.L4406
.L4394:
	movw	(%r8), %si
	movq	%r10, %rdi
	movq	%r10, -192(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EtLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-192(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4410
	call	_ZdlPv@PLT
.L4410:
	movq	-128(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L4406
.L4419:
	call	_ZdlPv@PLT
	jmp	.L4406
.L4398:
	leaq	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4406:
	leaq	-96(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	call	_ZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-176(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4425:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4402
.L4420:
	call	_ZdlPv@PLT
.L4402:
	movq	-160(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L4388
	call	_ZdlPv@PLT
.L4388:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4414
	call	__stack_chk_fail@PLT
.L4414:
	addq	$152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10594:
	.size	_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L4427
	leaq	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4427:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4428:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L4428
	cmpb	$120, %dl
	jg	.L4429
	cmpb	$99, %dl
	jg	.L4430
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L4431
	cmpb	$88, %dl
	je	.L4432
	jmp	.L4429
.L4430:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4429
	leaq	.L4434(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4434:
	.long	.L4435-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4435-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4437-.L4434
	.long	.L4436-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4435-.L4434
	.long	.L4429-.L4434
	.long	.L4435-.L4434
	.long	.L4429-.L4434
	.long	.L4429-.L4434
	.long	.L4433-.L4434
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4431:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L4438
	call	_ZdlPv@PLT
.L4438:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L4458
	jmp	.L4440
.L4429:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4463
	call	_ZdlPv@PLT
	jmp	.L4463
.L4435:
	movl	(%r9), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4460
.L4437:
	movb	$0, -57(%rbp)
	movl	(%r9), %eax
	leaq	-57(%rbp), %rsi
.L4445:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4445
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4460
.L4433:
	movl	(%r9), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4460:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4457
	jmp	.L4444
.L4432:
	movl	(%r9), %esi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L4448
	call	_ZdlPv@PLT
.L4448:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L4444
.L4457:
	call	_ZdlPv@PLT
	jmp	.L4444
.L4436:
	leaq	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4444:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4463:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4440
.L4458:
	call	_ZdlPv@PLT
.L4440:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4426
	call	_ZdlPv@PLT
.L4426:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4452
	call	__stack_chk_fail@PLT
.L4452:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10564:
	.size	_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10519:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4465
	leaq	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4465:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4466:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4466
	cmpb	$120, %dl
	jg	.L4467
	cmpb	$99, %dl
	jg	.L4468
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L4469
	cmpb	$88, %dl
	je	.L4470
	jmp	.L4467
.L4468:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4467
	leaq	.L4472(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4472:
	.long	.L4473-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4473-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4475-.L4472
	.long	.L4474-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4473-.L4472
	.long	.L4467-.L4472
	.long	.L4473-.L4472
	.long	.L4467-.L4472
	.long	.L4467-.L4472
	.long	.L4471-.L4472
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4469:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4476
	call	_ZdlPv@PLT
.L4476:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L4496
	jmp	.L4478
.L4467:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4501
	call	_ZdlPv@PLT
	jmp	.L4501
.L4473:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4498
.L4475:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4483:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4483
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4498
.L4471:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4498:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4495
	jmp	.L4482
.L4470:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4486
	call	_ZdlPv@PLT
.L4486:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4482
.L4495:
	call	_ZdlPv@PLT
	jmp	.L4482
.L4474:
	leaq	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4482:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4501:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4478
.L4496:
	call	_ZdlPv@PLT
.L4478:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L4464
	call	_ZdlPv@PLT
.L4464:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4490
	call	__stack_chk_fail@PLT
.L4490:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10519:
	.size	_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4503
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4503:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4504:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4504
	cmpb	$120, %dl
	jg	.L4505
	cmpb	$99, %dl
	jg	.L4506
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L4507
	cmpb	$88, %dl
	je	.L4508
	jmp	.L4505
.L4506:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4505
	leaq	.L4510(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4510:
	.long	.L4511-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4511-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4513-.L4510
	.long	.L4512-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4511-.L4510
	.long	.L4505-.L4510
	.long	.L4511-.L4510
	.long	.L4505-.L4510
	.long	.L4505-.L4510
	.long	.L4509-.L4510
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4507:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	-200(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r13, %rdx
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r8
	movq	-144(%rbp), %rdi
	popq	%r9
	cmpq	-216(%rbp), %rdi
	jne	.L4541
	jmp	.L4539
.L4505:
	pushq	%rax
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4539
.L4541:
	call	_ZdlPv@PLT
	jmp	.L4539
.L4511:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4536
.L4513:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4521:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4521
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4536
.L4509:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4536:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4533
	jmp	.L4520
.L4508:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4524
	call	_ZdlPv@PLT
.L4524:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4520
.L4533:
	call	_ZdlPv@PLT
	jmp	.L4520
.L4512:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4520:
	movq	-184(%rbp), %rsi
	leaq	-112(%rbp), %r13
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %r9
	movq	%r13, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4539:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4516
	call	_ZdlPv@PLT
.L4516:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4502
	call	_ZdlPv@PLT
.L4502:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4528
	call	__stack_chk_fail@PLT
.L4528:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10417:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4543
	call	__stack_chk_fail@PLT
.L4543:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10289:
	.size	_ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_:
.LFB10030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4545
	call	_ZdlPv@PLT
.L4545:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4547
	call	__stack_chk_fail@PLT
.L4547:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10030:
	.size	_ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"sock_send(%d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB153:
	.text
.LHOTB153:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$5, 16(%rdi)
	je	.L4550
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L4549:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4607
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4550:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4604
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L4608
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jg	.L4555
.L4614:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4556:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4604
	cmpl	$1, 16(%rbx)
	jg	.L4558
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4559:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -72(%rbp)
	jg	.L4560
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4561:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4605
	cmpl	$2, 16(%rbx)
	jle	.L4609
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L4564:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -68(%rbp)
	jg	.L4565
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4566:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4605
	cmpl	$3, 16(%rbx)
	jg	.L4568
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4569:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movw	%ax, -78(%rbp)
	jg	.L4570
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4571:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4604
	cmpl	$4, 16(%rbx)
	jg	.L4573
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4574:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r12
	movl	%eax, -64(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L4610
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4593
	cmpw	$1040, %cx
	jne	.L4576
.L4593:
	movq	23(%rdx), %r12
.L4578:
	testq	%r12, %r12
	je	.L4549
	cmpq	$0, 112(%r12)
	je	.L4611
	movq	16(%r12), %rax
	cmpb	$0, 2259(%rax)
	jne	.L4600
.L4581:
	leaq	-48(%rbp), %rdx
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	jne	.L4589
	movl	-68(%rbp), %eax
	movl	-72(%rbp), %edi
	movq	-48(%rbp), %rsi
	leal	0(,%rax,8), %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L4606
	movl	-64(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L4606
	movl	-68(%rbp), %edx
	testq	%rdx, %rdx
	je	.L4585
	salq	$4, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_Znwm@PLT
	movq	%rax, %r14
	leaq	(%rax,%r13), %rdx
	.p2align 4,,10
	.p2align 3
.L4587:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L4587
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %r8d
	movq	%r14, %rcx
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_ciovec_t@PLT
	testw	%ax, %ax
	jne	.L4612
.L4590:
	movzwl	-78(%rbp), %r8d
	movl	-68(%rbp), %ecx
	leaq	40(%r12), %rdi
	movq	%r14, %rdx
	movl	-76(%rbp), %esi
	leaq	-60(%rbp), %r9
	call	uvwasi_sock_send@PLT
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L4613
.L4588:
	movq	(%rbx), %rdx
	movzwl	%r12w, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	testq	%r14, %r14
	je	.L4549
.L4602:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L4549
	.p2align 4,,10
	.p2align 3
.L4604:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L4549
	.p2align 4,,10
	.p2align 3
.L4608:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -76(%rbp)
	jle	.L4614
.L4555:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4556
	.p2align 4,,10
	.p2align 3
.L4605:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L4549
	.p2align 4,,10
	.p2align 3
.L4560:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L4561
	.p2align 4,,10
	.p2align 3
.L4558:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4559
	.p2align 4,,10
	.p2align 3
.L4609:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4564
	.p2align 4,,10
	.p2align 3
.L4565:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4566
	.p2align 4,,10
	.p2align 3
.L4570:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4571
	.p2align 4,,10
	.p2align 3
.L4568:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4569
	.p2align 4,,10
	.p2align 3
.L4573:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4574
.L4585:
	movl	-72(%rbp), %edx
	movl	-68(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%r14d, %r14d
	movq	-48(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_readv_ciovec_t@PLT
	testw	%ax, %ax
	je	.L4590
	.p2align 4,,10
	.p2align 3
.L4589:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L4549
	.p2align 4,,10
	.p2align 3
.L4606:
	movabsq	$261993005056, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L4549
.L4610:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4576:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L4578
.L4611:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L4549
.L4612:
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L4602
.L4613:
	movl	-64(%rbp), %esi
	movl	-60(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L4588
.L4607:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7808:
.L4600:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	pushq	%rax
	leaq	-64(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-72(%rbp), %rcx
	pushq	%rax
	leaq	-76(%rbp), %rdx
	leaq	-78(%rbp), %r9
	leaq	-68(%rbp), %r8
	leaq	.LC152(%rip), %rsi
	call	_ZN4node7FPrintFIJRjS1_S1_RtS1_EEEvP8_IO_FILEPKcDpOT_
	popq	%rdx
	popq	%rcx
	jmp	.L4581
	.cfi_endproc
.LFE7808:
	.text
	.size	_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI8SockSendERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE153:
	.text
.LHOTE153:
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movl	$37, %esi
	movq	%rbx, %rdi
	subq	$184, %rsp
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L4616
	leaq	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4616:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%rbx, %rsi
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r9, -200(%rbp)
	movq	%r10, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4617:
	movq	%r10, %rbx
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movq	%r9, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r10, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	movq	-208(%rbp), %r9
	jne	.L4617
	cmpb	$120, %dl
	jg	.L4618
	cmpb	$99, %dl
	jg	.L4619
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -192(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -200(%rbp)
	je	.L4620
	cmpb	$88, %dl
	je	.L4621
	jmp	.L4618
.L4619:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4618
	leaq	.L4623(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4623:
	.long	.L4624-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4624-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4626-.L4623
	.long	.L4625-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4624-.L4623
	.long	.L4618-.L4623
	.long	.L4624-.L4623
	.long	.L4618-.L4623
	.long	.L4618-.L4623
	.long	.L4622-.L4623
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4620:
	movq	%r9, %rdx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %r11
	movq	%r13, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L4627
	call	_ZdlPv@PLT
.L4627:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L4647
	jmp	.L4629
.L4618:
	leaq	-112(%rbp), %rbx
	movq	%r14, %rcx
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r15, %r8
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4652
	call	_ZdlPv@PLT
	jmp	.L4652
.L4624:
	movq	(%r9), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC121(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4649
.L4626:
	movb	$0, -57(%rbp)
	movq	(%r9), %rax
	leaq	-57(%rbp), %rsi
.L4634:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4634
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4649
.L4622:
	movq	(%r9), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4649:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4646
	jmp	.L4633
.L4621:
	movq	(%r9), %rsi
	movq	%r11, %rdi
	movq	%rax, -216(%rbp)
	movq	%r11, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r11
	movq	-216(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L4637
	call	_ZdlPv@PLT
.L4637:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L4633
.L4646:
	call	_ZdlPv@PLT
	jmp	.L4633
.L4625:
	leaq	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4633:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4652:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4629
.L4647:
	call	_ZdlPv@PLT
.L4629:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L4615
	call	_ZdlPv@PLT
.L4615:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4641
	call	__stack_chk_fail@PLT
.L4641:
	addq	$184, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10603:
	.size	_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	%rcx, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4654
	leaq	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4654:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -200(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4655:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -216(%rbp)
	movb	%sil, -208(%rbp)
	call	strchr@PLT
	movb	-208(%rbp), %dl
	movq	-216(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4655
	cmpb	$120, %dl
	jg	.L4656
	cmpb	$99, %dl
	jg	.L4657
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -208(%rbp)
	leaq	-96(%rbp), %rcx
	leaq	-144(%rbp), %r11
	movq	%rcx, -216(%rbp)
	je	.L4658
	cmpb	$88, %dl
	je	.L4659
	jmp	.L4656
.L4657:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4656
	leaq	.L4661(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4661:
	.long	.L4662-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4662-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4664-.L4661
	.long	.L4663-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4662-.L4661
	.long	.L4656-.L4661
	.long	.L4662-.L4661
	.long	.L4656-.L4661
	.long	.L4656-.L4661
	.long	.L4660-.L4661
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4658:
	movq	-184(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rax, -192(%rbp)
	addq	$2, %rsi
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4665
	call	_ZdlPv@PLT
.L4665:
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L4685
	jmp	.L4667
.L4656:
	leaq	-112(%rbp), %r11
	movq	%r10, %rsi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-192(%rbp), %rcx
	movq	%r11, %rdi
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4690
	call	_ZdlPv@PLT
	jmp	.L4690
.L4662:
	movq	0(%r13), %r8
	leaq	-112(%rbp), %rdi
	movl	$32, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC121(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4687
.L4664:
	movb	$0, -57(%rbp)
	movq	0(%r13), %rax
	leaq	-57(%rbp), %rsi
.L4672:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4672
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4687
.L4660:
	movq	0(%r13), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4687:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4684
	jmp	.L4671
.L4659:
	movq	0(%r13), %rsi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4675
	call	_ZdlPv@PLT
.L4675:
	movq	-144(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4671
.L4684:
	call	_ZdlPv@PLT
	jmp	.L4671
.L4663:
	leaq	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4671:
	movq	-184(%rbp), %rsi
	movq	-192(%rbp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rcx
	leaq	-112(%rbp), %r13
	addq	$2, %rsi
	movq	%r13, %rdi
	call	_ZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4690:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4667
.L4685:
	call	_ZdlPv@PLT
.L4667:
	movq	-176(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L4653
	call	_ZdlPv@PLT
.L4653:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4679
	call	__stack_chk_fail@PLT
.L4679:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10602:
	.size	_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4692
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4692:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4693:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4693
	cmpb	$120, %dl
	jg	.L4694
	cmpb	$99, %dl
	jg	.L4695
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L4696
	cmpb	$88, %dl
	je	.L4697
	jmp	.L4694
.L4695:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4694
	leaq	.L4699(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4699:
	.long	.L4700-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4700-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4702-.L4699
	.long	.L4701-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4700-.L4699
	.long	.L4694-.L4699
	.long	.L4700-.L4699
	.long	.L4694-.L4699
	.long	.L4694-.L4699
	.long	.L4698-.L4699
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4696:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	-200(%rbp)
	movq	-192(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r13, %rdx
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r8
	movq	-144(%rbp), %rdi
	popq	%r9
	cmpq	-216(%rbp), %rdi
	jne	.L4730
	jmp	.L4728
.L4694:
	pushq	%rax
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	%r13, %rdx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4728
.L4730:
	call	_ZdlPv@PLT
	jmp	.L4728
.L4700:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4725
.L4702:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4710:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4710
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4725
.L4698:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4725:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4722
	jmp	.L4709
.L4697:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4713
	call	_ZdlPv@PLT
.L4713:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4709
.L4722:
	call	_ZdlPv@PLT
	jmp	.L4709
.L4701:
	leaq	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4709:
	movq	-184(%rbp), %rsi
	leaq	-112(%rbp), %r13
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	-192(%rbp), %rdx
	movq	-200(%rbp), %r9
	movq	%r13, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4728:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4705
	call	_ZdlPv@PLT
.L4705:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L4691
	call	_ZdlPv@PLT
.L4691:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4717
	call	__stack_chk_fail@PLT
.L4717:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10601:
	.size	_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%r11, %rdi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4732
	leaq	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4732:
	movq	%rax, %r10
	leaq	-176(%rbp), %r12
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r12, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	leaq	.LC59(%rip), %rcx
.L4733:
	movq	%r10, %rax
	movq	%r10, -184(%rbp)
	movq	%rcx, %rdi
	leaq	1(%r10), %r10
	movsbl	1(%rax), %esi
	movq	%r10, -224(%rbp)
	movb	%sil, -216(%rbp)
	call	strchr@PLT
	movb	-216(%rbp), %dl
	movq	-224(%rbp), %r10
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4733
	cmpb	$120, %dl
	jg	.L4734
	cmpb	$99, %dl
	jg	.L4735
	leaq	-128(%rbp), %rcx
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	movq	%rcx, -216(%rbp)
	leaq	-144(%rbp), %r11
	je	.L4736
	cmpb	$88, %dl
	je	.L4737
	jmp	.L4734
.L4735:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4734
	leaq	.L4739(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4739:
	.long	.L4740-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4740-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4742-.L4739
	.long	.L4741-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4740-.L4739
	.long	.L4734-.L4739
	.long	.L4740-.L4739
	.long	.L4734-.L4739
	.long	.L4734-.L4739
	.long	.L4738-.L4739
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4736:
	pushq	-208(%rbp)
	movq	%rbx, %r9
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	-184(%rbp), %rsi
	pushq	-200(%rbp)
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	movq	%r11, -224(%rbp)
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%r14, %rdi
	movq	%rax, %rdx
	movq	%r11, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	popq	%r9
	movq	-144(%rbp), %rdi
	popq	%r10
	cmpq	-216(%rbp), %rdi
	jne	.L4768
	jmp	.L4746
.L4734:
	pushq	-208(%rbp)
	leaq	-112(%rbp), %r11
	movq	%r15, %r8
	movq	%r10, %rsi
	pushq	-200(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r13, %rdx
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r13
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%r11, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	popq	%rsi
	popq	%r8
	cmpq	%rax, %rdi
	je	.L4746
.L4768:
	call	_ZdlPv@PLT
.L4746:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4763
	jmp	.L4745
.L4740:
	movl	0(%r13), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4765
.L4742:
	movb	$0, -57(%rbp)
	movl	0(%r13), %eax
	leaq	-57(%rbp), %rsi
.L4750:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4750
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4765
.L4738:
	movl	0(%r13), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4765:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4762
	jmp	.L4749
.L4737:
	movl	0(%r13), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4753
	call	_ZdlPv@PLT
.L4753:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L4749
.L4762:
	call	_ZdlPv@PLT
	jmp	.L4749
.L4741:
	leaq	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4749:
	movq	-184(%rbp), %rsi
	pushq	%rax
	leaq	-112(%rbp), %r13
	movq	%r15, %rcx
	pushq	-208(%rbp)
	movq	-200(%rbp), %r9
	movq	%rbx, %r8
	movq	%r13, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4745
.L4763:
	call	_ZdlPv@PLT
.L4745:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4731
	call	_ZdlPv@PLT
.L4731:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4757
	call	__stack_chk_fail@PLT
.L4757:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10592:
	.size	_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10562:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r11, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4770
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4770:
	movq	%rax, %r10
	leaq	-176(%rbp), %r14
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	movq	%rbx, -224(%rbp)
	movq	%r15, %rbx
	movq	%r14, -232(%rbp)
	movq	%r12, %r15
	leaq	.LC59(%rip), %rcx
	movq	%r13, %r14
	movq	%r10, %r12
.L4771:
	movq	%r12, %rax
	movq	%rcx, %rdi
	movq	%r12, -184(%rbp)
	leaq	1(%r12), %r12
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4771
	movl	%r13d, %edx
	movq	%r12, %r10
	movq	%r14, %r13
	movq	%r15, %r12
	movq	-232(%rbp), %r14
	movq	%rbx, %r15
	movq	-224(%rbp), %rbx
	cmpb	$120, %dl
	jg	.L4772
	cmpb	$99, %dl
	jg	.L4773
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	leaq	-144(%rbp), %r11
	je	.L4774
	cmpb	$88, %dl
	je	.L4775
	jmp	.L4772
.L4773:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4772
	leaq	.L4777(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4777:
	.long	.L4778-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4778-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4780-.L4777
	.long	.L4779-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4778-.L4777
	.long	.L4772-.L4777
	.long	.L4778-.L4777
	.long	.L4772-.L4777
	.long	.L4772-.L4777
	.long	.L4776-.L4777
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4774:
	movq	-184(%rbp), %rsi
	pushq	%rdi
	movq	%rbx, %r9
	movq	%rax, %rdi
	pushq	-216(%rbp)
	movq	-192(%rbp), %rcx
	movq	%r15, %r8
	movq	%r12, %rdx
	pushq	-208(%rbp)
	addq	$2, %rsi
	pushq	-200(%rbp)
	movq	%rax, -192(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-224(%rbp), %r11
	movq	%r14, %rsi
	addq	$32, %rsp
	movl	$37, %edx
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r11, %rsi
	jmp	.L4809
.L4772:
	pushq	%rsi
	leaq	-112(%rbp), %r11
	movq	-192(%rbp), %rcx
	movq	%r12, %rdx
	pushq	-216(%rbp)
	movq	%r11, %rdi
	movq	%r10, %rsi
	movq	%rbx, %r9
	pushq	-208(%rbp)
	movq	%r15, %r8
	leaq	-144(%rbp), %r12
	pushq	-200(%rbp)
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r12, %rsi
	movq	%r11, %rdx
.L4809:
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4784
	call	_ZdlPv@PLT
.L4784:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4801
	jmp	.L4783
.L4778:
	movl	(%r12), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	jmp	.L4805
.L4780:
	movb	$0, -57(%rbp)
	movl	(%r12), %eax
	leaq	-57(%rbp), %rsi
.L4788:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4788
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4803
.L4776:
	movl	(%r12), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4803:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
.L4805:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4800
	jmp	.L4787
.L4775:
	movl	(%r12), %esi
	movq	%r11, %rdi
	movq	%rax, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4791
	call	_ZdlPv@PLT
.L4791:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4787
.L4800:
	call	_ZdlPv@PLT
	jmp	.L4787
.L4779:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4787:
	pushq	-216(%rbp)
	leaq	-112(%rbp), %r12
	movq	%r15, %rcx
	movq	%rbx, %r8
	movq	-184(%rbp), %rsi
	pushq	-208(%rbp)
	movq	%r12, %rdi
	movq	-200(%rbp), %r9
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, %rdi
	je	.L4783
.L4801:
	call	_ZdlPv@PLT
.L4783:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4769
	call	_ZdlPv@PLT
.L4769:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4795
	call	__stack_chk_fail@PLT
.L4795:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10562:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10511:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r11
	movl	$37, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r8, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r11, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$200, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	strchr@PLT
	movq	-184(%rbp), %r11
	testq	%rax, %rax
	jne	.L4811
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4811:
	movq	%rax, %r10
	leaq	-176(%rbp), %r14
	movq	%r11, %rsi
	leaq	-160(%rbp), %rax
	movq	%r10, %rdx
	movq	%r14, %rdi
	movq	%r10, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-184(%rbp), %r10
	movq	%rbx, -232(%rbp)
	movq	%r15, %rbx
	movq	%r14, -240(%rbp)
	movq	%r12, %r15
	leaq	.LC59(%rip), %rcx
	movq	%r13, %r14
	movq	%r10, %r12
.L4812:
	movq	%r12, %rax
	movq	%rcx, %rdi
	movq	%r12, -184(%rbp)
	leaq	1(%r12), %r12
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	leaq	.LC59(%rip), %rcx
	testq	%rax, %rax
	jne	.L4812
	movl	%r13d, %edx
	movq	%r12, %r10
	movq	%r14, %r13
	movq	%r15, %r12
	movq	-240(%rbp), %r14
	movq	%rbx, %r15
	movq	-232(%rbp), %rbx
	cmpb	$120, %dl
	jg	.L4813
	cmpb	$99, %dl
	jg	.L4814
	cmpb	$37, %dl
	leaq	-112(%rbp), %rax
	leaq	-144(%rbp), %r11
	je	.L4815
	cmpb	$88, %dl
	je	.L4816
	jmp	.L4813
.L4814:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4813
	leaq	.L4818(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4818:
	.long	.L4819-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4819-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4821-.L4818
	.long	.L4820-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4819-.L4818
	.long	.L4813-.L4818
	.long	.L4819-.L4818
	.long	.L4813-.L4818
	.long	.L4813-.L4818
	.long	.L4817-.L4818
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4815:
	pushq	-224(%rbp)
	movq	%rax, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	movq	-184(%rbp), %rsi
	pushq	-216(%rbp)
	movq	%r12, %rdx
	movq	-192(%rbp), %rcx
	pushq	-208(%rbp)
	pushq	-200(%rbp)
	addq	$2, %rsi
	movq	%rax, -192(%rbp)
	movq	%r11, -232(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-232(%rbp), %r11
	movq	%r14, %rsi
	addq	$32, %rsp
	movl	$37, %edx
	movq	%r11, %rdi
	movq	%r11, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-192(%rbp), %rax
	movq	-184(%rbp), %r11
	movq	%rax, %rdx
	movq	%r11, %rsi
	jmp	.L4852
.L4813:
	pushq	-224(%rbp)
	leaq	-112(%rbp), %r11
	movq	%r12, %rdx
	movq	%r10, %rsi
	pushq	-216(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r15, %r8
	pushq	-208(%rbp)
	leaq	-144(%rbp), %r12
	movq	-192(%rbp), %rcx
	pushq	-200(%rbp)
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	addq	$32, %rsp
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r12, %rsi
	movq	%r11, %rdx
.L4852:
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4848
	call	_ZdlPv@PLT
	jmp	.L4848
.L4819:
	movl	(%r12), %r8d
	leaq	-112(%rbp), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC64(%rip), %rcx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	jmp	.L4846
.L4821:
	movb	$0, -57(%rbp)
	movl	(%r12), %eax
	leaq	-57(%rbp), %rsi
.L4829:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4829
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4844
.L4817:
	movl	(%r12), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4844:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
.L4846:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4841
	jmp	.L4828
.L4816:
	movl	(%r12), %esi
	movq	%r11, %rdi
	movq	%rax, -240(%rbp)
	movq	%r11, -232(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-232(%rbp), %r11
	movq	-240(%rbp), %rax
	movq	%r11, %rsi
	movq	%rax, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4832
	call	_ZdlPv@PLT
.L4832:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4828
.L4841:
	call	_ZdlPv@PLT
	jmp	.L4828
.L4820:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4828:
	movq	-184(%rbp), %rsi
	pushq	%rax
	leaq	-112(%rbp), %r12
	movq	%rbx, %r8
	pushq	-224(%rbp)
	movq	-192(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r15, %rcx
	pushq	-216(%rbp)
	addq	$2, %rsi
	movq	-200(%rbp), %r9
	pushq	-208(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	addq	$32, %rsp
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4848:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4824
	call	_ZdlPv@PLT
.L4824:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4810
	call	_ZdlPv@PLT
.L4810:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4836
	call	__stack_chk_fail@PLT
.L4836:
	leaq	-40(%rbp), %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10511:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB10409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movl	$37, %esi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$216, %rsp
	movq	16(%rbp), %rax
	movq	%rdx, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%rax, -200(%rbp)
	movq	24(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	32(%rbp), %rax
	movq	%rax, -216(%rbp)
	movq	40(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	48(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r10
	testq	%rax, %rax
	jne	.L4854
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4854:
	movq	%rax, %r11
	leaq	-176(%rbp), %r15
	leaq	-160(%rbp), %rax
	movq	%r13, %rsi
	movq	%r11, %rdx
	movq	%r15, %rdi
	movq	%r10, -256(%rbp)
	leaq	.LC59(%rip), %r13
	movq	%r11, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag.constprop.0
	movq	-256(%rbp), %r10
	movq	-184(%rbp), %r11
	movq	%r14, -240(%rbp)
	movq	%rbx, -248(%rbp)
	movq	%r15, -256(%rbp)
	movq	%r10, %rbx
	movq	%r12, %r15
	movq	%r11, %r12
.L4855:
	movq	%r12, %rax
	movq	%r13, %rdi
	movq	%r12, -184(%rbp)
	leaq	1(%r12), %r12
	movsbl	1(%rax), %esi
	movl	%esi, %r14d
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L4855
	movl	%r14d, %edx
	movq	%r12, %r11
	movq	%rbx, %r10
	movq	%r15, %r12
	movq	-240(%rbp), %r14
	movq	-248(%rbp), %rbx
	movq	-256(%rbp), %r15
	cmpb	$120, %dl
	jg	.L4856
	cmpb	$99, %dl
	jg	.L4857
	cmpb	$37, %dl
	leaq	-144(%rbp), %r13
	je	.L4858
	cmpb	$88, %dl
	je	.L4859
	jmp	.L4856
.L4857:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L4856
	leaq	.L4861(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L4861:
	.long	.L4862-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4862-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4864-.L4861
	.long	.L4863-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4862-.L4861
	.long	.L4856-.L4861
	.long	.L4862-.L4861
	.long	.L4856-.L4861
	.long	.L4856-.L4861
	.long	.L4860-.L4861
	.section	.text.unlikely._ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L4858:
	movq	-184(%rbp), %rsi
	pushq	%rcx
	leaq	-112(%rbp), %r11
	movq	%r10, %rdx
	pushq	-232(%rbp)
	movq	%r11, %rdi
	movq	%rbx, %r9
	movq	%r14, %r8
	pushq	-224(%rbp)
	addq	$2, %rsi
	movq	-192(%rbp), %rcx
	pushq	-216(%rbp)
	pushq	-208(%rbp)
	pushq	-200(%rbp)
	movq	%r11, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	addq	$48, %rsp
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r11
	movq	%r11, %rdx
	jmp	.L4894
.L4856:
	pushq	%rdx
	leaq	-112(%rbp), %rax
	movq	-192(%rbp), %rcx
	movq	%r10, %rdx
	pushq	-232(%rbp)
	movq	%rax, %rdi
	movq	%r11, %rsi
	movq	%rbx, %r9
	pushq	-224(%rbp)
	movq	%r14, %r8
	leaq	-144(%rbp), %r13
	pushq	-216(%rbp)
	pushq	-208(%rbp)
	pushq	-200(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	addq	$48, %rsp
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %rax
	movq	%rax, %rdx
.L4894:
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4890
	call	_ZdlPv@PLT
	jmp	.L4890
.L4862:
	movl	(%r10), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC64(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L4887
.L4864:
	movb	$0, -57(%rbp)
	movl	(%r10), %eax
	leaq	-57(%rbp), %rsi
.L4872:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L4872
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L4887
.L4860:
	movl	(%r10), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L4887:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	leaq	-176(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L4884
	jmp	.L4871
.L4859:
	movl	(%r10), %esi
	movq	%r13, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EjLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	leaq	-112(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4875
	call	_ZdlPv@PLT
.L4875:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4871
.L4884:
	call	_ZdlPv@PLT
	jmp	.L4871
.L4863:
	leaq	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4871:
	pushq	-232(%rbp)
	leaq	-112(%rbp), %r15
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	-184(%rbp), %rsi
	pushq	-224(%rbp)
	movq	%r15, %rdi
	movq	-192(%rbp), %rdx
	pushq	-216(%rbp)
	pushq	-208(%rbp)
	addq	$2, %rsi
	movq	-200(%rbp), %r9
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	addq	$32, %rsp
	movq	%r15, %rdx
	movq	%r12, %rdi
	leaq	-176(%rbp), %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L4890:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4867
	call	_ZdlPv@PLT
.L4867:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4853
	call	_ZdlPv@PLT
.L4853:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4879
	call	__stack_chk_fail@PLT
.L4879:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10409:
	.size	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB10277:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	48(%rbp)
	pushq	40(%rbp)
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4896
	call	__stack_chk_fail@PLT
.L4896:
	movq	%r12, %rax
	movq	-8(%rbp), %r12
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10277:
	.size	_ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_:
.LFB10004:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	48(%rbp)
	pushq	40(%rbp)
	pushq	32(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	_ZN4node7SPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	addq	$48, %rsp
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L4898
	call	_ZdlPv@PLT
.L4898:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L4900
	call	__stack_chk_fail@PLT
.L4900:
	leaq	-16(%rbp), %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10004:
	.size	_ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"path_open(%d, %d, %d, %d, %d, %d, %d, %d, %d)\n"
	.section	.text.unlikely
	.align 2
.LCOLDB155:
	.text
.LHOTB155:
	.align 2
	.p2align 4
	.globl	_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpl	$9, 16(%rdi)
	je	.L4903
	movabsq	$120259084288, %rcx
	movq	(%rdi), %rax
	movq	%rcx, 24(%rax)
.L4902:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4972
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4903:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4970
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L4973
	movq	8(%rbx), %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -104(%rbp)
	jg	.L4908
.L4977:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4909:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4971
	cmpl	$1, 16(%rbx)
	jg	.L4911
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4912:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, -100(%rbp)
	jg	.L4913
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4914:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4971
	cmpl	$2, 16(%rbx)
	jle	.L4974
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
.L4917:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$3, 16(%rbx)
	movl	%eax, -96(%rbp)
	jg	.L4918
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4919:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4970
	cmpl	$3, 16(%rbx)
	jg	.L4921
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4922:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%rbx)
	movl	%eax, -92(%rbp)
	jg	.L4923
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4924:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4971
	cmpl	$4, 16(%rbx)
	jg	.L4926
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4927:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$5, 16(%rbx)
	movl	%eax, -88(%rbp)
	jg	.L4928
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4929:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L4970
	cmpl	$5, 16(%rbx)
	jg	.L4931
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4932:
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$6, 16(%rbx)
	movq	%rax, -72(%rbp)
	jg	.L4933
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4934:
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L4971
	cmpl	$6, 16(%rbx)
	jg	.L4936
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4937:
	movq	%r12, %rsi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	cmpl	$7, 16(%rbx)
	movq	%rax, -64(%rbp)
	jg	.L4938
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4939:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4970
	cmpl	$7, 16(%rbx)
	jg	.L4941
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4942:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$8, 16(%rbx)
	movl	%eax, -84(%rbp)
	jg	.L4943
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4944:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L4970
	cmpl	$8, 16(%rbx)
	jg	.L4946
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L4947:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	8(%rbx), %r13
	movl	%eax, -80(%rbp)
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L4975
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L4960
	cmpw	$1040, %cx
	jne	.L4949
.L4960:
	movq	23(%rdx), %r13
.L4951:
	testq	%r13, %r13
	je	.L4902
	cmpq	$0, 112(%r13)
	je	.L4976
	movq	16(%r13), %rax
	cmpb	$0, 2259(%rax)
	jne	.L4964
.L4953:
	leaq	-56(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node4wasi4WASI12backingStoreEPPcPm
	testw	%ax, %ax
	je	.L4954
	movq	(%rbx), %rdx
	movzwl	%ax, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L4902
	.p2align 4,,10
	.p2align 3
.L4973:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$1, 16(%rbx)
	movl	%eax, -104(%rbp)
	jle	.L4977
.L4908:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4909
	.p2align 4,,10
	.p2align 3
.L4970:
	movabsq	$120259084288, %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L4902
	.p2align 4,,10
	.p2align 3
.L4971:
	movabsq	$120259084288, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L4902
	.p2align 4,,10
	.p2align 3
.L4913:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L4914
	.p2align 4,,10
	.p2align 3
.L4911:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L4912
	.p2align 4,,10
	.p2align 3
.L4974:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L4917
	.p2align 4,,10
	.p2align 3
.L4918:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4919
	.p2align 4,,10
	.p2align 3
.L4923:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4924
	.p2align 4,,10
	.p2align 3
.L4921:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L4922
	.p2align 4,,10
	.p2align 3
.L4928:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L4929
	.p2align 4,,10
	.p2align 3
.L4926:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L4927
	.p2align 4,,10
	.p2align 3
.L4933:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L4934
	.p2align 4,,10
	.p2align 3
.L4931:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L4932
.L4938:
	movq	8(%rbx), %rax
	leaq	-56(%rax), %rdi
	jmp	.L4939
.L4936:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L4937
.L4943:
	movq	8(%rbx), %rax
	leaq	-64(%rax), %rdi
	jmp	.L4944
.L4941:
	movq	8(%rbx), %rax
	leaq	-56(%rax), %rdi
	jmp	.L4942
.L4946:
	movq	8(%rbx), %rax
	leaq	-64(%rax), %rdi
	jmp	.L4947
.L4954:
	movl	-92(%rbp), %edx
	movl	-96(%rbp), %edi
	movq	-48(%rbp), %rsi
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L4966
	movl	-80(%rbp), %edi
	movq	-48(%rbp), %rsi
	movl	$4, %edx
	call	uvwasi_serdes_check_bounds@PLT
	testl	%eax, %eax
	je	.L4966
	leaq	-76(%rbp), %rax
	movzwl	-88(%rbp), %r9d
	movl	-100(%rbp), %edx
	leaq	40(%r13), %rdi
	movl	-92(%rbp), %r8d
	movl	-104(%rbp), %esi
	pushq	%rax
	movzwl	-84(%rbp), %eax
	movl	-96(%rbp), %ecx
	addq	-56(%rbp), %rcx
	pushq	%rax
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	call	uvwasi_path_open@PLT
	addq	$32, %rsp
	movl	%eax, %r12d
	testw	%ax, %ax
	je	.L4978
.L4957:
	movq	(%rbx), %rdx
	movzwl	%r12w, %eax
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L4902
.L4966:
	movabsq	$261993005056, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L4902
.L4975:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L4949:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L4951
.L4976:
	movq	(%rbx), %rdi
	addq	$32, %rdi
	call	_ZN4node11Environment19GetFromCallbackDataEN2v85LocalINS1_5ValueEEE
	movq	352(%rax), %rdi
	call	_ZN4node26THROW_ERR_WASI_NOT_STARTEDEPN2v87IsolateE
	jmp	.L4902
.L4972:
	call	__stack_chk_fail@PLT
.L4978:
	movl	-80(%rbp), %esi
	movl	-76(%rbp), %edx
	movq	-56(%rbp), %rdi
	call	uvwasi_serdes_write_size_t@PLT
	jmp	.L4957
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB7796:
.L4964:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	pushq	%rax
	leaq	-80(%rbp), %rax
	movq	stderr(%rip), %rdi
	leaq	-100(%rbp), %rcx
	pushq	%rax
	leaq	-84(%rbp), %rax
	leaq	-104(%rbp), %rdx
	pushq	%rax
	leaq	-64(%rbp), %rax
	leaq	-92(%rbp), %r9
	pushq	%rax
	leaq	-72(%rbp), %rax
	leaq	-96(%rbp), %r8
	pushq	%rax
	leaq	-88(%rbp), %rax
	leaq	.LC154(%rip), %rsi
	pushq	%rax
	call	_ZN4node7FPrintFIJRjS1_S1_S1_S1_RmS2_S1_S1_EEEvP8_IO_FILEPKcDpOT_
	addq	$48, %rsp
	jmp	.L4953
	.cfi_endproc
.LFE7796:
	.text
	.size	_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node4wasi4WASI8PathOpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE155:
	.text
.LHOTE155:
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node4wasi4WASIE
	.section	.data.rel.ro._ZTVN4node4wasi4WASIE,"awG",@progbits,_ZTVN4node4wasi4WASIE,comdat
	.align 8
	.type	_ZTVN4node4wasi4WASIE, @object
	.size	_ZTVN4node4wasi4WASIE, 112
_ZTVN4node4wasi4WASIE:
	.quad	0
	.quad	0
	.quad	_ZN4node4wasi4WASID1Ev
	.quad	_ZN4node4wasi4WASID0Ev
	.quad	_ZNK4node4wasi4WASI10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node4wasi4WASI14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node4wasi4WASI8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	-32
	.quad	0
	.quad	_ZN4node3mem18NgLibMemoryManagerINS_4wasi4WASIE12uvwasi_mem_sE18StopTrackingMemoryEPv
	.weak	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC156:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC158:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC158
	.weak	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC159:
	.string	"../src/debug_utils-inl.h:76"
.LC160:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJRjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC158
	.weak	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {long unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC161
	.weak	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJS1_RjS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC161
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC162:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {long unsigned int&, long unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC162
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC162
	.weak	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = short unsigned int&; Args = {unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC163
	.weak	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRtJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC163
	.weak	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = short unsigned int&; Args = {unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC164
	.weak	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRtJRjS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC164
	.weak	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC165:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, long unsigned int&, long unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC165
	.weak	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC165
	.weak	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC166:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {short unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC166
	.weak	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC166
	.weak	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {short unsigned int&, unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC167
	.weak	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC167
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC168:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, long unsigned int&, long unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC168
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC168
	.weak	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, long unsigned int&, long unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC169
	.weak	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC169
	.weak	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC170:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned char&; Args = {unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC170
	.weak	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRhJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC170
	.weak	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC171:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC171
	.weak	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC171
	.weak	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC172:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {unsigned char&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC172
	.weak	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC172
	.weak	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC173:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned char&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC173
	.weak	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRhJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC173
	.weak	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC174:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, short unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC174
	.weak	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC174
	.weak	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC175:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, short unsigned int&, unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC175
	.weak	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC175
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC176:
	.ascii	"std:"
	.string	":string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, unsigned int&, long unsigned int&, long unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC176
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC176
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, long unsigned int&, long unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC177
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC177
	.weak	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long int&; Args = {unsigned char&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC178
	.weak	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRlJRhRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC178
	.weak	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC179:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, long unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC179
	.weak	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC179
	.weak	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {long unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC180
	.weak	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJS1_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC180
	.weak	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC181:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC181
	.weak	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC181
	.weak	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC182:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = short unsigned int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC182
	.weak	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRtJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC182
	.weak	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC183:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {long unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC183
	.weak	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC183
	.weak	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC184:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {long unsigned int&, unsigned char&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC184
	.weak	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJS1_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC184
	.weak	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC185:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC185
	.weak	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJRjEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC185
	.weak	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC186:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned char&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC186
	.weak	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC186
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC187:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, short unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC187
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC187
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, short unsigned int&, unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC188
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RtS1_S2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC188
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC189:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC189
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC189
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC190:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC190
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC190
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC191:
	.ascii	"std::string node::S"
	.string	"PrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, unsigned int&, unsigned int&, long unsigned int&, long unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC191
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_RmS2_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC191
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC192:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, unsigned int&, unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC192
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_S1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC192
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC193:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, unsigned int&, long unsigned int&, long unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC193
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_RmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC193
	.weak	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC194:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {long int&, unsigned char&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC194
	.weak	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRlRhS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC194
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC195:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC195
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC195
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC196
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC196
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC197:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&, unsigned int&, long unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC197
	.weak	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_S1_RmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC197
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC198:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {long unsigned int&, long unsigned int&, short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC198
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRmS2_RtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC198
	.weak	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {long unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC199
	.weak	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC199
	.weak	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC200:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {short unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC200
	.weak	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRtEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC200
	.weak	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC201:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC201
	.weak	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC201
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC202:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {long unsigned int&, long unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC202
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRmS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC202
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC203:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {long unsigned int&, long unsigned int&, unsigned char&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC203
	.weak	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRmS2_RhEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC203
	.weak	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC204:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {long unsigned int&, unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC204
	.weak	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJRmS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC204
	.weak	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC205:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = unsigned int&; Args = {unsigned int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC205
	.weak	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRjJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC205
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC206:
	.string	"../src/debug_utils-inl.h:113"
.LC207:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC208:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC206
	.quad	.LC207
	.quad	.LC208
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC159
	.quad	.LC160
	.quad	.LC208
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC209:
	.string	"../src/util-inl.h:325"
.LC210:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC211:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.weak	_ZZN4node6CallocI16uvwasi_preopen_sEEPT_mE4args
	.section	.rodata.str1.1
.LC212:
	.string	"../src/util-inl.h:388"
.LC213:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC214:
	.string	"T* node::Calloc(size_t) [with T = uvwasi_preopen_s; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6CallocI16uvwasi_preopen_sEEPT_mE4args,"awG",@progbits,_ZZN4node6CallocI16uvwasi_preopen_sEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6CallocI16uvwasi_preopen_sEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6CallocI16uvwasi_preopen_sEEPT_mE4args, 24
_ZZN4node6CallocI16uvwasi_preopen_sEEPT_mE4args:
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.section	.rodata.str1.1
.LC215:
	.string	"../src/node_wasi.cc"
.LC216:
	.string	"wasi"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC215
	.quad	0
	.quad	_ZN4node4wasiL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC216
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC217:
	.string	"../src/node_wasi.cc:1666"
.LC218:
	.string	"(*store) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC219:
	.string	"uvwasi_errno_t node::wasi::WASI::backingStore(char**, size_t*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node4wasi4WASI12backingStoreEPPcPmE4args, @object
	.size	_ZZN4node4wasi4WASI12backingStoreEPPcPmE4args, 24
_ZZN4node4wasi4WASI12backingStoreEPPcPmE4args:
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.section	.rodata.str1.1
.LC220:
	.string	"../src/node_wasi.cc:1645"
.LC221:
	.string	"args[0]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC222:
	.string	"static void node::wasi::WASI::_SetMemory(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.section	.rodata.str1.1
.LC223:
	.string	"../src/node_wasi.cc:1644"
.LC224:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4wasi4WASI10_SetMemoryERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC222
	.section	.rodata.str1.1
.LC225:
	.string	"../src/node_wasi.cc:226"
	.section	.rodata.str1.8
	.align 8
.LC226:
	.string	"(options.preopens[index].real_path) != nullptr"
	.align 8
.LC227:
	.string	"static void node::wasi::WASI::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__14_, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__14_, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__14_:
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.section	.rodata.str1.1
.LC228:
	.string	"../src/node_wasi.cc:224"
	.section	.rodata.str1.8
	.align 8
.LC229:
	.string	"(options.preopens[index].mapped_path) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__13_, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__13_, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__13_:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC227
	.section	.rodata.str1.1
.LC230:
	.string	"../src/node_wasi.cc:220"
.LC231:
	.string	"real->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC227
	.section	.rodata.str1.1
.LC232:
	.string	"../src/node_wasi.cc:219"
.LC233:
	.string	"mapped->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_:
	.quad	.LC232
	.quad	.LC233
	.quad	.LC227
	.section	.rodata.str1.1
.LC234:
	.string	"../src/node_wasi.cc:212"
	.section	.rodata.str1.8
	.align 8
.LC235:
	.string	"(preopens->Length() % 2) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_:
	.quad	.LC234
	.quad	.LC235
	.quad	.LC227
	.section	.rodata.str1.1
.LC236:
	.string	"../src/node_wasi.cc:207"
.LC237:
	.string	"(options.envp[i]) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9:
	.quad	.LC236
	.quad	.LC237
	.quad	.LC227
	.section	.rodata.str1.1
.LC238:
	.string	"../src/node_wasi.cc:204"
.LC239:
	.string	"pair->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8:
	.quad	.LC238
	.quad	.LC239
	.quad	.LC227
	.section	.rodata.str1.1
.LC240:
	.string	"../src/node_wasi.cc:196"
.LC241:
	.string	"(options.argv[i]) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7:
	.quad	.LC240
	.quad	.LC241
	.quad	.LC227
	.section	.rodata.str1.1
.LC242:
	.string	"../src/node_wasi.cc:193"
.LC243:
	.string	"arg->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6:
	.quad	.LC242
	.quad	.LC243
	.quad	.LC227
	.section	.rodata.str1.1
.LC244:
	.string	"../src/node_wasi.cc:178"
.LC245:
	.string	"(stdio->Length()) == (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5:
	.quad	.LC244
	.quad	.LC245
	.quad	.LC227
	.section	.rodata.str1.1
.LC246:
	.string	"../src/node_wasi.cc:169"
.LC247:
	.string	"args[3]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC246
	.quad	.LC247
	.quad	.LC227
	.section	.rodata.str1.1
.LC248:
	.string	"../src/node_wasi.cc:168"
.LC249:
	.string	"args[2]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC227
	.section	.rodata.str1.1
.LC250:
	.string	"../src/node_wasi.cc:167"
.LC251:
	.string	"args[1]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC250
	.quad	.LC251
	.quad	.LC227
	.section	.rodata.str1.1
.LC252:
	.string	"../src/node_wasi.cc:166"
.LC253:
	.string	"args[0]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC252
	.quad	.LC253
	.quad	.LC227
	.section	.rodata.str1.1
.LC254:
	.string	"../src/node_wasi.cc:165"
.LC255:
	.string	"(args.Length()) == (4)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC254
	.quad	.LC255
	.quad	.LC227
	.section	.rodata.str1.1
.LC256:
	.string	"../src/node_wasi.cc:164"
.LC257:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4wasi4WASI3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC256
	.quad	.LC257
	.quad	.LC227
	.section	.rodata.str1.1
.LC258:
	.string	"../src/node_wasi.cc:152"
	.section	.rodata.str1.8
	.align 8
.LC259:
	.string	"(current_uvwasi_memory_) >= (previous_size)"
	.align 8
.LC260:
	.string	"void node::wasi::WASI::CheckAllocatedSize(size_t) const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node4wasi4WASI18CheckAllocatedSizeEmE4args, @object
	.size	_ZZNK4node4wasi4WASI18CheckAllocatedSizeEmE4args, 24
_ZZNK4node4wasi4WASI18CheckAllocatedSizeEmE4args:
	.quad	.LC258
	.quad	.LC259
	.quad	.LC260
	.section	.rodata.str1.1
.LC261:
	.string	"../src/node_wasi.cc:143"
	.section	.rodata.str1.8
	.align 8
.LC262:
	.string	"(current_uvwasi_memory_) == (0)"
	.align 8
.LC263:
	.string	"virtual node::wasi::WASI::~WASI()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasi4WASID4EvE4args, @object
	.size	_ZZN4node4wasi4WASID4EvE4args, 24
_ZZN4node4wasi4WASID4EvE4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.section	.rodata.str1.1
.LC264:
	.string	"../src/node_wasi.cc:98"
.LC265:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC266:
	.string	"v8::MaybeLocal<v8::Value> node::wasi::WASIException(v8::Local<v8::Context>, int, const char*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4wasiL13WASIExceptionEN2v85LocalINS1_7ContextEEEiPKcE4args, @object
	.size	_ZZN4node4wasiL13WASIExceptionEN2v85LocalINS1_7ContextEEEiPKcE4args, 24
_ZZN4node4wasiL13WASIExceptionEN2v85LocalINS1_7ContextEEEiPKcE4args:
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC267:
	.string	"../src/debug_utils-inl.h:67"
.LC268:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC269:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC267
	.quad	.LC268
	.quad	.LC269
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC270:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC271:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC272:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC273:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC274:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC275:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC276:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC277:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC278:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC279:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC280:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC281:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC279
	.quad	.LC280
	.quad	.LC281
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC282:
	.string	"../src/base_object-inl.h:44"
.LC283:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC282
	.quad	.LC283
	.quad	.LC281
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC284:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC285:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC286:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC284
	.quad	.LC285
	.quad	.LC286
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
