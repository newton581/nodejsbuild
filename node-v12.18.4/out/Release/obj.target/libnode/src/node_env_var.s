	.file	"node_env_var.cc"
	.text
	.section	.text._ZN4node12RealEnvStoreD2Ev,"axG",@progbits,_ZN4node12RealEnvStoreD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12RealEnvStoreD2Ev
	.type	_ZN4node12RealEnvStoreD2Ev, @function
_ZN4node12RealEnvStoreD2Ev:
.LFB9883:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9883:
	.size	_ZN4node12RealEnvStoreD2Ev, .-_ZN4node12RealEnvStoreD2Ev
	.weak	_ZN4node12RealEnvStoreD1Ev
	.set	_ZN4node12RealEnvStoreD1Ev,_ZN4node12RealEnvStoreD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB9904:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9904:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB9911:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9911:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB9914:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9914:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB9908:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE9908:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB9915:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE9915:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node12RealEnvStore5QueryEPKc
	.type	_ZNK4node12RealEnvStore5QueryEPKc, @function
_ZNK4node12RealEnvStore5QueryEPKc:
.LFB7284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	-40(%rbp), %rdx
	leaq	-26(%rbp), %rsi
	movq	%r12, %rdi
	movq	$2, -40(%rbp)
	call	uv_os_getenv@PLT
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movl	%eax, %ebx
	call	uv_mutex_unlock@PLT
	xorl	%eax, %eax
	cmpl	$-2, %ebx
	sete	%al
	negl	%eax
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L11
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7284:
	.size	_ZNK4node12RealEnvStore5QueryEPKc, .-_ZNK4node12RealEnvStore5QueryEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node12RealEnvStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_
	.type	_ZN4node12RealEnvStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_, @function
_ZN4node12RealEnvStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_:
.LFB7283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	subq	$2120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	-2144(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	leaq	-1088(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2128(%rbp), %rdi
	movq	-1072(%rbp), %rsi
	call	uv_os_setenv@PLT
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L13
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L13
	call	free@PLT
.L13:
	movq	-2128(%rbp), %rdi
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L14
	testq	%rdi, %rdi
	je	.L14
	call	free@PLT
.L14:
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$2120, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7283:
	.size	_ZN4node12RealEnvStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_, .-_ZN4node12RealEnvStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_
	.align 2
	.p2align 4
	.globl	_ZN4node12RealEnvStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.type	_ZN4node12RealEnvStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE, @function
_ZN4node12RealEnvStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE:
.LFB7286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	subq	$1056, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	-1072(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %rdi
	call	uv_os_unsetenv@PLT
	movq	-1056(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L31
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L31
	call	free@PLT
.L31:
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$1056, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7286:
	.size	_ZN4node12RealEnvStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE, .-_ZN4node12RealEnvStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB9916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L41
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L41:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9916:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB9909:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L45
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L45:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9909:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev,"axG",@progbits,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.type	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, @function
_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev:
.LFB7989:
	.cfi_startproc
	endbr64
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE7989:
	.size	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, .-_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev
	.set	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.section	.text._ZN4node12RealEnvStoreD0Ev,"axG",@progbits,_ZN4node12RealEnvStoreD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12RealEnvStoreD0Ev
	.type	_ZN4node12RealEnvStoreD0Ev, @function
_ZN4node12RealEnvStoreD0Ev:
.LFB9885:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9885:
	.size	_ZN4node12RealEnvStoreD0Ev, .-_ZN4node12RealEnvStoreD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB9913:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9913:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB9906:
	.cfi_startproc
	endbr64
	movl	$120, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9906:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.type	_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, @function
_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE:
.LFB7314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rsi, %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	leaq	-80(%rbp), %rcx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	movq	%rax, -104(%rbp)
	movq	%rcx, -112(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object19GetOwnPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L54
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L68
	xorl	%r15d, %r15d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L59
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L59
	movq	-96(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%rdx, -88(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-88(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L54
	movq	%rbx, %rsi
	movq	%rdx, -88(%rbp)
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	-88(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L54
	movq	(%r14), %rax
	movq	-104(%rbp), %rsi
	movq	%r14, %rdi
	call	*32(%rax)
.L59:
	addl	$1, %r15d
	cmpl	%r15d, %r13d
	je	.L68
.L62:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L78
.L54:
	xorl	%ebx, %ebx
	movb	$0, %bh
.L61:
	movq	-112(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$72, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	movl	$257, %ebx
	jmp	.L61
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7314:
	.size	_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE, .-_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"vector::reserve"
.LC1:
	.string	"vector::_M_realloc_insert"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node10MapKVStore9EnumerateEPN2v87IsolateE
	.type	_ZNK4node10MapKVStore9EnumerateEPN2v87IsolateE, @function
_ZNK4node10MapKVStore9EnumerateEPN2v87IsolateE:
.LFB7296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	movq	%rax, %rdi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	%rax, -56(%rbp)
	call	uv_mutex_lock@PLT
	movq	72(%r14), %rbx
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rbx
	ja	.L128
	testq	%rbx, %rbx
	jne	.L129
	movq	64(%r14), %r14
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	testq	%r14, %r14
	je	.L130
.L103:
	movq	%r15, %r13
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L133:
	movq	(%r14), %r14
	movq	%rdx, (%r15)
	addq	$8, %r15
	testq	%r14, %r14
	je	.L131
.L100:
	movq	8(%r14), %rsi
	movl	16(%r14), %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L132
.L88:
	cmpq	%r15, %rbx
	jne	.L133
	subq	%r13, %rbx
	movq	%rbx, %rax
	movq	%rbx, %rcx
	movabsq	$1152921504606846975, %rbx
	sarq	$3, %rax
	cmpq	%rbx, %rax
	je	.L134
	testq	%rax, %rax
	je	.L105
	movabsq	$9223372036854775800, %rbx
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L135
.L92:
	movq	%rbx, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rcx
	addq	%rax, %rbx
	leaq	8(%rax), %rsi
	movq	%rdx, (%rax,%rcx)
	cmpq	%r13, %r15
	je	.L108
.L137:
	leaq	-8(%r15), %rsi
	leaq	15(%rax), %rdx
	subq	%r13, %rsi
	subq	%r13, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L109
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L109
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L96:
	movdqu	0(%r13,%rdx), %xmm0
	movups	%xmm0, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L96
	movq	%rdi, %r9
	andq	$-2, %r9
	leaq	0(,%r9,8), %rcx
	leaq	0(%r13,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%r9, %rdi
	je	.L98
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L98:
	leaq	16(%rax,%rsi), %r15
.L94:
	testq	%r13, %r13
	je	.L99
	movq	%r13, %rdi
	movq	%rax, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
.L99:
	movq	(%r14), %r14
	movq	%rax, %r13
	testq	%r14, %r14
	jne	.L100
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	subq	%r13, %rdx
	sarq	$3, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L101
.L102:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L101:
	movq	-56(%rbp), %rdi
	call	uv_mutex_unlock@PLT
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	testq	%rsi, %rsi
	jne	.L136
	xorl	%eax, %eax
	movl	$8, %esi
	xorl	%ebx, %ebx
	movq	%rdx, (%rax,%rcx)
	cmpq	%r13, %r15
	jne	.L137
.L108:
	movq	%rsi, %r15
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L105:
	movl	$8, %ebx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L129:
	salq	$3, %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	64(%r14), %r14
	movq	%rax, %r13
	movq	%rax, %r15
	addq	%rax, %rbx
	testq	%r14, %r14
	jne	.L103
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%rax, %r12
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rax, %rcx
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%r15, %rdx
	jne	.L95
	jmp	.L98
.L130:
	movq	%r12, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%rax, %r12
	jmp	.L101
.L134:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L128:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L136:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	movq	%rax, %rbx
	cmovbe	%rsi, %rbx
	salq	$3, %rbx
	jmp	.L92
	.cfi_endproc
.LFE7296:
	.size	_ZNK4node10MapKVStore9EnumerateEPN2v87IsolateE, .-_ZNK4node10MapKVStore9EnumerateEPN2v87IsolateE
	.section	.text._ZN4node10MapKVStoreD0Ev,"axG",@progbits,_ZN4node10MapKVStoreD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10MapKVStoreD0Ev
	.type	_ZN4node10MapKVStoreD0Ev, @function
_ZN4node10MapKVStoreD0Ev:
.LFB9881:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10MapKVStoreE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	64(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	jne	.L143
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L155:
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L139
.L142:
	movq	%rbx, %r13
.L143:
	movq	40(%r13), %rdi
	leaq	56(%r13), %rax
	movq	0(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L140
	call	_ZdlPv@PLT
.L140:
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L155
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L142
.L139:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L144
	call	_ZdlPv@PLT
.L144:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9881:
	.size	_ZN4node10MapKVStoreD0Ev, .-_ZN4node10MapKVStoreD0Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Cannot create a string longer than 0x%x characters"
	.section	.rodata.str1.1
.LC4:
	.string	"ERR_STRING_TOO_LONG"
.LC5:
	.string	"code"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node12RealEnvStore9EnumerateEPN2v87IsolateE
	.type	_ZNK4node12RealEnvStore9EnumerateEPN2v87IsolateE, @function
_ZNK4node12RealEnvStore9EnumerateEPN2v87IsolateE:
.LFB7287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$2248, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	-2284(%rbp), %rsi
	leaq	-2280(%rbp), %rdi
	call	uv_os_environ@PLT
	testl	%eax, %eax
	jne	.L193
	leaq	-2248(%rbp), %r13
	movdqa	.LC2(%rip), %xmm0
	movslq	-2284(%rbp), %rbx
	leaq	-200(%rbp), %rdx
	movq	%r13, -2256(%rbp)
	movq	%r13, %rax
	movaps	%xmm0, -2272(%rbp)
	movq	%rbx, %r14
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L158:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L158
	movq	$0, -2248(%rbp)
	cmpq	$256, %rbx
	ja	.L194
	movq	%r13, %r8
.L159:
	movq	%rbx, -2272(%rbp)
	testl	%r14d, %r14d
	jle	.L177
	xorl	%ebx, %ebx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L172:
	movq	-2256(%rbp), %r8
	movq	%rax, (%r8,%rbx,8)
	addq	$1, %rbx
	cmpl	%ebx, -2284(%rbp)
	jle	.L195
.L173:
	movq	%rbx, %rax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r12, %rdi
	salq	$4, %rax
	addq	-2280(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L196
	leal	1(%rbx), %edx
	cmpq	%rbx, -2272(%rbp)
	ja	.L172
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm256EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L194:
	movabsq	$2305843009213693951, %rax
	leaq	0(,%rbx,8), %r14
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L197
	testq	%r14, %r14
	je	.L161
	movq	%r14, %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L198
	movq	%rax, -2256(%rbp)
	movl	-2284(%rbp), %r14d
	movq	%rbx, -2264(%rbp)
	jmp	.L159
.L196:
	movl	$1073741799, %r9d
	movl	$128, %ecx
	movl	$1, %edx
	movl	$128, %esi
	leaq	-192(%rbp), %r14
	leaq	.LC3(%rip), %r8
	movq	%r14, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L199
.L166:
	movq	%r14, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L200
.L167:
	movq	%r14, %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L201
.L168:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L202
.L169:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L203
.L170:
	movq	%r12, %rdi
	movq	%r14, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
.L171:
	movq	-2256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L174
	cmpq	%r13, %rdi
	je	.L174
	call	free@PLT
.L174:
	movl	-2284(%rbp), %esi
	movq	-2280(%rbp), %rdi
	call	uv_os_free_environ@PLT
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$2248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L195:
	.cfi_restore_state
	movslq	%edx, %rdx
.L164:
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%rax, %r12
	jmp	.L171
.L193:
	leaq	_ZZNK4node12RealEnvStore9EnumerateEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L161:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L198:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r14, %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L161
	movq	-2272(%rbp), %rax
	movq	%r8, -2256(%rbp)
	movq	%rbx, -2264(%rbp)
	movl	-2284(%rbp), %r14d
	testq	%rax, %rax
	je	.L159
	movq	%r8, %rdi
	leaq	0(,%rax,8), %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L159
.L197:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L177:
	xorl	%edx, %edx
	jmp	.L164
.L203:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L170
.L202:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L169
.L199:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L166
.L201:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L168
.L200:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L167
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7287:
	.size	_ZNK4node12RealEnvStore9EnumerateEPN2v87IsolateE, .-_ZNK4node12RealEnvStore9EnumerateEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZNK4node7KVStore5CloneEPN2v87IsolateE
	.type	_ZNK4node7KVStore5CloneEPN2v87IsolateE, @function
_ZNK4node7KVStore5CloneEPN2v87IsolateE:
.LFB7289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdx, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$120, %edi
	movq	%rax, -112(%rbp)
	call	_Znwm@PLT
	movl	$13, %ecx
	movq	%rax, %r14
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	leaq	16(%r14), %rbx
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r14)
	movq	%rbx, %rdi
	xorl	%eax, %eax
	rep stosq
	leaq	16+_ZTVN4node10MapKVStoreE(%rip), %rax
	leaq	24(%r14), %rdi
	movq	%rax, 16(%r14)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L220
	leaq	112(%r14), %rax
	movq	%r14, %xmm1
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, 64(%r14)
	movq	%rbx, %xmm0
	movq	-104(%rbp), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	$1, 72(%r14)
	movups	%xmm0, (%rax)
	movq	0(%r13), %rax
	movq	$0, 80(%r14)
	movq	$0, 88(%r14)
	movl	$0x3f800000, 96(%r14)
	movq	$0, 104(%r14)
	movq	$0, 112(%r14)
	call	*64(%rax)
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -116(%rbp)
	testl	%eax, %eax
	je	.L207
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L213:
	movq	-112(%rbp), %rsi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L221
.L208:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L222
.L209:
	leaq	_ZZNK4node7KVStore5CloneEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L209
	movq	-104(%rbp), %rax
	movq	%rdx, -88(%rbp)
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	(%rax), %r14
	movq	(%r14), %rax
	movq	32(%rax), %rax
	movq	%rax, -96(%rbp)
	movq	0(%r13), %rax
	call	*16(%rax)
	movq	-88(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L223
.L211:
	movq	-96(%rbp), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	addl	$1, %ebx
	call	*%rax
	cmpl	%ebx, -116(%rbp)
	jne	.L213
.L207:
	movq	-128(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	movq	-104(%rbp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L223:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	xorl	%ecx, %ecx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L224:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7289:
	.size	_ZNK4node7KVStore5CloneEPN2v87IsolateE, .-_ZNK4node7KVStore5CloneEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZNK4node12RealEnvStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.type	_ZNK4node12RealEnvStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE, @function
_ZNK4node12RealEnvStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE:
.LFB7285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	leaq	-1088(%rbp), %rdi
	subq	$1096, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1072(%rbp), %r12
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	leaq	-1096(%rbp), %rdx
	leaq	-26(%rbp), %rsi
	movq	$2, -1096(%rbp)
	movq	%r12, %rdi
	call	uv_os_getenv@PLT
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	cmpl	$-2, %eax
	sete	%al
	movzbl	%al, %eax
	negl	%eax
	movl	%eax, %r12d
	call	uv_mutex_unlock@PLT
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L225
	testq	%rdi, %rdi
	je	.L225
	call	free@PLT
.L225:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$1096, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L235:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7285:
	.size	_ZNK4node12RealEnvStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE, .-_ZNK4node12RealEnvStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB9907:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10MapKVStoreE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	movq	%rax, 16(%rdi)
	testq	%r12, %r12
	jne	.L241
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L253:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L237
.L240:
	movq	%r13, %r12
.L241:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	je	.L238
	call	_ZdlPv@PLT
.L238:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L253
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L240
.L237:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	64(%rbx), %rdi
	leaq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	cmpq	%rax, %rdi
	je	.L242
	call	_ZdlPv@PLT
.L242:
	addq	$8, %rsp
	leaq	24(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE9907:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN4node10MapKVStoreD2Ev,"axG",@progbits,_ZN4node10MapKVStoreD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10MapKVStoreD2Ev
	.type	_ZN4node10MapKVStoreD2Ev, @function
_ZN4node10MapKVStoreD2Ev:
.LFB9879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10MapKVStoreE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	jne	.L259
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L271:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L255
.L258:
	movq	%r13, %r12
.L259:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	je	.L256
	call	_ZdlPv@PLT
.L256:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L271
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L258
.L255:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L260
	call	_ZdlPv@PLT
.L260:
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE9879:
	.size	_ZN4node10MapKVStoreD2Ev, .-_ZN4node10MapKVStoreD2Ev
	.weak	_ZN4node10MapKVStoreD1Ev
	.set	_ZN4node10MapKVStoreD1Ev,_ZN4node10MapKVStoreD2Ev
	.section	.text._ZNSt10shared_ptrIN4node7KVStoreEED2Ev,"axG",@progbits,_ZNSt10shared_ptrIN4node7KVStoreEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10shared_ptrIN4node7KVStoreEED2Ev
	.type	_ZNSt10shared_ptrIN4node7KVStoreEED2Ev, @function
_ZNSt10shared_ptrIN4node7KVStoreEED2Ev:
.LFB6299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L272
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L275
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L281
.L272:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L272
.L281:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L278
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L279:
	cmpl	$1, %eax
	jne	.L272
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L279
	.cfi_endproc
.LFE6299:
	.size	_ZNSt10shared_ptrIN4node7KVStoreEED2Ev, .-_ZNSt10shared_ptrIN4node7KVStoreEED2Ev
	.weak	_ZNSt10shared_ptrIN4node7KVStoreEED1Ev
	.set	_ZNSt10shared_ptrIN4node7KVStoreEED1Ev,_ZNSt10shared_ptrIN4node7KVStoreEED2Ev
	.text
	.p2align 4
	.type	_ZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEE, @function
_ZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEE:
.LFB7317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L299
	cmpw	$1040, %cx
	jne	.L283
.L299:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L305
.L286:
	movq	(%r12), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L306
.L282:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L282
	movq	1392(%rax), %r14
	movq	1384(%rax), %rdi
	testq	%r14, %r14
	je	.L290
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	leaq	8(%r14), %r15
	testq	%r13, %r13
	je	.L291
	lock addl	$1, (%r15)
.L292:
	movq	(%rdi), %rcx
	movq	%r12, %rdx
	movq	352(%rax), %rsi
	call	*40(%rcx)
	movl	%eax, %r12d
	testq	%r13, %r13
	je	.L307
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
.L293:
	cmpl	$1, %eax
	je	.L308
.L294:
	cmpl	$-1, %r12d
	je	.L282
	movq	(%rbx), %rdx
	salq	$32, %r12
	movq	%r12, 32(%rdx)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L286
	.p2align 4,,10
	.p2align 3
.L305:
	leaq	_ZZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L307:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L291:
	addl	$1, 8(%r14)
	jmp	.L292
	.p2align 4,,10
	.p2align 3
.L290:
	movq	(%rdi), %rcx
	movq	%r12, %rdx
	movq	352(%rax), %rsi
	call	*40(%rcx)
	movl	%eax, %r12d
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L295
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L296:
	cmpl	$1, %eax
	jne	.L294
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L295:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L296
	.cfi_endproc
.LFE7317:
	.size	_ZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEE, .-_ZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEE
	.p2align 4
	.type	_ZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEE, @function
_ZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEE:
.LFB7319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L325
	cmpw	$1040, %cx
	jne	.L310
.L325:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L330
.L313:
	movq	(%r12), %rbx
	movq	1392(%rax), %r12
	movq	1384(%rax), %rdi
	testq	%r12, %r12
	je	.L314
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L315
	lock addl	$1, 8(%r12)
.L314:
	movq	352(%rax), %rsi
	movq	(%rdi), %rax
	call	*64(%rax)
	testq	%rax, %rax
	je	.L331
	movq	(%rax), %rax
.L317:
	movq	%rax, 32(%rbx)
	testq	%r12, %r12
	je	.L309
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L320
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L332
.L309:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	addl	$1, 8(%r12)
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L320:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L309
.L332:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L323
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L324:
	cmpl	$1, %eax
	jne	.L309
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L313
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	_ZZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L331:
	movq	24(%rbx), %rax
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L323:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L324
	.cfi_endproc
.LFE7319:
	.size	_ZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEE, .-_ZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEE
	.p2align 4
	.type	_ZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE:
.LFB7315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L352
	cmpw	$1040, %cx
	jne	.L334
.L352:
	movq	23(%rdx), %r13
	cmpb	$0, 1928(%r13)
	je	.L355
.L337:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsSymbolEv@PLT
	testb	%al, %al
	jne	.L356
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L340
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L340
	movq	1392(%r13), %r14
	movq	1384(%r13), %rdi
	testq	%r14, %r14
	je	.L342
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	leaq	8(%r14), %r15
	je	.L343
	lock addl	$1, (%r15)
.L344:
	movq	(%rdi), %rax
	movq	%r12, %rdx
	movq	352(%r13), %rsi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, %r12
	je	.L357
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
.L345:
	cmpl	$1, %eax
	je	.L358
.L346:
	testq	%r12, %r12
	je	.L333
	movq	(%r12), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 32(%rax)
.L333:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	leaq	_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L357:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L334:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	cmpb	$0, 1928(%r13)
	jne	.L337
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L343:
	addl	$1, 8(%r14)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L342:
	movq	(%rdi), %rax
	movq	%r12, %rdx
	movq	352(%r13), %rsi
	call	*16(%rax)
	movq	%rax, %r12
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L358:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L347
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L348:
	cmpl	$1, %eax
	jne	.L346
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L347:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L348
	.cfi_endproc
.LFE7315:
	.size	_ZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1
.LC7:
	.string	"DEP0104"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"Assigning any value other than a string, number, or boolean to a process.env property is deprecated. Please make sure to convert the value to a string before setting process.env with it."
	.text
	.p2align 4
	.type	_ZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EE, @function
_ZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EE:
.LFB7316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L404
	cmpw	$1040, %cx
	jne	.L360
.L404:
	movq	23(%rdx), %rbx
	cmpb	$0, 1928(%rbx)
	je	.L420
.L363:
	movq	1648(%rbx), %r13
	movq	1640(%rbx), %rdx
	testq	%r13, %r13
	je	.L364
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r8
	leaq	8(%r13), %rax
	testq	%r8, %r8
	je	.L365
	lock addl	$1, (%rax)
	movzbl	268(%rdx), %edx
	testb	%dl, %dl
	je	.L402
.L403:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L367
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L367
.L368:
	testq	%r13, %r13
	jne	.L421
.L378:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L359
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L359
	movq	1392(%rbx), %r15
	movq	1384(%rbx), %rdi
	testq	%r15, %r15
	je	.L422
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r8
	leaq	8(%r15), %rax
	testq	%r8, %r8
	je	.L383
	lock addl	$1, (%rax)
.L384:
	movq	(%rdi), %r9
	movq	%r8, -64(%rbp)
	movq	%r13, %rdx
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rsi
	call	*32(%r9)
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rax
	testq	%r8, %r8
	je	.L423
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L385:
	cmpl	$1, %eax
	je	.L424
.L387:
	movq	(%r14), %rax
	testq	%r12, %r12
	je	.L425
	movq	(%r12), %rdx
.L392:
	movq	%rdx, 32(%rax)
.L359:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	movzbl	268(%rdx), %edx
	addl	$1, 8(%r13)
	testb	%dl, %dl
	jne	.L403
.L402:
	testq	%r8, %r8
	je	.L370
	movl	$-1, %ecx
	lock xaddl	%ecx, (%rax)
	movl	%ecx, %eax
	cmpl	$1, %eax
	je	.L426
.L373:
	testb	%dl, %dl
	je	.L378
.L377:
	leaq	.LC7(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node29ProcessEmitDeprecationWarningEPNS_11EnvironmentEPKcS3_@PLT
	testb	%al, %al
	jne	.L378
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L368
	movq	%r12, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L368
	movzbl	1402(%rbx), %edx
	movb	$0, 1402(%rbx)
	testb	%dl, %dl
	je	.L368
	testq	%r13, %r13
	je	.L377
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r8
	leaq	8(%r13), %rax
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L364:
	cmpb	$0, 268(%rdx)
	jne	.L403
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L370:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L373
.L426:
	movq	0(%r13), %rax
	movq	%r8, -64(%rbp)
	movq	%r13, %rdi
	movb	%dl, -56(%rbp)
	call	*16(%rax)
	movq	-64(%rbp), %r8
	movzbl	-56(%rbp), %edx
	testq	%r8, %r8
	je	.L374
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L375:
	cmpl	$1, %eax
	jne	.L373
	movq	0(%r13), %rax
	movb	%dl, -56(%rbp)
	movq	%r13, %rdi
	call	*24(%rax)
	movzbl	-56(%rbp), %edx
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L383:
	addl	$1, 8(%r15)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L360:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	cmpb	$0, 1928(%rbx)
	jne	.L363
	.p2align 4,,10
	.p2align 3
.L420:
	leaq	_ZZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L421:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r8
	leaq	8(%r13), %rax
	xorl	%edx, %edx
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L422:
	movq	(%rdi), %rax
	movq	352(%rbx), %rsi
	movq	%r13, %rdx
	call	*32(%rax)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L423:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L374:
	movl	12(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 12(%r13)
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%r15), %rax
	movq	%r8, -56(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-56(%rbp), %r8
	testq	%r8, %r8
	je	.L388
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L389:
	cmpl	$1, %eax
	jne	.L387
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L388:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L425:
	movq	24(%rax), %rdx
	jmp	.L392
	.cfi_endproc
.LFE7316:
	.size	_ZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EE, .-_ZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EE
	.p2align 4
	.type	_ZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEE, @function
_ZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEE:
.LFB7318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L444
	cmpw	$1040, %cx
	jne	.L428
.L444:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L448
.L431:
	movq	(%r12), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L449
.L432:
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	112(%rdx), %rdx
	movq	%rdx, 32(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L449:
	.cfi_restore_state
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L432
	movq	1392(%rax), %r13
	movq	1384(%rax), %rdi
	testq	%r13, %r13
	je	.L433
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	8(%r13), %r15
	testq	%r14, %r14
	je	.L434
	lock addl	$1, (%r15)
.L435:
	movq	(%rdi), %rcx
	movq	352(%rax), %rsi
	movq	%r12, %rdx
	call	*56(%rcx)
	testq	%r14, %r14
	je	.L450
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
.L436:
	cmpl	$1, %eax
	jne	.L432
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L439
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L440:
	cmpl	$1, %eax
	jne	.L432
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L428:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L431
	.p2align 4,,10
	.p2align 3
.L448:
	leaq	_ZZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L450:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L434:
	addl	$1, 8(%r13)
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L433:
	movq	(%rdi), %rcx
	movq	352(%rax), %rsi
	movq	%r12, %rdx
	call	*56(%rcx)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L439:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L440
	.cfi_endproc
.LFE7318:
	.size	_ZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEE, .-_ZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEE
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node10MapKVStore5QueryEPKc
	.type	_ZNK4node10MapKVStore5QueryEPKc, @function
_ZNK4node10MapKVStore5QueryEPKc:
.LFB7293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	-96(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%r15, %r15
	je	.L481
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L482
	cmpq	$1, %rax
	jne	.L455
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L456:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-88(%rbp), %rsi
	movl	$3339675911, %edx
	movq	-96(%rbp), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	56(%rbx), %r12
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%r12
	movq	48(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L457
	movq	(%rax), %rbx
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rdi
	movq	72(%rbx), %rcx
	testq	%r9, %r9
	je	.L458
.L462:
	cmpq	%rcx, %r15
	je	.L483
.L459:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L461
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r12
	cmpq	%rdx, %r8
	je	.L462
.L461:
	movl	$-1, %eax
.L460:
	cmpq	%r13, %rdi
	je	.L464
	movl	%eax, -120(%rbp)
	call	_ZdlPv@PLT
	movl	-120(%rbp), %eax
.L464:
	movq	%r14, %rdi
	movl	%eax, -120(%rbp)
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-120(%rbp), %eax
	jne	.L484
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	cmpq	%rcx, %r15
	je	.L485
	.p2align 4,,10
	.p2align 3
.L463:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L461
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r12
	cmpq	%rdx, %r8
	jne	.L461
	cmpq	%rcx, %r15
	jne	.L463
.L485:
	cmpq	$0, 16(%rbx)
	jne	.L463
	xorl	%eax, %eax
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L455:
	testq	%rax, %rax
	jne	.L486
	movq	%r13, %rdx
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L483:
	cmpq	16(%rbx), %r9
	jne	.L459
	movq	8(%rbx), %rsi
	movq	%r9, %rdx
	movq	%r8, -136(%rbp)
	movq	%r9, -128(%rbp)
	movq	%rdi, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %rdi
	movq	-128(%rbp), %r9
	testl	%eax, %eax
	movq	-136(%rbp), %r8
	je	.L460
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L482:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L454:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L481:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L457:
	movq	-96(%rbp), %rdi
	jmp	.L461
.L484:
	call	__stack_chk_fail@PLT
.L486:
	movq	%r13, %rdi
	jmp	.L454
	.cfi_endproc
.LFE7293:
	.size	_ZNK4node10MapKVStore5QueryEPKc, .-_ZNK4node10MapKVStore5QueryEPKc
	.align 2
	.p2align 4
	.globl	_ZNK4node10MapKVStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.type	_ZNK4node10MapKVStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE, @function
_ZNK4node10MapKVStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE:
.LFB7294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-1072(%rbp), %rdi
	subq	$1064, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNK4node10MapKVStore5QueryEPKc
	movq	-1056(%rbp), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L487
	leaq	-1048(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L487
	call	free@PLT
.L487:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L497
	addq	$1064, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L497:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7294:
	.size	_ZNK4node10MapKVStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE, .-_ZNK4node10MapKVStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10MapKVStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.type	_ZN4node10MapKVStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE, @function
_ZN4node10MapKVStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE:
.LFB7295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$1176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r15
	movq	-1104(%rbp), %r12
	leaq	-1120(%rbp), %r13
	movq	%r13, -1136(%rbp)
	movq	%r15, %rax
	addq	%r12, %rax
	je	.L499
	testq	%r15, %r15
	je	.L559
.L499:
	movq	%r12, -1144(%rbp)
	cmpq	$15, %r12
	ja	.L560
	cmpq	$1, %r12
	jne	.L502
	movzbl	(%r15), %eax
	movb	%al, -1120(%rbp)
	movq	%r13, %rax
.L503:
	movq	%r12, -1128(%rbp)
	movl	$3339675911, %edx
	movb	$0, (%rax,%r12)
	movq	-1128(%rbp), %rsi
	movq	-1136(%rbp), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	56(%rbx), %r15
	xorl	%edx, %edx
	movq	%rax, %r11
	divq	%r15
	movq	48(%rbx), %rax
	movq	%rax, -1168(%rbp)
	leaq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	movq	(%rax), %r9
	movq	%rax, -1176(%rbp)
	testq	%r9, %r9
	je	.L557
	movq	(%r9), %r12
	movq	-1128(%rbp), %rax
	movq	%r9, %r8
	movq	-1136(%rbp), %rdi
	movq	%rax, -1160(%rbp)
	movq	72(%r12), %rcx
	testq	%rax, %rax
	je	.L506
	cmpq	%rcx, %r11
	je	.L561
	.p2align 4,,10
	.p2align 3
.L507:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L505
	movq	72(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r10
	jne	.L505
	movq	%rsi, %r12
	cmpq	%rcx, %r11
	jne	.L507
.L561:
	movq	-1160(%rbp), %rax
	cmpq	16(%r12), %rax
	jne	.L507
	movq	8(%r12), %rsi
	movq	%rax, %rdx
	movq	%r10, -1216(%rbp)
	movq	%r8, -1208(%rbp)
	movq	%r9, -1200(%rbp)
	movq	%r11, -1192(%rbp)
	movq	%rdi, -1184(%rbp)
	call	memcmp@PLT
	movq	-1184(%rbp), %rdi
	movq	-1192(%rbp), %r11
	testl	%eax, %eax
	movq	-1200(%rbp), %r9
	movq	-1208(%rbp), %r8
	movq	-1216(%rbp), %r10
	jne	.L507
	movq	(%r12), %rcx
	cmpq	%r8, %r9
	je	.L562
.L549:
	testq	%rcx, %rcx
	je	.L512
	movq	72(%rcx), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r10
	je	.L512
	movq	-1168(%rbp), %rax
	movq	%r8, (%rax,%rdx,8)
	movq	(%r12), %rcx
.L512:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	movq	%rcx, (%r8)
	cmpq	%rax, %rdi
	je	.L514
	call	_ZdlPv@PLT
.L514:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L515
	call	_ZdlPv@PLT
.L515:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 72(%rbx)
.L557:
	movq	-1136(%rbp), %rdi
.L505:
	cmpq	%r13, %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L517
	testq	%rdi, %rdi
	je	.L517
	call	free@PLT
.L517:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L563
	addq	$1176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L506:
	.cfi_restore_state
	cmpq	%rcx, %r11
	je	.L564
	.p2align 4,,10
	.p2align 3
.L510:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L505
	movq	72(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r10
	jne	.L505
	movq	%rsi, %r12
	cmpq	%rcx, %r11
	jne	.L510
.L564:
	cmpq	$0, 16(%r12)
	jne	.L510
	movq	(%r12), %rcx
	cmpq	%r8, %r9
	jne	.L549
.L562:
	testq	%rcx, %rcx
	je	.L523
	movq	72(%rcx), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r10
	je	.L512
	movq	-1168(%rbp), %rax
	movq	%r8, (%rax,%rdx,8)
	movq	-1176(%rbp), %rax
	movq	(%rax), %rax
.L511:
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L565
.L513:
	movq	-1176(%rbp), %rax
	movq	$0, (%rax)
	movq	(%r12), %rcx
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L502:
	testq	%r12, %r12
	jne	.L566
	movq	%r13, %rax
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L560:
	leaq	-1136(%rbp), %rdi
	leaq	-1144(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1136(%rbp)
	movq	%rax, %rdi
	movq	-1144(%rbp), %rax
	movq	%rax, -1120(%rbp)
.L501:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-1144(%rbp), %r12
	movq	-1136(%rbp), %rax
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L523:
	movq	%r8, %rax
	jmp	.L511
.L559:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L565:
	movq	%rcx, 64(%rbx)
	jmp	.L513
.L563:
	call	__stack_chk_fail@PLT
.L566:
	movq	%r13, %rdi
	jmp	.L501
	.cfi_endproc
.LFE7295:
	.size	_ZN4node10MapKVStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE, .-_ZN4node10MapKVStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node10MapKVStore5CloneEPN2v87IsolateE
	.type	_ZNK4node10MapKVStore5CloneEPN2v87IsolateE, @function
_ZNK4node10MapKVStore5CloneEPN2v87IsolateE:
.LFB7306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -72(%rbp)
	movl	$120, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%rax, %r14
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	leaq	24(%r14), %rdi
	movq	%rax, (%r14)
	leaq	16(%r14), %rax
	movq	%rax, -96(%rbp)
	leaq	16+_ZTVN4node10MapKVStoreE(%rip), %rax
	movq	%rax, 16(%r14)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L640
	movq	56(%rbx), %rdx
	movq	72(%rbx), %rax
	movq	$0, 64(%r14)
	leaq	112(%r14), %rdi
	movdqu	80(%rbx), %xmm1
	movq	$0, 80(%r14)
	movq	%rdx, 72(%r14)
	movq	%rax, 88(%r14)
	movq	$0, 112(%r14)
	movups	%xmm1, 96(%r14)
	cmpq	$1, %rdx
	je	.L570
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L641
	leaq	0(,%rdx,8), %r12
	movq	%r12, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %rdi
.L570:
	movq	64(%rbx), %rbx
	movq	%rdi, 64(%r14)
	testq	%rbx, %rbx
	je	.L584
	movl	$80, %edi
	call	_Znwm@PLT
	movq	16(%rbx), %r15
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %r12
	movq	%rdi, 8(%rax)
	movq	8(%rbx), %r13
	movq	%r13, %rax
	addq	%r15, %rax
	je	.L573
	testq	%r13, %r13
	je	.L578
.L573:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L642
	cmpq	$1, %r15
	jne	.L576
	movzbl	0(%r13), %eax
	movb	%al, 24(%r12)
.L577:
	movq	%r15, 16(%r12)
	movb	$0, (%rdi,%r15)
	leaq	56(%r12), %rdi
	movq	48(%rbx), %r15
	movq	%rdi, 40(%r12)
	movq	40(%rbx), %r13
	movq	%r13, %rax
	addq	%r15, %rax
	je	.L599
	testq	%r13, %r13
	je	.L578
.L599:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L643
	cmpq	$1, %r15
	jne	.L582
	movzbl	0(%r13), %eax
	movb	%al, 56(%r12)
.L583:
	movq	%r15, 48(%r12)
	xorl	%edx, %edx
	leaq	80(%r14), %rsi
	movb	$0, (%rdi,%r15)
	movq	72(%rbx), %rax
	movq	%r12, 80(%r14)
	movq	%rax, 72(%r12)
	divq	72(%r14)
	movq	64(%r14), %rax
	movq	%rsi, (%rax,%rdx,8)
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L584
	leaq	-64(%rbp), %r15
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L586:
	cmpq	$1, %r9
	jne	.L588
	movzbl	(%r10), %eax
	movb	%al, 24(%rbx)
.L589:
	movq	%r9, 16(%rbx)
	movb	$0, (%rdi,%r9)
	leaq	56(%rbx), %rdi
	movq	48(%r13), %r9
	movq	%rdi, 40(%rbx)
	movq	40(%r13), %r10
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L601
	testq	%r10, %r10
	je	.L578
.L601:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L644
	cmpq	$1, %r9
	jne	.L593
	movzbl	(%r10), %eax
	movb	%al, 56(%rbx)
.L594:
	movq	%r9, 48(%rbx)
	xorl	%edx, %edx
	movb	$0, (%rdi,%r9)
	movq	72(%r13), %rax
	movq	%rbx, (%r12)
	movq	%rax, 72(%rbx)
	divq	72(%r14)
	movq	64(%r14), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpq	$0, (%rax)
	je	.L645
	movq	0(%r13), %r13
	testq	%r13, %r13
	je	.L584
.L596:
	movq	%rbx, %r12
.L597:
	movl	$80, %edi
	call	_Znwm@PLT
	movq	16(%r13), %r9
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	%rdi, 8(%rax)
	movq	8(%r13), %r10
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L600
	testq	%r10, %r10
	je	.L578
.L600:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	jbe	.L586
	leaq	8(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%rbx)
.L587:
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movq	8(%rbx), %rdi
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L645:
	movq	%r12, (%rax)
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L596
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-96(%rbp), %xmm0
	movq	-72(%rbp), %rax
	movq	%r14, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, (%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	movq	-72(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	leaq	8(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%r12)
.L575:
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	8(%r12), %rdi
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L588:
	testq	%r9, %r9
	je	.L589
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L593:
	testq	%r9, %r9
	je	.L594
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L644:
	leaq	40(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r10
	movq	%rax, 40(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 56(%rbx)
.L592:
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movq	40(%rbx), %rdi
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L582:
	testq	%r15, %r15
	je	.L583
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L576:
	testq	%r15, %r15
	je	.L577
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L643:
	leaq	40(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 40(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 56(%r12)
.L581:
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	40(%r12), %rdi
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L640:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L578:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L641:
	call	_ZSt17__throw_bad_allocv@PLT
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7306:
	.size	_ZNK4node10MapKVStore5CloneEPN2v87IsolateE, .-_ZNK4node10MapKVStore5CloneEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZNK4node10MapKVStore3GetB5cxx11EPKc
	.type	_ZNK4node10MapKVStore3GetB5cxx11EPKc, @function
_ZNK4node10MapKVStore3GetB5cxx11EPKc:
.LFB7290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rsi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	leaq	-96(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%r15, %r15
	je	.L664
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L695
	cmpq	$1, %rax
	jne	.L651
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L652:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-88(%rbp), %rsi
	movl	$3339675911, %edx
	movq	-96(%rbp), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	56(%rbx), %r15
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%r15
	movq	48(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L653
	movq	(%rax), %rbx
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %rdi
	movq	72(%rbx), %rcx
	testq	%r10, %r10
	je	.L654
.L658:
	cmpq	%rcx, %r8
	je	.L696
.L655:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L657
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	je	.L658
.L657:
	cmpq	%r13, %rdi
	je	.L661
	call	_ZdlPv@PLT
.L661:
	leaq	24(%r12), %rax
	movb	$0, (%r12)
	movq	%rax, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
.L663:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L697
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_restore_state
	cmpq	%r8, %rcx
	je	.L698
	.p2align 4,,10
	.p2align 3
.L659:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L657
	movq	72(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r15
	cmpq	%rdx, %r9
	jne	.L657
	cmpq	%r8, %rcx
	jne	.L659
.L698:
	cmpq	$0, 16(%rbx)
	jne	.L659
.L656:
	cmpq	%r13, %rdi
	je	.L662
	call	_ZdlPv@PLT
.L662:
	leaq	24(%r12), %rdi
	movq	48(%rbx), %r13
	movb	$1, (%r12)
	movq	%rdi, 8(%r12)
	movq	40(%rbx), %r15
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L674
	testq	%r15, %r15
	je	.L664
.L674:
	movq	%r13, -104(%rbp)
	cmpq	$15, %r13
	ja	.L699
	cmpq	$1, %r13
	jne	.L668
	movzbl	(%r15), %eax
	movb	%al, 24(%r12)
.L669:
	movq	%r13, 16(%r12)
	movb	$0, (%rdi,%r13)
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L651:
	testq	%rax, %rax
	jne	.L700
	movq	%r13, %rdx
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L696:
	cmpq	16(%rbx), %r10
	jne	.L655
	movq	8(%rbx), %rsi
	movq	%r10, %rdx
	movq	%r10, -128(%rbp)
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	movq	%rdi, -120(%rbp)
	call	memcmp@PLT
	movq	-120(%rbp), %rdi
	movq	-128(%rbp), %r10
	testl	%eax, %eax
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %r9
	je	.L656
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L695:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -128(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-128(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L650:
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L664:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L668:
	testq	%r13, %r13
	je	.L669
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L699:
	leaq	8(%r12), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 24(%r12)
.L667:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r13
	movq	8(%r12), %rdi
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L653:
	movq	-96(%rbp), %rdi
	jmp	.L657
.L697:
	call	__stack_chk_fail@PLT
.L700:
	movq	%r13, %rdi
	jmp	.L650
	.cfi_endproc
.LFE7290:
	.size	_ZNK4node10MapKVStore3GetB5cxx11EPKc, .-_ZNK4node10MapKVStore3GetB5cxx11EPKc
	.align 2
	.p2align 4
	.globl	_ZNK4node10MapKVStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.type	_ZNK4node10MapKVStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE, @function
_ZNK4node10MapKVStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE:
.LFB7291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-1088(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1152, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1072(%rbp), %rdx
	leaq	-1136(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNK4node10MapKVStore3GetB5cxx11EPKc
	cmpb	$0, -1136(%rbp)
	jne	.L702
	xorl	%r12d, %r12d
.L703:
	movq	-1128(%rbp), %rdi
	leaq	-1112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L710
	call	_ZdlPv@PLT
.L710:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L711
	testq	%rdi, %rdi
	je	.L711
	call	free@PLT
.L711:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L727
	addq	$1152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L702:
	.cfi_restore_state
	movq	-1128(%rbp), %r14
	movq	-1120(%rbp), %r13
	leaq	-1152(%rbp), %rbx
	movq	%rbx, -1168(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L704
	testq	%r14, %r14
	je	.L728
.L704:
	movq	%r13, -1176(%rbp)
	cmpq	$15, %r13
	ja	.L729
	cmpq	$1, %r13
	jne	.L707
	movzbl	(%r14), %eax
	movb	%al, -1152(%rbp)
	movq	%rbx, %rax
.L708:
	movq	%r13, -1160(%rbp)
	movq	%r12, %rdi
	xorl	%edx, %edx
	movb	$0, (%rax,%r13)
	movl	-1160(%rbp), %ecx
	movq	-1168(%rbp), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1168(%rbp), %rdi
	movq	%rax, %r12
	cmpq	%rbx, %rdi
	je	.L703
	call	_ZdlPv@PLT
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L707:
	testq	%r13, %r13
	jne	.L730
	movq	%rbx, %rax
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L729:
	leaq	-1168(%rbp), %rdi
	leaq	-1176(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1168(%rbp)
	movq	%rax, %rdi
	movq	-1176(%rbp), %rax
	movq	%rax, -1152(%rbp)
.L706:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-1176(%rbp), %r13
	movq	-1168(%rbp), %rax
	jmp	.L708
.L728:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L727:
	call	__stack_chk_fail@PLT
.L730:
	movq	%rbx, %rdi
	jmp	.L706
	.cfi_endproc
.LFE7291:
	.size	_ZNK4node10MapKVStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE, .-_ZNK4node10MapKVStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node12RealEnvStore3GetB5cxx11EPKc
	.type	_ZNK4node12RealEnvStore3GetB5cxx11EPKc, @function
_ZNK4node12RealEnvStore3GetB5cxx11EPKc:
.LFB7272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-384(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-312(%rbp), %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movdqa	.LC2(%rip), %xmm0
	movq	%rbx, -320(%rbp)
	movq	$256, -384(%rbp)
	movb	$0, -312(%rbp)
	movaps	%xmm0, -336(%rbp)
	call	uv_os_getenv@PLT
	cmpl	$-105, %eax
	je	.L779
.L732:
	movq	-320(%rbp), %r13
	testl	%eax, %eax
	jns	.L780
	leaq	24(%r12), %rax
	movb	$0, (%r12)
	movq	%rax, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
.L751:
	cmpq	%rbx, %r13
	je	.L752
	testq	%r13, %r13
	je	.L752
	movq	%r13, %rdi
	call	free@PLT
.L752:
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L781
	addq	$360, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	movq	-384(%rbp), %r14
	movq	%r13, %rax
	leaq	-352(%rbp), %r15
	movq	%r15, -368(%rbp)
	addq	%r14, %rax
	je	.L739
	testq	%r13, %r13
	je	.L744
.L739:
	movq	%r14, -376(%rbp)
	cmpq	$15, %r14
	ja	.L782
	cmpq	$1, %r14
	jne	.L742
	movzbl	0(%r13), %eax
	movb	%al, -352(%rbp)
	movq	%r15, %rax
.L743:
	movq	%r14, -360(%rbp)
	leaq	24(%r12), %rdi
	movb	$0, (%rax,%r14)
	movq	-368(%rbp), %r14
	movq	-360(%rbp), %r13
	movb	$1, (%r12)
	movq	%r14, %rax
	movq	%rdi, 8(%r12)
	addq	%r13, %rax
	je	.L756
	testq	%r14, %r14
	je	.L744
.L756:
	movq	%r13, -376(%rbp)
	cmpq	$15, %r13
	ja	.L783
	cmpq	$1, %r13
	jne	.L748
	movzbl	(%r14), %eax
	movq	8(%r12), %rdi
	movb	%al, 24(%r12)
.L749:
	movq	%r13, 16(%r12)
	movb	$0, (%rdi,%r13)
	movq	-368(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L750
	call	_ZdlPv@PLT
.L750:
	movq	-320(%rbp), %r13
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L779:
	movq	-320(%rbp), %r8
	movq	-384(%rbp), %r15
	testq	%r8, %r8
	je	.L784
	cmpq	-328(%rbp), %r15
	jbe	.L734
	cmpq	%rbx, %r8
	movl	$0, %edi
	cmovne	%r8, %rdi
	setne	-393(%rbp)
	testq	%r15, %r15
	je	.L785
	movq	%r15, %rsi
	movq	%rdi, -392(%rbp)
	call	realloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L786
.L737:
	cmpb	$0, -393(%rbp)
	movq	%r8, -320(%rbp)
	movq	%r15, -328(%rbp)
	jne	.L734
	movq	-336(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L734
	movq	%r8, %rdi
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-320(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L734:
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%r13, %rdi
	movq	%r15, -336(%rbp)
	call	uv_os_getenv@PLT
	jmp	.L732
	.p2align 4,,10
	.p2align 3
.L742:
	testq	%r14, %r14
	jne	.L787
	movq	%r15, %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L748:
	testq	%r13, %r13
	je	.L749
	jmp	.L747
	.p2align 4,,10
	.p2align 3
.L782:
	leaq	-368(%rbp), %rdi
	leaq	-376(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -368(%rbp)
	movq	%rax, %rdi
	movq	-376(%rbp), %rax
	movq	%rax, -352(%rbp)
.L741:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-376(%rbp), %r14
	movq	-368(%rbp), %rax
	jmp	.L743
	.p2align 4,,10
	.p2align 3
.L783:
	leaq	8(%r12), %rdi
	leaq	-376(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	movq	-376(%rbp), %rax
	movq	%rax, 24(%r12)
.L747:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-376(%rbp), %r13
	movq	8(%r12), %rdi
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L784:
	leaq	_ZZN4node16MaybeStackBufferIcLm256EE25AllocateSufficientStorageEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L785:
	call	free@PLT
	xorl	%r8d, %r8d
	jmp	.L737
.L786:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-392(%rbp), %rdi
	movq	%r15, %rsi
	call	realloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	jne	.L737
	leaq	_ZZN4node7ReallocIcEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L781:
	call	__stack_chk_fail@PLT
.L744:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L787:
	movq	%r15, %rdi
	jmp	.L741
	.cfi_endproc
.LFE7272:
	.size	_ZNK4node12RealEnvStore3GetB5cxx11EPKc, .-_ZNK4node12RealEnvStore3GetB5cxx11EPKc
	.align 2
	.p2align 4
	.globl	_ZNK4node12RealEnvStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.type	_ZNK4node12RealEnvStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE, @function
_ZNK4node12RealEnvStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE:
.LFB7279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	leaq	-1088(%rbp), %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1152, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1072(%rbp), %rdx
	movq	%r13, %rsi
	leaq	-1136(%rbp), %rdi
	xorl	%r13d, %r13d
	call	_ZNK4node12RealEnvStore3GetB5cxx11EPKc
	cmpb	$0, -1136(%rbp)
	jne	.L814
.L796:
	movq	-1128(%rbp), %rdi
	leaq	-1112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L798
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L798
	call	free@PLT
.L798:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L815
	addq	$1152, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_restore_state
	movq	-1128(%rbp), %r14
	movq	-1120(%rbp), %r13
	leaq	-1152(%rbp), %rbx
	movq	%rbx, -1168(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L790
	testq	%r14, %r14
	je	.L816
.L790:
	movq	%r13, -1176(%rbp)
	cmpq	$15, %r13
	ja	.L817
	cmpq	$1, %r13
	jne	.L793
	movzbl	(%r14), %eax
	movb	%al, -1152(%rbp)
	movq	%rbx, %rax
.L794:
	movq	%r13, -1160(%rbp)
	movq	%r12, %rdi
	xorl	%edx, %edx
	movb	$0, (%rax,%r13)
	movl	-1160(%rbp), %ecx
	movq	-1168(%rbp), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1168(%rbp), %rdi
	movq	%rax, %r13
	cmpq	%rbx, %rdi
	je	.L796
	call	_ZdlPv@PLT
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L793:
	testq	%r13, %r13
	jne	.L818
	movq	%rbx, %rax
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L817:
	leaq	-1168(%rbp), %rdi
	leaq	-1176(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1168(%rbp)
	movq	%rax, %rdi
	movq	-1176(%rbp), %rax
	movq	%rax, -1152(%rbp)
.L792:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-1176(%rbp), %r13
	movq	-1168(%rbp), %rax
	jmp	.L794
.L815:
	call	__stack_chk_fail@PLT
.L818:
	movq	%rbx, %rdi
	jmp	.L792
.L816:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7279:
	.size	_ZNK4node12RealEnvStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE, .-_ZNK4node12RealEnvStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7KVStore16CreateMapKVStoreEv
	.type	_ZN4node7KVStore16CreateMapKVStoreEv, @function
_ZN4node7KVStore16CreateMapKVStoreEv:
.LFB7313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	movl	$120, %edi
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	call	_Znwm@PLT
	movl	$13, %ecx
	movq	%rax, %rbx
	movabsq	$4294967297, %rax
	movq	%rax, 8(%rbx)
	leaq	16(%rbx), %r12
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%r12, %rdi
	xorl	%eax, %eax
	rep stosq
	leaq	16+_ZTVN4node10MapKVStoreE(%rip), %rax
	leaq	24(%rbx), %rdi
	movq	%rax, 16(%rbx)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L822
	movq	%r12, %xmm0
	movq	%rbx, %xmm1
	leaq	112(%rbx), %rax
	movq	$1, 72(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 64(%rbx)
	movq	%r13, %rax
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movl	$0x3f800000, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movups	%xmm0, 0(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7313:
	.size	_ZN4node7KVStore16CreateMapKVStoreEv, .-_ZN4node7KVStore16CreateMapKVStoreEv
	.p2align 4
	.globl	_ZN4node17CreateEnvVarProxyEN2v85LocalINS0_7ContextEEEPNS0_7IsolateENS1_INS0_6ObjectEEE
	.type	_ZN4node17CreateEnvVarProxyEN2v85LocalINS0_7ContextEEEPNS0_7IsolateENS1_INS0_6ObjectEEE, @function
_ZN4node17CreateEnvVarProxyEN2v85LocalINS0_7ContextEEEPNS0_7IsolateENS1_INS0_6ObjectEEE:
.LFB7320:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-144(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v814ObjectTemplate3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	leaq	_ZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEE(%rip), %rcx
	leaq	-112(%rbp), %rsi
	movq	$0, -72(%rbp)
	movq	%rax, %r12
	leaq	_ZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EE(%rip), %rax
	movq	%rcx, %xmm0
	movq	$0, -64(%rbp)
	movq	%rax, %xmm1
	leaq	_ZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rbx, -56(%rbp)
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEE(%rip), %rax
	movl	$8, -48(%rbp)
	movq	%rax, %xmm2
	movaps	%xmm0, -112(%rbp)
	movq	%rcx, %xmm0
	leaq	_ZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEE(%rip), %rax
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -80(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v814ObjectTemplate10SetHandlerERKNS_33NamedPropertyHandlerConfigurationE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L826
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L826:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7320:
	.size	_ZN4node17CreateEnvVarProxyEN2v85LocalINS0_7ContextEEEPNS0_7IsolateENS1_INS0_6ObjectEEE, .-_ZN4node17CreateEnvVarProxyEN2v85LocalINS0_7ContextEEEPNS0_7IsolateENS1_INS0_6ObjectEEE
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_:
.LFB8559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	0(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L828
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	72(%rbx), %rsi
.L831:
	cmpq	%rsi, %r15
	je	.L872
.L829:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L828
	movq	72(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L831
.L828:
	movl	$80, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	leaq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L873
	movq	%rdx, 8(%rbx)
	movq	16(%r13), %rdx
	movq	%rdx, 24(%rbx)
.L833:
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	leaq	56(%rbx), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r13)
	movl	$1, %ecx
	movb	$0, 16(%r13)
	movq	8(%r12), %rsi
	movq	%rdx, 16(%rbx)
	movq	24(%r12), %rdx
	movq	%rax, 40(%rbx)
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L834
	movq	(%r12), %r8
	movq	%r15, 72(%rbx)
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L844
.L876:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L845:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L872:
	.cfi_restore_state
	movq	8(%r13), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L829
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L830
	movq	8(%rbx), %rsi
	movq	0(%r13), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L829
.L830:
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L834:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L874
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L875
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L837:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L839
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L841:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L842:
	testq	%rsi, %rsi
	je	.L839
.L840:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	72(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L841
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L848
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L840
	.p2align 4,,10
	.p2align 3
.L839:
	movq	(%r12), %rdi
	cmpq	%rdi, %r10
	je	.L843
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L843:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r8, (%r12)
	movq	%r15, 72(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L876
.L844:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L846
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
.L846:
	leaq	16(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L873:
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 24(%rbx)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L848:
	movq	%rdx, %rdi
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L874:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L837
.L875:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8559:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10MapKVStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_
	.type	_ZN4node10MapKVStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_, @function
_ZN4node10MapKVStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_:
.LFB7292:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	8(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$2216, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	%r12, %rsi
	leaq	-2160(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	leaq	-1104(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r13, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2144(%rbp), %rdi
	movq	-1088(%rbp), %r12
	testq	%rdi, %rdi
	je	.L879
	cmpq	$0, -2160(%rbp)
	jne	.L931
.L879:
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %r12
	je	.L930
	testq	%r12, %r12
	je	.L930
	movq	%r12, %rdi
	call	free@PLT
.L930:
	movq	-2144(%rbp), %rdi
.L880:
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L900
	testq	%rdi, %rdi
	je	.L900
	call	free@PLT
.L900:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L932
	addq	$2216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L931:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L880
	movq	-1104(%rbp), %r15
	leaq	-2176(%rbp), %r13
	leaq	-2192(%rbp), %rdi
	movq	%r13, -2192(%rbp)
	movq	%r15, -2232(%rbp)
	cmpq	$15, %r15
	ja	.L933
	cmpq	$1, %r15
	jne	.L883
	movzbl	(%r12), %eax
	movb	%al, -2176(%rbp)
	movq	%r13, %rax
.L884:
	movq	%r15, -2184(%rbp)
	leaq	-2208(%rbp), %r12
	movb	$0, (%rax,%r15)
	movq	-2144(%rbp), %r8
	movq	-2160(%rbp), %r15
	movq	%r12, -2224(%rbp)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L885
	testq	%r8, %r8
	je	.L934
.L885:
	movq	%r15, -2232(%rbp)
	cmpq	$15, %r15
	ja	.L935
	cmpq	$1, %r15
	jne	.L888
	movzbl	(%r8), %eax
	leaq	-2224(%rbp), %r9
	movb	%al, -2208(%rbp)
	movq	%r12, %rax
.L889:
	movq	%r15, -2216(%rbp)
	leaq	48(%rbx), %rdi
	movq	%r9, %rsi
	movb	$0, (%rax,%r15)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_S6_ESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movq	(%rax), %rdi
	movq	%rax, %rbx
	movq	-2192(%rbp), %rax
	cmpq	%r13, %rax
	je	.L936
	leaq	16(%rbx), %rdx
	movq	-2184(%rbp), %rcx
	cmpq	%rdx, %rdi
	je	.L937
	movq	%rax, (%rbx)
	movq	16(%rbx), %rdx
	movq	%rcx, 8(%rbx)
	movq	-2176(%rbp), %rax
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L895
	movq	%rdi, -2192(%rbp)
	movq	%rdx, -2176(%rbp)
.L893:
	movq	$0, -2184(%rbp)
	movb	$0, (%rdi)
	movq	-2224(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L896
	call	_ZdlPv@PLT
.L896:
	movq	-2192(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L897
	call	_ZdlPv@PLT
.L897:
	movq	-1088(%rbp), %r12
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L888:
	testq	%r15, %r15
	jne	.L938
	movq	%r12, %rax
	leaq	-2224(%rbp), %r9
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L883:
	testq	%r15, %r15
	jne	.L939
	movq	%r13, %rax
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L933:
	leaq	-2232(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -2192(%rbp)
	movq	%rax, %rdi
	movq	-2232(%rbp), %rax
	movq	%rax, -2176(%rbp)
.L882:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-2232(%rbp), %r15
	movq	-2192(%rbp), %rax
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L935:
	leaq	-2224(%rbp), %r9
	leaq	-2232(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -2256(%rbp)
	movq	%r9, %rdi
	movq	%r9, -2248(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2248(%rbp), %r9
	movq	-2256(%rbp), %r8
	movq	%rax, -2224(%rbp)
	movq	%rax, %rdi
	movq	-2232(%rbp), %rax
	movq	%rax, -2208(%rbp)
.L887:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r9, -2248(%rbp)
	call	memcpy@PLT
	movq	-2232(%rbp), %r15
	movq	-2224(%rbp), %rax
	movq	-2248(%rbp), %r9
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L936:
	movq	-2184(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L891
	cmpq	$1, %rdx
	je	.L940
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-2184(%rbp), %rdx
	movq	(%rbx), %rdi
.L891:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-2192(%rbp), %rdi
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L937:
	movq	%rax, (%rbx)
	movq	%rcx, 8(%rbx)
	movq	-2176(%rbp), %rax
	movq	%rax, 16(%rbx)
.L895:
	movq	%r13, -2192(%rbp)
	leaq	-2176(%rbp), %r13
	movq	%r13, %rdi
	jmp	.L893
.L940:
	movzbl	-2176(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2184(%rbp), %rdx
	movq	(%rbx), %rdi
	jmp	.L891
.L932:
	call	__stack_chk_fail@PLT
.L934:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L939:
	movq	%r13, %rdi
	jmp	.L882
.L938:
	movq	%r12, %rdi
	leaq	-2224(%rbp), %r9
	jmp	.L887
	.cfi_endproc
.LFE7292:
	.size	_ZN4node10MapKVStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_, .-_ZN4node10MapKVStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11per_process13env_var_mutexE, @function
_GLOBAL__sub_I__ZN4node11per_process13env_var_mutexE:
.LFB9931:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L944
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rsi
	leaq	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev(%rip), %rdi
	call	__cxa_atexit@PLT
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node12RealEnvStoreE(%rip), %rsi
	movabsq	$4294967297, %rcx
	leaq	_ZNSt10shared_ptrIN4node7KVStoreEED1Ev(%rip), %rdi
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	leaq	16(%rax), %rdx
	movq	%rsi, 16(%rax)
	leaq	_ZN4node11per_process18system_environmentE(%rip), %rsi
	movq	%rcx, (%rax)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rdx, _ZN4node11per_process18system_environmentE(%rip)
	leaq	__dso_handle(%rip), %rdx
	movq	%rax, 8+_ZN4node11per_process18system_environmentE(%rip)
	jmp	__cxa_atexit@PLT
.L944:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9931:
	.size	_GLOBAL__sub_I__ZN4node11per_process13env_var_mutexE, .-_GLOBAL__sub_I__ZN4node11per_process13env_var_mutexE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11per_process13env_var_mutexE
	.weak	_ZTVN4node12RealEnvStoreE
	.section	.data.rel.ro.local._ZTVN4node12RealEnvStoreE,"awG",@progbits,_ZTVN4node12RealEnvStoreE,comdat
	.align 8
	.type	_ZTVN4node12RealEnvStoreE, @object
	.size	_ZTVN4node12RealEnvStoreE, 104
_ZTVN4node12RealEnvStoreE:
	.quad	0
	.quad	0
	.quad	_ZN4node12RealEnvStoreD1Ev
	.quad	_ZN4node12RealEnvStoreD0Ev
	.quad	_ZNK4node12RealEnvStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.quad	_ZNK4node12RealEnvStore3GetB5cxx11EPKc
	.quad	_ZN4node12RealEnvStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_
	.quad	_ZNK4node12RealEnvStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.quad	_ZNK4node12RealEnvStore5QueryEPKc
	.quad	_ZN4node12RealEnvStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.quad	_ZNK4node12RealEnvStore9EnumerateEPN2v87IsolateE
	.quad	_ZNK4node7KVStore5CloneEPN2v87IsolateE
	.quad	_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.weak	_ZTVN4node7KVStoreE
	.section	.data.rel.ro._ZTVN4node7KVStoreE,"awG",@progbits,_ZTVN4node7KVStoreE,comdat
	.align 8
	.type	_ZTVN4node7KVStoreE, @object
	.size	_ZTVN4node7KVStoreE, 104
_ZTVN4node7KVStoreE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node7KVStore5CloneEPN2v87IsolateE
	.quad	_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.weak	_ZTVN4node10MapKVStoreE
	.section	.data.rel.ro.local._ZTVN4node10MapKVStoreE,"awG",@progbits,_ZTVN4node10MapKVStoreE,comdat
	.align 8
	.type	_ZTVN4node10MapKVStoreE, @object
	.size	_ZTVN4node10MapKVStoreE, 104
_ZTVN4node10MapKVStoreE:
	.quad	0
	.quad	0
	.quad	_ZN4node10MapKVStoreD1Ev
	.quad	_ZN4node10MapKVStoreD0Ev
	.quad	_ZNK4node10MapKVStore3GetEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.quad	_ZNK4node10MapKVStore3GetB5cxx11EPKc
	.quad	_ZN4node10MapKVStore3SetEPN2v87IsolateENS1_5LocalINS1_6StringEEES6_
	.quad	_ZNK4node10MapKVStore5QueryEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.quad	_ZNK4node10MapKVStore5QueryEPKc
	.quad	_ZN4node10MapKVStore6DeleteEPN2v87IsolateENS1_5LocalINS1_6StringEEE
	.quad	_ZNK4node10MapKVStore9EnumerateEPN2v87IsolateE
	.quad	_ZNK4node10MapKVStore5CloneEPN2v87IsolateE
	.quad	_ZN4node7KVStore16AssignFromObjectEN2v85LocalINS1_7ContextEEENS2_INS1_6ObjectEEE
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node12RealEnvStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node10MapKVStoreESaIS1_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node7ReallocIcEEPT_S2_mE4args
	.section	.rodata.str1.1
.LC10:
	.string	"../src/util-inl.h:374"
.LC11:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"T* node::Realloc(T*, size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIcEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIcEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIcEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIcEEPT_S2_mE4args, 24
_ZZN4node7ReallocIcEEPT_S2_mE4args:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC13
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC14:
	.string	"../src/util-inl.h:325"
.LC15:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm256EEixEmE4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/util.h:352"
.LC18:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 256; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm256EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm256EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm256EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm256EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm256EEixEmE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.weak	_ZZN4node16MaybeStackBufferIcLm256EE25AllocateSufficientStorageEmE4args
	.section	.rodata.str1.1
.LC20:
	.string	"../src/util.h:376"
.LC21:
	.string	"!IsInvalidated()"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::AllocateSufficientStorage(size_t) [with T = char; long unsigned int kStackStorageSize = 256; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIcLm256EE25AllocateSufficientStorageEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIcLm256EE25AllocateSufficientStorageEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIcLm256EE25AllocateSufficientStorageEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIcLm256EE25AllocateSufficientStorageEmE4args, 24
_ZZN4node16MaybeStackBufferIcLm256EE25AllocateSufficientStorageEmE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC23:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC25:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.section	.rodata.str1.1
.LC26:
	.string	"../src/node_env_var.cc:357"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC28:
	.string	"void node::EnvEnumerator(const v8::PropertyCallbackInfo<v8::Array>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEEE4args, @object
	.size	_ZZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEEE4args, 24
_ZZN4nodeL13EnvEnumeratorERKN2v820PropertyCallbackInfoINS0_5ArrayEEEE4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"../src/node_env_var.cc:345"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"void node::EnvDeleter(v8::Local<v8::Name>, const v8::PropertyCallbackInfo<v8::Boolean>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEEE4args, @object
	.size	_ZZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEEE4args, 24
_ZZN4nodeL10EnvDeleterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7BooleanEEEE4args:
	.quad	.LC29
	.quad	.LC27
	.quad	.LC30
	.section	.rodata.str1.1
.LC31:
	.string	"../src/node_env_var.cc:335"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"void node::EnvQuery(v8::Local<v8::Name>, const v8::PropertyCallbackInfo<v8::Integer>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEEE4args, @object
	.size	_ZZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEEE4args, 24
_ZZN4nodeL8EnvQueryEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_7IntegerEEEE4args:
	.quad	.LC31
	.quad	.LC27
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/node_env_var.cc:301"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"void node::EnvSetter(v8::Local<v8::Name>, v8::Local<v8::Value>, const v8::PropertyCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EEE4args, @object
	.size	_ZZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EEE4args, 24
_ZZN4nodeL9EnvSetterEN2v85LocalINS0_4NameEEENS1_INS0_5ValueEEERKNS0_20PropertyCallbackInfoIS4_EEE4args:
	.quad	.LC33
	.quad	.LC27
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"../src/node_env_var.cc:289"
.LC36:
	.string	"property->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"void node::EnvGetter(v8::Local<v8::Name>, const v8::PropertyCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args_0, @object
	.size	_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args_0, 24
_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args_0:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.section	.rodata.str1.1
.LC38:
	.string	"../src/node_env_var.cc:285"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL9EnvGetterEN2v85LocalINS0_4NameEEERKNS0_20PropertyCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC38
	.quad	.LC27
	.quad	.LC37
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_env_var.cc:186"
.LC40:
	.string	"key->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"virtual std::shared_ptr<node::KVStore> node::KVStore::Clone(v8::Isolate*) const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node7KVStore5CloneEPN2v87IsolateEE4args, @object
	.size	_ZZNK4node7KVStore5CloneEPN2v87IsolateEE4args, 24
_ZZNK4node7KVStore5CloneEPN2v87IsolateEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"../src/node_env_var.cc:154"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"(uv_os_environ(&items, &count)) == (0)"
	.align 8
.LC44:
	.string	"virtual v8::Local<v8::Array> node::RealEnvStore::Enumerate(v8::Isolate*) const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node12RealEnvStore9EnumerateEPN2v87IsolateEE4args, @object
	.size	_ZZNK4node12RealEnvStore9EnumerateEPN2v87IsolateEE4args, 24
_ZZNK4node12RealEnvStore9EnumerateEPN2v87IsolateEE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.globl	_ZN4node11per_process18system_environmentE
	.bss
	.align 16
	.type	_ZN4node11per_process18system_environmentE, @object
	.size	_ZN4node11per_process18system_environmentE, 16
_ZN4node11per_process18system_environmentE:
	.zero	16
	.globl	_ZN4node11per_process13env_var_mutexE
	.align 32
	.type	_ZN4node11per_process13env_var_mutexE, @object
	.size	_ZN4node11per_process13env_var_mutexE, 40
_ZN4node11per_process13env_var_mutexE:
	.zero	40
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	0
	.quad	256
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
