	.file	"node_crypto_common.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	", "
.LC1:
	.string	"DNS:"
	.text
	.p2align 4
	.type	_ZN4node6crypto12_GLOBAL__N_116SafeX509ExtPrintERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS3_XadL_Z12BIO_free_allEEEEEP17X509_extension_st, @function
_ZN4node6crypto12_GLOBAL__N_116SafeX509ExtPrintERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS3_XadL_Z12BIO_free_allEEEEEP17X509_extension_st:
.LFB9055:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	X509V3_EXT_get@PLT
	movl	$85, %edi
	movq	%rax, %r14
	call	X509V3_EXT_get_nid@PLT
	cmpq	%rax, %r14
	je	.L2
.L4:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	%r13, %rdi
	call	X509V3_EXT_d2i@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L4
	xorl	%ebx, %ebx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	i2v_GENERAL_NAME@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L4
	movq	(%r12), %rdi
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	X509V3_EXT_val_prn@PLT
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r15, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L8:
	addl	$1, %ebx
.L9:
	movq	%r13, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jge	.L5
	movl	%ebx, %esi
	movq	%r13, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %r15
	testl	%ebx, %ebx
	jne	.L21
.L6:
	cmpl	$2, (%r15)
	jne	.L7
	movq	8(%r15), %r15
	movq	(%r12), %rdi
	movl	$4, %edx
	leaq	.LC1(%rip), %rsi
	call	BIO_write@PLT
	movq	8(%r15), %rsi
	movl	(%r15), %edx
	movq	(%r12), %rdi
	call	BIO_write@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r12), %rdi
	movl	$2, %edx
	leaq	.LC0(%rip), %rsi
	call	BIO_write@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L5:
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	call	OPENSSL_sk_pop_free@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9055:
	.size	_ZN4node6crypto12_GLOBAL__N_116SafeX509ExtPrintERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS3_XadL_Z12BIO_free_allEEEEEP17X509_extension_st, .-_ZN4node6crypto12_GLOBAL__N_116SafeX509ExtPrintERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS3_XadL_Z12BIO_free_allEEEEEP17X509_extension_st
	.p2align 4
	.type	_ZN4node6crypto12_GLOBAL__N_13SetIN2v86ObjectEEEbNS3_5LocalINS3_7ContextEEENS5_IS4_EENS5_INS3_5ValueEEENS3_10MaybeLocalIT_EE, @function
_ZN4node6crypto12_GLOBAL__N_13SetIN2v86ObjectEEEbNS3_5LocalINS3_7ContextEEENS5_IS4_EENS5_INS3_5ValueEEENS3_10MaybeLocalIT_EE:
.LFB10000:
	.cfi_startproc
	testq	%rcx, %rcx
	je	.L36
	movq	%rsi, %r8
	movq	(%rcx), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	jne	.L25
	movq	-1(%rsi), %rax
	cmpw	$67, 11(%rax)
	jne	.L25
	cmpl	$5, 43(%rsi)
	movl	$1, %eax
	je	.L34
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10000:
	.size	_ZN4node6crypto12_GLOBAL__N_13SetIN2v86ObjectEEEbNS3_5LocalINS3_7ContextEEENS5_IS4_EENS5_INS3_5ValueEEENS3_10MaybeLocalIT_EE, .-_ZN4node6crypto12_GLOBAL__N_13SetIN2v86ObjectEEEbNS3_5LocalINS3_7ContextEEENS5_IS4_EENS5_INS3_5ValueEEENS3_10MaybeLocalIT_EE
	.p2align 4
	.type	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE, @function
_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE:
.LFB10062:
	.cfi_startproc
	testq	%rcx, %rcx
	je	.L51
	movq	%rsi, %r8
	movq	(%rcx), %rsi
	movq	%rsi, %rax
	andl	$3, %eax
	cmpq	$1, %rax
	jne	.L40
	movq	-1(%rsi), %rax
	cmpw	$67, 11(%rax)
	jne	.L40
	cmpl	$5, 43(%rsi)
	movl	$1, %eax
	je	.L49
.L40:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%r8, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10062:
	.size	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE, .-_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	.p2align 4
	.type	_ZN4node6crypto12_GLOBAL__N_19ToV8ValueEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS5_XadL_Z12BIO_free_allEEEEE, @function
_ZN4node6crypto12_GLOBAL__N_19ToV8ValueEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS5_XadL_Z12BIO_free_allEEEEE:
.LFB9036:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	leaq	-32(%rbp), %rcx
	movl	$115, %esi
	subq	$16, %rsp
	movq	(%rbx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	BIO_ctrl@PLT
	movq	-32(%rbp), %rax
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, %r12
	call	BIO_ctrl@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L55
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L55:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9036:
	.size	_ZN4node6crypto12_GLOBAL__N_19ToV8ValueEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS5_XadL_Z12BIO_free_allEEEEE, .-_ZN4node6crypto12_GLOBAL__N_19ToV8ValueEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS5_XadL_Z12BIO_free_allEEEEE
	.p2align 4
	.type	_ZN4node6crypto12_GLOBAL__N_120GetFingerprintDigestEPNS_11EnvironmentEPK9evp_md_stP7x509_st, @function
_ZN4node6crypto12_GLOBAL__N_120GetFingerprintDigestEPNS_11EnvironmentEPK9evp_md_stP7x509_st:
.LFB9056:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	leaq	-288(%rbp), %rbx
	leaq	-324(%rbp), %rcx
	movq	%rbx, %rdx
	subq	$336, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	X509_digest@PLT
	testl	%eax, %eax
	jne	.L69
	movq	352(%r12), %rax
	addq	$88, %rax
.L63:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L70
	addq	$336, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
	movdqa	.LC2(%rip), %xmm0
	movl	-324(%rbp), %r8d
	movb	$0, -304(%rbp)
	movaps	%xmm0, -320(%rbp)
	testl	%r8d, %r8d
	je	.L58
	leal	-1(%r8), %r9d
	movq	%rbx, %rax
	leaq	-223(%rbp), %rcx
	leaq	-287(%rbp,%r9), %rdi
	.p2align 4,,10
	.p2align 3
.L59:
	movzbl	(%rax), %edx
	addq	$1, %rax
	addq	$3, %rcx
	movl	%edx, %esi
	andl	$15, %edx
	shrb	$4, %sil
	movzbl	-320(%rbp,%rdx), %edx
	andl	$15, %esi
	movzbl	-320(%rbp,%rsi), %esi
	movb	%dl, -3(%rcx)
	movb	%sil, -4(%rcx)
	cmpq	%rdi, %rax
	jne	.L59
	leaq	(%r9,%r9,2), %rdx
	leaq	-222(%rbp), %rax
	leaq	-219(%rbp,%rdx), %rdx
	.p2align 4,,10
	.p2align 3
.L61:
	movb	$58, (%rax)
	addq	$3, %rax
	cmpq	%rax, %rdx
	jne	.L61
	leal	-1(%r8,%r8,2), %eax
	movb	$0, -224(%rbp,%rax)
.L64:
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	-224(%rbp), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L63
	movq	%rax, -344(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-344(%rbp), %rax
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L58:
	movb	$0, -224(%rbp)
	jmp	.L64
.L70:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9056:
	.size	_ZN4node6crypto12_GLOBAL__N_120GetFingerprintDigestEPNS_11EnvironmentEPK9evp_md_stP7x509_st, .-_ZN4node6crypto12_GLOBAL__N_120GetFingerprintDigestEPNS_11EnvironmentEPK9evp_md_stP7x509_st
	.p2align 4
	.globl	_ZN4node6crypto18SSL_CTX_get_issuerEP10ssl_ctx_stP7x509_stPS4_
	.type	_ZN4node6crypto18SSL_CTX_get_issuerEP10ssl_ctx_stP7x509_stPS4_, @function
_ZN4node6crypto18SSL_CTX_get_issuerEP10ssl_ctx_stP7x509_stPS4_:
.LFB8986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$8, %rsp
	call	SSL_CTX_get_cert_store@PLT
	movq	%rax, %r15
	call	X509_STORE_CTX_new@PLT
	testq	%rax, %rax
	je	.L71
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	X509_STORE_CTX_init@PLT
	cmpl	$1, %eax
	je	.L78
.L73:
	movq	%r12, %rdi
	call	X509_STORE_CTX_free@PLT
.L71:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	xorl	%ebx, %ebx
	call	X509_STORE_CTX_get1_issuer@PLT
	cmpl	$1, %eax
	sete	%bl
	jmp	.L73
	.cfi_endproc
.LFE8986:
	.size	_ZN4node6crypto18SSL_CTX_get_issuerEP10ssl_ctx_stP7x509_stPS4_, .-_ZN4node6crypto18SSL_CTX_get_issuerEP10ssl_ctx_stP7x509_stPS4_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata.str1.1
.LC4:
	.string	" "
	.text
	.p2align 4
	.globl	_ZN4node6crypto9LogSecretERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKcPKhm
	.type	_ZN4node6crypto9LogSecretERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKcPKhm, @function
_ZN4node6crypto9LogSecretERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKcPKhm:
.LFB8989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%rdx, -216(%rbp)
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_get_SSL_CTX@PLT
	movq	%rax, %rdi
	call	SSL_CTX_get_keylog_callback@PLT
	testq	%rax, %rax
	je	.L79
	leaq	-96(%rbp), %r14
	movq	(%rbx), %rdi
	movl	$32, %edx
	movq	%rax, %r12
	movq	%r14, %rsi
	call	SSL_get_client_random@PLT
	cmpq	$32, %rax
	je	.L100
.L79:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
	leaq	-192(%rbp), %rax
	movq	%rax, -224(%rbp)
	leaq	-176(%rbp), %rax
	movq	%rax, -232(%rbp)
	movq	%rax, -192(%rbp)
	testq	%r13, %r13
	je	.L102
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -200(%rbp)
	movq	%rax, %r9
	cmpq	$15, %rax
	ja	.L103
	cmpq	$1, %rax
	jne	.L87
	movzbl	0(%r13), %edx
	movb	%dl, -176(%rbp)
	movq	-232(%rbp), %rdx
.L88:
	movq	%rax, -184(%rbp)
	leaq	-160(%rbp), %r13
	movq	%r14, %rsi
	movb	$0, (%rdx,%rax)
	movq	%r13, %rdi
	movl	$32, %edx
	call	_ZN4node11StringBytes10hex_encodeB5cxx11EPKcm@PLT
	leaq	.LC4(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	-112(%rbp), %rsi
	movq	%rsi, -128(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	movq	%rsi, %r14
	cmpq	%rdx, %rcx
	je	.L104
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L90:
	movq	8(%rax), %rcx
	movq	-224(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movb	$0, 16(%rax)
	movq	-128(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	-120(%rbp), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L91
	call	_ZdlPv@PLT
.L91:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	movq	%rax, -240(%rbp)
	cmpq	%rax, %rdi
	je	.L92
	call	_ZdlPv@PLT
.L92:
	movq	-216(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN4node11StringBytes10hex_encodeB5cxx11EPKcm@PLT
	leaq	.LC4(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r8d
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r14, -128(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L105
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L94:
	movq	8(%rax), %rcx
	movq	-224(%rbp), %rdi
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movb	$0, 16(%rax)
	movq	-128(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	-120(%rbp), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L95
	call	_ZdlPv@PLT
.L95:
	movq	-160(%rbp), %rdi
	cmpq	-240(%rbp), %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	movq	(%rbx), %rdi
	movq	-192(%rbp), %rsi
	call	*%r12
	movq	-192(%rbp), %rdi
	cmpq	-232(%rbp), %rdi
	je	.L79
	call	_ZdlPv@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-224(%rbp), %rdi
	leaq	-200(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-240(%rbp), %r9
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-200(%rbp), %rax
	movq	%rax, -176(%rbp)
.L86:
	movq	%r9, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-200(%rbp), %rax
	movq	-192(%rbp), %rdx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	testq	%rax, %rax
	jne	.L106
	movq	-232(%rbp), %rdx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L105:
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -112(%rbp)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L104:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -112(%rbp)
	jmp	.L90
.L101:
	call	__stack_chk_fail@PLT
.L106:
	movq	-232(%rbp), %rdi
	jmp	.L86
	.cfi_endproc
.LFE8989:
	.size	_ZN4node6crypto9LogSecretERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKcPKhm, .-_ZN4node6crypto9LogSecretERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKcPKhm
	.p2align 4
	.globl	_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	8(%rsi), %edx
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	SSL_set_alpn_protos@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	sete	%al
	ret
	.cfi_endproc
.LFE8990:
	.size	_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEN2v85LocalINS8_5ValueEEE
	.type	_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEN2v85LocalINS8_5ValueEEE, @function
_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEN2v85LocalINS8_5ValueEEE:
.LFB8991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L119
.L109:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L120
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -56(%rbp)
	cmpq	$64, %rax
	ja	.L113
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L121
.L113:
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-192(%rbp), %r13
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	leaq	0(%r13,%rax), %rsi
	movq	%rsi, -64(%rbp)
.L112:
	movl	-56(%rbp), %edx
	movq	(%rbx), %rdi
	call	SSL_set_alpn_protos@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	-128(%rbp), %r13
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r13, -64(%rbp)
	movq	%r13, %rsi
	jmp	.L112
.L120:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8991:
	.size	_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEN2v85LocalINS8_5ValueEEE, .-_ZN4node6crypto7SetALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEN2v85LocalINS8_5ValueEEE
	.p2align 4
	.globl	_ZN4node6crypto18GetSSLOCSPResponseEPNS_11EnvironmentEP6ssl_stN2v85LocalINS5_5ValueEEE
	.type	_ZN4node6crypto18GetSSLOCSPResponseEPNS_11EnvironmentEP6ssl_stN2v85LocalINS5_5ValueEEE, @function
_ZN4node6crypto18GetSSLOCSPResponseEPNS_11EnvironmentEP6ssl_stN2v85LocalINS5_5ValueEEE:
.LFB8992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	movl	$70, %esi
	.cfi_offset 3, -32
	movq	%rdx, %rbx
	leaq	-32(%rbp), %rcx
	xorl	%edx, %edx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	SSL_ctrl@PLT
	movq	-32(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L129
	movslq	%eax, %rdx
	movq	%r12, %rdi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
.L124:
	movq	-24(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L130
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%rbx, %rax
	jmp	.L124
.L130:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8992:
	.size	_ZN4node6crypto18GetSSLOCSPResponseEPNS_11EnvironmentEP6ssl_stN2v85LocalINS5_5ValueEEE, .-_ZN4node6crypto18GetSSLOCSPResponseEPNS_11EnvironmentEP6ssl_stN2v85LocalINS5_5ValueEEE
	.p2align 4
	.globl	_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKhm
	.type	_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKhm, @function
_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKhm:
.LFB8993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$24, %rsp
	movq	%rsi, -40(%rbp)
	leaq	-40(%rbp), %rsi
	call	d2i_SSL_SESSION@PLT
	testq	%rax, %rax
	je	.L131
	movq	(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	SSL_set_session@PLT
	movq	%r12, %rdi
	cmpl	$1, %eax
	sete	%r13b
	call	SSL_SESSION_free@PLT
.L131:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8993:
	.size	_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKhm, .-_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEPKhm
	.p2align 4
	.globl	_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKS1_I14ssl_session_stNS3_IS8_XadL_Z16SSL_SESSION_freeEEEEE
	.type	_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKS1_I14ssl_session_stNS3_IS8_XadL_Z16SSL_SESSION_freeEEEEE, @function
_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKS1_I14ssl_session_stNS3_IS8_XadL_Z16SSL_SESSION_freeEEEEE:
.LFB8994:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L141
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	SSL_set_session@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$1, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8994:
	.size	_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKS1_I14ssl_session_stNS3_IS8_XadL_Z16SSL_SESSION_freeEEEEE, .-_ZN4node6crypto13SetTLSSessionERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEERKS1_I14ssl_session_stNS3_IS8_XadL_Z16SSL_SESSION_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto13GetTLSSessionEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node6crypto13GetTLSSessionEN2v85LocalINS1_5ValueEEE, @function
_ZN4node6crypto13GetTLSSessionEN2v85LocalINS1_5ValueEEE:
.LFB8995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$160, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L147
	movq	$0, 0(%r13)
.L146:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L154
	addq	$160, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -56(%rbp)
	cmpq	$64, %rax
	jbe	.L149
.L151:
	movq	%r12, %rdi
	leaq	-192(%rbp), %r14
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-192(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rbx, %rax
	movq	%rax, -64(%rbp)
.L150:
	movq	-56(%rbp), %rdx
	movq	%r14, %rsi
	xorl	%edi, %edi
	movq	%rax, -192(%rbp)
	call	d2i_SSL_SESSION@PLT
	movq	%rax, 0(%r13)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	jne	.L151
	leaq	-128(%rbp), %rbx
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	leaq	-192(%rbp), %r14
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%rbx, -64(%rbp)
	movq	%rbx, %rax
	jmp	.L150
.L154:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8995:
	.size	_ZN4node6crypto13GetTLSSessionEN2v85LocalINS1_5ValueEEE, .-_ZN4node6crypto13GetTLSSessionEN2v85LocalINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node6crypto13GetTLSSessionEPKhm
	.type	_ZN4node6crypto13GetTLSSessionEPKhm, @function
_ZN4node6crypto13GetTLSSessionEPKhm:
.LFB8996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	xorl	%edi, %edi
	subq	$24, %rsp
	movq	%rsi, -24(%rbp)
	leaq	-24(%rbp), %rsi
	call	d2i_SSL_SESSION@PLT
	movq	%rax, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8996:
	.size	_ZN4node6crypto13GetTLSSessionEPKhm, .-_ZN4node6crypto13GetTLSSessionEPKhm
	.section	.rodata.str1.1
.LC5:
	.string	"CN"
	.text
	.p2align 4
	.globl	_ZN4node6crypto16GetCertificateCNB5cxx11EP7x509_st
	.type	_ZN4node6crypto16GetCertificateCNB5cxx11EP7x509_st, @function
_ZN4node6crypto16GetCertificateCNB5cxx11EP7x509_st:
.LFB9024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	leaq	16(%r12), %r14
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	X509_get_subject_name@PLT
	testq	%rax, %rax
	je	.L158
	leaq	.LC5(%rip), %rdi
	movq	%rax, %r13
	call	OBJ_txt2nid@PLT
	movl	$-1, %edx
	movq	%r13, %rdi
	movl	%eax, %esi
	call	X509_NAME_get_index_by_NID@PLT
	movl	%eax, %esi
	cmpl	$-1, %eax
	jne	.L182
.L158:
	movq	%r14, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
.L157:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L183
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	%r13, %rdi
	call	X509_NAME_get_entry@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L158
	call	X509_NAME_ENTRY_get_data@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L158
	call	ASN1_STRING_get0_data@PLT
	movq	%r14, (%r12)
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L184
	movq	%rax, %rdi
	call	strlen@PLT
	movq	%rax, -48(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L185
	cmpq	$1, %rax
	jne	.L162
	movzbl	(%r15), %edx
	movb	%dl, 16(%r12)
.L163:
	movq	%rax, 8(%r12)
	movb	$0, (%r14,%rax)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L162:
	testq	%rax, %rax
	je	.L163
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %rax
	movq	(%r12), %r14
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r14
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L183:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9024:
	.size	_ZN4node6crypto16GetCertificateCNB5cxx11EP7x509_st, .-_ZN4node6crypto16GetCertificateCNB5cxx11EP7x509_st
	.p2align 4
	.globl	_ZN4node6crypto21VerifyPeerCertificateERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEl
	.type	_ZN4node6crypto21VerifyPeerCertificateERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEl, @function
_ZN4node6crypto21VerifyPeerCertificateERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEl:
.LFB9025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	SSL_get_peer_certificate@PLT
	testq	%rax, %rax
	je	.L187
	movq	%rax, %rdi
	call	X509_free@PLT
	movq	(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_get_verify_result@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	SSL_get_current_cipher@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	call	SSL_get_session@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	SSL_CIPHER_get_auth_nid@PLT
	cmpl	$1048, %eax
	je	.L188
	movq	%r14, %rdi
	call	SSL_SESSION_get_protocol_version@PLT
	cmpl	$772, %eax
	je	.L189
.L191:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	(%rbx), %rdi
	call	SSL_session_reused@PLT
	testl	%eax, %eax
	je	.L191
.L188:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9025:
	.size	_ZN4node6crypto21VerifyPeerCertificateERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEl, .-_ZN4node6crypto21VerifyPeerCertificateERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEEl
	.p2align 4
	.globl	_ZN4node6crypto13UseSNIContextERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEENS_17BaseObjectPtrImplINS0_13SecureContextELb0EEE
	.type	_ZN4node6crypto13UseSNIContextERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEENS_17BaseObjectPtrImplINS0_13SecureContextELb0EEE, @function
_ZN4node6crypto13UseSNIContextERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEENS_17BaseObjectPtrImplINS0_13SecureContextELb0EEE:
.LFB9026:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	32(%rax), %r14
	movq	%r14, %rdi
	call	SSL_CTX_get0_certificate@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	SSL_CTX_get0_privatekey@PLT
	leaq	-48(%rbp), %rcx
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r14, %rdi
	movq	%rax, %r12
	call	SSL_CTX_ctrl@PLT
	movl	%eax, %r8d
	cmpl	$1, %eax
	je	.L206
.L199:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L207
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	movq	(%rbx), %rdi
	movq	%r13, %rsi
	call	SSL_use_certificate@PLT
	movl	%eax, %r8d
	cmpl	$1, %eax
	jne	.L199
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	call	SSL_use_PrivateKey@PLT
	movl	%eax, %r8d
	cmpl	$1, %eax
	jne	.L199
	movq	-48(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L199
	movq	(%rbx), %rdi
	movl	$1, %edx
	movl	$88, %esi
	call	SSL_ctrl@PLT
	movl	%eax, %r8d
	jmp	.L199
.L207:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9026:
	.size	_ZN4node6crypto13UseSNIContextERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEENS_17BaseObjectPtrImplINS0_13SecureContextELb0EEE, .-_ZN4node6crypto13UseSNIContextERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEENS_17BaseObjectPtrImplINS0_13SecureContextELb0EEE
	.p2align 4
	.globl	_ZN4node6crypto18GetClientHelloALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto18GetClientHelloALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto18GetClientHelloALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE:
.LFB9027:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rcx
	leaq	-24(%rbp), %rdx
	call	SSL_client_hello_get0_ext@PLT
	testl	%eax, %eax
	je	.L212
	movq	-16(%rbp), %rcx
	cmpq	$1, %rcx
	jbe	.L212
	movq	-24(%rbp), %rax
	movzwl	(%rax), %edx
	addq	$3, %rax
	rolw	$8, %dx
	movzwl	%dx, %edx
	addq	$2, %rdx
	cmpq	%rdx, %rcx
	movl	$0, %edx
	cmovne	%rdx, %rax
.L208:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L215
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	.cfi_restore_state
	xorl	%eax, %eax
	jmp	.L208
.L215:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9027:
	.size	_ZN4node6crypto18GetClientHelloALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto18GetClientHelloALPNERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto24GetClientHelloServerNameERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto24GetClientHelloServerNameERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto24GetClientHelloServerNameERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE:
.LFB9028:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rcx
	leaq	-24(%rbp), %rdx
	call	SSL_client_hello_get0_ext@PLT
	testl	%eax, %eax
	je	.L219
	movq	-16(%rbp), %rsi
	cmpq	$2, %rsi
	jbe	.L219
	movq	-24(%rbp), %rax
	movzwl	(%rax), %ecx
	rolw	$8, %cx
	movzwl	%cx, %edx
	leaq	2(%rdx), %rdi
	cmpq	%rdi, %rsi
	jne	.L219
	movq	%rdx, -16(%rbp)
	testw	%cx, %cx
	je	.L219
	cmpb	$0, 2(%rax)
	jne	.L219
	subq	$1, %rdx
	movq	%rdx, -16(%rbp)
	cmpq	$2, %rdx
	jbe	.L219
	movzwl	3(%rax), %ecx
	rolw	$8, %cx
	movzwl	%cx, %ecx
	addq	$2, %rcx
	cmpq	%rcx, %rdx
	jb	.L219
	addq	$5, %rax
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L219:
	xorl	%eax, %eax
.L216:
	movq	-8(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L225
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L225:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9028:
	.size	_ZN4node6crypto24GetClientHelloServerNameERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto24GetClientHelloServerNameERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS2_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto13GetServerNameEP6ssl_st
	.type	_ZN4node6crypto13GetServerNameEP6ssl_st, @function
_ZN4node6crypto13GetServerNameEP6ssl_st:
.LFB9029:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	SSL_get_servername@PLT
	.cfi_endproc
.LFE9029:
	.size	_ZN4node6crypto13GetServerNameEP6ssl_st, .-_ZN4node6crypto13GetServerNameEP6ssl_st
	.p2align 4
	.globl	_ZN4node6crypto9SetGroupsEPNS0_13SecureContextEPKc
	.type	_ZN4node6crypto9SetGroupsEPNS0_13SecureContextEPKc, @function
_ZN4node6crypto9SetGroupsEPNS0_13SecureContextEPKc:
.LFB9030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	32(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movl	$92, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	SSL_CTX_ctrl@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpq	$1, %rax
	sete	%al
	ret
	.cfi_endproc
.LFE9030:
	.size	_ZN4node6crypto9SetGroupsEPNS0_13SecureContextEPKc, .-_ZN4node6crypto9SetGroupsEPNS0_13SecureContextEPKc
	.section	.rodata.str1.1
.LC6:
	.string	"HOSTNAME_MISMATCH"
.LC7:
	.string	"UNSPECIFIED"
.LC8:
	.string	"UNABLE_TO_GET_CRL"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"UNABLE_TO_DECRYPT_CERT_SIGNATURE"
	.align 8
.LC10:
	.string	"UNABLE_TO_DECRYPT_CRL_SIGNATURE"
	.align 8
.LC11:
	.string	"UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY"
	.section	.rodata.str1.1
.LC12:
	.string	"CERT_SIGNATURE_FAILURE"
.LC13:
	.string	"CRL_SIGNATURE_FAILURE"
.LC14:
	.string	"CERT_NOT_YET_VALID"
.LC15:
	.string	"CERT_HAS_EXPIRED"
.LC16:
	.string	"CRL_NOT_YET_VALID"
.LC17:
	.string	"CRL_HAS_EXPIRED"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"ERROR_IN_CERT_NOT_BEFORE_FIELD"
	.section	.rodata.str1.1
.LC19:
	.string	"ERROR_IN_CERT_NOT_AFTER_FIELD"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"ERROR_IN_CRL_LAST_UPDATE_FIELD"
	.align 8
.LC21:
	.string	"ERROR_IN_CRL_NEXT_UPDATE_FIELD"
	.section	.rodata.str1.1
.LC22:
	.string	"OUT_OF_MEM"
.LC23:
	.string	"DEPTH_ZERO_SELF_SIGNED_CERT"
.LC24:
	.string	"SELF_SIGNED_CERT_IN_CHAIN"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"UNABLE_TO_GET_ISSUER_CERT_LOCALLY"
	.align 8
.LC26:
	.string	"UNABLE_TO_VERIFY_LEAF_SIGNATURE"
	.section	.rodata.str1.1
.LC27:
	.string	"CERT_CHAIN_TOO_LONG"
.LC28:
	.string	"CERT_REVOKED"
.LC29:
	.string	"INVALID_CA"
.LC30:
	.string	"PATH_LENGTH_EXCEEDED"
.LC31:
	.string	"INVALID_PURPOSE"
.LC32:
	.string	"CERT_UNTRUSTED"
.LC33:
	.string	"CERT_REJECTED"
.LC34:
	.string	"UNABLE_TO_GET_ISSUER_CERT"
	.text
	.p2align 4
	.globl	_ZN4node6crypto13X509ErrorCodeEl
	.type	_ZN4node6crypto13X509ErrorCodeEl, @function
_ZN4node6crypto13X509ErrorCodeEl:
.LFB9031:
	.cfi_startproc
	endbr64
	cmpq	$62, %rdi
	ja	.L260
	leaq	.L232(%rip), %rdx
	movslq	(%rdx,%rdi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L232:
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L259-.L232
	.long	.L258-.L232
	.long	.L257-.L232
	.long	.L256-.L232
	.long	.L255-.L232
	.long	.L254-.L232
	.long	.L253-.L232
	.long	.L252-.L232
	.long	.L251-.L232
	.long	.L250-.L232
	.long	.L249-.L232
	.long	.L248-.L232
	.long	.L247-.L232
	.long	.L246-.L232
	.long	.L245-.L232
	.long	.L244-.L232
	.long	.L243-.L232
	.long	.L242-.L232
	.long	.L241-.L232
	.long	.L240-.L232
	.long	.L239-.L232
	.long	.L238-.L232
	.long	.L237-.L232
	.long	.L236-.L232
	.long	.L235-.L232
	.long	.L234-.L232
	.long	.L233-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L260-.L232
	.long	.L231-.L232
	.text
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	.LC7(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	.LC6(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	leaq	.LC33(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	.LC32(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	leaq	.LC31(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	leaq	.LC30(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	leaq	.LC29(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	.LC28(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	.LC27(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	.LC26(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	.LC25(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	.LC24(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	.LC23(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	.LC22(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	.LC21(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	.LC20(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	.LC19(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	.LC18(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	.LC17(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	.LC16(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	.LC15(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	leaq	.LC14(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	leaq	.LC13(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	.LC12(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	.LC11(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	.LC10(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	.LC9(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	leaq	.LC8(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	.LC34(%rip), %rax
	ret
	.cfi_endproc
.LFE9031:
	.size	_ZN4node6crypto13X509ErrorCodeEl, .-_ZN4node6crypto13X509ErrorCodeEl
	.p2align 4
	.globl	_ZN4node6crypto24GetValidationErrorReasonEPNS_11EnvironmentEi
	.type	_ZN4node6crypto24GetValidationErrorReasonEPNS_11EnvironmentEi, @function
_ZN4node6crypto24GetValidationErrorReasonEPNS_11EnvironmentEi:
.LFB9032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	testl	%esi, %esi
	jne	.L262
	movq	352(%rdi), %rax
	addq	$88, %rax
.L263:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	.cfi_restore_state
	movslq	%esi, %rdi
	call	X509_verify_cert_error_string@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L263
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rax
	jmp	.L263
	.cfi_endproc
.LFE9032:
	.size	_ZN4node6crypto24GetValidationErrorReasonEPNS_11EnvironmentEi, .-_ZN4node6crypto24GetValidationErrorReasonEPNS_11EnvironmentEi
	.p2align 4
	.globl	_ZN4node6crypto22GetValidationErrorCodeEPNS_11EnvironmentEi
	.type	_ZN4node6crypto22GetValidationErrorCodeEPNS_11EnvironmentEi, @function
_ZN4node6crypto22GetValidationErrorCodeEPNS_11EnvironmentEi:
.LFB9033:
	.cfi_startproc
	endbr64
	movq	352(%rdi), %rdi
	testl	%esi, %esi
	jne	.L267
	leaq	88(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	cmpl	$62, %esi
	ja	.L300
	leaq	.L271(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L271:
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L298-.L271
	.long	.L297-.L271
	.long	.L296-.L271
	.long	.L295-.L271
	.long	.L294-.L271
	.long	.L293-.L271
	.long	.L292-.L271
	.long	.L291-.L271
	.long	.L290-.L271
	.long	.L289-.L271
	.long	.L288-.L271
	.long	.L287-.L271
	.long	.L286-.L271
	.long	.L285-.L271
	.long	.L284-.L271
	.long	.L283-.L271
	.long	.L282-.L271
	.long	.L281-.L271
	.long	.L280-.L271
	.long	.L279-.L271
	.long	.L278-.L271
	.long	.L277-.L271
	.long	.L276-.L271
	.long	.L275-.L271
	.long	.L274-.L271
	.long	.L273-.L271
	.long	.L272-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L300-.L271
	.long	.L270-.L271
	.text
.L298:
	leaq	.LC34(%rip), %rsi
.L269:
	xorl	%edx, %edx
	movl	$-1, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L304
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L300:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	jmp	.L269
.L270:
	leaq	.LC6(%rip), %rsi
	jmp	.L269
.L272:
	leaq	.LC33(%rip), %rsi
	jmp	.L269
.L273:
	leaq	.LC32(%rip), %rsi
	jmp	.L269
.L274:
	leaq	.LC31(%rip), %rsi
	jmp	.L269
.L275:
	leaq	.LC30(%rip), %rsi
	jmp	.L269
.L276:
	leaq	.LC29(%rip), %rsi
	jmp	.L269
.L277:
	leaq	.LC28(%rip), %rsi
	jmp	.L269
.L278:
	leaq	.LC27(%rip), %rsi
	jmp	.L269
.L279:
	leaq	.LC26(%rip), %rsi
	jmp	.L269
.L280:
	leaq	.LC25(%rip), %rsi
	jmp	.L269
.L281:
	leaq	.LC24(%rip), %rsi
	jmp	.L269
.L282:
	leaq	.LC23(%rip), %rsi
	jmp	.L269
.L283:
	leaq	.LC22(%rip), %rsi
	jmp	.L269
.L284:
	leaq	.LC21(%rip), %rsi
	jmp	.L269
.L285:
	leaq	.LC20(%rip), %rsi
	jmp	.L269
.L286:
	leaq	.LC19(%rip), %rsi
	jmp	.L269
.L287:
	leaq	.LC18(%rip), %rsi
	jmp	.L269
.L288:
	leaq	.LC17(%rip), %rsi
	jmp	.L269
.L289:
	leaq	.LC16(%rip), %rsi
	jmp	.L269
.L290:
	leaq	.LC15(%rip), %rsi
	jmp	.L269
.L291:
	leaq	.LC14(%rip), %rsi
	jmp	.L269
.L292:
	leaq	.LC13(%rip), %rsi
	jmp	.L269
.L293:
	leaq	.LC12(%rip), %rsi
	jmp	.L269
.L294:
	leaq	.LC11(%rip), %rsi
	jmp	.L269
.L295:
	leaq	.LC10(%rip), %rsi
	jmp	.L269
.L296:
	leaq	.LC9(%rip), %rsi
	jmp	.L269
.L297:
	leaq	.LC8(%rip), %rsi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%rax, -8(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9033:
	.size	_ZN4node6crypto22GetValidationErrorCodeEPNS_11EnvironmentEi, .-_ZN4node6crypto22GetValidationErrorCodeEPNS_11EnvironmentEi
	.p2align 4
	.globl	_ZN4node6crypto13GetCipherNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto13GetCipherNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto13GetCipherNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE:
.LFB9072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	call	SSL_get_current_cipher@PLT
	testq	%rax, %rax
	je	.L309
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L310
.L307:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	352(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$88, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rax
	jmp	.L307
	.cfi_endproc
.LFE9072:
	.size	_ZN4node6crypto13GetCipherNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto13GetCipherNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto21GetCipherStandardNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto21GetCipherStandardNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto21GetCipherStandardNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE:
.LFB9073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	call	SSL_get_current_cipher@PLT
	testq	%rax, %rax
	je	.L315
	movq	%rax, %rdi
	call	SSL_CIPHER_standard_name@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L316
.L313:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movq	352(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$88, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rax
	jmp	.L313
	.cfi_endproc
.LFE9073:
	.size	_ZN4node6crypto21GetCipherStandardNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto21GetCipherStandardNameEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto16GetCipherVersionEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto16GetCipherVersionEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto16GetCipherVersionEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE:
.LFB9074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rdi
	call	SSL_get_current_cipher@PLT
	testq	%rax, %rax
	je	.L321
	movq	%rax, %rdi
	call	SSL_CIPHER_get_version@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L322
.L319:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	.cfi_restore_state
	movq	352(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	addq	$88, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rax
	jmp	.L319
	.cfi_endproc
.LFE9074:
	.size	_ZN4node6crypto16GetCipherVersionEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto16GetCipherVersionEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto21GetClientHelloCiphersEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto21GetClientHelloCiphersEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto21GetClientHelloCiphersEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE:
.LFB9075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	352(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-240(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r15), %rdi
	leaq	-248(%rbp), %rsi
	call	SSL_client_hello_get0_ciphers@PLT
	movdqa	.LC35(%rip), %xmm0
	leaq	-184(%rbp), %rdx
	movq	%rax, -264(%rbp)
	movq	%rax, %rbx
	shrq	%rax
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -184(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rdx, -288(%rbp)
	movq	%rdx, -192(%rbp)
	movq	$0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	cmpq	$33, %rbx
	jbe	.L324
	movq	%rax, %rbx
	leaq	0(,%rax,8), %r12
	movabsq	$2305843009213693951, %rax
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L398
	testq	%r12, %r12
	je	.L326
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L399
	movq	%rax, -192(%rbp)
	movq	-272(%rbp), %rax
	movq	%rax, -200(%rbp)
.L329:
	movq	%rax, -208(%rbp)
.L361:
	xorl	%r13d, %r13d
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L331:
	movq	%r12, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	352(%r14), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L397
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	1056(%rax), %rdx
.L360:
	movq	(%rcx), %rax
	movq	%rax, %rdi
	andl	$3, %edi
	cmpq	$1, %rdi
	jne	.L335
	movq	-1(%rax), %rdi
	cmpw	$67, 11(%rdi)
	jne	.L335
	cmpl	$5, 43(%rax)
	je	.L336
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L337
.L336:
	testq	%r12, %r12
	je	.L400
	movq	%r12, %rdi
	call	SSL_CIPHER_standard_name@PLT
	movq	352(%r14), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L397
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	1656(%rax), %rdx
.L359:
	movq	(%rcx), %rax
	movq	%rax, %rdi
	andl	$3, %edi
	cmpq	$1, %rdi
	jne	.L342
	movq	-1(%rax), %rdi
	cmpw	$67, 11(%rdi)
	jne	.L342
	cmpl	$5, 43(%rax)
	je	.L343
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L337
.L343:
	testq	%r12, %r12
	je	.L401
	movq	%r12, %rdi
	call	SSL_CIPHER_get_version@PLT
	movq	352(%r14), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L397
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	1896(%rax), %rdx
.L358:
	movq	(%rcx), %rax
	movq	%rax, %rdi
	andl	$3, %edi
	cmpq	$1, %rdi
	jne	.L349
	movq	-1(%rax), %rdi
	cmpw	$67, 11(%rdi)
	jne	.L349
	cmpl	$5, 43(%rax)
	je	.L350
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L337
.L350:
	cmpq	%r13, -208(%rbp)
	jbe	.L402
	movq	-192(%rbp), %rsi
	movq	%rbx, (%rsi,%r13,8)
	addq	$1, %r13
	leaq	(%r13,%r13), %rax
	cmpq	%rax, -264(%rbp)
	jbe	.L330
.L353:
	movq	-248(%rbp), %rsi
	movq	(%r15), %rdi
	call	SSL_CIPHER_find@PLT
	movq	352(%r14), %rdi
	addq	$2, -248(%rbp)
	movq	%rax, %r12
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rbx
	testq	%r12, %r12
	jne	.L331
	movq	360(%r14), %rax
	movq	352(%r14), %rcx
	movq	3280(%r14), %rsi
	movq	1056(%rax), %rdx
	cmpq	$-88, %rcx
	jne	.L403
.L337:
	xorl	%r12d, %r12d
.L351:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L354
	cmpq	-288(%rbp), %rdi
	je	.L354
	call	free@PLT
.L354:
	movq	-280(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	addq	$248, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	cmpq	$0, -264(%rbp)
	movq	%rax, -208(%rbp)
	jne	.L361
	movq	%rdx, %rsi
.L330:
	movq	352(%r14), %rdi
	movq	-272(%rbp), %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-280(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L400:
	movq	360(%r14), %rax
	movq	352(%r14), %rcx
	movq	3280(%r14), %rsi
	movq	1656(%rax), %rdx
	cmpq	$-88, %rcx
	je	.L337
	addq	$88, %rcx
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L401:
	movq	360(%r14), %rax
	movq	352(%r14), %rcx
	movq	3280(%r14), %rsi
	movq	1896(%rax), %rdx
	cmpq	$-88, %rcx
	je	.L337
	addq	$88, %rcx
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L403:
	addq	$88, %rcx
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L397:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L402:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L326:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L399:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r12, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L326
	movq	-272(%rbp), %rbx
	movq	-208(%rbp), %rax
	movq	%rdi, -192(%rbp)
	movq	%rbx, -200(%rbp)
	testq	%rax, %rax
	je	.L405
	movq	-288(%rbp), %rsi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	movq	-272(%rbp), %rax
	jmp	.L329
.L398:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L405:
	movq	%rbx, %rax
	jmp	.L329
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9075:
	.size	_ZN4node6crypto21GetClientHelloCiphersEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto21GetClientHelloCiphersEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto13GetCipherInfoEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto13GetCipherInfoEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto13GetCipherInfoEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE:
.LFB9079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	(%r12), %rdi
	movq	%rax, %r13
	call	SSL_get_current_cipher@PLT
	testq	%rax, %rax
	je	.L456
	movq	%rax, %rdi
	call	SSL_CIPHER_get_name@PLT
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L455
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	1056(%rax), %rdx
.L433:
	movq	(%rcx), %rax
	movq	%rax, %rdi
	andl	$3, %edi
	cmpq	$1, %rdi
	jne	.L411
	movq	-1(%rax), %rdi
	cmpw	$67, 11(%rdi)
	jne	.L411
	cmpl	$5, 43(%rax)
	je	.L412
	.p2align 4,,10
	.p2align 3
.L411:
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L412
.L413:
	xorl	%r12d, %r12d
.L427:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L457
	addq	$48, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	movq	(%r12), %rdi
	call	SSL_get_current_cipher@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L458
	call	SSL_CIPHER_standard_name@PLT
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L455
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	1656(%rax), %rdx
.L432:
	movq	(%rcx), %rax
	movq	%rax, %rdi
	andl	$3, %edi
	cmpq	$1, %rdi
	jne	.L418
	movq	-1(%rax), %rdi
	cmpw	$67, 11(%rdi)
	jne	.L418
	cmpl	$5, 43(%rax)
	je	.L419
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L413
.L419:
	movq	(%r12), %rdi
	call	SSL_get_current_cipher@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L459
	call	SSL_CIPHER_get_version@PLT
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L455
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	1896(%rax), %rdx
.L431:
	movq	(%rcx), %rax
	movq	%rax, %rdi
	andl	$3, %edi
	cmpq	$1, %rdi
	jne	.L425
	movq	-1(%rax), %rdi
	cmpw	$67, 11(%rdi)
	jne	.L425
	cmpl	$5, 43(%rax)
	jne	.L425
.L426:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L456:
	movq	360(%rbx), %rax
	movq	352(%rbx), %rcx
	movq	3280(%rbx), %rsi
	movq	1056(%rax), %rdx
	cmpq	$-88, %rcx
	je	.L413
	addq	$88, %rcx
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L458:
	movq	360(%rbx), %rax
	movq	352(%rbx), %rcx
	movq	3280(%rbx), %rsi
	movq	1656(%rax), %rdx
	cmpq	$-88, %rcx
	je	.L413
	addq	$88, %rcx
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L455:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L413
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L459:
	movq	360(%rbx), %rax
	movq	352(%rbx), %rcx
	movq	3280(%rbx), %rsi
	movq	1896(%rax), %rdx
	cmpq	$-88, %rcx
	je	.L413
	addq	$88, %rcx
	jmp	.L431
.L457:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9079:
	.size	_ZN4node6crypto13GetCipherInfoEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto13GetCipherInfoEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE:
.LFB9080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_is_server@PLT
	testl	%eax, %eax
	jne	.L545
	movq	352(%rbx), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	(%r14), %rdi
	xorl	%edx, %edx
	leaq	-104(%rbp), %rcx
	movl	$109, %esi
	movq	%rax, %r12
	call	SSL_ctrl@PLT
	testq	%rax, %rax
	je	.L546
	movq	-104(%rbp), %r14
	movq	3280(%rbx), %r15
	movq	%r14, %rdi
	call	EVP_PKEY_id@PLT
	movq	%r14, %rdi
	movl	%eax, -120(%rbp)
	call	EVP_PKEY_bits@PLT
	movl	-120(%rbp), %r8d
	movl	%eax, -124(%rbp)
	cmpl	$408, %r8d
	je	.L464
	jg	.L465
	cmpl	$28, %r8d
	jne	.L467
	movq	360(%rbx), %rax
	movq	472(%rax), %rcx
	movq	1768(%rax), %rdx
	testq	%rcx, %rcx
	je	.L487
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L544
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L547
.L544:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L487
.L489:
	movq	352(%rbx), %rdi
	movl	-124(%rbp), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1616(%rax), %rdx
	testq	%rcx, %rcx
	je	.L487
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L492
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L492
	cmpl	$5, 43(%rax)
	je	.L467
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L487
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L463:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L548
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L550:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	.p2align 4,,10
	.p2align 3
.L487:
	xorl	%r12d, %r12d
.L479:
	testq	%r14, %r14
	je	.L463
	movq	%r14, %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L465:
	leal	-1034(%r8), %eax
	cmpl	$1, %eax
	ja	.L467
	movl	%r8d, %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, -120(%rbp)
.L480:
	movq	360(%rbx), %rax
	movq	576(%rax), %rcx
	movq	1768(%rax), %rdx
	testq	%rcx, %rcx
	je	.L487
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L483
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	je	.L549
.L483:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L487
.L484:
	movq	352(%rbx), %rdi
	movq	-120(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L550
	movq	360(%rbx), %rax
	movq	1056(%rax), %r8
	movq	(%rcx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L488
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L488
	cmpl	$5, 43(%rax)
	je	.L489
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%r8, %rdx
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	_ZZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L464:
	movq	%r14, %rdi
	call	EVP_PKEY_get1_EC_KEY@PLT
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	EC_KEY_get0_group@PLT
	movq	%rax, %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, %edi
	call	OBJ_nid2sn@PLT
	movq	-136(%rbp), %r8
	movq	%rax, -120(%rbp)
	testq	%r8, %r8
	je	.L480
	movq	%r8, %rdi
	call	EC_KEY_free@PLT
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L549:
	cmpl	$5, 43(%rax)
	jne	.L483
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L547:
	cmpl	$5, 43(%rax)
	jne	.L544
	jmp	.L489
.L548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9080:
	.size	_ZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"Failed to get public key length"
	.section	.rodata.str1.1
.LC37:
	.string	"Failed to get public key"
	.text
	.p2align 4
	.globl	_ZN4node6crypto15ECPointToBufferEPNS_11EnvironmentEPK11ec_group_stPK11ec_point_st23point_conversion_form_tPPKc
	.type	_ZN4node6crypto15ECPointToBufferEPNS_11EnvironmentEPK11ec_group_stPK11ec_point_st23point_conversion_form_tPPKc, @function
_ZN4node6crypto15ECPointToBufferEPNS_11EnvironmentEPK11ec_group_stPK11ec_point_st23point_conversion_form_tPPKc:
.LFB9081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	xorl	%ecx, %ecx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	movl	%r14d, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%r13, %rsi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%r8, %rbx
	xorl	%r8d, %r8d
	subq	$56, %rsp
	call	EC_POINT_point2oct@PLT
	testq	%rax, %rax
	jne	.L552
	testq	%rbx, %rbx
	je	.L553
	leaq	.LC36(%rip), %rax
	movq	%rax, (%rbx)
.L553:
	xorl	%r12d, %r12d
.L571:
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movq	%rax, %rsi
	movq	360(%r15), %rax
	movq	%rsi, -88(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	-88(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L574
	call	uv_buf_init@PLT
	xorl	%r9d, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rdx, %r8
	movq	%rdx, -96(%rbp)
	movq	%rax, %rcx
	movl	%r14d, %edx
	movq	%rax, -88(%rbp)
	call	EC_POINT_point2oct@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	jne	.L575
	testq	%rbx, %rbx
	je	.L556
	leaq	.LC37(%rip), %rax
	movq	%rax, (%rbx)
.L556:
	xorl	%r12d, %r12d
.L557:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r10, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	uv_buf_init@PLT
	movq	-96(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	%rax, -72(%rbp)
	testq	%r10, %r10
	movq	%rdx, -64(%rbp)
	je	.L571
	movq	360(%r15), %rax
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L575:
	xorl	%ecx, %ecx
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	movq	%r10, -88(%rbp)
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb@PLT
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L557
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rdx, %rsi
	movq	%rax, %r10
	movq	%rsi, %r8
	jmp	.L557
	.p2align 4,,10
	.p2align 3
.L574:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9081:
	.size	_ZN4node6crypto15ECPointToBufferEPNS_11EnvironmentEPK11ec_group_stPK11ec_point_st23point_conversion_form_tPPKc, .-_ZN4node6crypto15ECPointToBufferEPNS_11EnvironmentEPK11ec_group_stPK11ec_point_st23point_conversion_form_tPPKc
	.section	.rodata.str1.1
.LC38:
	.string	"0x%x"
.LC39:
	.string	"0x%x%08x"
.LC40:
	.string	"../src/node_crypto_common.h"
	.text
	.p2align 4
	.globl	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st
	.type	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st, @function
_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st:
.LFB9085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-512(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$600, %rsp
	movq	352(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %rdi
	movq	3280(%rbx), %r12
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %r14
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	call	X509_get_subject_name@PLT
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	movl	$262162, %ecx
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	testl	%eax, %eax
	jle	.L836
	leaq	-544(%rbp), %r8
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r8, %rcx
	call	BIO_ctrl@PLT
	movq	-544(%rbp), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -584(%rbp)
	call	BIO_ctrl@PLT
	movq	-584(%rbp), %rcx
.L578:
	movq	360(%rbx), %rax
	movq	1688(%rax), %rdx
	testq	%rcx, %rcx
	je	.L659
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L581
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L581
	cmpl	$5, 43(%rax)
	je	.L582
	.p2align 4,,10
	.p2align 3
.L581:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L582
.L659:
	xorl	%r13d, %r13d
.L607:
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L683
	call	BIO_free_all@PLT
.L683:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L837
	addq	$600, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	.cfi_restore_state
	movq	%r13, %rdi
	call	X509_get_issuer_name@PLT
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	movl	$262162, %ecx
	movq	%rax, %rsi
	call	X509_NAME_print_ex@PLT
	testl	%eax, %eax
	jle	.L838
	leaq	-544(%rbp), %r8
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r8, %rcx
	call	BIO_ctrl@PLT
	movq	-544(%rbp), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -584(%rbp)
	call	BIO_ctrl@PLT
	movq	-584(%rbp), %rcx
.L585:
	movq	360(%rbx), %rax
	movq	952(%rax), %rdx
	testq	%rcx, %rcx
	je	.L659
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L588
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L588
	cmpl	$5, 43(%rax)
	je	.L589
	.p2align 4,,10
	.p2align 3
.L588:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L659
.L589:
	movl	$85, %esi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	X509_get_ext_by_NID@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L591
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
.L592:
	movq	360(%rbx), %rax
	movq	1696(%rax), %rdx
	testq	%rcx, %rcx
	je	.L659
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L597
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L597
	cmpl	$5, 43(%rax)
	jne	.L597
.L598:
	movl	$177, %esi
	movl	$-1, %edx
	movq	%r13, %rdi
	call	X509_get_ext_by_NID@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jns	.L599
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
.L600:
	movq	360(%rbx), %rax
	movq	888(%rax), %rdx
	testq	%rcx, %rcx
	je	.L659
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L605
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L605
	cmpl	$5, 43(%rax)
	je	.L606
	.p2align 4,,10
	.p2align 3
.L605:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L659
.L606:
	movq	%r13, %rdi
	call	X509_get_pubkey@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	je	.L835
	movq	%rax, %rdi
	call	EVP_PKEY_id@PLT
	cmpl	$6, %eax
	je	.L609
	cmpl	$408, %eax
	jne	.L818
	movq	-584(%rbp), %rdi
	call	EVP_PKEY_get1_EC_KEY@PLT
	movq	%rax, -592(%rbp)
	testq	%rax, %rax
	je	.L818
	movq	%rax, %rdi
	call	EC_KEY_get0_group@PLT
	movq	%rax, -600(%rbp)
	testq	%rax, %rax
	je	.L834
	movq	%rax, %rdi
	call	EC_GROUP_order_bits@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	jg	.L633
.L834:
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
.L632:
	movq	360(%rbx), %rax
	movq	224(%rax), %rdx
	testq	%rcx, %rcx
	je	.L647
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L636
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L636
	cmpl	$5, 43(%rax)
	je	.L637
.L636:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L637
.L647:
	movq	-592(%rbp), %rdi
	xorl	%r13d, %r13d
	call	EC_KEY_free@PLT
.L630:
	movq	-584(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L836:
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	BIO_ctrl@PLT
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L838:
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	BIO_ctrl@PLT
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L597:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L659
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%r13, %rdi
	call	X509_get_ext@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L839
	leaq	-576(%rbp), %rdi
	movq	%rax, -584(%rbp)
	call	_ZN4node6crypto12_GLOBAL__N_116SafeX509ExtPrintERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS3_XadL_Z12BIO_free_allEEEEEP17X509_extension_st
	testb	%al, %al
	jne	.L594
	movq	-584(%rbp), %rsi
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	X509V3_EXT_print@PLT
	cmpl	$1, %eax
	je	.L594
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	BIO_ctrl@PLT
	movq	352(%rbx), %rax
	leaq	104(%rax), %rcx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L594:
	leaq	-544(%rbp), %r8
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r8, %rcx
	call	BIO_ctrl@PLT
	movq	-544(%rbp), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -584(%rbp)
	call	BIO_ctrl@PLT
	movq	-584(%rbp), %rcx
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%r13, %rdi
	call	X509_get_ext@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L840
	leaq	-576(%rbp), %rdi
	movq	%rax, -584(%rbp)
	call	_ZN4node6crypto12_GLOBAL__N_116SafeX509ExtPrintERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS3_XadL_Z12BIO_free_allEEEEEP17X509_extension_st
	testb	%al, %al
	jne	.L602
	movq	-584(%rbp), %rsi
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	X509V3_EXT_print@PLT
	cmpl	$1, %eax
	je	.L602
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	call	BIO_ctrl@PLT
	movq	352(%rbx), %rax
	leaq	104(%rax), %rcx
	jmp	.L600
.L602:
	leaq	-544(%rbp), %r8
	movq	-576(%rbp), %rdi
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r8, %rcx
	call	BIO_ctrl@PLT
	movq	-544(%rbp), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -584(%rbp)
	call	BIO_ctrl@PLT
	movq	-584(%rbp), %rcx
	jmp	.L600
.L818:
	movq	-584(%rbp), %rdi
	call	EVP_PKEY_free@PLT
.L835:
	leaq	-544(%rbp), %r8
.L608:
	movq	%r13, %rdi
	movq	%r8, -584(%rbp)
	call	X509_get0_notBefore@PLT
	movq	-576(%rbp), %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	movq	-584(%rbp), %r8
	xorl	%edx, %edx
	movq	-576(%rbp), %rdi
	movl	$115, %esi
	movq	%r8, %rcx
	call	BIO_ctrl@PLT
	movq	-544(%rbp), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -584(%rbp)
	call	BIO_ctrl@PLT
	movq	-584(%rbp), %r8
	testq	%r8, %r8
	je	.L659
	movq	360(%rbx), %rax
	movq	1864(%rax), %rdx
	movq	(%r8), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L653
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L653
	cmpl	$5, 43(%rax)
	je	.L654
.L653:
	movq	%r8, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L659
.L654:
	movq	%r13, %rdi
	call	X509_get0_notAfter@PLT
	movq	-576(%rbp), %rdi
	movq	%rax, %rsi
	call	ASN1_TIME_print@PLT
	leaq	-576(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN4node6crypto12_GLOBAL__N_19ToV8ValueEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS5_XadL_Z12BIO_free_allEEEEE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1872(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L659
	movq	-576(%rbp), %rdi
	movq	$0, -576(%rbp)
	testq	%rdi, %rdi
	je	.L657
	call	BIO_free_all@PLT
.L657:
	call	EVP_sha1@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN4node6crypto12_GLOBAL__N_120GetFingerprintDigestEPNS_11EnvironmentEPK9evp_md_stP7x509_st
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	760(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L659
	call	EVP_sha256@PLT
	movq	%r13, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN4node6crypto12_GLOBAL__N_120GetFingerprintDigestEPNS_11EnvironmentEPK9evp_md_stP7x509_st
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	752(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L659
	movq	%r13, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$126, %esi
	call	X509_get_ext_d2i@PLT
	movq	%rax, -592(%rbp)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L660
	call	OPENSSL_sk_num@PLT
	movdqa	.LC35(%rip), %xmm0
	leaq	-328(%rbp), %rdx
	movl	%eax, -624(%rbp)
	cltq
	movq	%rax, -584(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, -608(%rbp)
	movq	%rax, -464(%rbp)
	movaps	%xmm0, -480(%rbp)
.L661:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L661
	movq	-584(%rbp), %rdx
	movq	$0, -456(%rbp)
	cmpq	$16, %rdx
	jbe	.L663
	movabsq	$2305843009213693951, %rax
	leaq	0(,%rdx,8), %r8
	andq	%rdx, %rax
	cmpq	%rax, %rdx
	jne	.L841
	testq	%r8, %r8
	je	.L667
	movq	%r8, %rdi
	movq	%r8, -600(%rbp)
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L842
.L666:
	movq	-584(%rbp), %rax
	movq	%rdi, -464(%rbp)
	movq	%rax, -472(%rbp)
	movq	-480(%rbp), %rax
	testq	%rax, %rax
	jne	.L843
.L663:
	movq	-584(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, -632(%rbp)
	movl	$0, -600(%rbp)
	movl	%edx, %r12d
	movq	%rax, -480(%rbp)
	leaq	-320(%rbp), %rax
	movq	%rax, -616(%rbp)
	jmp	.L673
.L670:
	addl	$1, %r12d
.L673:
	cmpl	%r12d, -624(%rbp)
	jle	.L669
	movq	-592(%rbp), %rdi
	movl	%r12d, %esi
	call	OPENSSL_sk_value@PLT
	movl	$1, %ecx
	movl	$256, %esi
	movq	-616(%rbp), %rdi
	movq	%rax, %rdx
	call	OBJ_obj2txt@PLT
	testl	%eax, %eax
	js	.L670
	movq	352(%rbx), %rdi
	movq	-616(%rbp), %rsi
	orl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L844
.L671:
	movslq	-600(%rbp), %rdx
	leal	1(%rdx), %ecx
	cmpq	-480(%rbp), %rdx
	jnb	.L845
	movq	-464(%rbp), %rsi
	movl	%ecx, -600(%rbp)
	movq	%rax, (%rsi,%rdx,8)
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L839:
	leaq	_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi85EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L609:
	movq	-584(%rbp), %rdi
	call	EVP_PKEY_get1_RSA@PLT
	movq	%rax, -592(%rbp)
	testq	%rax, %rax
	je	.L818
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	leaq	-560(%rbp), %rdx
	leaq	-568(%rbp), %rsi
	call	RSA_get0_key@PLT
	movq	-568(%rbp), %rsi
	movq	-576(%rbp), %rdi
	call	BN_print@PLT
	leaq	-544(%rbp), %r8
	xorl	%edx, %edx
	movq	-576(%rbp), %rdi
	movq	%r8, %rcx
	movl	$115, %esi
	movq	%r8, -608(%rbp)
	call	BIO_ctrl@PLT
	movq	-544(%rbp), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-576(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%rax, -600(%rbp)
	call	BIO_ctrl@PLT
	movq	-600(%rbp), %r9
	movq	-608(%rbp), %r8
	testq	%r9, %r9
	je	.L693
	movq	360(%rbx), %rax
	movq	1048(%rax), %rdx
	movq	(%r9), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L618
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L618
	cmpl	$5, 43(%rax)
	je	.L619
.L618:
	movq	%r9, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -600(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-600(%rbp), %r8
	testb	%al, %al
	jne	.L619
.L693:
	movq	-592(%rbp), %rdi
	xorl	%r13d, %r13d
	call	RSA_free@PLT
	jmp	.L630
.L619:
	movq	-568(%rbp), %rdi
	movq	%r8, -600(%rbp)
	call	BN_num_bits@PLT
	movq	352(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-600(%rbp), %r8
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	testq	%rcx, %rcx
	movq	224(%rax), %rdx
	je	.L693
	movq	(%rcx), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L623
	movq	-1(%rax), %rsi
	cmpw	$67, 11(%rsi)
	jne	.L623
	cmpl	$5, 43(%rax)
	je	.L624
.L623:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%r8, -600(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-600(%rbp), %r8
	testb	%al, %al
	je	.L693
.L624:
	movq	-560(%rbp), %rdi
	movq	%r8, -600(%rbp)
	call	BN_get_word@PLT
	movq	-600(%rbp), %r8
	movq	%rax, %rdx
	movq	%rax, %rcx
	shrq	$32, %rdx
	jne	.L625
	movq	-576(%rbp), %rdi
	movl	%eax, %edx
	leaq	.LC38(%rip), %rsi
	xorl	%eax, %eax
	call	BIO_printf@PLT
	movq	-600(%rbp), %r8
.L626:
	leaq	-576(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r8, -600(%rbp)
	call	_ZN4node6crypto12_GLOBAL__N_19ToV8ValueEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterIS5_XadL_Z12BIO_free_allEEEEE
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	680(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L693
	movq	-592(%rbp), %rdi
	xorl	%esi, %esi
	call	i2d_RSA_PUBKEY@PLT
	movq	-600(%rbp), %r8
	testl	%eax, %eax
	movl	%eax, %r9d
	js	.L846
	movq	360(%rbx), %rax
	movq	%r8, -608(%rbp)
	movslq	%r9d, %rsi
	movl	%r9d, -600(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	-600(%rbp), %r9d
	movq	-608(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L680
	movl	%r9d, %esi
	movq	%r8, -624(%rbp)
	call	uv_buf_init@PLT
	movq	-592(%rbp), %rdi
	leaq	-552(%rbp), %rsi
	movq	%rdx, -600(%rbp)
	movq	%rax, -552(%rbp)
	movq	%rax, -608(%rbp)
	call	i2d_RSA_PUBKEY@PLT
	movq	-608(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	-600(%rbp), %rdx
	movq	%r9, %rsi
	movq	%r9, -616(%rbp)
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb@PLT
	movq	-624(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, -608(%rbp)
	je	.L847
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%r8, -616(%rbp)
	call	uv_buf_init@PLT
	movq	-616(%rbp), %r8
	movq	%rdx, %rdi
	movq	%rax, -536(%rbp)
	movq	%rax, %r9
	movq	%rdi, -528(%rbp)
	movq	%rdi, -600(%rbp)
.L629:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r8, -624(%rbp)
	movq	%r9, -616(%rbp)
	call	uv_buf_init@PLT
	movq	-616(%rbp), %r9
	movq	-624(%rbp), %r8
	movq	%rax, -536(%rbp)
	testq	%r9, %r9
	movq	%rdx, -528(%rbp)
	je	.L691
	movq	360(%rbx), %rax
	movq	%r8, -616(%rbp)
	movq	%r9, %rsi
	movq	-600(%rbp), %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-616(%rbp), %r8
.L691:
	movq	360(%rbx), %rax
	movq	-608(%rbp), %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r8, -600(%rbp)
	movq	1440(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v86ObjectEEEbNS3_5LocalINS3_7ContextEEENS5_IS4_EENS5_INS3_5ValueEEENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L693
	movq	-584(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-592(%rbp), %rdi
	call	RSA_free@PLT
	movq	-600(%rbp), %r8
	jmp	.L608
.L840:
	leaq	_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi177EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L637:
	movq	-592(%rbp), %rdi
	call	EC_KEY_get0_public_key@PLT
	testq	%rax, %rax
	je	.L848
	movq	-592(%rbp), %rdi
	movq	%rax, -608(%rbp)
	call	EC_KEY_get_conv_form@PLT
	movq	-608(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	-600(%rbp), %rsi
	movl	%eax, %ecx
	call	_ZN4node6crypto15ECPointToBufferEPNS_11EnvironmentEPK11ec_group_stPK11ec_point_st23point_conversion_form_tPPKc
	movq	%rax, %rcx
.L640:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1440(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L647
	movq	-600(%rbp), %rdi
	call	EC_GROUP_get_curve_name@PLT
	movl	%eax, -600(%rbp)
	testl	%eax, %eax
	jne	.L849
.L642:
	movq	-584(%rbp), %rdi
	call	EVP_PKEY_free@PLT
	movq	-592(%rbp), %rdi
	call	EC_KEY_free@PLT
	leaq	-544(%rbp), %r8
	jmp	.L608
.L633:
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, %rcx
	jmp	.L632
.L837:
	call	__stack_chk_fail@PLT
.L848:
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
	jmp	.L640
.L625:
	movq	-576(%rbp), %rdi
	leaq	.LC39(%rip), %rsi
	xorl	%eax, %eax
	movq	%r8, -600(%rbp)
	call	BIO_printf@PLT
	movq	-600(%rbp), %r8
	jmp	.L626
.L849:
	movl	-600(%rbp), %edi
	call	OBJ_nid2sn@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L644
	movq	352(%rbx), %rdi
	orl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L850
.L646:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	208(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L647
	movl	-600(%rbp), %edi
	call	EC_curve_nid2nist@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L648
	movq	352(%rbx), %rdi
	orl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L851
.L650:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1080(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	jne	.L642
	jmp	.L647
.L847:
	movq	-616(%rbp), %r9
	jmp	.L629
.L846:
	leaq	_ZZN4node6crypto12_GLOBAL__N_19GetPubKeyEPNS_11EnvironmentERKSt10unique_ptrI6rsa_stNS_15FunctionDeleterIS5_XadL_Z8RSA_freeEEEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L680:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L669:
	movq	352(%rbx), %rdi
	movq	-584(%rbp), %rdx
	movq	-464(%rbp), %rsi
	movq	-632(%rbp), %r12
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-464(%rbp), %rdi
	movq	%rax, %rcx
	testq	%rdi, %rdi
	je	.L674
	cmpq	-608(%rbp), %rdi
	je	.L674
	movq	%rax, -584(%rbp)
	call	free@PLT
	movq	-584(%rbp), %rcx
.L674:
	movq	ASN1_OBJECT_free@GOTPCREL(%rip), %rsi
	movq	-592(%rbp), %rdi
	movq	%rcx, -584(%rbp)
	call	OPENSSL_sk_pop_free@PLT
	movq	-584(%rbp), %rcx
.L675:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	696(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L659
	movq	%r13, %rdi
	call	X509_get_serialNumber@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L687
	xorl	%esi, %esi
	call	ASN1_INTEGER_to_BN@PLT
	movq	%rax, -584(%rbp)
	testq	%rax, %rax
	je	.L687
	movq	%rax, %rdi
	call	BN_bn2hex@PLT
	testq	%rax, %rax
	je	.L677
	movq	352(%rbx), %rdi
	orl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%rax, -592(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-592(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L852
.L678:
	movq	%r8, %rdi
	movl	$19, %edx
	leaq	.LC40(%rip), %rsi
	movq	%rcx, -592(%rbp)
	call	CRYPTO_free@PLT
	movq	-584(%rbp), %rdi
	call	BN_free@PLT
	movq	-592(%rbp), %rcx
.L679:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1552(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v85ValueEEEbNS3_5LocalINS3_7ContextEEENS5_INS3_6ObjectEEENS5_IS4_EENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L659
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	i2d_X509@PLT
	movslq	%eax, %rsi
	movq	360(%rbx), %rax
	movl	%esi, -584(%rbp)
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movl	-584(%rbp), %r8d
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L680
	movl	%r8d, %esi
	call	uv_buf_init@PLT
	movq	%r13, %rdi
	leaq	-552(%rbp), %rsi
	movq	%rdx, -584(%rbp)
	movq	%rax, -552(%rbp)
	movq	%rax, -592(%rbp)
	call	i2d_X509@PLT
	movq	-592(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	-584(%rbp), %rdx
	movq	%r8, %rsi
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L853
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	uv_buf_init@PLT
	movq	%rdx, %rdi
	movq	%rax, -536(%rbp)
	movq	%rax, %r8
	movq	%rdi, -528(%rbp)
	movq	%rdi, -584(%rbp)
.L682:
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r8, -592(%rbp)
	call	uv_buf_init@PLT
	movq	-592(%rbp), %r8
	movq	%rax, -536(%rbp)
	movq	%rdx, -528(%rbp)
	testq	%r8, %r8
	je	.L685
	movq	360(%rbx), %rax
	movq	-584(%rbp), %rdx
	movq	%r8, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L685:
	movq	360(%rbx), %rax
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1456(%rax), %rdx
	call	_ZN4node6crypto12_GLOBAL__N_13SetIN2v86ObjectEEEbNS3_5LocalINS3_7ContextEEENS5_IS4_EENS5_INS3_5ValueEEENS3_10MaybeLocalIT_EE
	testb	%al, %al
	je	.L659
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
	jmp	.L607
.L853:
	movq	-592(%rbp), %r8
	jmp	.L682
.L852:
	movq	%rax, -600(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-600(%rbp), %rcx
	movq	-592(%rbp), %r8
	jmp	.L678
.L677:
	movq	-584(%rbp), %rdi
	call	BN_free@PLT
.L687:
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
	jmp	.L679
.L660:
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
	jmp	.L675
.L845:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L844:
	movq	%rax, -640(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-640(%rbp), %rax
	jmp	.L671
.L851:
	movq	%rax, -600(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-600(%rbp), %rcx
	jmp	.L650
.L648:
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
	jmp	.L650
.L850:
	movq	%rax, -608(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-608(%rbp), %rcx
	jmp	.L646
.L644:
	movq	352(%rbx), %rax
	leaq	88(%rax), %rcx
	jmp	.L646
.L843:
	movq	-608(%rbp), %rsi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	jmp	.L663
.L842:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-600(%rbp), %r8
	movq	%r8, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L666
.L667:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L841:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9085:
	.size	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st, .-_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st
	.p2align 4
	.globl	_ZN4node6crypto7GetCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.type	_ZN4node6crypto7GetCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, @function
_ZN4node6crypto7GetCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE:
.LFB9034:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdi
	call	SSL_get_certificate@PLT
	testq	%rax, %rax
	je	.L860
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st
	movq	%rax, %r12
	call	ERR_clear_error@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_restore_state
	movq	352(%r12), %r12
	call	ERR_clear_error@PLT
	addq	$8, %rsp
	addq	$88, %r12
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9034:
	.size	_ZN4node6crypto7GetCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE, .-_ZN4node6crypto7GetCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEE
	.p2align 4
	.globl	_ZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbb
	.type	_ZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbb, @function
_ZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbb:
.LFB9082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.L862
	call	SSL_get_peer_cert_chain@PLT
	movq	%rax, %r14
.L863:
	testq	%r14, %r14
	je	.L876
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	testl	%eax, %eax
	je	.L876
	testb	%bl, %bl
	je	.L965
	xorl	%esi, %esi
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rsi
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L874:
	movq	%r8, %rdi
	call	X509_free@PLT
.L873:
	testq	%r12, %r12
	je	.L876
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
.L876:
	movq	352(%r13), %r13
	addq	$88, %r13
.L866:
	call	ERR_clear_error@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L966
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L862:
	.cfi_restore_state
	call	SSL_get_peer_certificate@PLT
	movq	(%r15), %rdi
	movq	%rax, -72(%rbp)
	call	SSL_get_peer_cert_chain@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L863
	testb	%bl, %bl
	je	.L967
	movq	%rsi, %r14
.L908:
	movq	%r13, %rdi
	call	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st
	movq	%rax, %r13
.L902:
	testq	%r14, %r14
	je	.L866
	movq	%r14, %rdi
	call	X509_free@PLT
	jmp	.L866
.L967:
	xorl	%edi, %edi
	movq	%rsi, -72(%rbp)
	call	OPENSSL_sk_new@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	OPENSSL_sk_push@PLT
.L871:
	xorl	%ebx, %ebx
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L968:
	movq	%r14, %rdi
	movl	%ebx, %esi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rdi
	call	X509_dup@PLT
	testq	%rax, %rax
	je	.L873
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	OPENSSL_sk_push@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	je	.L874
	addl	$1, %ebx
.L875:
	movq	%r14, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jg	.L968
	testq	%r12, %r12
	je	.L876
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L969
	movq	%r13, %rdi
	call	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L970
	movq	352(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	OPENSSL_sk_delete@PLT
	movq	%rax, %r14
	movq	-96(%rbp), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L889:
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jle	.L880
.L971:
	movl	%ebx, %esi
	movq	%r12, %rdi
	call	OPENSSL_sk_value@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	X509_check_issued@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	je	.L881
	movq	%r12, %rdi
	addl	$1, %ebx
	call	OPENSSL_sk_num@PLT
	cmpl	%ebx, %eax
	jg	.L971
.L880:
	movq	%r12, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jne	.L889
	.p2align 4,,10
	.p2align 3
.L974:
	movq	352(%r13), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -104(%rbp)
	.p2align 4,,10
	.p2align 3
.L898:
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	X509_check_issued@PLT
	testl	%eax, %eax
	je	.L890
	movq	(%r15), %rdi
	call	SSL_get_SSL_CTX@PLT
	movq	%rax, %rdi
	call	SSL_CTX_get_cert_store@PLT
	movq	%rax, %rbx
	call	X509_STORE_CTX_new@PLT
	testq	%rax, %rax
	je	.L890
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -72(%rbp)
	call	X509_STORE_CTX_init@PLT
	movq	-72(%rbp), %r8
	cmpl	$1, %eax
	je	.L972
	movq	%r8, %rdi
	call	X509_STORE_CTX_free@PLT
.L890:
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	X509_check_issued@PLT
	testl	%eax, %eax
	je	.L973
.L900:
	movq	-96(%rbp), %r13
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L881:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L893
	movq	360(%r13), %rax
	movq	960(%rax), %r8
	movq	(%rcx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L885
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L885
	cmpl	$5, 43(%rax)
	je	.L886
	.p2align 4,,10
	.p2align 3
.L885:
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%r8, %rdx
	movq	%rcx, -72(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-72(%rbp), %rcx
	testb	%al, %al
	je	.L893
.L886:
	movl	%ebx, %esi
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	call	OPENSSL_sk_delete@PLT
	testq	%r14, %r14
	movq	-72(%rbp), %rcx
	je	.L887
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	call	X509_free@PLT
	movq	-80(%rbp), %rax
	movq	-72(%rbp), %rcx
.L887:
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	movq	%rax, %r14
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %ebx
	jne	.L889
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L893:
	xorl	%r13d, %r13d
.L888:
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L970:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	X509_free@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L972:
	movq	-104(%rbp), %rdi
	movq	%r8, %rsi
	movq	%r14, %rdx
	call	X509_STORE_CTX_get1_issuer@PLT
	movq	-72(%rbp), %r8
	movl	%eax, %ebx
	movq	%r8, %rdi
	call	X509_STORE_CTX_free@PLT
	cmpl	$1, %ebx
	jne	.L890
	movq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN4node6crypto12X509ToObjectEPNS_11EnvironmentEP7x509_st
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L893
	movq	360(%r13), %rax
	movq	960(%rax), %r8
	movq	(%rbx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L895
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L895
	cmpl	$5, 43(%rax)
	je	.L896
	.p2align 4,,10
	.p2align 3
.L895:
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r8, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L893
.L896:
	movq	-64(%rbp), %rax
	testq	%r14, %r14
	je	.L897
	movq	%r14, %rdi
	movq	%rax, -72(%rbp)
	call	X509_free@PLT
	movq	-72(%rbp), %rax
.L897:
	movq	%rbx, -80(%rbp)
	movq	%rax, %r14
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L969:
	leaq	_ZZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L973:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	960(%rax), %r8
	movq	-80(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L899
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L899
	cmpl	$5, 43(%rax)
	je	.L900
.L899:
	movq	-80(%rbp), %rcx
	movq	%r8, %rdx
	xorl	%r13d, %r13d
	movq	%rcx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L900
	jmp	.L888
.L966:
	call	__stack_chk_fail@PLT
.L965:
	xorl	%edi, %edi
	call	OPENSSL_sk_new@PLT
	movq	%rax, %r12
	jmp	.L871
	.cfi_endproc
.LFE9082:
	.size	_ZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbb, .-_ZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbb
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE:
.LFB11801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpq	$1, %rsi
	je	.L1003
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L1004
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%r12), %rax
	movq	%rax, -64(%rbp)
.L977:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L980
	leaq	16(%r12), %rax
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, -56(%rbp)
	xorl	%r15d, %r15d
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	(%r8), %rax
	movl	%edi, %r14d
	movq	%rax, (%rsi)
	movq	%rsi, (%r8)
.L983:
	movq	%rsi, %r8
	testq	%r10, %r10
	je	.L981
.L1007:
	movq	%r10, %rsi
.L979:
	movq	72(%rsi), %rax
	xorl	%edx, %edx
	movq	%r9, %r11
	movq	(%rsi), %r10
	divq	%rbx
	testq	%r8, %r8
	setne	%dil
	cmpq	%r11, %rdx
	movq	%rdx, %rcx
	movq	%rdx, %r9
	sete	%al
	andb	%al, %dil
	jne	.L1005
	testb	%r14b, %r14b
	je	.L984
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L984
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%r11, %rdx
	je	.L984
	movq	%r8, 0(%r13,%rdx,8)
.L984:
	leaq	0(%r13,%rcx,8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1006
	movq	(%rdx), %rdx
	xorl	%r14d, %r14d
	movq	%rsi, %r8
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
	testq	%r10, %r10
	jne	.L1007
.L981:
	testb	%dil, %dil
	je	.L980
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L980
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%rdx, %rcx
	je	.L980
	movq	%rsi, 0(%r13,%rdx,8)
.L980:
	movq	(%r12), %rdi
	cmpq	%rdi, -64(%rbp)
	je	.L987
	call	_ZdlPv@PLT
.L987:
	movq	%rbx, 8(%r12)
	movq	%r13, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	.cfi_restore_state
	movq	16(%r12), %rdx
	movq	%rdx, (%rsi)
	movq	-56(%rbp), %rdx
	movq	%rsi, 16(%r12)
	movq	%rdx, (%rax)
	cmpq	$0, (%rsi)
	je	.L989
	movq	%rsi, 0(%r13,%r15,8)
	xorl	%r14d, %r14d
	movq	%rcx, %r15
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L989:
	xorl	%r14d, %r14d
	movq	%rcx, %r15
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1003:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, -64(%rbp)
	jmp	.L977
.L1004:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11801:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_:
.LFB11612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	movl	$1, %ecx
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$72, %rsp
	movq	-8(%rdi), %rdx
	movq	-24(%rdi), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1050
.L1009:
	movq	%r13, 72(%r12)
	movq	8(%rbx), %r8
	movq	%r13, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	%rdx, %r9
	testq	%r15, %r15
	jne	.L1051
.L1010:
	movq	(%rbx), %rsi
	leaq	(%rsi,%r9,8), %r11
	movq	(%r11), %rcx
	testq	%rcx, %rcx
	je	.L1012
	movq	(%rcx), %r10
	movq	%r15, -72(%rbp)
	movq	%rcx, %r15
	movq	%rbx, %rcx
	movq	%r13, %rbx
	movq	%r8, %r13
	movq	72(%r10), %rsi
	movq	%r10, %r14
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1015
	movq	72(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r14, %r15
	movq	%rsi, %rax
	divq	%r13
	cmpq	%rdx, %r9
	jne	.L1015
	movq	%rdi, %r14
.L1016:
	cmpq	%rsi, %rbx
	jne	.L1013
	movq	16(%r12), %rdx
	cmpq	16(%r14), %rdx
	jne	.L1013
	movq	%r10, -64(%rbp)
	movq	8(%r12), %rdi
	movq	%r11, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1014
	movq	8(%r14), %rsi
	movq	%r9, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%rdi, -80(%rbp)
	call	memcmp@PLT
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	movq	-96(%rbp), %rcx
	movq	-104(%rbp), %r9
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	jne	.L1013
.L1014:
	movq	%r13, %r8
	movq	%rbx, %r13
	movq	%rcx, %rbx
	movq	%r15, %rcx
	movq	-72(%rbp), %r15
	movq	%r14, (%r12)
	movq	%r12, (%rcx)
	cmpq	%rcx, %r15
	je	.L1021
.L1018:
	addq	$1, 24(%rbx)
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1050:
	.cfi_restore_state
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	jmp	.L1009
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	%r10, (%r12)
	movq	(%r11), %rax
	movq	%rcx, %rbx
	movq	%r12, (%rax)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1051:
	cmpq	72(%r15), %r13
	jne	.L1010
	movq	16(%r12), %rdx
	cmpq	16(%r15), %rdx
	jne	.L1010
	movq	8(%r12), %rdi
	testq	%rdx, %rdx
	je	.L1011
	movq	8(%r15), %rsi
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdi
	movq	-64(%rbp), %rdx
	testl	%eax, %eax
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	jne	.L1010
.L1011:
	movq	(%r15), %rax
	movq	%rax, (%r12)
	movq	%r12, (%r15)
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1018
	movq	72(%rax), %r14
	cmpq	%r14, %r13
	je	.L1052
.L1019:
	movq	%r14, %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r9
	je	.L1018
	movq	(%rbx), %rax
	movq	%r12, (%rax,%rdx,8)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	16(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 16(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L1023
	movq	72(%rax), %rax
	xorl	%edx, %edx
	divq	%r8
	movq	%r12, (%rsi,%rdx,8)
.L1023:
	leaq	16(%rbx), %rax
	movq	%rax, (%r11)
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1052:
	cmpq	16(%rax), %rdx
	jne	.L1019
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1018
	movq	8(%rax), %rsi
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	je	.L1018
	jmp	.L1019
	.cfi_endproc
.LFE11612:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_
	.section	.rodata.str1.1
.LC42:
	.string	"IP Address:"
.LC43:
	.string	"basic_string::substr"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.section	.rodata.str1.1
.LC45:
	.string	"URI:"
	.text
	.p2align 4
	.globl	_ZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_st
	.type	_ZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_st, @function
_ZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_st:
.LFB8997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$408, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	48(%rdi), %rax
	movq	$1, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 24(%rdi)
	movl	$0x3f800000, 32(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movl	$-1, %edx
	movl	$85, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	X509_get_ext_by_NID@PLT
	testl	%eax, %eax
	jns	.L1182
.L1055:
	testq	%r14, %r14
	je	.L1053
	movq	%r14, %rdi
	call	BIO_free_all@PLT
.L1053:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1183
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	%eax, %esi
	call	X509_get_ext@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1184
	movq	%rax, %rdi
	call	X509V3_EXT_get@PLT
	movl	$85, %edi
	movq	%rax, %rbx
	movq	%rax, -384(%rbp)
	call	X509V3_EXT_get_nid@PLT
	cmpq	%rax, %rbx
	jne	.L1185
	movq	%r12, %rdi
	call	X509V3_EXT_d2i@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1055
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	movq	%r15, -392(%rbp)
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jge	.L1059
	.p2align 4,,10
	.p2align 3
.L1191:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	BIO_ctrl@PLT
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	OPENSSL_sk_value@PLT
	cmpl	$2, (%rax)
	movq	%rax, %rsi
	je	.L1186
	movq	-384(%rbp), %rdi
	xorl	%edx, %edx
	call	i2v_GENERAL_NAME@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1069
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r14, %rdi
	call	X509V3_EXT_val_prn@PLT
	movq	X509V3_conf_free@GOTPCREL(%rip), %rsi
	movq	%r13, %rdi
	leaq	-336(%rbp), %r15
	call	OPENSSL_sk_pop_free@PLT
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r14, %rdi
	leaq	-368(%rbp), %rcx
	call	BIO_ctrl@PLT
	movq	-368(%rbp), %rax
	movq	8(%rax), %r8
	movq	(%rax), %r13
	movq	%r15, -352(%rbp)
	movq	%r8, %rax
	addq	%r13, %rax
	je	.L1132
	testq	%r8, %r8
	je	.L1071
.L1132:
	movq	%r13, -360(%rbp)
	cmpq	$15, %r13
	ja	.L1187
	cmpq	$1, %r13
	jne	.L1075
	movzbl	(%r8), %eax
	leaq	-352(%rbp), %r9
	movb	%al, -336(%rbp)
	movq	%r15, %rax
.L1076:
	movq	%r13, -344(%rbp)
	xorl	%esi, %esi
	movq	%r9, %rdi
	leaq	.LC42(%rip), %rcx
	movb	$0, (%rax,%r13)
	movl	$11, %edx
	movq	%r9, -376(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEmmPKc@PLT
	movq	-376(%rbp), %r9
	testl	%eax, %eax
	jne	.L1077
	movq	-344(%rbp), %r8
	cmpq	$10, %r8
	jbe	.L1188
	leaq	-272(%rbp), %r13
	subq	$11, %r8
	movq	-352(%rbp), %rcx
	leaq	-288(%rbp), %rdi
	movq	%r13, -288(%rbp)
	movq	%r8, -360(%rbp)
	cmpq	$15, %r8
	ja	.L1189
	cmpq	$1, %r8
	jne	.L1081
	movzbl	11(%rcx), %eax
	movb	%al, -272(%rbp)
	movq	%r13, %rax
.L1082:
	movq	%r8, -280(%rbp)
	movl	$80, %edi
	movb	$0, (%rax,%r8)
	call	_Znwm@PLT
	movl	$28777, %esi
	leaq	24(%rax), %rdi
	movw	%si, 24(%rax)
	movq	%rax, %rcx
	leaq	56(%rax), %rax
	movq	$0, -56(%rax)
	movq	%rdi, -48(%rax)
	movq	$2, -40(%rax)
	movb	$0, -30(%rax)
	movq	%rax, 40(%rcx)
	movq	-288(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1190
	movq	%rax, 40(%rcx)
	movq	-272(%rbp), %rax
	movq	%rax, 56(%rcx)
.L1084:
	movq	-280(%rbp), %rax
	movl	$3339675911, %edx
	movl	$2, %esi
	movq	%rcx, -376(%rbp)
	movq	%r13, -288(%rbp)
	movq	%rax, 48(%rcx)
	movq	$0, -280(%rbp)
	movb	$0, -272(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	-392(%rbp), %rdi
	movq	-376(%rbp), %rcx
	xorl	%esi, %esi
	movq	%rax, %rdx
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_
	movq	-288(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1086
.L1165:
	call	_ZdlPv@PLT
	movq	-352(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L1166
	.p2align 4,,10
	.p2align 3
.L1069:
	addl	$1, %r12d
.L1194:
	movq	%rbx, %rdi
	call	OPENSSL_sk_num@PLT
	cmpl	%eax, %r12d
	jl	.L1191
.L1059:
	movq	GENERAL_NAME_free@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	movq	-392(%rbp), %r15
	call	OPENSSL_sk_pop_free@PLT
	jmp	.L1055
	.p2align 4,,10
	.p2align 3
.L1186:
	movq	8(%rax), %rax
	movq	%r14, %rdi
	leaq	-304(%rbp), %r13
	movq	8(%rax), %rsi
	movl	(%rax), %edx
	call	BIO_write@PLT
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r14, %rdi
	leaq	-368(%rbp), %rcx
	call	BIO_ctrl@PLT
	movq	-368(%rbp), %rax
	movq	8(%rax), %r8
	movq	(%rax), %r15
	movq	%r13, -320(%rbp)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L1061
	testq	%r8, %r8
	je	.L1071
.L1061:
	movq	%r15, -360(%rbp)
	cmpq	$15, %r15
	ja	.L1192
	cmpq	$1, %r15
	jne	.L1064
	movzbl	(%r8), %eax
	movb	%al, -304(%rbp)
	movq	%r13, %rax
.L1065:
	movq	%r15, -312(%rbp)
	movl	$80, %edi
	movb	$0, (%rax,%r15)
	call	_Znwm@PLT
	movl	$28260, %r8d
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %r15
	leaq	56(%rax), %rax
	movq	%rdi, -48(%rax)
	movw	%r8w, -32(%rax)
	movb	$115, 2(%rdi)
	movq	$3, -40(%rax)
	movb	$0, -29(%rax)
	movq	%rax, 40(%r15)
	movq	-320(%rbp), %rax
	cmpq	%r13, %rax
	je	.L1193
	movq	%rax, 40(%r15)
	movq	-304(%rbp), %rax
	movq	%rax, 56(%r15)
.L1067:
	movq	-312(%rbp), %rax
	movl	$3339675911, %edx
	movl	$3, %esi
	movq	%r13, -320(%rbp)
	movq	$0, -312(%rbp)
	movq	%rax, 48(%r15)
	movb	$0, -304(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	-392(%rbp), %rdi
	movq	%r15, %rcx
	xorl	%esi, %esi
	movq	%rax, %rdx
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_
	movq	-320(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1069
.L1166:
	call	_ZdlPv@PLT
	addl	$1, %r12d
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1077:
	xorl	%esi, %esi
	leaq	.LC45(%rip), %rcx
	movl	$4, %edx
	movq	%r9, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEmmPKc@PLT
	testl	%eax, %eax
	je	.L1195
.L1086:
	movq	-352(%rbp), %rdi
	cmpq	%r15, %rdi
	jne	.L1166
	addl	$1, %r12d
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1075:
	testq	%r13, %r13
	jne	.L1196
	movq	%r15, %rax
	leaq	-352(%rbp), %r9
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1187:
	leaq	-352(%rbp), %r9
	leaq	-360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -400(%rbp)
	movq	%r9, %rdi
	movq	%r9, -376(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-376(%rbp), %r9
	movq	-400(%rbp), %r8
	movq	%rax, -352(%rbp)
	movq	%rax, %rdi
	movq	-360(%rbp), %rax
	movq	%rax, -336(%rbp)
.L1074:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r9, -376(%rbp)
	call	memcpy@PLT
	movq	-360(%rbp), %r13
	movq	-352(%rbp), %rax
	movq	-376(%rbp), %r9
	jmp	.L1076
	.p2align 4,,10
	.p2align 3
.L1064:
	testq	%r15, %r15
	jne	.L1197
	movq	%r13, %rax
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1192:
	leaq	-320(%rbp), %rdi
	leaq	-360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -376(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-376(%rbp), %r8
	movq	%rax, -320(%rbp)
	movq	%rax, %rdi
	movq	-360(%rbp), %rax
	movq	%rax, -304(%rbp)
.L1063:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-360(%rbp), %r15
	movq	-320(%rbp), %rax
	jmp	.L1065
	.p2align 4,,10
	.p2align 3
.L1081:
	testq	%r8, %r8
	jne	.L1198
	movq	%r13, %rax
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1193:
	movdqa	-304(%rbp), %xmm1
	movups	%xmm1, 56(%r15)
	jmp	.L1067
	.p2align 4,,10
	.p2align 3
.L1189:
	leaq	-360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -400(%rbp)
	movq	%rcx, -376(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-376(%rbp), %rcx
	movq	-400(%rbp), %r8
	movq	%rax, -288(%rbp)
	movq	%rax, %rdi
	movq	-360(%rbp), %rax
	movq	%rax, -272(%rbp)
.L1080:
	movq	%r8, %rdx
	leaq	11(%rcx), %rsi
	call	memcpy@PLT
	movq	-360(%rbp), %r8
	movq	-288(%rbp), %rax
	jmp	.L1082
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	-344(%rbp), %r8
	cmpq	$3, %r8
	jbe	.L1199
	leaq	-304(%rbp), %r13
	subq	$4, %r8
	movq	-352(%rbp), %rcx
	leaq	-320(%rbp), %rdi
	movq	%r13, -320(%rbp)
	movq	%r8, -360(%rbp)
	cmpq	$15, %r8
	ja	.L1200
	cmpq	$1, %r8
	jne	.L1090
	movzbl	4(%rcx), %eax
	movb	%al, -304(%rbp)
	movq	%r13, %rax
.L1091:
	subq	$8, %rsp
	pxor	%xmm0, %xmm0
	movl	$-1, %edx
	xorl	%r9d, %r9d
	movq	%r8, -312(%rbp)
	leaq	-288(%rbp), %rcx
	movb	$0, (%rax,%r8)
	movq	-320(%rbp), %rdi
	xorl	%r8d, %r8d
	movabsq	$-4294967296, %rax
	movq	%rax, -288(%rbp)
	leaq	-264(%rbp), %rax
	movq	-312(%rbp), %rsi
	movq	%rax, -400(%rbp)
	movq	%rax, -280(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, -408(%rbp)
	movq	%rax, -248(%rbp)
	leaq	-200(%rbp), %rax
	pushq	$0
	movq	%rax, -416(%rbp)
	movq	%rax, -216(%rbp)
	leaq	-168(%rbp), %rax
	movq	%rax, -424(%rbp)
	movq	%rax, -184(%rbp)
	leaq	-136(%rbp), %rax
	movq	%rax, -432(%rbp)
	movq	%rax, -152(%rbp)
	leaq	-104(%rbp), %rax
	movq	$0, -272(%rbp)
	movb	$0, -264(%rbp)
	movq	$0, -240(%rbp)
	movb	$0, -232(%rbp)
	movq	$0, -208(%rbp)
	movb	$0, -200(%rbp)
	movq	$0, -176(%rbp)
	movb	$0, -168(%rbp)
	movq	$0, -144(%rbp)
	movb	$0, -136(%rbp)
	movq	%rax, -440(%rbp)
	movq	%rax, -120(%rbp)
	movq	$0, -112(%rbp)
	movb	$0, -104(%rbp)
	movq	$0, -72(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b@PLT
	movq	-320(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	cmpq	%r13, %rdi
	je	.L1092
	call	_ZdlPv@PLT
.L1092:
	testb	$3, -288(%rbp)
	je	.L1093
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r13
	movq	%rax, -376(%rbp)
	cmpq	%r13, %rax
	je	.L1112
	.p2align 4,,10
	.p2align 3
.L1098:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1095
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	-376(%rbp), %r13
	jne	.L1098
.L1114:
	movq	-88(%rbp), %r13
.L1112:
	testq	%r13, %r13
	je	.L1117
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1117:
	movq	-120(%rbp), %rdi
	cmpq	-440(%rbp), %rdi
	je	.L1118
	call	_ZdlPv@PLT
.L1118:
	movq	-152(%rbp), %rdi
	cmpq	-432(%rbp), %rdi
	je	.L1119
	call	_ZdlPv@PLT
.L1119:
	movq	-184(%rbp), %rdi
	cmpq	-424(%rbp), %rdi
	je	.L1120
	call	_ZdlPv@PLT
.L1120:
	movq	-216(%rbp), %rdi
	cmpq	-416(%rbp), %rdi
	je	.L1121
	call	_ZdlPv@PLT
.L1121:
	movq	-248(%rbp), %rdi
	cmpq	-408(%rbp), %rdi
	je	.L1122
	call	_ZdlPv@PLT
.L1122:
	movq	-280(%rbp), %rdi
	cmpq	-400(%rbp), %rdi
	jne	.L1165
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1190:
	movdqa	-272(%rbp), %xmm2
	movups	%xmm2, 56(%rcx)
	jmp	.L1084
	.p2align 4,,10
	.p2align 3
.L1090:
	testq	%r8, %r8
	jne	.L1201
	movq	%r13, %rax
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1095:
	addq	$32, %r13
	cmpq	%r13, -376(%rbp)
	jne	.L1098
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1093:
	movl	$80, %edi
	call	_Znwm@PLT
	movq	-184(%rbp), %r9
	movq	-176(%rbp), %r8
	movq	$0, (%rax)
	movq	%rax, %r13
	leaq	24(%rax), %rax
	movq	%rax, 8(%r13)
	movl	$29301, %eax
	leaq	56(%r13), %rdi
	movw	%ax, 24(%r13)
	movq	%r9, %rax
	addq	%r8, %rax
	movb	$105, 26(%r13)
	movq	$3, 16(%r13)
	movb	$0, 27(%r13)
	movq	%rdi, 40(%r13)
	je	.L1133
	testq	%r9, %r9
	je	.L1071
.L1133:
	movq	%r8, -360(%rbp)
	cmpq	$15, %r8
	ja	.L1202
	cmpq	$1, %r8
	jne	.L1110
	movzbl	(%r9), %eax
	movb	%al, 56(%r13)
.L1111:
	movq	%r8, 48(%r13)
	movl	$3339675911, %edx
	movb	$0, (%rdi,%r8)
	movq	16(%r13), %rsi
	movq	8(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	-392(%rbp), %rdi
	movq	%r13, %rcx
	xorl	%esi, %esi
	movq	%rax, %rdx
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb0EEEE20_M_insert_multi_nodeEPNSA_10_Hash_nodeIS8_Lb1EEEmSO_
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r13
	movq	%rax, -376(%rbp)
	cmpq	%r13, %rax
	je	.L1112
.L1116:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1113
.L1203:
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	-376(%rbp), %r13
	je	.L1114
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	jne	.L1203
.L1113:
	addq	$32, %r13
	cmpq	%r13, -376(%rbp)
	jne	.L1116
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1200:
	leaq	-360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -400(%rbp)
	movq	%rcx, -376(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-376(%rbp), %rcx
	movq	-400(%rbp), %r8
	movq	%rax, -320(%rbp)
	movq	%rax, %rdi
	movq	-360(%rbp), %rax
	movq	%rax, -304(%rbp)
.L1089:
	movq	%r8, %rdx
	leaq	4(%rcx), %rsi
	call	memcpy@PLT
	movq	-360(%rbp), %r8
	movq	-320(%rbp), %rax
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1184:
	leaq	_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1185:
	leaq	_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1110:
	testq	%r8, %r8
	je	.L1111
.L1109:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-360(%rbp), %r8
	movq	40(%r13), %rdi
	jmp	.L1111
.L1202:
	leaq	40(%r13), %rdi
	leaq	-360(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -448(%rbp)
	movq	%r8, -376(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-376(%rbp), %r8
	movq	-448(%rbp), %r9
	movq	%rax, 40(%r13)
	movq	%rax, %rdi
	movq	-360(%rbp), %rax
	movq	%rax, 56(%r13)
	jmp	.L1109
.L1183:
	call	__stack_chk_fail@PLT
.L1071:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1188:
	movq	%r8, %rcx
	movl	$11, %edx
	leaq	.LC43(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1201:
	movq	%r13, %rdi
	jmp	.L1089
.L1199:
	movq	%r8, %rcx
	movl	$4, %edx
	leaq	.LC43(%rip), %rsi
	leaq	.LC44(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1198:
	movq	%r13, %rdi
	jmp	.L1080
.L1197:
	movq	%r13, %rdi
	jmp	.L1063
.L1196:
	movq	%r15, %rdi
	leaq	-352(%rbp), %r9
	jmp	.L1074
	.cfi_endproc
.LFE8997:
	.size	_ZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_st, .-_ZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_st
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC46:
	.string	"../src/util-inl.h:374"
.LC47:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC49:
	.string	"../src/util-inl.h:325"
.LC50:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"../src/node_crypto_common.cc:716"
	.section	.rodata.str1.1
.LC53:
	.string	"(ext) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"v8::MaybeLocal<v8::Value> node::crypto::{anonymous}::GetInfoString(node::Environment*, const BIOPointer&, X509*) [with int nid = 177; node::crypto::BIOPointer = std::unique_ptr<bio_st, node::FunctionDeleter<bio_st, BIO_free_all> >; X509 = x509_st]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi177EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args, @object
	.size	_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi177EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args, 24
_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi177EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args:
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"v8::MaybeLocal<v8::Value> node::crypto::{anonymous}::GetInfoString(node::Environment*, const BIOPointer&, X509*) [with int nid = 85; node::crypto::BIOPointer = std::unique_ptr<bio_st, node::FunctionDeleter<bio_st, BIO_free_all> >; X509 = x509_st]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi85EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args, @object
	.size	_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi85EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args, 24
_ZZN4node6crypto12_GLOBAL__N_113GetInfoStringILi85EEEN2v810MaybeLocalINS3_5ValueEEEPNS_11EnvironmentERKSt10unique_ptrI6bio_stNS_15FunctionDeleterISA_XadL_Z12BIO_free_allEEEEEP7x509_stE4args:
	.quad	.LC52
	.quad	.LC53
	.quad	.LC55
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args
	.section	.rodata.str1.1
.LC56:
	.string	"../src/util.h:352"
.LC57:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 16; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"../src/node_crypto_common.cc:937"
	.section	.rodata.str1.1
.LC60:
	.string	"first_cert"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"v8::MaybeLocal<v8::Value> node::crypto::GetPeerCert(node::Environment*, const SSLPointer&, bool, bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbbE4args, @object
	.size	_ZZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbbE4args, 24
_ZZN4node6crypto11GetPeerCertEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEbbE4args:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"../src/node_crypto_common.cc:828"
	.align 8
.LC63:
	.string	"(SSL_is_server(ssl.get())) == (0)"
	.align 8
.LC64:
	.string	"v8::MaybeLocal<v8::Object> node::crypto::GetEphemeralKey(node::Environment*, const SSLPointer&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEE4args, @object
	.size	_ZZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEE4args, 24
_ZZN4node6crypto15GetEphemeralKeyEPNS_11EnvironmentERKSt10unique_ptrI6ssl_stNS_15FunctionDeleterIS4_XadL_Z8SSL_freeEEEEEE4args:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"../src/node_crypto_common.cc:670"
	.section	.rodata.str1.1
.LC66:
	.string	"(size) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"v8::MaybeLocal<v8::Object> node::crypto::{anonymous}::GetPubKey(node::Environment*, const RSAPointer&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto12_GLOBAL__N_19GetPubKeyEPNS_11EnvironmentERKSt10unique_ptrI6rsa_stNS_15FunctionDeleterIS5_XadL_Z8RSA_freeEEEEEE4args, @object
	.size	_ZZN4node6crypto12_GLOBAL__N_19GetPubKeyEPNS_11EnvironmentERKSt10unique_ptrI6rsa_stNS_15FunctionDeleterIS5_XadL_Z8RSA_freeEEEEEE4args, 24
_ZZN4node6crypto12_GLOBAL__N_19GetPubKeyEPNS_11EnvironmentERKSt10unique_ptrI6rsa_stNS_15FunctionDeleterIS5_XadL_Z8RSA_freeEEEEEE4args:
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"../src/node_crypto_common.cc:148"
	.align 8
.LC69:
	.string	"(method) == (X509V3_EXT_get_nid(85))"
	.align 8
.LC70:
	.string	"std::unordered_multimap<std::__cxx11::basic_string<char>, std::__cxx11::basic_string<char> > node::crypto::GetCertificateAltNames(X509*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args_0, @object
	.size	_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args_0, 24
_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args_0:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"../src/node_crypto_common.cc:146"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args, @object
	.size	_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args, 24
_ZZN4node6crypto22GetCertificateAltNamesB5cxx11EP7x509_stE4args:
	.quad	.LC71
	.quad	.LC53
	.quad	.LC70
	.weak	_ZZN4node11Environment8AllocateEmE4args
	.section	.rodata.str1.1
.LC72:
	.string	"../src/env-inl.h:889"
.LC73:
	.string	"(ret) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"char* node::Environment::Allocate(size_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8AllocateEmE4args,"awG",@progbits,_ZZN4node11Environment8AllocateEmE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8AllocateEmE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8AllocateEmE4args, 24
_ZZN4node11Environment8AllocateEmE4args:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	3978425819141910832
	.quad	5063528411713059128
	.align 16
.LC35:
	.quad	0
	.quad	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
