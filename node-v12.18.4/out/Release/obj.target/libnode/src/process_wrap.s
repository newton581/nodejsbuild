	.file	"process_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5801:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5801:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB7538:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7538:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB7539:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7539:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB7555:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7555:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ProcessWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_111ProcessWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7615:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7615:
	.size	_ZNK4node12_GLOBAL__N_111ProcessWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_111ProcessWrap10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ProcessWrap8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_111ProcessWrap8SelfSizeEv:
.LFB7617:
	.cfi_startproc
	endbr64
	movl	$224, %eax
	ret
	.cfi_endproc
.LFE7617:
	.size	_ZNK4node12_GLOBAL__N_111ProcessWrap8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_111ProcessWrap8SelfSizeEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9660:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9660:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9664:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9664:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7574:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L15:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7574:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7573:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L21:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7573:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.text
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ProcessWrapD2Ev, @function
_ZN4node12_GLOBAL__N_111ProcessWrapD2Ev:
.LFB9611:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9611:
	.size	_ZN4node12_GLOBAL__N_111ProcessWrapD2Ev, .-_ZN4node12_GLOBAL__N_111ProcessWrapD2Ev
	.set	_ZN4node12_GLOBAL__N_111ProcessWrapD1Ev,_ZN4node12_GLOBAL__N_111ProcessWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ProcessWrapD0Ev, @function
_ZN4node12_GLOBAL__N_111ProcessWrapD0Ev:
.LFB9613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdx
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$224, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9613:
	.size	_ZN4node12_GLOBAL__N_111ProcessWrapD0Ev, .-_ZN4node12_GLOBAL__N_111ProcessWrapD0Ev
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_111ProcessWrap14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_111ProcessWrap14MemoryInfoNameEv:
.LFB7616:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6301507182918595152, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7616:
	.size	_ZNK4node12_GLOBAL__N_111ProcessWrap14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_111ProcessWrap14MemoryInfoNameEv
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sli, @function
_ZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sli:
.LFB7633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-88(%rdi), %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	16(%r12), %r13
	movl	%edx, -120(%rbp)
	movq	352(%r13), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r13), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	pxor	%xmm0, %xmm0
	movq	352(%r13), %rdi
	cvtsi2sdq	%rbx, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	-120(%rbp), %r8d
	movq	%rax, -80(%rbp)
	movl	%r8d, %edi
	call	_ZN4node12signo_stringEi@PLT
	movq	352(%r13), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L44
.L28:
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -72(%rbp)
	movq	360(%r13), %rax
	movq	1168(%rax), %r13
	testq	%rdi, %rdi
	je	.L29
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L45
.L29:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L33
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L46
.L33:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L47
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rax
	jmp	.L28
.L47:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7633:
	.size	_ZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sli, .-_ZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sli
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L49
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L55
.L49:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L53
	cmpw	$1040, %cx
	jne	.L50
.L53:
	movq	23(%rdx), %r14
.L52:
	movq	8(%r12), %r13
	movl	$224, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	movl	$23, %r8d
	addq	$8, %r13
	movq	%rax, %rdi
	leaq	88(%rax), %rcx
	movq	%rax, %r12
	movq	%r13, %rdx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node12_GLOBAL__N_111ProcessWrapE(%rip), %rax
	movq	%r12, %rdi
	movq	%rax, (%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10HandleWrap19MarkAsUninitializedEv@PLT
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L49
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L52
	.cfi_endproc
.LFE7618:
	.size	_ZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ProcessWrap4KillERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111ProcessWrap4KillERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L68
	cmpw	$1040, %cx
	jne	.L57
.L68:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L74
.L60:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L69
	cmpw	$1040, %cx
	jne	.L61
.L69:
	movq	23(%rdx), %r13
.L63:
	testq	%r13, %r13
	je	.L56
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L75
	movq	8(%rbx), %rdi
.L66:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L76
.L67:
	sarq	$32, %rax
	leaq	88(%r13), %rdi
	movq	%rax, %rsi
	call	uv_process_kill@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L56:
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L60
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-40(%rbp), %rax
	jmp	.L67
	.cfi_endproc
.LFE7632:
	.size	_ZN4node12_GLOBAL__N_111ProcessWrap4KillERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_111ProcessWrap4KillERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3304, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%rdi, -3320(%rbp)
	movq	32(%r12), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L167
	cmpw	$1040, %cx
	jne	.L78
.L167:
	movq	23(%rdx), %rbx
.L80:
	movq	%r12, %rdi
	movq	3280(%rbx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L243
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L168
	cmpw	$1040, %cx
	jne	.L82
.L168:
	movq	23(%rdx), %rax
	movq	%rax, -3328(%rbp)
.L84:
	cmpq	$0, -3328(%rbp)
	je	.L77
	movq	-3320(%rbp), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jle	.L244
	movq	8(%rax), %rdi
.L87:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -3288(%rbp)
	testq	%rax, %rax
	je	.L245
.L88:
	leaq	_ZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sli(%rip), %rax
	movq	-3288(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	%r14, %rsi
	movq	%rax, -3280(%rbp)
	movq	360(%rbx), %rax
	movq	$0, -3224(%rbp)
	movq	1776(%rax), %rdx
	movups	%xmm0, -3272(%rbp)
	movups	%xmm0, -3256(%rbp)
	movups	%xmm0, -3240(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L246
.L89:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L90
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L247
.L90:
	movq	%r12, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L248
	movq	%r12, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	orl	$1, -3240(%rbp)
	movl	%eax, -3224(%rbp)
.L91:
	movq	360(%rbx), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	808(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L249
.L93:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L94
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L250
.L94:
	movq	%r12, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L251
	movq	%r12, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	orl	$2, -3240(%rbp)
	movl	%eax, -3220(%rbp)
.L95:
	movq	360(%rbx), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	744(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L252
.L97:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L253
.L98:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L140:
	movq	360(%rbx), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	1912(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L254
.L127:
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	je	.L142
	orl	$16, -3240(%rbp)
.L142:
	movq	360(%rbx), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	1920(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L255
.L143:
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	je	.L144
	orl	$4, -3240(%rbp)
.L144:
	movq	360(%rbx), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	464(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L256
.L145:
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	je	.L146
	orl	$8, -3240(%rbp)
.L146:
	movq	360(%rbx), %rax
	movq	-3328(%rbp), %r15
	leaq	-3280(%rbp), %rdx
	movq	2360(%rax), %rdi
	leaq	88(%r15), %rsi
	call	uv_spawn@PLT
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	_ZN4node10HandleWrap17MarkAsInitializedEv@PLT
	testl	%r12d, %r12d
	jne	.L147
	cmpq	%r15, 88(%r15)
	jne	.L257
	movq	-3328(%rbp), %rax
	movq	8(%rax), %r13
	testq	%r13, %r13
	je	.L149
	movzbl	11(%r13), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L258
.L149:
	movq	-3328(%rbp), %rax
	movq	352(%rbx), %rdi
	movl	192(%rax), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1336(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L147
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	movq	-3264(%rbp), %r8
	testq	%r8, %r8
	je	.L152
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L153
	movl	$8, %ebx
	.p2align 4,,10
	.p2align 3
.L154:
	call	free@PLT
	movq	-3264(%rbp), %r8
	movq	(%r8,%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L154
	testq	%r8, %r8
	je	.L152
.L153:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L152:
	movq	-3256(%rbp), %r8
	testq	%r8, %r8
	je	.L157
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L158
	movl	$8, %ebx
	.p2align 4,,10
	.p2align 3
.L159:
	call	free@PLT
	movq	-3256(%rbp), %r8
	movq	(%r8,%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L159
	testq	%r8, %r8
	je	.L157
.L158:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
.L157:
	movq	-3232(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L161
	call	_ZdaPv@PLT
.L161:
	movq	-3320(%rbp), %rax
	movq	-2144(%rbp), %rdi
	salq	$32, %r12
	movq	(%rax), %rax
	movq	%r12, 24(%rax)
	testq	%rdi, %rdi
	je	.L162
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L162
	call	free@PLT
.L162:
	movq	-3200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L77
	leaq	-3192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L77
	call	free@PLT
.L77:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L259
	addq	$3304, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L98
	movq	352(%rbx), %rsi
	leaq	-3216(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-3200(%rbp), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -3272(%rbp)
	movq	360(%rbx), %rax
	movq	200(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L260
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L261
.L101:
	movq	360(%rbx), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	432(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L262
.L110:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L111
	movq	-1(%rax), %rax
	movq	%r12, %rdx
	cmpw	$63, 11(%rax)
	ja	.L111
.L112:
	movq	352(%rbx), %rsi
	leaq	-2160(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -2160(%rbp)
	je	.L113
	movq	-2144(%rbp), %rax
	movq	%rax, -3248(%rbp)
.L113:
	movq	360(%rbx), %rax
	movq	-3288(%rbp), %rdi
	movq	%r14, %rsi
	movq	624(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L263
	movq	%rax, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L264
.L115:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %r15
	movq	-3288(%rbp), %rdi
	movq	1680(%rax), %rdx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -3304(%rbp)
	testq	%rax, %rax
	je	.L265
.L124:
	movq	-3304(%rbp), %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %edi
	movq	%rdi, %r13
	salq	$4, %rdi
	call	_Znam@PLT
	movl	%r13d, -3236(%rbp)
	movq	%rax, -3232(%rbp)
	leal	-1(%r13), %eax
	movq	%rax, -3312(%rbp)
	testl	%r13d, %r13d
	jne	.L141
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%r12, %rax
	salq	$4, %rax
	addq	-3232(%rbp), %rax
	movl	$0, (%rax)
.L131:
	leaq	1(%r12), %rax
	cmpq	%r12, -3312(%rbp)
	je	.L140
	movq	%rax, %r12
.L141:
	movq	-3304(%rbp), %rdi
	movl	%r12d, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L266
.L128:
	movq	360(%rbx), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L267
.L129:
	movq	360(%rbx), %rax
	movq	%rdi, -3296(%rbp)
	movq	880(%rax), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	movq	-3296(%rbp), %rdi
	testb	%al, %al
	jne	.L268
	movq	360(%rbx), %rax
	movq	%rdi, -3296(%rbp)
	movq	1352(%rax), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	movq	-3296(%rbp), %rdi
	testb	%al, %al
	je	.L132
	movq	%r12, %rax
	salq	$4, %rax
	addq	-3232(%rbp), %rax
	movq	%rax, -3296(%rbp)
	movl	$49, (%rax)
.L242:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	824(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L269
.L133:
	movq	%rbx, %rdi
	call	_ZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE@PLT
	movq	152(%rax), %rax
	testq	%rax, %rax
	je	.L270
	movq	-3296(%rbp), %rcx
	movq	%rax, 8(%rcx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%edx, %edx
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L132:
	movq	360(%rbx), %rax
	movq	1928(%rax), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L135
	movq	%r12, %rax
	salq	$4, %rax
	addq	-3232(%rbp), %rax
	movq	%rax, -3296(%rbp)
	movl	$4, (%rax)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%rax, -3296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3296(%rbp), %rdi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L266:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L135:
	movq	360(%rbx), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	728(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L271
.L138:
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L272
	movq	%r13, %rdi
	call	_ZNK2v87Integer5ValueEv@PLT
	movq	%rax, %r8
	movq	%r12, %rax
	salq	$4, %rax
	addq	-3232(%rbp), %rax
	movl	$2, (%rax)
	movl	%r8d, 8(%rax)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L247:
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L91
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L250:
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L95
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	movq	-3320(%rbp), %rax
	movq	(%rax), %r12
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -3328(%rbp)
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L261:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -3304(%rbp)
	testl	%eax, %eax
	js	.L273
	movl	%eax, %r15d
	leal	1(%rax), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rdi
	movq	%rax, -3336(%rbp)
	call	_Znam@PLT
	movq	%rax, -3264(%rbp)
	testl	%r15d, %r15d
	je	.L103
	leaq	-1080(%rbp), %rax
	xorl	%r13d, %r13d
	leaq	-1104(%rbp), %r15
	movq	%rax, -3312(%rbp)
	.p2align 4,,10
	.p2align 3
.L109:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L274
.L104:
	movq	352(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-3264(%rbp), %rax
	movq	-1088(%rbp), %rdi
	leaq	(%rax,%r13,8), %rdx
	movq	%rdx, -3296(%rbp)
	call	strdup@PLT
	movq	-3296(%rbp), %rdx
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L275
	movq	-1088(%rbp), %rdi
	cmpq	-3312(%rbp), %rdi
	je	.L106
	testq	%rdi, %rdi
	je	.L106
	call	free@PLT
	addq	$1, %r13
	cmpl	%r13d, -3304(%rbp)
	jg	.L109
.L107:
	movq	-3264(%rbp), %rax
.L103:
	movq	-3336(%rbp), %rcx
	movq	$0, -8(%rax,%rcx)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L106:
	addq	$1, %r13
	cmpl	%r13d, -3304(%rbp)
	jg	.L109
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%r12, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -3304(%rbp)
	testl	%eax, %eax
	js	.L276
	movl	%eax, %r15d
	leal	1(%rax), %eax
	cltq
	salq	$3, %rax
	movq	%rax, %rdi
	movq	%rax, -3336(%rbp)
	call	_Znam@PLT
	movq	%rax, -3256(%rbp)
	testl	%r15d, %r15d
	je	.L117
	leaq	-1080(%rbp), %rax
	xorl	%r13d, %r13d
	leaq	-1104(%rbp), %r15
	movq	%rax, -3312(%rbp)
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L277
.L118:
	movq	352(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-3256(%rbp), %rax
	movq	-1088(%rbp), %rdi
	leaq	(%rax,%r13,8), %rdx
	movq	%rdx, -3296(%rbp)
	call	strdup@PLT
	movq	-3296(%rbp), %rdx
	movq	%rax, (%rdx)
	testq	%rax, %rax
	je	.L278
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L120
	cmpq	-3312(%rbp), %rdi
	je	.L120
	call	free@PLT
	addq	$1, %r13
	cmpl	%r13d, -3304(%rbp)
	jg	.L123
.L121:
	movq	-3256(%rbp), %rax
.L117:
	movq	-3336(%rbp), %rcx
	movq	$0, -8(%rax,%rcx)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$1, %r13
	cmpl	%r13d, -3304(%rbp)
	jg	.L123
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap13StreamForWrapEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%rax, -3336(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3336(%rbp), %rsi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L274:
	movq	%rax, -3296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3296(%rbp), %rdx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L245:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L246:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L249:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rax, -3296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3296(%rbp), %rdx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%rax, -3296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3296(%rbp), %rdx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L260:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L263:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L265:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L271:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap17ParseStdioOptionsEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP20uv_process_options_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L258:
	movq	-3328(%rbp), %rax
	movq	0(%r13), %rsi
	movq	16(%rax), %rax
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
	jmp	.L149
.L256:
	movq	%rax, -3288(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3288(%rbp), %rdi
	jmp	.L145
.L255:
	movq	%rax, -3296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3296(%rbp), %rdi
	jmp	.L143
.L254:
	movq	%rax, -3296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3296(%rbp), %rdi
	jmp	.L127
.L273:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L257:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L276:
	leaq	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L259:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7628:
	.size	_ZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Process"
.LC1:
	.string	"spawn"
.LC2:
	.string	"kill"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L280
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L280
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L280
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L290
.L281:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L291
.L282:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_111ProcessWrap4KillERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L292
.L283:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L293
.L284:
	movq	3280(%rbx), %rsi
	movq	-56(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L294
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L292:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L293:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L294:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7614:
.L280:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7614:
	.text
	.size	_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE3:
	.text
.LHOTE3:
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L296
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L296:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L305
.L297:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L297
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.globl	_Z22_register_process_wrapv
	.type	_Z22_register_process_wrapv, @function
_Z22_register_process_wrapv:
.LFB7636:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7636:
	.size	_Z22_register_process_wrapv, .-_Z22_register_process_wrapv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L324
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L325
.L310:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L314
.L307:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L315
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L311:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L310
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L316
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L312:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L314:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L307
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L316:
	xorl	%edx, %edx
	jmp	.L312
	.cfi_endproc
.LFE8334:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L327
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L329
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L327
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L329
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L327
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L330
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r12, %rsi
	call	*%rax
.L334:
	movq	(%r12), %rax
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L335
	leaq	16(%r12), %rsi
.L336:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L337
	addq	$16, %r12
.L338:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L339
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L367
.L339:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L368
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L343
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L326
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L326:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L367:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L368:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L346
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L341:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L343:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	xorl	%edx, %edx
	jmp	.L341
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7604:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L371
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L374
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L373
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L374
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L371
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L374
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L373
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L374
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L371
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L374
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*32(%rax)
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L371:
	movq	%r12, %rsi
	call	*%rax
.L378:
	movq	(%r12), %rax
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L379
	leaq	40(%r12), %rsi
.L380:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L381
	addq	$40, %r12
.L382:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L383
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L411
.L383:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L412
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L387
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L370
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L370:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L413
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L411:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L412:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L390
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L385:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L387:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L390:
	xorl	%edx, %edx
	jmp	.L385
.L413:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7606:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_111ProcessWrapE, @object
	.size	_ZTVN4node12_GLOBAL__N_111ProcessWrapE, 112
_ZTVN4node12_GLOBAL__N_111ProcessWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_111ProcessWrapD1Ev
	.quad	_ZN4node12_GLOBAL__N_111ProcessWrapD0Ev
	.quad	_ZNK4node12_GLOBAL__N_111ProcessWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_111ProcessWrap14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_111ProcessWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.section	.rodata.str1.1
.LC4:
	.string	"../src/process_wrap.cc"
.LC5:
	.string	"process_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC4
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_111ProcessWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC5
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC6:
	.string	"../src/process_wrap.cc:296"
.LC7:
	.string	"(&wrap->process_) == (handle)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"static void node::{anonymous}::ProcessWrap::OnExit(uv_process_t*, int64_t, int)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sliE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sliE4args, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap6OnExitEP12uv_process_sliE4args:
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.section	.rodata.str1.1
.LC9:
	.string	"../src/process_wrap.cc:262"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"(wrap->process_.data) == (wrap)"
	.align 8
.LC11:
	.string	"static void node::{anonymous}::ProcessWrap::Spawn(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.1
.LC12:
	.string	"../src/process_wrap.cc:225"
.LC13:
	.string	"(options.env[i]) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC11
	.section	.rodata.str1.1
.LC14:
	.string	"../src/process_wrap.cc:219"
.LC15:
	.string	"(envc + 1) > (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC11
	.section	.rodata.str1.1
.LC16:
	.string	"../src/process_wrap.cc:199"
.LC17:
	.string	"(options.args[i]) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC11
	.section	.rodata.str1.1
.LC18:
	.string	"../src/process_wrap.cc:191"
.LC19:
	.string	"(argc + 1) > (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC11
	.section	.rodata.str1.1
.LC20:
	.string	"../src/process_wrap.cc:181"
.LC21:
	.string	"file_v->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC11
	.section	.rodata.str1.1
.LC22:
	.string	"../src/process_wrap.cc:170"
.LC23:
	.string	"gid_v->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC11
	.section	.rodata.str1.1
.LC24:
	.string	"../src/process_wrap.cc:160"
.LC25:
	.string	"uid_v->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap5SpawnERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC11
	.section	.rodata.str1.1
.LC26:
	.string	"../src/process_wrap.cc:134"
.LC27:
	.string	"fd_value->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"static void node::{anonymous}::ProcessWrap::ParseStdioOptions(node::Environment*, v8::Local<v8::Object>, uv_process_options_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap17ParseStdioOptionsEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP20uv_process_options_sE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap17ParseStdioOptionsEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP20uv_process_options_sE4args, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap17ParseStdioOptionsEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP20uv_process_options_sE4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"../src/process_wrap.cc:100"
.LC30:
	.string	"(stream) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"static uv_stream_t* node::{anonymous}::ProcessWrap::StreamForWrap(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap13StreamForWrapEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap13StreamForWrapEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap13StreamForWrapEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"../src/process_wrap.cc:80"
.LC33:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"static void node::{anonymous}::ProcessWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_111ProcessWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC35:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC37:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC38:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC38
	.quad	.LC36
	.quad	.LC39
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC40:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC42:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC43:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC45:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC46:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC48:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
