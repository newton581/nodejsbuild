	.file	"node_report.cc"
	.text
	.section	.text._ZNKSt5ctypeIcE8do_widenEc,"axG",@progbits,_ZNKSt5ctypeIcE8do_widenEc,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt5ctypeIcE8do_widenEc
	.type	_ZNKSt5ctypeIcE8do_widenEc, @function
_ZNKSt5ctypeIcE8do_widenEc:
.LFB5491:
	.cfi_startproc
	endbr64
	movl	%esi, %eax
	ret
	.cfi_endproc
.LFE5491:
	.size	_ZNKSt5ctypeIcE8do_widenEc, .-_ZNKSt5ctypeIcE8do_widenEc
	.section	.text._ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv,"axG",@progbits,_ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv
	.type	_ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv, @function
_ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv:
.LFB5792:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	pxor	%xmm0, %xmm0
	movb	$0, 16(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	leaq	48(%rdi), %rdx
	movq	$0, 8(%rdi)
	movq	%rdx, 32(%rdi)
	movq	$0, 40(%rdi)
	movb	$0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.cfi_endproc
.LFE5792:
	.size	_ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv, .-_ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv
	.section	.text._ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi,"axG",@progbits,_ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi
	.type	_ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi, @function
_ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi:
.LFB5797:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE5797:
	.size	_ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi, .-_ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED2Ev:
.LFB10029:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.cfi_endproc
.LFE10029:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED2Ev
	.section	.text._ZN4node28NativeSymbolDebuggingContextD0Ev,"axG",@progbits,_ZN4node28NativeSymbolDebuggingContextD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node28NativeSymbolDebuggingContextD0Ev
	.type	_ZN4node28NativeSymbolDebuggingContextD0Ev, @function
_ZN4node28NativeSymbolDebuggingContextD0Ev:
.LFB9151:
	.cfi_startproc
	endbr64
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9151:
	.size	_ZN4node28NativeSymbolDebuggingContextD0Ev, .-_ZN4node28NativeSymbolDebuggingContextD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED0Ev:
.LFB10031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L9
	movq	(%rdi), %rax
	call	*8(%rax)
.L9:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10031:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node10JSONWriter12write_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.isra.0, @function
_ZN4node10JSONWriter12write_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.isra.0:
.LFB10554:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-81(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	movq	%r14, %rsi
	subq	$72, %rsp
	.cfi_offset 12, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movb	$34, -81(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -81(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$72, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L18:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10554:
	.size	_ZN4node10JSONWriter12write_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.isra.0, .-_ZN4node10JSONWriter12write_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.isra.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"0x"
	.text
	.p2align 4
	.type	_ZN6reportL16PrintNativeStackEPN4node10JSONWriterE, @function
_ZN6reportL16PrintNativeStackEPN4node10JSONWriterE:
.LFB7785:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	-2616(%rbp), %rdi
	subq	$2696, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node28NativeSymbolDebuggingContext3NewEv@PLT
	movq	-2616(%rbp), %rdi
	leaq	_ZN4node28NativeSymbolDebuggingContext13GetStackTraceEPPvi(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L90
.L20:
	cmpl	$1, 16(%rbx)
	movq	(%rbx), %rdi
	leaq	-2617(%rbp), %r14
	je	.L91
.L21:
	cmpb	$0, 8(%rbx)
	je	.L92
.L22:
	leaq	-2528(%rbp), %rax
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movl	$25441, %r10d
	movq	%rax, -2640(%rbp)
	leaq	-2544(%rbp), %r15
	movq	%rax, -2544(%rbp)
	movabsq	$8382154890716406126, %rax
	movw	%r10w, -2520(%rbp)
	movq	%r15, -2648(%rbp)
	movq	%rax, -2528(%rbp)
	movb	$107, -2518(%rbp)
	movq	$11, -2536(%rbp)
	movb	$0, -2517(%rbp)
	movb	$34, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%rax, %r13
	leaq	-2512(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -2632(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-2504(%rbp), %rdx
	movq	-2512(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-2496(%rbp), %r13
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -2617(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2512(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	movq	-2544(%rbp), %rdi
	cmpq	-2640(%rbp), %rdi
	je	.L27
	call	_ZdlPv@PLT
.L27:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$58, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L28
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L28:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$91, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%rbx)
	movl	$0, 16(%rbx)
	cmpl	$1, %r12d
	jle	.L71
	movq	-2104(%rbp), %rax
	leaq	-2096(%rbp), %rcx
	movq	%r13, %r15
	movq	.LC1(%rip), %xmm4
	movq	%rcx, -2656(%rbp)
	movq	%rax, -2664(%rbp)
	leal	-2(%r12), %eax
	movhps	.LC3(%rip), %xmm4
	leaq	(%rcx,%rax,8), %rax
	movaps	%xmm4, -2720(%rbp)
	movq	%rax, -2736(%rbp)
	leaq	-2384(%rbp), %rax
	movq	%rax, -2688(%rbp)
	leaq	-2432(%rbp), %rax
	movq	%rax, -2672(%rbp)
	leaq	-2400(%rbp), %rax
	movq	%rax, -2680(%rbp)
	leaq	-2488(%rbp), %rax
	movq	%rax, -2728(%rbp)
	.p2align 4,,10
	.p2align 3
.L78:
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L32
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L32
	movl	12(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L32
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -2617(%rbp)
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L34
.L32:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$123, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%rbx)
	movq	-2688(%rbp), %r13
	movl	$0, 16(%rbx)
	movq	%r13, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%rax, -2384(%rbp)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm1, %xmm1
	movups	%xmm1, -2152(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm1, -2136(%rbp)
	movw	%di, -2160(%rbp)
	movq	%rax, -2512(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -2168(%rbp)
	movq	%rcx, -2512(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	$0, -2504(%rbp)
	movq	-2632(%rbp), %rax
	addq	-24(%rcx), %rax
	movq	%rax, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	xorl	%esi, %esi
	movq	%rax, -2496(%rbp)
	movq	-24(%rax), %rax
	addq	%r15, %rax
	movq	%rax, %rdi
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm1, %xmm1
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-2720(%rbp), %xmm2
	movq	-2672(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -2512(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -2512(%rbp)
	addq	$80, %rax
	movaps	%xmm2, -2496(%rbp)
	movaps	%xmm1, -2480(%rbp)
	movaps	%xmm1, -2464(%rbp)
	movaps	%xmm1, -2448(%rbp)
	movq	%rax, -2384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	movq	-2728(%rbp), %rsi
	movq	%rax, -2488(%rbp)
	movq	-2680(%rbp), %rax
	movl	$24, -2424(%rbp)
	movq	%rax, -2416(%rbp)
	movq	$0, -2408(%rbp)
	movb	$0, -2400(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$2, %edx
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2496(%rbp), %rax
	movq	-24(%rax), %r12
	addq	%r15, %r12
	cmpb	$0, 225(%r12)
	je	.L93
.L35:
	movb	$48, 224(%r12)
	movq	-24(%rax), %rdx
	movq	%r15, %rdi
	leaq	-2560(%rbp), %r12
	movq	-2664(%rbp), %rsi
	leaq	-2576(%rbp), %r13
	movq	$16, -2480(%rbp,%rdx)
	movq	-24(%rax), %rdx
	addq	%r15, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	-2448(%rbp), %rax
	movq	%r12, -2576(%rbp)
	movq	$0, -2568(%rbp)
	movb	$0, -2560(%rbp)
	testq	%rax, %rax
	je	.L39
	movq	-2464(%rbp), %r8
	movq	-2456(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L40
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L41:
	movq	.LC1(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-2416(%rbp), %rdi
	movq	%rax, -2512(%rbp)
	addq	$80, %rax
	movhps	.LC2(%rip), %xmm0
	movq	%rax, -2384(%rbp)
	movaps	%xmm0, -2496(%rbp)
	cmpq	-2680(%rbp), %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	movq	-2672(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -2488(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-2688(%rbp), %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -2512(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -2496(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -2496(%rbp,%rax)
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -2512(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -2512(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -2504(%rbp)
	movq	%rax, -2384(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	cmpl	$1, 16(%rbx)
	je	.L94
.L43:
	cmpb	$0, 8(%rbx)
	jne	.L44
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L44
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L44
	xorl	%ecx, %ecx
	movq	%r13, -2696(%rbp)
	movq	%r14, %r13
	movq	%r12, %r14
	movq	%rbx, %r12
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r12)
	jg	.L48
	movq	%r12, %rbx
	movq	%r14, %r12
	movq	%r13, %r14
	movq	-2696(%rbp), %r13
.L44:
	movq	-2640(%rbp), %rax
	movl	$25456, %esi
	movq	(%rbx), %rdi
	movl	$1, %edx
	movb	$34, -2617(%rbp)
	movq	%rax, -2544(%rbp)
	movw	%si, (%rax)
	movq	%r14, %rsi
	movq	$2, -2536(%rbp)
	movb	$0, -2526(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2648(%rbp), %rsi
	movq	-2632(%rbp), %rdi
	movq	%rax, -2696(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-2696(%rbp), %r8
	movq	-2504(%rbp), %rdx
	movq	-2512(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -2617(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2512(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	-2544(%rbp), %rdi
	cmpq	-2640(%rbp), %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$58, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L50
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L50:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2632(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -2696(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-2696(%rbp), %r8
	movq	-2504(%rbp), %rdx
	movq	-2512(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -2617(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2512(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L51
	call	_ZdlPv@PLT
.L51:
	movq	-2576(%rbp), %rdi
	movl	$1, 16(%rbx)
	cmpq	%r12, %rdi
	je	.L52
	call	_ZdlPv@PLT
.L52:
	movq	-2616(%rbp), %rsi
	leaq	_ZN4node28NativeSymbolDebuggingContext12LookupSymbolEPv(%rip), %rcx
	movq	(%rsi), %rax
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L53
	leaq	-2464(%rbp), %rax
	movq	%r15, -2512(%rbp)
	pxor	%xmm3, %xmm3
	movq	$0, -2504(%rbp)
	movb	$0, -2496(%rbp)
	movq	%rax, -2664(%rbp)
	movq	%rax, -2480(%rbp)
	movq	$0, -2472(%rbp)
	movb	$0, -2464(%rbp)
	movaps	%xmm3, -2448(%rbp)
.L54:
	leaq	-2608(%rbp), %rax
	movq	-2632(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -2696(%rbp)
	call	_ZNK4node28NativeSymbolDebuggingContext10SymbolInfo7DisplayB5cxx11Ev@PLT
	cmpl	$1, 16(%rbx)
	je	.L95
.L55:
	cmpb	$0, 8(%rbx)
	jne	.L56
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L56
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L56
	xorl	%ecx, %ecx
	movq	%r13, -2704(%rbp)
	movq	%r14, %r13
	movq	%r12, %r14
	movq	%rbx, %r12
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L60:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r12)
	jg	.L60
	movq	%r12, %rbx
	movq	%r14, %r12
	movq	%r13, %r14
	movq	-2704(%rbp), %r13
.L56:
	movl	$27759, %edx
	movq	%r12, -2576(%rbp)
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	movw	%dx, 4(%r12)
	movl	$1, %edx
	movl	$1651341683, (%r12)
	movb	$34, -2617(%rbp)
	movq	$6, -2568(%rbp)
	movb	$0, -2554(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2648(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -2704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-2704(%rbp), %r8
	movq	-2536(%rbp), %rdx
	movq	-2544(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -2617(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2544(%rbp), %rdi
	cmpq	-2640(%rbp), %rdi
	je	.L59
	call	_ZdlPv@PLT
.L59:
	movq	-2576(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L61
	call	_ZdlPv@PLT
.L61:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$58, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L62
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L62:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2696(%rbp), %rsi
	movq	-2648(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-2536(%rbp), %rdx
	movq	-2544(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$34, -2617(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2544(%rbp), %rdi
	cmpq	-2640(%rbp), %rdi
	je	.L63
	call	_ZdlPv@PLT
.L63:
	movq	-2608(%rbp), %rdi
	leaq	-2592(%rbp), %rax
	movl	$1, 16(%rbx)
	cmpq	%rax, %rdi
	je	.L64
	call	_ZdlPv@PLT
.L64:
	movq	-2480(%rbp), %rdi
	cmpq	-2664(%rbp), %rdi
	je	.L65
	call	_ZdlPv@PLT
.L65:
	movq	-2512(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L66
	call	_ZdlPv@PLT
.L66:
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L96
	subl	$2, 12(%rbx)
.L68:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$125, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%rbx)
	movq	-2656(%rbp), %rcx
	cmpq	%rcx, -2736(%rbp)
	je	.L71
	movq	(%rcx), %rax
	movq	%rcx, %r13
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	addq	$8, %r13
	movb	$44, -2617(%rbp)
	movq	%rax, -2664(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, -2656(%rbp)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 12(%rbx)
	jne	.L68
	testl	%eax, %eax
	jle	.L68
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L70:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -2617(%rbp)
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L70
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L40:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L93:
	movq	240(%r12), %r13
	testq	%r13, %r13
	je	.L97
	cmpb	$0, 56(%r13)
	jne	.L37
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L38
.L89:
	movq	-2496(%rbp), %rax
.L37:
	movb	$1, 225(%r12)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$44, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$44, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L53:
	movq	-2664(%rbp), %rdx
	movq	-2632(%rbp), %rdi
	call	*%rax
	leaq	-2464(%rbp), %rax
	movq	%rax, -2664(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	-2416(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L71:
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L98
	subl	$2, 12(%rbx)
.L73:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$93, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-2616(%rbp), %rdi
	movl	$1, 16(%rbx)
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	leaq	_ZN4node28NativeSymbolDebuggingContextD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L77
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$2696, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L22
	movl	12(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L22
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	addl	$1, %r13d
	movb	$32, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%rbx)
	jg	.L26
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 12(%rbx)
	jne	.L73
	testl	%eax, %eax
	jle	.L73
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -2617(%rbp)
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L75
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$32, %esi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$44, -2617(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	-2112(%rbp), %rsi
	movl	$256, %edx
	call	*%rax
	movl	%eax, %r12d
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L77:
	call	*%rax
	jmp	.L19
.L97:
	call	_ZSt16__throw_bad_castv@PLT
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7785:
	.size	_ZN6reportL16PrintNativeStackEPN4node10JSONWriterE, .-_ZN6reportL16PrintNativeStackEPN4node10JSONWriterE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.text
	.p2align 4
	.type	_ZN6reportL22PrintSystemInformationEPN4node10JSONWriterE, @function
_ZN6reportL22PrintSystemInformationEPN4node10JSONWriterE:
.LFB7791:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-288(%rbp), %r12
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%r15)
	je	.L262
.L101:
	cmpb	$0, 8(%r15)
	je	.L263
.L102:
	leaq	-128(%rbp), %rbx
	leaq	-112(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rbx, -352(%rbp)
	movq	%rax, -328(%rbp)
	movq	%rax, -128(%rbp)
	movq	$20, -288(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-288(%rbp), %rdx
	movdqa	.LC5(%rip), %xmm0
	movq	%r12, %rsi
	movq	%rax, -128(%rbp)
	movq	%rdx, -112(%rbp)
	movups	%xmm0, (%rax)
	movl	$1936026722, 16(%rax)
	movq	-288(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	(%r15), %rdi
	movl	$1, %edx
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%rbx, %rsi
	movq	%rax, %r13
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -336(%rbp)
	cmpq	%rax, %rdi
	je	.L105
	call	_ZdlPv@PLT
.L105:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L107
	call	_ZdlPv@PLT
.L107:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L108
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L108:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r15)
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movl	$0, 16(%r15)
	call	uv_mutex_lock@PLT
	leaq	-312(%rbp), %rdi
	leaq	-316(%rbp), %rsi
	call	uv_os_environ@PLT
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	movl	%eax, %ebx
	call	uv_mutex_unlock@PLT
	testl	%ebx, %ebx
	jne	.L109
	movl	-316(%rbp), %esi
	testl	%esi, %esi
	jle	.L110
	leaq	-176(%rbp), %rcx
	movl	16(%r15), %eax
	movq	-344(%rbp), %r14
	xorl	%r13d, %r13d
	movq	%rcx, -368(%rbp)
	leaq	-192(%rbp), %rcx
	movq	%rcx, -384(%rbp)
	leaq	-160(%rbp), %rcx
	movq	%rcx, -392(%rbp)
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r13, %rbx
	movq	(%r15), %rdi
	salq	$4, %rbx
	addq	-312(%rbp), %rbx
	movq	%rbx, -360(%rbp)
	cmpl	$1, %eax
	je	.L264
.L111:
	cmpb	$0, 8(%r15)
	jne	.L112
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L112
	movl	12(%r15), %eax
	testl	%eax, %eax
	jle	.L112
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L113
.L112:
	movq	-360(%rbp), %rax
	movq	(%rax), %r8
	movq	-368(%rbp), %rax
	movq	%rax, -192(%rbp)
	testq	%r8, %r8
	je	.L228
	movq	%r8, %rdi
	movq	%r8, -376(%rbp)
	call	strlen@PLT
	movq	-376(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -288(%rbp)
	movq	%rax, %r9
	ja	.L265
	cmpq	$1, %rax
	jne	.L115
	movzbl	(%r8), %edx
	movb	%dl, -176(%rbp)
	movq	-368(%rbp), %rdx
.L116:
	movq	%rax, -184(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%r15), %rdi
	movl	$1, %edx
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -376(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-376(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L117
	call	_ZdlPv@PLT
.L117:
	movq	-192(%rbp), %rdi
	cmpq	-368(%rbp), %rdi
	je	.L118
	call	_ZdlPv@PLT
.L118:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L119
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L119:
	movq	-360(%rbp), %rax
	leaq	-144(%rbp), %rbx
	movq	%rbx, -160(%rbp)
	movq	8(%rax), %r8
	testq	%r8, %r8
	je	.L228
	movq	%r8, %rdi
	movq	%r8, -360(%rbp)
	call	strlen@PLT
	movq	-360(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -288(%rbp)
	movq	%rax, %r9
	ja	.L266
	cmpq	$1, %rax
	jne	.L121
	movzbl	(%r8), %edx
	movb	%dl, -144(%rbp)
	movq	%rbx, %rdx
.L122:
	movq	%rax, -152(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%r15), %rdi
	movl	$1, %edx
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-392(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, -360(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-360(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L123
	call	_ZdlPv@PLT
.L123:
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L124
	call	_ZdlPv@PLT
.L124:
	movl	-316(%rbp), %esi
	addq	$1, %r13
	movl	$1, 16(%r15)
	cmpl	%r13d, %esi
	jle	.L110
	movl	$1, %eax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L110:
	movq	-312(%rbp), %rdi
	call	uv_os_free_environ@PLT
.L109:
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	je	.L267
	subl	$2, 12(%r15)
.L129:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r15), %r14d
	testl	%r14d, %r14d
	je	.L268
.L132:
	movl	$1, 16(%r15)
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	je	.L269
.L133:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, -102(%rbp)
	movq	-328(%rbp), %rax
	movl	$29556, %r13d
	movq	$10, -120(%rbp)
	movw	%r13w, -104(%rbp)
	movq	%rax, -128(%rbp)
	movabsq	$7596843923501577077, %rax
	movq	%rax, -112(%rbp)
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L138
	call	_ZdlPv@PLT
.L138:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L139
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L139:
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %r14
	movb	$123, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-240(%rbp), %rax
	addl	$2, 12(%r15)
	leaq	_ZZN6reportL22PrintSystemInformationEPN4node10JSONWriterEE14rlimit_strings(%rip), %r13
	movq	%rax, -384(%rbp)
	movq	%rax, -256(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, -392(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-304(%rbp), %rax
	movl	$0, 16(%r15)
	movq	$0, -248(%rbp)
	movb	$0, -240(%rbp)
	movq	$0, -216(%rbp)
	movb	$0, -208(%rbp)
	movq	%rax, -368(%rbp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L140:
	addq	$16, %r13
	leaq	160+_ZZN6reportL22PrintSystemInformationEPN4node10JSONWriterEE14rlimit_strings(%rip), %rax
	cmpq	%rax, %r13
	je	.L270
.L190:
	movl	8(%r13), %edi
	movq	-368(%rbp), %rsi
	call	getrlimit64@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	jne	.L140
	movq	0(%r13), %rax
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	movq	%rax, -360(%rbp)
	je	.L271
.L141:
	cmpb	$0, 8(%r14)
	jne	.L142
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L142
	movl	12(%r14), %r11d
	testl	%r11d, %r11d
	jle	.L142
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L143
.L142:
	movq	-328(%rbp), %rax
	cmpq	$0, -360(%rbp)
	movq	%rax, -128(%rbp)
	je	.L228
	movq	-360(%rbp), %rdi
	call	strlen@PLT
	movq	%rax, -288(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L272
	cmpq	$1, %rax
	jne	.L145
	movq	-360(%rbp), %rcx
	movzbl	(%rcx), %edx
	movb	%dl, -112(%rbp)
	movq	-328(%rbp), %rdx
.L146:
	movq	%rax, -120(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%r14), %rdi
	movl	$1, %edx
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L149
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L149:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r14)
	cmpq	$-1, -304(%rbp)
	movl	$0, 16(%r14)
	je	.L273
	cmpb	$0, 8(%r14)
	jne	.L161
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L161
	movl	12(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L161
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L165:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L165
.L161:
	movq	-328(%rbp), %rax
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, -128(%rbp)
	movl	$1952870259, (%rax)
	movq	$4, -120(%rbp)
	movb	$0, -108(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L164
	call	_ZdlPv@PLT
.L164:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L167
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L167:
	movq	-304(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
.L261:
	cmpq	$-1, -296(%rbp)
	movq	(%r14), %rdi
	movl	$1, 16(%r14)
	je	.L274
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L178
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L259
	movl	12(%r14), %edi
	xorl	%r15d, %r15d
	testl	%edi, %edi
	jle	.L259
	.p2align 4,,10
	.p2align 3
.L182:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L182
.L259:
	movq	(%r14), %rdi
.L178:
	movq	-328(%rbp), %rax
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, -128(%rbp)
	movl	$1685217640, (%rax)
	movq	$4, -120(%rbp)
	movb	$0, -108(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L181
	call	_ZdlPv@PLT
.L181:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L183
	call	_ZdlPv@PLT
.L183:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L184
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L184:
	movq	-296(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
.L260:
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	movl	$1, 16(%r14)
	je	.L275
	subl	$2, 12(%r14)
.L186:
	movq	%r12, %rsi
	movl	$1, %edx
	movb	$125, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %esi
	testl	%esi, %esi
	je	.L276
.L189:
	addq	$16, %r13
	leaq	160+_ZZN6reportL22PrintSystemInformationEPN4node10JSONWriterEE14rlimit_strings(%rip), %rax
	movl	$1, 16(%r14)
	cmpq	%rax, %r13
	jne	.L190
.L270:
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	movq	%r14, %r15
	je	.L277
	subl	$2, 12(%r14)
.L192:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r15), %ecx
	testl	%ecx, %ecx
	je	.L278
.L195:
	movl	$1, 16(%r15)
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	je	.L279
.L196:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$115, -100(%rbp)
	movq	-328(%rbp), %rax
	movl	$1952671082, -104(%rbp)
	movq	$13, -120(%rbp)
	movq	%rax, -128(%rbp)
	movabsq	$7083991125750540403, %rax
	movq	%rax, -112(%rbp)
	movb	$0, -99(%rbp)
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L199
	call	_ZdlPv@PLT
.L199:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L202
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L202:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$91, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r15)
	movq	%r12, %rdi
	movl	$0, 16(%r15)
	call	_ZN4node28NativeSymbolDebuggingContext18GetLoadedLibrariesB5cxx11Ev@PLT
	movq	-280(%rbp), %rax
	movq	-288(%rbp), %r13
	movq	%rax, -328(%rbp)
	cmpq	%rax, %r13
	je	.L280
	cmpl	$1, 16(%r15)
	movq	(%r15), %rdi
	movq	%r13, %rbx
	leaq	-317(%rbp), %r12
	je	.L213
.L206:
	cmpb	$0, 8(%r15)
	jne	.L207
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -317(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L207
	movl	12(%r15), %eax
	testl	%eax, %eax
	jle	.L207
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L209:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -317(%rbp)
	addl	$1, %r14d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r15)
	movq	(%r15), %rdi
	jg	.L209
.L207:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -317(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-344(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -317(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L210
	call	_ZdlPv@PLT
	addq	$32, %rbx
	movl	$1, 16(%r15)
	cmpq	%rbx, -328(%rbp)
	je	.L212
.L211:
	movq	(%r15), %rdi
	movq	%rbx, %r13
.L213:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -317(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L275:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L186
	testl	%eax, %eax
	jle	.L186
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	movq	(%r14), %rdi
	jg	.L188
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L274:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L171
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L171
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	jle	.L171
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L172:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L172
.L171:
	movq	-328(%rbp), %rax
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, -128(%rbp)
	movl	$1685217640, (%rax)
	movq	$4, -120(%rbp)
	movb	$0, -108(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L173
	call	_ZdlPv@PLT
.L173:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L174
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L174:
	movq	-328(%rbp), %rax
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$7310584013770223221, %rcx
	movb	$34, -288(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rcx, (%rax)
	movb	$100, 8(%rax)
	movq	$9, -120(%rbp)
	movb	$0, -103(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L175
	call	_ZdlPv@PLT
.L175:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L260
	call	_ZdlPv@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L273:
	cmpb	$0, 8(%r14)
	jne	.L151
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L151
	movl	12(%r14), %r10d
	testl	%r10d, %r10d
	jle	.L151
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L155:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L155
.L151:
	movq	-328(%rbp), %rax
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, -128(%rbp)
	movl	$1952870259, (%rax)
	movq	$4, -120(%rbp)
	movb	$0, -108(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L154
	call	_ZdlPv@PLT
.L154:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L156
	call	_ZdlPv@PLT
.L156:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L157
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L157:
	movq	-328(%rbp), %rax
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$7310584013770223221, %rcx
	movb	$34, -288(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rcx, (%rax)
	movb	$100, 8(%rax)
	movq	$9, -120(%rbp)
	movb	$0, -103(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-352(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-336(%rbp), %rdi
	je	.L158
	call	_ZdlPv@PLT
.L158:
	movq	-128(%rbp), %rdi
	cmpq	-328(%rbp), %rdi
	je	.L261
	call	_ZdlPv@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L145:
	testq	%r8, %r8
	jne	.L281
	movq	-328(%rbp), %rdx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, -376(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-376(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-288(%rbp), %rax
	movq	%rax, -112(%rbp)
.L144:
	movq	-360(%rbp), %rsi
	movq	%r8, %rdx
	call	memcpy@PLT
	movq	-288(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L276:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$1, 16(%r15)
	addq	$32, %rbx
	cmpq	%rbx, -328(%rbp)
	jne	.L211
.L212:
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	je	.L282
	subl	$2, 12(%r15)
.L214:
	movq	%r12, %rsi
	movl	$1, %edx
	movb	$93, -317(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-280(%rbp), %rbx
	movq	-288(%rbp), %r12
	movl	$1, 16(%r15)
	cmpq	%r12, %rbx
	je	.L217
	.p2align 4,,10
	.p2align 3
.L221:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L218
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L221
.L219:
	movq	-288(%rbp), %r12
.L217:
	testq	%r12, %r12
	je	.L222
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L222:
	movq	-224(%rbp), %rdi
	cmpq	-392(%rbp), %rdi
	je	.L223
	call	_ZdlPv@PLT
.L223:
	movq	-256(%rbp), %rdi
	cmpq	-384(%rbp), %rdi
	je	.L100
	call	_ZdlPv@PLT
.L100:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L283
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L221
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L271:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L102
	movl	12(%r15), %eax
	testl	%eax, %eax
	jle	.L102
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L106:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L106
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r15), %eax
	movq	(%r15), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 12(%r15)
	jne	.L129
	testl	%eax, %eax
	jle	.L129
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L131:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	movq	(%r15), %rdi
	jg	.L131
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L279:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L196
	movl	12(%r15), %edx
	testl	%edx, %edx
	jle	.L196
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L200:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L200
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L277:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L192
	testl	%eax, %eax
	jle	.L192
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -288(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	movq	(%r15), %rdi
	jg	.L194
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L282:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -317(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r15), %eax
	movq	(%r15), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 12(%r15)
	jne	.L214
	testl	%eax, %eax
	jle	.L214
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -317(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	movq	(%r15), %rdi
	jg	.L216
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L269:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L133
	movl	12(%r15), %ebx
	testl	%ebx, %ebx
	jle	.L133
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L137:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L137
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L121:
	testq	%r9, %r9
	jne	.L284
	movq	%rbx, %rdx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L115:
	testq	%r9, %r9
	jne	.L285
	movq	-368(%rbp), %rdx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L265:
	movq	-384(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -400(%rbp)
	movq	%r8, -376(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-376(%rbp), %r8
	movq	-400(%rbp), %r9
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-288(%rbp), %rax
	movq	%rax, -176(%rbp)
.L114:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-288(%rbp), %rax
	movq	-192(%rbp), %rdx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-392(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -376(%rbp)
	movq	%r8, -360(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-360(%rbp), %r8
	movq	-376(%rbp), %r9
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-288(%rbp), %rax
	movq	%rax, -144(%rbp)
.L120:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-288(%rbp), %rax
	movq	-160(%rbp), %rdx
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L278:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L262:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
	jmp	.L101
.L280:
	leaq	-317(%rbp), %r12
	jmp	.L212
.L285:
	movq	-368(%rbp), %rdi
	jmp	.L114
.L281:
	movq	-328(%rbp), %rdi
	jmp	.L144
.L283:
	call	__stack_chk_fail@PLT
.L284:
	movq	%rbx, %rdi
	jmp	.L120
	.cfi_endproc
.LFE7791:
	.size	_ZN6reportL22PrintSystemInformationEPN4node10JSONWriterE, .-_ZN6reportL22PrintSystemInformationEPN4node10JSONWriterE
	.p2align 4
	.type	_ZN6reportL17PrintGCStatisticsEPN4node10JSONWriterEPN2v87IsolateE, @function
_ZN6reportL17PrintGCStatisticsEPN4node10JSONWriterEPN2v87IsolateE:
.LFB7789:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-224(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$296, %rsp
	movq	%rsi, -328(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v814HeapStatisticsC1Ev@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	-280(%rbp), %r12
	call	_ZN2v87Isolate17GetHeapStatisticsEPNS_14HeapStatisticsE@PLT
	leaq	-272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZN2v819HeapSpaceStatisticsC1Ev@PLT
	cmpl	$1, 16(%r14)
	je	.L409
.L287:
	cmpb	$0, 8(%r14)
	je	.L410
.L288:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	leaq	-112(%rbp), %rbx
	movl	$28769, %r15d
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r13
	movabsq	$7598244868534985066, %rax
	movw	%r15w, -100(%rbp)
	movq	%rax, -112(%rbp)
	movl	$1699247216, -104(%rbp)
	movq	$14, -120(%rbp)
	movb	$0, -98(%rbp)
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%rax, %r15
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -296(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -304(%rbp)
	cmpq	%rax, %rdi
	je	.L291
	call	_ZdlPv@PLT
.L291:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L294
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L294:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rax
	addl	$2, 12(%r14)
	cmpb	$0, 8(%r14)
	movl	$0, 16(%r14)
	movq	%rax, -312(%rbp)
	je	.L411
.L295:
	movq	(%r14), %rdi
	movl	$29295, %r11d
	movq	%r12, %rsi
	movq	%rbx, -128(%rbp)
	movl	$1, %edx
	movw	%r11w, -104(%rbp)
	movabsq	$7882791850668748660, %rax
	movq	%rax, -112(%rbp)
	movb	$121, -102(%rbp)
	movq	$11, -120(%rbp)
	movb	$0, -101(%rbp)
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L298
	call	_ZdlPv@PLT
.L298:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L300
	call	_ZdlPv@PLT
.L300:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L301
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L301:
	movq	-312(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movq	-208(%rbp), %rax
	movb	$44, -280(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L412
.L302:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rbx, -128(%rbp)
	movq	$20, -280(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-280(%rbp), %rdx
	movdqa	.LC6(%rip), %xmm0
	movq	%r12, %rsi
	movq	%rax, -128(%rbp)
	movq	%rdx, -112(%rbp)
	movups	%xmm0, (%rax)
	movl	$2037542765, 16(%rax)
	movq	-280(%rbp), %rax
	movq	-128(%rbp), %rdx
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	(%r14), %rdi
	movl	$1, %edx
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L305
	call	_ZdlPv@PLT
.L305:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L307
	call	_ZdlPv@PLT
.L307:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L308
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L308:
	movq	-312(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movq	-192(%rbp), %rax
	movb	$44, -280(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L413
.L309:
	movq	(%r14), %rdi
	movl	$31090, %r8d
	movq	%r12, %rsi
	movq	%rbx, -128(%rbp)
	movl	$1, %edx
	movw	%r8w, -104(%rbp)
	movabsq	$8029185093742719861, %rax
	movq	%rax, -112(%rbp)
	movq	$10, -120(%rbp)
	movb	$0, -102(%rbp)
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L312
	call	_ZdlPv@PLT
.L312:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L314
	call	_ZdlPv@PLT
.L314:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L315
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L315:
	movq	-312(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movq	-200(%rbp), %rax
	movb	$44, -280(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L414
.L316:
	movq	(%r14), %rdi
	movl	$29295, %esi
	movl	$1, %edx
	movabsq	$7809911822066218593, %rax
	movw	%si, -100(%rbp)
	movq	%r12, %rsi
	movq	%rbx, -128(%rbp)
	movq	%rax, -112(%rbp)
	movl	$1835355493, -104(%rbp)
	movb	$121, -98(%rbp)
	movq	$15, -120(%rbp)
	movb	$0, -97(%rbp)
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L321
	call	_ZdlPv@PLT
.L321:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L322
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L322:
	movq	-312(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movq	-184(%rbp), %rax
	movb	$44, -280(%rbp)
	movq	%rax, -312(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L415
.L323:
	movq	(%r14), %rdi
	movl	$26989, %edx
	movq	%r12, %rsi
	movq	%rbx, -128(%rbp)
	movabsq	$7587573004615116141, %rax
	movw	%dx, -104(%rbp)
	movl	$1, %edx
	movq	%rax, -112(%rbp)
	movb	$116, -102(%rbp)
	movq	$11, -120(%rbp)
	movb	$0, -101(%rbp)
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L326
	call	_ZdlPv@PLT
.L326:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L328
	call	_ZdlPv@PLT
.L328:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L329
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L329:
	movq	-312(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movb	$44, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L416
.L330:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, -128(%rbp)
	movabsq	$7161128386165826920, %rax
	movq	$10, -120(%rbp)
	movq	%rax, -112(%rbp)
	movl	$29541, %eax
	movw	%ax, -104(%rbp)
	movb	$0, -102(%rbp)
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L335
	call	_ZdlPv@PLT
.L335:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L336
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L336:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -280(%rbp)
	movq	%r14, %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r14)
	movl	$0, 16(%r14)
	movq	$0, -312(%rbp)
	.p2align 4,,10
	.p2align 3
.L387:
	movq	-328(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v87Isolate18NumberOfHeapSpacesEv@PLT
	movq	-312(%rbp), %rdx
	cmpq	%rdx, %rax
	jbe	.L337
	movq	-336(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v87Isolate22GetHeapSpaceStatisticsEPNS_19HeapSpaceStatisticsEm@PLT
	cmpl	$1, 16(%r15)
	movq	-272(%rbp), %r14
	je	.L417
.L338:
	cmpb	$0, 8(%r15)
	jne	.L339
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L339
	movl	12(%r15), %eax
	testl	%eax, %eax
	jle	.L339
	xorl	%r8d, %r8d
	movq	%rbx, -320(%rbp)
	movl	%r8d, %ebx
	.p2align 4,,10
	.p2align 3
.L340:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L340
	movq	-320(%rbp), %rbx
.L339:
	movq	%rbx, -128(%rbp)
	testq	%r14, %r14
	jne	.L418
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -280(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L419
	cmpq	$1, %rax
	jne	.L342
	movzbl	(%r14), %edx
	movb	%dl, -112(%rbp)
	movq	%rbx, %rdx
.L343:
	movq	%rax, -120(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%r15), %rdi
	movl	$1, %edx
	movb	$34, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L344
	call	_ZdlPv@PLT
.L344:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L345
	call	_ZdlPv@PLT
.L345:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L346
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L346:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r15)
	cmpb	$0, 8(%r15)
	movl	$0, 16(%r15)
	movq	-264(%rbp), %r14
	jne	.L347
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L347
	movl	12(%r15), %r11d
	testl	%r11d, %r11d
	jle	.L347
	xorl	%r8d, %r8d
	movq	%rbx, -320(%rbp)
	movl	%r8d, %ebx
	.p2align 4,,10
	.p2align 3
.L351:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L351
	movq	-320(%rbp), %rbx
.L347:
	movq	%rbx, -128(%rbp)
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$7589543329452090733, %rax
	movb	$34, -280(%rbp)
	movq	%rax, (%rbx)
	movl	$25978, %eax
	movw	%ax, 8(%rbx)
	movq	$10, -120(%rbp)
	movb	$0, -102(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -320(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-320(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L350
	call	_ZdlPv@PLT
.L350:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L352
	call	_ZdlPv@PLT
.L352:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L353
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L353:
	movq	%r14, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r15)
	movq	-240(%rbp), %r14
	movb	$44, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L354
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L354
	movl	12(%r15), %r9d
	testl	%r9d, %r9d
	jle	.L354
	xorl	%r8d, %r8d
	movq	%rbx, -320(%rbp)
	movl	%r8d, %ebx
	.p2align 4,,10
	.p2align 3
.L358:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L358
	movq	-320(%rbp), %rbx
.L354:
	movl	$29295, %r10d
	movq	%rbx, -128(%rbp)
	movq	(%r15), %rdi
	movq	%r12, %rsi
	movabsq	$7310596091285434211, %rax
	movw	%r10w, 12(%rbx)
	movl	$1, %edx
	movq	%rax, (%rbx)
	movl	$1835355492, 8(%rbx)
	movb	$121, 14(%rbx)
	movb	$34, -280(%rbp)
	movq	$15, -120(%rbp)
	movb	$0, -97(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -320(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-320(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L357
	call	_ZdlPv@PLT
.L357:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L359
	call	_ZdlPv@PLT
.L359:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L360
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L360:
	movq	%r14, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r15)
	movq	-248(%rbp), %r14
	movb	$44, -280(%rbp)
	addq	-256(%rbp), %r14
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L361
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L361
	movl	12(%r15), %r8d
	testl	%r8d, %r8d
	jle	.L361
	xorl	%r8d, %r8d
	movq	%rbx, -320(%rbp)
	movl	%r8d, %ebx
	.p2align 4,,10
	.p2align 3
.L365:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L365
	movq	-320(%rbp), %rbx
.L361:
	movq	%rbx, -128(%rbp)
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$8751735851445150051, %rax
	movb	$34, -280(%rbp)
	movq	%rax, (%rbx)
	movq	$8, -120(%rbp)
	movb	$0, -104(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -320(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-320(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L364
	call	_ZdlPv@PLT
.L364:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L366
	call	_ZdlPv@PLT
.L366:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L367
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L367:
	movq	%r14, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r15)
	movq	-256(%rbp), %r14
	movb	$44, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L368
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L368
	movl	12(%r15), %edi
	testl	%edi, %edi
	jle	.L368
	xorl	%r8d, %r8d
	movq	%rbx, -320(%rbp)
	movl	%r8d, %ebx
	.p2align 4,,10
	.p2align 3
.L372:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L372
	movq	-320(%rbp), %rbx
.L368:
	movq	%rbx, -128(%rbp)
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1684370293, (%rbx)
	movb	$34, -280(%rbp)
	movq	$4, -120(%rbp)
	movb	$0, -108(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -320(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-320(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L371
	call	_ZdlPv@PLT
.L371:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L373
	call	_ZdlPv@PLT
.L373:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L374
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L374:
	movq	%r14, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r15)
	movq	-248(%rbp), %r14
	movb	$44, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L375
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	jne	.L375
	movl	12(%r15), %esi
	testl	%esi, %esi
	jle	.L375
	xorl	%r8d, %r8d
	movq	%rbx, -320(%rbp)
	movl	%r8d, %ebx
	.p2align 4,,10
	.p2align 3
.L379:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r15)
	jg	.L379
	movq	-320(%rbp), %rbx
.L375:
	movq	%rbx, -128(%rbp)
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$7809911822066218593, %rax
	movb	$101, 8(%rbx)
	movq	%rax, (%rbx)
	movb	$34, -280(%rbp)
	movq	$9, -120(%rbp)
	movb	$0, -103(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -320(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-320(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -280(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L378
	call	_ZdlPv@PLT
.L378:
	movq	-128(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L380
	call	_ZdlPv@PLT
.L380:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	jne	.L381
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r15), %rdi
.L381:
	movq	%r14, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	cmpb	$0, 8(%r15)
	movl	$1, 16(%r15)
	movq	(%r15), %rdi
	je	.L420
	subl	$2, 12(%r15)
.L383:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r15), %ecx
	testl	%ecx, %ecx
	je	.L421
.L386:
	addq	$1, -312(%rbp)
	movl	$1, 16(%r15)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L420:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r15), %eax
	movq	(%r15), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 12(%r15)
	jne	.L383
	testl	%eax, %eax
	jle	.L383
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L385:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	addl	$1, %r14d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r15)
	movq	(%r15), %rdi
	jg	.L385
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L342:
	testq	%r8, %r8
	jne	.L422
	movq	%rbx, %rdx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L421:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-280(%rbp), %rax
	movq	%rax, -112(%rbp)
.L341:
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-280(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L417:
	movq	(%r15), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L337:
	cmpb	$0, 8(%r15)
	movq	(%r15), %rdi
	movq	%r15, %r14
	je	.L423
	subl	$2, 12(%r15)
.L389:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %edx
	testl	%edx, %edx
	je	.L424
.L392:
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	movl	$1, 16(%r14)
	je	.L425
	subl	$2, 12(%r14)
.L394:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	testl	%eax, %eax
	je	.L426
.L397:
	movl	$1, 16(%r14)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L427
	addq	$296, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	.cfi_restore_state
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L330
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L330
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L334:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L334
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L415:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L323
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L323
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L327:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L327
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L414:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L316
	movl	12(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L316
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L320:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L320
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L413:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L309
	movl	12(%r14), %edi
	testl	%edi, %edi
	jle	.L309
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L313:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L313
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L412:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L302
	movl	12(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L302
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L306:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L306
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L411:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L295
	movl	12(%r14), %r10d
	testl	%r10d, %r10d
	jle	.L295
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L299:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L299
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L410:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L288
	movl	12(%r14), %ebx
	testl	%ebx, %ebx
	jle	.L288
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L292:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L292
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L425:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L394
	testl	%eax, %eax
	jle	.L394
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L396:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	movq	(%r14), %rdi
	jg	.L396
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r15), %eax
	movq	(%r15), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r15)
	movl	%eax, 12(%r15)
	jne	.L389
	testl	%eax, %eax
	jle	.L389
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L391:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -280(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	movq	(%r14), %rdi
	jg	.L391
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L426:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L409:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -280(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L287
.L427:
	call	__stack_chk_fail@PLT
.L422:
	movq	%rbx, %rdi
	jmp	.L341
	.cfi_endproc
.LFE7789:
	.size	_ZN6reportL17PrintGCStatisticsEPN4node10JSONWriterEPN2v87IsolateE, .-_ZN6reportL17PrintGCStatisticsEPN4node10JSONWriterEPN2v87IsolateE
	.section	.text._ZN4node10JSONWriter12write_stringEPKc,"axG",@progbits,_ZN4node10JSONWriter12write_stringEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10JSONWriter12write_stringEPKc
	.type	_ZN4node10JSONWriter12write_stringEPKc, @function
_ZN4node10JSONWriter12write_stringEPKc:
.LFB7312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-112(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -152(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, -128(%rbp)
	testq	%rsi, %rsi
	je	.L439
	movq	%rsi, %rdi
	movq	%rsi, %r15
	leaq	-128(%rbp), %r14
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L440
	cmpq	$1, %rax
	jne	.L432
	movzbl	(%r15), %edx
	leaq	-136(%rbp), %rbx
	movb	%dl, -112(%rbp)
	movq	%r13, %rdx
.L433:
	movq	%rax, -120(%rbp)
	movq	%rbx, %rsi
	movb	$0, (%rdx,%rax)
	movq	-152(%rbp), %rax
	movl	$1, %edx
	movb	$34, -136(%rbp)
	movq	(%rax), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%rbx, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L434
	call	_ZdlPv@PLT
.L434:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L428
	call	_ZdlPv@PLT
.L428:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L441
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L442
	movq	%r13, %rdx
	leaq	-136(%rbp), %rbx
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L440:
	leaq	-136(%rbp), %rbx
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L431:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L439:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L441:
	call	__stack_chk_fail@PLT
.L442:
	movq	%r13, %rdi
	leaq	-136(%rbp), %rbx
	jmp	.L431
	.cfi_endproc
.LFE7312:
	.size	_ZN4node10JSONWriter12write_stringEPKc, .-_ZN4node10JSONWriter12write_stringEPKc
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB7875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L444
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L444:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L446
.L460:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L460
.L446:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L461
.L447:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r14
	movl	$32, -240(%rbp)
	movq	%rax, -232(%rbp)
	andq	$-16, %r14
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$48, -236(%rbp)
	call	*%r10
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L462
	cmpq	$1, %r13
	jne	.L450
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L451:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L463
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L450:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L451
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L449:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L461:
	orq	$0, -8(%rsp,%rcx)
	jmp	.L447
.L463:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7875:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.text._ZN4node10JSONWriter16json_objectstartIPKcEEvT_,"axG",@progbits,_ZN4node10JSONWriter16json_objectstartIPKcEEvT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	.type	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_, @function
_ZN4node10JSONWriter16json_objectstartIPKcEEvT_:
.LFB8553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rbx)
	je	.L484
.L465:
	cmpb	$0, 8(%rbx)
	je	.L485
.L466:
	leaq	-112(%rbp), %r14
	leaq	-128(%rbp), %r15
	movq	%r14, -128(%rbp)
	testq	%r13, %r13
	jne	.L486
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L486:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L487
	cmpq	$1, %rax
	jne	.L469
	movzbl	0(%r13), %edx
	leaq	-136(%rbp), %r12
	movb	%dl, -112(%rbp)
	movq	%r14, %rdx
.L470:
	movq	%rax, -120(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%rbx), %rdi
	movl	$1, %edx
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L471
	call	_ZdlPv@PLT
.L471:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L473
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L473:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%rbx)
	movl	$0, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L488
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	leaq	-136(%rbp), %r12
	movl	$1, %edx
	movb	$10, -136(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L466
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L466
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L467:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	jg	.L467
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L469:
	testq	%r8, %r8
	jne	.L489
	movq	%r14, %rdx
	leaq	-136(%rbp), %r12
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L487:
	leaq	-136(%rbp), %r12
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r8, -152(%rbp)
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L468:
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L484:
	leaq	-136(%rbp), %r12
	movl	$1, %edx
	movb	$44, -136(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L465
.L488:
	call	__stack_chk_fail@PLT
.L489:
	movq	%r14, %rdi
	leaq	-136(%rbp), %r12
	jmp	.L468
	.cfi_endproc
.LFE8553:
	.size	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_, .-_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	.section	.rodata.str1.1
.LC8:
	.string	"userCpuSeconds"
.LC9:
	.string	"kernelCpuSeconds"
.LC11:
	.string	"cpuConsumptionPercent"
.LC12:
	.string	"maxRss"
.LC13:
	.string	"pageFaults"
.LC14:
	.string	"IORequired"
.LC15:
	.string	"IONotRequired"
.LC16:
	.string	"fsActivity"
.LC17:
	.string	"reads"
.LC18:
	.string	"writes"
.LC19:
	.string	"uvthreadResourceUsage"
	.text
	.p2align 4
	.type	_ZN6reportL18PrintResourceUsageEPN4node10JSONWriterE, @function
_ZN6reportL18PrintResourceUsageEPN4node10JSONWriterE:
.LFB7790:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$408, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_hrtime@PLT
	subq	_ZN4node11per_process15node_start_timeE(%rip), %rax
	cmpq	$999999999, %rax
	jbe	.L491
	movabsq	$19342813113834067, %rdx
	shrq	$9, %rax
	mulq	%rdx
	shrq	$11, %rdx
	movq	%rdx, %r14
.L491:
	cmpl	$1, 16(%rbx)
	movq	(%rbx), %rdi
	leaq	-272(%rbp), %r12
	je	.L616
.L492:
	cmpb	$0, 8(%rbx)
	je	.L617
.L493:
	movq	(%rbx), %rdi
	leaq	-128(%rbp), %r8
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$7305808869231650162, %rax
	leaq	-112(%rbp), %r13
	movb	$101, -100(%rbp)
	movq	%r8, -440(%rbp)
	movq	%r13, -128(%rbp)
	movq	%rax, -112(%rbp)
	movl	$1734439765, -104(%rbp)
	movq	$13, -120(%rbp)
	movb	$0, -99(%rbp)
	movb	$34, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-440(%rbp), %r8
	leaq	-96(%rbp), %rdi
	movq	%rax, %r15
	movq	%r8, %rsi
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -272(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L496
	call	_ZdlPv@PLT
.L496:
	movq	-128(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L498
	call	_ZdlPv@PLT
.L498:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L499
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L499:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%rbx)
	leaq	-416(%rbp), %rdi
	movl	$0, 16(%rbx)
	call	uv_getrusage@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L618
.L500:
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L619
	subl	$2, 12(%rbx)
.L555:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %r10d
	testl	%r10d, %r10d
	je	.L620
.L558:
	movl	$1, 16(%rbx)
	movq	%r12, %rsi
	movl	$1, %edi
	call	getrusage@PLT
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L621
.L490:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L622
	addq	$408, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L619:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 12(%rbx)
	jne	.L555
	testl	%eax, %eax
	jle	.L555
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L557:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	addl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L557
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L617:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L493
	movl	12(%rbx), %edi
	testl	%edi, %edi
	jle	.L493
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L497:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%rbx)
	jg	.L497
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L618:
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	cmpl	$1, 16(%rbx)
	movq	(%rbx), %rdi
	cvtsi2sdq	-408(%rbp), %xmm1
	movsd	.LC7(%rip), %xmm2
	cvtsi2sdq	-416(%rbp), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	-392(%rbp), %xmm0
	movsd	%xmm1, -440(%rbp)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-400(%rbp), %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -448(%rbp)
	je	.L623
.L501:
	cmpb	$0, 8(%rbx)
	je	.L624
.L502:
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L625
.L504:
	movsd	-440(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%rbx)
	movb	$44, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L626
.L507:
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L627
.L509:
	movsd	-448(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	pxor	%xmm1, %xmm1
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movsd	-440(%rbp), %xmm3
	cvtsi2sdq	%r14, %xmm1
	movl	$1, %edx
	addsd	-448(%rbp), %xmm3
	movl	$1, 16(%rbx)
	movb	$44, -272(%rbp)
	movapd	%xmm3, %xmm0
	divsd	%xmm1, %xmm0
	mulsd	.LC10(%rip), %xmm0
	movsd	%xmm0, -440(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L628
.L512:
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L629
.L514:
	movsd	-440(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-384(%rbp), %rax
	movl	$1, 16(%rbx)
	movb	$44, -272(%rbp)
	salq	$10, %rax
	movq	%rax, -440(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L630
.L517:
	leaq	.LC12(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L631
.L519:
	movq	-440(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, 16(%rbx)
	leaq	.LC13(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	cmpl	$1, 16(%rbx)
	je	.L632
.L522:
	cmpb	$0, 8(%rbx)
	je	.L633
.L523:
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L634
.L525:
	movq	-344(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%rbx)
	movb	$44, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L635
.L528:
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L636
.L530:
	movq	-352(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	cmpb	$0, 8(%rbx)
	movl	$1, 16(%rbx)
	movq	(%rbx), %rdi
	je	.L637
	subl	$2, 12(%rbx)
.L534:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	testl	%eax, %eax
	je	.L638
.L537:
	movl	$1, 16(%rbx)
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	cmpl	$1, 16(%rbx)
	je	.L639
.L538:
	cmpb	$0, 8(%rbx)
	je	.L640
.L539:
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L641
.L541:
	movq	-328(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%rbx)
	movb	$44, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L642
.L544:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L643
.L546:
	movq	-320(%rbp), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	cmpb	$0, 8(%rbx)
	movl	$1, 16(%rbx)
	movq	(%rbx), %rdi
	je	.L644
	subl	$2, 12(%rbx)
.L550:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %r11d
	testl	%r11d, %r11d
	je	.L645
.L553:
	movl	$1, 16(%rbx)
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L637:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 12(%rbx)
	jne	.L534
	testl	%eax, %eax
	jle	.L534
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L536
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L633:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L523
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L523
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L527
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L635:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L528
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L528
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L532:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L532
	jmp	.L528
	.p2align 4,,10
	.p2align 3
.L624:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L502
	movl	12(%rbx), %esi
	testl	%esi, %esi
	jle	.L502
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L506:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L506
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L626:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L507
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L507
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L511:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L511
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L628:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L512
	movl	12(%rbx), %edx
	testl	%edx, %edx
	jle	.L512
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L516:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L516
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L630:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L517
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L517
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L521:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L521
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L644:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 12(%rbx)
	jne	.L550
	testl	%eax, %eax
	jle	.L550
	.p2align 4,,10
	.p2align 3
.L552:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	addl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L552
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L640:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L539
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L539
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L543:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L543
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L642:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L544
	movl	12(%rbx), %r15d
	testl	%r15d, %r15d
	jle	.L544
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L548:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L548
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L621:
	leaq	.LC19(%rip), %rsi
	movq	%rbx, %rdi
	leaq	-417(%rbp), %r12
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	pxor	%xmm1, %xmm1
	pxor	%xmm0, %xmm0
	movsd	.LC7(%rip), %xmm2
	cvtsi2sdq	-264(%rbp), %xmm1
	cmpl	$1, 16(%rbx)
	cvtsi2sdq	-272(%rbp), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	-248(%rbp), %xmm0
	movsd	%xmm1, -440(%rbp)
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-256(%rbp), %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -448(%rbp)
	je	.L646
.L560:
	cmpb	$0, 8(%rbx)
	je	.L647
.L561:
	leaq	.LC8(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L648
.L563:
	movsd	-440(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%rbx)
	movb	$44, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L649
.L566:
	leaq	.LC9(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L650
.L568:
	movsd	-448(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	pxor	%xmm1, %xmm1
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movsd	-440(%rbp), %xmm5
	cvtsi2sdq	%r14, %xmm1
	movl	$1, %edx
	addsd	-448(%rbp), %xmm5
	movl	$1, 16(%rbx)
	movb	$44, -417(%rbp)
	movapd	%xmm5, %xmm0
	divsd	%xmm1, %xmm0
	mulsd	.LC10(%rip), %xmm0
	movsd	%xmm0, -440(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L651
.L571:
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L652
.L573:
	movsd	-440(%rbp), %xmm0
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	movl	$1, 16(%rbx)
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	cmpl	$1, 16(%rbx)
	je	.L653
.L576:
	cmpb	$0, 8(%rbx)
	je	.L654
.L577:
	leaq	.LC17(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L655
.L579:
	movq	-184(%rbp), %rsi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%rbx)
	movb	$44, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	je	.L656
.L582:
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	je	.L657
.L584:
	movq	-176(%rbp), %rsi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	cmpb	$0, 8(%rbx)
	movl	$1, 16(%rbx)
	movq	(%rbx), %rdi
	je	.L658
	subl	$2, 12(%rbx)
.L588:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %edx
	testl	%edx, %edx
	je	.L659
.L591:
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	movl	$1, 16(%rbx)
	je	.L660
	subl	$2, 12(%rbx)
.L593:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	testl	%eax, %eax
	je	.L661
.L596:
	movl	$1, 16(%rbx)
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L658:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 12(%rbx)
	jne	.L588
	testl	%eax, %eax
	jle	.L588
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L590:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -417(%rbp)
	addl	$1, %r14d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L590
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L647:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L561
	movl	12(%rbx), %r9d
	testl	%r9d, %r9d
	jle	.L561
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L565:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L565
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L649:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L566
	movl	12(%rbx), %r8d
	testl	%r8d, %r8d
	jle	.L566
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L570:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%rbx)
	jg	.L570
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L651:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L571
	movl	12(%rbx), %edi
	testl	%edi, %edi
	jle	.L571
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L575:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	jg	.L575
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L654:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L577
	movl	12(%rbx), %esi
	testl	%esi, %esi
	jle	.L577
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L581:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	jg	.L581
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L656:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L582
	movl	12(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L582
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L586:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	jg	.L586
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L660:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%rbx), %eax
	movq	(%rbx), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%rbx)
	movl	%eax, 12(%rbx)
	jne	.L593
	testl	%eax, %eax
	jle	.L593
	.p2align 4,,10
	.p2align 3
.L595:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -417(%rbp)
	addl	$1, %r13d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%rbx)
	movq	(%rbx), %rdi
	jg	.L595
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L620:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L616:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L629:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L625:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L657:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L655:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L652:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L650:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L648:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L643:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L641:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L636:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L634:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L631:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L659:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L661:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L645:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L638:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L653:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L639:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L623:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L646:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -417(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L632:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -272(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L522
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7790:
	.size	_ZN6reportL18PrintResourceUsageEPN4node10JSONWriterE, .-_ZN6reportL18PrintResourceUsageEPN4node10JSONWriterE
	.section	.text._ZN4node10JSONWriter15json_arraystartIPKcEEvT_,"axG",@progbits,_ZN4node10JSONWriter15json_arraystartIPKcEEvT_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	.type	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_, @function
_ZN4node10JSONWriter15json_arraystartIPKcEEvT_:
.LFB8565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rbx)
	je	.L682
.L663:
	cmpb	$0, 8(%rbx)
	je	.L683
.L664:
	leaq	-112(%rbp), %r14
	leaq	-128(%rbp), %r15
	movq	%r14, -128(%rbp)
	testq	%r13, %r13
	jne	.L684
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L685
	cmpq	$1, %rax
	jne	.L667
	movzbl	0(%r13), %edx
	leaq	-136(%rbp), %r12
	movb	%dl, -112(%rbp)
	movq	%r14, %rdx
.L668:
	movq	%rax, -120(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%rbx), %rdi
	movl	$1, %edx
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L669
	call	_ZdlPv@PLT
.L669:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L670
	call	_ZdlPv@PLT
.L670:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L671
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L671:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$91, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%rbx)
	movl	$0, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L686
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L683:
	.cfi_restore_state
	leaq	-136(%rbp), %r12
	movl	$1, %edx
	movb	$10, -136(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L664
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L664
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L665:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	jg	.L665
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L667:
	testq	%r8, %r8
	jne	.L687
	movq	%r14, %rdx
	leaq	-136(%rbp), %r12
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	-136(%rbp), %r12
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r8, -152(%rbp)
	movq	%r12, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L666:
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L682:
	leaq	-136(%rbp), %r12
	movl	$1, %edx
	movb	$44, -136(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L663
.L686:
	call	__stack_chk_fail@PLT
.L687:
	movq	%r14, %rdi
	leaq	-136(%rbp), %r12
	jmp	.L666
	.cfi_endproc
.LFE8565:
	.size	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_, .-_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	.section	.rodata.str1.1
.LC20:
	.string	"true"
.LC21:
	.string	"false"
.LC22:
	.string	"networkInterfaces"
.LC23:
	.string	"%02x:%02x:%02x:%02x:%02x:%02x"
	.text
	.p2align 4
	.type	_ZN6reportL25PrintNetworkInterfaceInfoEPN4node10JSONWriterE, @function
_ZN6reportL25PrintNetworkInterfaceInfoEPN4node10JSONWriterE:
.LFB7779:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-276(%rbp), %rsi
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-272(%rbp), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$296, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_interface_addresses@PLT
	movl	%eax, -320(%rbp)
	testl	%eax, %eax
	je	.L939
.L688:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L940
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L939:
	.cfi_restore_state
	leaq	.LC22(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	movl	-276(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L941
	leaq	-224(%rbp), %rax
	cmpl	$1, 16(%r14)
	movl	$0, -316(%rbp)
	leaq	-264(%rbp), %r12
	movq	%rax, -296(%rbp)
	leaq	-208(%rbp), %rax
	movq	(%r14), %rdi
	leaq	-256(%rbp), %r13
	movq	%rax, -304(%rbp)
	leaq	-240(%rbp), %rbx
	movq	$0, -312(%rbp)
	je	.L866
.L693:
	cmpb	$0, 8(%r14)
	jne	.L694
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L694
	movl	12(%r14), %r11d
	testl	%r11d, %r11d
	jle	.L694
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L696:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	movq	(%r14), %rdi
	jg	.L696
.L694:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r14)
	movq	-312(%rbp), %rax
	addq	-272(%rbp), %rax
	cmpb	$0, 8(%r14)
	movl	$0, 16(%r14)
	movq	%rax, -328(%rbp)
	jne	.L697
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L697
	movl	12(%r14), %r10d
	testl	%r10d, %r10d
	jle	.L697
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L701:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L701
.L697:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, -256(%rbp)
	movl	$1701667182, -240(%rbp)
	movq	$4, -248(%rbp)
	movb	$0, -236(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L700
	call	_ZdlPv@PLT
.L700:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L702
	call	_ZdlPv@PLT
.L702:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L703
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L703:
	movq	-328(%rbp), %rax
	movq	(%rax), %r15
	movq	%rbx, -256(%rbp)
	testq	%r15, %r15
	jne	.L942
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L942:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -264(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L943
	cmpq	$1, %rax
	jne	.L705
	movzbl	(%r15), %edx
	movb	%dl, -240(%rbp)
	movq	%rbx, %rdx
.L706:
	movq	%rax, -248(%rbp)
	movq	%r12, %rsi
	movb	$0, (%rdx,%rax)
	movq	(%r14), %rdi
	movl	$1, %edx
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L707
	call	_ZdlPv@PLT
.L707:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L708
	call	_ZdlPv@PLT
.L708:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-272(%rbp), %rax
	movq	-312(%rbp), %rcx
	movl	16(%rax,%rcx), %r15d
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L709
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L709
	movl	12(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L709
	xorl	%ecx, %ecx
	movq	%rbx, -328(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L713:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L713
	movq	-328(%rbp), %rbx
.L709:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movabsq	$7809644666444607081, %rax
	movq	%rbx, -256(%rbp)
	movq	%rax, -240(%rbp)
	movq	$8, -248(%rbp)
	movb	$0, -232(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -328(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-328(%rbp), %r8
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L712
	call	_ZdlPv@PLT
.L712:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L714
	call	_ZdlPv@PLT
.L714:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L715
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L715:
	cmpl	$1, %r15d
	leaq	.LC21(%rip), %rax
	leaq	.LC20(%rip), %rsi
	sbbq	%rdx, %rdx
	notq	%rdx
	addq	$5, %rdx
	testl	%r15d, %r15d
	leaq	-192(%rbp), %r15
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-312(%rbp), %rax
	subq	$8, %rsp
	addq	-272(%rbp), %rax
	movl	$1, 16(%r14)
	movzbl	13(%rax), %edx
	movl	$18, %ecx
	movq	%r15, %rdi
	movzbl	8(%rax), %r9d
	leaq	.LC23(%rip), %r8
	movl	$18, %esi
	pushq	%rdx
	movzbl	12(%rax), %edx
	pushq	%rdx
	movzbl	11(%rax), %edx
	pushq	%rdx
	movzbl	10(%rax), %edx
	pushq	%rdx
	movzbl	9(%rax), %eax
	movl	$1, %edx
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	addq	$48, %rsp
	cmpl	$1, 16(%r14)
	je	.L944
.L871:
	cmpb	$0, 8(%r14)
	jne	.L717
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L717
	movl	12(%r14), %edi
	testl	%edi, %edi
	jle	.L717
	xorl	%ecx, %ecx
	movq	%rbx, -328(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L721:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L721
	movq	-328(%rbp), %rbx
.L717:
	movq	(%r14), %rdi
	movl	$24941, %r8d
	movl	$1, %edx
	movq	%r12, %rsi
	movw	%r8w, -240(%rbp)
	movq	%rbx, -256(%rbp)
	movb	$99, -238(%rbp)
	movq	$3, -248(%rbp)
	movb	$0, -237(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -328(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-328(%rbp), %r8
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L720
	call	_ZdlPv@PLT
.L720:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L722
	call	_ZdlPv@PLT
.L722:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L723
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L723:
	movq	%rbx, -256(%rbp)
	movq	%r15, %rcx
.L724:
	movl	(%rcx), %edx
	addq	$4, %rcx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L724
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rcx), %rdx
	cmove	%rdx, %rcx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rcx
	subq	%r15, %rcx
	movq	%rcx, -264(%rbp)
	cmpq	$15, %rcx
	ja	.L945
	cmpq	$1, %rcx
	jne	.L728
	movzbl	-192(%rbp), %eax
	movb	%al, -240(%rbp)
	movq	%rbx, %rax
.L729:
	movq	%rcx, -248(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, (%rax,%rcx)
	movq	(%r14), %rdi
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L736
	call	_ZdlPv@PLT
.L736:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L737
	call	_ZdlPv@PLT
.L737:
	movq	-312(%rbp), %rdi
	addq	-272(%rbp), %rdi
	movl	$1, 16(%r14)
	movzwl	20(%rdi), %eax
	cmpw	$2, %ax
	je	.L946
	cmpw	$10, %ax
	je	.L947
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L852
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L852
	movl	12(%r14), %edx
	testl	%edx, %edx
	jle	.L852
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L856:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L856
.L852:
	movq	(%r14), %rdi
	movl	$31084, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	movw	%cx, -236(%rbp)
	movq	%rbx, -256(%rbp)
	movl	$1768776038, -240(%rbp)
	movq	$6, -248(%rbp)
	movb	$0, -234(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L855
	call	_ZdlPv@PLT
.L855:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L857
	call	_ZdlPv@PLT
.L857:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L858
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L858:
	movl	$30575, %eax
	movq	%rbx, -256(%rbp)
	movl	$1852534389, -240(%rbp)
	movw	%ax, -236(%rbp)
	movb	$110, -234(%rbp)
	movq	$7, -248(%rbp)
	movb	$0, -233(%rbp)
.L938:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L859
	call	_ZdlPv@PLT
.L859:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L860
	call	_ZdlPv@PLT
.L860:
	movl	$1, 16(%r14)
.L791:
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	je	.L948
	subl	$2, 12(%r14)
.L862:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$1, -316(%rbp)
	movl	-316(%rbp), %eax
	addq	$80, -312(%rbp)
	movl	$1, 16(%r14)
	cmpl	%eax, -276(%rbp)
	jle	.L865
	movq	(%r14), %rdi
.L866:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L948:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L862
	testl	%eax, %eax
	jle	.L862
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L864:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	movq	(%r14), %rdi
	jg	.L864
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%rbx, %rax
	testq	%rcx, %rcx
	je	.L729
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L705:
	testq	%r8, %r8
	jne	.L949
	movq	%rbx, %rdx
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L945:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rcx, -328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-264(%rbp), %rdx
	movq	-328(%rbp), %rcx
	movq	%rax, -256(%rbp)
	movq	%rdx, -240(%rbp)
.L727:
	movl	%ecx, %edx
	cmpl	$8, %ecx
	jnb	.L730
	andl	$4, %ecx
	jne	.L950
	testl	%edx, %edx
	jne	.L951
.L731:
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rax
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L943:
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, -328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-328(%rbp), %r8
	movq	%rax, -256(%rbp)
	movq	%rax, %rdi
	movq	-264(%rbp), %rax
	movq	%rax, -240(%rbp)
.L704:
	movq	%r8, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-264(%rbp), %rax
	movq	-256(%rbp), %rdx
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L947:
	leaq	-160(%rbp), %r15
	addq	$20, %rdi
	movl	$46, %edx
	movq	%r15, %rsi
	call	uv_ip6_name@PLT
	movq	-312(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	addq	-272(%rbp), %rdi
	addq	$48, %rdi
	movl	$46, %edx
	movq	%rsi, -328(%rbp)
	call	uv_ip6_name@PLT
	cmpl	$1, 16(%r14)
	je	.L952
.L793:
	cmpb	$0, 8(%r14)
	jne	.L794
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L794
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L794
	xorl	%ecx, %ecx
	movq	%r13, -336(%rbp)
	movq	%r14, %r13
	movq	%rbx, %r14
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L798:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L798
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	-336(%rbp), %r13
.L794:
	movq	(%r14), %rdi
	movl	$1, %edx
	movl	$29541, %eax
	movq	%r12, %rsi
	movq	%rbx, -256(%rbp)
	movl	$1919181921, -240(%rbp)
	movw	%ax, -236(%rbp)
	movb	$115, -234(%rbp)
	movq	$7, -248(%rbp)
	movb	$0, -233(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -336(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-336(%rbp), %r8
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L797
	call	_ZdlPv@PLT
.L797:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L799
	call	_ZdlPv@PLT
.L799:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L800
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L800:
	movq	%rbx, -256(%rbp)
	movq	%r15, %rcx
.L801:
	movl	(%rcx), %edx
	addq	$4, %rcx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L801
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rcx), %rdx
	cmove	%rdx, %rcx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rcx
	subq	%r15, %rcx
	movq	%rcx, -264(%rbp)
	cmpq	$15, %rcx
	ja	.L953
	cmpq	$1, %rcx
	jne	.L805
	movzbl	-160(%rbp), %eax
	movb	%al, -240(%rbp)
	movq	%rbx, %rax
.L806:
	movq	%rcx, -248(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, (%rax,%rcx)
	movq	(%r14), %rdi
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L813
	call	_ZdlPv@PLT
.L813:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L814
	call	_ZdlPv@PLT
.L814:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L815
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L815
	movl	12(%r14), %r10d
	testl	%r10d, %r10d
	jle	.L815
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L819:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L819
.L815:
	movq	(%r14), %rdi
	movl	$29537, %r11d
	movl	$1, %edx
	movq	%r12, %rsi
	movw	%r11w, -236(%rbp)
	movq	%rbx, -256(%rbp)
	movl	$1836344686, -240(%rbp)
	movb	$107, -234(%rbp)
	movq	$7, -248(%rbp)
	movb	$0, -233(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L818
	call	_ZdlPv@PLT
.L818:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L820
	call	_ZdlPv@PLT
.L820:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L821
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L821:
	movq	%rbx, -256(%rbp)
	movq	-328(%rbp), %r15
.L822:
	movl	(%r15), %edx
	addq	$4, %r15
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L822
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r15), %rdx
	cmove	%rdx, %r15
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %r15
	subq	-328(%rbp), %r15
	movq	%r15, -264(%rbp)
	cmpq	$15, %r15
	ja	.L954
	cmpq	$1, %r15
	jne	.L826
	movzbl	-112(%rbp), %eax
	movb	%al, -240(%rbp)
	movq	%rbx, %rax
.L827:
	movq	%r15, -248(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, (%rax,%r15)
	movq	(%r14), %rdi
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L834
	call	_ZdlPv@PLT
.L834:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L835
	call	_ZdlPv@PLT
.L835:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L836
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L836
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	jle	.L836
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L840:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L840
.L836:
	movq	(%r14), %rdi
	movl	$31084, %r9d
	movl	$1, %edx
	movq	%r12, %rsi
	movw	%r9w, -236(%rbp)
	movq	%rbx, -256(%rbp)
	movl	$1768776038, -240(%rbp)
	movq	$6, -248(%rbp)
	movb	$0, -234(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L839
	call	_ZdlPv@PLT
.L839:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L841
	call	_ZdlPv@PLT
.L841:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L842
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L842:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, -256(%rbp)
	movl	$913723465, -240(%rbp)
	movq	$4, -248(%rbp)
	movb	$0, -236(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L843
	call	_ZdlPv@PLT
.L843:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L844
	call	_ZdlPv@PLT
.L844:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	-312(%rbp), %rax
	addq	-272(%rbp), %rax
	movb	$44, -264(%rbp)
	movq	%rax, -328(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L845
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L845
	movl	12(%r14), %esi
	testl	%esi, %esi
	jle	.L845
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L849:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L849
.L845:
	movl	$26981, %edi
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%rbx, -256(%rbp)
	movw	%di, -236(%rbp)
	movq	(%r14), %rdi
	movl	$1886348147, -240(%rbp)
	movb	$100, -234(%rbp)
	movq	$7, -248(%rbp)
	movb	$0, -233(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L848
	call	_ZdlPv@PLT
.L848:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L850
	call	_ZdlPv@PLT
.L850:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L851
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L851:
	movq	-328(%rbp), %rax
	movl	44(%rax), %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, 16(%r14)
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L946:
	leaq	-160(%rbp), %r15
	addq	$20, %rdi
	movl	$46, %edx
	movq	%r15, %rsi
	call	uv_ip4_name@PLT
	movq	-312(%rbp), %rdi
	leaq	-112(%rbp), %rsi
	addq	-272(%rbp), %rdi
	addq	$48, %rdi
	movl	$46, %edx
	movq	%rsi, -328(%rbp)
	call	uv_ip4_name@PLT
	cmpl	$1, 16(%r14)
	je	.L955
.L739:
	cmpb	$0, 8(%r14)
	jne	.L740
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L740
	movl	12(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L740
	xorl	%ecx, %ecx
	movq	%r13, -336(%rbp)
	movq	%r14, %r13
	movq	%rbx, %r14
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L744:
	movq	0(%r13), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r13)
	jg	.L744
	movq	%r14, %rbx
	movq	%r13, %r14
	movq	-336(%rbp), %r13
.L740:
	movq	(%r14), %rdi
	movl	$29541, %esi
	movl	$1, %edx
	movq	%rbx, -256(%rbp)
	movw	%si, -236(%rbp)
	movq	%r12, %rsi
	movl	$1919181921, -240(%rbp)
	movb	$115, -234(%rbp)
	movq	$7, -248(%rbp)
	movb	$0, -233(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, -336(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-336(%rbp), %r8
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L743
	call	_ZdlPv@PLT
.L743:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L745
	call	_ZdlPv@PLT
.L745:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L746
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L746:
	movq	%rbx, -256(%rbp)
	movq	%r15, %rcx
.L747:
	movl	(%rcx), %edx
	addq	$4, %rcx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L747
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rcx), %rdx
	cmove	%rdx, %rcx
	movl	%eax, %esi
	addb	%al, %sil
	sbbq	$3, %rcx
	subq	%r15, %rcx
	movq	%rcx, -264(%rbp)
	cmpq	$15, %rcx
	ja	.L956
	cmpq	$1, %rcx
	jne	.L751
	movzbl	-160(%rbp), %eax
	movb	%al, -240(%rbp)
	movq	%rbx, %rax
.L752:
	movq	%rcx, -248(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, (%rax,%rcx)
	movq	(%r14), %rdi
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L759
	call	_ZdlPv@PLT
.L759:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L760
	call	_ZdlPv@PLT
.L760:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L761
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L761
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L761
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L765:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L765
.L761:
	movq	(%r14), %rdi
	movl	$29537, %edx
	movq	%r12, %rsi
	movq	%rbx, -256(%rbp)
	movw	%dx, -236(%rbp)
	movl	$1, %edx
	movl	$1836344686, -240(%rbp)
	movb	$107, -234(%rbp)
	movq	$7, -248(%rbp)
	movb	$0, -233(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L764
	call	_ZdlPv@PLT
.L764:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L766
	call	_ZdlPv@PLT
.L766:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L767
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L767:
	movq	%rbx, -256(%rbp)
	movq	-328(%rbp), %r15
.L768:
	movl	(%r15), %edx
	addq	$4, %r15
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L768
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r15), %rdx
	cmove	%rdx, %r15
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %r15
	subq	-328(%rbp), %r15
	movq	%r15, -264(%rbp)
	cmpq	$15, %r15
	ja	.L957
	cmpq	$1, %r15
	jne	.L772
	movzbl	-112(%rbp), %eax
	movb	%al, -240(%rbp)
	movq	%rbx, %rax
.L773:
	movq	%r15, -248(%rbp)
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$0, (%rax,%r15)
	movq	(%r14), %rdi
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L780
	call	_ZdlPv@PLT
.L780:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L781
	call	_ZdlPv@PLT
.L781:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L782
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L782
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L782
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L786:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r15d
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	jg	.L786
.L782:
	movq	(%r14), %rdi
	movl	$1, %edx
	movl	$31084, %eax
	movq	%r12, %rsi
	movq	%rbx, -256(%rbp)
	movl	$1768776038, -240(%rbp)
	movw	%ax, -236(%rbp)
	movq	$6, -248(%rbp)
	movb	$0, -234(%rbp)
	movb	$34, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-296(%rbp), %rdi
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-216(%rbp), %rdx
	movq	-224(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -264(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-224(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L785
	call	_ZdlPv@PLT
.L785:
	movq	-256(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L787
	call	_ZdlPv@PLT
.L787:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L788
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L788:
	movq	%rbx, -256(%rbp)
	movl	$880169033, -240(%rbp)
	movq	$4, -248(%rbp)
	movb	$0, -236(%rbp)
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L944:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L730:
	movq	-192(%rbp), %rdx
	movq	%rdx, (%rax)
	movl	%ecx, %edx
	movq	-8(%r15,%rdx), %rsi
	movq	%rsi, -8(%rax,%rdx)
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rax
	leal	(%rcx,%rax), %edx
	subq	%rax, %r15
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L731
	andl	$-8, %edx
	xorl	%eax, %eax
.L734:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%r15,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L734
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rax
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L941:
	leaq	-264(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L865:
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	je	.L958
	subl	$2, 12(%r14)
.L867:
	movq	%r12, %rsi
	movl	$1, %edx
	movb	$93, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r14)
	movl	-276(%rbp), %esi
	movq	-272(%rbp), %rdi
	call	uv_free_interface_addresses@PLT
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L958:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L867
	testl	%eax, %eax
	jle	.L867
	movl	-320(%rbp), %ebx
	.p2align 4,,10
	.p2align 3
.L869:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -264(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	movq	(%r14), %rdi
	jg	.L869
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L826:
	movq	%rbx, %rax
	testq	%r15, %r15
	je	.L827
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L805:
	movq	%rbx, %rax
	testq	%rcx, %rcx
	je	.L806
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L953:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rcx, -336(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-264(%rbp), %rdx
	movq	-336(%rbp), %rcx
	movq	%rax, -256(%rbp)
	movq	%rdx, -240(%rbp)
.L804:
	movl	%ecx, %edx
	cmpl	$8, %ecx
	jb	.L959
	movq	-160(%rbp), %rdx
	movq	%rdx, (%rax)
	movl	%ecx, %edx
	movq	-8(%r15,%rdx), %rsi
	movq	%rsi, -8(%rax,%rdx)
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rax
	leal	(%rcx,%rax), %edx
	subq	%rax, %r15
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L808
	andl	$-8, %edx
	xorl	%eax, %eax
.L811:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%r15,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L811
.L808:
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rax
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L954:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-264(%rbp), %rdx
	movq	%rax, -256(%rbp)
	movq	%rdx, -240(%rbp)
.L825:
	movl	%r15d, %edx
	cmpl	$8, %r15d
	jb	.L960
	movq	-112(%rbp), %rdx
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	movq	%rdx, (%rax)
	movq	-328(%rbp), %rdi
	movl	%r15d, %edx
	movq	-8(%rdi,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rsi, %rax
	leal	(%r15,%rax), %edx
	subq	%rax, %rdi
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L829
	andl	$-8, %edx
	xorl	%eax, %eax
.L832:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rdi,%rcx), %r8
	movq	%r8, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L832
.L829:
	movq	-264(%rbp), %r15
	movq	-256(%rbp), %rax
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L951:
	movzbl	(%r15), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L731
	movzwl	-2(%r15,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L751:
	movq	%rbx, %rax
	testq	%rcx, %rcx
	je	.L752
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L960:
	andl	$4, %r15d
	jne	.L961
	testl	%edx, %edx
	je	.L829
	movq	-328(%rbp), %rcx
	movzbl	(%rcx), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L829
	movq	-328(%rbp), %rcx
	movzwl	-2(%rcx,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L959:
	andl	$4, %ecx
	jne	.L962
	testl	%edx, %edx
	je	.L808
	movzbl	(%r15), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L808
	movzwl	-2(%r15,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L772:
	movq	%rbx, %rax
	testq	%r15, %r15
	je	.L773
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L957:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-264(%rbp), %rdx
	movq	%rax, -256(%rbp)
	movq	%rdx, -240(%rbp)
.L771:
	movl	%r15d, %edx
	cmpl	$8, %r15d
	jnb	.L774
	andl	$4, %r15d
	jne	.L963
	testl	%edx, %edx
	je	.L775
	movq	-328(%rbp), %rcx
	movzbl	(%rcx), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L775
	movq	-328(%rbp), %rcx
	movzwl	-2(%rcx,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	.p2align 4,,10
	.p2align 3
.L775:
	movq	-264(%rbp), %r15
	movq	-256(%rbp), %rax
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L956:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rcx, -336(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-264(%rbp), %rdx
	movq	-336(%rbp), %rcx
	movq	%rax, -256(%rbp)
	movq	%rdx, -240(%rbp)
.L750:
	movl	%ecx, %edx
	cmpl	$8, %ecx
	jnb	.L753
	andl	$4, %ecx
	jne	.L964
	testl	%edx, %edx
	je	.L754
	movzbl	(%r15), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	je	.L754
	movzwl	-2(%r15,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	.p2align 4,,10
	.p2align 3
.L754:
	movq	-264(%rbp), %rcx
	movq	-256(%rbp), %rax
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L952:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L955:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -264(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L753:
	movq	-160(%rbp), %rdx
	movq	%rdx, (%rax)
	movl	%ecx, %edx
	movq	-8(%r15,%rdx), %rsi
	movq	%rsi, -8(%rax,%rdx)
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rax
	leal	(%rcx,%rax), %edx
	subq	%rax, %r15
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L754
	andl	$-8, %edx
	xorl	%eax, %eax
.L757:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%r15,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L757
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L774:
	movq	-112(%rbp), %rdx
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	movq	%rdx, (%rax)
	movq	-328(%rbp), %rdi
	movl	%r15d, %edx
	movq	-8(%rdi,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rsi, %rax
	leal	(%r15,%rax), %edx
	subq	%rax, %rdi
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L775
	andl	$-8, %edx
	xorl	%eax, %eax
.L778:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rdi,%rcx), %r8
	movq	%r8, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L778
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L950:
	movl	(%r15), %ecx
	movl	%ecx, (%rax)
	movl	-4(%r15,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L731
.L962:
	movl	(%r15), %ecx
	movl	%ecx, (%rax)
	movl	-4(%r15,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L808
.L961:
	movq	-328(%rbp), %rsi
	movl	(%rsi), %ecx
	movl	%ecx, (%rax)
	movl	-4(%rsi,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L829
.L963:
	movq	-328(%rbp), %rsi
	movl	(%rsi), %ecx
	movl	%ecx, (%rax)
	movl	-4(%rsi,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L775
.L964:
	movl	(%r15), %ecx
	movl	%ecx, (%rax)
	movl	-4(%r15,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L754
.L940:
	call	__stack_chk_fail@PLT
.L949:
	movq	%rbx, %rdi
	jmp	.L704
	.cfi_endproc
.LFE7779:
	.size	_ZN6reportL25PrintNetworkInterfaceInfoEPN4node10JSONWriterE, .-_ZN6reportL25PrintNetworkInterfaceInfoEPN4node10JSONWriterE
	.section	.text._ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_,"axG",@progbits,_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	.type	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_, @function
_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_:
.LFB8575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-136(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rbx)
	je	.L984
.L966:
	cmpb	$0, 8(%rbx)
	je	.L985
.L967:
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	leaq	-128(%rbp), %r15
	movq	%r14, -128(%rbp)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L986
	cmpq	$1, %rax
	jne	.L973
	movzbl	0(%r13), %edx
	movb	%dl, -112(%rbp)
	movq	%r14, %rdx
.L974:
	movq	%rax, -120(%rbp)
	movq	%r12, %rsi
	leaq	-96(%rbp), %r13
	movb	$0, (%rdx,%rax)
	movq	(%rbx), %rdi
	movl	$1, %edx
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-80(%rbp), %r15
	movq	%rax, -160(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-160(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L975
	call	_ZdlPv@PLT
.L975:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L976
	call	_ZdlPv@PLT
.L976:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L977
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L977:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L978
	call	_ZdlPv@PLT
.L978:
	movl	$1, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L987
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L967
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L967
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L971:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	jg	.L971
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L973:
	testq	%rax, %rax
	jne	.L988
	movq	%r14, %rdx
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L986:
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L972:
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L984:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L966
.L987:
	call	__stack_chk_fail@PLT
.L988:
	movq	%r14, %rdi
	jmp	.L972
	.cfi_endproc
.LFE8575:
	.size	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_, .-_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	.section	.text._ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_,"axG",@progbits,_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	.type	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_, @function
_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_:
.LFB8618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-136(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%rdx, -152(%rbp)
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$1, 16(%rbx)
	je	.L1008
.L990:
	cmpb	$0, 8(%rbx)
	je	.L1009
.L991:
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	leaq	-128(%rbp), %r15
	movq	%r14, -128(%rbp)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L1010
	cmpq	$1, %rax
	jne	.L997
	movzbl	0(%r13), %edx
	movb	%dl, -112(%rbp)
	movq	%r14, %rdx
.L998:
	movq	%rax, -120(%rbp)
	movq	%r12, %rsi
	leaq	-96(%rbp), %r13
	movb	$0, (%rdx,%rax)
	movq	(%rbx), %rdi
	movl	$1, %edx
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	leaq	-80(%rbp), %r15
	movq	%rax, -160(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-160(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L999
	call	_ZdlPv@PLT
.L999:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1000
	call	_ZdlPv@PLT
.L1000:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	movq	(%rbx), %rdi
	jne	.L1001
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
.L1001:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-152(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -136(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1002
	call	_ZdlPv@PLT
.L1002:
	movl	$1, 16(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1011
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1009:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%rbx)
	jne	.L991
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jle	.L991
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L995:
	movq	(%rbx), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r14d
	movb	$32, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%rbx)
	jg	.L995
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L997:
	testq	%rax, %rax
	jne	.L1012
	movq	%r14, %rdx
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L996:
	movq	%r8, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1008:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -136(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%rbx), %rdi
	jmp	.L990
.L1011:
	call	__stack_chk_fail@PLT
.L1012:
	movq	%r14, %rdi
	jmp	.L996
	.cfi_endproc
.LFE8618:
	.size	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_, .-_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	.section	.rodata.str1.1
.LC24:
	.string	"v"
.LC25:
	.string	"12.18.4"
.LC26:
	.string	"nodejsVersion"
.LC27:
	.string	"gnu_get_libc_version"
.LC28:
	.string	"glibcVersionRuntime"
.LC29:
	.string	"."
.LC30:
	.string	"glibcVersionCompiler"
.LC31:
	.string	"wordSize"
.LC32:
	.string	"arch"
.LC33:
	.string	"platform"
.LC34:
	.string	"componentVersions"
.LC35:
	.string	"node"
.LC36:
	.string	"v8"
.LC37:
	.string	"uv"
.LC38:
	.string	"zlib"
.LC39:
	.string	"brotli"
.LC40:
	.string	"ares"
.LC41:
	.string	"modules"
.LC42:
	.string	"nghttp2"
.LC43:
	.string	"napi"
.LC44:
	.string	"llhttp"
.LC45:
	.string	"http_parser"
.LC46:
	.string	"openssl"
.LC47:
	.string	"cldr"
.LC48:
	.string	"icu"
.LC49:
	.string	"tz"
.LC50:
	.string	"unicode"
.LC51:
	.string	"release"
.LC52:
	.string	"name"
.LC53:
	.string	"lts"
.LC54:
	.string	"headersUrl"
.LC55:
	.string	"sourceUrl"
.LC56:
	.string	"osName"
.LC57:
	.string	"osRelease"
.LC58:
	.string	"osVersion"
.LC59:
	.string	"osMachine"
.LC60:
	.string	"cpus"
.LC61:
	.string	"host"
	.text
	.p2align 4
	.type	_ZN6reportL23PrintVersionInformationEPN4node10JSONWriterE, @function
_ZN6reportL23PrintVersionInformationEPN4node10JSONWriterE:
.LFB7777:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-1536(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1424(%rbp), %rbx
	movq	%rbx, %rdi
	subq	$1688, %rsp
	movq	.LC62(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -1696(%rbp)
	movhps	.LC3(%rip), %xmm1
	movaps	%xmm1, -1632(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -1424(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -1192(%rbp)
	movups	%xmm0, -1176(%rbp)
	movw	%r12w, -1200(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -1536(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -1208(%rbp)
	addq	%r13, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1632(%rbp), %xmm1
	movq	%rax, -1424(%rbp)
	leaq	-1472(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -1536(%rbp)
	movaps	%xmm0, -1520(%rbp)
	movaps	%xmm0, -1504(%rbp)
	movaps	%xmm0, -1488(%rbp)
	movq	%rax, -1680(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rbx, %rdi
	movl	$16, -1464(%rbp)
	movq	%rax, -1528(%rbp)
	leaq	-1440(%rbp), %rax
	movq	%rax, -1688(%rbp)
	movq	%rax, -1456(%rbp)
	leaq	-1528(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -1672(%rbp)
	movq	$0, -1448(%rbp)
	movb	$0, -1440(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rdi
	movl	$7, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1552(%rbp), %rax
	leaq	-1568(%rbp), %rdi
	movq	$0, -1560(%rbp)
	movq	%rax, -1640(%rbp)
	movq	%rax, -1568(%rbp)
	movq	-1488(%rbp), %rax
	movq	%rdi, -1632(%rbp)
	movb	$0, -1552(%rbp)
	testq	%rax, %rax
	je	.L1014
	movq	-1504(%rbp), %r8
	movq	-1496(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1271
	subq	%rcx, %r8
.L1270:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	-1456(%rbp), %rax
	movq	%rax, -1664(%rbp)
.L1016:
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	leaq	-1608(%rbp), %r12
	je	.L1272
.L1017:
	cmpb	$0, 8(%r14)
	jne	.L1018
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1018
	movl	12(%r14), %r11d
	testl	%r11d, %r11d
	jle	.L1018
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1022:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1022
.L1018:
	leaq	.LC26(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1020
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1020:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	leaq	-1152(%rbp), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1152(%rbp), %rdi
	leaq	-1136(%rbp), %rax
	movq	%rax, -1648(%rbp)
	cmpq	%rax, %rdi
	je	.L1023
	call	_ZdlPv@PLT
.L1023:
	movl	$1, 16(%r14)
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1024
	call	_ZdlPv@PLT
.L1024:
	movq	-1648(%rbp), %rbx
	movq	-1448(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	-1664(%rbp), %rdi
	movb	$0, -1136(%rbp)
	movq	%rbx, %rcx
	movq	%rbx, -1152(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	testb	$3, -1464(%rbp)
	movl	$0, %ecx
	movq	-1672(%rbp), %rdi
	cmovne	-1448(%rbp), %rcx
	movq	-1456(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	-1152(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1026
	call	_ZdlPv@PLT
.L1026:
	xorl	%edi, %edi
	leaq	.LC27(%rip), %rsi
	call	dlsym@PLT
	testq	%rax, %rax
	je	.L1027
	call	*%rax
	cmpl	$1, 16(%r14)
	movq	%rax, -1704(%rbp)
	je	.L1273
.L1028:
	cmpb	$0, 8(%r14)
	jne	.L1029
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1029
	movl	12(%r14), %r10d
	testl	%r10d, %r10d
	jle	.L1029
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1033:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1033
.L1029:
	leaq	.LC28(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1031
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1031:
	movq	-1704(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movl	$1, 16(%r14)
.L1027:
	movq	%r13, %rdi
	movl	$2, %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC29(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$31, %esi
	movq	%r13, %rdi
	call	_ZNSolsEi@PLT
	movq	-1640(%rbp), %rax
	movq	$0, -1560(%rbp)
	movb	$0, -1552(%rbp)
	movq	%rax, -1568(%rbp)
	movq	-1488(%rbp), %rax
	testq	%rax, %rax
	je	.L1034
	movq	-1504(%rbp), %r8
	movq	-1496(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1035
	subq	%rcx, %rax
	movq	-1632(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1036:
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	je	.L1274
.L1037:
	cmpb	$0, 8(%r14)
	jne	.L1038
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1038
	movl	12(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L1038
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1042
.L1038:
	leaq	.LC30(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1040
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1040:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1648(%rbp), %rdi
	je	.L1043
	call	_ZdlPv@PLT
.L1043:
	movl	$1, 16(%r14)
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1044
	call	_ZdlPv@PLT
.L1044:
	movq	-1648(%rbp), %rbx
	movq	-1448(%rbp), %rdx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	-1664(%rbp), %rdi
	movb	$0, -1136(%rbp)
	movq	%rbx, %rcx
	movq	%rbx, -1152(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	testb	$3, -1464(%rbp)
	movl	$0, %ecx
	movq	-1672(%rbp), %rdi
	cmovne	-1448(%rbp), %rcx
	movq	-1456(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	-1152(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1046
	call	_ZdlPv@PLT
.L1046:
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	je	.L1275
.L1047:
	cmpb	$0, 8(%r14)
	jne	.L1048
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1048
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	jle	.L1048
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1052
.L1048:
	leaq	.LC31(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1050
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1050:
	movl	$64, %esi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, 16(%r14)
	leaq	640+_ZN4node11per_process8metadataE(%rip), %rdx
	movq	%r14, %rdi
	leaq	.LC32(%rip), %rsi
	call	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	cmpl	$1, 16(%r14)
	je	.L1276
.L1053:
	cmpb	$0, 8(%r14)
	jne	.L1054
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1054
	movl	12(%r14), %edi
	testl	%edi, %edi
	jle	.L1054
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1058:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1058
.L1054:
	leaq	.LC33(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1056
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1056:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	672+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1648(%rbp), %rdi
	je	.L1059
	call	_ZdlPv@PLT
.L1059:
	movq	.LC1(%rip), %xmm1
	movl	$1, 16(%r14)
	leaq	-1024(%rbp), %r13
	movq	%r13, %rdi
	movhps	.LC3(%rip), %xmm1
	movaps	%xmm1, -1664(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -792(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -776(%rbp)
	movw	%si, -800(%rbp)
	xorl	%esi, %esi
	movq	%rax, -1024(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -808(%rbp)
	movq	%rbx, -1152(%rbp)
	movq	%rcx, -1152(%rbp,%rax)
	movq	$0, -1144(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-1648(%rbp), %rdi
	xorl	%esi, %esi
	addq	-24(%rax), %rdi
	movq	%rax, -1136(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movdqa	-1664(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -1152(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -1152(%rbp)
	addq	$80, %rax
	movq	%rax, -1024(%rbp)
	leaq	-1072(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1664(%rbp)
	movaps	%xmm1, -1136(%rbp)
	movaps	%xmm0, -1120(%rbp)
	movaps	%xmm0, -1104(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-1128(%rbp), %rsi
	movq	%rax, -1128(%rbp)
	leaq	-1040(%rbp), %rax
	movl	$24, -1064(%rbp)
	movq	%rax, -1672(%rbp)
	movq	%rax, -1056(%rbp)
	movq	$0, -1048(%rbp)
	movb	$0, -1040(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	cmpl	$1, 16(%r14)
	je	.L1277
.L1060:
	cmpb	$0, 8(%r14)
	jne	.L1061
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1061
	movl	12(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L1061
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1065
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1061:
	leaq	.LC34(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1063
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1063:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$123, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r14)
	movq	%r14, %rdi
	leaq	_ZN4node11per_process8metadataE(%rip), %rdx
	movl	$0, 16(%r14)
	leaq	.LC35(%rip), %rsi
	call	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	cmpl	$1, 16(%r14)
	je	.L1278
.L1066:
	cmpb	$0, 8(%r14)
	jne	.L1067
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1067
	movl	12(%r14), %edx
	testl	%edx, %edx
	jle	.L1067
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1071
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1067:
	leaq	.LC36(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1069
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1069:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rdi
	leaq	32+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1072
	call	_ZdlPv@PLT
.L1072:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1073
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1073
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1073
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1077
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1073:
	leaq	.LC37(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1075
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1075:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rdi
	leaq	64+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1078
	call	_ZdlPv@PLT
.L1078:
	movl	$1, 16(%r14)
	leaq	96+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC38(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	cmpl	$1, 16(%r14)
	je	.L1279
.L1079:
	cmpb	$0, 8(%r14)
	jne	.L1080
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1080
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1080
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1084
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1080:
	leaq	.LC39(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1082
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1082:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rdi
	leaq	128+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1085
	call	_ZdlPv@PLT
.L1085:
	movl	$1, 16(%r14)
	leaq	160+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC40(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	leaq	192+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC41(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	leaq	224+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC42(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	leaq	256+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC43(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	cmpl	$1, 16(%r14)
	je	.L1280
.L1086:
	cmpb	$0, 8(%r14)
	jne	.L1087
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1087
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1087
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1091:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1091
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1087:
	leaq	.LC44(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1089
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1089:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rdi
	leaq	288+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1092
	call	_ZdlPv@PLT
.L1092:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1093
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1093
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1093
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1097:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1097
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1093:
	leaq	.LC45(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1095
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1095:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rdi
	leaq	320+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1098
	call	_ZdlPv@PLT
.L1098:
	movl	$1, 16(%r14)
	leaq	352+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC46(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	leaq	384+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC47(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	cmpl	$1, 16(%r14)
	je	.L1281
.L1099:
	cmpb	$0, 8(%r14)
	jne	.L1100
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1100
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1100
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1104:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1104
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1100:
	leaq	.LC48(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1102
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1102:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rdi
	leaq	416+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1105
	call	_ZdlPv@PLT
.L1105:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1106
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1106
	movl	12(%r14), %r11d
	testl	%r11d, %r11d
	jle	.L1106
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r12d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1110
	movq	%r13, %r12
	movq	-1704(%rbp), %r13
.L1106:
	leaq	.LC49(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1108
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1108:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1632(%rbp), %rdi
	leaq	448+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1111
	call	_ZdlPv@PLT
.L1111:
	movl	$1, 16(%r14)
	movq	%r14, %rdi
	leaq	480+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC50(%rip), %rsi
	call	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	je	.L1282
	subl	$2, 12(%r14)
.L1113:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %r10d
	testl	%r10d, %r10d
	je	.L1283
.L1116:
	movq	.LC1(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movl	$1, 16(%r14)
	movq	%rax, -1152(%rbp)
	addq	$80, %rax
	movq	-1056(%rbp), %rdi
	movhps	.LC2(%rip), %xmm0
	movq	%rax, -1024(%rbp)
	movaps	%xmm0, -1136(%rbp)
	cmpq	-1672(%rbp), %rdi
	je	.L1117
	call	_ZdlPv@PLT
.L1117:
	movq	-1664(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1128(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r13, %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -1152(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -1136(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1136(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -1152(%rbp)
	movq	%rcx, -1152(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1024(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	leaq	.LC51(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	leaq	512+_ZN4node11per_process8metadataE(%rip), %rdx
	leaq	.LC52(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA5_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	cmpl	$1, 16(%r14)
	je	.L1284
.L1118:
	cmpb	$0, 8(%r14)
	jne	.L1119
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1119
	movl	12(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L1119
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1123:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1123
.L1119:
	leaq	.LC53(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1121
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1121:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	544+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1648(%rbp), %rdi
	je	.L1124
	call	_ZdlPv@PLT
.L1124:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1125
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1125
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	jle	.L1125
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1129
.L1125:
	leaq	.LC54(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1127
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1127:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	608+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1648(%rbp), %rdi
	je	.L1130
	call	_ZdlPv@PLT
.L1130:
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1131
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1131
	movl	12(%r14), %edi
	testl	%edi, %edi
	jle	.L1131
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1135:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1135
.L1131:
	leaq	.LC55(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1133
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1133:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	576+_ZN4node11per_process8metadataE(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1144(%rbp), %rdx
	movq	-1152(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$34, -1608(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1152(%rbp), %rdi
	cmpq	-1648(%rbp), %rdi
	je	.L1136
	call	_ZdlPv@PLT
.L1136:
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	movl	$1, 16(%r14)
	je	.L1285
	subl	$2, 12(%r14)
.L1138:
	movq	%r12, %rsi
	movl	$1, %edx
	movb	$125, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %esi
	testl	%esi, %esi
	je	.L1286
.L1141:
	movl	$1, 16(%r14)
	movq	%r15, %rdi
	call	uv_os_uname@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L1287
.L1142:
	leaq	-1612(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, %rsi
	movq	%rax, -1712(%rbp)
	call	uv_cpu_info@PLT
	movl	%eax, -1716(%rbp)
	testl	%eax, %eax
	je	.L1288
.L1164:
	movq	%r14, %rdi
	leaq	-128(%rbp), %r13
	call	_ZN6reportL25PrintNetworkInterfaceInfoEPN4node10JSONWriterE
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$65, -1608(%rbp)
	call	uv_os_gethostname@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L1289
.L1230:
	movq	.LC62(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-1456(%rbp), %rdi
	movq	%rax, -1424(%rbp)
	movhps	.LC2(%rip), %xmm0
	movaps	%xmm0, -1536(%rbp)
	cmpq	-1688(%rbp), %rdi
	je	.L1237
	call	_ZdlPv@PLT
.L1237:
	movq	-1680(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1528(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1696(%rbp), %rdi
	movq	%rax, -1536(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1536(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1424(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1290
	addq	$1688, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1271:
	.cfi_restore_state
	subq	%rcx, %rax
	movq	%rax, %r8
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L1138
	testl	%eax, %eax
	jle	.L1138
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1140:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	movq	(%r14), %rdi
	jg	.L1140
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1282:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L1113
	testl	%eax, %eax
	jle	.L1113
	xorl	%ecx, %ecx
	movq	%r13, -1704(%rbp)
	movq	%r12, %r13
	movl	%ecx, %r12d
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	(%r14), %rdi
.L1115:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1608(%rbp)
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, 12(%r14)
	jg	.L1114
	movq	%r13, %r12
	movq	(%r14), %rdi
	movq	-1704(%rbp), %r13
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	-1632(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	jne	.L1037
.L1274:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L1037
	.p2align 4,,10
	.p2align 3
.L1283:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1289:
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	je	.L1291
.L1231:
	cmpb	$0, 8(%r14)
	je	.L1292
.L1232:
	leaq	.LC61(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movq	-1712(%rbp), %rsi
	movl	$1, %edx
	movb	$58, -1612(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1293
.L1234:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movl	$1, 16(%r14)
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	-1712(%rbp), %rsi
	movl	$1, %edx
	movb	$10, -1612(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1232
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1232
	movq	-1712(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L1236:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1612(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1236
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1288:
	leaq	.LC60(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	movl	-1612(%rbp), %eax
	testl	%eax, %eax
	jle	.L1294
	leaq	-1600(%rbp), %rax
	cmpl	$1, 16(%r14)
	movl	$0, -1672(%rbp)
	leaq	-1613(%rbp), %r13
	movq	$0, -1648(%rbp)
	movq	(%r14), %rdi
	leaq	-1584(%rbp), %rbx
	movq	%rax, -1664(%rbp)
	je	.L1226
.L1168:
	cmpb	$0, 8(%r14)
	jne	.L1169
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1169
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1169
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	movq	(%r14), %rdi
	jg	.L1171
.L1169:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$123, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, 12(%r14)
	movq	-1648(%rbp), %r15
	addq	-1608(%rbp), %r15
	cmpb	$0, 8(%r14)
	movl	$0, 16(%r14)
	jne	.L1172
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1172
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1172
	xorl	%ecx, %ecx
	movq	%rbx, -1704(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1176:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1176
	movq	-1704(%rbp), %rbx
.L1172:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, -1600(%rbp)
	movl	$1701080941, -1584(%rbp)
	movb	$108, -1580(%rbp)
	movq	$5, -1592(%rbp)
	movb	$0, -1579(%rbp)
	movb	$34, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -1613(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1175
	call	_ZdlPv@PLT
.L1175:
	movq	-1600(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1177
	call	_ZdlPv@PLT
.L1177:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1178
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1178:
	movq	(%r15), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	$1, 16(%r14)
	movq	-1648(%rbp), %r15
	movb	$44, -1613(%rbp)
	addq	-1608(%rbp), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1179
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1179
	movl	12(%r14), %r11d
	testl	%r11d, %r11d
	jle	.L1179
	xorl	%ecx, %ecx
	movq	%rbx, -1704(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1183:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1183
	movq	-1704(%rbp), %rbx
.L1179:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, -1600(%rbp)
	movl	$1701146739, -1584(%rbp)
	movb	$100, -1580(%rbp)
	movq	$5, -1592(%rbp)
	movb	$0, -1579(%rbp)
	movb	$34, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -1613(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1182
	call	_ZdlPv@PLT
.L1182:
	movq	-1600(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1184
	call	_ZdlPv@PLT
.L1184:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1185
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1185:
	movl	8(%r15), %esi
	call	_ZNSolsEi@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	$1, 16(%r14)
	movq	-1648(%rbp), %r15
	movb	$44, -1613(%rbp)
	addq	-1608(%rbp), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1186
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1186
	movl	12(%r14), %r10d
	testl	%r10d, %r10d
	jle	.L1186
	xorl	%ecx, %ecx
	movq	%rbx, -1704(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1190
	movq	-1704(%rbp), %rbx
.L1186:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, -1600(%rbp)
	movl	$1919251317, -1584(%rbp)
	movq	$4, -1592(%rbp)
	movb	$0, -1580(%rbp)
	movb	$34, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -1613(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1189
	call	_ZdlPv@PLT
.L1189:
	movq	-1600(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1191
	call	_ZdlPv@PLT
.L1191:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1192
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1192:
	movq	16(%r15), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	$1, 16(%r14)
	movq	-1648(%rbp), %r15
	movb	$44, -1613(%rbp)
	addq	-1608(%rbp), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1193
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1193
	movl	12(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L1193
	xorl	%ecx, %ecx
	movq	%rbx, -1704(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1197
	movq	-1704(%rbp), %rbx
.L1193:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, -1600(%rbp)
	movl	$1701013870, -1584(%rbp)
	movq	$4, -1592(%rbp)
	movb	$0, -1580(%rbp)
	movb	$34, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -1613(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1196
	call	_ZdlPv@PLT
.L1196:
	movq	-1600(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1198
	call	_ZdlPv@PLT
.L1198:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1199
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1199:
	movq	24(%r15), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	$1, 16(%r14)
	movq	-1648(%rbp), %r15
	movb	$44, -1613(%rbp)
	addq	-1608(%rbp), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1200
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1200
	movl	12(%r14), %edi
	testl	%edi, %edi
	jle	.L1200
	xorl	%ecx, %ecx
	movq	%rbx, -1704(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1204
	movq	-1704(%rbp), %rbx
.L1200:
	movq	(%r14), %rdi
	movl	$31091, %r8d
	movl	$1, %edx
	movq	%r13, %rsi
	movw	%r8w, -1584(%rbp)
	movq	%rbx, -1600(%rbp)
	movb	$115, -1582(%rbp)
	movq	$3, -1592(%rbp)
	movb	$0, -1581(%rbp)
	movb	$34, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -1613(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1203
	call	_ZdlPv@PLT
.L1203:
	movq	-1600(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1205
	call	_ZdlPv@PLT
.L1205:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1206
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1206:
	movq	32(%r15), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	$1, 16(%r14)
	movq	-1648(%rbp), %r15
	movb	$44, -1613(%rbp)
	addq	-1608(%rbp), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1207
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1207
	movl	12(%r14), %esi
	testl	%esi, %esi
	jle	.L1207
	xorl	%ecx, %ecx
	movq	%rbx, -1704(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1211
	movq	-1704(%rbp), %rbx
.L1207:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, -1600(%rbp)
	movl	$1701602409, -1584(%rbp)
	movq	$4, -1592(%rbp)
	movb	$0, -1580(%rbp)
	movb	$34, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -1613(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1210
	call	_ZdlPv@PLT
.L1210:
	movq	-1600(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1212
	call	_ZdlPv@PLT
.L1212:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1213
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1213:
	movq	40(%r15), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movl	$1, 16(%r14)
	movq	-1648(%rbp), %r15
	movb	$44, -1613(%rbp)
	addq	-1608(%rbp), %r15
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1214
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1214
	movl	12(%r14), %edx
	testl	%edx, %edx
	jle	.L1214
	xorl	%ecx, %ecx
	movq	%rbx, -1704(%rbp)
	movl	%ecx, %ebx
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1218
	movq	-1704(%rbp), %rbx
.L1214:
	movq	(%r14), %rdi
	movl	$29289, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	movw	%cx, -1584(%rbp)
	movq	%rbx, -1600(%rbp)
	movb	$113, -1582(%rbp)
	movq	$3, -1592(%rbp)
	movb	$0, -1581(%rbp)
	movb	$34, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	movq	%rax, -1704(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1704(%rbp), %r8
	movq	-1560(%rbp), %rdx
	movq	-1568(%rbp), %rsi
	movq	%r8, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -1613(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1568(%rbp), %rdi
	cmpq	-1640(%rbp), %rdi
	je	.L1217
	call	_ZdlPv@PLT
.L1217:
	movq	-1600(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1219
	call	_ZdlPv@PLT
.L1219:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	jne	.L1220
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
.L1220:
	movq	48(%r15), %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	cmpb	$0, 8(%r14)
	movl	$1, 16(%r14)
	movq	(%r14), %rdi
	je	.L1295
	subl	$2, 12(%r14)
.L1222:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$125, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$1, -1672(%rbp)
	movl	-1672(%rbp), %eax
	addq	$56, -1648(%rbp)
	movl	$1, 16(%r14)
	cmpl	%eax, -1612(%rbp)
	jle	.L1225
	movq	(%r14), %rdi
.L1226:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1295:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L1222
	testl	%eax, %eax
	jle	.L1222
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L1224:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r15d, 12(%r14)
	movq	(%r14), %rdi
	jg	.L1224
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1294:
	leaq	-1613(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L1225:
	cmpb	$0, 8(%r14)
	movq	(%r14), %rdi
	je	.L1296
	subl	$2, 12(%r14)
.L1227:
	movq	%r13, %rsi
	movl	$1, %edx
	movb	$93, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, 16(%r14)
	movl	-1612(%rbp), %esi
	movq	-1608(%rbp), %rdi
	call	uv_free_cpu_info@PLT
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1296:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -1613(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r14), %eax
	movq	(%r14), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r14)
	movl	%eax, 12(%r14)
	jne	.L1227
	testl	%eax, %eax
	jle	.L1227
	movl	-1716(%rbp), %ebx
	.p2align 4,,10
	.p2align 3
.L1229:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -1613(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	movq	(%r14), %rdi
	jg	.L1229
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1287:
	cmpl	$1, 16(%r14)
	movq	(%r14), %rdi
	je	.L1297
.L1143:
	cmpb	$0, 8(%r14)
	je	.L1298
.L1144:
	leaq	.LC56(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1299
.L1146:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1300
.L1149:
	leaq	.LC57(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1301
.L1151:
	leaq	-896(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1302
.L1154:
	leaq	.LC58(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1303
.L1156:
	leaq	-640(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movl	$1, 16(%r14)
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1304
.L1159:
	leaq	.LC59(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$58, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	je	.L1305
.L1161:
	leaq	-384(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movl	$1, 16(%r14)
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1298:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1144
	movl	12(%r14), %ecx
	testl	%ecx, %ecx
	jle	.L1144
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1148:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%r14)
	jg	.L1148
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1300:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1149
	movl	12(%r14), %edx
	testl	%edx, %edx
	jle	.L1149
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1153:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%r14)
	jg	.L1153
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1302:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1154
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1154
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1158:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %r13d
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r13d, 12(%r14)
	jg	.L1158
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1304:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r14)
	jne	.L1159
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.L1159
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r14)
	jg	.L1163
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1272:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1079
	.p2align 4,,10
	.p2align 3
.L1278:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1066
	.p2align 4,,10
	.p2align 3
.L1277:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1275:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1299:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1293:
	movq	(%r14), %rdi
	movq	-1712(%rbp), %rsi
	movl	$1, %edx
	movb	$32, -1612(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	-1664(%rbp), %rsi
	movq	-1632(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1036
	.p2align 4,,10
	.p2align 3
.L1014:
	leaq	-1456(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -1664(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1273:
	movq	(%r14), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1028
	.p2align 4,,10
	.p2align 3
.L1297:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -1608(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1291:
	movq	-1712(%rbp), %rsi
	movl	$1, %edx
	movb	$44, -1612(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r14), %rdi
	jmp	.L1231
.L1290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7777:
	.size	_ZN6reportL23PrintVersionInformationEPN4node10JSONWriterE, .-_ZN6reportL23PrintVersionInformationEPN4node10JSONWriterE
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC63:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L1333
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L1324
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1334
.L1308:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L1323:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L1335
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L1311:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L1312
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L1332:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L1336
.L1316:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L1313
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L1312:
	cmpq	%r12, %rbx
	je	.L1317
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L1321:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L1337
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L1321
.L1319:
	subq	%rbx, %r12
	addq	%r12, %r8
.L1317:
	testq	%r15, %r15
	je	.L1322
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L1322:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1337:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L1321
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1334:
	testq	%r8, %r8
	jne	.L1309
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1324:
	movl	$32, %esi
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1335:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L1311
.L1309:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L1308
.L1333:
	leaq	.LC63(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9112:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN4node28NativeSymbolDebuggingContextD2Ev,"axG",@progbits,_ZN4node28NativeSymbolDebuggingContextD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node28NativeSymbolDebuggingContextD2Ev
	.type	_ZN4node28NativeSymbolDebuggingContextD2Ev, @function
_ZN4node28NativeSymbolDebuggingContextD2Ev:
.LFB9149:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9149:
	.size	_ZN4node28NativeSymbolDebuggingContextD2Ev, .-_ZN4node28NativeSymbolDebuggingContextD2Ev
	.weak	_ZN4node28NativeSymbolDebuggingContextD1Ev
	.set	_ZN4node28NativeSymbolDebuggingContextD1Ev,_ZN4node28NativeSymbolDebuggingContextD2Ev
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB9557:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1340
	testq	%rsi, %rsi
	je	.L1356
.L1340:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L1357
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L1343
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L1344:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1358
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1343:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1344
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1357:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1342:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1344
.L1356:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9557:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.rodata.str1.1
.LC64:
	.string	"FatalError"
.LC65:
	.string	"Signal"
.LC66:
	.string	"No stack.\nUnavailable.\n"
.LC67:
	.string	"stack"
.LC68:
	.string	"message"
.LC69:
	.string	"basic_string::substr"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"%s: __pos (which is %zu) > this->size() (which is %zu)"
	.text
	.p2align 4
	.type	_ZN6reportL25PrintJavaScriptErrorStackEPN4node10JSONWriterEPN2v87IsolateENS3_5LocalINS3_6ObjectEEEPKc, @function
_ZN6reportL25PrintJavaScriptErrorStackEPN4node10JSONWriterEPN2v87IsolateENS3_5LocalINS3_6ObjectEEEPKc:
.LFB7781:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$360, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-208(%rbp), %rax
	movb	$0, -208(%rbp)
	movq	%rax, -352(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-272(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -376(%rbp)
	movq	$0, -216(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movl	$11, %ecx
	leaq	.LC64(%rip), %rdi
	movq	%rbx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1360
	movl	$7, %ecx
	leaq	.LC65(%rip), %rdi
	movq	%rbx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L1361
.L1360:
	leaq	-224(%rbp), %rax
	movq	-216(%rbp), %rdx
	movl	$23, %r8d
	xorl	%esi, %esi
	leaq	.LC66(%rip), %rcx
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1362:
	movq	-336(%rbp), %rdi
	xorl	%edx, %edx
	movl	$10, %esi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEcm@PLT
	movq	%rax, %rbx
	cmpl	$-1, %eax
	je	.L1546
	movq	-216(%rbp), %rax
	movslq	%ebx, %r14
	movq	-224(%rbp), %r15
	leaq	-176(%rbp), %rcx
	movq	%rcx, -360(%rbp)
	cmpq	%rax, %r14
	movq	%rcx, -192(%rbp)
	cmova	%rax, %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L1375
	testq	%r15, %r15
	je	.L1390
.L1375:
	movq	%r14, -288(%rbp)
	cmpq	$15, %r14
	ja	.L1547
	cmpq	$1, %r14
	jne	.L1378
	movzbl	(%r15), %eax
	leaq	-192(%rbp), %rsi
	leaq	-288(%rbp), %r13
	movq	%rsi, -328(%rbp)
	movb	%al, -176(%rbp)
	movq	-360(%rbp), %rax
.L1379:
	movq	%r14, -184(%rbp)
	movb	$0, (%rax,%r14)
	cmpl	$1, 16(%r12)
	je	.L1548
.L1380:
	cmpb	$0, 8(%r12)
	jne	.L1381
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r12)
	jne	.L1381
	movl	12(%r12), %edx
	testl	%edx, %edx
	jle	.L1381
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %r14d
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r14d, 12(%r12)
	jg	.L1385
.L1381:
	movq	(%r12), %rdi
	movl	$26465, %ecx
	leaq	-112(%rbp), %rax
	movq	%r13, %rsi
	movl	$1, %edx
	leaq	-128(%rbp), %r15
	movw	%cx, -108(%rbp)
	movq	%r15, -368(%rbp)
	movq	%rax, -304(%rbp)
	movq	%rax, -128(%rbp)
	movl	$1936942445, -112(%rbp)
	movb	$101, -106(%rbp)
	movq	$7, -120(%rbp)
	movb	$0, -105(%rbp)
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r15, %rsi
	movq	%rax, %r14
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -344(%rbp)
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	movq	%rax, -296(%rbp)
	cmpq	%rax, %rdi
	je	.L1384
	call	_ZdlPv@PLT
.L1384:
	movq	-128(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L1386
	call	_ZdlPv@PLT
.L1386:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r12)
	movq	(%r12), %rdi
	jne	.L1387
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rdi
.L1387:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-328(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1388
	call	_ZdlPv@PLT
.L1388:
	movl	$1, 16(%r12)
	leaq	.LC67(%rip), %rsi
	movq	%r12, %rdi
	addl	$1, %ebx
	movslq	%ebx, %rbx
	call	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	movq	-216(%rbp), %r14
	cmpq	%r14, %rbx
	ja	.L1549
	movq	-224(%rbp), %rax
	leaq	-144(%rbp), %r15
	movq	%r15, -160(%rbp)
	leaq	(%rax,%rbx), %r8
	addq	%r14, %rax
	je	.L1466
	testq	%r8, %r8
	je	.L1390
.L1466:
	subq	%rbx, %r14
	movq	%r14, -288(%rbp)
	cmpq	$15, %r14
	ja	.L1550
	cmpq	$1, %r14
	jne	.L1394
	movzbl	(%r8), %eax
	movb	%al, -144(%rbp)
	movq	%r15, %rax
.L1395:
	movq	%r14, -152(%rbp)
	movb	$0, (%rax,%r14)
	movq	-160(%rbp), %rdx
	movq	-224(%rbp), %rdi
	cmpq	%r15, %rdx
	je	.L1551
	movq	-152(%rbp), %rax
	movq	-144(%rbp), %rcx
	cmpq	-352(%rbp), %rdi
	je	.L1552
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	-208(%rbp), %rsi
	movq	%rdx, -224(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, -216(%rbp)
	testq	%rdi, %rdi
	je	.L1401
	movq	%rdi, -160(%rbp)
	movq	%rsi, -144(%rbp)
.L1399:
	movq	$0, -152(%rbp)
	movb	$0, (%rdi)
	movq	-160(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1402
	call	_ZdlPv@PLT
.L1402:
	movq	-336(%rbp), %rdi
	xorl	%edx, %edx
	movl	$10, %esi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEcm@PLT
	movl	%eax, -312(%rbp)
	cmpl	$-1, %eax
	je	.L1451
	movq	-216(%rbp), %r14
	cltq
	movq	-224(%rbp), %r15
	movq	-304(%rbp), %rsi
	cmpq	%r14, %rax
	cmovbe	%rax, %r14
	movq	%r15, %rax
	movq	%rsi, -128(%rbp)
	addq	%r14, %rax
	je	.L1406
	testq	%r15, %r15
	je	.L1390
.L1406:
	leaq	-112(%rbp), %rax
	movq	%rax, -384(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -392(%rbp)
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	%r14, -288(%rbp)
	cmpq	$15, %r14
	ja	.L1553
	cmpq	$1, %r14
	jne	.L1411
	movzbl	(%r15), %eax
	movb	%al, -112(%rbp)
	movq	-128(%rbp), %rax
.L1412:
	movq	%r14, -120(%rbp)
	movb	$0, (%rax,%r14)
	movq	-192(%rbp), %rdi
	movq	-128(%rbp), %rax
	cmpq	-304(%rbp), %rax
	je	.L1554
	movq	-120(%rbp), %xmm0
	movq	-112(%rbp), %rdx
	cmpq	-360(%rbp), %rdi
	je	.L1555
	movq	%rdx, %xmm1
	movq	-176(%rbp), %rcx
	movq	%rax, -192(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -184(%rbp)
	testq	%rdi, %rdi
	je	.L1418
	movq	%rdi, -128(%rbp)
	movq	%rcx, -112(%rbp)
.L1416:
	movq	$0, -120(%rbp)
	movb	$0, (%rdi)
	movq	-128(%rbp), %rdi
	cmpq	-304(%rbp), %rdi
	je	.L1419
	call	_ZdlPv@PLT
.L1419:
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rbx
	movq	%rax, %rdx
	leaq	(%rbx,%rax), %rcx
	sarq	$2, %rdx
	movq	%rcx, -320(%rbp)
	testq	%rdx, %rdx
	jle	.L1464
	leaq	(%rbx,%rdx,4), %r14
	movq	%rbx, %r15
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1560:
	movsbl	1(%r15), %edi
	call	iswspace@PLT
	testl	%eax, %eax
	je	.L1556
	movsbl	2(%r15), %edi
	call	iswspace@PLT
	testl	%eax, %eax
	je	.L1557
	movsbl	3(%r15), %edi
	call	iswspace@PLT
	testl	%eax, %eax
	je	.L1558
	addq	$4, %r15
	cmpq	%r14, %r15
	je	.L1559
.L1425:
	movsbl	(%r15), %edi
	call	iswspace@PLT
	testl	%eax, %eax
	jne	.L1560
.L1421:
	cmpq	%r15, -320(%rbp)
	je	.L1430
	movq	-328(%rbp), %rdi
	movq	%r15, %rdx
	xorl	%esi, %esi
	subq	%rbx, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE8_M_eraseEmm@PLT
	cmpl	$1, 16(%r12)
	movq	(%r12), %rdi
	je	.L1561
.L1433:
	cmpb	$0, 8(%r12)
	jne	.L1434
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, 8(%r12)
	movq	(%r12), %rdi
	jne	.L1434
	movl	12(%r12), %eax
	testl	%eax, %eax
	jle	.L1434
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1436:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -288(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r12)
	movq	(%r12), %rdi
	jg	.L1436
.L1434:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-328(%rbp), %rsi
	movq	-344(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -288(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-96(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1437
	call	_ZdlPv@PLT
.L1437:
	movl	-312(%rbp), %edx
	movq	-216(%rbp), %r15
	movl	$1, 16(%r12)
	addl	$1, %edx
	movslq	%edx, %rdx
	cmpq	%r15, %rdx
	ja	.L1562
	movq	-296(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-224(%rbp), %rax
	leaq	(%rax,%rdx), %r14
	addq	%r15, %rax
	je	.L1467
	testq	%r14, %r14
	je	.L1390
.L1467:
	subq	%rdx, %r15
	movq	%r15, -288(%rbp)
	cmpq	$15, %r15
	ja	.L1563
	cmpq	$1, %r15
	jne	.L1442
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	-296(%rbp), %rax
.L1443:
	movq	%r15, -88(%rbp)
	movb	$0, (%rax,%r15)
	movq	-224(%rbp), %rdi
	movq	-96(%rbp), %rax
	cmpq	-296(%rbp), %rax
	je	.L1564
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %xmm0
	cmpq	-352(%rbp), %rdi
	je	.L1565
	movq	%rdx, %xmm2
	movq	-208(%rbp), %rcx
	movq	%rax, -224(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, -216(%rbp)
	testq	%rdi, %rdi
	je	.L1449
	movq	%rdi, -96(%rbp)
	movq	%rcx, -80(%rbp)
.L1447:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1450
	call	_ZdlPv@PLT
.L1450:
	movq	-336(%rbp), %rdi
	xorl	%edx, %edx
	movl	$10, %esi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE4findEcm@PLT
	movl	%eax, -312(%rbp)
	cmpl	$-1, %eax
	je	.L1451
	movq	-216(%rbp), %r14
	cltq
	movq	-224(%rbp), %r15
	movq	-304(%rbp), %rcx
	cmpq	%r14, %rax
	cmovbe	%rax, %r14
	movq	%r15, %rax
	movq	%rcx, -128(%rbp)
	addq	%r14, %rax
	je	.L1515
	testq	%r15, %r15
	jne	.L1515
.L1390:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1361:
	leaq	-224(%rbp), %rax
	movq	%rax, -336(%rbp)
	testq	%r13, %r13
	je	.L1362
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC67(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1566
.L1364:
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	leaq	-224(%rbp), %rax
	movq	%rax, -336(%rbp)
	testq	%rdx, %rdx
	je	.L1362
	leaq	-288(%rbp), %r13
	movq	%r14, %rsi
	leaq	-80(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movslq	-280(%rbp), %rdx
	leaq	-96(%rbp), %rdi
	movq	-288(%rbp), %rsi
	movq	%rbx, -296(%rbp)
	addq	%rsi, %rdx
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-96(%rbp), %rax
	movq	-224(%rbp), %rdi
	cmpq	%rbx, %rax
	je	.L1567
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rdx
	cmpq	-352(%rbp), %rdi
	je	.L1568
	movq	%rdx, %xmm0
	movq	%rcx, %xmm7
	movq	-208(%rbp), %rsi
	movq	%rax, -224(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -216(%rbp)
	testq	%rdi, %rdi
	je	.L1370
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L1368:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L1372
	call	_ZdlPv@PLT
.L1372:
	movq	%r13, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	leaq	-224(%rbp), %rax
	movq	%rax, -336(%rbp)
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1547:
	leaq	-192(%rbp), %rax
	leaq	-288(%rbp), %r13
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rax, -328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-288(%rbp), %rax
	movq	%rax, -176(%rbp)
.L1377:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-288(%rbp), %r14
	movq	-192(%rbp), %rax
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	-320(%rbp), %rax
	subq	%r15, %rax
.L1420:
	cmpq	$2, %rax
	je	.L1426
	cmpq	$3, %rax
	je	.L1427
	cmpq	$1, %rax
	jne	.L1430
.L1428:
	movsbl	(%r15), %edi
	call	iswspace@PLT
	testl	%eax, %eax
	je	.L1421
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	$0, -184(%rbp)
	movb	$0, (%rbx)
	cmpl	$1, 16(%r12)
	movq	(%r12), %rdi
	jne	.L1433
.L1561:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	(%r12), %rdi
	jmp	.L1433
	.p2align 4,,10
	.p2align 3
.L1411:
	testq	%r14, %r14
	jne	.L1569
	movq	-304(%rbp), %rax
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1442:
	testq	%r15, %r15
	jne	.L1570
	movq	-296(%rbp), %rax
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	-368(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-288(%rbp), %rax
	movq	%rax, -112(%rbp)
.L1410:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-288(%rbp), %r14
	movq	-128(%rbp), %rax
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	-344(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-288(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1441:
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-288(%rbp), %r15
	movq	-96(%rbp), %rax
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1414
	cmpq	$1, %rdx
	je	.L1571
	movq	-304(%rbp), %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rdx
	movq	-192(%rbp), %rdi
.L1414:
	movq	%rdx, -184(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1445
	cmpq	$1, %rdx
	je	.L1572
	movq	-296(%rbp), %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-224(%rbp), %rdi
.L1445:
	movq	%rdx, -216(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	%rdx, %xmm3
	movq	%rax, -192(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -184(%rbp)
.L1418:
	movq	-304(%rbp), %rax
	movq	-384(%rbp), %rdi
	movq	%rax, -128(%rbp)
	movq	%rdi, -304(%rbp)
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	%rdx, %xmm4
	movq	%rax, -224(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -216(%rbp)
.L1449:
	movq	-296(%rbp), %rax
	movq	-392(%rbp), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdi, -296(%rbp)
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1556:
	addq	$1, %r15
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1557:
	addq	$2, %r15
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1558:
	addq	$3, %r15
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1451:
	cmpb	$0, 8(%r12)
	movq	(%r12), %rdi
	je	.L1573
	subl	$2, 12(%r12)
.L1453:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$93, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-192(%rbp), %rdi
	movl	$1, 16(%r12)
	cmpq	-360(%rbp), %rdi
	je	.L1374
	call	_ZdlPv@PLT
.L1374:
	movq	-376(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-224(%rbp), %rdi
	cmpq	-352(%rbp), %rdi
	je	.L1359
	call	_ZdlPv@PLT
.L1359:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1574
	addq	$360, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1573:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	12(%r12), %eax
	movq	(%r12), %rdi
	subl	$2, %eax
	cmpb	$0, 8(%r12)
	movl	%eax, 12(%r12)
	jne	.L1453
	testl	%eax, %eax
	jle	.L1453
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1455:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -288(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, 12(%r12)
	movq	(%r12), %rdi
	jg	.L1455
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1427:
	movsbl	(%r15), %edi
	call	iswspace@PLT
	testl	%eax, %eax
	je	.L1421
	addq	$1, %r15
.L1426:
	movsbl	(%r15), %edi
	call	iswspace@PLT
	testl	%eax, %eax
	je	.L1421
	addq	$1, %r15
	jmp	.L1428
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	-336(%rbp), %rdx
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	jmp	.L1374
	.p2align 4,,10
	.p2align 3
.L1571:
	movzbl	-112(%rbp), %eax
	movb	%al, (%rdi)
	movq	-120(%rbp), %rdx
	movq	-192(%rbp), %rdi
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1572:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	-224(%rbp), %rdi
	jmp	.L1445
	.p2align 4,,10
	.p2align 3
.L1394:
	testq	%r14, %r14
	jne	.L1575
	movq	%r15, %rax
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1378:
	testq	%r14, %r14
	jne	.L1576
	leaq	-192(%rbp), %rsi
	movq	-360(%rbp), %rax
	leaq	-288(%rbp), %r13
	movq	%rsi, -328(%rbp)
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1550:
	leaq	-160(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r8, -312(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-312(%rbp), %r8
	movq	%rax, -160(%rbp)
	movq	%rax, %rdi
	movq	-288(%rbp), %rax
	movq	%rax, -144(%rbp)
.L1393:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-288(%rbp), %r14
	movq	-160(%rbp), %rax
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	-152(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1397
	cmpq	$1, %rdx
	je	.L1577
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %rdx
	movq	-224(%rbp), %rdi
.L1397:
	movq	%rdx, -216(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-160(%rbp), %rdi
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, -224(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, -216(%rbp)
.L1401:
	movq	%r15, -160(%rbp)
	leaq	-144(%rbp), %r15
	movq	%r15, %rdi
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	(%r12), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -288(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1380
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	%rbx, %r15
	jmp	.L1420
.L1568:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm7
	movq	%rax, -224(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, -216(%rbp)
.L1370:
	movq	-296(%rbp), %rax
	movq	%rax, -96(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -296(%rbp)
	movq	%rax, %rdi
	jmp	.L1368
.L1577:
	movzbl	-144(%rbp), %eax
	movb	%al, (%rdi)
	movq	-152(%rbp), %rdx
	movq	-224(%rbp), %rdi
	jmp	.L1397
.L1567:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1366
	cmpq	$1, %rdx
	je	.L1578
	movq	-296(%rbp), %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-224(%rbp), %rdi
.L1366:
	movq	%rdx, -216(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1368
.L1566:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1364
.L1578:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	-224(%rbp), %rdi
	jmp	.L1366
.L1562:
	movq	%r15, %rcx
	leaq	.LC69(%rip), %rsi
	leaq	.LC70(%rip), %rdi
	xorl	%eax, %eax
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1574:
	call	__stack_chk_fail@PLT
.L1549:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	leaq	.LC69(%rip), %rsi
	xorl	%eax, %eax
	leaq	.LC70(%rip), %rdi
	call	_ZSt24__throw_out_of_range_fmtPKcz@PLT
.L1575:
	movq	%r15, %rdi
	jmp	.L1393
.L1576:
	leaq	-192(%rbp), %rax
	movq	-360(%rbp), %rdi
	leaq	-288(%rbp), %r13
	movq	%rax, -328(%rbp)
	jmp	.L1377
.L1569:
	movq	-304(%rbp), %rdi
	jmp	.L1410
.L1570:
	movq	-296(%rbp), %rdi
	jmp	.L1441
	.cfi_endproc
.LFE7781:
	.size	_ZN6reportL25PrintJavaScriptErrorStackEPN4node10JSONWriterEPN2v87IsolateENS3_5LocalINS3_6ObjectEEEPKc, .-_ZN6reportL25PrintJavaScriptErrorStackEPN4node10JSONWriterEPN2v87IsolateENS3_5LocalINS3_6ObjectEEEPKc
	.section	.rodata.str1.1
.LC71:
	.string	"header"
.LC72:
	.string	"filename"
.LC73:
	.string	"null"
.LC74:
	.string	"%4d-%02d-%02dT%02d:%02d:%02dZ"
.LC75:
	.string	"%ld"
.LC76:
	.string	"dumpEventTimeStamp"
.LC77:
	.string	"threadId"
.LC78:
	.string	"cwd"
.LC79:
	.string	"commandLine"
.LC80:
	.string	"javascriptStack"
.LC81:
	.string	"errorProperties"
.LC82:
	.string	"libuv"
.LC83:
	.string	"type"
.LC84:
	.string	"loop"
.LC85:
	.string	"is_active"
.LC86:
	.string	"address"
.LC87:
	.string	"workers"
.LC88:
	.string	"vector::reserve"
	.text
	.p2align 4
	.type	_ZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEb, @function
_ZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEb:
.LFB7771:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1208, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	24(%rbp), %ebx
	movq	%rcx, -5224(%rbp)
	movq	%r9, %r15
	movq	%rdx, %r12
	movq	%r8, %r14
	movq	%r9, -5296(%rbp)
	leaq	-4896(%rbp), %r13
	movq	%rsi, -5264(%rbp)
	movq	%rdi, -5288(%rbp)
	leaq	-5072(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node18DiagnosticFilename9LocalTimeEP2tm@PLT
	call	uv_os_getpid@PLT
	movq	%r13, %rdi
	movq	%r13, -5304(%rbp)
	movl	%eax, -5280(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%r13, %rdi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -4664(%rbp)
	movq	%rax, -4896(%rbp)
	xorl	%eax, %eax
	movw	%ax, -4672(%rbp)
	movups	%xmm0, -4648(%rbp)
	movq	$0, -4680(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	-24(%rax), %rsi
	addq	%r15, %rsi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE7copyfmtERKS2_@PLT
	movq	%r15, -5152(%rbp)
	movb	%bl, -5144(%rbp)
	movq	$0, -5140(%rbp)
	testb	%bl, %bl
	je	.L1926
	movq	-5296(%rbp), %rdi
	leaq	-5120(%rbp), %r13
.L1580:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$123, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-5152(%rbp), %rax
	leaq	.LC71(%rip), %rsi
	addl	$2, -5140(%rbp)
	movq	%rax, %rdi
	movq	%rax, -5256(%rbp)
	movl	$0, -5136(%rbp)
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	cmpl	$1, -5136(%rbp)
	je	.L1927
.L1584:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1928
.L1585:
	movl	$1, %edx
	leaq	-4944(%rbp), %rax
	movq	%r13, %rsi
	movabsq	$7302151880672568690, %rcx
	leaq	-4928(%rbp), %r15
	movq	%rcx, -4928(%rbp)
	movq	%rax, -5240(%rbp)
	movq	%r15, -4944(%rbp)
	movl	$1869181810, -4920(%rbp)
	movb	$110, -4916(%rbp)
	movq	$13, -4936(%rbp)
	movb	$0, -4915(%rbp)
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-4624(%rbp), %rcx
	movq	-5240(%rbp), %rsi
	movq	%rcx, %rdi
	movq	%rcx, -5232(%rbp)
	movq	%rax, %rbx
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	leaq	-4608(%rbp), %rax
	movq	%rax, -5248(%rbp)
	cmpq	%rax, %rdi
	je	.L1589
	call	_ZdlPv@PLT
.L1589:
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1590
	call	_ZdlPv@PLT
.L1590:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1591
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
.L1591:
	movl	$2, %esi
	call	_ZNSolsEi@PLT
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movl	$1, -5136(%rbp)
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1929
.L1592:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, -4944(%rbp)
	movl	$1852143205, -4928(%rbp)
	movb	$116, -4924(%rbp)
	movq	$5, -4936(%rbp)
	movb	$0, -4923(%rbp)
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5240(%rbp), %rsi
	movq	-5232(%rbp), %rdi
	movq	%rax, %rbx
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1596
	call	_ZdlPv@PLT
.L1596:
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1597
	call	_ZdlPv@PLT
.L1597:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1598
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1598:
	movq	-5256(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movl	$1, -5136(%rbp)
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1930
.L1599:
	movl	$25959, %r8d
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, -4944(%rbp)
	movw	%r8w, -4924(%rbp)
	movl	$1734963828, -4928(%rbp)
	movb	$114, -4922(%rbp)
	movq	$7, -4936(%rbp)
	movb	$0, -4921(%rbp)
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5240(%rbp), %rsi
	movq	-5232(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1603
	call	_ZdlPv@PLT
.L1603:
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1604
	call	_ZdlPv@PLT
.L1604:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1605
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1605:
	movq	-5224(%rbp), %rsi
	movq	-5256(%rbp), %rdi
	call	_ZN4node10JSONWriter12write_stringEPKc
	cmpq	$0, 8(%r14)
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	movq	-5152(%rbp), %rdi
	movl	$1, -5136(%rbp)
	je	.L1606
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	je	.L1607
.L1610:
	movq	-5256(%rbp), %rdi
	leaq	.LC72(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1931
.L1608:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5232(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1919
	call	_ZdlPv@PLT
.L1919:
	movl	-5072(%rbp), %eax
	subq	$8, %rsp
	leaq	-4224(%rbp), %rbx
	leaq	.LC74(%rip), %r8
	movl	$64, %ecx
	movl	$1, %edx
	movl	$64, %esi
	movq	%rbx, %rdi
	pushq	%rax
	movl	-5068(%rbp), %eax
	movl	$1, -5136(%rbp)
	pushq	%rax
	movl	-5064(%rbp), %eax
	pushq	%rax
	movl	-5060(%rbp), %eax
	pushq	%rax
	movl	-5056(%rbp), %eax
	addl	$1, %eax
	pushq	%rax
	movl	-5052(%rbp), %eax
	leal	1900(%rax), %r9d
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	addq	$48, %rsp
	cmpl	$1, -5136(%rbp)
	je	.L1932
.L1619:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1933
.L1620:
	movl	$1, %edx
	movq	%r13, %rsi
	movabsq	$7954894357430891876, %rax
	movq	%r15, -4944(%rbp)
	movq	%rax, -4928(%rbp)
	movl	$1835619444, -4920(%rbp)
	movb	$101, -4916(%rbp)
	movq	$13, -4936(%rbp)
	movb	$0, -4915(%rbp)
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5240(%rbp), %rsi
	movq	-5232(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1624
	call	_ZdlPv@PLT
.L1624:
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1625
	call	_ZdlPv@PLT
.L1625:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1626
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1626:
	movq	%r15, -4944(%rbp)
	movq	%rbx, %r12
.L1627:
	movl	(%r12), %edx
	addq	$4, %r12
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L1627
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r12), %rdx
	cmove	%rdx, %r12
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %r12
	subq	%rbx, %r12
	movq	%r12, -5120(%rbp)
	cmpq	$15, %r12
	ja	.L1934
	cmpq	$1, %r12
	jne	.L1631
	movzbl	-4224(%rbp), %eax
	movb	%al, -4928(%rbp)
	movq	%r15, %rax
.L1632:
	movq	%r12, -4936(%rbp)
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$0, (%rax,%r12)
	movq	-5152(%rbp), %rdi
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5240(%rbp), %rsi
	movq	-5232(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1639
	call	_ZdlPv@PLT
.L1639:
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1640
	call	_ZdlPv@PLT
.L1640:
	leaq	-5200(%rbp), %rdi
	movl	$1, -5136(%rbp)
	call	uv_gettimeofday@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L1641
.L1920:
	movl	-5136(%rbp), %eax
	movq	-5152(%rbp), %rdi
	cmpl	$1, %eax
	je	.L1651
.L1652:
	cmpb	$0, -5144(%rbp)
	jne	.L1653
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1653
	movl	-5140(%rbp), %eax
	testl	%eax, %eax
	jle	.L1653
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1656:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1656
.L1653:
	movl	$1, %edx
	movq	%r13, %rsi
	movabsq	$5292700866387604080, %rax
	movq	%r15, -4944(%rbp)
	movq	%rax, -4928(%rbp)
	movb	$100, -4920(%rbp)
	movq	$9, -4936(%rbp)
	movb	$0, -4919(%rbp)
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5240(%rbp), %rsi
	movq	-5232(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1657
	call	_ZdlPv@PLT
.L1657:
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1658
	call	_ZdlPv@PLT
.L1658:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1659
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
.L1659:
	movl	-5280(%rbp), %esi
	call	_ZNSolsEi@PLT
	movq	-5264(%rbp), %rax
	movl	$1, -5136(%rbp)
	testq	%rax, %rax
	je	.L1660
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	movq	1936(%rax), %r12
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1661
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1661
	movl	-5140(%rbp), %eax
	testl	%eax, %eax
	jle	.L1661
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1664:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1664
.L1661:
	movl	$1, %edx
	movq	%r13, %rsi
	movabsq	$7226417446569273460, %rax
	movq	%r15, -4944(%rbp)
	movq	%rax, -4928(%rbp)
	movq	$8, -4936(%rbp)
	movb	$0, -4920(%rbp)
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5240(%rbp), %rsi
	movq	-5232(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1665
	call	_ZdlPv@PLT
.L1665:
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1666
	call	_ZdlPv@PLT
.L1666:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1667
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
.L1667:
	movq	%r12, %rsi
	call	_ZNSo9_M_insertImEERSoT_@PLT
	movl	$1, -5136(%rbp)
.L1668:
	leaq	-4160(%rbp), %r14
	movq	%r13, %rsi
	movq	$4096, -5120(%rbp)
	movq	%r14, %rdi
	call	uv_cwd@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L1935
.L1674:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	424(%rax), %rcx
	cmpq	%rcx, 416(%rax)
	je	.L1681
	movq	-5256(%rbp), %rdi
	leaq	.LC79(%rip), %rsi
	call	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	416(%rax), %r12
	movq	424(%rax), %rax
	movq	%rax, -5280(%rbp)
	cmpq	%r12, %rax
	je	.L1692
	cmpl	$1, -5136(%rbp)
	movq	-5152(%rbp), %rdi
	movq	%r12, %rbx
	je	.L1693
.L1685:
	cmpb	$0, -5144(%rbp)
	jne	.L1686
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1686
	movl	-5140(%rbp), %r14d
	testl	%r14d, %r14d
	jle	.L1686
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L1689:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %r14d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%r14d, -5140(%rbp)
	jg	.L1689
.L1686:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5232(%rbp), %rdi
	movq	%r12, %rsi
	movq	%rax, %r14
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1690
	call	_ZdlPv@PLT
	addq	$32, %rbx
	movl	$1, -5136(%rbp)
	cmpq	%rbx, -5280(%rbp)
	je	.L1692
.L1691:
	movq	-5152(%rbp), %rdi
	movq	%rbx, %r12
.L1693:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	jmp	.L1685
	.p2align 4,,10
	.p2align 3
.L1933:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1620
	movl	-5140(%rbp), %edx
	testl	%edx, %edx
	jle	.L1620
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1623:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%r12d, -5140(%rbp)
	jg	.L1623
	jmp	.L1620
	.p2align 4,,10
	.p2align 3
.L1930:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1599
	movl	-5140(%rbp), %r9d
	testl	%r9d, %r9d
	jle	.L1599
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1602:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1602
	jmp	.L1599
	.p2align 4,,10
	.p2align 3
.L1929:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1592
	movl	-5140(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L1592
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1595:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1595
	jmp	.L1592
	.p2align 4,,10
	.p2align 3
.L1928:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1585
	movl	-5140(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.L1585
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1588:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1588
	jmp	.L1585
	.p2align 4,,10
	.p2align 3
.L1926:
	leaq	-5120(%rbp), %r13
	movq	%r15, %rdi
	movl	$1, %edx
	movb	$10, -5120(%rbp)
	movq	%r13, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1580
	movl	-5140(%rbp), %ebx
	testl	%ebx, %ebx
	jle	.L1580
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1583:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1583
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1606:
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	je	.L1614
.L1617:
	movq	-5256(%rbp), %rdi
	leaq	.LC72(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1936
.L1615:
	movl	$4, %edx
	leaq	.LC73(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1919
	.p2align 4,,10
	.p2align 3
.L1631:
	movq	%r15, %rax
	testq	%r12, %r12
	je	.L1632
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1931:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1936:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1610
	movl	-5140(%rbp), %esi
	testl	%esi, %esi
	jle	.L1610
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, -5140(%rbp)
	jg	.L1611
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1617
	movl	-5140(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L1617
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, -5140(%rbp)
	jg	.L1618
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1690:
	movl	$1, -5136(%rbp)
	addq	$32, %rbx
	cmpq	%rbx, -5280(%rbp)
	jne	.L1691
.L1692:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1937
	subl	$2, -5140(%rbp)
.L1694:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$93, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, -5136(%rbp)
.L1681:
	movq	-5256(%rbp), %rdi
	call	_ZN6reportL23PrintVersionInformationEPN4node10JSONWriterE
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1938
	subl	$2, -5140(%rbp)
.L1698:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$125, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %r12d
	testl	%r12d, %r12d
	je	.L1939
.L1701:
	movq	-5256(%rbp), %rbx
	leaq	.LC80(%rip), %rsi
	movl	$1, -5136(%rbp)
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	movq	-5288(%rbp), %r14
	movq	16(%rbp), %rdx
	movq	%rbx, %rdi
	movq	-5224(%rbp), %rcx
	movq	%r14, %rsi
	call	_ZN6reportL25PrintJavaScriptErrorStackEPN4node10JSONWriterEPN2v87IsolateENS3_5LocalINS3_6ObjectEEEPKc
	leaq	.LC81(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter16json_objectstartIPKcEEvT_
	cmpq	$0, 16(%rbp)
	je	.L1702
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	16(%rbp), %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	16(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	_ZN2v86Object19GetOwnPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1940
	movq	%rax, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	testl	%eax, %eax
	je	.L1831
	leaq	-5184(%rbp), %rcx
	xorl	%r14d, %r14d
	movq	%r13, -5320(%rbp)
	movl	%eax, %r13d
	movq	%rcx, -5280(%rbp)
	leaq	-5168(%rbp), %rcx
	movq	%rcx, -5336(%rbp)
	movq	%r15, -5312(%rbp)
	movl	%r14d, %r15d
	.p2align 4,,10
	.p2align 3
.L1726:
	movl	%r15d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1725
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1725
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1725
	movq	16(%rbp), %rdi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1725
	movq	%rbx, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L1725
	movq	-5288(%rbp), %rsi
	movq	-5280(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r9, -5328(%rbp)
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-5184(%rbp), %rdx
	movl	$6, %ecx
	leaq	.LC67(%rip), %rdi
	movq	-5328(%rbp), %r9
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1921
	movl	$8, %ecx
	movq	%rdx, %rsi
	leaq	.LC68(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1921
	movq	-5288(%rbp), %rsi
	movq	-5336(%rbp), %rdi
	movq	%r9, %rdx
	leaq	-5201(%rbp), %r14
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-5168(%rbp), %rsi
	movslq	-5160(%rbp), %rdx
	movq	-5248(%rbp), %rax
	movq	-5232(%rbp), %rdi
	addq	%rsi, %rdx
	movq	%rax, -4624(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movslq	-5176(%rbp), %rdx
	movq	-5184(%rbp), %rsi
	movq	-5312(%rbp), %rax
	movq	-5240(%rbp), %rdi
	addq	%rsi, %rdx
	movq	%rax, -4944(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	cmpl	$1, -5136(%rbp)
	je	.L1941
.L1716:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1717
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$10, -5201(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1717
	movl	-5140(%rbp), %r11d
	testl	%r11d, %r11d
	jle	.L1717
	xorl	%ecx, %ecx
	movq	%rbx, -5328(%rbp)
	movl	%ecx, %ebx
	jmp	.L1720
	.p2align 4,,10
	.p2align 3
.L1719:
	movq	-5152(%rbp), %rdi
.L1720:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -5201(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, -5140(%rbp)
	jg	.L1719
	movq	-5328(%rbp), %rbx
	movq	-5152(%rbp), %rdi
.L1717:
	movq	-5240(%rbp), %rsi
	call	_ZN4node10JSONWriter12write_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.isra.0
	movq	-5152(%rbp), %rdi
	movq	%r14, %rsi
	movl	$1, %edx
	movb	$58, -5201(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1721
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -5201(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
.L1721:
	movq	-5232(%rbp), %rsi
	call	_ZN4node10JSONWriter12write_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE.isra.0
	movq	-4944(%rbp), %rdi
	movl	$1, -5136(%rbp)
	cmpq	-5312(%rbp), %rdi
	je	.L1723
	call	_ZdlPv@PLT
.L1723:
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1724
	call	_ZdlPv@PLT
.L1724:
	movq	-5336(%rbp), %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
.L1921:
	movq	-5280(%rbp), %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1725:
	addl	$1, %r15d
	cmpl	%r15d, %r13d
	jne	.L1726
	movq	-5320(%rbp), %r13
	movq	-5312(%rbp), %r15
.L1831:
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
.L1702:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1942
	subl	$2, -5140(%rbp)
.L1728:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$125, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L1943
.L1731:
	movl	$1, -5136(%rbp)
.L1709:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1944
	subl	$2, -5140(%rbp)
.L1733:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$125, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L1945
.L1736:
	movq	-5256(%rbp), %rbx
	movl	$1, -5136(%rbp)
	movq	%rbx, %rdi
	call	_ZN6reportL16PrintNativeStackEPN4node10JSONWriterE
	movq	-5288(%rbp), %rsi
	movq	%rbx, %rdi
	call	_ZN6reportL17PrintGCStatisticsEPN4node10JSONWriterEPN2v87IsolateE
	movq	%rbx, %rdi
	call	_ZN6reportL18PrintResourceUsageEPN4node10JSONWriterE
	leaq	.LC82(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	movq	-5264(%rbp), %rax
	testq	%rax, %rax
	je	.L1737
	movq	360(%rax), %rax
	movq	_ZN6report10WalkHandleEP11uv_handle_sPv@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdx
	movq	2360(%rax), %rdi
	call	uv_walk@PLT
	cmpl	$1, -5136(%rbp)
	je	.L1946
.L1738:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1739
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1739
	movl	-5140(%rbp), %r8d
	testl	%r8d, %r8d
	jle	.L1739
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1742:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1742
.L1739:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$123, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	addl	$2, -5140(%rbp)
	cmpb	$0, -5144(%rbp)
	movl	$0, -5136(%rbp)
	jne	.L1746
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1746
	movl	-5140(%rbp), %esi
	testl	%esi, %esi
	jle	.L1746
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1747:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, -5140(%rbp)
	jg	.L1747
.L1746:
	movq	-5256(%rbp), %rdi
	leaq	.LC83(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1744
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
.L1744:
	movq	-5256(%rbp), %rdi
	leaq	.LC84(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5264(%rbp), %rax
	movl	$1, -5136(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_loop_alive@PLT
	cmpl	$1, -5136(%rbp)
	movl	%eax, %ebx
	je	.L1947
.L1748:
	cmpb	$0, -5144(%rbp)
	jne	.L1752
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1752
	movl	-5140(%rbp), %ecx
	testl	%ecx, %ecx
	jle	.L1752
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1753:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %r12d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%r12d, -5140(%rbp)
	jg	.L1753
.L1752:
	movq	-5256(%rbp), %rdi
	leaq	.LC85(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	jne	.L1750
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
.L1750:
	cmpl	$1, %ebx
	leaq	.LC21(%rip), %rax
	movq	.LC1(%rip), %xmm1
	leaq	.LC20(%rip), %rsi
	sbbq	%rdx, %rdx
	leaq	-4496(%rbp), %r12
	leaq	-4544(%rbp), %r14
	notq	%rdx
	movhps	.LC3(%rip), %xmm1
	addq	$5, %rdx
	testl	%ebx, %ebx
	movaps	%xmm1, -5280(%rbp)
	cmove	%rax, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5264(%rbp), %rax
	movq	%r12, %rdi
	movl	$1, -5136(%rbp)
	movq	360(%rax), %rax
	movq	2360(%rax), %r8
	movq	%r8, -5288(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	16+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movw	%dx, -4272(%rbp)
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movups	%xmm0, -4264(%rbp)
	movq	-5232(%rbp), %rdi
	movups	%xmm0, -4248(%rbp)
	movq	%rax, -4496(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -4280(%rbp)
	movq	%rbx, -4624(%rbp)
	movq	%rcx, -4624(%rbp,%rax)
	movq	$0, -4616(%rbp)
	addq	-24(%rbx), %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-5248(%rbp), %rdi
	xorl	%esi, %esi
	addq	-24(%rax), %rdi
	movq	%rax, -4608(%rbp)
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movdqa	-5280(%rbp), %xmm1
	movq	-24(%rax), %rax
	movq	%rcx, -4624(%rbp,%rax)
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -4624(%rbp)
	addq	$80, %rax
	movaps	%xmm1, -4608(%rbp)
	movaps	%xmm0, -4592(%rbp)
	movaps	%xmm0, -4576(%rbp)
	movaps	%xmm0, -4560(%rbp)
	movq	%rax, -4496(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-4600(%rbp), %rsi
	movq	%rax, -4600(%rbp)
	leaq	-4512(%rbp), %rax
	movq	%rax, -5280(%rbp)
	movq	%rax, -4528(%rbp)
	movl	$24, -4536(%rbp)
	movq	$0, -4520(%rbp)
	movb	$0, -4512(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-5248(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4608(%rbp), %rax
	movq	-5248(%rbp), %rdx
	movq	-5288(%rbp), %r8
	addq	-24(%rax), %rdx
	cmpb	$0, 225(%rdx)
	je	.L1948
.L1824:
	movb	$48, 224(%rdx)
	movq	-24(%rax), %rdx
	movq	%r8, %rsi
	movq	-5248(%rbp), %rdi
	movq	$16, -4592(%rbp,%rdx)
	movq	-24(%rax), %rdx
	addq	%rdi, %rdx
	movl	24(%rdx), %eax
	andl	$-75, %eax
	orl	$8, %eax
	movl	%eax, 24(%rdx)
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movq	-4560(%rbp), %rax
	movq	%r15, -4944(%rbp)
	movq	$0, -4936(%rbp)
	movb	$0, -4928(%rbp)
	testq	%rax, %rax
	je	.L1758
	movq	-4576(%rbp), %r8
	movq	-4568(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1759
	subq	%rcx, %rax
	movq	-5240(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1760:
	movq	.LC1(%rip), %xmm0
	leaq	24+_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-4528(%rbp), %rdi
	movq	%rax, -4624(%rbp)
	addq	$80, %rax
	movhps	.LC2(%rip), %xmm0
	movq	%rax, -4496(%rbp)
	movaps	%xmm0, -4608(%rbp)
	cmpq	-5280(%rbp), %rdi
	je	.L1761
	call	_ZdlPv@PLT
.L1761:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -4600(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	48+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	-24(%rax), %rax
	movq	%rcx, -4624(%rbp,%rax)
	movq	32+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	40+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -4608(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -4608(%rbp,%rax)
	movq	-24(%rbx), %rax
	movq	24+_ZTTNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rbx, -4624(%rbp)
	movq	%rcx, -4624(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -4496(%rbp)
	movq	$0, -4616(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-5256(%rbp), %rdi
	movq	-5240(%rbp), %rdx
	leaq	.LC86(%rip), %rsi
	call	_ZN4node10JSONWriter13json_keyvalueIA8_cNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEvRKT_RKT0_
	movq	-4944(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L1762
	call	_ZdlPv@PLT
.L1762:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1949
	subl	$2, -5140(%rbp)
.L1764:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$125, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, -5136(%rbp)
.L1737:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1950
	subl	$2, -5140(%rbp)
.L1768:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$93, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5256(%rbp), %rdi
	leaq	.LC87(%rip), %rsi
	movl	$1, -5136(%rbp)
	call	_ZN4node10JSONWriter15json_arraystartIPKcEEvT_
	cmpq	$0, -5264(%rbp)
	je	.L1771
	movq	-5240(%rbp), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1951
	movq	-5232(%rbp), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L1952
	movq	-5264(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$0, -5104(%rbp)
	movaps	%xmm0, -5120(%rbp)
	movq	1960(%rax), %rbx
	testq	%rbx, %rbx
	je	.L1774
	movq	$0, -5248(%rbp)
	movq	%r13, -5264(%rbp)
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L1954:
	movq	16(%rdx), %rdi
	movq	%r14, 16(%rdx)
	testq	%rdi, %rdi
	je	.L1778
.L1922:
	movq	(%rdi), %rax
	call	*8(%rax)
.L1778:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	leaq	1000(%r15), %rdi
	call	uv_async_send@PLT
	movq	%r15, %rdi
	call	_ZN4node11Environment22RequestInterruptFromV8Ev@PLT
	addq	$1, -5248(%rbp)
.L1775:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1953
.L1780:
	movq	8(%rbx), %r13
	leaq	176(%r13), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r13), %r15
	testq	%r15, %r15
	je	.L1775
	movl	$56, %edi
	leaq	2488(%r15), %r13
	call	_Znwm@PLT
	leaq	-5224(%rbp), %rdx
	movq	%r13, %rdi
	movb	$0, 8(%rax)
	movq	%rax, %r14
	movq	$0, 16(%rax)
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_EE(%rip), %rax
	movq	%rax, (%r14)
	movq	-5240(%rbp), %rax
	movq	%rdx, 24(%r14)
	movq	%rax, 32(%r14)
	movq	-5264(%rbp), %rax
	movq	%rax, 40(%r14)
	movq	-5232(%rbp), %rax
	movq	%rax, 48(%r14)
	call	uv_mutex_lock@PLT
	movq	2568(%r15), %rdx
	lock addq	$1, 2552(%r15)
	movq	%r14, 2568(%r15)
	testq	%rdx, %rdx
	jne	.L1954
	movq	2560(%r15), %rdi
	movq	%r14, 2560(%r15)
	testq	%rdi, %rdi
	jne	.L1922
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1953:
	movq	-5240(%rbp), %rdi
	movq	-5264(%rbp), %r13
	call	uv_mutex_lock@PLT
	movabsq	$288230376151711743, %rax
	cmpq	%rax, -5248(%rbp)
	ja	.L1955
	movq	-5120(%rbp), %rbx
	movq	-5112(%rbp), %r12
	movq	-5104(%rbp), %rax
	movq	%r12, %r14
	subq	%rbx, %rax
	subq	%rbx, %r14
	sarq	$5, %rax
	cmpq	%rax, -5248(%rbp)
	ja	.L1956
.L1782:
	movq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rax, -5248(%rbp)
	jbe	.L1794
	movq	-5248(%rbp), %r14
	movq	-5240(%rbp), %r15
	movq	-5232(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	uv_cond_wait@PLT
	movq	-5112(%rbp), %r12
	movq	-5120(%rbp), %rdx
	movq	%r12, %rax
	subq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%r14, %rax
	jb	.L1790
	movq	%rdx, %rbx
.L1794:
	leaq	-4992(%rbp), %rax
	movq	%rax, -5248(%rbp)
	leaq	-5008(%rbp), %rax
	movq	%rax, -5264(%rbp)
	cmpq	%r12, %rbx
	je	.L1809
	movq	%r13, -5280(%rbp)
	.p2align 4,,10
	.p2align 3
.L1810:
	movq	-5248(%rbp), %rax
	movq	8(%rbx), %r14
	movq	%rax, -5008(%rbp)
	movq	(%rbx), %r13
	movq	%r13, %rax
	addq	%r14, %rax
	je	.L1795
	testq	%r13, %r13
	je	.L1957
.L1795:
	movq	%r14, -5168(%rbp)
	cmpq	$15, %r14
	ja	.L1958
	cmpq	$1, %r14
	jne	.L1798
	movzbl	0(%r13), %eax
	movb	%al, -4992(%rbp)
	movq	-5248(%rbp), %rax
.L1799:
	movq	%r14, -5000(%rbp)
	movb	$0, (%rax,%r14)
	cmpl	$1, -5136(%rbp)
	je	.L1959
.L1800:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %r13
	je	.L1960
	movl	-5140(%rbp), %edx
.L1803:
	movq	-5264(%rbp), %rsi
	leaq	-4976(%rbp), %rdi
	call	_ZN4node8ReindentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi@PLT
	movq	-4968(%rbp), %rdx
	movq	-4976(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4976(%rbp), %rdi
	leaq	-4960(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1806
	call	_ZdlPv@PLT
.L1806:
	movl	$1, -5136(%rbp)
	movq	-5008(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1807
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L1810
.L1925:
	movq	-5280(%rbp), %r13
.L1809:
	movq	-5240(%rbp), %rdi
	call	uv_mutex_unlock@PLT
	movq	-5112(%rbp), %rbx
	movq	-5120(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1792
	.p2align 4,,10
	.p2align 3
.L1793:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1811
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1793
.L1812:
	movq	-5120(%rbp), %r12
.L1792:
	testq	%r12, %r12
	je	.L1814
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1814:
	movq	-5232(%rbp), %rdi
	call	uv_cond_destroy@PLT
	movq	-5240(%rbp), %rdi
	call	uv_mutex_destroy@PLT
.L1771:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1961
	subl	$2, -5140(%rbp)
.L1816:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$93, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5256(%rbp), %rdi
	movl	$1, -5136(%rbp)
	call	_ZN6reportL22PrintSystemInformationEPN4node10JSONWriterE
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1962
	subl	$2, -5140(%rbp)
.L1820:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$125, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	testl	%eax, %eax
	je	.L1963
.L1823:
	movq	-5296(%rbp), %rcx
	movq	-5304(%rbp), %rbx
	movl	$1, -5136(%rbp)
	movq	(%rcx), %rax
	movq	%rbx, %rsi
	addq	-24(%rax), %rcx
	movq	%rcx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE7copyfmtERKS2_@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, -4896(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1964
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1950:
	.cfi_restore_state
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1768
	testl	%eax, %eax
	jle	.L1768
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1770:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1770
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1944:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1733
	testl	%eax, %eax
	jle	.L1733
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1735:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1735
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1938:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1698
	testl	%eax, %eax
	jle	.L1698
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1700:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1700
	jmp	.L1698
	.p2align 4,,10
	.p2align 3
.L1962:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1820
	testl	%eax, %eax
	jle	.L1820
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1822:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1822
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L1961:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1816
	testl	%eax, %eax
	jle	.L1816
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1818:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1818
	jmp	.L1816
	.p2align 4,,10
	.p2align 3
.L1942:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1728
	testl	%eax, %eax
	jle	.L1728
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1730:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1730
	jmp	.L1728
	.p2align 4,,10
	.p2align 3
.L1960:
	leaq	-5168(%rbp), %r14
	movl	$1, %edx
	movq	%r13, %rdi
	movb	$10, -5168(%rbp)
	movq	%r14, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1965
	movl	-5140(%rbp), %edx
	movq	-5152(%rbp), %r13
	testl	%edx, %edx
	jle	.L1803
	xorl	%r15d, %r15d
	movq	%r13, %rdi
	jmp	.L1805
	.p2align 4,,10
	.p2align 3
.L1804:
	movq	-5152(%rbp), %rdi
.L1805:
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$32, -5168(%rbp)
	addl	$1, %r15d
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %edx
	cmpl	%r15d, %edx
	jg	.L1804
	movq	-5152(%rbp), %r13
	jmp	.L1803
	.p2align 4,,10
	.p2align 3
.L1811:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1793
	jmp	.L1812
	.p2align 4,,10
	.p2align 3
.L1807:
	addq	$32, %rbx
	cmpq	%rbx, %r12
	jne	.L1810
	jmp	.L1925
	.p2align 4,,10
	.p2align 3
.L1798:
	testq	%r14, %r14
	jne	.L1966
	movq	-5248(%rbp), %rax
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1958:
	movq	-5264(%rbp), %rdi
	leaq	-5168(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -5008(%rbp)
	movq	%rax, %rdi
	movq	-5168(%rbp), %rax
	movq	%rax, -4992(%rbp)
.L1797:
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-5168(%rbp), %r14
	movq	-5008(%rbp), %rax
	jmp	.L1799
	.p2align 4,,10
	.p2align 3
.L1959:
	movq	-5152(%rbp), %rdi
	leaq	-5168(%rbp), %rsi
	movl	$1, %edx
	movb	$44, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1949:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1764
	testl	%eax, %eax
	jle	.L1764
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1766:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1766
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1937:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1694
	testl	%eax, %eax
	jle	.L1694
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1696:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1696
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L1945:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1736
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1701
	.p2align 4,,10
	.p2align 3
.L1935:
	cmpl	$1, -5136(%rbp)
	movq	-5152(%rbp), %rdi
	leaq	-5168(%rbp), %r12
	je	.L1967
.L1675:
	cmpb	$0, -5144(%rbp)
	je	.L1676
.L1679:
	movq	-5256(%rbp), %rdi
	leaq	.LC78(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r12, %rsi
	movl	$1, %edx
	movb	$58, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	je	.L1968
.L1677:
	movq	-5256(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movl	$1, -5136(%rbp)
	jmp	.L1674
	.p2align 4,,10
	.p2align 3
.L1676:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1679
	movl	-5140(%rbp), %eax
	testl	%eax, %eax
	jle	.L1679
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	addl	$1, %ebx
	movb	$32, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, -5140(%rbp)
	jg	.L1680
	jmp	.L1679
	.p2align 4,,10
	.p2align 3
.L1641:
	movslq	-5192(%rbp), %rax
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC75(%rip), %rcx
	imulq	$1000, -5200(%rbp), %r8
	movq	-5240(%rbp), %rdi
	movq	%rax, %rdx
	imulq	$274877907, %rax, %rax
	sarl	$31, %edx
	sarq	$38, %rax
	subl	%edx, %eax
	movl	$32, %edx
	cltq
	addq	%rax, %r8
	xorl	%eax, %eax
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	cmpl	$1, -5136(%rbp)
	je	.L1969
.L1643:
	cmpb	$0, -5144(%rbp)
	je	.L1644
.L1647:
	movq	-5256(%rbp), %rdi
	leaq	.LC76(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1970
.L1645:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5240(%rbp), %rsi
	movq	-5232(%rbp), %rdi
	movq	%rax, %r12
	call	_ZN4node15EscapeJsonCharsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-4616(%rbp), %rdx
	movq	-4624(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$34, -5120(%rbp)
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-4624(%rbp), %rdi
	cmpq	-5248(%rbp), %rdi
	je	.L1649
	call	_ZdlPv@PLT
.L1649:
	movq	-4944(%rbp), %rdi
	movl	$1, -5136(%rbp)
	cmpq	%r15, %rdi
	je	.L1971
	call	_ZdlPv@PLT
	jmp	.L1920
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1647
	movl	-5140(%rbp), %eax
	testl	%eax, %eax
	jle	.L1647
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, -5140(%rbp)
	jg	.L1648
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1934:
	movq	-5240(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-5120(%rbp), %rdx
	movq	%rax, -4944(%rbp)
	movq	%rdx, -4928(%rbp)
.L1630:
	movl	%r12d, %edx
	cmpl	$8, %r12d
	jnb	.L1633
	andl	$4, %r12d
	jne	.L1972
	testl	%edx, %edx
	je	.L1634
	movzbl	(%rbx), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	jne	.L1973
.L1634:
	movq	-5120(%rbp), %r12
	movq	-4944(%rbp), %rax
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	je	.L1669
.L1672:
	movq	-5256(%rbp), %rdi
	leaq	.LC77(%rip), %rsi
	call	_ZN4node10JSONWriter12write_stringEPKc
	movq	-5152(%rbp), %rdi
	movq	%r13, %rsi
	movl	$1, %edx
	movb	$58, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1974
.L1670:
	movl	$4, %edx
	leaq	.LC73(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	$1, -5136(%rbp)
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1963:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1823
	.p2align 4,,10
	.p2align 3
.L1943:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	-5240(%rbp), %rdi
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	-5152(%rbp), %rdi
.L1651:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1619
	.p2align 4,,10
	.p2align 3
.L1927:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1970:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	jmp	.L1645
	.p2align 4,,10
	.p2align 3
.L1968:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1677
	.p2align 4,,10
	.p2align 3
.L1974:
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	-4224(%rbp), %rdx
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	movq	%rdx, (%rax)
	movl	%r12d, %edx
	movq	-8(%rbx,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rsi, %rax
	leal	(%r12,%rax), %edx
	subq	%rax, %rbx
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L1634
	andl	$-8, %edx
	xorl	%eax, %eax
.L1637:
	movl	%eax, %ecx
	addl	$8, %eax
	movq	(%rbx,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%edx, %eax
	jb	.L1637
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$10, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpb	$0, -5144(%rbp)
	jne	.L1672
	movl	-5140(%rbp), %eax
	testl	%eax, %eax
	jle	.L1672
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1673:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	addl	$1, %ebx
	movb	$32, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpl	%ebx, -5140(%rbp)
	jg	.L1673
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1948:
	movq	240(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L1975
	cmpb	$0, 56(%rdi)
	jne	.L1756
	movq	%rdx, -5320(%rbp)
	movq	%r8, -5312(%rbp)
	movq	%rdi, -5288(%rbp)
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	-5288(%rbp), %rdi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rcx
	movq	-5312(%rbp), %r8
	movq	-5320(%rbp), %rdx
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1757
	movq	-4608(%rbp), %rax
.L1756:
	movb	$1, 225(%rdx)
	jmp	.L1824
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	-5248(%rbp), %rax
	movq	%rax, %r15
	salq	$5, %r15
	testq	%rax, %rax
	je	.L1836
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	-5112(%rbp), %r12
	movq	%rax, %rbx
	movq	-5120(%rbp), %rax
.L1783:
	cmpq	%r12, %rax
	je	.L1784
	movq	%rbx, %rdx
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1785:
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L1924:
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%r12, %rax
	je	.L1976
.L1788:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	jne	.L1785
	movdqu	16(%rax), %xmm2
	movups	%xmm2, 16(%rdx)
	jmp	.L1924
	.p2align 4,,10
	.p2align 3
.L1976:
	movq	-5120(%rbp), %r12
.L1784:
	testq	%r12, %r12
	je	.L1789
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1789:
	leaq	(%rbx,%r14), %r12
	addq	%rbx, %r15
	movq	%rbx, -5120(%rbp)
	movq	%r12, -5112(%rbp)
	movq	%r15, -5104(%rbp)
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1940:
	cmpb	$0, -5144(%rbp)
	movq	-5152(%rbp), %rdi
	je	.L1977
	subl	$2, -5140(%rbp)
	leaq	-5168(%rbp), %r12
.L1705:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$125, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L1978
.L1708:
	movl	$1, -5136(%rbp)
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1738
	.p2align 4,,10
	.p2align 3
.L1947:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	-5240(%rbp), %rdi
	leaq	-4528(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1977:
	leaq	-5168(%rbp), %r12
	movl	$1, %edx
	movb	$10, -5168(%rbp)
	movq	%r12, %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movl	-5140(%rbp), %eax
	movq	-5152(%rbp), %rdi
	subl	$2, %eax
	cmpb	$0, -5144(%rbp)
	movl	%eax, -5140(%rbp)
	jne	.L1705
	testl	%eax, %eax
	jle	.L1705
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L1707:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$32, -5168(%rbp)
	addl	$1, %ebx
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	cmpl	%ebx, -5140(%rbp)
	jg	.L1707
	jmp	.L1705
	.p2align 4,,10
	.p2align 3
.L1969:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	movb	$44, -5120(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1643
	.p2align 4,,10
	.p2align 3
.L1967:
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$44, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-5152(%rbp), %rdi
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1951:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1952:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1774:
	movq	-5240(%rbp), %rdi
	call	uv_mutex_lock@PLT
	movq	-5120(%rbp), %rbx
	movq	-5112(%rbp), %r12
	jmp	.L1794
.L1978:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r12, %rsi
	movb	$10, -5168(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1708
.L1836:
	movq	%rbx, %rax
	xorl	%ebx, %ebx
	jmp	.L1783
.L1941:
	movq	-5152(%rbp), %rdi
	movl	$1, %edx
	movq	%r14, %rsi
	movb	$44, -5201(%rbp)
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L1716
.L1757:
	movq	%rdx, -5312(%rbp)
	movl	$32, %esi
	movq	%r8, -5288(%rbp)
	call	*%rax
	movq	-4608(%rbp), %rax
	movq	-5312(%rbp), %rdx
	movq	-5288(%rbp), %r8
	jmp	.L1756
.L1972:
	movl	(%rbx), %ecx
	movl	%ecx, (%rax)
	movl	-4(%rbx,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L1634
.L1973:
	movzwl	-2(%rbx,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L1634
.L1957:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1964:
	call	__stack_chk_fail@PLT
.L1975:
	call	_ZSt16__throw_bad_castv@PLT
.L1955:
	leaq	.LC88(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1965:
	movq	-5152(%rbp), %r13
	movl	-5140(%rbp), %edx
	jmp	.L1803
.L1966:
	movq	-5248(%rbp), %rdi
	jmp	.L1797
	.cfi_endproc
.LFE7771:
	.size	_ZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEb, .-_ZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEb
	.p2align 4
	.globl	_ZN6report13GetNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_NS0_5LocalINS0_6ObjectEEERSo
	.type	_ZN6report13GetNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_NS0_5LocalINS0_6ObjectEEERSo, @function
_ZN6report13GetNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_NS0_5LocalINS0_6ObjectEEERSo:
.LFB7770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-48(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	pushq	$0
	pushq	%r8
	leaq	-64(%rbp), %r8
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	movb	$0, -48(%rbp)
	call	_ZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEb
	movq	-64(%rbp), %rdi
	popq	%rax
	popq	%rdx
	cmpq	%rbx, %rdi
	je	.L1979
	call	_ZdlPv@PLT
.L1979:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1983
	movq	-8(%rbp), %rbx
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1983:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7770:
	.size	_ZN6report13GetNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_NS0_5LocalINS0_6ObjectEEERSo, .-_ZN6report13GetNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_NS0_5LocalINS0_6ObjectEEERSo
	.section	.rodata.str1.1
.LC89:
	.string	"Worker thread subreport"
	.text
	.align 2
	.p2align 4
	.type	_ZZZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEbENKUlPNS3_6worker6WorkerEE_clESM_ENKUlS5_E_clES5_, @function
_ZZZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEbENKUlPNS3_6worker6WorkerEE_clESM_ENKUlS5_E_clES5_:
.LFB7773:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-432(%rbp), %r15
	leaq	-464(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-320(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-448(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$472, %rsp
	movq	%rsi, -512(%rbp)
	movq	.LC62(%rip), %xmm1
	movhps	.LC3(%rip), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm1, -480(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movw	%ax, -96(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r15, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-480(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	movq	%rax, -480(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -488(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	(%rbx), %rax
	movq	%r15, %r9
	movq	%r14, %r8
	movq	-512(%rbp), %r10
	leaq	.LC89(%rip), %rdx
	movq	(%rax), %rcx
	movq	%r12, -464(%rbp)
	movq	352(%r10), %rdi
	pushq	$0
	movq	%r10, %rsi
	pushq	$0
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	call	_ZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEb
	movq	-464(%rbp), %rdi
	popq	%rdx
	popq	%rcx
	cmpq	%r12, %rdi
	je	.L1985
	call	_ZdlPv@PLT
.L1985:
	movq	8(%rbx), %r15
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	-384(%rbp), %rax
	movq	%r12, -464(%rbp)
	movq	$0, -456(%rbp)
	movq	16(%rbx), %r9
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L1986
	movq	-400(%rbp), %r8
	movq	%r9, -512(%rbp)
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1997
	subq	%rcx, %r8
.L1996:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-512(%rbp), %r9
.L1988:
	movq	8(%r9), %rsi
	cmpq	16(%r9), %rsi
	je	.L1989
	leaq	16(%rsi), %rax
	movq	%rax, (%rsi)
	movq	-464(%rbp), %rax
	cmpq	%r12, %rax
	je	.L1998
	movq	%rax, (%rsi)
	movq	-448(%rbp), %rax
	movq	%rax, 16(%rsi)
.L1991:
	movq	-456(%rbp), %rax
	movq	%rax, 8(%rsi)
	movb	$0, -448(%rbp)
	addq	$32, 8(%r9)
.L1992:
	movq	.LC62(%rip), %xmm0
	movq	24(%rbx), %rdi
	movhps	.LC2(%rip), %xmm0
	movaps	%xmm0, -512(%rbp)
	call	uv_cond_signal@PLT
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	movdqa	-512(%rbp), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movaps	%xmm0, -432(%rbp)
	cmpq	-488(%rbp), %rdi
	je	.L1993
	call	_ZdlPv@PLT
.L1993:
	movq	-480(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r13, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rdx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1999
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1997:
	.cfi_restore_state
	subq	%rcx, %rax
	movq	%rax, %r8
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L1989:
	movq	%r9, %rdi
	movq	%r14, %rdx
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-464(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1992
	call	_ZdlPv@PLT
	jmp	.L1992
	.p2align 4,,10
	.p2align 3
.L1998:
	movdqa	-448(%rbp), %xmm2
	movups	%xmm2, 16(%rsi)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L1986:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r9, -512(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-512(%rbp), %r9
	jmp	.L1988
.L1999:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7773:
	.size	_ZZZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEbENKUlPNS3_6worker6WorkerEE_clESM_ENKUlS5_E_clES5_, .-_ZZZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEbENKUlPNS3_6worker6WorkerEE_clESM_ENKUlS5_E_clES5_
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_E4CallES2_:
.LFB10458:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZZZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEbENKUlPNS3_6worker6WorkerEE_clESM_ENKUlS5_E_clES5_
	.cfi_endproc
.LFE10458:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_E4CallES2_
	.section	.rodata.str1.1
.LC90:
	.string	"json"
.LC91:
	.string	"report"
.LC92:
	.string	"stdout"
.LC93:
	.string	"stderr"
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"\nFailed to open Node.js report file: "
	.section	.rodata.str1.1
.LC95:
	.string	" directory: "
.LC96:
	.string	" (errno: "
.LC97:
	.string	")"
.LC98:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"\nWriting Node.js report to file: "
	.section	.rodata.str1.1
.LC100:
	.string	"\nNode.js report completed"
	.text
	.p2align 4
	.globl	_ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE
	.type	_ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE, @function
_ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE:
.LFB7766:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-672(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$728, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -728(%rbp)
	movq	%rdx, -712(%rbp)
	movq	%rcx, -736(%rbp)
	movq	%r8, -744(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-656(%rbp), %rax
	cmpq	$0, 8(%r9)
	movq	$0, -664(%rbp)
	movq	%rax, -696(%rbp)
	movq	%rax, -672(%rbp)
	movb	$0, -656(%rbp)
	je	.L2002
	movq	%r9, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	leaq	-576(%rbp), %rax
	movq	%rax, -704(%rbp)
.L2003:
	leaq	-328(%rbp), %rbx
	leaq	-568(%rbp), %r14
	movq	%rbx, %rdi
	movq	%rbx, -720(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -328(%rbp)
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movaps	%xmm0, -96(%rbp)
	movq	-704(%rbp), %rdi
	movaps	%xmm0, -80(%rbp)
	movw	%si, -104(%rbp)
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	movq	%rax, -576(%rbp)
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -112(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	.LC92(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L2041
	leaq	.LC93(%rip), %rsi
	movq	%r13, %rdi
	leaq	-464(%rbp), %r15
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	leaq	_ZSt4cerr(%rip), %r9
	testl	%eax, %eax
	jne	.L2058
.L2009:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	movq	%r9, -752(%rbp)
	call	uv_mutex_lock@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	movzbl	308(%rax), %ebx
	call	uv_mutex_unlock@PLT
	movq	-744(%rbp), %rcx
	movq	%r13, %r8
	movq	-736(%rbp), %rdx
	movq	-752(%rbp), %r9
	movq	-712(%rbp), %rsi
	pushq	%rbx
	pushq	16(%rbp)
	movq	-728(%rbp), %rdi
	call	_ZN6reportL15WriteNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS0_5LocalINS0_6ObjectEEEb
	movq	%r15, %rdi
	call	_ZNKSt12__basic_fileIcE7is_openEv@PLT
	popq	%rdx
	popq	%rcx
	testb	%al, %al
	jne	.L2059
.L2031:
	leaq	.LC93(%rip), %rsi
	movq	%r13, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L2060
.L2033:
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	-672(%rbp), %rax
	cmpq	-696(%rbp), %rax
	je	.L2061
	movq	%rax, (%r12)
	movq	-656(%rbp), %rax
	movq	%rax, 16(%r12)
.L2038:
	movq	-664(%rbp), %rax
	movb	$0, -656(%rbp)
	movq	$0, -664(%rbp)
	movq	%rax, 8(%r12)
	movq	-696(%rbp), %rax
	movq	%rax, -672(%rbp)
.L2028:
	leaq	64+_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r14, %rdi
	movq	.LC101(%rip), %xmm0
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -576(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	movq	%r15, %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-512(%rbp), %rdi
	movq	%rax, -568(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTSt14basic_ofstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	-720(%rbp), %rdi
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -328(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-672(%rbp), %rdi
	cmpq	-696(%rbp), %rdi
	je	.L2001
	call	_ZdlPv@PLT
.L2001:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2062
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2041:
	.cfi_restore_state
	leaq	_ZSt4cout(%rip), %r9
	leaq	-464(%rbp), %r15
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2002:
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	leaq	-592(%rbp), %rbx
	movq	$0, -600(%rbp)
	movq	%rbx, -608(%rbp)
	leaq	-608(%rbp), %r14
	movb	$0, -592(%rbp)
	call	uv_mutex_lock@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	%r14, %rdi
	leaq	344(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-600(%rbp), %rsi
	testq	%rsi, %rsi
	jne	.L2063
	movq	-712(%rbp), %rax
	testq	%rax, %rax
	je	.L2006
	movq	1936(%rax), %rsi
.L2006:
	leaq	-576(%rbp), %rax
	leaq	.LC90(%rip), %rcx
	leaq	.LC91(%rip), %rdx
	movq	%rax, %rdi
	movq	%rax, -704(%rbp)
	call	_ZN4node18DiagnosticFilename12MakeFilenameB5cxx11EmPKcS2_@PLT
	movq	-576(%rbp), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%r14, %rcx
	xorl	%esi, %esi
	movq	-664(%rbp), %rdx
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-576(%rbp), %rdi
	leaq	-560(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2005
	call	_ZdlPv@PLT
.L2005:
	movq	-608(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2003
	call	_ZdlPv@PLT
	jmp	.L2003
	.p2align 4,,10
	.p2align 3
.L2059:
	movq	%r14, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	testq	%rax, %rax
	jne	.L2031
	movq	-576(%rbp), %rax
	movq	-704(%rbp), %rdi
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2031
	.p2align 4,,10
	.p2align 3
.L2060:
	movl	$25, %edx
	leaq	.LC100(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	_ZSt4cerr(%rip), %rax
	leaq	_ZSt4cerr(%rip), %rdx
	movq	-24(%rax), %rax
	movq	240(%rdx,%rax), %r13
	testq	%r13, %r13
	je	.L2034
	cmpb	$0, 56(%r13)
	je	.L2035
	movsbl	67(%r13), %esi
.L2036:
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	jmp	.L2033
	.p2align 4,,10
	.p2align 3
.L2061:
	movdqa	-656(%rbp), %xmm2
	movups	%xmm2, 16(%r12)
	jmp	.L2038
	.p2align 4,,10
	.p2align 3
.L2058:
	leaq	-624(%rbp), %rax
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	movq	$0, -632(%rbp)
	movq	%rax, -752(%rbp)
	leaq	-640(%rbp), %r15
	movq	%rax, -640(%rbp)
	movb	$0, -624(%rbp)
	call	uv_mutex_lock@PLT
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movq	%r15, %rdi
	leaq	312(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	leaq	_ZN4node11per_process17cli_options_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-632(%rbp), %r15
	testq	%r15, %r15
	je	.L2010
	movq	-640(%rbp), %r8
	leaq	-592(%rbp), %rbx
	leaq	-608(%rbp), %r9
	movq	%rbx, -608(%rbp)
	testq	%r8, %r8
	je	.L2064
	movq	%r15, -680(%rbp)
	cmpq	$15, %r15
	ja	.L2065
	cmpq	$1, %r15
	jne	.L2043
	movzbl	(%r8), %eax
	movb	%al, -592(%rbp)
	movq	%rbx, %rax
.L2014:
	movq	%r15, -600(%rbp)
	movb	$0, (%rax,%r15)
	movq	-608(%rbp), %rdx
	movl	$15, %eax
	movq	-600(%rbp), %r15
	cmpq	%rbx, %rdx
	cmovne	-592(%rbp), %rax
	leaq	1(%r15), %r10
	cmpq	%rax, %r10
	ja	.L2066
.L2016:
	movb	$47, (%rdx,%r15)
	movq	-608(%rbp), %rax
	movq	%r9, %rdi
	movq	%r10, -600(%rbp)
	movb	$0, 1(%rax,%r15)
	movq	-664(%rbp), %rdx
	movq	-672(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-608(%rbp), %rsi
	movq	%r14, %rdi
	movl	$20, %edx
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	-704(%rbp), %rdi
	testq	%rax, %rax
	movq	-576(%rbp), %rax
	je	.L2067
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L2018:
	movq	-608(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2020
	call	_ZdlPv@PLT
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2063:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	leaq	-576(%rbp), %rax
	movq	%rax, -704(%rbp)
	jmp	.L2005
	.p2align 4,,10
	.p2align 3
.L2035:
	movq	%r13, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	0(%r13), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2036
	movq	%r13, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2036
	.p2align 4,,10
	.p2align 3
.L2010:
	movq	-672(%rbp), %rsi
	movq	%r14, %rdi
	movl	$20, %edx
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	-704(%rbp), %rdi
	testq	%rax, %rax
	movq	-576(%rbp), %rax
	je	.L2068
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
.L2020:
	leaq	-464(%rbp), %r15
	movq	%r15, %rdi
	call	_ZNKSt12__basic_fileIcE7is_openEv@PLT
	testb	%al, %al
	jne	.L2022
	movl	$37, %edx
	leaq	.LC94(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-664(%rbp), %rdx
	movq	-672(%rbp), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	cmpq	$0, -632(%rbp)
	jne	.L2069
.L2023:
	movl	$9, %edx
	leaq	.LC96(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	call	__errno_location@PLT
	leaq	_ZSt4cerr(%rip), %rdi
	movl	(%rax), %esi
	call	_ZNSolsEi@PLT
	movl	$1, %edx
	leaq	.LC97(%rip), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	0(%r13), %rax
	movq	-24(%rax), %rax
	movq	240(%r13,%rax), %rbx
	testq	%rbx, %rbx
	je	.L2034
	cmpb	$0, 56(%rbx)
	je	.L2025
	movsbl	67(%rbx), %esi
.L2026:
	movq	%r13, %rdi
	call	_ZNSo3putEc@PLT
	movq	%rax, %rdi
	call	_ZNSo5flushEv@PLT
	leaq	16(%r12), %rax
	leaq	.LC98(%rip), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	%rdx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-640(%rbp), %rdi
	cmpq	-752(%rbp), %rdi
	je	.L2028
	call	_ZdlPv@PLT
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2022:
	movl	$33, %edx
	leaq	.LC99(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-664(%rbp), %rdx
	movq	-672(%rbp), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-640(%rbp), %rdi
	cmpq	-752(%rbp), %rdi
	je	.L2029
	call	_ZdlPv@PLT
.L2029:
	movq	-704(%rbp), %r9
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2025:
	movq	%rbx, %rdi
	call	_ZNKSt5ctypeIcE13_M_widen_initEv@PLT
	movq	(%rbx), %rax
	movl	$10, %esi
	leaq	_ZNKSt5ctypeIcE8do_widenEc(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2026
	movq	%rbx, %rdi
	call	*%rax
	movsbl	%al, %esi
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	%rbx, %rdi
.L2013:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r9, -760(%rbp)
	call	memcpy@PLT
	movq	-680(%rbp), %r15
	movq	-608(%rbp), %rax
	movq	-760(%rbp), %r9
	jmp	.L2014
	.p2align 4,,10
	.p2align 3
.L2066:
	xorl	%edx, %edx
	movq	%r9, %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%r10, -768(%rbp)
	movq	%r9, -760(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-608(%rbp), %rdx
	movq	-768(%rbp), %r10
	movq	-760(%rbp), %r9
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2069:
	movl	$12, %edx
	leaq	.LC95(%rip), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-632(%rbp), %rdx
	movq	-640(%rbp), %rsi
	leaq	_ZSt4cerr(%rip), %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	jmp	.L2023
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	%r9, %rdi
	leaq	-680(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -768(%rbp)
	movq	%r9, -760(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-760(%rbp), %r9
	movq	-768(%rbp), %r8
	movq	%rax, -608(%rbp)
	movq	%rax, %rdi
	movq	-680(%rbp), %rax
	movq	%rax, -592(%rbp)
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L2068:
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2067:
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L2018
.L2062:
	call	__stack_chk_fail@PLT
.L2034:
	call	_ZSt16__throw_bad_castv@PLT
.L2064:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE7766:
	.size	_ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE, .-_ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE, @function
_GLOBAL__sub_I__ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE:
.LFB10464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10464:
	.size	_GLOBAL__sub_I__ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE, .-_GLOBAL__sub_I__ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN6report17TriggerNodeReportEPN2v87IsolateEPN4node11EnvironmentEPKcS7_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEENS0_5LocalINS0_6ObjectEEE
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZN6reportL15WriteNodeReportEPN2v87IsolateES2_PKcSA_RKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERSoNS6_5LocalINS6_6ObjectEEEbENKUlPNS_6worker6WorkerEE_clESP_EUlS2_E_E4CallES2_
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC102:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC104:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC105:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC107:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.section	.rodata.str1.1
.LC108:
	.string	"core_file_size_blocks"
.LC109:
	.string	"data_seg_size_kbytes"
.LC110:
	.string	"file_size_blocks"
.LC111:
	.string	"max_locked_memory_bytes"
.LC112:
	.string	"max_memory_size_kbytes"
.LC113:
	.string	"open_files"
.LC114:
	.string	"stack_size_bytes"
.LC115:
	.string	"cpu_time_seconds"
.LC116:
	.string	"max_user_processes"
.LC117:
	.string	"virtual_memory_kbytes"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZZN6reportL22PrintSystemInformationEPN4node10JSONWriterEE14rlimit_strings, @object
	.size	_ZZN6reportL22PrintSystemInformationEPN4node10JSONWriterEE14rlimit_strings, 160
_ZZN6reportL22PrintSystemInformationEPN4node10JSONWriterEE14rlimit_strings:
	.quad	.LC108
	.long	4
	.zero	4
	.quad	.LC109
	.long	2
	.zero	4
	.quad	.LC110
	.long	1
	.zero	4
	.quad	.LC111
	.long	8
	.zero	4
	.quad	.LC112
	.long	5
	.zero	4
	.quad	.LC113
	.long	7
	.zero	4
	.quad	.LC114
	.long	3
	.zero	4
	.quad	.LC115
	.long	0
	.zero	4
	.quad	.LC116
	.long	6
	.zero	4
	.quad	.LC117
	.long	9
	.zero	4
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.data.rel.ro,"aw"
	.align 8
.LC1:
	.quad	_ZTVNSt7__cxx1118basic_stringstreamIcSt11char_traitsIcESaIcEEE+64
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.align 8
.LC3:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.quad	7885362534758641253
	.quad	7019267256621493861
	.align 16
.LC6:
	.quad	7885595605319577460
	.quad	7299600957945702765
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	2696277389
	.long	1051772663
	.align 8
.LC10:
	.long	0
	.long	1079574528
	.section	.data.rel.ro
	.align 8
.LC62:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC101:
	.quad	_ZTVSt14basic_ofstreamIcSt11char_traitsIcEE+24
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
