	.file	"node_trace_events.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4286:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4288:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7098:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7145:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7145:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7146:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7146:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7147:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7147:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7149:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L9
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7149:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZNK4node15NodeCategorySet8SelfSizeEv,"axG",@progbits,_ZNK4node15NodeCategorySet8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15NodeCategorySet8SelfSizeEv
	.type	_ZNK4node15NodeCategorySet8SelfSizeEv, @function
_ZNK4node15NodeCategorySet8SelfSizeEv:
.LFB7602:
	.cfi_startproc
	endbr64
	movl	$88, %eax
	ret
	.cfi_endproc
.LFE7602:
	.size	_ZNK4node15NodeCategorySet8SelfSizeEv, .-_ZNK4node15NodeCategorySet8SelfSizeEv
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10288:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L11
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ret
	.cfi_endproc
.LFE10288:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10290:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L14
	call	_ZdlPv@PLT
.L14:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10290:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev:
.LFB7601:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$2037542759, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7310575037220941646, %rcx
	movq	%rdx, (%rdi)
	movl	$25939, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$116, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE7601:
	.size	_ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15NodeCategorySet7DisableERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node15NodeCategorySet7DisableERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node15NodeCategorySet7DisableERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7630:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L32
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L24
	cmpw	$1040, %cx
	jne	.L19
.L24:
	movq	23(%rdx), %rbx
.L21:
	testq	%rbx, %rbx
	je	.L17
	cmpb	$0, 32(%rbx)
	je	.L17
	cmpq	$0, 80(%rbx)
	je	.L17
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L23
	movl	24+_ZN4node11per_process11v8_platformE(%rip), %esi
	leaq	40(%rbx), %rdx
	call	_ZN4node7tracing5Agent7DisableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE@PLT
.L23:
	movb	$0, 32(%rbx)
.L17:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7630:
	.size	_ZN4node15NodeCategorySet7DisableERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node15NodeCategorySet7DisableERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L44
	cmpw	$1040, %cx
	jne	.L34
.L44:
	movq	23(%rdx), %r12
.L36:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L37
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L52
.L39:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L53
	movq	8(%rbx), %r13
.L41:
	movq	3032(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L42
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3032(%r12)
.L42:
	testq	%r13, %r13
	je	.L33
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3032(%r12)
.L33:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L37:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L39
.L52:
	leaq	_ZZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L36
	.cfi_endproc
.LFE7632:
	.size	_ZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node20GetEnabledCategoriesERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node20GetEnabledCategoriesERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node20GetEnabledCategoriesERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L62
	cmpw	$1040, %cx
	jne	.L55
.L62:
	movq	23(%rdx), %r12
.L57:
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZNK4node7tracing5Agent20GetEnabledCategoriesB5cxx11Ev@PLT
	movq	-56(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L67
.L58:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L54
	call	_ZdlPv@PLT
.L54:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	352(%r12), %rdi
	movq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	(%rbx), %rbx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L69
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L69:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L58
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7631:
	.size	_ZN4node20GetEnabledCategoriesERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node20GetEnabledCategoriesERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata._ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"categories"
	.section	.rodata._ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"cannot create std::deque larger than max_size()"
	.section	.rodata._ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE.str1.1
.LC3:
	.string	"std::basic_string"
	.section	.text._ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	48(%rdi), %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	64(%rdi), %r12
	je	.L70
	movq	64(%rsi), %rax
	movq	%rdi, %rbx
	movq	%rsi, %r14
	cmpq	32(%rsi), %rax
	je	.L72
	cmpq	72(%rsi), %rax
	je	.L114
.L73:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L72
	subq	$48, 64(%rax)
.L72:
	movl	$72, %edi
	leaq	-64(%rbp), %r13
	call	_Znwm@PLT
	movl	$10, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC1(%rip), %rcx
	movq	%rax, (%r15)
	leaq	48(%r15), %rax
	leaq	32(%r15), %rdi
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r14), %rdi
	movb	$0, 24(%r15)
	movq	%r13, %rsi
	movq	$48, 64(%r15)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	movq	(%rdi), %rax
	call	*8(%rax)
.L74:
	movq	64(%r14), %rdi
	cmpq	32(%r14), %rdi
	je	.L75
	movq	%rdi, %rax
	cmpq	72(%r14), %rdi
	je	.L115
.L76:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L75
	movq	8(%r14), %rdi
	leaq	.LC1(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%r14), %rdi
.L75:
	movq	80(%r14), %rax
	subq	$8, %rax
	cmpq	%rax, %rdi
	je	.L77
	movq	%r15, (%rdi)
	addq	$8, %rdi
	movq	%rdi, 64(%r14)
.L78:
	movq	64(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L92
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L88
	movq	8(%r14), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L88:
	movq	%rbx, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %rbx
	cmpq	%rax, %r12
	je	.L116
.L92:
	movq	40(%rbx), %r11
	testq	%r11, %r11
	movq	%r11, -72(%rbp)
	je	.L88
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$17, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC3(%rip), %rcx
	movq	%rax, (%r15)
	leaq	48(%r15), %rax
	leaq	32(%r15), %rdi
	movq	$0, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r14), %rdi
	movq	-72(%rbp), %r11
	movq	%r13, %rsi
	movb	$0, 24(%r15)
	movq	(%rdi), %rax
	movq	%r11, 64(%r15)
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L89
	movq	(%rdi), %rax
	call	*8(%rax)
.L89:
	movq	64(%r14), %rax
	cmpq	32(%r14), %rax
	je	.L88
	cmpq	72(%r14), %rax
	jne	.L91
	movq	88(%r14), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L116:
	movq	64(%r14), %rdi
.L86:
	cmpq	72(%r14), %rdi
	je	.L93
	subq	$8, %rdi
	movq	%rdi, 64(%r14)
.L70:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L117
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	88(%rsi), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L115:
	movq	88(%r14), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L77:
	movq	88(%r14), %r9
	movq	56(%r14), %rsi
	subq	72(%r14), %rdi
	movq	%r9, %rcx
	sarq	$3, %rdi
	subq	%rsi, %rcx
	movq	%rcx, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rax
	salq	$6, %rax
	addq	%rax, %rdi
	movq	48(%r14), %rax
	subq	32(%r14), %rax
	sarq	$3, %rax
	addq	%rdi, %rax
	movabsq	$1152921504606846975, %rdi
	cmpq	%rdi, %rax
	je	.L118
	movq	16(%r14), %r8
	movq	24(%r14), %rax
	movq	%r9, %r11
	subq	%r8, %r11
	movq	%rax, %r10
	sarq	$3, %r11
	subq	%r11, %r10
	cmpq	$1, %r10
	jbe	.L119
.L80:
	movl	$512, %edi
	movq	%r9, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r9
	movq	%rax, 8(%r9)
	movq	64(%r14), %rax
	movq	%r15, (%rax)
	movq	88(%r14), %rax
	movq	8(%rax), %rdi
	addq	$8, %rax
	movq	%rax, %xmm1
	movq	%rdi, %xmm0
	leaq	512(%rdi), %rcx
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 64(%r14)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%r14)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L93:
	call	_ZdlPv@PLT
	movq	88(%r14), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm3
	leaq	504(%rax), %rcx
	movq	%rax, %xmm2
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r14)
	movq	%rax, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 80(%r14)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L119:
	addq	$2, %rdx
	leaq	(%rdx,%rdx), %r11
	cmpq	%r11, %rax
	jbe	.L81
	subq	%rdx, %rax
	shrq	%rax
	leaq	(%r8,%rax,8), %r8
	leaq	8(%r9), %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%r8, %rsi
	jbe	.L82
	cmpq	%rax, %rsi
	je	.L83
	movq	%r8, %rdi
	movq	%rcx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rcx
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%r8), %rax
	movq	(%r8), %xmm0
	leaq	(%r8,%rcx), %r9
	movq	%r8, 56(%r14)
	movq	%r9, 88(%r14)
	addq	$512, %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 40(%r14)
	movq	(%r9), %rax
	movq	%rax, 72(%r14)
	addq	$512, %rax
	movq	%rax, 80(%r14)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L81:
	testq	%rax, %rax
	movl	$1, %esi
	cmovne	%rax, %rsi
	leaq	2(%rax,%rsi), %rax
	movq	%rax, -72(%rbp)
	cmpq	%rdi, %rax
	ja	.L120
	leaq	0(,%rax,8), %rdi
	movq	%rdx, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdx
	movq	56(%r14), %rsi
	movq	%rax, %rcx
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	subq	%rdx, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r8
	movq	88(%r14), %rax
	movq	-88(%rbp), %rcx
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L85
	movq	%r8, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	-88(%rbp), %rcx
	movq	%rax, %r8
.L85:
	movq	16(%r14), %rdi
	movq	%r8, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rax
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rcx
	movq	%rax, 16(%r14)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%r14)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L82:
	cmpq	%rax, %rsi
	je	.L83
	leaq	8(%rcx), %rdi
	movq	%rcx, -80(%rbp)
	subq	%rdx, %rdi
	movq	%r8, -72(%rbp)
	addq	%r8, %rdi
	call	memmove@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	jmp	.L83
.L117:
	call	__stack_chk_fail@PLT
.L118:
	leaq	.LC2(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L120:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7600:
	.size	_ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"getEnabledCategories"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"setTraceCategoryStateUpdateHandler"
	.section	.rodata.str1.1
.LC6:
	.string	"enable"
.LC7:
	.string	"disable"
.LC8:
	.string	"CategorySet"
.LC9:
	.string	"isTraceCategoryEnabled"
.LC10:
	.string	"trace"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB11:
	.text
.LHOTB11:
	.align 2
	.p2align 4
	.globl	_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L122
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L122
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L122
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node20GetEnabledCategoriesERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L144
.L123:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L145
.L124:
	movq	-56(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L146
.L125:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L147
.L126:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L148
.L127:
	movq	%r8, %rdx
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L149
.L128:
	movq	%r8, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node15NodeCategorySet6EnableERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L150
.L129:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node15NodeCategorySet7DisableERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L151
.L130:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L152
.L131:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L153
.L132:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L154
.L133:
	movq	352(%rbx), %rdi
	movl	$22, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L155
.L134:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L156
.L135:
	movq	%r12, %rdi
	call	_ZN2v87Context22GetExtrasBindingObjectEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L157
.L136:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L158
.L137:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L159
.L138:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L160
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L145:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L146:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L147:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L149:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L152:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L153:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L154:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L155:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L156:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L158:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7633:
.L122:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7633:
	.text
	.size	_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE11:
	.text
.LHOTE11:
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L162
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L162:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L172
.L163:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L164
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L163
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L174
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L175:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L174
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L174
	movq	%r9, %rdi
.L177:
	cmpq	%rsi, %rbx
	jne	.L175
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L175
	cmpq	16(%rdi), %rbx
	jne	.L175
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L209
	testq	%rsi, %rsi
	je	.L179
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L179
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L179:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L174:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L210
.L182:
	cmpq	$0, 8(%rbx)
	je	.L173
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L186
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L211
.L186:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L173
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L173:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L191
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L179
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L178:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L213
.L180:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L211:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%r10, %rax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L210:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L214
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L182
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%rsi, 2600(%r12)
	jmp	.L180
.L214:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7084:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7086:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.text
	.p2align 4
	.globl	_Z22_register_trace_eventsv
	.type	_Z22_register_trace_eventsv, @function
_Z22_register_trace_eventsv:
.LFB7634:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7634:
	.size	_Z22_register_trace_eventsv, .-_Z22_register_trace_eventsv
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8901:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L233
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L222:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L220
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L218
.L221:
	movq	%rbx, %r12
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L221
.L218:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8901:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZN4node15NodeCategorySetD2Ev,"axG",@progbits,_ZN4node15NodeCategorySetD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15NodeCategorySetD2Ev
	.type	_ZN4node15NodeCategorySetD2Ev, @function
_ZN4node15NodeCategorySetD2Ev:
.LFB10280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15NodeCategorySetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	56(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L237
	leaq	40(%rdi), %r14
.L240:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L238
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L237
.L239:
	movq	%rbx, %r12
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L239
.L237:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE10280:
	.size	_ZN4node15NodeCategorySetD2Ev, .-_ZN4node15NodeCategorySetD2Ev
	.weak	_ZN4node15NodeCategorySetD1Ev
	.set	_ZN4node15NodeCategorySetD1Ev,_ZN4node15NodeCategorySetD2Ev
	.section	.text._ZN4node15NodeCategorySetD0Ev,"axG",@progbits,_ZN4node15NodeCategorySetD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15NodeCategorySetD0Ev
	.type	_ZN4node15NodeCategorySetD0Ev, @function
_ZN4node15NodeCategorySetD0Ev:
.LFB10282:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15NodeCategorySetE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	56(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L252
	leaq	40(%rdi), %r14
.L255:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L253
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L252
.L254:
	movq	%rbx, %r12
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L254
.L252:
	movq	%r13, %rdi
	call	_ZN4node10BaseObjectD2Ev
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$88, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10282:
	.size	_ZN4node15NodeCategorySetD0Ev, .-_ZN4node15NodeCategorySetD0Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_:
.LFB9421:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L292
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L274:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L270
.L293:
	movq	%rax, %r15
.L269:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L271
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L272
.L271:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L273
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L274
.L272:
	testl	%eax, %eax
	js	.L274
.L273:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L293
.L270:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L268
.L276:
	testq	%rdx, %rdx
	je	.L279
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L280
.L279:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L281
	cmpq	$-2147483648, %rcx
	jl	.L282
	movl	%ecx, %eax
.L280:
	testl	%eax, %eax
	js	.L282
.L281:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L268:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L294
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L294:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9421:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.section	.rodata._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC12:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_:
.LFB8942:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	0(%r13), %r8
	leaq	48(%rax), %r14
	movq	%r14, 32(%rax)
	testq	%r8, %r8
	je	.L317
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	movq	%rax, %r12
	leaq	32(%rax), %r15
	call	strlen@PLT
	movq	-72(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -64(%rbp)
	movq	%rax, %r13
	ja	.L318
	cmpq	$1, %rax
	jne	.L299
	movzbl	(%r8), %edx
	movb	%dl, 48(%r12)
	movq	%r14, %rdx
.L300:
	movq	%rax, 40(%r12)
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movb	$0, (%rdx,%rax)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	movq	%rax, %r13
	movq	%rdx, %r15
	testq	%rdx, %rdx
	je	.L301
	leaq	8(%rbx), %rcx
	movl	$1, %edi
	testq	%rax, %rax
	je	.L319
.L302:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%rbx)
	movq	%r12, %rax
	movl	$1, %edx
.L305:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L320
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L321
	movq	%r14, %rdx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%r15, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L298:
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %rax
	movq	32(%r12), %rdx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	.LC12(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	movq	32(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L306
	call	_ZdlPv@PLT
.L306:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%r13, %rax
	xorl	%edx, %edx
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L319:
	cmpq	%rcx, %rdx
	je	.L302
	movq	40(%r12), %r13
	movq	40(%rdx), %r14
	cmpq	%r14, %r13
	movq	%r14, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L303
	movq	32(%r12), %rdi
	movq	32(%r15), %rsi
	movq	%rcx, -72(%rbp)
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %edi
	jne	.L304
.L303:
	subq	%r14, %r13
	xorl	%edi, %edi
	cmpq	$2147483647, %r13
	jg	.L302
	cmpq	$-2147483648, %r13
	jl	.L312
	movl	%r13d, %edi
.L304:
	shrl	$31, %edi
	jmp	.L302
.L312:
	movl	$1, %edi
	jmp	.L302
.L320:
	call	__stack_chk_fail@PLT
.L321:
	movq	%r14, %rdi
	jmp	.L298
	.cfi_endproc
.LFE8942:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%rdi, -1176(%rbp)
	movq	32(%rsi), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L371
	cmpw	$1040, %cx
	jne	.L323
.L371:
	movq	23(%rdx), %r12
.L325:
	leaq	-1144(%rbp), %rax
	movl	$0, -1144(%rbp)
	movq	%rax, -1184(%rbp)
	movq	%rax, -1128(%rbp)
	movq	%rax, -1120(%rbp)
	movq	-1176(%rbp), %rax
	movq	$0, -1136(%rbp)
	movq	$0, -1112(%rbp)
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L326
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L419
.L328:
	movq	-1176(%rbp), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jle	.L420
	movq	8(%rax), %r13
.L330:
	xorl	%ebx, %ebx
	leaq	-1104(%rbp), %r14
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L416:
	movq	352(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %rax
	testq	%rax, %rax
	je	.L366
	leaq	-1152(%rbp), %r15
	leaq	-1160(%rbp), %rsi
	movq	%rax, -1160(%rbp)
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE17_M_emplace_uniqueIJPcEEESt4pairISt17_Rb_tree_iteratorIS5_EbEDpOT_
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L334
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L334
	call	free@PLT
.L334:
	addq	$1, %rbx
.L335:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %eax
	cmpq	%rbx, %rax
	jbe	.L331
	movq	3280(%r12), %rsi
	movl	%ebx, %edx
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L416
.L366:
	movq	-1136(%rbp), %r12
	leaq	-1152(%rbp), %r15
	testq	%r12, %r12
	je	.L322
.L336:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L337
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L322
.L339:
	movq	%rbx, %r12
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L326:
	movq	8(%rax), %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L328
.L419:
	leaq	_ZZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L420:
	movq	(%rax), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L339
.L322:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L421
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	-1176(%rbp), %rax
	movl	$88, %edi
	movq	8(%rax), %r14
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%rax, %r13
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	addq	$8, %r14
	movq	%rax, 0(%r13)
	movq	%r14, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm1
	movq	$0, 24(%r13)
	movq	%r14, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r13)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L422
	movq	%r13, %rdx
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%r12), %rbx
	movl	$40, %edi
	leaq	1(%rbx), %rax
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r10
	xorl	%edx, %edx
	movq	%rbx, 24(%rax)
	movq	%rax, %r14
	movq	%r10, 8(%rax)
	movq	%r13, 16(%rax)
	movq	2592(%r12), %r8
	movq	$0, (%rax)
	movq	%r13, %rax
	divq	%r8
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %rbx
	testq	%rdi, %rdi
	je	.L341
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L342:
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L341
	movq	32(%rsi), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L341
	movq	%rsi, %rax
.L344:
	cmpq	%r13, %rcx
	jne	.L342
	cmpq	%r10, 8(%rax)
	jne	.L342
	cmpq	16(%rax), %rcx
	jne	.L342
	cmpq	$0, (%rdi)
	je	.L341
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L341:
	movq	2608(%r12), %rdx
	leaq	2616(%r12), %rdi
	movl	$1, %ecx
	movq	%r8, %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r15
	testb	%al, %al
	jne	.L345
	movq	2584(%r12), %r8
.L346:
	addq	%r8, %rbx
	movq	%r13, 32(%r14)
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L355
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	(%rbx), %rax
	movq	%r14, (%rax)
.L356:
	leaq	16+_ZTVN4node15NodeCategorySetE(%rip), %rax
	leaq	48(%r13), %rdx
	addq	$1, 2608(%r12)
	addq	$1, 2656(%r12)
	movq	%rax, 0(%r13)
	movq	-1136(%rbp), %rax
	movb	$0, 32(%r13)
	testq	%rax, %rax
	je	.L358
	movl	-1144(%rbp), %ecx
	movq	%rax, 56(%r13)
	movl	%ecx, 48(%r13)
	movq	-1128(%rbp), %rcx
	movq	%rcx, 64(%r13)
	movq	-1120(%rbp), %rcx
	movq	%rcx, 72(%r13)
	movq	%rdx, 8(%rax)
	movq	-1112(%rbp), %rax
	movq	$0, -1136(%rbp)
	movq	%rax, 80(%r13)
	movq	-1184(%rbp), %rax
	movq	$0, -1112(%rbp)
	movq	%rax, -1128(%rbp)
	movq	%rax, -1120(%rbp)
.L359:
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L362
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L361
.L362:
	movq	8(%r13), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L361:
	movq	-1136(%rbp), %r12
	testq	%r12, %r12
	je	.L322
	leaq	-1152(%rbp), %r15
.L365:
	movq	24(%r12), %rsi
	movq	%r15, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L363
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L322
.L364:
	movq	%rbx, %r12
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L364
	jmp	.L322
.L358:
	movl	$0, 48(%r13)
	movq	$0, 56(%r13)
	movq	%rdx, 64(%r13)
	movq	%rdx, 72(%r13)
	movq	$0, 80(%r13)
	jmp	.L359
.L345:
	cmpq	$1, %rdx
	je	.L423
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L424
	leaq	0(,%rdx,8), %rbx
	movq	%rbx, %rdi
	call	_Znwm@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%r12), %r10
	movq	%rax, %r8
.L348:
	movq	2600(%r12), %rsi
	movq	$0, 2600(%r12)
	testq	%rsi, %rsi
	je	.L350
	xorl	%edi, %edi
	leaq	2600(%r12), %r9
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L353:
	testq	%rsi, %rsi
	je	.L350
.L351:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r15
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L352
	movq	2600(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L370
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L353
.L350:
	movq	2584(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L354
	movq	%r8, -1176(%rbp)
	call	_ZdlPv@PLT
	movq	-1176(%rbp), %r8
.L354:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r15, 2592(%r12)
	divq	%r15
	movq	%r8, 2584(%r12)
	leaq	0(,%rdx,8), %rbx
	jmp	.L346
.L355:
	movq	2600(%r12), %rax
	movq	%rax, (%r14)
	movq	%r14, 2600(%r12)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L357
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r14, (%r8,%rdx,8)
.L357:
	leaq	2600(%r12), %rax
	movq	%rax, (%rbx)
	jmp	.L356
.L370:
	movq	%rdx, %rdi
	jmp	.L353
.L422:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L423:
	leaq	2632(%r12), %r8
	movq	$0, 2632(%r12)
	movq	%r8, %r10
	jmp	.L348
.L424:
	call	_ZSt17__throw_bad_allocv@PLT
.L421:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7619:
	.size	_ZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_:
.LFB9774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L469
	movq	8(%rdx), %r14
	movq	40(%rsi), %r15
	movq	%rsi, %rbx
	movq	32(%rsi), %r9
	movq	0(%r13), %r8
	cmpq	%r15, %r14
	movq	%r15, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L432
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	movq	-72(%rbp), %rdx
	jne	.L470
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$2147483647, %rax
	jg	.L451
.L452:
	cmpq	$-2147483648, %rax
	jl	.L435
	testl	%eax, %eax
	js	.L435
	testq	%rdx, %rdx
	je	.L442
.L451:
	movq	%r8, %rsi
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L443
.L442:
	subq	%r14, %r15
	cmpq	$2147483647, %r15
	jg	.L444
	cmpq	$-2147483648, %r15
	jl	.L445
	movl	%r15d, %eax
.L443:
	testl	%eax, %eax
	js	.L445
.L444:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L461:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	.cfi_restore_state
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$2147483647, %rax
	jle	.L452
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L470:
	jns	.L451
.L435:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%r12)
	je	.L461
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %rcx
	movq	%rax, %r15
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L438
	movq	-56(%rbp), %r8
	movq	32(%rax), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, %rsi
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	jne	.L439
.L438:
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L427
	cmpq	$-2147483648, %rcx
	jl	.L440
	movl	%ecx, %eax
.L439:
	testl	%eax, %eax
	jns	.L427
.L440:
	cmpq	$0, 24(%r15)
	movl	$0, %eax
	cmovne	%rbx, %rax
	cmove	%r15, %rbx
	addq	$40, %rsp
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L427
	movq	32(%rdi), %rbx
	movq	8(%rdx), %r15
	movq	40(%rbx), %r14
	movq	%r15, %rdx
	cmpq	%r15, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L428
	movq	32(%rbx), %rdi
	movq	0(%r13), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L429
.L428:
	subq	%r15, %r14
	cmpq	$2147483647, %r14
	jg	.L427
	cmpq	$-2147483648, %r14
	jl	.L468
	movl	%r14d, %eax
.L429:
	testl	%eax, %eax
	jns	.L427
.L468:
	addq	$40, %rsp
	movq	%rbx, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE24_M_get_insert_unique_posERKS5_
	.p2align 4,,10
	.p2align 3
.L445:
	.cfi_restore_state
	cmpq	%rbx, 32(%r12)
	je	.L468
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %rcx
	movq	%rax, %r15
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L447
	movq	-56(%rbp), %r8
	movq	32(%rax), %rsi
	movq	%rcx, -64(%rbp)
	movq	%r8, %rdi
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	jne	.L448
.L447:
	subq	%rcx, %r14
	cmpq	$2147483647, %r14
	jg	.L427
	cmpq	$-2147483648, %r14
	jl	.L449
	movl	%r14d, %eax
.L448:
	testl	%eax, %eax
	jns	.L427
.L449:
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	cmovne	%r15, %rbx
	cmovne	%r15, %rax
	movq	%rbx, %rdx
	jmp	.L461
	.cfi_endproc
.LFE9774:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15NodeCategorySet6EnableERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node15NodeCategorySet6EnableERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node15NodeCategorySet6EnableERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L534
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L508
	cmpw	$1040, %cx
	jne	.L473
.L508:
	movq	23(%rdx), %r15
.L475:
	testq	%r15, %r15
	je	.L471
	cmpb	$0, 32(%r15)
	je	.L535
.L471:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	cmpq	$0, 80(%r15)
	je	.L471
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L478
	movl	24+_ZN4node11per_process11v8_platformE(%rip), %esi
	cmpl	$-1, %esi
	je	.L537
.L479:
	leaq	40(%r15), %rdx
	call	_ZN4node7tracing5Agent6EnableEiRKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EE@PLT
.L478:
	movb	$1, 32(%r15)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L473:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L534:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L537:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movl	$44, %edx
	leaq	-144(%rbp), %rdi
	leaq	56(%rax), %rsi
	call	_ZN4node11SplitStringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEc@PLT
	movq	8+_ZN4node11per_process11v8_platformE(%rip), %rax
	movl	$1416, %edi
	movq	%rax, -200(%rbp)
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	88(%rax), %r12
	call	_Znwm@PLT
	movq	%r12, %rsi
	leaq	-112(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN4node7tracing15NodeTraceWriterC1ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-136(%rbp), %rax
	movq	-144(%rbp), %r14
	movq	%rbx, -168(%rbp)
	leaq	-104(%rbp), %rbx
	movl	$0, -104(%rbp)
	movq	%rax, -184(%rbp)
	movq	$0, -96(%rbp)
	movq	%rbx, -88(%rbp)
	movq	%rbx, -80(%rbp)
	movq	$0, -72(%rbp)
	cmpq	%r14, %rax
	jne	.L489
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rdx, 32(%rsi)
	movq	16(%r14), %rdx
	movq	%rdx, 48(%rsi)
.L488:
	movq	8(%r14), %rdx
	movq	%rbx, %rcx
	movl	%r13d, %edi
	movq	%rdx, 40(%rsi)
	movq	%r10, %rdx
	movq	%rax, (%r14)
	movq	$0, 8(%r14)
	movb	$0, 16(%r14)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -72(%rbp)
.L483:
	addq	$32, %r14
	cmpq	%r14, -184(%rbp)
	je	.L490
.L489:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS5_ERKS5_
	movq	%rdx, %r10
	testq	%rdx, %rdx
	je	.L483
	testq	%rax, %rax
	jne	.L507
	cmpq	%rbx, %rdx
	jne	.L538
.L507:
	movl	$1, %r13d
.L484:
	movl	$64, %edi
	movq	%r10, -192(%rbp)
	call	_Znwm@PLT
	movq	-192(%rbp), %r10
	movq	%rax, %rsi
	leaq	48(%rax), %rax
	movq	%rax, 32(%rsi)
	movq	(%r14), %rdx
	leaq	16(%r14), %rax
	cmpq	%rax, %rdx
	jne	.L487
	movdqu	16(%r14), %xmm0
	movups	%xmm0, 48(%rsi)
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L490:
	movq	-200(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	leaq	-168(%rbp), %rcx
	call	_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE@PLT
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L482
	movl	24+_ZN4node11per_process11v8_platformE(%rip), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L482:
	movq	-160(%rbp), %rax
	movq	-96(%rbp), %r13
	movq	%rax, 16+_ZN4node11per_process11v8_platformE(%rip)
	movl	-152(%rbp), %eax
	movl	%eax, 24+_ZN4node11per_process11v8_platformE(%rip)
	testq	%r13, %r13
	je	.L495
.L491:
	movq	24(%r13), %rsi
	movq	%r12, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r13), %rdi
	leaq	48(%r13), %rax
	movq	16(%r13), %rbx
	cmpq	%rax, %rdi
	je	.L494
	call	_ZdlPv@PLT
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L495
.L496:
	movq	%rbx, %r13
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L496
.L495:
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L493
	movq	(%rdi), %rax
	call	*8(%rax)
.L493:
	movq	-136(%rbp), %rbx
	movq	-144(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L497
	.p2align 4,,10
	.p2align 3
.L501:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L498
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L501
.L499:
	movq	-144(%rbp), %r12
.L497:
	testq	%r12, %r12
	je	.L502
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L502:
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rdi
	testq	%rdi, %rdi
	je	.L478
	movl	24+_ZN4node11per_process11v8_platformE(%rip), %esi
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L498:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L501
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L538:
	movq	8(%r14), %rcx
	movq	40(%rdx), %r13
	cmpq	%r13, %rcx
	movq	%r13, %rdx
	cmovbe	%rcx, %rdx
	testq	%rdx, %rdx
	je	.L485
	movq	32(%r10), %rsi
	movq	(%r14), %rdi
	movq	%rcx, -208(%rbp)
	movq	%r10, -192(%rbp)
	call	memcmp@PLT
	movq	-192(%rbp), %r10
	movq	-208(%rbp), %rcx
	testl	%eax, %eax
	jne	.L486
.L485:
	subq	%r13, %rcx
	movl	$2147483648, %eax
	xorl	%r13d, %r13d
	cmpq	%rax, %rcx
	jge	.L484
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L507
	movl	%ecx, %eax
.L486:
	shrl	$31, %eax
	movl	%eax, %r13d
	jmp	.L484
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7629:
	.size	_ZN4node15NodeCategorySet6EnableERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node15NodeCategorySet6EnableERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node15NodeCategorySetE
	.section	.data.rel.ro._ZTVN4node15NodeCategorySetE,"awG",@progbits,_ZTVN4node15NodeCategorySetE,comdat
	.align 8
	.type	_ZTVN4node15NodeCategorySetE, @object
	.size	_ZTVN4node15NodeCategorySetE, 88
_ZTVN4node15NodeCategorySetE:
	.quad	0
	.quad	0
	.quad	_ZN4node15NodeCategorySetD1Ev
	.quad	_ZN4node15NodeCategorySetD0Ev
	.quad	_ZNK4node15NodeCategorySet10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15NodeCategorySet14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15NodeCategorySet8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.section	.rodata.str1.1
.LC13:
	.string	"../src/node_trace_events.cc"
.LC14:
	.string	"trace_events"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC13
	.quad	0
	.quad	_ZN4node15NodeCategorySet10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC14
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"../src/node_trace_events.cc:115"
	.section	.rodata.str1.1
.LC16:
	.string	"args[0]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"void node::SetTraceCategoryStateUpdateHandler(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4nodeL34SetTraceCategoryStateUpdateHandlerERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"../src/node_trace_events.cc:61"
	.section	.rodata.str1.1
.LC19:
	.string	"args[0]->IsArray()"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"static void node::NodeCategorySet::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node15NodeCategorySet3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC21:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC23:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC24:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC26:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC27:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC29:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC30:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC32:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC33:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC35:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
