	.file	"uv.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"DEP0119"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Directly calling process.binding('uv').errname(<val>) is being deprecated. Please make sure to use util.getSystemErrorName() instead."
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L31
	cmpw	$1040, %cx
	jne	.L2
.L31:
	movq	23(%rdx), %r12
.L4:
	movq	1648(%r12), %r13
	movq	1640(%r12), %rdx
	testq	%r13, %r13
	je	.L5
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r13), %rax
	testq	%r15, %r15
	je	.L6
	lock addl	$1, (%rax)
.L7:
	movzbl	268(%rdx), %r14d
	testb	%r14b, %r14b
	je	.L24
	movzbl	1403(%r12), %r14d
	movb	$0, 1403(%r12)
.L24:
	testq	%r15, %r15
	je	.L8
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L35
.L11:
	testb	%r14b, %r14b
	jne	.L15
.L16:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L19
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L20:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testb	%al, %al
	je	.L1
	sarq	$32, %rdi
	testl	%edi, %edi
	jns	.L36
	call	uv_err_name@PLT
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%rbx), %rbx
	movq	352(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L37
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	addl	$1, 8(%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L19:
	movq	8(%rbx), %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L5:
	cmpb	$0, 268(%rdx)
	je	.L16
	movzbl	1403(%r12), %eax
	movb	$0, 1403(%r12)
	testb	%al, %al
	je	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node29ProcessEmitDeprecationWarningEPNS_11EnvironmentEPKcS3_@PLT
	testb	%al, %al
	jne	.L16
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L11
.L35:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L12
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L13:
	cmpl	$1, %eax
	jne	.L11
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	_ZZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1
	.cfi_endproc
.LFE7080:
	.size	_ZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC2:
	.string	"errname"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata.str1.1
.LC4:
	.string	"basic_string::append"
.LC5:
	.string	"getErrorMap"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -152(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	je	.L39
	movq	%rdi, %r15
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L39
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L39
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %r13
	movq	2680(%rbx), %rdx
	pushq	$0
	movq	%rbx, -176(%rbp)
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	3280(%rbx), %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rsi
	popq	%rdi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L72
.L40:
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L73
.L41:
	movq	-176(%rbp), %rax
	movq	%r12, %rcx
	movq	%r15, %rdi
	movq	3280(%rax), %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L74
.L42:
	leaq	-112(%rbp), %r9
	movb	$95, -110(%rbp)
	leaq	-80(%rbp), %r12
	movl	$22101, %ecx
	movq	%r9, -184(%rbp)
	leaq	_ZN4node11per_processL13uv_errors_mapE(%rip), %rbx
	movl	$3, %r8d
	movq	%r9, -128(%rbp)
	movw	%cx, -112(%rbp)
	movq	$3, -120(%rbp)
	movb	$0, -109(%rbp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L80:
	call	_ZdlPv@PLT
	addq	$24, %rbx
	leaq	1920+_ZN4node11per_processL13uv_errors_mapE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L52
.L53:
	movq	-128(%rbp), %r9
	movq	-120(%rbp), %r8
.L54:
	movq	%r9, %rax
	movq	%r12, -96(%rbp)
	movq	8(%rbx), %r14
	addq	%r8, %rax
	je	.L43
	testq	%r9, %r9
	je	.L75
.L43:
	movq	%r8, -136(%rbp)
	cmpq	$15, %r8
	ja	.L76
	cmpq	$1, %r8
	jne	.L46
	movzbl	(%r9), %eax
	movb	%al, -80(%rbp)
	movq	%r12, %rax
.L47:
	movq	%r8, -88(%rbp)
	movq	%r14, %rdi
	movb	$0, (%rax,%r8)
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	%rax, %rdx
	ja	.L77
	movq	%r14, %rsi
	leaq	-96(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L78
.L49:
	movl	(%rbx), %esi
	movq	%r13, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-152(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L79
.L50:
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	jne	.L80
	addq	$24, %rbx
	leaq	1920+_ZN4node11per_processL13uv_errors_mapE(%rip), %rax
	cmpq	%rbx, %rax
	jne	.L53
.L52:
	movq	-176(%rbp), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_19GetErrMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L81
.L55:
	movq	-176(%rbp), %rax
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L82
.L56:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L83
.L57:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	-128(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L38
	call	_ZdlPv@PLT
.L38:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L84
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L85
	movq	%r12, %rax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	-96(%rbp), %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -168(%rbp)
	movq	%r9, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %r9
	movq	-168(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L45:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r8
	movq	-96(%rbp), %rax
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L79:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L78:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L49
.L72:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L40
.L73:
	movq	%rax, -160(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-160(%rbp), %rdx
	jmp	.L41
.L74:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L42
.L81:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L55
.L82:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L56
.L83:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L57
.L77:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L75:
	leaq	.LC3(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L84:
	call	__stack_chk_fail@PLT
.L85:
	movq	%r12, %rdi
	jmp	.L45
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7084:
.L39:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7084:
	.text
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE6:
	.text
.LHOTE6:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_19GetErrMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_19GetErrMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%rdi, -96(%rbp)
	movq	32(%rsi), %rdx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L98
	cmpw	$1040, %cx
	jne	.L87
.L98:
	movq	23(%rdx), %rax
.L89:
	movq	352(%rax), %r12
	movq	3280(%rax), %r15
	leaq	_ZN4node11per_processL13uv_errors_mapE(%rip), %rbx
	leaq	-80(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v83Map3NewEPNS_7IsolateE@PLT
	movq	%rax, %r13
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L90:
	movq	16(%rbx), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L101
.L91:
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movl	(%rbx), %esi
	movq	%r12, %rdi
	movq	%rax, -88(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-88(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v83Map3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testq	%rax, %rax
	je	.L86
	addq	$24, %rbx
	leaq	1920+_ZN4node11per_processL13uv_errors_mapE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L102
.L94:
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L90
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rax
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-96(%rbp), %rax
	movq	(%rax), %rax
	testq	%r13, %r13
	je	.L103
	movq	0(%r13), %rdx
.L96:
	movq	%rdx, 24(%rax)
	.p2align 4,,10
	.p2align 3
.L86:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L104
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L87:
	.cfi_restore_state
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L89
.L103:
	movq	16(%rax), %rdx
	jmp	.L96
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7081:
	.size	_ZN4node12_GLOBAL__N_19GetErrMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_19GetErrMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_Z12_register_uvv
	.type	_Z12_register_uvv, @function
_Z12_register_uvv:
.LFB7085:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7085:
	.size	_Z12_register_uvv, .-_Z12_register_uvv
	.section	.rodata.str1.1
.LC7:
	.string	"../src/uv.cc"
.LC8:
	.string	"uv"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC7
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_110InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC8
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC9:
	.string	"../src/uv.cc:74"
.LC10:
	.string	"(err) < (0)"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"void node::{anonymous}::ErrName(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_17ErrNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.1
.LC12:
	.string	"E2BIG"
.LC13:
	.string	"argument list too long"
.LC14:
	.string	"EACCES"
.LC15:
	.string	"permission denied"
.LC16:
	.string	"EADDRINUSE"
.LC17:
	.string	"address already in use"
.LC18:
	.string	"EADDRNOTAVAIL"
.LC19:
	.string	"address not available"
.LC20:
	.string	"EAFNOSUPPORT"
.LC21:
	.string	"address family not supported"
.LC22:
	.string	"EAGAIN"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"resource temporarily unavailable"
	.section	.rodata.str1.1
.LC24:
	.string	"EAI_ADDRFAMILY"
.LC25:
	.string	"EAI_AGAIN"
.LC26:
	.string	"temporary failure"
.LC27:
	.string	"EAI_BADFLAGS"
.LC28:
	.string	"bad ai_flags value"
.LC29:
	.string	"EAI_BADHINTS"
.LC30:
	.string	"invalid value for hints"
.LC31:
	.string	"EAI_CANCELED"
.LC32:
	.string	"request canceled"
.LC33:
	.string	"EAI_FAIL"
.LC34:
	.string	"permanent failure"
.LC35:
	.string	"EAI_FAMILY"
.LC36:
	.string	"ai_family not supported"
.LC37:
	.string	"EAI_MEMORY"
.LC38:
	.string	"out of memory"
.LC39:
	.string	"EAI_NODATA"
.LC40:
	.string	"no address"
.LC41:
	.string	"EAI_NONAME"
.LC42:
	.string	"unknown node or service"
.LC43:
	.string	"EAI_OVERFLOW"
.LC44:
	.string	"argument buffer overflow"
.LC45:
	.string	"EAI_PROTOCOL"
.LC46:
	.string	"resolved protocol is unknown"
.LC47:
	.string	"EAI_SERVICE"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"service not available for socket type"
	.section	.rodata.str1.1
.LC49:
	.string	"EAI_SOCKTYPE"
.LC50:
	.string	"socket type not supported"
.LC51:
	.string	"EALREADY"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"connection already in progress"
	.section	.rodata.str1.1
.LC53:
	.string	"EBADF"
.LC54:
	.string	"bad file descriptor"
.LC55:
	.string	"EBUSY"
.LC56:
	.string	"resource busy or locked"
.LC57:
	.string	"ECANCELED"
.LC58:
	.string	"operation canceled"
.LC59:
	.string	"ECHARSET"
.LC60:
	.string	"invalid Unicode character"
.LC61:
	.string	"ECONNABORTED"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"software caused connection abort"
	.section	.rodata.str1.1
.LC63:
	.string	"ECONNREFUSED"
.LC64:
	.string	"connection refused"
.LC65:
	.string	"ECONNRESET"
.LC66:
	.string	"connection reset by peer"
.LC67:
	.string	"EDESTADDRREQ"
.LC68:
	.string	"destination address required"
.LC69:
	.string	"EEXIST"
.LC70:
	.string	"file already exists"
.LC71:
	.string	"EFAULT"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"bad address in system call argument"
	.section	.rodata.str1.1
.LC73:
	.string	"EFBIG"
.LC74:
	.string	"file too large"
.LC75:
	.string	"EHOSTUNREACH"
.LC76:
	.string	"host is unreachable"
.LC77:
	.string	"EINTR"
.LC78:
	.string	"interrupted system call"
.LC79:
	.string	"EINVAL"
.LC80:
	.string	"invalid argument"
.LC81:
	.string	"EIO"
.LC82:
	.string	"i/o error"
.LC83:
	.string	"EISCONN"
.LC84:
	.string	"socket is already connected"
.LC85:
	.string	"EISDIR"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"illegal operation on a directory"
	.section	.rodata.str1.1
.LC87:
	.string	"ELOOP"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"too many symbolic links encountered"
	.section	.rodata.str1.1
.LC89:
	.string	"EMFILE"
.LC90:
	.string	"too many open files"
.LC91:
	.string	"EMSGSIZE"
.LC92:
	.string	"message too long"
.LC93:
	.string	"ENAMETOOLONG"
.LC94:
	.string	"name too long"
.LC95:
	.string	"ENETDOWN"
.LC96:
	.string	"network is down"
.LC97:
	.string	"ENETUNREACH"
.LC98:
	.string	"network is unreachable"
.LC99:
	.string	"ENFILE"
.LC100:
	.string	"file table overflow"
.LC101:
	.string	"ENOBUFS"
.LC102:
	.string	"no buffer space available"
.LC103:
	.string	"ENODEV"
.LC104:
	.string	"no such device"
.LC105:
	.string	"ENOENT"
.LC106:
	.string	"no such file or directory"
.LC107:
	.string	"ENOMEM"
.LC108:
	.string	"not enough memory"
.LC109:
	.string	"ENONET"
.LC110:
	.string	"machine is not on the network"
.LC111:
	.string	"ENOPROTOOPT"
.LC112:
	.string	"protocol not available"
.LC113:
	.string	"ENOSPC"
.LC114:
	.string	"no space left on device"
.LC115:
	.string	"ENOSYS"
.LC116:
	.string	"function not implemented"
.LC117:
	.string	"ENOTCONN"
.LC118:
	.string	"socket is not connected"
.LC119:
	.string	"ENOTDIR"
.LC120:
	.string	"not a directory"
.LC121:
	.string	"ENOTEMPTY"
.LC122:
	.string	"directory not empty"
.LC123:
	.string	"ENOTSOCK"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"socket operation on non-socket"
	.section	.rodata.str1.1
.LC125:
	.string	"ENOTSUP"
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"operation not supported on socket"
	.section	.rodata.str1.1
.LC127:
	.string	"EPERM"
.LC128:
	.string	"operation not permitted"
.LC129:
	.string	"EPIPE"
.LC130:
	.string	"broken pipe"
.LC131:
	.string	"EPROTO"
.LC132:
	.string	"protocol error"
.LC133:
	.string	"EPROTONOSUPPORT"
.LC134:
	.string	"protocol not supported"
.LC135:
	.string	"EPROTOTYPE"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"protocol wrong type for socket"
	.section	.rodata.str1.1
.LC137:
	.string	"ERANGE"
.LC138:
	.string	"result too large"
.LC139:
	.string	"EROFS"
.LC140:
	.string	"read-only file system"
.LC141:
	.string	"ESHUTDOWN"
	.section	.rodata.str1.8
	.align 8
.LC142:
	.string	"cannot send after transport endpoint shutdown"
	.section	.rodata.str1.1
.LC143:
	.string	"ESPIPE"
.LC144:
	.string	"invalid seek"
.LC145:
	.string	"ESRCH"
.LC146:
	.string	"no such process"
.LC147:
	.string	"ETIMEDOUT"
.LC148:
	.string	"connection timed out"
.LC149:
	.string	"ETXTBSY"
.LC150:
	.string	"text file is busy"
.LC151:
	.string	"EXDEV"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"cross-device link not permitted"
	.section	.rodata.str1.1
.LC153:
	.string	"UNKNOWN"
.LC154:
	.string	"unknown error"
.LC155:
	.string	"EOF"
.LC156:
	.string	"end of file"
.LC157:
	.string	"ENXIO"
.LC158:
	.string	"no such device or address"
.LC159:
	.string	"EMLINK"
.LC160:
	.string	"too many links"
.LC161:
	.string	"EHOSTDOWN"
.LC162:
	.string	"host is down"
.LC163:
	.string	"EREMOTEIO"
.LC164:
	.string	"remote I/O error"
.LC165:
	.string	"ENOTTY"
	.section	.rodata.str1.8
	.align 8
.LC166:
	.string	"inappropriate ioctl for device"
	.section	.rodata.str1.1
.LC167:
	.string	"EFTYPE"
	.section	.rodata.str1.8
	.align 8
.LC168:
	.string	"inappropriate file type or format"
	.section	.rodata.str1.1
.LC169:
	.string	"EILSEQ"
.LC170:
	.string	"illegal byte sequence"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN4node11per_processL13uv_errors_mapE, @object
	.size	_ZN4node11per_processL13uv_errors_mapE, 1920
_ZN4node11per_processL13uv_errors_mapE:
	.long	-7
	.zero	4
	.quad	.LC12
	.quad	.LC13
	.long	-13
	.zero	4
	.quad	.LC14
	.quad	.LC15
	.long	-98
	.zero	4
	.quad	.LC16
	.quad	.LC17
	.long	-99
	.zero	4
	.quad	.LC18
	.quad	.LC19
	.long	-97
	.zero	4
	.quad	.LC20
	.quad	.LC21
	.long	-11
	.zero	4
	.quad	.LC22
	.quad	.LC23
	.long	-3000
	.zero	4
	.quad	.LC24
	.quad	.LC21
	.long	-3001
	.zero	4
	.quad	.LC25
	.quad	.LC26
	.long	-3002
	.zero	4
	.quad	.LC27
	.quad	.LC28
	.long	-3013
	.zero	4
	.quad	.LC29
	.quad	.LC30
	.long	-3003
	.zero	4
	.quad	.LC31
	.quad	.LC32
	.long	-3004
	.zero	4
	.quad	.LC33
	.quad	.LC34
	.long	-3005
	.zero	4
	.quad	.LC35
	.quad	.LC36
	.long	-3006
	.zero	4
	.quad	.LC37
	.quad	.LC38
	.long	-3007
	.zero	4
	.quad	.LC39
	.quad	.LC40
	.long	-3008
	.zero	4
	.quad	.LC41
	.quad	.LC42
	.long	-3009
	.zero	4
	.quad	.LC43
	.quad	.LC44
	.long	-3014
	.zero	4
	.quad	.LC45
	.quad	.LC46
	.long	-3010
	.zero	4
	.quad	.LC47
	.quad	.LC48
	.long	-3011
	.zero	4
	.quad	.LC49
	.quad	.LC50
	.long	-114
	.zero	4
	.quad	.LC51
	.quad	.LC52
	.long	-9
	.zero	4
	.quad	.LC53
	.quad	.LC54
	.long	-16
	.zero	4
	.quad	.LC55
	.quad	.LC56
	.long	-125
	.zero	4
	.quad	.LC57
	.quad	.LC58
	.long	-4080
	.zero	4
	.quad	.LC59
	.quad	.LC60
	.long	-103
	.zero	4
	.quad	.LC61
	.quad	.LC62
	.long	-111
	.zero	4
	.quad	.LC63
	.quad	.LC64
	.long	-104
	.zero	4
	.quad	.LC65
	.quad	.LC66
	.long	-89
	.zero	4
	.quad	.LC67
	.quad	.LC68
	.long	-17
	.zero	4
	.quad	.LC69
	.quad	.LC70
	.long	-14
	.zero	4
	.quad	.LC71
	.quad	.LC72
	.long	-27
	.zero	4
	.quad	.LC73
	.quad	.LC74
	.long	-113
	.zero	4
	.quad	.LC75
	.quad	.LC76
	.long	-4
	.zero	4
	.quad	.LC77
	.quad	.LC78
	.long	-22
	.zero	4
	.quad	.LC79
	.quad	.LC80
	.long	-5
	.zero	4
	.quad	.LC81
	.quad	.LC82
	.long	-106
	.zero	4
	.quad	.LC83
	.quad	.LC84
	.long	-21
	.zero	4
	.quad	.LC85
	.quad	.LC86
	.long	-40
	.zero	4
	.quad	.LC87
	.quad	.LC88
	.long	-24
	.zero	4
	.quad	.LC89
	.quad	.LC90
	.long	-90
	.zero	4
	.quad	.LC91
	.quad	.LC92
	.long	-36
	.zero	4
	.quad	.LC93
	.quad	.LC94
	.long	-100
	.zero	4
	.quad	.LC95
	.quad	.LC96
	.long	-101
	.zero	4
	.quad	.LC97
	.quad	.LC98
	.long	-23
	.zero	4
	.quad	.LC99
	.quad	.LC100
	.long	-105
	.zero	4
	.quad	.LC101
	.quad	.LC102
	.long	-19
	.zero	4
	.quad	.LC103
	.quad	.LC104
	.long	-2
	.zero	4
	.quad	.LC105
	.quad	.LC106
	.long	-12
	.zero	4
	.quad	.LC107
	.quad	.LC108
	.long	-64
	.zero	4
	.quad	.LC109
	.quad	.LC110
	.long	-92
	.zero	4
	.quad	.LC111
	.quad	.LC112
	.long	-28
	.zero	4
	.quad	.LC113
	.quad	.LC114
	.long	-38
	.zero	4
	.quad	.LC115
	.quad	.LC116
	.long	-107
	.zero	4
	.quad	.LC117
	.quad	.LC118
	.long	-20
	.zero	4
	.quad	.LC119
	.quad	.LC120
	.long	-39
	.zero	4
	.quad	.LC121
	.quad	.LC122
	.long	-88
	.zero	4
	.quad	.LC123
	.quad	.LC124
	.long	-95
	.zero	4
	.quad	.LC125
	.quad	.LC126
	.long	-1
	.zero	4
	.quad	.LC127
	.quad	.LC128
	.long	-32
	.zero	4
	.quad	.LC129
	.quad	.LC130
	.long	-71
	.zero	4
	.quad	.LC131
	.quad	.LC132
	.long	-93
	.zero	4
	.quad	.LC133
	.quad	.LC134
	.long	-91
	.zero	4
	.quad	.LC135
	.quad	.LC136
	.long	-34
	.zero	4
	.quad	.LC137
	.quad	.LC138
	.long	-30
	.zero	4
	.quad	.LC139
	.quad	.LC140
	.long	-108
	.zero	4
	.quad	.LC141
	.quad	.LC142
	.long	-29
	.zero	4
	.quad	.LC143
	.quad	.LC144
	.long	-3
	.zero	4
	.quad	.LC145
	.quad	.LC146
	.long	-110
	.zero	4
	.quad	.LC147
	.quad	.LC148
	.long	-26
	.zero	4
	.quad	.LC149
	.quad	.LC150
	.long	-18
	.zero	4
	.quad	.LC151
	.quad	.LC152
	.long	-4094
	.zero	4
	.quad	.LC153
	.quad	.LC154
	.long	-4095
	.zero	4
	.quad	.LC155
	.quad	.LC156
	.long	-6
	.zero	4
	.quad	.LC157
	.quad	.LC158
	.long	-31
	.zero	4
	.quad	.LC159
	.quad	.LC160
	.long	-112
	.zero	4
	.quad	.LC161
	.quad	.LC162
	.long	-121
	.zero	4
	.quad	.LC163
	.quad	.LC164
	.long	-25
	.zero	4
	.quad	.LC165
	.quad	.LC166
	.long	-4028
	.zero	4
	.quad	.LC167
	.quad	.LC168
	.long	-84
	.zero	4
	.quad	.LC169
	.quad	.LC170
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
