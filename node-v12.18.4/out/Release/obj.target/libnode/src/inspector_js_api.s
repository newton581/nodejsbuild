	.file	"inspector_js_api.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4286:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4288:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7098:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7151:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7151:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7152:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7152:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7153:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7153:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7155:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L9
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7155:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD2Ev:
.LFB9429:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9429:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD2Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD1Ev,_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD2Ev:
.LFB9457:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9457:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD2Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD1Ev,_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD2Ev
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8SelfSizeEv, @function
_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8SelfSizeEv:
.LFB9857:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE9857:
	.size	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8SelfSizeEv, .-_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8SelfSizeEv
	.set	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8SelfSizeEv,_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8SelfSizeEv
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L33
	cmpw	$1040, %cx
	jne	.L14
.L33:
	movq	23(%rdx), %r13
.L16:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L17
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L18:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L35
.L19:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L19
	movq	(%rbx), %rax
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v86String5ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movslq	-72(%rbp), %rax
	cmpl	$1, 16(%rbx)
	movb	$0, -64(%rbp)
	movq	%rax, -56(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -48(%rbp)
	jg	.L21
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L22:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L36
	cmpl	$1, 16(%rbx)
	jg	.L24
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L25:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %r12
	testb	%al, %al
	je	.L37
.L26:
	addq	%r12, %r12
	cmpl	$2, 16(%rbx)
	jg	.L27
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L28:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L38
	movq	(%rbx), %rax
	cmpl	$2, 16(%rbx)
	movq	8(%rax), %rsi
	leaq	88(%rsi), %rdi
	jle	.L31
	movq	8(%rbx), %rdi
	subq	$16, %rdi
.L31:
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	movq	2080(%r13), %rdi
	leaq	-64(%rbp), %rsi
	movq	%r12, %rdx
	movzbl	%al, %ecx
	call	_ZN4node9inspector5Agent18AsyncTaskScheduledERKN12v8_inspector10StringViewEPvb@PLT
	movq	%r14, %rdi
	call	_ZN2v86String5ValueD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L39
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L21:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L27:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L24:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L39:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7263:
	.size	_ZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB9825:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L40
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	ret
	.cfi_endproc
.LFE9825:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD0Ev:
.LFB9431:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9431:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD0Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD0Ev:
.LFB9459:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9459:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD0Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD0Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB9827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L45
	call	_ZdlPv@PLT
.L45:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9827:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED2Ev:
.LFB8886:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L48:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*8(%rax)
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE8886:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED2Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED1Ev,_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED2Ev:
.LFB8897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L58:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rax
	call	*8(%rax)
.L59:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE8897:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED2Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED1Ev,_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED2Ev
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE:
.LFB7231:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-2096(%rbp), %rdi
	subq	$2120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node12TwoByteValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2096(%rbp), %rax
	movq	%r12, %rdi
	leaq	-2128(%rbp), %rsi
	movb	$0, -2128(%rbp)
	movq	%rax, -2120(%rbp)
	movq	-2080(%rbp), %rax
	movq	%rax, -2112(%rbp)
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	movq	-2080(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L67
	leaq	-2072(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L67
	call	free@PLT
.L67:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	addq	$2120, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L77:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7231:
	.size	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE:
.LFB9851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r12, %rdi
	movq	352(%rax), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%r14), %rcx
	movq	16(%r14), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L86
.L79:
	movq	16(%rbx), %r15
	movq	%rax, -88(%rbp)
	movq	64(%r15), %rsi
	testq	%rsi, %rsi
	je	.L80
	movq	16(%r15), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L80:
	leaq	-88(%rbp), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L87
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L79
.L87:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9851:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE:
.LFB9850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%r12, %rdi
	movq	352(%rax), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%r14), %rcx
	movq	16(%r14), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L96
.L89:
	movq	16(%rbx), %r15
	movq	%rax, -88(%rbp)
	movq	64(%r15), %rsi
	testq	%rsi, %rsi
	je	.L90
	movq	16(%r15), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L90:
	leaq	-88(%rbp), %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L89
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9850:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"callback"
.LC2:
	.string	"InspectorSession"
.LC3:
	.string	"session"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L119
	movq	(%rax), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L119
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %r13
	movq	%r13, %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L108
	cmpq	72(%rbx), %rcx
	je	.L120
.L103:
	movq	-8(%rcx), %rsi
.L102:
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	call	*%r14
.L100:
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$16, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC2(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	$8, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -48(%rbp)
	call	*%rax
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L104
	movq	(%rdi), %rax
	call	*8(%rax)
.L104:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L98
	cmpq	72(%rbx), %rax
	je	.L121
.L106:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L98
	movq	8(%rbx), %rdi
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L98:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L122
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L121:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L120:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L108:
	xorl	%esi, %esi
	jmp	.L102
.L122:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9852:
	.size	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9855:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	64(%rdi), %rax
	testq	%rax, %rax
	je	.L144
	movq	(%rax), %rsi
	movq	(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L144
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %r13
	movq	%r13, %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L133
	cmpq	72(%rbx), %rcx
	je	.L145
.L128:
	movq	-8(%rcx), %rsi
.L127:
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	call	*%r14
.L125:
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$16, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC2(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	$8, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -48(%rbp)
	call	*%rax
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L129
	movq	(%rdi), %rax
	call	*8(%rax)
.L129:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L123
	cmpq	72(%rbx), %rax
	je	.L146
.L131:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L123
	movq	8(%rbx), %rdi
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L123:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	-48(%rbp), %r13
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L146:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L145:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L133:
	xorl	%esi, %esi
	jmp	.L127
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9855:
	.size	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE14MemoryInfoNameEv, @function
_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE14MemoryInfoNameEv:
.LFB9856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$20, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC4(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$1852795252, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L151
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L151:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9856:
	.size	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE14MemoryInfoNameEv, .-_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE14MemoryInfoNameEv
	.set	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE14MemoryInfoNameEv,_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED0Ev:
.LFB8888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L153
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L153:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L154
	movq	(%rdi), %rax
	call	*8(%rax)
.L154:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8888:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED0Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED0Ev:
.LFB8899:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L163:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L164
	movq	(%rdi), %rax
	call	*8(%rax)
.L164:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8899:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED0Ev, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED0Ev
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L192
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L181
	cmpw	$1040, %cx
	jne	.L174
.L181:
	movq	23(%rdx), %r12
.L176:
	testq	%r12, %r12
	je	.L172
	movq	56(%r12), %rdi
	movq	$0, 56(%r12)
	testq	%rdi, %rdi
	je	.L178
	movq	(%rdi), %rax
	call	*8(%rax)
.L178:
	movq	64(%r12), %rdi
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEEE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L179
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L179:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L180
	movq	(%rdi), %rax
	call	*8(%rax)
.L180:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8485:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L213
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L202
	cmpw	$1040, %cx
	jne	.L195
.L202:
	movq	23(%rdx), %r12
.L197:
	testq	%r12, %r12
	je	.L193
	movq	56(%r12), %rdi
	movq	$0, 56(%r12)
	testq	%rdi, %rdi
	je	.L199
	movq	(%rdi), %rax
	call	*8(%rax)
.L199:
	movq	64(%r12), %rdi
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEEE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L200
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L200:
	movq	56(%r12), %rdi
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*8(%rax)
.L201:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8481:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19IsEnabledERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_19IsEnabledERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rbx
	movq	32(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L220
	cmpw	$1040, %cx
	jne	.L215
.L220:
	movq	23(%rdx), %rax
.L217:
	movq	2080(%rax), %rdi
	call	_ZN4node9inspector5Agent8IsActiveEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	%rdi, %r12
	xorl	%esi, %esi
	leaq	32(%rbx), %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r12), %rbx
	jmp	.L217
	.cfi_endproc
.LFE7265:
	.size	_ZN4node9inspector12_GLOBAL__N_19IsEnabledERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_19IsEnabledERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_115WaitForDebuggerERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_115WaitForDebuggerERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7273:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L229
	cmpw	$1040, %cx
	jne	.L223
.L229:
	movq	23(%rdx), %rax
.L225:
	movq	2080(%rax), %r12
	movq	%r12, %rdi
	call	_ZN4node9inspector5Agent8IsActiveEv@PLT
	testb	%al, %al
	jne	.L234
.L226:
	movq	%r12, %rdi
	movq	(%rbx), %rbx
	call	_ZN4node9inspector5Agent8IsActiveEv@PLT
	cmpb	$1, %al
	movq	8(%rbx), %rdx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN4node9inspector5Agent14WaitForConnectEv@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L223:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L225
	.cfi_endproc
.LFE7273:
	.size	_ZN4node9inspector12_GLOBAL__N_115WaitForDebuggerERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_115WaitForDebuggerERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L246
	cmpw	$1040, %cx
	jne	.L236
.L246:
	movq	23(%rdx), %r13
.L238:
	cmpl	$1, 16(%rbx)
	je	.L255
	leaq	_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L255:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L256
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L257
	movq	8(%rbx), %r12
.L241:
	movq	2920(%r13), %rdi
	movq	352(%r13), %r14
	testq	%rdi, %rdi
	je	.L242
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2920(%r13)
.L242:
	testq	%r12, %r12
	je	.L235
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2920(%r13)
.L235:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L236:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7255:
	.size	_ZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L273
	cmpw	$1040, %cx
	jne	.L259
.L273:
	movq	23(%rdx), %r13
.L261:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L262
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L284
.L264:
	movq	8(%rbx), %r14
	movl	16(%rbx), %eax
	movq	%r14, %r12
	testl	%eax, %eax
	jle	.L285
.L265:
	movl	$72, %edi
	call	_Znwm@PLT
	movsd	.LC5(%rip), %xmm0
	leaq	8(%r14), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$46, %ecx
	movq	%rax, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEEE(%rip), %rax
	movq	$0, 56(%rbx)
	movq	352(%r13), %rdi
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L266
	movq	%r12, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %r12
.L266:
	movq	%r12, 64(%rbx)
	movl	$24, %edi
	movq	2080(%r13), %r12
	call	_Znwm@PLT
	leaq	-56(%rbp), %rdi
	leaq	-48(%rbp), %rdx
	xorl	%ecx, %ecx
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateE(%rip), %rsi
	movq	%r13, 8(%rax)
	movq	%rsi, (%rax)
	movq	%r12, %rsi
	movq	%rbx, 16(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L267
	movq	(%rdi), %rax
	call	*8(%rax)
.L267:
	movq	-56(%rbp), %rax
	movq	56(%rbx), %rdi
	movq	$0, -56(%rbp)
	movq	%rax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L258
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L258
	movq	(%rdi), %rax
	call	*8(%rax)
.L258:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L286
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L262:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L264
.L284:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L261
.L286:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8479:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L302
	cmpw	$1040, %cx
	jne	.L288
.L302:
	movq	23(%rdx), %r13
.L290:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L291
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L313
.L293:
	movq	8(%rbx), %r14
	movl	16(%rbx), %eax
	movq	%r14, %r12
	testl	%eax, %eax
	jle	.L314
.L294:
	movl	$72, %edi
	call	_Znwm@PLT
	movsd	.LC5(%rip), %xmm0
	leaq	8(%r14), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movl	$46, %ecx
	movq	%rax, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEEE(%rip), %rax
	movq	$0, 56(%rbx)
	movq	352(%r13), %rdi
	movq	%rax, (%rbx)
	testq	%r12, %r12
	je	.L295
	movq	%r12, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %r12
.L295:
	movq	%r12, 64(%rbx)
	movl	$24, %edi
	movq	2080(%r13), %r12
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateE(%rip), %rsi
	leaq	-56(%rbp), %rdi
	movl	$1, %ecx
	movq	%rsi, (%rax)
	leaq	-48(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r13, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector5Agent19ConnectToMainThreadESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L296
	movq	(%rdi), %rax
	call	*8(%rax)
.L296:
	movq	-56(%rbp), %rax
	movq	56(%rbx), %rdi
	movq	$0, -56(%rbp)
	movq	%rax, 56(%rbx)
	testq	%rdi, %rdi
	je	.L287
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L287
	movq	(%rdi), %rax
	call	*8(%rax)
.L287:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L315
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L291:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L293
.L313:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L288:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L290
.L315:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8483:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_13UrlERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_13UrlERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7274:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L327
	cmpw	$1040, %cx
	jne	.L317
.L327:
	movq	23(%rdx), %r12
.L319:
	movq	2080(%r12), %rsi
	leaq	-64(%rbp), %rdi
	call	_ZNK4node9inspector5Agent8GetWsUrlB5cxx11Ev@PLT
	cmpq	$0, -56(%rbp)
	jne	.L320
.L325:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L316
	call	_ZdlPv@PLT
.L316:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L331
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	movq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%rbx), %rbx
	movq	352(%r12), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L332
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L317:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L332:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L325
.L331:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7274:
	.size	_ZN4node9inspector12_GLOBAL__N_13UrlERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_13UrlERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8048:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L343
	cmpw	$1040, %cx
	jne	.L334
.L343:
	movq	23(%rdx), %r12
.L336:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L337
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L345
.L339:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L346
	movq	8(%rbx), %rdi
.L341:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L347
.L342:
	movq	2080(%r12), %rdi
	addq	$16, %rsp
	leaq	(%rdx,%rdx), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector5Agent17AsyncTaskFinishedEPv@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L337:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L339
.L345:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L334:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%rdx, -24(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-24(%rbp), %rdx
	jmp	.L342
	.cfi_endproc
.LFE8048:
	.size	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8046:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L358
	cmpw	$1040, %cx
	jne	.L349
.L358:
	movq	23(%rdx), %r12
.L351:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L352
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L360
.L354:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L361
	movq	8(%rbx), %rdi
.L356:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L362
.L357:
	movq	2080(%r12), %rdi
	addq	$16, %rsp
	leaq	(%rdx,%rdx), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector5Agent17AsyncTaskCanceledEPv@PLT
	.p2align 4,,10
	.p2align 3
.L361:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L352:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L354
.L360:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L349:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%rdx, -24(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-24(%rbp), %rdx
	jmp	.L357
	.cfi_endproc
.LFE8046:
	.size	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L373
	cmpw	$1040, %cx
	jne	.L364
.L373:
	movq	23(%rdx), %r12
.L366:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L367
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L375
.L369:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L376
	movq	8(%rbx), %rdi
.L371:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L377
.L372:
	movq	2080(%r12), %rdi
	addq	$16, %rsp
	leaq	(%rdx,%rdx), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector5Agent16AsyncTaskStartedEPv@PLT
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L367:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L369
.L375:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L364:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%rdx, -24(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-24(%rbp), %rdx
	jmp	.L372
	.cfi_endproc
.LFE8047:
	.size	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L392
	cmpw	$1040, %cx
	jne	.L379
.L392:
	movq	23(%rdx), %r12
.L381:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L382
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L394
.L384:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L395
	movq	8(%rbx), %r13
	cmpl	$1, %eax
	je	.L396
	leaq	-8(%r13), %rdi
.L388:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L397
	cmpl	$1, 16(%rbx)
	jg	.L390
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	addq	$88, %rcx
.L391:
	movq	352(%r12), %rsi
	movq	2080(%r12), %rdi
	addq	$8, %rsp
	movq	%r13, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9inspector5Agent17RegisterAsyncHookEPN2v87IsolateENS2_5LocalINS2_8FunctionEEES7_@PLT
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r13
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L382:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L384
.L394:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L390:
	movq	8(%rbx), %rcx
	subq	$8, %rcx
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L379:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L396:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L388
	.cfi_endproc
.LFE7264:
	.size	_ZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L413
	cmpw	$1040, %cx
	jne	.L399
.L413:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L425
.L402:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L414
	cmpw	$1040, %cx
	jne	.L403
.L414:
	movq	23(%rdx), %rax
.L405:
	testq	%rax, %rax
	je	.L398
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L426
	movq	8(%rbx), %rdx
.L408:
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L427
.L409:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L427:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L409
	movq	56(%rax), %r12
	testq	%r12, %r12
	je	.L398
	movq	(%r12), %rax
	movq	352(%r13), %rsi
	leaq	-48(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE
	movq	-48(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L398
	movq	(%rdi), %rax
	call	*8(%rax)
.L398:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L428
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L426:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L399:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L402
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L403:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L405
.L428:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8480:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE:
.LFB8484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L444
	cmpw	$1040, %cx
	jne	.L430
.L444:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L456
.L433:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L445
	cmpw	$1040, %cx
	jne	.L434
.L445:
	movq	23(%rdx), %rax
.L436:
	testq	%rax, %rax
	je	.L429
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L457
	movq	8(%rbx), %rdx
.L439:
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L458
.L440:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L458:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L440
	movq	56(%rax), %r12
	testq	%r12, %r12
	je	.L429
	movq	(%r12), %rax
	movq	352(%r13), %rsi
	leaq	-48(%rbp), %rdi
	movq	16(%rax), %rbx
	call	_ZN4node9inspector12_GLOBAL__N_116ToProtocolStringEPN2v87IsolateENS2_5LocalINS2_5ValueEEE
	movq	-48(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%rbx
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L429
	movq	(%rdi), %rax
	call	*8(%rax)
.L429:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L459
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L457:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L430:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L433
	.p2align 4,,10
	.p2align 3
.L456:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L434:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L436
.L459:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8484:
	.size	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L490
	cmpw	$1040, %cx
	jne	.L461
.L490:
	movq	23(%rdx), %r13
.L463:
	cmpl	$1, 16(%rbx)
	jg	.L506
	leaq	_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L506:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L507
	leaq	-8248(%rbp), %r12
	movdqa	.LC6(%rip), %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r12, -8256(%rbp)
	movq	%r12, %rax
	movaps	%xmm0, -8272(%rbp)
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L465:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L465
	movq	$0, -8248(%rbp)
	movslq	16(%rbx), %r14
	cmpq	$2, %r14
	ja	.L466
.L477:
	movq	2080(%r13), %rdi
	leaq	-8304(%rbp), %rsi
	movabsq	$7957614712154583618, %rax
	leaq	-8288(%rbp), %r14
	movq	%rax, -8288(%rbp)
	movl	$29810, %eax
	movq	%r14, -8304(%rbp)
	movl	$1635021600, -8280(%rbp)
	movw	%ax, -8276(%rbp)
	movq	$14, -8296(%rbp)
	movb	$0, -8274(%rbp)
	call	_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-8304(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L468
	call	_ZdlPv@PLT
.L468:
	movl	16(%rbx), %eax
	movq	-8256(%rbp), %r8
	movq	-8272(%rbp), %rcx
	testl	%eax, %eax
	jg	.L479
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %rdx
.L482:
	movq	3280(%r13), %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	je	.L484
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L484:
	movq	-8256(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L460
	testq	%rdi, %rdi
	je	.L460
	call	free@PLT
.L460:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L508
	addq	$8280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L466:
	.cfi_restore_state
	leaq	-2(%r14), %r15
	cmpq	$1024, %r15
	ja	.L509
	movq	%r12, %rcx
.L469:
	movq	%rcx, %rax
	leaq	-16(%rcx,%r14,8), %rsi
	movq	%r15, -8272(%rbp)
	movl	$2, %ecx
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L510:
	cmpl	16(%rbx), %ecx
	jge	.L474
	movq	8(%rbx), %rdi
	movslq	%ecx, %rdx
	salq	$3, %rdx
	subq	%rdx, %rdi
	movq	%rdi, %rdx
.L476:
	movq	%rdx, (%rax)
	addq	$8, %rax
	addl	$1, %ecx
	cmpq	%rsi, %rax
	je	.L477
.L478:
	testl	%ecx, %ecx
	jns	.L510
.L474:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L479:
	movq	8(%rbx), %rdi
	cmpl	$1, %eax
	je	.L511
	leaq	-8(%rdi), %rdx
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L509:
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r15,8), %rdi
	andq	%r15, %rax
	cmpq	%rax, %r15
	jne	.L512
	testq	%rdi, %rdi
	je	.L471
	movq	%rdi, -8312(%rbp)
	call	malloc@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L513
	movq	%rax, -8256(%rbp)
	movq	%r15, -8264(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L461:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L513:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-8312(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L471
	movq	-8272(%rbp), %rdx
	movq	%rax, -8256(%rbp)
	movq	%r15, -8264(%rbp)
	testq	%rdx, %rdx
	je	.L469
	salq	$3, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L508:
	call	__stack_chk_fail@PLT
.L511:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L482
	.cfi_endproc
.LFE7256:
	.size	_ZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7260:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L545
	cmpw	$1040, %cx
	jne	.L515
.L545:
	movq	23(%rdx), %r15
.L517:
	movq	352(%r15), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	16(%rbx), %r14d
	movq	%rax, %r12
	cmpl	$1, %r14d
	jle	.L564
	leaq	-8248(%rbp), %r13
	movdqa	.LC6(%rip), %xmm0
	leaq	-56(%rbp), %rdx
	movq	%r13, -8256(%rbp)
	movq	%r13, %rax
	movaps	%xmm0, -8272(%rbp)
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L519:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L519
	movq	$0, -8248(%rbp)
	cmpl	$2, %r14d
	jne	.L520
.L528:
	movq	2080(%r15), %rdi
	call	_ZN4node9inspector5Agent8IsActiveEv@PLT
	testb	%al, %al
	je	.L531
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L565
	movq	8(%rbx), %r14
.L533:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L566
	cmpb	$0, 2088(%r15)
	jne	.L531
	movq	(%rbx), %rdx
	movq	-8256(%rbp), %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	movb	$1, 2088(%r15)
	movl	-8272(%rbp), %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movb	$0, 2088(%r15)
	testq	%rax, %rax
	je	.L563
	.p2align 4,,10
	.p2align 3
.L531:
	cmpl	$1, 16(%rbx)
	jle	.L567
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r14
.L540:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L568
	movq	(%rbx), %rdx
	movq	-8256(%rbp), %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	-8272(%rbp), %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
.L563:
	movq	-8256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L514
	cmpq	%r13, %rdi
	je	.L514
	call	free@PLT
.L514:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L569
	addq	$8264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movslq	%r14d, %r8
	leaq	-2(%r8), %r9
	cmpq	$1024, %r9
	jbe	.L544
	leaq	0(,%r9,8), %rdi
	movq	%r8, -8296(%rbp)
	movq	%r9, -8288(%rbp)
	movq	%rdi, -8280(%rbp)
	call	malloc@PLT
	movq	-8280(%rbp), %rdi
	movq	-8288(%rbp), %r9
	testq	%rax, %rax
	movq	-8296(%rbp), %r8
	movq	%rax, %rcx
	je	.L570
	movq	%rax, -8256(%rbp)
	movq	%r9, -8264(%rbp)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L567:
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%r13, %rcx
.L523:
	movq	%r9, -8272(%rbp)
	movq	%rcx, %rax
	leaq	-16(%rcx,%r8,8), %rsi
	movl	$2, %edx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L527:
	movq	8(%rbx), %rdi
	movslq	%edx, %rcx
	salq	$3, %rcx
	subq	%rcx, %rdi
	movq	%rdi, %rcx
.L558:
	movq	%rcx, (%rax)
	addq	$8, %rax
	addl	$1, %edx
	cmpq	%rax, %rsi
	je	.L528
.L530:
	cmpl	%edx, %r14d
	jg	.L527
	movq	(%rbx), %rcx
	movq	8(%rcx), %rcx
	addq	$88, %rcx
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L565:
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L515:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L564:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L568:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%rdi, -8296(%rbp)
	movq	%r8, -8280(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-8296(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L525
	movq	-8272(%rbp), %rdx
	movl	16(%rbx), %r14d
	movq	%rax, -8256(%rbp)
	movq	-8288(%rbp), %r9
	movq	-8280(%rbp), %r8
	testq	%rdx, %rdx
	movq	%r9, -8264(%rbp)
	je	.L523
	salq	$3, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r9, -8288(%rbp)
	movq	%r8, -8280(%rbp)
	call	memcpy@PLT
	movq	-8280(%rbp), %r8
	movq	-8288(%rbp), %r9
	movq	%rax, %rcx
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L566:
	leaq	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L525:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L569:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7260:
	.size	_ZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"basic_string::_M_construct null not valid"
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_14OpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node9inspector12_GLOBAL__N_14OpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7266:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1144, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L631
	cmpw	$1040, %cx
	jne	.L572
.L631:
	movq	23(%rdx), %r13
.L574:
	movl	16(%rbx), %edx
	movq	2080(%r13), %r12
	testl	%edx, %edx
	jg	.L645
.L575:
	movq	%r12, %rdi
	call	_ZN4node9inspector5Agent13StartIoThreadEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L591
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L647
	movq	8(%rbx), %rdi
.L579:
	call	_ZNK2v86Uint325ValueEv@PLT
	movq	176(%r12), %r15
	movq	168(%r12), %r14
	movl	%eax, -1160(%rbp)
	testq	%r15, %r15
	je	.L580
	leaq	8(%r15), %rax
	movq	%rax, -1168(%rbp)
	movq	%rax, %rsi
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L581
	lock addl	$1, (%rsi)
	lock addl	$1, (%rsi)
.L583:
	movq	%r14, %rdi
	movq	%rax, -1176(%rbp)
	call	uv_mutex_lock@PLT
	movq	-1176(%rbp), %rax
	testq	%rax, %rax
	je	.L648
	movq	-1168(%rbp), %rsi
	movl	$-1, %edx
	lock xaddl	%edx, (%rsi)
.L584:
	cmpl	$1, %edx
	je	.L649
.L588:
	movq	%rax, -1176(%rbp)
	movl	-1160(%rbp), %eax
	movq	%r14, %rdi
	movl	%eax, 72(%r14)
	call	uv_mutex_unlock@PLT
	movq	-1176(%rbp), %rax
	testq	%rax, %rax
	je	.L650
	movq	-1168(%rbp), %rsi
	movl	$-1, %edx
	lock xaddl	%edx, (%rsi)
.L589:
	cmpl	$1, %edx
	je	.L651
	.p2align 4,,10
	.p2align 3
.L591:
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jle	.L575
	movq	8(%rbx), %rdx
	movq	-8(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L575
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L575
	movq	352(%r13), %rsi
	subq	$8, %rdx
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	176(%r12), %r13
	movq	168(%r12), %r14
	testq	%r13, %r13
	je	.L595
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	leaq	8(%r13), %rbx
	testq	%rax, %rax
	je	.L596
	lock addl	$1, (%rbx)
	lock addl	$1, (%rbx)
.L598:
	movq	%r14, %rdi
	movq	%rax, -1160(%rbp)
	call	uv_mutex_lock@PLT
	leaq	40(%r14), %rax
	movq	%rax, -1168(%rbp)
	movq	-1160(%rbp), %rax
	testq	%rax, %rax
	je	.L652
	movl	$-1, %edx
	lock xaddl	%edx, (%rbx)
.L599:
	cmpl	$1, %edx
	je	.L653
.L600:
	movq	-1088(%rbp), %r8
	leaq	-1120(%rbp), %rbx
	leaq	-1136(%rbp), %r15
	movq	%rbx, -1136(%rbp)
	testq	%r8, %r8
	jne	.L654
	leaq	.LC7(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L647:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L572:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movl	-1160(%rbp), %eax
	movq	%r14, %rdi
	movl	%eax, 72(%r14)
	call	uv_mutex_unlock@PLT
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L581:
	addl	$1, 8(%r15)
	addl	$1, 8(%r15)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L650:
	movl	8(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L648:
	movl	8(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r15)
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L651:
	movq	(%r15), %rdx
	movq	%rax, -1160(%rbp)
	movq	%r15, %rdi
	call	*16(%rdx)
	movq	-1160(%rbp), %rax
	testq	%rax, %rax
	je	.L592
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L593:
	cmpl	$1, %eax
	jne	.L591
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L596:
	addl	$1, 8(%r13)
	addl	$1, 8(%r13)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L654:
	movq	%r8, %rdi
	movq	%r8, -1160(%rbp)
	call	strlen@PLT
	movq	-1160(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -1144(%rbp)
	movq	%rax, %r9
	ja	.L655
	cmpq	$1, %rax
	jne	.L604
	movzbl	(%r8), %edx
	movb	%dl, -1120(%rbp)
	movq	%rbx, %rdx
.L605:
	movq	%rax, -1128(%rbp)
	movq	-1168(%rbp), %rdi
	movq	%r15, %rsi
	movb	$0, (%rdx,%rax)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-1136(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L606
	call	_ZdlPv@PLT
.L606:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testq	%r13, %r13
	je	.L608
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L609
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r13)
.L610:
	cmpl	$1, %edx
	je	.L656
.L608:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L575
	testq	%rdi, %rdi
	je	.L575
	call	free@PLT
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L649:
	movq	(%r15), %rdx
	movq	%rax, -1176(%rbp)
	movq	%r15, %rdi
	call	*16(%rdx)
	movq	-1176(%rbp), %rax
	testq	%rax, %rax
	je	.L586
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r15)
.L587:
	cmpl	$1, %edx
	jne	.L588
	movq	(%r15), %rdx
	movq	%rax, -1176(%rbp)
	movq	%r15, %rdi
	call	*24(%rdx)
	movq	-1176(%rbp), %rax
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L604:
	testq	%r9, %r9
	jne	.L657
	movq	%rbx, %rdx
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L655:
	movq	%r15, %rdi
	leaq	-1144(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -1176(%rbp)
	movq	%r8, -1160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1160(%rbp), %r8
	movq	-1176(%rbp), %r9
	movq	%rax, -1136(%rbp)
	movq	%rax, %rdi
	movq	-1144(%rbp), %rax
	movq	%rax, -1120(%rbp)
.L603:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-1144(%rbp), %rax
	movq	-1136(%rbp), %rdx
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	leaq	40(%r14), %rax
	movq	%rax, -1168(%rbp)
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L609:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L652:
	movl	8(%r13), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r13)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L656:
	movq	0(%r13), %rdx
	movq	%rax, -1160(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-1160(%rbp), %rax
	testq	%rax, %rax
	je	.L612
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L613:
	cmpl	$1, %eax
	jne	.L608
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L653:
	movq	0(%r13), %rdx
	movq	%rax, -1160(%rbp)
	movq	%r13, %rdi
	call	*16(%rdx)
	movq	-1160(%rbp), %rax
	testq	%rax, %rax
	je	.L601
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L602:
	cmpl	$1, %eax
	jne	.L600
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L592:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L586:
	movl	12(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r15)
	jmp	.L587
.L612:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L613
.L601:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L602
.L646:
	call	__stack_chk_fail@PLT
.L657:
	movq	%rbx, %rdi
	jmp	.L603
	.cfi_endproc
.LFE7266:
	.size	_ZN4node9inspector12_GLOBAL__N_14OpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node9inspector12_GLOBAL__N_14OpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC8:
	.string	"consoleCall"
.LC9:
	.string	"setConsoleExtensionInstaller"
.LC10:
	.string	"callAndPauseOnStart"
.LC11:
	.string	"open"
.LC12:
	.string	"url"
.LC13:
	.string	"waitForDebugger"
.LC14:
	.string	"asyncTaskScheduled"
.LC15:
	.string	"asyncTaskCanceled"
.LC16:
	.string	"asyncTaskStarted"
.LC17:
	.string	"asyncTaskFinished"
.LC18:
	.string	"registerAsyncHook"
.LC19:
	.string	"isEnabled"
.LC20:
	.string	"Connection"
.LC21:
	.string	"dispatch"
.LC22:
	.string	"disconnect"
.LC23:
	.string	"MainThreadConnection"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB24:
	.text
.LHOTB24:
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7275:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L659
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L659
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L659
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	leaq	_ZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L710
.L660:
	movq	352(%rbx), %rdi
	movl	$11, %ecx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L711
.L661:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L712
.L662:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L713
.L663:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L714
.L664:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L715
.L665:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L716
.L666:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L717
.L667:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L718
.L668:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_14OpenERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L719
.L669:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L720
.L670:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L721
.L671:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node9inspector12_GLOBAL__N_13UrlERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L722
.L672:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L723
.L673:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L724
.L674:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_115WaitForDebuggerERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L725
.L675:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L726
.L676:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L727
.L677:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L728
.L678:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L729
.L679:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L730
.L680:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L731
.L681:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L732
.L682:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L733
.L683:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L734
.L684:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L735
.L685:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L736
.L686:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L737
.L687:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L738
.L688:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L739
.L689:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L740
.L690:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L741
.L691:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L742
.L692:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node9inspector12_GLOBAL__N_19IsEnabledERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r15
	movq	%rax, %r13
	popq	%rax
	testq	%r13, %r13
	je	.L743
.L693:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L744
.L694:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L745
.L695:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L746
.L696:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L747
.L697:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L748
.L698:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L749
.L699:
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L750
.L700:
	movq	352(%rbx), %rdi
	movl	$20, %ecx
	xorl	%edx, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L751
.L701:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L752
.L702:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10DisconnectERKN2v820FunctionCallbackInfoINS5_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L753
.L703:
	movq	%r13, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L754
.L704:
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L755
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L710:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L711:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L712:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L713:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L714:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L715:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L716:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L717:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L718:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L719:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L669
	.p2align 4,,10
	.p2align 3
.L720:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L721:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L722:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L723:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L724:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L725:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L726:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L727:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L677
	.p2align 4,,10
	.p2align 3
.L728:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L729:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L730:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L731:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L732:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L733:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L734:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L735:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L736:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L686
	.p2align 4,,10
	.p2align 3
.L737:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L738:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L739:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L740:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L741:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L742:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L743:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L744:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L745:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L746:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L749:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L750:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L751:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L755:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7275:
.L659:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7275:
	.text
	.size	_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE24:
	.text
.LHOTE24:
	.p2align 4
	.globl	_Z19_register_inspectorv
	.type	_Z19_register_inspectorv, @function
_Z19_register_inspectorv:
.LFB7276:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7276:
	.size	_Z19_register_inspectorv, .-_Z19_register_inspectorv
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEEE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEEE, 96
_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEED0Ev
	.quad	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE14MemoryInfoNameEv
	.quad	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEEE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEEE, 96
_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEED0Ev
	.quad	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE14MemoryInfoNameEv
	.quad	_ZNK4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateE, 40
_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegateD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateE, 40
_ZTVN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegateD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE25JSBindingsSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"../src/inspector_js_api.cc:141"
	.section	.rodata.str1.1
.LC26:
	.string	"info[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"static void node::inspector::{anonymous}::JSBindingsConnection<ConnectionType>::Dispatch(const v8::FunctionCallbackInfo<v8::Value>&) [with ConnectionType = node::inspector::{anonymous}::MainThreadConnection]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"../src/inspector_js_api.cc:121"
	.section	.rodata.str1.1
.LC29:
	.string	"info[0]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"static void node::inspector::{anonymous}::JSBindingsConnection<ConnectionType>::New(const v8::FunctionCallbackInfo<v8::Value>&) [with ConnectionType = node::inspector::{anonymous}::MainThreadConnection]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_20MainThreadConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"static void node::inspector::{anonymous}::JSBindingsConnection<ConnectionType>::Dispatch(const v8::FunctionCallbackInfo<v8::Value>&) [with ConnectionType = node::inspector::{anonymous}::LocalConnection]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE8DispatchERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC31
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"static void node::inspector::{anonymous}::JSBindingsConnection<ConnectionType>::New(const v8::FunctionCallbackInfo<v8::Value>&) [with ConnectionType = node::inspector::{anonymous}::LocalConnection]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_120JSBindingsConnectionINS1_15LocalConnectionEE3NewERKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC32
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC33:
	.string	"../src/util-inl.h:374"
.LC34:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC36:
	.string	"../src/util-inl.h:325"
.LC37:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"../src/inspector_js_api.cc:236"
	.section	.rodata.str1.1
.LC40:
	.string	"args[0]->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"void node::inspector::{anonymous}::InvokeAsyncTaskFnWithId(const v8::FunctionCallbackInfo<v8::Value>&) [with void (node::inspector::Agent::* asyncTaskFn)(void*) = &node::inspector::Agent::AsyncTaskFinished]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskFinishedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"void node::inspector::{anonymous}::InvokeAsyncTaskFnWithId(const v8::FunctionCallbackInfo<v8::Value>&) [with void (node::inspector::Agent::* asyncTaskFn)(void*) = &node::inspector::Agent::AsyncTaskStarted]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent16AsyncTaskStartedEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC42
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::inspector::{anonymous}::InvokeAsyncTaskFnWithId(const v8::FunctionCallbackInfo<v8::Value>&) [with void (node::inspector::Agent::* asyncTaskFn)(void*) = &node::inspector::Agent::AsyncTaskCanceled]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_1L23InvokeAsyncTaskFnWithIdIXadL_ZNS0_5Agent17AsyncTaskCanceledEPvEEEEvRKN2v820FunctionCallbackInfoINS5_5ValueEEEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/inspector_js_api.cc"
.LC45:
	.string	"inspector"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC44
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC45
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"../src/inspector_js_api.cc:264"
	.section	.rodata.str1.1
.LC47:
	.string	"args[1]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"void node::inspector::{anonymous}::RegisterAsyncHookWrapper(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"../src/inspector_js_api.cc:262"
	.section	.rodata.str1.1
.LC50:
	.string	"args[0]->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_1L24RegisterAsyncHookWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC48
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"../src/inspector_js_api.cc:253"
	.section	.rodata.str1.1
.LC52:
	.string	"args[2]->IsBoolean()"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"void node::inspector::{anonymous}::AsyncTaskScheduledWrapper(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"../src/inspector_js_api.cc:249"
	.section	.rodata.str1.1
.LC55:
	.string	"args[1]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC53
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"../src/inspector_js_api.cc:244"
	.section	.rodata.str1.1
.LC57:
	.string	"args[0]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_1L25AsyncTaskScheduledWrapperERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC53
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"../src/inspector_js_api.cc:214"
	.section	.rodata.str1.1
.LC59:
	.string	"node_method->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"void node::inspector::{anonymous}::InspectorConsoleCall(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../src/inspector_js_api.cc:199"
	.align 8
.LC62:
	.string	"inspector_method->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC60
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"../src/inspector_js_api.cc:195"
	.section	.rodata.str1.1
.LC64:
	.string	"(info.Length()) >= (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_120InspectorConsoleCallERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC60
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"../src/inspector_js_api.cc:180"
	.align 8
.LC66:
	.string	"void node::inspector::{anonymous}::CallAndPauseOnStart(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC65
	.quad	.LC50
	.quad	.LC66
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"../src/inspector_js_api.cc:179"
	.section	.rodata.str1.1
.LC68:
	.string	"(args.Length()) > (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_119CallAndPauseOnStartERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC66
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"../src/inspector_js_api.cc:172"
	.align 8
.LC70:
	.string	"void node::inspector::{anonymous}::SetConsoleExtensionInstaller(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC69
	.quad	.LC29
	.quad	.LC70
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"../src/inspector_js_api.cc:171"
	.section	.rodata.str1.1
.LC72:
	.string	"(info.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_128SetConsoleExtensionInstallerERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC71
	.quad	.LC72
	.quad	.LC70
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC73:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC75:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.quad	7956000642101826378
	.quad	7162252202994791271
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC5:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16
	.align 16
.LC6:
	.quad	0
	.quad	1024
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
