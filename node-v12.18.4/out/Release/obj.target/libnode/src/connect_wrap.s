	.file	"connect_wrap.cc"
	.text
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7098:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7541:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7541:
	.size	_ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node11ConnectWrap8SelfSizeEv,"axG",@progbits,_ZNK4node11ConnectWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11ConnectWrap8SelfSizeEv
	.type	_ZNK4node11ConnectWrap8SelfSizeEv, @function
_ZNK4node11ConnectWrap8SelfSizeEv:
.LFB7543:
	.cfi_startproc
	endbr64
	movl	$184, %eax
	ret
	.cfi_endproc
.LFE7543:
	.size	_ZNK4node11ConnectWrap8SelfSizeEv, .-_ZNK4node11ConnectWrap8SelfSizeEv
	.section	.text._ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv:
.LFB9490:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE9490:
	.size	_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI12uv_connect_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI12uv_connect_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI12uv_connect_sE6CancelEv
	.type	_ZN4node7ReqWrapI12uv_connect_sE6CancelEv, @function
_ZN4node7ReqWrapI12uv_connect_sE6CancelEv:
.LFB9489:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9489:
	.size	_ZN4node7ReqWrapI12uv_connect_sE6CancelEv, .-_ZN4node7ReqWrapI12uv_connect_sE6CancelEv
	.section	.text._ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv:
.LFB9506:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE9506:
	.size	_ZThn56_N4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI12uv_connect_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI12uv_connect_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI12uv_connect_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI12uv_connect_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI12uv_connect_sE6CancelEv:
.LFB9508:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9508:
	.size	_ZThn56_N4node7ReqWrapI12uv_connect_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI12uv_connect_sE6CancelEv
	.section	.text._ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev:
.LFB7542:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6301771065893744451, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7542:
	.size	_ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node11ConnectWrapD2Ev,"axG",@progbits,_ZN4node11ConnectWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node11ConnectWrapD1Ev
	.type	_ZThn56_N4node11ConnectWrapD1Ev, @function
_ZThn56_N4node11ConnectWrapD1Ev:
.LFB9507:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI12uv_connect_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L19
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9507:
	.size	_ZThn56_N4node11ConnectWrapD1Ev, .-_ZThn56_N4node11ConnectWrapD1Ev
	.section	.text._ZN4node11ConnectWrapD0Ev,"axG",@progbits,_ZN4node11ConnectWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node11ConnectWrapD0Ev
	.type	_ZThn56_N4node11ConnectWrapD0Ev, @function
_ZThn56_N4node11ConnectWrapD0Ev:
.LFB9510:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI12uv_connect_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L23
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9510:
	.size	_ZThn56_N4node11ConnectWrapD0Ev, .-_ZThn56_N4node11ConnectWrapD0Ev
	.section	.text._ZN4node11ConnectWrapD2Ev,"axG",@progbits,_ZN4node11ConnectWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11ConnectWrapD2Ev
	.type	_ZN4node11ConnectWrapD2Ev, @function
_ZN4node11ConnectWrapD2Ev:
.LFB9456:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI12uv_connect_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L29
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9456:
	.size	_ZN4node11ConnectWrapD2Ev, .-_ZN4node11ConnectWrapD2Ev
	.weak	_ZN4node11ConnectWrapD1Ev
	.set	_ZN4node11ConnectWrapD1Ev,_ZN4node11ConnectWrapD2Ev
	.section	.text._ZN4node11ConnectWrapD0Ev,"axG",@progbits,_ZN4node11ConnectWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11ConnectWrapD0Ev
	.type	_ZN4node11ConnectWrapD0Ev, @function
_ZN4node11ConnectWrapD0Ev:
.LFB9458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI12uv_connect_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L33
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9458:
	.size	_ZN4node11ConnectWrapD0Ev, .-_ZN4node11ConnectWrapD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11ConnectWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.type	_ZN4node11ConnectWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE, @function
_ZN4node11ConnectWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE:
.LFB7545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	.LC0(%rip), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r12)
	movq	%rax, 56(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	%rax, 72(%rbx)
	je	.L37
	movq	2112(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r12)
	leaq	16+_ZTVN4node11ConnectWrapE(%rip), %rax
	movq	%rdx, 64(%rbx)
	leaq	2112(%r12), %rdx
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rdx, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 56(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7545:
	.size	_ZN4node11ConnectWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE, .-_ZN4node11ConnectWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.globl	_ZN4node11ConnectWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.set	_ZN4node11ConnectWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE,_ZN4node11ConnectWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.weak	_ZTVN4node11ReqWrapBaseE
	.section	.data.rel.ro._ZTVN4node11ReqWrapBaseE,"awG",@progbits,_ZTVN4node11ReqWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11ReqWrapBaseE, @object
	.size	_ZTVN4node11ReqWrapBaseE, 48
_ZTVN4node11ReqWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node7ReqWrapI12uv_connect_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI12uv_connect_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI12uv_connect_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI12uv_connect_sEE, @object
	.size	_ZTVN4node7ReqWrapI12uv_connect_sEE, 160
_ZTVN4node7ReqWrapI12uv_connect_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI12uv_connect_sE6CancelEv
	.quad	_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI12uv_connect_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.weak	_ZTVN4node11ConnectWrapE
	.section	.data.rel.ro._ZTVN4node11ConnectWrapE,"awG",@progbits,_ZTVN4node11ConnectWrapE,comdat
	.align 8
	.type	_ZTVN4node11ConnectWrapE, @object
	.size	_ZTVN4node11ConnectWrapE, 160
_ZTVN4node11ConnectWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node11ConnectWrapD1Ev
	.quad	_ZN4node11ConnectWrapD0Ev
	.quad	_ZNK4node11ConnectWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node11ConnectWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node11ConnectWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI12uv_connect_sE6CancelEv
	.quad	_ZN4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node11ConnectWrapD1Ev
	.quad	_ZThn56_N4node11ConnectWrapD0Ev
	.quad	_ZThn56_N4node7ReqWrapI12uv_connect_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI12uv_connect_sE12GetAsyncWrapEv
	.weak	_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"../src/req_wrap-inl.h:28"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(false) == (persistent().IsEmpty())"
	.align 8
.LC3:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_connect_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI12uv_connect_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args, 24
_ZZN4node7ReqWrapI12uv_connect_sED4EvE4args:
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.weak	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC4:
	.string	"../src/req_wrap-inl.h:13"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC6:
	.string	"node::ReqWrapBase::ReqWrapBase(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, 24
_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
