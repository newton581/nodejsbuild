	.file	"tcp_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5790:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6068:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6068:
	.size	_ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node7TCPWrap8SelfSizeEv,"axG",@progbits,_ZNK4node7TCPWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TCPWrap8SelfSizeEv
	.type	_ZNK4node7TCPWrap8SelfSizeEv, @function
_ZNK4node7TCPWrap8SelfSizeEv:
.LFB6069:
	.cfi_startproc
	endbr64
	movl	$408, %eax
	ret
	.cfi_endproc
.LFE6069:
	.size	_ZNK4node7TCPWrap8SelfSizeEv, .-_ZNK4node7TCPWrap8SelfSizeEv
	.section	.text._ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_,"axG",@progbits,_ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_
	.type	_ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_, @function
_ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_:
.LFB8876:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	%rdx, %r9
	movq	(%rdi), %rax
	movq	(%rcx), %rdx
	movl	(%r9), %esi
	movq	(%r8), %rdi
	jmp	*%rax
	.cfi_endproc
.LFE8876:
	.size	_ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_, .-_ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_
	.section	.text._ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB8879:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L10
	cmpl	$2, %edx
	jne	.L12
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8879:
	.size	_ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.section	.text._ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_,"axG",@progbits,_ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_,comdat
	.p2align 4
	.weak	_ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_
	.type	_ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_, @function
_ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_:
.LFB8889:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	%rdx, %r9
	movq	(%rdi), %rax
	movq	(%rcx), %rdx
	movl	(%r9), %esi
	movq	(%r8), %rdi
	jmp	*%rax
	.cfi_endproc
.LFE8889:
	.size	_ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_, .-_ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_
	.section	.text._ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,"axG",@progbits,_ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation,comdat
	.p2align 4
	.weak	_ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.type	_ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation:
.LFB8891:
	.cfi_startproc
	endbr64
	cmpl	$1, %edx
	je	.L15
	cmpl	$2, %edx
	jne	.L17
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
.L17:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8891:
	.size	_ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation
	.text
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP11sockaddr_inE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP11sockaddr_inE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB8897:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L19
	cmpl	$3, %edx
	je	.L20
	cmpl	$1, %edx
	je	.L24
.L20:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8897:
	.size	_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP11sockaddr_inE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP11sockaddr_inE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP12sockaddr_in6E_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP12sockaddr_in6E_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation:
.LFB8912:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L26
	cmpl	$3, %edx
	je	.L27
	cmpl	$1, %edx
	je	.L31
.L27:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	(%rsi), %eax
	movl	%eax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE8912:
	.size	_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP12sockaddr_in6E_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP12sockaddr_in6E_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9869:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9869:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9873:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9873:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7603:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7603:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7602:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L45:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7602:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i
	.type	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i, @function
_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i:
.LFB9436:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rdx
	leaq	-88(%rdi), %rcx
	subl	$1, 2156(%rdx)
	js	.L51
	jmp	*80(%rcx)
	.p2align 4,,10
	.p2align 3
.L51:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9436:
	.size	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i, .-_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i
	.section	.text._ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,"axG",@progbits,_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.type	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, @function
_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_:
.LFB7534:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %rdi
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7534:
	.size	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, .-_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.text
	.p2align 4
	.type	_ZNSt17_Function_handlerIFiPKcP11sockaddr_inEZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_, @function
_ZNSt17_Function_handlerIFiPKcP11sockaddr_inEZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_:
.LFB8896:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	(%rdx), %rdx
	movl	(%rdi), %esi
	movq	(%r8), %rdi
	jmp	uv_ip4_addr@PLT
	.cfi_endproc
.LFE8896:
	.size	_ZNSt17_Function_handlerIFiPKcP11sockaddr_inEZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_, .-_ZNSt17_Function_handlerIFiPKcP11sockaddr_inEZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_
	.p2align 4
	.type	_ZNSt17_Function_handlerIFiPKcP12sockaddr_in6EZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_, @function
_ZNSt17_Function_handlerIFiPKcP12sockaddr_in6EZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_:
.LFB8911:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	movq	(%rdx), %rdx
	movl	(%rdi), %esi
	movq	(%r8), %rdi
	jmp	uv_ip6_addr@PLT
	.cfi_endproc
.LFE8911:
	.size	_ZNSt17_Function_handlerIFiPKcP12sockaddr_in6EZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_, .-_ZNSt17_Function_handlerIFiPKcP12sockaddr_in6EZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_
	.section	.text._ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev:
.LFB6070:
	.cfi_startproc
	endbr64
	movl	32(%rsi), %edx
	movq	%rdi, %rax
	cmpl	$31, %edx
	je	.L56
	cmpl	$32, %edx
	jne	.L57
	leaq	16(%rdi), %rdx
	movl	$1634883444, 24(%rdi)
	movabsq	$7308044150129050452, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$112, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	16(%rdi), %rdx
	movl	$1634883442, 24(%rdi)
	movabsq	$7311156824597611348, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$112, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
.L57:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node7TCPWrap14MemoryInfoNameB5cxx11EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE6070:
	.size	_ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node7TCPWrapD0Ev,"axG",@progbits,_ZN4node7TCPWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node7TCPWrapD0Ev
	.type	_ZThn88_N4node7TCPWrapD0Ev, @function
_ZThn88_N4node7TCPWrapD0Ev:
.LFB9964:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L62
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L63
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L65
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L81:
	cmpq	%rax, %rdx
	je	.L84
.L65:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L81
.L63:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L83:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L86:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L85
	movq	%rax, %rbx
.L69:
	testq	%rbx, %rbx
	jne	.L86
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%r12), %rdx
	subq	$88, %r12
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$408, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L84:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L62
	.cfi_endproc
.LFE9964:
	.size	_ZThn88_N4node7TCPWrapD0Ev, .-_ZThn88_N4node7TCPWrapD0Ev
	.section	.text._ZN4node7TCPWrapD2Ev,"axG",@progbits,_ZN4node7TCPWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node7TCPWrapD1Ev
	.type	_ZThn88_N4node7TCPWrapD1Ev, @function
_ZThn88_N4node7TCPWrapD1Ev:
.LFB9955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	48(%rdi), %rcx
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 40(%rdi)
	testq	%rcx, %rcx
	je	.L88
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L89
	leaq	40(%rdi), %rdx
	cmpq	%rax, %rdx
	jne	.L91
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L107:
	cmpq	%rax, %rdx
	je	.L110
.L91:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L107
.L89:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L109:
	movq	56(%rdi), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	8(%r12), %rbx
	movq	%rax, (%r12)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L112:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	8(%r12), %rax
	cmpq	%rbx, %rax
	je	.L111
	movq	%rax, %rbx
.L95:
	testq	%rbx, %rbx
	jne	.L112
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%r12), %rdx
	leaq	-88(%r12), %rdi
	movq	%rax, -88(%r12)
	movq	-24(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L110:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L88
	.cfi_endproc
.LFE9955:
	.size	_ZThn88_N4node7TCPWrapD1Ev, .-_ZThn88_N4node7TCPWrapD1Ev
	.align 2
	.p2align 4
	.weak	_ZN4node7TCPWrapD2Ev
	.type	_ZN4node7TCPWrapD2Ev, @function
_ZN4node7TCPWrapD2Ev:
.LFB9832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	136(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 128(%rdi)
	testq	%rdx, %rdx
	je	.L114
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L115
	leaq	128(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L117
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L133:
	cmpq	%rax, %rcx
	je	.L136
.L117:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L133
.L115:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	movq	144(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	96(%r12), %rbx
	movq	%rax, 88(%r12)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	96(%r12), %rax
	cmpq	%rbx, %rax
	je	.L137
	movq	%rax, %rbx
.L121:
	testq	%rbx, %rbx
	jne	.L138
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L136:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L114
	.cfi_endproc
.LFE9832:
	.size	_ZN4node7TCPWrapD2Ev, .-_ZN4node7TCPWrapD2Ev
	.weak	_ZN4node7TCPWrapD1Ev
	.set	_ZN4node7TCPWrapD1Ev,_ZN4node7TCPWrapD2Ev
	.section	.text._ZN4node7TCPWrapD0Ev,"axG",@progbits,_ZN4node7TCPWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7TCPWrapD0Ev
	.type	_ZN4node7TCPWrapD0Ev, @function
_ZN4node7TCPWrapD0Ev:
.LFB9834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	136(%rdi), %rdx
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 128(%rdi)
	testq	%rdx, %rdx
	je	.L140
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L141
	leaq	128(%rdi), %rcx
	cmpq	%rax, %rcx
	jne	.L143
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L159:
	cmpq	%rax, %rcx
	je	.L162
.L143:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L159
.L141:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L161:
	movq	144(%rdi), %rax
	movq	%rax, 8(%rdx)
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	96(%r12), %rbx
	movq	%rax, 88(%r12)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	96(%r12), %rax
	cmpq	%rbx, %rax
	je	.L163
	movq	%rax, %rbx
.L147:
	testq	%rbx, %rbx
	jne	.L164
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$408, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	16(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rax, 96(%r12)
	movups	%xmm0, 8(%rbx)
	movq	%rax, %rbx
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L162:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L140
	.cfi_endproc
.LFE9834:
	.size	_ZN4node7TCPWrapD0Ev, .-_ZN4node7TCPWrapD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movq	%rdi, %rbx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L166
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L181
.L166:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L182
	movq	8(%rbx), %rdi
.L168:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L183
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L179
	cmpw	$1040, %cx
	jne	.L170
.L179:
	movq	23(%rdx), %r12
.L172:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L173
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L174:
	call	_ZNK2v85Int325ValueEv@PLT
	testl	%eax, %eax
	jne	.L184
	movl	$32, %r14d
.L175:
	movq	8(%rbx), %r13
	movl	$408, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movl	%r14d, %ecx
	addq	$8, %r13
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%r13, %rdx
	call	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node7TCPWrapE(%rip), %rax
	leaq	160(%rbx), %rsi
	movq	%rax, (%rbx)
	addq	$208, %rax
	movq	%rax, 88(%rbx)
	movq	360(%r12), %rax
	movq	2360(%rax), %rdi
	call	uv_tcp_init@PLT
	testl	%eax, %eax
	jne	.L185
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L181:
	cmpl	$5, 43(%rax)
	jne	.L166
	leaq	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L184:
	cmpl	$1, %eax
	jne	.L186
	movl	$31, %r14d
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L173:
	movq	8(%rbx), %rdi
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L183:
	leaq	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	_ZZN4node7TCPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L172
.L186:
	leaq	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7643:
	.size	_ZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap10SetNoDelayERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap10SetNoDelayERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap10SetNoDelayERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L198
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L196
	cmpw	$1040, %cx
	jne	.L189
.L196:
	movq	23(%rdx), %r12
.L191:
	testq	%r12, %r12
	je	.L199
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L200
	movq	8(%rbx), %rdi
.L195:
	call	_ZNK2v85Value6IsTrueEv@PLT
	leaq	160(%r12), %rdi
	movzbl	%al, %esi
	call	uv_tcp_nodelay@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L200:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L199:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7663:
	.size	_ZN4node7TCPWrap10SetNoDelayERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap10SetNoDelayERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L213
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L211
	cmpw	$1040, %cx
	jne	.L203
.L211:
	movq	23(%rdx), %r12
.L205:
	testq	%r12, %r12
	je	.L214
	movl	16(%rbx), %edx
	movq	16(%r12), %rax
	testl	%edx, %edx
	jle	.L215
	movq	8(%rbx), %rdi
.L209:
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L201
	movq	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si@GOTPCREL(%rip), %rdx
	sarq	$32, %rsi
	leaq	160(%r12), %rdi
	call	uv_listen@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L201:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L203:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L214:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7675:
	.size	_ZN4node7TCPWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L228
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L226
	cmpw	$1040, %cx
	jne	.L218
.L226:
	movq	23(%rdx), %r12
.L220:
	movq	(%rbx), %rax
	testq	%r12, %r12
	je	.L229
	movq	8(%rax), %rdi
	movl	16(%rbx), %eax
	leaq	88(%rdi), %r13
	testl	%eax, %eax
	jg	.L230
.L224:
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L216
	movl	%edx, %esi
	leaq	160(%r12), %rdi
	call	uv_tcp_open@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L216:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	.cfi_restore_state
	movq	8(%rbx), %r13
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L229:
	movabsq	$-38654705664, %rcx
	movq	%rcx, 24(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7665:
	.size	_ZN4node7TCPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap12SetKeepAliveERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap12SetKeepAliveERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap12SetKeepAliveERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L245
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L243
	cmpw	$1040, %cx
	jne	.L233
.L243:
	movq	23(%rdx), %r12
.L235:
	testq	%r12, %r12
	je	.L246
	movl	16(%rbx), %edx
	movq	16(%r12), %rax
	testl	%edx, %edx
	jle	.L247
	movq	8(%rbx), %rdi
.L239:
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L231
	sarq	$32, %rsi
	cmpl	$1, 16(%rbx)
	movq	%rsi, %r13
	jg	.L241
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L242:
	call	_ZNK2v86Uint325ValueEv@PLT
	leaq	160(%r12), %rdi
	movl	%r13d, %esi
	movl	%eax, %edx
	call	uv_tcp_keepalive@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L231:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L247:
	.cfi_restore_state
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L241:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L246:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7664:
	.size	_ZN4node7TCPWrap12SetKeepAliveERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap12SetKeepAliveERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L249
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L249:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L258
.L250:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L250
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7528:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TCP"
.LC1:
	.string	"reading"
.LC2:
	.string	"open"
.LC3:
	.string	"bind"
.LC4:
	.string	"listen"
.LC5:
	.string	"connect"
.LC6:
	.string	"bind6"
.LC7:
	.string	"connect6"
.LC8:
	.string	"getsockname"
.LC9:
	.string	"getpeername"
.LC10:
	.string	"setNoDelay"
.LC11:
	.string	"setKeepAlive"
.LC12:
	.string	"TCPConnectWrap"
.LC13:
	.string	"SOCKET"
.LC15:
	.string	"SERVER"
.LC17:
	.string	"UV_TCP_IPV6ONLY"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB18:
	.text
.LHOTB18:
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L260
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L260
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L260
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$3, %ecx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L297
.L261:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	movl	$7, %ecx
	movq	%rax, -64(%rbp)
	leaq	120(%rdi), %r9
	movq	%r9, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L298
.L262:
	movq	%r9, %rdx
	movq	%r8, %rdi
	xorl	%ecx, %ecx
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	352(%rbx), %rax
	leaq	104(%rax), %rdx
	movq	360(%rbx), %rax
	movq	152(%rax), %rsi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	352(%rbx), %rax
	leaq	104(%rax), %rdx
	movq	360(%rbx), %rax
	movq	1144(%rax), %rsi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%rbx, %rdi
	call	_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L299
.L263:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L300
.L264:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap6ListenERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L301
.L265:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L302
.L266:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L303
.L267:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L304
.L268:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L305
.L269:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L306
.L270:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap10SetNoDelayERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L307
.L271:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TCPWrap12SetKeepAliveERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L308
.L272:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L309
.L273:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L310
.L274:
	movq	3248(%rbx), %rdi
	movq	352(%rbx), %r15
	testq	%rdi, %rdi
	je	.L275
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3248(%rbx)
.L275:
	testq	%r12, %r12
	je	.L276
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3248(%rbx)
.L276:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$14, %ecx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L311
.L277:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L312
.L278:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L313
.L279:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L314
.L280:
	movq	%r15, %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L315
.L281:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC15(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L316
.L282:
	movq	.LC16(%rip), %rax
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L317
.L283:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC17(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L318
.L284:
	movq	.LC16(%rip), %rax
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L319
.L285:
	movq	360(%rbx), %rax
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	352(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L320
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L298:
	movq	%r9, -72(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L302:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L313:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L315:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L317:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L319:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7642:
.L260:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7642:
	.text
	.size	_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE18:
	.text
.LHOTE18:
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE
	.type	_ZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE, @function
_ZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE:
.LFB7638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %r15
	movsd	40(%r12), %xmm0
	movq	1216(%r15), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	je	.L322
	comisd	.LC14(%rip), %xmm0
	jb	.L327
.L322:
	movq	1256(%r15), %rax
	movq	3248(%rbx), %rdi
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -120(%rbp)
	testq	%rdi, %rdi
	je	.L328
	movq	3280(%rbx), %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L329
	movq	352(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%rbx), %rsi
	leaq	-104(%rbp), %rcx
	movq	%r12, %rdi
	movl	$1, %edx
	movq	%rax, -104(%rbp)
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movsd	-120(%rbp), %xmm2
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	1256(%r15), %rax
	movsd	%xmm2, 24(%rax)
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L330
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L329:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	leaq	_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L330:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7638:
	.size	_ZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE, .-_ZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.type	_ZN4node7TCPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE, @function
_ZN4node7TCPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE:
.LFB7661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node7TCPWrapE(%rip), %rax
	leaq	160(%rbx), %rsi
	movq	%rax, (%rbx)
	addq	$208, %rax
	movq	%rax, 88(%rbx)
	movq	360(%r12), %rax
	movq	2360(%rax), %rdi
	call	uv_tcp_init@PLT
	testl	%eax, %eax
	jne	.L334
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	leaq	_ZZN4node7TCPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7661:
	.size	_ZN4node7TCPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE, .-_ZN4node7TCPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.globl	_ZN4node7TCPWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.set	_ZN4node7TCPWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE,_ZN4node7TCPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.p2align 4
	.globl	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE
	.type	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE, @function
_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE:
.LFB7687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	je	.L358
.L336:
	movzwl	0(%r13), %eax
	cmpw	$2, %ax
	je	.L337
	cmpw	$10, %ax
	jne	.L338
	leaq	-112(%rbp), %r15
	leaq	8(%r13), %rsi
	movl	$46, %ecx
	movl	$10, %edi
	movq	%r15, %rdx
	call	uv_inet_ntop@PLT
	movzwl	2(%r13), %r13d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	rolw	$8, %r13w
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movzwl	%r13w, %r13d
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L359
.L339:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	184(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L360
.L340:
	movq	360(%rbx), %rax
	movq	936(%rax), %rcx
.L357:
	movq	712(%rax), %rdx
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L361
.L346:
	movq	352(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1384(%rax), %rdx
.L353:
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L362
.L343:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L363
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L337:
	.cfi_restore_state
	leaq	-112(%rbp), %r15
	leaq	4(%r13), %rsi
	movl	$46, %ecx
	movl	$2, %edi
	movq	%r15, %rdx
	call	uv_inet_ntop@PLT
	movzwl	2(%r13), %r13d
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	rolw	$8, %r13w
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movzwl	%r13w, %r13d
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L364
.L344:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	184(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L365
.L345:
	movq	360(%rbx), %rax
	movq	928(%rax), %rcx
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L338:
	movq	352(%rbx), %rax
	leaq	128(%rax), %rcx
	movq	360(%rbx), %rax
	movq	184(%rax), %rdx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L358:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %r12
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L362:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L361:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L359:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rcx
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rcx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L365:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L360:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L340
.L363:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7687:
	.size	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE, .-_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE
	.section	.text._ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L382
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L380
	cmpw	$1040, %cx
	jne	.L368
.L380:
	movq	23(%rdx), %r13
.L370:
	testq	%r13, %r13
	je	.L383
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L384
	movq	8(%rbx), %rdi
.L374:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L385
	leaq	-176(%rbp), %r14
	leaq	-180(%rbp), %rdx
	movl	$128, -180(%rbp)
	leaq	160(%r13), %rdi
	movq	%r14, %rsi
	call	uv_tcp_getpeername@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L376
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L386
	movq	8(%rbx), %rdx
.L378:
	movq	16(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE
.L376:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
.L366:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L384:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L386:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L368:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L383:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L382:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8432:
	.size	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8431:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L404
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L402
	cmpw	$1040, %cx
	jne	.L390
.L402:
	movq	23(%rdx), %r13
.L392:
	testq	%r13, %r13
	je	.L405
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L406
	movq	8(%rbx), %rdi
.L396:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L407
	leaq	-176(%rbp), %r14
	leaq	-180(%rbp), %rdx
	movl	$128, -180(%rbp)
	leaq	160(%r13), %rdi
	movq	%r14, %rsi
	call	uv_tcp_getsockname@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L398
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L408
	movq	8(%rbx), %rdx
.L400:
	movq	16(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE
.L398:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
.L388:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L409
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L406:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L408:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L390:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L405:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L404:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L407:
	leaq	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L409:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8431:
	.size	_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.text
	.p2align 4
	.globl	_Z18_register_tcp_wrapv
	.type	_Z18_register_tcp_wrapv, @function
_Z18_register_tcp_wrapv:
.LFB7688:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7688:
	.size	_Z18_register_tcp_wrapv, .-_Z18_register_tcp_wrapv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L428
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L429
.L414:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L418
.L411:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L429:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L419
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L415:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L414
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L420
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L416:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L418:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L411
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L428:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L419:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L420:
	xorl	%edx, %edx
	jmp	.L416
	.cfi_endproc
.LFE8393:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L431
	leaq	16(%r12), %rsi
.L432:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L433
	addq	$16, %r12
.L434:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L435
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L451
.L435:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L452
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L439
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L453
.L430:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L454
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L451:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L433:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L431:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L453:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L430
	.p2align 4,,10
	.p2align 3
.L452:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L442
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L437:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L439:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	xorl	%edx, %edx
	jmp	.L437
.L454:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7633:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L456
	leaq	40(%r12), %rsi
.L457:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L458
	addq	$40, %r12
.L459:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L460
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L476
.L460:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L477
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L464
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L478
.L455:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L479
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L478:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L467
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L462:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L464:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L467:
	xorl	%edx, %edx
	jmp	.L462
.L479:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7635:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.section	.text._ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE,"axG",@progbits,_ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE
	.type	_ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE, @function
_ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE:
.LFB8458:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1128, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L518
	movq	(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L504
	cmpw	$1040, %cx
	jne	.L482
.L504:
	movq	23(%rdx), %r14
.L484:
	testq	%r14, %r14
	je	.L519
	movl	16(%rbx), %eax
	movq	16(%r14), %r15
	testl	%eax, %eax
	jle	.L520
	movq	8(%rbx), %rdx
.L488:
	movq	352(%r15), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jg	.L489
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L490:
	movq	3280(%r15), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L501
	sarq	$32, %rax
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	cmpl	$10, %r13d
	je	.L521
.L492:
	movq	-1088(%rbp), %rax
	leaq	-1120(%rbp), %r13
	cmpq	$0, 16(%r12)
	movl	%edx, -1140(%rbp)
	movq	%r13, -1128(%rbp)
	movq	%rax, -1136(%rbp)
	je	.L522
	movl	%r8d, -1160(%rbp)
	leaq	-1128(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-1140(%rbp), %rdx
	leaq	-1136(%rbp), %rsi
	call	*24(%r12)
	movl	-1160(%rbp), %r8d
	testl	%eax, %eax
	je	.L523
.L497:
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	-1088(%rbp), %rdi
	movq	%rax, 24(%rdx)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L480
.L517:
	testq	%rdi, %rdi
	je	.L480
	call	free@PLT
.L480:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L524
	addq	$1128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L489:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L501:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L517
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L521:
	cmpl	$2, 16(%rbx)
	jg	.L493
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L494:
	movq	3280(%r15), %rsi
	movq	%rdx, -1160(%rbp)
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r8
	testb	%al, %al
	je	.L501
	movq	-1160(%rbp), %rdx
	shrq	$32, %r8
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L519:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L523:
	leaq	160(%r14), %rdi
	movl	%r8d, %edx
	movq	%r13, %rsi
	call	uv_tcp_bind@PLT
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L518:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L493:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L494
.L524:
	call	__stack_chk_fail@PLT
.L522:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE8458:
	.size	_ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE, .-_ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIPFiPKciP11sockaddr_inEE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	movl	$2, %esi
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-64(%rbp), %r12
	movq	%r12, %rdx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	uv_ip4_addr@GOTPCREL(%rip), %rax
	movq	%rax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFiPKciP11sockaddr_inEPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node7TCPWrap4BindI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L525
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L525:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L532
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L532:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7667:
	.size	_ZN4node7TCPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE,"axG",@progbits,_ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE
	.type	_ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE, @function
_ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE:
.LFB8469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1144, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L571
	movq	(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L557
	cmpw	$1040, %cx
	jne	.L535
.L557:
	movq	23(%rdx), %r14
.L537:
	testq	%r14, %r14
	je	.L572
	movl	16(%rbx), %eax
	movq	16(%r14), %r15
	testl	%eax, %eax
	jle	.L573
	movq	8(%rbx), %rdx
.L541:
	movq	352(%r15), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jg	.L542
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L543:
	movq	3280(%r15), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L554
	sarq	$32, %rax
	xorl	%r8d, %r8d
	movq	%rax, %rdx
	cmpl	$10, %r13d
	je	.L574
.L545:
	movq	-1088(%rbp), %rax
	leaq	-1136(%rbp), %r13
	cmpq	$0, 16(%r12)
	movl	%edx, -1156(%rbp)
	movq	%r13, -1144(%rbp)
	movq	%rax, -1152(%rbp)
	je	.L575
	movl	%r8d, -1176(%rbp)
	leaq	-1144(%rbp), %rcx
	movq	%r12, %rdi
	leaq	-1156(%rbp), %rdx
	leaq	-1152(%rbp), %rsi
	call	*24(%r12)
	movl	-1176(%rbp), %r8d
	testl	%eax, %eax
	je	.L576
.L550:
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	-1088(%rbp), %rdi
	movq	%rax, 24(%rdx)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L533
.L570:
	testq	%rdi, %rdi
	je	.L533
	call	free@PLT
.L533:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L577
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L542:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L554:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L570
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L574:
	cmpl	$2, 16(%rbx)
	jg	.L546
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L547:
	movq	3280(%r15), %rsi
	movq	%rdx, -1176(%rbp)
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r8
	testb	%al, %al
	je	.L554
	movq	-1176(%rbp), %rdx
	shrq	$32, %r8
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L572:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L576:
	leaq	160(%r14), %rdi
	movl	%r8d, %edx
	movq	%r13, %rsi
	call	uv_tcp_bind@PLT
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L546:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L547
.L577:
	call	__stack_chk_fail@PLT
.L575:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE8469:
	.size	_ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE, .-_ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZNSt14_Function_base13_Base_managerIPFiPKciP12sockaddr_in6EE10_M_managerERSt9_Any_dataRKS8_St18_Manager_operation(%rip), %rcx
	movl	$10, %esi
	movq	%rcx, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-64(%rbp), %r12
	movq	%r12, %rdx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	uv_ip6_addr@GOTPCREL(%rip), %rax
	movq	%rax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFiPKciP12sockaddr_in6EPS4_E9_M_invokeERKSt9_Any_dataOS1_OiOS3_(%rip), %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node7TCPWrap4BindI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEiSt8functionIFiPKciPT_EE
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L578
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L578:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L585
	addq	$56, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L585:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7671:
	.size	_ZN4node7TCPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE,"axG",@progbits,_ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE,comdat
	.p2align 4
	.weak	_ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE
	.type	_ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE, @function
_ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE:
.LFB8477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1128, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L614
	cmpw	$1040, %cx
	jne	.L587
.L614:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L623
.L590:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L615
	cmpw	$1040, %cx
	jne	.L591
.L615:
	movq	23(%rdx), %r12
.L593:
	testq	%r12, %r12
	je	.L624
	movl	16(%r14), %edx
	testl	%edx, %edx
	jle	.L625
	movq	8(%r14), %rdi
.L597:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L626
	movl	16(%r14), %eax
	cmpl	$1, %eax
	jg	.L599
	movq	(%r14), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
.L600:
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L627
.L601:
	leaq	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L625:
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L627:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L601
	testl	%eax, %eax
	jg	.L603
	movq	(%r14), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -1144(%rbp)
.L604:
	movq	352(%r13), %rsi
	leaq	-1104(%rbp), %rdi
	leaq	-1120(%rbp), %r15
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %rax
	cmpq	$0, 16(%rbx)
	movq	%r15, -1128(%rbp)
	movq	%rax, -1136(%rbp)
	je	.L628
	movq	%rbx, %rdi
	leaq	-1128(%rbp), %rdx
	leaq	-1136(%rbp), %rsi
	call	*24(%rbx)
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L629
.L606:
	movq	(%r14), %rax
	salq	$32, %rbx
	movq	-1088(%rbp), %rdi
	movq	%rbx, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L586
	testq	%rdi, %rdi
	je	.L586
	call	free@PLT
.L586:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L630
	addq	$1128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	movq	8(%r14), %rcx
	leaq	-8(%rcx), %rdx
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L603:
	movq	8(%r14), %rax
	movq	%rax, -1144(%rbp)
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r14), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L590
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L629:
	movq	16(%r12), %r10
	movsd	40(%r12), %xmm0
	movq	1216(%r10), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	je	.L607
	comisd	.LC14(%rip), %xmm0
	jb	.L631
.L607:
	movq	1256(%r10), %rax
	movl	$184, %edi
	movq	%r10, -1152(%rbp)
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -1160(%rbp)
	call	_Znwm@PLT
	movq	-1144(%rbp), %rdx
	movq	%r13, %rsi
	movl	$30, %ecx
	movq	%rax, %rdi
	movq	%rax, -1144(%rbp)
	call	_ZN4node11ConnectWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE@PLT
	movq	-1144(%rbp), %r9
	movq	-1152(%rbp), %r10
	leaq	160(%r12), %rsi
	cmpq	$0, 80(%r9)
	movq	%r9, 88(%r9)
	jne	.L632
	movq	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si@GOTPCREL(%rip), %rax
	leaq	88(%r9), %rdi
	movq	%r15, %rdx
	leaq	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i(%rip), %rcx
	movq	%r10, -1152(%rbp)
	movq	%rax, 80(%r9)
	movq	%r9, -1144(%rbp)
	call	uv_tcp_connect@PLT
	movq	-1144(%rbp), %r9
	movq	-1152(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L611
	movq	16(%r9), %rax
	addl	$1, 2156(%rax)
	testl	%ebx, %ebx
	jne	.L611
.L610:
	movq	1256(%r10), %rax
	movsd	-1160(%rbp), %xmm2
	movsd	%xmm2, 24(%rax)
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L624:
	movabsq	$-38654705664, %rbx
	movq	(%r14), %rax
	movq	%rbx, 24(%rax)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L611:
	movq	(%r9), %rax
	movq	%r10, -1144(%rbp)
	movq	%r9, %rdi
	call	*8(%rax)
	movq	-1144(%rbp), %r10
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L631:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L630:
	call	__stack_chk_fail@PLT
.L628:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE8477:
	.size	_ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE, .-_ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpl	$2, 16(%rdi)
	jg	.L634
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L645
.L636:
	cmpl	$2, 16(%r12)
	jle	.L646
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
.L638:
	call	_ZNK2v86Uint325ValueEv@PLT
	leaq	_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP11sockaddr_inE_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rcx
	leaq	-64(%rbp), %r13
	movq	%r12, %rdi
	movl	%eax, -64(%rbp)
	leaq	_ZNSt17_Function_handlerIFiPKcP11sockaddr_inEZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_(%rip), %rax
	movq	%rcx, %xmm0
	movq	%r13, %rsi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L633
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L633:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L647
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L634:
	.cfi_restore_state
	movq	8(%rdi), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	jne	.L636
.L645:
	leaq	_ZZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L646:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L638
.L647:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7676:
	.size	_ZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE,"axG",@progbits,_ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE,comdat
	.p2align 4
	.weak	_ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE
	.type	_ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE, @function
_ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE:
.LFB8489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1144, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L676
	cmpw	$1040, %cx
	jne	.L649
.L676:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L685
.L652:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L677
	cmpw	$1040, %cx
	jne	.L653
.L677:
	movq	23(%rdx), %r12
.L655:
	testq	%r12, %r12
	je	.L686
	movl	16(%r14), %edx
	testl	%edx, %edx
	jle	.L687
	movq	8(%r14), %rdi
.L659:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L688
	movl	16(%r14), %eax
	cmpl	$1, %eax
	jg	.L661
	movq	(%r14), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
.L662:
	movq	(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L689
.L663:
	leaq	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L687:
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L689:
	movq	-1(%rcx), %rcx
	cmpw	$63, 11(%rcx)
	ja	.L663
	testl	%eax, %eax
	jg	.L665
	movq	(%r14), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -1160(%rbp)
.L666:
	movq	352(%r13), %rsi
	leaq	-1104(%rbp), %rdi
	leaq	-1136(%rbp), %r15
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %rax
	cmpq	$0, 16(%rbx)
	movq	%r15, -1144(%rbp)
	movq	%rax, -1152(%rbp)
	je	.L690
	movq	%rbx, %rdi
	leaq	-1144(%rbp), %rdx
	leaq	-1152(%rbp), %rsi
	call	*24(%rbx)
	movl	%eax, %ebx
	testl	%eax, %eax
	je	.L691
.L668:
	movq	(%r14), %rax
	salq	$32, %rbx
	movq	-1088(%rbp), %rdi
	movq	%rbx, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L648
	testq	%rdi, %rdi
	je	.L648
	call	free@PLT
.L648:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L692
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	.cfi_restore_state
	movq	8(%r14), %rcx
	leaq	-8(%rcx), %rdx
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L665:
	movq	8(%r14), %rax
	movq	%rax, -1160(%rbp)
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L649:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r14), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L652
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L691:
	movq	16(%r12), %r10
	movsd	40(%r12), %xmm0
	movq	1216(%r10), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	je	.L669
	comisd	.LC14(%rip), %xmm0
	jb	.L693
.L669:
	movq	1256(%r10), %rax
	movl	$184, %edi
	movq	%r10, -1168(%rbp)
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -1176(%rbp)
	call	_Znwm@PLT
	movq	-1160(%rbp), %rdx
	movq	%r13, %rsi
	movl	$30, %ecx
	movq	%rax, %rdi
	movq	%rax, -1160(%rbp)
	call	_ZN4node11ConnectWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeE@PLT
	movq	-1160(%rbp), %r9
	movq	-1168(%rbp), %r10
	leaq	160(%r12), %rsi
	cmpq	$0, 80(%r9)
	movq	%r9, 88(%r9)
	jne	.L694
	movq	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si@GOTPCREL(%rip), %rax
	leaq	88(%r9), %rdi
	movq	%r15, %rdx
	leaq	_ZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE7WrapperES2_i(%rip), %rcx
	movq	%r10, -1168(%rbp)
	movq	%rax, 80(%r9)
	movq	%r9, -1160(%rbp)
	call	uv_tcp_connect@PLT
	movq	-1160(%rbp), %r9
	movq	-1168(%rbp), %r10
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L673
	movq	16(%r9), %rax
	addl	$1, 2156(%rax)
	testl	%ebx, %ebx
	jne	.L673
.L672:
	movq	1256(%r10), %rax
	movsd	-1176(%rbp), %xmm2
	movsd	%xmm2, 24(%rax)
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L686:
	movabsq	$-38654705664, %rbx
	movq	(%r14), %rax
	movq	%rbx, 24(%rax)
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L673:
	movq	(%r9), %rax
	movq	%r10, -1160(%rbp)
	movq	%r9, %rdi
	call	*8(%rax)
	movq	-1160(%rbp), %r10
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L693:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L692:
	call	__stack_chk_fail@PLT
.L690:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE8489:
	.size	_ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE, .-_ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L708
	cmpw	$1040, %cx
	jne	.L696
.L708:
	movq	23(%rdx), %rbx
.L698:
	cmpl	$2, 16(%r12)
	jg	.L699
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L713
.L701:
	cmpl	$2, 16(%r12)
	jle	.L714
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
.L703:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L695
	sarq	$32, %rax
	leaq	_ZNSt14_Function_base13_Base_managerIZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPKcP12sockaddr_in6E_E10_M_managerERSt9_Any_dataRKSF_St18_Manager_operation(%rip), %rbx
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	movl	%eax, -80(%rbp)
	leaq	_ZNSt17_Function_handlerIFiPKcP12sockaddr_in6EZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS1_S3_E_E9_M_invokeERKSt9_Any_dataOS1_OS3_(%rip), %rax
	movq	%rbx, %xmm0
	movq	%r13, %rsi
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EE
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L695
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	*%rax
.L695:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L715
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L714:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L699:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	jne	.L701
.L713:
	leaq	_ZZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L696:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L698
.L715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7681:
	.size	_ZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.weak	_ZTVN4node7TCPWrapE
	.section	.data.rel.ro._ZTVN4node7TCPWrapE,"awG",@progbits,_ZTVN4node7TCPWrapE,comdat
	.align 8
	.type	_ZTVN4node7TCPWrapE, @object
	.size	_ZTVN4node7TCPWrapE, 368
_ZTVN4node7TCPWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node7TCPWrapD1Ev
	.quad	_ZN4node7TCPWrapD0Ev
	.quad	_ZNK4node7TCPWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node7TCPWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node7TCPWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node15LibuvStreamWrap5GetFDEv
	.quad	_ZN4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZN4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZN4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZN4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	-88
	.quad	0
	.quad	_ZThn88_N4node7TCPWrapD1Ev
	.quad	_ZThn88_N4node7TCPWrapD0Ev
	.quad	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.quad	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/req_wrap-inl.h:130"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC20:
	.string	"(req_wrap->original_callback_) == nullptr"
	.align 8
.LC21:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, void (*)"
	.ascii	"(ReqT*, Args"
	.string	" ...)>::For(node::ReqWrap<T>*, node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_connect_s; Args = {int}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_connect_s*, int)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI12uv_connect_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.weak	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0
	.section	.rodata.str1.1
.LC22:
	.string	"../src/tcp_wrap.cc:311"
.LC23:
	.string	"args[1]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"static void node::TCPWrap::Connect(const v8::FunctionCallbackInfo<v8::Value>&, std::function<int(const char*, T*)>) [with T = sockaddr_in6]"
	.section	.data.rel.ro.local._ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0,"awG",@progbits,_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0,comdat
	.align 16
	.type	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0, @gnu_unique_object
	.size	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0, 24
_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.weak	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args
	.section	.rodata.str1.1
.LC25:
	.string	"../src/tcp_wrap.cc:310"
.LC26:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local._ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args,"awG",@progbits,_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args,comdat
	.align 16
	.type	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args, @gnu_unique_object
	.size	_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args, 24
_ZZN4node7TCPWrap7ConnectI12sockaddr_in6EEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC24
	.weak	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"static void node::TCPWrap::Connect(const v8::FunctionCallbackInfo<v8::Value>&, std::function<int(const char*, T*)>) [with T = sockaddr_in]"
	.section	.data.rel.ro.local._ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0,"awG",@progbits,_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0,comdat
	.align 16
	.type	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0, @gnu_unique_object
	.size	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0, 24
_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args_0:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC27
	.weak	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args
	.section	.data.rel.ro.local._ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args,"awG",@progbits,_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args,comdat
	.align 16
	.type	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args, @gnu_unique_object
	.size	_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args, 24
_ZZN4node7TCPWrap7ConnectI11sockaddr_inEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEESt8functionIFiPKcPT_EEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.weak	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC28:
	.string	"../src/node_internals.h:78"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"void node::GetSockOrPeerName(const v8::FunctionCallbackInfo<v8::Value>&) [with T = node::TCPWrap; int (* F)(const typename T::HandleType*, sockaddr*, int*) = uv_tcp_getpeername]"
	.section	.data.rel.ro.local._ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC28
	.quad	.LC26
	.quad	.LC29
	.weak	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"void node::GetSockOrPeerName(const v8::FunctionCallbackInfo<v8::Value>&) [with T = node::TCPWrap; int (* F)(const typename T::HandleType*, sockaddr*, int*) = uv_tcp_getsockname]"
	.section	.data.rel.ro.local._ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node17GetSockOrPeerNameINS_7TCPWrapEXadL_Z18uv_tcp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC28
	.quad	.LC26
	.quad	.LC30
	.section	.rodata.str1.1
.LC31:
	.string	"../src/tcp_wrap.cc"
.LC32:
	.string	"tcp_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC31
	.quad	0
	.quad	_ZN4node7TCPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC32
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC33:
	.string	"../src/tcp_wrap.cc:291"
.LC34:
	.string	"args[2]->IsUint32()"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"static void node::TCPWrap::Connect6(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TCPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.section	.rodata.str1.1
.LC36:
	.string	"../src/tcp_wrap.cc:279"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"static void node::TCPWrap::Connect(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TCPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC36
	.quad	.LC34
	.quad	.LC37
	.section	.rodata.str1.1
.LC38:
	.string	"../src/tcp_wrap.cc:163"
.LC39:
	.string	"(r) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"node::TCPWrap::TCPWrap(node::Environment*, v8::Local<v8::Object>, node::AsyncWrap::ProviderType)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TCPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEE4args, @object
	.size	_ZZN4node7TCPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEE4args, 24
_ZZN4node7TCPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_9AsyncWrap12ProviderTypeEE4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.section	.rodata.str1.1
.LC41:
	.string	"../src/tcp_wrap.cc:153"
.LC42:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"static void node::TCPWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/tcp_wrap.cc:138"
.LC45:
	.string	"args[0]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC43
	.section	.rodata.str1.1
.LC46:
	.string	"../src/tcp_wrap.cc:137"
.LC47:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TCPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC43
	.section	.rodata.str1.1
.LC48:
	.string	"../src/tcp_wrap.cc:63"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"(constructor.IsEmpty()) == (false)"
	.align 8
.LC50:
	.string	"static v8::MaybeLocal<v8::Object> node::TCPWrap::Instantiate(node::Environment*, node::AsyncWrap*, node::TCPWrap::SocketType)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0, @object
	.size	_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0, 24
_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args_0:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.section	.rodata.str1.1
.LC51:
	.string	"../src/tcp_wrap.cc:59"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"(env->tcp_constructor_template().IsEmpty()) == (false)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args, @object
	.size	_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args, 24
_ZZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC50
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC53:
	.string	"../src/stream_base-inl.h:103"
.LC54:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC56:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC58:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC59:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC59
	.quad	.LC57
	.quad	.LC60
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC61:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC63:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC64:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC66:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC67:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC69:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC70:
	.string	"../src/env-inl.h:399"
.LC71:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC73:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC75:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.weak	_ZZNK4node7TCPWrap14MemoryInfoNameB5cxx11EvE4args
	.section	.rodata.str1.1
.LC76:
	.string	"../src/tcp_wrap.h:58"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"virtual std::string node::TCPWrap::MemoryInfoName() const"
	.section	.data.rel.ro.local._ZZNK4node7TCPWrap14MemoryInfoNameB5cxx11EvE4args,"awG",@progbits,_ZZNK4node7TCPWrap14MemoryInfoNameB5cxx11EvE4args,comdat
	.align 16
	.type	_ZZNK4node7TCPWrap14MemoryInfoNameB5cxx11EvE4args, @gnu_unique_object
	.size	_ZZNK4node7TCPWrap14MemoryInfoNameB5cxx11EvE4args, 24
_ZZNK4node7TCPWrap14MemoryInfoNameB5cxx11EvE4args:
	.quad	.LC76
	.quad	.LC42
	.quad	.LC77
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC14:
	.long	0
	.long	0
	.align 8
.LC16:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
