	.file	"connection_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5790:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9716:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9716:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9720:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9720:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7602:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L13:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7602:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7601:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7601:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L21:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L30
.L22:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L22
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7141:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.text._ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE,"axG",@progbits,_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC5EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.type	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE, @function
_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE:
.LFB8383:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	leaq	160(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$208, %rax
	movq	%rax, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8383:
	.size	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE, .-_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.weak	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC1EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.set	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC1EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE,_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.section	.text._ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE,"axG",@progbits,_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC5EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.type	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE, @function
_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE:
.LFB8386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%ecx, %r8d
	leaq	160(%rdi), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$208, %rax
	movq	%rax, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8386:
	.size	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE, .-_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.weak	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC1EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.set	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC1EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE,_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEC2EPNS_11EnvironmentEN2v85LocalINS6_6ObjectEEENS_9AsyncWrap12ProviderTypeE
	.section	.text._ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si,"axG",@progbits,_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si,comdat
	.p2align 4
	.weak	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si
	.type	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si, @function
_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si:
.LFB8388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L74
	leaq	160(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L75
	movq	16(%r12), %rbx
	movl	%esi, %r13d
	leaq	-112(%rbp), %r15
	movq	%rdi, -120(%rbp)
	movq	%r15, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpq	$0, 8(%r12)
	movq	-120(%rbp), %r8
	je	.L76
	testl	%r13d, %r13d
	je	.L77
	movq	352(%rbx), %rdi
	leaq	88(%rdi), %rdx
.L46:
	movl	%r13d, %esi
	movq	%rdx, -120(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-120(%rbp), %rdx
	movq	8(%r12), %rdi
	movq	%rax, -80(%rbp)
	movq	360(%rbx), %rax
	movq	%rdx, -72(%rbp)
	movq	16(%r12), %rdx
	movq	1144(%rax), %r13
	testq	%rdi, %rdi
	je	.L48
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L78
.L48:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L51
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L79
.L51:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	je	.L51
	movq	%rax, %rdi
	movq	%r8, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r8
	testl	%eax, %eax
	jle	.L81
	movq	(%rdx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L57
	cmpw	$1040, %si
	jne	.L42
.L57:
	movq	23(%rcx), %rax
.L44:
	movq	%rdx, -120(%rbp)
	testq	%rax, %rax
	je	.L51
	leaq	160(%rax), %rsi
	movq	%r8, %rdi
	call	uv_accept@PLT
	testl	%eax, %eax
	jne	.L51
	movq	352(%rbx), %rdi
	movq	-120(%rbp), %rdx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L78:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L74:
	leaq	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	movq	%r8, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	jmp	.L44
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8388:
	.size	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si, .-_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_si
	.section	.text._ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si,"axG",@progbits,_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si,comdat
	.p2align 4
	.weak	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si
	.type	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si, @function
_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si:
.LFB8390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L121
	leaq	160(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L122
	movq	16(%r12), %rbx
	movl	%esi, %r13d
	leaq	-112(%rbp), %r15
	movq	%rdi, -120(%rbp)
	movq	%r15, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpq	$0, 8(%r12)
	movq	-120(%rbp), %r8
	je	.L123
	testl	%r13d, %r13d
	je	.L124
	movq	352(%rbx), %rdi
	leaq	88(%rdi), %rdx
.L93:
	movl	%r13d, %esi
	movq	%rdx, -120(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-120(%rbp), %rdx
	movq	8(%r12), %rdi
	movq	%rax, -80(%rbp)
	movq	360(%rbx), %rax
	movq	%rdx, -72(%rbp)
	movq	16(%r12), %rdx
	movq	1144(%rax), %r13
	testq	%rdi, %rdi
	je	.L95
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L125
.L95:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L98
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L126
.L98:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	je	.L98
	movq	%rax, %rdi
	movq	%r8, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r8
	testl	%eax, %eax
	jle	.L128
	movq	(%rdx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L104
	cmpw	$1040, %si
	jne	.L89
.L104:
	movq	23(%rcx), %rax
.L91:
	movq	%rdx, -120(%rbp)
	testq	%rax, %rax
	je	.L98
	leaq	160(%rax), %rsi
	movq	%r8, %rdi
	call	uv_accept@PLT
	testl	%eax, %eax
	jne	.L98
	movq	352(%rbx), %rdi
	movq	-120(%rbp), %rdx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L125:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	leaq	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	movq	%r8, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %rdx
	jmp	.L91
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8390:
	.size	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si, .-_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_si
	.section	.text._ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si,"axG",@progbits,_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si,comdat
	.p2align 4
	.weak	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si
	.type	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si, @function
_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si:
.LFB8391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L162
	movq	72(%rdi), %rax
	movq	(%rax), %rbx
	movq	16(%rbx), %r15
	cmpq	%r15, 16(%r12)
	jne	.L163
	movl	%esi, -144(%rbp)
	leaq	-128(%rbp), %r14
	movq	352(%r15), %rsi
	movq	%rdi, -136(%rbp)
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r15), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpq	$0, 8(%r12)
	movq	-136(%rbp), %rcx
	movl	-144(%rbp), %r8d
	je	.L164
	cmpq	$0, 8(%rbx)
	je	.L165
	movb	$0, -144(%rbp)
	movb	$0, -136(%rbp)
	testl	%r8d, %r8d
	je	.L166
.L134:
	movq	352(%r15), %rdi
	movl	%r8d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, -96(%rbp)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L135
	movzbl	11(%rax), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L167
.L135:
	movq	%rax, -88(%rbp)
	movq	16(%r12), %rsi
	movq	8(%r12), %rax
	movq	352(%rsi), %r8
	testq	%rax, %rax
	je	.L148
	movzbl	11(%rax), %ecx
	movq	%rax, %rdi
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L168
.L136:
	movq	352(%r15), %rcx
	movq	%rax, -80(%rbp)
	leaq	112(%rcx), %rax
	addq	$120, %rcx
	cmpb	$0, -136(%rbp)
	movq	%rax, %r9
	cmove	%rcx, %r9
	cmpb	$0, -144(%rbp)
	cmovne	%rax, %rcx
	movq	360(%r15), %rax
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	1136(%rax), %r15
	testq	%rdi, %rdi
	je	.L141
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L169
.L141:
	movq	3280(%rsi), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L144
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L170
.L144:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L171
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L166:
	.cfi_restore_state
	movq	72(%rcx), %rdi
	movl	%r8d, -148(%rbp)
	movq	%rcx, -144(%rbp)
	call	uv_is_readable@PLT
	movq	-144(%rbp), %rcx
	testl	%eax, %eax
	movq	72(%rcx), %rdi
	setne	-136(%rbp)
	call	uv_is_writable@PLT
	movl	-148(%rbp), %r8d
	testl	%eax, %eax
	setne	-144(%rbp)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L169:
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L167:
	movq	16(%rbx), %rcx
	movq	(%rax), %rsi
	movq	352(%rcx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L168:
	movq	(%rax), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rsi
	movq	8(%r12), %rdi
	movq	352(%rsi), %r8
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L170:
	leaq	-96(%rbp), %rcx
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%edi, %edi
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L162:
	leaq	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L163:
	leaq	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L171:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8391:
	.size	_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si, .-_ZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_si
	.section	.text._ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si,"axG",@progbits,_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si,comdat
	.p2align 4
	.weak	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si
	.type	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si, @function
_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si:
.LFB8393:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L205
	movq	72(%rdi), %rax
	movq	(%rax), %rbx
	movq	16(%rbx), %r15
	cmpq	%r15, 16(%r12)
	jne	.L206
	movl	%esi, -144(%rbp)
	leaq	-128(%rbp), %r14
	movq	352(%r15), %rsi
	movq	%rdi, -136(%rbp)
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r15), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpq	$0, 8(%r12)
	movq	-136(%rbp), %rcx
	movl	-144(%rbp), %r8d
	je	.L207
	cmpq	$0, 8(%rbx)
	je	.L208
	movb	$0, -144(%rbp)
	movb	$0, -136(%rbp)
	testl	%r8d, %r8d
	je	.L209
.L177:
	movq	352(%r15), %rdi
	movl	%r8d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, -96(%rbp)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L178
	movzbl	11(%rax), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L210
.L178:
	movq	%rax, -88(%rbp)
	movq	16(%r12), %rsi
	movq	8(%r12), %rax
	movq	352(%rsi), %r8
	testq	%rax, %rax
	je	.L191
	movzbl	11(%rax), %ecx
	movq	%rax, %rdi
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L211
.L179:
	movq	352(%r15), %rcx
	movq	%rax, -80(%rbp)
	leaq	112(%rcx), %rax
	addq	$120, %rcx
	cmpb	$0, -136(%rbp)
	movq	%rax, %r9
	cmove	%rcx, %r9
	cmpb	$0, -144(%rbp)
	cmovne	%rax, %rcx
	movq	360(%r15), %rax
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	1136(%rax), %r15
	testq	%rdi, %rdi
	je	.L184
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L212
.L184:
	movq	3280(%rsi), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L187
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L213
.L187:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L214
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	72(%rcx), %rdi
	movl	%r8d, -148(%rbp)
	movq	%rcx, -144(%rbp)
	call	uv_is_readable@PLT
	movq	-144(%rbp), %rcx
	testl	%eax, %eax
	movq	72(%rcx), %rdi
	setne	-136(%rbp)
	call	uv_is_writable@PLT
	movl	-148(%rbp), %r8d
	testl	%eax, %eax
	setne	-144(%rbp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L212:
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rsi
	movq	%rax, %rdi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L210:
	movq	16(%rbx), %rcx
	movq	(%rax), %rsi
	movq	352(%rcx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%rax), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rsi
	movq	8(%r12), %rdi
	movq	352(%rsi), %r8
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L213:
	leaq	-96(%rbp), %rcx
	movl	$5, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L191:
	xorl	%edi, %edi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	leaq	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L208:
	leaq	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L214:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8393:
	.size	_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si, .-_ZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_si
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L232
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L233
.L218:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L222
.L215:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L223
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L219:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L218
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L224
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L220:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L222:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L215
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L224:
	xorl	%edx, %edx
	jmp	.L220
	.cfi_endproc
.LFE8349:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L235
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L237
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L235
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L237
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L235
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L235:
	movq	%r12, %rsi
	call	*%rax
.L242:
	movq	(%r12), %rax
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L243
	leaq	16(%r12), %rsi
.L244:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L245
	addq	$16, %r12
.L246:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L247
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L275
.L247:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L276
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L251
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L234
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L234:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L277
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L275:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L245:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L243:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L276:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L254
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L249:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L251:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	xorl	%edx, %edx
	jmp	.L249
.L277:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7632:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L279
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L282
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L281
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L282
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L279
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L282
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L281
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L282
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L279
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L282
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*32(%rax)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L279:
	movq	%r12, %rsi
	call	*%rax
.L286:
	movq	(%r12), %rax
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L287
	leaq	40(%r12), %rsi
.L288:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L289
	addq	$40, %r12
.L290:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L291
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L319
.L291:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L320
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L295
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L278
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L278:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L321
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L319:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L298
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L293:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L295:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L298:
	xorl	%edx, %edx
	jmp	.L293
.L321:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7634:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEE
	.section	.data.rel.ro._ZTVN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEE,"awG",@progbits,_ZTVN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEE,comdat
	.align 8
	.type	_ZTVN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEE, @object
	.size	_ZTVN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEE, 368
_ZTVN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node15LibuvStreamWrap5GetFDEv
	.quad	_ZN4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZN4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZN4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZN4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	-88
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.quad	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEE
	.section	.data.rel.ro._ZTVN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEE,"awG",@progbits,_ZTVN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEE,comdat
	.align 8
	.type	_ZTVN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEE, @object
	.size	_ZTVN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEE, 368
_ZTVN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node15LibuvStreamWrap5GetFDEv
	.quad	_ZN4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZN4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZN4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZN4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	-88
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.quad	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_2
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../src/connection_wrap.cc:92"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"(wrap->persistent().IsEmpty()) == (false)"
	.align 8
.LC2:
	.string	"static void node::ConnectionWrap<WrapType, UVType>::AfterConnect(uv_connect_t*, int) [with WrapType = node::TCPWrap; UVType = uv_tcp_s; uv_connect_t = uv_connect_s]"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_2,"awG",@progbits,_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_2,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_2, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_2, 24
_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_2:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.weak	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_1
	.section	.rodata.str1.1
.LC3:
	.string	"../src/connection_wrap.cc:91"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"(req_wrap->persistent().IsEmpty()) == (false)"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_1,"awG",@progbits,_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_1,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_1, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_1, 24
_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_1:
	.quad	.LC3
	.quad	.LC4
	.quad	.LC2
	.weak	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_0
	.section	.rodata.str1.1
.LC5:
	.string	"../src/connection_wrap.cc:84"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"(req_wrap->env()) == (wrap->env())"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_0,"awG",@progbits,_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_0,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_0, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_0, 24
_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args_0:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC2
	.weak	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args
	.section	.rodata.str1.1
.LC7:
	.string	"../src/connection_wrap.cc:82"
.LC8:
	.string	"(req_wrap) != nullptr"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args,"awG",@progbits,_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args, 24
_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12AfterConnectEP12uv_connect_siE4args:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC2
	.weak	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_2
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"static void node::ConnectionWrap<WrapType, UVType>::AfterConnect(uv_connect_t*, int) [with WrapType = node::PipeWrap; UVType = uv_pipe_s; uv_connect_t = uv_connect_s]"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_2,"awG",@progbits,_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_2,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_2, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_2, 24
_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_2:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC9
	.weak	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_1
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_1,"awG",@progbits,_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_1,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_1, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_1, 24
_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_1:
	.quad	.LC3
	.quad	.LC4
	.quad	.LC9
	.weak	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_0
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_0,"awG",@progbits,_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_0,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_0, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_0, 24
_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args_0:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC9
	.weak	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args,"awG",@progbits,_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args, 24
_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12AfterConnectEP12uv_connect_siE4args:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.weak	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_1
	.section	.rodata.str1.1
.LC10:
	.string	"../src/connection_wrap.cc:45"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"(wrap_data->persistent().IsEmpty()) == (false)"
	.align 8
.LC12:
	.string	"static void node::ConnectionWrap<WrapType, UVType>::OnConnection(uv_stream_t*, int) [with WrapType = node::TCPWrap; UVType = uv_tcp_s; uv_stream_t = uv_stream_s]"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_1,"awG",@progbits,_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_1,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_1, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_1, 24
_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_1:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.weak	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_0
	.section	.rodata.str1.1
.LC13:
	.string	"../src/connection_wrap.cc:37"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"(&wrap_data->handle_) == (reinterpret_cast<UVType*>(handle))"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_0,"awG",@progbits,_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_0,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_0, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_0, 24
_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args_0:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC12
	.weak	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args
	.section	.rodata.str1.1
.LC15:
	.string	"../src/connection_wrap.cc:36"
.LC16:
	.string	"(wrap_data) != nullptr"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args,"awG",@progbits,_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args, 24
_ZZN4node14ConnectionWrapINS_7TCPWrapE8uv_tcp_sE12OnConnectionEP11uv_stream_siE4args:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC12
	.weak	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_1
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"static void node::ConnectionWrap<WrapType, UVType>::OnConnection(uv_stream_t*, int) [with WrapType = node::PipeWrap; UVType = uv_pipe_s; uv_stream_t = uv_stream_s]"
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_1,"awG",@progbits,_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_1,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_1, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_1, 24
_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_1:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC17
	.weak	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_0
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_0,"awG",@progbits,_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_0,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_0, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_0, 24
_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args_0:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC17
	.weak	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args
	.section	.data.rel.ro.local._ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args,"awG",@progbits,_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args,comdat
	.align 16
	.type	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args, @gnu_unique_object
	.size	_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args, 24
_ZZN4node14ConnectionWrapINS_8PipeWrapE9uv_pipe_sE12OnConnectionEP11uv_stream_siE4args:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC18:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC20:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC21:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC21
	.quad	.LC19
	.quad	.LC22
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC23:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC25:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC26:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC28:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC29:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC31:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
