	.file	"spawn_sync.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe13WriteCallbackEP10uv_write_si
	.type	_ZN4node20SyncProcessStdioPipe13WriteCallbackEP10uv_write_si, @function
_ZN4node20SyncProcessStdioPipe13WriteCallbackEP10uv_write_si:
.LFB7525:
	.cfi_startproc
	endbr64
	testl	%esi, %esi
	js	.L4
.L1:
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	80(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movl	504(%rax), %edx
	testl	%edx, %edx
	jne	.L1
	movl	%esi, 504(%rax)
	ret
	.cfi_endproc
.LFE7525:
	.size	_ZN4node20SyncProcessStdioPipe13WriteCallbackEP10uv_write_si, .-_ZN4node20SyncProcessStdioPipe13WriteCallbackEP10uv_write_si
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe16ShutdownCallbackEP13uv_shutdown_si
	.type	_ZN4node20SyncProcessStdioPipe16ShutdownCallbackEP13uv_shutdown_si, @function
_ZN4node20SyncProcessStdioPipe16ShutdownCallbackEP13uv_shutdown_si:
.LFB7526:
	.cfi_startproc
	endbr64
	cmpl	$-107, %esi
	je	.L5
	testl	%esi, %esi
	js	.L10
.L5:
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	64(%rdi), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movl	504(%rax), %edx
	testl	%edx, %edx
	jne	.L5
	movl	%esi, 504(%rax)
	ret
	.cfi_endproc
.LFE7526:
	.size	_ZN4node20SyncProcessStdioPipe16ShutdownCallbackEP13uv_shutdown_si, .-_ZN4node20SyncProcessStdioPipe16ShutdownCallbackEP13uv_shutdown_si
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s
	.type	_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s, @function
_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s:
.LFB7527:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$4, 584(%rax)
	ret
	.cfi_endproc
.LFE7527:
	.size	_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s, .-_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s
	.type	_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s, @function
_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s:
.LFB7577:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7577:
	.size	_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s, .-_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner12ExitCallbackEP12uv_process_sli
	.type	_ZN4node17SyncProcessRunner12ExitCallbackEP12uv_process_sli, @function
_ZN4node17SyncProcessRunner12ExitCallbackEP12uv_process_sli:
.LFB7575:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$8, %rsp
	movq	(%rdi), %r13
	call	uv_close@PLT
	testq	%rbx, %rbx
	js	.L17
	movq	%rbx, 328(%r13)
	movl	%r12d, 336(%r13)
.L13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movl	500(%r13), %eax
	testl	%eax, %eax
	jne	.L13
	movl	%ebx, 500(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7575:
	.size	_ZN4node17SyncProcessRunner12ExitCallbackEP12uv_process_sli, .-_ZN4node17SyncProcessRunner12ExitCallbackEP12uv_process_sli
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"spawn"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB1:
	.text
.LHOTB1:
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	testq	%rdx, %rdx
	je	.L19
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L19
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L19
	movq	271(%rax), %r12
	movq	352(%r12), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movq	352(%r12), %rdi
	pushq	$0
	leaq	_ZN4node17SyncProcessRunner5SpawnERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L27
.L20:
	movq	352(%r12), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L28
.L21:
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L29
.L22:
	leaq	-32(%rbp), %rsp
	movq	%r12, %rsi
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L28:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L29:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L22
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7528:
.L19:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7528:
	.text
	.size	_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE1:
	.text
.LHOTE1:
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe13AllocCallbackEP11uv_handle_smP8uv_buf_t
	.type	_ZN4node20SyncProcessStdioPipe13AllocCallbackEP11uv_handle_smP8uv_buf_t, @function
_ZN4node20SyncProcessStdioPipe13AllocCallbackEP11uv_handle_smP8uv_buf_t:
.LFB7523:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdx, %rbx
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L36
	movl	65536(%rdi), %eax
	movl	$65536, %esi
	subl	%eax, %esi
	cmpl	$65536, %eax
	je	.L37
.L34:
	addq	%rax, %rdi
	call	uv_buf_init@PLT
	movq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movl	$65552, %edi
	call	_Znwm@PLT
	movl	$65552, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	%rax, %xmm0
	movq	%rax, %rdi
	movl	$0, 65536(%rax)
	movq	$0, 65544(%rax)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 32(%r12)
.L32:
	xorl	%eax, %eax
	movl	$65536, %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$65552, %edi
	call	_Znwm@PLT
	movl	$65552, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	$0, 65544(%rax)
	movq	%rax, %rdi
	movl	$0, 65536(%rax)
	movq	40(%r12), %rax
	movq	%rdi, 65544(%rax)
	movq	%rdi, 40(%r12)
	jmp	.L32
	.cfi_endproc
.LFE7523:
	.size	_ZN4node20SyncProcessStdioPipe13AllocCallbackEP11uv_handle_smP8uv_buf_t, .-_ZN4node20SyncProcessStdioPipe13AllocCallbackEP11uv_handle_smP8uv_buf_t
	.align 2
	.p2align 4
	.type	_ZN4node17SyncProcessRunner4KillEv.part.0, @function
_ZN4node17SyncProcessRunner4KillEv.part.0:
.LFB9797:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, 328(%rdi)
	movb	$1, 312(%rdi)
	js	.L62
.L40:
	cmpl	$1, 508(%rbx)
	jg	.L63
.L43:
	cmpb	$0, 72(%rbx)
	je	.L44
	movq	56(%rbx), %r14
	movq	48(%rbx), %r12
	cmpq	%r12, %r14
	je	.L64
	cmpq	$0, 24(%rbx)
	leaq	_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s(%rip), %r15
	je	.L65
.L49:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L47
	movl	584(%r13), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L66
	leaq	48(%r13), %rdi
	movq	%r15, %rsi
	call	uv_close@PLT
	movl	$3, 584(%r13)
.L47:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L49
	cmpl	$1, 508(%rbx)
	movb	$0, 72(%rbx)
	jg	.L67
.L44:
	cmpb	$0, 496(%rbx)
	je	.L38
	cmpq	$0, 8(%rbx)
	je	.L68
	cmpq	$0, 24(%rbx)
	je	.L69
	leaq	344(%rbx), %r12
	movq	%r12, %rdi
	call	uv_ref@PLT
	leaq	_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	movb	$0, 496(%rbx)
.L38:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	16(%rdi), %esi
	leaq	176(%rdi), %r12
	movq	%r12, %rdi
	call	uv_process_kill@PLT
	testl	%eax, %eax
	jns	.L40
	cmpl	$-3, %eax
	je	.L40
	movl	500(%rbx), %edx
	testl	%edx, %edx
	jne	.L42
	movl	%eax, 500(%rbx)
.L42:
	movl	$9, %esi
	movq	%r12, %rdi
	call	uv_process_kill@PLT
	cmpl	$1, 508(%rbx)
	jle	.L43
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	_ZZN4node20SyncProcessStdioPipe5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9797:
	.size	_ZN4node17SyncProcessRunner4KillEv.part.0, .-_ZN4node17SyncProcessRunner4KillEv.part.0
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner17KillTimerCallbackEP10uv_timer_s
	.type	_ZN4node17SyncProcessRunner17KillTimerCallbackEP10uv_timer_s, @function
_ZN4node17SyncProcessRunner17KillTimerCallbackEP10uv_timer_s:
.LFB7576:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movl	500(%rdi), %eax
	testl	%eax, %eax
	jne	.L71
	movl	$-110, 500(%rdi)
.L71:
	cmpb	$0, 312(%rdi)
	jne	.L70
	jmp	_ZN4node17SyncProcessRunner4KillEv.part.0
	.p2align 4,,10
	.p2align 3
.L70:
	ret
	.cfi_endproc
.LFE7576:
	.size	_ZN4node17SyncProcessRunner17KillTimerCallbackEP10uv_timer_s, .-_ZN4node17SyncProcessRunner17KillTimerCallbackEP10uv_timer_s
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe12ReadCallbackEP11uv_stream_slPK8uv_buf_t
	.type	_ZN4node20SyncProcessStdioPipe12ReadCallbackEP11uv_stream_slPK8uv_buf_t, @function
_ZN4node20SyncProcessStdioPipe12ReadCallbackEP11uv_stream_slPK8uv_buf_t:
.LFB7524:
	.cfi_startproc
	endbr64
	cmpq	$-4095, %rsi
	je	.L90
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rsi, %rsi
	js	.L93
	movq	40(%rdi), %r8
	movl	65536(%r8), %ecx
	movq	%rcx, %rax
	addq	%r8, %rcx
	cmpq	%rcx, (%rdx)
	jne	.L94
	movq	(%rdi), %rdi
	addl	%esi, %eax
	movl	%eax, 65536(%r8)
	addq	320(%rdi), %rsi
	movsd	(%rdi), %xmm1
	comisd	.LC2(%rip), %xmm1
	movq	%rsi, 320(%rdi)
	jbe	.L73
	testq	%rsi, %rsi
	js	.L83
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L84:
	comisd	%xmm1, %xmm0
	jbe	.L73
	movl	500(%rdi), %eax
	testl	%eax, %eax
	jne	.L86
	movl	$-105, 500(%rdi)
.L86:
	cmpb	$0, 312(%rdi)
	jne	.L73
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN4node17SyncProcessRunner4KillEv.part.0
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	testl	%esi, %esi
	je	.L95
	movq	(%rdi), %rax
	movl	504(%rax), %edx
	testl	%edx, %edx
	je	.L96
.L78:
	cmpl	$2, 584(%rdi)
	jg	.L97
	addq	$48, %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uv_read_stop@PLT
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movl	%esi, 504(%rax)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	_ZZN4node23SyncProcessOutputBuffer6OnReadEPK8uv_buf_tmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	leaq	_ZZN4node20SyncProcessStdioPipe8SetErrorEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7524:
	.size	_ZN4node20SyncProcessStdioPipe12ReadCallbackEP11uv_stream_slPK8uv_buf_t, .-_ZN4node20SyncProcessStdioPipe12ReadCallbackEP11uv_stream_slPK8uv_buf_t
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipeC2EPNS_17SyncProcessRunnerEbb8uv_buf_t
	.type	_ZN4node20SyncProcessStdioPipeC2EPNS_17SyncProcessRunnerEbb8uv_buf_t, @function
_ZN4node20SyncProcessStdioPipeC2EPNS_17SyncProcessRunnerEbb8uv_buf_t:
.LFB7497:
	.cfi_startproc
	endbr64
	movq	%rsi, %r11
	pxor	%xmm0, %xmm0
	movb	%cl, 9(%rdi)
	movq	%rdi, %r10
	movups	%xmm0, 32(%rdi)
	xorl	%eax, %eax
	movl	%ecx, %esi
	addq	$48, %rdi
	movl	$33, %ecx
	movq	%r11, -48(%rdi)
	pxor	%xmm0, %xmm0
	movb	%dl, -40(%rdi)
	movq	%r8, -32(%rdi)
	movq	%r9, -24(%rdi)
	rep stosq
	leaq	312(%r10), %rdi
	movl	$24, %ecx
	rep stosq
	movups	%xmm0, 504(%r10)
	movl	$0, 584(%r10)
	movups	%xmm0, 520(%r10)
	movups	%xmm0, 536(%r10)
	movups	%xmm0, 552(%r10)
	movups	%xmm0, 568(%r10)
	testb	%dl, %dl
	jne	.L98
	testb	%sil, %sil
	je	.L103
.L98:
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node20SyncProcessStdioPipeC4EPNS_17SyncProcessRunnerEbb8uv_buf_tE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7497:
	.size	_ZN4node20SyncProcessStdioPipeC2EPNS_17SyncProcessRunnerEbb8uv_buf_t, .-_ZN4node20SyncProcessStdioPipeC2EPNS_17SyncProcessRunnerEbb8uv_buf_t
	.globl	_ZN4node20SyncProcessStdioPipeC1EPNS_17SyncProcessRunnerEbb8uv_buf_t
	.set	_ZN4node20SyncProcessStdioPipeC1EPNS_17SyncProcessRunnerEbb8uv_buf_t,_ZN4node20SyncProcessStdioPipeC2EPNS_17SyncProcessRunnerEbb8uv_buf_t
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipeD2Ev
	.type	_ZN4node20SyncProcessStdioPipeD2Ev, @function
_ZN4node20SyncProcessStdioPipeD2Ev:
.LFB7500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testl	$-5, 584(%rdi)
	jne	.L113
	movq	32(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L104
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rbx, %rdi
	movq	65544(%rbx), %rbx
	movl	$65552, %esi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L107
.L104:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	leaq	_ZZN4node20SyncProcessStdioPipeD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7500:
	.size	_ZN4node20SyncProcessStdioPipeD2Ev, .-_ZN4node20SyncProcessStdioPipeD2Ev
	.globl	_ZN4node20SyncProcessStdioPipeD1Ev
	.set	_ZN4node20SyncProcessStdioPipeD1Ev,_ZN4node20SyncProcessStdioPipeD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_s
	.type	_ZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_s, @function
_ZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_s:
.LFB7502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	584(%rdi), %eax
	testl	%eax, %eax
	jne	.L119
	movq	%rsi, %rdi
	xorl	%edx, %edx
	leaq	48(%rbx), %rsi
	call	uv_pipe_init@PLT
	testl	%eax, %eax
	js	.L114
	cmpl	$2, 584(%rbx)
	jg	.L120
	movq	%rbx, 48(%rbx)
	xorl	%eax, %eax
	movl	$1, 584(%rbx)
.L114:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
	leaq	_ZZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7502:
	.size	_ZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_s, .-_ZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_s
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe5StartEv
	.type	_ZN4node20SyncProcessStdioPipe5StartEv, @function
_ZN4node20SyncProcessStdioPipe5StartEv:
.LFB7503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$1, 584(%rdi)
	jne	.L134
	cmpb	$0, 8(%rdi)
	movq	%rdi, %rbx
	movl	$2, 584(%rdi)
	je	.L123
	cmpq	$0, 24(%rdi)
	leaq	48(%rdi), %r12
	je	.L124
	cmpq	$0, 16(%rdi)
	je	.L135
	leaq	48(%rdi), %r12
	leaq	16(%rdi), %rdx
	movl	$1, %ecx
	leaq	312(%rdi), %rdi
	leaq	_ZN4node20SyncProcessStdioPipe13WriteCallbackEP10uv_write_si(%rip), %r8
	movq	%r12, %rsi
	call	uv_write@PLT
	testl	%eax, %eax
	js	.L121
	cmpl	$2, 584(%rbx)
	jg	.L128
.L124:
	leaq	504(%rbx), %rdi
	leaq	_ZN4node20SyncProcessStdioPipe16ShutdownCallbackEP13uv_shutdown_si(%rip), %rdx
	movq	%r12, %rsi
	call	uv_shutdown@PLT
	testl	%eax, %eax
	js	.L121
	cmpb	$0, 9(%rbx)
	je	.L129
	cmpl	$2, 584(%rbx)
	jle	.L130
.L128:
	leaq	_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	cmpb	$0, 9(%rdi)
	leaq	48(%rdi), %r12
	je	.L129
.L130:
	leaq	_ZN4node20SyncProcessStdioPipe12ReadCallbackEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN4node20SyncProcessStdioPipe13AllocCallbackEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r12, %rdi
	call	uv_read_start@PLT
	movl	$0, %edx
	testl	%eax, %eax
	cmovg	%edx, %eax
.L121:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	leaq	_ZZN4node20SyncProcessStdioPipe5StartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	_ZZN4node20SyncProcessStdioPipe5StartEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7503:
	.size	_ZN4node20SyncProcessStdioPipe5StartEv, .-_ZN4node20SyncProcessStdioPipe5StartEv
	.align 2
	.p2align 4
	.globl	_ZN4node20SyncProcessStdioPipe5CloseEv
	.type	_ZN4node20SyncProcessStdioPipe5CloseEv, @function
_ZN4node20SyncProcessStdioPipe5CloseEv:
.LFB7504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	584(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L139
	movq	%rdi, %rbx
	leaq	_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s(%rip), %rsi
	leaq	48(%rdi), %rdi
	call	uv_close@PLT
	movl	$3, 584(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	leaq	_ZZN4node20SyncProcessStdioPipe5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7504:
	.size	_ZN4node20SyncProcessStdioPipe5CloseEv, .-_ZN4node20SyncProcessStdioPipe5CloseEv
	.align 2
	.p2align 4
	.globl	_ZNK4node20SyncProcessStdioPipe17GetOutputAsBufferEPNS_11EnvironmentE
	.type	_ZNK4node20SyncProcessStdioPipe17GetOutputAsBufferEPNS_11EnvironmentE, @function
_ZNK4node20SyncProcessStdioPipe17GetOutputAsBufferEPNS_11EnvironmentE:
.LFB7505:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	xorl	%esi, %esi
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L141
	.p2align 4,,10
	.p2align 3
.L142:
	movl	65536(%rax), %edx
	movq	65544(%rax), %rax
	addq	%rdx, %rsi
	testq	%rax, %rax
	jne	.L142
.L141:
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L153
.L143:
	movq	%r14, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	32(%rbx), %rbx
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.L144
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L145:
	movl	65536(%rbx), %edx
	leaq	0(%r13,%r12), %rdi
	movq	%rbx, %rsi
	call	memcpy@PLT
	movl	65536(%rbx), %eax
	movq	65544(%rbx), %rbx
	addq	%rax, %r12
	testq	%rbx, %rbx
	jne	.L145
.L144:
	popq	%rbx
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L143
	.cfi_endproc
.LFE7505:
	.size	_ZNK4node20SyncProcessStdioPipe17GetOutputAsBufferEPNS_11EnvironmentE, .-_ZNK4node20SyncProcessStdioPipe17GetOutputAsBufferEPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunnerC2EPNS_11EnvironmentE
	.type	_ZN4node17SyncProcessRunnerC2EPNS_11EnvironmentE, @function
_ZN4node17SyncProcessRunnerC2EPNS_11EnvironmentE:
.LFB7540:
	.cfi_startproc
	endbr64
	pxor	%xmm0, %xmm0
	movq	$0x000000000, (%rdi)
	movq	%rdi, %rdx
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	movq	$0, 8(%rdi)
	movl	$17, %ecx
	leaq	176(%rdi), %rdi
	movl	$15, -160(%rdi)
	movq	$0, -152(%rdi)
	movl	$0, -144(%rdi)
	movups	%xmm0, -136(%rdi)
	movups	%xmm0, -120(%rdi)
	movb	$0, -104(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	%xmm1, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	%xmm1, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, -16(%rdi)
	rep stosq
	leaq	344(%rdx), %rdi
	movl	$19, %ecx
	movb	$0, 312(%rdx)
	movq	$0, 320(%rdx)
	movq	$-1, 328(%rdx)
	movl	$-1, 336(%rdx)
	rep stosq
	movb	$0, 496(%rdx)
	movl	$0, 500(%rdx)
	movq	$0, 504(%rdx)
	movq	%rsi, 512(%rdx)
	ret
	.cfi_endproc
.LFE7540:
	.size	_ZN4node17SyncProcessRunnerC2EPNS_11EnvironmentE, .-_ZN4node17SyncProcessRunnerC2EPNS_11EnvironmentE
	.globl	_ZN4node17SyncProcessRunnerC1EPNS_11EnvironmentE
	.set	_ZN4node17SyncProcessRunnerC1EPNS_11EnvironmentE,_ZN4node17SyncProcessRunnerC2EPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunnerD2Ev
	.type	_ZN4node17SyncProcessRunnerD2Ev, @function
_ZN4node17SyncProcessRunnerD2Ev:
.LFB7543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$2, 508(%rdi)
	jne	.L207
	movq	56(%rdi), %rax
	movq	48(%rdi), %r14
	movq	%rdi, %rbx
	movq	%rax, -56(%rbp)
	cmpq	%rax, %r14
	je	.L157
	movq	%r14, %r15
	.p2align 4,,10
	.p2align 3
.L162:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L158
	testl	$-5, 584(%r13)
	jne	.L170
	movq	32(%r13), %r12
	testq	%r12, %r12
	je	.L161
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%r12, %rdi
	movq	65544(%r12), %r12
	movl	$65552, %esi
	call	_ZdlPvm@PLT
	testq	%r12, %r12
	jne	.L160
.L161:
	movl	$592, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L158:
	addq	$8, %r15
	cmpq	%r15, -56(%rbp)
	jne	.L162
	movq	%r14, 56(%rbx)
.L157:
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	_ZdaPv@PLT
.L163:
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L164
	call	_ZdaPv@PLT
.L164:
	movq	168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L165
	call	_ZdaPv@PLT
.L165:
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L166
	call	_ZdaPv@PLT
.L166:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L167
	call	_ZdaPv@PLT
.L167:
	movq	56(%rbx), %r15
	movq	48(%rbx), %r12
	cmpq	%r12, %r15
	je	.L168
	.p2align 4,,10
	.p2align 3
.L173:
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L169
	testl	$-5, 584(%r14)
	jne	.L170
	movq	32(%r14), %r13
	testq	%r13, %r13
	je	.L172
	.p2align 4,,10
	.p2align 3
.L171:
	movq	%r13, %rdi
	movq	65544(%r13), %r13
	movl	$65552, %esi
	call	_ZdlPvm@PLT
	testq	%r13, %r13
	jne	.L171
.L172:
	movl	$592, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L169:
	addq	$8, %r12
	cmpq	%r12, %r15
	jne	.L173
	movq	48(%rbx), %r12
.L168:
	testq	%r12, %r12
	je	.L155
	addq	$24, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	leaq	_ZZN4node20SyncProcessStdioPipeD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	leaq	_ZZN4node17SyncProcessRunnerD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7543:
	.size	_ZN4node17SyncProcessRunnerD2Ev, .-_ZN4node17SyncProcessRunnerD2Ev
	.globl	_ZN4node17SyncProcessRunnerD1Ev
	.set	_ZN4node17SyncProcessRunnerD1Ev,_ZN4node17SyncProcessRunnerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner15CloseStdioPipesEv
	.type	_ZN4node17SyncProcessRunner15CloseStdioPipesEv, @function
_ZN4node17SyncProcessRunner15CloseStdioPipesEv:
.LFB7554:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpl	$1, 508(%rdi)
	jg	.L222
	cmpb	$0, 72(%rdi)
	movq	%rdi, %r12
	je	.L208
	movq	56(%rdi), %r14
	movq	48(%rdi), %rbx
	cmpq	%rbx, %r14
	je	.L223
	cmpq	$0, 24(%rdi)
	leaq	_ZN4node20SyncProcessStdioPipe13CloseCallbackEP11uv_handle_s(%rip), %r15
	je	.L224
.L215:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L213
	movl	584(%r13), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	ja	.L225
	leaq	48(%r13), %rdi
	movq	%r15, %rsi
	call	uv_close@PLT
	movl	$3, 584(%r13)
.L213:
	addq	$8, %rbx
	cmpq	%rbx, %r14
	jne	.L215
	movb	$0, 72(%r12)
.L208:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	leaq	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	_ZZN4node20SyncProcessStdioPipe5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7554:
	.size	_ZN4node17SyncProcessRunner15CloseStdioPipesEv, .-_ZN4node17SyncProcessRunner15CloseStdioPipesEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEv
	.type	_ZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEv, @function
_ZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEv:
.LFB7553:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$1, 508(%rdi)
	jg	.L244
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L228
	call	_ZN4node17SyncProcessRunner15CloseStdioPipesEv
	cmpl	$1, 508(%rbx)
	jg	.L245
	cmpb	$0, 496(%rbx)
	je	.L230
	cmpq	$0, 8(%rbx)
	je	.L246
	cmpq	$0, 24(%rbx)
	je	.L247
	leaq	344(%rbx), %r12
	movq	%r12, %rdi
	call	uv_ref@PLT
	leaq	_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	movb	$0, 496(%rbx)
.L230:
	cmpl	$10, 192(%rbx)
	je	.L248
.L234:
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	uv_run@PLT
	testl	%eax, %eax
	js	.L249
	movq	24(%rbx), %rdi
	call	_ZN4node18CheckedUvLoopCloseEP9uv_loop_s@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L237
	movl	$848, %esi
	call	_ZdlPvm@PLT
.L237:
	movq	$0, 24(%rbx)
	movl	$2, 508(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	cmpb	$0, 72(%rdi)
	jne	.L250
	cmpb	$0, 496(%rdi)
	jne	.L251
	movl	$2, 508(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	.cfi_restore_state
	leaq	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	176(%rbx), %r12
	movq	%r12, %rdi
	call	uv_is_closing@PLT
	testl	%eax, %eax
	jne	.L234
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	uv_close@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L249:
	call	_ZN4node5AbortEv@PLT
	.cfi_endproc
.LFE7553:
	.size	_ZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEv, .-_ZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner14CloseKillTimerEv
	.type	_ZN4node17SyncProcessRunner14CloseKillTimerEv, @function
_ZN4node17SyncProcessRunner14CloseKillTimerEv:
.LFB7555:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpl	$1, 508(%rdi)
	jg	.L258
	cmpb	$0, 496(%rdi)
	movq	%rdi, %rbx
	je	.L252
	cmpq	$0, 8(%rdi)
	je	.L259
	cmpq	$0, 24(%rdi)
	je	.L260
	leaq	344(%rdi), %r12
	movq	%r12, %rdi
	call	uv_ref@PLT
	leaq	_ZN4node17SyncProcessRunner22KillTimerCloseCallbackEP11uv_handle_s(%rip), %rsi
	movq	%r12, %rdi
	call	uv_close@PLT
	movb	$0, 496(%rbx)
.L252:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L260:
	leaq	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7555:
	.size	_ZN4node17SyncProcessRunner14CloseKillTimerEv, .-_ZN4node17SyncProcessRunner14CloseKillTimerEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner4KillEv
	.type	_ZN4node17SyncProcessRunner4KillEv, @function
_ZN4node17SyncProcessRunner4KillEv:
.LFB7556:
	.cfi_startproc
	endbr64
	cmpb	$0, 312(%rdi)
	jne	.L261
	jmp	_ZN4node17SyncProcessRunner4KillEv.part.0
	.p2align 4,,10
	.p2align 3
.L261:
	ret
	.cfi_endproc
.LFE7556:
	.size	_ZN4node17SyncProcessRunner4KillEv, .-_ZN4node17SyncProcessRunner4KillEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner35IncrementBufferSizeAndCheckOverflowEl
	.type	_ZN4node17SyncProcessRunner35IncrementBufferSizeAndCheckOverflowEl, @function
_ZN4node17SyncProcessRunner35IncrementBufferSizeAndCheckOverflowEl:
.LFB7557:
	.cfi_startproc
	endbr64
	addq	320(%rdi), %rsi
	movsd	(%rdi), %xmm1
	comisd	.LC2(%rip), %xmm1
	movq	%rsi, 320(%rdi)
	jbe	.L263
	testq	%rsi, %rsi
	js	.L267
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L268:
	comisd	%xmm1, %xmm0
	jbe	.L263
	movl	500(%rdi), %eax
	testl	%eax, %eax
	jne	.L270
	movl	$-105, 500(%rdi)
.L270:
	cmpb	$0, 312(%rdi)
	jne	.L263
	jmp	_ZN4node17SyncProcessRunner4KillEv.part.0
	.p2align 4,,10
	.p2align 3
.L263:
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L268
	.cfi_endproc
.LFE7557:
	.size	_ZN4node17SyncProcessRunner35IncrementBufferSizeAndCheckOverflowEl, .-_ZN4node17SyncProcessRunner35IncrementBufferSizeAndCheckOverflowEl
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner6OnExitEli
	.type	_ZN4node17SyncProcessRunner6OnExitEli, @function
_ZN4node17SyncProcessRunner6OnExitEli:
.LFB7558:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	js	.L275
	movq	%rsi, 328(%rdi)
	movl	%edx, 336(%rdi)
.L272:
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	movl	500(%rdi), %eax
	testl	%eax, %eax
	jne	.L272
	movl	%esi, 500(%rdi)
	ret
	.cfi_endproc
.LFE7558:
	.size	_ZN4node17SyncProcessRunner6OnExitEli, .-_ZN4node17SyncProcessRunner6OnExitEli
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner18OnKillTimerTimeoutEv
	.type	_ZN4node17SyncProcessRunner18OnKillTimerTimeoutEv, @function
_ZN4node17SyncProcessRunner18OnKillTimerTimeoutEv:
.LFB7559:
	.cfi_startproc
	endbr64
	movl	500(%rdi), %eax
	testl	%eax, %eax
	jne	.L277
	movl	$-110, 500(%rdi)
.L277:
	cmpb	$0, 312(%rdi)
	jne	.L276
	jmp	_ZN4node17SyncProcessRunner4KillEv.part.0
	.p2align 4,,10
	.p2align 3
.L276:
	ret
	.cfi_endproc
.LFE7559:
	.size	_ZN4node17SyncProcessRunner18OnKillTimerTimeoutEv, .-_ZN4node17SyncProcessRunner18OnKillTimerTimeoutEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner8GetErrorEv
	.type	_ZN4node17SyncProcessRunner8GetErrorEv, @function
_ZN4node17SyncProcessRunner8GetErrorEv:
.LFB7560:
	.cfi_startproc
	endbr64
	movl	500(%rdi), %eax
	testl	%eax, %eax
	jne	.L279
	movl	504(%rdi), %eax
.L279:
	ret
	.cfi_endproc
.LFE7560:
	.size	_ZN4node17SyncProcessRunner8GetErrorEv, .-_ZN4node17SyncProcessRunner8GetErrorEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner8SetErrorEi
	.type	_ZN4node17SyncProcessRunner8SetErrorEi, @function
_ZN4node17SyncProcessRunner8SetErrorEi:
.LFB7561:
	.cfi_startproc
	endbr64
	movl	500(%rdi), %eax
	testl	%eax, %eax
	jne	.L281
	movl	%esi, 500(%rdi)
.L281:
	ret
	.cfi_endproc
.LFE7561:
	.size	_ZN4node17SyncProcessRunner8SetErrorEi, .-_ZN4node17SyncProcessRunner8SetErrorEi
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner12SetPipeErrorEi
	.type	_ZN4node17SyncProcessRunner12SetPipeErrorEi, @function
_ZN4node17SyncProcessRunner12SetPipeErrorEi:
.LFB7562:
	.cfi_startproc
	endbr64
	movl	504(%rdi), %eax
	testl	%eax, %eax
	jne	.L283
	movl	%esi, 504(%rdi)
.L283:
	ret
	.cfi_endproc
.LFE7562:
	.size	_ZN4node17SyncProcessRunner12SetPipeErrorEi, .-_ZN4node17SyncProcessRunner12SetPipeErrorEi
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner16BuildOutputArrayEv
	.type	_ZN4node17SyncProcessRunner16BuildOutputArrayEv, @function
_ZN4node17SyncProcessRunner16BuildOutputArrayEv:
.LFB7564:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	508(%rdi), %eax
	testl	%eax, %eax
	jle	.L333
	movq	56(%rdi), %rax
	movq	%rdi, %r14
	cmpq	%rax, 48(%rdi)
	je	.L334
	movq	512(%rdi), %rax
	movq	352(%rax), %rsi
	leaq	-176(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	56(%r14), %r13
	movq	48(%r14), %r12
	leaq	-120(%rbp), %rax
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, -208(%rbp)
	movq	%r13, %r15
	movq	%rax, -128(%rbp)
	subq	%r12, %r15
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r15, %rbx
	movups	%xmm0, -120(%rbp)
	sarq	$3, %rbx
	movq	$0, -120(%rbp)
	movq	%rbx, %r10
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	cmpq	$8, %rbx
	ja	.L335
	movq	%rax, %r11
.L288:
	movq	%rbx, -144(%rbp)
	movq	512(%r14), %rdi
	cmpq	%r13, %r12
	je	.L293
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L294:
	movq	352(%rdi), %rax
	addq	$104, %rax
	cmpq	%r10, %r15
	jnb	.L299
	movq	-184(%rbp), %rcx
	movq	%rax, (%r11,%rcx)
.L303:
	movq	48(%r14), %r12
	movq	56(%r14), %rax
	leal	1(%rbx), %r15d
	movq	%r15, %rbx
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%rax, %r15
	jnb	.L293
.L304:
	movq	(%r12,%r15,8), %r12
	leaq	0(,%r15,8), %rax
	movq	%rax, -184(%rbp)
	testq	%r12, %r12
	je	.L294
	cmpb	$0, 9(%r12)
	je	.L294
	movq	32(%r12), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	je	.L295
	.p2align 4,,10
	.p2align 3
.L296:
	movl	65536(%rax), %ecx
	movq	65544(%rax), %rax
	addq	%rcx, %rsi
	testq	%rax, %rax
	jne	.L296
.L295:
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEm@PLT
	movq	%rax, -200(%rbp)
	testq	%rax, %rax
	je	.L336
.L297:
	movq	-200(%rbp), %rdi
	xorl	%r13d, %r13d
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	32(%r12), %r12
	movq	%rax, -192(%rbp)
	testq	%r12, %r12
	je	.L301
	.p2align 4,,10
	.p2align 3
.L298:
	movq	-192(%rbp), %rax
	movl	65536(%r12), %edx
	movq	%r12, %rsi
	leaq	(%rax,%r13), %rdi
	call	memcpy@PLT
	movl	65536(%r12), %eax
	movq	65544(%r12), %r12
	addq	%rax, %r13
	testq	%r12, %r12
	jne	.L298
.L301:
	movq	-144(%rbp), %r10
	cmpq	%r15, %r10
	jbe	.L299
	movq	-128(%rbp), %r11
	movq	-184(%rbp), %rax
	movq	-200(%rbp), %rsi
	movq	512(%r14), %rdi
	movq	%rsi, (%r11,%rax)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L335:
	movabsq	$2305843009213693951, %rax
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L337
	movq	%rbx, -184(%rbp)
	testq	%r15, %r15
	je	.L290
	movq	%r15, %rdi
	call	malloc@PLT
	movq	-184(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L338
	movq	%rax, -128(%rbp)
	movq	%rbx, -136(%rbp)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L293:
	movq	352(%rdi), %rdi
	movq	%r11, %rsi
	movq	%r10, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-216(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-208(%rbp), %rdi
	je	.L305
	testq	%rdi, %rdi
	je	.L305
	call	free@PLT
.L305:
	movq	-216(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L339
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L334:
	leaq	_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L290:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L338:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %r11
	testq	%rax, %rax
	je	.L290
	movq	-144(%rbp), %rax
	movq	48(%r14), %r12
	movq	%r11, -128(%rbp)
	movq	%rbx, -136(%rbp)
	movq	56(%r14), %r13
	testq	%rax, %rax
	movq	-184(%rbp), %r10
	je	.L288
	movq	-208(%rbp), %rsi
	movq	%r11, %rdi
	leaq	0(,%rax,8), %rdx
	movq	%r10, -184(%rbp)
	call	memcpy@PLT
	movq	-184(%rbp), %r10
	movq	%rax, %r11
	jmp	.L288
.L337:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L339:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7564:
	.size	_ZN4node17SyncProcessRunner16BuildOutputArrayEv, .-_ZN4node17SyncProcessRunner16BuildOutputArrayEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner17BuildResultObjectEv
	.type	_ZN4node17SyncProcessRunner17BuildResultObjectEv, @function
_ZN4node17SyncProcessRunner17BuildResultObjectEv:
.LFB7563:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	512(%rbx), %rax
	movq	352(%rax), %rdi
	movq	3280(%rax), %r13
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	500(%rbx), %esi
	movq	%rax, %r12
	testl	%esi, %esi
	je	.L375
.L341:
	movq	512(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	512(%rbx), %rax
	movq	360(%rax), %rax
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L376
.L342:
	movq	512(%rbx), %rdx
	movq	328(%rbx), %rax
	movq	352(%rdx), %rdi
	testq	%rax, %rax
	js	.L344
	movl	336(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L345
.L344:
	movq	360(%rdx), %rax
	leaq	104(%rdi), %rcx
.L370:
	movq	1672(%rax), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L377
.L347:
	movl	336(%rbx), %edi
	testl	%edi, %edi
	jle	.L350
	call	_ZN4node12signo_stringEi@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	512(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L378
.L351:
	movq	512(%rbx), %rax
.L372:
	movq	360(%rax), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	1600(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L379
.L353:
	cmpq	$0, 328(%rbx)
	js	.L355
	movq	%rbx, %rdi
	call	_ZN4node17SyncProcessRunner16BuildOutputArrayEv
	movq	%rax, %rcx
	movq	512(%rbx), %rax
.L374:
	movq	360(%rax), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	1296(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L380
.L357:
	pxor	%xmm0, %xmm0
	movq	512(%rbx), %rax
	cvtsi2sdl	280(%rbx), %xmm0
	movq	352(%rax), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	512(%rbx), %rax
	movq	360(%rax), %rax
	movq	1336(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L381
.L359:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	addq	$64, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movl	504(%rbx), %esi
	testl	%esi, %esi
	je	.L342
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L345:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, %rcx
	movq	512(%rbx), %rax
	movq	360(%rax), %rax
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L355:
	movq	512(%rbx), %rax
	movq	352(%rax), %rcx
	addq	$104, %rcx
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L350:
	movq	512(%rbx), %rax
	movq	352(%rax), %rsi
	leaq	104(%rsi), %rcx
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L379:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L377:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L380:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rcx
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L376:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L342
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7563:
	.size	_ZN4node17SyncProcessRunner17BuildResultObjectEv, .-_ZN4node17SyncProcessRunner17BuildResultObjectEv
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEE, @function
_ZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEE:
.LFB7567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	512(%rdi), %rax
	movq	%r12, %rdi
	movq	3280(%rax), %r13
	movq	360(%rax), %rax
	movq	1768(%rax), %rdx
	movq	%r13, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L435
.L384:
	movq	512(%r15), %rax
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	880(%rax), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L385
	cmpl	32(%r15), %ebx
	jnb	.L436
	movq	48(%r15), %rax
	cmpq	$0, (%rax,%rbx,8)
	jne	.L437
	salq	$4, %rbx
	addq	40(%r15), %rbx
	xorl	%eax, %eax
	movl	$0, (%rbx)
.L383:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	movq	512(%r15), %rax
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	1352(%rax), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	movq	512(%r15), %rax
	je	.L389
	movq	352(%rax), %r14
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	1472(%rax), %rdx
	movq	1936(%rax), %rax
	movq	%rax, -64(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L438
.L390:
	movq	%r14, %rsi
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	movq	-64(%rbp), %rdx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movb	%al, -56(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L439
.L391:
	movq	%r14, %rsi
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movb	%al, -73(%rbp)
	call	uv_buf_init@PLT
	cmpb	$0, -56(%rbp)
	movq	%rax, -64(%rbp)
	movq	%rdx, -72(%rbp)
	jne	.L440
.L393:
	cmpl	32(%r15), %ebx
	jnb	.L441
	movq	48(%r15), %rax
	leaq	0(,%rbx,8), %r14
	cmpq	$0, (%rax,%rbx,8)
	jne	.L442
	movl	$592, %edi
	call	_Znwm@PLT
	movzbl	-56(%rbp), %edx
	movzbl	-73(%rbp), %esi
	pxor	%xmm0, %xmm0
	movq	%r15, (%rax)
	movq	%rax, %r13
	movl	$33, %ecx
	movb	%dl, 8(%rax)
	leaq	48(%r13), %r12
	movb	%sil, 9(%rax)
	movq	-64(%rbp), %rax
	movq	%r12, %rdi
	movups	%xmm0, 32(%r13)
	pxor	%xmm0, %xmm0
	movq	%rax, 16(%r13)
	movq	-72(%rbp), %rax
	movq	%rax, 24(%r13)
	xorl	%eax, %eax
	rep stosq
	leaq	312(%r13), %rdi
	movl	$24, %ecx
	rep stosq
	movups	%xmm0, 504(%r13)
	movl	$0, 584(%r13)
	movups	%xmm0, 520(%r13)
	movups	%xmm0, 536(%r13)
	movups	%xmm0, 552(%r13)
	movups	%xmm0, 568(%r13)
	testb	%dl, %dl
	jne	.L400
	testb	%sil, %sil
	je	.L443
.L400:
	movq	24(%r15), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	uv_pipe_init@PLT
	testl	%eax, %eax
	js	.L401
	cmpl	$2, 584(%r13)
	jg	.L444
	salq	$4, %rbx
	addq	40(%r15), %rbx
	cmpb	$1, 8(%r13)
	movq	%r13, 48(%r13)
	movl	$1, 584(%r13)
	sbbl	%edx, %edx
	andl	$-16, %edx
	addl	$49, %edx
	cmpb	$1, 8(%r13)
	sbbl	%eax, %eax
	andl	$-16, %eax
	addl	$17, %eax
	cmpb	$0, 9(%r13)
	movq	%r12, 8(%rbx)
	cmovne	%edx, %eax
	addq	48(%r15), %r14
	movq	%r14, %r12
	movq	(%r14), %r14
	movl	%eax, (%rbx)
	movq	%r13, (%r12)
	testq	%r14, %r14
	je	.L434
	testl	$-5, 584(%r14)
	jne	.L410
	movq	32(%r14), %rbx
	testq	%rbx, %rbx
	je	.L412
	.p2align 4,,10
	.p2align 3
.L411:
	movq	%rbx, %rdi
	movq	65544(%rbx), %rbx
	movl	$65552, %esi
	call	_ZdlPvm@PLT
	testq	%rbx, %rbx
	jne	.L411
.L412:
	movl	$592, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	xorl	%eax, %eax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L389:
	movq	360(%rax), %rax
	movq	%r14, %rdi
	movq	896(%rax), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L445
.L413:
	movq	512(%r15), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	728(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L446
.L414:
	movq	%r13, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L447
.L415:
	sarq	$32, %r12
	cmpl	32(%r15), %ebx
	jnb	.L448
	movq	48(%r15), %rax
	cmpq	$0, (%rax,%rbx,8)
	jne	.L449
	salq	$4, %rbx
	addq	40(%r15), %rbx
	movl	$2, (%rbx)
	movl	%r12d, 8(%rbx)
.L434:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movq	512(%r15), %rax
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	904(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L450
.L394:
	movq	%r12, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	jne	.L451
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L419
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L419
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L393
	.p2align 4,,10
	.p2align 3
.L419:
	movl	$-22, %eax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L445:
	movq	512(%r15), %rax
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	728(%rax), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	jne	.L413
	leaq	_ZZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L436:
	leaq	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L435:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%r12, %rdi
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movl	%r13d, %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	movq	%rax, -64(%rbp)
	movq	%rdx, -72(%rbp)
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L401:
	testl	$-5, 584(%r13)
	jne	.L410
	movq	32(%r13), %rbx
	testq	%rbx, %rbx
	je	.L408
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%rbx, %rdi
	movq	65544(%rbx), %rbx
	movl	$65552, %esi
	movl	%eax, -56(%rbp)
	call	_ZdlPvm@PLT
	movl	-56(%rbp), %eax
	testq	%rbx, %rbx
	jne	.L406
.L408:
	movl	$592, %esi
	movq	%r13, %rdi
	movl	%eax, -56(%rbp)
	call	_ZdlPvm@PLT
	movl	-56(%rbp), %eax
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdi
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L446:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L447:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L448:
	leaq	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L443:
	leaq	_ZZN4node20SyncProcessStdioPipeC4EPNS_17SyncProcessRunnerEbb8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L394
.L444:
	leaq	_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L410:
	leaq	_ZZN4node20SyncProcessStdioPipeD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7567:
	.size	_ZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEE, .-_ZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner5IsSetEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node17SyncProcessRunner5IsSetEN2v85LocalINS1_5ValueEEE, @function
_ZN4node17SyncProcessRunner5IsSetEN2v85LocalINS1_5ValueEEE:
.LFB7572:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movl	$1, %r8d
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L452
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L456
.L452:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	setne	%r8b
	movl	%r8d, %eax
	ret
	.cfi_endproc
.LFE7572:
	.size	_ZN4node17SyncProcessRunner5IsSetEN2v85LocalINS1_5ValueEEE, .-_ZN4node17SyncProcessRunner5IsSetEN2v85LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner12CopyJsStringEN2v85LocalINS1_5ValueEEEPPKc
	.type	_ZN4node17SyncProcessRunner12CopyJsStringEN2v85LocalINS1_5ValueEEEPPKc, @function
_ZN4node17SyncProcessRunner12CopyJsStringEN2v85LocalINS1_5ValueEEEPPKc:
.LFB7573:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	512(%rdi), %rax
	movq	%rdx, %rbx
	movq	352(%rax), %r14
	movq	(%rsi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L458
	movq	-1(%rax), %rax
	movq	%rsi, %r13
	cmpw	$63, 11(%rax)
	ja	.L458
.L459:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	testb	%al, %al
	je	.L462
	leaq	1(%rdx), %rdi
	call	_Znam@PLT
	movq	%r13, %rcx
	movq	%r14, %rdi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	$-1, %rdx
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	movq	%r12, (%rbx)
	movb	$0, (%r12,%rax)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	jne	.L459
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7573:
	.size	_ZN4node17SyncProcessRunner12CopyJsStringEN2v85LocalINS1_5ValueEEEPPKc, .-_ZN4node17SyncProcessRunner12CopyJsStringEN2v85LocalINS1_5ValueEEEPPKc
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner17CopyJsStringArrayEN2v85LocalINS1_5ValueEEEPPc
	.type	_ZN4node17SyncProcessRunner17CopyJsStringArrayEN2v85LocalINS1_5ValueEEEPPc, @function
_ZN4node17SyncProcessRunner17CopyJsStringArrayEN2v85LocalINS1_5ValueEEEPPc:
.LFB7574:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	512(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	%rsi, %rdi
	movq	%rdx, -80(%rbp)
	movq	352(%rax), %rax
	movq	%rax, -56(%rbp)
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L468
	movabsq	$-94489280511, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	movq	-64(%rbp), %rax
	movq	%r12, %rdi
	movq	512(%rax), %rax
	movq	3280(%rax), %r14
	call	_ZN2v86Object5CloneEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -68(%rbp)
	movl	%eax, %ebx
	leal	1(%rax), %eax
	salq	$3, %rax
	movq	%rax, -96(%rbp)
	testl	%ebx, %ebx
	je	.L470
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L493:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L496
.L473:
	movq	-56(%rbp), %rdi
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	testb	%al, %al
	je	.L474
	leaq	1(%rbx,%rdx), %rbx
	movq	%rbx, %rdx
	leaq	8(%rbx), %rax
	andl	$7, %edx
	subq	%rdx, %rax
	testq	%rdx, %rdx
	cmovne	%rax, %rbx
	leal	1(%r12), %eax
	cmpl	%eax, -68(%rbp)
	je	.L479
	movl	%eax, %r12d
.L480:
	movl	%r12d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L497
.L471:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L472
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L473
.L472:
	movq	-64(%rbp), %rax
	movq	512(%rax), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L493
.L474:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L496:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L479:
	movq	-96(%rbp), %rax
	xorl	%r15d, %r15d
	leaq	(%rax,%rbx), %rdi
	call	_Znam@PLT
	movl	%r12d, %esi
	movq	%r13, -88(%rbp)
	movq	-96(%rbp), %r12
	movq	%rax, %rbx
	movq	%r14, %rax
	movq	%rsi, -64(%rbp)
	movq	%r15, %r14
	movq	%rax, %r15
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L487:
	movq	%rax, %r14
.L483:
	leaq	(%rbx,%r12), %r13
	movq	-88(%rbp), %rdi
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%r13, (%rbx,%r14,8)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L498
.L481:
	movq	-56(%rbp), %rdi
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%r13, %rsi
	movq	$-1, %rdx
	call	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	addq	%r12, %rax
	leaq	1(%rax), %r12
	movb	$0, (%rbx,%rax)
	addq	$9, %rax
	movq	%r12, %rdx
	andl	$7, %edx
	subq	%rdx, %rax
	testq	%rdx, %rdx
	cmovne	%rax, %r12
	leaq	1(%r14), %rax
	cmpq	%r14, -64(%rbp)
	jne	.L487
.L486:
	movl	-68(%rbp), %eax
	movq	$0, (%rbx,%rax,8)
	movq	-80(%rbp), %rax
	movq	%rbx, (%rax)
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rcx
	jmp	.L481
.L470:
	movq	%rax, %rdi
	call	_Znam@PLT
	movq	%rax, %rbx
	jmp	.L486
	.cfi_endproc
.LFE7574:
	.size	_ZN4node17SyncProcessRunner17CopyJsStringArrayEN2v85LocalINS1_5ValueEEEPPc, .-_ZN4node17SyncProcessRunner17CopyJsStringArrayEN2v85LocalINS1_5ValueEEEPPc
	.p2align 4
	.globl	_Z20_register_spawn_syncv
	.type	_Z20_register_spawn_syncv, @function
_Z20_register_spawn_syncv:
.LFB7578:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7578:
	.size	_Z20_register_spawn_syncv, .-_Z20_register_spawn_syncv
	.section	.rodata._ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC4:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm
	.type	_ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm, @function
_ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm:
.LFB8766:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L542
	movabsq	$1152921504606846975, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%rdx, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %rcx
	movq	%rcx, %r12
	subq	(%rdi), %r12
	movq	%r12, %rax
	sarq	$3, %rax
	movq	%rax, -72(%rbp)
	subq	%rax, %rsi
	movq	16(%rdi), %rax
	subq	%rcx, %rax
	sarq	$3, %rax
	cmpq	%rbx, %rax
	jb	.L502
	cmpq	$1, %rbx
	je	.L520
	leaq	-2(%rbx), %rdx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L504:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rcx,%rsi)
	cmpq	%rdx, %rax
	jb	.L504
	leaq	(%rdx,%rdx), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L505
.L503:
	movq	$0, (%rdx)
.L505:
	leaq	(%rcx,%rbx,8), %rax
	movq	%rax, 8(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rsi
	jb	.L545
	movq	-72(%rbp), %rcx
	cmpq	%rbx, %rcx
	movq	%rcx, %rax
	cmovb	%rbx, %rax
	addq	%rcx, %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	_Znwm@PLT
	movq	%rax, -80(%rbp)
	leaq	(%rax,%r12), %rdx
	cmpq	$1, %rbx
	je	.L519
	leaq	-2(%rbx), %rcx
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%rax, %rsi
	addq	$1, %rax
	salq	$4, %rsi
	movups	%xmm0, (%rdx,%rsi)
	cmpq	%rcx, %rax
	jb	.L511
	leaq	(%rcx,%rcx), %rax
	salq	$4, %rcx
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	je	.L509
.L519:
	movq	$0, (%rdx)
.L509:
	movq	8(%r13), %rax
	movq	0(%r13), %r12
	movq	%rax, -64(%rbp)
	cmpq	%r12, %rax
	je	.L512
	movq	-80(%rbp), %rax
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L517:
	movq	(%r12), %rax
	movq	-56(%rbp), %rcx
	movq	$0, (%r12)
	movq	%rax, (%rcx)
	movq	(%r12), %r14
	testq	%r14, %r14
	je	.L513
	testl	$-5, 584(%r14)
	jne	.L546
	movq	32(%r14), %r15
	testq	%r15, %r15
	je	.L516
	.p2align 4,,10
	.p2align 3
.L515:
	movq	%r15, %rdi
	movq	65544(%r15), %r15
	movl	$65552, %esi
	call	_ZdlPvm@PLT
	testq	%r15, %r15
	jne	.L515
.L516:
	movl	$592, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L513:
	addq	$8, -56(%rbp)
	addq	$8, %r12
	cmpq	%r12, -64(%rbp)
	jne	.L517
	movq	0(%r13), %r12
.L512:
	testq	%r12, %r12
	je	.L518
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L518:
	movq	-80(%rbp), %rax
	addq	-72(%rbp), %rbx
	movq	%rax, 0(%r13)
	movq	%rax, %rcx
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 8(%r13)
	movq	-88(%rbp), %rax
	addq	%rcx, %rax
	movq	%rax, 16(%r13)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	leaq	_ZZN4node20SyncProcessStdioPipeD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L520:
	movq	%rcx, %rdx
	jmp	.L503
.L545:
	leaq	.LC4(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8766:
	.size	_ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm, .-_ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner17ParseStdioOptionsEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node17SyncProcessRunner17ParseStdioOptionsEN2v85LocalINS1_5ValueEEE, @function
_ZN4node17SyncProcessRunner17ParseStdioOptionsEN2v85LocalINS1_5ValueEEE:
.LFB7566:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	movq	352(%rax), %rsi
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L565
	movq	512(%rbx), %rax
	movq	%r14, %rdi
	movq	3280(%rax), %rax
	movq	%rax, -88(%rbp)
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, 32(%rbx)
	movl	%eax, %edi
	salq	$4, %rdi
	call	_Znam@PLT
	movq	48(%rbx), %r12
	movq	%rax, 40(%rbx)
	movq	56(%rbx), %rax
	movq	%r12, -104(%rbp)
	movq	%rax, -96(%rbp)
	cmpq	%rax, %r12
	je	.L550
	.p2align 4,,10
	.p2align 3
.L555:
	movq	(%r12), %r13
	testq	%r13, %r13
	je	.L551
	testl	$-5, 584(%r13)
	jne	.L559
	movq	32(%r13), %r15
	testq	%r15, %r15
	je	.L554
	.p2align 4,,10
	.p2align 3
.L553:
	movq	%r15, %rdi
	movq	65544(%r15), %r15
	movl	$65552, %esi
	call	_ZdlPvm@PLT
	testq	%r15, %r15
	jne	.L553
.L554:
	movl	$592, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L551:
	addq	$8, %r12
	cmpq	%r12, -96(%rbp)
	jne	.L555
	movq	-104(%rbp), %rax
	movq	48(%rbx), %rcx
	movl	32(%rbx), %esi
	movq	%rax, 56(%rbx)
	subq	%rcx, %rax
	sarq	$3, %rax
	movq	%rsi, %rdx
	cmpq	%rax, %rsi
	ja	.L567
	jnb	.L557
	leaq	(%rcx,%rsi,8), %rax
	movq	%rax, -96(%rbp)
	cmpq	%rax, -104(%rbp)
	je	.L557
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L562:
	movq	(%r15), %r12
	testq	%r12, %r12
	je	.L558
	testl	$-5, 584(%r12)
	jne	.L559
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L561
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%r13, %rdi
	movq	65544(%r13), %r13
	movl	$65552, %esi
	call	_ZdlPvm@PLT
	testq	%r13, %r13
	jne	.L560
.L561:
	movl	$592, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L558:
	addq	$8, %r15
	cmpq	%r15, -104(%rbp)
	jne	.L562
	movq	-96(%rbp), %rax
	movl	32(%rbx), %edx
	movq	%rax, 56(%rbx)
	.p2align 4,,10
	.p2align 3
.L557:
	movb	$1, 72(%rbx)
	testl	%edx, %edx
	je	.L569
	xorl	%r12d, %r12d
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L565
.L593:
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	_ZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEE
	testl	%eax, %eax
	js	.L549
	movl	32(%rbx), %eax
	addl	$1, %r12d
	cmpl	%r12d, %eax
	jbe	.L563
.L566:
	movq	-88(%rbp), %rsi
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L564
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L593
	.p2align 4,,10
	.p2align 3
.L565:
	movl	$-22, %eax
.L549:
	movq	-112(%rbp), %rdi
	movl	%eax, -88(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-88(%rbp), %eax
	jne	.L594
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	movl	32(%rbx), %esi
	movq	%rsi, %rdx
	testq	%rsi, %rsi
	je	.L557
	xorl	%eax, %eax
.L567:
	subq	%rax, %rsi
	leaq	48(%rbx), %rdi
	call	_ZNSt6vectorISt10unique_ptrIN4node20SyncProcessStdioPipeESt14default_deleteIS2_EESaIS5_EE17_M_default_appendEm
	movl	32(%rbx), %edx
	jmp	.L557
.L569:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L563:
	movq	40(%rbx), %rdx
	movl	%eax, 124(%rbx)
	xorl	%eax, %eax
	movq	%rdx, 128(%rbx)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	_ZZN4node20SyncProcessStdioPipeD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L594:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7566:
	.size	_ZN4node17SyncProcessRunner17ParseStdioOptionsEN2v85LocalINS1_5ValueEEE, .-_ZN4node17SyncProcessRunner17ParseStdioOptionsEN2v85LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEE, @function
_ZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEE:
.LFB7565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	movq	%rbx, %rdi
	movq	352(%rax), %r14
	movq	%r14, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L596
	movabsq	$-94489280511, %r12
.L597:
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L658
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L596:
	.cfi_restore_state
	movq	512(%r12), %rax
	movq	%r13, %rdi
	movq	3280(%rax), %r15
	movq	360(%rax), %rax
	movq	744(%rax), %rdx
	movq	%r15, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L659
.L598:
	leaq	144(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner12CopyJsStringEN2v85LocalINS1_5ValueEEEPPKc
	testb	%al, %al
	je	.L599
	sarq	$32, %rax
	testl	%eax, %eax
	jns	.L600
.L657:
	salq	$32, %rax
	orq	$1, %rax
	movq	%rax, %r12
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L599:
	xorl	%r12d, %r12d
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L600:
	movq	144(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, 88(%r12)
	movq	512(%r12), %rax
	movq	360(%rax), %rax
	movq	200(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L660
.L601:
	leaq	152(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner17CopyJsStringArrayEN2v85LocalINS1_5ValueEEEPPc
	testb	%al, %al
	je	.L599
	sarq	$32, %rax
	testl	%eax, %eax
	js	.L657
	movq	152(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, 96(%r12)
	movq	512(%r12), %rax
	movq	360(%rax), %rax
	movq	432(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L661
.L604:
	movq	(%rsi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L605
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L605
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	jne	.L605
.L606:
	movq	512(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	360(%rax), %rax
	movq	624(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L662
.L609:
	movq	(%rsi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L610
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L610
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	jne	.L610
.L611:
	movq	512(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	1776(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L663
.L614:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L615
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L615
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L616
.L615:
	movq	%rdi, -88(%rbp)
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movq	-88(%rbp), %rdi
	testb	%al, %al
	je	.L664
	call	_ZNK2v85Int325ValueEv@PLT
	orl	$1, 120(%r12)
	movl	%eax, 136(%r12)
.L616:
	movq	512(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	808(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L665
.L618:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L619
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L619
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L620
.L619:
	movq	%rdi, -88(%rbp)
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movq	-88(%rbp), %rdi
	testb	%al, %al
	je	.L666
	call	_ZNK2v85Int325ValueEv@PLT
	orl	$2, 120(%r12)
	movl	%eax, 140(%r12)
.L620:
	movq	512(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	464(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L667
.L622:
	movq	%r14, %rsi
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	testb	%al, %al
	je	.L623
	orl	$8, 120(%r12)
.L623:
	movq	512(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	1912(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L668
.L624:
	movq	%r14, %rsi
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	testb	%al, %al
	je	.L625
	orl	$16, 120(%r12)
.L625:
	movq	512(%r12), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	1920(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L669
.L626:
	movq	%r14, %rsi
	call	_ZNK2v85Value12BooleanValueEPNS_7IsolateE@PLT
	testb	%al, %al
	je	.L627
	orl	$4, 120(%r12)
.L627:
	movq	512(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	360(%rax), %rax
	movq	1736(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L670
.L628:
	movq	(%r14), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L629
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L629
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L630
.L629:
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L671
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %r14
	testb	%al, %al
	je	.L672
.L632:
	movq	%r14, 8(%r12)
.L630:
	movq	512(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	360(%rax), %rax
	movq	1000(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L673
.L633:
	movq	(%r14), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L634
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L634
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L635
.L634:
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L674
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK2v85Value11NumberValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L675
.L637:
	movsd	%xmm0, (%r12)
.L635:
	movq	512(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	360(%rax), %rax
	movq	968(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L676
.L638:
	movq	(%r14), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L639
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L639
	movslq	43(%rax), %rax
	subq	$3, %rax
	testq	$-3, %rax
	je	.L640
.L639:
	movq	%r14, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L677
	movq	%r14, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, 16(%r12)
.L640:
	movq	512(%r12), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	360(%rax), %rax
	movq	1680(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L678
.L642:
	movq	%r12, %rdi
	movl	$1, %r12d
	call	_ZN4node17SyncProcessRunner17ParseStdioOptionsEN2v85LocalINS1_5ValueEEE
	testl	%eax, %eax
	jns	.L597
	movb	$1, %r12b
	salq	$32, %rax
	movl	%r12d, %r12d
	orq	%rax, %r12
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L659:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L605:
	leaq	168(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner12CopyJsStringEN2v85LocalINS1_5ValueEEEPPKc
	testb	%al, %al
	je	.L599
	sarq	$32, %rax
	testl	%eax, %eax
	js	.L657
	movq	168(%r12), %rax
	movq	%rax, 112(%r12)
	jmp	.L606
.L610:
	leaq	160(%r12), %rdx
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner17CopyJsStringArrayEN2v85LocalINS1_5ValueEEEPPc
	testb	%al, %al
	je	.L599
	sarq	$32, %rax
	testl	%eax, %eax
	js	.L657
	movq	160(%r12), %rax
	movq	%rax, 104(%r12)
	jmp	.L611
.L660:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L601
.L661:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L604
.L662:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L609
.L663:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L614
.L665:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L618
.L676:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L638
.L670:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L628
.L669:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L626
.L668:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L624
.L667:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L622
.L678:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L642
.L673:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L633
.L658:
	call	__stack_chk_fail@PLT
.L666:
	leaq	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L664:
	leaq	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L671:
	leaq	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L674:
	leaq	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L675:
	movsd	%xmm0, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movsd	-88(%rbp), %xmm0
	jmp	.L637
.L677:
	leaq	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L672:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L632
	.cfi_endproc
.LFE7565:
	.size	_ZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEE, .-_ZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEE, @function
_ZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEE:
.LFB7550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movl	508(%rdi), %r8d
	testl	%r8d, %r8d
	jne	.L712
	movl	$1, 508(%rdi)
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movl	$848, %edi
	call	_Znwm@PLT
	movq	%rax, 24(%rbx)
	movq	%rax, %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L713
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEE
	testb	%al, %al
	je	.L682
	sarq	$32, %rax
	testl	%eax, %eax
	js	.L709
	cmpq	$0, 8(%rbx)
	movq	24(%rbx), %rdi
	jne	.L714
.L687:
	leaq	_ZN4node17SyncProcessRunner12ExitCallbackEP12uv_process_sli(%rip), %rax
	leaq	80(%rbx), %rdx
	movq	%rax, 80(%rbx)
	leaq	176(%rbx), %rsi
	call	uv_spawn@PLT
	testl	%eax, %eax
	js	.L709
	movq	56(%rbx), %r13
	movq	48(%rbx), %r12
	movq	%rbx, 176(%rbx)
	cmpq	%r12, %r13
	je	.L697
	.p2align 4,,10
	.p2align 3
.L696:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L695
	call	_ZN4node20SyncProcessStdioPipe5StartEv
	testl	%eax, %eax
	js	.L715
.L695:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L696
.L697:
	movq	24(%rbx), %rdi
	xorl	%esi, %esi
	call	uv_run@PLT
	testl	%eax, %eax
	js	.L716
	cmpq	$0, 328(%rbx)
	movl	$257, %eax
	jns	.L701
	leaq	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	344(%rbx), %r12
	movq	%r12, %rsi
	call	uv_timer_init@PLT
	testl	%eax, %eax
	jns	.L688
	.p2align 4,,10
	.p2align 3
.L709:
	movl	500(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L717
.L685:
	movl	$1, %eax
.L701:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	movb	$0, %ah
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L717:
	.cfi_restore_state
	movl	%eax, 500(%rbx)
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L712:
	leaq	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%r12, %rdi
	call	uv_unref@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%rbx, 344(%rbx)
	leaq	_ZN4node17SyncProcessRunner17KillTimerCallbackEP10uv_timer_s(%rip), %rsi
	movb	$1, 496(%rbx)
	call	uv_timer_start@PLT
	testl	%eax, %eax
	js	.L709
	movq	24(%rbx), %rdi
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L715:
	movl	504(%rbx), %edx
	testl	%edx, %edx
	jne	.L685
	movl	%eax, 504(%rbx)
	jmp	.L685
.L716:
	call	_ZN4node5AbortEv@PLT
	.cfi_endproc
.LFE7550:
	.size	_ZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEE, .-_ZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEE
	.type	_ZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEE, @function
_ZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEE:
.LFB7546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-80(%rbp), %r14
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	512(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movl	508(%r12), %eax
	testl	%eax, %eax
	jne	.L724
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEE
	movq	%r12, %rdi
	movl	%eax, %r13d
	call	_ZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEv
	testb	%r13b, %r13b
	jne	.L720
	xorl	%r12d, %r12d
.L721:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L725
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L720:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner17BuildResultObjectEv
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	_ZZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L725:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7546:
	.size	_ZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEE, .-_ZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node17SyncProcessRunner5SpawnERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node17SyncProcessRunner5SpawnERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node17SyncProcessRunner5SpawnERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$560, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L738
	cmpw	$1040, %cx
	jne	.L727
.L738:
	movq	23(%rdx), %r12
.L729:
	movq	%r12, %rdi
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%eax, %eax
	movl	$17, %ecx
	movl	16(%rbx), %edx
	leaq	-384(%rbp), %rdi
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	movq	$0x000000000, -560(%rbp)
	rep stosq
	movl	$19, %ecx
	leaq	-216(%rbp), %rdi
	movq	$0, -552(%rbp)
	rep stosq
	movb	$0, -64(%rbp)
	movl	$15, -544(%rbp)
	movq	$0, -536(%rbp)
	movl	$0, -528(%rbp)
	movb	$0, -488(%rbp)
	movb	$0, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	$-1, -232(%rbp)
	movl	$-1, -224(%rbp)
	movl	$0, -60(%rbp)
	movq	$0, -56(%rbp)
	movq	%r12, -48(%rbp)
	movups	%xmm0, -520(%rbp)
	movups	%xmm0, -504(%rbp)
	movaps	%xmm1, -480(%rbp)
	movaps	%xmm1, -464(%rbp)
	movaps	%xmm1, -448(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	testl	%edx, %edx
	jg	.L730
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L731:
	movq	352(%r12), %rsi
	leaq	-592(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movl	-52(%rbp), %eax
	testl	%eax, %eax
	jne	.L741
	leaq	-560(%rbp), %r12
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEE
	movq	%r12, %rdi
	movl	%eax, %r14d
	call	_ZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEv
	testb	%r14b, %r14b
	jne	.L733
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
.L740:
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunnerD1Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L742
	addq	$560, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L733:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN4node17SyncProcessRunner17BuildResultObjectEv
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v811HandleScopeD2Ev@PLT
	testq	%r14, %r14
	je	.L740
	movq	(%r14), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L730:
	movq	8(%rbx), %r14
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L727:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L741:
	leaq	_ZZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L742:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7529:
	.size	_ZN4node17SyncProcessRunner5SpawnERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node17SyncProcessRunner5SpawnERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC5:
	.string	"../src/util-inl.h:374"
.LC6:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC8:
	.string	"../src/util-inl.h:325"
.LC9:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args
	.section	.rodata.str1.1
.LC11:
	.string	"../src/util.h:352"
.LC12:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 8; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.section	.rodata.str1.1
.LC14:
	.string	"../src/spawn_sync.cc"
.LC15:
	.string	"spawn_sync"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC14
	.quad	0
	.quad	_ZN4node17SyncProcessRunner10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC15
	.quad	0
	.quad	0
	.weak	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args_0
	.section	.rodata.str1.1
.LC16:
	.string	"../src/spawn_sync.cc:968"
.LC17:
	.string	"!stdio_pipes_[child_fd]"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"int node::SyncProcessRunner::AddStdioInheritFD(uint32_t, int)"
	.section	.data.rel.ro.local._ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args_0,"awG",@progbits,_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args_0,comdat
	.align 16
	.type	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args_0, @gnu_unique_object
	.size	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args_0, 24
_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args_0:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.weak	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/spawn_sync.cc:967"
.LC20:
	.string	"(child_fd) < (stdio_count_)"
	.section	.data.rel.ro.local._ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args,"awG",@progbits,_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args,comdat
	.align 16
	.type	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args, @gnu_unique_object
	.size	_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args, 24
_ZZN4node17SyncProcessRunner17AddStdioInheritFDEjiE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC18
	.weak	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args_0
	.section	.rodata.str1.1
.LC21:
	.string	"../src/spawn_sync.cc:946"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"int node::SyncProcessRunner::AddStdioPipe(uint32_t, bool, bool, uv_buf_t)"
	.section	.data.rel.ro.local._ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args_0,"awG",@progbits,_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args_0,comdat
	.align 16
	.type	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args_0, @gnu_unique_object
	.size	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args_0, 24
_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args_0:
	.quad	.LC21
	.quad	.LC17
	.quad	.LC22
	.weak	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args
	.section	.rodata.str1.1
.LC23:
	.string	"../src/spawn_sync.cc:945"
	.section	.data.rel.ro.local._ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args,"awG",@progbits,_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args,comdat
	.align 16
	.type	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args, @gnu_unique_object
	.size	_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args, 24
_ZZN4node17SyncProcessRunner12AddStdioPipeEjbb8uv_buf_tE4args:
	.quad	.LC23
	.quad	.LC20
	.quad	.LC22
	.weak	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args_0
	.section	.rodata.str1.1
.LC24:
	.string	"../src/spawn_sync.cc:933"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"int node::SyncProcessRunner::AddStdioIgnore(uint32_t)"
	.section	.data.rel.ro.local._ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args_0,"awG",@progbits,_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args_0,comdat
	.align 16
	.type	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args_0, @gnu_unique_object
	.size	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args_0, 24
_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args_0:
	.quad	.LC24
	.quad	.LC17
	.quad	.LC25
	.weak	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args
	.section	.rodata.str1.1
.LC26:
	.string	"../src/spawn_sync.cc:932"
	.section	.data.rel.ro.local._ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args,"awG",@progbits,_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args,comdat
	.align 16
	.type	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args, @gnu_unique_object
	.size	_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args, 24
_ZZN4node17SyncProcessRunner14AddStdioIgnoreEjE4args:
	.quad	.LC26
	.quad	.LC20
	.quad	.LC25
	.section	.rodata.str1.1
.LC27:
	.string	"../src/spawn_sync.cc:925"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"0 && \"invalid child stdio type\""
	.align 8
.LC29:
	.string	"int node::SyncProcessRunner::ParseStdioOption(int, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEEE4args, @object
	.size	_ZZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node17SyncProcessRunner16ParseStdioOptionEiN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/spawn_sync.cc:831"
.LC31:
	.string	"js_kill_signal->IsInt32()"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"v8::Maybe<int> node::SyncProcessRunner::ParseOptions(v8::Local<v8::Value>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_3, 24
_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_3:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/spawn_sync.cc:824"
.LC34:
	.string	"js_max_buffer->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_2, 24
_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_2:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC32
	.section	.rodata.str1.1
.LC35:
	.string	"../src/spawn_sync.cc:816"
.LC36:
	.string	"js_timeout->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_1, 24
_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_1:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC32
	.section	.rodata.str1.1
.LC37:
	.string	"../src/spawn_sync.cc:790"
.LC38:
	.string	"js_gid->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_0, 24
_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args_0:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC32
	.section	.rodata.str1.1
.LC39:
	.string	"../src/spawn_sync.cc:781"
.LC40:
	.string	"js_uid->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args, @object
	.size	_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args, 24
_ZZN4node17SyncProcessRunner12ParseOptionsEN2v85LocalINS1_5ValueEEEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC32
	.section	.rodata.str1.1
.LC41:
	.string	"../src/spawn_sync.cc:722"
.LC42:
	.string	"!stdio_pipes_.empty()"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"v8::Local<v8::Array> node::SyncProcessRunner::BuildOutputArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args_0, @object
	.size	_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args_0, 24
_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args_0:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/spawn_sync.cc:721"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"(lifecycle_) >= (kInitialized)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args, @object
	.size	_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args, 24
_ZZN4node17SyncProcessRunner16BuildOutputArrayEvE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC43
	.section	.rodata.str1.1
.LC46:
	.string	"../src/spawn_sync.cc:579"
.LC47:
	.string	"(uv_loop_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"void node::SyncProcessRunner::CloseKillTimer()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_1, @object
	.size	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_1, 24
_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_1:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.section	.rodata.str1.1
.LC49:
	.string	"../src/spawn_sync.cc:578"
.LC50:
	.string	"(timeout_) > (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_0, @object
	.size	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_0, 24
_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args_0:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC48
	.section	.rodata.str1.1
.LC51:
	.string	"../src/spawn_sync.cc:575"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"(lifecycle_) < (kHandlesClosed)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args, @object
	.size	_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args, 24
_ZZN4node17SyncProcessRunner14CloseKillTimerEvE4args:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC48
	.section	.rodata.str1.1
.LC53:
	.string	"../src/spawn_sync.cc:562"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"void node::SyncProcessRunner::CloseStdioPipes()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_1, @object
	.size	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_1, 24
_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_1:
	.quad	.LC53
	.quad	.LC47
	.quad	.LC54
	.section	.rodata.str1.1
.LC55:
	.string	"../src/spawn_sync.cc:561"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_0, @object
	.size	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_0, 24
_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args_0:
	.quad	.LC55
	.quad	.LC42
	.quad	.LC54
	.section	.rodata.str1.1
.LC56:
	.string	"../src/spawn_sync.cc:558"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args, @object
	.size	_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args, 24
_ZZN4node17SyncProcessRunner15CloseStdioPipesEvE4args:
	.quad	.LC56
	.quad	.LC52
	.quad	.LC54
	.section	.rodata.str1.1
.LC57:
	.string	"../src/spawn_sync.cc:550"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"(false) == (kill_timer_initialized_)"
	.align 8
.LC59:
	.string	"void node::SyncProcessRunner::CloseHandlesAndDeleteLoop()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_1, @object
	.size	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_1, 24
_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_1:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.section	.rodata.str1.1
.LC60:
	.string	"../src/spawn_sync.cc:549"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"(false) == (stdio_pipes_initialized_)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_0, @object
	.size	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_0, 24
_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args_0:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC59
	.section	.rodata.str1.1
.LC62:
	.string	"../src/spawn_sync.cc:521"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args, @object
	.size	_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args, 24
_ZZN4node17SyncProcessRunner25CloseHandlesAndDeleteLoopEvE4args:
	.quad	.LC62
	.quad	.LC52
	.quad	.LC59
	.section	.rodata.str1.1
.LC63:
	.string	"../src/spawn_sync.cc:515"
.LC64:
	.string	"(exit_status_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"v8::Maybe<bool> node::SyncProcessRunner::TryInitializeAndRunLoop(v8::Local<v8::Value>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_1, 24
_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_1:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.section	.rodata.str1.1
.LC66:
	.string	"../src/spawn_sync.cc:460"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"(uv_loop_init(uv_loop_)) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_0, 24
_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args_0:
	.quad	.LC66
	.quad	.LC67
	.quad	.LC65
	.section	.rodata.str1.1
.LC68:
	.string	"../src/spawn_sync.cc:452"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"(lifecycle_) == (kUninitialized)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args, @object
	.size	_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args, 24
_ZZN4node17SyncProcessRunner23TryInitializeAndRunLoopEN2v85LocalINS1_5ValueEEEE4args:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC65
	.section	.rodata.str1.1
.LC70:
	.string	"../src/spawn_sync.cc:436"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"v8::MaybeLocal<v8::Object> node::SyncProcessRunner::Run(v8::Local<v8::Value>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEEE4args, @object
	.size	_ZZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEEE4args, 24
_ZZN4node17SyncProcessRunner3RunEN2v85LocalINS1_5ValueEEEE4args:
	.quad	.LC70
	.quad	.LC69
	.quad	.LC71
	.section	.rodata.str1.1
.LC72:
	.string	"../src/spawn_sync.cc:418"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"(lifecycle_) == (kHandlesClosed)"
	.align 8
.LC74:
	.string	"node::SyncProcessRunner::~SyncProcessRunner()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node17SyncProcessRunnerD4EvE4args, @object
	.size	_ZZN4node17SyncProcessRunnerD4EvE4args, 24
_ZZN4node17SyncProcessRunnerD4EvE4args:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.weak	_ZZN4node20SyncProcessStdioPipe8SetErrorEiE4args
	.section	.rodata.str1.1
.LC75:
	.string	"../src/spawn_sync.cc:310"
.LC76:
	.string	"(error) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"void node::SyncProcessStdioPipe::SetError(int)"
	.section	.data.rel.ro.local._ZZN4node20SyncProcessStdioPipe8SetErrorEiE4args,"awG",@progbits,_ZZN4node20SyncProcessStdioPipe8SetErrorEiE4args,comdat
	.align 16
	.type	_ZZN4node20SyncProcessStdioPipe8SetErrorEiE4args, @gnu_unique_object
	.size	_ZZN4node20SyncProcessStdioPipe8SetErrorEiE4args, 24
_ZZN4node20SyncProcessStdioPipe8SetErrorEiE4args:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.weak	_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args
	.section	.rodata.str1.1
.LC78:
	.string	"../src/spawn_sync.cc:219"
.LC79:
	.string	"(lifecycle_) < (kClosing)"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"uv_pipe_t* node::SyncProcessStdioPipe::uv_pipe() const"
	.section	.data.rel.ro.local._ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args,"awG",@progbits,_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args,comdat
	.align 16
	.type	_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args, @gnu_unique_object
	.size	_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args, 24
_ZZNK4node20SyncProcessStdioPipe7uv_pipeEvE4args:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.section	.rodata.str1.1
.LC81:
	.string	"../src/spawn_sync.cc:179"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"lifecycle_ == kInitialized || lifecycle_ == kStarted"
	.align 8
.LC83:
	.string	"void node::SyncProcessStdioPipe::Close()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SyncProcessStdioPipe5CloseEvE4args, @object
	.size	_ZZN4node20SyncProcessStdioPipe5CloseEvE4args, 24
_ZZN4node20SyncProcessStdioPipe5CloseEvE4args:
	.quad	.LC81
	.quad	.LC82
	.quad	.LC83
	.section	.rodata.str1.1
.LC84:
	.string	"../src/spawn_sync.cc:152"
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"(input_buffer_.base) != nullptr"
	.align 8
.LC86:
	.string	"int node::SyncProcessStdioPipe::Start()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SyncProcessStdioPipe5StartEvE4args_0, @object
	.size	_ZZN4node20SyncProcessStdioPipe5StartEvE4args_0, 24
_ZZN4node20SyncProcessStdioPipe5StartEvE4args_0:
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.section	.rodata.str1.1
.LC87:
	.string	"../src/spawn_sync.cc:144"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"(lifecycle_) == (kInitialized)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SyncProcessStdioPipe5StartEvE4args, @object
	.size	_ZZN4node20SyncProcessStdioPipe5StartEvE4args, 24
_ZZN4node20SyncProcessStdioPipe5StartEvE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC86
	.section	.rodata.str1.1
.LC89:
	.string	"../src/spawn_sync.cc:130"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"int node::SyncProcessStdioPipe::Initialize(uv_loop_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_sE4args, @object
	.size	_ZZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_sE4args, 24
_ZZN4node20SyncProcessStdioPipe10InitializeEP9uv_loop_sE4args:
	.quad	.LC89
	.quad	.LC69
	.quad	.LC90
	.section	.rodata.str1.1
.LC91:
	.string	"../src/spawn_sync.cc:117"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"lifecycle_ == kUninitialized || lifecycle_ == kClosed"
	.align 8
.LC93:
	.string	"node::SyncProcessStdioPipe::~SyncProcessStdioPipe()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SyncProcessStdioPipeD4EvE4args, @object
	.size	_ZZN4node20SyncProcessStdioPipeD4EvE4args, 24
_ZZN4node20SyncProcessStdioPipeD4EvE4args:
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.section	.rodata.str1.1
.LC94:
	.string	"../src/spawn_sync.cc:112"
.LC95:
	.string	"readable || writable"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"node::SyncProcessStdioPipe::SyncProcessStdioPipe(node::SyncProcessRunner*, bool, bool, uv_buf_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SyncProcessStdioPipeC4EPNS_17SyncProcessRunnerEbb8uv_buf_tE4args, @object
	.size	_ZZN4node20SyncProcessStdioPipeC4EPNS_17SyncProcessRunnerEbb8uv_buf_tE4args, 24
_ZZN4node20SyncProcessStdioPipeC4EPNS_17SyncProcessRunnerEbb8uv_buf_tE4args:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.weak	_ZZN4node23SyncProcessOutputBuffer6OnReadEPK8uv_buf_tmE4args
	.section	.rodata.str1.1
.LC97:
	.string	"../src/spawn_sync.cc:64"
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"(buf->base) == (data_ + used())"
	.align 8
.LC99:
	.string	"void node::SyncProcessOutputBuffer::OnRead(const uv_buf_t*, size_t)"
	.section	.data.rel.ro.local._ZZN4node23SyncProcessOutputBuffer6OnReadEPK8uv_buf_tmE4args,"awG",@progbits,_ZZN4node23SyncProcessOutputBuffer6OnReadEPK8uv_buf_tmE4args,comdat
	.align 16
	.type	_ZZN4node23SyncProcessOutputBuffer6OnReadEPK8uv_buf_tmE4args, @gnu_unique_object
	.size	_ZZN4node23SyncProcessOutputBuffer6OnReadEPK8uv_buf_tmE4args, 24
_ZZN4node23SyncProcessOutputBuffer6OnReadEPK8uv_buf_tmE4args:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	0
	.quad	8
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
