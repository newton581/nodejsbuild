	.file	"node_crypto_bio.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4286:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4288:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4588:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4588:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4589:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4589:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7145:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7145:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7146:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7146:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7147:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7147:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7149:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L10
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7149:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZNK4node6crypto7NodeBIO8SelfSizeEv,"axG",@progbits,_ZNK4node6crypto7NodeBIO8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6crypto7NodeBIO8SelfSizeEv
	.type	_ZNK4node6crypto7NodeBIO8SelfSizeEv, @function
_ZNK4node6crypto7NodeBIO8SelfSizeEv:
.LFB8071:
	.cfi_startproc
	endbr64
	movl	$64, %eax
	ret
	.cfi_endproc
.LFE8071:
	.size	_ZNK4node6crypto7NodeBIO8SelfSizeEv, .-_ZNK4node6crypto7NodeBIO8SelfSizeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO3NewEP6bio_st
	.type	_ZN4node6crypto7NodeBIO3NewEP6bio_st, @function
_ZN4node6crypto7NodeBIO3NewEP6bio_st:
.LFB8082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$64, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %rsi
	movups	%xmm0, 32(%rax)
	leaq	16+_ZTVN4node6crypto7NodeBIOE(%rip), %rax
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%rsi)
	movq	$0, 8(%rsi)
	movl	$-1, 40(%rsi)
	movups	%xmm0, 16(%rsi)
	pxor	%xmm0, %xmm0
	movups	%xmm0, 48(%rsi)
	call	BIO_set_data@PLT
	movq	%r12, %rdi
	movl	$1, %esi
	call	BIO_set_init@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8082:
	.size	_ZN4node6crypto7NodeBIO3NewEP6bio_st, .-_ZN4node6crypto7NodeBIO3NewEP6bio_st
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10248:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L14
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	ret
	.cfi_endproc
.LFE10248:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10250:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10250:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.rodata._ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE.str1.1,"aMS",@progbits,1
.LC2:
	.string	"NodeBIO::Buffer"
.LC3:
	.string	"buffer"
	.section	.text._ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8069:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L19
	movl	$72, %edi
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movl	$15, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC2(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 64(%r12)
	leaq	-48(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	%r12, -48(%rbp)
	call	*8(%rax)
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	(%rdi), %rax
	call	*8(%rax)
.L22:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L19
	cmpq	72(%rbx), %rax
	je	.L33
.L24:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L19
	movq	8(%rbx), %rdi
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L19:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L24
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8069:
	.size	_ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev:
.LFB8070:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$79, 22(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$18754, %edx
	movl	$1701080910, 16(%rdi)
	movw	%dx, 20(%rdi)
	movq	$7, 8(%rdi)
	movb	$0, 23(%rdi)
	ret
	.cfi_endproc
.LFE8070:
	.size	_ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIOD0Ev
	.type	_ZN4node6crypto7NodeBIOD0Ev, @function
_ZN4node6crypto7NodeBIOD0Ev:
.LFB8107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6crypto7NodeBIOE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	jne	.L37
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	je	.L40
	cmpq	40(%r12), %r14
	jg	.L53
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	cmpq	48(%r13), %rbx
	je	.L44
.L37:
	movq	%rbx, %r15
	movq	32(%rbx), %rbx
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.L38
	call	_ZdaPv@PLT
.L38:
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	je	.L40
	movq	352(%rdx), %r12
	movq	24(%r15), %rax
	movq	32(%r12), %r14
	subq	%rax, %r14
	movq	%r14, %rdx
	subq	48(%r12), %rdx
	movq	%r14, 32(%r12)
	cmpq	$33554432, %rdx
	jg	.L54
.L41:
	testq	%rax, %rax
	jle	.L42
	movq	40(%r12), %rcx
	subq	%rax, %rcx
	cmpq	$67108864, %rcx
	jle	.L40
	movq	%rcx, 40(%r12)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L44:
	addq	$24, %rsp
	movq	%r13, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L40
	.cfi_endproc
.LFE8107:
	.size	_ZN4node6crypto7NodeBIOD0Ev, .-_ZN4node6crypto7NodeBIOD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIOD2Ev
	.type	_ZN4node6crypto7NodeBIOD2Ev, @function
_ZN4node6crypto7NodeBIOD2Ev:
.LFB8105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6crypto7NodeBIOE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	48(%rdi), %rbx
	movq	%rax, (%rdi)
	testq	%rbx, %rbx
	je	.L55
	movq	%rdi, %r15
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L61:
	je	.L59
	cmpq	40(%r13), %r14
	jg	.L72
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	cmpq	%rbx, 48(%r15)
	je	.L55
.L56:
	movq	%rbx, %r12
	movq	32(%rbx), %rbx
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZdaPv@PLT
.L57:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L59
	movq	352(%rax), %r13
	movq	24(%r12), %rdx
	movq	32(%r13), %r14
	subq	%rdx, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L73
.L60:
	testq	%rdx, %rdx
	jle	.L61
	movq	40(%r13), %rax
	subq	%rdx, %rax
	cmpq	$67108864, %rax
	jle	.L59
	movq	%rax, 40(%r13)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L73:
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L59
	.cfi_endproc
.LFE8105:
	.size	_ZN4node6crypto7NodeBIOD2Ev, .-_ZN4node6crypto7NodeBIOD2Ev
	.globl	_ZN4node6crypto7NodeBIOD1Ev
	.set	_ZN4node6crypto7NodeBIOD1Ev,_ZN4node6crypto7NodeBIOD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv
	.type	_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv, @function
_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv:
.LFB8093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L106
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	cmpl	$12, %ebx
	jg	.L76
	testl	%ebx, %ebx
	jle	.L97
	cmpl	$12, %ebx
	ja	.L97
	leaq	.L79(%rip), %rcx
	movslq	(%rcx,%rbx,4), %rdx
	addq	%rcx, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L79:
	.long	.L97-.L79
	.long	.L85-.L79
	.long	.L84-.L79
	.long	.L83-.L79
	.long	.L97-.L79
	.long	.L97-.L79
	.long	.L97-.L79
	.long	.L97-.L79
	.long	.L82-.L79
	.long	.L81-.L79
	.long	.L80-.L79
	.long	.L105-.L79
	.long	.L105-.L79
	.text
.L81:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	BIO_set_shutdown@PLT
.L105:
	movl	$1, %eax
.L74:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	jg	.L97
	cmpl	$114, %ebx
	je	.L87
	cmpl	$115, %ebx
	je	.L88
.L97:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	cmpl	$130, %ebx
	jne	.L107
	movl	%r14d, 40(%rax)
	movl	$1, %eax
	jmp	.L74
.L80:
	movq	24(%rax), %rax
	jmp	.L74
.L85:
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	je	.L105
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rsi
	cmpq	%rsi, %rcx
	je	.L90
	pxor	%xmm0, %xmm0
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L91:
	movq	24(%rax), %rdi
	addq	%rcx, %rdi
	subq	%rsi, %rdi
	movq	%rdi, 24(%rax)
	movups	%xmm0, 8(%rdx)
	movq	32(%rdx), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rsi
	movq	%rdx, 48(%rax)
	cmpq	%rsi, %rcx
	je	.L92
.L93:
	cmpq	%rsi, %rcx
	jb	.L91
	leaq	_ZZN4node6crypto7NodeBIO5ResetEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L84:
	cmpq	$0, 24(%rax)
	sete	%al
	movzbl	%al, %eax
	jmp	.L74
.L82:
	movq	%r12, %rdi
	call	BIO_get_shutdown@PLT
	cltq
	jmp	.L74
.L83:
	movq	24(%rax), %rax
	testq	%r13, %r13
	je	.L74
	movq	$0, 0(%r13)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L88:
	leaq	_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L90:
	movq	24(%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rdx, 56(%rax)
	testq	%rdi, %rdi
	je	.L105
	leaq	_ZZN4node6crypto7NodeBIO5ResetEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L87:
	leaq	_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8093:
	.size	_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv, .-_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO4FreeEP6bio_st
	.type	_ZN4node6crypto7NodeBIO4FreeEP6bio_st, @function
_ZN4node6crypto7NodeBIO4FreeEP6bio_st:
.LFB8086:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L115
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	call	BIO_get_shutdown@PLT
	testl	%eax, %eax
	jne	.L110
.L129:
	popq	%r12
	movl	$1, %eax
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	%r12, %rdi
	call	BIO_get_init@PLT
	testl	%eax, %eax
	je	.L129
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L129
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L130
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L113
	movq	(%rax), %rax
	leaq	_ZN4node6crypto7NodeBIOD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L114
	call	_ZN4node6crypto7NodeBIOD1Ev
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L113:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	BIO_set_data@PLT
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	xorl	%eax, %eax
	ret
.L114:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	call	*%rax
	jmp	.L113
.L130:
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8086:
	.size	_ZN4node6crypto7NodeBIO4FreeEP6bio_st, .-_ZN4node6crypto7NodeBIO4FreeEP6bio_st
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"node.js SSL buffer"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO3NewEPNS_11EnvironmentE
	.type	_ZN4node6crypto7NodeBIO3NewEPNS_11EnvironmentE, @function
_ZN4node6crypto7NodeBIO3NewEPNS_11EnvironmentE:
.LFB8080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	testq	%rdi, %rdi
	je	.L142
.L132:
	call	BIO_new@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L131
	testq	%rax, %rax
	je	.L131
	movq	%rax, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L143
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movq	%rbx, 8(%rax)
.L131:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	leaq	.LC4(%rip), %rsi
	movl	$1025, %edi
	call	BIO_meth_new@PLT
	leaq	_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, _ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip)
	call	BIO_meth_set_write@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci(%rip), %rsi
	call	BIO_meth_set_read@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc(%rip), %rsi
	call	BIO_meth_set_puts@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci(%rip), %rsi
	call	BIO_meth_set_gets@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv(%rip), %rsi
	call	BIO_meth_set_ctrl@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO3NewEP6bio_st(%rip), %rsi
	call	BIO_meth_set_create@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4FreeEP6bio_st(%rip), %rsi
	call	BIO_meth_set_destroy@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8080:
	.size	_ZN4node6crypto7NodeBIO3NewEPNS_11EnvironmentE, .-_ZN4node6crypto7NodeBIO3NewEPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO8NewFixedEPKcmPNS_11EnvironmentE
	.type	_ZN4node6crypto7NodeBIO8NewFixedEPKcmPNS_11EnvironmentE, @function
_ZN4node6crypto7NodeBIO8NewFixedEPKcmPNS_11EnvironmentE:
.LFB8081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$8, %rsp
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	testq	%rdi, %rdi
	je	.L157
.L145:
	call	BIO_new@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L146
	testq	%r14, %r14
	je	.L147
	movq	%rax, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L158
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movq	%r14, 8(%rax)
.L147:
	cmpq	$2147483647, %rbx
	jbe	.L159
.L149:
	movq	$0, 0(%r13)
	movq	%r12, %rdi
	call	BIO_free_all@PLT
.L144:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	BIO_write@PLT
	cmpl	%ebx, %eax
	jne	.L149
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$130, %esi
	movq	%r12, %rdi
	call	BIO_ctrl@PLT
	cmpq	$1, %rax
	jne	.L149
	movq	%r12, 0(%r13)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L146:
	movq	$0, 0(%r13)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	.LC4(%rip), %rsi
	movl	$1025, %edi
	call	BIO_meth_new@PLT
	leaq	_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, _ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip)
	call	BIO_meth_set_write@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci(%rip), %rsi
	call	BIO_meth_set_read@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc(%rip), %rsi
	call	BIO_meth_set_puts@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci(%rip), %rsi
	call	BIO_meth_set_gets@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv(%rip), %rsi
	call	BIO_meth_set_ctrl@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO3NewEP6bio_st(%rip), %rsi
	call	BIO_meth_set_create@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4FreeEP6bio_st(%rip), %rsi
	call	BIO_meth_set_destroy@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8081:
	.size	_ZN4node6crypto7NodeBIO8NewFixedEPKcmPNS_11EnvironmentE, .-_ZN4node6crypto7NodeBIO8NewFixedEPKcmPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO4PeekEPm
	.type	_ZN4node6crypto7NodeBIO4PeekEPm, @function
_ZN4node6crypto7NodeBIO4PeekEPm:
.LFB8088:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rdx
	movq	16(%rdx), %rax
	subq	8(%rdx), %rax
	movq	%rax, (%rsi)
	movq	8(%rdx), %rax
	addq	40(%rdx), %rax
	ret
	.cfi_endproc
.LFE8088:
	.size	_ZN4node6crypto7NodeBIO4PeekEPm, .-_ZN4node6crypto7NodeBIO4PeekEPm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO12PeekMultipleEPPcPmS4_
	.type	_ZN4node6crypto7NodeBIO12PeekMultipleEPPcPmS4_, @function
_ZN4node6crypto7NodeBIO12PeekMultipleEPPcPmS4_:
.LFB8089:
	.cfi_startproc
	endbr64
	movq	(%rcx), %r10
	movq	48(%rdi), %rax
	testq	%r10, %r10
	je	.L164
	movq	56(%rdi), %r11
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L167:
	movq	32(%rax), %rax
	cmpq	%rdi, %r10
	je	.L162
.L163:
	movq	16(%rax), %r8
	subq	8(%rax), %r8
	movq	%r8, (%rdx,%rdi,8)
	addq	%r8, %r9
	movq	8(%rax), %r8
	addq	40(%rax), %r8
	movq	%r8, (%rsi,%rdi,8)
	addq	$1, %rdi
	cmpq	%rax, %r11
	jne	.L167
	movq	%rdi, %r10
.L162:
	movq	%r10, (%rcx)
	movq	%r9, %rax
	ret
.L164:
	xorl	%r9d, %r9d
	jmp	.L162
	.cfi_endproc
.LFE8089:
	.size	_ZN4node6crypto7NodeBIO12PeekMultipleEPPcPmS4_, .-_ZN4node6crypto7NodeBIO12PeekMultipleEPPcPmS4_
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO9GetMethodEv
	.type	_ZN4node6crypto7NodeBIO9GetMethodEv, @function
_ZN4node6crypto7NodeBIO9GetMethodEv:
.LFB8094:
	.cfi_startproc
	endbr64
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rax
	testq	%rax, %rax
	je	.L174
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC4(%rip), %rsi
	movl	$1025, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	BIO_meth_new@PLT
	leaq	_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, _ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip)
	call	BIO_meth_set_write@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci(%rip), %rsi
	call	BIO_meth_set_read@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc(%rip), %rsi
	call	BIO_meth_set_puts@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci(%rip), %rsi
	call	BIO_meth_set_gets@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4CtrlEP6bio_stilPv(%rip), %rsi
	call	BIO_meth_set_ctrl@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO3NewEP6bio_st(%rip), %rsi
	call	BIO_meth_set_create@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rdi
	leaq	_ZN4node6crypto7NodeBIO4FreeEP6bio_st(%rip), %rsi
	call	BIO_meth_set_destroy@PLT
	movq	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method(%rip), %rax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8094:
	.size	_ZN4node6crypto7NodeBIO9GetMethodEv, .-_ZN4node6crypto7NodeBIO9GetMethodEv
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO15TryMoveReadHeadEv
	.type	_ZN4node6crypto7NodeBIO15TryMoveReadHeadEv, @function
_ZN4node6crypto7NodeBIO15TryMoveReadHeadEv:
.LFB8095:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	pxor	%xmm0, %xmm0
.L177:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L175
	cmpq	%rdx, 16(%rax)
	jne	.L175
	movups	%xmm0, 8(%rax)
	cmpq	%rax, 56(%rdi)
	jne	.L181
.L175:
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movq	32(%rax), %rax
	movq	%rax, 48(%rdi)
	jmp	.L177
	.cfi_endproc
.LFE8095:
	.size	_ZN4node6crypto7NodeBIO15TryMoveReadHeadEv, .-_ZN4node6crypto7NodeBIO15TryMoveReadHeadEv
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO9FreeEmptyEv
	.type	_ZN4node6crypto7NodeBIO9FreeEmptyEv, @function
_ZN4node6crypto7NodeBIO9FreeEmptyEv:
.LFB8097:
	.cfi_startproc
	endbr64
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L204
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rax), %r15
	cmpq	%r15, %rax
	je	.L182
	movq	48(%rdi), %rdx
	movq	%rdi, %rbx
	cmpq	%r15, %rdx
	je	.L182
	movq	32(%r15), %r12
	cmpq	%r12, %rax
	je	.L182
	cmpq	%r12, %rdx
	je	.L182
	movq	8(%r12), %rax
	cmpq	%rax, 16(%r12)
	jne	.L207
.L185:
	movq	40(%r12), %rdi
	movq	32(%r12), %r13
	testq	%rdi, %rdi
	je	.L186
	call	_ZdaPv@PLT
.L186:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L188
	movq	352(%rax), %r14
	movq	24(%r12), %rdx
	movq	32(%r14), %rax
	subq	%rdx, %rax
	movq	%rax, %rcx
	subq	48(%r14), %rcx
	movq	%rax, 32(%r14)
	cmpq	$33554432, %rcx
	jg	.L208
.L189:
	testq	%rdx, %rdx
	jg	.L209
	je	.L188
	cmpq	40(%r14), %rax
	jg	.L210
	.p2align 4,,10
	.p2align 3
.L188:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	cmpq	%r13, 48(%rbx)
	je	.L211
	cmpq	%r13, 56(%rbx)
	je	.L212
	movq	%r13, %r12
	movq	8(%r12), %rax
	cmpq	%rax, 16(%r12)
	je	.L185
.L207:
	leaq	_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%r13, 32(%r15)
.L182:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	40(%r14), %rax
	subq	%rdx, %rax
	cmpq	$67108864, %rax
	jle	.L188
	movq	%rax, 40(%r14)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%r14, %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdx
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%r14, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE8097:
	.size	_ZN4node6crypto7NodeBIO9FreeEmptyEv, .-_ZN4node6crypto7NodeBIO9FreeEmptyEv
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO4ReadEPcm
	.type	_ZN4node6crypto7NodeBIO4ReadEPcm, @function
_ZN4node6crypto7NodeBIO4ReadEPcm:
.LFB8096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	24(%rdi), %rax
	cmpq	%rax, %rdx
	movq	%rax, %r8
	cmovbe	%rdx, %r8
	testq	%r8, %r8
	je	.L214
	movq	48(%rdi), %rax
	movq	%rsi, %r15
	movq	%rdx, %r13
	xorl	%r14d, %r14d
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L215:
	movq	16(%rax), %rbx
	cmpq	%rsi, %rbx
	jb	.L233
	subq	%rsi, %rbx
	cmpq	%rbx, %r13
	cmovbe	%r13, %rbx
	testq	%r15, %r15
	je	.L218
	addq	40(%rax), %rsi
	leaq	(%r15,%r14), %rdi
	movq	%rbx, %rdx
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	movq	48(%r12), %rax
	movq	-56(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	8(%rax), %rsi
.L218:
	addq	%rbx, %rsi
	addq	%rbx, %r14
	subq	%rbx, %r13
	movq	%rsi, 8(%rax)
.L221:
	testq	%rsi, %rsi
	je	.L219
	cmpq	%rsi, 16(%rax)
	jne	.L220
	movups	%xmm0, 8(%rax)
	cmpq	%rax, 56(%r12)
	jne	.L234
.L219:
	xorl	%esi, %esi
.L220:
	cmpq	%r14, %r8
	ja	.L215
	jne	.L222
	movq	24(%r12), %rax
.L214:
	subq	%r8, %rax
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	movq	%rax, 24(%r12)
	call	_ZN4node6crypto7NodeBIO9FreeEmptyEv
	movq	-56(%rbp), %r8
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	movq	%r8, %rax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	32(%rax), %rax
	movq	%rax, 48(%r12)
	movq	8(%rax), %rsi
	jmp	.L221
.L233:
	leaq	_ZZN4node6crypto7NodeBIO4ReadEPcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L222:
	leaq	_ZZN4node6crypto7NodeBIO4ReadEPcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8096:
	.size	_ZN4node6crypto7NodeBIO4ReadEPcm, .-_ZN4node6crypto7NodeBIO4ReadEPcm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci
	.type	_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci, @function
_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci:
.LFB8087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movl	$15, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L242
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movq	%r14, %rsi
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node6crypto7NodeBIO4ReadEPcm
	movl	%eax, %r14d
	testl	%eax, %eax
	jne	.L235
	movl	40(%r13), %r14d
	testl	%r14d, %r14d
	jne	.L243
.L235:
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$9, %esi
	call	BIO_set_flags@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8087:
	.size	_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci, .-_ZN4node6crypto7NodeBIO4ReadEP6bio_stPci
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci
	.type	_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci, @function
_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci:
.LFB8092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L275
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	BIO_get_data@PLT
	movq	24(%rax), %r11
	movq	%rax, %rdi
	testq	%r11, %r11
	jne	.L276
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movslq	%ebx, %r8
	movq	%r11, %r10
	movq	48(%rax), %rsi
	cmpq	%r11, %r8
	cmovbe	%r8, %r10
	testq	%r10, %r10
	je	.L260
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L248:
	movq	8(%rsi), %rcx
	movq	16(%rsi), %rdx
	cmpq	%rdx, %rcx
	ja	.L277
	subq	%rcx, %rdx
	movq	40(%rsi), %r13
	cmpq	%r8, %rdx
	cmova	%r8, %rdx
	addq	%rcx, %r13
	testq	%rdx, %rdx
	je	.L251
	xorl	%r9d, %r9d
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L279:
	addq	$1, %r9
	cmpq	%r9, %rdx
	je	.L278
.L253:
	cmpb	$10, 0(%r13,%r9)
	jne	.L279
	leaq	(%rax,%r9), %r10
.L273:
	movl	%r10d, %r13d
	notl	%r10d
	shrl	$31, %r10d
.L247:
	cmpl	%r13d, %ebx
	jle	.L256
	testb	%r10b, %r10b
	je	.L256
	movslq	%r13d, %r14
	cmpq	%r11, %r14
	jnb	.L257
	addl	$1, %r13d
.L256:
	cmpl	%ebx, %r13d
	jne	.L274
	subl	$1, %r13d
.L274:
	movslq	%r13d, %r14
.L257:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	_ZN4node6crypto7NodeBIO4ReadEPcm
	popq	%rbx
	movb	$0, (%r12,%r14)
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L278:
	.cfi_restore_state
	addq	%rdx, %rax
	subq	%rdx, %r8
.L251:
	addq	%rcx, %rdx
	cmpq	24(%rsi), %rdx
	jne	.L254
	movq	32(%rsi), %rsi
.L254:
	cmpq	%rax, %r10
	ja	.L248
	je	.L273
	leaq	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L275:
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$1, %r10d
	jmp	.L247
	.cfi_endproc
.LFE8092:
	.size	_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci, .-_ZN4node6crypto7NodeBIO4GetsEP6bio_stPci
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO7IndexOfEcm
	.type	_ZN4node6crypto7NodeBIO7IndexOfEcm, @function
_ZN4node6crypto7NodeBIO7IndexOfEcm:
.LFB8098:
	.cfi_startproc
	endbr64
	cmpq	%rdx, 24(%rdi)
	movq	%rdx, %r11
	cmovbe	24(%rdi), %r11
	movq	48(%rdi), %r9
	testq	%r11, %r11
	je	.L297
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r10d, %r10d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	.p2align 4,,10
	.p2align 3
.L282:
	movq	8(%r9), %r8
	movq	16(%r9), %rcx
	cmpq	%rcx, %r8
	ja	.L300
	subq	%r8, %rcx
	movq	40(%r9), %rdi
	cmpq	%rdx, %rcx
	cmova	%rdx, %rcx
	addq	%r8, %rdi
	testq	%rcx, %rcx
	je	.L285
	xorl	%eax, %eax
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L302:
	addq	$1, %rax
	cmpq	%rax, %rcx
	je	.L301
.L287:
	cmpb	%sil, (%rdi,%rax)
	jne	.L302
	leaq	(%r10,%rax), %r11
.L280:
	movq	%r11, %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	addq	%rcx, %r10
	subq	%rcx, %rdx
	addq	%rcx, %r8
.L285:
	cmpq	%r8, 24(%r9)
	jne	.L288
	movq	32(%r9), %r9
.L288:
	cmpq	%r11, %r10
	jb	.L282
	je	.L280
	leaq	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L297:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	%r11, %rax
	ret
	.cfi_endproc
.LFE8098:
	.size	_ZN4node6crypto7NodeBIO7IndexOfEcm, .-_ZN4node6crypto7NodeBIO7IndexOfEcm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO12PeekWritableEPm
	.type	_ZN4node6crypto7NodeBIO12PeekWritableEPm, @function
_ZN4node6crypto7NodeBIO12PeekWritableEPm:
.LFB8100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	56(%rdi), %rbx
	movq	(%rsi), %rdx
	testq	%rbx, %rbx
	je	.L304
	movq	16(%rbx), %rax
	movq	24(%rbx), %rcx
	cmpq	%rcx, %rax
	je	.L326
.L305:
	subq	%rax, %rcx
	testq	%rdx, %rdx
	je	.L307
	cmpq	%rdx, %rcx
	ja	.L315
.L307:
	movq	%rcx, (%r15)
	movq	16(%rbx), %rax
.L315:
	addq	40(%rbx), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	movq	32(%rbx), %rax
	cmpq	%rax, 48(%rdi)
	je	.L318
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.L307
.L318:
	movl	$16384, %r12d
.L306:
	cmpq	%r12, %rdx
	movq	32(%r13), %rax
	cmovnb	%rdx, %r12
	cmpq	%rax, %r12
	jnb	.L308
	movq	$0, 32(%r13)
	movq	%rax, %r12
.L308:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	%rax, %r14
	movq	8(%r13), %rax
	movq	%r12, 24(%r14)
	movq	%rax, (%r14)
	movq	$0, 32(%r14)
	movups	%xmm0, 8(%r14)
	call	_Znam@PLT
	movq	%rax, 40(%r14)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L310
	movq	352(%rax), %rdi
	movq	32(%rdi), %rax
	addq	%r12, %rax
	movq	%rax, %rdx
	subq	48(%rdi), %rdx
	movq	%rax, 32(%rdi)
	cmpq	$33554432, %rdx
	jg	.L327
.L311:
	testq	%r12, %r12
	js	.L328
	je	.L310
	cmpq	40(%rdi), %rax
	jg	.L329
	.p2align 4,,10
	.p2align 3
.L310:
	testq	%rbx, %rbx
	je	.L330
	movq	32(%rbx), %rax
	movq	(%r15), %rdx
	movq	%rax, 32(%r14)
	movq	%r14, 32(%rbx)
	movq	56(%r13), %rbx
	movq	24(%rbx), %rcx
	movq	16(%rbx), %rax
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L304:
	movq	16(%rdi), %r12
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r14, %xmm0
	movq	%r14, 32(%r14)
	movq	24(%r14), %rcx
	movq	%r14, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	16(%r14), %rax
	movq	(%r15), %rdx
	movups	%xmm0, 48(%r13)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L328:
	movq	40(%rdi), %rdx
	addq	%r12, %rdx
	cmpq	$67108864, %rdx
	jle	.L310
	movq	%rdx, 40(%rdi)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%rax, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdi
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L329:
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L310
	.cfi_endproc
.LFE8100:
	.size	_ZN4node6crypto7NodeBIO12PeekWritableEPm, .-_ZN4node6crypto7NodeBIO12PeekWritableEPm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm
	.type	_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm, @function
_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm:
.LFB8102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	56(%rdi), %r13
	testq	%r13, %r13
	je	.L332
	movq	24(%r13), %rax
	cmpq	%rax, 16(%r13)
	je	.L348
.L331:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	32(%r13), %rax
	cmpq	48(%rdi), %rax
	je	.L343
	cmpq	$0, 16(%rax)
	je	.L331
.L343:
	movl	$16384, %r14d
.L334:
	cmpq	%rsi, %r14
	movq	32(%rbx), %rax
	cmovb	%rsi, %r14
	cmpq	%rax, %r14
	jnb	.L335
	movq	$0, 32(%rbx)
	movq	%rax, %r14
.L335:
	movl	$48, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movq	%rax, %r12
	movq	8(%rbx), %rax
	movq	%r14, 24(%r12)
	movq	%rax, (%r12)
	movq	$0, 32(%r12)
	movups	%xmm0, 8(%r12)
	call	_Znam@PLT
	movq	%rax, 40(%r12)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L337
	movq	352(%rax), %r15
	movq	32(%r15), %rax
	addq	%r14, %rax
	movq	%rax, %rdx
	subq	48(%r15), %rdx
	movq	%rax, 32(%r15)
	cmpq	$33554432, %rdx
	jg	.L349
.L338:
	testq	%r14, %r14
	js	.L350
	je	.L337
	cmpq	40(%r15), %rax
	jg	.L351
	.p2align 4,,10
	.p2align 3
.L337:
	testq	%r13, %r13
	je	.L352
	movq	32(%r13), %rax
	movq	%rax, 32(%r12)
	movq	%r12, 32(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L332:
	.cfi_restore_state
	movq	16(%rdi), %r14
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L352:
	movq	%r12, %xmm0
	movq	%r12, 32(%r12)
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	addq	40(%r15), %r14
	cmpq	$67108864, %r14
	jle	.L337
	movq	%r14, 40(%r15)
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%r15, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L351:
	movq	%r15, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L337
	.cfi_endproc
.LFE8102:
	.size	_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm, .-_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO5WriteEPKcm
	.type	_ZN4node6crypto7NodeBIO5WriteEPKcm, @function
_ZN4node6crypto7NodeBIO5WriteEPKcm:
.LFB8099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	call	_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm
	testq	%r13, %r13
	je	.L353
	movq	56(%r14), %rax
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L355:
	movq	16(%rax), %rdi
	movq	24(%rax), %rbx
	cmpq	%rbx, %rdi
	ja	.L370
	subq	%rdi, %rbx
	leaq	(%r12,%r15), %rsi
	cmpq	%r13, %rbx
	cmova	%r13, %rbx
	addq	40(%rax), %rdi
	movq	%rbx, %rdx
	subq	%rbx, %r13
	addq	%rbx, %r15
	call	memcpy@PLT
	movq	56(%r14), %rax
	addq	%rbx, 24(%r14)
	addq	16(%rax), %rbx
	movq	%rbx, 16(%rax)
	movq	24(%rax), %rax
	cmpq	%rax, %rbx
	ja	.L371
	testq	%r13, %r13
	jne	.L372
.L353:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L372:
	.cfi_restore_state
	cmpq	%rax, %rbx
	jne	.L373
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm
	movq	56(%r14), %rax
	pxor	%xmm0, %xmm0
	movq	32(%rax), %rcx
	movq	48(%r14), %rax
	movq	%rcx, 56(%r14)
.L360:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L362
	cmpq	%rdx, 16(%rax)
	jne	.L362
	movups	%xmm0, 8(%rax)
	cmpq	%rax, %rcx
	je	.L355
	movq	32(%rax), %rax
	movq	%rax, 48(%r14)
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L370:
	leaq	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%rcx, %rax
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L371:
	leaq	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L373:
	leaq	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8099:
	.size	_ZN4node6crypto7NodeBIO5WriteEPKcm, .-_ZN4node6crypto7NodeBIO5WriteEPKcm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci
	.type	_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci, @function
_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci:
.LFB8090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	movl	$15, %esi
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L377
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO5WriteEPKcm
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8090:
	.size	_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci, .-_ZN4node6crypto7NodeBIO5WriteEP6bio_stPKci
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc
	.type	_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc, @function
_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc:
.LFB8091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	call	strlen@PLT
	movq	%r12, %rdi
	movl	$15, %esi
	movq	%rax, %rbx
	call	BIO_clear_flags@PLT
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L381
	movq	%r12, %rdi
	call	BIO_get_data@PLT
	movslq	%ebx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO5WriteEPKcm
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	.cfi_restore_state
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8091:
	.size	_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc, .-_ZN4node6crypto7NodeBIO4PutsEP6bio_stPKc
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO6CommitEm
	.type	_ZN4node6crypto7NodeBIO6CommitEm, @function
_ZN4node6crypto7NodeBIO6CommitEm:
.LFB8101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	56(%rdi), %rax
	movq	16(%rax), %rdx
	addq	%rsi, %rdx
	movq	%rdx, 16(%rax)
	addq	%rsi, 24(%rdi)
	cmpq	24(%rax), %rdx
	ja	.L390
	movq	%rdi, %rbx
	xorl	%esi, %esi
	call	_ZN4node6crypto7NodeBIO19TryAllocateForWriteEm
	movq	56(%rbx), %rax
	movq	24(%rax), %rdi
	cmpq	%rdi, 16(%rax)
	je	.L391
.L382:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	.cfi_restore_state
	movq	32(%rax), %rcx
	movq	48(%rbx), %rax
	pxor	%xmm0, %xmm0
	movq	%rcx, 56(%rbx)
.L385:
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L382
	cmpq	%rdx, 16(%rax)
	jne	.L382
	movups	%xmm0, 8(%rax)
	cmpq	%rcx, %rax
	je	.L382
	movq	32(%rax), %rax
	movq	%rax, 48(%rbx)
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L390:
	leaq	_ZZN4node6crypto7NodeBIO6CommitEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8101:
	.size	_ZN4node6crypto7NodeBIO6CommitEm, .-_ZN4node6crypto7NodeBIO6CommitEm
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO5ResetEv
	.type	_ZN4node6crypto7NodeBIO5ResetEv, @function
_ZN4node6crypto7NodeBIO5ResetEv:
.LFB8103:
	.cfi_startproc
	endbr64
	movq	48(%rdi), %rax
	testq	%rax, %rax
	je	.L405
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rdx, %rcx
	je	.L394
	pxor	%xmm0, %xmm0
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L395:
	movq	24(%rdi), %rsi
	addq	%rdx, %rsi
	subq	%rcx, %rsi
	movq	%rsi, 24(%rdi)
	movups	%xmm0, 8(%rax)
	movq	32(%rax), %rax
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	movq	%rax, 48(%rdi)
	cmpq	%rcx, %rdx
	je	.L396
.L397:
	cmpq	%rdx, %rcx
	ja	.L395
	leaq	_ZZN4node6crypto7NodeBIO5ResetEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L394:
	movq	24(%rdi), %rsi
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%rax, 56(%rdi)
	testq	%rsi, %rsi
	jne	.L408
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore 6
	ret
.L408:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	leaq	_ZZN4node6crypto7NodeBIO5ResetEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8103:
	.size	_ZN4node6crypto7NodeBIO5ResetEv, .-_ZN4node6crypto7NodeBIO5ResetEv
	.align 2
	.p2align 4
	.globl	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st
	.type	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st, @function
_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st:
.LFB8108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	BIO_get_data@PLT
	testq	%rax, %rax
	je	.L412
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	BIO_get_data@PLT
	.p2align 4,,10
	.p2align 3
.L412:
	.cfi_restore_state
	leaq	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8108:
	.size	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st, .-_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node6crypto7NodeBIOE
	.section	.data.rel.ro.local._ZTVN4node6crypto7NodeBIOE,"awG",@progbits,_ZTVN4node6crypto7NodeBIOE,comdat
	.align 8
	.type	_ZTVN4node6crypto7NodeBIOE, @object
	.size	_ZTVN4node6crypto7NodeBIOE, 72
_ZTVN4node6crypto7NodeBIOE:
	.quad	0
	.quad	0
	.quad	_ZN4node6crypto7NodeBIOD1Ev
	.quad	_ZN4node6crypto7NodeBIOD0Ev
	.quad	_ZNK4node6crypto7NodeBIO10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6crypto7NodeBIO14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6crypto7NodeBIO8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.rodata.str1.1
.LC5:
	.string	"../src/node_crypto_bio.cc:497"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"(BIO_get_data(bio)) != nullptr"
	.align 8
.LC7:
	.string	"static node::crypto::NodeBIO* node::crypto::NodeBIO::FromBIO(BIO*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args, @object
	.size	_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args, 24
_ZZN4node6crypto7NodeBIO7FromBIOEP6bio_stE4args:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"../src/node_crypto_bio.cc:476"
.LC9:
	.string	"(length_) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"void node::crypto::NodeBIO::Reset()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO5ResetEvE4args_0, @object
	.size	_ZZN4node6crypto7NodeBIO5ResetEvE4args_0, 24
_ZZN4node6crypto7NodeBIO5ResetEvE4args_0:
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"../src/node_crypto_bio.cc:467"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"read_head_->write_pos_ > read_head_->read_pos_"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO5ResetEvE4args, @object
	.size	_ZZN4node6crypto7NodeBIO5ResetEvE4args, 24
_ZZN4node6crypto7NodeBIO5ResetEvE4args:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC10
	.section	.rodata.str1.1
.LC13:
	.string	"../src/node_crypto_bio.cc:415"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"(write_head_->write_pos_) <= (write_head_->len_)"
	.align 8
.LC15:
	.string	"void node::crypto::NodeBIO::Commit(size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO6CommitEmE4args, @object
	.size	_ZZN4node6crypto7NodeBIO6CommitEmE4args, 24
_ZZN4node6crypto7NodeBIO6CommitEmE4args:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/node_crypto_bio.cc:388"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"(write_head_->write_pos_) == (write_head_->len_)"
	.align 8
.LC18:
	.string	"void node::crypto::NodeBIO::Write(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_1, @object
	.size	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_1, 24
_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_1:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"../src/node_crypto_bio.cc:384"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_0, @object
	.size	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_0, 24
_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args_0:
	.quad	.LC19
	.quad	.LC14
	.quad	.LC18
	.section	.rodata.str1.1
.LC20:
	.string	"../src/node_crypto_bio.cc:368"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args, @object
	.size	_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args, 24
_ZZN4node6crypto7NodeBIO5WriteEPKcmE4args:
	.quad	.LC20
	.quad	.LC14
	.quad	.LC18
	.section	.rodata.str1.1
.LC21:
	.string	"../src/node_crypto_bio.cc:353"
.LC22:
	.string	"(max) == (bytes_read)"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"size_t node::crypto::NodeBIO::IndexOf(char, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args_0, @object
	.size	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args_0, 24
_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args_0:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"../src/node_crypto_bio.cc:326"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"(current->read_pos_) <= (current->write_pos_)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args, @object
	.size	_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args, 24
_ZZN4node6crypto7NodeBIO7IndexOfEcmE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC23
	.section	.rodata.str1.1
.LC26:
	.string	"../src/node_crypto_bio.cc:309"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"(cur->write_pos_) == (cur->read_pos_)"
	.align 8
.LC28:
	.string	"void node::crypto::NodeBIO::FreeEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args_0, @object
	.size	_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args_0, 24
_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args_0:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"../src/node_crypto_bio.cc:308"
.LC30:
	.string	"(cur) != (write_head_)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args, @object
	.size	_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args, 24
_ZZN4node6crypto7NodeBIO9FreeEmptyEvE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC28
	.section	.rodata.str1.1
.LC31:
	.string	"../src/node_crypto_bio.cc:286"
.LC32:
	.string	"(expected) == (bytes_read)"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"size_t node::crypto::NodeBIO::Read(char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO4ReadEPcmE4args_0, @object
	.size	_ZZN4node6crypto7NodeBIO4ReadEPcmE4args_0, 24
_ZZN4node6crypto7NodeBIO4ReadEPcmE4args_0:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.section	.rodata.str1.1
.LC34:
	.string	"../src/node_crypto_bio.cc:269"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"(read_head_->read_pos_) <= (read_head_->write_pos_)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO4ReadEPcmE4args, @object
	.size	_ZZN4node6crypto7NodeBIO4ReadEPcmE4args, 24
_ZZN4node6crypto7NodeBIO4ReadEPcmE4args:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC33
	.local	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method
	.comm	_ZZN4node6crypto7NodeBIO9GetMethodEvE6method,8,8
	.section	.rodata.str1.1
.LC36:
	.string	"../src/node_crypto_bio.cc:194"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"0 && \"Can't use GET_BUF_MEM_PTR with NodeBIO\""
	.align 8
.LC38:
	.string	"static long int node::crypto::NodeBIO::Ctrl(BIO*, int, long int, void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args_0, @object
	.size	_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args_0, 24
_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args_0:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_crypto_bio.cc:191"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"0 && \"Can't use SET_BUF_MEM_PTR with NodeBIO\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args, @object
	.size	_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args, 24
_ZZN4node6crypto7NodeBIO4CtrlEP6bio_stilPvE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC38
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	1024
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
