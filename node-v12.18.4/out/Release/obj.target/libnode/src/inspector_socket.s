	.file	"inspector_socket.cc"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4349:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4349:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler15CancelHandshakeEv, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler15CancelHandshakeEv:
.LFB4350:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4350:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler15CancelHandshakeEv, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler15CancelHandshakeEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler17WaitForCloseReplyEv, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler17WaitForCloseReplyEv:
.LFB4356:
	.cfi_startproc
	endbr64
	movq	$17, 40(%rdi)
	movq	$0, 48(%rdi)
	ret
	.cfi_endproc
.LFE4356:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler17WaitForCloseReplyEv, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler17WaitForCloseReplyEv
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_115allocate_bufferEP11uv_handle_smP8uv_buf_t, @function
_ZN4node9inspector12_GLOBAL__N_115allocate_bufferEP11uv_handle_smP8uv_buf_t:
.LFB4319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znam@PLT
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	popq	%rbx
	movq	%rax, (%r12)
	movq	%rdx, 8(%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4319:
	.size	_ZN4node9inspector12_GLOBAL__N_115allocate_bufferEP11uv_handle_smP8uv_buf_t, .-_ZN4node9inspector12_GLOBAL__N_115allocate_bufferEP11uv_handle_smP8uv_buf_t
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_112WriteRequest7CleanupEP10uv_write_si, @function
_ZN4node9inspector12_GLOBAL__N_112WriteRequest7CleanupEP10uv_write_si:
.LFB4315:
	.cfi_startproc
	endbr64
	cmpq	$32, %rdi
	je	.L7
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-32(%rdi), %r12
	subq	$8, %rsp
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L9
	call	_ZdlPv@PLT
.L9:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ret
	.cfi_endproc
.LFE4315:
	.size	_ZN4node9inspector12_GLOBAL__N_112WriteRequest7CleanupEP10uv_write_si, .-_ZN4node9inspector12_GLOBAL__N_112WriteRequest7CleanupEP10uv_write_si
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler19OnCloseFrameWrittenEP10uv_write_si, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler19OnCloseFrameWrittenEP10uv_write_si:
.LFB4355:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	-32(%rdi), %rbx
	cmpq	$32, %rdi
	je	.L17
	leaq	-32(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L18
	call	_ZdlPv@PLT
.L18:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L17:
	movq	24(%rbx), %rax
	addq	32(%rbx), %rbx
	movq	%rbx, %rdi
	testb	$1, %al
	je	.L19
	movq	(%rbx), %rdx
	movq	-1(%rdx,%rax), %rax
.L19:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE4355:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler19OnCloseFrameWrittenEP10uv_write_si, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler19OnCloseFrameWrittenEP10uv_write_si
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler5OnEofEv, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler5OnEofEv:
.LFB4389:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	movq	$0, 16(%rdi)
	testq	%r8, %r8
	je	.L30
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	movq	%r8, %rdi
	jmp	uv_close@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	ret
	.cfi_endproc
.LFE4389:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler5OnEofEv, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler5OnEofEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandlerD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandlerD2Ev:
.LFB5918:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	jmp	uv_close@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	ret
	.cfi_endproc
.LFE5918:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandlerD2Ev, .-_ZN4node9inspector12_GLOBAL__N_19WsHandlerD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_19WsHandlerD1Ev,_ZN4node9inspector12_GLOBAL__N_19WsHandlerD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandlerD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandlerD0Ev:
.LFB5920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L35
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L35:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE5920:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandlerD0Ev, .-_ZN4node9inspector12_GLOBAL__N_19WsHandlerD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::append"
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderFieldEP18llhttp__internal_sPKcm, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderFieldEP18llhttp__internal_sPKcm:
.LFB4398:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	cmpb	$0, -8(%rdi)
	leaq	-32(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	je	.L41
	movq	232(%rax), %rcx
	movb	$0, 24(%rax)
	movq	$0, 240(%rax)
	movb	$0, (%rcx)
.L41:
	movabsq	$4611686018427387903, %rcx
	subq	240(%rax), %rcx
	leaq	232(%rax), %rdi
	cmpq	%rcx, %rdx
	ja	.L44
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4398:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderFieldEP18llhttp__internal_sPKcm, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderFieldEP18llhttp__internal_sPKcm
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler25ThenCloseAndReportFailureEP10uv_write_si, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler25ThenCloseAndReportFailureEP10uv_write_si:
.LFB4396:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	-32(%rdi), %rbx
	cmpq	$32, %rdi
	je	.L46
	leaq	-32(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L46:
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movq	$0, (%rax)
	testq	%rdi, %rdi
	je	.L45
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4396:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler25ThenCloseAndReportFailureEP10uv_write_si, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler25ThenCloseAndReportFailureEP10uv_write_si
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s
	.type	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s, @function
_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s:
.LFB4422:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L56
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	264(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	movq	248(%r12), %rdi
	testq	%rdi, %rdi
	je	.L59
	movq	(%rdi), %rax
	call	*32(%rax)
.L59:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$288, %esi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	ret
	.cfi_endproc
.LFE4422:
	.size	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s, .-_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnPathEP18llhttp__internal_sPKcm, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnPathEP18llhttp__internal_sPKcm:
.LFB4399:
	.cfi_startproc
	endbr64
	movabsq	$4611686018427387903, %rax
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	288(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	%rax, %rdx
	ja	.L72
	leaq	280(%rdi), %r8
	movq	%r8, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L72:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4399:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnPathEP18llhttp__internal_sPKcm, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnPathEP18llhttp__internal_sPKcm
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler5OnEofEv, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler5OnEofEv:
.LFB4351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	movq	$0, 16(%r12)
	testq	%rdi, %rdi
	je	.L74
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L74:
	cmpb	$0, 56(%r12)
	jne	.L84
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L76
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L76:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4351:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler5OnEofEv, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler5OnEofEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15ProtocolHandlerC2EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE
	.type	_ZN4node9inspector15ProtocolHandlerC2EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE, @function
_ZN4node9inspector15ProtocolHandlerC2EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE:
.LFB4406:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	movq	%rax, 16(%rdi)
	testq	%rax, %rax
	je	.L90
	movq	%rdi, 256(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector15ProtocolHandlerC4EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE4406:
	.size	_ZN4node9inspector15ProtocolHandlerC2EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE, .-_ZN4node9inspector15ProtocolHandlerC2EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE
	.globl	_ZN4node9inspector15ProtocolHandlerC1EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE
	.set	_ZN4node9inspector15ProtocolHandlerC1EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE,_ZN4node9inspector15ProtocolHandlerC2EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15ProtocolHandler8delegateEv
	.type	_ZN4node9inspector15ProtocolHandler8delegateEv, @function
_ZN4node9inspector15ProtocolHandler8delegateEv:
.LFB4409:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	movq	248(%rax), %rax
	ret
	.cfi_endproc
.LFE4409:
	.size	_ZN4node9inspector15ProtocolHandler8delegateEv, .-_ZN4node9inspector15ProtocolHandler8delegateEv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector15ProtocolHandler7GetHostB5cxx11Ev
	.type	_ZNK4node9inspector15ProtocolHandler7GetHostB5cxx11Ev, @function
_ZNK4node9inspector15ProtocolHandler7GetHostB5cxx11Ev:
.LFB4410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	leaq	-252(%rbp), %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	16(%r12), %r13
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	16(%rsi), %rdi
	movq	%r14, %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$128, -252(%rbp)
	call	uv_tcp_getsockname@PLT
	testl	%eax, %eax
	jne	.L124
	leaq	-112(%rbp), %r15
	cmpw	$10, -240(%rbp)
	movl	$46, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	je	.L125
	call	uv_ip4_name@PLT
	movq	%r13, (%r12)
	testl	%eax, %eax
	je	.L97
.L127:
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
.L92:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L126
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	call	uv_ip6_name@PLT
	movq	%r13, (%r12)
	testl	%eax, %eax
	jne	.L127
.L97:
	movq	%r15, %rbx
.L98:
	movl	(%rbx), %edx
	addq	$4, %rbx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L98
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rbx), %rdx
	cmove	%rdx, %rbx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rbx
	subq	%r15, %rbx
	movq	%rbx, -248(%rbp)
	cmpq	$15, %rbx
	ja	.L128
	cmpq	$1, %rbx
	jne	.L102
	movzbl	-112(%rbp), %eax
	movb	%al, 16(%r12)
.L103:
	movq	%rbx, 8(%r12)
	movb	$0, 0(%r13,%rbx)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r13, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L102:
	testq	%rbx, %rbx
	je	.L103
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r13
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L101:
	movl	%ebx, %eax
	cmpl	$8, %ebx
	jnb	.L104
	andl	$4, %ebx
	jne	.L129
	testl	%eax, %eax
	je	.L105
	movzbl	(%r15), %edx
	movb	%dl, 0(%r13)
	testb	$2, %al
	je	.L105
	movzwl	-2(%r15,%rax), %edx
	movw	%dx, -2(%r13,%rax)
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-248(%rbp), %rbx
	movq	(%r12), %r13
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-112(%rbp), %rax
	leaq	8(%r13), %rsi
	andq	$-8, %rsi
	movq	%rax, 0(%r13)
	movl	%ebx, %eax
	movq	-8(%r15,%rax), %rdx
	movq	%rdx, -8(%r13,%rax)
	subq	%rsi, %r13
	leal	(%rbx,%r13), %eax
	subq	%r13, %r15
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L105
	andl	$-8, %eax
	xorl	%edx, %edx
.L108:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%r15,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L108
	jmp	.L105
.L129:
	movl	(%r15), %edx
	movl	%edx, 0(%r13)
	movl	-4(%r15,%rax), %edx
	movl	%edx, -4(%r13,%rax)
	jmp	.L105
.L126:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4410:
	.size	_ZNK4node9inspector15ProtocolHandler7GetHostB5cxx11Ev, .-_ZNK4node9inspector15ProtocolHandler7GetHostB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolderC2ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE
	.type	_ZN4node9inspector9TcpHolderC2ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE, @function
_ZN4node9inspector9TcpHolderC2ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE:
.LFB4413:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	movl	$31, %ecx
	movq	%rdi, %rdx
	rep stosq
	movq	(%rsi), %rax
	movq	$0, (%rsi)
	movq	$0, 256(%rdx)
	movq	%rax, 248(%rdx)
	movq	$0, 264(%rdx)
	movq	$0, 272(%rdx)
	movq	$0, 280(%rdx)
	ret
	.cfi_endproc
.LFE4413:
	.size	_ZN4node9inspector9TcpHolderC2ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE, .-_ZN4node9inspector9TcpHolderC2ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE
	.globl	_ZN4node9inspector9TcpHolderC1ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE
	.set	_ZN4node9inspector9TcpHolderC1ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE,_ZN4node9inspector9TcpHolderC2ESt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS4_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder6AcceptEP11uv_stream_sSt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS6_EE
	.type	_ZN4node9inspector9TcpHolder6AcceptEP11uv_stream_sSt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS6_EE, @function
_ZN4node9inspector9TcpHolder6AcceptEP11uv_stream_sSt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS6_EE:
.LFB4415:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	movl	$288, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdx), %rbx
	movq	$0, (%rdx)
	call	_Znwm@PLT
	movl	$31, %ecx
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r12, %rsi
	rep stosq
	movq	8(%r14), %rdi
	movq	%rbx, 248(%r12)
	movq	$0, 256(%r12)
	movq	$0, 264(%r12)
	movq	$0, 272(%r12)
	movq	$0, 280(%r12)
	call	uv_tcp_init@PLT
	testl	%eax, %eax
	je	.L143
.L132:
	movq	264(%r12), %rdi
	testq	%rdi, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	movq	248(%r12), %rdi
	testq	%rdi, %rdi
	je	.L135
	movq	(%rdi), %rax
	call	*32(%rax)
.L135:
	movq	%r12, %rdi
	movl	$288, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	movq	%r13, %rax
	popq	%r12
	movq	$0, 0(%r13)
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	uv_accept@PLT
	testl	%eax, %eax
	jne	.L132
	leaq	_ZN4node9inspector9TcpHolder16OnDataReceivedCbEP11uv_stream_slPK8uv_buf_t(%rip), %rdx
	leaq	_ZN4node9inspector12_GLOBAL__N_115allocate_bufferEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%r12, %rdi
	call	uv_read_start@PLT
	testl	%eax, %eax
	jne	.L132
	popq	%rbx
	movq	%r12, 0(%r13)
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4415:
	.size	_ZN4node9inspector9TcpHolder6AcceptEP11uv_stream_sSt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS6_EE, .-_ZN4node9inspector9TcpHolder6AcceptEP11uv_stream_sSt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS6_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder10SetHandlerEPNS0_15ProtocolHandlerE
	.type	_ZN4node9inspector9TcpHolder10SetHandlerEPNS0_15ProtocolHandlerE, @function
_ZN4node9inspector9TcpHolder10SetHandlerEPNS0_15ProtocolHandlerE:
.LFB4419:
	.cfi_startproc
	endbr64
	movq	%rsi, 256(%rdi)
	ret
	.cfi_endproc
.LFE4419:
	.size	_ZN4node9inspector9TcpHolder10SetHandlerEPNS0_15ProtocolHandlerE, .-_ZN4node9inspector9TcpHolder10SetHandlerEPNS0_15ProtocolHandlerE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	.type	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE, @function
_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE:
.LFB4420:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$240, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	call	_Znwm@PLT
	movq	%rax, %r12
	movq	256(%r13), %rax
	movq	$0, 8(%r12)
	movq	%rax, (%r12)
	movq	8(%rbx), %rax
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	subq	(%rbx), %rax
	je	.L146
	js	.L157
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	8(%rbx), %rbx
	movq	%rax, %xmm0
	movq	%rax, %r8
	leaq	(%rax,%r15), %rax
	subq	%rsi, %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 24(%r12)
	movq	%rbx, -56(%rbp)
	movq	%rbx, %rcx
	movups	%xmm0, 8(%r12)
	jne	.L158
.L148:
	addq	%r8, %rcx
	xorl	%eax, %eax
	leaq	32(%r12), %rdi
	movl	%ebx, %esi
	movq	%rcx, 16(%r12)
	movl	$24, %ecx
	rep stosq
	movq	%r8, %rdi
	call	uv_buf_init@PLT
	leaq	32(%r12), %rdi
	movq	%r14, %r8
	movq	%r13, %rsi
	movq	%rdx, 232(%r12)
	movl	$1, %ecx
	leaq	224(%r12), %rdx
	movq	%rax, 224(%r12)
	call	uv_write@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L159
.L149:
	addq	$24, %rsp
	movl	%ebx, %eax
	shrl	$31, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movq	%r8, %rdi
	movq	%rbx, %rdx
	call	memmove@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, %r8
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L159:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L150
	call	_ZdlPv@PLT
.L150:
	movl	$240, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L146:
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	movups	%xmm0, 8(%r12)
	jmp	.L148
.L157:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE4420:
	.size	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE, .-_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15ProtocolHandler8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	.type	_ZN4node9inspector15ProtocolHandler8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE, @function
_ZN4node9inspector15ProtocolHandler8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE:
.LFB4408:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	jmp	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	.cfi_endproc
.LFE4408:
	.size	_ZN4node9inspector15ProtocolHandler8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE, .-_ZN4node9inspector15ProtocolHandler8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler5WriteESt6vectorIcSaIcEE, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler5WriteESt6vectorIcSaIcEE:
.LFB4394:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	leaq	_ZN4node9inspector12_GLOBAL__N_112WriteRequest7CleanupEP10uv_write_si(%rip), %rdx
	jmp	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	.cfi_endproc
.LFE4394:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler5WriteESt6vectorIcSaIcEE, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler5WriteESt6vectorIcSaIcEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler15CancelHandshakeEv, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler15CancelHandshakeEv:
.LFB4388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$101, %edi
	subq	$152, %rsp
	movdqa	.LC1(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$10, %eax
	movl	$224683380, -32(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqa	.LC2(%rip), %xmm0
	movw	%ax, -28(%rbp)
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC3(%rip), %xmm0
	movq	$0, -144(%rbp)
	movaps	%xmm0, -96(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm0, -64(%rbp)
	movdqa	.LC6(%rip), %xmm0
	movaps	%xmm0, -48(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	call	_Znwm@PLT
	movl	-32(%rbp), %ecx
	movdqa	-128(%rbp), %xmm1
	leaq	-160(%rbp), %rsi
	movdqa	-112(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm3
	leaq	101(%rax), %rdx
	movq	%rax, -160(%rbp)
	movdqa	-80(%rbp), %xmm4
	movdqa	-64(%rbp), %xmm5
	movl	%ecx, 96(%rax)
	movzbl	-28(%rbp), %ecx
	movdqa	-48(%rbp), %xmm6
	movq	%rdx, -144(%rbp)
	movq	16(%rbx), %rdi
	movq	%rdx, -152(%rbp)
	leaq	_ZN4node9inspector12_GLOBAL__N_111HttpHandler25ThenCloseAndReportFailureEP10uv_write_si(%rip), %rdx
	movb	%cl, 100(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	call	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	movq	-160(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L162
	call	_ZdlPv@PLT
.L162:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L169
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L169:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4388:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler15CancelHandshakeEv, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler15CancelHandshakeEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler8ShutdownEv, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler8ShutdownEv:
.LFB4354:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 16(%rdi)
	je	.L171
	movb	$1, 56(%rdi)
	pxor	%xmm0, %xmm0
	movl	$2, %edi
	movaps	%xmm0, -48(%rbp)
	movq	$0, -32(%rbp)
	call	_Znwm@PLT
	movzwl	_ZN4node9inspector12_GLOBAL__N_1L11CLOSE_FRAMEE(%rip), %ecx
	movq	16(%r12), %rdi
	leaq	-48(%rbp), %rsi
	leaq	2(%rax), %rdx
	movq	%rax, -48(%rbp)
	movw	%cx, (%rax)
	movq	%rdx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	leaq	_ZN4node9inspector12_GLOBAL__N_19WsHandler19OnCloseFrameWrittenEP10uv_write_si(%rip), %rdx
	call	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movl	$64, %esi
	call	_ZdlPvm@PLT
	jmp	.L170
.L179:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4354:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler8ShutdownEv, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler8ShutdownEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler18CloseFrameReceivedEv, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler18CloseFrameReceivedEv:
.LFB4358:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$17, 24(%rdi)
	movq	$0, 32(%rdi)
	movl	$2, %edi
	movaps	%xmm0, -48(%rbp)
	movq	$0, -32(%rbp)
	call	_Znwm@PLT
	movzwl	_ZN4node9inspector12_GLOBAL__N_1L11CLOSE_FRAMEE(%rip), %ecx
	movq	16(%rbx), %rdi
	leaq	-48(%rbp), %rsi
	leaq	2(%rax), %rdx
	movq	%rax, -48(%rbp)
	movw	%cx, (%rax)
	movq	%rdx, -32(%rbp)
	movq	%rdx, -40(%rbp)
	leaq	_ZN4node9inspector12_GLOBAL__N_19WsHandler19OnCloseFrameWrittenEP10uv_write_si(%rip), %rdx
	call	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L180
	call	_ZdlPv@PLT
.L180:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L187:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4358:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler18CloseFrameReceivedEv, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler18CloseFrameReceivedEv
	.section	.rodata.str1.1
.LC7:
	.string	"localhost"
.LC8:
	.string	"localhost6"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnDataEPSt6vectorIcSaIcEE, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnDataEPSt6vectorIcSaIcEE:
.LFB4390:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	32(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movq	%r12, %rdi
	subq	$216, %rsp
	movq	(%rsi), %rsi
	movq	8(%rbx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subq	%rsi, %rdx
	call	llhttp_execute@PLT
	cmpl	$22, %eax
	je	.L322
	movq	(%rbx), %rdx
	cmpq	%rdx, 8(%rbx)
	je	.L192
	movq	%rdx, 8(%rbx)
.L192:
	testl	%eax, %eax
	jne	.L323
.L191:
	movq	208(%r14), %rax
	movq	216(%r14), %r13
	pxor	%xmm0, %xmm0
	leaq	-224(%rbp), %rsi
	movq	%rsi, -240(%rbp)
	leaq	-176(%rbp), %rbx
	movq	%rax, -248(%rbp)
	movq	%rax, %r15
	movq	$0, 224(%r14)
	movups	%xmm0, 208(%r14)
	cmpq	%r13, %rax
	je	.L249
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	72(%r15), %r12
	movq	$-1, %rdx
	movl	$58, %esi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5rfindEcm@PLT
	cmpq	$-1, %rax
	je	.L324
	movq	$-1, %rdx
	movl	$93, %esi
	movq	%r12, %rdi
	movq	%rax, -232(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE5rfindEcm@PLT
	movq	-232(%rbp), %rcx
	cmpq	$-1, %rax
	je	.L270
	cmpq	%rax, %rcx
	ja	.L270
	movq	%rbx, -192(%rbp)
	movq	72(%r15), %r9
	movq	80(%r15), %r10
	movq	%r9, %rax
	addq	%r10, %rax
	je	.L272
	testq	%r9, %r9
	je	.L207
.L272:
	movq	%r10, -224(%rbp)
	cmpq	$15, %r10
	ja	.L325
.L214:
	cmpq	$1, %r10
	jne	.L216
	movzbl	(%r9), %eax
	movb	%al, -176(%rbp)
	movq	%rbx, %rax
.L217:
	movq	%r10, -184(%rbp)
	movb	$0, (%rax,%r10)
.L204:
	movq	-184(%rbp), %rax
	movq	-192(%rbp), %rdi
	testq	%rax, %rax
	je	.L218
	cmpq	$3, %rax
	jbe	.L219
	cmpb	$91, (%rdi)
	je	.L326
.L219:
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdi, %rdx
	je	.L268
	movq	%rdi, %rcx
	xorl	%esi, %esi
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L221:
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L268
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	je	.L327
.L223:
	movsbl	(%rcx), %eax
	cmpb	$46, %al
	jne	.L221
	addq	$1, %rcx
	addl	$1, %esi
	cmpq	%rcx, %rdx
	jne	.L223
.L327:
	cmpl	$3, %esi
	jne	.L268
.L218:
	cmpq	%rbx, %rdi
	je	.L236
	call	_ZdlPv@PLT
.L236:
	cmpb	$0, 33(%r15)
	je	.L237
	cmpb	$0, 32(%r15)
	jne	.L238
	movq	16(%r14), %rax
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	248(%rax), %rdi
	movq	(%rdi), %rax
	call	*(%rax)
.L242:
	addq	$104, %r15
	cmpq	%r15, %r13
	jne	.L195
	movq	-248(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L251:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L246
	call	_ZdlPv@PLT
.L246:
	movq	40(%rbx), %rdi
	leaq	56(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L247
	call	_ZdlPv@PLT
.L247:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L248
	call	_ZdlPv@PLT
	addq	$104, %rbx
	cmpq	%rbx, %r13
	jne	.L251
.L249:
	cmpq	$0, -248(%rbp)
	je	.L188
.L196:
	movq	-248(%rbp), %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L328
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	cmpq	$0, 48(%r15)
	je	.L329
	movq	16(%r14), %rax
	leaq	40(%r15), %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	248(%rax), %rdi
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%rbx, -192(%rbp)
	movq	72(%r15), %r9
	movq	80(%r15), %r10
	movq	%r9, %rax
	addq	%r10, %rax
	je	.L199
	testq	%r9, %r9
	je	.L207
.L199:
	movq	%r10, -224(%rbp)
	cmpq	$15, %r10
	jbe	.L214
	movq	-240(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r10, -256(%rbp)
	movq	%r9, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-232(%rbp), %r9
	movq	-256(%rbp), %r10
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, -176(%rbp)
.L215:
	movq	%r10, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-224(%rbp), %r10
	movq	-192(%rbp), %rax
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L270:
	movq	80(%r15), %r9
	movq	%rbx, -192(%rbp)
	movq	72(%r15), %r10
	cmpq	%r9, %rcx
	cmovbe	%rcx, %r9
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L271
	testq	%r10, %r10
	je	.L207
.L271:
	movq	%r9, -224(%rbp)
	cmpq	$15, %r9
	ja	.L330
	cmpq	$1, %r9
	jne	.L211
	movzbl	(%r10), %eax
	movb	%al, -176(%rbp)
	movq	%rbx, %rax
.L212:
	movq	%r9, -184(%rbp)
	movb	$0, (%rax,%r9)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L216:
	testq	%r10, %r10
	jne	.L331
	movq	%rbx, %rax
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%rdi, %rsi
	leaq	.LC7(%rip), %rcx
.L224:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L332
.L225:
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L269
	leal	-65(%rax), %r10d
	leal	32(%rax), %r9d
	addq	$1, %rsi
	cmpb	$26, %r10b
	cmovb	%r9d, %eax
	leal	-65(%rdx), %r9d
	addq	$1, %rcx
	cmpb	$25, %r9b
	ja	.L228
	addl	$32, %edx
	cmpb	%al, %dl
	je	.L224
.L269:
	movq	%rdi, %rsi
	leaq	.LC8(%rip), %rcx
.L230:
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	je	.L333
.L231:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L232
	leal	-65(%rdx), %r10d
	leal	32(%rdx), %r9d
	addq	$1, %rsi
	cmpb	$26, %r10b
	cmovb	%r9d, %edx
	leal	-65(%rax), %r9d
	addq	$1, %rcx
	cmpb	$25, %r9b
	ja	.L234
	addl	$32, %eax
	cmpb	%dl, %al
	je	.L230
.L232:
	cmpq	%rbx, %rdi
	je	.L237
	call	_ZdlPv@PLT
.L237:
	movdqa	.LC1(%rip), %xmm0
	movl	$10, %edx
	movl	$101, %edi
	movl	$224683380, -64(%rbp)
	movw	%dx, -60(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	.LC2(%rip), %xmm0
	movq	$0, -208(%rbp)
	movaps	%xmm0, -144(%rbp)
	movdqa	.LC3(%rip), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	.LC6(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	leaq	101(%rax), %rdx
	movdqa	-96(%rbp), %xmm5
	movdqa	-80(%rbp), %xmm6
	movq	%rax, -224(%rbp)
	movdqa	-160(%rbp), %xmm1
	movdqa	-144(%rbp), %xmm2
	movq	%rdx, -208(%rbp)
	movups	%xmm3, 32(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
.L317:
	movl	-64(%rbp), %ecx
	leaq	-224(%rbp), %rsi
	movl	%ecx, 96(%rax)
	movzbl	-60(%rbp), %ecx
	movb	%cl, 100(%rax)
	movq	16(%r14), %rdi
	movq	%rdx, -216(%rbp)
	leaq	_ZN4node9inspector12_GLOBAL__N_111HttpHandler25ThenCloseAndReportFailureEP10uv_write_si(%rip), %rdx
	call	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	-248(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L245:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L253
	call	_ZdlPv@PLT
.L253:
	movq	40(%rbx), %rdi
	leaq	56(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L254
	call	_ZdlPv@PLT
.L254:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L255
	call	_ZdlPv@PLT
	addq	$104, %rbx
	cmpq	%rbx, %r13
	jne	.L245
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L188
.L335:
	call	_ZdlPv@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L228:
	cmpb	%al, %dl
	jne	.L269
	movzbl	(%rsi), %eax
	testb	%al, %al
	jne	.L225
.L332:
	cmpb	$0, (%rcx)
	je	.L218
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L234:
	cmpb	%dl, %al
	jne	.L232
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	jne	.L231
.L333:
	cmpb	$0, (%rcx)
	je	.L218
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L211:
	testq	%r9, %r9
	jne	.L334
	movq	%rbx, %rax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L330:
	movq	-240(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r9, -256(%rbp)
	movq	%r10, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-232(%rbp), %r10
	movq	-256(%rbp), %r9
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, -176(%rbp)
.L210:
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-224(%rbp), %r9
	movq	-192(%rbp), %rax
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L325:
	movq	-240(%rbp), %rsi
	leaq	-192(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r9, -256(%rbp)
	movq	%r10, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-232(%rbp), %r10
	movq	-256(%rbp), %r9
	movq	%rax, -192(%rbp)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, -176(%rbp)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L326:
	cmpb	$93, -1(%rdi,%rax)
	jne	.L219
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L255:
	addq	$104, %rbx
	cmpq	%rbx, %r13
	jne	.L245
	movq	-248(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L335
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L323:
	movdqa	.LC1(%rip), %xmm0
	movl	$10, %ecx
	movl	$101, %edi
	movl	$224683380, -64(%rbp)
	movw	%cx, -60(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	.LC2(%rip), %xmm0
	movq	$0, -208(%rbp)
	movaps	%xmm0, -144(%rbp)
	movdqa	.LC3(%rip), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	.LC6(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -224(%rbp)
	call	_Znwm@PLT
	movl	-64(%rbp), %ecx
	movdqa	-128(%rbp), %xmm1
	leaq	-224(%rbp), %rsi
	movdqa	-160(%rbp), %xmm7
	movdqa	-112(%rbp), %xmm2
	leaq	101(%rax), %rdx
	movq	%rax, -224(%rbp)
	movdqa	-96(%rbp), %xmm3
	movdqa	-80(%rbp), %xmm4
	movl	%ecx, 96(%rax)
	movzbl	-60(%rbp), %ecx
	movups	%xmm7, (%rax)
	movdqa	-144(%rbp), %xmm7
	movq	16(%r14), %rdi
	movq	%rdx, -208(%rbp)
	movb	%cl, 100(%rax)
	movq	%rdx, -216(%rbp)
	leaq	_ZN4node9inspector12_GLOBAL__N_111HttpHandler25ThenCloseAndReportFailureEP10uv_write_si(%rip), %rdx
	movups	%xmm7, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	movups	%xmm4, 80(%rax)
	call	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	movq	-224(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L191
	call	_ZdlPv@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L248:
	addq	$104, %rbx
	cmpq	%rbx, %r13
	jne	.L251
	cmpq	$0, -248(%rbp)
	jne	.L196
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%r12, %rdi
	call	llhttp_resume_after_upgrade@PLT
	movq	(%rbx), %rax
	cmpq	8(%rbx), %rax
	je	.L191
	movq	%rax, 8(%rbx)
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L329:
	movdqa	.LC1(%rip), %xmm0
	movl	$10, %eax
	movl	$101, %edi
	movl	$224683380, -64(%rbp)
	movw	%ax, -60(%rbp)
	movaps	%xmm0, -160(%rbp)
	movdqa	.LC2(%rip), %xmm0
	movq	$0, -208(%rbp)
	movaps	%xmm0, -144(%rbp)
	movdqa	.LC3(%rip), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	.LC6(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -224(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm5
	movdqa	-128(%rbp), %xmm7
	movdqa	-144(%rbp), %xmm6
	leaq	101(%rax), %rdx
	movq	%rax, -224(%rbp)
	movups	%xmm5, (%rax)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm6, 16(%rax)
	movdqa	-96(%rbp), %xmm6
	movups	%xmm7, 32(%rax)
	movdqa	-80(%rbp), %xmm7
	movq	%rdx, -208(%rbp)
	movups	%xmm5, 48(%rax)
	movups	%xmm6, 64(%rax)
	movups	%xmm7, 80(%rax)
	jmp	.L317
.L207:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L328:
	call	__stack_chk_fail@PLT
.L331:
	movq	%rbx, %rdi
	jmp	.L215
.L334:
	movq	%rbx, %rdi
	jmp	.L210
	.cfi_endproc
.LFE4390:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnDataEPSt6vectorIcSaIcEE, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnDataEPSt6vectorIcSaIcEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder8delegateEv
	.type	_ZN4node9inspector9TcpHolder8delegateEv, @function
_ZN4node9inspector9TcpHolder8delegateEv:
.LFB4421:
	.cfi_startproc
	endbr64
	movq	248(%rdi), %rax
	ret
	.cfi_endproc
.LFE4421:
	.size	_ZN4node9inspector9TcpHolder8delegateEv, .-_ZN4node9inspector9TcpHolder8delegateEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder20DisconnectAndDisposeEPS1_
	.type	_ZN4node9inspector9TcpHolder20DisconnectAndDisposeEPS1_, @function
_ZN4node9inspector9TcpHolder20DisconnectAndDisposeEPS1_:
.LFB4424:
	.cfi_startproc
	endbr64
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	jmp	uv_close@PLT
	.cfi_endproc
.LFE4424:
	.size	_ZN4node9inspector9TcpHolder20DisconnectAndDisposeEPS1_, .-_ZN4node9inspector9TcpHolder20DisconnectAndDisposeEPS1_
	.section	.rodata.str1.1
.LC10:
	.string	"vector::_M_range_insert"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder12ReclaimUvBufEPK8uv_buf_tl
	.type	_ZN4node9inspector9TcpHolder12ReclaimUvBufEPK8uv_buf_tl, @function
_ZN4node9inspector9TcpHolder12ReclaimUvBufEPK8uv_buf_tl:
.LFB4425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %r13
	testq	%rdx, %rdx
	jg	.L362
.L339:
	testq	%r13, %r13
	je	.L338
	addq	$56, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdaPv@PLT
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	272(%rdi), %r14
	movq	%rdi, %rbx
	movq	%rsi, %r15
	movq	%rdx, %r12
	movq	280(%rdi), %rax
	subq	%r14, %rax
	cmpq	%rax, %rdx
	ja	.L340
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memmove@PLT
	addq	%r12, 272(%rbx)
.L341:
	movq	(%r15), %r13
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L340:
	movq	264(%rdi), %r9
	movq	%r14, %rdx
	movabsq	$9223372036854775807, %rsi
	movq	%rsi, %rax
	subq	%r9, %rdx
	subq	%rdx, %rax
	cmpq	%rax, %r12
	ja	.L363
	cmpq	%rdx, %r12
	movq	%rdx, %rax
	cmovnb	%r12, %rax
	addq	%rdx, %rax
	movq	%rax, %rcx
	jc	.L354
	testq	%rax, %rax
	js	.L354
	jne	.L345
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L354:
	movq	%rsi, %rcx
.L345:
	movq	%rcx, %rdi
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rcx
	movq	264(%rbx), %r9
	movq	%r14, %rdx
	movq	%rax, -56(%rbp)
	addq	%rax, %rcx
	subq	%r9, %rdx
	movq	%rcx, -64(%rbp)
.L352:
	movq	-56(%rbp), %rax
	leaq	(%rax,%rdx), %r11
	leaq	(%r11,%r12), %r10
	testq	%rdx, %rdx
	jne	.L364
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	memcpy@PLT
	movq	272(%rbx), %rdx
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L347
.L349:
	testq	%r9, %r9
	jne	.L348
.L350:
	movq	-56(%rbp), %xmm0
	movq	-64(%rbp), %rax
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, 280(%rbx)
	movups	%xmm0, 264(%rbx)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L364:
	movq	%r9, %rsi
	movq	%rax, %rdi
	movq	%r10, -88(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r11, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r11
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r11, %rdi
	call	memcpy@PLT
	movq	272(%rbx), %rdx
	movq	-88(%rbp), %r10
	movq	-72(%rbp), %r9
	subq	%r14, %rdx
	leaq	(%r10,%rdx), %r12
	jne	.L347
.L348:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L347:
	movq	%r14, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	jmp	.L349
.L363:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE4425:
	.size	_ZN4node9inspector9TcpHolder12ReclaimUvBufEPK8uv_buf_tl, .-_ZN4node9inspector9TcpHolder12ReclaimUvBufEPK8uv_buf_tl
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocketD2Ev
	.type	_ZN4node9inspector15InspectorSocketD2Ev, @function
_ZN4node9inspector15InspectorSocketD2Ev:
.LFB4427:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L365
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L365:
	ret
	.cfi_endproc
.LFE4427:
	.size	_ZN4node9inspector15InspectorSocketD2Ev, .-_ZN4node9inspector15InspectorSocketD2Ev
	.globl	_ZN4node9inspector15InspectorSocketD1Ev
	.set	_ZN4node9inspector15InspectorSocketD1Ev,_ZN4node9inspector15InspectorSocketD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocket8ShutdownEPNS0_15ProtocolHandlerE
	.type	_ZN4node9inspector15InspectorSocket8ShutdownEPNS0_15ProtocolHandlerE, @function
_ZN4node9inspector15InspectorSocket8ShutdownEPNS0_15ProtocolHandlerE:
.LFB4429:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.cfi_endproc
.LFE4429:
	.size	_ZN4node9inspector15InspectorSocket8ShutdownEPNS0_15ProtocolHandlerE, .-_ZN4node9inspector15InspectorSocket8ShutdownEPNS0_15ProtocolHandlerE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocket6AcceptEP11uv_stream_sSt10unique_ptrINS1_8DelegateESt14default_deleteIS5_EE
	.type	_ZN4node9inspector15InspectorSocket6AcceptEP11uv_stream_sSt10unique_ptrINS1_8DelegateESt14default_deleteIS5_EE, @function
_ZN4node9inspector15InspectorSocket6AcceptEP11uv_stream_sSt10unique_ptrINS1_8DelegateESt14default_deleteIS5_EE:
.LFB4432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-56(%rbp), %rdi
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	leaq	-48(%rbp), %rdx
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector9TcpHolder6AcceptEP11uv_stream_sSt10unique_ptrINS0_15InspectorSocket8DelegateESt14default_deleteIS6_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L369
	movq	(%rdi), %rax
	call	*32(%rax)
.L369:
	cmpq	$0, -56(%rbp)
	je	.L370
	movl	$8, %edi
	call	_Znwm@PLT
	movl	$344, %edi
	movq	-56(%rbp), %r13
	movq	$0, -56(%rbp)
	movq	$0, (%rax)
	movq	%rax, %r14
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rax, (%rbx)
	movq	%r14, 8(%rbx)
	movq	%r13, 16(%rbx)
	testq	%r13, %r13
	je	.L383
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111HttpHandlerE(%rip), %rax
	pxor	%xmm0, %xmm0
	leaq	32(%rbx), %rdi
	movb	$0, 24(%rbx)
	movq	%rbx, 256(%r13)
	leaq	128(%rbx), %r13
	movl	$1, %esi
	movq	%rax, (%rbx)
	movq	%r13, %rdx
	leaq	248(%rbx), %rax
	movq	%rax, 232(%rbx)
	leaq	272(%rbx), %rax
	movq	%rax, 288(%rbx)
	movq	%rax, 296(%rbx)
	leaq	328(%rbx), %rax
	movq	%rax, 312(%rbx)
	movups	%xmm0, 208(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 240(%rbx)
	movb	$0, 248(%rbx)
	movl	$0, 272(%rbx)
	movq	$0, 280(%rbx)
	movq	$0, 304(%rbx)
	movq	$0, 320(%rbx)
	movb	$0, 328(%rbx)
	call	llhttp_init@PLT
	movq	%r13, %rdi
	call	llhttp_settings_init@PLT
	leaq	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderValueEP18llhttp__internal_sPKcm(%rip), %rax
	leaq	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderFieldEP18llhttp__internal_sPKcm(%rip), %rcx
	movq	(%r14), %rdi
	movq	%rax, %xmm1
	movq	%rcx, %xmm0
	leaq	_ZN4node9inspector12_GLOBAL__N_111HttpHandler17OnMessageCompleteEP18llhttp__internal_s(%rip), %rax
	movq	%rbx, (%r14)
	movq	%rax, 184(%rbx)
	punpcklqdq	%xmm1, %xmm0
	leaq	_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnPathEP18llhttp__internal_sPKcm(%rip), %rax
	movq	%rax, 136(%rbx)
	movups	%xmm0, 152(%rbx)
	testq	%rdi, %rdi
	je	.L372
	movq	(%rdi), %rax
	call	*40(%rax)
.L372:
	movq	-56(%rbp), %rdi
	movq	%r14, (%r12)
	testq	%rdi, %rdi
	je	.L368
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L368:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L384
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	$0, (%r12)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	_ZZN4node9inspector15ProtocolHandlerC4EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4432:
	.size	_ZN4node9inspector15InspectorSocket6AcceptEP11uv_stream_sSt10unique_ptrINS1_8DelegateESt14default_deleteIS5_EE, .-_ZN4node9inspector15InspectorSocket6AcceptEP11uv_stream_sSt10unique_ptrINS1_8DelegateESt14default_deleteIS5_EE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocket13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector15InspectorSocket13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector15InspectorSocket13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4461:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*(%rax)
	.cfi_endproc
.LFE4461:
	.size	_ZN4node9inspector15InspectorSocket13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector15InspectorSocket13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocket15CancelHandshakeEv
	.type	_ZN4node9inspector15InspectorSocket15CancelHandshakeEv, @function
_ZN4node9inspector15InspectorSocket15CancelHandshakeEv:
.LFB4462:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.cfi_endproc
.LFE4462:
	.size	_ZN4node9inspector15InspectorSocket15CancelHandshakeEv, .-_ZN4node9inspector15InspectorSocket15CancelHandshakeEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocket7GetHostB5cxx11Ev
	.type	_ZN4node9inspector15InspectorSocket7GetHostB5cxx11Ev, @function
_ZN4node9inspector15InspectorSocket7GetHostB5cxx11Ev:
.LFB4463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-240(%rbp), %r14
	leaq	-252(%rbp), %rdx
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	16(%r12), %r13
	subq	$216, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%r14, %rsi
	movl	$128, -252(%rbp)
	movq	16(%rax), %rdi
	call	uv_tcp_getsockname@PLT
	testl	%eax, %eax
	jne	.L419
	leaq	-112(%rbp), %r15
	cmpw	$10, -240(%rbp)
	movl	$46, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	je	.L420
	call	uv_ip4_name@PLT
	movq	%r13, (%r12)
	testl	%eax, %eax
	je	.L392
.L422:
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
.L387:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L421
	addq	$216, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	call	uv_ip6_name@PLT
	movq	%r13, (%r12)
	testl	%eax, %eax
	jne	.L422
.L392:
	movq	%r15, %rbx
.L393:
	movl	(%rbx), %edx
	addq	$4, %rbx
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L393
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rbx), %rdx
	cmove	%rdx, %rbx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rbx
	subq	%r15, %rbx
	movq	%rbx, -248(%rbp)
	cmpq	$15, %rbx
	ja	.L423
	cmpq	$1, %rbx
	jne	.L397
	movzbl	-112(%rbp), %eax
	movb	%al, 16(%r12)
.L398:
	movq	%rbx, 8(%r12)
	movb	$0, 0(%r13,%rbx)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%r13, (%r12)
	movq	$0, 8(%r12)
	movb	$0, 16(%r12)
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L397:
	testq	%rbx, %rbx
	je	.L398
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L423:
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r13
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L396:
	movl	%ebx, %eax
	cmpl	$8, %ebx
	jnb	.L399
	andl	$4, %ebx
	jne	.L424
	testl	%eax, %eax
	je	.L400
	movzbl	(%r15), %edx
	movb	%dl, 0(%r13)
	testb	$2, %al
	je	.L400
	movzwl	-2(%r15,%rax), %edx
	movw	%dx, -2(%r13,%rax)
	.p2align 4,,10
	.p2align 3
.L400:
	movq	-248(%rbp), %rbx
	movq	(%r12), %r13
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L399:
	movq	-112(%rbp), %rax
	leaq	8(%r13), %rsi
	andq	$-8, %rsi
	movq	%rax, 0(%r13)
	movl	%ebx, %eax
	movq	-8(%r15,%rax), %rdx
	movq	%rdx, -8(%r13,%rax)
	subq	%rsi, %r13
	leal	(%rbx,%r13), %eax
	subq	%r13, %r15
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L400
	andl	$-8, %eax
	xorl	%edx, %edx
.L403:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%r15,%rcx), %rdi
	movq	%rdi, (%rsi,%rcx)
	cmpl	%eax, %edx
	jb	.L403
	jmp	.L400
.L424:
	movl	(%r15), %edx
	movl	%edx, 0(%r13)
	movl	-4(%r15,%rax), %edx
	movl	%edx, -4(%r13,%rax)
	jmp	.L400
.L421:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4463:
	.size	_ZN4node9inspector15InspectorSocket7GetHostB5cxx11Ev, .-_ZN4node9inspector15InspectorSocket7GetHostB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocket14SwitchProtocolEPNS0_15ProtocolHandlerE
	.type	_ZN4node9inspector15InspectorSocket14SwitchProtocolEPNS0_15ProtocolHandlerE, @function
_ZN4node9inspector15InspectorSocket14SwitchProtocolEPNS0_15ProtocolHandlerE:
.LFB4464:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	movq	%rsi, (%rdi)
	testq	%r8, %r8
	je	.L425
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	40(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L425:
	ret
	.cfi_endproc
.LFE4464:
	.size	_ZN4node9inspector15InspectorSocket14SwitchProtocolEPNS0_15ProtocolHandlerE, .-_ZN4node9inspector15InspectorSocket14SwitchProtocolEPNS0_15ProtocolHandlerE
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"cannot create std::vector larger than max_size()"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector15InspectorSocket5WriteEPKcm
	.type	_ZN4node9inspector15InspectorSocket5WriteEPKcm, @function
_ZN4node9inspector15InspectorSocket5WriteEPKcm:
.LFB4466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	movq	24(%rax), %r15
	testq	%rdx, %rdx
	js	.L438
	movl	$0, %ebx
	je	.L429
	movq	%rdx, %rdi
	movq	%rdx, %r12
	movq	%rsi, %r13
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	leaq	(%rax,%r12), %rbx
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	movq	%rbx, -64(%rbp)
	call	memcpy@PLT
.L429:
	movq	%r14, %rdi
	movq	%rbx, -72(%rbp)
	leaq	-80(%rbp), %rsi
	call	*%r15
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L427
	call	_ZdlPv@PLT
.L427:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L439
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L438:
	.cfi_restore_state
	leaq	.LC11(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L439:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4466:
	.size	_ZN4node9inspector15InspectorSocket5WriteEPKcm, .-_ZN4node9inspector15InspectorSocket5WriteEPKcm
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E:
.LFB5283:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L456
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L445:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L442
	call	_ZdlPv@PLT
.L442:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L443
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L440
.L444:
	movq	%rbx, %r12
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L444
.L440:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L456:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE5283:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD2Ev:
.LFB5914:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111HttpHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	312(%rdi), %rdi
	leaq	328(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L460
	call	_ZdlPv@PLT
.L460:
	movq	280(%rbx), %r12
	leaq	264(%rbx), %r14
	testq	%r12, %r12
	je	.L466
.L461:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L464
	call	_ZdlPv@PLT
.L464:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L465
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L466
.L467:
	movq	%r13, %r12
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L465:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L467
.L466:
	movq	232(%rbx), %rdi
	leaq	248(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movq	216(%rbx), %r13
	movq	208(%rbx), %r12
	cmpq	%r12, %r13
	je	.L468
	.p2align 4,,10
	.p2align 3
.L474:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L469
	call	_ZdlPv@PLT
.L469:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L470
	call	_ZdlPv@PLT
.L470:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L471
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L474
.L472:
	movq	208(%rbx), %r12
.L468:
	testq	%r12, %r12
	je	.L475
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L475:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L459
	popq	%rbx
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_close@PLT
	.p2align 4,,10
	.p2align 3
.L471:
	.cfi_restore_state
	addq	$104, %r12
	cmpq	%r12, %r13
	jne	.L474
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L459:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5914:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD2Ev, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD1Ev,_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD2Ev
	.section	.rodata.str1.1
.LC12:
	.string	"vector::_M_realloc_insert"
.LC13:
	.string	""
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler17OnMessageCompleteEP18llhttp__internal_s, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler17OnMessageCompleteEP18llhttp__internal_s:
.LFB4402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rax
	addq	$240, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-144(%rbp), %r8
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-32(%rdi), %rbx
	subq	$296, %rsp
	movq	%rdi, -256(%rbp)
	movq	288(%rbx), %r12
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
	movq	%r8, -296(%rbp)
	movq	%r8, -160(%rbp)
	movl	$1953722184, -144(%rbp)
	movq	$4, -152(%rbp)
	movb	$0, -140(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	movq	%rax, -248(%rbp)
	cmpq	%rax, %r12
	je	.L489
	movl	$4, %edi
	movq	32(%r12), %r9
	xorl	%r15d, %r15d
	leaq	-96(%rbp), %r14
	testq	%rdi, %rdi
	je	.L490
	.p2align 4,,10
	.p2align 3
.L664:
	xorl	%eax, %eax
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L662:
	testb	%cl, %cl
	je	.L490
	addq	$1, %rax
	cmpq	%rdi, %rax
	je	.L490
.L494:
	movzbl	(%r9,%rax), %ecx
	leal	-65(%rcx), %edx
	leal	32(%rcx), %esi
	cmpb	$26, %dl
	movzbl	(%r8,%rax), %edx
	cmovnb	%ecx, %esi
	leal	-65(%rdx), %r11d
	leal	32(%rdx), %r10d
	cmpb	$26, %r11b
	cmovb	%r10d, %edx
	cmpb	%sil, %dl
	je	.L662
.L493:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, -248(%rbp)
	je	.L663
	movq	-152(%rbp), %rdi
	movq	-160(%rbp), %r8
	movq	32(%r12), %r9
	testq	%rdi, %rdi
	jne	.L664
	.p2align 4,,10
	.p2align 3
.L490:
	testb	%r15b, %r15b
	jne	.L665
	leaq	64(%r12), %rsi
	movq	%r14, %rdi
	movl	$1, %r15d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L663:
	movq	-96(%rbp), %rax
	leaq	-112(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -272(%rbp)
	movq	%rcx, -128(%rbp)
	cmpq	%r13, %rax
	je	.L498
	movq	%rax, -128(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -112(%rbp)
.L501:
	movq	%rdx, -120(%rbp)
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L665:
	movq	-96(%rbp), %rdi
	leaq	-112(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -272(%rbp)
	movq	%rax, -128(%rbp)
	movb	$0, -112(%rbp)
	cmpq	%r13, %rdi
	je	.L496
	call	_ZdlPv@PLT
.L496:
	leaq	-208(%rbp), %rax
	xorl	%edx, %edx
	leaq	-224(%rbp), %rdi
	movq	$17, -232(%rbp)
	movq	%rax, -288(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, -304(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-232(%rbp), %rdx
	movdqa	.LC14(%rip), %xmm0
	movq	%rax, -224(%rbp)
	movq	%rdx, -208(%rbp)
	movups	%xmm0, (%rax)
	movq	-224(%rbp), %rdx
	movb	$121, 16(%rax)
	movq	-232(%rbp), %rax
	movq	%rax, -216(%rbp)
	movb	$0, (%rdx,%rax)
	movq	288(%rbx), %r12
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	cmpq	-248(%rbp), %r12
	je	.L502
	xorl	%r15d, %r15d
	leaq	-96(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-216(%rbp), %rdi
	movq	-224(%rbp), %r8
	movq	32(%r12), %r9
	testq	%rdi, %rdi
	je	.L503
	xorl	%eax, %eax
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L666:
	testb	%cl, %cl
	je	.L503
	addq	$1, %rax
	cmpq	%rax, %rdi
	je	.L503
.L507:
	movzbl	(%r9,%rax), %ecx
	leal	-65(%rcx), %edx
	leal	32(%rcx), %esi
	cmpb	$26, %dl
	movzbl	(%r8,%rax), %edx
	cmovnb	%ecx, %esi
	leal	-65(%rdx), %r11d
	leal	32(%rdx), %r10d
	cmpb	$26, %r11b
	cmovb	%r10d, %edx
	cmpb	%sil, %dl
	je	.L666
.L506:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	-248(%rbp), %rax
	jne	.L510
	movq	-96(%rbp), %rax
	leaq	-176(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	%rcx, -264(%rbp)
	movq	%rcx, -192(%rbp)
	cmpq	%r13, %rax
	je	.L575
	movq	%rax, -192(%rbp)
	movq	-80(%rbp), %rax
	movq	%rax, -176(%rbp)
.L512:
	movq	%rdx, -184(%rbp)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L503:
	testb	%r15b, %r15b
	jne	.L667
	leaq	64(%r12), %rsi
	movq	%r14, %rdi
	movl	$1, %r15d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L667:
	movq	-96(%rbp), %rdi
	leaq	-176(%rbp), %rax
	movq	$0, -184(%rbp)
	movq	%rax, -264(%rbp)
	movq	%rax, -192(%rbp)
	movb	$0, -176(%rbp)
	cmpq	%r13, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	-256(%rbp), %rax
	leaq	312(%rbx), %rsi
	movq	216(%rbx), %r14
	movq	%rsi, -280(%rbp)
	cmpb	$1, 73(%rax)
	sete	%cl
	cmpq	224(%rbx), %r14
	je	.L513
	leaq	16(%r14), %rdi
	cmpb	$0, 80(%rax)
	movq	320(%rbx), %r15
	movq	%rdi, (%r14)
	movq	312(%rbx), %r13
	setne	%r12b
	movq	%r13, %rax
	addq	%r15, %rax
	je	.L514
	testq	%r13, %r13
	je	.L519
.L514:
	movq	%r15, -232(%rbp)
	cmpq	$15, %r15
	ja	.L668
	cmpq	$1, %r15
	jne	.L517
	movzbl	0(%r13), %eax
	movb	%al, 16(%r14)
.L518:
	movq	%r15, 8(%r14)
	movb	$0, (%rdi,%r15)
	movq	-184(%rbp), %r15
	leaq	56(%r14), %rdi
	movb	%r12b, 32(%r14)
	movq	-192(%rbp), %r12
	movb	%cl, 33(%r14)
	movq	%r12, %rax
	movq	%rdi, 40(%r14)
	addq	%r15, %rax
	je	.L583
	testq	%r12, %r12
	je	.L519
.L583:
	movq	%r15, -232(%rbp)
	cmpq	$15, %r15
	ja	.L669
	cmpq	$1, %r15
	jne	.L523
	movzbl	(%r12), %eax
	movb	%al, 56(%r14)
.L524:
	movq	%r15, 48(%r14)
	movb	$0, (%rdi,%r15)
	movq	-128(%rbp), %r12
	leaq	88(%r14), %rdi
	movq	-120(%rbp), %r15
	movq	%rdi, 72(%r14)
	movq	%r12, %rax
	addq	%r15, %rax
	je	.L584
	testq	%r12, %r12
	je	.L519
.L584:
	movq	%r15, -232(%rbp)
	cmpq	$15, %r15
	ja	.L670
	cmpq	$1, %r15
	jne	.L528
	movzbl	(%r12), %eax
	movb	%al, 88(%r14)
.L529:
	movq	%r15, 80(%r14)
	movb	$0, (%rdi,%r15)
	addq	$104, 216(%rbx)
.L530:
	movq	-192(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L562
	call	_ZdlPv@PLT
.L562:
	movq	-224(%rbp), %rdi
	cmpq	-288(%rbp), %rdi
	je	.L563
	call	_ZdlPv@PLT
.L563:
	movq	-128(%rbp), %rdi
	cmpq	-272(%rbp), %rdi
	je	.L564
	call	_ZdlPv@PLT
.L564:
	movq	-160(%rbp), %rdi
	cmpq	-296(%rbp), %rdi
	je	.L565
	call	_ZdlPv@PLT
.L565:
	movq	320(%rbx), %rdx
	movq	-280(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	.LC13(%rip), %rcx
	leaq	264(%rbx), %r14
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	280(%rbx), %r12
	movb	$0, 24(%rbx)
	testq	%r12, %r12
	je	.L569
.L566:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L567
	call	_ZdlPv@PLT
.L567:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L568
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L569
.L570:
	movq	%r13, %r12
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L668:
	movq	-304(%rbp), %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	movb	%cl, -256(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movzbl	-256(%rbp), %ecx
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 16(%r14)
.L516:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movb	%cl, -256(%rbp)
	call	memcpy@PLT
	movq	-232(%rbp), %r15
	movq	(%r14), %rdi
	movzbl	-256(%rbp), %ecx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L502:
	leaq	-176(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -264(%rbp)
	movq	%rax, -192(%rbp)
.L575:
	movdqa	-80(%rbp), %xmm6
	movaps	%xmm6, -176(%rbp)
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	-112(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -272(%rbp)
	movq	%rax, -128(%rbp)
.L498:
	movdqa	-80(%rbp), %xmm5
	movaps	%xmm5, -112(%rbp)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L570
.L569:
	movq	-248(%rbp), %rax
	movq	240(%rbx), %rdx
	xorl	%esi, %esi
	movq	$0, 280(%rbx)
	movq	$0, 304(%rbx)
	xorl	%r8d, %r8d
	leaq	232(%rbx), %rdi
	leaq	.LC13(%rip), %rcx
	movq	%rax, 288(%rbx)
	movq	%rax, 296(%rbx)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L671
	addq	$296, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L513:
	.cfi_restore_state
	movq	208(%rbx), %rax
	movq	%r14, %r15
	movabsq	$5675921253449092805, %rdx
	subq	%rax, %r15
	movq	%rax, -320(%rbp)
	movq	%r15, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$88686269585142075, %rdx
	cmpq	%rdx, %rax
	je	.L672
	testq	%rax, %rax
	je	.L579
	leaq	(%rax,%rax), %rsi
	movq	%rsi, -328(%rbp)
	cmpq	%rsi, %rax
	jbe	.L673
	movabsq	$9223372036854775800, %rax
	movq	%rax, -328(%rbp)
.L532:
	movq	-328(%rbp), %rdi
	movb	%cl, -336(%rbp)
	call	_Znwm@PLT
	movzbl	-336(%rbp), %ecx
	movq	%rax, -312(%rbp)
.L533:
	movq	-256(%rbp), %rax
	movq	312(%rbx), %r9
	movq	320(%rbx), %r13
	addq	-312(%rbp), %r15
	cmpb	$0, 80(%rax)
	movq	%r9, %rax
	leaq	16(%r15), %rdi
	setne	%r12b
	addq	%r13, %rax
	movq	%rdi, (%r15)
	je	.L585
	testq	%r9, %r9
	je	.L519
.L585:
	movq	%r13, -232(%rbp)
	cmpq	$15, %r13
	ja	.L674
	cmpq	$1, %r13
	jne	.L537
	movzbl	(%r9), %eax
	movb	%al, 16(%r15)
.L538:
	movq	%r13, 8(%r15)
	movb	$0, (%rdi,%r13)
	movq	-192(%rbp), %r13
	leaq	56(%r15), %rdi
	movb	%r12b, 32(%r15)
	movq	-184(%rbp), %r12
	movq	%r13, %rax
	movb	%cl, 33(%r15)
	addq	%r12, %rax
	movq	%rdi, 40(%r15)
	je	.L586
	testq	%r13, %r13
	je	.L519
.L586:
	movq	%r12, -232(%rbp)
	cmpq	$15, %r12
	ja	.L675
	cmpq	$1, %r12
	jne	.L542
	movzbl	0(%r13), %eax
	movb	%al, 56(%r15)
.L543:
	movq	%r12, 48(%r15)
	movb	$0, (%rdi,%r12)
	movq	-128(%rbp), %r13
	leaq	88(%r15), %rdi
	movq	-120(%rbp), %r12
	movq	%rdi, 72(%r15)
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L587
	testq	%r13, %r13
	je	.L519
.L587:
	movq	%r12, -232(%rbp)
	cmpq	$15, %r12
	ja	.L676
	cmpq	$1, %r12
	jne	.L547
	movzbl	0(%r13), %eax
	movb	%al, 88(%r15)
.L548:
	movq	-320(%rbp), %rax
	movq	%r12, 80(%r15)
	movb	$0, (%rdi,%r12)
	cmpq	%rax, %r14
	je	.L582
	movq	-312(%rbp), %r13
	leaq	16(%rax), %r15
	leaq	56(%rax), %r12
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L550:
	movq	%rax, 0(%r13)
	movq	(%r15), %rax
	movq	%rax, 16(%r13)
.L551:
	movq	-8(%r15), %rax
	movq	%rax, 8(%r13)
	movzbl	16(%r15), %eax
	movq	%r15, -16(%r15)
	movq	$0, -8(%r15)
	movb	$0, (%r15)
	movb	%al, 32(%r13)
	movzbl	17(%r15), %eax
	movb	%al, 33(%r13)
	leaq	56(%r13), %rax
	movq	%rax, 40(%r13)
	movq	24(%r15), %rax
	cmpq	%r12, %rax
	je	.L677
	movq	%rax, 40(%r13)
	movq	40(%r15), %rax
	movq	%rax, 56(%r13)
.L553:
	movq	32(%r15), %rax
	leaq	72(%r15), %rdx
	movq	%rax, 48(%r13)
	leaq	88(%r13), %rax
	movq	%r12, 24(%r15)
	movq	$0, 32(%r15)
	movb	$0, 40(%r15)
	movq	%rax, 72(%r13)
	movq	56(%r15), %rax
	cmpq	%rdx, %rax
	je	.L678
	movq	%rax, 72(%r13)
	movq	72(%r15), %rax
	movq	%rax, 88(%r13)
.L555:
	movq	64(%r15), %rax
	movq	24(%r15), %rdi
	movq	%rax, 80(%r13)
	cmpq	%r12, %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	-16(%r15), %rdi
	cmpq	%r15, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	leaq	104(%r15), %rax
	addq	$88, %r15
	addq	$104, %r13
	addq	$104, %r12
	cmpq	%r15, %r14
	je	.L558
	movq	%rax, %r15
.L560:
	leaq	16(%r13), %rax
	movq	%rax, 0(%r13)
	movq	-16(%r15), %rax
	cmpq	%r15, %rax
	jne	.L550
	movdqu	(%r15), %xmm1
	movups	%xmm1, 16(%r13)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L517:
	testq	%r15, %r15
	je	.L518
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L528:
	testq	%r15, %r15
	je	.L529
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L523:
	testq	%r15, %r15
	je	.L524
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L670:
	movq	-304(%rbp), %rsi
	leaq	72(%r14), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 72(%r14)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 88(%r14)
.L527:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r15
	movq	72(%r14), %rdi
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L669:
	movq	-304(%rbp), %rsi
	leaq	40(%r14), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 40(%r14)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 56(%r14)
.L522:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r15
	movq	40(%r14), %rdi
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L579:
	movq	$104, -328(%rbp)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L678:
	movdqu	72(%r15), %xmm3
	movups	%xmm3, 88(%r13)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L677:
	movdqu	40(%r15), %xmm2
	movups	%xmm2, 56(%r13)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L558:
	leaq	-104(%r14), %rax
	subq	-320(%rbp), %rax
	movabsq	$1064235235021704901, %r14
	movq	-312(%rbp), %rcx
	shrq	$3, %rax
	imulq	%r14, %rax
	movabsq	$2305843009213693951, %r14
	andq	%r14, %rax
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rax,%rdx,4), %rax
	leaq	(%rcx,%rax,8), %r14
.L549:
	movq	-320(%rbp), %rax
	addq	$104, %r14
	testq	%rax, %rax
	je	.L561
	movq	%rax, %rdi
	call	_ZdlPv@PLT
.L561:
	movq	-312(%rbp), %rax
	movq	%r14, %xmm4
	movq	%rax, %xmm0
	addq	-328(%rbp), %rax
	punpcklqdq	%xmm4, %xmm0
	movq	%rax, 224(%rbx)
	movups	%xmm0, 208(%rbx)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L673:
	testq	%rsi, %rsi
	jne	.L679
	movq	$0, -312(%rbp)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L547:
	testq	%r12, %r12
	je	.L548
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L542:
	testq	%r12, %r12
	je	.L543
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L537:
	testq	%r13, %r13
	je	.L538
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L674:
	movq	-304(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r9, -336(%rbp)
	movb	%cl, -256(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movzbl	-256(%rbp), %ecx
	movq	-336(%rbp), %r9
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 16(%r15)
.L536:
	movq	%r13, %rdx
	movq	%r9, %rsi
	movb	%cl, -256(%rbp)
	call	memcpy@PLT
	movq	-232(%rbp), %r13
	movq	(%r15), %rdi
	movzbl	-256(%rbp), %ecx
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L676:
	movq	-304(%rbp), %rsi
	leaq	72(%r15), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 72(%r15)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 88(%r15)
.L546:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r12
	movq	72(%r15), %rdi
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L675:
	movq	-304(%rbp), %rsi
	leaq	40(%r15), %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 40(%r15)
	movq	%rax, %rdi
	movq	-232(%rbp), %rax
	movq	%rax, 56(%r15)
.L541:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-232(%rbp), %r12
	movq	40(%r15), %rdi
	jmp	.L543
.L519:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L582:
	movq	-312(%rbp), %r14
	jmp	.L549
.L671:
	call	__stack_chk_fail@PLT
.L672:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L679:
	cmpq	%rdx, %rsi
	cmovbe	%rsi, %rdx
	imulq	$104, %rdx, %rax
	movq	%rax, -328(%rbp)
	jmp	.L532
	.cfi_endproc
.LFE4402:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler17OnMessageCompleteEP18llhttp__internal_s, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler17OnMessageCompleteEP18llhttp__internal_s
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD0Ev:
.LFB5916:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111HttpHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	312(%rdi), %rdi
	leaq	328(%r13), %rax
	cmpq	%rax, %rdi
	je	.L681
	call	_ZdlPv@PLT
.L681:
	movq	280(%r13), %r12
	leaq	264(%r13), %r14
	testq	%r12, %r12
	je	.L687
.L682:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L685
	call	_ZdlPv@PLT
.L685:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L686
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L687
.L688:
	movq	%rbx, %r12
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L688
.L687:
	movq	232(%r13), %rdi
	leaq	248(%r13), %rax
	cmpq	%rax, %rdi
	je	.L684
	call	_ZdlPv@PLT
.L684:
	movq	216(%r13), %rbx
	movq	208(%r13), %r12
	cmpq	%r12, %rbx
	je	.L689
	.p2align 4,,10
	.p2align 3
.L695:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L690
	call	_ZdlPv@PLT
.L690:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L691
	call	_ZdlPv@PLT
.L691:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L692
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %rbx
	jne	.L695
.L693:
	movq	208(%r13), %r12
.L689:
	testq	%r12, %r12
	je	.L696
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L696:
	movq	16(%r13), %rdi
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L697
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L697:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$344, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L692:
	.cfi_restore_state
	addq	$104, %r12
	cmpq	%r12, %rbx
	jne	.L695
	jmp	.L693
	.cfi_endproc
.LFE5916:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD0Ev, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler8ShutdownEv, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler8ShutdownEv:
.LFB4395:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111HttpHandlerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	312(%rdi), %rdi
	leaq	328(%r13), %rax
	cmpq	%rax, %rdi
	je	.L713
	call	_ZdlPv@PLT
.L713:
	movq	280(%r13), %r12
	leaq	264(%r13), %r14
	testq	%r12, %r12
	je	.L719
.L714:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE8_M_eraseEPSt13_Rb_tree_nodeIS8_E
	movq	64(%r12), %rdi
	leaq	80(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L717
	call	_ZdlPv@PLT
.L717:
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L718
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L719
.L720:
	movq	%rbx, %r12
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L718:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L720
.L719:
	movq	232(%r13), %rdi
	leaq	248(%r13), %rax
	cmpq	%rax, %rdi
	je	.L716
	call	_ZdlPv@PLT
.L716:
	movq	216(%r13), %rbx
	movq	208(%r13), %r12
	cmpq	%r12, %rbx
	je	.L721
	.p2align 4,,10
	.p2align 3
.L727:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L722
	call	_ZdlPv@PLT
.L722:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L723
	call	_ZdlPv@PLT
.L723:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L724
	call	_ZdlPv@PLT
	addq	$104, %r12
	cmpq	%r12, %rbx
	jne	.L727
.L725:
	movq	208(%r13), %r12
.L721:
	testq	%r12, %r12
	je	.L728
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L728:
	movq	16(%r13), %rdi
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L729
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L729:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$344, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	addq	$104, %r12
	cmpq	%r12, %rbx
	jne	.L727
	jmp	.L725
	.cfi_endproc
.LFE4395:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler8ShutdownEv, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler8ShutdownEv
	.section	.text._ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.type	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, @function
_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_:
.LFB5443:
	.cfi_startproc
	endbr64
	movabsq	$9223372036854775807, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rsi
	movq	(%rdi), %r15
	movq	%rsi, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	je	.L758
	movq	%rdx, %r13
	movq	%r8, %rdx
	movq	%rdi, %r12
	subq	%r15, %rdx
	testq	%rax, %rax
	je	.L753
	leaq	(%rax,%rax), %r14
	cmpq	%r14, %rax
	jbe	.L759
.L755:
	movq	%rcx, %r14
.L746:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L752:
	movzbl	0(%r13), %eax
	subq	%r8, %rsi
	leaq	1(%rbx,%rdx), %r10
	movq	%rsi, %r13
	movb	%al, (%rbx,%rdx)
	leaq	(%r10,%rsi), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L760
	testq	%rsi, %rsi
	jg	.L748
	testq	%r15, %r15
	jne	.L751
.L749:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	.cfi_restore_state
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	jg	.L748
.L751:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L759:
	testq	%r14, %r14
	js	.L755
	jne	.L746
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	testq	%r15, %r15
	je	.L749
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L753:
	movl	$1, %r14d
	jmp	.L746
.L758:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5443:
	.size	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_, .-_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler6OnDataEPSt6vectorIcSaIcEE, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler6OnDataEPSt6vectorIcSaIcEE:
.LFB4352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-81(%rbp), %rbx
	subq	$88, %rsp
	movq	8(%rsi), %rcx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rbx, -112(%rbp)
	movq	(%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L789:
	movq	%rcx, %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	subq	%rax, %rdx
	movaps	%xmm0, -80(%rbp)
	cmpq	$1, %rdx
	jbe	.L761
	movzbl	(%rax), %ebx
	movl	%ebx, %r12d
	testb	$48, %bl
	notl	%r12d
	setne	%sil
	shrb	$7, %r12b
	orb	%sil, %r12b
	jne	.L817
	movl	%ebx, %esi
	andl	$15, %esi
	cmpb	$1, %sil
	je	.L765
	cmpb	$8, %sil
	jne	.L817
	movl	$1, %r12d
.L765:
	movzbl	1(%rax), %r9d
	testb	%r9b, %r9b
	jns	.L817
	andl	$64, %ebx
	movl	%r9d, %esi
	leaq	2(%rax), %r10
	movb	%bl, -113(%rbp)
	andl	$127, %esi
	movq	%r9, %rbx
	andl	$127, %ebx
	cmpb	$125, %sil
	jbe	.L767
	subq	%r10, %rcx
	cmpb	$126, %sil
	je	.L818
	cmpq	$7, %rcx
	jle	.L815
	movzbl	2(%rax), %ecx
	movzbl	3(%rax), %r9d
	leaq	10(%rax), %r10
	salq	$8, %rcx
	orq	%rcx, %r9
	movq	%r9, %rcx
	movzbl	4(%rax), %r9d
	salq	$8, %rcx
	orq	%rcx, %r9
	movzbl	5(%rax), %ecx
	salq	$8, %r9
	orq	%r9, %rcx
	salq	$8, %rcx
	movq	%rcx, %r9
	movzbl	6(%rax), %ecx
	orq	%r9, %rcx
	movzbl	7(%rax), %r9d
	salq	$8, %rcx
	orq	%rcx, %r9
	movq	%r9, %rcx
	movzbl	8(%rax), %r9d
	salq	$8, %rcx
	orq	%rcx, %r9
	movzbl	9(%rax), %ecx
	salq	$8, %r9
	orq	%r9, %rcx
	movq	%rcx, %rbx
	jns	.L767
	.p2align 4,,10
	.p2align 3
.L817:
	movq	16(%r14), %rdi
.L764:
	movq	$0, 16(%r14)
	testq	%rdi, %rdi
	je	.L780
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L780:
	cmpb	$0, 56(%r14)
	jne	.L819
.L781:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L761
	call	_ZdlPv@PLT
.L761:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L820
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L815:
	.cfi_restore_state
	cmpb	$0, -113(%rbp)
	jne	.L817
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L818:
	cmpq	$1, %rcx
	jle	.L815
	movzbl	2(%rax), %r9d
	movzbl	3(%rax), %ecx
	leaq	4(%rax), %r10
	salq	$8, %r9
	movq	%rcx, %rbx
	orq	%r9, %rbx
.L767:
	subq	$4, %rdx
	cmpq	%rbx, %rdx
	jb	.L815
	testq	%rbx, %rbx
	je	.L772
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%r15d, %r15d
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L821:
	addq	$1, %r15
	movb	%al, (%rdx)
	addq	$1, -72(%rbp)
	cmpq	%r15, %rbx
	je	.L774
.L775:
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rsi
.L776:
	movq	%r15, %rax
	andl	$3, %eax
	movzbl	(%r10,%rax), %eax
	xorb	4(%r10,%r15), %al
	movb	%al, -81(%rbp)
	cmpq	%rdx, %rsi
	jne	.L821
	movq	-112(%rbp), %rdx
	leaq	-80(%rbp), %rdi
	addq	$1, %r15
	movq	%r10, -104(%rbp)
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	cmpq	%rbx, %r15
	movq	-104(%rbp), %r10
	jne	.L775
.L774:
	movq	0(%r13), %rax
.L772:
	leaq	4(%r10,%rbx), %r15
	subq	%rax, %r15
	testb	%r12b, %r12b
	jne	.L822
	cmpb	$0, -113(%rbp)
	movq	16(%r14), %rdi
	jne	.L764
	movq	248(%rdi), %rdi
	leaq	-80(%rbp), %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L791
	call	_ZdlPv@PLT
.L791:
	testl	%r15d, %r15d
	jle	.L761
	movq	0(%r13), %rdi
	movq	8(%r13), %rcx
	movslq	%r15d, %r15
	addq	%rdi, %r15
	movq	%rcx, %rdx
	movq	%rdi, %rax
	subq	%r15, %rdx
	cmpq	%rcx, %r15
	je	.L785
	movq	%r15, %rsi
	call	memmove@PLT
	movq	8(%r13), %rcx
	movq	%rax, %rdi
	movq	0(%r13), %rax
	movq	%rcx, %rdx
	subq	%r15, %rdx
.L785:
	addq	%rdx, %rdi
	cmpq	%rcx, %rdi
	je	.L786
	movq	%rdi, 8(%r13)
	cmpq	%rax, %rdi
	je	.L761
	movq	%rdi, %rcx
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L786:
	cmpq	%rcx, %rax
	jne	.L789
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L819:
	movq	16(%r14), %rdi
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rax
	movq	%rax, (%r14)
	testq	%rdi, %rdi
	je	.L782
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L782:
	movl	$64, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L822:
	cmpb	$0, -113(%rbp)
	jne	.L817
	movq	48(%r14), %rdi
	movq	40(%r14), %rax
	addq	%r14, %rdi
	testb	$1, %al
	je	.L783
	movq	(%rdi), %rdx
	movq	-1(%rdx,%rax), %rax
.L783:
	call	*%rax
	jmp	.L781
.L820:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4352:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler6OnDataEPSt6vectorIcSaIcEE, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler6OnDataEPSt6vectorIcSaIcEE
	.section	.text._ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag
	.type	_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag, @function
_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag:
.LFB5446:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L860
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.L826
	movq	%rdi, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r14
	jnb	.L827
	movq	%rdi, %r15
	movq	%r14, %rdx
	subq	%r14, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	addq	%r14, 8(%rbx)
	movq	%rax, %rdi
	subq	%r13, %r15
	jne	.L864
.L828:
	movq	%r14, %rdx
.L863:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L826:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rdx
	movq	(%rbx), %r8
	movq	%rdx, %rax
	subq	%r8, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L865
	cmpq	%rdi, %r14
	movq	%rdi, %rax
	cmovnb	%r14, %rax
	addq	%rax, %rdi
	movq	%rdi, %r15
	jc	.L843
	testq	%rdi, %rdi
	js	.L843
	jne	.L834
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L840:
	movq	%r13, %rdx
	subq	%r8, %rdx
	leaq	(%r14,%rdx), %r9
	leaq	(%rcx,%rdx), %r10
	addq	%rcx, %r9
	testq	%rdx, %rdx
	jne	.L866
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L836
.L838:
	testq	%r8, %r8
	jne	.L837
.L839:
	movq	%rcx, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
.L823:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	leaq	(%rdx,%r15), %rsi
	subq	%rsi, %rcx
	jne	.L867
.L829:
	subq	%r15, %r14
	addq	%r14, %rdi
	movq	%rdi, 8(%rbx)
	testq	%r15, %r15
	je	.L823
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	addq	%r15, 8(%rbx)
	movq	%r15, %rdx
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L860:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L834:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r8
	movq	%rax, %rcx
	addq	%rax, %r15
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L864:
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L867:
	movq	%rcx, %rdx
	call	memmove@PLT
	movq	8(%rbx), %rdi
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L836
.L837:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L838
.L865:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5446:
	.size	_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag, .-_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector9TcpHolder16OnDataReceivedCbEP11uv_stream_slPK8uv_buf_t
	.type	_ZN4node9inspector9TcpHolder16OnDataReceivedCbEP11uv_stream_slPK8uv_buf_t, @function
_ZN4node9inspector9TcpHolder16OnDataReceivedCbEP11uv_stream_slPK8uv_buf_t:
.LFB4423:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdx), %rdi
	testq	%rsi, %rsi
	jg	.L880
	testq	%rdi, %rdi
	je	.L872
.L870:
	call	_ZdaPv@PLT
.L872:
	movq	256(%rbx), %rdi
	leaq	264(%rbx), %r14
	movq	(%rdi), %rax
	testq	%r12, %r12
	jns	.L874
	popq	%rbx
	movq	16(%rax), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L880:
	.cfi_restore_state
	movq	272(%rbx), %rsi
	leaq	264(%rbx), %r14
	movq	%rdx, %r13
	leaq	(%rdi,%r12), %rcx
	movq	%rdi, %rdx
	movq	%r14, %rdi
	call	_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L870
	movq	256(%rbx), %rdi
	movq	(%rdi), %rax
.L874:
	popq	%rbx
	movq	%r14, %rsi
	popq	%r12
	movq	8(%rax), %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE4423:
	.size	_ZN4node9inspector9TcpHolder16OnDataReceivedCbEP11uv_stream_slPK8uv_buf_t, .-_ZN4node9inspector9TcpHolder16OnDataReceivedCbEP11uv_stream_slPK8uv_buf_t
	.section	.text._ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag
	.type	_ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag, @function
_ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag:
.LFB5447:
	.cfi_startproc
	endbr64
	cmpq	%rdx, %rcx
	je	.L918
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.L884
	movq	%rdi, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r14
	jnb	.L885
	movq	%rdi, %r15
	movq	%r14, %rdx
	subq	%r14, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	addq	%r14, 8(%rbx)
	movq	%rax, %rdi
	subq	%r13, %r15
	jne	.L922
.L886:
	movq	%r14, %rdx
.L921:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L884:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rdx
	movq	(%rbx), %r8
	movq	%rdx, %rax
	subq	%r8, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L923
	cmpq	%rdi, %r14
	movq	%rdi, %rax
	cmovnb	%r14, %rax
	addq	%rax, %rdi
	movq	%rdi, %r15
	jc	.L901
	testq	%rdi, %rdi
	js	.L901
	jne	.L892
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L898:
	movq	%r13, %rdx
	subq	%r8, %rdx
	leaq	(%r14,%rdx), %r9
	leaq	(%rcx,%rdx), %r10
	addq	%rcx, %r9
	testq	%rdx, %rdx
	jne	.L924
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L894
.L896:
	testq	%r8, %r8
	jne	.L895
.L897:
	movq	%rcx, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
.L881:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L885:
	.cfi_restore_state
	leaq	(%rdx,%r15), %rsi
	subq	%rsi, %rcx
	jne	.L925
.L887:
	subq	%r15, %r14
	addq	%r14, %rdi
	movq	%rdi, 8(%rbx)
	testq	%r15, %r15
	je	.L881
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	addq	%r15, 8(%rbx)
	movq	%r15, %rdx
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L892:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r8
	movq	%rax, %rcx
	addq	%rax, %r15
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L922:
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L925:
	movq	%rcx, %rdx
	call	memmove@PLT
	movq	8(%rbx), %rdi
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L924:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L894
.L895:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L894:
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L896
.L923:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5447:
	.size	_ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag, .-_ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_19WsHandler5WriteESt6vectorIcSaIcEE, @function
_ZN4node9inspector12_GLOBAL__N_19WsHandler5WriteESt6vectorIcSaIcEE:
.LFB4353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-97(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	movq	%r15, %rdx
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	xorl	%esi, %esi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movb	$-127, -97(%rbp)
	movaps	%xmm0, -96(%rbp)
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	%rcx, %r12
	subq	%rdx, %r12
	cmpq	$125, %r12
	jbe	.L946
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rsi
	cmpq	$65535, %r12
	ja	.L931
	movb	$126, -97(%rbp)
	cmpq	%rax, %rsi
	je	.L932
	movb	$126, (%rax)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -88(%rbp)
.L933:
	movq	%r12, %rax
	shrq	$8, %rax
	movb	%al, -97(%rbp)
	cmpq	%rsi, -80(%rbp)
	je	.L934
	movb	%al, (%rsi)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -88(%rbp)
.L935:
	movb	%r12b, -97(%rbp)
	cmpq	%rsi, -80(%rbp)
	je	.L936
	movb	%r12b, (%rsi)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -88(%rbp)
.L937:
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L931:
	movb	$127, -97(%rbp)
	cmpq	%rax, %rsi
	je	.L938
	movb	$127, (%rax)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -88(%rbp)
.L939:
	leaq	-64(%rbp), %rdx
	leaq	-56(%rbp), %rcx
	movq	%r13, %rdi
	bswap	%r12
	movq	%r12, -64(%rbp)
	call	_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	-88(%rbp), %rsi
.L930:
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE15_M_range_insertIN9__gnu_cxx17__normal_iteratorIPKcS1_EEEEvNS4_IPcS1_EET_SA_St20forward_iterator_tag
	movq	16(%r14), %rdi
	leaq	_ZN4node9inspector12_GLOBAL__N_112WriteRequest7CleanupEP10uv_write_si(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L926
	call	_ZdlPv@PLT
.L926:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L947
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L946:
	.cfi_restore_state
	movb	%r12b, -97(%rbp)
	movq	-88(%rbp), %rsi
	cmpq	-80(%rbp), %rsi
	je	.L928
	movb	%r12b, (%rsi)
	movq	-88(%rbp), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, -88(%rbp)
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	movq	-88(%rbp), %rsi
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L936:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	movq	-88(%rbp), %rsi
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L934:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	movq	-88(%rbp), %rsi
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L932:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	movq	-88(%rbp), %rsi
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L928:
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIcSaIcEE17_M_realloc_insertIJcEEEvN9__gnu_cxx17__normal_iteratorIPcS1_EEDpOT_
	movq	8(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	-88(%rbp), %rsi
	jmp	.L930
.L947:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4353:
	.size	_ZN4node9inspector12_GLOBAL__N_19WsHandler5WriteESt6vectorIcSaIcEE, .-_ZN4node9inspector12_GLOBAL__N_19WsHandler5WriteESt6vectorIcSaIcEE
	.section	.text._ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag,"axG",@progbits,_ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag
	.type	_ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag, @function
_ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag:
.LFB5491:
	.cfi_startproc
	endbr64
	cmpq	%rcx, %rdx
	je	.L985
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	subq	%rdx, %r14
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	16(%rbx), %rax
	subq	%rdi, %rax
	cmpq	%r14, %rax
	jb	.L951
	movq	%rdi, %r15
	subq	%rsi, %r15
	cmpq	%r15, %r14
	jnb	.L952
	movq	%rdi, %r15
	movq	%r14, %rdx
	subq	%r14, %r15
	movq	%r15, %rsi
	call	memmove@PLT
	addq	%r14, 8(%rbx)
	movq	%rax, %rdi
	subq	%r13, %r15
	jne	.L989
.L953:
	movq	%r14, %rdx
.L988:
	addq	$40, %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	memmove@PLT
	.p2align 4,,10
	.p2align 3
.L951:
	.cfi_restore_state
	movabsq	$9223372036854775807, %rdx
	movq	(%rbx), %r8
	movq	%rdx, %rax
	subq	%r8, %rdi
	subq	%rdi, %rax
	cmpq	%rax, %r14
	ja	.L990
	cmpq	%rdi, %r14
	movq	%rdi, %rax
	cmovnb	%r14, %rax
	addq	%rax, %rdi
	movq	%rdi, %r15
	jc	.L968
	testq	%rdi, %rdi
	js	.L968
	jne	.L959
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L965:
	movq	%r13, %rdx
	subq	%r8, %rdx
	leaq	(%r14,%rdx), %r9
	leaq	(%rcx,%rdx), %r10
	addq	%rcx, %r9
	testq	%rdx, %rdx
	jne	.L991
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L961
.L963:
	testq	%r8, %r8
	jne	.L962
.L964:
	movq	%rcx, %xmm0
	movq	%r12, %xmm1
	movq	%r15, 16(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rbx)
.L948:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L952:
	.cfi_restore_state
	leaq	(%rdx,%r15), %rsi
	subq	%rsi, %rcx
	jne	.L992
.L954:
	subq	%r15, %r14
	addq	%r14, %rdi
	movq	%rdi, 8(%rbx)
	testq	%r15, %r15
	je	.L948
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	addq	%r15, 8(%rbx)
	movq	%r15, %rdx
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L985:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L968:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdx, %r15
.L959:
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	(%rbx), %r8
	movq	%rax, %rcx
	addq	%rax, %r15
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L989:
	subq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memmove@PLT
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L992:
	movq	%rcx, %rdx
	call	memmove@PLT
	movq	8(%rbx), %rdi
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L991:
	movq	%r8, %rsi
	movq	%rcx, %rdi
	movq	%r9, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	movq	%r10, -80(%rbp)
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	movq	-72(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	subq	%r13, %rdx
	leaq	(%r9,%rdx), %r12
	jne	.L961
.L962:
	movq	%r8, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L961:
	movq	%r13, %rsi
	movq	%r9, %rdi
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rcx
	jmp	.L963
.L990:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE5491:
	.size	_ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag, .-_ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4387:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-208(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$224, %rsp
	movq	(%rsi), %r14
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, -224(%rbp)
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L994
	testq	%r14, %r14
	je	.L1022
.L994:
	movq	%r13, -256(%rbp)
	cmpq	$15, %r13
	ja	.L1023
	cmpq	$1, %r13
	jne	.L997
	movzbl	(%r14), %eax
	movb	%al, -208(%rbp)
	movq	%r12, %rax
.L998:
	movq	%r13, -216(%rbp)
	movb	$0, (%rax,%r13)
	movabsq	$4611686018427387903, %rax
	subq	-216(%rbp), %rax
	cmpq	$35, %rax
	jbe	.L1024
	leaq	-224(%rbp), %rdi
	movl	$36, %edx
	leaq	_ZZN4node9inspector12_GLOBAL__N_1L22generate_accept_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPA28_cE8ws_magic(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-216(%rbp), %rsi
	movq	-224(%rbp), %rdi
	leaq	-144(%rbp), %rdx
	call	SHA1@PLT
	movzbl	-144(%rbp), %edx
	leaq	_ZZN4nodeL13base64_encodeEPKcmPcmE5table(%rip), %rax
	movzbl	-143(%rbp), %ecx
	movzbl	-142(%rbp), %esi
	movl	%edx, %edi
	sall	$4, %edx
	shrl	$2, %edi
	andl	$48, %edx
	movzbl	(%rax,%rdi), %edi
	movb	%dil, -176(%rbp)
	movl	%ecx, %edi
	sall	$2, %ecx
	shrl	$4, %edi
	andl	$60, %ecx
	orl	%edi, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, -175(%rbp)
	movl	%esi, %edx
	andl	$63, %esi
	shrl	$6, %edx
	andl	$3, %edx
	orl	%ecx, %edx
	movzbl	-140(%rbp), %ecx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, -174(%rbp)
	movzbl	(%rax,%rsi), %edx
	movzbl	-139(%rbp), %esi
	movb	%dl, -173(%rbp)
	movzbl	-141(%rbp), %edx
	movl	%edx, %edi
	sall	$4, %edx
	shrl	$2, %edi
	andl	$48, %edx
	movzbl	(%rax,%rdi), %edi
	movb	%dil, -172(%rbp)
	movl	%ecx, %edi
	sall	$2, %ecx
	shrl	$4, %edi
	andl	$60, %ecx
	orl	%edi, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, -171(%rbp)
	movl	%esi, %edx
	andl	$63, %esi
	shrl	$6, %edx
	andl	$3, %edx
	orl	%ecx, %edx
	movzbl	-138(%rbp), %ecx
	movzbl	(%rax,%rdx), %edx
	movl	%ecx, %edi
	sall	$4, %ecx
	movb	%dl, -170(%rbp)
	movzbl	(%rax,%rsi), %edx
	shrl	$2, %edi
	andl	$48, %ecx
	movzbl	(%rax,%rdi), %edi
	movzbl	-136(%rbp), %esi
	movb	%dl, -169(%rbp)
	movzbl	-137(%rbp), %edx
	movb	%dil, -168(%rbp)
	movl	%edx, %edi
	sall	$2, %edx
	shrl	$4, %edi
	andl	$60, %edx
	orl	%edi, %ecx
	movzbl	(%rax,%rcx), %ecx
	movb	%cl, -167(%rbp)
	movl	%esi, %ecx
	shrl	$6, %ecx
	andl	$3, %ecx
	andl	$63, %esi
	orl	%ecx, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, -166(%rbp)
	movzbl	(%rax,%rsi), %edx
	movb	%dl, -165(%rbp)
	movzbl	-135(%rbp), %ecx
	movzbl	-134(%rbp), %edx
	movzbl	-133(%rbp), %esi
	movl	%ecx, %edi
	sall	$4, %ecx
	shrl	$2, %edi
	andl	$48, %ecx
	movzbl	(%rax,%rdi), %edi
	movb	%dil, -164(%rbp)
	movl	%edx, %edi
	sall	$2, %edx
	shrl	$4, %edi
	andl	$60, %edx
	orl	%edi, %ecx
	movzbl	(%rax,%rcx), %ecx
	movb	%cl, -163(%rbp)
	movl	%esi, %ecx
	andl	$63, %esi
	shrl	$6, %ecx
	andl	$3, %ecx
	orl	%ecx, %edx
	movzbl	-132(%rbp), %ecx
	movzbl	(%rax,%rdx), %edx
	movl	%ecx, %edi
	sall	$4, %ecx
	movb	%dl, -162(%rbp)
	movzbl	(%rax,%rsi), %edx
	shrl	$2, %edi
	andl	$48, %ecx
	movzbl	(%rax,%rdi), %edi
	movzbl	-130(%rbp), %esi
	movb	%dl, -161(%rbp)
	movzbl	-131(%rbp), %edx
	movb	%dil, -160(%rbp)
	movl	%edx, %edi
	sall	$2, %edx
	shrl	$4, %edi
	andl	$60, %edx
	orl	%edi, %ecx
	movzbl	(%rax,%rcx), %ecx
	movb	%cl, -159(%rbp)
	movl	%esi, %ecx
	andl	$63, %esi
	shrl	$6, %ecx
	andl	$3, %ecx
	orl	%ecx, %edx
	movzbl	-129(%rbp), %ecx
	movzbl	(%rax,%rdx), %edx
	movl	%ecx, %edi
	sall	$4, %ecx
	movb	%dl, -158(%rbp)
	movzbl	(%rax,%rsi), %edx
	shrl	$2, %edi
	andl	$48, %ecx
	movzbl	(%rax,%rdi), %edi
	movzbl	-127(%rbp), %esi
	movb	%dl, -157(%rbp)
	movzbl	-128(%rbp), %edx
	movb	%dil, -156(%rbp)
	movl	%edx, %edi
	sall	$2, %edx
	shrl	$4, %edi
	andl	$60, %edx
	orl	%edi, %ecx
	movzbl	(%rax,%rcx), %ecx
	movb	%cl, -155(%rbp)
	movl	%esi, %ecx
	shrl	$6, %ecx
	andl	$63, %esi
	andl	$3, %ecx
	orl	%ecx, %edx
	movzbl	(%rax,%rdx), %edx
	movb	%dl, -154(%rbp)
	movzbl	(%rax,%rsi), %edx
	movb	%dl, -153(%rbp)
	movzbl	-126(%rbp), %edx
	movzbl	-125(%rbp), %ecx
	movq	-224(%rbp), %rdi
	movb	$61, -149(%rbp)
	movl	%edx, %esi
	sall	$4, %edx
	shrl	$2, %esi
	andl	$48, %edx
	movzbl	(%rax,%rsi), %esi
	movb	%sil, -152(%rbp)
	movl	%ecx, %esi
	sall	$2, %ecx
	shrl	$4, %esi
	andl	$60, %ecx
	orl	%esi, %edx
	movzbl	(%rax,%rdx), %edx
	movzbl	(%rax,%rcx), %eax
	movb	%dl, -151(%rbp)
	movb	%al, -150(%rbp)
	cmpq	%r12, %rdi
	je	.L1000
	call	_ZdlPv@PLT
.L1000:
	movdqa	.LC15(%rip), %xmm0
	movl	$32, %eax
	movl	$97, %edi
	movl	$168626701, -181(%rbp)
	movw	%ax, -48(%rbp)
	leaq	-256(%rbp), %r12
	movaps	%xmm0, -144(%rbp)
	movdqa	.LC16(%rip), %xmm0
	movb	$0, -177(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqa	.LC17(%rip), %xmm0
	movq	$0, -240(%rbp)
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC18(%rip), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqa	.LC19(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
	movdqa	.LC20(%rip), %xmm0
	movaps	%xmm0, -64(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -256(%rbp)
	call	_Znwm@PLT
	movzbl	-48(%rbp), %edx
	movdqa	-128(%rbp), %xmm2
	leaq	-148(%rbp), %rcx
	movdqa	-144(%rbp), %xmm1
	movdqa	-112(%rbp), %xmm3
	leaq	97(%rax), %rsi
	movq	%r12, %rdi
	movdqa	-96(%rbp), %xmm4
	movdqa	-80(%rbp), %xmm5
	movb	%dl, 96(%rax)
	leaq	-176(%rbp), %rdx
	movdqa	-64(%rbp), %xmm6
	movups	%xmm1, (%rax)
	movups	%xmm2, 16(%rax)
	movups	%xmm3, 32(%rax)
	movups	%xmm4, 48(%rax)
	movups	%xmm5, 64(%rax)
	movups	%xmm6, 80(%rax)
	movq	%rax, -256(%rbp)
	movq	%rsi, -240(%rbp)
	movq	%rsi, -248(%rbp)
	call	_ZNSt6vectorIcSaIcEE15_M_range_insertIPcEEvN9__gnu_cxx17__normal_iteratorIS3_S1_EET_S7_St20forward_iterator_tag
	movq	-248(%rbp), %rsi
	movq	%r12, %rdi
	leaq	-181(%rbp), %rdx
	leaq	-177(%rbp), %rcx
	call	_ZNSt6vectorIcSaIcEE15_M_range_insertIPKcEEvN9__gnu_cxx17__normal_iteratorIPcS1_EET_S9_St20forward_iterator_tag
	movq	16(%rbx), %rdi
	leaq	_ZN4node9inspector12_GLOBAL__N_112WriteRequest7CleanupEP10uv_write_si(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector9TcpHolder8WriteRawERKSt6vectorIcSaIcEEPFvP10uv_write_siE
	testl	%eax, %eax
	jns	.L1025
	movq	16(%rbx), %rdi
	movq	$0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L1004
	leaq	_ZN4node9inspector9TcpHolder8OnClosedEP11uv_handle_s(%rip), %rsi
	call	uv_close@PLT
.L1004:
	movq	-256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L993
	call	_ZdlPv@PLT
.L993:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1026
	addq	$224, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L997:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1027
	movq	%r12, %rax
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	16(%rbx), %r12
	movl	$64, %edi
	movq	$0, 16(%rbx)
	movq	8(%rbx), %r13
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	leaq	16+_ZTVN4node9inspector15ProtocolHandlerE(%rip), %rbx
	movq	%rbx, (%rax)
	movq	%rdx, 8(%rax)
	movq	%r12, 16(%rax)
	testq	%r12, %r12
	je	.L1028
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_19WsHandlerE(%rip), %rbx
	movq	%rax, 256(%r12)
	movq	%rbx, (%rax)
	leaq	_ZN4node9inspector12_GLOBAL__N_19WsHandler17WaitForCloseReplyEv(%rip), %rbx
	movq	%rbx, 24(%rax)
	leaq	_ZN4node9inspector12_GLOBAL__N_19WsHandler18CloseFrameReceivedEv(%rip), %rbx
	movq	$0, 32(%rax)
	movq	%rbx, 40(%rax)
	movq	$0, 48(%rax)
	movq	0(%r13), %rdi
	movb	$0, 56(%rax)
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L1004
	movq	(%rdi), %rax
	call	*40(%rax)
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1023:
	leaq	-224(%rbp), %rdi
	leaq	-256(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	movq	-256(%rbp), %rax
	movq	%rax, -208(%rbp)
.L996:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-256(%rbp), %r13
	movq	-224(%rbp), %rax
	jmp	.L998
.L1022:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	_ZZN4node9inspector15ProtocolHandlerC4EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1024:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1026:
	call	__stack_chk_fail@PLT
.L1027:
	movq	%r12, %rdi
	jmp	.L996
	.cfi_endproc
.LFE4387:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_:
.LFB5692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %r15
	movq	%rdi, -72(%rbp)
	movq	%rsi, -64(%rbp)
	testq	%r15, %r15
	je	.L1055
	movq	8(%rsi), %r14
	movq	(%rsi), %rbx
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	16(%r15), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L1033
.L1056:
	movq	%rax, %r15
.L1032:
	movq	40(%r15), %r12
	movq	32(%r15), %r13
	cmpq	%r12, %r14
	movq	%r12, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L1034
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %rdx
	testl	%eax, %eax
	jne	.L1035
.L1034:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r12, %rax
	cmpq	%rcx, %rax
	jge	.L1036
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L1037
.L1035:
	testl	%eax, %eax
	js	.L1037
.L1036:
	movq	24(%r15), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L1056
.L1033:
	movq	%r15, %r8
	testb	%sil, %sil
	jne	.L1031
.L1039:
	testq	%rdx, %rdx
	je	.L1042
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L1043
.L1042:
	movq	%r12, %rcx
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L1044
	cmpq	$-2147483648, %rcx
	jl	.L1045
	movl	%ecx, %eax
.L1043:
	testl	%eax, %eax
	js	.L1045
.L1044:
	addq	$40, %rsp
	movq	%r15, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1045:
	.cfi_restore_state
	addq	$40, %rsp
	xorl	%eax, %eax
	movq	%r8, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1055:
	.cfi_restore_state
	leaq	8(%rdi), %r15
.L1031:
	movq	-72(%rbp), %rax
	cmpq	24(%rax), %r15
	je	.L1057
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	-64(%rbp), %rbx
	movq	%r15, %r8
	movq	40(%rax), %r12
	movq	32(%rax), %r13
	movq	%rax, %r15
	movq	8(%rbx), %r14
	movq	(%rbx), %rbx
	cmpq	%r14, %r12
	movq	%r14, %rdx
	cmovbe	%r12, %rdx
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1057:
	addq	$40, %rsp
	movq	%r15, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5692:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_:
.LFB5500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	cmpq	%rax, %rsi
	je	.L1102
	movq	8(%rdx), %r14
	movq	40(%rsi), %r15
	movq	%rsi, %rbx
	movq	32(%rsi), %r9
	movq	0(%r13), %r8
	cmpq	%r15, %r14
	movq	%r15, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L1065
	movq	%r9, %rsi
	movq	%r8, %rdi
	movq	%rdx, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %r9
	testl	%eax, %eax
	movq	-72(%rbp), %rdx
	jne	.L1103
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$2147483647, %rax
	jg	.L1084
.L1085:
	cmpq	$-2147483648, %rax
	jl	.L1068
	testl	%eax, %eax
	js	.L1068
	testq	%rdx, %rdx
	je	.L1075
.L1084:
	movq	%r8, %rsi
	movq	%r9, %rdi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L1076
.L1075:
	subq	%r14, %r15
	cmpq	$2147483647, %r15
	jg	.L1077
	cmpq	$-2147483648, %r15
	jl	.L1078
	movl	%r15d, %eax
.L1076:
	testl	%eax, %eax
	js	.L1078
.L1077:
	movq	%rbx, %rax
	xorl	%edx, %edx
.L1094:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1065:
	.cfi_restore_state
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$2147483647, %rax
	jle	.L1085
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1103:
	jns	.L1084
.L1068:
	movq	%rbx, %rax
	movq	%rbx, %rdx
	cmpq	%rbx, 24(%r12)
	je	.L1094
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %rcx
	movq	%rax, %r15
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L1071
	movq	-56(%rbp), %r8
	movq	32(%rax), %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, %rsi
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1072
.L1071:
	subq	%r14, %rcx
	cmpq	$2147483647, %rcx
	jg	.L1060
	cmpq	$-2147483648, %rcx
	jl	.L1073
	movl	%ecx, %eax
.L1072:
	testl	%eax, %eax
	jns	.L1060
.L1073:
	cmpq	$0, 24(%r15)
	movl	$0, %eax
	cmovne	%rbx, %rax
	cmove	%r15, %rbx
	addq	$40, %rsp
	movq	%rbx, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1102:
	.cfi_restore_state
	cmpq	$0, 40(%rdi)
	je	.L1060
	movq	32(%rdi), %rbx
	movq	8(%rdx), %r15
	movq	40(%rbx), %r14
	movq	%r15, %rdx
	cmpq	%r15, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L1061
	movq	32(%rbx), %rdi
	movq	0(%r13), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1062
.L1061:
	subq	%r15, %r14
	cmpq	$2147483647, %r14
	jg	.L1060
	cmpq	$-2147483648, %r14
	jl	.L1101
	movl	%r14d, %eax
.L1062:
	testl	%eax, %eax
	jns	.L1060
.L1101:
	addq	$40, %rsp
	movq	%rbx, %rdx
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1060:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE24_M_get_insert_unique_posERS7_
	.p2align 4,,10
	.p2align 3
.L1078:
	.cfi_restore_state
	cmpq	%rbx, 32(%r12)
	je	.L1101
	movq	%rbx, %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base@PLT
	movq	40(%rax), %rcx
	movq	%rax, %r15
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L1080
	movq	-56(%rbp), %r8
	movq	32(%rax), %rsi
	movq	%rcx, -64(%rbp)
	movq	%r8, %rdi
	call	memcmp@PLT
	movq	-64(%rbp), %rcx
	testl	%eax, %eax
	jne	.L1081
.L1080:
	subq	%rcx, %r14
	cmpq	$2147483647, %r14
	jg	.L1060
	cmpq	$-2147483648, %r14
	jl	.L1082
	movl	%r14d, %eax
.L1081:
	testl	%eax, %eax
	jns	.L1060
.L1082:
	cmpq	$0, 24(%rbx)
	movl	$0, %eax
	cmovne	%r15, %rbx
	cmovne	%r15, %rax
	movq	%rbx, %rdx
	jmp	.L1094
	.cfi_endproc
.LFE5500:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderValueEP18llhttp__internal_sPKcm, @function
_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderValueEP18llhttp__internal_sPKcm:
.LFB4397:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-32(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	240(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -96(%rbp)
	movq	280(%r15), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, 24(%r15)
	testq	%rbx, %rbx
	je	.L1130
	movq	240(%r15), %r8
	movq	232(%r15), %r11
	movq	%r13, %r12
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1107
.L1106:
	movq	40(%rbx), %r10
	movq	%r8, %rdx
	cmpq	%r8, %r10
	cmovbe	%r10, %rdx
	testq	%rdx, %rdx
	je	.L1108
	movq	32(%rbx), %rdi
	movq	%r11, %rsi
	movq	%r8, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	call	memcmp@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r10
	testl	%eax, %eax
	movq	-88(%rbp), %r8
	jne	.L1109
.L1108:
	movq	%r10, %rax
	movl	$2147483648, %ecx
	subq	%r8, %rax
	cmpq	%rcx, %rax
	jge	.L1110
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L1111
.L1109:
	testl	%eax, %eax
	js	.L1111
.L1110:
	movq	%rbx, %r12
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L1106
.L1107:
	cmpq	%r12, %r13
	je	.L1105
	movq	40(%r12), %rbx
	cmpq	%rbx, %r8
	movq	%rbx, %rdx
	cmovbe	%r8, %rdx
	testq	%rdx, %rdx
	je	.L1113
	movq	32(%r12), %rsi
	movq	%r11, %rdi
	movq	%r8, -72(%rbp)
	call	memcmp@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	jne	.L1114
.L1113:
	subq	%rbx, %r8
	cmpq	$2147483647, %r8
	jg	.L1115
	cmpq	$-2147483648, %r8
	jl	.L1105
	movl	%r8d, %eax
.L1114:
	testl	%eax, %eax
	js	.L1105
.L1115:
	movabsq	$4611686018427387903, %rax
	subq	72(%r12), %rax
	leaq	64(%r12), %rdi
	cmpq	%rax, %r14
	ja	.L1151
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1152
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1130:
	.cfi_restore_state
	movq	%r13, %r12
.L1105:
	movl	$96, %edi
	movq	%r12, -72(%rbp)
	call	_Znwm@PLT
	movq	232(%r15), %r11
	movq	240(%r15), %r8
	leaq	48(%rax), %rbx
	movq	%rax, %r12
	leaq	32(%rax), %r10
	movq	%rbx, 32(%rax)
	movq	%r11, %rax
	addq	%r8, %rax
	je	.L1116
	testq	%r11, %r11
	je	.L1153
.L1116:
	movq	%r8, -64(%rbp)
	cmpq	$15, %r8
	ja	.L1154
	cmpq	$1, %r8
	jne	.L1119
	movzbl	(%r11), %eax
	movb	%al, 48(%r12)
	movq	%rbx, %rax
.L1120:
	leaq	80(%r12), %rcx
	movq	%r8, 40(%r12)
	movq	-72(%rbp), %rsi
	movq	%r10, %rdx
	movb	$0, (%rax,%r8)
	leaq	264(%r15), %rdi
	movq	%rcx, 64(%r12)
	movq	$0, 72(%r12)
	movb	$0, 80(%r12)
	movq	%rcx, -80(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_S5_ESt10_Select1stIS8_ESt4lessIS5_ESaIS8_EE29_M_get_insert_hint_unique_posESt23_Rb_tree_const_iteratorIS8_ERS7_
	movq	-80(%rbp), %rcx
	testq	%rdx, %rdx
	movq	%rdx, %r8
	je	.L1121
	cmpq	%rdx, %r13
	je	.L1134
	testq	%rax, %rax
	je	.L1155
.L1134:
	movl	$1, %edi
.L1122:
	movq	%r13, %rcx
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 304(%r15)
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1119:
	testq	%r8, %r8
	jne	.L1156
	movq	%rbx, %rax
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1154:
	movq	%r10, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -104(%rbp)
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r11
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %r8
	movq	%rax, 48(%r12)
.L1118:
	movq	%r8, %rdx
	movq	%r11, %rsi
	movq	%r10, -80(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	32(%r12), %rax
	movq	-80(%rbp), %r10
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1121:
	movq	64(%r12), %rdi
	cmpq	%rdi, %rcx
	je	.L1126
	movq	%rax, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
.L1126:
	movq	32(%r12), %rdi
	cmpq	%rdi, %rbx
	je	.L1127
	movq	%rax, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
.L1127:
	movq	%r12, %rdi
	movq	%rax, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rax
	movq	%rax, %r12
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	40(%r12), %rbx
	movq	40(%rdx), %rcx
	cmpq	%rcx, %rbx
	movq	%rcx, %rdx
	cmovbe	%rbx, %rdx
	testq	%rdx, %rdx
	je	.L1123
	movq	32(%r8), %rsi
	movq	32(%r12), %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcmp@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %edi
	je	.L1123
.L1124:
	shrl	$31, %edi
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1123:
	subq	%rcx, %rbx
	xorl	%edi, %edi
	cmpq	$2147483647, %rbx
	jg	.L1122
	cmpq	$-2147483648, %rbx
	jl	.L1134
	movl	%ebx, %edi
	jmp	.L1124
.L1153:
	leaq	.LC9(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1151:
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1152:
	call	__stack_chk_fail@PLT
.L1156:
	movq	%rbx, %rdi
	jmp	.L1118
	.cfi_endproc
.LFE4397:
	.size	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderValueEP18llhttp__internal_sPKcm, .-_ZN4node9inspector12_GLOBAL__N_111HttpHandler13OnHeaderValueEP18llhttp__internal_sPKcm
	.weak	_ZTVN4node9inspector15ProtocolHandlerE
	.section	.data.rel.ro._ZTVN4node9inspector15ProtocolHandlerE,"awG",@progbits,_ZTVN4node9inspector15ProtocolHandlerE,comdat
	.align 8
	.type	_ZTVN4node9inspector15ProtocolHandlerE, @object
	.size	_ZTVN4node9inspector15ProtocolHandlerE, 80
_ZTVN4node9inspector15ProtocolHandlerE:
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	0
	.quad	0
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_19WsHandlerE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_19WsHandlerE, 80
_ZTVN4node9inspector12_GLOBAL__N_19WsHandlerE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandler6OnDataEPSt6vectorIcSaIcEE
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandler5OnEofEv
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandler5WriteESt6vectorIcSaIcEE
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandler15CancelHandshakeEv
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandler8ShutdownEv
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandlerD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_19WsHandlerD0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_111HttpHandlerE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_111HttpHandlerE, 80
_ZTVN4node9inspector12_GLOBAL__N_111HttpHandlerE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandler13AcceptUpgradeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandler6OnDataEPSt6vectorIcSaIcEE
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandler5OnEofEv
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandler5WriteESt6vectorIcSaIcEE
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandler15CancelHandshakeEv
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandler8ShutdownEv
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111HttpHandlerD0Ev
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"../src/inspector_socket.cc:606"
	.section	.rodata.str1.1
.LC22:
	.string	"(tcp_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"node::inspector::ProtocolHandler::ProtocolHandler(node::inspector::InspectorSocket*, node::inspector::TcpHolder::Pointer)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector15ProtocolHandlerC4EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEEE4args, @object
	.size	_ZZN4node9inspector15ProtocolHandlerC4EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEEE4args, 24
_ZZN4node9inspector15ProtocolHandlerC4EPNS0_15InspectorSocketESt10unique_ptrINS0_9TcpHolderENS_15FunctionDeleterIS5_XadL_ZNS5_20DisconnectAndDisposeEPS5_EEEEEE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata
	.align 32
	.type	_ZZN4node9inspector12_GLOBAL__N_1L22generate_accept_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPA28_cE8ws_magic, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_1L22generate_accept_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPA28_cE8ws_magic, 37
_ZZN4node9inspector12_GLOBAL__N_1L22generate_accept_stringERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPA28_cE8ws_magic:
	.string	"258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
	.type	_ZN4node9inspector12_GLOBAL__N_1L11CLOSE_FRAMEE, @object
	.size	_ZN4node9inspector12_GLOBAL__N_1L11CLOSE_FRAMEE, 2
_ZN4node9inspector12_GLOBAL__N_1L11CLOSE_FRAMEE:
	.string	"\210"
	.align 32
	.type	_ZZN4nodeL13base64_encodeEPKcmPcmE5table, @object
	.size	_ZZN4nodeL13base64_encodeEPKcmPcmE5table, 65
_ZZN4nodeL13base64_encodeEPKcmPcmE5table:
	.string	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	3471766442030158920
	.quad	7233135182548579360
	.align 16
.LC2:
	.quad	8391162085809410592
	.quad	7954892334481738253
	.align 16
.LC3:
	.quad	2322280091609214324
	.quad	7887043400459380084
	.align 16
.LC4:
	.quad	8318818562674277228
	.quad	4047968968097231973
	.align 16
.LC5:
	.quad	6008476277370849805
	.quad	8223699863333462895
	.align 16
.LC6:
	.quad	8583988928740421989
	.quad	7162254444797653857
	.align 16
.LC14:
	.quad	6008476277963711827
	.quad	7298977599025013615
	.align 16
.LC15:
	.quad	3543824036068086856
	.quad	7599634293940302112
	.align 16
.LC16:
	.quad	5773728446201488244
	.quad	8317145084708810610
	.align 16
.LC17:
	.quad	7233188265125546509
	.quad	8030870748461546085
	.align 16
.LC18:
	.quad	8017262814553271139
	.quad	7957695015191670382
	.align 16
.LC19:
	.quad	7233188265125552186
	.quad	6281786342087724389
	.align 16
.LC20:
	.quad	8387227955861086821
	.quad	4212115132259713325
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
