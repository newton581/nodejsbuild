	.file	"string_decoder.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"kIncompleteCharactersStart"
.LC1:
	.string	"kIncompleteCharactersEnd"
.LC2:
	.string	"kMissingBytes"
.LC3:
	.string	"kBufferedBytes"
.LC4:
	.string	"kEncodingField"
.LC5:
	.string	"kNumFields"
.LC6:
	.string	"ascii"
.LC7:
	.string	"utf8"
.LC8:
	.string	"base64"
.LC9:
	.string	"utf16le"
.LC10:
	.string	"hex"
.LC11:
	.string	"buffer"
.LC12:
	.string	"latin1"
.LC13:
	.string	"encodings"
.LC14:
	.string	"kSize"
.LC15:
	.string	"decode"
.LC16:
	.string	"flush"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB17:
	.text
.LHOTB17:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L2
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L2
	movq	271(%rax), %rbx
	xorl	%esi, %esi
	movq	352(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$26, %ecx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L44
.L3:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L45
.L4:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$24, %ecx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L46
.L5:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L47
.L6:
	movl	$4, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$13, %ecx
	movq	%r12, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L48
.L7:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L49
.L8:
	movl	$5, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L50
.L9:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L51
.L10:
	movl	$6, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%r12, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L52
.L11:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L53
.L12:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r12, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L54
.L13:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L55
.L14:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movl	$5, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L56
.L15:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L57
.L16:
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L58
.L17:
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L59
.L18:
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L60
.L19:
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L61
.L20:
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L62
.L21:
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L63
.L22:
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L64
.L23:
	movl	$5, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L65
.L24:
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L66
.L25:
	movl	$6, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L67
.L26:
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L68
.L27:
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L69
.L28:
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L70
.L29:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L71
.L30:
	movl	$7, %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%edx, %edx
	movl	$5, %ecx
	movq	%r12, %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L72
.L31:
	movq	%r15, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L73
.L32:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r13
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L74
.L33:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L75
.L34:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L76
.L35:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L77
.L36:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L78
.L37:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L79
.L38:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L45:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L49:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L51:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L53:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L55:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L57:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L59:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L61:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L63:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L65:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L67:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L68:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L69:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L71:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L73:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L74:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L75:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L76:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L77:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L78:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L79:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L38
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7096:
.L2:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7096:
	.text
	.size	_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE17:
	.text
.LHOTE17:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -40
	movl	16(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jg	.L81
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L109
.L83:
	movq	(%r12), %rax
	movzbl	6(%rbx), %ecx
	movq	8(%rax), %r13
	movl	%ecx, %eax
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L100
	testl	%ecx, %ecx
	jne	.L84
.L100:
	cmpb	$0, 4(%rbx)
	jne	.L110
	cmpb	$0, 5(%rbx)
	jne	.L111
.L87:
	leaq	128(%r13), %rax
.L91:
	testq	%rax, %rax
	je	.L80
	movq	(%rax), %rdx
	movq	(%r12), %rax
	movq	%rdx, 24(%rax)
.L80:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L83
.L109:
	leaq	_ZZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	movzbl	5(%rbx), %edx
	cmpl	$3, %ecx
	jne	.L88
	testb	$1, %dl
	je	.L89
	subl	$1, %edx
	subb	$1, 4(%rbx)
	movb	%dl, 5(%rbx)
.L89:
	testb	%dl, %dl
	je	.L87
	movq	$0, -48(%rbp)
	movzbl	%dl, %edx
.L97:
	leaq	-48(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L113
.L93:
	xorl	%edx, %edx
	movw	%dx, 4(%rbx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	testb	%dl, %dl
	je	.L87
	movq	$0, -48(%rbp)
	cmpl	$1, %ecx
	jne	.L97
	movzbl	%dl, %ecx
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L113:
	cmpq	$0, -48(%rbp)
	je	.L114
	movq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%ecx, %ecx
	movw	%cx, 4(%rbx)
	jmp	.L80
.L114:
	leaq	_ZZN4node12_GLOBAL__N_110MakeStringEPN2v87IsolateEPKcmNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7095:
	.size	_ZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPm
	.type	_ZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPm, @function
_ZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPm:
.LFB7092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rcx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	(%rcx), %r13
	movzbl	6(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leal	-1(%rcx), %edx
	cmpb	$2, %dl
	ja	.L116
	movzbl	4(%rdi), %esi
	movq	%rdi, %rbx
	movq	%rsi, %rdx
	testl	%esi, %esi
	jne	.L117
	testq	%r13, %r13
	je	.L118
.L213:
	xorl	%r14d, %r14d
	cmpl	$1, %ecx
	je	.L214
.L136:
	cmpl	$3, %ecx
	je	.L215
	cmpl	$2, %ecx
	jne	.L154
	movabsq	$-6148914691236517205, %rdx
	movq	%r13, %rax
	mulq	%rdx
	movq	%rdx, %rax
	andq	$-2, %rdx
	shrq	%rax
	addq	%rax, %rdx
	movq	%r13, %rax
	subq	%rdx, %rax
	movb	%al, 5(%rbx)
	movzbl	%al, %edx
	jne	.L216
.L155:
	movq	$0, -72(%rbp)
	cmpl	$1, %ecx
	jne	.L161
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L117:
	movzbl	5(%rdi), %edi
	addl	%edi, %esi
	movq	%rdi, %rax
	cmpl	$4, %esi
	ja	.L217
	cmpl	$1, %ecx
	je	.L218
.L121:
	cmpq	%rdx, %r13
	movzbl	%al, %eax
	cmovbe	%r13, %rdx
	addq	%rbx, %rax
	cmpl	$8, %edx
	jnb	.L125
	testb	$4, %dl
	jne	.L219
	testl	%edx, %edx
	je	.L126
	movzbl	(%r12), %ecx
	movb	%cl, (%rax)
	testb	$2, %dl
	jne	.L220
.L126:
	movzbl	4(%rbx), %eax
	addq	%rdx, %r12
	subq	%rdx, %r13
	subl	%edx, %eax
	addb	5(%rbx), %dl
	movb	%al, 4(%rbx)
	movb	%dl, 5(%rbx)
	testb	%al, %al
	jne	.L221
	movzbl	6(%rbx), %ecx
	movq	$0, -80(%rbp)
	movq	%r9, -88(%rbp)
	cmpl	$1, %ecx
	je	.L222
	movzbl	%dl, %edx
	leaq	-80(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L223
.L132:
	movzbl	5(%rbx), %eax
	addq	%rax, (%r9)
	movb	$0, 5(%rbx)
	testq	%r13, %r13
	je	.L178
	movzbl	6(%rbx), %ecx
	cmpl	$1, %ecx
	jne	.L136
.L214:
	leaq	-1(%r13), %rax
	movzbl	5(%rbx), %edx
	leaq	(%r12,%rax), %rcx
	cmpb	$0, (%rcx)
	js	.L224
.L138:
	testb	%dl, %dl
	jne	.L151
	.p2align 4,,10
	.p2align 3
.L210:
	movq	$0, -72(%rbp)
.L170:
	xorl	%edx, %edx
	movl	%r13d, %ecx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L162
	xorl	%eax, %eax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L116:
	movl	%ecx, %eax
	subl	$4, %eax
	cmpb	$1, %al
	jbe	.L179
	testl	%ecx, %ecx
	jne	.L167
.L179:
	leaq	-64(%rbp), %r8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	$0, -64(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L225
.L169:
	movq	%rbx, %rax
.L166:
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L226
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	(%r12), %rcx
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	movq	%rcx, (%rax)
	movl	%edx, %ecx
	movq	-8(%r12,%rcx), %rsi
	movq	%rsi, -8(%rax,%rcx)
	subq	%rdi, %rax
	movq	%r12, %rsi
	subq	%rax, %rsi
	addl	%edx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L215:
	testb	$1, %r13b
	je	.L150
	movl	$257, %edx
	movw	%dx, 4(%rbx)
	movl	$1, %edx
.L151:
	subq	%rdx, (%r9)
	movzbl	5(%rbx), %ecx
	subq	%rdx, %r13
	leaq	(%r12,%r13), %rsi
	cmpl	$8, %ecx
	jnb	.L156
	testb	$4, %cl
	jne	.L227
	testl	%ecx, %ecx
	je	.L157
	movzbl	(%rsi), %eax
	movb	%al, (%rbx)
	testb	$2, %cl
	jne	.L228
.L157:
	testq	%r13, %r13
	jne	.L229
	leaq	128(%r15), %rdx
.L162:
	testq	%r14, %r14
	je	.L135
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L118:
	leaq	128(%r15), %rdx
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rdx, %rax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L156:
	movq	(%rsi), %rax
	leaq	8(%rbx), %rdi
	andq	$-8, %rdi
	movq	%rax, (%rbx)
	movq	-8(%rcx,%rsi), %rax
	movq	%rax, -8(%rbx,%rcx)
	movq	%rbx, %rax
	subq	%rdi, %rax
	addl	%eax, %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L150:
	movsbl	-1(%r12,%r13), %eax
	andl	$252, %eax
	cmpl	$216, %eax
	je	.L152
.L154:
	movzbl	5(%rbx), %edx
.L153:
	testb	%dl, %dl
	jne	.L151
	movq	$0, -72(%rbp)
.L161:
	movq	%r13, %rdx
	leaq	-72(%rbp), %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L162
	cmpq	$0, -72(%rbp)
	je	.L164
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%eax, %eax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L218:
	xorl	%r14d, %r14d
	testq	%r13, %r13
	jne	.L122
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L123:
	addq	$1, %r14
	cmpq	%r14, %r13
	je	.L121
	cmpq	%rdx, %r14
	je	.L121
.L122:
	movsbl	(%r12,%r14), %esi
	leaq	(%r12,%r14), %r8
	andl	$192, %esi
	cmpl	$128, %esi
	je	.L123
	movb	$0, 4(%rbx)
	movq	%r14, %rdx
	movq	%r12, %rsi
	leaq	(%rbx,%rax), %rdi
	movq	%r9, -96(%rbp)
	subq	%r14, %r13
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movzbl	5(%rbx), %eax
	movq	-88(%rbp), %r8
	movzbl	4(%rbx), %edx
	movq	-96(%rbp), %r9
	addl	%r14d, %eax
	movq	%r8, %r12
	movb	%al, 5(%rbx)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L222:
	movzbl	%dl, %ecx
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-88(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L132
	xorl	%eax, %eax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L224:
	addl	$1, %edx
	movb	%dl, 5(%rbx)
	movsbl	(%rcx), %ecx
	movl	%ecx, %esi
	andl	$192, %esi
	cmpl	$128, %esi
	jne	.L141
	testq	%rax, %rax
	je	.L144
	cmpb	$3, %dl
	jbe	.L142
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L149:
	cmpb	$3, %dl
	ja	.L144
	testq	%rax, %rax
	je	.L144
.L142:
	subq	$1, %rax
	addl	$1, %edx
	movb	%dl, 5(%rbx)
	movsbl	(%r12,%rax), %ecx
	movl	%ecx, %esi
	andl	$192, %esi
	cmpl	$128, %esi
	je	.L149
.L141:
	movl	%ecx, %eax
	andl	$224, %eax
	cmpl	$192, %eax
	je	.L176
	movl	%ecx, %eax
	andl	$240, %eax
	cmpl	$224, %eax
	je	.L177
	andl	$248, %ecx
	movl	$4, %eax
	cmpl	$240, %ecx
	jne	.L144
.L147:
	cmpb	%al, %dl
	jnb	.L148
	movzbl	5(%rbx), %edx
	subl	%edx, %eax
	movb	%al, 4(%rbx)
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L223:
	cmpq	$0, -80(%rbp)
	je	.L164
	movq	-80(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	xorl	%eax, %eax
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L225:
	cmpq	$0, -64(%rbp)
	je	.L164
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L144:
	movb	$0, 5(%rbx)
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L216:
	movl	$3, %esi
	subl	%eax, %esi
	movb	%sil, 4(%rbx)
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$514, %eax
	movl	$2, %edx
	movw	%ax, 4(%rbx)
	jmp	.L151
.L167:
	leaq	_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	leaq	_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	testq	%r13, %r13
	je	.L118
	movzbl	6(%rbx), %ecx
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%r14, %rdx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L227:
	movl	(%rsi), %eax
	movl	%eax, (%rbx)
	movl	-4(%rcx,%rsi), %eax
	movl	%eax, -4(%rbx,%rcx)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L219:
	movl	(%r12), %ecx
	movl	%ecx, (%rax)
	movl	%edx, %ecx
	movl	-4(%r12,%rcx), %esi
	movl	%esi, -4(%rax,%rcx)
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	_ZZN4node12_GLOBAL__N_110MakeStringEPN2v87IsolateEPKcmNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L148:
	xorl	%ecx, %ecx
	movw	%cx, 4(%rbx)
	jmp	.L210
.L176:
	movl	$2, %eax
	jmp	.L147
.L228:
	movzwl	-2(%rcx,%rsi), %eax
	movw	%ax, -2(%rbx,%rcx)
	jmp	.L157
.L220:
	movl	%edx, %ecx
	movzwl	-2(%r12,%rcx), %esi
	movw	%si, -2(%rax,%rcx)
	jmp	.L126
.L177:
	movl	$3, %eax
	jmp	.L147
.L226:
	call	__stack_chk_fail@PLT
.L229:
	movzbl	6(%rbx), %ecx
	jmp	.L155
	.cfi_endproc
.LFE7092:
	.size	_ZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPm, .-_ZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPm
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L231
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L246
.L233:
	cmpl	$1, 16(%rbx)
	jle	.L247
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L235:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L248
	cmpl	$1, 16(%rbx)
	jg	.L237
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L238:
	movq	%r12, %rdi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	ja	.L241
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L249
.L241:
	movq	%r12, %rdi
	leaq	-208(%rbp), %r15
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %r14
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	leaq	(%r14,%rax), %rdx
	movq	%rdx, -80(%rbp)
.L240:
	movq	-72(%rbp), %rax
	movq	%r15, %rcx
	movq	%r13, %rdi
	movq	%rax, -208(%rbp)
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	call	_ZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPm
	testq	%rax, %rax
	je	.L230
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L230:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L250
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L233
.L246:
	leaq	_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L237:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r12
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L249:
	leaq	-144(%rbp), %r14
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	-208(%rbp), %r15
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -80(%rbp)
	movq	%r14, %rdx
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L248:
	leaq	_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L250:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7094:
	.size	_ZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13StringDecoder9FlushDataEPN2v87IsolateE
	.type	_ZN4node13StringDecoder9FlushDataEPN2v87IsolateE, @function
_ZN4node13StringDecoder9FlushDataEPN2v87IsolateE:
.LFB7093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movzbl	6(%rdi), %ecx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%ecx, %edx
	subl	$4, %edx
	cmpb	$1, %dl
	jbe	.L267
	testl	%ecx, %ecx
	jne	.L252
.L267:
	cmpb	$0, 4(%rbx)
	jne	.L277
	cmpb	$0, 5(%rbx)
	jne	.L278
.L255:
	leaq	128(%r13), %rax
.L259:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L279
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	movzbl	5(%rdi), %edx
	cmpl	$3, %ecx
	jne	.L256
	testb	$1, %dl
	je	.L257
	subl	$1, %edx
	subb	$1, 4(%rdi)
	movb	%dl, 5(%rdi)
.L257:
	testb	%dl, %dl
	je	.L255
	movq	$0, -48(%rbp)
	movzbl	%dl, %edx
.L264:
	leaq	-48(%rbp), %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L280
.L261:
	xorl	%eax, %eax
	movw	%ax, 4(%rbx)
	movq	%r12, %rax
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L256:
	testb	%dl, %dl
	je	.L255
	movq	$0, -48(%rbp)
	cmpl	$1, %ecx
	jne	.L264
	movzbl	%dl, %ecx
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	cmpq	$0, -48(%rbp)
	je	.L281
	movq	-48(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L261
.L281:
	leaq	_ZZN4node12_GLOBAL__N_110MakeStringEPN2v87IsolateEPKcmNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7093:
	.size	_ZN4node13StringDecoder9FlushDataEPN2v87IsolateE, .-_ZN4node13StringDecoder9FlushDataEPN2v87IsolateE
	.p2align 4
	.globl	_Z24_register_string_decoderv
	.type	_Z24_register_string_decoderv, @function
_Z24_register_string_decoderv:
.LFB7097:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7097:
	.size	_Z24_register_string_decoderv, .-_Z24_register_string_decoderv
	.section	.rodata.str1.1
.LC18:
	.string	"../src/string_decoder.cc"
.LC19:
	.string	"string_decoder"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC18
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_123InitializeStringDecoderEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC19
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC20:
	.string	"../src/string_decoder.cc:273"
.LC21:
	.string	"(decoder) != nullptr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC22:
	.string	"void node::{anonymous}::FlushData(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_19FlushDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.section	.rodata.str1.1
.LC23:
	.string	"../src/string_decoder.cc:260"
.LC24:
	.string	"args[1]->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"void node::{anonymous}::DecodeData(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.section	.rodata.str1.1
.LC26:
	.string	"../src/string_decoder.cc:258"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_110DecodeDataERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC26
	.quad	.LC21
	.quad	.LC25
	.section	.rodata.str1.1
.LC27:
	.string	"../src/string_decoder.cc:229"
.LC28:
	.string	"(BufferedBytes()) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"v8::MaybeLocal<v8::String> node::StringDecoder::FlushData(v8::Isolate*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args_0, @object
	.size	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args_0, 24
_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args_0:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/string_decoder.cc:228"
.LC31:
	.string	"(MissingBytes()) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args, @object
	.size	_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args, 24
_ZZN4node13StringDecoder9FlushDataEPN2v87IsolateEE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC29
	.section	.rodata.str1.1
.LC32:
	.string	"../src/string_decoder.cc:221"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"Encoding() == ASCII || Encoding() == HEX || Encoding() == LATIN1"
	.align 8
.LC34:
	.string	"v8::MaybeLocal<v8::String> node::StringDecoder::DecodeData(v8::Isolate*, const char*, size_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args_0, @object
	.size	_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args_0, 24
_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args_0:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"../src/string_decoder.cc:72"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"(MissingBytes() + BufferedBytes()) <= (kIncompleteCharactersEnd)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args, @object
	.size	_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args, 24
_ZZN4node13StringDecoder10DecodeDataEPN2v87IsolateEPKcPmE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC34
	.section	.rodata.str1.1
.LC37:
	.string	"../src/string_decoder.cc:47"
.LC38:
	.string	"!error.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"v8::MaybeLocal<v8::String> node::{anonymous}::MakeString(v8::Isolate*, const char*, size_t, node::encoding)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_110MakeStringEPN2v87IsolateEPKcmNS_8encodingEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_110MakeStringEPN2v87IsolateEPKcmNS_8encodingEE4args, 24
_ZZN4node12_GLOBAL__N_110MakeStringEPN2v87IsolateEPKcmNS_8encodingEE4args:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
