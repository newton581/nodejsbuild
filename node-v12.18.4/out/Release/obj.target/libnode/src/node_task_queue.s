	.file	"node_task_queue.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4391:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4391:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4392:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4392:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.text
	.p2align 4
	.type	_ZN4node10task_queueL13RunMicrotasksERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10task_queueL13RunMicrotasksERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7611:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	jmp	_ZN2v815MicrotasksScope17PerformCheckpointEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7611:
	.size	_ZN4node10task_queueL13RunMicrotasksERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10task_queueL13RunMicrotasksERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L14
	cmpw	$1040, %cx
	jne	.L6
.L14:
	movq	23(%rdx), %rax
.L8:
	movl	16(%rbx), %edx
	movq	352(%rax), %r12
	testl	%edx, %edx
	jg	.L9
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L16
.L11:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L17
	movq	8(%rbx), %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate16EnqueueMicrotaskENS_5LocalINS_8FunctionEEE@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	_ZN2v87Isolate16EnqueueMicrotaskENS_5LocalINS_8FunctionEEE@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L11
.L16:
	leaq	_ZZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L8
	.cfi_endproc
.LFE7610:
	.size	_ZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7612:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L29
	cmpw	$1040, %cx
	jne	.L19
.L29:
	movq	23(%rdx), %r12
.L21:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L22
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L37
.L24:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L38
	movq	8(%rbx), %r13
.L26:
	movq	3008(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L27
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3008(%r12)
.L27:
	testq	%r13, %r13
	je	.L18
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3008(%r12)
.L18:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L22:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L24
.L37:
	leaq	_ZZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L21
	.cfi_endproc
.LFE7612:
	.size	_ZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7615:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L50
	cmpw	$1040, %cx
	jne	.L40
.L50:
	movq	23(%rdx), %r12
.L42:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L43
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L58
.L45:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L59
	movq	8(%rbx), %r13
.L47:
	movq	2984(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L48
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2984(%r12)
.L48:
	testq	%r13, %r13
	je	.L39
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2984(%r12)
.L39:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L43:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L45
.L58:
	leaq	_ZZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L42
	.cfi_endproc
.LFE7615:
	.size	_ZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"enqueueMicrotask"
.LC1:
	.string	"setTickCallback"
.LC2:
	.string	"runMicrotasks"
.LC3:
	.string	"tickInfo"
.LC4:
	.string	"kPromiseRejectWithNoHandler"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"kPromiseHandlerAddedAfterReject"
	.section	.rodata.str1.1
.LC8:
	.string	"kPromiseResolveAfterResolved"
.LC10:
	.string	"kPromiseRejectAfterResolved"
.LC12:
	.string	"promiseRejectEvents"
.LC13:
	.string	"setPromiseRejectCallback"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB14:
	.text
.LHOTB14:
	.p2align 4
	.type	_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L61
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L61
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L61
	movq	271(%rax), %rbx
	movq	352(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L94
.L62:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L95
.L63:
	movq	-56(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L96
.L64:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L97
.L65:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L98
.L66:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L99
.L67:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node10task_queueL13RunMicrotasksERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L100
.L68:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L101
.L69:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L102
.L70:
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	1368(%rbx), %r12
	testq	%r12, %r12
	je	.L71
	movq	(%r12), %rsi
	movq	1336(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L71:
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L103
.L72:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L104
.L73:
	movq	%r14, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L105
.L74:
	movq	%r15, %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L106
.L75:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L107
.L76:
	movsd	.LC7(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L108
.L77:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L109
.L78:
	movsd	.LC9(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L110
.L79:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L111
.L80:
	movsd	.LC11(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L112
.L81:
	xorl	%edx, %edx
	movl	$19, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L113
.L82:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L114
.L83:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L115
.L84:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L116
.L85:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L117
.L86:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L95:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L96:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L97:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L99:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L100:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L102:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L104:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L106:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L108:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L110:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L112:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L115:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L116:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L117:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L86
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7616:
.L61:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7616:
	.text
	.size	_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE14:
	.text
.LHOTE14:
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"node,node.promises,node.promises.rejections"
	.section	.rodata.str1.1
.LC16:
	.string	"unhandled"
.LC17:
	.string	"handledAfter"
.LC18:
	.string	"rejections"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Exception in PromiseRejectCallback:\n"
	.text
	.p2align 4
	.globl	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE
	.type	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE, @function
_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE:
.LFB7613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	32(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v86Object10GetIsolateEv@PLT
	movl	24(%rbp), %r14d
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L118
	leaq	-192(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L122
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L122
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L122
	movq	271(%rax), %r15
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r15, %r15
	je	.L118
	movq	2984(%r15), %rax
	movq	%rax, -208(%rbp)
	testq	%rax, %rax
	je	.L184
	movl	%r14d, %eax
	pxor	%xmm0, %xmm0
	movq	352(%r15), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, -216(%rbp)
	testl	%r14d, %r14d
	je	.L185
	cmpl	$1, %r14d
	je	.L186
	cmpl	$3, %r14d
	je	.L183
	cmpl	$2, %r14d
	je	.L183
	.p2align 4,,10
	.p2align 3
.L118:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	lock addq	$1, _ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE19unhandledRejections(%rip)
	movq	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic75(%rip), %rdx
	testq	%rdx, %rdx
	je	.L188
.L129:
	leaq	88(%r12), %r11
	leaq	-96(%rbp), %r14
	testb	$5, (%rdx)
	jne	.L189
.L131:
	movq	-216(%rbp), %xmm0
	testq	%rbx, %rbx
	movq	%r13, %rdi
	movq	%r11, -224(%rbp)
	cmove	%r11, %rbx
	movhps	-200(%rbp), %xmm0
	movaps	%xmm0, -96(%rbp)
	movq	352(%r15), %rsi
	movq	%rbx, -80(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-224(%rbp), %r11
	movq	%r14, %r8
	movl	$3, %ecx
	movq	-208(%rbp), %rdi
	movq	3280(%r15), %rsi
	movq	%r15, -144(%rbp)
	movl	$0, -136(%rbp)
	movq	%r11, %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L190
.L145:
	movq	%r13, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L183:
	leaq	88(%r12), %r11
	leaq	-96(%rbp), %r14
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L145
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC19(%rip), %rdi
	call	fwrite@PLT
	movq	3280(%r15), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN4node20PrintCaughtExceptionEPN2v87IsolateENS0_5LocalINS0_7ContextEEERKNS0_8TryCatchE@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%r11, -224(%rbp)
	leaq	.LC17(%rip), %rsi
	movq	%rdx, -232(%rbp)
	leaq	.LC16(%rip), %rdx
	movq	%rsi, %xmm1
	movl	$771, %esi
	movq	%rdx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE22rejectionsHandledAfter(%rip), %rcx
	movq	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE19unhandledRejections(%rip), %rax
	cltq
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %xmm0
	movslq	%ecx, %rax
	movq	%rax, %xmm2
	movw	%si, -58(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-224(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L132
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-232(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L191
.L132:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L133
	movq	(%rdi), %rax
	movq	%r11, -224(%rbp)
	call	*8(%rax)
	movq	-224(%rbp), %r11
.L133:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L131
	movq	(%rdi), %rax
	movq	%r11, -224(%rbp)
	call	*8(%rax)
	movq	-224(%rbp), %r11
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L188:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L130
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L192
.L130:
	movq	%rdx, _ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic75(%rip)
	mfence
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	88(%r12), %rbx
	lock addq	$1, _ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE22rejectionsHandledAfter(%rip)
	movq	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic82(%rip), %rdx
	testq	%rdx, %rdx
	je	.L193
.L137:
	movq	%rbx, %r11
	leaq	-96(%rbp), %r14
	testb	$5, (%rdx)
	je	.L131
	movq	%rdx, -224(%rbp)
	leaq	.LC17(%rip), %rsi
	leaq	.LC16(%rip), %rdx
	movq	%rdx, %xmm0
	movq	%rsi, %xmm3
	movl	$771, %edx
	punpcklqdq	%xmm3, %xmm0
	movq	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE22rejectionsHandledAfter(%rip), %rcx
	movq	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE19unhandledRejections(%rip), %rax
	cltq
	movaps	%xmm0, -128(%rbp)
	movq	%rax, %xmm0
	movslq	%ecx, %rax
	movq	%rax, %xmm4
	movw	%dx, -58(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -112(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L139
	movq	(%rax), %rax
	movq	-224(%rbp), %rdx
	movq	24(%rax), %r11
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rax
	cmpq	%rax, %r11
	jne	.L194
.L139:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	movq	%rbx, %r11
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L138
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L195
.L138:
	movq	%rdx, _ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic82(%rip)
	mfence
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	.LC15(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L191:
	subq	$8, %rsp
	leaq	-112(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movl	$67, %esi
	pushq	%r14
	pushq	%rcx
	leaq	-58(%rbp), %rcx
	pushq	%rcx
	leaq	-128(%rbp), %rcx
	pushq	%rcx
	leaq	.LC18(%rip), %rcx
	pushq	$2
	pushq	$0
	call	*%rax
	movq	-224(%rbp), %r11
	addq	$64, %rsp
	jmp	.L132
.L195:
	leaq	.LC15(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L138
.L194:
	subq	$8, %rsp
	leaq	-112(%rbp), %rax
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC18(%rip), %rcx
	movl	$67, %esi
	pushq	%r14
	pushq	%rax
	leaq	-58(%rbp), %rax
	pushq	%rax
	leaq	-128(%rbp), %rax
	pushq	%rax
	pushq	$2
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L139
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7613:
	.size	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE, .-_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE
	.p2align 4
	.globl	_Z20_register_task_queuev
	.type	_Z20_register_task_queuev, @function
_Z20_register_task_queuev:
.LFB7617:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7617:
	.size	_Z20_register_task_queuev, .-_Z20_register_task_queuev
	.section	.rodata.str1.1
.LC20:
	.string	"../src/node_task_queue.cc"
.LC21:
	.string	"task_queue"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC20
	.quad	0
	.quad	_ZN4node10task_queueL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC21
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC22:
	.string	"../src/node_task_queue.cc:116"
.LC23:
	.string	"args[0]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"void node::task_queue::SetPromiseRejectCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10task_queueL24SetPromiseRejectCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.local	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic82
	.comm	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic82,8,8
	.local	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic75
	.comm	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE27trace_event_unique_atomic75,8,8
	.section	.rodata.str1.1
.LC25:
	.string	"../src/node_task_queue.cc:67"
.LC26:
	.string	"!callback.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"void node::task_queue::PromiseRejectCallback(v8::PromiseRejectMessage)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE4args, @object
	.size	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE4args, 24
_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.local	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE22rejectionsHandledAfter
	.comm	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE22rejectionsHandledAfter,8,8
	.local	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE19unhandledRejections
	.comm	_ZZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageEE19unhandledRejections,8,8
	.section	.rodata.str1.1
.LC28:
	.string	"../src/node_task_queue.cc:48"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"void node::task_queue::SetTickCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10task_queueL15SetTickCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC28
	.quad	.LC23
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/node_task_queue.cc:37"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"void node::task_queue::EnqueueMicrotask(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10task_queueL16EnqueueMicrotaskERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC30
	.quad	.LC23
	.quad	.LC31
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	0
	.long	1074266112
	.align 8
.LC11:
	.long	0
	.long	1073741824
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
