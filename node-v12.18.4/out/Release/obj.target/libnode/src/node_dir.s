	.file	"node_dir.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB2785:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE2785:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB2786:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE2786:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4687:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4687:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4689:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4689:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node6fs_dir9DirHandle8SelfSizeEv,"axG",@progbits,_ZNK4node6fs_dir9DirHandle8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6fs_dir9DirHandle8SelfSizeEv
	.type	_ZNK4node6fs_dir9DirHandle8SelfSizeEv, @function
_ZNK4node6fs_dir9DirHandle8SelfSizeEv:
.LFB6103:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE6103:
	.size	_ZNK4node6fs_dir9DirHandle8SelfSizeEv, .-_ZNK4node6fs_dir9DirHandle8SelfSizeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7179:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7179:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7667:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7667:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7668:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7668:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7669:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7669:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7671:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L12
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7671:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED2Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED2Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED2Ev:
.LFB10187:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L13
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L13:
	ret
	.cfi_endproc
.LFE10187:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED2Ev
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED1Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED2Ev
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED2Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED2Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED2Ev:
.LFB10237:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L15:
	ret
	.cfi_endproc
.LFE10237:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED2Ev
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED1Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED2Ev
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv:
.LFB10720:
	.cfi_startproc
	endbr64
	movl	$696, %eax
	ret
	.cfi_endproc
.LFE10720:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv:
.LFB10722:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE10722:
	.size	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv:
.LFB10733:
	.cfi_startproc
	endbr64
	movl	$696, %eax
	ret
	.cfi_endproc
.LFE10733:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv
	.section	.text._ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_
	.type	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_, @function
_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_:
.LFB10247:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rdx
	leaq	-88(%rdi), %rcx
	subl	$1, 2156(%rdx)
	js	.L25
	jmp	*80(%rcx)
	.p2align 4,,10
	.p2align 3
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10247:
	.size	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_, .-_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7718:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L26
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L31
.L26:
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	$5, 43(%rax)
	jne	.L26
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7718:
	.size	_ZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED0Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED0Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED0Ev:
.LFB10189:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L33
	movq	(%rdi), %rax
	call	*8(%rax)
.L33:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10189:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED0Ev
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED0Ev,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED0Ev
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED0Ev:
.LFB10239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	call	*8(%rax)
.L39:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10239:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED0Ev
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.type	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv, @function
_ZN4node7ReqWrapI7uv_fs_sE6CancelEv:
.LFB10721:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L46
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE10721:
	.size	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv, .-_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev:
.LFB10715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L54
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node2fs9FSReqBaseD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10715:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.set	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev:
.LFB10728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L62
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L57
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L57:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node2fs9FSReqBaseD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10728:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.set	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev
	.section	.rodata._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Closing directory handle on garbage collection"
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_:
.LFB10711:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	xorl	%eax, %eax
	leaq	.LC1(%rip), %rsi
	jmp	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz@PLT
	.cfi_endproc
.LFE10711:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10673:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L64
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L64:
	ret
	.cfi_endproc
.LFE10673:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10675:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L67
	call	_ZdlPv@PLT
.L67:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10675:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.text
	.p2align 4
	.globl	_ZN4node6fs_dir10AfterCloseEP7uv_fs_s
	.type	_ZN4node6fs_dir10AfterCloseEP7uv_fs_s, @function
_ZN4node6fs_dir10AfterCloseEP7uv_fs_s:
.LFB7730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	-88(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s@PLT
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope7ProceedEv@PLT
	testb	%al, %al
	je	.L70
	movq	-72(%rbx), %rdx
	movq	-88(%rbx), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L70:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L76
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L76:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7730:
	.size	_ZN4node6fs_dir10AfterCloseEP7uv_fs_s, .-_ZN4node6fs_dir10AfterCloseEP7uv_fs_s
	.section	.rodata._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_.str1.1,"aMS",@progbits,1
.LC2:
	.string	"close"
	.section	.text._ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_,"axG",@progbits,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_:
.LFB10713:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movdqa	.LC3(%rip), %xmm0
	movq	352(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$100, %eax
	movl	$1701603681, -80(%rbp)
	movaps	%xmm0, -128(%rbp)
	movdqa	.LC4(%rip), %xmm0
	movw	%ax, -76(%rbp)
	movaps	%xmm0, -112(%rbp)
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm0, -96(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	24(%r12), %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %r14
	movq	%r15, %rcx
	leaq	.LC2(%rip), %rdx
	movq	%r14, %rdi
	call	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L80:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10713:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev:
.LFB10732:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8030569533516436294, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1702062445, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE10732:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.section	.rodata._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE.str1.1,"aMS",@progbits,1
.LC6:
	.string	"stats_field_array"
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB10731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE@PLT
	movq	688(%r12), %rax
	testq	%rax, %rax
	je	.L82
	movq	656(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L82
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L89
	cmpq	72(%rbx), %rcx
	je	.L94
.L87:
	movq	-8(%rcx), %rsi
.L86:
	leaq	.LC6(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L82:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%esi, %esi
	jmp	.L86
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10731:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB10718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node2fs9FSReqBase10MemoryInfoEPNS_13MemoryTrackerE@PLT
	movq	688(%r12), %rax
	testq	%rax, %rax
	je	.L96
	movq	656(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L96
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L103
	cmpq	72(%rbx), %rcx
	je	.L108
.L101:
	movq	-8(%rcx), %rsi
.L100:
	leaq	.LC6(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L96:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L103:
	xorl	%esi, %esi
	jmp	.L100
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10718:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.text
	.p2align 4
	.type	_ZN4node6fs_dirL17DirentListToArrayEPNS_11EnvironmentEP11uv_dirent_siNS_8encodingEPN2v85LocalINS6_5ValueEEE.isra.0.constprop.0, @function
_ZN4node6fs_dirL17DirentListToArrayEPNS_11EnvironmentEP11uv_dirent_siNS_8encodingEPN2v85LocalINS6_5ValueEEE.isra.0.constprop.0:
.LFB10871:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leal	(%rdx,%rdx), %r15d
	pushq	%r14
	movslq	%r15d, %r15
	.cfi_offset 14, -32
	movl	%edx, %r14d
	leaq	-56(%rbp), %rdx
	pushq	%r13
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$600, %rsp
	movq	%r8, -640(%rbp)
	movdqa	.LC7(%rip), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-568(%rbp), %rax
	movaps	%xmm0, -592(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -632(%rbp)
	movq	%rax, -576(%rbp)
	.p2align 4,,10
	.p2align 3
.L111:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L111
	movq	$0, -568(%rbp)
	cmpq	$64, %r15
	jbe	.L126
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r15,8), %rdi
	andq	%r15, %rax
	cmpq	%rax, %r15
	jne	.L146
	testq	%rdi, %rdi
	je	.L114
	movq	%rdi, -616(%rbp)
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L147
	movq	%rax, -576(%rbp)
	movq	%r15, -584(%rbp)
	jmp	.L112
.L126:
	movq	-632(%rbp), %r8
.L112:
	movq	%r15, -592(%rbp)
	testl	%r14d, %r14d
	jle	.L127
	leal	-1(%r14), %eax
	xorl	%r15d, %r15d
	leaq	2(%rax,%rax), %rax
	movq	%rax, -624(%rbp)
	leaq	-600(%rbp), %rax
	movq	%rax, -616(%rbp)
.L121:
	movq	$0, -600(%rbp)
	movq	(%r12,%r15,8), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	movq	352(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r14, %rsi
	movq	-616(%rbp), %r8
	movq	%rax, %rdx
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L148
	cmpq	%r15, -592(%rbp)
	jbe	.L120
	movq	-576(%rbp), %rdx
	movl	8(%r12,%r15,8), %esi
	leaq	0(,%r15,8), %r14
	movq	352(%rbx), %rdi
	movq	%rax, (%rdx,%r15,8)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	leal	2(%r15), %ecx
	leaq	1(%r15), %rdx
	cmpq	%rdx, -592(%rbp)
	jbe	.L120
	movq	-576(%rbp), %r8
	addq	$2, %r15
	movq	%rax, 8(%r8,%r14)
	cmpq	-624(%rbp), %r15
	jne	.L121
	movslq	%ecx, %rdx
.L117:
	movq	352(%rbx), %rdi
	movq	%r8, %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%rax, %r12
.L124:
	movq	-576(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L122
	cmpq	-632(%rbp), %rdi
	je	.L122
	call	free@PLT
.L122:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L149
	addq	$600, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L120:
	.cfi_restore_state
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm64EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L148:
	movq	-600(%rbp), %rax
	movq	-640(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	%rax, (%rbx)
	jmp	.L124
.L114:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L147:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-616(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L114
	movq	-592(%rbp), %rax
	movq	%r8, -576(%rbp)
	movq	%r15, -584(%rbp)
	testq	%rax, %rax
	je	.L112
	movq	-632(%rbp), %rsi
	movq	%r8, %rdi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L112
.L146:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L127:
	xorl	%edx, %edx
	jmp	.L117
.L149:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10871:
	.size	_ZN4node6fs_dirL17DirentListToArrayEPNS_11EnvironmentEP11uv_dirent_siNS_8encodingEPN2v85LocalINS6_5ValueEEE.isra.0.constprop.0, .-_ZN4node6fs_dirL17DirentListToArrayEPNS_11EnvironmentEP11uv_dirent_siNS_8encodingEPN2v85LocalINS6_5ValueEEE.isra.0.constprop.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC8:
	.string	"dir"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node6fs_dir9DirHandle10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6fs_dir9DirHandle10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6fs_dir9DirHandle10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$72, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movl	$3, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC8(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r12)
	leaq	-32(%rbp), %rsi
	movq	$56, 64(%r12)
	movq	(%rdi), %rax
	movq	%r12, -32(%rbp)
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L151
	movq	(%rdi), %rax
	call	*8(%rax)
.L151:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L150
	cmpq	72(%rbx), %rax
	je	.L162
.L153:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L150
	movq	8(%rbx), %rdi
	leaq	.LC8(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L150:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L153
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7723:
	.size	_ZNK4node6fs_dir9DirHandle10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6fs_dir9DirHandle10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv:
.LFB10861:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE10861:
	.size	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.text
	.p2align 4
	.type	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.part.0, @function
_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.part.0:
.LFB10837:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	3120(%rdi), %rdi
	movq	3280(%rbx), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L166
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L175
	movq	3280(%rbx), %rdi
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L175
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	1424(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L175
	movl	$696, %edi
	call	_Znwm@PLT
	movl	$8, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movsd	.LC9(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L177
	movq	2112(%rbx), %rdx
	movdqa	.LC7(%rip), %xmm0
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r15
	movq	%r13, %rdi
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	584(%r12), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%rbx), %rdx
	movq	352(%rbx), %rbx
	movq	%rax, 576(%r12)
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rbx, %rsi
	movq	%rdx, 72(%r12)
	movq	%rax, 56(%r12)
	movups	%xmm0, 560(%r12)
	movdqa	.LC10(%rip), %xmm0
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 528(%r12)
	movl	$1, 536(%r12)
	movb	$0, 540(%r12)
	movq	$0, 544(%r12)
	movb	$1, 552(%r12)
	movb	$0, 584(%r12)
	movb	$0, 648(%r12)
	movq	%rbx, 656(%r12)
	movq	$0, 688(%r12)
	movups	%xmm0, 664(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	656(%r12), %rdi
	movl	$144, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movl	$18, %edx
	movq	%r14, %rdi
	movq	672(%r12), %rsi
	movq	%rax, 680(%r12)
	call	_ZN2v814BigUint64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L221
.L178:
	movq	%rbx, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -112(%rbp)
	movq	688(%r12), %rdi
	testq	%rdi, %rdi
	je	.L181
.L180:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 688(%r12)
	movq	-112(%rbp), %rax
.L181:
	testq	%rax, %rax
	je	.L179
	movq	%rax, 688(%r12)
	leaq	688(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L179:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L166:
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L214
	.p2align 4,,10
	.p2align 3
.L175:
	xorl	%r12d, %r12d
.L165:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movq	3280(%rbx), %rdi
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L175
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	1424(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L175
	movl	$696, %edi
	call	_Znwm@PLT
	movl	$8, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movsd	.LC9(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L177
	movq	2112(%rbx), %rdx
	movdqa	.LC7(%rip), %xmm0
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r15
	movq	%r13, %rdi
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	584(%r12), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%rbx), %rdx
	movq	352(%rbx), %rbx
	movq	%rax, 576(%r12)
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$144, %rax
	movq	%rbx, %rsi
	movq	%rdx, 72(%r12)
	movq	%rax, 56(%r12)
	movups	%xmm0, 560(%r12)
	movdqa	.LC10(%rip), %xmm0
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	$0, 528(%r12)
	movl	$1, 536(%r12)
	movb	$0, 540(%r12)
	movq	$0, 544(%r12)
	movb	$0, 552(%r12)
	movb	$0, 584(%r12)
	movb	$0, 648(%r12)
	movq	%rbx, 656(%r12)
	movq	$0, 688(%r12)
	movups	%xmm0, 664(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	656(%r12), %rdi
	movl	$144, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movl	$18, %edx
	movq	%r14, %rdi
	movq	672(%r12), %rsi
	movq	%rax, 680(%r12)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L178
.L221:
	movq	$0, -112(%rbp)
	movq	688(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L180
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L177:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10837:
	.size	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.part.0, .-_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.part.0
	.section	.text._ZN4node7ReqWrapI7uv_fs_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI7uv_fs_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv:
.LFB10862:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L225
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE10862:
	.size	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.section	.text._ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev:
.LFB6102:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$101, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7810489017873623364, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movq	$9, 8(%rdi)
	movb	$0, 25(%rdi)
	ret
	.cfi_endproc
.LFE6102:
	.size	_ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev:
.LFB10719:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8030569533516436294, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1702062445, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE10719:
	.size	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev:
.LFB10863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L235
	leaq	-56(%rdi), %r12
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L230
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L230:
	movq	%r12, %rdi
	call	_ZN4node2fs9FSReqBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10863:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev:
.LFB10864:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L243
	leaq	-56(%rdi), %r12
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L238
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L238:
	movq	%r12, %rdi
	call	_ZN4node2fs9FSReqBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10864:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev:
.LFB10717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L251
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L246
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L246:
	movq	%r12, %rdi
	call	_ZN4node2fs9FSReqBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10717:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev:
.LFB10730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$144, %rax
	cmpb	$0, 648(%rdi)
	movq	%rax, 56(%rdi)
	je	.L259
	movq	%rdi, %r12
	movq	688(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L254
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L254:
	movq	%r12, %rdi
	call	_ZN4node2fs9FSReqBaseD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$696, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10730:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev:
.LFB10865:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L267
	movq	%rdi, %rbx
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L262
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L262:
	addq	$8, %rsp
	leaq	-56(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node2fs9FSReqBaseD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10865:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED2Ev,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.type	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev, @function
_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev:
.LFB10866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	%rax, -56(%rdi)
	addq	$144, %rax
	cmpb	$0, 592(%rdi)
	movq	%rax, (%rdi)
	je	.L275
	movq	%rdi, %rbx
	movq	632(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L270
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L270:
	addq	$8, %rsp
	leaq	-56(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node2fs9FSReqBaseD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	leaq	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10866:
	.size	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev, .-_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE:
.LFB10726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L277
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L285
.L277:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L286
.L278:
	movq	(%r12), %rbx
	call	_ZN2v87Promise8Resolver10GetPromiseEv@PLT
	testq	%rax, %rax
	je	.L287
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L287:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rdi
	jmp	.L278
	.cfi_endproc
.LFE10726:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE:
.LFB10737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L289
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L297
.L289:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L298
.L290:
	movq	(%r12), %rbx
	call	_ZN2v87Promise8Resolver10GetPromiseEv@PLT
	testq	%rax, %rax
	je	.L299
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L299:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rdi
	jmp	.L290
	.cfi_endproc
.LFE10737:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE:
.LFB10724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L301
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L309
.L301:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L310
.L302:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L311
.L303:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L312
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L309:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L302
.L312:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10724:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE:
.LFB10723:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L314
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L322
.L314:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L323
.L315:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L324
.L316:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L325
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L324:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L315
.L325:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10723:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE:
.LFB10735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L327
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L335
.L327:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L336
.L328:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L337
.L329:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L338
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L335:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L337:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L336:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L328
.L338:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10735:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE:
.LFB10734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movb	$1, 648(%rdi)
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L340
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L348
.L340:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L349
.L341:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L350
.L342:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L351
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L350:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L341
.L351:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10734:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.section	.rodata.str1.1
.LC11:
	.string	"opendir"
.LC12:
	.string	"read"
.LC13:
	.string	"DirHandle"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB14:
	.text
.LHOTB14:
	.p2align 4
	.globl	_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L353
	movq	%rdi, %r15
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L353
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L353
	movq	271(%rax), %rbx
	movq	352(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L374
.L354:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L375
.L355:
	movq	-56(%rbp), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L376
.L356:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rbx, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L377
.L357:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L378
.L358:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	-64(%rbp), %rdi
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L379
.L359:
	movq	%rdx, %rsi
	movq	%r12, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	-56(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L380
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L381
.L361:
	movq	3088(%rbx), %rdi
	movq	352(%rbx), %r12
	testq	%rdi, %rdi
	je	.L362
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3088(%rbx)
.L362:
	testq	%r14, %r14
	je	.L352
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3088(%rbx)
.L352:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L375:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L376:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L377:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%rdx, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L361
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L361
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7743:
.L353:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7743:
	.text
	.size	_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE14:
	.text
.LHOTE14:
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t:
.LFB10736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	680(%rdi), %rax
	movq	%rdx, (%rax)
	movq	8(%rsi), %rdx
	movq	%rdx, 8(%rax)
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%rax)
	movq	24(%rsi), %rdx
	movq	%rdx, 24(%rax)
	movq	32(%rsi), %rdx
	movq	%rdx, 32(%rax)
	movq	40(%rsi), %rdx
	movq	%rdx, 40(%rax)
	movq	64(%rsi), %rdx
	movq	%rdx, 48(%rax)
	movq	48(%rsi), %rdx
	movq	%rdx, 56(%rax)
	movq	56(%rsi), %rdx
	movq	%rdx, 64(%rax)
	movq	72(%rsi), %rdx
	movq	%rdx, 72(%rax)
	movq	96(%rsi), %rdx
	movq	%rdx, 80(%rax)
	movq	104(%rsi), %rdx
	movq	%rdx, 88(%rax)
	movq	112(%rsi), %rdx
	movq	688(%rdi), %r12
	movq	%rdx, 96(%rax)
	movq	120(%rsi), %rdx
	movq	%rdx, 104(%rax)
	movq	128(%rsi), %rdx
	movq	%rdx, 112(%rax)
	movq	136(%rsi), %rdx
	movq	%rdx, 120(%rax)
	movq	144(%rsi), %rdx
	movq	%rdx, 128(%rax)
	movq	152(%rsi), %rdx
	movq	%rdx, 136(%rax)
	testq	%r12, %r12
	je	.L383
	movq	(%r12), %rsi
	movq	656(%rdi), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L383:
	movq	16(%rbx), %rax
	movb	$1, 648(%rbx)
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L384
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L395
.L384:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L396
.L385:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L397
.L386:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L398
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L395:
	.cfi_restore_state
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L397:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L385
.L398:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10736:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.section	.text._ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t,"axG",@progbits,_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.type	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t, @function
_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t:
.LFB10725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$96, %rsp
	movq	(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	680(%rdi), %rax
	testq	%rdx, %rdx
	js	.L400
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	8(%rsi), %rdx
	movsd	%xmm0, (%rax)
	testq	%rdx, %rdx
	js	.L402
.L452:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	16(%rsi), %rdx
	movsd	%xmm0, 8(%rax)
	testq	%rdx, %rdx
	js	.L404
.L453:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	24(%rsi), %rdx
	movsd	%xmm0, 16(%rax)
	testq	%rdx, %rdx
	js	.L406
.L454:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	32(%rsi), %rdx
	movsd	%xmm0, 24(%rax)
	testq	%rdx, %rdx
	js	.L408
.L455:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	40(%rsi), %rdx
	movsd	%xmm0, 32(%rax)
	testq	%rdx, %rdx
	js	.L410
.L456:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	64(%rsi), %rdx
	movsd	%xmm0, 40(%rax)
	testq	%rdx, %rdx
	js	.L412
.L457:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	48(%rsi), %rdx
	movsd	%xmm0, 48(%rax)
	testq	%rdx, %rdx
	js	.L414
.L458:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	56(%rsi), %rdx
	movsd	%xmm0, 56(%rax)
	testq	%rdx, %rdx
	js	.L416
.L459:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	72(%rsi), %rdx
	movsd	%xmm0, 64(%rax)
	testq	%rdx, %rdx
	js	.L418
.L460:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	96(%rsi), %rdx
	movsd	%xmm0, 72(%rax)
	testq	%rdx, %rdx
	js	.L420
.L461:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	104(%rsi), %rdx
	movsd	%xmm0, 80(%rax)
	testq	%rdx, %rdx
	js	.L422
.L462:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	112(%rsi), %rdx
	movsd	%xmm0, 88(%rax)
	testq	%rdx, %rdx
	js	.L424
.L463:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
	movq	120(%rsi), %rdx
	movsd	%xmm0, 96(%rax)
	testq	%rdx, %rdx
	js	.L426
.L464:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L427:
	movq	128(%rsi), %rdx
	movsd	%xmm0, 104(%rax)
	testq	%rdx, %rdx
	js	.L428
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L429:
	movq	136(%rsi), %rdx
	movsd	%xmm0, 112(%rax)
	testq	%rdx, %rdx
	js	.L430
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L431:
	movq	144(%rsi), %rdx
	movsd	%xmm0, 120(%rax)
	testq	%rdx, %rdx
	js	.L432
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L433:
	movq	152(%rsi), %rdx
	movsd	%xmm0, 128(%rax)
	testq	%rdx, %rdx
	js	.L434
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L435:
	movq	688(%rbx), %r12
	movsd	%xmm0, 136(%rax)
	testq	%r12, %r12
	je	.L436
	movq	(%r12), %rsi
	movq	656(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L436:
	movq	16(%rbx), %rax
	movb	$1, 648(%rbx)
	leaq	-112(%rbp), %r13
	leaq	-80(%rbp), %r14
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L437
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L448
.L437:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1424(%rdx), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L449
.L438:
	movq	16(%rbx), %rax
	movq	%r12, %rdx
	movq	3280(%rax), %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L450
.L439:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L451
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	8(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, (%rax)
	testq	%rdx, %rdx
	jns	.L452
.L402:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	16(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rax)
	testq	%rdx, %rdx
	jns	.L453
.L404:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	24(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rax)
	testq	%rdx, %rdx
	jns	.L454
.L406:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	32(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 24(%rax)
	testq	%rdx, %rdx
	jns	.L455
.L408:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	40(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rax)
	testq	%rdx, %rdx
	jns	.L456
.L410:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	64(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 40(%rax)
	testq	%rdx, %rdx
	jns	.L457
.L412:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	48(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 48(%rax)
	testq	%rdx, %rdx
	jns	.L458
.L414:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	56(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 56(%rax)
	testq	%rdx, %rdx
	jns	.L459
.L416:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	72(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 64(%rax)
	testq	%rdx, %rdx
	jns	.L460
.L418:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	96(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rax)
	testq	%rdx, %rdx
	jns	.L461
.L420:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	104(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 80(%rax)
	testq	%rdx, %rdx
	jns	.L462
.L422:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	112(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 88(%rax)
	testq	%rdx, %rdx
	jns	.L463
.L424:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	movq	120(%rsi), %rdx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 96(%rax)
	testq	%rdx, %rdx
	jns	.L464
.L426:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L448:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L438
.L451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10725:
	.size	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t, .-_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L466
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L466:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L476
.L467:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L468
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L476:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L467
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7177:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.1
.LC15:
	.string	"closedir"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"node,node.fs_dir,node.fs_dir.sync"
	.section	.rodata.str1.1
.LC17:
	.string	"fs_dir.sync.closedir"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7731:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L536
	cmpw	$1040, %cx
	jne	.L478
.L536:
	movl	16(%r12), %r14d
	movq	23(%rdx), %rbx
	testl	%r14d, %r14d
	jle	.L575
.L481:
	movq	(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L490
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L537
	cmpw	$1040, %cx
	jne	.L483
.L537:
	movq	23(%rdx), %r15
.L485:
	testq	%r15, %r15
	je	.L477
	movl	16(%r12), %edx
	movl	$256, %eax
	movw	%ax, 88(%r15)
	testl	%edx, %edx
	jle	.L576
	movq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L489
.L579:
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L490
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L538
	cmpw	$1040, %cx
	jne	.L491
.L538:
	movq	23(%rdx), %r13
.L494:
	testq	%r13, %r13
	je	.L496
.L580:
	leaq	.LC15(%rip), %rax
	cmpq	$0, 80(%r13)
	movq	56(%r15), %rdx
	movl	$1, 536(%r13)
	movq	%rax, 544(%r13)
	movq	%r13, 88(%r13)
	jne	.L577
	leaq	_ZN4node6fs_dir10AfterCloseEP7uv_fs_s(%rip), %rax
	leaq	88(%r13), %r14
	movq	%rax, 80(%r13)
	movq	16(%r13), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_closedir@PLT
	testl	%eax, %eax
	js	.L498
	movq	16(%r13), %rax
	movq	%r12, %rsi
	movq	%r13, %rdi
	addl	$1, 2156(%rax)
	movq	0(%r13), %rax
	call	*120(%rax)
.L477:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L576:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	movq	%r13, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L579
.L489:
	movq	2784(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L496
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.part.0
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.L580
.L496:
	cmpl	$2, %r14d
	jne	.L581
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L501
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %r13
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%r13, %rdx
	jne	.L582
.L502:
	cmpb	$0, (%rax)
	jne	.L583
.L501:
	cmpl	$1, 16(%r12)
	movq	56(%r15), %r14
	jle	.L584
	movq	8(%r12), %r13
	subq	$8, %r13
.L511:
	movq	%rbx, %rdi
	leaq	-512(%rbp), %r12
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_closedir@PLT
	testl	%eax, %eax
	js	.L585
.L512:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L517
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %r13
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%r13, %rdx
	jne	.L586
.L518:
	cmpb	$0, (%rax)
	jne	.L587
.L517:
	movq	%r12, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L477
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L584:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L498:
	cltq
	leaq	-576(%rbp), %r12
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, 176(%r13)
	movq	%r12, %rdi
	movq	$0, 192(%r13)
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s@PLT
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope7ProceedEv@PLT
	testb	%al, %al
	je	.L528
	movq	16(%r13), %rdx
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	352(%rdx), %rsi
	addq	$88, %rsi
	call	*104(%rax)
.L528:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L478:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movl	16(%r12), %r14d
	movq	%rax, %rbx
	testl	%r14d, %r14d
	jg	.L481
	.p2align 4,,10
	.p2align 3
.L575:
	leaq	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L583:
	movq	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic161(%rip), %r14
	testq	%r14, %r14
	je	.L588
.L505:
	testb	$5, (%r14)
	je	.L501
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -528(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L507
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L589
.L507:
	movq	-520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L508
	movq	(%rdi), %rax
	call	*8(%rax)
.L508:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L501
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L483:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L490:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L587:
	movq	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic164(%rip), %r14
	testq	%r14, %r14
	je	.L590
.L521:
	testb	$5, (%r14)
	je	.L517
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -528(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L523
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L591
.L523:
	movq	-520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L524
	movq	(%rdi), %rax
	call	*8(%rax)
.L524:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L517
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L585:
	movq	352(%rbx), %r15
	movl	%eax, %esi
	movq	3280(%rbx), %r14
	movq	%r15, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L592
.L513:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L593
.L514:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L512
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L582:
	leaq	.LC16(%rip), %rsi
	call	*%rdx
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L491:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L586:
	leaq	.LC16(%rip), %rsi
	call	*%rdx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L577:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	leaq	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L588:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L506
	movq	(%rax), %rax
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	16(%rax), %rax
	cmpq	%r13, %rax
	jne	.L594
.L506:
	movq	%r14, _ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic161(%rip)
	mfence
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L590:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L522
	movq	(%rax), %rax
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	16(%rax), %rax
	cmpq	%r13, %rax
	jne	.L595
.L522:
	movq	%r14, _ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic164(%rip)
	mfence
	jmp	.L521
.L593:
	movq	%rax, -584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-584(%rbp), %rcx
	jmp	.L514
.L592:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L513
.L595:
	leaq	.LC16(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L522
.L594:
	leaq	.LC16(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L506
.L589:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-528(%rbp), %rdx
	pushq	$0
	leaq	.LC17(%rip), %rcx
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L507
.L591:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-528(%rbp), %rdx
	pushq	$0
	leaq	.LC17(%rip), %rcx
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L523
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7731:
	.size	_ZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s
	.type	_ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s, @function
_ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s:
.LFB7715:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	.LC9(%rip), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	movl	$1, %ecx
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node6fs_dir9DirHandleE(%rip), %rax
	xorl	%edx, %edx
	movq	%r12, 56(%rbx)
	movq	%rax, (%rbx)
	movq	24(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movq	$0, 80(%rbx)
	movw	%dx, 88(%rbx)
	testq	%rax, %rax
	je	.L599
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L598
.L599:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	56(%rbx), %r12
.L598:
	movq	$0, 8(%r12)
	movq	$0, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7715:
	.size	_ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s, .-_ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s
	.globl	_ZN4node6fs_dir9DirHandleC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s
	.set	_ZN4node6fs_dir9DirHandleC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s,_ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s
	.align 2
	.p2align 4
	.globl	_ZN4node6fs_dir9DirHandle3NewEPNS_11EnvironmentEP8uv_dir_s
	.type	_ZN4node6fs_dir9DirHandle3NewEPNS_11EnvironmentEP8uv_dir_s, @function
_ZN4node6fs_dir9DirHandle3NewEPNS_11EnvironmentEP8uv_dir_s:
.LFB7717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	xorl	%r12d, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	movq	3088(%rdi), %rdi
	movq	3280(%r13), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L601
	movl	$96, %edi
	movq	%rax, %r14
	call	_Znwm@PLT
	movl	$1, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movsd	.LC9(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node6fs_dir9DirHandleE(%rip), %rax
	movq	%rbx, 56(%r12)
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movw	%ax, 88(%r12)
	movq	24(%r12), %rax
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 80(%r12)
	testq	%rax, %rax
	je	.L607
	movl	(%rax), %edx
	movb	$1, 8(%rax)
	testl	%edx, %edx
	jne	.L604
.L607:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	56(%r12), %rbx
.L604:
	movq	$0, 8(%rbx)
	movq	$0, (%rbx)
.L601:
	popq	%rbx
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7717:
	.size	_ZN4node6fs_dir9DirHandle3NewEPNS_11EnvironmentEP8uv_dir_s, .-_ZN4node6fs_dir9DirHandle3NewEPNS_11EnvironmentEP8uv_dir_s
	.p2align 4
	.globl	_ZN4node6fs_dir12AfterOpenDirEP7uv_fs_s
	.type	_ZN4node6fs_dir12AfterOpenDirEP7uv_fs_s, @function
_ZN4node6fs_dir12AfterOpenDirEP7uv_fs_s:
.LFB7738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	-88(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	movq	%r13, %rsi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s@PLT
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope7ProceedEv@PLT
	testb	%al, %al
	je	.L620
	movq	96(%rbx), %rsi
	movq	16(%r13), %rdi
	call	_ZN4node6fs_dir9DirHandle3NewEPNS_11EnvironmentEP8uv_dir_s
	movq	-88(%rbx), %rdx
	movq	8(%rax), %rsi
	movq	104(%rdx), %rbx
	testq	%rsi, %rsi
	je	.L614
	movzbl	11(%rsi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L621
.L614:
	movq	%r13, %rdi
	call	*%rbx
.L620:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L622
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	movq	16(%rax), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L614
.L622:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7738:
	.size	_ZN4node6fs_dir12AfterOpenDirEP7uv_fs_s, .-_ZN4node6fs_dir12AfterOpenDirEP7uv_fs_s
	.section	.rodata.str1.1
.LC18:
	.string	"fs_dir.sync.opendir"
	.text
	.p2align 4
	.type	_ZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1544, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L692
	cmpw	$1040, %cx
	jne	.L624
.L692:
	movq	23(%rdx), %r13
.L626:
	movl	16(%r12), %r15d
	movq	352(%r13), %r14
	cmpl	$2, %r15d
	jg	.L749
	leaq	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L749:
	movq	8(%r12), %rdx
	leaq	-1104(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN4node11BufferValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpq	$0, -1088(%rbp)
	je	.L750
	cmpl	$1, 16(%r12)
	jle	.L751
	movq	8(%r12), %rax
	leaq	-8(%rax), %rsi
.L629:
	movl	$1, %edx
	movq	%r14, %rdi
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	cmpl	$2, 16(%r12)
	movl	%eax, %ebx
	jg	.L630
	movq	(%r12), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
.L631:
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L632
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L752
	movq	(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L693
	cmpw	$1040, %cx
	jne	.L634
.L693:
	movq	23(%rdx), %r14
.L637:
	testq	%r14, %r14
	je	.L639
.L755:
	leaq	.LC11(%rip), %rax
	cmpq	$0, 80(%r14)
	movq	%r14, 88(%r14)
	movq	%rax, 544(%r14)
	movq	-1088(%rbp), %rdx
	movl	%ebx, 536(%r14)
	jne	.L753
	leaq	_ZN4node6fs_dir12AfterOpenDirEP7uv_fs_s(%rip), %rax
	leaq	88(%r14), %r13
	movq	%rax, 80(%r14)
	movq	16(%r14), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_opendir@PLT
	testl	%eax, %eax
	js	.L641
	movq	16(%r14), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	addl	$1, 2156(%rax)
	movq	(%r14), %rax
	call	*120(%rax)
.L642:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L623
	testq	%rdi, %rdi
	je	.L623
	call	free@PLT
.L623:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L754
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L751:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L632:
	movq	2784(%r13), %rsi
	movq	%r14, %rdi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L639
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.part.0
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.L755
.L639:
	cmpl	$4, %r15d
	jne	.L756
	movq	$0, -1112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L645
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %r14
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%r14, %rdx
	jne	.L757
.L646:
	cmpb	$0, (%rax)
	jne	.L758
.L645:
	cmpl	$3, 16(%r12)
	movq	-1088(%rbp), %rbx
	jle	.L759
	movq	8(%r12), %rax
	leaq	-24(%rax), %r14
.L655:
	movq	%r13, %rdi
	leaq	-1552(%rbp), %r15
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	%rbx, %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	360(%r13), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_opendir@PLT
	movl	%eax, %ebx
	testl	%eax, %eax
	js	.L760
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L674
.L660:
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %r14
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%r14, %rdx
	jne	.L761
.L663:
	cmpb	$0, (%rax)
	jne	.L762
.L665:
	testl	%ebx, %ebx
	js	.L748
.L674:
	movq	-1456(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN4node6fs_dir9DirHandle3NewEPNS_11EnvironmentEP8uv_dir_s
	movq	(%r12), %rbx
	movq	%rax, %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L678
	movzbl	11(%rax), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	je	.L763
.L679:
	movq	(%rax), %rax
.L680:
	movq	%rax, 24(%rbx)
.L748:
	movq	%r15, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-1112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L642
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L630:
	movq	8(%r12), %rax
	leaq	-16(%rax), %r14
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L759:
	movq	(%r12), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L641:
	movq	$0, 192(%r14)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r14)
	call	_ZN4node6fs_dir12AfterOpenDirEP7uv_fs_s
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L758:
	movq	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic333(%rip), %r15
	testq	%r15, %r15
	je	.L764
.L649:
	testb	$5, (%r15)
	je	.L645
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1568(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L651
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L765
.L651:
	movq	-1560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L652
	movq	(%rdi), %rax
	call	*8(%rax)
.L652:
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L645
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L624:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L762:
	movq	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic336(%rip), %rdx
	testq	%rdx, %rdx
	je	.L766
.L667:
	testb	$5, (%rdx)
	je	.L665
	pxor	%xmm0, %xmm0
	movq	%rdx, -1576(%rbp)
	movaps	%xmm0, -1568(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L670
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-1576(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L767
.L670:
	movq	-1560(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L671
	movq	(%rdi), %rax
	call	*8(%rax)
.L671:
	movq	-1568(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L665
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L750:
	leaq	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L763:
	movq	16(%rdx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	jne	.L679
	.p2align 4,,10
	.p2align 3
.L678:
	movq	16(%rbx), %rax
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L760:
	movq	352(%r13), %r8
	movq	3280(%r13), %rax
	movl	%ebx, %esi
	movq	%r8, %rdi
	movq	%r8, -1584(%rbp)
	movq	%rax, -1576(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-1576(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%r13), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-1584(%rbp), %r8
	testb	%al, %al
	je	.L768
.L657:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L769
.L658:
	movq	360(%r13), %rax
	movq	-1576(%rbp), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L770
.L659:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L660
	jmp	.L748
	.p2align 4,,10
	.p2align 3
.L757:
	leaq	.LC16(%rip), %rsi
	call	*%rdx
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L761:
	leaq	.LC16(%rip), %rsi
	call	*%rdx
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L634:
	movq	%r14, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L752:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L753:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L766:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L668
	movq	(%rax), %rax
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L771
.L668:
	movq	%rdx, _ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic336(%rip)
	mfence
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L764:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r15
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L650
	movq	(%rax), %rax
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L772
.L650:
	movq	%r15, _ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic333(%rip)
	mfence
	jmp	.L649
.L770:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L659
.L769:
	movq	%rax, -1584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1584(%rbp), %rcx
	jmp	.L658
.L768:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-1584(%rbp), %r8
	jmp	.L657
.L772:
	leaq	.LC16(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L650
.L771:
	leaq	.LC16(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L668
.L765:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-1568(%rbp), %rdx
	pushq	$0
	leaq	.LC18(%rip), %rcx
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L651
.L767:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-1568(%rbp), %rcx
	pushq	$0
	pushq	%rcx
	leaq	.LC18(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L670
.L754:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7739:
	.size	_ZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node6fs_dir9DirHandle7GCCloseEv,"axG",@progbits,_ZN4node6fs_dir9DirHandle7GCCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node6fs_dir9DirHandle7GCCloseEv
	.type	_ZN4node6fs_dir9DirHandle7GCCloseEv, @function
_ZN4node6fs_dir9DirHandle7GCCloseEv:
.LFB7724:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$456, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 89(%rdi)
	je	.L796
.L773:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L797
	addq	$456, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L796:
	.cfi_restore_state
	movq	56(%rdi), %rdx
	leaq	-480(%rbp), %r13
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	xorl	%edi, %edi
	call	uv_fs_closedir@PLT
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	uv_fs_req_cleanup@PLT
	movl	$256, %eax
	movl	$32, %edi
	movw	%ax, 88(%rbx)
	movq	16(%rbx), %rbx
	testl	%r12d, %r12d
	js	.L798
	call	_Znwm@PLT
	movq	2480(%rbx), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_EE(%rip), %rcx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, (%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L782
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L773
.L795:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L798:
	call	_Znwm@PLT
	movq	2480(%rbx), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_EE(%rip), %rsi
	movb	$1, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rsi, (%rax)
	movl	%r12d, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L776
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L778
.L794:
	movq	(%rdi), %rax
	call	*8(%rax)
.L778:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L799
.L780:
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L782:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L795
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L799:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L776:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L794
	jmp	.L778
.L797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7724:
	.size	_ZN4node6fs_dir9DirHandle7GCCloseEv, .-_ZN4node6fs_dir9DirHandle7GCCloseEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6fs_dir9DirHandleD2Ev
	.type	_ZN4node6fs_dir9DirHandleD2Ev, @function
_ZN4node6fs_dir9DirHandleD2Ev:
.LFB7720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6fs_dir9DirHandleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$0, 88(%rdi)
	movq	%rax, (%rdi)
	jne	.L808
	movq	%rdi, %r12
	call	_ZN4node6fs_dir9DirHandle7GCCloseEv
	cmpb	$0, 89(%r12)
	je	.L809
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L803
	call	_ZdlPv@PLT
.L803:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L808:
	.cfi_restore_state
	leaq	_ZZN4node6fs_dir9DirHandleD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L809:
	leaq	_ZZN4node6fs_dir9DirHandleD4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7720:
	.size	_ZN4node6fs_dir9DirHandleD2Ev, .-_ZN4node6fs_dir9DirHandleD2Ev
	.globl	_ZN4node6fs_dir9DirHandleD1Ev
	.set	_ZN4node6fs_dir9DirHandleD1Ev,_ZN4node6fs_dir9DirHandleD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node6fs_dir9DirHandleD0Ev
	.type	_ZN4node6fs_dir9DirHandleD0Ev, @function
_ZN4node6fs_dir9DirHandleD0Ev:
.LFB7722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6fs_dir9DirHandleE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	cmpb	$0, 88(%rdi)
	movq	%rax, (%rdi)
	jne	.L818
	movq	%rdi, %r12
	call	_ZN4node6fs_dir9DirHandle7GCCloseEv
	cmpb	$0, 89(%r12)
	je	.L819
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	je	.L813
	call	_ZdlPv@PLT
.L813:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L818:
	.cfi_restore_state
	leaq	_ZZN4node6fs_dir9DirHandleD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L819:
	leaq	_ZZN4node6fs_dir9DirHandleD4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7722:
	.size	_ZN4node6fs_dir9DirHandleD0Ev, .-_ZN4node6fs_dir9DirHandleD0Ev
	.p2align 4
	.globl	_Z16_register_fs_dirv
	.type	_Z16_register_fs_dirv, @function
_Z16_register_fs_dirv:
.LFB7744:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7744:
	.size	_Z16_register_fs_dirv, .-_Z16_register_fs_dirv
	.section	.text._ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC5EPS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_
	.type	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_, @function
_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_:
.LFB8635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L838
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L839
.L824:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L828
.L821:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L839:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L829
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L825:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L824
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L830
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L826:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L828:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L821
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L838:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L829:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L830:
	xorl	%edx, %edx
	jmp	.L826
	.cfi_endproc
.LFE8635:
	.size	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_, .-_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_
	.weak	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	.set	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_,_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC2EPS2_
	.text
	.p2align 4
	.type	_ZN4node6fs_dirL12AfterDirReadEP7uv_fs_s, @function
_ZN4node6fs_dirL12AfterDirReadEP7uv_fs_s:
.LFB7736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-88(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-112(%rbp), %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node17BaseObjectPtrImplINS_2fs9FSReqBaseELb0EEC1EPS2_
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rbx, %rdx
	call	_ZN4node2fs15FSReqAfterScopeC1EPNS0_9FSReqBaseEP7uv_fs_s@PLT
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope7ProceedEv@PLT
	testb	%al, %al
	je	.L882
	movq	-112(%rbp), %rax
	movq	88(%rbx), %rdx
	movq	16(%rax), %rdi
	testq	%rdx, %rdx
	je	.L883
	movq	96(%rbx), %rsi
	movl	536(%rax), %ecx
	leaq	-104(%rbp), %r8
	movq	$0, -104(%rbp)
	movq	(%rsi), %rsi
	call	_ZN4node6fs_dirL17DirentListToArrayEPNS_11EnvironmentEP11uv_dirent_siNS_8encodingEPN2v85LocalINS6_5ValueEEE.isra.0.constprop.0
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L884
.L872:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope5ClearEv@PLT
	movq	-112(%rbp), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*104(%rax)
.L882:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScopeD1Ev@PLT
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L840
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L885
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L886
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L840
	cmpb	$0, 9(%rdx)
	je	.L854
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L840:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L887
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L883:
	.cfi_restore_state
	movq	352(%rdi), %r13
	addq	$104, %r13
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L885:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L854:
	cmpb	$0, 8(%rdx)
	je	.L840
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L840
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L884:
	movq	%r12, %rdi
	call	_ZN4node2fs15FSReqAfterScope5ClearEv@PLT
	movq	-112(%rbp), %rdi
	movq	-104(%rbp), %rsi
	movq	(%rdi), %rax
	call	*96(%rax)
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L886:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L887:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7736:
	.size	_ZN4node6fs_dirL12AfterDirReadEP7uv_fs_s, .-_ZN4node6fs_dirL12AfterDirReadEP7uv_fs_s
	.section	.rodata._ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC19:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm
	.type	_ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm, @function
_ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm:
.LFB9172:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L909
	movabsq	$576460752303423487, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdx, %rsi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rcx, %rbx
	subq	(%rdi), %rbx
	subq	%rcx, %rax
	movq	%rbx, %r15
	sarq	$4, %rax
	sarq	$4, %r15
	subq	%r15, %rsi
	cmpq	%r12, %rax
	jb	.L890
	movq	%rcx, %rax
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L891:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	subq	$1, %rdx
	jne	.L891
	salq	$4, %r12
	addq	%r12, %rcx
	movq	%rcx, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L890:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%r12, %rsi
	jb	.L912
	cmpq	%r12, %r15
	movq	%r12, %r14
	cmovnb	%r15, %r14
	addq	%r15, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$4, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r12, %rdx
	movq	%rax, %rcx
	leaq	(%rax,%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L895:
	movq	$0, (%rax)
	addq	$16, %rax
	movl	$0, -8(%rax)
	subq	$1, %rdx
	jne	.L895
	movq	0(%r13), %rbx
	movq	8(%r13), %rdx
	subq	%rbx, %rdx
	testq	%rdx, %rdx
	jg	.L913
	testq	%rbx, %rbx
	jne	.L897
.L898:
	addq	%r15, %r12
	addq	%rcx, %r14
	movq	%rcx, 0(%r13)
	salq	$4, %r12
	movq	%r14, 16(%r13)
	leaq	(%rcx,%r12), %rax
	movq	%rax, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L913:
	.cfi_restore_state
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	call	memmove@PLT
	movq	%rax, %rcx
.L897:
	movq	%rbx, %rdi
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L898
.L912:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9172:
	.size	_ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm, .-_ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm
	.section	.rodata.str1.1
.LC21:
	.string	"readdir"
.LC22:
	.string	"fs_dir.sync.readdir"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L995
	cmpw	$1040, %cx
	jne	.L915
.L995:
	movq	23(%rdx), %r13
.L917:
	movl	16(%r12), %r14d
	movq	352(%r13), %r15
	cmpl	$2, %r14d
	jg	.L1043
	leaq	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	8(%r12), %rsi
	movq	%r15, %rdi
	movl	$1, %edx
	call	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE@PLT
	movq	(%r12), %rbx
	movl	%eax, -552(%rbp)
	movq	%rbx, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L936
	movq	(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L996
	cmpw	$1040, %cx
	jne	.L919
.L996:
	movq	23(%rdx), %rbx
.L921:
	testq	%rbx, %rbx
	je	.L914
	cmpl	$1, 16(%r12)
	jle	.L1044
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L924:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1045
	cmpl	$1, 16(%r12)
	jg	.L926
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L927:
	call	_ZNK2v86Number5ValueEv@PLT
	movsd	.LC20(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L928
	cvttsd2siq	%xmm0, %rax
.L929:
	movq	72(%rbx), %rsi
	movq	64(%rbx), %rcx
	movq	%rsi, %rdx
	subq	%rcx, %rdx
	sarq	$4, %rdx
	cmpq	%rdx, %rax
	je	.L930
	ja	.L1046
	jnb	.L932
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	%rcx, %rdx
	cmpq	%rdx, %rsi
	je	.L932
	movq	%rdx, 72(%rbx)
.L932:
	movq	56(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, (%rdx)
.L930:
	cmpl	$2, 16(%r12)
	jg	.L933
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L934:
	movq	%rdi, -560(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	-560(%rbp), %rdi
	testb	%al, %al
	je	.L935
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L936
	movq	-560(%rbp), %rdi
	movq	(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L997
	cmpw	$1040, %cx
	jne	.L937
.L997:
	movq	23(%rdx), %r8
.L940:
	testq	%r8, %r8
	je	.L942
.L1049:
	leaq	.LC21(%rip), %rax
	movq	56(%rbx), %rdx
	cmpq	$0, 80(%r8)
	movq	%r8, 88(%r8)
	movq	%rax, 544(%r8)
	movl	-552(%rbp), %eax
	movl	%eax, 536(%r8)
	jne	.L1047
	leaq	_ZN4node6fs_dirL12AfterDirReadEP7uv_fs_s(%rip), %rax
	leaq	88(%r8), %r13
	movq	%r8, -552(%rbp)
	movq	%rax, 80(%r8)
	movq	16(%r8), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE7WrapperES2_(%rip), %rcx
	movq	%r13, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_fs_readdir@PLT
	movq	-552(%rbp), %r8
	testl	%eax, %eax
	js	.L944
	movq	16(%r8), %rax
	movq	%r12, %rsi
	movq	%r8, %rdi
	addl	$1, 2156(%rax)
	movq	(%r8), %rax
	call	*120(%rax)
.L914:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1048
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L935:
	movq	2784(%r13), %rsi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L942
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN4node2fs10GetReqWrapEPNS_11EnvironmentEN2v85LocalINS3_5ValueEEEb.part.0
	movq	%rax, %r8
	testq	%r8, %r8
	jne	.L1049
.L942:
	cmpl	$4, %r14d
	jne	.L1050
	movq	$0, -72(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L947
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L1051
.L948:
	cmpb	$0, (%rax)
	jne	.L1052
.L947:
	cmpl	$3, 16(%r12)
	movq	56(%rbx), %rdx
	jle	.L1053
	movq	8(%r12), %rax
	subq	$24, %rax
	movq	%rax, -568(%rbp)
.L957:
	movq	%r13, %rdi
	movq	%rdx, -560(%rbp)
	leaq	-512(%rbp), %r14
	call	_ZNK4node11Environment14PrintSyncTraceEv@PLT
	movq	360(%r13), %rax
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	-560(%rbp), %rdx
	movq	2360(%rax), %rdi
	call	uv_fs_readdir@PLT
	movl	%eax, -560(%rbp)
	testl	%eax, %eax
	js	.L1054
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L986
.L987:
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%rcx, %rdx
	jne	.L1055
.L963:
	cmpb	$0, (%rax)
	jne	.L1056
.L965:
	movl	-560(%rbp), %eax
	testl	%eax, %eax
	js	.L975
.L986:
	movq	-424(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1057
	js	.L1058
	movq	56(%rbx), %rax
	movl	-552(%rbp), %ecx
	leaq	-536(%rbp), %r8
	movq	%r13, %rdi
	movq	$0, -536(%rbp)
	movq	(%rax), %rsi
	call	_ZN4node6fs_dirL17DirentListToArrayEPNS_11EnvironmentEP11uv_dirent_siNS_8encodingEPN2v85LocalINS6_5ValueEEE.isra.0.constprop.0
	testq	%rax, %rax
	je	.L1059
	movq	(%rax), %rdx
	movq	(%r12), %rax
	movq	%rdx, 24(%rax)
.L975:
	movq	%r14, %rdi
	call	uv_fs_req_cleanup@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L914
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L933:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L928:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L926:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	(%r12), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -568(%rbp)
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L944:
	movq	$0, 192(%r8)
	cltq
	movq	%r13, %rdi
	movq	%rax, 176(%r8)
	call	_ZN4node6fs_dirL12AfterDirReadEP7uv_fs_s
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L915:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L1046:
	movq	%rax, %rsi
	leaq	64(%rbx), %rdi
	movq	%rax, -560(%rbp)
	subq	%rdx, %rsi
	call	_ZNSt6vectorI11uv_dirent_sSaIS0_EE17_M_default_appendEm
	movq	64(%rbx), %rcx
	movq	-560(%rbp), %rax
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L919:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic265(%rip), %r14
	testq	%r14, %r14
	je	.L1060
.L951:
	testb	$5, (%r14)
	je	.L947
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -528(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L953
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1061
.L953:
	movq	-520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L954
	movq	(%rdi), %rax
	call	*8(%rax)
.L954:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L947
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L936:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic268(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1062
.L967:
	testb	$5, (%rdx)
	je	.L965
	pxor	%xmm0, %xmm0
	movq	%rdx, -568(%rbp)
	movaps	%xmm0, -528(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L970
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-568(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1063
.L970:
	movq	-520(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L971
	movq	(%rdi), %rax
	call	*8(%rax)
.L971:
	movq	-528(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L965
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1045:
	leaq	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	352(%r13), %r8
	movl	%eax, %esi
	movq	3280(%r13), %rax
	movq	%r8, %rdi
	movq	%r8, -584(%rbp)
	movq	%rax, -576(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-576(%rbp), %rsi
	movq	-568(%rbp), %rdi
	movq	%rax, %rcx
	movq	360(%r13), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-584(%rbp), %r8
	testb	%al, %al
	je	.L1064
.L959:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC21(%rip), %rsi
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1065
.L960:
	movq	360(%r13), %rax
	movq	-576(%rbp), %rsi
	movq	-568(%rbp), %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1066
.L961:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L987
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	(%r12), %rax
	cmpq	$-104, %r15
	je	.L1067
	movq	104(%r15), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	%rcx, -560(%rbp)
	leaq	.LC16(%rip), %rsi
	call	*%rdx
	movq	-560(%rbp), %rcx
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L937:
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r8
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%rcx, -568(%rbp)
	leaq	.LC16(%rip), %rsi
	call	*%rdx
	movq	-568(%rbp), %rcx
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1059:
	cmpl	$2, 16(%r12)
	jg	.L1068
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L982:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	-536(%rbp), %rcx
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1047:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1050:
	leaq	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1060:
	movq	%rcx, -560(%rbp)
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L952
	movq	(%rax), %rax
	movq	-560(%rbp), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1069
.L952:
	movq	%r14, _ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic265(%rip)
	mfence
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1062:
	movq	%rcx, -568(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L968
	movq	(%rax), %rax
	movq	-568(%rbp), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1070
.L968:
	movq	%rdx, _ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic268(%rip)
	mfence
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L1058:
	leaq	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	8(%r12), %rdi
	subq	$16, %rdi
	jmp	.L982
.L1066:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L961
.L1065:
	movq	%rax, -584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-584(%rbp), %rcx
	jmp	.L960
.L1064:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-584(%rbp), %r8
	jmp	.L959
.L1067:
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L975
.L1070:
	leaq	.LC16(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L968
.L1069:
	leaq	.LC16(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L952
.L1063:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$69, %esi
	leaq	-528(%rbp), %rcx
	pushq	$0
	pushq	%rcx
	leaq	.LC22(%rip), %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L970
.L1061:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$66, %esi
	leaq	-528(%rbp), %rdx
	pushq	$0
	leaq	.LC22(%rip), %rcx
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L953
.L1048:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7737:
	.size	_ZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s, @function
_GLOBAL__sub_I__ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s:
.LFB10754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10754:
	.size	_GLOBAL__sub_I__ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s, .-_GLOBAL__sub_I__ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node6fs_dir9DirHandleC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEP8uv_dir_s
	.weak	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args
	.section	.rodata.str1.1
.LC23:
	.string	"../src/node_file-inl.h:152"
.LC24:
	.string	"finished_"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"node::fs::FSReqPromise<AliasedBufferT>::~FSReqPromise() [with AliasedBufferT = node::AliasedBufferBase<long unsigned int, v8::BigUint64Array>]"
	.section	.data.rel.ro.local._ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args,"awG",@progbits,_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args,comdat
	.align 16
	.type	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args, @gnu_unique_object
	.size	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args, 24
_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED4EvE4args:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.weak	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"node::fs::FSReqPromise<AliasedBufferT>::~FSReqPromise() [with AliasedBufferT = node::AliasedBufferBase<double, v8::Float64Array>]"
	.section	.data.rel.ro.local._ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args,"awG",@progbits,_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args,comdat
	.align 16
	.type	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args, @gnu_unique_object
	.size	_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args, 24
_ZZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED4EvE4args:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC26
	.weak	_ZTVN4node11ReqWrapBaseE
	.section	.data.rel.ro._ZTVN4node11ReqWrapBaseE,"awG",@progbits,_ZTVN4node11ReqWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11ReqWrapBaseE, @object
	.size	_ZTVN4node11ReqWrapBaseE, 48
_ZTVN4node11ReqWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE
	.section	.data.rel.ro._ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE,"awG",@progbits,_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE,comdat
	.align 8
	.type	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE, @object
	.size	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE, 192
_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED1Ev
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseImN2v814BigUint64ArrayEvEEED0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE
	.section	.data.rel.ro._ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE,"awG",@progbits,_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE,comdat
	.align 8
	.type	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE, @object
	.size	_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE, 192
_ZTVN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZN4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE6RejectENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE7ResolveENS3_5LocalINS3_5ValueEEE
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE11ResolveStatEPK9uv_stat_t
	.quad	_ZN4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEE14SetReturnValueERKNS3_20FunctionCallbackInfoINS3_5ValueEEE
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED1Ev
	.quad	_ZThn56_N4node2fs12FSReqPromiseINS_17AliasedBufferBaseIdN2v812Float64ArrayEvEEED0Ev
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI7uv_fs_sE12GetAsyncWrapEv
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node6fs_dir9DirHandleE
	.section	.data.rel.ro._ZTVN4node6fs_dir9DirHandleE,"awG",@progbits,_ZTVN4node6fs_dir9DirHandleE,comdat
	.align 8
	.type	_ZTVN4node6fs_dir9DirHandleE, @object
	.size	_ZTVN4node6fs_dir9DirHandleE, 96
_ZTVN4node6fs_dir9DirHandleE:
	.quad	0
	.quad	0
	.quad	_ZN4node6fs_dir9DirHandleD1Ev
	.quad	_ZN4node6fs_dir9DirHandleD0Ev
	.quad	_ZNK4node6fs_dir9DirHandle10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6fs_dir9DirHandle14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6fs_dir9DirHandle8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_EE
	.section	.data.rel.ro.local._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_EE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_EE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E_E4CallES2_
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_EE
	.section	.data.rel.ro.local._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_EE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_EE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6fs_dir9DirHandle7GCCloseEvEUlS2_E0_E4CallES2_
	.weak	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args
	.section	.rodata.str1.1
.LC27:
	.string	"../src/req_wrap-inl.h:130"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"(req_wrap->original_callback_) == nullptr"
	.align 8
.LC29:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, vo"
	.string	"id (*)(ReqT*, Args ...)>::For(node::ReqWrap<T>*, node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_fs_s; Args = {}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_fs_s*)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI7uv_fs_sPFvPS1_EE3ForEPNS_7ReqWrapIS1_EES4_E4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC30:
	.string	"../src/util-inl.h:374"
.LC31:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC33:
	.string	"../src/util-inl.h:325"
.LC34:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm64EEixEmE4args
	.section	.rodata.str1.1
.LC36:
	.string	"../src/util.h:352"
.LC37:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 64; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm64EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm64EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm64EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm64EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm64EEixEmE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_dir.cc"
.LC40:
	.string	"fs_dir"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC39
	.quad	0
	.quad	_ZN4node6fs_dir10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC40
	.quad	0
	.quad	0
	.local	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic336
	.comm	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic336,8,8
	.local	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic333
	.comm	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic333,8,8
	.section	.rodata.str1.1
.LC41:
	.string	"../src/node_dir.cc:331"
.LC42:
	.string	"(argc) == (4)"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::fs_dir::OpenDir(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/node_dir.cc:322"
.LC45:
	.string	"(*path) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC43
	.section	.rodata.str1.1
.LC46:
	.string	"../src/node_dir.cc:319"
.LC47:
	.string	"(argc) >= (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node6fs_dirL7OpenDirERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC43
	.section	.rodata.str1.1
.LC48:
	.string	"../src/node_dir.cc:280"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"(req_wrap_sync.req.result) >= (0)"
	.align 8
.LC50:
	.string	"static void node::fs_dir::DirHandle::Read(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.local	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic268
	.comm	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic268,8,8
	.local	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic265
	.comm	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic265,8,8
	.section	.rodata.str1.1
.LC51:
	.string	"../src/node_dir.cc:263"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC51
	.quad	.LC42
	.quad	.LC50
	.section	.rodata.str1.1
.LC52:
	.string	"../src/node_dir.cc:249"
.LC53:
	.string	"args[1]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC52
	.quad	.LC53
	.quad	.LC50
	.section	.rodata.str1.1
.LC54:
	.string	"../src/node_dir.cc:242"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6fs_dir9DirHandle4ReadERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC54
	.quad	.LC47
	.quad	.LC50
	.local	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic164
	.comm	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic164,8,8
	.local	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic161
	.comm	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic161,8,8
	.section	.rodata.str1.1
.LC55:
	.string	"../src/node_dir.cc:159"
.LC56:
	.string	"(argc) == (2)"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"static void node::fs_dir::DirHandle::Close(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.section	.rodata.str1.1
.LC58:
	.string	"../src/node_dir.cc:146"
.LC59:
	.string	"(argc) >= (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6fs_dir9DirHandle5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC57
	.section	.rodata.str1.1
.LC60:
	.string	"../src/node_dir.cc:85"
.LC61:
	.string	"closed_"
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"virtual node::fs_dir::DirHandle::~DirHandle()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandleD4EvE4args_0, @object
	.size	_ZZN4node6fs_dir9DirHandleD4EvE4args_0, 24
_ZZN4node6fs_dir9DirHandleD4EvE4args_0:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.section	.rodata.str1.1
.LC63:
	.string	"../src/node_dir.cc:83"
.LC64:
	.string	"!closing_"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandleD4EvE4args, @object
	.size	_ZZN4node6fs_dir9DirHandleD4EvE4args, 24
_ZZN4node6fs_dir9DirHandleD4EvE4args:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC62
	.section	.rodata.str1.1
.LC65:
	.string	"../src/node_dir.cc:79"
.LC66:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"static void node::fs_dir::DirHandle::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6fs_dir9DirHandle3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.weak	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC68:
	.string	"../src/req_wrap-inl.h:13"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC70:
	.string	"node::ReqWrapBase::ReqWrapBase(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, 24
_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC71:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC73:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC74:
	.string	"../src/base_object-inl.h:195"
.LC75:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC73
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC76:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC78:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC79:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC81:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC79
	.quad	.LC80
	.quad	.LC81
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC82:
	.string	"../src/env-inl.h:399"
.LC83:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	2334956331002195011
	.quad	8245937412991248740
	.align 16
.LC4:
	.quad	7308326720558866553
	.quad	7093839458615586592
	.align 16
.LC5:
	.quad	7812741925116929889
	.quad	7359003215913050981
	.align 16
.LC7:
	.quad	0
	.quad	64
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC9:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16
	.align 16
.LC10:
	.quad	18
	.quad	0
	.section	.rodata.cst8
	.align 8
.LC20:
	.long	0
	.long	1138753536
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
