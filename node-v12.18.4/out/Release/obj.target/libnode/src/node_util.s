	.file	"node_util.cc"
	.text
	.section	.text._ZNK4node11Environment26alpn_buffer_private_symbolEv,"axG",@progbits,_ZNK4node11Environment26alpn_buffer_private_symbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment26alpn_buffer_private_symbolEv
	.type	_ZNK4node11Environment26alpn_buffer_private_symbolEv, @function
_ZNK4node11Environment26alpn_buffer_private_symbolEv:
.LFB6837:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	64(%rax), %rax
	ret
	.cfi_endproc
.LFE6837:
	.size	_ZNK4node11Environment26alpn_buffer_private_symbolEv, .-_ZNK4node11Environment26alpn_buffer_private_symbolEv
	.section	.text._ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv,"axG",@progbits,_ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv
	.type	_ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv, @function
_ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv:
.LFB6838:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	72(%rax), %rax
	ret
	.cfi_endproc
.LFE6838:
	.size	_ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv, .-_ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv
	.section	.text._ZNK4node11Environment28arrow_message_private_symbolEv,"axG",@progbits,_ZNK4node11Environment28arrow_message_private_symbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment28arrow_message_private_symbolEv
	.type	_ZNK4node11Environment28arrow_message_private_symbolEv, @function
_ZNK4node11Environment28arrow_message_private_symbolEv:
.LFB6839:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	80(%rax), %rax
	ret
	.cfi_endproc
.LFE6839:
	.size	_ZNK4node11Environment28arrow_message_private_symbolEv, .-_ZNK4node11Environment28arrow_message_private_symbolEv
	.section	.text._ZNK4node11Environment33contextify_context_private_symbolEv,"axG",@progbits,_ZNK4node11Environment33contextify_context_private_symbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment33contextify_context_private_symbolEv
	.type	_ZNK4node11Environment33contextify_context_private_symbolEv, @function
_ZNK4node11Environment33contextify_context_private_symbolEv:
.LFB6840:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	88(%rax), %rax
	ret
	.cfi_endproc
.LFE6840:
	.size	_ZNK4node11Environment33contextify_context_private_symbolEv, .-_ZNK4node11Environment33contextify_context_private_symbolEv
	.section	.text._ZNK4node11Environment32contextify_global_private_symbolEv,"axG",@progbits,_ZNK4node11Environment32contextify_global_private_symbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment32contextify_global_private_symbolEv
	.type	_ZNK4node11Environment32contextify_global_private_symbolEv, @function
_ZNK4node11Environment32contextify_global_private_symbolEv:
.LFB6841:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	96(%rax), %rax
	ret
	.cfi_endproc
.LFE6841:
	.size	_ZNK4node11Environment32contextify_global_private_symbolEv, .-_ZNK4node11Environment32contextify_global_private_symbolEv
	.section	.text._ZNK4node11Environment24decorated_private_symbolEv,"axG",@progbits,_ZNK4node11Environment24decorated_private_symbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment24decorated_private_symbolEv
	.type	_ZNK4node11Environment24decorated_private_symbolEv, @function
_ZNK4node11Environment24decorated_private_symbolEv:
.LFB6842:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	104(%rax), %rax
	ret
	.cfi_endproc
.LFE6842:
	.size	_ZNK4node11Environment24decorated_private_symbolEv, .-_ZNK4node11Environment24decorated_private_symbolEv
	.section	.text._ZNK4node11Environment12napi_wrapperEv,"axG",@progbits,_ZNK4node11Environment12napi_wrapperEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment12napi_wrapperEv
	.type	_ZNK4node11Environment12napi_wrapperEv, @function
_ZNK4node11Environment12napi_wrapperEv:
.LFB6843:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	112(%rax), %rax
	ret
	.cfi_endproc
.LFE6843:
	.size	_ZNK4node11Environment12napi_wrapperEv, .-_ZNK4node11Environment12napi_wrapperEv
	.section	.text._ZNK4node11Environment26sab_lifetimepartner_symbolEv,"axG",@progbits,_ZNK4node11Environment26sab_lifetimepartner_symbolEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment26sab_lifetimepartner_symbolEv
	.type	_ZNK4node11Environment26sab_lifetimepartner_symbolEv, @function
_ZNK4node11Environment26sab_lifetimepartner_symbolEv:
.LFB6844:
	.cfi_startproc
	endbr64
	movq	360(%rdi), %rax
	movq	120(%rax), %rax
	ret
	.cfi_endproc
.LFE6844:
	.size	_ZNK4node11Environment26sab_lifetimepartner_symbolEv, .-_ZNK4node11Environment26sab_lifetimepartner_symbolEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7245:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7245:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNK4node4util13WeakReference8SelfSizeEv,"axG",@progbits,_ZNK4node4util13WeakReference8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4util13WeakReference8SelfSizeEv
	.type	_ZNK4node4util13WeakReference8SelfSizeEv, @function
_ZNK4node4util13WeakReference8SelfSizeEv:
.LFB7298:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE7298:
	.size	_ZNK4node4util13WeakReference8SelfSizeEv, .-_ZNK4node4util13WeakReference8SelfSizeEv
	.section	.text._ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7299:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7299:
	.size	_ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE
	.text
	.p2align 4
	.globl	_ZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jg	.L14
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L22
.L16:
	movl	16(%rbx), %eax
	movq	(%rbx), %r12
	testl	%eax, %eax
	jle	.L23
	movq	8(%rbx), %rdi
.L18:
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	movq	8(%r12), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L16
.L22:
	leaq	_ZZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	movq	8(%r12), %rax
	leaq	88(%rax), %rdi
	jmp	.L18
	.cfi_endproc
.LFE7289:
	.size	_ZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7294:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L36
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L31
	cmpw	$1040, %cx
	jne	.L26
.L31:
	movq	23(%rdx), %rax
.L28:
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L24
	movq	(%rbx), %rbx
	movq	(%rax), %rsi
	movq	8(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L37
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L24:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L24
	.cfi_endproc
.LFE7294:
	.size	_ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.text
	.p2align 4
	.type	_ZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7288:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jg	.L39
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L45
.L41:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L46
	movq	8(%rbx), %rdi
.L43:
	call	_ZNK2v86Uint325ValueEv@PLT
	addq	$8, %rsp
	popq	%rbx
	movl	%eax, %edi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_sleep@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	jne	.L41
.L45:
	leaq	_ZZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L43
	.cfi_endproc
.LFE7288:
	.size	_ZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7279:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jg	.L48
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L56
.L50:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L57
	movq	8(%rbx), %rdi
.L52:
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L58
	movq	(%rax), %rax
.L54:
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L50
.L56:
	leaq	_ZZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L57:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L58:
	movq	16(%rdx), %rax
	jmp	.L54
	.cfi_endproc
.LFE7279:
	.size	_ZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L81
	cmpw	$1040, %cx
	jne	.L60
.L81:
	movq	23(%rdx), %r13
.L62:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L63
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L89
.L65:
	cmpl	$1, 16(%rbx)
	jle	.L90
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L67:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L91
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L69
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	movq	%r12, %rdi
.L72:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$7, %eax
	ja	.L92
	movl	%eax, %eax
	leaq	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods(%rip), %rdx
	salq	$4, %rax
	addq	%rdx, %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rax
	addq	%r13, %rdi
	testb	$1, %al
	je	.L74
	movq	(%rdi), %rdx
	movq	-1(%rdx,%rax), %rax
.L74:
	call	*%rax
	cmpl	$2, 16(%rbx)
	movq	%rax, %rdx
	jg	.L75
	movq	(%rbx), %rax
	movq	8(%rax), %rcx
	addq	$88, %rcx
.L76:
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L59
	movq	(%rbx), %rdx
	movzbl	%ah, %eax
	cmpb	$1, %al
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L63:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L65
.L89:
	leaq	_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	movq	8(%rbx), %r12
	cmpl	$1, %eax
	je	.L93
	leaq	-8(%r12), %rdi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rcx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L60:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L93:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L72
	.cfi_endproc
.LFE7287:
	.size	_ZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node4utilL17GetPromiseDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL17GetPromiseDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7280:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L95
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L96:
	call	_ZNK2v85Value9IsPromiseEv@PLT
	testb	%al, %al
	je	.L94
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	movl	16(%rbx), %eax
	leaq	88(%r13), %r14
	testl	%eax, %eax
	jle	.L99
	movq	8(%rbx), %r14
.L99:
	movq	%r14, %rdi
	call	_ZN2v87Promise5StateEv@PLT
	movq	%r13, %rdi
	movl	%eax, %esi
	movl	%eax, %r12d
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	$0, -56(%rbp)
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	testl	%r12d, %r12d
	jne	.L110
.L100:
	leaq	-64(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L111
	movq	(%rax), %rax
.L102:
	movq	%rax, 24(%rdx)
.L94:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v87Promise6ResultEv@PLT
	movl	$2, %edx
	movq	%rax, -56(%rbp)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L95:
	movq	8(%rdi), %rdi
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L111:
	movq	16(%rdx), %rax
	jmp	.L102
.L112:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7280:
	.size	_ZN4node4utilL17GetPromiseDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL17GetPromiseDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node4utilL15GetProxyDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL15GetProxyDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L114
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L115:
	call	_ZNK2v85Value7IsProxyEv@PLT
	testb	%al, %al
	je	.L113
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L117
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, %r12
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	je	.L120
.L119:
	movq	%r12, %rdi
	call	_ZN2v85Proxy9GetTargetEv@PLT
	movq	%r12, %rdi
	movq	%rax, -48(%rbp)
	call	_ZN2v85Proxy10GetHandlerEv@PLT
	movq	(%rbx), %rbx
	leaq	-48(%rbp), %rsi
	movl	$2, %edx
	movq	%rax, -40(%rbp)
	movq	8(%rbx), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L137
	movq	(%rax), %rax
.L125:
	movq	%rax, 24(%rbx)
.L113:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L138
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L117:
	movq	8(%rbx), %r12
	cmpl	$1, %eax
	je	.L119
	leaq	-8(%r12), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	jne	.L119
.L120:
	movq	%r12, %rdi
	call	_ZN2v85Proxy9GetTargetEv@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L139
	movq	(%rax), %rax
.L126:
	movq	%rax, 24(%rdx)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L137:
	movq	16(%rbx), %rax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L139:
	movq	16(%rdx), %rax
	jmp	.L126
.L138:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7281:
	.size	_ZN4node4utilL15GetProxyDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL15GetProxyDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev:
.LFB7297:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1668179314, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7306638850118477143, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$101, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE7297:
	.size	_ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev, .-_ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7295:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L155
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L147
	cmpw	$1040, %cx
	jne	.L143
.L147:
	movq	23(%rdx), %rax
.L145:
	movq	40(%rax), %rsi
	movq	32(%rax), %rdi
	leaq	1(%rsi), %rdx
	movq	%rdx, 40(%rax)
	cmpq	$1, %rdx
	jne	.L141
	testq	%rdi, %rdi
	jne	.L156
.L141:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7295:
	.size	_ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7296:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L172
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L164
	cmpw	$1040, %cx
	jne	.L159
.L164:
	movq	23(%rdx), %rax
.L161:
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L173
	subq	$1, %rdx
	cmpq	$0, 32(%rax)
	movq	%rdx, 40(%rax)
	je	.L157
	testq	%rdx, %rdx
	je	.L174
.L157:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	32(%rax), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V88MakeWeakEPPm@PLT
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	_ZZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7296:
	.size	_ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.text
	.p2align 4
	.type	_ZN4node4utilL14PreviewEntriesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL14PreviewEntriesERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L176
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L203
.L175:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L204
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L175
.L203:
	movq	(%rbx), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L193
	cmpw	$1040, %cx
	jne	.L179
.L193:
	movq	23(%rdx), %r12
.L181:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L182
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L183:
	leaq	-65(%rbp), %rsi
	call	_ZN2v86Object14PreviewEntriesEPb@PLT
	testq	%rax, %rax
	je	.L175
	cmpl	$1, 16(%rbx)
	movq	(%rbx), %r13
	je	.L189
	movq	352(%r12), %rdi
	cmpb	$0, -65(%rbp)
	movq	%rax, -64(%rbp)
	leaq	120(%rdi), %rax
	jne	.L205
.L188:
	leaq	-64(%rbp), %rsi
	movl	$2, %edx
	movq	%rax, -56(%rbp)
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L206
.L189:
	movq	(%rax), %rax
	movq	%rax, 24(%r13)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L182:
	movq	8(%rbx), %rdi
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	112(%rdi), %rax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L179:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L206:
	movq	16(%r13), %rax
	movq	%rax, 24(%r13)
	jmp	.L175
.L204:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7283:
	.size	_ZN4node4utilL14PreviewEntriesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL14PreviewEntriesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"UNKNOWN"
.LC1:
	.string	"TCP"
.LC2:
	.string	"UDP"
.LC3:
	.string	"FILE"
.LC4:
	.string	"PIPE"
.LC5:
	.string	"TTY"
	.text
	.p2align 4
	.type	_ZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L226
	cmpw	$1040, %cx
	jne	.L208
.L226:
	movq	23(%rdx), %r12
.L210:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L211
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L212:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testb	%al, %al
	je	.L207
	sarq	$32, %rdi
	testl	%edi, %edi
	js	.L228
	call	uv_guess_handle@PLT
	cmpl	$17, %eax
	ja	.L215
	leaq	.L217(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L217:
	.long	.L222-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L221-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L215-.L217
	.long	.L220-.L217
	.long	.L215-.L217
	.long	.L219-.L217
	.long	.L225-.L217
	.long	.L215-.L217
	.long	.L216-.L217
	.text
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	.LC4(%rip), %rsi
.L218:
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%rbx), %rbx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L229
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L207:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L208:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	.LC0(%rip), %rsi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	.LC3(%rip), %rsi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	.LC1(%rip), %rsi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	_ZZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L229:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L207
.L215:
	call	_ZN4node5AbortEv@PLT
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	.LC2(%rip), %rsi
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L219:
	leaq	.LC5(%rip), %rsi
	jmp	.L218
	.cfi_endproc
.LFE7300:
	.size	_ZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L246
	cmpw	$1040, %cx
	jne	.L231
.L246:
	movq	23(%rdx), %rax
.L233:
	movq	3280(%rax), %r13
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L234
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L248
.L236:
	cmpl	$1, 16(%rbx)
	jle	.L249
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L238:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L250
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L240
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	movq	%r12, %rdi
.L243:
	call	_ZNK2v86Uint325ValueEv@PLT
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%r13, %rsi
	movl	%eax, %ecx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v86Object16GetPropertyNamesENS_5LocalINS_7ContextEEENS_17KeyCollectionModeENS_14PropertyFilterENS_11IndexFilterENS_17KeyConversionModeE@PLT
	testq	%rax, %rax
	je	.L230
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L230:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L249:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L234:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L236
.L248:
	leaq	_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L240:
	movq	8(%rbx), %r12
	cmpl	$1, %eax
	je	.L251
	leaq	-8(%r12), %rdi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L231:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L250:
	leaq	_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L251:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L243
	.cfi_endproc
.LFE7278:
	.size	_ZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7286:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L270
	cmpw	$1040, %cx
	jne	.L253
.L270:
	movq	23(%rdx), %r13
.L255:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L256
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L275
.L258:
	cmpl	$1, 16(%rbx)
	jle	.L276
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L260:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L277
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L262
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	movq	%r12, %rdi
.L265:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$7, %eax
	ja	.L278
	movl	%eax, %eax
	leaq	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods(%rip), %rdx
	salq	$4, %rax
	addq	%rdx, %rax
	movq	8(%rax), %rdi
	movq	(%rax), %rax
	addq	%r13, %rdi
	testb	$1, %al
	je	.L267
	movq	(%rdi), %rdx
	movq	-1(%rdx,%rax), %rax
.L267:
	call	*%rax
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	testq	%rax, %rax
	je	.L252
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L252:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L256:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L258
.L275:
	leaq	_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	movq	8(%rbx), %r12
	cmpl	$1, %eax
	je	.L279
	leaq	-8(%r12), %rdi
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L253:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L277:
	leaq	_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L278:
	leaq	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L279:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L265
	.cfi_endproc
.LFE7286:
	.size	_ZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L281
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L281:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L291
.L282:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L283
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L282
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7243:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.1
.LC6:
	.string	"alpn_buffer_private_symbol"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"arraybuffer_untransferable_private_symbol"
	.section	.rodata.str1.1
.LC8:
	.string	"arrow_message_private_symbol"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"contextify_context_private_symbol"
	.align 8
.LC10:
	.string	"contextify_global_private_symbol"
	.section	.rodata.str1.1
.LC11:
	.string	"decorated_private_symbol"
.LC12:
	.string	"napi_wrapper"
.LC13:
	.string	"sab_lifetimepartner_symbol"
.LC14:
	.string	"kPending"
.LC15:
	.string	"kFulfilled"
.LC16:
	.string	"kRejected"
.LC17:
	.string	"getHiddenValue"
.LC18:
	.string	"setHiddenValue"
.LC19:
	.string	"getPromiseDetails"
.LC20:
	.string	"getProxyDetails"
.LC21:
	.string	"previewEntries"
.LC22:
	.string	"getOwnNonIndexProperties"
.LC23:
	.string	"getConstructorName"
.LC24:
	.string	"sleep"
.LC25:
	.string	"arrayBufferViewHasBuffer"
.LC26:
	.string	"ALL_PROPERTIES"
.LC28:
	.string	"ONLY_WRITABLE"
.LC30:
	.string	"ONLY_ENUMERABLE"
.LC32:
	.string	"ONLY_CONFIGURABLE"
.LC34:
	.string	"SKIP_STRINGS"
.LC36:
	.string	"SKIP_SYMBOLS"
.LC38:
	.string	"propertyFilter"
.LC39:
	.string	"shouldAbortOnUncaughtToggle"
.LC40:
	.string	"WeakReference"
.LC41:
	.string	"get"
.LC42:
	.string	"incRef"
.LC43:
	.string	"decRef"
.LC44:
	.string	"guessHandleType"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB45:
	.text
.LHOTB45:
	.p2align 4
	.globl	_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L293
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L293
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L293
	movq	271(%rax), %rbx
	xorl	%esi, %esi
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$26, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L377
.L294:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L378
.L295:
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$41, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L379
.L296:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L380
.L297:
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$28, %ecx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L381
.L298:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L382
.L299:
	movq	352(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$33, %ecx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L383
.L300:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L384
.L301:
	movq	352(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$32, %ecx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L385
.L302:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L386
.L303:
	movq	352(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$24, %ecx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L387
.L304:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L388
.L305:
	movq	352(%rbx), %rdi
	movl	$6, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L389
.L306:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L390
.L307:
	movq	352(%rbx), %rdi
	movl	$7, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$26, %ecx
	leaq	.LC13(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L391
.L308:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L392
.L309:
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L393
.L310:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L394
.L311:
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L395
.L312:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L396
.L313:
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L397
.L314:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L398
.L315:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L399
.L316:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L400
.L317:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L401
.L318:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L402
.L319:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L403
.L320:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L404
.L321:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node4utilL17GetPromiseDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L405
.L322:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L406
.L323:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L407
.L324:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node4utilL15GetProxyDetailsERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L408
.L325:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L409
.L326:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L410
.L327:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node4utilL14PreviewEntriesERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	movq	%rax, %r14
	popq	%rax
	testq	%r14, %r14
	je	.L411
.L328:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L412
.L329:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L413
.L330:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L414
.L331:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L415
.L332:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L416
.L333:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L417
.L334:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L418
.L335:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L419
.L336:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L420
.L337:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L421
.L338:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L422
.L339:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L423
.L340:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L424
.L341:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L425
.L342:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC26(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L426
.L343:
	movq	%r15, %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L427
.L344:
	movq	%r14, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC28(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L428
.L345:
	movsd	.LC29(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L429
.L346:
	movq	%r14, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC30(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L430
.L347:
	movsd	.LC31(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L431
.L348:
	movq	%r14, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC32(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L432
.L349:
	movsd	.LC33(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L433
.L350:
	movq	%r14, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC34(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L434
.L351:
	movsd	.LC35(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L435
.L352:
	movq	%r14, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC36(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L436
.L353:
	movsd	.LC37(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L437
.L354:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$14, %ecx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L438
.L355:
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L439
.L356:
	movq	352(%rbx), %rdi
	movl	$27, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L440
.L357:
	movq	1800(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L358
	movq	(%rcx), %rsi
	movq	1768(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
.L358:
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L441
	shrw	$8, %ax
	je	.L442
.L360:
	movq	352(%rbx), %rdi
	movl	$13, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L443
.L361:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node4util13WeakReference3GetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC41(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L444
.L362:
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4util13WeakReference6IncRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC42(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L445
.L363:
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC43(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L446
.L364:
	movq	%r14, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L447
.L365:
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L448
.L366:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L449
.L367:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC44(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L450
.L368:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L451
.L369:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L380:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L382:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L384:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L385:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L386:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L388:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L389:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L390:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L392:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L394:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L396:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L397:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L398:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L399:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L401:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L402:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L403:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L404:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L405:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L407:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L408:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L410:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L411:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L412:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L413:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L414:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L416:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L417:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L419:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L420:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L422:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L423:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L425:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L427:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L429:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L431:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L433:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L435:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L437:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L439:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L440:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L441:
	movl	%eax, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-56(%rbp), %eax
	shrw	$8, %ax
	jne	.L360
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	_ZZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L446:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L451:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L443:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L448:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L449:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L363
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7301:
.L293:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7301:
	.text
	.size	_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE45:
	.text
.LHOTE45:
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7231:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L453
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L454:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L453
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L453
	movq	%r9, %rdi
.L456:
	cmpq	%rsi, %rbx
	jne	.L454
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L454
	cmpq	16(%rdi), %rbx
	jne	.L454
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L488
	testq	%rsi, %rsi
	je	.L458
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L458
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L458:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L453:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L489
.L461:
	cmpq	$0, 8(%rbx)
	je	.L452
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L465
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L490
.L465:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L452
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L452:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L491
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L470
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L458
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L457:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L492
.L459:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L490:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L470:
	movq	%r10, %rax
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L489:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L493
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L461
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L492:
	movq	%rsi, 2600(%r12)
	jmp	.L459
.L493:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L491:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7231:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN4node4util13WeakReferenceD2Ev,"axG",@progbits,_ZN4node4util13WeakReferenceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4util13WeakReferenceD2Ev
	.type	_ZN4node4util13WeakReferenceD2Ev, @function
_ZN4node4util13WeakReferenceD2Ev:
.LFB9285:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4util13WeakReferenceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L495
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L495:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.cfi_endproc
.LFE9285:
	.size	_ZN4node4util13WeakReferenceD2Ev, .-_ZN4node4util13WeakReferenceD2Ev
	.weak	_ZN4node4util13WeakReferenceD1Ev
	.set	_ZN4node4util13WeakReferenceD1Ev,_ZN4node4util13WeakReferenceD2Ev
	.section	.text._ZN4node4util13WeakReferenceD0Ev,"axG",@progbits,_ZN4node4util13WeakReferenceD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node4util13WeakReferenceD0Ev
	.type	_ZN4node4util13WeakReferenceD0Ev, @function
_ZN4node4util13WeakReferenceD0Ev:
.LFB9287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node4util13WeakReferenceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L501
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L501:
	movq	%r12, %rdi
	call	_ZN4node10BaseObjectD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9287:
	.size	_ZN4node4util13WeakReferenceD0Ev, .-_ZN4node4util13WeakReferenceD0Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7233:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.text
	.p2align 4
	.globl	_Z14_register_utilv
	.type	_Z14_register_utilv, @function
_Z14_register_utilv:
.LFB7302:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7302:
	.size	_Z14_register_utilv, .-_Z14_register_utilv
	.section	.text._ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB8656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movdqu	0(%r13), %xmm0
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	$0, (%rax)
	movups	%xmm0, 8(%rax)
	movq	16(%r13), %rax
	movq	16(%r12), %r13
	movq	%rax, 24(%r12)
	movq	%r13, %rax
	divq	%rsi
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	leaq	0(,%rdx,8), %r15
	testq	%rdi, %rdi
	je	.L510
	movq	(%rdi), %rax
	movq	%rdx, %r8
	movq	32(%rax), %rcx
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L511:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L510
	movq	32(%r9), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L510
	movq	%r9, %rax
.L513:
	cmpq	%rcx, %r13
	jne	.L511
	movq	8(%rax), %rcx
	cmpq	%rcx, 8(%r12)
	jne	.L511
	cmpq	16(%rax), %r13
	jne	.L511
	movq	(%rdi), %r14
	testq	%r14, %r14
	je	.L510
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r14, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L510:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L515
	movq	(%rbx), %r8
	movq	%r13, 32(%r12)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L525
.L549:
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%r15), %rax
	movq	%r12, (%rax)
.L526:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L547
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L548
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L518:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L520
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L522:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L523:
	testq	%rsi, %rsi
	je	.L520
.L521:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L522
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L529
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L521
	.p2align 4,,10
	.p2align 3
.L520:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L524
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L524:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%rbx)
	divq	%r14
	movq	%r8, (%rbx)
	movq	%r13, 32(%r12)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L549
.L525:
	movq	16(%rbx), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L527
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r12, (%r8,%rdx,8)
.L527:
	leaq	16(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%rdx, %rdi
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L518
.L548:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8656:
	.size	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.text._ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L568
	cmpw	$1040, %cx
	jne	.L551
.L568:
	movq	23(%rdx), %r12
.L553:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L554
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L579
.L554:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L580
	movq	8(%rbx), %rdi
.L556:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L581
	movq	8(%rbx), %r13
	movl	16(%rbx), %edx
	leaq	8(%r13), %r14
	testl	%edx, %edx
	jg	.L558
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L558:
	movl	$48, %edi
	call	_Znwm@PLT
	movq	352(%r12), %rdi
	movq	%r14, %rsi
	movq	%rax, %rbx
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rbx)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%r12, %xmm1
	movq	$0, 24(%rbx)
	movq	%r14, %rdi
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L582
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	-64(%rbp), %rsi
	movq	%rbx, -56(%rbp)
	movq	2640(%r12), %rax
	leaq	2584(%r12), %rdi
	leaq	1(%rax), %rdx
	movq	%rax, -48(%rbp)
	movq	%rdx, 2640(%r12)
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rdx
	movq	%rdx, -64(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	testb	%dl, %dl
	je	.L583
	addq	$1, 2656(%r12)
	leaq	16+_ZTVN4node4util13WeakReferenceE(%rip), %rax
	movq	%rax, (%rbx)
	movq	24(%rbx), %rax
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	testq	%rax, %rax
	je	.L563
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L584
.L563:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	32(%rbx), %rdi
	movq	352(%r12), %r12
	testq	%rdi, %rdi
	je	.L561
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 32(%rbx)
.L561:
	testq	%r13, %r13
	je	.L564
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 32(%rbx)
.L564:
	leaq	32(%rbx), %rdi
	call	_ZN2v82V88MakeWeakEPPm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L585
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	addq	$88, %rdi
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L579:
	cmpl	$5, 43(%rax)
	jne	.L554
	leaq	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L551:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r12
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L584:
	movq	352(%r12), %r12
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L581:
	leaq	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L582:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L585:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7293:
	.size	_ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.weak	_ZTVN4node4util13WeakReferenceE
	.section	.data.rel.ro._ZTVN4node4util13WeakReferenceE,"awG",@progbits,_ZTVN4node4util13WeakReferenceE,comdat
	.align 8
	.type	_ZTVN4node4util13WeakReferenceE, @object
	.size	_ZTVN4node4util13WeakReferenceE, 88
_ZTVN4node4util13WeakReferenceE:
	.quad	0
	.quad	0
	.quad	_ZN4node4util13WeakReferenceD1Ev
	.quad	_ZN4node4util13WeakReferenceD0Ev
	.quad	_ZNK4node4util13WeakReference10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node4util13WeakReference14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node4util13WeakReference8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.section	.rodata.str1.1
.LC46:
	.string	"../src/node_util.cc"
.LC47:
	.string	"util"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC46
	.quad	0
	.quad	_ZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC47
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC48:
	.string	"../src/node_util.cc:315"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"target ->Set(env->context(), should_abort_on_uncaught_toggle, env->should_abort_on_uncaught_toggle().GetJSArray()) .FromJust()"
	.align 8
.LC50:
	.string	"void node::util::Initialize(v8::Local<v8::Object>, v8::Local<v8::Value>, v8::Local<v8::Context>, void*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvE4args, @object
	.size	_ZZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvE4args, 24
_ZZN4node4util10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.section	.rodata.str1.1
.LC51:
	.string	"../src/node_util.cc:234"
.LC52:
	.string	"(fd) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"void node::util::GuessHandleType(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4utilL15GuessHandleTypeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.weak	_ZZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC54:
	.string	"../src/node_util.cc:215"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"(weak_ref->reference_count_) >= (1)"
	.align 8
.LC56:
	.string	"static void node::util::WeakReference::DecRef(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local._ZZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4util13WeakReference6DecRefERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.weak	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0
	.section	.rodata.str1.1
.LC57:
	.string	"../src/node_util.cc:195"
.LC58:
	.string	"args[0]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"static void node::util::WeakReference::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local._ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.weak	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC60:
	.string	"../src/node_util.cc:194"
.LC61:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local._ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node4util13WeakReference3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC60
	.quad	.LC61
	.quad	.LC59
	.section	.rodata.str1.1
.LC62:
	.string	"../src/node_util.cc:179"
.LC63:
	.string	"args[0]->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"void node::util::ArrayBufferViewHasBuffer(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4util24ArrayBufferViewHasBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.section	.rodata.str1.1
.LC65:
	.string	"../src/node_util.cc:173"
.LC66:
	.string	"args[0]->IsUint32()"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"void node::util::Sleep(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4utilL5SleepERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.section	.rodata.str1.1
.LC68:
	.string	"../src/node_util.cc:162"
.LC69:
	.string	"args[1]->IsUint32()"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"void node::util::SetHiddenValue(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.section	.rodata.str1.1
.LC71:
	.string	"../src/node_util.cc:161"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4utilL14SetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC71
	.quad	.LC58
	.quad	.LC70
	.section	.rodata.str1.1
.LC72:
	.string	"../src/node_util.cc:148"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"void node::util::GetHiddenValue(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC72
	.quad	.LC69
	.quad	.LC73
	.section	.rodata.str1.1
.LC74:
	.string	"../src/node_util.cc:147"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4utilL14GetHiddenValueERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC74
	.quad	.LC58
	.quad	.LC73
	.weak	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args
	.section	.rodata.str1.1
.LC75:
	.string	"../src/node_util.cc:140"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"(index) < (arraysize(methods))"
	.align 8
.LC77:
	.string	"v8::Local<v8::Private> node::util::IndexToPrivateSymbol(node::Environment*, uint32_t)"
	.section	.data.rel.ro.local._ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args,"awG",@progbits,_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args,comdat
	.align 16
	.type	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args, @gnu_unique_object
	.size	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args, 24
_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE4args:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.weak	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods
	.section	.data.rel.ro.local._ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods,"awG",@progbits,_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods,comdat
	.align 32
	.type	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods, @gnu_unique_object
	.size	_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods, 128
_ZZN4node4util20IndexToPrivateSymbolEPNS_11EnvironmentEjE7methods:
	.quad	_ZNK4node11Environment26alpn_buffer_private_symbolEv
	.quad	0
	.quad	_ZNK4node11Environment41arraybuffer_untransferable_private_symbolEv
	.quad	0
	.quad	_ZNK4node11Environment28arrow_message_private_symbolEv
	.quad	0
	.quad	_ZNK4node11Environment33contextify_context_private_symbolEv
	.quad	0
	.quad	_ZNK4node11Environment32contextify_global_private_symbolEv
	.quad	0
	.quad	_ZNK4node11Environment24decorated_private_symbolEv
	.quad	0
	.quad	_ZNK4node11Environment12napi_wrapperEv
	.quad	0
	.quad	_ZNK4node11Environment26sab_lifetimepartner_symbolEv
	.quad	0
	.section	.rodata.str1.1
.LC78:
	.string	"../src/node_util.cc:62"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"void node::util::GetConstructorName(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4utilL18GetConstructorNameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC78
	.quad	.LC58
	.quad	.LC79
	.section	.rodata.str1.1
.LC80:
	.string	"../src/node_util.cc:41"
	.section	.rodata.str1.8
	.align 8
.LC81:
	.string	"void node::util::GetOwnNonIndexProperties(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC80
	.quad	.LC69
	.quad	.LC81
	.section	.rodata.str1.1
.LC82:
	.string	"../src/node_util.cc:40"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node4utilL24GetOwnNonIndexPropertiesERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC82
	.quad	.LC58
	.quad	.LC81
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC83:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC85:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC86:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC88:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC89:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC91:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC92:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC94:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC92
	.quad	.LC93
	.quad	.LC94
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC95:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC97:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC29:
	.long	0
	.long	1072693248
	.align 8
.LC31:
	.long	0
	.long	1073741824
	.align 8
.LC33:
	.long	0
	.long	1074790400
	.align 8
.LC35:
	.long	0
	.long	1075838976
	.align 8
.LC37:
	.long	0
	.long	1076887552
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
