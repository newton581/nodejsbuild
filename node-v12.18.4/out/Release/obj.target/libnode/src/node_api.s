	.file	"node_api.cc"
	.text
	.section	.text._ZN6v8impl10RefTrackerD2Ev,"axG",@progbits,_ZN6v8impl10RefTrackerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6v8impl10RefTrackerD2Ev
	.type	_ZN6v8impl10RefTrackerD2Ev, @function
_ZN6v8impl10RefTrackerD2Ev:
.LFB7467:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7467:
	.size	_ZN6v8impl10RefTrackerD2Ev, .-_ZN6v8impl10RefTrackerD2Ev
	.weak	_ZN6v8impl10RefTrackerD1Ev
	.set	_ZN6v8impl10RefTrackerD1Ev,_ZN6v8impl10RefTrackerD2Ev
	.section	.text._ZN6v8impl10RefTracker8FinalizeEb,"axG",@progbits,_ZN6v8impl10RefTracker8FinalizeEb,comdat
	.align 2
	.p2align 4
	.weak	_ZN6v8impl10RefTracker8FinalizeEb
	.type	_ZN6v8impl10RefTracker8FinalizeEb, @function
_ZN6v8impl10RefTracker8FinalizeEb:
.LFB7470:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7470:
	.size	_ZN6v8impl10RefTracker8FinalizeEb, .-_ZN6v8impl10RefTracker8FinalizeEb
	.section	.text._ZNK10napi_env__16can_call_into_jsEv,"axG",@progbits,_ZNK10napi_env__16can_call_into_jsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK10napi_env__16can_call_into_jsEv
	.type	_ZNK10napi_env__16can_call_into_jsEv, @function
_ZNK10napi_env__16can_call_into_jsEv:
.LFB7484:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7484:
	.size	_ZNK10napi_env__16can_call_into_jsEv, .-_ZNK10napi_env__16can_call_into_jsEv
	.section	.text._ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE,"axG",@progbits,_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.type	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE, @function
_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE:
.LFB7485:
	.cfi_startproc
	endbr64
	movl	$257, %eax
	ret
	.cfi_endproc
.LFE7485:
	.size	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE, .-_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv, @function
_ZN12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv:
.LFB7758:
	.cfi_startproc
	endbr64
	movq	184(%rdi), %r8
	movq	192(%rdi), %rsi
	movq	200(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE7758:
	.size	_ZN12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv, .-_ZN12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv
	.section	.text._ZN6v8impl10RefTrackerD0Ev,"axG",@progbits,_ZN6v8impl10RefTrackerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6v8impl10RefTrackerD0Ev
	.type	_ZN6v8impl10RefTrackerD0Ev, @function
_ZN6v8impl10RefTrackerD0Ev:
.LFB7469:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7469:
	.size	_ZN6v8impl10RefTrackerD0Ev, .-_ZN6v8impl10RefTrackerD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_16uvimpl4WorkD2Ev, @function
_ZN12_GLOBAL__N_16uvimpl4WorkD2Ev:
.LFB10428:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_16uvimpl4WorkE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node13AsyncResourceD2Ev@PLT
	.cfi_endproc
.LFE10428:
	.size	_ZN12_GLOBAL__N_16uvimpl4WorkD2Ev, .-_ZN12_GLOBAL__N_16uvimpl4WorkD2Ev
	.set	_ZN12_GLOBAL__N_16uvimpl4WorkD1Ev,_ZN12_GLOBAL__N_16uvimpl4WorkD2Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_16uvimpl4WorkD0Ev, @function
_ZN12_GLOBAL__N_16uvimpl4WorkD0Ev:
.LFB10430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_16uvimpl4WorkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node13AsyncResourceD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$216, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10430:
	.size	_ZN12_GLOBAL__N_16uvimpl4WorkD0Ev, .-_ZN12_GLOBAL__N_16uvimpl4WorkD0Ev
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_s, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_s:
.LFB7727:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	subq	$-128, %rdi
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_idle_start@PLT
	testl	%eax, %eax
	jne	.L14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	leaq	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7727:
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_s, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_s
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Failed to retrieve undefined value"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ERR_NAPI_TSFN_GET_UNDEFINED"
.LC2:
	.string	"Failed to call JS callback"
.LC3:
	.string	"ERR_NAPI_TSFN_CALL_JS"
	.text
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6CallJsEP10napi_env__P12napi_value__PvS6_, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6CallJsEP10napi_env__P12napi_value__PvS6_:
.LFB7725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L15
	movq	%rsi, %r13
	testq	%rsi, %rsi
	jne	.L33
.L15:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L34
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	leaq	-32(%rbp), %rsi
	movq	%rdi, %r12
	call	napi_get_undefined@PLT
	testl	%eax, %eax
	jne	.L35
	movq	-32(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	napi_call_function@PLT
	testl	%eax, %eax
	je	.L15
	cmpl	$10, %eax
	je	.L15
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	napi_throw_error@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	napi_throw_error@PLT
	jmp	.L15
.L34:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7725:
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6CallJsEP10napi_env__P12napi_value__PvS6_, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6CallJsEP10napi_env__P12napi_value__PvS6_
	.p2align 4
	.type	_ZZN6v8impl12_GLOBAL__N_1L6NewEnvEN2v85LocalINS1_7ContextEEEENUlPvE_4_FUNES5_, @function
_ZZN6v8impl12_GLOBAL__N_1L6NewEnvEN2v85LocalINS1_7ContextEEEENUlPvE_4_FUNES5_:
.LFB7681:
	.cfi_startproc
	endbr64
	subl	$1, 112(%rdi)
	jne	.L36
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L36:
	ret
	.cfi_endproc
.LFE7681:
	.size	_ZZN6v8impl12_GLOBAL__N_1L6NewEnvEN2v85LocalINS1_7ContextEEEENUlPvE_4_FUNES5_, .-_ZZN6v8impl12_GLOBAL__N_1L6NewEnvEN2v85LocalINS1_7ContextEEEENUlPvE_4_FUNES5_
	.section	.text._ZN10napi_env__D2Ev,"axG",@progbits,_ZN10napi_env__D5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN10napi_env__D2Ev
	.type	_ZN10napi_env__D2Ev, @function
_ZN10napi_env__D2Ev:
.LFB7478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %rbx
.L44:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L40:
	cmpq	%rbx, %rax
	je	.L40
	movl	$1, %esi
	call	*%rax
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L44
.L39:
	movq	40(%r12), %rdi
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %rbx
	testq	%rdi, %rdi
	je	.L42
.L43:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L45:
	cmpq	%rbx, %rax
	je	.L45
	movl	$1, %esi
	call	*%rax
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L43
.L42:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L46
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L46:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L38
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7478:
	.size	_ZN10napi_env__D2Ev, .-_ZN10napi_env__D2Ev
	.weak	_ZN10napi_env__D1Ev
	.set	_ZN10napi_env__D1Ev,_ZN10napi_env__D2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD2Ev, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD2Ev:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE(%rip), %rax
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rdx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	456(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	movq	456(%r12), %rdi
	subl	$1, 112(%rdi)
	jne	.L66
	movq	(%rdi), %rax
	call	*8(%rax)
.L66:
	movq	448(%r12), %rdi
	testq	%rdi, %rdi
	je	.L67
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L67:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L68
	movq	160(%r12), %rax
	movq	128(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L70
	movq	88(%r12), %rdi
.L69:
	call	_ZdlPv@PLT
.L68:
	movq	80(%r12), %r13
	testq	%r13, %r13
	je	.L71
	movq	%r13, %rdi
	call	uv_cond_destroy@PLT
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L71:
	leaq	40(%r12), %rdi
	call	uv_mutex_destroy@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node13AsyncResourceD2Ev@PLT
	.cfi_endproc
.LFE7695:
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD2Ev, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD2Ev
	.set	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD1Ev,_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD2Ev
	.p2align 4
	.type	_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD1Ev, @function
_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD1Ev:
.LFB10618:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN12_GLOBAL__N_16uvimpl4WorkE(%rip), %rax
	subq	$40, %rdi
	movq	%rax, (%rdi)
	jmp	_ZN4node13AsyncResourceD2Ev@PLT
	.cfi_endproc
.LFE10618:
	.size	_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD1Ev, .-_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD1Ev
	.align 2
	.p2align 4
	.type	_ZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi, @function
_ZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi:
.LFB7759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 208(%rdi)
	je	.L84
	movq	184(%rdi), %rdx
	leaq	-144(%rbp), %r13
	movq	%rdi, %rbx
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rdx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN4node13AsyncResource13CallbackScopeC1EPS0_@PLT
	movq	184(%rbx), %r12
	movl	$1, %esi
	movl	108(%r12), %eax
	movl	104(%r12), %r15d
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	movq	208(%rbx), %r8
	movl	%eax, -152(%rbp)
	movl	-148(%rbp), %eax
	movq	192(%rbx), %rdx
	cmpl	$-22, %eax
	je	.L87
	xorl	%esi, %esi
	testl	%eax, %eax
	je	.L87
	xorl	%esi, %esi
	cmpl	$-125, %eax
	sete	%sil
	leal	9(%rsi,%rsi), %esi
.L87:
	movq	%r12, %rdi
	call	*%r8
	cmpl	104(%r12), %r15d
	jne	.L103
	movl	-152(%rbp), %eax
	cmpl	108(%r12), %eax
	jne	.L104
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L91
	movq	8(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v89Exception13CreateMessageEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L91
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%r12)
.L91:
	movq	%r14, %rdi
	call	_ZN4node13CallbackScopeD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L84:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L105
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	leaq	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L105:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7759:
	.size	_ZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi, .-_ZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi
	.p2align 4
	.type	_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD0Ev, @function
_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD0Ev:
.LFB10619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN12_GLOBAL__N_16uvimpl4WorkE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-40(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	movq	%r12, %rdi
	call	_ZN4node13AsyncResourceD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$216, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10619:
	.size	_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD0Ev, .-_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD0Ev
	.p2align 4
	.type	_ZThn40_N12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv, @function
_ZThn40_N12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv:
.LFB10620:
	.cfi_startproc
	endbr64
	movq	144(%rdi), %r8
	movq	152(%rdi), %rsi
	movq	160(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE10620:
	.size	_ZThn40_N12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv, .-_ZThn40_N12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_E4CallES2_:
.LFB10461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	cmpq	$0, 8(%rax)
	je	.L109
	movq	(%rax), %rax
	leaq	-80(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	24(%r12), %rax
	movq	(%rax), %rax
	movq	16(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	24(%r12), %rax
	movq	(%rax), %rbx
	movl	108(%rbx), %eax
	movq	$0, 96(%rbx)
	movq	%rbx, %rdi
	movq	$0, 88(%rbx)
	movl	104(%rbx), %r14d
	movl	%eax, -84(%rbp)
	movq	24(%r12), %rax
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	call	*8(%rax)
	cmpl	104(%rbx), %r14d
	jne	.L122
	movl	-84(%rbp), %eax
	cmpl	108(%rbx), %eax
	jne	.L123
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L115
	movq	8(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L115:
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L109:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	leaq	_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L124:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10461:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_E4CallES2_
	.section	.text._ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_,"axG",@progbits,_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_,comdat
	.p2align 4
	.weak	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_
	.type	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_, @function
_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_:
.LFB7657:
	.cfi_startproc
	endbr64
	movq	-16(%rdi), %rax
	leaq	_ZThn40_N12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv(%rip), %rdx
	leaq	-16(%rdi), %r8
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L126
	movq	152(%r8), %rsi
	movq	144(%r8), %rdi
	jmp	*160(%r8)
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE7657:
	.size	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_, .-_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_
	.text
	.p2align 4
	.type	_ZThn40_N12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi, @function
_ZThn40_N12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi:
.LFB10621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -148(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 168(%rdi)
	je	.L127
	movq	144(%rdi), %rdx
	leaq	-144(%rbp), %r13
	movq	%rdi, %rbx
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rdx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-40(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN4node13AsyncResource13CallbackScopeC1EPS0_@PLT
	movq	144(%rbx), %r12
	movl	$1, %esi
	movl	108(%r12), %eax
	movl	104(%r12), %r15d
	movq	$0, 96(%r12)
	movq	$0, 88(%r12)
	movq	168(%rbx), %r8
	movl	%eax, -152(%rbp)
	movl	-148(%rbp), %eax
	movq	152(%rbx), %rdx
	cmpl	$-22, %eax
	je	.L130
	xorl	%esi, %esi
	testl	%eax, %eax
	je	.L130
	xorl	%esi, %esi
	cmpl	$-125, %eax
	sete	%sil
	leal	9(%rsi,%rsi), %esi
.L130:
	movq	%r12, %rdi
	call	*%r8
	cmpl	104(%r12), %r15d
	jne	.L146
	movl	-152(%rbp), %eax
	cmpl	108(%r12), %eax
	jne	.L147
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L134
	movq	8(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v89Exception13CreateMessageEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L134
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%r12)
.L134:
	movq	%r14, %rdi
	call	_ZN4node13CallbackScopeD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L127:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_restore_state
	leaq	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10621:
	.size	_ZThn40_N12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi, .-_ZThn40_N12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi
	.section	.text._ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i,"axG",@progbits,_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i,comdat
	.p2align 4
	.weak	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i
	.type	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i, @function
_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i:
.LFB7660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-16(%rdi), %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	8(%r12), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	subl	$1, 2156(%rdx)
	js	.L171
	movq	-16(%rdi), %rax
	leaq	_ZThn40_N12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi(%rip), %rdx
	movq	%rdi, %rbx
	movl	%esi, %r15d
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L151
	cmpq	$0, 168(%r12)
	je	.L149
	movq	144(%r12), %rax
	leaq	-144(%rbp), %r13
	leaq	-112(%rbp), %r14
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-56(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN4node13AsyncResource13CallbackScopeC1EPS0_@PLT
	movq	144(%r12), %rbx
	movl	$1, %esi
	movl	108(%rbx), %ecx
	movl	104(%rbx), %eax
	movq	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	152(%r12), %rdx
	movl	%ecx, -152(%rbp)
	movq	168(%r12), %rcx
	cmpl	$-22, %r15d
	je	.L154
	xorl	%esi, %esi
	testl	%r15d, %r15d
	je	.L154
	xorl	%esi, %esi
	cmpl	$-125, %r15d
	sete	%sil
	leal	9(%rsi,%rsi), %esi
.L154:
	movl	%eax, -148(%rbp)
	movq	%rbx, %rdi
	call	*%rcx
	movl	-148(%rbp), %eax
	cmpl	104(%rbx), %eax
	jne	.L172
	movl	-152(%rbp), %eax
	cmpl	108(%rbx), %eax
	jne	.L173
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L158
	movq	8(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v89Exception13CreateMessageEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L158
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L158:
	movq	%r14, %rdi
	call	_ZN4node13CallbackScopeD2Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L149:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L172:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7660:
	.size	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i, .-_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i
	.section	.text._ZN15node_napi_env__D2Ev,"axG",@progbits,_ZN15node_napi_env__D5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN15node_napi_env__D2Ev
	.type	_ZN15node_napi_env__D2Ev, @function
_ZN15node_napi_env__D2Ev:
.LFB10432:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L176
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %r12
.L181:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L177:
	cmpq	%r12, %rax
	je	.L177
	movl	$1, %esi
	call	*%rax
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L181
.L176:
	movq	40(%rbx), %rdi
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %r12
	testq	%rdi, %rdi
	je	.L179
.L180:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L182:
	cmpq	%r12, %rax
	je	.L182
	movl	$1, %esi
	call	*%rax
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L180
.L179:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L183
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L183:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L175
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10432:
	.size	_ZN15node_napi_env__D2Ev, .-_ZN15node_napi_env__D2Ev
	.weak	_ZN15node_napi_env__D1Ev
	.set	_ZN15node_napi_env__D1Ev,_ZN15node_napi_env__D2Ev
	.section	.text._ZN10napi_env__D0Ev,"axG",@progbits,_ZN10napi_env__D5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN10napi_env__D0Ev
	.type	_ZN10napi_env__D0Ev, @function
_ZN10napi_env__D0Ev:
.LFB7480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L203
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %rbx
.L208:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L204:
	cmpq	%rbx, %rax
	je	.L204
	movl	$1, %esi
	call	*%rax
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L208
.L203:
	movq	40(%r12), %rdi
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %rbx
	testq	%rdi, %rdi
	je	.L206
.L207:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L209:
	cmpq	%rbx, %rax
	je	.L209
	movl	$1, %esi
	call	*%rax
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L207
.L206:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L210
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L210:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L211
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L211:
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7480:
	.size	_ZN10napi_env__D0Ev, .-_ZN10napi_env__D0Ev
	.section	.text._ZN15node_napi_env__D0Ev,"axG",@progbits,_ZN15node_napi_env__D5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN15node_napi_env__D0Ev
	.type	_ZN15node_napi_env__D0Ev, @function
_ZN15node_napi_env__D0Ev:
.LFB10434:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L233
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %rbx
.L238:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L234:
	cmpq	%rbx, %rax
	je	.L234
	movl	$1, %esi
	call	*%rax
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L238
.L233:
	movq	40(%r12), %rdi
	leaq	_ZN6v8impl10RefTracker8FinalizeEb(%rip), %rbx
	testq	%rdi, %rdi
	je	.L236
.L237:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
.L239:
	cmpq	%rbx, %rax
	je	.L239
	movl	$1, %esi
	call	*%rax
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L237
.L236:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L240
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L240:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L241
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L241:
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10434:
	.size	_ZN15node_napi_env__D0Ev, .-_ZN15node_napi_env__D0Ev
	.section	.text.unlikely,"ax",@progbits
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4
	.type	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_, @function
_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_:
.LFB8598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%r14), %rax
	subl	$1, 2152(%rax)
	movq	16(%r14), %rax
	movq	%rax, (%rdi)
	movq	288(%rdi), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	288(%rbx), %rax
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L263
	movq	%r15, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L263
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L263
	movq	271(%rax), %r15
	movl	$24, %edi
	addl	$1, 2152(%r15)
	call	_Znwm@PLT
	movq	128(%rbx), %rdx
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbENKUlPS2_E_clES6_EUlS6_E_EEvPT_T0_ENUlS6_E_4_FUNES6_(%rip), %rsi
	leaq	128(%rbx), %rdi
	movq	%r15, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 128(%rbx)
	call	uv_close@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L270
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L270:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_.cold, @function
_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_.cold:
.LFSB8598:
.L263:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	2152, %eax
	ud2
	.cfi_endproc
.LFE8598:
	.text
	.size	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_, .-_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_
	.section	.text.unlikely
	.size	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_.cold, .-_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_.cold
.LCOLDE4:
	.text
.LHOTE4:
	.section	.text._ZNK15node_napi_env__16can_call_into_jsEv,"axG",@progbits,_ZNK15node_napi_env__16can_call_into_jsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15node_napi_env__16can_call_into_jsEv
	.type	_ZNK15node_napi_env__16can_call_into_jsEv, @function
_ZNK15node_napi_env__16can_call_into_jsEv:
.LFB7667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L274
	movq	%rbx, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L274
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rsi
	movq	55(%rax), %rax
	cmpq	%rsi, 295(%rax)
	jne	.L274
	movq	271(%rax), %rdx
.L273:
	xorl	%eax, %eax
	movzbl	1930(%rdx), %ecx
	testb	%cl, %cl
	jne	.L279
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	movzbl	2664(%rdx), %eax
	testb	%al, %al
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L273
	.cfi_endproc
.LFE7667:
	.size	_ZNK15node_napi_env__16can_call_into_jsEv, .-_ZNK15node_napi_env__16can_call_into_jsEv
	.section	.text.unlikely
.LCOLDB5:
	.text
.LHOTB5:
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv:
.LFB7728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	40(%rbx), %r12
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	456(%rdi), %rax
	movq	%r13, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	cmpq	$0, 440(%rbx)
	movb	$1, 424(%rbx)
	jne	.L291
.L281:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	cmpb	$0, 488(%rbx)
	jne	.L290
	movq	456(%rbx), %rax
	movb	$1, 488(%rbx)
	movq	16(%rax), %r12
	testq	%r12, %r12
	je	.L284
	movq	%r12, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L284
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L284
	movq	271(%rax), %r12
	movl	$24, %edi
	addl	$1, 2152(%r12)
	call	_Znwm@PLT
	movq	168(%rbx), %rdx
	leaq	168(%rbx), %rdi
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_(%rip), %rsi
	movq	%r12, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 168(%rbx)
	call	uv_close@PLT
.L290:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L292
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	.cfi_restore_state
	movq	80(%rbx), %rdi
	call	uv_cond_signal@PLT
	jmp	.L281
.L292:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv.cold, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv.cold:
.LFSB7728:
.L284:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movl	2152, %eax
	ud2
	.cfi_endproc
.LFE7728:
	.text
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv
	.section	.text.unlikely
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv.cold, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv.cold
.LCOLDE5:
	.text
.LHOTE5:
	.section	.text._ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE,"axG",@progbits,_ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.type	_ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE, @function
_ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE:
.LFB7668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r13
	movq	8(%rdi), %rax
	testq	%r13, %r13
	je	.L294
	movq	%rdi, %rbx
	movq	%r13, %rdi
	leaq	112(%rax), %r12
	movq	%rsi, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L294
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L294
	movq	271(%rax), %rax
	movq	16(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	movq	360(%rax), %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	72(%rax), %rdx
	jmp	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
.L294:
	.cfi_restore_state
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7668:
	.size	_ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE, .-_ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.text
	.p2align 4
	.type	_ZZN4node11Environment11CloseHandleI11uv_handle_sZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbENKUlPS2_E_clES6_EUlS6_E_EEvPT_T0_ENUlS6_E_4_FUNES6_, @function
_ZZN4node11Environment11CloseHandleI11uv_handle_sZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbENKUlPS2_E_clES6_EUlS6_E_EEvPT_T0_ENUlS6_E_4_FUNES6_:
.LFB8592:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-296(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	subl	$1, 2152(%rax)
	movq	16(%r13), %rax
	movq	%rax, (%rdi)
	movq	456(%r12), %rax
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpq	$0, 472(%r12)
	je	.L300
	leaq	-112(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -160(%rbp)
	call	_ZN4node13AsyncResource13CallbackScopeC1EPS0_@PLT
	movq	456(%r12), %r15
	movl	104(%r15), %eax
	movq	$0, 96(%r15)
	movq	%r15, %rdi
	movq	$0, 88(%r15)
	movq	432(%r12), %rdx
	movl	%eax, -152(%rbp)
	movl	108(%r15), %eax
	movq	464(%r12), %rsi
	movl	%eax, -164(%rbp)
	call	*472(%r12)
	movl	-152(%rbp), %eax
	cmpl	104(%r15), %eax
	movq	-160(%rbp), %r8
	jne	.L332
	movl	-164(%rbp), %eax
	cmpl	108(%r15), %eax
	jne	.L333
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L304
	movq	8(%r15), %rdi
	movq	(%rax), %rsi
	movq	%r8, -152(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r15), %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	24(%r15), %rdi
	movq	-152(%rbp), %r8
	testq	%rdi, %rdi
	je	.L304
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%r15)
	movq	-152(%rbp), %r8
.L304:
	movq	%r8, %rdi
	call	_ZN4node13CallbackScopeD2Ev@PLT
.L300:
	movq	104(%r12), %rax
	cmpq	%rax, 136(%r12)
	je	.L307
	.p2align 4,,10
	.p2align 3
.L306:
	movq	(%rax), %rcx
	movq	432(%r12), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	*480(%r12)
	movq	120(%r12), %rcx
	movq	104(%r12), %rax
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L308
	addq	$8, %rax
	movq	%rax, 104(%r12)
	cmpq	136(%r12), %rax
	jne	.L306
.L307:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE(%rip), %rax
	movq	%r12, %rdx
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv(%rip), %rsi
	movq	%rax, -296(%rbx)
	movq	456(%r12), %rax
	movq	8(%rax), %rdi
	call	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	movq	456(%r12), %rdi
	subl	$1, 112(%rdi)
	je	.L334
.L310:
	movq	448(%r12), %rdi
	testq	%rdi, %rdi
	je	.L311
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L311:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L312
	movq	160(%r12), %rax
	movq	128(%r12), %rbx
	leaq	8(%rax), %r15
	cmpq	%rbx, %r15
	jbe	.L313
	.p2align 4,,10
	.p2align 3
.L314:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r15
	ja	.L314
	movq	88(%r12), %rdi
.L313:
	call	_ZdlPv@PLT
.L312:
	movq	80(%r12), %r15
	testq	%r15, %r15
	je	.L315
	movq	%r15, %rdi
	call	uv_cond_destroy@PLT
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L315:
	leaq	40(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movq	%r12, %rdi
	call	_ZN4node13AsyncResourceD2Ev@PLT
	movl	$496, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L335
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	.cfi_restore_state
	movq	112(%r12), %rdi
	call	_ZdlPv@PLT
	movq	128(%r12), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 128(%r12)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 112(%r12)
	movq	%rdx, 120(%r12)
	movq	%rax, 104(%r12)
	cmpq	%rax, 136(%r12)
	jne	.L306
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L334:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L333:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8592:
	.size	_ZZN4node11Environment11CloseHandleI11uv_handle_sZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbENKUlPS2_E_clES6_EUlS6_E_EEvPT_T0_ENUlS6_E_4_FUNES6_, .-_ZZN4node11Environment11CloseHandleI11uv_handle_sZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbENKUlPS2_E_clES6_EUlS6_E_EEvPT_T0_ENUlS6_E_4_FUNES6_
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD0Ev, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD0Ev:
.LFB7697:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE(%rip), %rax
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rdx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	456(%rdi), %rax
	movq	8(%rax), %rdi
	call	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	movq	456(%r12), %rdi
	subl	$1, 112(%rdi)
	jne	.L337
	movq	(%rdi), %rax
	call	*8(%rax)
.L337:
	movq	448(%r12), %rdi
	testq	%rdi, %rdi
	je	.L338
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L338:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L339
	movq	160(%r12), %rax
	movq	128(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L340
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L341
	movq	88(%r12), %rdi
.L340:
	call	_ZdlPv@PLT
.L339:
	movq	80(%r12), %r13
	testq	%r13, %r13
	je	.L342
	movq	%r13, %rdi
	call	uv_cond_destroy@PLT
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L342:
	leaq	40(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movq	%r12, %rdi
	call	_ZN4node13AsyncResourceD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$496, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7697:
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD0Ev, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD0Ev
	.section	.text.unlikely
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s:
.LFB7726:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-256(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-296(%rdi), %rbx
	movq	%r13, %rdi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movzbl	424(%rbx), %r14d
	testb	%r14b, %r14b
	jne	.L399
	movq	160(%rbx), %rax
	subq	128(%rbx), %rax
	sarq	$3, %rax
	movq	120(%rbx), %rsi
	movq	104(%rbx), %rcx
	subq	$1, %rax
	salq	$6, %rax
	movq	%rsi, %r12
	movq	%rax, %rdx
	movq	136(%rbx), %rax
	subq	%rcx, %r12
	subq	144(%rbx), %rax
	sarq	$3, %rax
	sarq	$3, %r12
	addq	%rdx, %rax
	addq	%rax, %r12
	je	.L376
	subq	$8, %rsi
	movq	(%rcx), %r15
	cmpq	%rsi, %rcx
	je	.L360
	addq	$8, %rcx
	movq	%rcx, 104(%rbx)
.L361:
	cmpq	440(%rbx), %r12
	je	.L400
.L362:
	cmpq	$1, %r12
	je	.L377
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L363:
	movq	456(%rbx), %rax
	leaq	-144(%rbp), %r12
	leaq	-112(%rbp), %r13
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN4node13AsyncResource13CallbackScopeC1EPS0_@PLT
	movq	448(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L369
	movq	456(%rbx), %rax
	movq	(%rsi), %rsi
	movq	8(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
.L369:
	movq	456(%rbx), %r14
	movq	%r15, %rcx
	movl	104(%r14), %eax
	movq	$0, 96(%r14)
	movq	%r14, %rdi
	movq	$0, 88(%r14)
	movq	432(%rbx), %rdx
	movl	%eax, -152(%rbp)
	movl	108(%r14), %eax
	movl	%eax, -156(%rbp)
	call	*480(%rbx)
	movl	-152(%rbp), %eax
	cmpl	104(%r14), %eax
	jne	.L401
	movl	-156(%rbp), %eax
	cmpl	108(%r14), %eax
	jne	.L402
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L373
	movq	8(%r14), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r14), %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L373
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%r14)
.L373:
	movq	%r13, %rdi
	call	_ZN4node13CallbackScopeD2Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L376:
	xorl	%r15d, %r15d
	cmpq	$0, 416(%rbx)
	jne	.L364
.L405:
	cmpq	$0, 440(%rbx)
	movb	$1, 424(%rbx)
	jne	.L403
.L365:
	movq	456(%rbx), %rax
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpb	$0, 488(%rbx)
	jne	.L397
	movq	456(%rbx), %rax
	movb	$1, 488(%rbx)
	movq	16(%rax), %rdx
	testq	%rdx, %rdx
	je	.L368
	movq	%rdx, %rdi
	movq	%rdx, -152(%rbp)
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L368
	movq	-152(%rbp), %rdx
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	(%rdx), %rax
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L368
	movq	271(%rax), %rdx
	movl	$24, %edi
	addl	$1, 2152(%rdx)
	movq	%rdx, -152(%rbp)
	call	_Znwm@PLT
	movq	-152(%rbp), %rdx
	leaq	168(%rbx), %rdi
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_(%rip), %rsi
	movq	%rdx, (%rax)
	movq	168(%rbx), %rdx
	movq	%rax, 168(%rbx)
	movq	%rdx, 16(%rax)
	call	uv_close@PLT
.L397:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L367:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	testb	%r14b, %r14b
	jne	.L363
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L399:
	movq	456(%rbx), %rax
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpb	$0, 488(%rbx)
	jne	.L398
	movq	456(%rbx), %rax
	movb	$1, 488(%rbx)
	movq	16(%rax), %r15
	testq	%r15, %r15
	je	.L368
	movq	%r15, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L368
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L368
	movq	271(%rax), %r15
	movl	$24, %edi
	addl	$1, 2152(%r15)
	call	_Znwm@PLT
	movq	168(%rbx), %rdx
	leaq	-128(%r12), %rdi
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction26CloseHandlesAndMaybeDeleteEbEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_(%rip), %rsi
	movq	%r15, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 168(%rbx)
	call	uv_close@PLT
.L398:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L354:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	cmpq	$0, 416(%rbx)
	movl	$1, %r14d
	je	.L405
.L364:
	leaq	296(%rbx), %rdi
	call	uv_idle_stop@PLT
	testl	%eax, %eax
	je	.L367
	leaq	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L403:
	movq	80(%rbx), %rdi
	call	uv_cond_signal@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L400:
	movq	80(%rbx), %rdi
	call	uv_cond_signal@PLT
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L360:
	movq	112(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	128(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 128(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 112(%rbx)
	movq	%rdx, 120(%rbx)
	movq	%rax, 104(%rbx)
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L401:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L402:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s.cold, @function
_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s.cold:
.LFSB7726:
.L368:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	2152, %eax
	ud2
	.cfi_endproc
.LFE7726:
	.text
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s
	.section	.text.unlikely
	.size	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s.cold, .-_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6IdleCbEP9uv_idle_s.cold
.LCOLDE6:
	.text
.LHOTE6:
	.p2align 4
	.type	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_, @function
_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_:
.LFB8577:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %r13
	movq	0(%r13), %rax
	subl	$1, 2152(%rax)
	movq	16(%r13), %rax
	movq	%rax, (%rdi)
	cmpq	$168, %rdi
	je	.L407
	leaq	-168(%rdi), %r12
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE(%rip), %rax
	movq	%rax, -168(%rdi)
	movq	456(%r12), %rax
	movq	%r12, %rdx
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv(%rip), %rsi
	movq	8(%rax), %rdi
	call	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	movq	456(%r12), %rdi
	subl	$1, 112(%rdi)
	jne	.L408
	movq	(%rdi), %rax
	call	*8(%rax)
.L408:
	movq	448(%r12), %rdi
	testq	%rdi, %rdi
	je	.L409
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L409:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L410
	movq	160(%r12), %rax
	movq	128(%r12), %rbx
	leaq	8(%rax), %r14
	cmpq	%rbx, %r14
	jbe	.L411
	.p2align 4,,10
	.p2align 3
.L412:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r14
	ja	.L412
	movq	88(%r12), %rdi
.L411:
	call	_ZdlPv@PLT
.L410:
	movq	80(%r12), %r14
	testq	%r14, %r14
	je	.L413
	movq	%r14, %rdi
	call	uv_cond_destroy@PLT
	movl	$48, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L413:
	leaq	40(%r12), %rdi
	call	uv_mutex_destroy@PLT
	movq	%r12, %rdi
	call	_ZN4node13AsyncResourceD2Ev@PLT
	movl	$496, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L407:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$24, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8577:
	.size	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_, .-_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_
	.section	.text._ZN6v8impl9FinalizerD2Ev,"axG",@progbits,_ZN6v8impl9FinalizerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6v8impl9FinalizerD2Ev
	.type	_ZN6v8impl9FinalizerD2Ev, @function
_ZN6v8impl9FinalizerD2Ev:
.LFB7496:
	.cfi_startproc
	endbr64
	cmpb	$0, 33(%rdi)
	je	.L428
	movq	(%rdi), %rdi
	subl	$1, 112(%rdi)
	jne	.L428
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L428:
	ret
	.cfi_endproc
.LFE7496:
	.size	_ZN6v8impl9FinalizerD2Ev, .-_ZN6v8impl9FinalizerD2Ev
	.weak	_ZN6v8impl9FinalizerD1Ev
	.set	_ZN6v8impl9FinalizerD1Ev,_ZN6v8impl9FinalizerD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED2Ev:
.LFB10029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L431
	movq	%r12, %rdi
	call	_ZN6v8impl9FinalizerD1Ev
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L431:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L430
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10029:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED2Ev
	.section	.text.unlikely
.LCOLDB7:
	.text
.LHOTB7:
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv, @function
_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv:
.LFB7669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rax
	movq	%rdi, 16(%rsi)
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.L438
	movq	%rbx, %rdi
	movq	%rsi, %r12
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L438
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L438
	movq	271(%rax), %rbx
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_EE(%rip), %rcx
	movq	2480(%rbx), %rdx
	movb	$1, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, (%rax)
	movq	%r12, 24(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L439
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L441
.L452:
	movq	(%rdi), %rax
	call	*8(%rax)
.L441:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L453
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L453:
	.cfi_restore_state
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L452
	jmp	.L441
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv.cold, @function
_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv.cold:
.LFSB7669:
.L438:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movl	$32, %edi
	call	_Znwm@PLT
	movq	2480, %rax
	ud2
	.cfi_endproc
.LFE7669:
	.text
	.size	_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv, .-_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv
	.section	.text.unlikely
	.size	_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv.cold, .-_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv.cold
.LCOLDE7:
	.text
.LHOTE7:
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED0Ev:
.LFB10031:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	24(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L455
	movq	%r13, %rdi
	call	_ZN6v8impl9FinalizerD1Ev
	movl	$40, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L455:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L456
	movq	(%rdi), %rax
	call	*8(%rax)
.L456:
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10031:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED0Ev
	.p2align 4
	.globl	napi_module_register
	.type	napi_module_register, @function
napi_module_register:
.LFB7732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$64, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movl	$-1, (%rax)
	movq	%rax, %rdi
	movl	4(%rbx), %eax
	movq	$0, 8(%rdi)
	orl	$8, %eax
	movq	$0, 24(%rdi)
	movl	%eax, 4(%rdi)
	movq	8(%rbx), %rax
	movq	%rbx, 48(%rdi)
	movq	%rax, 16(%rdi)
	leaq	_ZL23napi_module_register_cbN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv(%rip), %rax
	movq	%rax, 32(%rdi)
	movq	24(%rbx), %rax
	movq	$0, 56(%rdi)
	movq	%rax, 40(%rdi)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7732:
	.size	napi_module_register, .-napi_module_register
	.p2align 4
	.globl	napi_add_env_cleanup_hook
	.type	napi_add_env_cleanup_hook, @function
napi_add_env_cleanup_hook:
.LFB7733:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L469
	testq	%rsi, %rsi
	je	.L473
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L473:
	.cfi_restore 6
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L469:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7733:
	.size	napi_add_env_cleanup_hook, .-napi_add_env_cleanup_hook
	.p2align 4
	.globl	napi_remove_env_cleanup_hook
	.type	napi_remove_env_cleanup_hook, @function
napi_remove_env_cleanup_hook:
.LFB7734:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L477
	testq	%rsi, %rsi
	je	.L481
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore 6
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7734:
	.size	napi_remove_env_cleanup_hook, .-napi_remove_env_cleanup_hook
	.p2align 4
	.globl	napi_fatal_exception
	.type	napi_fatal_exception, @function
napi_fatal_exception:
.LFB7735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L492
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L484
.L485:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L482:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L484:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rsi, %r12
	call	*16(%rax)
	testb	%al, %al
	je	.L485
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movq	$0, 96(%rbx)
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L507
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Exception13CreateMessageEPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdx
	xorl	%r12d, %r12d
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS4_INS1_7MessageEEEb@PLT
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
.L487:
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L508
.L488:
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L492:
	movl	$1, %r12d
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L507:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L508:
	movq	-64(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L489
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L489:
	testq	%r14, %r14
	je	.L488
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L488
.L506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7735:
	.size	napi_fatal_exception, .-napi_fatal_exception
	.p2align 4
	.globl	napi_fatal_error
	.type	napi_fatal_error, @function
napi_fatal_error:
.LFB7736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdx, %r13
	pushq	%r12
	leaq	-96(%rbp), %r10
	.cfi_offset 12, -32
	movq	%rcx, %r12
	subq	$80, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movb	$0, -80(%rbp)
	movq	%rax, -96(%rbp)
	leaq	-48(%rbp), %rax
	movq	$0, -88(%rbp)
	movq	%rax, -64(%rbp)
	movq	$0, -56(%rbp)
	movb	$0, -48(%rbp)
	cmpq	$-1, %rsi
	jne	.L516
	xorl	%eax, %eax
	movq	%rsi, %rcx
	repnz scasb
	notq	%rcx
	leaq	-1(%rcx), %r8
.L516:
	xorl	%edx, %edx
	movq	%r9, %rcx
	xorl	%esi, %esi
	movq	%r10, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-56(%rbp), %rdx
	cmpq	$-1, %r12
	je	.L512
	leaq	-64(%rbp), %rdi
	movq	%r12, %r8
	movq	%r13, %rcx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L513:
	movq	-64(%rbp), %rsi
	movq	-96(%rbp), %rdi
	call	_ZN4node10FatalErrorEPKcS1_@PLT
.L512:
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rcx
	xorl	%esi, %esi
	repnz scasb
	leaq	-64(%rbp), %rdi
	movq	%rcx, %rax
	movq	%r13, %rcx
	notq	%rax
	leaq	-1(%rax), %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L513
	.cfi_endproc
.LFE7736:
	.size	napi_fatal_error, .-napi_fatal_error
	.p2align 4
	.globl	napi_open_callback_scope
	.type	napi_open_callback_scope, @function
napi_open_callback_scope:
.LFB7737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L522
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L520
	movq	%rsi, %rdi
	movq	16(%rbx), %rsi
	testq	%rdi, %rdi
	je	.L520
	movq	%rdx, %r13
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L527
	movq	0(%r13), %rdx
	movl	$56, %edi
	movq	8(%r13), %r15
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	movq	8(%rbx), %rsi
	movq	%r15, %xmm1
	movq	%rax, %r13
	movq	%rax, %rdi
	movq	%rdx, %xmm0
	movq	%r14, %rdx
	call	_ZN4node13CallbackScopeC1EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE@PLT
	movq	%r13, (%r12)
	xorl	%eax, %eax
	addl	$1, 108(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	.cfi_restore_state
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L517:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$2, %eax
	jmp	.L517
	.cfi_endproc
.LFE7737:
	.size	napi_open_callback_scope, .-napi_open_callback_scope
	.p2align 4
	.globl	napi_close_callback_scope
	.type	napi_close_callback_scope, @function
napi_close_callback_scope:
.LFB7738:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L531
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L538
	movl	108(%rdi), %edx
	movl	$14, %eax
	testl	%edx, %edx
	je	.L528
	subl	$1, %edx
	movl	%edx, 108(%rdi)
	movq	%rsi, %rdi
	call	_ZN4node13CallbackScopeD1Ev@PLT
	movl	$56, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
.L528:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L538:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7738:
	.size	napi_close_callback_scope, .-napi_close_callback_scope
	.p2align 4
	.globl	napi_async_init
	.type	napi_async_init, @function
napi_async_init:
.LFB7739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L547
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L542
	movq	%rcx, %r13
	testq	%rcx, %rcx
	je	.L542
	movq	%rsi, %rdi
	movq	16(%rbx), %rsi
	movq	8(%rbx), %r14
	movq	%rsi, -56(%rbp)
	testq	%rdi, %rdi
	je	.L543
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-56(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L552
	movq	%r12, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L553
.L546:
	movl	$16, %edi
	movq	%rax, -56(%rbp)
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	-56(%rbp), %rdx
	movq	%r15, %rsi
	movups	%xmm0, (%rax)
	movsd	.LC8(%rip), %xmm0
	movq	%rax, %r12
	movq	%r14, %rdi
	call	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEd@PLT
	movq	%r12, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movsd	%xmm0, (%r12)
	movsd	%xmm1, 8(%r12)
.L539:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L542:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	jne	.L546
.L553:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$24, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$2, %eax
	jmp	.L539
	.cfi_endproc
.LFE7739:
	.size	napi_async_init, .-napi_async_init
	.p2align 4
	.globl	napi_async_destroy
	.type	napi_async_destroy, @function
napi_async_destroy:
.LFB7740:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L560
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L565
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L559
	movq	%r13, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L559
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L559
	movq	271(%rax), %rdi
.L558:
	movsd	(%r12), %xmm0
	movsd	8(%r12), %xmm1
	call	_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE@PLT
	movq	%r12, %rdi
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L559:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	xorl	%edi, %edi
	jmp	.L558
	.cfi_endproc
.LFE7740:
	.size	napi_async_destroy, .-napi_async_destroy
	.p2align 4
	.globl	napi_make_callback
	.type	napi_make_callback, @function
napi_make_callback:
.LFB7741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	%r9, -120(%rbp)
	movq	%rax, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L584
	cmpq	$0, 24(%rdi)
	movq	%rdi, %r13
	je	.L568
.L569:
	movabsq	$42949672960, %rax
	movq	$0, 88(%r13)
	movl	$10, %r12d
	movq	%rax, 96(%r13)
.L566:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L611
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, -136(%rbp)
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	%r8, %rbx
	call	*16(%rax)
	testb	%al, %al
	je	.L569
	movq	$0, 88(%r13)
	movq	8(%r13), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%r13)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-136(%rbp), %rdx
	movq	%r13, -64(%rbp)
	testq	%rdx, %rdx
	je	.L572
	testq	%rbx, %rbx
	je	.L585
	cmpq	$0, -120(%rbp)
	je	.L572
.L585:
	movq	16(%r13), %rsi
	movq	%rdx, %rdi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L612
	movq	%rax, -136(%rbp)
	testq	%r12, %r12
	je	.L572
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L572
	testq	%r14, %r14
	leaq	_ZZ18napi_make_callbackE13empty_context(%rip), %rax
	movq	8(%r13), %rdi
	movl	%ebx, %ecx
	cmove	%rax, %r14
	movq	-120(%rbp), %r8
	movq	-136(%rbp), %rsi
	movq	%r12, %rdx
	movsd	(%r14), %xmm0
	movsd	8(%r14), %xmm1
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	movq	%r15, %rdi
	movq	%rax, %rbx
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L579
	testq	%rbx, %rbx
	je	.L613
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L578
	movq	%rbx, (%rax)
.L578:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L579
	xorl	%r12d, %r12d
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L572:
	movabsq	$4294967296, %rax
	movq	$0, 88(%r13)
	movl	$1, %r12d
	movq	%rax, 96(%r13)
.L571:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L614
.L580:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$1, %r12d
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L614:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L581
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L581:
	testq	%r13, %r13
	je	.L580
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L612:
	movabsq	$8589934592, %rax
	movq	$0, 88(%r13)
	movl	$2, %r12d
	movq	%rax, 96(%r13)
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L579:
	movabsq	$42949672960, %rax
	movq	$0, 88(%r13)
	movl	$10, %r12d
	movq	%rax, 96(%r13)
	jmp	.L571
.L613:
	movabsq	$38654705664, %rax
	movq	$0, 88(%r13)
	movl	$9, %r12d
	movq	%rax, 96(%r13)
	jmp	.L571
.L611:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7741:
	.size	napi_make_callback, .-napi_make_callback
	.p2align 4
	.globl	napi_create_buffer
	.type	napi_create_buffer, @function
napi_create_buffer:
.LFB7742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L627
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L617
.L618:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L615:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L646
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L617:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rsi, %r12
	movq	%rdx, %r13
	movq	%rcx, %r15
	call	*16(%rax)
	testb	%al, %al
	je	.L618
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r15, %r15
	je	.L647
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN4node6Buffer3NewEPN2v87IsolateEm@PLT
	testq	%rax, %rax
	je	.L648
	movq	%rax, (%r15)
	testq	%r13, %r13
	je	.L622
	movq	%rax, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	%rax, 0(%r13)
.L622:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L649
	.p2align 4,,10
	.p2align 3
.L620:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L650
.L623:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L627:
	movl	$1, %r12d
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L647:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L650:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L624
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L624:
	testq	%r13, %r13
	je	.L623
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L649:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L648:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L620
.L646:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7742:
	.size	napi_create_buffer, .-napi_create_buffer
	.p2align 4
	.globl	napi_create_external_buffer
	.type	napi_create_external_buffer, @function
napi_create_external_buffer:
.LFB7743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -120(%rbp)
	movq	%r8, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L662
	cmpq	$0, 24(%rdi)
	movq	%rdi, %r14
	je	.L653
.L654:
	movabsq	$42949672960, %rax
	movq	$0, 88(%r14)
	movl	$10, %r12d
	movq	%rax, 96(%r14)
.L651:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L678
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rsi, %r13
	movq	%rdx, %r12
	movq	%r9, %rbx
	call	*16(%rax)
	testb	%al, %al
	je	.L654
	movq	$0, 88(%r14)
	movq	8(%r14), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%r14)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%r14, -64(%rbp)
	testq	%rbx, %rbx
	je	.L679
	movq	8(%r14), %r9
	movl	$40, %edi
	movq	%r9, -136(%rbp)
	call	_Znwm@PLT
	addl	$1, 112(%r14)
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, (%rax)
	movq	%rax, %r8
	movq	-120(%rbp), %rax
	leaq	_ZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPv(%rip), %rcx
	movq	-136(%rbp), %r9
	movq	$0, 16(%r8)
	movq	%rax, 8(%r8)
	movq	-128(%rbp), %rax
	movq	%r9, %rdi
	movq	%rax, 24(%r8)
	movl	$256, %eax
	movw	%ax, 32(%r8)
	call	_ZN4node6Buffer3NewEPN2v87IsolateEPcmPFvS4_PvES5_@PLT
	testq	%rax, %rax
	je	.L680
	movq	%rax, (%rbx)
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L656
	movabsq	$42949672960, %rax
	movq	$0, 88(%r14)
	movl	$10, %r12d
	movq	%rax, 96(%r14)
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L662:
	movl	$1, %r12d
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L679:
	movabsq	$4294967296, %rax
	movq	$0, 88(%r14)
	movl	$1, %r12d
	movq	%rax, 96(%r14)
.L656:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L681
.L658:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L681:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L659
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L659:
	testq	%r13, %r13
	je	.L658
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L680:
	movabsq	$38654705664, %rax
	movq	$0, 88(%r14)
	movl	$9, %r12d
	movq	%rax, 96(%r14)
	jmp	.L656
.L678:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7743:
	.size	napi_create_external_buffer, .-napi_create_external_buffer
	.p2align 4
	.globl	napi_create_buffer_copy
	.type	napi_create_buffer_copy, @function
napi_create_buffer_copy:
.LFB7744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L694
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L684
.L685:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L682:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L713
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L684:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, -120(%rbp)
	movq	%rsi, %r15
	movq	%rcx, %r13
	movq	%r8, %r12
	call	*16(%rax)
	testb	%al, %al
	je	.L685
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movq	-120(%rbp), %r9
	je	.L714
	movq	8(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r9, %rsi
	call	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm@PLT
	testq	%rax, %rax
	je	.L715
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L689
	movq	%rax, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_6ObjectEEE@PLT
	movq	%rax, 0(%r13)
.L689:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L716
	.p2align 4,,10
	.p2align 3
.L687:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L717
.L690:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L694:
	movl	$1, %r12d
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L714:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L717:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L691
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L691:
	testq	%r13, %r13
	je	.L690
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L716:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L715:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L687
.L713:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7744:
	.size	napi_create_buffer_copy, .-napi_create_buffer_copy
	.p2align 4
	.globl	napi_is_buffer
	.type	napi_is_buffer, @function
napi_is_buffer:
.LFB7745:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L722
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L721
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L721
	movq	%rsi, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L721:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L722:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7745:
	.size	napi_is_buffer, .-napi_is_buffer
	.p2align 4
	.globl	napi_get_buffer_info
	.type	napi_get_buffer_info, @function
napi_get_buffer_info:
.LFB7746:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L735
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L746
	movq	%rdx, %r14
	movq	%rcx, %r13
	testq	%rdx, %rdx
	je	.L733
	movq	%rsi, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, (%r14)
.L733:
	testq	%r13, %r13
	je	.L734
	movq	%r12, %rdi
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, 0(%r13)
.L734:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L746:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L735:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7746:
	.size	napi_get_buffer_info, .-napi_get_buffer_info
	.p2align 4
	.globl	napi_get_node_version
	.type	napi_get_node_version, @function
napi_get_node_version:
.LFB7747:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L750
	testq	%rsi, %rsi
	je	.L751
	leaq	_ZZ21napi_get_node_versionE7version(%rip), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L751:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L750:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7747:
	.size	napi_get_node_version, .-napi_get_node_version
	.p2align 4
	.globl	napi_create_async_work
	.type	napi_create_async_work, @function
napi_create_async_work:
.LFB7764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%r8, -88(%rbp)
	movq	16(%rbp), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L765
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L755
	testq	%r13, %r13
	je	.L755
	movq	%rsi, %rdi
	movq	16(%rbx), %rsi
	movq	%rdx, %r15
	movq	%r9, %r14
	movq	%rsi, -96(%rbp)
	testq	%rdi, %rdi
	je	.L756
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-96(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L775
.L757:
	movq	%r9, -96(%rbp)
	testq	%r15, %r15
	je	.L755
	movq	%r15, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	-96(%rbp), %r9
	testq	%rax, %rax
	je	.L776
	movl	$216, %edi
	movq	%r9, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	leaq	-80(%rbp), %r10
	movq	-96(%rbp), %rdx
	movq	8(%rbx), %rsi
	movq	%r10, %rdi
	movq	%rax, %r15
	movq	%r10, -96(%rbp)
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-104(%rbp), %r9
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	movsd	.LC8(%rip), %xmm0
	movq	-80(%rbp), %rcx
	movq	%r9, %rdx
	call	_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd@PLT
	movq	-96(%rbp), %r10
	movq	%r10, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L774
	movq	%rdx, %rdi
	movq	%rdx, -96(%rbp)
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	movq	-96(%rbp), %rdx
	cmpl	$35, %eax
	jbe	.L774
	movq	(%rdx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L774
	movq	271(%rax), %rax
	leaq	16+_ZTVN4node14ThreadPoolWorkE(%rip), %rcx
	movq	%rcx, 40(%r15)
	movq	%rax, 48(%r15)
	testq	%rax, %rax
	je	.L760
	movq	%rbx, %xmm0
	movq	%r14, %xmm1
	leaq	16+_ZTVN12_GLOBAL__N_16uvimpl4WorkE(%rip), %rax
	movq	%r15, 0(%r13)
	punpcklqdq	%xmm1, %xmm0
	movq	%rax, (%r15)
	addq	$48, %rax
	movups	%xmm0, 184(%r15)
	movq	%r12, %xmm0
	movhps	-88(%rbp), %xmm0
	movq	%rax, 40(%r15)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movups	%xmm0, 200(%r15)
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L755:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L752:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L777
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L765:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L756:
	movq	8(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	%rax, %r9
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L776:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$3, %eax
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L775:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$2, %eax
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L774:
	leaq	16+_ZTVN4node14ThreadPoolWorkE(%rip), %rax
	movq	$0, 48(%r15)
	movq	%rax, 40(%r15)
.L760:
	leaq	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L777:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7764:
	.size	napi_create_async_work, .-napi_create_async_work
	.p2align 4
	.globl	napi_delete_async_work
	.type	napi_delete_async_work, @function
napi_delete_async_work:
.LFB7765:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L781
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L786
	leaq	16+_ZTVN12_GLOBAL__N_16uvimpl4WorkE(%rip), %rax
	movq	%rsi, %rdi
	movq	%rax, (%rsi)
	call	_ZN4node13AsyncResourceD2Ev@PLT
	movq	%r12, %rdi
	movl	$216, %esi
	call	_ZdlPvm@PLT
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L781:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7765:
	.size	napi_delete_async_work, .-napi_delete_async_work
	.section	.text.unlikely
.LCOLDB9:
	.text
.LHOTB9:
	.p2align 4
	.globl	napi_get_uv_event_loop
	.type	napi_get_uv_event_loop, @function
napi_get_uv_event_loop:
.LFB7766:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L791
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L799
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L790
	movq	%r13, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L790
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L790
	movq	271(%rax), %rax
	movq	360(%rax), %rax
	movq	2360(%rax), %rax
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	napi_get_uv_event_loop.cold, @function
napi_get_uv_event_loop.cold:
.LFSB7766:
.L790:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7766:
	.text
	.size	napi_get_uv_event_loop, .-napi_get_uv_event_loop
	.section	.text.unlikely
	.size	napi_get_uv_event_loop.cold, .-napi_get_uv_event_loop.cold
.LCOLDE9:
	.text
.LHOTE9:
	.p2align 4
	.globl	napi_queue_async_work
	.type	napi_queue_async_work, @function
napi_queue_async_work:
.LFB7767:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L805
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L813
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L803
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
.L803:
	movq	$0, 96(%rbx)
	movq	48(%r12), %rax
	leaq	56(%r12), %rsi
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_siE0_4_FUNES2_i(%rip), %rcx
	movq	$0, 88(%rbx)
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvENUlP9uv_work_sE_4_FUNES2_(%rip), %rdx
	addl	$1, 2156(%rax)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_queue_work@PLT
	testl	%eax, %eax
	jne	.L814
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L813:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L814:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7767:
	.size	napi_queue_async_work, .-napi_queue_async_work
	.p2align 4
	.globl	napi_cancel_async_work
	.type	napi_cancel_async_work, @function
napi_cancel_async_work:
.LFB7768:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L820
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L827
	leaq	56(%rsi), %rdi
	call	uv_cancel@PLT
	movl	%eax, %edx
	cmpl	$-22, %eax
	je	.L821
	testl	%eax, %eax
	je	.L819
	xorl	%eax, %eax
	cmpl	$-125, %edx
	sete	%al
	leal	9(%rax,%rax), %eax
.L818:
	movl	%eax, 100(%rbx)
	movl	%edx, 96(%rbx)
	movq	$0, 88(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L820:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L819:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L821:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L818
	.cfi_endproc
.LFE7768:
	.size	napi_cancel_async_work, .-napi_cancel_async_work
	.section	.text.unlikely
.LCOLDB10:
	.text
.LHOTB10:
	.p2align 4
	.globl	napi_create_threadsafe_function
	.type	napi_create_threadsafe_function, @function
napi_create_threadsafe_function:
.LFB7769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movq	16(%rbp), %r10
	movq	32(%rbp), %r11
	movq	40(%rbp), %r9
	movq	%rax, -88(%rbp)
	movq	48(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L857
	movq	%rdi, %rbx
	movq	%rcx, %r14
	testq	%rcx, %rcx
	je	.L831
	testq	%r12, %r12
	jne	.L894
.L831:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L828:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L895
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L831
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L896
	movq	%rsi, %rdi
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rdx, -96(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r8
	testb	%al, %al
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	movq	-128(%rbp), %r9
	je	.L831
.L833:
	movq	16(%rbx), %rsi
	movq	%r9, -136(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rsi, -96(%rbp)
	testq	%rdx, %rdx
	je	.L897
	movq	%rdx, %rdi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	movq	%rax, -128(%rbp)
	movq	-136(%rbp), %r9
	je	.L898
.L835:
	movq	%r14, %rdi
	movq	%r9, -120(%rbp)
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r8, -96(%rbp)
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	-96(%rbp), %r8
	movq	-104(%rbp), %r10
	testq	%rax, %rax
	movq	-112(%rbp), %r11
	movq	-120(%rbp), %r9
	je	.L899
	movl	$496, %edi
	movq	%r9, -136(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r8, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdx
	movq	8(%rbx), %rsi
	movq	%rax, %r14
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -96(%rbp)
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	8(%rbx), %rsi
	movq	-80(%rbp), %rcx
	movq	%r14, %rdi
	movsd	.LC8(%rip), %xmm0
	movq	-128(%rbp), %rdx
	call	_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd@PLT
	movq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE(%rip), %rax
	movq	%rax, (%r14)
	leaq	40(%r14), %rax
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	uv_mutex_init@PLT
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r10
	testl	%eax, %eax
	movq	-120(%rbp), %r11
	movq	-136(%rbp), %r9
	jne	.L900
	pxor	%xmm0, %xmm0
	movl	$64, %edi
	movq	$8, 96(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 104(%r14)
	movups	%xmm0, 120(%r14)
	movups	%xmm0, 136(%r14)
	movups	%xmm0, 152(%r14)
	movq	%r9, -128(%rbp)
	movq	%r11, -120(%rbp)
	movq	%r10, -112(%rbp)
	movq	%r8, -104(%rbp)
	call	_Znwm@PLT
	movq	96(%r14), %rdx
	movl	$512, %edi
	movq	%rax, 88(%r14)
	leaq	-4(,%rdx,4), %rdx
	andq	$-8, %rdx
	addq	%rax, %rdx
	movq	%rdx, -96(%rbp)
	call	_Znwm@PLT
	movq	-96(%rbp), %rdx
	movq	-128(%rbp), %r9
	movq	%r12, 416(%r14)
	leaq	512(%rax), %rcx
	movq	%rax, %xmm0
	movq	-120(%rbp), %r11
	movq	%rax, 144(%r14)
	movq	%rax, (%rdx)
	testq	%r9, %r9
	movq	-104(%rbp), %r8
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, 136(%r14)
	movq	-88(%rbp), %rax
	movq	-112(%rbp), %r10
	movq	%rdx, 128(%r14)
	movq	%rax, 472(%r14)
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction6CallJsEP10napi_env__P12napi_value__PvS6_(%rip), %rax
	movq	8(%rbx), %rdi
	cmove	%rax, %r9
	movq	%rcx, 120(%r14)
	movq	%rdx, 160(%r14)
	movq	%rcx, 152(%r14)
	movb	$0, 424(%r14)
	movq	%r11, 432(%r14)
	movq	%r8, 440(%r14)
	movq	$0, 448(%r14)
	movq	%rbx, 456(%r14)
	movq	%r10, 464(%r14)
	movq	%r9, 480(%r14)
	movb	$0, 488(%r14)
	movups	%xmm0, 104(%r14)
	testq	%r13, %r13
	je	.L839
	movq	%r13, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 448(%r14)
	movq	456(%r14), %rax
	movq	8(%rax), %rdi
.L839:
	movq	%r14, %rdx
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv(%rip), %rsi
	call	_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	movq	456(%r14), %rax
	movq	16(%rax), %r12
	addl	$1, 112(%rax)
	testq	%r12, %r12
	je	.L840
	movq	%r12, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L840
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L840
	movq	271(%rax), %rax
	leaq	168(%r14), %r12
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_s(%rip), %rdx
	movq	%r12, %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %r13
	movq	%r13, %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L841
	cmpq	$0, 440(%r14)
	jne	.L901
.L843:
	leaq	296(%r14), %rsi
	movq	%r13, %rdi
	call	uv_idle_init@PLT
	testl	%eax, %eax
	jne	.L847
	movq	%r14, (%r15)
.L848:
	movl	%eax, 100(%rbx)
	movl	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L896:
	testq	%r9, %r9
	jne	.L833
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L857:
	movl	$1, %eax
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L901:
	movl	$48, %edi
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, -88(%rbp)
	call	uv_cond_init@PLT
	movq	-88(%rbp), %rdx
	testl	%eax, %eax
	jne	.L902
	movq	80(%r14), %rdi
	movq	%rdx, 80(%r14)
	testq	%rdi, %rdi
	je	.L843
	movq	%rdi, -88(%rbp)
	call	uv_cond_destroy@PLT
	movq	-88(%rbp), %rdi
	movl	$48, %esi
	call	_ZdlPvm@PLT
	cmpq	$0, 440(%r14)
	je	.L843
	cmpq	$0, 80(%r14)
	jne	.L843
	movq	456(%r14), %rax
	movq	16(%rax), %r13
	testq	%r13, %r13
	je	.L849
	movq	%r13, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L849
	movq	0(%r13), %rax
	movq	55(%rax), %rax
	movq	295(%rax), %rcx
	cmpq	%rcx, _ZN4node11Environment18kNodeContextTagPtrE(%rip)
	jne	.L849
	movq	271(%rax), %r13
	movl	$24, %edi
	addl	$1, 2152(%r13)
	call	_Znwm@PLT
	movq	168(%r14), %rdx
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvEUlPS2_E_EEvPT_T0_ENUlS6_E_4_FUNES6_(%rip), %rsi
	movq	%r12, %rdi
	movq	%r13, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 168(%r14)
	call	uv_close@PLT
	movl	$9, %eax
	jmp	.L848
.L841:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE(%rip), %rax
	movq	%r14, %rdx
	leaq	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7CleanupEPv(%rip), %rsi
	movq	%rax, (%r14)
	movq	456(%r14), %rax
	movq	8(%rax), %rdi
	call	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_@PLT
	movq	456(%r14), %rdi
	subl	$1, 112(%rdi)
	jne	.L850
	movq	(%rdi), %rax
	call	*8(%rax)
.L850:
	movq	448(%r14), %rdi
	testq	%rdi, %rdi
	je	.L851
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L851:
	movq	88(%r14), %rdi
	testq	%rdi, %rdi
	je	.L852
	movq	160(%r14), %rax
	movq	128(%r14), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L853
	.p2align 4,,10
	.p2align 3
.L854:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L854
	movq	88(%r14), %rdi
.L853:
	call	_ZdlPv@PLT
.L852:
	movq	80(%r14), %r12
	testq	%r12, %r12
	je	.L855
	movq	%r12, %rdi
	call	uv_cond_destroy@PLT
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L855:
	movq	-144(%rbp), %rdi
	call	uv_mutex_destroy@PLT
	movq	%r14, %rdi
	call	_ZN4node13AsyncResourceD2Ev@PLT
	movl	$496, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movl	$9, %eax
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L897:
	movq	8(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %r8
	movq	-112(%rbp), %r10
	movq	-120(%rbp), %r11
	movq	%rax, -128(%rbp)
	movq	-136(%rbp), %r9
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L899:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$3, %eax
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L898:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$2, %eax
	jmp	.L828
.L900:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L847:
	leaq	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L902:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L895:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	napi_create_threadsafe_function.cold, @function
napi_create_threadsafe_function.cold:
.LFSB7769:
.L849:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	2152, %eax
	ud2
.L840:
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7769:
	.text
	.size	napi_create_threadsafe_function, .-napi_create_threadsafe_function
	.section	.text.unlikely
	.size	napi_create_threadsafe_function.cold, .-napi_create_threadsafe_function.cold
.LCOLDE10:
	.text
.LHOTE10:
	.p2align 4
	.globl	napi_get_threadsafe_function_context
	.type	napi_get_threadsafe_function_context, @function
napi_get_threadsafe_function_context:
.LFB7770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testq	%rdi, %rdi
	je	.L907
	testq	%rsi, %rsi
	je	.L908
	movq	432(%rdi), %rax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L907:
	.cfi_restore_state
	leaq	_ZZ36napi_get_threadsafe_function_contextE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L908:
	leaq	_ZZ36napi_get_threadsafe_function_contextE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7770:
	.size	napi_get_threadsafe_function_context, .-napi_get_threadsafe_function_context
	.p2align 4
	.globl	napi_acquire_threadsafe_function
	.type	napi_acquire_threadsafe_function, @function
napi_acquire_threadsafe_function:
.LFB7772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdi, %rdi
	je	.L914
	leaq	40(%rdi), %r12
	movq	%rdi, %rbx
	movl	$16, %r13d
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	cmpb	$0, 424(%rbx)
	jne	.L911
	addq	$1, 416(%rbx)
	xorl	%r13d, %r13d
.L911:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L914:
	.cfi_restore_state
	leaq	_ZZ32napi_acquire_threadsafe_functionE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7772:
	.size	napi_acquire_threadsafe_function, .-napi_acquire_threadsafe_function
	.p2align 4
	.globl	napi_release_threadsafe_function
	.type	napi_release_threadsafe_function, @function
napi_release_threadsafe_function:
.LFB7773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rdi, %rdi
	je	.L936
	leaq	40(%rdi), %r13
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	$1, %r14d
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	416(%rbx), %rax
	testq	%rax, %rax
	je	.L917
	subq	$1, %rax
	movq	%rax, 416(%rbx)
	jne	.L937
	cmpb	$0, 424(%rbx)
	jne	.L920
	cmpl	$1, %r12d
	sete	424(%rbx)
	je	.L921
.L922:
	leaq	168(%rbx), %rdi
	movl	$9, %r14d
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L917
.L920:
	xorl	%r14d, %r14d
.L917:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_restore_state
	cmpl	$1, %r12d
	jne	.L920
	cmpb	$0, 424(%rbx)
	jne	.L920
	movb	$1, 424(%rbx)
.L921:
	cmpq	$0, 440(%rbx)
	je	.L922
	movq	80(%rbx), %rdi
	call	uv_cond_signal@PLT
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L936:
	leaq	_ZZ32napi_release_threadsafe_functionE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7773:
	.size	napi_release_threadsafe_function, .-napi_release_threadsafe_function
	.p2align 4
	.globl	napi_unref_threadsafe_function
	.type	napi_unref_threadsafe_function, @function
napi_unref_threadsafe_function:
.LFB7774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rsi, %rsi
	je	.L941
	movq	%rsi, %rbx
	leaq	168(%rsi), %rdi
	call	uv_unref@PLT
	leaq	296(%rbx), %rdi
	call	uv_unref@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L941:
	.cfi_restore_state
	leaq	_ZZ30napi_unref_threadsafe_functionE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7774:
	.size	napi_unref_threadsafe_function, .-napi_unref_threadsafe_function
	.p2align 4
	.globl	napi_ref_threadsafe_function
	.type	napi_ref_threadsafe_function, @function
napi_ref_threadsafe_function:
.LFB7775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	testq	%rsi, %rsi
	je	.L945
	movq	%rsi, %rbx
	leaq	168(%rsi), %rdi
	call	uv_ref@PLT
	leaq	296(%rbx), %rdi
	call	uv_ref@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L945:
	.cfi_restore_state
	leaq	_ZZ28napi_ref_threadsafe_functionE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7775:
	.size	napi_ref_threadsafe_function, .-napi_ref_threadsafe_function
	.section	.text._ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB9309:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$24, %rsp
	call	_Znwm@PLT
	movdqu	0(%r13), %xmm0
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rax, %r12
	movq	$0, (%rax)
	movups	%xmm0, 8(%rax)
	movq	16(%r13), %rax
	movq	16(%r12), %r13
	movq	%rax, 24(%r12)
	movq	%r13, %rax
	divq	%rsi
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	leaq	0(,%rdx,8), %r15
	testq	%rdi, %rdi
	je	.L947
	movq	(%rdi), %rax
	movq	%rdx, %r8
	movq	32(%rax), %rcx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L948:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L947
	movq	32(%r9), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L947
	movq	%r9, %rax
.L950:
	cmpq	%rcx, %r13
	jne	.L948
	movq	8(%rax), %rcx
	cmpq	%rcx, 8(%r12)
	jne	.L948
	cmpq	16(%rax), %r13
	jne	.L948
	movq	(%rdi), %r14
	testq	%r14, %r14
	je	.L947
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	addq	$24, %rsp
	movq	%r14, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L947:
	.cfi_restore_state
	movq	24(%rbx), %rdx
	leaq	32(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L952
	movq	(%rbx), %r8
	movq	%r13, 32(%r12)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L962
.L986:
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%r15), %rax
	movq	%r12, (%rax)
.L963:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r12, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L952:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L984
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L985
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%rbx), %r10
	movq	%rax, %r8
.L955:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L957
	xorl	%edi, %edi
	leaq	16(%rbx), %r9
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L959:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L960:
	testq	%rsi, %rsi
	je	.L957
.L958:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L959
	movq	16(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L966
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L958
	.p2align 4,,10
	.p2align 3
.L957:
	movq	(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L961
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L961:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 8(%rbx)
	divq	%r14
	movq	%r8, (%rbx)
	movq	%r13, 32(%r12)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L986
.L962:
	movq	16(%rbx), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.L964
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r12, (%r8,%rdx,8)
.L964:
	leaq	16(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L966:
	movq	%rdx, %rdi
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L984:
	leaq	48(%rbx), %r8
	movq	$0, 48(%rbx)
	movq	%r8, %r10
	jmp	.L955
.L985:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9309:
	.size	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"Module has no declared entry point."
	.section	.rodata.str1.1
.LC12:
	.string	"exports"
	.section	.text.unlikely
.LCOLDB13:
	.text
.LHOTB13:
	.p2align 4
	.globl	_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E
	.type	_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E, @function
_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E:
.LFB7730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1025
	movq	%rdi, %r14
	movl	$128, %edi
	movq	%rcx, %rbx
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rax, (%r12)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, 8(%r12)
	movq	%rax, %r15
	testq	%r13, %r13
	je	.L1008
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	8(%r12), %r15
.L993:
	movq	%rax, 16(%r12)
	leaq	16+_ZTVN6v8impl10RefTrackerE(%rip), %rax
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movq	$0, 48(%r12)
	movq	%rax, 56(%r12)
	movq	$0, 64(%r12)
	movq	$0, 72(%r12)
	movq	$0, 104(%r12)
	movl	$1, 112(%r12)
	movq	$0, 120(%r12)
	call	_ZN2v87Context10GetIsolateEv@PLT
	cmpq	%rax, %r15
	jne	.L1026
	movq	16(%r12), %r13
	leaq	16+_ZTV15node_napi_env__(%rip), %rax
	movq	%rax, (%r12)
	testq	%r13, %r13
	je	.L996
	movq	%r13, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L996
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %r15
	movq	55(%rax), %rax
	cmpq	%r15, 295(%rax)
	jne	.L996
	cmpq	$0, 271(%rax)
	je	.L996
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L999
	movq	%r13, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L999
	movq	0(%r13), %rax
	movq	55(%rax), %rax
	cmpq	%r15, 295(%rax)
	jne	.L999
	movq	271(%rax), %rdi
	leaq	_ZZN6v8impl12_GLOBAL__N_1L6NewEnvEN2v85LocalINS1_7ContextEEEENUlPvE_4_FUNES5_(%rip), %rcx
	leaq	-80(%rbp), %rsi
	movq	2640(%rdi), %rax
	addq	$2584, %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rdi)
	movq	%rcx, -80(%rbp)
	movq	%r12, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE10_M_emplaceIJS1_EEESt4pairINS3_14_Node_iteratorIS1_Lb1ELb1EEEbESt17integral_constantIbLb1EEDpOT_
	testb	%dl, %dl
	je	.L1027
	movl	104(%r12), %r13d
	movl	108(%r12), %r15d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	call	*%rbx
	movq	%rax, %rbx
	cmpl	104(%r12), %r13d
	jne	.L1028
	cmpl	108(%r12), %r15d
	jne	.L1029
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L1004
	movq	8(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1004
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%r12)
.L1004:
	testq	%rbx, %rbx
	je	.L987
	cmpq	%r14, %rbx
	je	.L987
	movq	-88(%rbp), %rsi
	movq	%rbx, %rcx
	leaq	.LC12(%rip), %rdx
	movq	%r12, %rdi
	call	napi_set_named_property@PLT
.L987:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1030
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L990
	movq	%rdx, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L990
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L990
	movq	271(%rax), %rbx
	testq	%rbx, %rbx
	je	.L990
	movq	352(%rbx), %rsi
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r13
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1031
.L991:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L996:
	leaq	_ZZN15node_napi_env__C4EN2v85LocalINS0_7ContextEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1008:
	xorl	%eax, %eax
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L990:
	leaq	_ZZ30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_EE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1026:
	leaq	_ZZN10napi_env__C4EN2v85LocalINS0_7ContextEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1027:
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1028:
	leaq	_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1029:
	leaq	_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L991
.L1030:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E.cold, @function
_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E.cold:
.LFSB7730:
.L999:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2640, %rax
	ud2
	.cfi_endproc
.LFE7730:
	.text
	.size	_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E, .-_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E
	.section	.text.unlikely
	.size	_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E.cold, .-_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E.cold
.LCOLDE13:
	.text
.LHOTE13:
	.p2align 4
	.type	_ZL23napi_module_register_cbN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, @function
_ZL23napi_module_register_cbN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv:
.LFB7729:
	.cfi_startproc
	endbr64
	movq	16(%rcx), %rcx
	jmp	_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E
	.cfi_endproc
.LFE7729:
	.size	_ZL23napi_module_register_cbN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv, .-_ZL23napi_module_register_cbN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPv
	.section	.rodata._ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC14:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	.type	_ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_, @function
_ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_:
.LFB9417:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L1042
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L1043
.L1035:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	%rax, 8(%r14)
	movq	(%r12), %rdx
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 72(%rbx)
	movq	8(%rax), %rax
	leaq	512(%rax), %rdx
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1043:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1044
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L1045
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1040
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L1040:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L1038:
	movq	%r15, 40(%rbx)
	movq	(%r15), %rax
	leaq	(%r15,%r13), %r14
	movq	(%r15), %xmm0
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L1035
	.p2align 4,,10
	.p2align 3
.L1044:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1037
	cmpq	%r14, %rsi
	je	.L1038
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1037:
	cmpq	%r14, %rsi
	je	.L1038
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1038
.L1042:
	leaq	.LC14(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1045:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9417:
	.size	_ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_, .-_ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	.text
	.p2align 4
	.globl	napi_call_threadsafe_function
	.type	napi_call_threadsafe_function, @function
napi_call_threadsafe_function:
.LFB7771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1074
	leaq	40(%rdi), %r12
	movq	%rdi, %rbx
	movl	%edx, %r13d
	movq	%rsi, -48(%rbp)
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	testl	%r13d, %r13d
	jne	.L1051
	jmp	.L1048
	.p2align 4,,10
	.p2align 3
.L1075:
	testq	%rsi, %rsi
	je	.L1049
	testb	%dil, %dil
	jne	.L1053
	movq	80(%rbx), %rdi
	movq	%r12, %rsi
	call	uv_cond_wait@PLT
.L1051:
	movq	160(%rbx), %rdx
	subq	128(%rbx), %rdx
	sarq	$3, %rdx
	movq	136(%rbx), %rax
	subq	144(%rbx), %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	440(%rbx), %rsi
	movzbl	424(%rbx), %edi
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	120(%rbx), %rax
	subq	104(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	jbe	.L1075
.L1049:
	testb	%dil, %dil
	jne	.L1053
	leaq	168(%rbx), %rdi
	movl	$9, %r13d
	call	uv_async_send@PLT
	testl	%eax, %eax
	je	.L1076
	.p2align 4,,10
	.p2align 3
.L1052:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1077
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1053:
	.cfi_restore_state
	movq	416(%rbx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	je	.L1052
	subq	$1, %rax
	movl	$16, %r13d
	movq	%rax, 416(%rbx)
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	160(%rbx), %rdx
	subq	128(%rbx), %rdx
	sarq	$3, %rdx
	movq	136(%rbx), %rax
	subq	144(%rbx), %rax
	subq	$1, %rdx
	sarq	$3, %rax
	movq	440(%rbx), %rsi
	movzbl	424(%rbx), %edi
	movq	%rdx, %rcx
	salq	$6, %rcx
	leaq	(%rcx,%rax), %rdx
	movq	120(%rbx), %rax
	subq	104(%rbx), %rax
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rsi, %rax
	jb	.L1049
	testq	%rsi, %rsi
	je	.L1049
	testb	%dil, %dil
	jne	.L1053
	movl	$15, %r13d
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	152(%rbx), %rax
	movq	136(%rbx), %rdx
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L1055
	movq	-48(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, (%rdx)
	addq	$8, 136(%rbx)
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1074:
	leaq	_ZZ29napi_call_threadsafe_functionE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1055:
	leaq	-48(%rbp), %rsi
	leaq	88(%rbx), %rdi
	xorl	%r13d, %r13d
	call	_ZNSt5dequeIPvSaIS0_EE16_M_push_back_auxIJRKS0_EEEvDpOT_
	jmp	.L1052
.L1077:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7771:
	.size	napi_call_threadsafe_function, .-napi_call_threadsafe_function
	.weak	_ZTVN4node14ThreadPoolWorkE
	.section	.data.rel.ro._ZTVN4node14ThreadPoolWorkE,"awG",@progbits,_ZTVN4node14ThreadPoolWorkE,comdat
	.align 8
	.type	_ZTVN4node14ThreadPoolWorkE, @object
	.size	_ZTVN4node14ThreadPoolWorkE, 48
_ZTVN4node14ThreadPoolWorkE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN6v8impl10RefTrackerE
	.section	.data.rel.ro.local._ZTVN6v8impl10RefTrackerE,"awG",@progbits,_ZTVN6v8impl10RefTrackerE,comdat
	.align 8
	.type	_ZTVN6v8impl10RefTrackerE, @object
	.size	_ZTVN6v8impl10RefTrackerE, 40
_ZTVN6v8impl10RefTrackerE:
	.quad	0
	.quad	0
	.quad	_ZN6v8impl10RefTrackerD1Ev
	.quad	_ZN6v8impl10RefTrackerD0Ev
	.quad	_ZN6v8impl10RefTracker8FinalizeEb
	.weak	_ZTV10napi_env__
	.section	.data.rel.ro.local._ZTV10napi_env__,"awG",@progbits,_ZTV10napi_env__,comdat
	.align 8
	.type	_ZTV10napi_env__, @object
	.size	_ZTV10napi_env__, 48
_ZTV10napi_env__:
	.quad	0
	.quad	0
	.quad	_ZN10napi_env__D1Ev
	.quad	_ZN10napi_env__D0Ev
	.quad	_ZNK10napi_env__16can_call_into_jsEv
	.quad	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.weak	_ZTV15node_napi_env__
	.section	.data.rel.ro.local._ZTV15node_napi_env__,"awG",@progbits,_ZTV15node_napi_env__,comdat
	.align 8
	.type	_ZTV15node_napi_env__, @object
	.size	_ZTV15node_napi_env__, 48
_ZTV15node_napi_env__:
	.quad	0
	.quad	0
	.quad	_ZN15node_napi_env__D1Ev
	.quad	_ZN15node_napi_env__D0Ev
	.quad	_ZNK15node_napi_env__16can_call_into_jsEv
	.quad	_ZNK15node_napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE, @object
	.size	_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE, 32
_ZTVN6v8impl12_GLOBAL__N_118ThreadSafeFunctionE:
	.quad	0
	.quad	0
	.quad	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD1Ev
	.quad	_ZN6v8impl12_GLOBAL__N_118ThreadSafeFunctionD0Ev
	.align 8
	.type	_ZTVN12_GLOBAL__N_16uvimpl4WorkE, @object
	.size	_ZTVN12_GLOBAL__N_16uvimpl4WorkE, 96
_ZTVN12_GLOBAL__N_16uvimpl4WorkE:
	.quad	0
	.quad	0
	.quad	_ZN12_GLOBAL__N_16uvimpl4WorkD1Ev
	.quad	_ZN12_GLOBAL__N_16uvimpl4WorkD0Ev
	.quad	_ZN12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv
	.quad	_ZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi
	.quad	-40
	.quad	0
	.quad	_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD1Ev
	.quad	_ZThn40_N12_GLOBAL__N_16uvimpl4WorkD0Ev
	.quad	_ZThn40_N12_GLOBAL__N_16uvimpl4Work16DoThreadPoolWorkEv
	.quad	_ZThn40_N12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEi
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvEUlS2_E_E4CallES2_
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC15:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC17:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.section	.rodata.str1.1
.LC18:
	.string	"../src/js_native_api_v8.h:97"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"(open_callback_scopes) == (open_callback_scopes_before)"
	.align 8
.LC20:
	.string	"void napi_env__::CallIntoModule(T&&, U&&) [with T = {anonymous}::uvimpl::Work::AfterThreadPoolWork(int)::<lambda(napi_env)>; U = {anonymous}::uvimpl::Work::AfterThreadPoolWork(int)::<lambda(napi_env, v8::Local<v8::Value>)>]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args_0, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args_0, 24
_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args_0:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"../src/js_native_api_v8.h:96"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"(open_handle_scopes) == (open_handle_scopes_before)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args, 24
_ZZN10napi_env__14CallIntoModuleIZN12_GLOBAL__N_16uvimpl4Work19AfterThreadPoolWorkEiEUlPS_E_ZNS3_19AfterThreadPoolWorkEiEUlS4_N2v85LocalINS6_5ValueEEEE0_EEvOT_OT0_E4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC20
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"void napi_env__::CallIntoModule(T&&, U&&) [with T = napi_module_register_by_symbol(v8::Local<v8::Object>, v8::Local<v8::Value>, v8::Local<v8::Context>, napi_addon_register_func)::<lambda(napi_env)>; U = void(napi_env__*, v8::Local<v8::Value>)]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args_0, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args_0, 24
_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args_0:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC23
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args, 24
_ZZN10napi_env__14CallIntoModuleIZ30napi_module_register_by_symbolN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPFP12napi_value__PS_SA_EEUlSB_E_FvSB_S6_EEEvOT_OT0_E4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"void napi_env__::CallIntoModule(T&&, U&&) [with T = v8impl::{anonymous}::ThreadSafeFunction::Finalize()::<lambda(napi_env)>; U = void(napi_env__*, v8::Local<v8::Value>)]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC24
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction8FinalizeEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC24
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"void napi_env__::CallIntoModule(T&&, U&&) [with T = v8impl::{anonymous}::ThreadSafeFunction::DispatchOne()::<lambda(napi_env)>; U = void(napi_env__*, v8::Local<v8::Value>)]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC25
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC25
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC26:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC28:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"void napi_env__::CallIntoModule(T&&, U&&) [with T = v8impl::{anonymous}::BufferFinalizer::FinalizeBufferCallback(char*, void*)::<lambda(node::Environment*)>::<lambda(napi_env)>; U = void(napi_env__*, v8::Local<v8::Value>)]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args_0, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args_0, 24
_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args_0:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC29
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args, 24
_ZZN10napi_env__14CallIntoModuleIZZN6v8impl12_GLOBAL__N_115BufferFinalizer22FinalizeBufferCallbackEPcPvENKUlPN4node11EnvironmentEE_clES8_EUlPS_E_FvSA_N2v85LocalINSC_5ValueEEEEEEvOT_OT0_E4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/node_api.cc:1078"
.LC31:
	.string	"(func) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"napi_status napi_ref_threadsafe_function(napi_env, napi_threadsafe_function)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ28napi_ref_threadsafe_functionE4args, @object
	.size	_ZZ28napi_ref_threadsafe_functionE4args, 24
_ZZ28napi_ref_threadsafe_functionE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/node_api.cc:1072"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"napi_status napi_unref_threadsafe_function(napi_env, napi_threadsafe_function)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ30napi_unref_threadsafe_functionE4args, @object
	.size	_ZZ30napi_unref_threadsafe_functionE4args, 24
_ZZ30napi_unref_threadsafe_functionE4args:
	.quad	.LC33
	.quad	.LC31
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"../src/node_api.cc:1066"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"napi_status napi_release_threadsafe_function(napi_threadsafe_function, napi_threadsafe_function_release_mode)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ32napi_release_threadsafe_functionE4args, @object
	.size	_ZZ32napi_release_threadsafe_functionE4args, 24
_ZZ32napi_release_threadsafe_functionE4args:
	.quad	.LC35
	.quad	.LC31
	.quad	.LC36
	.section	.rodata.str1.1
.LC37:
	.string	"../src/node_api.cc:1059"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"napi_status napi_acquire_threadsafe_function(napi_threadsafe_function)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ32napi_acquire_threadsafe_functionE4args, @object
	.size	_ZZ32napi_acquire_threadsafe_functionE4args, 24
_ZZ32napi_acquire_threadsafe_functionE4args:
	.quad	.LC37
	.quad	.LC31
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_api.cc:1052"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"napi_status napi_call_threadsafe_function(napi_threadsafe_function, void*, napi_threadsafe_function_call_mode)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ29napi_call_threadsafe_functionE4args, @object
	.size	_ZZ29napi_call_threadsafe_functionE4args, 24
_ZZ29napi_call_threadsafe_functionE4args:
	.quad	.LC39
	.quad	.LC31
	.quad	.LC40
	.section	.rodata.str1.1
.LC41:
	.string	"../src/node_api.cc:1042"
.LC42:
	.string	"(result) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"napi_status napi_get_threadsafe_function_context(napi_threadsafe_function, void**)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ36napi_get_threadsafe_function_contextE4args_0, @object
	.size	_ZZ36napi_get_threadsafe_function_contextE4args_0, 24
_ZZ36napi_get_threadsafe_function_contextE4args_0:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/node_api.cc:1041"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ36napi_get_threadsafe_function_contextE4args, @object
	.size	_ZZ36napi_get_threadsafe_function_contextE4args, 24
_ZZ36napi_get_threadsafe_function_contextE4args:
	.quad	.LC44
	.quad	.LC31
	.quad	.LC43
	.section	.rodata.str1.1
.LC45:
	.string	"node"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ21napi_get_node_versionE7version, @object
	.size	_ZZ21napi_get_node_versionE7version, 24
_ZZ21napi_get_node_versionE7version:
	.long	12
	.long	18
	.long	4
	.zero	4
	.quad	.LC45
	.local	_ZZ18napi_make_callbackE13empty_context
	.comm	_ZZ18napi_make_callbackE13empty_context,16,16
	.section	.rodata.str1.1
.LC46:
	.string	"../src/node_api.cc:448"
.LC47:
	.string	"(node_env) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"void napi_module_register_by_symbol(v8::Local<v8::Object>, v8::Local<v8::Value>, v8::Local<v8::Context>, napi_addon_register_func)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_EE4args, @object
	.size	_ZZ30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_EE4args, 24
_ZZ30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_EE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.section	.rodata.str1.1
.LC49:
	.string	"../src/node_api.cc:395"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"(0) == (uv_idle_start(&ts_fn->idle, IdleCb))"
	.align 8
.LC51:
	.string	"static void v8impl::{anonymous}::ThreadSafeFunction::AsyncCb(uv_async_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_sE4args, @object
	.size	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_sE4args, 24
_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction7AsyncCbEP10uv_async_sE4args:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.section	.rodata.str1.1
.LC52:
	.string	"../src/node_api.cc:296"
.LC53:
	.string	"(0) == (uv_idle_stop(&idle))"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"void v8impl::{anonymous}::ThreadSafeFunction::DispatchOne()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvE4args, @object
	.size	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvE4args, 24
_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction11DispatchOneEvE4args:
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.section	.rodata.str1.1
.LC55:
	.string	"../src/node_api.cc:231"
	.section	.rodata.str1.8
	.align 8
.LC56:
	.string	"(0) == (uv_idle_init(loop, &idle))"
	.align 8
.LC57:
	.string	"napi_status v8impl::{anonymous}::ThreadSafeFunction::Init()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvE4args, @object
	.size	_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvE4args, 24
_ZZN6v8impl12_GLOBAL__N_118ThreadSafeFunction4InitEvE4args:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.weak	_ZZN15node_napi_env__C4EN2v85LocalINS0_7ContextEEEE4args
	.section	.rodata.str1.1
.LC58:
	.string	"../src/node_api.cc:17"
.LC59:
	.string	"(node_env()) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"node_napi_env__::node_napi_env__(v8::Local<v8::Context>)"
	.section	.data.rel.ro.local._ZZN15node_napi_env__C4EN2v85LocalINS0_7ContextEEEE4args,"awG",@progbits,_ZZN15node_napi_env__C4EN2v85LocalINS0_7ContextEEEE4args,comdat
	.align 16
	.type	_ZZN15node_napi_env__C4EN2v85LocalINS0_7ContextEEEE4args, @gnu_unique_object
	.size	_ZZN15node_napi_env__C4EN2v85LocalINS0_7ContextEEEE4args, 24
_ZZN15node_napi_env__C4EN2v85LocalINS0_7ContextEEEE4args:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.weak	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../src/threadpoolwork-inl.h:46"
	.section	.rodata.str1.1
.LC62:
	.string	"(status) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"void node::ThreadPoolWork::ScheduleWork()"
	.section	.data.rel.ro.local._ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args,"awG",@progbits,_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args,comdat
	.align 16
	.type	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args, @gnu_unique_object
	.size	_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args, 24
_ZZN4node14ThreadPoolWork12ScheduleWorkEvE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.weak	_ZZN10napi_env__C4EN2v85LocalINS0_7ContextEEEE4args
	.section	.rodata.str1.1
.LC64:
	.string	"../src/js_native_api_v8.h:58"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"(isolate) == (context->GetIsolate())"
	.align 8
.LC66:
	.string	"napi_env__::napi_env__(v8::Local<v8::Context>)"
	.section	.data.rel.ro.local._ZZN10napi_env__C4EN2v85LocalINS0_7ContextEEEE4args,"awG",@progbits,_ZZN10napi_env__C4EN2v85LocalINS0_7ContextEEEE4args,comdat
	.align 16
	.type	_ZZN10napi_env__C4EN2v85LocalINS0_7ContextEEEE4args, @gnu_unique_object
	.size	_ZZN10napi_env__C4EN2v85LocalINS0_7ContextEEEE4args, 24
_ZZN10napi_env__C4EN2v85LocalINS0_7ContextEEEE4args:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.weak	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC67:
	.string	"../src/node_internals.h:264"
.LC68:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"node::ThreadPoolWork::ThreadPoolWork(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args, 24
_ZZN4node14ThreadPoolWorkC4EPNS_11EnvironmentEE4args:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC70:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC72:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC73:
	.string	"../src/env-inl.h:399"
.LC74:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC73
	.quad	.LC74
	.quad	.LC75
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	0
	.long	-1074790400
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
