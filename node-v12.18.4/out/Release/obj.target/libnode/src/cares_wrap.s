	.file	"cares_wrap.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4289:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4289:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4291:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4291:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4588:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4588:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4589:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4589:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4858:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4858:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4859:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4859:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7098:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7538:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7538:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7539:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7539:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7540:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7540:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7542:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L13
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7542:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task8SelfSizeEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task8SelfSizeEv:
.LFB7595:
	.cfi_startproc
	endbr64
	movl	$184, %eax
	ret
	.cfi_endproc
.LFE7595:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task8SelfSizeEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap8SelfSizeEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap8SelfSizeEv:
.LFB7607:
	.cfi_startproc
	endbr64
	movl	$144, %eax
	ret
	.cfi_endproc
.LFE7607:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap8SelfSizeEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7633:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7633:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE,_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap8SelfSizeEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap8SelfSizeEv:
.LFB7635:
	.cfi_startproc
	endbr64
	movl	$256, %eax
	ret
	.cfi_endproc
.LFE7635:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap8SelfSizeEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap8SelfSizeEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap8SelfSizeEv:
.LFB7642:
	.cfi_startproc
	endbr64
	movl	$1408, %eax
	ret
	.cfi_endproc
.LFE7642:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap8SelfSizeEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv:
.LFB7721:
	.cfi_startproc
	endbr64
	movl	$88, %eax
	ret
	.cfi_endproc
.LFE7721:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.set	_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap8SelfSizeEv,_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD2Ev:
.LFB12120:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12120:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD2Ev
	.section	.text._ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv:
.LFB12136:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE12136:
	.size	_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv:
.LFB12138:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE12138:
	.size	_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhi:
.LFB7703:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7703:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci:
.LFB7686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKciE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7686:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent:
.LFB7704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostentE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7704:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKc:
.LFB7685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKcE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7685:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKc
	.section	.text._ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_
	.type	_ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_, @function
_ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_:
.LFB10215:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rcx
	leaq	-88(%rdi), %r8
	subl	$1, 2156(%rcx)
	js	.L36
	jmp	*80(%r8)
	.p2align 4,,10
	.p2align 3
.L36:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10215:
	.size	_ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_, .-_ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_
	.section	.text._ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_
	.type	_ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_, @function
_ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_:
.LFB10233:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %r8
	leaq	-88(%rdi), %r9
	subl	$1, 2156(%r8)
	js	.L42
	jmp	*80(%r9)
	.p2align 4,,10
	.p2align 3
.L42:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10233:
	.size	_ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_, .-_ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB12095:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L43
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	ret
	.cfi_endproc
.LFE12095:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD0Ev:
.LFB12122:
	.cfi_startproc
	endbr64
	movl	$184, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12122:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD0Ev
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_118ares_poll_close_cbEP9uv_poll_s, @function
_ZN4node10cares_wrap12_GLOBAL__N_118ares_poll_close_cbEP9uv_poll_s:
.LFB7649:
	.cfi_startproc
	endbr64
	cmpq	$24, %rdi
	je	.L46
	subq	$24, %rdi
	movl	$184, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L46:
	ret
	.cfi_endproc
.LFE7649:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_118ares_poll_close_cbEP9uv_poll_s, .-_ZN4node10cares_wrap12_GLOBAL__N_118ares_poll_close_cbEP9uv_poll_s
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB12097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12097:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112ares_poll_cbEP9uv_poll_sii, @function
_ZN4node10cares_wrap12_GLOBAL__N_112ares_poll_cbEP9uv_poll_sii:
.LFB7647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	-16(%rdi), %r14
	leaq	-24(%rdi), %rbx
	movq	56(%r14), %rdi
	call	uv_timer_again@PLT
	testl	%r13d, %r13d
	js	.L61
	movl	$-1, %edx
	testb	$2, %r12b
	je	.L53
	movl	16(%rbx), %edx
.L53:
	andl	$1, %r12d
	movl	$-1, %esi
	jne	.L62
.L54:
	movq	64(%r14), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ares_process_fd@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	movl	16(%rbx), %esi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L61:
	movl	16(%rbx), %esi
	movq	64(%r14), %rdi
	popq	%rbx
	popq	%r12
	movl	%esi, %edx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ares_process_fd@PLT
	.cfi_endproc
.LFE7647:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112ares_poll_cbEP9uv_poll_sii, .-_ZN4node10cares_wrap12_GLOBAL__N_112ares_poll_cbEP9uv_poll_sii
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_s, @function
_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_s:
.LFB7646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	cmpq	56(%rax), %rdi
	jne	.L67
	cmpq	$0, 112(%rax)
	je	.L68
	movq	64(%rax), %rdi
	movl	$-1, %edx
	movl	$-1, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	ares_process_fd@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7646:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_s, .-_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_s
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$1136, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	8(%rax), %r13
	movl	16(%rdi), %eax
	leaq	88(%r13), %rdx
	testl	%eax, %eax
	jle	.L71
	movq	8(%rdi), %rdx
.L71:
	leaq	-1152(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-1168(%rbp), %r12
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1136(%rbp), %r14
	movq	%r12, %rdx
	movl	$2, %edi
	movq	%r14, %rsi
	call	uv_inet_pton@PLT
	testl	%eax, %eax
	je	.L96
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	$10, %edi
	call	uv_inet_pton@PLT
	testl	%eax, %eax
	je	.L82
	movq	-1136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L69
	leaq	-1128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L69
.L95:
	call	free@PLT
.L69:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L97
	addq	$1136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	movl	$2, %edi
.L72:
	leaq	-96(%rbp), %r14
	movl	$46, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdx
	call	uv_inet_ntop@PLT
	testl	%eax, %eax
	jne	.L98
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L99
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L79:
	movq	-1136(%rbp), %rdi
	leaq	-1128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L69
	testq	%rdi, %rdi
	jne	.L95
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$10, %edi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L79
.L97:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7823:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.type	_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv, @function
_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv:
.LFB12135:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L102
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE12135:
	.size	_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv, .-_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.section	.text._ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.type	_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv, @function
_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv:
.LFB12137:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L105
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE12137:
	.size	_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv, .-_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.section	.text._ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,"axG",@progbits,_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.type	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, @function
_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_:
.LFB7102:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %rdi
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7102:
	.size	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, .-_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.section	.text._ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev,"axG",@progbits,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.type	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, @function
_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev:
.LFB12140:
	.cfi_startproc
	endbr64
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE12140:
	.size	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, .-_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev
	.set	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.text
	.p2align 4
	.type	_ZZN4node11Environment11CloseHandleI10uv_timer_sZNS_10cares_wrap12_GLOBAL__N_111ChannelWrap10CloseTimerEvEUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESC_, @function
_ZZN4node11Environment11CloseHandleI10uv_timer_sZNS_10cares_wrap12_GLOBAL__N_111ChannelWrap10CloseTimerEvEUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESC_:
.LFB8635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$152, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	(%r12), %rax
	subl	$1, 2152(%rax)
	call	_ZdlPvm@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8635:
	.size	_ZZN4node11Environment11CloseHandleI10uv_timer_sZNS_10cares_wrap12_GLOBAL__N_111ChannelWrap10CloseTimerEvEUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESC_, .-_ZZN4node11Environment11CloseHandleI10uv_timer_sZNS_10cares_wrap12_GLOBAL__N_111ChannelWrap10CloseTimerEvEUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESC_
	.section	.text._ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_,"axG",@progbits,_ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_,comdat
	.p2align 4
	.weak	_ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_
	.type	_ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_, @function
_ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_:
.LFB8617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	(%r12), %rax
	subl	$1, 2152(%rax)
	movq	16(%r12), %rax
	movq	%rax, (%rdi)
	call	*8(%r12)
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8617:
	.size	_ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_, .-_ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1128, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L162
	cmpw	$1040, %cx
	jne	.L113
.L162:
	movq	23(%rdx), %rbx
.L115:
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L187
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L163
	cmpw	$1040, %cx
	jne	.L117
.L163:
	movq	23(%rdx), %rax
	movq	%rax, -1112(%rbp)
.L119:
	testq	%rax, %rax
	je	.L112
	movl	80(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L188
	movl	16(%r12), %edx
	testl	%edx, %edx
	jg	.L122
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L123:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L189
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L125
	movq	(%r12), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, %r15
.L126:
	movq	%r15, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -1124(%rbp)
	testl	%eax, %eax
	je	.L190
	movl	-1124(%rbp), %eax
	leaq	(%rax,%rax,4), %rdx
	leaq	0(,%rdx,8), %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, -1152(%rbp)
	xorl	%ecx, %ecx
	leaq	(%rax,%r13), %rdx
	xorl	%esi, %esi
	movq	$0, -1072(%rbp)
	xorl	%edi, %edi
	movaps	%xmm0, -1104(%rbp)
	movaps	%xmm0, -1088(%rbp)
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r8, (%rax)
	addq	$40, %rax
	movq	%r9, -32(%rax)
	movq	%rsi, -24(%rax)
	movq	%rdi, -16(%rax)
	movq	%rcx, -8(%rax)
	cmpq	%rax, %rdx
	jne	.L128
	leaq	-1104(%rbp), %rax
	movq	%r12, -1160(%rbp)
	xorl	%r13d, %r13d
	movq	-1152(%rbp), %r14
	movq	$0, -1120(%rbp)
	movq	%rax, -1144(%rbp)
.L155:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movl	%r13d, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L191
.L129:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L192
	movq	3280(%rbx), %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L193
.L131:
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L194
.L132:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L195
.L133:
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L196
	movq	3280(%rbx), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	testq	%rax, %rax
	je	.L197
.L135:
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L198
.L136:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L152:
	addl	$1, %r13d
	addq	$40, %r14
	cmpl	%r13d, -1124(%rbp)
	jne	.L154
.L185:
	movq	-1112(%rbp), %rax
	movq	-1152(%rbp), %rsi
	movq	-1160(%rbp), %r12
	movq	64(%rax), %rdi
	call	ares_set_servers_ports@PLT
	testl	%eax, %eax
	jne	.L199
	movq	-1112(%rbp), %rax
	movb	$0, 73(%rax)
	xorl	%eax, %eax
.L157:
	movq	(%r12), %rdx
	movq	-1152(%rbp), %rdi
	movq	%rax, 24(%rdx)
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L200
	addq	$1128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
	movabsq	$-4294967296000, %rcx
	movq	(%r12), %rax
	movq	%rcx, 24(%rax)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L198:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L136
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L201
.L138:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L202
.L139:
	sarq	$32, %rax
	testl	%eax, %eax
	je	.L203
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L204
.L141:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L205
.L142:
	movq	3280(%rbx), %rsi
	sarq	$32, %rax
	movl	$1, %edx
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L206
.L143:
	movq	352(%rbx), %rsi
	movq	-1144(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L207
.L144:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L208
.L145:
	sarq	$32, %rax
	movq	%r14, %r12
	movl	%eax, 28(%r14)
	movl	%eax, 32(%r14)
	movq	-1136(%rbp), %rax
	cmpl	$4, %eax
	jne	.L209
	movq	-1088(%rbp), %rsi
	leaq	12(%r14), %rdx
	movl	$2, %edi
	movl	$2, 8(%r14)
	call	uv_inet_pton@PLT
.L149:
	testl	%eax, %eax
	jne	.L150
	movq	-1120(%rbp), %rax
	movq	$0, (%r14)
	testq	%rax, %rax
	je	.L151
	movq	%r14, (%rax)
.L151:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L152
	testq	%rdi, %rdi
	je	.L152
	call	free@PLT
	addl	$1, %r13d
	addq	$40, %r14
	cmpl	%r13d, -1124(%rbp)
	je	.L185
.L154:
	movq	%r12, -1120(%rbp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L209:
	cmpl	$6, %eax
	jne	.L210
	movq	-1088(%rbp), %rsi
	leaq	12(%r14), %rdx
	movl	$10, %edi
	movl	$10, 8(%r14)
	call	uv_inet_pton@PLT
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L122:
	movq	8(%r12), %rdi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L125:
	movq	8(%r12), %rax
	movq	%rax, %r15
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	32(%r13), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r12), %r13
	movq	%rax, %rbx
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L117:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -1112(%rbp)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L190:
	movq	-1112(%rbp), %rax
	xorl	%esi, %esi
	movq	64(%rax), %rdi
	call	ares_set_servers@PLT
	movq	(%r12), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1136(%rbp), %rdi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L195:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-1136(%rbp), %rax
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1136(%rbp), %rdi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L193:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1136(%rbp), %rax
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L203:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-1136(%rbp), %rax
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1136(%rbp), %rdi
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1136(%rbp), %rdi
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rax, -1168(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-1168(%rbp), %rax
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%rax, -1168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1168(%rbp), %rdi
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%rax, -1168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1168(%rbp), %rdx
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%rax, -1136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-1136(%rbp), %rax
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	movq	-1160(%rbp), %r12
	cmpq	%rax, %rdi
	je	.L156
	testq	%rdi, %rdi
	je	.L156
	call	free@PLT
.L156:
	movabsq	$73014444032, %rax
	jmp	.L157
.L199:
	salq	$32, %rax
	jmp	.L157
.L210:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L200:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7831:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0, @function
_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0:
.LFB12489:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$72, %rsp
	movl	%edi, -84(%rbp)
	movq	16(%rbp), %r12
	movq	%rsi, -96(%rbp)
	movq	%r8, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$8, (%r9)
	movaps	%xmm0, -80(%rbp)
	jne	.L212
	movq	(%r12), %rax
	movq	%rax, -80(%rbp)
.L212:
	cmpb	$8, 1(%rbx)
	jne	.L213
	movq	8(%r12), %rax
	movq	%rax, -72(%rbp)
.L213:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L218
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	xorl	%r15d, %r15d
	movq	24(%rax), %r10
	cmpq	%rcx, %r10
	jne	.L227
.L214:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L215
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L215:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L211
	movq	(%rdi), %rdx
	call	*8(%rdx)
.L211:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L228
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	xorl	%r15d, %r15d
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L227:
	subq	$8, %rsp
	leaq	-80(%rbp), %rax
	movsbl	-84(%rbp), %esi
	movq	-96(%rbp), %rdx
	pushq	$6
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	pushq	%rax
	pushq	%r12
	pushq	%rbx
	pushq	-104(%rbp)
	pushq	$2
	pushq	$0
	call	*%r10
	movq	%rax, %r15
	addq	$64, %rsp
	jmp	.L214
.L228:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12489:
	.size	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0, .-_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	.section	.text._ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv:
.LFB12473:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE12473:
	.size	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv:
.LFB12474:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE12474:
	.size	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_113ParseTxtReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, @function
_ZN4node10cares_wrap12_GLOBAL__N_113ParseTxtReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb:
.LFB7708:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rcx, -120(%rbp)
	movq	352(%rdi), %rsi
	movl	%r8d, -144(%rbp)
	movb	%r8b, -109(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -136(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	3280(%rbx), %r14
	call	ares_parse_txt_reply_ext@PLT
	movl	%eax, -128(%rbp)
	testl	%eax, %eax
	je	.L275
.L232:
	movq	-136(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L276
	movl	-128(%rbp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	movq	-120(%rbp), %rdi
	movq	-88(%rbp), %r15
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	call	_ZNK2v85Array6LengthEv@PLT
	movl	$0, -108(%rbp)
	movl	%eax, -124(%rbp)
	testq	%r15, %r15
	jne	.L233
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L279:
	movl	%r13d, %edx
	addl	$1, %r13d
.L237:
	movq	%r8, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L277
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L245
.L233:
	movq	8(%r15), %rsi
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	16(%r15), %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L278
.L235:
	cmpb	$0, 24(%r15)
	je	.L279
	testq	%r12, %r12
	je	.L238
	movl	-108(%rbp), %eax
	movl	-124(%rbp), %r9d
	addl	%eax, %r9d
	addl	$1, %eax
	cmpb	$0, -109(%rbp)
	movl	%eax, -108(%rbp)
	je	.L239
	movq	352(%rbx), %rdi
	movl	%r9d, -140(%rbp)
	movq	%r8, -104(%rbp)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%rax, %r13
	movq	360(%rbx), %rax
	movq	%r13, %rdi
	movq	608(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-104(%rbp), %r8
	movl	-140(%rbp), %r9d
	testb	%al, %al
	je	.L280
.L240:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%r9d, -140(%rbp)
	movq	%r8, -104(%rbp)
	movq	552(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-104(%rbp), %r8
	movl	-140(%rbp), %r9d
	testb	%al, %al
	je	.L281
.L241:
	movq	%r8, -104(%rbp)
	movq	%r13, %rcx
.L272:
	movq	-120(%rbp), %rdi
	movl	%r9d, %edx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	movq	-104(%rbp), %r8
	testb	%al, %al
	je	.L282
.L238:
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r8, -104(%rbp)
	movl	$1, %r13d
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	-104(%rbp), %r8
	xorl	%edx, %edx
	movq	%rax, %r12
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%r8, -104(%rbp)
	movq	%r12, %rcx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L277:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L233
	.p2align 4,,10
	.p2align 3
.L245:
	testq	%r12, %r12
	je	.L234
	movl	-108(%rbp), %r8d
	addl	-124(%rbp), %r8d
	movq	%r12, %rcx
	cmpb	$0, -144(%rbp)
	je	.L274
	movq	352(%rbx), %rdi
	movl	%r8d, -104(%rbp)
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%rax, %r13
	movq	360(%rbx), %rax
	movq	%r13, %rdi
	movq	608(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movl	-104(%rbp), %r8d
	testb	%al, %al
	je	.L283
.L249:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%r8d, -104(%rbp)
	movq	552(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movl	-104(%rbp), %r8d
	testb	%al, %al
	je	.L284
.L250:
	movq	%r13, %rcx
.L274:
	movq	-120(%rbp), %rdi
	movl	%r8d, %edx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L234
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	movq	-88(%rbp), %rdi
	call	ares_free_data@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %r8
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L282:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-104(%rbp), %r8
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L281:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-140(%rbp), %r9d
	movq	-104(%rbp), %r8
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L280:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-140(%rbp), %r9d
	movq	-104(%rbp), %r8
	jmp	.L240
.L284:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-104(%rbp), %r8d
	jmp	.L250
.L283:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-104(%rbp), %r8d
	jmp	.L249
.L276:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7708:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_113ParseTxtReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, .-_ZN4node10cares_wrap12_GLOBAL__N_113ParseTxtReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	.section	.text._ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv:
.LFB12477:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L287
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE12477:
	.size	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.section	.text._ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE6CancelEv:
.LFB12478:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L290
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE12478:
	.size	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task14MemoryInfoNameEv:
.LFB7594:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 30(%rdi)
	movq	%rdi, %rax
	movabsq	$7310012207526604654, %rcx
	movq	%rdx, (%rdi)
	movl	$27507, %edx
	movq	%rcx, 16(%rdi)
	movl	$1635016563, 24(%rdi)
	movw	%dx, 28(%rdi)
	movq	$14, 8(%rdi)
	ret
	.cfi_endproc
.LFE7594:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD2Ev:
.LFB7669:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111ChannelWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	call	ares_destroy@PLT
	cmpb	$0, 74(%r12)
	jne	.L306
.L293:
	movq	56(%r12), %r13
	testq	%r13, %r13
	je	.L294
	movq	16(%r12), %rbx
	movl	$24, %edi
	addl	$1, 2152(%rbx)
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	leaq	_ZZN4node11Environment11CloseHandleI10uv_timer_sZNS_10cares_wrap12_GLOBAL__N_111ChannelWrap10CloseTimerEvEUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESC_(%rip), %rsi
	movq	%r13, %rdi
	movq	%rbx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 0(%r13)
	call	uv_close@PLT
	movq	$0, 56(%r12)
.L294:
	movq	104(%r12), %rbx
	testq	%rbx, %rbx
	je	.L295
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L296
.L295:
	movq	96(%r12), %rax
	movq	88(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	88(%r12), %rdi
	leaq	136(%r12), %rax
	movq	$0, 112(%r12)
	movq	$0, 104(%r12)
	cmpq	%rax, %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	call	ares_library_cleanup@PLT
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L293
	.cfi_endproc
.LFE7669:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD0Ev:
.LFB7671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$144, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7671:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD0Ev
	.p2align 4
	.type	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD1Ev, @function
_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD1Ev:
.LFB12475:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L314
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L314:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE12475:
	.size	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD1Ev, .-_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD1Ev
	.p2align 4
	.type	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev, @function
_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev:
.LFB12482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L318
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$256, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE12482:
	.size	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev, .-_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev
	.p2align 4
	.type	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD1Ev, @function
_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD1Ev:
.LFB12476:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L324
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L324:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE12476:
	.size	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD1Ev, .-_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD1Ev
	.p2align 4
	.type	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev, @function
_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev:
.LFB12481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L328
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1408, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE12481:
	.size	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev, .-_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD2Ev:
.LFB9414:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L334
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L334:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9414:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD2Ev:
.LFB9425:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L340
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L340:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9425:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev:
.LFB9416:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L344
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$256, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9416:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev:
.LFB9427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L348
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1408, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9427:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviii, @function
_ZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviii:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsi, %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%ecx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	xorl	%edx, %edx
	pushq	%r12
	movl	%r13d, %r11d
	pushq	%rbx
	orl	%r14d, %r11d
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	96(%rdi), %rcx
	movq	88(%rdi), %r10
	divq	%rcx
	movq	(%r10,%rdx,8), %rax
	testq	%rax, %rax
	je	.L350
	movq	(%rax), %rdi
	movq	%rdx, %r9
	movq	16(%rdi), %r8
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L351:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L350
	movq	16(%rdi), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L350
.L353:
	cmpq	%r8, %rsi
	jne	.L351
	movq	8(%rdi), %r12
	cmpl	16(%r12), %r15d
	jne	.L351
	testl	%r11d, %r11d
	je	.L435
	addq	$24, %r12
.L356:
	xorl	%esi, %esi
	testl	%r13d, %r13d
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_112ares_poll_cbEP9uv_poll_sii(%rip), %rdx
	movq	%r12, %rdi
	setne	%sil
	movl	%esi, %eax
	orl	$2, %eax
	testl	%r14d, %r14d
	cmovne	%eax, %esi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_poll_start@PLT
	.p2align 4,,10
	.p2align 3
.L435:
	.cfi_restore_state
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rcx
	leaq	(%r10,%rdx,8), %rsi
	movq	%rdx, %r11
	movq	(%rsi), %rdx
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%rax, %r8
	movq	(%rax), %rax
	cmpq	%rax, %rdi
	jne	.L378
	movq	(%rdi), %r9
	cmpq	%r8, %rdx
	je	.L436
	testq	%r9, %r9
	je	.L381
	movq	16(%r9), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L381
	movq	%r8, (%r10,%rdx,8)
	movq	(%rdi), %r9
.L381:
	movq	%r9, (%r8)
	call	_ZdlPv@PLT
	movq	16(%rbx), %r13
	subq	$1, 112(%rbx)
	movl	$24, %edi
	addl	$1, 2152(%r13)
	call	_Znwm@PLT
	movq	24(%r12), %rdx
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_poll_close_cbEP9uv_poll_s(%rip), %rcx
	leaq	24(%r12), %rdi
	movq	%r13, (%rax)
	leaq	_ZZN4node11Environment11CloseHandleI9uv_poll_sPFvPS2_EEEvPT_T0_ENUlP11uv_handle_sE_4_FUNESA_(%rip), %rsi
	movq	%rcx, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%r12)
	call	uv_close@PLT
	cmpq	$0, 112(%rbx)
	jne	.L349
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L349
	movq	16(%rbx), %r13
	movl	$24, %edi
	addl	$1, 2152(%r13)
	call	_Znwm@PLT
	movq	(%r12), %rdx
	leaq	_ZZN4node11Environment11CloseHandleI10uv_timer_sZNS_10cares_wrap12_GLOBAL__N_111ChannelWrap10CloseTimerEvEUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESC_(%rip), %rsi
	movq	%r12, %rdi
	movq	%r13, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	call	uv_close@PLT
	movq	$0, 56(%rbx)
.L349:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	testl	%r11d, %r11d
	jne	.L354
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviiiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L436:
	testq	%r9, %r9
	je	.L392
	movq	16(%r9), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L381
	movq	%r8, (%r10,%rdx,8)
	movq	(%rsi), %rax
.L380:
	leaq	104(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L437
.L382:
	movq	$0, (%rsi)
	movq	(%rdi), %r9
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L354:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L438
	movq	%r12, %rdi
	call	uv_is_active@PLT
	testl	%eax, %eax
	jne	.L359
.L358:
	movl	76(%rbx), %eax
	movl	$1, %edx
	testl	%eax, %eax
	jne	.L439
.L360:
	movq	56(%rbx), %rdi
	movq	%rdx, %rcx
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_s(%rip), %rsi
	call	uv_timer_start@PLT
.L359:
	movl	$184, %edi
	call	_Znwm@PLT
	movl	$23, %ecx
	movl	%r15d, %edx
	movq	%rax, %r8
	xorl	%eax, %eax
	movq	%r8, %rdi
	leaq	24(%r8), %r12
	movq	%r8, -56(%rbp)
	rep stosq
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_114node_ares_taskE(%rip), %rax
	movq	%rbx, 8(%r8)
	movq	%r12, %rsi
	movq	%rax, (%r8)
	movq	16(%rbx), %rax
	movl	%r15d, 16(%r8)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_poll_init_socket@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	js	.L440
	movslq	16(%r8), %r15
	movq	96(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r15, %rax
	movq	%r15, %r10
	divq	%rdi
	movq	88(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r11
	testq	%rax, %rax
	je	.L362
	movq	(%rax), %rcx
	movq	16(%rcx), %rsi
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L361:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L362
	movq	16(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L362
.L363:
	cmpq	%rsi, %r15
	jne	.L361
	movq	8(%rcx), %rax
	cmpl	16(%rax), %r10d
	jne	.L361
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L439:
	cmpl	$1000, %eax
	jbe	.L441
	movl	$1000, %edx
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L392:
	movq	%r8, %rax
	jmp	.L380
	.p2align 4,,10
	.p2align 3
.L362:
	movl	$24, %edi
	movq	%r8, -56(%rbp)
	movq	%r11, -64(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r8
	movq	112(%rbx), %rdx
	leaq	120(%rbx), %rdi
	movq	$0, (%rax)
	movq	96(%rbx), %rsi
	movl	$1, %ecx
	movq	%r8, 8(%rax)
	movq	%rax, -56(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-56(%rbp), %r9
	testb	%al, %al
	movq	%rdx, %r8
	jne	.L364
	movq	88(%rbx), %r10
	movq	-64(%rbp), %r11
.L365:
	addq	%r10, %r11
	movq	%r15, 16(%r9)
	movq	(%r11), %rax
	testq	%rax, %rax
	je	.L374
	movq	(%rax), %rax
	movq	%rax, (%r9)
	movq	(%r11), %rax
	movq	%r9, (%rax)
.L375:
	addq	$1, 112(%rbx)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L440:
	addq	$40, %rsp
	movl	$184, %esi
	movq	%r8, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L438:
	.cfi_restore_state
	movl	$152, %edi
	call	_Znwm@PLT
	movl	$19, %ecx
	movq	%rax, %rsi
	movq	%rax, %rdi
	movq	%r12, %rax
	rep stosq
	movq	16(%rbx), %rax
	movq	%rbx, (%rsi)
	movq	%rsi, 56(%rbx)
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_timer_init@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L437:
	movq	%r9, 104(%rbx)
	jmp	.L382
.L364:
	cmpq	$1, %rdx
	je	.L442
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L443
	leaq	0(,%rdx,8), %rdx
	movq	%r9, -80(%rbp)
	movq	%rdx, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %r8
	movq	%rax, %r10
	leaq	136(%rbx), %rax
	movq	%rax, -64(%rbp)
.L367:
	movq	104(%rbx), %rsi
	movq	$0, 104(%rbx)
	testq	%rsi, %rsi
	je	.L369
	leaq	104(%rbx), %rax
	xorl	%edi, %edi
	movq	%rax, -56(%rbp)
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L371:
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	(%r11), %rax
	movq	%rcx, (%rax)
.L372:
	testq	%rsi, %rsi
	je	.L369
.L370:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	16(%rcx), %rax
	divq	%r8
	leaq	(%r10,%rdx,8), %r11
	movq	(%r11), %rax
	testq	%rax, %rax
	jne	.L371
	movq	104(%rbx), %rax
	movq	%rax, (%rcx)
	movq	-56(%rbp), %rax
	movq	%rcx, 104(%rbx)
	movq	%rax, (%r11)
	cmpq	$0, (%rcx)
	je	.L391
	movq	%rcx, (%r10,%rdi,8)
	movq	%rdx, %rdi
	jmp	.L372
.L369:
	movq	88(%rbx), %rdi
	cmpq	-64(%rbp), %rdi
	je	.L373
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r10
	movq	-56(%rbp), %r9
.L373:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r8, 96(%rbx)
	divq	%r8
	movq	%r10, 88(%rbx)
	leaq	0(,%rdx,8), %r11
	jmp	.L365
.L374:
	movq	104(%rbx), %rax
	movq	%r9, 104(%rbx)
	movq	%rax, (%r9)
	testq	%rax, %rax
	je	.L376
	movq	16(%rax), %rax
	xorl	%edx, %edx
	divq	96(%rbx)
	movq	%r9, (%r10,%rdx,8)
.L376:
	leaq	104(%rbx), %rax
	movq	%rax, (%r11)
	jmp	.L375
.L391:
	movq	%rdx, %rdi
	jmp	.L372
.L442:
	leaq	136(%rbx), %rax
	movq	$0, 136(%rbx)
	movq	%rax, -64(%rbp)
	movq	%rax, %r10
	jmp	.L367
.L441:
	movslq	%eax, %rdx
	jmp	.L360
.L443:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7655:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviii, .-_ZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviii
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114HostentToNamesEPNS_11EnvironmentEP7hostentN2v85LocalINS6_5ArrayEEE.isra.0, @function
_ZN4node10cares_wrap12_GLOBAL__N_114HostentToNamesEPNS_11EnvironmentEP7hostentN2v85LocalINS6_5ArrayEEE.isra.0:
.LFB12326:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$88, %rsp
	movq	%rdx, -112(%rbp)
	movq	352(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r12), %r15
	testq	%rbx, %rbx
	je	.L472
	movq	-112(%rbp), %r13
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -100(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L456
.L454:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L451:
	movq	352(%r12), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L473
.L447:
	movl	-100(%rbp), %eax
	movq	%r15, %rsi
	movq	%r13, %rdi
	leal	(%rbx,%rax), %edx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L474
.L448:
	movq	(%r14), %rax
	leal	1(%rbx), %edx
	movq	%rdx, %rbx
	movq	(%rax,%rdx,8), %rsi
	testq	%rsi, %rsi
	jne	.L451
	cmpq	$0, -112(%rbp)
	je	.L446
.L452:
	movq	-120(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L475
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L474:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L447
.L456:
	movq	-112(%rbp), %r13
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L472:
	movq	352(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -100(%rbp)
	movq	(%r14), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L454
	.p2align 4,,10
	.p2align 3
.L446:
	movq	-120(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r13
	jmp	.L452
.L475:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12326:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114HostentToNamesEPNS_11EnvironmentEP7hostentN2v85LocalINS6_5ArrayEEE.isra.0, .-_ZN4node10cares_wrap12_GLOBAL__N_114HostentToNamesEPNS_11EnvironmentEP7hostentN2v85LocalINS6_5ArrayEEE.isra.0
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112ParseMxReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, @function
_ZN4node10cares_wrap12_GLOBAL__N_112ParseMxReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb:
.LFB7707:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rcx, -104(%rbp)
	movq	352(%rdi), %rsi
	movb	%r8b, -105(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	3280(%rbx), %r14
	call	ares_parse_mx_reply@PLT
	movl	%eax, -112(%rbp)
	testl	%eax, %eax
	je	.L502
.L477:
	movq	-120(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L503
	movl	-112(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	-88(%rbp), %r13
	movl	%eax, %r15d
	testq	%r13, %r13
	jne	.L487
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L482:
	movq	-104(%rbp), %rdi
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L504
	movq	0(%r13), %r13
	addl	$1, %r15d
	testq	%r13, %r13
	je	.L485
.L487:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	8(%r13), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%rbx), %rdi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L505
.L479:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	656(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L506
.L480:
	movzwl	16(%r13), %esi
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1408(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L507
.L481:
	cmpb	$0, -105(%rbp)
	je	.L482
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	504(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L482
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L504:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	0(%r13), %r13
	addl	$1, %r15d
	testq	%r13, %r13
	jne	.L487
	.p2align 4,,10
	.p2align 3
.L485:
	movq	-88(%rbp), %r13
.L478:
	movq	%r13, %rdi
	call	ares_free_data@PLT
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L506:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L480
	.p2align 4,,10
	.p2align 3
.L505:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L479
	.p2align 4,,10
	.p2align 3
.L507:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L481
.L503:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7707:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112ParseMxReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, .-_ZN4node10cares_wrap12_GLOBAL__N_112ParseMxReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_113ParseSrvReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, @function
_ZN4node10cares_wrap12_GLOBAL__N_113ParseSrvReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb:
.LFB7709:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rcx, -104(%rbp)
	movq	352(%rdi), %rsi
	movb	%r8b, -105(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	3280(%rbx), %r14
	call	ares_parse_srv_reply@PLT
	movl	%eax, -112(%rbp)
	testl	%eax, %eax
	je	.L535
.L509:
	movq	-120(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L536
	movl	-112(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	-88(%rbp), %r13
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %r15d
	testq	%r13, %r13
	jne	.L520
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L516:
	movq	-104(%rbp), %rdi
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L537
	movq	0(%r13), %r13
	addl	$1, %r15d
	testq	%r13, %r13
	je	.L510
.L520:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	8(%r13), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%rbx), %rdi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L538
.L511:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1056(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L539
.L512:
	movzwl	20(%r13), %esi
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1384(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L540
.L513:
	movzwl	16(%r13), %esi
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1408(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L541
.L514:
	movzwl	18(%r13), %esi
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1904(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L542
.L515:
	cmpb	$0, -105(%rbp)
	je	.L516
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	544(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L516
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L537:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	0(%r13), %r13
	addl	$1, %r15d
	testq	%r13, %r13
	jne	.L520
	.p2align 4,,10
	.p2align 3
.L510:
	movq	-88(%rbp), %rdi
	call	ares_free_data@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L538:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L542:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L541:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L540:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L539:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L512
.L536:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7709:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_113ParseSrvReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, .-_ZN4node10cares_wrap12_GLOBAL__N_113ParseSrvReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap14MemoryInfoNameEv:
.LFB7720:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8750003115893552465, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1885434455, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE7720:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap14MemoryInfoNameEv:
.LFB7778:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8534976024895911249, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1885434455, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE7778:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap14MemoryInfoNameEv:
.LFB7634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L548
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L548:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7634:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap14MemoryInfoNameEv:
.LFB7786:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8247305300162737489, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1885434455, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE7786:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap14MemoryInfoNameEv:
.LFB7794:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 30(%rdi)
	movq	%rdi, %rax
	movabsq	$8097839888506123601, %rcx
	movq	%rdx, (%rdi)
	movl	$28769, %edx
	movq	%rcx, 16(%rdi)
	movl	$1918333556, 24(%rdi)
	movw	%dx, 28(%rdi)
	movq	$14, 8(%rdi)
	ret
	.cfi_endproc
.LFE7794:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap14MemoryInfoNameEv:
.LFB7762:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6301466587138717009, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7762:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap14MemoryInfoNameEv:
.LFB7802:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$7020922125169292625, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1885434455, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE7802:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap14MemoryInfoNameEv:
.LFB7754:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6302872862510642513, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7754:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap14MemoryInfoNameEv:
.LFB7810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$17, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC2(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movb	$112, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L557
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L557:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7810:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap14MemoryInfoNameEv:
.LFB7746:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 30(%rdi)
	movq	%rdi, %rax
	movabsq	$7020623058006537553, %rcx
	movq	%rdx, (%rdi)
	movl	$28769, %edx
	movq	%rcx, 16(%rdi)
	movl	$1918330221, 24(%rdi)
	movw	%dx, 28(%rdi)
	movq	$14, 8(%rdi)
	ret
	.cfi_endproc
.LFE7746:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap14MemoryInfoNameEv:
.LFB7641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L562
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L562:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7641:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap14MemoryInfoNameEv:
.LFB7730:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$10, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8239126033163711825, %rcx
	movq	%rdx, (%rdi)
	movl	$28769, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE7730:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap14MemoryInfoNameEv:
.LFB7606:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$6299521503757166659, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE7606:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap14MemoryInfoNameEv:
.LFB7738:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1634883425, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7016961684286043473, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$112, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE7738:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap14MemoryInfoNameEv
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap14MemoryInfoNameEv, @function
_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap14MemoryInfoNameEv:
.LFB7770:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8392550786191947089, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1885434455, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE7770:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap14MemoryInfoNameEv, .-_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap14MemoryInfoNameEv
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_115ParseNaptrReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, @function
_ZN4node10cares_wrap12_GLOBAL__N_115ParseNaptrReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb:
.LFB7710:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	%rcx, -104(%rbp)
	movq	352(%rdi), %rsi
	movb	%r8b, -105(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -120(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-88(%rbp), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	movq	3280(%rbx), %r14
	call	ares_parse_naptr_reply@PLT
	movl	%eax, -112(%rbp)
	testl	%eax, %eax
	je	.L599
.L568:
	movq	-120(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L600
	movl	-112(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	movq	%r15, %rdi
	movq	-88(%rbp), %r13
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %r15d
	testq	%r13, %r13
	jne	.L584
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L580:
	movq	-104(%rbp), %rdi
	movq	%r12, %rcx
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L601
	movq	0(%r13), %r13
	addl	$1, %r15d
	testq	%r13, %r13
	je	.L569
.L584:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	8(%r13), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%rbx), %rdi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L602
.L570:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L603
.L571:
	movq	16(%r13), %rsi
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L604
.L572:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1576(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L605
.L573:
	movq	24(%r13), %rsi
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L606
.L574:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1496(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L607
.L575:
	movq	32(%r13), %rsi
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L608
.L576:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	1512(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L609
.L577:
	movzwl	40(%r13), %esi
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1288(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L610
.L578:
	movzwl	42(%r13), %esi
	movq	352(%rbx), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1392(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L611
.L579:
	cmpb	$0, -105(%rbp)
	je	.L580
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	512(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L580
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L601:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	0(%r13), %r13
	addl	$1, %r15d
	testq	%r13, %r13
	jne	.L584
	.p2align 4,,10
	.p2align 3
.L569:
	movq	-88(%rbp), %rdi
	call	ares_free_data@PLT
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L610:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L609:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L608:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L607:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L611:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L602:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L606:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L605:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L604:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L603:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L571
.L600:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7710:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_115ParseNaptrReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb, .-_ZN4node10cares_wrap12_GLOBAL__N_115ParseNaptrReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"node,node.dns,node.dns.native"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_:
.LFB7700:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r15, -72(%rbp)
	movq	%rax, -80(%rbp)
	movq	%rbx, -64(%rbp)
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_E28trace_event_unique_atomic719(%rip), %rdx
	testq	%rdx, %rdx
	je	.L650
.L614:
	testb	$5, (%rdx)
	jne	.L651
.L616:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	movq	360(%rax), %rdx
	movq	1136(%rdx), %r15
	testq	%rdi, %rdi
	je	.L620
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L652
.L620:
	movq	3280(%rax), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L623
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L653
.L623:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L654
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L651:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movq	%rdx, -136(%rbp)
	movq	72(%r12), %r15
	movaps	%xmm0, -96(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L617
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-136(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L655
.L617:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L618
	movq	(%rdi), %rax
	call	*8(%rax)
.L618:
	movq	-96(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L616
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L650:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L615
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L656
.L615:
	movq	%rdx, _ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_E28trace_event_unique_atomic719(%rip)
	mfence
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L652:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%r12), %rax
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L653:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	movl	$3, %edx
	movq	%r15, %rsi
	sete	%al
	leaq	-80(%rbp), %rcx
	movq	%r12, %rdi
	subl	%eax, %edx
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L656:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L655:
	subq	$8, %rsp
	leaq	-96(%rbp), %rcx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	pushq	$6
	movl	$101, %esi
	pushq	%rcx
	movq	%r15, %rcx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L617
.L654:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7700:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap5ParseEP7hostent, @function
_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap5ParseEP7hostent:
.LFB7812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rdi
	xorl	%edx, %edx
	leaq	8(%rbx), %rsi
	call	_ZN4node10cares_wrap12_GLOBAL__N_114HostentToNamesEPNS_11EnvironmentEP7hostentN2v85LocalINS6_5ArrayEEE.isra.0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L660
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L660:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7812:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap5ParseEP7hostent, .-_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap5ParseEP7hostent
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_18StrErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_18StrErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7833:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L672
	cmpw	$1040, %cx
	jne	.L662
.L672:
	movq	23(%rdx), %r13
.L664:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L665
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L666:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L674
.L667:
	sarq	$32, %rbx
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L19EMSG_ESETSRVPENDINGE(%rip), %rsi
	cmpl	$-1000, %ebx
	je	.L668
	movl	%ebx, %edi
	call	ares_strerror@PLT
	movq	%rax, %rsi
.L668:
	movq	352(%r13), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%r12), %rbx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L675
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L665:
	.cfi_restore_state
	movq	8(%r12), %rdi
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L662:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L675:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L674:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L667
	.cfi_endproc
.LFE7833:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_18StrErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_18StrErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC5:
	.string	"UNKNOWN_ARES_ERROR"
.LC6:
	.string	"EADDRGETNETWORKPARAMS"
.LC7:
	.string	"EBADFLAGS"
.LC8:
	.string	"EBADHINTS"
.LC9:
	.string	"EBADNAME"
.LC10:
	.string	"EBADQUERY"
.LC11:
	.string	"EBADRESP"
.LC12:
	.string	"EBADSTR"
.LC13:
	.string	"ECANCELLED"
.LC14:
	.string	"ECONNREFUSED"
.LC15:
	.string	"EDESTRUCTION"
.LC16:
	.string	"EFILE"
.LC17:
	.string	"EFORMERR"
.LC18:
	.string	"ELOADIPHLPAPI"
.LC19:
	.string	"ENODATA"
.LC20:
	.string	"ENOMEM"
.LC21:
	.string	"ENONAME"
.LC22:
	.string	"ENOTFOUND"
.LC23:
	.string	"ENOTIMP"
.LC24:
	.string	"ENOTINITIALIZED"
.LC25:
	.string	"EOF"
.LC26:
	.string	"EREFUSED"
.LC27:
	.string	"ESERVFAIL"
.LC28:
	.string	"ETIMEOUT"
.LC29:
	.string	"EBADFAMILY"
.LC30:
	.string	"error"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi:
.LFB7702:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	je	.L743
	movq	16(%rdi), %rax
	movl	%esi, %ebx
	leaq	-112(%rbp), %r14
	movq	%rdi, %r12
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpl	$24, %ebx
	ja	.L678
	leaq	.L680(%rip), %rcx
	movl	%ebx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L680:
	.long	.L678-.L680
	.long	.L703-.L680
	.long	.L702-.L680
	.long	.L701-.L680
	.long	.L700-.L680
	.long	.L699-.L680
	.long	.L698-.L680
	.long	.L697-.L680
	.long	.L696-.L680
	.long	.L695-.L680
	.long	.L694-.L680
	.long	.L693-.L680
	.long	.L692-.L680
	.long	.L691-.L680
	.long	.L690-.L680
	.long	.L689-.L680
	.long	.L688-.L680
	.long	.L687-.L680
	.long	.L686-.L680
	.long	.L685-.L680
	.long	.L684-.L680
	.long	.L683-.L680
	.long	.L682-.L680
	.long	.L718-.L680
	.long	.L679-.L680
	.text
	.p2align 4,,10
	.p2align 3
.L743:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L718:
	leaq	.LC6(%rip), %rsi
	.p2align 4,,10
	.p2align 3
.L681:
	movq	16(%r12), %rax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L744
.L704:
	movq	%rax, -136(%rbp)
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE28trace_event_unique_atomic731(%rip), %rdx
	testq	%rdx, %rdx
	je	.L745
.L706:
	testb	$5, (%rdx)
	jne	.L746
.L708:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	movq	360(%rax), %rdx
	movq	1136(%rdx), %r15
	testq	%rdi, %rdi
	je	.L712
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L747
.L712:
	movq	3280(%rax), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L716
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L748
.L716:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L749
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L695:
	.cfi_restore_state
	leaq	.LC29(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L690:
	leaq	.LC16(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L682:
	leaq	.LC18(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L696:
	leaq	.LC9(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L697:
	leaq	.LC10(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L698:
	leaq	.LC26(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L691:
	leaq	.LC25(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L692:
	leaq	.LC28(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L693:
	leaq	.LC14(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L686:
	leaq	.LC7(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L687:
	leaq	.LC12(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	.LC15(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L689:
	leaq	.LC20(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L684:
	leaq	.LC8(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L685:
	leaq	.LC21(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L683:
	leaq	.LC24(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	.LC11(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L699:
	leaq	.LC23(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L700:
	leaq	.LC22(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L701:
	leaq	.LC27(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L702:
	leaq	.LC17(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	.LC19(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	.LC13(%rip), %rsi
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	.LC30(%rip), %rax
	pxor	%xmm0, %xmm0
	movslq	%ebx, %rbx
	movq	%rdx, -152(%rbp)
	movq	%rax, -128(%rbp)
	movq	72(%r12), %r15
	movb	$3, -137(%rbp)
	movq	%rbx, -120(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L709
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rcx
	movq	-152(%rbp), %rdx
	movq	24(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L750
.L709:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L710
	movq	(%rdi), %rax
	call	*8(%rax)
.L710:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L708
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L745:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rdx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L707
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rcx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L751
.L707:
	movq	%rdx, _ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE28trace_event_unique_atomic731(%rip)
	mfence
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L747:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%r12), %rax
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L748:
	leaq	-136(%rbp), %rcx
	movl	$1, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L744:
	movq	%rax, -152(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-152(%rbp), %rax
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L751:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rdx
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L750:
	subq	$8, %rsp
	leaq	-80(%rbp), %rcx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	pushq	$6
	movl	$101, %esi
	pushq	%rcx
	leaq	-120(%rbp), %rcx
	pushq	%rcx
	leaq	-137(%rbp), %rcx
	pushq	%rcx
	leaq	-128(%rbp), %rcx
	pushq	%rcx
	movq	%r15, %rcx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L709
.L678:
	leaq	.LC5(%rip), %rsi
	jmp	.L681
.L749:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7702:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_E4CallES2_:
.LFB12133:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	64(%r12), %rax
	testq	%rax, %rax
	je	.L767
	movl	(%rax), %esi
	movq	%rdi, %rbx
	testl	%esi, %esi
	jne	.L768
	cmpb	$0, 4(%rax)
	movq	(%r12), %rcx
	je	.L769
	movq	104(%rcx), %rdx
	movq	8(%rax), %r13
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap5ParseEP7hostent(%rip), %rax
	cmpq	%rax, %rdx
	jne	.L757
	movq	16(%r12), %rax
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rdi
	leaq	8(%r13), %rsi
	xorl	%edx, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_114HostentToNamesEPNS_11EnvironmentEP7hostentN2v85LocalINS6_5ArrayEEE.isra.0
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L755:
	movq	24(%rbx), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L770
.L758:
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L760
	movb	$1, 9(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L771
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L769:
	.cfi_restore_state
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	movq	%r12, %rdi
	call	*96(%rcx)
	movq	24(%rbx), %rbx
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L758
.L770:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L763
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L759:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
.L760:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L768:
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L767:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap13AfterResponseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L757:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L763:
	xorl	%edx, %edx
	jmp	.L759
.L771:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12133:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_E4CallES2_
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap5ParseEPhi:
.LFB7804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	leaq	-88(%rbp), %rdx
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	ares_parse_soa_reply@PLT
	testl	%eax, %eax
	jne	.L786
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	movq	-88(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L787
.L775:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	1096(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L788
.L776:
	movq	-88(%rbp), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	8(%rax), %rsi
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L789
.L777:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	856(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L790
.L778:
	movq	-88(%rbp), %rax
	movl	16(%rax), %esi
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	1560(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L791
.L779:
	movq	-88(%rbp), %rax
	movl	20(%rax), %esi
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	1488(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L792
.L780:
	movq	-88(%rbp), %rax
	movl	24(%rax), %esi
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	1528(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L793
.L781:
	movq	-88(%rbp), %rax
	movl	28(%rax), %esi
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	672(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L794
.L782:
	movq	-88(%rbp), %rax
	movl	32(%rax), %esi
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	1032(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L795
.L783:
	movq	-88(%rbp), %rdi
	call	ares_free_data@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L772:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L796
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L786:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L795:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L783
	.p2align 4,,10
	.p2align 3
.L787:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rcx
	jmp	.L775
	.p2align 4,,10
	.p2align 3
.L788:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L789:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rcx
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L790:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L778
	.p2align 4,,10
	.p2align 3
.L791:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L792:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L793:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L794:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L782
.L796:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7804:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap5ParseEPhi:
.LFB7796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	16(%r12), %rdi
	movl	-84(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN4node10cares_wrap12_GLOBAL__N_115ParseNaptrReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	testl	%eax, %eax
	jne	.L802
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L797:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L803
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L802:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L797
.L803:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7796:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap5ParseEPhi:
.LFB7780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	16(%r12), %rdi
	movl	-84(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN4node10cares_wrap12_GLOBAL__N_113ParseSrvReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	testl	%eax, %eax
	jne	.L809
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L804:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L810
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L809:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L804
.L810:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7780:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap5ParseEPhi:
.LFB7772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	16(%r12), %rdi
	movl	-84(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN4node10cares_wrap12_GLOBAL__N_113ParseTxtReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	testl	%eax, %eax
	jne	.L816
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L811:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L817
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L816:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L811
.L817:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7772:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap5ParseEPhi:
.LFB7756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%edx, -84(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	16(%r12), %rdi
	movl	-84(%rbp), %edx
	xorl	%r8d, %r8d
	movq	%rax, %rcx
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN4node10cares_wrap12_GLOBAL__N_112ParseMxReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	testl	%eax, %eax
	jne	.L823
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L818:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L824
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L823:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L818
.L824:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7756:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap5ParseEPhi
	.section	.rodata.str1.1
.LC31:
	.string	"cancel"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L859
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L841
	cmpw	$1040, %cx
	jne	.L827
.L841:
	movq	23(%rdx), %rbx
.L829:
	testq	%rbx, %rbx
	je	.L825
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2161(%rip), %r12
	testq	%r12, %r12
	je	.L860
	testb	$5, (%r12)
	jne	.L861
.L834:
	movq	64(%rbx), %rdi
	call	ares_cancel@PLT
.L825:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L862
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L861:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L835
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L863
.L835:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L836
	movq	(%rdi), %rax
	call	*8(%rax)
.L836:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L834
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L860:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L833
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L864
.L833:
	movq	%r12, _ZZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2161(%rip)
	mfence
	testb	$5, (%r12)
	je	.L834
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L827:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L859:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L864:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L863:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$16
	leaq	.LC31(%rip), %rcx
	movl	$73, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L835
.L862:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7832:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_, @function
_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_:
.LFB7706:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %r8
	movl	%edx, -192(%rbp)
	movq	%r9, -176(%rbp)
	movq	352(%rdi), %rsi
	movq	%r8, -168(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-144(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	(%rbx), %eax
	movq	3280(%r12), %r14
	addl	$1, %eax
	cmpl	$29, %eax
	ja	.L866
	leaq	.L868(%rip), %rdx
	movq	-168(%rbp), %r8
	movq	-176(%rbp), %rcx
	movslq	(%rdx,%rax,4), %rax
	movl	-192(%rbp), %r10d
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L868:
	.long	.L870-.L868
	.long	.L866-.L868
	.long	.L870-.L868
	.long	.L871-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L870-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L869-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L866-.L868
	.long	.L867-.L868
	.text
	.p2align 4,,10
	.p2align 3
.L870:
	leaq	-152(%rbp), %rdx
	movl	%r10d, %esi
	movq	%r15, %rdi
	call	ares_parse_a_reply@PLT
	movl	%eax, -168(%rbp)
.L872:
	movl	-168(%rbp), %eax
	testl	%eax, %eax
	jne	.L873
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	je	.L920
	cmpl	$5, %eax
	je	.L921
	cmpl	$2, %eax
	je	.L922
	cmpl	$12, %eax
	jne	.L878
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	-152(%rbp), %rdi
	movl	%eax, %r15d
	movq	8(%rdi), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L880
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L885:
	movq	352(%r12), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L923
.L881:
	leal	(%r15,%rbx), %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L924
.L882:
	movq	-152(%rbp), %rdi
	leal	1(%rbx), %edx
	movq	%rdx, %rbx
	movq	8(%rdi), %rax
	movq	(%rax,%rdx,8), %rsi
	testq	%rsi, %rsi
	jne	.L885
.L880:
	call	ares_free_hostent@PLT
.L873:
	movq	-184(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L925
	movl	-168(%rbp), %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	.cfi_restore_state
	movq	-152(%rbp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L926
.L875:
	movl	$1, (%rbx)
.L878:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	-152(%rbp), %rdi
	movl	%eax, -176(%rbp)
	movq	24(%rdi), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L880
	xorl	%ebx, %ebx
	leaq	-112(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L891:
	movl	16(%rdi), %edi
	movl	$46, %ecx
	movq	%r15, %rdx
	call	uv_inet_ntop@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	352(%r12), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L927
.L887:
	movl	-176(%rbp), %eax
	movq	%r14, %rsi
	movq	%r13, %rdi
	leal	(%rax,%rbx), %edx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L928
.L888:
	movq	-152(%rbp), %rdi
	leal	1(%rbx), %edx
	movq	%rdx, %rbx
	movq	24(%rdi), %rax
	movq	(%rax,%rdx,8), %rsi
	testq	%rsi, %rsi
	jne	.L891
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L871:
	leaq	-152(%rbp), %rdx
	movl	%r10d, %esi
	movq	%r15, %rdi
	call	ares_parse_ns_reply@PLT
	movl	%eax, -168(%rbp)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L867:
	leaq	-152(%rbp), %rdx
	movl	%r10d, %esi
	movq	%r15, %rdi
	call	ares_parse_aaaa_reply@PLT
	movl	%eax, -168(%rbp)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L869:
	leaq	-152(%rbp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r10d, %esi
	movl	$2, %r8d
	movq	%r15, %rdi
	call	ares_parse_ptr_reply@PLT
	movl	%eax, -168(%rbp)
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L926:
	movq	8(%rax), %rax
	cmpq	$0, (%rax)
	je	.L875
.L893:
	movl	$5, (%rbx)
	movq	352(%r12), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L929
.L876:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %edx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L930
.L877:
	movq	-152(%rbp), %rdi
	call	ares_free_hostent@PLT
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L927:
	movq	%rax, -192(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-192(%rbp), %rcx
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L928:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L921:
	movq	-152(%rbp), %rax
	movq	(%rax), %rsi
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L924:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L923:
	movq	%rax, -176(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-176(%rbp), %rcx
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L922:
	movq	-152(%rbp), %rax
	movq	%r12, %rdi
	movq	%r13, %rdx
	leaq	8(%rax), %rsi
	call	_ZN4node10cares_wrap12_GLOBAL__N_114HostentToNamesEPNS_11EnvironmentEP7hostentN2v85LocalINS6_5ArrayEEE.isra.0
	movq	-152(%rbp), %rdi
	jmp	.L880
.L866:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L930:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L877
.L929:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L876
.L925:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7706:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_, .-_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap5ParseEPhi:
.LFB7788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movl	$12, -84(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	subq	$8, %rsp
	movl	-100(%rbp), %edx
	leaq	-84(%rbp), %rcx
	pushq	$0
	movq	16(%r12), %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L936
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L931:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L937
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L936:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L931
.L937:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7788:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap5ParseEPhi:
.LFB7764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movl	$2, -84(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	subq	$8, %rsp
	movl	-100(%rbp), %edx
	leaq	-84(%rbp), %rcx
	pushq	$0
	movq	16(%r12), %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	movq	%r14, %rsi
	movq	%rax, %r13
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L943
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L938:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L944
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L943:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L938
.L944:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7764:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap5ParseEPhi:
.LFB7748:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movl	%edx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	subq	$8, %rsp
	movl	-100(%rbp), %edx
	leaq	-84(%rbp), %rcx
	pushq	$0
	movq	16(%r12), %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r8
	movq	%r14, %rsi
	movl	$5, -84(%rbp)
	movq	%rax, %r13
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L950
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L945:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L951
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L950:
	.cfi_restore_state
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L945
.L951:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7748:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap5ParseEPhi:
.LFB7740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1224, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -5336(%rbp)
	movq	%rsi, %rbx
	leaq	-5312(%rbp), %r15
	movq	%rdi, %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movl	$256, -5320(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	subq	$8, %rsp
	movq	%rbx, %rsi
	movl	-5336(%rbp), %edx
	movq	%rax, %r13
	leaq	-5320(%rbp), %rax
	movq	16(%r12), %rdi
	leaq	-5316(%rbp), %rcx
	pushq	%rax
	movq	%r13, %r8
	leaq	-5184(%rbp), %r9
	movl	$28, -5316(%rbp)
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L982
	movq	16(%r12), %rax
	movdqa	.LC32(%rip), %xmm0
	movslq	-5320(%rbp), %rbx
	movq	352(%rax), %r9
	movaps	%xmm0, -5280(%rbp)
	leaq	-5256(%rbp), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, -5256(%rbp)
	movq	%rax, -5360(%rbp)
	movq	%rax, -5264(%rbp)
	movq	$0, -5256(%rbp)
	movups	%xmm0, -5240(%rbp)
	movups	%xmm0, -5224(%rbp)
	movups	%xmm0, -5208(%rbp)
	cmpq	$8, %rbx
	ja	.L983
	movq	%rbx, -5280(%rbp)
	testq	%rbx, %rbx
	je	.L967
.L965:
	leaq	-5168(%rbp), %rcx
	xorl	%edx, %edx
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L962:
	movq	-5264(%rbp), %rsi
	addq	$20, %rcx
	movq	%rax, (%rsi,%rdx,8)
	addq	$1, %rdx
	cmpq	%rdx, %rbx
	jbe	.L961
.L963:
	movl	(%rcx), %esi
	movq	%r9, %rdi
	movq	%rdx, -5352(%rbp)
	movq	%rcx, -5344(%rbp)
	movq	%r9, -5336(%rbp)
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-5352(%rbp), %rdx
	cmpq	%rdx, -5280(%rbp)
	movq	-5336(%rbp), %r9
	movq	-5344(%rbp), %rcx
	ja	.L962
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L983:
	movabsq	$2305843009213693951, %rax
	leaq	0(,%rbx,8), %rdi
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L984
	movq	%r9, -5336(%rbp)
	testq	%rdi, %rdi
	je	.L957
	movq	%rdi, -5344(%rbp)
	call	malloc@PLT
	movq	-5336(%rbp), %r9
	testq	%rax, %rax
	je	.L985
	movq	%rax, -5264(%rbp)
	movq	%rbx, -5272(%rbp)
.L960:
	movq	%rbx, -5280(%rbp)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L982:
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L952:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L986
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L967:
	.cfi_restore_state
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L961:
	movq	%rbx, %rdx
	movq	%r9, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-5264(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L964
	cmpq	-5360(%rbp), %rdi
	je	.L964
	call	free@PLT
.L964:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L985:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-5344(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L957
	movq	-5280(%rbp), %rdx
	movq	%rax, -5264(%rbp)
	movq	%rbx, -5272(%rbp)
	movq	-5336(%rbp), %r9
	testq	%rdx, %rdx
	je	.L960
	movq	-5360(%rbp), %rsi
	salq	$3, %rdx
	movq	%r9, -5336(%rbp)
	call	memcpy@PLT
	movq	-5336(%rbp), %r9
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L957:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L984:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L986:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7740:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap5ParseEPhi
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap5ParseEPhi:
.LFB7732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-2240(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2112(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$2248, %rsp
	movl	%edx, -2264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movl	$256, -2248(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%rbx, %rsi
	movq	%rax, -2280(%rbp)
	movq	%rax, %r8
	leaq	-2248(%rbp), %rax
	movl	-2264(%rbp), %edx
	pushq	%rax
	movq	16(%r12), %rdi
	leaq	-2244(%rbp), %rcx
	movl	$1, -2244(%rbp)
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	jne	.L1017
	movq	16(%r12), %rax
	movdqa	.LC32(%rip), %xmm0
	movslq	-2248(%rbp), %rbx
	movq	352(%rax), %r10
	movaps	%xmm0, -2208(%rbp)
	leaq	-2184(%rbp), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, -2184(%rbp)
	movq	%rax, -2288(%rbp)
	movq	%rax, -2192(%rbp)
	movq	$0, -2184(%rbp)
	movups	%xmm0, -2168(%rbp)
	movups	%xmm0, -2152(%rbp)
	movups	%xmm0, -2136(%rbp)
	cmpq	$8, %rbx
	jbe	.L990
	movabsq	$2305843009213693951, %rax
	leaq	0(,%rbx,8), %rdi
	andq	%rbx, %rax
	cmpq	%rax, %rbx
	jne	.L1018
	movq	%r10, -2264(%rbp)
	testq	%rdi, %rdi
	je	.L992
	movq	%rdi, -2272(%rbp)
	call	malloc@PLT
	movq	-2264(%rbp), %r10
	testq	%rax, %rax
	je	.L1019
	movq	%rax, -2192(%rbp)
	movq	%rbx, -2200(%rbp)
.L995:
	movq	%rbx, -2208(%rbp)
.L1000:
	xorl	%edx, %edx
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L997:
	movq	-2192(%rbp), %rsi
	movq	%rax, (%rsi,%rdx,8)
	addq	$1, %rdx
	cmpq	%rdx, %rbx
	jbe	.L996
.L998:
	movl	4(%r13,%rdx,8), %esi
	movq	%r10, %rdi
	movq	%rdx, -2272(%rbp)
	movq	%r10, -2264(%rbp)
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-2272(%rbp), %rdx
	cmpq	%rdx, -2208(%rbp)
	movq	-2264(%rbp), %r10
	ja	.L997
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L990:
	movq	%rbx, -2208(%rbp)
	testq	%rbx, %rbx
	jne	.L1000
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L996:
	movq	%r10, %rdi
	movq	%rbx, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-2192(%rbp), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L999
	cmpq	-2288(%rbp), %rdi
	je	.L999
	call	free@PLT
.L999:
	movq	-2280(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1017:
	movl	%eax, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L987:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1020
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1019:
	.cfi_restore_state
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2272(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L992
	movq	-2208(%rbp), %rdx
	movq	%rax, -2192(%rbp)
	movq	%rbx, -2200(%rbp)
	movq	-2264(%rbp), %r10
	testq	%rdx, %rdx
	je	.L995
	movq	-2288(%rbp), %rsi
	salq	$3, %rdx
	movq	%r10, -2264(%rbp)
	call	memcpy@PLT
	movq	-2264(%rbp), %r10
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L992:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1018:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1020:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7732:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap5ParseEPhi
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1022
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L1022:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L1032
.L1023:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L1024
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L1032:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L1023
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.1
.LC33:
	.string	"ipv4"
.LC34:
	.string	"ipv6"
.LC35:
	.string	"name"
.LC36:
	.string	"family"
.LC37:
	.string	"reverse"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKc:
.LFB7808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	movq	%r13, %rdx
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$2, %edi
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_inet_pton@PLT
	testl	%eax, %eax
	je	.L1046
	movq	%r13, %rdx
	movq	%r15, %rsi
	movl	$10, %edi
	call	uv_inet_pton@PLT
	testl	%eax, %eax
	jne	.L1047
	movl	$16, -148(%rbp)
	movl	$10, %r14d
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762(%rip), %r12
	testq	%r12, %r12
	jne	.L1037
.L1069:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1038
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1065
.L1038:
	movq	%r12, _ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762(%rip)
	mfence
	testb	$5, (%r12)
	je	.L1039
.L1070:
	cmpl	$2, %r14d
	leaq	.LC34(%rip), %rdx
	movq	%r15, -128(%rbp)
	leaq	.LC33(%rip), %rax
	cmovne	%rdx, %rax
	leaq	.LC35(%rip), %rcx
	leaq	.LC36(%rip), %rdx
	movq	%rdx, %xmm1
	movq	%rcx, %xmm0
	movl	$1543, %edx
	punpcklqdq	%xmm1, %xmm0
	movw	%dx, -82(%rbp)
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1041
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1066
.L1041:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1042
	movq	(%rdi), %rax
	call	*8(%rax)
.L1042:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1039
	movq	(%rdi), %rax
	call	*8(%rax)
.L1039:
	cmpq	$0, 80(%rbx)
	jne	.L1067
	movl	$8, %edi
	call	_Znwm@PLT
	movl	-148(%rbp), %edx
	movl	%r14d, %ecx
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	movq	%rax, %r9
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiP7hostent(%rip), %r8
	movq	%rax, 80(%rbx)
	movq	56(%rbx), %rax
	movq	64(%rax), %rdi
	call	ares_gethostbyaddr@PLT
	xorl	%eax, %eax
.L1033:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1068
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1046:
	.cfi_restore_state
	movl	$4, -148(%rbp)
	movl	$2, %r14d
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762(%rip), %r12
	testq	%r12, %r12
	je	.L1069
.L1037:
	testb	$5, (%r12)
	je	.L1039
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1067:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap19MakeCallbackPointerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1066:
	subq	$8, %rsp
	leaq	-112(%rbp), %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC37(%rip), %rcx
	movl	$98, %esi
	pushq	%rdx
	leaq	-128(%rbp), %rdx
	pushq	%rdx
	leaq	-82(%rbp), %rdx
	pushq	%rdx
	leaq	-144(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$2
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1065:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1038
.L1047:
	movl	$-22, %eax
	jmp	.L1033
.L1068:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7808:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKc
	.section	.rodata.str1.1
.LC39:
	.string	"ip"
.LC40:
	.string	"port"
.LC41:
	.string	"lookupService"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1256, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1108
	cmpw	$1040, %cx
	jne	.L1072
.L1108:
	movq	23(%rdx), %rbx
.L1074:
	movl	16(%r15), %esi
	testl	%esi, %esi
	jg	.L1075
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1127
.L1077:
	movl	16(%r15), %edx
	cmpl	$1, %edx
	jle	.L1128
	movq	8(%r15), %rax
	subq	$8, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1129
.L1080:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	(%r15), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1080
.L1129:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1080
	cmpl	$2, %edx
	jg	.L1082
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1083:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1130
	movl	16(%r15), %eax
	testl	%eax, %eax
	jg	.L1085
	movq	(%r15), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	movq	%r14, %rdx
.L1088:
	movq	352(%rbx), %rsi
	leaq	-1120(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$2, 16(%r15)
	jg	.L1089
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1090:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L1131
.L1091:
	shrq	$32, %r13
	movq	-1104(%rbp), %rdi
	movq	%r13, %rax
	movq	%r13, -1288(%rbp)
	leaq	-1248(%rbp), %r13
	movq	%r13, %rdx
	movl	%eax, %esi
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	jne	.L1132
.L1092:
	movl	$1408, %edi
	call	_Znwm@PLT
	movl	$10, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L1133
	movq	2112(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapE(%rip), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%rbx), %rdx
	movq	%rax, (%r12)
	addq	$112, %rax
	movq	%rdx, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movq	%rax, 56(%r12)
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2021(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1134
.L1095:
	testb	$5, (%rsi)
	jne	.L1135
.L1097:
	cmpq	$0, 80(%r12)
	movq	%r12, 88(%r12)
	jne	.L1136
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_(%rip), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE7WrapperES2_iS4_S4_(%rip), %rdx
	movl	$8, %r8d
	movq	%r13, %rcx
	movq	%rax, 80(%r12)
	movq	16(%r12), %rax
	leaq	88(%r12), %rsi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_getnameinfo@PLT
	movq	%rax, %rdx
	salq	$32, %rdx
	testl	%eax, %eax
	js	.L1099
	movq	16(%r12), %rcx
	addl	$1, 2156(%rcx)
	movq	(%r15), %rcx
	testl	%eax, %eax
	jne	.L1100
	movq	%rdx, 24(%rcx)
.L1101:
	movq	-1104(%rbp), %rdi
	leaq	-1096(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1071
	testq	%rdi, %rdi
	je	.L1071
	call	free@PLT
.L1071:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1137
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1075:
	.cfi_restore_state
	movq	8(%r15), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1077
.L1127:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	8(%r15), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1089:
	movq	8(%r15), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	8(%r15), %r14
	cmpl	$1, %eax
	je	.L1138
	leaq	-8(%r14), %rdx
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	(%r15), %rcx
.L1100:
	leaq	16+_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE(%rip), %rax
	movq	%rdx, 24(%rcx)
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L1139
	movq	64(%r12), %rdx
	movq	72(%r12), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movl	$1408, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1135:
	subq	$8, %rsp
	movq	%r12, %rcx
	leaq	-58(%rbp), %r9
	movl	$98, %edi
	leaq	.LC40(%rip), %rax
	leaq	.LC39(%rip), %rbx
	movq	%rax, %xmm1
	movq	%rbx, %xmm0
	movl	$519, %eax
	punpcklqdq	%xmm1, %xmm0
	movw	%ax, -58(%rbp)
	leaq	-1264(%rbp), %rax
	leaq	.LC41(%rip), %rdx
	pushq	%rax
	leaq	-1280(%rbp), %r8
	movaps	%xmm0, -1280(%rbp)
	movq	-1104(%rbp), %xmm0
	movhps	-1288(%rbp), %xmm0
	movaps	%xmm0, -1264(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	popq	%rdx
	popq	%rcx
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1134:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1096
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1140
.L1096:
	movq	%rsi, _ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2021(%rip)
	mfence
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1072:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1130:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1131:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1132:
	movl	-1288(%rbp), %esi
	movq	-1104(%rbp), %rdi
	movq	%r13, %rdx
	call	uv_ip6_addr@PLT
	testl	%eax, %eax
	je	.L1092
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1133:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1136:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE3ForEPNS_7ReqWrapIS1_EES6_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1140:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1139:
	leaq	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1137:
	call	__stack_chk_fail@PLT
.L1138:
	movq	(%r15), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L1088
	.cfi_endproc
.LFE7828:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%rdi, -160(%rbp)
	movq	32(%r12), %rdx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1160
	cmpw	$1040, %cx
	jne	.L1142
.L1160:
	movq	%r12, %rdi
	movq	23(%rdx), %rbx
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1170
.L1145:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1161
	cmpw	$1040, %cx
	jne	.L1146
.L1161:
	movq	23(%rdx), %r12
.L1148:
	testq	%r12, %r12
	je	.L1141
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	64(%r12), %rdi
	leaq	-136(%rbp), %rsi
	movq	%rax, %r14
	call	ares_get_servers_ports@PLT
	testl	%eax, %eax
	jne	.L1171
	movq	-136(%rbp), %r15
	testq	%r15, %r15
	je	.L1151
	leaq	-128(%rbp), %rax
	xorl	%r12d, %r12d
	leaq	-112(%rbp), %r13
	movq	%rax, -152(%rbp)
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1172
.L1153:
	movl	28(%r15), %esi
	movq	352(%rbx), %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	movl	$2, %edx
	movq	-152(%rbp), %rsi
	movq	%rax, -120(%rbp)
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	3280(%rbx), %rsi
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1173
	movq	(%r15), %r15
	addl	$1, %r12d
	testq	%r15, %r15
	je	.L1174
.L1155:
	movl	8(%r15), %edi
	leaq	12(%r15), %rsi
	movl	$46, %ecx
	movq	%r13, %rdx
	call	uv_inet_ntop@PLT
	testl	%eax, %eax
	je	.L1152
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1174:
	movq	-136(%rbp), %r15
.L1151:
	movq	-160(%rbp), %rax
	movq	(%rax), %rax
	testq	%r14, %r14
	je	.L1175
	movq	(%r14), %rdx
.L1157:
	movq	%rdx, 24(%rax)
	movq	%r15, %rdi
	call	ares_free_data@PLT
.L1141:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1176
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1172:
	.cfi_restore_state
	movq	%rax, -168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-168(%rbp), %rax
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1142:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	movq	-160(%rbp), %rax
	movq	(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L1145
	.p2align 4,,10
	.p2align 3
.L1170:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	-136(%rbp), %rdi
	call	ares_free_data@PLT
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1171:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1175:
	movq	16(%rax), %rdx
	jmp	.L1157
.L1176:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7829:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap5SetupEv, @function
_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap5SetupEv:
.LFB7662:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$13, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	leaq	-152(%rbp), %rdx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rdx, %rdi
	subq	$176, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 74(%rbx)
	movl	$128, -160(%rbp)
	rep stosq
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviii(%rip), %rax
	movq	%rbx, -80(%rbp)
	movq	%rax, -88(%rbp)
	movl	76(%rbx), %eax
	movl	%eax, -156(%rbp)
	jne	.L1178
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movl	$1, %edi
	call	ares_library_init@PLT
	testl	%eax, %eax
	jne	.L1243
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
.L1178:
	leaq	-160(%rbp), %rsi
	leaq	64(%rbx), %rdi
	movl	$8705, %edx
	call	ares_init_options@PLT
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.L1244
	movb	$1, 74(%rbx)
.L1177:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1245
	addq	$176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1243:
	.cfi_restore_state
	movq	16(%rbx), %rbx
	cmpl	$24, %eax
	ja	.L1209
	leaq	.L1182(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1182:
	.long	.L1209-.L1182
	.long	.L1234-.L1182
	.long	.L1233-.L1182
	.long	.L1232-.L1182
	.long	.L1231-.L1182
	.long	.L1230-.L1182
	.long	.L1229-.L1182
	.long	.L1228-.L1182
	.long	.L1227-.L1182
	.long	.L1226-.L1182
	.long	.L1225-.L1182
	.long	.L1224-.L1182
	.long	.L1223-.L1182
	.long	.L1222-.L1182
	.long	.L1221-.L1182
	.long	.L1220-.L1182
	.long	.L1219-.L1182
	.long	.L1218-.L1182
	.long	.L1217-.L1182
	.long	.L1216-.L1182
	.long	.L1215-.L1182
	.long	.L1214-.L1182
	.long	.L1213-.L1182
	.long	.L1239-.L1182
	.long	.L1210-.L1182
	.text
	.p2align 4,,10
	.p2align 3
.L1244:
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	call	ares_library_cleanup@PLT
	movq	16(%rbx), %rbx
	cmpl	$24, %r12d
	ja	.L1209
	leaq	.L1211(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1211:
	.long	.L1209-.L1211
	.long	.L1234-.L1211
	.long	.L1233-.L1211
	.long	.L1232-.L1211
	.long	.L1231-.L1211
	.long	.L1230-.L1211
	.long	.L1229-.L1211
	.long	.L1228-.L1211
	.long	.L1227-.L1211
	.long	.L1226-.L1211
	.long	.L1225-.L1211
	.long	.L1224-.L1211
	.long	.L1223-.L1211
	.long	.L1222-.L1211
	.long	.L1221-.L1211
	.long	.L1220-.L1211
	.long	.L1219-.L1211
	.long	.L1218-.L1211
	.long	.L1217-.L1211
	.long	.L1216-.L1211
	.long	.L1215-.L1211
	.long	.L1214-.L1211
	.long	.L1213-.L1211
	.long	.L1239-.L1211
	.long	.L1210-.L1211
	.text
	.p2align 4,,10
	.p2align 3
.L1239:
	leaq	.LC6(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	352(%rbx), %rsi
	leaq	-192(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	352(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1246
.L1235:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1226:
	leaq	.LC29(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1213:
	leaq	.LC18(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1214:
	leaq	.LC24(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1215:
	leaq	.LC8(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1216:
	leaq	.LC21(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1217:
	leaq	.LC7(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1218:
	leaq	.LC12(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1219:
	leaq	.LC15(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1220:
	leaq	.LC20(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1221:
	leaq	.LC16(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1230:
	leaq	.LC23(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1231:
	leaq	.LC22(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1232:
	leaq	.LC27(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1233:
	leaq	.LC17(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1234:
	leaq	.LC19(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1210:
	leaq	.LC13(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1222:
	leaq	.LC25(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1223:
	leaq	.LC28(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1224:
	leaq	.LC14(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1225:
	leaq	.LC11(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1228:
	leaq	.LC10(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1229:
	leaq	.LC26(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1227:
	leaq	.LC9(%rip), %r14
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1246:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L1235
.L1209:
	leaq	.LC5(%rip), %r14
	jmp	.L1212
.L1245:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7662:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap5SetupEv, .-_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap5SetupEv
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0:
.LFB12494:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	56(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 72(%r12)
	jne	.L1254
	cmpb	$0, 73(%r12)
	jne	.L1286
.L1254:
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKciiE28trace_event_unique_atomic614(%rip), %r12
	testq	%r12, %r12
	je	.L1287
.L1256:
	testb	$5, (%r12)
	jne	.L1288
.L1258:
	cmpq	$0, 80(%rbx)
	jne	.L1289
.L1262:
	movl	$8, %edi
	call	_Znwm@PLT
	movl	%r14d, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%rbx, (%rax)
	movq	%rax, %r9
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiPhi(%rip), %r8
	movq	%rax, 80(%rbx)
	movq	56(%rbx), %rax
	movq	64(%rax), %rdi
	call	ares_query@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1290
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1286:
	.cfi_restore_state
	movq	64(%r12), %rdi
	leaq	-88(%rbp), %rsi
	movq	$0, -88(%rbp)
	call	ares_get_servers_ports@PLT
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1254
	cmpq	$0, (%rdi)
	jne	.L1251
	cmpl	$2, 8(%rdi)
	jne	.L1251
	cmpl	$16777343, 12(%rdi)
	jne	.L1251
	movl	32(%rdi), %edx
	testl	%edx, %edx
	jne	.L1251
	movl	28(%rdi), %eax
	testl	%eax, %eax
	jne	.L1251
	call	ares_free_data@PLT
	movq	64(%r12), %rdi
	movq	$0, -88(%rbp)
	call	ares_destroy@PLT
	movq	56(%r12), %r15
	testq	%r15, %r15
	je	.L1253
	movq	16(%r12), %rdx
	movl	$24, %edi
	addl	$1, 2152(%rdx)
	movq	%rdx, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %rdx
	leaq	_ZZN4node11Environment11CloseHandleI10uv_timer_sZNS_10cares_wrap12_GLOBAL__N_111ChannelWrap10CloseTimerEvEUlPS2_E_EEvPT_T0_ENUlP11uv_handle_sE_4_FUNESC_(%rip), %rsi
	movq	%r15, %rdi
	movq	%rdx, (%rax)
	movq	(%r15), %rdx
	movq	%rdx, 16(%rax)
	movq	%rax, (%r15)
	call	uv_close@PLT
	movq	$0, 56(%r12)
.L1253:
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap5SetupEv
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1251:
	call	ares_free_data@PLT
	movb	$0, 73(%r12)
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKciiE28trace_event_unique_atomic614(%rip), %r12
	testq	%r12, %r12
	jne	.L1256
.L1287:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1257
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1291
.L1257:
	movq	%r12, _ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKciiE28trace_event_unique_atomic614(%rip)
	mfence
	testb	$5, (%r12)
	je	.L1258
.L1288:
	leaq	.LC35(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$7, -97(%rbp)
	movq	72(%rbx), %r15
	movq	%rax, -96(%rbp)
	movq	%r13, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1259
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1292
.L1259:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1260
	movq	(%rdi), %rax
	call	*8(%rax)
.L1260:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1258
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpq	$0, 80(%rbx)
	je	.L1262
.L1289:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap19MakeCallbackPointerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1291:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1292:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	$6
	movq	%r15, %rcx
	movl	$98, %esi
	pushq	%rdx
	leaq	-88(%rbp), %rdx
	pushq	%rdx
	leaq	-97(%rbp), %rdx
	pushq	%rdx
	leaq	-96(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1259
.L1290:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12494:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap4SendEPKc:
.LFB7718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$255, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7718:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap4SendEPKc:
.LFB7728:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7728:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap4SendEPKc:
.LFB7736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$28, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7736:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap4SendEPKc:
.LFB7744:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$5, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7744:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap4SendEPKc:
.LFB7752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$15, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7752:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap4SendEPKc:
.LFB7760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$2, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7760:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap4SendEPKc:
.LFB7768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7768:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap4SendEPKc:
.LFB7776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$33, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7776:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap4SendEPKc:
.LFB7784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$12, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7784:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap4SendEPKc:
.LFB7792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$35, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7792:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap4SendEPKc, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap4SendEPKc:
.LFB7800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$6, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	xorl	%eax, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7800:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap4SendEPKc, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap4SendEPKc
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB7632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1316
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1331
.L1316:
	cmpl	$1, 16(%r12)
	je	.L1332
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1332:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L1333
	movl	16(%r12), %ecx
	testl	%ecx, %ecx
	jle	.L1334
	movq	8(%r12), %rdi
.L1319:
	call	_ZNK2v85Int325ValueEv@PLT
	movq	(%r12), %rdi
	movl	%eax, %ebx
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1328
	cmpw	$1040, %cx
	jne	.L1320
.L1328:
	movq	23(%rdx), %r14
.L1322:
	movq	8(%r12), %r13
	movl	$144, %edi
	call	_Znwm@PLT
	movsd	.LC38(%rip), %xmm0
	movl	$2, %ecx
	movq	%r14, %rsi
	addq	$8, %r13
	movq	%rax, %rdi
	movq	%rax, %r12
	movq	%r13, %rdx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111ChannelWrapE(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$257, %edx
	movq	%rax, (%r12)
	leaq	136(%r12), %rax
	movq	%rax, 88(%r12)
	movq	24(%r12), %rax
	movw	%dx, 72(%r12)
	movb	$0, 74(%r12)
	movl	%ebx, 76(%r12)
	movl	$0, 80(%r12)
	movq	$1, 96(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movl	$0x3f800000, 120(%r12)
	movq	$0, 128(%r12)
	movq	$0, 136(%r12)
	movups	%xmm0, 56(%r12)
	testq	%rax, %rax
	je	.L1325
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1324
.L1325:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L1324:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap5SetupEv
	.p2align 4,,10
	.p2align 3
.L1334:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1331:
	cmpl	$5, 43(%rax)
	jne	.L1316
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1333:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1320:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1322
	.cfi_endproc
.LFE7632:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.section	.rodata.str1.1
.LC43:
	.string	"hostname"
.LC44:
	.string	"service"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_, @function
_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_:
.LFB7819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-176(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%rdx, -184(%rbp)
	movq	%r15, %rdi
	movq	%rcx, -192(%rbp)
	movq	16(%r12), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	movq	%rax, -96(%rbp)
	leaq	104(%rdi), %rax
	movq	%rax, -88(%rbp)
	movq	%rax, -80(%rbp)
	testl	%r13d, %r13d
	je	.L1379
.L1336:
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_E29trace_event_unique_atomic1909(%rip), %r13
	testq	%r13, %r13
	je	.L1380
.L1340:
	testb	$5, 0(%r13)
	jne	.L1381
.L1342:
	movq	360(%rbx), %rax
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	1136(%rax), %r13
	testq	%rdi, %rdi
	je	.L1346
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1382
.L1346:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1349
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L1383
.L1349:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	16+_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L1384
	movq	72(%r12), %rax
	movq	64(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movl	$1408, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1385
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1381:
	.cfi_restore_state
	leaq	.LC44(%rip), %rax
	leaq	.LC43(%rip), %rcx
	movq	%rax, %xmm2
	movq	-184(%rbp), %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -128(%rbp)
	movl	$1799, %eax
	movw	%ax, -58(%rbp)
	movq	-192(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -120(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1343
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1386
.L1343:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1344
	movq	(%rdi), %rax
	call	*8(%rax)
.L1344:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1342
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1380:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1341
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1387
.L1341:
	movq	%r13, _ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_E29trace_event_unique_atomic1909(%rip)
	mfence
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	-184(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1388
.L1337:
	movq	352(%rbx), %rdi
	movq	-192(%rbp), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1389
.L1338:
	movq	%r13, %xmm0
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, -88(%rbp)
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1383:
	leaq	-96(%rbp), %rcx
	movl	$3, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1384:
	leaq	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1387:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1386:
	subq	$8, %rsp
	leaq	-112(%rbp), %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC41(%rip), %rcx
	movl	$101, %esi
	pushq	%rdx
	leaq	-128(%rbp), %rdx
	pushq	%rdx
	leaq	-58(%rbp), %rdx
	pushq	%rdx
	leaq	-144(%rbp), %rdx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$2
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1388:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rax
	jmp	.L1338
.L1385:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7819:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_, .-_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_
	.section	.rodata.str1.1
.LC45:
	.string	"count"
.LC46:
	.string	"verbatim"
.LC47:
	.string	"lookup"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfo, @function
_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfo:
.LFB7814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%rdx, -232(%rbp)
	movq	%r15, %rdi
	movq	16(%r12), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	movq	$0, -216(%rbp)
	movq	%rax, %xmm0
	movzbl	248(%r12), %eax
	leaq	104(%rdi), %rdx
	movq	%rdx, %xmm1
	movb	%al, -217(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -176(%rbp)
	testl	%r13d, %r13d
	je	.L1461
.L1391:
	movq	-232(%rbp), %rdi
	call	uv_freeaddrinfo@PLT
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoE29trace_event_unique_atomic1875(%rip), %r13
	testq	%r13, %r13
	je	.L1462
.L1410:
	testb	$5, 0(%r13)
	jne	.L1463
.L1412:
	movq	360(%rbx), %rax
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	1136(%rax), %r13
	testq	%rdi, %rdi
	je	.L1416
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1464
.L1416:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1419
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L1465
.L1419:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	16+_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpq	$0, 8(%r12)
	movq	%rax, 56(%r12)
	je	.L1466
	movq	72(%r12), %rax
	movq	64(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movl	$256, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1467
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1464:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1461:
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	%rax, -248(%rbp)
	movq	-232(%rbp), %rax
	testq	%rax, %rax
	je	.L1392
	movq	%rax, %r13
	leaq	-112(%rbp), %rax
	movq	%rax, -240(%rbp)
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1394:
	cmpl	$10, %edi
	jne	.L1399
	cmpb	$0, -217(%rbp)
	je	.L1399
	movq	24(%r13), %rax
	leaq	8(%rax), %rsi
.L1395:
	movq	-240(%rbp), %rdx
	movl	$46, %ecx
	call	uv_inet_ntop@PLT
	testl	%eax, %eax
	jne	.L1399
	movq	352(%rbx), %rdi
	movq	-240(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1468
.L1397:
	movq	3280(%rbx), %rsi
	movl	-216(%rbp), %edx
	movq	-248(%rbp), %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1469
.L1398:
	addq	$1, -216(%rbp)
.L1399:
	movq	40(%r13), %r13
	testq	%r13, %r13
	je	.L1470
.L1401:
	cmpl	$1, 8(%r13)
	jne	.L1402
	movl	4(%r13), %edi
	cmpl	$2, %edi
	jne	.L1394
	movq	24(%r13), %rax
	leaq	4(%rax), %rsi
	jmp	.L1395
	.p2align 4,,10
	.p2align 3
.L1463:
	leaq	.LC46(%rip), %rax
	leaq	.LC45(%rip), %rcx
	movl	$258, %edx
	movq	%rax, %xmm2
	movq	-216(%rbp), %rax
	movq	%rcx, %xmm0
	movw	%dx, -112(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -144(%rbp)
	xorl	%eax, %eax
	movb	-217(%rbp), %al
	movaps	%xmm0, -160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%rax, -136(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1413
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1471
.L1413:
	movq	-120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1414
	movq	(%rdi), %rax
	call	*8(%rax)
.L1414:
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1412
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1462:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1411
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1472
.L1411:
	movq	%r13, _ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoE29trace_event_unique_atomic1875(%rip)
	mfence
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1476:
	leaq	-112(%rbp), %rax
	movq	-232(%rbp), %r13
	movq	%rax, -240(%rbp)
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1475:
	cmpl	$10, 4(%r13)
	jne	.L1406
	movq	24(%r13), %rax
	movl	$46, %ecx
	movl	$10, %edi
	movq	-240(%rbp), %rdx
	leaq	8(%rax), %rsi
	call	uv_inet_ntop@PLT
	testl	%eax, %eax
	jne	.L1406
	movq	352(%rbx), %rdi
	movq	-240(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1473
.L1404:
	movq	3280(%rbx), %rsi
	movl	-216(%rbp), %edx
	movq	-248(%rbp), %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1474
.L1405:
	addq	$1, -216(%rbp)
.L1406:
	movq	40(%r13), %r13
	testq	%r13, %r13
	je	.L1423
.L1407:
	cmpl	$1, 8(%r13)
	je	.L1475
	.p2align 4,,10
	.p2align 3
.L1402:
	leaq	_ZZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoENKUlbbE_clEbbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1465:
	leaq	-176(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1470:
	cmpb	$0, -217(%rbp)
	je	.L1476
.L1423:
	cmpq	$0, -216(%rbp)
	je	.L1392
.L1408:
	movq	-248(%rbp), %rax
	movq	%rax, -168(%rbp)
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1469:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	%rax, -256(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-256(%rbp), %rcx
	jmp	.L1397
	.p2align 4,,10
	.p2align 3
.L1472:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1471:
	subq	$8, %rsp
	leaq	-128(%rbp), %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC47(%rip), %rcx
	movl	$101, %esi
	pushq	%rdx
	leaq	-144(%rbp), %rdx
	pushq	%rdx
	leaq	-112(%rbp), %rdx
	pushq	%rdx
	leaq	-160(%rbp), %rdx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$2
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1466:
	leaq	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	352(%rbx), %rdi
	movl	$-3007, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	$0, -216(%rbp)
	movq	%rax, -176(%rbp)
	jmp	.L1408
.L1474:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1405
.L1473:
	movq	%rax, -256(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-256(%rbp), %rcx
	jmp	.L1404
.L1467:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7814:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfo, .-_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfo
	.section	.rodata.str1.1
.LC48:
	.string	"unspec"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1192, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1532
	cmpw	$1040, %cx
	jne	.L1478
.L1532:
	movq	23(%rdx), %r12
.L1480:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L1481
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1559
.L1483:
	movl	16(%rbx), %edx
	cmpl	$1, %edx
	jle	.L1560
	movq	8(%rbx), %rax
	subq	$8, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1561
.L1486:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1486
.L1561:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1486
	cmpl	$2, %edx
	jg	.L1488
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1489:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L1562
	cmpl	$4, 16(%rbx)
	jg	.L1491
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1492:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1563
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1494
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	movq	%r13, %rdx
.L1497:
	movq	352(%r12), %rsi
	leaq	-1120(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$3, 16(%rbx)
	jg	.L1498
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1499:
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movl	$0, -1220(%rbp)
	testb	%al, %al
	je	.L1500
	cmpl	$3, 16(%rbx)
	jle	.L1564
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
.L1502:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, -1220(%rbp)
.L1500:
	cmpl	$2, 16(%rbx)
	jg	.L1503
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, %r15d
	cmpl	$4, %eax
	je	.L1526
.L1572:
	cmpl	$6, %eax
	jne	.L1565
	movl	$10, %r15d
.L1505:
	cmpl	$4, 16(%rbx)
	jg	.L1506
.L1573:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1507:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	$256, %edi
	movb	%al, -1221(%rbp)
	call	_Znwm@PLT
	movl	$9, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r12)
	movq	%rax, 56(%r14)
	leaq	64(%r14), %rax
	movq	%rax, 64(%r14)
	movq	%rax, 72(%r14)
	je	.L1566
	movq	2112(%r12), %rdx
	pxor	%xmm0, %xmm0
	movl	%r15d, -1212(%rbp)
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapE(%rip), %rax
	movq	%rax, (%r14)
	addq	$112, %rax
	movq	%rax, 56(%r14)
	movzbl	-1221(%rbp), %eax
	movq	%rdx, 64(%r14)
	leaq	2112(%r12), %rdx
	movb	%al, 248(%r14)
	movl	-1220(%rbp), %eax
	movups	%xmm0, -1208(%rbp)
	movq	%rdx, 72(%r14)
	movq	$0, 80(%r14)
	movq	$0, 88(%r14)
	movq	$0, -1176(%rbp)
	movl	$1, -1208(%rbp)
	movl	%eax, -1216(%rbp)
	movups	%xmm0, -1192(%rbp)
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic1986(%rip), %r12
	testq	%r12, %r12
	je	.L1567
.L1510:
	testb	$5, (%r12)
	je	.L1512
	leaq	.LC33(%rip), %rax
	cmpl	$2, %r15d
	je	.L1513
	cmpl	$10, %r15d
	leaq	.LC34(%rip), %rax
	leaq	.LC48(%rip), %rdx
	cmovne	%rdx, %rax
.L1513:
	leaq	.LC36(%rip), %rdx
	leaq	.LC43(%rip), %rsi
	movq	%rax, -1144(%rbp)
	movq	%rdx, %xmm1
	movq	%rsi, %xmm0
	movq	-1104(%rbp), %rdx
	punpcklqdq	%xmm1, %xmm0
	movq	%rdx, -1152(%rbp)
	movl	$1543, %edx
	movaps	%xmm0, -1168(%rbp)
	pxor	%xmm0, %xmm0
	movw	%dx, -58(%rbp)
	movaps	%xmm0, -1136(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1514
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1568
.L1514:
	movq	-1128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1515
	movq	(%rdi), %rax
	call	*8(%rax)
.L1515:
	movq	-1136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1512
	movq	(%rdi), %rax
	call	*8(%rax)
.L1512:
	cmpq	$0, 80(%r14)
	movq	-1104(%rbp), %rcx
	movq	%r14, 88(%r14)
	jne	.L1569
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfo(%rip), %rax
	leaq	88(%r14), %rsi
	xorl	%r8d, %r8d
	movq	%rax, 80(%r14)
	movq	16(%r14), %rax
	leaq	_ZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE7WrapperES2_iS4_(%rip), %rdx
	leaq	-1216(%rbp), %r9
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_getaddrinfo@PLT
	movq	%rax, %rdx
	salq	$32, %rdx
	testl	%eax, %eax
	js	.L1518
	movq	16(%r14), %rcx
	addl	$1, 2156(%rcx)
	movq	(%rbx), %rcx
	testl	%eax, %eax
	jne	.L1519
	movq	%rdx, 24(%rcx)
.L1520:
	movq	-1104(%rbp), %rdi
	leaq	-1096(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1477
	testq	%rdi, %rdi
	je	.L1477
	call	free@PLT
.L1477:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1570
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1481:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1483
.L1559:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1565:
	testl	%eax, %eax
	je	.L1505
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1489
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1492
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	8(%rbx), %r13
	cmpl	$1, %eax
	je	.L1571
	leaq	-8(%r13), %rdx
	jmp	.L1497
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, %r15d
	cmpl	$4, %eax
	jne	.L1572
.L1526:
	cmpl	$4, 16(%rbx)
	movl	$2, %r15d
	jle	.L1573
.L1506:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1507
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	(%rbx), %rcx
.L1519:
	leaq	16+_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE(%rip), %rax
	movq	%rdx, 24(%rcx)
	movq	%rax, (%r14)
	addq	$112, %rax
	cmpq	$0, 8(%r14)
	movq	%rax, 56(%r14)
	je	.L1574
	movq	64(%r14), %rdx
	movq	72(%r14), %rax
	movq	%r14, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	movl	$256, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1502
	.p2align 4,,10
	.p2align 3
.L1478:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1567:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1511
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1575
.L1511:
	movq	%r12, _ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic1986(%rip)
	mfence
	jmp	.L1510
	.p2align 4,,10
	.p2align 3
.L1562:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1563:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1566:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1569:
	leaq	_ZZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE3ForEPNS_7ReqWrapIS1_EES6_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1568:
	subq	$8, %rsp
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movl	$98, %esi
	leaq	-1136(%rbp), %rdx
	pushq	$6
	leaq	.LC47(%rip), %rcx
	pushq	%rdx
	leaq	-1152(%rbp), %rdx
	pushq	%rdx
	leaq	-58(%rbp), %rdx
	pushq	%rdx
	leaq	-1168(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$2
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1514
	.p2align 4,,10
	.p2align 3
.L1575:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L1574:
	leaq	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1570:
	call	__stack_chk_fail@PLT
.L1571:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L1497
	.cfi_endproc
.LFE7827:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node13OneByteStringEPN2v87IsolateEPKci,"axG",@progbits,_ZN4node13OneByteStringEPN2v87IsolateEPKci,comdat
	.p2align 4
	.weak	_ZN4node13OneByteStringEPN2v87IsolateEPKci
	.type	_ZN4node13OneByteStringEPN2v87IsolateEPKci, @function
_ZN4node13OneByteStringEPN2v87IsolateEPKci:
.LFB4101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1579
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1579:
	.cfi_restore_state
	movq	%rax, -8(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4101:
	.size	_ZN4node13OneByteStringEPN2v87IsolateEPKci, .-_ZN4node13OneByteStringEPN2v87IsolateEPKci
	.text
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhi:
.LFB7722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$3336, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%edx, -7404(%rbp)
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	%edx, %r14d
	movq	%rsi, -7400(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	352(%rax), %rsi
	leaq	-7296(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -7432(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	xorl	%esi, %esi
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	subq	$8, %rsp
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%rax, %rbx
	movq	16(%r12), %rdi
	leaq	-7372(%rbp), %rax
	leaq	-7232(%rbp), %r9
	movq	%rax, %rcx
	movq	%rax, -7424(%rbp)
	leaq	-7368(%rbp), %rax
	movq	%rbx, %r8
	pushq	%rax
	movq	%r9, -7384(%rbp)
	movl	$256, -7368(%rbp)
	movl	$-1, -7372(%rbp)
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	movq	%rbx, %rdi
	movl	%eax, %r14d
	call	_ZNK2v85Array6LengthEv@PLT
	popq	%r10
	popq	%r11
	movl	%eax, -7392(%rbp)
	cmpl	$1, %r14d
	ja	.L1713
	cmpl	$1, -7372(%rbp)
	je	.L1714
	movl	-7392(%rbp), %edi
	testl	%edi, %edi
	je	.L1593
	xorl	%r15d, %r15d
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	1880(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1715
.L1597:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	496(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1716
.L1598:
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1717
	addl	$1, %r15d
	cmpl	%r15d, -7392(%rbp)
	je	.L1593
.L1601:
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L1596
	movq	%rax, -7384(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-7384(%rbp), %rcx
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1714:
	cmpl	%eax, -7368(%rbp)
	jne	.L1718
	leal	-1(%rax), %esi
	xorl	%r15d, %r15d
	movq	%rsi, -7416(%rbp)
	testl	%eax, %eax
	jne	.L1595
	.p2align 4,,10
	.p2align 3
.L1593:
	subq	$8, %rsp
	movq	16(%r12), %rdi
	movq	%rbx, %r8
	leaq	-7364(%rbp), %rax
	pushq	%rax
	movq	-7424(%rbp), %rcx
	leaq	-5184(%rbp), %r9
	movl	-7404(%rbp), %edx
	movq	-7400(%rbp), %rsi
	movl	$256, -7364(%rbp)
	movl	$28, -7372(%rbp)
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	movq	%rbx, %rdi
	movl	%eax, %r15d
	call	_ZNK2v85Array6LengthEv@PLT
	popq	%r8
	popq	%r9
	movl	%eax, %r14d
	cmpl	$1, %r15d
	ja	.L1719
	subl	-7392(%rbp), %eax
	cmpl	%eax, -7364(%rbp)
	jne	.L1720
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%eax, %r14d
	jne	.L1603
	leaq	-5168(%rbp), %r15
	movq	%r15, -7384(%rbp)
	movl	-7392(%rbp), %r15d
	jmp	.L1604
	.p2align 4,,10
	.p2align 3
.L1726:
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1721
.L1606:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	184(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1722
.L1607:
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	movq	-7384(%rbp), %rax
	movl	(%rax), %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	1760(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1723
.L1608:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	488(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1724
.L1609:
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1725
.L1610:
	addq	$20, -7384(%rbp)
	addl	$1, %r15d
.L1604:
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r15d, %eax
	ja	.L1726
	movq	-7400(%rbp), %rsi
	movq	16(%r12), %rdi
	movl	$1, %r8d
	movq	%rbx, %rcx
	movl	-7404(%rbp), %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_112ParseMxReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	movl	%eax, %esi
	cmpl	$1, %eax
	ja	.L1711
	movl	$2, -7372(%rbp)
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	16(%r12), %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	subq	$8, %rsp
	movq	-7424(%rbp), %rcx
	movq	-7400(%rbp), %rsi
	movl	%eax, %r15d
	pushq	$0
	movl	-7404(%rbp), %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	popq	%rcx
	popq	%rsi
	cmpl	$1, %eax
	jbe	.L1612
.L1712:
	movl	%eax, %esi
.L1711:
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
.L1582:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-7432(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1580:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1727
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1732:
	.cfi_restore_state
	movq	%rax, -7440(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-7440(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	184(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1728
.L1589:
	movq	-7384(%rbp), %rax
	movl	4(%rax,%r15,8), %esi
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%r12), %rax
	movq	360(%rax), %rax
	movq	1760(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1729
.L1590:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	480(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1730
.L1591:
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1731
	leaq	1(%r15), %rax
	cmpq	-7416(%rbp), %r15
	je	.L1593
.L1594:
	movq	%rax, %r15
.L1595:
	movq	16(%r12), %rax
	movl	%r15d, -7440(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	jne	.L1588
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1737:
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1733
.L1614:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	1880(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1734
.L1615:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	520(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1735
.L1616:
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1736
.L1617:
	addl	$1, %r15d
.L1612:
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r15d, %eax
	ja	.L1737
	movq	-7400(%rbp), %rsi
	movq	16(%r12), %rdi
	movl	$1, %r8d
	movq	%rbx, %rcx
	movl	-7404(%rbp), %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_113ParseTxtReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	movl	%eax, %esi
	cmpl	$1, %eax
	ja	.L1711
	movq	16(%r12), %rdi
	movl	-7404(%rbp), %edx
	movl	$1, %r8d
	movq	%rbx, %rcx
	movq	-7400(%rbp), %rsi
	call	_ZN4node10cares_wrap12_GLOBAL__N_113ParseSrvReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	cmpl	$1, %eax
	ja	.L1582
	movl	$12, -7372(%rbp)
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	16(%r12), %rdi
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	subq	$8, %rsp
	movl	-7404(%rbp), %edx
	movl	%eax, %r15d
	movq	-7424(%rbp), %rcx
	pushq	$0
	movq	-7400(%rbp), %rsi
	call	_ZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_
	popq	%rax
	popq	%rdx
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1742:
	movq	16(%r12), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1738
.L1620:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	1880(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1739
.L1621:
	movq	16(%r12), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	528(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1740
.L1622:
	movq	%r14, %rcx
	movl	%r15d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1741
.L1623:
	addl	$1, %r15d
.L1624:
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r15d, %eax
	ja	.L1742
	movq	16(%r12), %rdi
	movl	-7404(%rbp), %edx
	movl	$1, %r8d
	movq	%rbx, %rcx
	movq	-7400(%rbp), %rsi
	call	_ZN4node10cares_wrap12_GLOBAL__N_115ParseNaptrReplyEPNS_11EnvironmentEPKhiN2v85LocalINS6_5ArrayEEEb
	cmpl	$1, %eax
	ja	.L1712
	movq	16(%r12), %r14
	leaq	-7264(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -7448(%rbp)
	movq	352(%r14), %rsi
	movq	%r14, -7440(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r14), %rax
	movl	-7404(%rbp), %edx
	leaq	-7360(%rbp), %rcx
	leaq	-7352(%rbp), %r8
	movq	%rax, -7464(%rbp)
	movq	-7400(%rbp), %rax
	movzwl	6(%rax), %esi
	leaq	12(%rax), %r14
	movq	%r14, %rdi
	movw	%si, -7392(%rbp)
	movq	%rax, %rsi
	call	ares_expand_name@PLT
	movl	%eax, %esi
	movq	-7360(%rbp), %rax
	movq	%rax, -7456(%rbp)
	testl	%esi, %esi
	jne	.L1743
	movq	-7352(%rbp), %rax
	leaq	4(%r14,%rax), %r15
	movslq	-7404(%rbp), %rax
	addq	-7400(%rbp), %rax
	movq	%rax, -7384(%rbp)
	cmpq	%rax, %r15
	ja	.L1627
	movzwl	-7392(%rbp), %eax
	rolw	$8, %ax
	movzwl	%ax, %esi
	movl	%esi, -7424(%rbp)
	testw	%ax, %ax
	je	.L1629
	leaq	-7336(%rbp), %rax
	xorl	%r14d, %r14d
	movq	%rax, -7416(%rbp)
	leaq	-7344(%rbp), %rax
	movq	%rax, -7392(%rbp)
	movl	%r14d, %eax
	movq	%r15, %r14
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	-7416(%rbp), %r8
	movl	-7404(%rbp), %edx
	movq	%r14, %rdi
	movq	-7392(%rbp), %rcx
	movq	-7400(%rbp), %rsi
	call	ares_expand_name@PLT
	movq	-7344(%rbp), %rdi
	testl	%eax, %eax
	jne	.L1744
	movq	-7336(%rbp), %r10
	addq	%r14, %r10
	leaq	10(%r10), %rdx
	cmpq	%rdx, -7384(%rbp)
	jb	.L1668
	movzwl	8(%r10), %eax
	rolw	$8, %ax
	cmpw	$1536, (%r10)
	je	.L1745
	movzwl	%ax, %eax
	leaq	(%rdx,%rax), %r14
	testq	%rdi, %rdi
	je	.L1652
	call	ares_free_string@PLT
	addl	$1, %r15d
	cmpl	%r15d, -7424(%rbp)
	jne	.L1654
.L1629:
	cmpq	$0, -7456(%rbp)
	je	.L1710
	movq	-7456(%rbp), %rdi
	call	ares_free_string@PLT
.L1710:
	movq	-7448(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
.L1659:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-7432(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1717:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	addl	$1, %r15d
	cmpl	%r15d, -7392(%rbp)
	jne	.L1601
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1716:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1715:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1597
	.p2align 4,,10
	.p2align 3
.L1725:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1724:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1723:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1608
	.p2align 4,,10
	.p2align 3
.L1722:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1721:
	movq	%rax, -7392(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-7392(%rbp), %rcx
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1713:
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1719:
	movl	%r15d, %esi
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEi
	jmp	.L1582
	.p2align 4,,10
	.p2align 3
.L1731:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	leaq	1(%r15), %rax
	cmpq	-7416(%rbp), %r15
	jne	.L1594
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1730:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1729:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1590
	.p2align 4,,10
	.p2align 3
.L1728:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1589
	.p2align 4,,10
	.p2align 3
.L1733:
	movq	%rax, -7384(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-7384(%rbp), %rcx
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1735:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1734:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1736:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1720:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1652:
	addl	$1, %r15d
	cmpl	%r15d, -7424(%rbp)
	jne	.L1654
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1603:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1741:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1623
.L1740:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1622
.L1739:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1621
.L1738:
	movq	%rax, -7384(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-7384(%rbp), %rcx
	jmp	.L1620
.L1718:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1743:
	movl	%esi, %r14d
	cmpl	$8, %esi
	je	.L1627
.L1628:
	movq	-7456(%rbp), %rdi
	movl	%esi, -7384(%rbp)
	testq	%rdi, %rdi
	je	.L1708
	call	ares_free_string@PLT
.L1708:
	movq	-7448(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	cmpl	$1, %r14d
	movl	-7384(%rbp), %esi
	jbe	.L1659
	jmp	.L1711
.L1627:
	movq	-7456(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1706
	call	ares_free_string@PLT
.L1706:
	movq	-7448(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movl	$10, %esi
	jmp	.L1711
.L1745:
	movq	%rdx, %r10
	movq	-7400(%rbp), %rsi
	movl	-7404(%rbp), %edx
	movq	%rdi, -7424(%rbp)
	leaq	-7328(%rbp), %rcx
	movq	%r10, %rdi
	leaq	-7320(%rbp), %r8
	movq	%r10, -7416(%rbp)
	call	ares_expand_name@PLT
	movq	-7416(%rbp), %r10
	movq	-7424(%rbp), %r9
	movl	%eax, %esi
	movq	-7328(%rbp), %rax
	testl	%esi, %esi
	movq	%rax, -7392(%rbp)
	je	.L1634
	cmpl	$8, %esi
	movl	$10, %eax
	cmove	%eax, %esi
.L1635:
	movq	-7392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1651
	movl	%esi, -7392(%rbp)
	movq	%r9, -7384(%rbp)
	call	ares_free_string@PLT
	movl	-7392(%rbp), %esi
	movq	-7384(%rbp), %r9
.L1651:
	movl	%esi, %r14d
.L1632:
	testq	%r9, %r9
	je	.L1628
	movq	%r9, %rdi
	movl	%esi, -7384(%rbp)
	call	ares_free_string@PLT
	movl	-7384(%rbp), %esi
	jmp	.L1628
.L1668:
	movq	%rdi, %r9
.L1705:
	movl	$10, %r14d
	movl	$10, %esi
	jmp	.L1632
.L1744:
	movq	%rdi, %r9
	movl	%eax, %esi
	movl	%eax, %r14d
	cmpl	$8, %eax
	jne	.L1632
	jmp	.L1705
.L1634:
	addq	-7320(%rbp), %r10
	movq	-7400(%rbp), %rsi
	leaq	-7312(%rbp), %rcx
	leaq	-7304(%rbp), %r8
	movl	-7404(%rbp), %edx
	movq	%r10, %rdi
	movq	%r9, -7416(%rbp)
	movq	%r10, %r14
	call	ares_expand_name@PLT
	movq	-7312(%rbp), %r15
	movq	-7416(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %esi
	jne	.L1746
	movq	-7304(%rbp), %rax
	movl	$10, %esi
	addq	%r14, %rax
	leaq	20(%rax), %rdx
	cmpq	%rdx, -7384(%rbp)
	jb	.L1637
	movl	(%rax), %edx
	movq	%r9, -7472(%rbp)
	bswap	%edx
	movl	%edx, -7384(%rbp)
	movl	4(%rax), %edx
	bswap	%edx
	movl	%edx, -7400(%rbp)
	movl	8(%rax), %edx
	bswap	%edx
	movl	%edx, -7404(%rbp)
	movl	12(%rax), %edx
	movl	16(%rax), %eax
	bswap	%edx
	movl	%edx, -7416(%rbp)
	bswap	%eax
	movl	%eax, -7424(%rbp)
	movq	-7440(%rbp), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	-7392(%rbp), %rsi
	orl	$-1, %edx
	movq	%rax, %r14
	movq	-7440(%rbp), %rax
	movq	352(%rax), %rdi
	call	_ZN4node13OneByteStringEPN2v87IsolateEPKci
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	-7440(%rbp), %rax
	movq	360(%rax), %rax
	movq	1096(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7472(%rbp), %r9
	testb	%al, %al
	je	.L1747
.L1638:
	movq	-7440(%rbp), %rax
	orl	$-1, %edx
	movq	%r15, %rsi
	movq	%r9, -7472(%rbp)
	movq	352(%rax), %rdi
	call	_ZN4node13OneByteStringEPN2v87IsolateEPKci
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	-7440(%rbp), %rax
	movq	360(%rax), %rax
	movq	856(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7472(%rbp), %r9
	testb	%al, %al
	je	.L1748
.L1639:
	movq	-7440(%rbp), %rax
	movl	-7384(%rbp), %esi
	movq	%r9, -7472(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	-7440(%rbp), %rax
	movq	360(%rax), %rax
	movq	1560(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7472(%rbp), %r9
	testb	%al, %al
	je	.L1749
.L1640:
	movq	-7440(%rbp), %rax
	movl	-7400(%rbp), %esi
	movq	%r9, -7384(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	-7440(%rbp), %rax
	movq	360(%rax), %rax
	movq	1488(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7384(%rbp), %r9
	testb	%al, %al
	je	.L1750
.L1641:
	movq	-7440(%rbp), %rax
	movl	-7404(%rbp), %esi
	movq	%r9, -7384(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	-7440(%rbp), %rax
	movq	360(%rax), %rax
	movq	1528(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7384(%rbp), %r9
	testb	%al, %al
	je	.L1751
.L1642:
	movq	-7440(%rbp), %rax
	movl	-7416(%rbp), %esi
	movq	%r9, -7384(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	-7440(%rbp), %rax
	movq	360(%rax), %rax
	movq	672(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7384(%rbp), %r9
	testb	%al, %al
	je	.L1752
.L1643:
	movq	-7440(%rbp), %rax
	movl	-7424(%rbp), %esi
	movq	%r9, -7384(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	-7440(%rbp), %rax
	movq	360(%rax), %rax
	movq	1032(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7384(%rbp), %r9
	testb	%al, %al
	je	.L1753
.L1644:
	movq	-7440(%rbp), %rax
	movq	-7464(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r9, -7384(%rbp)
	movq	360(%rax), %rax
	movq	536(%rax), %rcx
	movq	1768(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-7384(%rbp), %r9
	testb	%al, %al
	je	.L1754
.L1645:
	movq	-7448(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r9, -7384(%rbp)
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	testq	%r15, %r15
	movq	-7384(%rbp), %r9
	movq	%rax, %r14
	je	.L1646
	movq	%r15, %rdi
	call	ares_free_string@PLT
	movq	-7384(%rbp), %r9
.L1646:
	movq	-7392(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1647
	movq	%r9, -7384(%rbp)
	call	ares_free_string@PLT
	movq	-7384(%rbp), %r9
.L1647:
	testq	%r9, %r9
	je	.L1648
	movq	%r9, %rdi
	call	ares_free_string@PLT
.L1648:
	cmpq	$0, -7456(%rbp)
	je	.L1709
	movq	-7456(%rbp), %rdi
	call	ares_free_string@PLT
.L1709:
	movq	-7448(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	testq	%r14, %r14
	je	.L1659
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%eax, %edx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L1659
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1659
.L1746:
	cmpl	$8, %eax
	jne	.L1637
	movl	$10, %esi
.L1637:
	testq	%r15, %r15
	je	.L1635
	movq	%r15, %rdi
	movl	%esi, -7400(%rbp)
	movq	%r9, -7384(%rbp)
	call	ares_free_string@PLT
	movl	-7400(%rbp), %esi
	movq	-7384(%rbp), %r9
	jmp	.L1635
.L1727:
	call	__stack_chk_fail@PLT
.L1753:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7384(%rbp), %r9
	jmp	.L1644
.L1752:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7384(%rbp), %r9
	jmp	.L1643
.L1754:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7384(%rbp), %r9
	jmp	.L1645
.L1747:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7472(%rbp), %r9
	jmp	.L1638
.L1751:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7384(%rbp), %r9
	jmp	.L1642
.L1750:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7384(%rbp), %r9
	jmp	.L1641
.L1749:
	movq	%r9, -7384(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7384(%rbp), %r9
	jmp	.L1640
.L1748:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-7472(%rbp), %r9
	jmp	.L1639
	.cfi_endproc
.LFE7722:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhi
	.section	.text._ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE,"axG",@progbits,_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	.type	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE, @function
_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE:
.LFB6435:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	352(%rdi), %rdi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	movq	%r13, %rsi
	xorl	%r9d, %r9d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movq	%r12, %rsi
	movl	$1, %edx
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1758
.L1756:
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r12, %rsi
	movq	%r13, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	leaq	-32(%rbp), %rsp
	movq	%r12, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L1758:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1756
	.cfi_endproc
.LFE6435:
	.size	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE, .-_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	.section	.rodata.str1.1
.LC49:
	.string	"getaddrinfo"
.LC50:
	.string	"getnameinfo"
.LC51:
	.string	"canonicalizeIP"
.LC52:
	.string	"strerror"
.LC53:
	.string	"AF_INET"
.LC54:
	.string	"AF_INET6"
.LC55:
	.string	"AF_UNSPEC"
.LC56:
	.string	"AI_ADDRCONFIG"
.LC57:
	.string	"AI_ALL"
.LC58:
	.string	"AI_V4MAPPED"
.LC59:
	.string	"GetAddrInfoReqWrap"
.LC60:
	.string	"GetNameInfoReqWrap"
.LC61:
	.string	"QueryReqWrap"
.LC62:
	.string	"queryAny"
.LC63:
	.string	"queryA"
.LC64:
	.string	"queryAaaa"
.LC65:
	.string	"queryCname"
.LC66:
	.string	"queryMx"
.LC67:
	.string	"queryNs"
.LC68:
	.string	"queryTxt"
.LC69:
	.string	"querySrv"
.LC70:
	.string	"queryPtr"
.LC71:
	.string	"queryNaptr"
.LC72:
	.string	"querySoa"
.LC73:
	.string	"getHostByAddr"
.LC74:
	.string	"getServers"
.LC75:
	.string	"setServers"
.LC76:
	.string	"ChannelWrap"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB77:
	.text
.LHOTB77:
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L1760
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L1760
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L1760
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1802
.L1761:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC49(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1803
.L1762:
	movq	-56(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1804
.L1763:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1805
.L1764:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC50(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1806
.L1765:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L1807
.L1766:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L1808
.L1767:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC51(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1809
.L1768:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L1810
.L1769:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_18StrErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	movq	%rax, %r12
	popq	%rax
	testq	%r12, %r12
	je	.L1811
.L1770:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC52(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L1812
.L1771:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L1813
.L1772:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC53(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1814
.L1773:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1815
.L1774:
	movq	352(%rbx), %rdi
	movl	$10, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC54(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1816
.L1775:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1817
.L1776:
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC55(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1818
.L1777:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1819
.L1778:
	movq	352(%rbx), %rdi
	movl	$32, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$13, %ecx
	leaq	.LC56(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1820
.L1779:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1821
.L1780:
	movq	352(%rbx), %rdi
	movl	$16, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC57(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1822
.L1781:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1823
.L1782:
	movq	352(%rbx), %rdi
	movl	$8, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC58(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1824
.L1783:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1825
.L1784:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$18, %ecx
	leaq	.LC59(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1826
.L1785:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1827
.L1786:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1828
.L1787:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$18, %ecx
	leaq	.LC60(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1829
.L1788:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1830
.L1789:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1831
.L1790:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movl	$12, %ecx
	xorl	%edx, %edx
	leaq	.LC61(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1832
.L1791:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1833
.L1792:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1834
.L1793:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC62(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC63(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC64(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC65(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC66(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC67(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC68(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC69(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC70(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC71(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC72(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC73(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$1, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC74(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L1835
.L1794:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC75(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	leaq	.LC31(%rip), %rdx
	call	_ZN4node11Environment14SetProtoMethodEN2v85LocalINS1_16FunctionTemplateEEEPKcPFvRKNS1_20FunctionCallbackInfoINS1_5ValueEEEE
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC76(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1836
.L1795:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1837
.L1796:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1838
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1802:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1761
	.p2align 4,,10
	.p2align 3
.L1803:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1762
	.p2align 4,,10
	.p2align 3
.L1804:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1763
	.p2align 4,,10
	.p2align 3
.L1805:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1806:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L1765
	.p2align 4,,10
	.p2align 3
.L1807:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1808:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L1768
	.p2align 4,,10
	.p2align 3
.L1810:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L1769
	.p2align 4,,10
	.p2align 3
.L1811:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L1812:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L1771
	.p2align 4,,10
	.p2align 3
.L1813:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L1772
	.p2align 4,,10
	.p2align 3
.L1814:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1773
	.p2align 4,,10
	.p2align 3
.L1815:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1774
	.p2align 4,,10
	.p2align 3
.L1816:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L1817:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1776
	.p2align 4,,10
	.p2align 3
.L1818:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1777
	.p2align 4,,10
	.p2align 3
.L1819:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1778
	.p2align 4,,10
	.p2align 3
.L1820:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1779
	.p2align 4,,10
	.p2align 3
.L1821:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L1822:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1781
	.p2align 4,,10
	.p2align 3
.L1823:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1782
	.p2align 4,,10
	.p2align 3
.L1824:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1783
	.p2align 4,,10
	.p2align 3
.L1825:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1784
	.p2align 4,,10
	.p2align 3
.L1826:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1785
	.p2align 4,,10
	.p2align 3
.L1827:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1786
	.p2align 4,,10
	.p2align 3
.L1828:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1787
	.p2align 4,,10
	.p2align 3
.L1829:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1788
	.p2align 4,,10
	.p2align 3
.L1830:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1789
	.p2align 4,,10
	.p2align 3
.L1831:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1790
	.p2align 4,,10
	.p2align 3
.L1832:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1791
	.p2align 4,,10
	.p2align 3
.L1833:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1792
	.p2align 4,,10
	.p2align 3
.L1834:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1793
	.p2align 4,,10
	.p2align 3
.L1835:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1836:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1837:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L1796
	.p2align 4,,10
	.p2align 3
.L1838:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7834:
.L1760:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7834:
	.text
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE77:
	.text
.LHOTE77:
	.section	.text._ZN4node10BaseObject12pointer_dataEv,"axG",@progbits,_ZN4node10BaseObject12pointer_dataEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject12pointer_dataEv
	.type	_ZN4node10BaseObject12pointer_dataEv, @function
_ZN4node10BaseObject12pointer_dataEv:
.LFB7107:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L1847
	ret
	.p2align 4,,10
	.p2align 3
.L1847:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$24, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1842
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1841:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1842:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1841
	.cfi_endproc
.LFE7107:
	.size	_ZN4node10BaseObject12pointer_dataEv, .-_ZN4node10BaseObject12pointer_dataEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED0Ev:
.LFB11424:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	32(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1850
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L1860
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1861
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L1862
.L1850:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1855
	movq	(%rdi), %rax
	call	*8(%rax)
.L1855:
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1862:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L1854
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1860:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1854:
	cmpb	$0, 8(%rdx)
	je	.L1850
	cmpq	$0, 8(%r13)
	je	.L1850
	movq	%r13, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r13, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1850
	movq	8(%r13), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1861:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11424:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED2Ev:
.LFB11422:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	32(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1865
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1872
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1873
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1865
	cmpb	$0, 9(%rdx)
	je	.L1869
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1865:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L1863
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1863:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1872:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1869:
	cmpb	$0, 8(%rdx)
	je	.L1865
	cmpq	$0, 8(%r12)
	je	.L1865
	movq	%r12, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r12, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1865
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1865
	.p2align 4,,10
	.p2align 3
.L1873:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11422:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev:
.LFB7682:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	cmpq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	je	.L1915
	movq	80(%rdi), %rax
	movq	%rdi, %r12
	testq	%rax, %rax
	je	.L1876
	movq	$0, (%rax)
.L1876:
	movq	64(%r12), %r14
	testq	%r14, %r14
	je	.L1877
	movq	16(%r14), %rdi
	call	free@PLT
	movq	8(%r14), %r13
	testq	%r13, %r13
	je	.L1878
	movq	24(%r13), %r8
	testq	%r8, %r8
	je	.L1879
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1880
	movl	$8, %ebx
	.p2align 4,,10
	.p2align 3
.L1881:
	call	free@PLT
	movq	24(%r13), %r8
	movq	(%r8,%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L1881
.L1880:
	movq	%r8, %rdi
	call	free@PLT
	movq	$0, 24(%r13)
.L1879:
	movq	8(%r13), %r8
	testq	%r8, %r8
	je	.L1882
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L1883
	movl	$8, %ebx
	.p2align 4,,10
	.p2align 3
.L1884:
	call	free@PLT
	movq	8(%r13), %r8
	movq	(%r8,%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L1884
.L1883:
	movq	%r8, %rdi
	call	free@PLT
.L1882:
	movq	0(%r13), %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
.L1878:
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L1877:
	movq	56(%r12), %r13
	testq	%r13, %r13
	je	.L1886
	movq	24(%r13), %rdx
	testq	%rdx, %rdx
	je	.L1916
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1917
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1886
	cmpb	$0, 9(%rdx)
	je	.L1890
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
.L1886:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1915:
	.cfi_restore_state
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1890:
	cmpb	$0, 8(%rdx)
	je	.L1886
	cmpq	$0, 8(%r13)
	je	.L1886
	movq	%r13, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r13, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1886
	movq	8(%r13), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r13, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1886
	.p2align 4,,10
	.p2align 3
.L1916:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1917:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7682:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD2Ev:
.LFB10440:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10440:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD0Ev:
.LFB10442:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10442:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD0Ev
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8872:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1144, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1973
	cmpw	$1040, %cx
	jne	.L1922
.L1973:
	movq	23(%rdx), %rax
	movq	%r12, %rdi
	movq	%rax, -1176(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1993
.L1925:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1974
	cmpw	$1040, %cx
	jne	.L1926
.L1974:
	movq	23(%rdx), %r13
.L1928:
	testq	%r13, %r13
	je	.L1921
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1994
.L1930:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1994:
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1930
	cmpl	$5, 43(%rax)
	jne	.L1930
	movl	16(%rbx), %r9d
	testl	%r9d, %r9d
	jg	.L1932
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1933:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1995
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L1935
	movq	(%rbx), %rdx
	movq	8(%rdx), %r8
	leaq	88(%r8), %r14
.L1936:
	movq	(%r14), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1996
.L1937:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1937
	testl	%eax, %eax
	jg	.L1939
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
.L1940:
	movl	$88, %edi
	call	_Znwm@PLT
	movq	16(%r13), %rsi
	movq	%r15, %rdx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movl	$25, %ecx
	movq	%rax, %r12
	movq	%r13, %r15
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L1997
.L1941:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L1964
.L1944:
	leaq	.LC37(%rip), %rax
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapE(%rip), %r15
	movq	%r14, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	movq	-1176(%rbp), %rax
	leaq	-1136(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%r15, (%r12)
	movq	352(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L1957
	movq	-1120(%rbp), %rcx
	leaq	-80(%rbp), %r14
	movl	$2, %edi
	movq	%r14, %rdx
	movq	%rcx, %rsi
	movq	%rcx, -1176(%rbp)
	call	uv_inet_pton@PLT
	movq	-1176(%rbp), %rcx
	testl	%eax, %eax
	je	.L1969
	movq	%r14, %rdx
	movq	%rcx, %rsi
	movl	$10, %edi
	call	uv_inet_pton@PLT
	testl	%eax, %eax
	jne	.L1947
	movq	-1176(%rbp), %rcx
	movl	$10, %r13d
	movl	$16, %r15d
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762(%rip), %rsi
	testq	%rsi, %rsi
	je	.L1998
.L1949:
	testb	$5, (%rsi)
	je	.L1951
	cmpl	$2, %r13d
	leaq	.LC34(%rip), %rdx
	leaq	.LC33(%rip), %rax
	movq	%rcx, -1152(%rbp)
	cmovne	%rdx, %rax
	subq	$8, %rsp
	movq	%r12, %rcx
	leaq	.LC36(%rip), %rdx
	leaq	.LC35(%rip), %rdi
	movq	%rax, -1144(%rbp)
	leaq	-1152(%rbp), %rax
	movq	%rdx, %xmm1
	movq	%rdi, %xmm0
	pushq	%rax
	movl	$1543, %edx
	punpcklqdq	%xmm1, %xmm0
	leaq	-82(%rbp), %r9
	movw	%dx, -82(%rbp)
	leaq	-1168(%rbp), %r8
	leaq	.LC37(%rip), %rdx
	movl	$98, %edi
	movaps	%xmm0, -1168(%rbp)
	call	_ZN4node7tracingL17AddTraceEventImplEcPKhPKcS4_mmiPS4_S2_PKmj.constprop.0
	popq	%rcx
	popq	%rsi
.L1951:
	cmpq	$0, 80(%r12)
	jne	.L1999
	movl	$8, %edi
	call	_Znwm@PLT
	movl	%r13d, %ecx
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r12, (%rax)
	movq	%rax, %r9
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiP7hostent(%rip), %r8
	movq	%rax, 80(%r12)
	movq	56(%r12), %rax
	movq	64(%rax), %rdi
	call	ares_gethostbyaddr@PLT
	movq	(%rbx), %rax
	movq	-1120(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1921
	testq	%rdi, %rdi
	je	.L1921
	call	free@PLT
.L1921:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2000
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1922:
	.cfi_restore_state
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, -1176(%rbp)
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L1925
	.p2align 4,,10
	.p2align 3
.L1993:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1926:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1928
	.p2align 4,,10
	.p2align 3
.L1932:
	movq	8(%rbx), %rdi
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1935:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r14
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1939:
	movq	8(%rbx), %r15
	jmp	.L1940
	.p2align 4,,10
	.p2align 3
.L1997:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%r8d, %r8d
	movq	$0, (%rax)
	movw	%r8w, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1967
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1942:
	movq	56(%r12), %r15
	movb	%dl, 8(%rax)
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%r15), %rax
	testq	%rax, %rax
	jne	.L1941
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r15), %rdx
	xorl	%edi, %edi
	movq	$0, (%rax)
	movw	%di, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1968
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1943:
	movb	%dl, 8(%rax)
	movq	%r15, 16(%rax)
	movq	%rax, 24(%r15)
	movl	$1, (%rax)
.L1964:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1944
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L1944
	.p2align 4,,10
	.p2align 3
.L1969:
	movl	$2, %r13d
	movl	$4, %r15d
	movq	_ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762(%rip), %rsi
	testq	%rsi, %rsi
	jne	.L1949
.L1998:
	movq	%rcx, -1176(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	-1176(%rbp), %rcx
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1950
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rsi
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2001
.L1950:
	movq	%rsi, _ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762(%rip)
	mfence
	jmp	.L1949
.L1947:
	subl	$1, 80(%r13)
	js	.L1957
	movq	(%rbx), %rax
	movl	$2147483637, %ebx
	movq	-1120(%rbp), %rdi
	salq	$33, %rbx
	movq	%rbx, 24(%rax)
	leaq	-1112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1960
	testq	%rdi, %rdi
	je	.L1960
	call	free@PLT
.L1960:
	movq	%r15, (%r12)
	movq	%r12, %rdi
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	movl	$88, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L1995:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1957:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1999:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap19MakeCallbackPointerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2001:
	leaq	.LC4(%rip), %rsi
	call	*%rax
	movq	-1176(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L1950
	.p2align 4,,10
	.p2align 3
.L1967:
	xorl	%edx, %edx
	jmp	.L1942
	.p2align 4,,10
	.p2align 3
.L1968:
	xorl	%edx, %edx
	jmp	.L1943
.L2000:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8872:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD2Ev:
.LFB10423:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10423:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD0Ev:
.LFB10425:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10425:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD0Ev
	.section	.rodata.str1.1
.LC78:
	.string	"resolveSoa"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8869:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2036
	cmpw	$1040, %cx
	jne	.L2006
.L2036:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2051
.L2009:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2037
	cmpw	$1040, %cx
	jne	.L2010
.L2037:
	movq	23(%rdx), %r13
.L2012:
	testq	%r13, %r13
	je	.L2005
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2052
.L2014:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2021
	testl	%eax, %eax
	jg	.L2023
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2024:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2053
.L2025:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2031
.L2028:
	leaq	.LC78(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2054
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$6, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2005
	testq	%rdi, %rdi
	je	.L2005
	call	free@PLT
.L2005:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2055
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2052:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2014
	cmpl	$5, 43(%rax)
	jne	.L2014
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2016
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2017:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2056
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2019
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2020:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2057
.L2021:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2006:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2009
	.p2align 4,,10
	.p2align 3
.L2051:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2010:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2012
	.p2align 4,,10
	.p2align 3
.L2016:
	movq	8(%rbx), %rdi
	jmp	.L2017
	.p2align 4,,10
	.p2align 3
.L2019:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2020
	.p2align 4,,10
	.p2align 3
.L2023:
	movq	8(%rbx), %rdx
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2053:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2034
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2026:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2025
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2035
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2027:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2031:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2028
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2028
	.p2align 4,,10
	.p2align 3
.L2056:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2054:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2034:
	xorl	%edx, %edx
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L2035:
	xorl	%ecx, %ecx
	jmp	.L2027
.L2055:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8869:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD2Ev:
.LFB10406:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10406:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD0Ev:
.LFB10408:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10408:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD0Ev
	.section	.rodata.str1.1
.LC79:
	.string	"resolveNaptr"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8866:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2092
	cmpw	$1040, %cx
	jne	.L2062
.L2092:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2107
.L2065:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2093
	cmpw	$1040, %cx
	jne	.L2066
.L2093:
	movq	23(%rdx), %r13
.L2068:
	testq	%r13, %r13
	je	.L2061
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2108
.L2070:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2077
	testl	%eax, %eax
	jg	.L2079
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2080:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2109
.L2081:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2087
.L2084:
	leaq	.LC79(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2110
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$35, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2061
	testq	%rdi, %rdi
	je	.L2061
	call	free@PLT
.L2061:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2111
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2108:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2070
	cmpl	$5, 43(%rax)
	jne	.L2070
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2072
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2073:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2112
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2075
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2076:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2113
.L2077:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2062:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2065
	.p2align 4,,10
	.p2align 3
.L2107:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2066:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2072:
	movq	8(%rbx), %rdi
	jmp	.L2073
	.p2align 4,,10
	.p2align 3
.L2075:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2076
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	8(%rbx), %rdx
	jmp	.L2080
	.p2align 4,,10
	.p2align 3
.L2109:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2090
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2082:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2081
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2091
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2083:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2087:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2084
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2084
	.p2align 4,,10
	.p2align 3
.L2112:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2110:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2090:
	xorl	%edx, %edx
	jmp	.L2082
	.p2align 4,,10
	.p2align 3
.L2091:
	xorl	%ecx, %ecx
	jmp	.L2083
.L2111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8866:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD2Ev:
.LFB10389:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10389:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD0Ev:
.LFB10391:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10391:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD0Ev
	.section	.rodata.str1.1
.LC80:
	.string	"resolvePtr"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8863:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2148
	cmpw	$1040, %cx
	jne	.L2118
.L2148:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2163
.L2121:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2149
	cmpw	$1040, %cx
	jne	.L2122
.L2149:
	movq	23(%rdx), %r13
.L2124:
	testq	%r13, %r13
	je	.L2117
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2164
.L2126:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2169:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2133
	testl	%eax, %eax
	jg	.L2135
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2136:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2165
.L2137:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2143
.L2140:
	leaq	.LC80(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2166
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$12, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2117
	testq	%rdi, %rdi
	je	.L2117
	call	free@PLT
.L2117:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2167
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2164:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2126
	cmpl	$5, 43(%rax)
	jne	.L2126
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2128
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2129:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2168
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2131
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2132:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2169
.L2133:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2118:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2121
	.p2align 4,,10
	.p2align 3
.L2163:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2122:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2124
	.p2align 4,,10
	.p2align 3
.L2128:
	movq	8(%rbx), %rdi
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2131:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2132
	.p2align 4,,10
	.p2align 3
.L2135:
	movq	8(%rbx), %rdx
	jmp	.L2136
	.p2align 4,,10
	.p2align 3
.L2165:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2146
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2138:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2137
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2147
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2139:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2143:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2140
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2168:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2166:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2146:
	xorl	%edx, %edx
	jmp	.L2138
	.p2align 4,,10
	.p2align 3
.L2147:
	xorl	%ecx, %ecx
	jmp	.L2139
.L2167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8863:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD2Ev:
.LFB10372:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10372:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD0Ev:
.LFB10374:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10374:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD0Ev
	.section	.rodata.str1.1
.LC81:
	.string	"resolveSrv"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2204
	cmpw	$1040, %cx
	jne	.L2174
.L2204:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2219
.L2177:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2205
	cmpw	$1040, %cx
	jne	.L2178
.L2205:
	movq	23(%rdx), %r13
.L2180:
	testq	%r13, %r13
	je	.L2173
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2220
.L2182:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2225:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2189
	testl	%eax, %eax
	jg	.L2191
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2192:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2221
.L2193:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2199
.L2196:
	leaq	.LC81(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2222
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$33, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2173
	testq	%rdi, %rdi
	je	.L2173
	call	free@PLT
.L2173:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2223
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2220:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2182
	cmpl	$5, 43(%rax)
	jne	.L2182
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2184
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2185:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2224
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2187
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2188:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2225
.L2189:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2174:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2177
	.p2align 4,,10
	.p2align 3
.L2219:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2178:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2180
	.p2align 4,,10
	.p2align 3
.L2184:
	movq	8(%rbx), %rdi
	jmp	.L2185
	.p2align 4,,10
	.p2align 3
.L2187:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2188
	.p2align 4,,10
	.p2align 3
.L2191:
	movq	8(%rbx), %rdx
	jmp	.L2192
	.p2align 4,,10
	.p2align 3
.L2221:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2202
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2194:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2193
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2203
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2195:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2199:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2196
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2196
	.p2align 4,,10
	.p2align 3
.L2224:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2222:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2202:
	xorl	%edx, %edx
	jmp	.L2194
	.p2align 4,,10
	.p2align 3
.L2203:
	xorl	%ecx, %ecx
	jmp	.L2195
.L2223:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8860:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD2Ev:
.LFB10355:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10355:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD0Ev:
.LFB10357:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10357:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD0Ev
	.section	.rodata.str1.1
.LC82:
	.string	"resolveTxt"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8857:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2260
	cmpw	$1040, %cx
	jne	.L2230
.L2260:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2275
.L2233:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2261
	cmpw	$1040, %cx
	jne	.L2234
.L2261:
	movq	23(%rdx), %r13
.L2236:
	testq	%r13, %r13
	je	.L2229
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2276
.L2238:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2281:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2245
	testl	%eax, %eax
	jg	.L2247
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2248:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2277
.L2249:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2255
.L2252:
	leaq	.LC82(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2278
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$16, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2229
	testq	%rdi, %rdi
	je	.L2229
	call	free@PLT
.L2229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2279
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2276:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2238
	cmpl	$5, 43(%rax)
	jne	.L2238
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2240
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2241:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2280
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2243
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2244:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2281
.L2245:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2230:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2233
	.p2align 4,,10
	.p2align 3
.L2275:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2234:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2236
	.p2align 4,,10
	.p2align 3
.L2240:
	movq	8(%rbx), %rdi
	jmp	.L2241
	.p2align 4,,10
	.p2align 3
.L2243:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2244
	.p2align 4,,10
	.p2align 3
.L2247:
	movq	8(%rbx), %rdx
	jmp	.L2248
	.p2align 4,,10
	.p2align 3
.L2277:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2258
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2250:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2249
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2259
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2251:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2255:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2252
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2252
	.p2align 4,,10
	.p2align 3
.L2280:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2278:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2258:
	xorl	%edx, %edx
	jmp	.L2250
	.p2align 4,,10
	.p2align 3
.L2259:
	xorl	%ecx, %ecx
	jmp	.L2251
.L2279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8857:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD2Ev:
.LFB10338:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10338:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD0Ev:
.LFB10340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10340:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD0Ev
	.section	.rodata.str1.1
.LC83:
	.string	"resolveNs"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2316
	cmpw	$1040, %cx
	jne	.L2286
.L2316:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2331
.L2289:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2317
	cmpw	$1040, %cx
	jne	.L2290
.L2317:
	movq	23(%rdx), %r13
.L2292:
	testq	%r13, %r13
	je	.L2285
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2332
.L2294:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2337:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2301
	testl	%eax, %eax
	jg	.L2303
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2304:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2333
.L2305:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2311
.L2308:
	leaq	.LC83(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2334
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$2, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2285
	testq	%rdi, %rdi
	je	.L2285
	call	free@PLT
.L2285:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2335
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2332:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2294
	cmpl	$5, 43(%rax)
	jne	.L2294
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2296
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2297:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2336
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2299
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2300:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2337
.L2301:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2286:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2289
	.p2align 4,,10
	.p2align 3
.L2331:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2290:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2292
	.p2align 4,,10
	.p2align 3
.L2296:
	movq	8(%rbx), %rdi
	jmp	.L2297
	.p2align 4,,10
	.p2align 3
.L2299:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2300
	.p2align 4,,10
	.p2align 3
.L2303:
	movq	8(%rbx), %rdx
	jmp	.L2304
	.p2align 4,,10
	.p2align 3
.L2333:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2314
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2306:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2305
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2315
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2307:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2311:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2308
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2308
	.p2align 4,,10
	.p2align 3
.L2336:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2334:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2314:
	xorl	%edx, %edx
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2315:
	xorl	%ecx, %ecx
	jmp	.L2307
.L2335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8854:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD2Ev:
.LFB10321:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10321:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD0Ev:
.LFB10323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10323:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD0Ev
	.section	.rodata.str1.1
.LC84:
	.string	"resolveMx"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2372
	cmpw	$1040, %cx
	jne	.L2342
.L2372:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2387
.L2345:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2373
	cmpw	$1040, %cx
	jne	.L2346
.L2373:
	movq	23(%rdx), %r13
.L2348:
	testq	%r13, %r13
	je	.L2341
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2388
.L2350:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2393:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2357
	testl	%eax, %eax
	jg	.L2359
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2360:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2389
.L2361:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2367
.L2364:
	leaq	.LC84(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2390
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$15, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2341
	testq	%rdi, %rdi
	je	.L2341
	call	free@PLT
.L2341:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2391
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2388:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2350
	cmpl	$5, 43(%rax)
	jne	.L2350
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2352
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2353:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2392
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2355
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2356:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2393
.L2357:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2342:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2345
	.p2align 4,,10
	.p2align 3
.L2387:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2346:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2348
	.p2align 4,,10
	.p2align 3
.L2352:
	movq	8(%rbx), %rdi
	jmp	.L2353
	.p2align 4,,10
	.p2align 3
.L2355:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2356
	.p2align 4,,10
	.p2align 3
.L2359:
	movq	8(%rbx), %rdx
	jmp	.L2360
	.p2align 4,,10
	.p2align 3
.L2389:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2370
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2362:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2361
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2371
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2363:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2367:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2364
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2364
	.p2align 4,,10
	.p2align 3
.L2392:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2390:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2370:
	xorl	%edx, %edx
	jmp	.L2362
	.p2align 4,,10
	.p2align 3
.L2371:
	xorl	%ecx, %ecx
	jmp	.L2363
.L2391:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8851:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD2Ev:
.LFB10304:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10304:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD0Ev:
.LFB10306:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10306:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD0Ev
	.section	.rodata.str1.1
.LC85:
	.string	"resolveCname"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8848:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2428
	cmpw	$1040, %cx
	jne	.L2398
.L2428:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2443
.L2401:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2429
	cmpw	$1040, %cx
	jne	.L2402
.L2429:
	movq	23(%rdx), %r13
.L2404:
	testq	%r13, %r13
	je	.L2397
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2444
.L2406:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2449:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2413
	testl	%eax, %eax
	jg	.L2415
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2416:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2445
.L2417:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2423
.L2420:
	leaq	.LC85(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2446
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$5, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2397
	testq	%rdi, %rdi
	je	.L2397
	call	free@PLT
.L2397:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2447
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2444:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2406
	cmpl	$5, 43(%rax)
	jne	.L2406
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2408
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2409:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2448
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2411
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2412:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2449
.L2413:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2398:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2401
	.p2align 4,,10
	.p2align 3
.L2443:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2402:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2404
	.p2align 4,,10
	.p2align 3
.L2408:
	movq	8(%rbx), %rdi
	jmp	.L2409
	.p2align 4,,10
	.p2align 3
.L2411:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2415:
	movq	8(%rbx), %rdx
	jmp	.L2416
	.p2align 4,,10
	.p2align 3
.L2445:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2426
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2418:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2417
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2427
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2419:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2423:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2420
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2420
	.p2align 4,,10
	.p2align 3
.L2448:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2446:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2426:
	xorl	%edx, %edx
	jmp	.L2418
	.p2align 4,,10
	.p2align 3
.L2427:
	xorl	%ecx, %ecx
	jmp	.L2419
.L2447:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8848:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD2Ev:
.LFB10287:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10287:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD0Ev:
.LFB10289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10289:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD0Ev
	.section	.rodata.str1.1
.LC86:
	.string	"resolve6"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8845:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2484
	cmpw	$1040, %cx
	jne	.L2454
.L2484:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2499
.L2457:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2485
	cmpw	$1040, %cx
	jne	.L2458
.L2485:
	movq	23(%rdx), %r13
.L2460:
	testq	%r13, %r13
	je	.L2453
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2500
.L2462:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2469
	testl	%eax, %eax
	jg	.L2471
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2472:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2501
.L2473:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2479
.L2476:
	leaq	.LC86(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2502
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$28, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2453
	testq	%rdi, %rdi
	je	.L2453
	call	free@PLT
.L2453:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2503
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2500:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2462
	cmpl	$5, 43(%rax)
	jne	.L2462
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2464
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2465:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2504
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2467
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2468:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2505
.L2469:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2454:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2457
	.p2align 4,,10
	.p2align 3
.L2499:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2458:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2460
	.p2align 4,,10
	.p2align 3
.L2464:
	movq	8(%rbx), %rdi
	jmp	.L2465
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2468
	.p2align 4,,10
	.p2align 3
.L2471:
	movq	8(%rbx), %rdx
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2501:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2482
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2474:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2473
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2483
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2475:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2479:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2476
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2476
	.p2align 4,,10
	.p2align 3
.L2504:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2502:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2482:
	xorl	%edx, %edx
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2483:
	xorl	%ecx, %ecx
	jmp	.L2475
.L2503:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8845:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD2Ev:
.LFB10270:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_110QueryAWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10270:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD0Ev:
.LFB10272:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_110QueryAWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10272:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD0Ev
	.section	.rodata.str1.1
.LC87:
	.string	"resolve4"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8842:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2540
	cmpw	$1040, %cx
	jne	.L2510
.L2540:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2555
.L2513:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2541
	cmpw	$1040, %cx
	jne	.L2514
.L2541:
	movq	23(%rdx), %r13
.L2516:
	testq	%r13, %r13
	je	.L2509
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2556
.L2518:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2561:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2525
	testl	%eax, %eax
	jg	.L2527
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2528:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2557
.L2529:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2535
.L2532:
	leaq	.LC87(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_110QueryAWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2558
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2509
	testq	%rdi, %rdi
	je	.L2509
	call	free@PLT
.L2509:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2559
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2556:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2518
	cmpl	$5, 43(%rax)
	jne	.L2518
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2520
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2521:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2560
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2523
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2524:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2561
.L2525:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2510:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2513
	.p2align 4,,10
	.p2align 3
.L2555:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2514:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2516
	.p2align 4,,10
	.p2align 3
.L2520:
	movq	8(%rbx), %rdi
	jmp	.L2521
	.p2align 4,,10
	.p2align 3
.L2523:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2524
	.p2align 4,,10
	.p2align 3
.L2527:
	movq	8(%rbx), %rdx
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2557:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2538
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2530:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2529
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2539
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2531:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2535:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2532
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2532
	.p2align 4,,10
	.p2align 3
.L2560:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2558:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2538:
	xorl	%edx, %edx
	jmp	.L2530
	.p2align 4,,10
	.p2align 3
.L2539:
	xorl	%ecx, %ecx
	jmp	.L2531
.L2559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8842:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD2Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD2Ev:
.LFB10253:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	.cfi_endproc
.LFE10253:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD2Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD2Ev
	.set	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD1Ev,_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD0Ev, @function
_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD0Ev:
.LFB10255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$88, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10255:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD0Ev, .-_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD0Ev
	.section	.rodata.str1.1
.LC88:
	.string	"resolveAny"
	.text
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB8839:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1080, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2596
	cmpw	$1040, %cx
	jne	.L2566
.L2596:
	movq	%r12, %rdi
	movq	23(%rdx), %r14
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2611
.L2569:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2597
	cmpw	$1040, %cx
	jne	.L2570
.L2597:
	movq	23(%rdx), %r13
.L2572:
	testq	%r13, %r13
	je	.L2565
	movq	(%rbx), %rdx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2612
.L2574:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2617:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L2581
	testl	%eax, %eax
	jg	.L2583
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2584:
	movl	$88, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	movq	-1112(%rbp), %rdx
	movq	16(%r13), %rsi
	movl	$25, %ecx
	movsd	.LC38(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE(%rip), %rax
	movq	%r13, 56(%r12)
	movq	%r13, %rdx
	movq	%rax, (%r12)
	movq	24(%r13), %rax
	testq	%rax, %rax
	je	.L2613
.L2585:
	movl	(%rax), %ecx
	leal	1(%rcx), %esi
	movl	%esi, (%rax)
	testl	%ecx, %ecx
	je	.L2591
.L2588:
	leaq	.LC88(%rip), %rax
	movq	352(%r14), %rsi
	movq	%r15, %rdx
	movq	$0, 64(%r12)
	movq	%rax, 72(%r12)
	leaq	16+_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapE(%rip), %rax
	leaq	-1104(%rbp), %rdi
	movq	$0, 80(%r12)
	movq	%rax, (%r12)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	addl	$1, 80(%r13)
	js	.L2614
	movq	-1088(%rbp), %rsi
	movq	%r12, %rdi
	movl	$255, %edx
	call	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKcii.constprop.0
	movq	(%rbx), %rax
	movq	-1088(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2565
	testq	%rdi, %rdi
	je	.L2565
	call	free@PLT
.L2565:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2615
	addq	$1080, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2612:
	.cfi_restore_state
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2574
	cmpl	$5, 43(%rax)
	jne	.L2574
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jg	.L2576
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2577:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2616
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L2579
	movq	(%rbx), %rdx
	movq	8(%rdx), %r15
	addq	$88, %r15
.L2580:
	movq	(%r15), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L2617
.L2581:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2566:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r14
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L2569
	.p2align 4,,10
	.p2align 3
.L2611:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2570:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L2572
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	8(%rbx), %rdi
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2579:
	movq	8(%rbx), %rsi
	leaq	-8(%rsi), %r15
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2583:
	movq	8(%rbx), %rdx
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L2613:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2594
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2586:
	movb	%dl, 8(%rax)
	movq	56(%r12), %rdx
	movq	%r13, 16(%rax)
	movq	%rax, 24(%r13)
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L2585
	movl	$24, %edi
	movq	%rdx, -1112(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	-1112(%rbp), %rdx
	movq	$0, (%rax)
	movq	8(%rdx), %rcx
	movq	$0, 16(%rax)
	testq	%rcx, %rcx
	je	.L2595
	movzbl	11(%rcx), %ecx
	andl	$7, %ecx
	cmpb	$2, %cl
	sete	%cl
.L2587:
	movb	%cl, 8(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
	movl	$1, (%rax)
.L2591:
	movq	8(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L2588
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2616:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2614:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2594:
	xorl	%edx, %edx
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L2595:
	xorl	%ecx, %ecx
	jmp	.L2587
.L2615:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8839:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiPhi, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiPhi:
.LFB7693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	testq	%r12, %r12
	je	.L2689
	movl	%r8d, %r14d
	movl	%esi, %ebx
	movl	$8, %esi
	movq	%rcx, %r13
	movq	$0, 80(%r12)
	xorl	%r15d, %r15d
	call	_ZdlPvm@PLT
	movslq	%r14d, %rax
	movq	%rax, -56(%rbp)
	testl	%ebx, %ebx
	je	.L2690
.L2620:
	movl	$32, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	64(%r12), %r13
	movq	%rax, %r14
	movl	$0, (%rax)
	movb	$0, 4(%rax)
	movq	$0, 24(%rax)
	movq	%rax, 64(%r12)
	testq	%r13, %r13
	je	.L2622
	movq	16(%r13), %rdi
	call	free@PLT
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.L2623
	movq	24(%r14), %r8
	testq	%r8, %r8
	je	.L2624
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L2625
	movl	$8, %eax
	.p2align 4,,10
	.p2align 3
.L2626:
	movq	%rax, -64(%rbp)
	call	free@PLT
	movq	-64(%rbp), %rax
	movq	24(%r14), %r8
	movq	(%r8,%rax), %rdi
	addq	$8, %rax
	testq	%rdi, %rdi
	jne	.L2626
.L2625:
	movq	%r8, %rdi
	call	free@PLT
	movq	$0, 24(%r14)
.L2624:
	movq	8(%r14), %r8
	testq	%r8, %r8
	je	.L2627
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L2628
	movl	$8, %eax
	.p2align 4,,10
	.p2align 3
.L2629:
	movq	%rax, -64(%rbp)
	call	free@PLT
	movq	-64(%rbp), %rax
	movq	8(%r14), %r8
	movq	(%r8,%rax), %rdi
	addq	$8, %rax
	testq	%rdi, %rdi
	jne	.L2629
.L2628:
	movq	%r8, %rdi
	call	free@PLT
.L2627:
	movq	(%r14), %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	free@PLT
.L2623:
	movq	%r13, %rdi
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	64(%r12), %r14
	movq	16(%r14), %r13
.L2622:
	movl	%ebx, (%r14)
	movq	%r13, %rdi
	movb	$0, 4(%r14)
	call	free@PLT
	movq	-56(%rbp), %rax
	movq	%r15, 16(%r14)
	movq	%rax, 24(%r14)
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L2691
.L2630:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L2692
.L2632:
	movq	16(%r12), %r13
.L2648:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L2693
.L2635:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	2480(%r13), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_EE(%rip), %rsi
	movb	$1, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rsi, (%rax)
	movq	%r12, 24(%rax)
	movq	%r12, 32(%rax)
	lock addq	$1, 2464(%r13)
	movq	%rax, 2480(%r13)
	testq	%rdx, %rdx
	je	.L2636
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L2638
.L2688:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2638:
	movq	1312(%r13), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L2694
.L2640:
	addl	$1, %eax
	cmpl	$11, %ebx
	movl	%eax, 4(%rdx)
	movq	56(%r12), %rdx
	setne	72(%rdx)
	subl	$1, 80(%rdx)
	js	.L2695
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2696
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L2697
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L2618
	cmpb	$0, 9(%rdx)
	jne	.L2698
	cmpb	$0, 8(%rdx)
	je	.L2618
	cmpq	$0, 8(%r12)
	je	.L2618
	movq	%r12, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r12, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2618
	movq	8(%r12), %rdi
	addq	$24, %rsp
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	popq	%rbx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L2692:
	.cfi_restore_state
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2632
	call	_ZN2v82V89ClearWeakEPm@PLT
	movq	24(%r12), %rax
	movq	16(%r12), %r13
	testq	%rax, %rax
	jne	.L2648
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2651
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2634:
	movb	%dl, 8(%rax)
	movl	(%rax), %edx
	movq	%r12, 16(%rax)
	leal	1(%rdx), %ecx
	movq	%rax, 24(%r12)
	movl	%ecx, (%rax)
	testl	%edx, %edx
	jne	.L2635
.L2693:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2635
	call	_ZN2v82V89ClearWeakEPm@PLT
	jmp	.L2635
	.p2align 4,,10
	.p2align 3
.L2690:
	testq	%rax, %rax
	movl	$1, %r14d
	cmovne	%rax, %r14
	movq	%r14, %rdi
	call	malloc@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2699
.L2621:
	movq	-56(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	jmp	.L2620
	.p2align 4,,10
	.p2align 3
.L2618:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2694:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%r13), %rdx
	movl	4(%rdx), %eax
	jmp	.L2640
	.p2align 4,,10
	.p2align 3
.L2636:
	movq	2472(%r13), %rdi
	movq	%rax, 2472(%r13)
	testq	%rdi, %rdi
	jne	.L2688
	jmp	.L2638
	.p2align 4,,10
	.p2align 3
.L2691:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r12), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2650
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2631:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
	jmp	.L2630
	.p2align 4,,10
	.p2align 3
.L2689:
	addq	$24, %rsp
	movl	$8, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2698:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2696:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2695:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2699:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r14, %rdi
	call	malloc@PLT
	cmpq	$0, -56(%rbp)
	movq	%rax, %r15
	je	.L2621
	testq	%rax, %rax
	jne	.L2621
	leaq	_ZZN4node6MallocIhEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2650:
	xorl	%edx, %edx
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2697:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2651:
	xorl	%edx, %edx
	jmp	.L2634
	.cfi_endproc
.LFE7693:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiPhi, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiPhi
	.p2align 4
	.type	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiP7hostent, @function
_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiP7hostent:
.LFB7694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movl	%esi, -52(%rbp)
	testq	%r12, %r12
	je	.L2846
	movq	$0, 80(%r12)
	movl	$8, %esi
	movq	%rcx, %rbx
	xorl	%r14d, %r14d
	call	_ZdlPvm@PLT
	movl	-52(%rbp), %esi
	testl	%esi, %esi
	je	.L2847
.L2702:
	movl	$32, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	64(%r12), %r15
	movq	%rax, %rbx
	movl	$0, (%rax)
	movb	$0, 4(%rax)
	movq	$0, 24(%rax)
	movq	%rax, 64(%r12)
	testq	%r15, %r15
	je	.L2848
	movq	16(%r15), %rdi
	call	free@PLT
	movq	8(%r15), %r13
	testq	%r13, %r13
	je	.L2726
	movq	24(%r13), %r8
	testq	%r8, %r8
	je	.L2727
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L2728
	movl	$8, %ebx
	.p2align 4,,10
	.p2align 3
.L2729:
	call	free@PLT
	movq	24(%r13), %r8
	movq	(%r8,%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L2729
.L2728:
	movq	%r8, %rdi
	call	free@PLT
	movq	$0, 24(%r13)
.L2727:
	movq	8(%r13), %r8
	testq	%r8, %r8
	je	.L2730
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L2731
	movl	$8, %ebx
	.p2align 4,,10
	.p2align 3
.L2732:
	call	free@PLT
	movq	8(%r13), %r8
	movq	(%r8,%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L2732
.L2731:
	movq	%r8, %rdi
	call	free@PLT
.L2730:
	movq	0(%r13), %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
.L2726:
	movq	%r15, %rdi
	movl	$32, %esi
	call	_ZdlPvm@PLT
	movq	64(%r12), %rbx
	movl	-52(%rbp), %eax
	movq	8(%rbx), %r15
	movl	%eax, (%rbx)
	movq	%r14, 8(%rbx)
	testq	%r15, %r15
	je	.L2725
	movq	24(%r15), %r8
	testq	%r8, %r8
	je	.L2733
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L2734
	movl	$8, %r14d
	.p2align 4,,10
	.p2align 3
.L2735:
	call	free@PLT
	movq	24(%r15), %r8
	movq	(%r8,%r14), %rdi
	addq	$8, %r14
	testq	%rdi, %rdi
	jne	.L2735
.L2734:
	movq	%r8, %rdi
	call	free@PLT
	movq	$0, 24(%r15)
.L2733:
	movq	8(%r15), %r8
	testq	%r8, %r8
	je	.L2736
	movq	(%r8), %rdi
	testq	%rdi, %rdi
	je	.L2737
	movl	$8, %r14d
	.p2align 4,,10
	.p2align 3
.L2738:
	call	free@PLT
	movq	8(%r15), %r8
	movq	(%r8,%r14), %rdi
	addq	$8, %r14
	testq	%rdi, %rdi
	jne	.L2738
.L2737:
	movq	%r8, %rdi
	call	free@PLT
.L2736:
	movq	(%r15), %rdi
	call	free@PLT
	movq	%r15, %rdi
	call	free@PLT
	movq	24(%r12), %rax
	movb	$1, 4(%rbx)
	testq	%rax, %rax
	jne	.L2739
.L2855:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%r12), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2767
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2740:
	movb	%dl, 8(%rax)
	movl	(%rax), %edx
	movq	%r12, 16(%rax)
	leal	1(%rdx), %ecx
	movq	%rax, 24(%r12)
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L2849
	.p2align 4,,10
	.p2align 3
.L2741:
	movq	16(%r12), %rbx
.L2757:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	jne	.L2744
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2744
	call	_ZN2v82V89ClearWeakEPm@PLT
.L2744:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	2480(%rbx), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_EE(%rip), %rcx
	movb	$1, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rcx, (%rax)
	movq	%r12, 24(%rax)
	movq	%r12, 32(%rax)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L2745
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L2747
.L2845:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2747:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L2850
.L2749:
	addl	$1, %eax
	cmpl	$11, -52(%rbp)
	movl	%eax, 4(%rdx)
	movq	56(%r12), %rdx
	setne	72(%rdx)
	subl	$1, 80(%rdx)
	js	.L2851
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L2852
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L2853
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L2700
	cmpb	$0, 9(%rdx)
	jne	.L2854
	cmpb	$0, 8(%rdx)
	je	.L2700
	cmpq	$0, 8(%r12)
	je	.L2700
	movq	%r12, %rdi
	call	_ZN4node10BaseObject12pointer_dataEv
	movq	%r12, %rdi
	movb	$1, 8(%rax)
	call	_ZN4node10BaseObject12pointer_dataEv
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2700
	movq	8(%r12), %rdi
	addq	$56, %rsp
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	popq	%rbx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L2848:
	.cfi_restore_state
	movl	-52(%rbp), %eax
	movq	%r14, 8(%rbx)
	movl	%eax, (%rbx)
.L2725:
	movq	24(%r12), %rax
	movb	$1, 4(%rbx)
	testq	%rax, %rax
	je	.L2855
.L2739:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	jne	.L2741
.L2849:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L2741
	call	_ZN2v82V89ClearWeakEPm@PLT
	movq	24(%r12), %rax
	movq	16(%r12), %rbx
	testq	%rax, %rax
	jne	.L2757
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2768
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2743:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L2847:
	movl	$32, %edi
	call	malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2856
.L2703:
	movq	(%rbx), %r13
	movq	$0, 16(%r14)
	pxor	%xmm0, %xmm0
	movq	$0, 24(%r14)
	movups	%xmm0, (%r14)
	movq	%r13, %rdi
	call	strlen@PLT
	leaq	1(%rax), %r15
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2857
.L2704:
	movq	%rdi, (%r14)
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	8(%rbx), %rdx
	cmpq	$0, (%rdx)
	je	.L2706
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L2707:
	movq	%r13, %rax
	addq	$1, %r13
	leaq	0(,%r13,8), %rcx
	cmpq	$0, (%rdx,%r13,8)
	movq	%rcx, -96(%rbp)
	jne	.L2707
	movabsq	$2305843009213693951, %rdx
	addq	$2, %rax
	andq	%rax, %rdx
	leaq	0(,%rax,8), %r15
	cmpq	%rdx, %rax
	jne	.L2717
	testq	%r15, %r15
	je	.L2709
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L2760
	movq	%rax, 8(%r14)
.L2763:
	xorl	%r15d, %r15d
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L2713:
	movq	%rax, (%r8)
	movq	(%rdi), %rdi
	addq	$1, %r15
	movq	(%rsi), %rsi
	call	memcpy@PLT
	cmpq	%r13, %r15
	je	.L2858
.L2714:
	movq	8(%rbx), %rsi
	leaq	0(,%r15,8), %rax
	movq	%rax, -64(%rbp)
	addq	%rax, %rsi
	movq	(%rsi), %rdi
	movq	%rsi, -88(%rbp)
	call	strlen@PLT
	movq	-64(%rbp), %r8
	addq	8(%r14), %r8
	leaq	1(%rax), %rdx
	movq	%r8, -80(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -72(%rbp)
	call	malloc@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %rdx
	testq	%rax, %rax
	movq	-88(%rbp), %rsi
	movq	%r8, %rdi
	jne	.L2713
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-72(%rbp), %rdx
	movq	%rdx, %rdi
	call	malloc@PLT
	movq	-72(%rbp), %rdx
	movq	-80(%rbp), %r8
	testq	%rax, %rax
	jne	.L2859
.L2705:
	leaq	_ZZN4node6MallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2856:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movl	$32, %edi
	call	malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L2703
	leaq	_ZZN4node6MallocI7hostentEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2700:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2850:
	.cfi_restore_state
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	jmp	.L2749
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L2845
	jmp	.L2747
	.p2align 4,,10
	.p2align 3
.L2846:
	addq	$56, %rsp
	movl	$8, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L2858:
	.cfi_restore_state
	movq	8(%r14), %rax
.L2712:
	movq	-96(%rbp), %rcx
	movq	24(%rbx), %rdx
	movq	$0, (%rax,%rcx)
	cmpq	$0, (%rdx)
	je	.L2715
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L2716:
	movq	%r13, %rax
	addq	$1, %r13
	leaq	0(,%r13,8), %rcx
	cmpq	$0, (%rdx,%r13,8)
	movq	%rcx, -64(%rbp)
	jne	.L2716
	movabsq	$2305843009213693951, %rdx
	addq	$2, %rax
	andq	%rax, %rdx
	leaq	0(,%rax,8), %r15
	cmpq	%rdx, %rax
	jne	.L2717
	testq	%r15, %r15
	je	.L2709
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2758
	movq	%rax, 24(%r14)
.L2762:
	xorl	%r15d, %r15d
	jmp	.L2723
	.p2align 4,,10
	.p2align 3
.L2720:
	movq	%rax, (%rdx)
	movq	24(%rbx), %rax
	movq	%r8, %rdx
	addq	$1, %r15
	movq	(%r9), %rdi
	movq	(%rax,%rsi), %rsi
	call	memcpy@PLT
	movq	24(%r14), %rdx
	cmpq	%r13, %r15
	je	.L2719
.L2723:
	movslq	20(%rbx), %r8
	leaq	0(,%r15,8), %rsi
	movl	$1, %edi
	addq	%rsi, %rdx
	movq	%rsi, -96(%rbp)
	testq	%r8, %r8
	movq	%rdx, -88(%rbp)
	cmovne	%r8, %rdi
	movq	%r8, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	malloc@PLT
	movq	-88(%rbp), %rdx
	movq	-72(%rbp), %rdi
	testq	%rax, %rax
	movq	-80(%rbp), %r8
	movq	-96(%rbp), %rsi
	movq	%rdx, %r9
	jne	.L2720
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	%rdi, -88(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-88(%rbp), %rdi
	call	malloc@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %rdx
	movq	-96(%rbp), %rsi
	testq	%r8, %r8
	je	.L2771
	testq	%rax, %rax
	je	.L2705
.L2771:
	movq	24(%r14), %r9
	movslq	20(%rbx), %r8
	addq	%rsi, %r9
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2854:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L2857:
	.cfi_restore_state
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2705
	movq	(%rbx), %r13
	jmp	.L2704
	.p2align 4,,10
	.p2align 3
.L2852:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2768:
	xorl	%edx, %edx
	jmp	.L2743
.L2715:
	movl	$8, %edi
	call	malloc@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2860
	movq	%rdx, 24(%r14)
	movq	$0, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L2719:
	movq	-64(%rbp), %rax
	movq	$0, (%rdx,%rax)
	movq	16(%rbx), %rax
	movq	%rax, 16(%r14)
	jmp	.L2702
	.p2align 4,,10
	.p2align 3
.L2851:
	leaq	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2717:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2861:
	movq	$0, -96(%rbp)
	movl	$8, %r15d
	xorl	%r13d, %r13d
.L2760:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L2709
	movq	%rax, 8(%r14)
	testq	%r13, %r13
	jne	.L2763
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L2709:
	leaq	_ZZN4node6MallocIPcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2706:
	movl	$8, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L2861
	movq	%rax, 8(%r14)
	movq	$0, -96(%rbp)
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L2767:
	xorl	%edx, %edx
	jmp	.L2740
.L2860:
	movq	$0, -64(%rbp)
	xorl	%r13d, %r13d
	movl	$8, %r15d
.L2758:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2709
	movq	%rax, 24(%r14)
	testq	%r13, %r13
	jne	.L2762
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2853:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2859:
	movq	-64(%rbp), %rcx
	movq	8(%rbx), %rsi
	addq	%rcx, %rsi
	addq	8(%r14), %rcx
	movq	%rcx, %rdi
	jmp	.L2713
	.cfi_endproc
.LFE7694:
	.size	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiP7hostent, .-_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap8CallbackEPviiP7hostent
	.p2align 4
	.globl	_Z20_register_cares_wrapv
	.type	_Z20_register_cares_wrapv, @function
_Z20_register_cares_wrapv:
.LFB7835:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7835:
	.size	_Z20_register_cares_wrapv, .-_Z20_register_cares_wrapv
	.section	.text._ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB9993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L2864
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L2874
.L2890:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L2875:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2864:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L2888
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L2889
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L2867:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L2869
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L2870
	.p2align 4,,10
	.p2align 3
.L2871:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L2872:
	testq	%rsi, %rsi
	je	.L2869
.L2870:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L2871
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L2877
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L2870
	.p2align 4,,10
	.p2align 3
.L2869:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L2873
	call	_ZdlPv@PLT
.L2873:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L2890
.L2874:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L2876
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L2876:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L2875
	.p2align 4,,10
	.p2align 3
.L2877:
	movq	%rdx, %rdi
	jmp	.L2872
	.p2align 4,,10
	.p2align 3
.L2888:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L2867
.L2889:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9993:
	.size	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc.str1.1,"aMS",@progbits,1
.LC89:
	.string	"wrapped"
.LC90:
	.string	"wrapper"
	.section	.text._ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc,"axG",@progbits,_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	.type	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc, @function
_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc:
.LFB7567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	104(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	divq	%rsi
	movq	96(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L2892
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L2894
	.p2align 4,,10
	.p2align 3
.L2946:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2892
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L2892
.L2894:
	cmpq	%rdi, %r12
	jne	.L2946
	movq	16(%rcx), %r15
.L2891:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2947
	addq	$120, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2892:
	.cfi_restore_state
	movl	$72, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rax, (%r15)
	leaq	48(%r15), %rax
	movq	%r12, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rax, -152(%rbp)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	testq	%r12, %r12
	je	.L2948
	movq	(%rbx), %rsi
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testq	%rax, %rax
	je	.L2896
	movq	8(%rbx), %rdi
	leaq	-136(%rbp), %rsi
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	%rax, -136(%rbp)
	call	*%rdx
	movq	%rax, 16(%r15)
.L2896:
	movq	8(%r15), %rsi
	leaq	-96(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	32(%r15), %rdi
	cmpq	%rcx, %rdx
	je	.L2949
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rax
	cmpq	%rdi, -152(%rbp)
	je	.L2950
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	48(%r15), %r8
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r15)
	testq	%rdi, %rdi
	je	.L2902
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L2900:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L2903
	call	_ZdlPv@PLT
.L2903:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, 64(%r15)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2904
	movq	(%rdi), %rax
	call	*8(%rax)
.L2904:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L2905
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2907
	.p2align 4,,10
	.p2align 3
.L2951:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2905
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L2905
.L2907:
	cmpq	%rsi, %r12
	jne	.L2951
	addq	$16, %rcx
.L2911:
	movq	%r15, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2908
	cmpq	72(%rbx), %rax
	je	.L2952
.L2909:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2908
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L2908:
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L2891
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC89(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r15), %rsi
	movq	%r15, %rdx
	leaq	.LC90(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L2891
	.p2align 4,,10
	.p2align 3
.L2949:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2898
	cmpq	$1, %rdx
	je	.L2953
	movq	%rcx, %rsi
	movq	%rcx, -152(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	movq	-152(%rbp), %rcx
.L2898:
	movq	%rdx, 40(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L2900
	.p2align 4,,10
	.p2align 3
.L2952:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2909
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%r15)
.L2902:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L2900
	.p2align 4,,10
	.p2align 3
.L2905:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L2911
	.p2align 4,,10
	.p2align 3
.L2948:
	leaq	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2953:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	jmp	.L2898
.L2947:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7567:
	.size	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc, .-_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC91:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB9997:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L2963
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L2964
.L2956:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2964:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L2965
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L2966
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L2961
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L2961:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L2959:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L2956
	.p2align 4,,10
	.p2align 3
.L2965:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L2958
	cmpq	%r14, %rsi
	je	.L2959
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2959
	.p2align 4,,10
	.p2align 3
.L2958:
	cmpq	%r14, %rsi
	je	.L2959
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L2959
.L2963:
	leaq	.LC91(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2966:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9997:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata.str1.1
.LC92:
	.string	"channel"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L2967
	movq	104(%rsi), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rsi, %rbx
	divq	%rdi
	movq	96(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L2969
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2971
	.p2align 4,,10
	.p2align 3
.L3015:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2969
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L2969
.L2971:
	cmpq	%rsi, %r12
	jne	.L3015
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3016
	cmpq	72(%rbx), %rax
	je	.L3017
.L2972:
	movq	-8(%rax), %rsi
.L2987:
	leaq	.LC92(%rip), %rcx
	call	*%r8
.L2967:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3018
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3017:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2972
	.p2align 4,,10
	.p2align 3
.L2969:
	movq	(%rbx), %rsi
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L2974
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L2975
	.p2align 4,,10
	.p2align 3
.L3019:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L2974
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%r8, %rdx
	jne	.L2974
.L2975:
	cmpq	%rsi, %r12
	jne	.L3019
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L2977
	cmpq	72(%rbx), %rax
	je	.L3020
.L2976:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2977
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	leaq	.LC92(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L2977:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2967
	.p2align 4,,10
	.p2align 3
.L3016:
	xorl	%esi, %esi
	jmp	.L2987
	.p2align 4,,10
	.p2align 3
.L2974:
	leaq	.LC92(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	movq	80(%rbx), %rcx
	movq	64(%rbx), %rdx
	movq	%rax, -72(%rbp)
	subq	$8, %rcx
	cmpq	%rcx, %rdx
	je	.L2978
	movq	%rax, (%rdx)
	addq	$8, %rdx
	movq	%rdx, 64(%rbx)
.L2979:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-72(%rbp), %r14
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L2990
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L3021
.L2981:
	movq	-8(%rax), %rax
.L2980:
	cmpq	%r14, %rax
	jne	.L3022
	cmpq	$0, 64(%rax)
	je	.L3023
	cmpq	72(%rbx), %rdi
	je	.L2984
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L2977
	.p2align 4,,10
	.p2align 3
.L3020:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2976
.L3021:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L2981
.L2978:
	leaq	-72(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L2979
.L2984:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L2977
.L3022:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3023:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2990:
	xorl	%eax, %eax
	jmp	.L2980
.L3018:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7650:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1
.LC93:
	.string	"uv_timer_t"
.LC94:
	.string	"timer_handle"
.LC95:
	.string	"node_ares_task_list"
.LC96:
	.string	"task_list"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 56(%rdi)
	je	.L3025
	movl	$72, %edi
	leaq	-128(%rbp), %r14
	call	_Znwm@PLT
	movl	$10, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r13
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC93(%rip), %rcx
	movq	%rax, -184(%rbp)
	leaq	32(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	48(%r13), %rax
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movb	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	$0, 40(%r13)
	movb	$0, 48(%r13)
	movq	$0, 64(%r13)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r13)
	movq	%r14, %rsi
	movq	$152, 64(%r13)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r13, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3026
	movq	(%rdi), %rax
	call	*8(%rax)
.L3026:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3027
	movq	72(%rbx), %rdx
	movq	88(%rbx), %rcx
	movq	%rax, %rsi
	cmpq	%rdx, %rax
	je	.L3160
	movq	-8(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L3029
.L3179:
	movq	8(%rbx), %rdi
	leaq	.LC94(%rip), %rcx
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3025:
	cmpq	$0, 104(%r12)
	je	.L3024
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3161
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rsi
	movq	72(%rbx), %rdx
	movq	88(%rbx), %rcx
	leaq	-128(%rbp), %r14
	movq	%rsi, -184(%rbp)
.L3086:
	cmpq	%rdx, %rax
	je	.L3162
.L3034:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L3033
	subq	$56, 64(%rax)
.L3033:
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$19, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r13
	movq	-184(%rbp), %rax
	leaq	.LC95(%rip), %rcx
	movq	$0, 8(%r13)
	leaq	32(%r13), %rdi
	movq	%rax, 0(%r13)
	leaq	48(%r13), %rax
	movq	$0, 16(%r13)
	movb	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	$0, 40(%r13)
	movb	$0, 48(%r13)
	movq	$0, 64(%r13)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r13)
	movq	%r14, %rsi
	movq	$56, 64(%r13)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r13, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3036
	movq	(%rdi), %rax
	call	*8(%rax)
.L3036:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3037
	movq	%rax, %rdx
	cmpq	72(%rbx), %rax
	je	.L3163
.L3038:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L3037
	movq	8(%rbx), %rdi
	leaq	.LC96(%rip), %rcx
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rax
.L3037:
	leaq	16(%rbx), %rcx
	movq	%r13, -128(%rbp)
	movq	%rcx, -200(%rbp)
	movq	80(%rbx), %rcx
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L3039
	movq	%r13, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L3040:
	movq	104(%r12), %r13
	leaq	-160(%rbp), %r15
	testq	%r13, %r13
	je	.L3078
	movq	%r14, -192(%rbp)
	.p2align 4,,10
	.p2align 3
.L3041:
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L3044
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L3045
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3047
	.p2align 4,,10
	.p2align 3
.L3164:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3045
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L3045
.L3047:
	cmpq	%rsi, %r12
	jne	.L3164
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3165
	cmpq	72(%rbx), %rax
	je	.L3166
.L3048:
	movq	-8(%rax), %rsi
.L3081:
	xorl	%ecx, %ecx
	call	*%r8
.L3044:
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L3041
.L3078:
	movq	64(%rbx), %rdi
	cmpq	72(%rbx), %rdi
	je	.L3167
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3024:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3168
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3166:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3048
	.p2align 4,,10
	.p2align 3
.L3045:
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L3049
	movq	(%rax), %rsi
	movq	8(%rsi), %rdi
	movq	%rsi, %r8
	movq	%rdi, %r10
	jmp	.L3052
	.p2align 4,,10
	.p2align 3
.L3169:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L3056
	movq	8(%r8), %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L3056
.L3052:
	cmpq	%r10, %r12
	jne	.L3169
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3054
	cmpq	72(%rbx), %rax
	je	.L3170
.L3053:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3054
	movq	8(%rbx), %rdi
	movq	16(%r8), %rdx
	xorl	%ecx, %ecx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3054:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L3044
	.p2align 4,,10
	.p2align 3
.L3171:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L3049
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L3049
.L3056:
	cmpq	%rdi, %r12
	jne	.L3171
	movq	16(%rsi), %r14
.L3070:
	movq	80(%rbx), %rdx
	movq	64(%rbx), %rax
	movq	%r14, -128(%rbp)
	subq	$8, %rdx
	cmpq	%rdx, %rax
	je	.L3071
	movq	%r14, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L3072:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-128(%rbp), %r14
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L3091
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L3172
.L3074:
	movq	-8(%rax), %rax
.L3073:
	cmpq	%r14, %rax
	jne	.L3173
	cmpq	$0, 64(%rax)
	je	.L3174
	cmpq	72(%rbx), %rdi
	je	.L3077
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L3044
	.p2align 4,,10
	.p2align 3
.L3165:
	xorl	%esi, %esi
	jmp	.L3081
	.p2align 4,,10
	.p2align 3
.L3163:
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L3038
	.p2align 4,,10
	.p2align 3
.L3162:
	movq	-8(%rcx), %rax
	addq	$512, %rax
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3049:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	-192(%rbp), %rdi
	movq	(%rbx), %rsi
	movq	%rax, %r14
	movq	-184(%rbp), %rax
	movq	%r12, 8(%r14)
	movq	%rax, (%r14)
	leaq	48(%r14), %rax
	movq	%rax, 32(%r14)
	movq	$0, 16(%r14)
	movb	$0, 24(%r14)
	movq	$0, 40(%r14)
	movb	$0, 48(%r14)
	movq	$0, 64(%r14)
	movq	%rax, -208(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testq	%rax, %rax
	je	.L3084
	movq	8(%rbx), %rdi
	leaq	-168(%rbp), %rsi
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	%rax, -168(%rbp)
	call	*%rdx
	movq	%rax, 16(%r14)
.L3084:
	movq	8(%r14), %rsi
	leaq	-96(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rax
	leaq	-80(%rbp), %rcx
	movq	32(%r14), %rdi
	movq	-88(%rbp), %rdx
	cmpq	%rcx, %rax
	je	.L3175
	movq	-80(%rbp), %rsi
	cmpq	%rdi, -208(%rbp)
	je	.L3176
	movq	%rdx, %xmm0
	movq	%rsi, %xmm5
	movq	48(%r14), %r8
	movq	%rax, 32(%r14)
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 40(%r14)
	testq	%rdi, %rdi
	je	.L3062
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L3060:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L3063
	call	_ZdlPv@PLT
.L3063:
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-192(%rbp), %rdi
	movq	%rax, 64(%r14)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	-192(%rbp), %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r14, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3064
	movq	(%rdi), %rax
	call	*8(%rax)
.L3064:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3065
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3067
	.p2align 4,,10
	.p2align 3
.L3177:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3065
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3065
.L3067:
	cmpq	%rsi, %r12
	jne	.L3177
	addq	$16, %rcx
.L3079:
	movq	%r14, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3068
	cmpq	72(%rbx), %rax
	je	.L3178
.L3069:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3068
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3068:
	movq	16(%r14), %rdx
	testq	%rdx, %rdx
	je	.L3070
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	leaq	.LC89(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r14), %rsi
	movq	%r14, %rdx
	leaq	.LC90(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L3070
	.p2align 4,,10
	.p2align 3
.L3170:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3053
	.p2align 4,,10
	.p2align 3
.L3160:
	movq	-8(%rcx), %rsi
	addq	$512, %rsi
	movq	-8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L3179
.L3029:
	cmpq	$0, 104(%r12)
	jne	.L3086
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3039:
	movq	%r14, %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L3040
	.p2align 4,,10
	.p2align 3
.L3167:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3172:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3074
	.p2align 4,,10
	.p2align 3
.L3027:
	cmpq	$0, 104(%r12)
	jne	.L3033
	jmp	.L3024
	.p2align 4,,10
	.p2align 3
.L3071:
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L3072
	.p2align 4,,10
	.p2align 3
.L3161:
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	-128(%rbp), %r14
	movq	%rax, -184(%rbp)
	jmp	.L3033
	.p2align 4,,10
	.p2align 3
.L3077:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm4
	leaq	504(%rax), %rcx
	movq	%rax, %xmm3
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3054
	.p2align 4,,10
	.p2align 3
.L3173:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3174:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3091:
	xorl	%eax, %eax
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3175:
	testq	%rdx, %rdx
	je	.L3058
	cmpq	$1, %rdx
	je	.L3180
	movq	%rcx, %rsi
	movq	%rcx, -208(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	32(%r14), %rdi
	movq	-208(%rbp), %rcx
.L3058:
	movq	%rdx, 40(%r14)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L3060
.L3178:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3069
.L3176:
	movq	%rdx, %xmm0
	movq	%rsi, %xmm6
	movq	%rax, 32(%r14)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 40(%r14)
.L3062:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3065:
	movl	$24, %edi
	movq	%r9, -208(%rbp)
	call	_Znwm@PLT
	movq	-208(%rbp), %r9
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	%r9, %rsi
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L3079
.L3180:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	32(%r14), %rdi
	jmp	.L3058
.L3168:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7605:
	.size	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__Z20_register_cares_wrapv, @function
_GLOBAL__sub_I__Z20_register_cares_wrapv:
.LFB12142:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L3184
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE(%rip), %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	leaq	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
.L3184:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE12142:
	.size	_GLOBAL__sub_I__Z20_register_cares_wrapv, .-_GLOBAL__sub_I__Z20_register_cares_wrapv
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__Z20_register_cares_wrapv
	.weak	_ZTVN4node11ReqWrapBaseE
	.section	.data.rel.ro._ZTVN4node11ReqWrapBaseE,"awG",@progbits,_ZTVN4node11ReqWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11ReqWrapBaseE, @object
	.size	_ZTVN4node11ReqWrapBaseE, 48
_ZTVN4node11ReqWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_114node_ares_taskE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_114node_ares_taskE, 72
_ZTVN4node10cares_wrap12_GLOBAL__N_114node_ares_taskE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114node_ares_taskD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114node_ares_task8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE, @object
	.size	_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE, 160
_ZTVN4node7ReqWrapI16uv_getaddrinfo_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.quad	_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapE, 160
_ZTVN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.quad	_ZN4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD1Ev
	.quad	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetAddrInfoReqWrapD0Ev
	.quad	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI16uv_getaddrinfo_sE12GetAsyncWrapEv
	.weak	_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI16uv_getnameinfo_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE, @object
	.size	_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE, 160
_ZTVN4node7ReqWrapI16uv_getnameinfo_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.quad	_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.section	.data.rel.ro
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapE, 160
_ZTVN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.quad	_ZN4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD1Ev
	.quad	_ZThn56_N4node10cares_wrap12_GLOBAL__N_118GetNameInfoReqWrapD0Ev
	.quad	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI16uv_getnameinfo_sE12GetAsyncWrapEv
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_111ChannelWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_111ChannelWrapE, 96
_ZTVN4node10cares_wrap12_GLOBAL__N_111ChannelWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111ChannelWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111ChannelWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_19QueryWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_110QueryAWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_110QueryAWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_110QueryAWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_110QueryAWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_110QueryAWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_113QueryAaaaWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryCnameWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryMxWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryMxWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_111QueryNsWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_111QueryNsWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryTxtWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySrvWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QueryPtrWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_114QueryNaptrWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_112QuerySoaWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostent
	.align 8
	.type	_ZTVN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapE, @object
	.size	_ZTVN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapE, 128
_ZTVN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD1Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrapD0Ev
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap14MemoryInfoNameEv
	.quad	_ZNK4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKc
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKci
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhi
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap5ParseEP7hostent
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10cares_wrap12_GLOBAL__N_19QueryWrap21QueueResponseCallbackEiEUlS2_E_E4CallES2_
	.weak	_ZZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE3ForEPNS_7ReqWrapIS1_EES6_E4args
	.section	.rodata.str1.1
.LC97:
	.string	"../src/req_wrap-inl.h:130"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC98:
	.string	"(req_wrap->original_callback_) == nullptr"
	.align 8
.LC99:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, void (*)"
	.ascii	"(ReqT*, Args ...)>::For(node::ReqWrap<T>*, node::MakeLibuvRe"
	.ascii	"questCallbac"
	.string	"k<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_getnameinfo_s; Args = {int, const char*, const char*}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_getnameinfo_s*, int, const char*, const char*)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE3ForEPNS_7ReqWrapIS1_EES6_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE3ForEPNS_7ReqWrapIS1_EES6_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE3ForEPNS_7ReqWrapIS1_EES6_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE3ForEPNS_7ReqWrapIS1_EES6_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI16uv_getnameinfo_sPFvPS1_iPKcS4_EE3ForEPNS_7ReqWrapIS1_EES6_E4args:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC99
	.weak	_ZZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE3ForEPNS_7ReqWrapIS1_EES6_E4args
	.section	.rodata.str1.8
	.align 8
.LC100:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, void (*)"
	.ascii	"(ReqT*, Args ...)>::For(node::ReqWrap<T>*,"
	.string	" node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_getaddrinfo_s; Args = {int, addrinfo*}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_getaddrinfo_s*, int, addrinfo*)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE3ForEPNS_7ReqWrapIS1_EES6_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE3ForEPNS_7ReqWrapIS1_EES6_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE3ForEPNS_7ReqWrapIS1_EES6_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE3ForEPNS_7ReqWrapIS1_EES6_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI16uv_getaddrinfo_sPFvPS1_iP8addrinfoEE3ForEPNS_7ReqWrapIS1_EES6_E4args:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC100
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args
	.section	.rodata.str1.1
.LC101:
	.string	"../src/util.h:352"
.LC102:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 8; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm8EEixEmE4args:
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC104:
	.string	"../src/util-inl.h:374"
.LC105:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC107:
	.string	"../src/util-inl.h:325"
.LC108:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.section	.rodata.str1.1
.LC110:
	.string	"../src/cares_wrap.cc:1797"
.LC111:
	.string	"args[1]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::GetHostByAddrWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.section	.rodata.str1.1
.LC113:
	.string	"../src/cares_wrap.cc:1796"
.LC114:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC112
	.section	.rodata.str1.1
.LC115:
	.string	"../src/cares_wrap.cc:1795"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"(false) == (args.IsConstructCall())"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_17GetHostByAddrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC112
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QuerySoaWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC117
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC117
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySoaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryNaptrWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC118
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC118
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryNaptrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC118
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryPtrWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC119
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC119
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryPtrWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC119
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QuerySrvWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC120
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC120
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QuerySrvWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC120
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryTxtWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC121
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC121
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryTxtWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC121
	.section	.rodata.str1.8
	.align 8
.LC122:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryNsWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC122
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC122
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryNsWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC122
	.section	.rodata.str1.8
	.align 8
.LC123:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryMxWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC123
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC123
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_11QueryMxWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC123
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryCnameWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC124
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC124
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_14QueryCnameWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC124
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryAaaaWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC125
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC125
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_13QueryAaaaWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC125
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryAWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC126
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC126
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_10QueryAWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC126
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"void node::cares_wrap::{anonymous}::Query(const v8::FunctionCallbackInfo<v8::Value>&) [with Wrap = node::cares_wrap::{anonymous}::QueryAnyWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC127
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC127
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_1L5QueryINS1_12QueryAnyWrapEEEvRKN2v820FunctionCallbackInfoINS4_5ValueEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC127
	.weak	_ZZN4node6MallocI7hostentEEPT_mE4args
	.section	.rodata.str1.1
.LC128:
	.string	"../src/util-inl.h:381"
	.section	.rodata.str1.8
	.align 8
.LC129:
	.string	"T* node::Malloc(size_t) [with T = hostent; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocI7hostentEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocI7hostentEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocI7hostentEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocI7hostentEEPT_mE4args, 24
_ZZN4node6MallocI7hostentEEPT_mE4args:
	.quad	.LC128
	.quad	.LC105
	.quad	.LC129
	.weak	_ZZN4node6MallocIhEEPT_mE4args
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"T* node::Malloc(size_t) [with T = unsigned char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIhEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIhEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIhEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIhEEPT_mE4args, 24
_ZZN4node6MallocIhEEPT_mE4args:
	.quad	.LC128
	.quad	.LC105
	.quad	.LC130
	.weak	_ZZN4node6MallocIPcEEPT_mE4args
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"T* node::Malloc(size_t) [with T = char*; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIPcEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIPcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIPcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIPcEEPT_mE4args, 24
_ZZN4node6MallocIPcEEPT_mE4args:
	.quad	.LC128
	.quad	.LC105
	.quad	.LC131
	.weak	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args
	.section	.rodata.str1.1
.LC132:
	.string	"../src/req_wrap-inl.h:28"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"(false) == (persistent().IsEmpty())"
	.align 8
.LC134:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_getnameinfo_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args, 24
_ZZN4node7ReqWrapI16uv_getnameinfo_sED4EvE4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.weak	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_getaddrinfo_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args, 24
_ZZN4node7ReqWrapI16uv_getaddrinfo_sED4EvE4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC135
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC136:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC138:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.weak	_ZZN4node6MallocIcEEPT_mE4args
	.section	.rodata.str1.8
	.align 8
.LC139:
	.string	"T* node::Malloc(size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIcEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIcEEPT_mE4args, 24
_ZZN4node6MallocIcEEPT_mE4args:
	.quad	.LC128
	.quad	.LC105
	.quad	.LC139
	.section	.rodata.str1.1
.LC140:
	.string	"../src/cares_wrap.cc"
.LC141:
	.string	"cares_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC140
	.quad	0
	.quad	_ZN4node10cares_wrap12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC141
	.quad	0
	.quad	0
	.section	.rodata
	.align 16
	.type	_ZN4node10cares_wrap12_GLOBAL__N_1L19EMSG_ESETSRVPENDINGE, @object
	.size	_ZN4node10cares_wrap12_GLOBAL__N_1L19EMSG_ESETSRVPENDINGE, 27
_ZN4node10cares_wrap12_GLOBAL__N_1L19EMSG_ESETSRVPENDINGE:
	.string	"There are pending queries."
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2161
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_16CancelERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2161,8,8
	.section	.rodata.str1.1
.LC142:
	.string	"../src/cares_wrap.cc:2132"
.LC143:
	.string	"0 && \"Bad address family.\""
	.section	.rodata.str1.8
	.align 8
.LC144:
	.string	"void node::cares_wrap::{anonymous}::SetServers(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.section	.rodata.str1.1
.LC145:
	.string	"../src/cares_wrap.cc:2109"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"elm->Get(env->context(), 2).ToLocalChecked()->Int32Value(env->context()).FromJust()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC145
	.quad	.LC146
	.quad	.LC144
	.section	.rodata.str1.1
.LC147:
	.string	"../src/cares_wrap.cc:2108"
	.section	.rodata.str1.8
	.align 8
.LC148:
	.string	"elm->Get(env->context(), 1).ToLocalChecked()->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC147
	.quad	.LC148
	.quad	.LC144
	.section	.rodata.str1.1
.LC149:
	.string	"../src/cares_wrap.cc:2106"
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"elm->Get(env->context(), 0).ToLocalChecked()->Int32Value(env->context()).FromJust()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC149
	.quad	.LC150
	.quad	.LC144
	.section	.rodata.str1.1
.LC151:
	.string	"../src/cares_wrap.cc:2101"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"arr->Get(env->context(), i).ToLocalChecked()->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC151
	.quad	.LC152
	.quad	.LC144
	.section	.rodata.str1.1
.LC153:
	.string	"../src/cares_wrap.cc:2084"
.LC154:
	.string	"args[0]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110SetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC144
	.section	.rodata.str1.1
.LC155:
	.string	"../src/cares_wrap.cc:2057"
.LC156:
	.string	"(err) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"void node::cares_wrap::{anonymous}::GetServers(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC155
	.quad	.LC156
	.quad	.LC157
	.section	.rodata.str1.1
.LC158:
	.string	"../src/cares_wrap.cc:2047"
.LC159:
	.string	"(r) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_110GetServersERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC158
	.quad	.LC159
	.quad	.LC157
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2021
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic2021,8,8
	.section	.rodata.str1.1
.LC160:
	.string	"../src/cares_wrap.cc:2016"
	.section	.rodata.str1.8
	.align 8
.LC161:
	.string	"uv_ip4_addr(*ip, port, reinterpret_cast<sockaddr_in*>(&addr)) == 0 || uv_ip6_addr(*ip, port, reinterpret_cast<sockaddr_in6*>(&addr)) == 0"
	.align 8
.LC162:
	.string	"void node::cares_wrap::{anonymous}::GetNameInfo(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.section	.rodata.str1.1
.LC163:
	.string	"../src/cares_wrap.cc:2010"
.LC164:
	.string	"args[2]->IsUint32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC163
	.quad	.LC164
	.quad	.LC162
	.section	.rodata.str1.1
.LC165:
	.string	"../src/cares_wrap.cc:2009"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC165
	.quad	.LC111
	.quad	.LC162
	.section	.rodata.str1.1
.LC166:
	.string	"../src/cares_wrap.cc:2008"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetNameInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC166
	.quad	.LC114
	.quad	.LC162
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic1986
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE29trace_event_unique_atomic1986,8,8
	.section	.rodata.str1.1
.LC167:
	.string	"../src/cares_wrap.cc:1973"
.LC168:
	.string	"0 && \"bad address family\""
	.section	.rodata.str1.8
	.align 8
.LC169:
	.string	"void node::cares_wrap::{anonymous}::GetAddrInfo(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.section	.rodata.str1.1
.LC170:
	.string	"../src/cares_wrap.cc:1951"
.LC171:
	.string	"args[4]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC170
	.quad	.LC171
	.quad	.LC169
	.section	.rodata.str1.1
.LC172:
	.string	"../src/cares_wrap.cc:1950"
.LC173:
	.string	"args[2]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC172
	.quad	.LC173
	.quad	.LC169
	.section	.rodata.str1.1
.LC174:
	.string	"../src/cares_wrap.cc:1949"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC174
	.quad	.LC111
	.quad	.LC169
	.section	.rodata.str1.1
.LC175:
	.string	"../src/cares_wrap.cc:1948"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111GetAddrInfoERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC175
	.quad	.LC114
	.quad	.LC169
	.section	.rodata.str1.1
.LC176:
	.string	"../src/cares_wrap.cc:1939"
	.section	.rodata.str1.8
	.align 8
.LC177:
	.string	"(0) == (uv_inet_ntop(af, &result, canonical_ip, sizeof(canonical_ip)))"
	.align 8
.LC178:
	.string	"void node::cares_wrap::{anonymous}::CanonicalizeIP(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_114CanonicalizeIPERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_E29trace_event_unique_atomic1909
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetNameInfoEP16uv_getnameinfo_siPKcS5_E29trace_event_unique_atomic1909,8,8
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoE29trace_event_unique_atomic1875
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoE29trace_event_unique_atomic1875,8,8
	.section	.rodata.str1.1
.LC179:
	.string	"../src/cares_wrap.cc:1838"
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"(p->ai_socktype) == (SOCK_STREAM)"
	.align 8
.LC181:
	.string	"node::cares_wrap::{anonymous}::AfterGetAddrInfo(uv_getaddrinfo_t*, int, addrinfo*)::<lambda(bool, bool)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoENKUlbbE_clEbbE4args, @object
	.size	_ZZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoENKUlbbE_clEbbE4args, 24
_ZZZN4node10cares_wrap12_GLOBAL__N_116AfterGetAddrInfoEP16uv_getaddrinfo_siP8addrinfoENKUlbbE_clEbbE4args:
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_117GetHostByAddrWrap4SendEPKcE29trace_event_unique_atomic1762,8,8
	.section	.rodata.str1.1
.LC182:
	.string	"../src/cares_wrap.cc:1266"
	.section	.rodata.str1.8
	.align 8
.LC183:
	.string	"(ret->Length()) == (a_count + aaaa_count)"
	.align 8
.LC184:
	.string	"virtual void node::cares_wrap::{anonymous}::QueryAnyWrap::Parse(unsigned char*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_1:
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.section	.rodata.str1.1
.LC185:
	.string	"../src/cares_wrap.cc:1265"
	.section	.rodata.str1.8
	.align 8
.LC186:
	.string	"(aaaa_count) == (static_cast<uint32_t>(naddr6ttls))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args_0:
	.quad	.LC185
	.quad	.LC186
	.quad	.LC184
	.section	.rodata.str1.1
.LC187:
	.string	"../src/cares_wrap.cc:1219"
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"(static_cast<uint32_t>(naddrttls)) == (a_count)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_112QueryAnyWrap5ParseEPhiE4args:
	.quad	.LC187
	.quad	.LC188
	.quad	.LC184
	.section	.rodata.str1.1
.LC189:
	.string	"../src/cares_wrap.cc:807"
.LC190:
	.string	"0 && \"Bad NS type\""
	.section	.rodata.str1.8
	.align 8
.LC191:
	.string	"int node::cares_wrap::{anonymous}::ParseGeneralReply(node::Environment*, const unsigned char*, int, int*, v8::Local<v8::Array>, void*, int*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_E4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_E4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_117ParseGeneralReplyEPNS_11EnvironmentEPKhiPiN2v85LocalINS7_5ArrayEEEPvS6_E4args:
	.quad	.LC189
	.quad	.LC190
	.quad	.LC191
	.section	.rodata.str1.1
.LC192:
	.string	"../src/cares_wrap.cc:743"
.LC193:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC194:
	.string	"virtual void node::cares_wrap::{anonymous}::QueryWrap::Parse(hostent*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostentE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostentE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEP7hostentE4args:
	.quad	.LC192
	.quad	.LC193
	.quad	.LC194
	.section	.rodata.str1.1
.LC195:
	.string	"../src/cares_wrap.cc:739"
	.section	.rodata.str1.8
	.align 8
.LC196:
	.string	"virtual void node::cares_wrap::{anonymous}::QueryWrap::Parse(unsigned char*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhiE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhiE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap5ParseEPhiE4args:
	.quad	.LC195
	.quad	.LC193
	.quad	.LC196
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE28trace_event_unique_atomic731
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE28trace_event_unique_atomic731,8,8
	.section	.rodata.str1.1
.LC197:
	.string	"../src/cares_wrap.cc:726"
.LC198:
	.string	"(status) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC199:
	.string	"void node::cares_wrap::{anonymous}::QueryWrap::ParseError(int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap10ParseErrorEiE4args:
	.quad	.LC197
	.quad	.LC198
	.quad	.LC199
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_E28trace_event_unique_atomic719
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap14CallOnCompleteEN2v85LocalINS3_5ValueEEES6_E28trace_event_unique_atomic719,8,8
	.section	.rodata.str1.1
.LC200:
	.string	"../src/cares_wrap.cc:643"
.LC201:
	.string	"(callback_ptr_) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC202:
	.string	"void* node::cares_wrap::{anonymous}::QueryWrap::MakeCallbackPointer()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap19MakeCallbackPointerEvE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap19MakeCallbackPointerEvE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap19MakeCallbackPointerEvE4args:
	.quad	.LC200
	.quad	.LC201
	.quad	.LC202
	.section	.rodata.str1.1
.LC203:
	.string	"../src/cares_wrap.cc:629"
.LC204:
	.string	"response_data_"
	.section	.rodata.str1.8
	.align 8
.LC205:
	.string	"void node::cares_wrap::{anonymous}::QueryWrap::AfterResponse()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap13AfterResponseEvE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap13AfterResponseEvE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap13AfterResponseEvE4args:
	.quad	.LC203
	.quad	.LC204
	.quad	.LC205
	.local	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKciiE28trace_event_unique_atomic614
	.comm	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap9AresQueryEPKciiE28trace_event_unique_atomic614,8,8
	.section	.rodata.str1.1
.LC206:
	.string	"../src/cares_wrap.cc:605"
	.section	.rodata.str1.8
	.align 8
.LC207:
	.string	"virtual int node::cares_wrap::{anonymous}::QueryWrap::Send(const char*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKciE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKciE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKciE4args:
	.quad	.LC206
	.quad	.LC193
	.quad	.LC207
	.section	.rodata.str1.1
.LC208:
	.string	"../src/cares_wrap.cc:600"
	.section	.rodata.str1.8
	.align 8
.LC209:
	.string	"virtual int node::cares_wrap::{anonymous}::QueryWrap::Send(const char*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKcE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKcE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrap4SendEPKcE4args:
	.quad	.LC208
	.quad	.LC193
	.quad	.LC209
	.section	.rodata.str1.1
.LC210:
	.string	"../src/cares_wrap.cc:591"
	.section	.rodata.str1.8
	.align 8
.LC211:
	.string	"virtual node::cares_wrap::{anonymous}::QueryWrap::~QueryWrap()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD4EvE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD4EvE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_19QueryWrapD4EvE4args:
	.quad	.LC210
	.quad	.LC133
	.quad	.LC211
	.section	.rodata.str1.1
.LC212:
	.string	"../src/cares_wrap.cc:532"
.LC213:
	.string	"(active_query_count_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC214:
	.string	"void node::cares_wrap::{anonymous}::ChannelWrap::ModifyActivityQueryCount(int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap24ModifyActivityQueryCountEiE4args:
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.section	.rodata.str1.1
.LC215:
	.string	"../src/cares_wrap.cc:360"
	.section	.rodata.str1.8
	.align 8
.LC216:
	.string	"task && \"When an ares socket is closed we should have a handle for it\""
	.align 8
.LC217:
	.string	"void node::cares_wrap::{anonymous}::ares_sockstate_cb(void*, ares_socket_t, int, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviiiE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviiiE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_117ares_sockstate_cbEPviiiE4args:
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.section	.rodata.str1.1
.LC218:
	.string	"../src/cares_wrap.cc:267"
	.section	.rodata.str1.8
	.align 8
.LC219:
	.string	"(false) == (channel->task_list()->empty())"
	.align 8
.LC220:
	.string	"static void node::cares_wrap::{anonymous}::ChannelWrap::AresTimeout(uv_timer_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args_0:
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.section	.rodata.str1.1
.LC221:
	.string	"../src/cares_wrap.cc:266"
	.section	.rodata.str1.8
	.align 8
.LC222:
	.string	"(channel->timer_handle()) == (handle)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap11AresTimeoutEP10uv_timer_sE4args:
	.quad	.LC221
	.quad	.LC222
	.quad	.LC220
	.section	.rodata.str1.1
.LC223:
	.string	"../src/cares_wrap.cc:217"
.LC224:
	.string	"args[0]->IsInt32()"
	.section	.rodata.str1.8
	.align 8
.LC225:
	.string	"static void node::cares_wrap::{anonymous}::ChannelWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_1:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC225
	.section	.rodata.str1.1
.LC226:
	.string	"../src/cares_wrap.cc:216"
.LC227:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args_0:
	.quad	.LC226
	.quad	.LC227
	.quad	.LC225
	.section	.rodata.str1.1
.LC228:
	.string	"../src/cares_wrap.cc:215"
.LC229:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @object
	.size	_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node10cares_wrap12_GLOBAL__N_111ChannelWrap3NewERKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC228
	.quad	.LC229
	.quad	.LC225
	.local	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE
	.comm	_ZN4node10cares_wrap12_GLOBAL__N_118ares_library_mutexE,40,32
	.weak	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC230:
	.string	"../src/req_wrap-inl.h:13"
	.section	.rodata.str1.8
	.align 8
.LC231:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC232:
	.string	"node::ReqWrapBase::ReqWrapBase(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, 24
_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args:
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0
	.section	.rodata.str1.8
	.align 8
.LC233:
	.string	"../src/memory_tracker-inl.h:269"
	.section	.rodata.str1.1
.LC234:
	.string	"(n->size_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC235:
	.string	"void node::MemoryTracker::Track(const node::MemoryRetainer*, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0:
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args
	.section	.rodata.str1.8
	.align 8
.LC236:
	.string	"../src/memory_tracker-inl.h:268"
	.section	.rodata.str1.1
.LC237:
	.string	"(CurrentNode()) == (n)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args:
	.quad	.LC236
	.quad	.LC237
	.quad	.LC235
	.weak	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args
	.section	.rodata.str1.8
	.align 8
.LC238:
	.string	"../src/memory_tracker-inl.h:27"
	.section	.rodata.str1.1
.LC239:
	.string	"(retainer_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC240:
	.string	"node::MemoryRetainerNode::MemoryRetainerNode(node::MemoryTracker*, const node::MemoryRetainer*)"
	.section	.data.rel.ro.local._ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,"awG",@progbits,_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,comdat
	.align 16
	.type	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, @gnu_unique_object
	.size	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, 24
_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args:
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC241:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC242:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC243:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC241
	.quad	.LC242
	.quad	.LC243
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC244:
	.string	"../src/base_object-inl.h:195"
.LC245:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC244
	.quad	.LC245
	.quad	.LC243
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC246:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC247:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC248:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC246
	.quad	.LC247
	.quad	.LC248
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC249:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC250:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC251:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC249
	.quad	.LC250
	.quad	.LC251
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC252:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC253:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC254:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC252
	.quad	.LC253
	.quad	.LC254
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC255:
	.string	"../src/env-inl.h:399"
.LC256:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC257:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC255
	.quad	.LC256
	.quad	.LC257
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	5292402893871408455
	.quad	8239178723286279790
	.align 16
.LC2:
	.quad	4788579225595831623
	.quad	7021770917846204793
	.align 16
.LC3:
	.quad	5288753602112021831
	.quad	8239178723286279790
	.align 16
.LC32:
	.quad	0
	.quad	8
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC38:
	.long	0
	.long	-1074790400
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
