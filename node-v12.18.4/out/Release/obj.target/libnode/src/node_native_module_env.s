	.file	"node_native_module_env.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv18ConfigStringGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node13native_module15NativeModuleEnv18ConfigStringGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node13native_module15NativeModuleEnv18ConfigStringGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB7143:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rsi), %rbx
	movq	16(%rbx), %r12
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN4node13native_module18NativeModuleLoader15GetConfigStringEPN2v87IsolateE@PLT
	testq	%rax, %rax
	je	.L6
	movq	(%rax), %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
	movq	24(%rbx), %rax
	movq	%rax, 32(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7143:
	.size	_ZN4node13native_module15NativeModuleEnv18ConfigStringGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node13native_module15NativeModuleEnv18ConfigStringGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"moduleIds"
.LC1:
	.string	"moduleCategories"
.LC2:
	.string	"getCacheUsage"
.LC3:
	.string	"compileFunction"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.type	_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7150:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L8
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L8
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L8
	movq	271(%rax), %rbx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	leaq	_ZN4node13native_module15NativeModuleEnv18ConfigStringGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	344(%rax), %rdx
	pushq	$0
	pushq	$1
	pushq	$0
	pushq	$0
	call	_ZN2v86Object11SetAccessorENS_5LocalINS_7ContextEEENS1_INS_4NameEEEPFvS5_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS5_NS1_IS7_EERKNS6_IvEEENS_10MaybeLocalIS7_EENS_13AccessControlENS_17PropertyAttributeENS_14SideEffectTypeESN_@PLT
	addq	$32, %rsp
	testb	%al, %al
	je	.L25
.L9:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L26
.L10:
	movq	3280(%rbx), %rsi
	pushq	$0
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	leaq	_ZN4node13native_module15NativeModuleEnv15ModuleIdsGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movq	%r12, %rdi
	pushq	$0
	pushq	$0
	call	_ZN2v86Object11SetAccessorENS_5LocalINS_7ContextEEENS1_INS_4NameEEEPFvS5_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS5_NS1_IS7_EERKNS6_IvEEENS_10MaybeLocalIS7_EENS_13AccessControlENS_17PropertyAttributeENS_14SideEffectTypeESN_@PLT
	addq	$32, %rsp
	testb	%al, %al
	je	.L27
.L11:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16, %ecx
	leaq	.LC1(%rip), %rsi
	movq	2680(%rbx), %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L28
.L12:
	movq	3280(%rbx), %rsi
	pushq	$0
	movq	%r14, %r9
	xorl	%r8d, %r8d
	pushq	$1
	leaq	_ZN4node13native_module15NativeModuleEnv19GetModuleCategoriesEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movq	%r12, %rdi
	pushq	$0
	pushq	$0
	call	_ZN2v86Object11SetAccessorENS_5LocalINS_7ContextEEENS1_INS_4NameEEEPFvS5_RKNS_20PropertyCallbackInfoINS_5ValueEEEEPFvS5_NS1_IS7_EERKNS6_IvEEENS_10MaybeLocalIS7_EENS_13AccessControlENS_17PropertyAttributeENS_14SideEffectTypeESN_@PLT
	addq	$32, %rsp
	testb	%al, %al
	je	.L29
.L13:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node13native_module15NativeModuleEnv13GetCacheUsageERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L30
.L14:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L31
.L15:
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L32
.L16:
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L33
.L17:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L34
.L18:
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L35
.L19:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object17SetIntegrityLevelENS_5LocalINS_7ContextEEENS_14IntegrityLevelE@PLT
	testb	%al, %al
	je	.L36
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L27:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L29:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L30:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L32:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L33:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L34:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L35:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7150:
.L8:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7150:
	.text
	.size	_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE4:
	.text
.LHOTE4:
	.p2align 4
	.globl	_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE
	.type	_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE, @function
_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE:
.LFB7124:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	addq	$8, %rbx
	subq	$24, %rsp
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v83Set3NewEPNS_7IsolateE@PLT
	movq	16(%rbx), %r12
	movq	%rax, %r13
	cmpq	%r12, %rbx
	jne	.L43
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v83Set3AddENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L47
.L40:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rax, %rbx
	je	.L44
.L43:
	movq	32(%r12), %rsi
	movl	40(%r12), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	jne	.L39
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v83Set3AddENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	jne	.L40
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r12
	cmpq	%rbx, %rax
	jne	.L43
.L44:
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7124:
	.size	_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE, .-_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE
	.section	.rodata.str1.1
.LC5:
	.string	"compiledWithCache"
.LC6:
	.string	"compiledWithoutCache"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv13GetCacheUsageERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node13native_module15NativeModuleEnv13GetCacheUsageERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node13native_module15NativeModuleEnv13GetCacheUsageERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L58
	cmpw	$1040, %cx
	jne	.L49
.L58:
	movq	23(%rdx), %rbx
.L51:
	movq	352(%rbx), %r14
	movq	3280(%rbx), %r15
	movq	%r14, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	leaq	8(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L60
.L52:
	movq	3280(%rbx), %rsi
	movq	%r8, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L61
.L53:
	movq	%r15, %rdi
	leaq	56(%rbx), %rsi
	call	_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L62
.L54:
	movq	3280(%rbx), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L63
	movq	0(%r13), %rax
	testq	%r12, %r12
	je	.L64
.L56:
	movq	(%r12), %rdx
.L57:
	movq	%rdx, 24(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r8, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r8
	movq	-56(%rbp), %rdx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L61:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L63:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	0(%r13), %rax
	testq	%r12, %r12
	jne	.L56
	.p2align 4,,10
	.p2align 3
.L64:
	movq	16(%rax), %rdx
	jmp	.L57
	.cfi_endproc
.LFE7135:
	.size	_ZN4node13native_module15NativeModuleEnv13GetCacheUsageERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node13native_module15NativeModuleEnv13GetCacheUsageERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv6ExistsEPKc
	.type	_ZN4node13native_module15NativeModuleEnv6ExistsEPKc, @function
_ZN4node13native_module15NativeModuleEnv6ExistsEPKc:
.LFB7125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node13native_module18NativeModuleLoader6ExistsEPKc@PLT
	.cfi_endproc
.LFE7125:
	.size	_ZN4node13native_module15NativeModuleEnv6ExistsEPKc, .-_ZN4node13native_module15NativeModuleEnv6ExistsEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv15GetSourceObjectEN2v85LocalINS2_7ContextEEE
	.type	_ZN4node13native_module15NativeModuleEnv15GetSourceObjectEN2v85LocalINS2_7ContextEEE, @function
_ZN4node13native_module15NativeModuleEnv15GetSourceObjectEN2v85LocalINS2_7ContextEEE:
.LFB7126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node13native_module18NativeModuleLoader15GetSourceObjectEN2v85LocalINS2_7ContextEEE@PLT
	.cfi_endproc
.LFE7126:
	.size	_ZN4node13native_module15NativeModuleEnv15GetSourceObjectEN2v85LocalINS2_7ContextEEE, .-_ZN4node13native_module15NativeModuleEnv15GetSourceObjectEN2v85LocalINS2_7ContextEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv15GetConfigStringEPN2v87IsolateE
	.type	_ZN4node13native_module15NativeModuleEnv15GetConfigStringEPN2v87IsolateE, @function
_ZN4node13native_module15NativeModuleEnv15GetConfigStringEPN2v87IsolateE:
.LFB7127:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	addq	$8, %rsp
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node13native_module18NativeModuleLoader15GetConfigStringEPN2v87IsolateE@PLT
	.cfi_endproc
.LFE7127:
	.size	_ZN4node13native_module15NativeModuleEnv15GetConfigStringEPN2v87IsolateE, .-_ZN4node13native_module15NativeModuleEnv15GetConfigStringEPN2v87IsolateE
	.p2align 4
	.globl	_Z23_register_native_modulev
	.type	_Z23_register_native_modulev, @function
_Z23_register_native_modulev:
.LFB7151:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7151:
	.size	_Z23_register_native_modulev, .-_Z23_register_native_modulev
	.section	.text._ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE,"axG",@progbits,_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE,comdat
	.p2align 4
	.weak	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	.type	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE, @function
_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE:
.LFB7841:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1128, %rsp
	movq	%rdi, -1144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L126
.L73:
	leaq	-1136(%rbp), %r14
	movq	%r12, %rsi
	leaq	-1080(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	(%rbx), %rcx
	movq	8(%rbx), %r15
	movq	%r13, %rax
	movdqa	.LC7(%rip), %xmm0
	movq	%r13, -1088(%rbp)
	leaq	-56(%rbp), %rdx
	subq	%rcx, %r15
	sarq	$5, %r15
	movaps	%xmm0, -1104(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L74:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L74
	movq	$0, -1080(%rbp)
	cmpq	$128, %r15
	jbe	.L81
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r15,8), %rdi
	andq	%r15, %rax
	cmpq	%rax, %r15
	jne	.L127
	movq	%rcx, -1160(%rbp)
	movq	%r9, -1152(%rbp)
	testq	%rdi, %rdi
	je	.L77
	movq	%rdi, -1168(%rbp)
	call	malloc@PLT
	movq	-1168(%rbp), %rdi
	movq	-1152(%rbp), %r9
	testq	%rax, %rax
	movq	-1160(%rbp), %rcx
	je	.L128
	movq	%rax, -1088(%rbp)
	movq	%r15, -1096(%rbp)
.L81:
	movq	%r9, -1104(%rbp)
	testq	%r9, %r9
	je	.L82
	xorl	%r15d, %r15d
	testq	%r12, %r12
	jne	.L87
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rax, (%rdx)
	movq	(%rbx), %rcx
	addq	$1, %r15
	movq	8(%rbx), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%r15, %rax
	jbe	.L96
.L87:
	movq	%r15, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movq	8(%rax), %rcx
	cmpq	$1073741798, %rcx
	ja	.L84
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r9
	cmpq	%r15, %r9
	jbe	.L92
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	testq	%rax, %rax
	jne	.L129
	jmp	.L97
.L122:
	movq	%rax, (%rdx)
	movq	(%rbx), %rcx
	addq	$1, %r15
	movq	8(%rbx), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%rax, %r15
	jnb	.L96
.L83:
	movq	%r15, %rsi
	movq	-1144(%rbp), %rdi
	salq	$5, %rsi
	addq	%rcx, %rsi
	movq	%rsi, -1152(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-1152(%rbp), %rsi
	movq	%rax, %rdi
	movq	8(%rsi), %rcx
	cmpq	$1073741798, %rcx
	ja	.L130
	movq	(%rsi), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r9
	cmpq	%r15, %r9
	jbe	.L92
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	testq	%rax, %rax
	jne	.L122
	jmp	.L97
.L92:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L130:
	movq	%rax, %r12
.L84:
	movq	%r12, %rdi
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
	cmpq	-1104(%rbp), %r15
	jnb	.L92
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
.L97:
	movq	$0, (%rdx)
	xorl	%r12d, %r12d
.L95:
	testq	%rsi, %rsi
	je	.L94
	cmpq	%r13, %rsi
	je	.L94
	movq	%rsi, %rdi
	call	free@PLT
.L94:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	addq	$1128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	movq	-1088(%rbp), %rsi
.L96:
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-1088(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L95
.L126:
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r12
	jmp	.L73
.L77:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L128:
	movq	%rdi, -1152(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-1152(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L77
	movq	(%rbx), %rcx
	movq	8(%rbx), %r9
	movq	%rdi, -1088(%rbp)
	movq	-1104(%rbp), %rax
	movq	%r15, -1096(%rbp)
	subq	%rcx, %r9
	sarq	$5, %r9
	testq	%rax, %rax
	je	.L80
	leaq	0(,%rax,8), %rdx
	movq	%r13, %rsi
	movq	%r9, -1160(%rbp)
	movq	%rcx, -1152(%rbp)
	call	memcpy@PLT
	movq	-1160(%rbp), %r9
	movq	-1152(%rbp), %rcx
.L80:
	movq	%r15, -1104(%rbp)
	cmpq	%r9, %r15
	jnb	.L81
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L127:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7841:
	.size	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE, .-_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv15ModuleIdsGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node13native_module15NativeModuleEnv15ModuleIdsGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node13native_module15NativeModuleEnv15ModuleIdsGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB7136:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	16(%rax), %r14
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN4node13native_module18NativeModuleLoader12GetModuleIdsB5cxx11Ev@PLT
	movq	%r14, %rdi
	movq	(%r12), %rbx
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	testq	%rax, %rax
	je	.L146
	movq	(%rax), %rax
	movq	%rax, 32(%rbx)
.L140:
	movq	-56(%rbp), %rbx
	movq	-64(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L134
	.p2align 4,,10
	.p2align 3
.L138:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L135
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L138
.L136:
	movq	-64(%rbp), %r12
.L134:
	testq	%r12, %r12
	je	.L132
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L132:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L147
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L138
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L146:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	24(%rbx), %rax
	movq	%rax, 32(%rbx)
	jmp	.L140
.L147:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7136:
	.size	_ZN4node13native_module15NativeModuleEnv15ModuleIdsGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node13native_module15NativeModuleEnv15ModuleIdsGetterEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8216:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L163
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L152:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L150
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L148
.L151:
	movq	%rbx, %r12
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L151
.L148:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8216:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_:
.LFB8223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%rax, -56(%rbp)
	testq	%rbx, %rbx
	je	.L167
	movq	8(%rsi), %r15
	movq	(%rsi), %r11
	movabsq	$-2147483649, %r14
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L173:
	movq	16(%rbx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L169
.L207:
	movq	%rax, %rbx
.L168:
	movq	40(%rbx), %r8
	movq	32(%rbx), %r10
	cmpq	%r8, %r15
	movq	%r8, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L170
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r8, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	jne	.L171
.L170:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r8, %rax
	cmpq	%rcx, %rax
	jge	.L172
	cmpq	%r14, %rax
	jle	.L173
.L171:
	testl	%eax, %eax
	js	.L173
.L172:
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L207
.L169:
	movq	%rbx, %r14
	testb	%sil, %sil
	jne	.L208
.L176:
	testq	%rdx, %rdx
	je	.L177
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L178
.L177:
	subq	%r15, %r8
	cmpq	$2147483647, %r8
	jg	.L179
	cmpq	$-2147483648, %r8
	jl	.L180
	movl	%r8d, %eax
.L178:
	testl	%eax, %eax
	js	.L180
.L179:
	addq	$56, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L209
	movl	$1, %r8d
	cmpq	%r14, -56(%rbp)
	jne	.L210
.L181:
	movl	$64, %edi
	movl	%r8d, -64(%rbp)
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movl	-64(%rbp), %r8d
	movq	%rax, %rbx
	leaq	48(%rax), %rax
	movq	%rax, 32(%rbx)
	leaq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L211
	movq	%rdx, 32(%rbx)
	movq	16(%r13), %rdx
	movq	%rdx, 48(%rbx)
.L185:
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	movq	%rbx, %rsi
	movl	%r8d, %edi
	movq	$0, 8(%r13)
	movq	-56(%rbp), %rcx
	movq	%rdx, 40(%rbx)
	movq	%r14, %rdx
	movb	$0, 16(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r12)
	addq	$56, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	cmpq	%rbx, 24(%r12)
	je	.L212
.L187:
	movq	%rbx, %rdi
	movq	%r11, -64(%rbp)
	movq	%rbx, %r14
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdx
	movq	-64(%rbp), %r11
	movq	40(%rax), %r8
	movq	32(%rax), %r10
	movq	%rax, %rbx
	cmpq	%r15, %r8
	cmovbe	%r8, %rdx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L211:
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 48(%rbx)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%rbx, %r14
	movl	$1, %r8d
	cmpq	%r14, -56(%rbp)
	je	.L181
.L210:
	movq	40(%r14), %rbx
	cmpq	%rbx, %r15
	movq	%rbx, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L182
	movq	32(%r14), %rsi
	movq	0(%r13), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L183
.L182:
	movq	%r15, %r9
	xorl	%r8d, %r8d
	subq	%rbx, %r9
	cmpq	$2147483647, %r9
	jg	.L181
	cmpq	$-2147483648, %r9
	jl	.L206
	movl	%r9d, %eax
.L183:
	shrl	$31, %eax
	movl	%eax, %r8d
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L167:
	leaq	8(%rdi), %rax
	cmpq	24(%rdi), %rax
	je	.L193
	movq	8(%rsi), %r15
	movq	(%rsi), %r11
	movq	%rax, %rbx
	jmp	.L187
.L193:
	leaq	8(%r12), %r14
.L206:
	movl	$1, %r8d
	jmp	.L181
.L209:
	xorl	%ebx, %ebx
	jmp	.L179
	.cfi_endproc
.LFE8223:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv12RecordResultEPKcNS0_18NativeModuleLoader6ResultEPNS_11EnvironmentE
	.type	_ZN4node13native_module15NativeModuleEnv12RecordResultEPKcNS0_18NativeModuleLoader6ResultEPNS_11EnvironmentE, @function
_ZN4node13native_module15NativeModuleEnv12RecordResultEPKcNS0_18NativeModuleLoader6ResultEPNS_11EnvironmentE:
.LFB7144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.L214
	leaq	-80(%rbp), %r14
	leaq	-96(%rbp), %r15
	movq	%r14, -96(%rbp)
	testq	%rdi, %rdi
	je	.L222
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L236
	cmpq	$1, %rax
	jne	.L218
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%r14, %rdx
.L219:
	movq	%rax, -88(%rbp)
	leaq	8(%rbx), %rdi
	movq	%r15, %rsi
	movb	$0, (%rdx,%rax)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	movq	-96(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L213
.L235:
	call	_ZdlPv@PLT
.L213:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L237
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	leaq	-112(%rbp), %r14
	leaq	-128(%rbp), %r15
	movq	%r14, -128(%rbp)
	testq	%rdi, %rdi
	je	.L222
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L238
	cmpq	$1, %rax
	jne	.L225
	movzbl	(%r12), %edx
	movb	%dl, -112(%rbp)
	movq	%r14, %rdx
.L226:
	movq	%rax, -120(%rbp)
	leaq	56(%rbx), %rdi
	movq	%r15, %rsi
	movb	$0, (%rdx,%rax)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L235
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	.LC8(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L225:
	testq	%rax, %rax
	jne	.L239
	movq	%r14, %rdx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L218:
	testq	%rax, %rax
	jne	.L240
	movq	%r14, %rdx
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%r15, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -80(%rbp)
.L217:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L238:
	movq	%r15, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L224:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L226
.L237:
	call	__stack_chk_fail@PLT
.L240:
	movq	%r14, %rdi
	jmp	.L217
.L239:
	movq	%r14, %rdi
	jmp	.L224
	.cfi_endproc
.LFE7144:
	.size	_ZN4node13native_module15NativeModuleEnv12RecordResultEPKcNS0_18NativeModuleLoader6ResultEPNS_11EnvironmentE, .-_ZN4node13native_module15NativeModuleEnv12RecordResultEPKcNS0_18NativeModuleLoader6ResultEPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7145:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$1072, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L252
	cmpw	$1040, %cx
	jne	.L242
.L252:
	movq	23(%rdx), %r13
.L244:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L245
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L246:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L263
.L247:
	leaq	_ZZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L263:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L247
	movq	352(%r13), %rsi
	leaq	-1088(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1072(%rbp), %r14
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	movq	3280(%r13), %rsi
	leaq	-1092(%rbp), %rcx
	movq	%rax, %rdi
	movq	%r14, %rdx
	call	_ZN4node13native_module18NativeModuleLoader15CompileAsModuleEN2v85LocalINS2_7ContextEEEPKcPNS1_6ResultE@PLT
	movl	-1092(%rbp), %esi
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN4node13native_module15NativeModuleEnv12RecordResultEPKcNS0_18NativeModuleLoader6ResultEPNS_11EnvironmentE
	testq	%r12, %r12
	je	.L249
	movq	(%r12), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L249:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L241
	testq	%rdi, %rdi
	je	.L241
	call	free@PLT
.L241:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L264
	addq	$1072, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	movq	8(%rbx), %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L244
.L264:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7145:
	.size	_ZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS_11EnvironmentE
	.type	_ZN4node13native_module15NativeModuleEnv16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS_11EnvironmentE, @function
_ZN4node13native_module15NativeModuleEnv16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS_11EnvironmentE:
.LFB7149:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	movq	%r14, %rsi
	leaq	-44(%rbp), %r8
	movq	%r15, %rcx
	movq	%rax, %rdi
	movq	%r12, %rdx
	call	_ZN4node13native_module18NativeModuleLoader16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS1_6ResultE@PLT
	movq	%rax, %r14
	testq	%r13, %r13
	je	.L266
	movl	-44(%rbp), %esi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN4node13native_module15NativeModuleEnv12RecordResultEPKcNS0_18NativeModuleLoader6ResultEPNS_11EnvironmentE
.L266:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L272:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7149:
	.size	_ZN4node13native_module15NativeModuleEnv16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS_11EnvironmentE, .-_ZN4node13native_module15NativeModuleEnv16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS_11EnvironmentE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_:
.LFB8531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%rax, -64(%rbp)
	testq	%rbx, %rbx
	je	.L274
	movq	8(%rsi), %r13
	movq	(%rsi), %r12
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%r9, %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r9, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r9
	testl	%eax, %eax
	jne	.L333
	movq	%r15, %rax
	movl	$2147483648, %esi
	subq	%r13, %rax
	cmpq	%rsi, %rax
	jge	.L301
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L279
.L335:
	testl	%eax, %eax
	js	.L279
	testq	%r14, %r14
	je	.L282
.L301:
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L283
.L282:
	movq	%r13, %rax
	movl	$2147483648, %ecx
	subq	%r15, %rax
	cmpq	%rcx, %rax
	jge	.L284
	movabsq	$-2147483649, %rdi
	cmpq	%rdi, %rax
	jle	.L285
.L283:
	testl	%eax, %eax
	jns	.L284
.L285:
	movq	%rbx, -64(%rbp)
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L274
.L275:
	movq	40(%rbx), %r15
	movq	%r13, %r14
	movq	32(%rbx), %r9
	cmpq	%r13, %r15
	cmovbe	%r15, %r14
	testq	%r14, %r14
	jne	.L334
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r13, %rax
	cmpq	%rcx, %rax
	jge	.L282
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jg	.L335
	.p2align 4,,10
	.p2align 3
.L279:
	movq	24(%rbx), %rbx
.L336:
	testq	%rbx, %rbx
	jne	.L275
.L274:
	movq	-64(%rbp), %rax
.L329:
	movq	-64(%rbp), %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	jns	.L301
	movq	24(%rbx), %rbx
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L284:
	movq	24(%rbx), %r14
	movq	16(%rbx), %r15
	testq	%r14, %r14
	jne	.L287
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L293:
	movq	%r14, -64(%rbp)
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.L299
.L287:
	movq	40(%r14), %r10
	cmpq	%r10, %r13
	movq	%r10, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L290
	movq	32(%r14), %rsi
	movq	%r12, %rdi
	movq	%r10, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r10
	testl	%eax, %eax
	jne	.L291
.L290:
	movq	%r13, %rax
	movl	$2147483648, %ecx
	subq	%r10, %rax
	cmpq	%rcx, %rax
	jge	.L292
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L293
.L291:
	testl	%eax, %eax
	js	.L293
.L292:
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.L287
	.p2align 4,,10
	.p2align 3
.L299:
	testq	%r15, %r15
	je	.L288
.L337:
	movq	40(%r15), %r14
	cmpq	%r14, %r13
	movq	%r14, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L295
	movq	32(%r15), %rdi
	movq	%r12, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L296
.L295:
	movq	%r14, %r9
	movl	$2147483648, %eax
	subq	%r13, %r9
	cmpq	%rax, %r9
	jge	.L297
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r9
	jle	.L298
	movl	%r9d, %eax
.L296:
	testl	%eax, %eax
	jns	.L297
.L298:
	movq	24(%r15), %r15
	testq	%r15, %r15
	jne	.L337
.L288:
	movq	%rbx, %rax
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L297:
	movq	%r15, %rbx
	movq	16(%r15), %r15
	jmp	.L299
	.cfi_endproc
.LFE8531:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_:
.LFB8218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE11equal_rangeERKS5_
	movq	40(%rbx), %r14
	movq	%rdx, %r12
	movq	%rax, %rdi
	cmpq	24(%rbx), %rax
	je	.L360
.L339:
	leaq	8(%rbx), %r15
	cmpq	%r12, %rax
	jne	.L350
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%r13, %rdi
.L350:
	movq	%rdi, -56(%rbp)
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	-56(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rax, %r13
	call	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_@PLT
	movq	32(%rax), %rdi
	movq	%rax, %r8
	leaq	48(%rax), %rax
	cmpq	%rax, %rdi
	je	.L347
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L347:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	movq	40(%rbx), %rax
	subq	$1, %rax
	movq	%rax, 40(%rbx)
	cmpq	%r13, %r12
	jne	.L362
	subq	%rax, %r14
.L342:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	.cfi_restore_state
	leaq	8(%rbx), %r13
	cmpq	%r13, %rdx
	jne	.L339
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L345
.L343:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r15
	cmpq	%rax, %rdi
	je	.L344
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	je	.L345
.L346:
	movq	%r15, %r12
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L344:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r15, %r15
	jne	.L346
.L345:
	movq	$0, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	%r13, 32(%rbx)
	movq	$0, 40(%rbx)
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L361:
	xorl	%r14d, %r14d
	jmp	.L342
	.cfi_endproc
.LFE8218:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_:
.LFB8800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -72(%rbp)
	movl	$64, %edi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	32(%rbx), %r9
	movq	40(%rbx), %r8
	leaq	48(%rax), %rdi
	movq	%rax, %r12
	movq	%rdi, 32(%rax)
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L364
	testq	%r9, %r9
	je	.L371
.L364:
	movq	%r8, -64(%rbp)
	cmpq	$15, %r8
	ja	.L409
	cmpq	$1, %r8
	jne	.L367
	movzbl	(%r9), %eax
	movb	%al, 48(%r12)
.L368:
	movq	%r8, 40(%r12)
	movb	$0, (%rdi,%r8)
	movl	(%rbx), %eax
	movq	24(%rbx), %rsi
	movq	%r15, 8(%r12)
	movl	%eax, (%r12)
	movq	$0, 16(%r12)
	movq	$0, 24(%r12)
	testq	%rsi, %rsi
	je	.L369
	movq	-72(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r12, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r12)
.L369:
	movq	16(%rbx), %r15
	leaq	-64(%rbp), %rax
	movq	%r12, %rbx
	movq	%rax, -96(%rbp)
	testq	%r15, %r15
	je	.L363
.L370:
	movl	$64, %edi
	call	_Znwm@PLT
	movq	40(%r15), %r9
	leaq	48(%rax), %rdi
	movq	%rax, %r13
	movq	%rdi, 32(%rax)
	movq	32(%r15), %r11
	movq	%r11, %rax
	addq	%r9, %rax
	je	.L382
	testq	%r11, %r11
	je	.L371
.L382:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L410
	cmpq	$1, %r9
	jne	.L375
	movzbl	(%r11), %eax
	movb	%al, 48(%r13)
.L376:
	movq	%r9, 40(%r13)
	movb	$0, (%rdi,%r9)
	movl	(%r15), %eax
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movl	%eax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	%rbx, 8(%r13)
	movq	24(%r15), %rsi
	testq	%rsi, %rsi
	je	.L377
	movq	-72(%rbp), %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	%rax, 24(%r13)
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.L363
.L379:
	movq	%r13, %rbx
	jmp	.L370
	.p2align 4,,10
	.p2align 3
.L367:
	testq	%r8, %r8
	je	.L368
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L377:
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L379
.L363:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L411
	addq	$56, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	testq	%r9, %r9
	je	.L376
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L410:
	movq	-96(%rbp), %rsi
	leaq	32(%r13), %rdi
	xorl	%edx, %edx
	movq	%r11, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r11
	movq	%rax, 32(%r13)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r13)
.L374:
	movq	%r9, %rdx
	movq	%r11, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	movq	32(%r13), %rdi
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	32(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -88(%rbp)
	movq	%r9, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r8
	movq	%rax, 32(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%r12)
.L366:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	32(%r12), %rdi
	jmp	.L368
.L411:
	call	__stack_chk_fail@PLT
.L371:
	leaq	.LC8(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE8800:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	.section	.rodata.str1.1
.LC9:
	.string	"cannotBeRequired"
.LC10:
	.string	"canBeRequired"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13native_module15NativeModuleEnv19GetModuleCategoriesEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node13native_module15NativeModuleEnv19GetModuleCategoriesEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node13native_module15NativeModuleEnv19GetModuleCategoriesEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB7128:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rsi), %rdi
	movq	%rsi, -224(%rbp)
	movq	40(%rdi), %rdx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L440
	cmpw	$1040, %cx
	jne	.L413
.L440:
	movq	23(%rdx), %rax
	movq	%rax, -216(%rbp)
.L415:
	movq	352(%rax), %r15
	movq	3280(%rax), %r12
	leaq	-192(%rbp), %r14
	leaq	-144(%rbp), %r13
	movq	%r15, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rbx
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	movq	%rax, %rdi
	call	_ZN4node13native_module18NativeModuleLoader19GetCannotBeRequiredB5cxx11Ev@PLT
	leaq	-184(%rbp), %rdx
	movq	$0, -176(%rbp)
	movq	%rdx, -168(%rbp)
	movq	%rdx, -160(%rbp)
	movq	16(%rax), %rsi
	movl	$0, -184(%rbp)
	movq	$0, -152(%rbp)
	movq	%rax, -232(%rbp)
	testq	%rsi, %rsi
	je	.L416
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	%r14, -144(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	-232(%rbp), %r8
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L417:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L417
	movq	%rcx, -168(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L418:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L418
	movq	40(%r8), %rdx
	movq	%rcx, -160(%rbp)
	movq	%rax, -176(%rbp)
	movq	%rdx, -152(%rbp)
.L416:
	call	_ZN4node13native_module18NativeModuleLoader11GetInstanceEv@PLT
	movq	%rax, %rdi
	call	_ZN4node13native_module18NativeModuleLoader16GetCanBeRequiredB5cxx11Ev@PLT
	leaq	-136(%rbp), %rdx
	movq	$0, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	16(%rax), %rsi
	movl	$0, -136(%rbp)
	movq	$0, -104(%rbp)
	movq	%rax, -232(%rbp)
	testq	%rsi, %rsi
	je	.L419
	leaq	-200(%rbp), %rcx
	movq	%r13, %rdi
	movq	%r13, -200(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE7_M_copyINSB_11_Alloc_nodeEEEPSt13_Rb_tree_nodeIS5_EPKSF_PSt18_Rb_tree_node_baseRT_
	movq	-232(%rbp), %r8
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L420:
	movq	%rdx, %rcx
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L420
	movq	%rcx, -120(%rbp)
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%rdx, %rcx
	movq	24(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L421
	movq	40(%r8), %rdx
	movq	%rcx, -112(%rbp)
	movq	%rax, -128(%rbp)
	movq	%rdx, -104(%rbp)
.L419:
	movq	-216(%rbp), %rax
	testb	$2, 1932(%rax)
	je	.L470
.L422:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, -216(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-216(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L471
.L425:
	movq	%r8, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L472
.L426:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node13native_module7ToJsSetEN2v85LocalINS1_7ContextEEERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessISB_ESaISB_EE
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -216(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-216(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L473
.L427:
	movq	%r8, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L474
	movq	-224(%rbp), %rax
	movq	(%rax), %rax
	testq	%rbx, %rbx
	je	.L475
.L429:
	movq	(%rbx), %rdx
.L430:
	movq	-128(%rbp), %r12
	movq	%rdx, 32(%rax)
	testq	%r12, %r12
	je	.L431
.L434:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L432
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L431
.L433:
	movq	%rbx, %r12
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L432:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L433
.L431:
	movq	-176(%rbp), %r12
	testq	%r12, %r12
	je	.L412
.L438:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L436
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L412
.L437:
	movq	%rbx, %r12
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L437
.L412:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L476
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L470:
	.cfi_restore_state
	leaq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rax
	movq	%r13, %rdi
	movb	$0, -68(%rbp)
	movabsq	$8531329958186938996, %rcx
	movq	%rsi, -232(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rcx, -80(%rbp)
	movl	$1937010277, -72(%rbp)
	movq	$12, -88(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE5eraseERKS5_
	movq	-96(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	movq	-232(%rbp), %rsi
	je	.L423
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rsi
.L423:
	movq	-216(%rbp), %rax
	movq	%r14, %rdi
	movb	$0, -68(%rbp)
	movabsq	$8531329958186938996, %rcx
	movq	%rcx, -80(%rbp)
	movq	%rax, -96(%rbp)
	movl	$1937010277, -72(%rbp)
	movq	$12, -88(%rbp)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	movq	-96(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L422
	call	_ZdlPv@PLT
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L413:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -216(%rbp)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L472:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L473:
	movq	%r8, -232(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r8, -232(%rbp)
	movq	%rax, -216(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-232(%rbp), %r8
	movq	-216(%rbp), %rdx
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L474:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-224(%rbp), %rax
	movq	(%rax), %rax
	testq	%rbx, %rbx
	jne	.L429
	.p2align 4,,10
	.p2align 3
.L475:
	movq	24(%rax), %rdx
	jmp	.L430
.L476:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7128:
	.size	_ZN4node13native_module15NativeModuleEnv19GetModuleCategoriesEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node13native_module15NativeModuleEnv19GetModuleCategoriesEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args
	.section	.rodata.str1.1
.LC11:
	.string	"../src/util.h:352"
.LC12:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC14:
	.string	"../src/util.h:391"
.LC15:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/util-inl.h:374"
.LC18:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC20:
	.string	"../src/util-inl.h:325"
.LC21:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"../src/node_native_module_env.cc"
	.section	.rodata.str1.1
.LC24:
	.string	"native_module"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC23
	.quad	0
	.quad	_ZN4node13native_module15NativeModuleEnv10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC24
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"../src/node_native_module_env.cc:122"
	.section	.rodata.str1.1
.LC26:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"static void node::native_module::NativeModuleEnv::CompileFunction(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node13native_module15NativeModuleEnv15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.quad	0
	.quad	128
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
