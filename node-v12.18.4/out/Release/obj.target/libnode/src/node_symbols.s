	.file	"node_symbols.cc"
	.text
	.section	.text.unlikely,"ax",@progbits
.LCOLDB0:
	.text
.LHOTB0:
	.p2align 4
	.type	_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	testq	%rdx, %rdx
	je	.L2
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L2
	movq	271(%rax), %rbx
	movq	360(%rbx), %rax
	movq	128(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L15
.L3:
	movq	360(%rbx), %rax
	movq	136(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L16
.L4:
	movq	360(%rbx), %rax
	movq	144(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L17
.L5:
	movq	360(%rbx), %rax
	movq	152(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L18
.L6:
	movq	360(%rbx), %rax
	movq	160(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L19
.L7:
	movq	360(%rbx), %rax
	movq	168(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L20
.L8:
	movq	360(%rbx), %rax
	movq	176(%rax), %r13
	movq	%r13, %rdi
	call	_ZNK2v86Symbol4NameEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L21
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L17:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L18:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L20:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7080:
.L2:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7080:
	.text
	.size	_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE0:
	.text
.LHOTE0:
	.p2align 4
	.globl	_Z17_register_symbolsv
	.type	_Z17_register_symbolsv, @function
_Z17_register_symbolsv:
.LFB7081:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7081:
	.size	_Z17_register_symbolsv, .-_Z17_register_symbolsv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"../src/node_symbols.cc"
.LC2:
	.string	"symbols"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC1
	.quad	0
	.quad	_ZN4node7symbolsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC2
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
