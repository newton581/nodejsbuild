	.file	"js_native_api_v8.cc"
	.text
	.section	.text._ZNK10napi_env__16can_call_into_jsEv,"axG",@progbits,_ZNK10napi_env__16can_call_into_jsEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK10napi_env__16can_call_into_jsEv
	.type	_ZNK10napi_env__16can_call_into_jsEv, @function
_ZNK10napi_env__16can_call_into_jsEv:
.LFB7484:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7484:
	.size	_ZNK10napi_env__16can_call_into_jsEv, .-_ZNK10napi_env__16can_call_into_jsEv
	.section	.text._ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE,"axG",@progbits,_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.type	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE, @function
_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE:
.LFB7485:
	.cfi_startproc
	endbr64
	movl	$257, %eax
	ret
	.cfi_endproc
.LFE7485:
	.size	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE, .-_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.text
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_19Reference18SecondPassCallbackERKN2v816WeakCallbackInfoIS1_EE, @function
_ZN6v8impl12_GLOBAL__N_19Reference18SecondPassCallbackERKN2v816WeakCallbackInfoIS1_EE:
.LFB7543:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%esi, %esi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmp	*%rax
	.cfi_endproc
.LFE7543:
	.size	_ZN6v8impl12_GLOBAL__N_19Reference18SecondPassCallbackERKN2v816WeakCallbackInfoIS1_EE, .-_ZN6v8impl12_GLOBAL__N_19Reference18SecondPassCallbackERKN2v816WeakCallbackInfoIS1_EE
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper12GetNewTargetEv, @function
_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper12GetNewTargetEv:
.LFB7562:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	movq	40(%rax), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L6
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L9
.L6:
	addq	$40, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$5, 43(%rdx)
	jne	.L6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7562:
	.size	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper12GetNewTargetEv, .-_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper12GetNewTargetEv
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper4ArgsEPP12napi_value__m, @function
_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper4ArgsEPP12napi_value__m:
.LFB7563:
	.cfi_startproc
	endbr64
	cmpq	%rdx, 16(%rdi)
	movq	%rdx, %r8
	cmovbe	16(%rdi), %r8
	testq	%r8, %r8
	je	.L11
	movq	32(%rdi), %r9
	xorl	%eax, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	%eax, 16(%r9)
	jle	.L12
	movq	8(%r9), %r10
	salq	$3, %rcx
	subq	%rcx, %r10
	movq	%r10, %rcx
	movq	%rcx, (%rsi,%rax,8)
	addq	$1, %rax
	cmpq	%r8, %rax
	je	.L11
.L15:
	movslq	%eax, %rcx
	testl	%eax, %eax
	jns	.L28
.L12:
	movq	(%r9), %rcx
	movq	8(%rcx), %rcx
	addq	$88, %rcx
	movq	%rcx, (%rsi,%rax,8)
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L15
.L11:
	cmpq	%r8, %rdx
	jbe	.L10
	movq	32(%rdi), %rax
	subq	%r8, %rdx
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	cmpq	$1, %rdx
	je	.L17
	movq	%rdx, %rcx
	movq	%rdi, %xmm0
	leaq	(%rsi,%r8,8), %rax
	shrq	%rcx
	punpcklqdq	%xmm0, %xmm0
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L18:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L18
	movq	%rdx, %rax
	andq	$-2, %rax
	addq	%rax, %r8
	cmpq	%rdx, %rax
	je	.L10
.L17:
	movq	%rdi, (%rsi,%r8,8)
.L10:
	ret
	.cfi_endproc
.LFE7563:
	.size	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper4ArgsEPP12napi_value__m, .-_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper4ArgsEPP12napi_value__m
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEE12GetNewTargetEv, @function
_ZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEE12GetNewTargetEv:
.LFB9990:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9990:
	.size	_ZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEE12GetNewTargetEv, .-_ZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEE12GetNewTargetEv
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper14SetReturnValueEP12napi_value__, @function
_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper14SetReturnValueEP12napi_value__:
.LFB7564:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	movq	(%rax), %rax
	testq	%rsi, %rsi
	je	.L33
	movq	(%rsi), %rdx
	movq	%rdx, 24(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	ret
	.cfi_endproc
.LFE7564:
	.size	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper14SetReturnValueEP12napi_value__, .-_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper14SetReturnValueEP12napi_value__
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE, @function
_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE:
.LFB7542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L35
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 72(%r12)
.L35:
	movq	16(%rbx), %rax
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference18SecondPassCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%rdx, (%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7542:
	.size	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE, .-_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE
	.section	.text._ZN10napi_env__D2Ev,"axG",@progbits,_ZN10napi_env__D5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN10napi_env__D2Ev
	.type	_ZN10napi_env__D2Ev, @function
_ZN10napi_env__D2Ev:
.LFB7478:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L60
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rdi), %rax
	movl	$1, %esi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L41
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L61
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdi), %rax
	movl	$1, %esi
	call	*16(%rax)
.L60:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L43
.L61:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L45
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L45:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L40
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7478:
	.size	_ZN10napi_env__D2Ev, .-_ZN10napi_env__D2Ev
	.weak	_ZN10napi_env__D1Ev
	.set	_ZN10napi_env__D1Ev,_ZN10napi_env__D2Ev
	.text
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_1L20DeleteCallbackBundleEP10napi_env__PvS3_, @function
_ZN6v8impl12_GLOBAL__N_1L20DeleteCallbackBundleEP10napi_env__PvS3_:
.LFB7577:
	.cfi_startproc
	endbr64
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L62
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	ret
	.cfi_endproc
.LFE7577:
	.size	_ZN6v8impl12_GLOBAL__N_1L20DeleteCallbackBundleEP10napi_env__PvS3_, .-_ZN6v8impl12_GLOBAL__N_1L20DeleteCallbackBundleEP10napi_env__PvS3_
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7558:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movslq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	%rdi, -64(%rbp)
	movq	(%rdi), %rdi
	movq	%rdx, -80(%rbp)
	addq	$8, %rax
	movq	$0, -72(%rbp)
	addq	$32, %rdi
	movq	%rax, -88(%rbp)
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEEE(%rip), %rax
	movq	%rax, -96(%rbp)
	call	_ZNK2v88External5ValueEv@PLT
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_123FunctionCallbackWrapperE(%rip), %rcx
	leaq	-96(%rbp), %rsi
	movq	(%rax), %rbx
	movq	8(%rax), %rdx
	movq	%rax, -56(%rbp)
	movq	%rcx, -96(%rbp)
	movl	104(%rbx), %r13d
	movl	108(%rbx), %r14d
	movq	%rdx, -72(%rbp)
	movq	%rbx, %rdi
	movq	16(%rax), %rax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	call	*%rax
	cmpl	104(%rbx), %r13d
	jne	.L79
	cmpl	108(%rbx), %r14d
	jne	.L80
	movq	%rax, %r12
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L68
	movq	8(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L68:
	testq	%r12, %r12
	je	.L64
	movq	-64(%rbp), %rax
	movq	(%r12), %rdx
	movq	(%rax), %rax
	movq	%rdx, 24(%rax)
.L64:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L81
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L81:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7558:
	.size	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv, @function
_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv:
.LFB7578:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %xmm1
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	$32, %edi
	subq	$16, %rsp
	movaps	%xmm0, -48(%rbp)
	call	_Znwm@PLT
	movdqa	-48(%rbp), %xmm0
	movq	8(%rbx), %rdi
	movq	%r12, 16(%rax)
	movq	%rax, %rsi
	movq	%rax, %r13
	movq	$0, 24(%rax)
	movups	%xmm0, (%rax)
	call	_ZN2v88External3NewEPNS_7IsolateEPv@PLT
	movl	$80, %edi
	movq	%rax, %r14
	call	_Znwm@PLT
	leaq	56(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, 24(%rax)
	movq	%rax, %r12
	leaq	_ZN6v8impl12_GLOBAL__N_1L20DeleteCallbackBundleEP10napi_env__PvS3_(%rip), %rax
	movq	%rdx, %xmm2
	movq	%rax, 32(%r12)
	movq	64(%rbx), %rax
	movq	%r13, 40(%r12)
	movq	%rax, %xmm0
	movw	%cx, 56(%r12)
	punpcklqdq	%xmm2, %xmm0
	movb	$1, 64(%r12)
	movq	$0, 48(%r12)
	movl	$0, 60(%r12)
	movups	%xmm0, 8(%r12)
	testq	%rax, %rax
	je	.L83
	movq	%r12, 16(%rax)
.L83:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%r12, 64(%rbx)
	movq	8(%rbx), %rdi
	movq	%rax, (%r12)
	testq	%r14, %r14
	je	.L91
	movq	%r14, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movl	60(%r12), %edx
	movq	%rax, 72(%r12)
	testl	%edx, %edx
	je	.L85
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	$0, 72(%r12)
	xorl	%eax, %eax
.L85:
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	xorl	%ecx, %ecx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7578:
	.size	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv, .-_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	.section	.text._ZN10napi_env__D0Ev,"axG",@progbits,_ZN10napi_env__D5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN10napi_env__D0Ev
	.type	_ZN10napi_env__D0Ev, @function
_ZN10napi_env__D0Ev:
.LFB7480:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	64(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L115
	.p2align 4,,10
	.p2align 3
.L93:
	movq	(%rdi), %rax
	movl	$1, %esi
	call	*16(%rax)
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L93
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L116
	.p2align 4,,10
	.p2align 3
.L95:
	movq	(%rdi), %rax
	movl	$1, %esi
	call	*16(%rax)
.L115:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L95
.L116:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L97
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L97:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L98
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L98:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7480:
	.size	_ZN10napi_env__D0Ev, .-_ZN10napi_env__D0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEb, @function
_ZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEb:
.LFB7535:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rdi)
	je	.L118
	movq	24(%rdi), %rax
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	24(%r12), %rbx
	movl	108(%rbx), %eax
	movl	104(%rbx), %r15d
	movq	%rbx, %rdi
	movq	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	48(%r12), %rdx
	movl	%eax, -84(%rbp)
	movq	40(%r12), %rsi
	call	*32(%r12)
	cmpl	104(%rbx), %r15d
	jne	.L142
	movl	-84(%rbp), %eax
	cmpl	108(%rbx), %eax
	jne	.L143
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L122
	movq	8(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L122
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L122:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L118:
	movzbl	64(%r12), %edx
	testb	%dl, %dl
	jne	.L131
	testb	%r13b, %r13b
	jne	.L131
	movb	$1, 56(%r12)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L131:
	movq	16(%r12), %rcx
	movq	8(%r12), %rax
	testq	%rcx, %rcx
	je	.L126
	movq	%rax, 8(%rcx)
	movq	8(%r12), %rax
.L126:
	testq	%rax, %rax
	je	.L127
	movq	%rcx, 16(%rax)
.L127:
	movl	60(%r12), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	testl	%eax, %eax
	jne	.L128
	testb	%dl, %dl
	je	.L144
.L128:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L117:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L145
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	cmpb	$0, 56(%r12)
	jne	.L128
	movb	$1, 64(%r12)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L145:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7535:
	.size	_ZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEb, .-_ZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEb
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEb, @function
_ZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEb:
.LFB7546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%sil, %sil
	je	.L147
	movq	24(%rdi), %rax
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	72(%r12), %rax
	testq	%rax, %rax
	je	.L149
	movq	24(%r12), %rdx
	movq	(%rax), %rsi
	movq	8(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L149
	movq	%rax, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	je	.L182
	movq	%r14, %rdi
	call	_ZNK2v811ArrayBuffer12IsDetachableEv@PLT
	testb	%al, %al
	jne	.L183
.L151:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L147:
	cmpq	$0, 32(%r12)
	je	.L152
	movq	24(%r12), %rax
	leaq	-80(%rbp), %r15
	movq	%r15, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	24(%r12), %rbx
	movl	108(%rbx), %eax
	movl	104(%rbx), %r14d
	movq	%rbx, %rdi
	movq	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	movq	48(%r12), %rdx
	movl	%eax, -84(%rbp)
	movq	40(%r12), %rsi
	call	*32(%r12)
	cmpl	104(%rbx), %r14d
	jne	.L184
	movl	-84(%rbp), %eax
	cmpl	108(%rbx), %eax
	jne	.L185
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L156
	movq	8(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L156
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L156:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L152:
	movzbl	64(%r12), %ecx
	testb	%r13b, %r13b
	jne	.L165
	testb	%cl, %cl
	jne	.L165
	movb	$1, 56(%r12)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L165:
	movq	16(%r12), %rdx
	movq	8(%r12), %rax
	testq	%rdx, %rdx
	je	.L160
	movq	%rax, 8(%rdx)
	movq	8(%r12), %rax
.L160:
	testq	%rax, %rax
	je	.L161
	movq	%rdx, 16(%rax)
.L161:
	movl	60(%r12), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%r12)
	testl	%eax, %eax
	jne	.L162
	testb	%cl, %cl
	je	.L186
.L162:
	movq	%r12, %rdi
	call	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD0Ev
.L146:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	leaq	_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	cmpb	$0, 56(%r12)
	jne	.L162
	movb	$1, 64(%r12)
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r14, %rdi
	call	_ZN2v811ArrayBuffer6DetachEv@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7546:
	.size	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEb, .-_ZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEb
	.p2align 4
	.type	napi_define_properties.part.0, @function
napi_define_properties.part.0:
.LFB10024:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-112(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%rdx, -136(%rbp)
	movq	8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%r15, -64(%rbp)
	testq	%rbx, %rbx
	jne	.L189
	testq	%r14, %r14
	jne	.L191
.L189:
	movq	16(%r15), %r14
	testq	%r13, %r13
	je	.L191
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -152(%rbp)
	testq	%rax, %rax
	je	.L282
	cmpq	$0, -136(%rbp)
	je	.L193
	movq	$0, -144(%rbp)
	movq	%r12, -168(%rbp)
.L217:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L194
	movq	8(%r15), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L224
	movq	24(%rbx), %rsi
	movq	%rax, -160(%rbp)
	testq	%rsi, %rsi
	je	.L283
.L198:
	movq	56(%rbx), %rdx
	xorl	%r10d, %r10d
	movq	%r15, %rdi
	movq	%r10, -176(%rbp)
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	movq	-176(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L276
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%r10, -176(%rbp)
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L276
	movq	32(%rbx), %rsi
	movq	-176(%rbp), %r10
	testq	%rsi, %rsi
	je	.L203
.L222:
	movq	56(%rbx), %rdx
	movq	%r15, %rdi
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L276
	xorl	%r9d, %r9d
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L276
.L203:
	leaq	-120(%rbp), %r12
	movq	%r10, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEES3_@PLT
.L281:
	movl	48(%rbx), %esi
	movq	%r12, %rdi
	shrl	%esi
	andl	$1, %esi
	call	_ZN2v818PropertyDescriptor14set_enumerableEb@PLT
	movl	48(%rbx), %esi
	movq	%r12, %rdi
	shrl	$2, %esi
	andl	$1, %esi
	call	_ZN2v818PropertyDescriptor16set_configurableEb@PLT
	movq	-160(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	-152(%rbp), %rdi
	call	_ZN2v86Object14DefinePropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEERNS_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L215
	shrw	$8, %ax
	je	.L215
.L216:
	movq	%r12, %rdi
	addq	$64, %rbx
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
	addq	$1, -144(%rbp)
	movq	-144(%rbp), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L217
	movq	-168(%rbp), %r12
.L193:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L190
	movabsq	$42949672960, %rax
	movq	$0, 88(%r15)
	movl	$10, %r13d
	movq	%rax, 96(%r15)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L191:
	movabsq	$4294967296, %rax
	movq	$0, 88(%r15)
	movl	$1, %r13d
	movq	%rax, 96(%r15)
.L190:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L284
.L218:
	movq	%r12, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L285
	addq	$136, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	-64(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L219
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L219:
	testq	%r14, %r14
	je	.L218
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L215:
	movabsq	$4294967296, %rax
	movq	$0, 88(%r15)
	movq	%r12, %r13
	movq	-168(%rbp), %r12
	movq	%rax, 96(%r15)
	movq	%r13, %rdi
	movl	$1, %r13d
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L194:
	movq	8(%rbx), %r13
	movq	%r13, %rdi
	call	_ZNK2v85Value6IsNameEv@PLT
	testb	%al, %al
	je	.L225
	movq	24(%rbx), %rsi
	movq	%r13, -160(%rbp)
	testq	%rsi, %rsi
	jne	.L198
.L283:
	movq	32(%rbx), %rsi
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	jne	.L222
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L209
	movq	56(%rbx), %rdx
	movq	%r15, %rdi
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L212
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movl	$1, %r8d
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L212
	movl	48(%rbx), %edx
	leaq	-120(%rbp), %r12
	movq	%r12, %rdi
	andl	$1, %edx
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEEb@PLT
	movl	48(%rbx), %esi
	movq	%r12, %rdi
	shrl	%esi
	andl	$1, %esi
	call	_ZN2v818PropertyDescriptor14set_enumerableEb@PLT
	movl	48(%rbx), %esi
	movq	%r12, %rdi
	shrl	$2, %esi
	andl	$1, %esi
	call	_ZN2v818PropertyDescriptor16set_configurableEb@PLT
	movq	-160(%rbp), %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	-152(%rbp), %rdi
	call	_ZN2v86Object14DefinePropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEERNS_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L213
	shrw	$8, %ax
	jne	.L216
	.p2align 4,,10
	.p2align 3
.L213:
	movabsq	$38654705664, %rax
	movq	$0, 88(%r15)
	movq	%r12, %r13
	movq	-168(%rbp), %r12
	movq	%rax, 96(%r15)
	movq	%r13, %rdi
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
.L211:
	movl	$9, %r13d
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L282:
	movabsq	$8589934592, %rax
	movq	$0, 88(%r15)
	movl	$2, %r13d
	movq	%rax, 96(%r15)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L224:
	movq	-168(%rbp), %r12
	movl	$9, %r13d
.L195:
	movl	%r13d, 100(%r15)
	movl	$0, 96(%r15)
	movq	$0, 88(%r15)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L225:
	movq	-168(%rbp), %r12
	movl	$4, %r13d
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L276:
	movabsq	$38654705664, %rax
	movq	$0, 88(%r15)
	movq	-168(%rbp), %r12
	movl	$9, %r13d
	movq	%rax, 96(%r15)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L209:
	movl	48(%rbx), %edx
	leaq	-120(%rbp), %r12
	movq	40(%rbx), %rsi
	movq	%r12, %rdi
	andl	$1, %edx
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEEb@PLT
	jmp	.L281
.L212:
	movq	$0, 88(%r15)
	movq	-168(%rbp), %r12
	movabsq	$38654705664, %rax
	movq	%rax, 96(%r15)
	jmp	.L211
.L285:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10024:
	.size	napi_define_properties.part.0, .-napi_define_properties.part.0
	.section	.text._ZN6v8impl9FinalizerD2Ev,"axG",@progbits,_ZN6v8impl9FinalizerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6v8impl9FinalizerD2Ev
	.type	_ZN6v8impl9FinalizerD2Ev, @function
_ZN6v8impl9FinalizerD2Ev:
.LFB7496:
	.cfi_startproc
	endbr64
	cmpb	$0, 33(%rdi)
	jne	.L315
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	subl	$1, 112(%r12)
	jne	.L286
	movq	(%r12), %rax
	leaq	_ZN10napi_env__D0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L288
	movq	64(%r12), %rdi
	leaq	16+_ZTV10napi_env__(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L314
	.p2align 4,,10
	.p2align 3
.L289:
	movq	(%rdi), %rax
	movl	$1, %esi
	call	*16(%rax)
	movq	64(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L289
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L316
	.p2align 4,,10
	.p2align 3
.L291:
	movq	(%rdi), %rax
	movl	$1, %esi
	call	*16(%rax)
.L314:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L291
.L316:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L293
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L293:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L294
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L294:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	.cfi_remember_state
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L286:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7496:
	.size	_ZN6v8impl9FinalizerD2Ev, .-_ZN6v8impl9FinalizerD2Ev
	.weak	_ZN6v8impl9FinalizerD1Ev
	.set	_ZN6v8impl9FinalizerD1Ev,_ZN6v8impl9FinalizerD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_17RefBaseD2Ev, @function
_ZN6v8impl12_GLOBAL__N_17RefBaseD2Ev:
.LFB8408:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	_ZN6v8impl9FinalizerD2Ev
	.cfi_endproc
.LFE8408:
	.size	_ZN6v8impl12_GLOBAL__N_17RefBaseD2Ev, .-_ZN6v8impl12_GLOBAL__N_17RefBaseD2Ev
	.set	_ZN6v8impl12_GLOBAL__N_17RefBaseD1Ev,_ZN6v8impl12_GLOBAL__N_17RefBaseD2Ev
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_17RefBaseD0Ev, @function
_ZN6v8impl12_GLOBAL__N_17RefBaseD0Ev:
.LFB8410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	24(%rdi), %rdi
	subq	$8, %rsp
	call	_ZN6v8impl9FinalizerD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8410:
	.size	_ZN6v8impl12_GLOBAL__N_17RefBaseD0Ev, .-_ZN6v8impl12_GLOBAL__N_17RefBaseD0Ev
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_19ReferenceD2Ev, @function
_ZN6v8impl12_GLOBAL__N_19ReferenceD2Ev:
.LFB9036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L321
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L321:
	addq	$8, %rsp
	leaq	24(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6v8impl9FinalizerD2Ev
	.cfi_endproc
.LFE9036:
	.size	_ZN6v8impl12_GLOBAL__N_19ReferenceD2Ev, .-_ZN6v8impl12_GLOBAL__N_19ReferenceD2Ev
	.set	_ZN6v8impl12_GLOBAL__N_19ReferenceD1Ev,_ZN6v8impl12_GLOBAL__N_19ReferenceD2Ev
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD2Ev, @function
_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD2Ev:
.LFB9955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L327
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L327:
	addq	$8, %rsp
	leaq	24(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6v8impl9FinalizerD2Ev
	.cfi_endproc
.LFE9955:
	.size	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD2Ev, .-_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD2Ev
	.set	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD1Ev,_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD2Ev
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_19ReferenceD0Ev, @function
_ZN6v8impl12_GLOBAL__N_19ReferenceD0Ev:
.LFB9038:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L333
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L333:
	leaq	24(%r12), %rdi
	call	_ZN6v8impl9FinalizerD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9038:
	.size	_ZN6v8impl12_GLOBAL__N_19ReferenceD0Ev, .-_ZN6v8impl12_GLOBAL__N_19ReferenceD0Ev
	.align 2
	.p2align 4
	.type	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD0Ev, @function
_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD0Ev:
.LFB9957:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L339
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L339:
	leaq	24(%r12), %rdi
	call	_ZN6v8impl9FinalizerD2Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9957:
	.size	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD0Ev, .-_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD0Ev
	.p2align 4
	.globl	napi_get_last_error_info
	.type	napi_get_last_error_info, @function
napi_get_last_error_info:
.LFB7580:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L348
	testq	%rsi, %rsi
	je	.L352
	movslq	100(%rdi), %rax
	cmpl	$20, %eax
	jg	.L353
	leaq	_ZL14error_messages(%rip), %rdx
	addq	$80, %rdi
	movq	(%rdx,%rax,8), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	movq	%rdi, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZ24napi_get_last_error_infoE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7580:
	.size	napi_get_last_error_info, .-napi_get_last_error_info
	.p2align 4
	.globl	napi_create_function
	.type	napi_create_function, @function
napi_create_function:
.LFB7582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L373
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L356
.L358:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L354:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r10
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r13
	movq	%r9, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L400
.L357:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movq	%r8, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r8
	je	.L361
	movq	%r8, -160(%rbp)
	movq	%r10, -152(%rbp)
	testq	%r13, %r13
	je	.L361
	leaq	-144(%rbp), %r11
	movq	8(%rbx), %rsi
	movq	%r11, %rdi
	movq	%r11, -168(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-160(%rbp), %r8
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	movq	-168(%rbp), %r11
	movq	-152(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L398
	movq	16(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$1, %r8d
	movq	%r11, -152(%rbp)
	movq	%r10, -160(%rbp)
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	-152(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L398
	movq	%r11, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	testq	%r14, %r14
	movq	-152(%rbp), %r11
	movq	%rax, %r13
	je	.L365
	movq	-160(%rbp), %r10
	movabsq	$-2147483650, %rax
	leaq	-2147483648(%r10), %rdx
	cmpq	%rax, %rdx
	ja	.L366
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r11, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L361:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L360:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L401
.L369:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L373:
	movl	$1, %r12d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L400:
	movq	%r8, -160(%rbp)
	movq	%r10, -152(%rbp)
	call	*%rax
	movq	-152(%rbp), %r10
	movq	-160(%rbp), %r8
	testb	%al, %al
	je	.L358
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L401:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L370
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L370:
	testq	%r13, %r13
	je	.L369
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L398:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L363
.L366:
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	movl	%r10d, %ecx
	movl	$1, %edx
	movq	%r11, -152(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-152(%rbp), %r11
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L398
	movq	%r13, %rdi
	movq	%r11, -152(%rbp)
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	-152(%rbp), %r11
.L365:
	movq	%r13, (%r12)
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	movq	%r11, -152(%rbp)
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	-152(%rbp), %r11
	testb	%al, %al
	je	.L363
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L363
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7582:
	.size	napi_create_function, .-napi_create_function
	.p2align 4
	.globl	napi_get_property_names
	.type	napi_get_property_names, @function
napi_get_property_names:
.LFB7593:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L417
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L404
.L406:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L437
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L404:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L438
.L405:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L409
	movq	16(%rbx), %r15
	testq	%r13, %r13
	je	.L409
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L439
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$18, %ecx
	movl	$1, %edx
	movq	%r15, %rsi
	call	_ZN2v86Object16GetPropertyNamesENS_5LocalINS_7ContextEEENS_17KeyCollectionModeENS_14PropertyFilterENS_11IndexFilterENS_17KeyConversionModeE@PLT
	testq	%rax, %rax
	je	.L440
	movq	%rax, (%r12)
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L408
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L409:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L408:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L441
.L413:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$1, %r12d
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L438:
	call	*%rax
	testb	%al, %al
	je	.L406
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L441:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L414
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L414:
	testq	%r13, %r13
	je	.L413
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L439:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movl	$0, 96(%rbx)
	testb	%al, %al
	movq	$0, 88(%rbx)
	setne	%r12b
	addl	$9, %r12d
	movl	%r12d, 100(%rbx)
	jmp	.L408
.L437:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7593:
	.size	napi_get_property_names, .-napi_get_property_names
	.p2align 4
	.globl	napi_get_all_property_names
	.type	napi_get_all_property_names, @function
napi_get_all_property_names:
.LFB7594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L465
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L444
.L446:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L442:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L494
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L444:
	.cfi_restore_state
	movq	(%rdi), %rax
	movl	%edx, %r10d
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movl	%r8d, %r14d
	movq	%r9, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L495
.L445:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movl	%ecx, -120(%rbp)
	movl	%r10d, -116(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movl	-116(%rbp), %r10d
	movl	-120(%rbp), %ecx
	je	.L458
	movl	%ecx, -120(%rbp)
	movq	16(%rbx), %rsi
	movl	%r10d, -116(%rbp)
	testq	%r13, %r13
	je	.L458
	movq	%r13, %rdi
	movq	%rsi, -128(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-128(%rbp), %rsi
	movl	-116(%rbp), %r10d
	testq	%rax, %rax
	movl	-120(%rbp), %ecx
	movq	%rax, %rdi
	je	.L496
	movl	%ecx, %edx
	andl	$3, %edx
	movl	%edx, %eax
	orl	$1, %eax
	testb	$4, %cl
	cmovne	%eax, %edx
	testb	$8, %cl
	je	.L454
	orl	$8, %edx
.L454:
	movl	%edx, %eax
	orl	$16, %eax
	andl	$16, %ecx
	cmovne	%eax, %edx
	movl	%edx, %ecx
	testl	%r10d, %r10d
	je	.L466
	cmpl	$1, %r10d
	jne	.L458
	xorl	%edx, %edx
.L456:
	testl	%r14d, %r14d
	je	.L468
	xorl	%r9d, %r9d
	cmpl	$1, %r14d
	jne	.L458
.L457:
	xorl	%r8d, %r8d
	call	_ZN2v86Object16GetPropertyNamesENS_5LocalINS_7ContextEEENS_17KeyCollectionModeENS_14PropertyFilterENS_11IndexFilterENS_17KeyConversionModeE@PLT
	testq	%rax, %rax
	je	.L497
	movq	%rax, (%r12)
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L448
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L458:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L448:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L498
.L461:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L465:
	movl	$1, %r12d
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L495:
	movl	%ecx, -120(%rbp)
	movl	%r10d, -116(%rbp)
	call	*%rax
	movl	-116(%rbp), %r10d
	movl	-120(%rbp), %ecx
	testb	%al, %al
	je	.L446
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L498:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L462
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L462:
	testq	%r13, %r13
	je	.L461
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L496:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L466:
	movl	$1, %edx
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L468:
	movl	$1, %r9d
	jmp	.L457
.L497:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movl	$0, 96(%rbx)
	testb	%al, %al
	movq	$0, 88(%rbx)
	setne	%r12b
	addl	$9, %r12d
	movl	%r12d, 100(%rbx)
	jmp	.L448
.L494:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7594:
	.size	napi_get_all_property_names, .-napi_get_all_property_names
	.p2align 4
	.globl	napi_set_property
	.type	napi_set_property, @function
napi_set_property:
.LFB7595:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L514
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L501
.L503:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L499:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L539
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L540
.L502:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L506
	testq	%r12, %r12
	je	.L506
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L506
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L541
	movq	%r12, %rcx
	movq	%r13, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L508
	shrw	$8, %ax
	jne	.L509
.L508:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L506:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L505:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L542
.L510:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L514:
	movl	$1, %r12d
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L540:
	call	*%rax
	testb	%al, %al
	je	.L503
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L542:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L511
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L511:
	testq	%r13, %r13
	je	.L510
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L541:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L505
.L509:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L505
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L505
.L539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7595:
	.size	napi_set_property, .-napi_set_property
	.p2align 4
	.globl	napi_has_property
	.type	napi_has_property, @function
napi_has_property:
.LFB7596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L557
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L545
.L547:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L543:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L579
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L580
.L546:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L550
	testq	%r13, %r13
	je	.L550
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L550
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L581
	movq	%r13, %rdx
	call	_ZN2v86Object3HasENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L552
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L550:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L549:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L582
.L553:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L557:
	movl	$1, %r12d
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L580:
	call	*%rax
	testb	%al, %al
	je	.L547
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L582:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L554
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L554:
	testq	%r13, %r13
	je	.L553
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L581:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L549
.L552:
	movzbl	%ah, %eax
	movq	%r15, %rdi
	movb	%al, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L549
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L549
.L579:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7596:
	.size	napi_has_property, .-napi_has_property
	.p2align 4
	.globl	napi_get_property
	.type	napi_get_property, @function
napi_get_property:
.LFB7597:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L597
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L585
.L587:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L583:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L619
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L620
.L586:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L590
	testq	%r12, %r12
	je	.L590
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L590
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L621
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L622
	movq	%rax, (%r12)
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L589
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L590:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L589:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L623
.L593:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$1, %r12d
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L620:
	call	*%rax
	testb	%al, %al
	je	.L587
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L623:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L594
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L594:
	testq	%r13, %r13
	je	.L593
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L621:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L622:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L589
.L619:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7597:
	.size	napi_get_property, .-napi_get_property
	.p2align 4
	.globl	napi_delete_property
	.type	napi_delete_property, @function
napi_delete_property:
.LFB7598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L639
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L626
.L628:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L624:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L661
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L626:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L662
.L627:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L631
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L631
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L663
	movq	%r13, %rdx
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L633
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L631:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L630:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L664
.L635:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L639:
	movl	$1, %r12d
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L662:
	call	*%rax
	testb	%al, %al
	je	.L628
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L664:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L636
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L636:
	testq	%r13, %r13
	je	.L635
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L663:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L633:
	testq	%r12, %r12
	je	.L634
	movzbl	%ah, %eax
	movb	%al, (%r12)
.L634:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L630
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L630
.L661:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7598:
	.size	napi_delete_property, .-napi_delete_property
	.p2align 4
	.globl	napi_has_own_property
	.type	napi_has_own_property, @function
napi_has_own_property:
.LFB7599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L680
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L667
.L669:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L665:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L702
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L667:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L703
.L668:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L672
	testq	%r12, %r12
	je	.L672
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L672
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L704
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value6IsNameEv@PLT
	movq	-120(%rbp), %rsi
	testb	%al, %al
	jne	.L674
	movabsq	$17179869184, %rax
	movq	$0, 88(%rbx)
	movl	$4, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L672:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L671:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L705
.L676:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L680:
	movl	$1, %r12d
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L703:
	call	*%rax
	testb	%al, %al
	je	.L669
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L705:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L677
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L677:
	testq	%r13, %r13
	je	.L676
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L704:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L671
.L674:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object14HasOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	testb	%al, %al
	jne	.L675
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L671
.L675:
	movzbl	%ah, %eax
	movq	%r15, %rdi
	movb	%al, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L671
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L671
.L702:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7599:
	.size	napi_has_own_property, .-napi_has_own_property
	.p2align 4
	.globl	napi_set_named_property
	.type	napi_set_named_property, @function
napi_set_named_property:
.LFB7600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L721
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L708
.L710:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L706:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L749
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L750
.L709:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L713
	movq	16(%rbx), %r8
	testq	%r14, %r14
	je	.L713
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L751
	movq	%r8, -120(%rbp)
	testq	%r13, %r13
	je	.L713
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r13, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L716
	movq	%r12, %rcx
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L716
	shrw	$8, %ax
	jne	.L752
	.p2align 4,,10
	.p2align 3
.L716:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L713:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L712:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L753
.L717:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L721:
	movl	$1, %r12d
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L750:
	call	*%rax
	testb	%al, %al
	je	.L710
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L753:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L718
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L718:
	testq	%r13, %r13
	je	.L717
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L751:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L712
.L752:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L712
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L712
.L749:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7600:
	.size	napi_set_named_property, .-napi_set_named_property
	.p2align 4
	.globl	napi_has_named_property
	.type	napi_has_named_property, @function
napi_has_named_property:
.LFB7601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L769
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L756
.L758:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L754:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L794
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L795
.L757:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L761
	movq	16(%rbx), %r8
	testq	%r14, %r14
	je	.L761
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L796
	movq	%r8, -120(%rbp)
	testq	%r13, %r13
	je	.L761
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r13, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L764
	movq	%r8, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3HasENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L797
.L764:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L761:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L760:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L798
.L765:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L769:
	movl	$1, %r12d
	jmp	.L754
	.p2align 4,,10
	.p2align 3
.L795:
	call	*%rax
	testb	%al, %al
	je	.L758
	jmp	.L757
	.p2align 4,,10
	.p2align 3
.L798:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L766
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L766:
	testq	%r13, %r13
	je	.L765
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L796:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L760
.L797:
	movzbl	%ah, %eax
	movq	%r15, %rdi
	movb	%al, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L760
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L760
.L794:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7601:
	.size	napi_has_named_property, .-napi_has_named_property
	.p2align 4
	.globl	napi_get_named_property
	.type	napi_get_named_property, @function
napi_get_named_property:
.LFB7602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L814
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L801
.L803:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L799:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L839
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L840
.L802:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L806
	movq	16(%rbx), %r8
	movq	%r8, -120(%rbp)
	testq	%r13, %r13
	je	.L806
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movl	$-1, %ecx
	movl	$1, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L809
	testq	%r14, %r14
	je	.L806
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L841
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L809
	movq	%rax, (%r12)
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L805
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L806:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L805:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L842
.L810:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L814:
	movl	$1, %r12d
	jmp	.L799
	.p2align 4,,10
	.p2align 3
.L840:
	call	*%rax
	testb	%al, %al
	je	.L803
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L842:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L811
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L811:
	testq	%r13, %r13
	je	.L810
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L809:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L841:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L805
.L839:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7602:
	.size	napi_get_named_property, .-napi_get_named_property
	.p2align 4
	.globl	napi_set_element
	.type	napi_set_element, @function
napi_set_element:
.LFB7603:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L858
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L845
.L847:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L843:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L880
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L845:
	.cfi_restore_state
	movq	(%rdi), %rax
	movl	%edx, %r13d
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L881
.L846:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L850
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L850
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L882
	movq	%r12, %rcx
	movl	%r13d, %edx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L852
	shrw	$8, %ax
	jne	.L853
.L852:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L850:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L849:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L883
.L854:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L858:
	movl	$1, %r12d
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L881:
	call	*%rax
	testb	%al, %al
	je	.L847
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L883:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L855
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L855:
	testq	%r13, %r13
	je	.L854
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L882:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L849
.L853:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L849
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L849
.L880:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7603:
	.size	napi_set_element, .-napi_set_element
	.p2align 4
	.globl	napi_has_element
	.type	napi_has_element, @function
napi_has_element:
.LFB7604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L898
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L886
.L888:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L884:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L917
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	movq	(%rdi), %rax
	movl	%edx, %r13d
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L918
.L887:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L891
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L891
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L919
	movl	%r13d, %edx
	call	_ZN2v86Object3HasENS_5LocalINS_7ContextEEEj@PLT
	testb	%al, %al
	jne	.L893
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L891:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L890:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L920
.L894:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L898:
	movl	$1, %r12d
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L918:
	call	*%rax
	testb	%al, %al
	je	.L888
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L920:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L895
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L895:
	testq	%r13, %r13
	je	.L894
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L919:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L893:
	movzbl	%ah, %eax
	movq	%r15, %rdi
	movb	%al, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L890
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L890
.L917:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7604:
	.size	napi_has_element, .-napi_has_element
	.p2align 4
	.globl	napi_get_element
	.type	napi_get_element, @function
napi_get_element:
.LFB7605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L935
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L923
.L925:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L921:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L954
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L923:
	.cfi_restore_state
	movq	(%rdi), %rax
	movl	%edx, %r13d
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L955
.L924:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L928
	movq	16(%rbx), %rsi
	testq	%r14, %r14
	je	.L928
	movq	%r14, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L956
	movl	%r13d, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	testq	%rax, %rax
	je	.L957
	movq	%rax, (%r12)
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L927
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L928:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L927:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L958
.L931:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L935:
	movl	$1, %r12d
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L955:
	call	*%rax
	testb	%al, %al
	je	.L925
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L958:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L932
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L932:
	testq	%r13, %r13
	je	.L931
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L956:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L957:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L927
.L954:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7605:
	.size	napi_get_element, .-napi_get_element
	.p2align 4
	.globl	napi_delete_element
	.type	napi_delete_element, @function
napi_delete_element:
.LFB7606:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L973
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L961
.L963:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L959:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L992
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L961:
	.cfi_restore_state
	movq	(%rdi), %rax
	movl	%edx, %r13d
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r15
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L993
.L962:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	movq	16(%rbx), %rsi
	testq	%r15, %r15
	je	.L994
	movq	%r15, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L995
	movl	%r13d, %edx
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEEj@PLT
	testb	%al, %al
	jne	.L967
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L994:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L965:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L996
.L969:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L973:
	movl	$1, %r12d
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L993:
	call	*%rax
	testb	%al, %al
	je	.L963
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L996:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L970
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L970:
	testq	%r13, %r13
	je	.L969
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L995:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L967:
	testq	%r12, %r12
	je	.L968
	movzbl	%ah, %eax
	movb	%al, (%r12)
.L968:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L965
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L965
.L992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7606:
	.size	napi_delete_element, .-napi_delete_element
	.p2align 4
	.globl	napi_define_properties
	.type	napi_define_properties, @function
napi_define_properties:
.LFB7607:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1002
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	cmpq	$0, 24(%rdi)
	je	.L999
.L1001:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$10, %eax
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L999:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %r8
	movq	16(%rax), %rax
	cmpq	%r8, %rax
	jne	.L1010
.L1000:
	leave
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	napi_define_properties.part.0
	.p2align 4,,10
	.p2align 3
.L1002:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	movq	%rcx, -32(%rbp)
	movq	%rdx, -24(%rbp)
	movq	%rsi, -16(%rbp)
	movq	%rdi, -8(%rbp)
	call	*%rax
	movq	-8(%rbp), %rdi
	movq	-16(%rbp), %rsi
	testb	%al, %al
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rcx
	je	.L1001
	jmp	.L1000
	.cfi_endproc
.LFE7607:
	.size	napi_define_properties, .-napi_define_properties
	.p2align 4
	.globl	napi_is_array
	.type	napi_is_array, @function
napi_is_array:
.LFB7608:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1015
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1014
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L1014
	movq	%rsi, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1014:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7608:
	.size	napi_is_array, .-napi_is_array
	.p2align 4
	.globl	napi_get_array_length
	.type	napi_get_array_length, @function
napi_get_array_length:
.LFB7609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1036
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1025
.L1027:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1023:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1055
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1025:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1056
.L1026:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L1030
	testq	%r12, %r12
	je	.L1030
	movq	%r13, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L1031
	movabsq	$34359738368, %rax
	movq	$0, 88(%rbx)
	movl	$8, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L1030:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1029:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1057
.L1032:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1036:
	movl	$1, %r12d
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1056:
	call	*%rax
	testb	%al, %al
	je	.L1027
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1033
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1033:
	testq	%r13, %r13
	je	.L1032
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	%r14, %rdi
	movl	%eax, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1029
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1029
.L1055:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7609:
	.size	napi_get_array_length, .-napi_get_array_length
	.p2align 4
	.globl	napi_strict_equals
	.type	napi_strict_equals, @function
napi_strict_equals:
.LFB7610:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1070
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1060
.L1062:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1058:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1092
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1060:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1093
.L1061:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r14, %r14
	je	.L1065
	testq	%r13, %r13
	je	.L1065
	testq	%r12, %r12
	je	.L1065
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNK2v85Value12StrictEqualsENS_5LocalIS0_EE@PLT
	movq	%r15, %rdi
	movb	%al, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1064
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1065:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1064:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1094
.L1066:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1070:
	movl	$1, %r12d
	jmp	.L1058
	.p2align 4,,10
	.p2align 3
.L1093:
	call	*%rax
	testb	%al, %al
	je	.L1062
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1094:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L1067
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1067:
	testq	%r13, %r13
	je	.L1066
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1066
.L1092:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7610:
	.size	napi_strict_equals, .-napi_strict_equals
	.p2align 4
	.globl	napi_get_prototype
	.type	napi_get_prototype, @function
napi_get_prototype:
.LFB7611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1108
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1097
.L1099:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1095:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1127
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1097:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1128
.L1098:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L1102
	movq	16(%rbx), %rsi
	testq	%r13, %r13
	je	.L1102
	movq	%r13, %rdi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1129
	call	_ZN2v86Object12GetPrototypeEv@PLT
	movq	%r14, %rdi
	movq	%rax, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1101
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1102:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1101:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1130
.L1104:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1108:
	movl	$1, %r12d
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1128:
	call	*%rax
	testb	%al, %al
	je	.L1099
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1105
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1105:
	testq	%r13, %r13
	je	.L1104
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1129:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1101
.L1127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7611:
	.size	napi_get_prototype, .-napi_get_prototype
	.p2align 4
	.globl	napi_create_object
	.type	napi_create_object, @function
napi_create_object:
.LFB7612:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1134
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1139
	movq	8(%rdi), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1139:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1134:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7612:
	.size	napi_create_object, .-napi_create_object
	.p2align 4
	.globl	napi_create_array
	.type	napi_create_array, @function
napi_create_array:
.LFB7613:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1143
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1148
	movq	8(%rdi), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1148:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1143:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7613:
	.size	napi_create_array, .-napi_create_array
	.p2align 4
	.globl	napi_create_array_with_length
	.type	napi_create_array_with_length, @function
napi_create_array_with_length:
.LFB7614:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1152
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L1157
	movq	8(%rdi), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1157:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1152:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7614:
	.size	napi_create_array_with_length, .-napi_create_array_with_length
	.p2align 4
	.globl	napi_create_string_latin1
	.type	napi_create_string_latin1, @function
napi_create_string_latin1:
.LFB7615:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1163
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	je	.L1161
	leaq	-2147483648(%rdx), %rcx
	movabsq	$-2147483650, %rax
	cmpq	%rax, %rcx
	ja	.L1168
.L1161:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1158:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1168:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	%edx, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1169
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1163:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$9, %eax
	jmp	.L1158
	.cfi_endproc
.LFE7615:
	.size	napi_create_string_latin1, .-napi_create_string_latin1
	.p2align 4
	.globl	napi_create_string_utf8
	.type	napi_create_string_utf8, @function
napi_create_string_utf8:
.LFB7616:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1175
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	je	.L1173
	leaq	-2147483648(%rdx), %rcx
	movabsq	$-2147483650, %rax
	cmpq	%rax, %rcx
	ja	.L1180
.L1173:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1170:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1180:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	%edx, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1181
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1175:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$9, %eax
	jmp	.L1170
	.cfi_endproc
.LFE7616:
	.size	napi_create_string_utf8, .-napi_create_string_utf8
	.p2align 4
	.globl	napi_create_string_utf16
	.type	napi_create_string_utf16, @function
napi_create_string_utf16:
.LFB7617:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1187
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rcx, %rcx
	je	.L1185
	leaq	-2147483648(%rdx), %rcx
	movabsq	$-2147483650, %rax
	cmpq	%rax, %rcx
	ja	.L1192
.L1185:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1182:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1192:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	movl	%edx, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1193
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1187:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1193:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$9, %eax
	jmp	.L1182
	.cfi_endproc
.LFE7617:
	.size	napi_create_string_utf16, .-napi_create_string_utf16
	.p2align 4
	.globl	napi_create_double
	.type	napi_create_double, @function
napi_create_double:
.LFB7618:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1197
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1202
	movq	8(%rdi), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1202:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7618:
	.size	napi_create_double, .-napi_create_double
	.p2align 4
	.globl	napi_create_int32
	.type	napi_create_int32, @function
napi_create_int32:
.LFB7619:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1206
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L1211
	movq	8(%rdi), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1211:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1206:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7619:
	.size	napi_create_int32, .-napi_create_int32
	.p2align 4
	.globl	napi_create_uint32
	.type	napi_create_uint32, @function
napi_create_uint32:
.LFB7620:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1215
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L1220
	movq	8(%rdi), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1220:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1215:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7620:
	.size	napi_create_uint32, .-napi_create_uint32
	.p2align 4
	.globl	napi_create_int64
	.type	napi_create_int64, @function
napi_create_int64:
.LFB7621:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1224
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L1229
	pxor	%xmm0, %xmm0
	movq	8(%rdi), %rdi
	cvtsi2sdq	%rsi, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1229:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1224:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7621:
	.size	napi_create_int64, .-napi_create_int64
	.p2align 4
	.globl	napi_create_bigint_int64
	.type	napi_create_bigint_int64, @function
napi_create_bigint_int64:
.LFB7622:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1233
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L1238
	movq	8(%rdi), %rdi
	call	_ZN2v86BigInt3NewEPNS_7IsolateEl@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1238:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1233:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7622:
	.size	napi_create_bigint_int64, .-napi_create_bigint_int64
	.p2align 4
	.globl	napi_create_bigint_uint64
	.type	napi_create_bigint_uint64, @function
napi_create_bigint_uint64:
.LFB7623:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1242
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L1247
	movq	8(%rdi), %rdi
	call	_ZN2v86BigInt15NewFromUnsignedEPNS_7IsolateEm@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1247:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1242:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7623:
	.size	napi_create_bigint_uint64, .-napi_create_bigint_uint64
	.p2align 4
	.globl	napi_create_bigint_words
	.type	napi_create_bigint_words, @function
napi_create_bigint_words:
.LFB7624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1262
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1250
.L1252:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1248:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1279
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1250:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r14
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movl	%esi, %r9d
	movq	%rcx, %r13
	movq	%r8, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1280
.L1251:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movl	%r9d, -116(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r13, %r13
	movq	%rbx, -64(%rbp)
	movl	-116(%rbp), %r9d
	je	.L1255
	testq	%r12, %r12
	je	.L1255
	movq	16(%rbx), %rdi
	cmpq	$2147483647, %r14
	jbe	.L1281
.L1255:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1254:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1282
.L1258:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1262:
	movl	$1, %r12d
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1280:
	movl	%esi, -116(%rbp)
	call	*%rax
	movl	-116(%rbp), %r9d
	testb	%al, %al
	je	.L1252
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L1259
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1259:
	testq	%r13, %r13
	je	.L1258
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	%r13, %rcx
	movl	%r14d, %edx
	movl	%r9d, %esi
	call	_ZN2v86BigInt12NewFromWordsENS_5LocalINS_7ContextEEEiiPKm@PLT
	movq	%r15, %rdi
	movq	%rax, %r13
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1256
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1256:
	testq	%r13, %r13
	je	.L1283
	movq	%r13, (%r12)
	xorl	%r12d, %r12d
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1254
.L1283:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1254
.L1279:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7624:
	.size	napi_create_bigint_words, .-napi_create_bigint_words
	.p2align 4
	.globl	napi_get_boolean
	.type	napi_get_boolean, @function
napi_get_boolean:
.LFB7625:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1289
	testq	%rdx, %rdx
	je	.L1290
	movq	8(%rdi), %rax
	leaq	112(%rax), %rcx
	addq	$120, %rax
	testb	%sil, %sil
	cmovne	%rcx, %rax
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1290:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1289:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7625:
	.size	napi_get_boolean, .-napi_get_boolean
	.p2align 4
	.globl	napi_create_symbol
	.type	napi_create_symbol, @function
napi_create_symbol:
.LFB7626:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1298
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L1303
	movq	8(%rdi), %rdi
	testq	%rsi, %rsi
	je	.L1297
	movq	(%rsi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1296
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1297
.L1296:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1297:
	.cfi_restore_state
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1303:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7626:
	.size	napi_create_symbol, .-napi_create_symbol
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"code"
	.text
	.p2align 4
	.globl	napi_create_error
	.type	napi_create_error, @function
napi_create_error:
.LFB7628:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1315
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L1307
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L1307
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1311
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1309
.L1311:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1307:
	.cfi_restore_state
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1304:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1309:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rsi, %r13
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r14
	testq	%r13, %r13
	je	.L1310
	movq	0(%r13), %rax
	movq	16(%rbx), %r15
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1311
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1311
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1314
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1314
	shrw	$8, %ax
	jne	.L1310
.L1314:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$9, %eax
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	%r14, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1315:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7628:
	.size	napi_create_error, .-napi_create_error
	.p2align 4
	.globl	napi_create_type_error
	.type	napi_create_type_error, @function
napi_create_type_error:
.LFB7629:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1343
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L1335
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L1335
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1339
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1337
.L1339:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1335:
	.cfi_restore_state
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1332:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1337:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rsi, %r13
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r14
	testq	%r13, %r13
	je	.L1338
	movq	0(%r13), %rax
	movq	16(%rbx), %r15
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1339
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1339
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1342
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1342
	shrw	$8, %ax
	jne	.L1338
.L1342:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$9, %eax
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1338:
	movq	%r14, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1343:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7629:
	.size	napi_create_type_error, .-napi_create_type_error
	.p2align 4
	.globl	napi_create_range_error
	.type	napi_create_range_error, @function
napi_create_range_error:
.LFB7630:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1371
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L1363
	movq	%rcx, %r12
	testq	%rcx, %rcx
	je	.L1363
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1367
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L1365
.L1367:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1363:
	.cfi_restore_state
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1360:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1365:
	.cfi_restore_state
	movq	%rdx, %rdi
	movq	%rsi, %r13
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r14
	testq	%r13, %r13
	je	.L1366
	movq	0(%r13), %rax
	movq	16(%rbx), %r15
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1367
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1367
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1370
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1370
	shrw	$8, %ax
	jne	.L1366
.L1370:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$9, %eax
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1366:
	movq	%r14, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1371:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7630:
	.size	napi_create_range_error, .-napi_create_range_error
	.p2align 4
	.globl	napi_typeof
	.type	napi_typeof, @function
napi_typeof:
.LFB7631:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1402
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L1391
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1391
	movq	%rsi, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1392
	movl	$3, 0(%r13)
.L1393:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1391:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1402:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1392:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L1394
	movl	$9, 0(%r13)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1410
.L1395:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L1396
	movl	$7, 0(%r13)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1395
	movl	$4, 0(%r13)
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsExternalEv@PLT
	testb	%al, %al
	je	.L1397
	movl	$8, 0(%r13)
	jmp	.L1393
.L1397:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1398
	movl	$6, 0(%r13)
	jmp	.L1393
.L1398:
	movq	%r12, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1399
	movl	$2, 0(%r13)
	jmp	.L1393
.L1399:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	subq	$1, %rdx
	jne	.L1400
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1400
	cmpl	$5, 43(%rax)
	jne	.L1400
	movl	$0, 0(%r13)
	jmp	.L1393
.L1400:
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsSymbolEv@PLT
	testb	%al, %al
	je	.L1401
	movl	$5, 0(%r13)
	jmp	.L1393
.L1401:
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	subq	$1, %rdx
	jne	.L1391
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1391
	cmpl	$3, 43(%rax)
	jne	.L1391
	movl	$1, 0(%r13)
	jmp	.L1393
	.cfi_endproc
.LFE7631:
	.size	napi_typeof, .-napi_typeof
	.p2align 4
	.globl	napi_get_undefined
	.type	napi_get_undefined, @function
napi_get_undefined:
.LFB7632:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1414
	testq	%rsi, %rsi
	je	.L1415
	movq	8(%rdi), %rax
	addq	$88, %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1415:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1414:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7632:
	.size	napi_get_undefined, .-napi_get_undefined
	.p2align 4
	.globl	napi_get_null
	.type	napi_get_null, @function
napi_get_null:
.LFB7633:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1419
	testq	%rsi, %rsi
	je	.L1420
	movq	8(%rdi), %rax
	addq	$104, %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1420:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1419:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7633:
	.size	napi_get_null, .-napi_get_null
	.p2align 4
	.globl	napi_get_cb_info
	.type	napi_get_cb_info, @function
napi_get_cb_info:
.LFB7634:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1441
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	testq	%rsi, %rsi
	je	.L1425
	testq	%rcx, %rcx
	je	.L1424
	testq	%rdx, %rdx
	je	.L1425
	movq	(%rdx), %r12
	movq	16(%rsi), %r13
	cmpq	%r13, %r12
	movq	%r13, %rbx
	cmovbe	%r12, %rbx
	testq	%rbx, %rbx
	je	.L1426
	movq	32(%rsi), %r11
	xorl	%eax, %eax
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1465:
	cmpl	16(%r11), %eax
	jge	.L1427
	movq	8(%r11), %r14
	salq	$3, %r10
	subq	%r10, %r14
	movq	%r14, %r10
	movq	%r10, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rbx
	je	.L1426
.L1430:
	movslq	%eax, %r10
	testl	%eax, %eax
	jns	.L1465
.L1427:
	movq	(%r11), %r10
	movq	8(%r10), %r10
	addq	$88, %r10
	movq	%r10, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L1430
.L1426:
	cmpq	%rbx, %r12
	ja	.L1466
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	%r13, (%rdx)
.L1438:
	testq	%r8, %r8
	je	.L1439
	movq	8(%rsi), %rax
	movq	%rax, (%r8)
.L1439:
	testq	%r9, %r9
	je	.L1440
	movq	24(%rsi), %rax
	movq	%rax, (%r9)
.L1440:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	movq	$0, 88(%rdi)
	popq	%r13
	popq	%r14
	movq	$0, 96(%rdi)
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1425:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	popq	%rbx
	movq	$0, 88(%rdi)
	popq	%r12
	popq	%r13
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1424:
	.cfi_restore_state
	testq	%rdx, %rdx
	je	.L1438
	movq	16(%rsi), %r13
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	32(%rsi), %rax
	subq	%rbx, %r12
	movq	(%rax), %rax
	movq	8(%rax), %r11
	addq	$88, %r11
	cmpq	$1, %r12
	je	.L1433
	movq	%r12, %r10
	movq	%r11, %xmm0
	leaq	(%rcx,%rbx,8), %rax
	shrq	%r10
	punpcklqdq	%xmm0, %xmm0
	salq	$4, %r10
	addq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L1435:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%r10, %rax
	jne	.L1435
	movq	%r12, %rax
	andq	$-2, %rax
	addq	%rax, %rbx
	cmpq	%rax, %r12
	je	.L1432
.L1433:
	movq	%r11, (%rcx,%rbx,8)
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1441:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7634:
	.size	napi_get_cb_info, .-napi_get_cb_info
	.p2align 4
	.globl	napi_get_new_target
	.type	napi_get_new_target, @function
napi_get_new_target:
.LFB7635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rdi, %rdi
	je	.L1471
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L1470
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L1470
	movq	(%rsi), %rax
	call	*(%rax)
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1470:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1471:
	.cfi_restore_state
	popq	%rbx
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7635:
	.size	napi_get_new_target, .-napi_get_new_target
	.p2align 4
	.globl	napi_call_function
	.type	napi_call_function, @function
napi_call_function:
.LFB7636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1492
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1478
.L1480:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1476:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1519
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1478:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r12
	movq	%r8, %r14
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1520
.L1479:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
	je	.L1483
	testq	%r14, %r14
	jne	.L1493
	testq	%rcx, %rcx
	jne	.L1483
.L1493:
	movq	16(%rbx), %rsi
	movq	%r9, -136(%rbp)
	movq	%rcx, -128(%rbp)
	movq	%rsi, -120(%rbp)
	testq	%r13, %r13
	je	.L1483
	movq	%r13, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-120(%rbp), %rsi
	movq	-128(%rbp), %rcx
	testb	%al, %al
	movq	-136(%rbp), %r9
	jne	.L1521
	.p2align 4,,10
	.p2align 3
.L1483:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1482:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1522
.L1488:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1492:
	movl	$1, %r12d
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%r9, -128(%rbp)
	movq	%rcx, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %rcx
	movq	-128(%rbp), %r9
	testb	%al, %al
	je	.L1480
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1522:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L1489
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1489:
	testq	%r13, %r13
	je	.L1488
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r13, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	-120(%rbp), %r9
	testb	%al, %al
	je	.L1485
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1482
.L1485:
	testq	%r9, %r9
	je	.L1486
	testq	%r12, %r12
	je	.L1523
	movq	%r12, (%r9)
.L1486:
	movq	$0, 88(%rbx)
	xorl	%r12d, %r12d
	movq	$0, 96(%rbx)
	jmp	.L1482
.L1523:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1482
.L1519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7636:
	.size	napi_call_function, .-napi_call_function
	.p2align 4
	.globl	napi_get_global
	.type	napi_get_global, @function
napi_get_global:
.LFB7637:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1527
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1532
	movq	16(%rdi), %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%rax, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1532:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1527:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7637:
	.size	napi_get_global, .-napi_get_global
	.p2align 4
	.globl	napi_throw
	.type	napi_throw, @function
napi_throw:
.LFB7638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1544
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1535
.L1537:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1533:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1558
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1535:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1559
.L1536:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movq	$0, 96(%rbx)
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L1560
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
.L1539:
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1561
.L1540:
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1560:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1544:
	movl	$1, %r12d
	jmp	.L1533
	.p2align 4,,10
	.p2align 3
.L1559:
	call	*%rax
	testb	%al, %al
	je	.L1537
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	-64(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1541
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1541:
	testq	%r14, %r14
	je	.L1540
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1540
.L1558:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7638:
	.size	napi_throw, .-napi_throw
	.p2align 4
	.globl	napi_throw_error
	.type	napi_throw_error, @function
napi_throw_error:
.LFB7639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1577
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1564
.L1566:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1562:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1603
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1564:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1604
.L1565:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	movq	8(%rbx), %r15
	testq	%r12, %r12
	je	.L1605
	movq	%r15, %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1572
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L1570
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%rax, -120(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1572
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1572
	movq	-120(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1572
	shrw	$8, %ax
	je	.L1572
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1605:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1568:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1606
.L1573:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1572:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1568
	.p2align 4,,10
	.p2align 3
.L1577:
	movl	$1, %r12d
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1604:
	call	*%rax
	testb	%al, %al
	je	.L1566
	jmp	.L1565
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1574
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1574:
	testq	%r13, %r13
	je	.L1573
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1573
.L1603:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7639:
	.size	napi_throw_error, .-napi_throw_error
	.p2align 4
	.globl	napi_throw_type_error
	.type	napi_throw_type_error, @function
napi_throw_type_error:
.LFB7640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1622
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1609
.L1611:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1607:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1648
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1609:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1649
.L1610:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	movq	8(%rbx), %r15
	testq	%r12, %r12
	je	.L1650
	movq	%r15, %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1617
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L1615
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%rax, -120(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1617
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1617
	movq	-120(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1617
	shrw	$8, %ax
	je	.L1617
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1650:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1613:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1651
.L1618:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1617:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1613
	.p2align 4,,10
	.p2align 3
.L1622:
	movl	$1, %r12d
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1649:
	call	*%rax
	testb	%al, %al
	je	.L1611
	jmp	.L1610
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1619
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1619:
	testq	%r13, %r13
	je	.L1618
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1618
.L1648:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7640:
	.size	napi_throw_type_error, .-napi_throw_type_error
	.p2align 4
	.globl	napi_throw_range_error
	.type	napi_throw_range_error, @function
napi_throw_range_error:
.LFB7641:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1667
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1654
.L1656:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1652:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1693
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1654:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1694
.L1655:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	movq	8(%rbx), %r15
	testq	%r12, %r12
	je	.L1695
	movq	%r15, %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1662
	call	_ZN2v89Exception10RangeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %r12
	testq	%r13, %r13
	je	.L1660
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%rax, -120(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1662
	movq	8(%rbx), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1662
	movq	-120(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1662
	shrw	$8, %ax
	je	.L1662
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	%r12, %rsi
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1695:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1658:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1696
.L1663:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1662:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1667:
	movl	$1, %r12d
	jmp	.L1652
	.p2align 4,,10
	.p2align 3
.L1694:
	call	*%rax
	testb	%al, %al
	je	.L1656
	jmp	.L1655
	.p2align 4,,10
	.p2align 3
.L1696:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1664
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1664:
	testq	%r13, %r13
	je	.L1663
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1663
.L1693:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7641:
	.size	napi_throw_range_error, .-napi_throw_range_error
	.p2align 4
	.globl	napi_is_error
	.type	napi_is_error, @function
napi_is_error:
.LFB7642:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1701
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1700
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L1700
	movq	%rsi, %rdi
	call	_ZNK2v85Value13IsNativeErrorEv@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1700:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1701:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7642:
	.size	napi_is_error, .-napi_is_error
	.p2align 4
	.globl	napi_get_value_double
	.type	napi_get_value_double, @function
napi_get_value_double:
.LFB7643:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1714
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L1712
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1712
	movq	%rsi, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L1713
	movabsq	$25769803776, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$6, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1712:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1713:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v86Number5ValueEv@PLT
	xorl	%eax, %eax
	movsd	%xmm0, 0(%r13)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1714:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7643:
	.size	napi_get_value_double, .-napi_get_value_double
	.p2align 4
	.globl	napi_get_value_int32
	.type	napi_get_value_int32, @function
napi_get_value_int32:
.LFB7644:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1730
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L1725
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1725
	movq	%rsi, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movq	%r12, %rdi
	testb	%al, %al
	je	.L1726
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, 0(%r13)
.L1727:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1725:
	.cfi_restore_state
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1722:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1726:
	.cfi_restore_state
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L1728
	movabsq	$25769803776, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$6, %eax
	jmp	.L1722
	.p2align 4,,10
	.p2align 3
.L1730:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1728:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L1738
.L1729:
	sarq	$32, %r12
	movl	%r12d, 0(%r13)
	jmp	.L1727
.L1738:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1729
	.cfi_endproc
.LFE7644:
	.size	napi_get_value_int32, .-napi_get_value_int32
	.p2align 4
	.globl	napi_get_value_uint32
	.type	napi_get_value_uint32, @function
napi_get_value_uint32:
.LFB7645:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1747
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L1742
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1742
	movq	%rsi, %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	movq	%r12, %rdi
	testb	%al, %al
	je	.L1743
	call	_ZNK2v86Uint325ValueEv@PLT
	movl	%eax, 0(%r13)
.L1744:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1742:
	.cfi_restore_state
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1739:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1743:
	.cfi_restore_state
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L1745
	movabsq	$25769803776, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$6, %eax
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1747:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1745:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testb	%al, %al
	je	.L1755
.L1746:
	shrq	$32, %r12
	movl	%r12d, 0(%r13)
	jmp	.L1744
.L1755:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1746
	.cfi_endproc
.LFE7645:
	.size	napi_get_value_uint32, .-napi_get_value_uint32
	.p2align 4
	.globl	napi_get_value_int64
	.type	napi_get_value_int64, @function
napi_get_value_int64:
.LFB7646:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1765
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L1759
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1759
	movq	%rsi, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	movq	%r12, %rdi
	testb	%al, %al
	jne	.L1773
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	jne	.L1761
	movabsq	$25769803776, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$6, %eax
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1756:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1773:
	.cfi_restore_state
	call	_ZNK2v85Int325ValueEv@PLT
	cltq
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1765:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1761:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r12, %rdi
	call	_ZNK2v86Number5ValueEv@PLT
	movsd	.LC2(%rip), %xmm1
	andpd	.LC1(%rip), %xmm0
	ucomisd	%xmm0, %xmm1
	jnb	.L1774
	movq	$0, 0(%r13)
.L1764:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	jmp	.L1756
	.p2align 4,,10
	.p2align 3
.L1774:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L1775
.L1763:
	movq	%rdx, 0(%r13)
	jmp	.L1764
.L1775:
	movq	%rdx, -40(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-40(%rbp), %rdx
	jmp	.L1763
	.cfi_endproc
.LFE7646:
	.size	napi_get_value_int64, .-napi_get_value_int64
	.p2align 4
	.globl	napi_get_value_bigint_int64
	.type	napi_get_value_bigint_int64, @function
napi_get_value_bigint_int64:
.LFB7647:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1781
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1779
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1779
	movq	%rcx, %r14
	testq	%rcx, %rcx
	je	.L1779
	movq	%rsi, %rdi
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	jne	.L1780
	movabsq	$73014444032, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$17, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1779:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1780:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v86BigInt10Int64ValueEPb@PLT
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1781:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7647:
	.size	napi_get_value_bigint_int64, .-napi_get_value_bigint_int64
	.p2align 4
	.globl	napi_get_value_bigint_uint64
	.type	napi_get_value_bigint_uint64, @function
napi_get_value_bigint_uint64:
.LFB7648:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1797
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L1795
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1795
	movq	%rcx, %r14
	testq	%rcx, %rcx
	je	.L1795
	movq	%rsi, %rdi
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	jne	.L1796
	movabsq	$73014444032, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$17, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1795:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1796:
	.cfi_restore_state
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v86BigInt11Uint64ValueEPb@PLT
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1797:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7648:
	.size	napi_get_value_bigint_uint64, .-napi_get_value_bigint_uint64
	.p2align 4
	.globl	napi_get_value_bigint_words
	.type	napi_get_value_bigint_words, @function
napi_get_value_bigint_words:
.LFB7649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1816
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L1811
	movq	%rcx, %r15
	testq	%rcx, %rcx
	je	.L1811
	movq	%rsi, %rdi
	movq	%rdx, %r14
	movq	%r8, %r13
	call	_ZNK2v85Value8IsBigIntEv@PLT
	testb	%al, %al
	je	.L1827
	movq	(%r15), %rax
	movl	%eax, -60(%rbp)
	movq	%r14, %rax
	orq	%r13, %rax
	je	.L1828
	testq	%r14, %r14
	je	.L1811
	testq	%r13, %r13
	je	.L1811
	leaq	-60(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNK2v86BigInt12ToWordsArrayEPiS1_Pm@PLT
	movl	-60(%rbp), %eax
	jmp	.L1814
	.p2align 4,,10
	.p2align 3
.L1811:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L1808:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L1829
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1827:
	.cfi_restore_state
	movabsq	$73014444032, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$17, %eax
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1816:
	movl	$1, %eax
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1828:
	movq	%r12, %rdi
	call	_ZNK2v86BigInt9WordCountEv@PLT
.L1814:
	cltq
	movq	%rax, (%r15)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L1808
.L1829:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7649:
	.size	napi_get_value_bigint_words, .-napi_get_value_bigint_words
	.p2align 4
	.globl	napi_get_value_bool
	.type	napi_get_value_bool, @function
napi_get_value_bool:
.LFB7650:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L1835
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L1833
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L1833
	movq	%rsi, %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L1834
	movabsq	$30064771072, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$7, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1833:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1834:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v87Boolean5ValueEv@PLT
	movb	%al, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1835:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7650:
	.size	napi_get_value_bool, .-napi_get_value_bool
	.p2align 4
	.globl	napi_get_value_string_latin1
	.type	napi_get_value_string_latin1, @function
napi_get_value_string_latin1:
.LFB7651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L1854
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L1849
	movq	(%rsi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L1862
.L1846:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$16, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1862:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1846
	movq	%r8, %r12
	testq	%rdx, %rdx
	je	.L1863
	testq	%rcx, %rcx
	jne	.L1864
	testq	%r8, %r8
	je	.L1850
	movq	$0, (%r8)
.L1850:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1863:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L1865
.L1849:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1854:
	.cfi_restore_state
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1865:
	.cfi_restore_state
	call	_ZNK2v86String6LengthEv@PLT
	cltq
	movq	%rax, (%r12)
	jmp	.L1850
	.p2align 4,,10
	.p2align 3
.L1864:
	movq	8(%rbx), %rsi
	leal	-1(%rcx), %r8d
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movq	%rdx, -24(%rbp)
	call	_ZNK2v86String12WriteOneByteEPNS_7IsolateEPhiii@PLT
	movq	-24(%rbp), %rdx
	cltq
	movb	$0, (%rdx,%rax)
	testq	%r12, %r12
	je	.L1850
	movq	%rax, (%r12)
	jmp	.L1850
	.cfi_endproc
.LFE7651:
	.size	napi_get_value_string_latin1, .-napi_get_value_string_latin1
	.p2align 4
	.globl	napi_get_value_string_utf8
	.type	napi_get_value_string_utf8, @function
napi_get_value_string_utf8:
.LFB7652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L1877
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L1872
	movq	(%rsi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L1885
.L1869:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$16, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1885:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1869
	movq	%r8, %r12
	testq	%rdx, %rdx
	je	.L1886
	testq	%rcx, %rcx
	jne	.L1887
	testq	%r8, %r8
	je	.L1873
	movq	$0, (%r8)
.L1873:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1886:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L1888
.L1872:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1877:
	.cfi_restore_state
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1888:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	cltq
	movq	%rax, (%r12)
	jmp	.L1873
	.p2align 4,,10
	.p2align 3
.L1887:
	movq	8(%rbx), %rsi
	subl	$1, %ecx
	movl	$10, %r9d
	xorl	%r8d, %r8d
	movq	%rdx, -24(%rbp)
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movq	-24(%rbp), %rdx
	cltq
	movb	$0, (%rdx,%rax)
	testq	%r12, %r12
	je	.L1873
	movq	%rax, (%r12)
	jmp	.L1873
	.cfi_endproc
.LFE7652:
	.size	napi_get_value_string_utf8, .-napi_get_value_string_utf8
	.p2align 4
	.globl	napi_get_value_string_utf16
	.type	napi_get_value_string_utf16, @function
napi_get_value_string_utf16:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L1900
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L1895
	movq	(%rsi), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L1908
.L1892:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$16, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1908:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1892
	movq	%r8, %r12
	testq	%rdx, %rdx
	je	.L1909
	testq	%rcx, %rcx
	jne	.L1910
	testq	%r8, %r8
	je	.L1896
	movq	$0, (%r8)
.L1896:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1909:
	.cfi_restore_state
	testq	%r8, %r8
	jne	.L1911
.L1895:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1900:
	.cfi_restore_state
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1911:
	.cfi_restore_state
	call	_ZNK2v86String6LengthEv@PLT
	cltq
	movq	%rax, (%r12)
	jmp	.L1896
	.p2align 4,,10
	.p2align 3
.L1910:
	movq	8(%rbx), %rsi
	leal	-1(%rcx), %r8d
	movl	$2, %r9d
	xorl	%ecx, %ecx
	movq	%rdx, -24(%rbp)
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	movq	-24(%rbp), %rdx
	xorl	%ecx, %ecx
	cltq
	movw	%cx, (%rdx,%rax,2)
	testq	%r12, %r12
	je	.L1896
	movq	%rax, (%r12)
	jmp	.L1896
	.cfi_endproc
.LFE7653:
	.size	napi_get_value_string_utf16, .-napi_get_value_string_utf16
	.p2align 4
	.globl	napi_coerce_to_bool
	.type	napi_coerce_to_bool, @function
napi_coerce_to_bool:
.LFB7654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1924
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1914
.L1916:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1912:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1943
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1914:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1944
.L1915:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L1919
	testq	%r12, %r12
	je	.L1919
	movq	8(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value9ToBooleanEPNS_7IsolateE@PLT
	movq	%r14, %rdi
	movq	%rax, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1918
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1918
	.p2align 4,,10
	.p2align 3
.L1919:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1918:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1945
.L1920:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L1924:
	movl	$1, %r12d
	jmp	.L1912
	.p2align 4,,10
	.p2align 3
.L1944:
	call	*%rax
	testb	%al, %al
	je	.L1916
	jmp	.L1915
	.p2align 4,,10
	.p2align 3
.L1945:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1921
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1921:
	testq	%r13, %r13
	je	.L1920
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1920
.L1943:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7654:
	.size	napi_coerce_to_bool, .-napi_coerce_to_bool
	.p2align 4
	.globl	napi_coerce_to_number
	.type	napi_coerce_to_number, @function
napi_coerce_to_number:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1959
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1948
.L1950:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1946:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1978
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1948:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1979
.L1949:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L1953
	testq	%r12, %r12
	je	.L1953
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value8ToNumberENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L1980
	movq	%rax, (%r12)
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1952
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1953:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1952:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1981
.L1955:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1959:
	movl	$1, %r12d
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L1979:
	call	*%rax
	testb	%al, %al
	je	.L1950
	jmp	.L1949
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1956
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1956:
	testq	%r13, %r13
	je	.L1955
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1955
	.p2align 4,,10
	.p2align 3
.L1980:
	movabsq	$25769803776, %rax
	movq	$0, 88(%rbx)
	movl	$6, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1952
.L1978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7655:
	.size	napi_coerce_to_number, .-napi_coerce_to_number
	.p2align 4
	.globl	napi_coerce_to_object
	.type	napi_coerce_to_object, @function
napi_coerce_to_object:
.LFB7656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1995
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L1984
.L1986:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L1982:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2014
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1984:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2015
.L1985:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L1989
	testq	%r12, %r12
	je	.L1989
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L2016
	movq	%rax, (%r12)
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1988
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L1989:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L1988:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2017
.L1991:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L1995:
	movl	$1, %r12d
	jmp	.L1982
	.p2align 4,,10
	.p2align 3
.L2015:
	call	*%rax
	testb	%al, %al
	je	.L1986
	jmp	.L1985
	.p2align 4,,10
	.p2align 3
.L2017:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L1992
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L1992:
	testq	%r13, %r13
	je	.L1991
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L2016:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L1988
.L2014:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7656:
	.size	napi_coerce_to_object, .-napi_coerce_to_object
	.p2align 4
	.globl	napi_coerce_to_string
	.type	napi_coerce_to_string, @function
napi_coerce_to_string:
.LFB7657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2031
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2020
.L2022:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2018:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2050
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2020:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2051
.L2021:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L2025
	testq	%r12, %r12
	je	.L2025
	movq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L2052
	movq	%rax, (%r12)
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2024
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L2025:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2024:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2053
.L2027:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2031:
	movl	$1, %r12d
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2051:
	call	*%rax
	testb	%al, %al
	je	.L2022
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2028
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2028:
	testq	%r13, %r13
	je	.L2027
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2052:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movl	$3, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2024
.L2050:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7657:
	.size	napi_coerce_to_string, .-napi_coerce_to_string
	.section	.text.unlikely,"ax",@progbits
.LCOLDB3:
	.text
.LHOTB3:
	.p2align 4
	.globl	napi_wrap
	.type	napi_wrap, @function
napi_wrap:
.LFB7658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -136(%rbp)
	movq	%r8, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2080
	cmpq	$0, 24(%rdi)
	movq	%rdi, %r14
	je	.L2056
.L2058:
	movabsq	$42949672960, %rax
	movq	$0, 88(%r14)
	movl	$10, %r12d
	movq	%rax, 96(%r14)
.L2054:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2111
	addq	$120, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2056:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r12
	movq	%rcx, %rbx
	movq	%r9, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2112
.L2057:
	movq	$0, 88(%r14)
	movq	8(%r14), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%r14)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%r14, -64(%rbp)
	testq	%r12, %r12
	je	.L2061
	movq	16(%r14), %rax
	movq	%r12, %rdi
	movq	%rax, -144(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L2113
.L2061:
	movabsq	$4294967296, %rax
	movq	$0, 88(%r14)
	movl	$1, %r12d
	movq	%rax, 96(%r14)
.L2060:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2114
.L2076:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2080:
	movl	$1, %r12d
	jmp	.L2054
	.p2align 4,,10
	.p2align 3
.L2112:
	call	*%rax
	testb	%al, %al
	je	.L2058
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2114:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L2077
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2077:
	testq	%r13, %r13
	je	.L2076
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2076
	.p2align 4,,10
	.p2align 3
.L2113:
	movq	-144(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2073
	movq	%rsi, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2073
	movq	-144(%rbp), %rsi
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	(%rsi), %rax
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L2073
	movq	271(%rax), %rax
	movq	%r12, %rdi
	movq	360(%rax), %rax
	movq	112(%rax), %rdx
	call	_ZN2v86Object10HasPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	testb	%al, %al
	je	.L2115
.L2063:
	shrw	$8, %ax
	jne	.L2061
	testq	%r13, %r13
	je	.L2064
	testq	%rbx, %rbx
	je	.L2061
	movq	-136(%rbp), %xmm0
	movl	$80, %edi
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm0
	xorl	%edi, %edi
	leaq	56(%r14), %rdx
	movq	%r14, 24(%rax)
	movq	%rax, %r8
	movq	%rdx, %xmm1
	movups	%xmm0, 40(%rax)
	movq	%rbx, 32(%rax)
	movw	%di, 56(%rax)
	movl	$0, 60(%rax)
	movb	$0, 64(%rax)
	movq	64(%r14), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r8)
	testq	%rax, %rax
	je	.L2065
	movq	%r8, 16(%rax)
.L2065:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%r8, 64(%r14)
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	movq	%rax, (%r8)
	movq	%r8, -128(%rbp)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %r8
	movl	60(%r8), %esi
	movq	%rax, 72(%r8)
	testl	%esi, %esi
	je	.L2116
.L2066:
	movq	%r8, 0(%r13)
.L2067:
	movq	8(%r14), %rdi
	movq	%r8, %rsi
	call	_ZN2v88External3NewEPNS_7IsolateEPv@PLT
	movq	-144(%rbp), %rbx
	movq	%rax, %r13
	movq	%rbx, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2073
	movq	(%rbx), %rax
	movq	%rbx, %rsi
	movq	55(%rax), %rax
	movq	295(%rax), %rbx
	cmpq	%rbx, _ZN4node11Environment18kNodeContextTagPtrE(%rip)
	jne	.L2073
	movq	271(%rax), %rax
	movq	%r13, %rcx
	movq	%r12, %rdi
	movq	360(%rax), %rax
	movq	112(%rax), %rdx
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L2117
.L2074:
	shrw	$8, %ax
	je	.L2118
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2060
	movabsq	$42949672960, %rax
	movq	$0, 88(%r14)
	movl	$10, %r12d
	movq	%rax, 96(%r14)
	jmp	.L2060
.L2064:
	testq	%rbx, %rbx
	cmovne	-128(%rbp), %r13
	movl	$80, %edi
	movq	%r13, -128(%rbp)
	call	_Znwm@PLT
	xorl	%ecx, %ecx
	leaq	32(%r14), %rdx
	testq	%rbx, %rbx
	movq	-136(%rbp), %xmm0
	movq	%r14, 24(%rax)
	movq	%rax, %r8
	movq	%rbx, 32(%rax)
	movhps	-128(%rbp), %xmm0
	movw	%cx, 56(%rax)
	movl	$0, 60(%rax)
	movb	$1, 64(%rax)
	movups	%xmm0, 40(%rax)
	leaq	56(%r14), %rax
	cmove	%rdx, %rax
	movq	8(%rax), %rdx
	movq	%rax, 16(%r8)
	movq	%rdx, 8(%r8)
	testq	%rdx, %rdx
	je	.L2071
	movq	%r8, 16(%rdx)
.L2071:
	movq	%r8, 8(%rax)
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	movq	%rax, (%r8)
	movq	%r8, -128(%rbp)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-128(%rbp), %r8
	movl	60(%r8), %edx
	movq	%rax, 72(%r8)
	testl	%edx, %edx
	jne	.L2067
	movq	%r8, %rsi
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%rax, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-128(%rbp), %r8
	jmp	.L2067
.L2115:
	movl	%eax, -148(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-148(%rbp), %eax
	jmp	.L2063
.L2116:
	movq	%r8, %rsi
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%rax, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-128(%rbp), %r8
	jmp	.L2066
.L2118:
	leaq	_ZZN6v8impl12_GLOBAL__N_14WrapILNS0_8WrapTypeE0EEE11napi_statusP10napi_env__P12napi_value__PvPFvS5_S8_S8_ES8_PP10napi_ref__E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2117:
	movl	%eax, -128(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-128(%rbp), %eax
	jmp	.L2074
.L2111:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	napi_wrap.cold, @function
napi_wrap.cold:
.LFSB7658:
.L2073:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7658:
	.text
	.size	napi_wrap, .-napi_wrap
	.section	.text.unlikely
	.size	napi_wrap.cold, .-napi_wrap.cold
.LCOLDE3:
	.text
.LHOTE3:
	.section	.text.unlikely
.LCOLDB4:
	.text
.LHOTB4:
	.p2align 4
	.globl	napi_unwrap
	.type	napi_unwrap, @function
napi_unwrap:
.LFB7659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2133
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2121
.L2123:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2119:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2161
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2121:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2162
.L2122:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L2126
	testq	%r12, %r12
	je	.L2126
	movq	8(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L2163
.L2126:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2125:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2164
.L2129:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2133:
	movl	$1, %r12d
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2162:
	call	*%rax
	testb	%al, %al
	je	.L2123
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2130
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2130:
	testq	%r13, %r13
	je	.L2129
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2129
	.p2align 4,,10
	.p2align 3
.L2163:
	testq	%r15, %r15
	je	.L2127
	movq	%r15, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2127
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L2127
	movq	271(%rax), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	112(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2165
.L2128:
	movq	%r13, %rdi
	call	_ZNK2v85Value10IsExternalEv@PLT
	testb	%al, %al
	je	.L2126
	movq	%r13, %rdi
	call	_ZNK2v88External5ValueEv@PLT
	movq	%r14, %rdi
	movq	40(%rax), %rax
	movq	%rax, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2125
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2125
.L2165:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2128
.L2161:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	napi_unwrap.cold, @function
napi_unwrap.cold:
.LFSB7659:
.L2127:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7659:
	.text
	.size	napi_unwrap, .-napi_unwrap
	.section	.text.unlikely
	.size	napi_unwrap.cold, .-napi_unwrap.cold
.LCOLDE4:
	.text
.LHOTE4:
	.section	.text.unlikely
.LCOLDB5:
	.text
.LHOTB5:
	.p2align 4
	.globl	napi_remove_wrap
	.type	napi_remove_wrap, @function
napi_remove_wrap:
.LFB7660:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2188
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2168
.L2170:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2222
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2168:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r15
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2223
.L2169:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movq	$0, 96(%rbx)
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r15, %r15
	je	.L2173
	movq	8(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L2224
.L2173:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2172:
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2225
.L2184:
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2188:
	movl	$1, %r12d
	jmp	.L2166
	.p2align 4,,10
	.p2align 3
.L2223:
	call	*%rax
	testb	%al, %al
	je	.L2170
	jmp	.L2169
	.p2align 4,,10
	.p2align 3
.L2225:
	movq	-64(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2185
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2185:
	testq	%r14, %r14
	je	.L2184
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2184
	.p2align 4,,10
	.p2align 3
.L2224:
	testq	%r14, %r14
	je	.L2177
	movq	%r14, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2177
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L2177
	movq	271(%rax), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	112(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2226
.L2175:
	movq	%rdi, -120(%rbp)
	call	_ZNK2v85Value10IsExternalEv@PLT
	movq	-120(%rbp), %rdi
	testb	%al, %al
	je	.L2173
	call	_ZNK2v88External5ValueEv@PLT
	movq	%rax, %r8
	testq	%r12, %r12
	je	.L2176
	movq	40(%rax), %rax
	movq	%rax, (%r12)
.L2176:
	movq	%r14, %rdi
	movq	%r8, -120(%rbp)
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2177
	movq	(%r14), %rax
	movq	55(%rax), %rax
	movq	295(%rax), %rcx
	cmpq	%rcx, _ZN4node11Environment18kNodeContextTagPtrE(%rip)
	jne	.L2177
	movq	271(%rax), %rax
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	360(%rax), %rax
	movq	112(%rax), %rdx
	call	_ZN2v86Object13DeletePrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	-120(%rbp), %r8
	testb	%al, %al
	je	.L2227
.L2178:
	shrw	$8, %ax
	je	.L2228
	movq	16(%r8), %rdx
	movq	8(%r8), %rax
	testq	%rdx, %rdx
	je	.L2180
	movq	%rax, 8(%rdx)
	movq	8(%r8), %rax
.L2180:
	testq	%rax, %rax
	je	.L2181
	movq	%rdx, 16(%rax)
.L2181:
	movl	60(%r8), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%r8)
	testl	%eax, %eax
	jne	.L2182
	cmpb	$0, 64(%r8)
	jne	.L2182
	cmpb	$0, 56(%r8)
	jne	.L2182
	movb	$1, 64(%r8)
	.p2align 4,,10
	.p2align 3
.L2183:
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2172
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2172
	.p2align 4,,10
	.p2align 3
.L2182:
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
	jmp	.L2183
.L2226:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L2175
.L2228:
	leaq	_ZZN6v8impl12_GLOBAL__N_1L6UnwrapEP10napi_env__P12napi_value__PPvNS0_12UnwrapActionEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2227:
	movl	%eax, -124(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-124(%rbp), %eax
	movq	-120(%rbp), %r8
	jmp	.L2178
.L2222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	napi_remove_wrap.cold, @function
napi_remove_wrap.cold:
.LFSB7660:
.L2177:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	360, %rax
	ud2
	.cfi_endproc
.LFE7660:
	.text
	.size	napi_remove_wrap, .-napi_remove_wrap
	.section	.text.unlikely
	.size	napi_remove_wrap.cold, .-napi_remove_wrap.cold
.LCOLDE5:
	.text
.LHOTE5:
	.p2align 4
	.globl	napi_create_external
	.type	napi_create_external, @function
napi_create_external:
.LFB7661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2246
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2231
.L2233:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2229:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2263
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2231:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rcx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rcx
	movq	%rsi, %r14
	movq	%r8, %r12
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2264
.L2232:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movq	-128(%rbp), %rdx
	je	.L2265
	movq	%r13, %xmm1
	movq	8(%rbx), %rdi
	movq	%r14, %xmm0
	movq	%r14, %rsi
	punpcklqdq	%xmm1, %xmm0
	movq	%rdx, -136(%rbp)
	movaps	%xmm0, -128(%rbp)
	call	_ZN2v88External3NewEPNS_7IsolateEPv@PLT
	movl	$80, %edi
	movq	%rax, %r14
	call	_Znwm@PLT
	movq	-136(%rbp), %rdx
	movdqa	-128(%rbp), %xmm0
	xorl	%ecx, %ecx
	movw	%cx, 56(%rax)
	movq	%rax, %r13
	leaq	32(%rbx), %rcx
	movq	%rdx, 32(%rax)
	testq	%rdx, %rdx
	movq	%rbx, 24(%rax)
	movl	$0, 60(%rax)
	movb	$1, 64(%rax)
	movups	%xmm0, 40(%rax)
	leaq	56(%rbx), %rax
	cmove	%rcx, %rax
	movq	8(%rax), %rdx
	movq	%rax, 16(%r13)
	movq	%rdx, 8(%r13)
	testq	%rdx, %rdx
	je	.L2238
	movq	%r13, 16(%rdx)
.L2238:
	movq	%r13, 8(%rax)
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	8(%rbx), %rdi
	movq	%rax, 0(%r13)
	testq	%r14, %r14
	je	.L2266
	movq	%r14, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movl	60(%r13), %edx
	movq	%rax, 72(%r13)
	testl	%edx, %edx
	je	.L2240
.L2241:
	movq	%r14, (%r12)
	xorl	%r12d, %r12d
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
.L2235:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2267
.L2242:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2265:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2246:
	movl	$1, %r12d
	jmp	.L2229
	.p2align 4,,10
	.p2align 3
.L2264:
	movq	%rdx, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2233
	jmp	.L2232
	.p2align 4,,10
	.p2align 3
.L2267:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L2243
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2243:
	testq	%r13, %r13
	je	.L2242
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2242
	.p2align 4,,10
	.p2align 3
.L2266:
	movq	$0, 72(%r13)
	xorl	%eax, %eax
.L2240:
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2241
.L2263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7661:
	.size	napi_create_external, .-napi_create_external
	.p2align 4
	.globl	napi_get_value_external
	.type	napi_get_value_external, @function
napi_get_value_external:
.LFB7662:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2272
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2271
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L2271
	movq	%rsi, %rdi
	call	_ZNK2v85Value10IsExternalEv@PLT
	testb	%al, %al
	jne	.L2283
.L2271:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2283:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v88External5ValueEv@PLT
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2272:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7662:
	.size	napi_get_value_external, .-napi_get_value_external
	.p2align 4
	.globl	napi_create_reference
	.type	napi_create_reference, @function
napi_create_reference:
.LFB7663:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2291
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2287
	movq	%rcx, %r15
	testq	%rcx, %rcx
	je	.L2287
	movq	%rsi, %rdi
	movl	%edx, %r14d
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L2288
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L2288
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$2, %eax
	jmp	.L2284
	.p2align 4,,10
	.p2align 3
.L2287:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L2284:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2288:
	.cfi_restore_state
	movl	$80, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	leaq	32(%rbx), %rdx
	movups	%xmm0, 40(%rax)
	movq	%rax, %r13
	movq	%rdx, %xmm1
	movq	%rbx, 24(%rax)
	movq	$0, 32(%rax)
	movw	%cx, 56(%rax)
	movl	%r14d, 60(%rax)
	movb	$0, 64(%rax)
	movq	40(%rbx), %rax
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r13)
	testq	%rax, %rax
	je	.L2289
	movq	%r13, 16(%rax)
.L2289:
	movq	%r13, 40(%rbx)
	movq	8(%rbx), %rdi
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%r12, %rsi
	movq	%rax, 0(%r13)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movl	60(%r13), %edx
	movq	%rax, 72(%r13)
	testl	%edx, %edx
	je	.L2302
.L2290:
	movq	%r13, (%r15)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2302:
	.cfi_restore_state
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2290
	.p2align 4,,10
	.p2align 3
.L2291:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7663:
	.size	napi_create_reference, .-napi_create_reference
	.p2align 4
	.globl	napi_delete_reference
	.type	napi_delete_reference, @function
napi_delete_reference:
.LFB7664:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2310
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2321
	movq	16(%rsi), %rdx
	movq	8(%rsi), %rax
	testq	%rdx, %rdx
	je	.L2306
	movq	%rax, 8(%rdx)
	movq	8(%rsi), %rax
.L2306:
	testq	%rax, %rax
	je	.L2307
	movq	%rdx, 16(%rax)
.L2307:
	movl	60(%rsi), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rsi)
	testl	%eax, %eax
	jne	.L2308
	cmpb	$0, 64(%rsi)
	je	.L2322
.L2308:
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	call	*8(%rax)
.L2309:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2321:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2322:
	.cfi_restore_state
	cmpb	$0, 56(%rsi)
	jne	.L2308
	movb	$1, 64(%rsi)
	jmp	.L2309
	.p2align 4,,10
	.p2align 3
.L2310:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7664:
	.size	napi_delete_reference, .-napi_delete_reference
	.p2align 4
	.globl	napi_reference_ref
	.type	napi_reference_ref, @function
napi_reference_ref:
.LFB7665:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2328
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	testq	%rsi, %rsi
	je	.L2336
	movl	60(%rsi), %eax
	leal	1(%rax), %r12d
	movl	%r12d, 60(%rsi)
	cmpl	$1, %r12d
	je	.L2337
	testq	%rdx, %rdx
	je	.L2327
.L2338:
	movl	%r12d, (%rdx)
.L2327:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2336:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2337:
	.cfi_restore_state
	movq	72(%rsi), %rdi
	movq	%rdx, -24(%rbp)
	call	_ZN2v82V89ClearWeakEPm@PLT
	movq	-24(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L2338
	jmp	.L2327
	.p2align 4,,10
	.p2align 3
.L2328:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7665:
	.size	napi_reference_ref, .-napi_reference_ref
	.p2align 4
	.globl	napi_reference_unref
	.type	napi_reference_unref, @function
napi_reference_unref:
.LFB7666:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2345
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2353
	movl	60(%rsi), %eax
	testl	%eax, %eax
	jne	.L2342
	movabsq	$38654705664, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$9, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2342:
	.cfi_restore_state
	leal	-1(%rax), %r13d
	movq	%rdx, %r12
	movl	%r13d, 60(%rsi)
	cmpl	$1, %eax
	je	.L2354
.L2343:
	testq	%r12, %r12
	je	.L2344
	movl	%r13d, (%r12)
.L2344:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2353:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2354:
	.cfi_restore_state
	movq	72(%rsi), %rdi
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2343
	.p2align 4,,10
	.p2align 3
.L2345:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7666:
	.size	napi_reference_unref, .-napi_reference_unref
	.p2align 4
	.globl	napi_get_reference_value
	.type	napi_get_reference_value, @function
napi_get_reference_value:
.LFB7667:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2360
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L2358
	testq	%rdx, %rdx
	je	.L2358
	movq	72(%rsi), %rax
	testq	%rax, %rax
	je	.L2359
	movq	24(%rsi), %rcx
	movq	(%rax), %rsi
	movq	%rdx, -24(%rbp)
	movq	8(%rcx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-24(%rbp), %rdx
.L2359:
	movq	%rax, (%rdx)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2358:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2360:
	.cfi_restore 3
	.cfi_restore 6
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7667:
	.size	napi_get_reference_value, .-napi_get_reference_value
	.p2align 4
	.globl	napi_open_handle_scope
	.type	napi_open_handle_scope, @function
napi_open_handle_scope:
.LFB7668:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2374
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2379
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, (%r12)
	xorl	%eax, %eax
	addl	$1, 104(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2379:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2374:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7668:
	.size	napi_open_handle_scope, .-napi_open_handle_scope
	.p2align 4
	.globl	napi_close_handle_scope
	.type	napi_close_handle_scope, @function
napi_close_handle_scope:
.LFB7669:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2383
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2390
	movl	104(%rdi), %edx
	movl	$13, %eax
	testl	%edx, %edx
	je	.L2380
	subl	$1, %edx
	movl	%edx, 104(%rdi)
	movq	%rsi, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
.L2380:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2390:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2383:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7669:
	.size	napi_close_handle_scope, .-napi_close_handle_scope
	.p2align 4
	.globl	napi_open_escapable_handle_scope
	.type	napi_open_escapable_handle_scope, @function
napi_open_escapable_handle_scope:
.LFB7673:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2394
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L2399
	movl	$40, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, (%r12)
	xorl	%eax, %eax
	addl	$1, 104(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movb	$0, 32(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2399:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2394:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7673:
	.size	napi_open_escapable_handle_scope, .-napi_open_escapable_handle_scope
	.p2align 4
	.globl	napi_close_escapable_handle_scope
	.type	napi_close_escapable_handle_scope, @function
napi_close_escapable_handle_scope:
.LFB7674:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2403
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2409
	movl	104(%rdi), %edx
	movl	$13, %eax
	testl	%edx, %edx
	je	.L2400
	movq	%rsi, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movl	$40, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	subl	$1, 104(%rbx)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
.L2400:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2409:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2403:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7674:
	.size	napi_close_escapable_handle_scope, .-napi_close_escapable_handle_scope
	.p2align 4
	.globl	napi_escape_handle
	.type	napi_escape_handle, @function
napi_escape_handle:
.LFB7678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	je	.L2415
	movq	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L2413
	testq	%rdx, %rdx
	je	.L2413
	testq	%rcx, %rcx
	movq	%rcx, -24(%rbp)
	je	.L2413
	cmpb	$0, 32(%rsi)
	je	.L2423
	movabsq	$51539607552, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$24, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2413:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2423:
	.cfi_restore_state
	movb	$1, 32(%rsi)
	movq	%rdx, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-24(%rbp), %rcx
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2415:
	.cfi_restore_state
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7678:
	.size	napi_escape_handle, .-napi_escape_handle
	.p2align 4
	.globl	napi_new_instance
	.type	napi_new_instance, @function
napi_new_instance:
.LFB7679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2438
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2426
.L2428:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2424:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2464
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2426:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rcx, %r14
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rcx
	movq	%rsi, %r12
	movq	%r8, %r13
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2465
.L2427:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movq	%rdx, -120(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movq	-120(%rbp), %rdx
	je	.L2431
	testq	%r14, %r14
	jne	.L2440
	testq	%rdx, %rdx
	jne	.L2431
.L2440:
	movq	%rdx, -120(%rbp)
	testq	%r13, %r13
	je	.L2431
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	%rax, -128(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-120(%rbp), %rdx
	testb	%al, %al
	jne	.L2466
	.p2align 4,,10
	.p2align 3
.L2431:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2430:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2467
.L2434:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2438:
	movl	$1, %r12d
	jmp	.L2424
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	%rdx, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %rdx
	testb	%al, %al
	je	.L2428
	jmp	.L2427
	.p2align 4,,10
	.p2align 3
.L2467:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L2435
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2435:
	testq	%r13, %r13
	je	.L2434
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2434
	.p2align 4,,10
	.p2align 3
.L2466:
	movq	-128(%rbp), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L2468
	movq	%rax, 0(%r13)
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2430
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2430
.L2468:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2430
.L2464:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7679:
	.size	napi_new_instance, .-napi_new_instance
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"Constructor must be a function"
	.section	.rodata.str1.1
.LC7:
	.string	"ERR_NAPI_CONS_FUNCTION"
	.text
	.p2align 4
	.globl	napi_instanceof
	.type	napi_instanceof, @function
napi_instanceof:
.LFB7680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2484
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2471
.L2473:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2469:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2506
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2471:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2507
.L2472:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r14, %r14
	je	.L2476
	testq	%r12, %r12
	je	.L2476
	movb	$0, (%r12)
	movq	16(%rbx), %rsi
	testq	%r13, %r13
	je	.L2476
	movq	%r13, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L2508
	movq	%rax, %rdi
	movq	%rsi, -120(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-120(%rbp), %rsi
	testb	%al, %al
	jne	.L2478
	leaq	.LC6(%rip), %rdx
	leaq	.LC7(%rip), %rsi
	movq	%rbx, %rdi
	movl	$5, %r12d
	call	napi_throw_type_error
	movq	$0, 88(%rbx)
	movabsq	$21474836480, %rax
	movq	%rax, 96(%rbx)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2476:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2475:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2509
.L2480:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2484:
	movl	$1, %r12d
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2507:
	call	*%rax
	testb	%al, %al
	je	.L2473
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2509:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L2481
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2481:
	testq	%r13, %r13
	je	.L2480
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2480
	.p2align 4,,10
	.p2align 3
.L2508:
	movabsq	$8589934592, %rax
	movq	$0, 88(%rbx)
	movl	$2, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2475
	.p2align 4,,10
	.p2align 3
.L2478:
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN2v85Value10InstanceOfENS_5LocalINS_7ContextEEENS1_INS_6ObjectEEE@PLT
	testb	%al, %al
	jne	.L2479
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2475
.L2479:
	movzbl	%ah, %eax
	movq	%r15, %rdi
	movb	%al, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2475
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2475
.L2506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7680:
	.size	napi_instanceof, .-napi_instanceof
	.p2align 4
	.globl	napi_is_exception_pending
	.type	napi_is_exception_pending, @function
napi_is_exception_pending:
.LFB7681:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2513
	testq	%rsi, %rsi
	je	.L2514
	cmpq	$0, 24(%rdi)
	setne	(%rsi)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2514:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2513:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7681:
	.size	napi_is_exception_pending, .-napi_is_exception_pending
	.p2align 4
	.globl	napi_get_and_clear_last_exception
	.type	napi_get_and_clear_last_exception, @function
napi_get_and_clear_last_exception:
.LFB7682:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2520
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2528
	movq	24(%rdi), %rax
	movq	8(%rdi), %rdi
	testq	%rax, %rax
	je	.L2529
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	24(%rbx), %rdi
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L2519
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2519:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2528:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2529:
	.cfi_restore_state
	addq	$88, %rdi
	movq	%rdi, (%rsi)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2520:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7682:
	.size	napi_get_and_clear_last_exception, .-napi_get_and_clear_last_exception
	.p2align 4
	.globl	napi_is_arraybuffer
	.type	napi_is_arraybuffer, @function
napi_is_arraybuffer:
.LFB7683:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2534
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2533
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2533
	movq	%rsi, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2533:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2534:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7683:
	.size	napi_is_arraybuffer, .-napi_is_arraybuffer
	.p2align 4
	.globl	napi_create_arraybuffer
	.type	napi_create_arraybuffer, @function
napi_create_arraybuffer:
.LFB7684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2554
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2544
.L2546:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2542:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2573
	addq	$136, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2544:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r15
	movq	%rcx, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2574
.L2545:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-176(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -128(%rbp)
	testq	%r12, %r12
	je	.L2575
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rax, %r15
	testq	%r13, %r13
	je	.L2549
	movq	%rax, %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movq	%rax, 0(%r13)
.L2549:
	movq	%r15, (%r12)
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2548
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2575:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2548:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2576
.L2550:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2554:
	movl	$1, %r12d
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2574:
	call	*%rax
	testb	%al, %al
	je	.L2546
	jmp	.L2545
	.p2align 4,,10
	.p2align 3
.L2576:
	movq	-128(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-128(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2551
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2551:
	testq	%r13, %r13
	je	.L2550
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2550
.L2573:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7684:
	.size	napi_create_arraybuffer, .-napi_create_arraybuffer
	.p2align 4
	.globl	napi_create_external_arraybuffer
	.type	napi_create_external_arraybuffer, @function
napi_create_external_arraybuffer:
.LFB7685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2595
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2579
.L2581:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2577:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2617
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2579:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rcx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rcx
	movq	%rsi, %r13
	movq	%r9, %r15
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2618
.L2580:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	movq	%rdx, -136(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r15, %r15
	movq	%rbx, -64(%rbp)
	movq	-136(%rbp), %rdx
	je	.L2619
	movq	8(%rbx), %rdi
	movl	$1, %ecx
	movq	%r13, %rsi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	leaq	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE(%rip), %rdx
	movq	%rax, %r8
	movq	(%rbx), %rax
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2620
.L2585:
	testq	%r12, %r12
	je	.L2586
	movq	%r13, %xmm0
	movl	$80, %edi
	movq	%r8, -136(%rbp)
	movhps	-128(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm0
	xorl	%ecx, %ecx
	leaq	56(%rbx), %rdx
	movq	%rbx, 24(%rax)
	movq	%rax, %r13
	movq	%rdx, %xmm1
	movq	-136(%rbp), %r8
	movups	%xmm0, 40(%rax)
	movq	%r12, 32(%rax)
	movw	%cx, 56(%rax)
	movl	$0, 60(%rax)
	movb	$1, 64(%rax)
	movq	64(%rbx), %rax
	movq	%rax, %xmm0
	testq	%rax, %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r13)
	je	.L2587
	movq	%r13, 16(%rax)
.L2587:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%r13, 64(%rbx)
	movq	8(%rbx), %rdi
	movq	%rax, 0(%r13)
	testq	%r8, %r8
	je	.L2621
	movq	%r8, %rsi
	movq	%r8, -128(%rbp)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movl	60(%r13), %edx
	movq	-128(%rbp), %r8
	movq	%rax, 72(%r13)
	testl	%edx, %edx
	je	.L2589
.L2590:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_120ArrayBufferReferenceE(%rip), %rax
	movq	%rax, 0(%r13)
.L2586:
	movq	%r8, (%r15)
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2583
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2583
	.p2align 4,,10
	.p2align 3
.L2619:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2583:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2622
.L2591:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2595:
	movl	$1, %r12d
	jmp	.L2577
	.p2align 4,,10
	.p2align 3
.L2618:
	movq	%rdx, -136(%rbp)
	call	*%rax
	movq	-136(%rbp), %rdx
	testb	%al, %al
	je	.L2581
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L2622:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2592
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2592:
	testq	%r13, %r13
	je	.L2591
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2620:
	movq	%r8, -136(%rbp)
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	*%rax
	movq	-136(%rbp), %r8
	testb	%al, %al
	jne	.L2585
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2583
.L2621:
	movq	$0, 72(%r13)
	xorl	%eax, %eax
.L2589:
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movq	%r8, -128(%rbp)
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	-128(%rbp), %r8
	jmp	.L2590
.L2617:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7685:
	.size	napi_create_external_arraybuffer, .-napi_create_external_arraybuffer
	.p2align 4
	.globl	napi_get_arraybuffer_info
	.type	napi_get_arraybuffer_info, @function
napi_get_arraybuffer_info:
.LFB7686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$64, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2630
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L2626
	movq	%rsi, %rdi
	movq	%rdx, %r14
	movq	%rcx, %r13
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L2641
.L2626:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L2623:
	movq	-40(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L2642
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2641:
	.cfi_restore_state
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	testq	%r14, %r14
	je	.L2627
	movq	-96(%rbp), %rax
	movq	%rax, (%r14)
.L2627:
	testq	%r13, %r13
	je	.L2628
	movq	-88(%rbp), %rax
	movq	%rax, 0(%r13)
.L2628:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2630:
	movl	$1, %eax
	jmp	.L2623
.L2642:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7686:
	.size	napi_get_arraybuffer_info, .-napi_get_arraybuffer_info
	.p2align 4
	.globl	napi_is_typedarray
	.type	napi_is_typedarray, @function
napi_is_typedarray:
.LFB7687:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2647
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2646
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2646
	movq	%rsi, %rdi
	call	_ZNK2v85Value12IsTypedArrayEv@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2646:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2647:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7687:
	.size	napi_is_typedarray, .-napi_is_typedarray
	.section	.rodata.str1.1
.LC8:
	.string	"Invalid typed array length"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"ERR_NAPI_INVALID_TYPEDARRAY_LENGTH"
	.align 8
.LC10:
	.string	"start offset of Int16Array should be a multiple of 2"
	.align 8
.LC11:
	.string	"ERR_NAPI_INVALID_TYPEDARRAY_ALIGNMENT"
	.align 8
.LC12:
	.string	"start offset of Uint16Array should be a multiple of 2"
	.align 8
.LC13:
	.string	"start offset of Int32Array should be a multiple of 4"
	.align 8
.LC14:
	.string	"start offset of Uint32Array should be a multiple of 4"
	.align 8
.LC15:
	.string	"start offset of Float32Array should be a multiple of 4"
	.align 8
.LC16:
	.string	"start offset of Float64Array should be a multiple of 8"
	.align 8
.LC17:
	.string	"start offset of BigInt64Array should be a multiple of 8"
	.align 8
.LC18:
	.string	"start offset of BigUint64Array should be a multiple of 8"
	.text
	.p2align 4
	.globl	napi_create_typedarray
	.type	napi_create_typedarray, @function
napi_create_typedarray:
.LFB7688:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2691
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2657
.L2659:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r15d
	movq	%rax, 96(%rbx)
.L2655:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2715
	addq	$88, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2657:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rcx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rcx
	movl	%esi, %r15d
	movq	%r8, %r13
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2716
.L2658:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	je	.L2663
	testq	%r9, %r9
	movq	%rdx, -128(%rbp)
	movq	%r9, -120(%rbp)
	je	.L2663
	movq	%r12, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L2717
.L2663:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r15d
	movq	%rax, 96(%rbx)
.L2661:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2718
.L2687:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2655
	.p2align 4,,10
	.p2align 3
.L2717:
	cmpl	$10, %r15d
	ja	.L2663
	leaq	.L2665(%rip), %rsi
	movl	%r15d, %ecx
	movq	-120(%rbp), %r9
	movq	-128(%rbp), %rdx
	movslq	(%rsi,%rcx,4), %rax
	addq	%rsi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2665:
	.long	.L2675-.L2665
	.long	.L2674-.L2665
	.long	.L2673-.L2665
	.long	.L2672-.L2665
	.long	.L2671-.L2665
	.long	.L2670-.L2665
	.long	.L2669-.L2665
	.long	.L2668-.L2665
	.long	.L2667-.L2665
	.long	.L2666-.L2665
	.long	.L2664-.L2665
	.text
	.p2align 4,,10
	.p2align 3
.L2691:
	movl	$1, %r15d
	jmp	.L2655
	.p2align 4,,10
	.p2align 3
.L2716:
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	testb	%al, %al
	je	.L2659
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L2718:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r12
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r13
	testq	%rdi, %rdi
	je	.L2688
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2688:
	testq	%r12, %r12
	je	.L2687
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2687
.L2666:
	testb	$7, %r13b
	je	.L2685
	leaq	.LC17(%rip), %rdx
	leaq	.LC11(%rip), %rsi
	movq	%rbx, %rdi
	call	napi_throw_range_error
	movq	$0, 88(%rbx)
	movabsq	$38654705664, %rax
	movq	%rax, 96(%rbx)
	jmp	.L2661
.L2667:
	testb	$7, %r13b
	je	.L2684
	leaq	.LC16(%rip), %rdx
.L2713:
	leaq	.LC11(%rip), %rsi
.L2714:
	movq	%rbx, %rdi
	movl	$9, %r15d
	call	napi_throw_range_error
	movq	$0, 88(%rbx)
	movabsq	$38654705664, %rax
	movq	%rax, 96(%rbx)
	jmp	.L2661
.L2668:
	testb	$3, %r13b
	je	.L2683
	leaq	.LC15(%rip), %rdx
	jmp	.L2713
.L2670:
	testb	$3, %r13b
	je	.L2681
	leaq	.LC13(%rip), %rdx
	jmp	.L2713
.L2669:
	testb	$3, %r13b
	je	.L2682
	leaq	.LC14(%rip), %rdx
	jmp	.L2713
.L2671:
	testb	$1, %r13b
	je	.L2680
	leaq	.LC12(%rip), %rdx
	jmp	.L2713
.L2672:
	testb	$1, %r13b
	je	.L2679
	leaq	.LC10(%rip), %rdx
	jmp	.L2713
.L2664:
	testb	$7, %r13b
	je	.L2686
	leaq	.LC18(%rip), %rdx
	jmp	.L2713
.L2674:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
.L2677:
	movq	%rax, (%r9)
	movq	%r14, %rdi
	xorl	%r15d, %r15d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2661
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r15d
	movq	%rax, 96(%rbx)
	jmp	.L2661
.L2673:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v817Uint8ClampedArray3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2675:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %r9
	cmpq	%rax, %r15
	ja	.L2678
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v89Int8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-120(%rbp), %r9
	jmp	.L2677
.L2678:
	leaq	.LC8(%rip), %rdx
	leaq	.LC9(%rip), %rsi
	jmp	.L2714
.L2680:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,2), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v811Uint16Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2679:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,2), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v810Int16Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2685:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,8), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v813BigInt64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2686:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,8), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v814BigUint64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2684:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,8), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2681:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,4), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v810Int32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2683:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,4), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v812Float32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2682:
	movq	%r12, %rdi
	leaq	0(%r13,%rdx,4), %r15
	movq	%r9, -128(%rbp)
	movq	%rdx, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, %r15
	ja	.L2678
	movq	-120(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-128(%rbp), %r9
	jmp	.L2677
.L2715:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7688:
	.size	napi_create_typedarray, .-napi_create_typedarray
	.p2align 4
	.globl	napi_get_typedarray_info
	.type	napi_get_typedarray_info, @function
napi_get_typedarray_info:
.LFB7689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, -120(%rbp)
	movq	16(%rbp), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2741
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L2722
	movq	%rsi, %rdi
	movq	%rdx, -128(%rbp)
	movq	%rcx, %r14
	movq	%r9, %r12
	call	_ZNK2v85Value12IsTypedArrayEv@PLT
	testb	%al, %al
	je	.L2722
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2724
	movq	%r13, %rdi
	call	_ZNK2v85Value11IsInt8ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2725
	movl	$0, (%rdx)
.L2724:
	testq	%r14, %r14
	je	.L2736
	movq	%r13, %rdi
	call	_ZN2v810TypedArray6LengthEv@PLT
	movq	%rax, (%r14)
.L2736:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	cmpq	$0, -120(%rbp)
	movq	%rax, %rdx
	je	.L2737
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	-128(%rbp), %rdx
	addq	%rax, %r14
	movq	-120(%rbp), %rax
	movq	%r14, (%rax)
.L2737:
	testq	%r12, %r12
	je	.L2738
	movq	%rdx, (%r12)
.L2738:
	testq	%r15, %r15
	je	.L2739
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%rax, (%r15)
.L2739:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2722:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L2719:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2761
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2741:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L2719
	.p2align 4,,10
	.p2align 3
.L2725:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value12IsUint8ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2726
	movl	$1, (%rdx)
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L2726:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value19IsUint8ClampedArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2727
	movl	$2, (%rdx)
	jmp	.L2724
.L2727:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value12IsInt16ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2728
	movl	$3, (%rdx)
	jmp	.L2724
.L2728:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value13IsUint16ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2729
	movl	$4, (%rdx)
	jmp	.L2724
.L2729:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value12IsInt32ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2730
	movl	$5, (%rdx)
	jmp	.L2724
.L2761:
	call	__stack_chk_fail@PLT
.L2730:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value13IsUint32ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2731
	movl	$6, (%rdx)
	jmp	.L2724
.L2731:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value14IsFloat32ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2732
	movl	$7, (%rdx)
	jmp	.L2724
.L2732:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value14IsFloat64ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2733
	movl	$8, (%rdx)
	jmp	.L2724
.L2733:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value15IsBigInt64ArrayEv@PLT
	movq	-128(%rbp), %rdx
	testb	%al, %al
	je	.L2734
	movl	$9, (%rdx)
	jmp	.L2724
.L2734:
	movq	%r13, %rdi
	movq	%rdx, -128(%rbp)
	call	_ZNK2v85Value16IsBigUint64ArrayEv@PLT
	testb	%al, %al
	je	.L2724
	movq	-128(%rbp), %rdx
	movl	$10, (%rdx)
	jmp	.L2724
	.cfi_endproc
.LFE7689:
	.size	napi_get_typedarray_info, .-napi_get_typedarray_info
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"byte_offset + byte_length should be less than or equal to the size in bytes of the array passed in"
	.align 8
.LC20:
	.string	"ERR_NAPI_INVALID_DATAVIEW_ARGS"
	.text
	.p2align 4
	.globl	napi_create_dataview
	.type	napi_create_dataview, @function
napi_create_dataview:
.LFB7690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2775
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2764
.L2766:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2762:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2797
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2764:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r9
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r14
	movq	%rcx, %r13
	movq	%r8, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2798
.L2765:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r15
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-120(%rbp), %r9
	movq	%rbx, -64(%rbp)
	testq	%r9, %r9
	je	.L2769
	testq	%r12, %r12
	je	.L2769
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	movq	-120(%rbp), %r9
	testb	%al, %al
	jne	.L2799
.L2769:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2768:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2800
.L2771:
	movq	%r15, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2762
	.p2align 4,,10
	.p2align 3
.L2775:
	movl	$1, %r12d
	jmp	.L2762
	.p2align 4,,10
	.p2align 3
.L2798:
	movq	%r9, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %r9
	testb	%al, %al
	je	.L2766
	jmp	.L2765
	.p2align 4,,10
	.p2align 3
.L2800:
	movq	-64(%rbp), %rbx
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r14
	testq	%rdi, %rdi
	je	.L2772
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2772:
	testq	%r13, %r13
	je	.L2771
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2771
	.p2align 4,,10
	.p2align 3
.L2799:
	leaq	(%r14,%r13), %rax
	movq	%r9, %rdi
	movq	%r9, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	cmpq	%rax, -120(%rbp)
	movq	-128(%rbp), %r9
	jbe	.L2770
	leaq	.LC19(%rip), %rdx
	leaq	.LC20(%rip), %rsi
	movq	%rbx, %rdi
	movl	$10, %r12d
	call	napi_throw_range_error
	movq	$0, 88(%rbx)
	movabsq	$42949672960, %rax
	movq	%rax, 96(%rbx)
	jmp	.L2768
	.p2align 4,,10
	.p2align 3
.L2770:
	movq	%r9, %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v88DataView3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%r15, %rdi
	movq	%rax, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2768
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2768
.L2797:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7690:
	.size	napi_create_dataview, .-napi_create_dataview
	.p2align 4
	.globl	napi_is_dataview
	.type	napi_is_dataview, @function
napi_is_dataview:
.LFB7691:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2805
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2804
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2804
	movq	%rsi, %rdi
	call	_ZNK2v85Value10IsDataViewEv@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2804:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2805:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7691:
	.size	napi_is_dataview, .-napi_is_dataview
	.p2align 4
	.globl	napi_get_dataview_info
	.type	napi_get_dataview_info, @function
napi_get_dataview_info:
.LFB7692:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2822
	movq	%rdi, %rbx
	movq	%rsi, %r13
	testq	%rsi, %rsi
	je	.L2816
	movq	%rsi, %rdi
	movq	%rdx, %r15
	movq	%rcx, %r14
	movq	%r8, %r12
	call	_ZNK2v85Value10IsDataViewEv@PLT
	testb	%al, %al
	je	.L2816
	testq	%r15, %r15
	je	.L2817
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, (%r15)
.L2817:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdx
	testq	%r14, %r14
	je	.L2818
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -128(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %r15
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	-128(%rbp), %rdx
	addq	%rax, %r15
	movq	%r15, (%r14)
.L2818:
	testq	%r12, %r12
	je	.L2819
	movq	%rdx, (%r12)
.L2819:
	movq	-120(%rbp), %r14
	testq	%r14, %r14
	je	.L2820
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%rax, (%r14)
.L2820:
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	jmp	.L2813
	.p2align 4,,10
	.p2align 3
.L2816:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L2813:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2839
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2822:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L2813
.L2839:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7692:
	.size	napi_get_dataview_info, .-napi_get_dataview_info
	.p2align 4
	.globl	napi_get_version
	.type	napi_get_version, @function
napi_get_version:
.LFB7693:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2843
	testq	%rsi, %rsi
	je	.L2844
	movl	$6, (%rsi)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2844:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2843:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7693:
	.size	napi_get_version, .-napi_get_version
	.p2align 4
	.globl	napi_create_promise
	.type	napi_create_promise, @function
napi_create_promise:
.LFB7694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2858
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2847
.L2849:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2845:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2877
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2847:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2878
.L2848:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L2852
	testq	%r12, %r12
	je	.L2852
	movq	16(%rbx), %rdi
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2879
	movl	$8, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	movq	$0, (%rax)
	movq	%rax, -120(%rbp)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-120(%rbp), %rdx
	movq	%r15, %rdi
	movq	%rax, (%rdx)
	movq	%rdx, 0(%r13)
	call	_ZN2v87Promise8Resolver10GetPromiseEv@PLT
	movq	%r14, %rdi
	movq	%rax, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2851
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2851
	.p2align 4,,10
	.p2align 3
.L2852:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2851:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2880
.L2854:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2858:
	movl	$1, %r12d
	jmp	.L2845
	.p2align 4,,10
	.p2align 3
.L2878:
	call	*%rax
	testb	%al, %al
	je	.L2849
	jmp	.L2848
	.p2align 4,,10
	.p2align 3
.L2880:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2855
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2855:
	testq	%r13, %r13
	je	.L2854
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2854
	.p2align 4,,10
	.p2align 3
.L2879:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2851
.L2877:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7694:
	.size	napi_create_promise, .-napi_create_promise
	.p2align 4
	.globl	napi_resolve_deferred
	.type	napi_resolve_deferred, @function
napi_resolve_deferred:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2896
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2883
.L2885:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2881:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2921
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2883:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2922
.L2884:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L2923
	movq	8(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	0(%r13), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L2888
	movq	8(%rbx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2888:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN2v87Promise8Resolver7ResolveENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	movq	0(%r13), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L2889
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2889:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	testb	%r12b, %r12b
	je	.L2890
	shrw	$8, %r12w
	je	.L2890
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2887
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2887
	.p2align 4,,10
	.p2align 3
.L2923:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2887:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2924
.L2892:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2890:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2887
	.p2align 4,,10
	.p2align 3
.L2896:
	movl	$1, %r12d
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2922:
	call	*%rax
	testb	%al, %al
	je	.L2885
	jmp	.L2884
	.p2align 4,,10
	.p2align 3
.L2924:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2893
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2893:
	testq	%r13, %r13
	je	.L2892
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2892
.L2921:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7695:
	.size	napi_resolve_deferred, .-napi_resolve_deferred
	.p2align 4
	.globl	napi_reject_deferred
	.type	napi_reject_deferred, @function
napi_reject_deferred:
.LFB7696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2940
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2927
.L2929:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2925:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2965
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2927:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2966
.L2928:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r12, %r12
	je	.L2967
	movq	8(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	0(%r13), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L2932
	movq	8(%rbx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2932:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	movq	0(%r13), %rdi
	movl	%eax, %r12d
	testq	%rdi, %rdi
	je	.L2933
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2933:
	movl	$8, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	testb	%r12b, %r12b
	je	.L2934
	shrw	$8, %r12w
	je	.L2934
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2931
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2931
	.p2align 4,,10
	.p2align 3
.L2967:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2931:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2968
.L2936:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2934:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2931
	.p2align 4,,10
	.p2align 3
.L2940:
	movl	$1, %r12d
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2966:
	call	*%rax
	testb	%al, %al
	je	.L2929
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2968:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2937
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2937:
	testq	%r13, %r13
	je	.L2936
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2936
.L2965:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7696:
	.size	napi_reject_deferred, .-napi_reject_deferred
	.p2align 4
	.globl	napi_is_promise
	.type	napi_is_promise, @function
napi_is_promise:
.LFB7697:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L2973
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L2972
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2972
	movq	%rsi, %rdi
	call	_ZNK2v85Value9IsPromiseEv@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2972:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2973:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7697:
	.size	napi_is_promise, .-napi_is_promise
	.p2align 4
	.globl	napi_create_date
	.type	napi_create_date, @function
napi_create_date:
.LFB7698:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L2993
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L2983
.L2985:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L2981:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3009
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2983:
	.cfi_restore_state
	movq	(%rdi), %rax
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3010
.L2984:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movq	$0, 96(%rbx)
	movq	%r13, %rdi
	movsd	%xmm0, -120(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movsd	-120(%rbp), %xmm0
	je	.L3011
	movq	16(%rbx), %rdi
	call	_ZN2v84Date3NewENS_5LocalINS_7ContextEEEd@PLT
	testq	%rax, %rax
	je	.L3012
	movq	%rax, (%r12)
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L2987
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2987
	.p2align 4,,10
	.p2align 3
.L3011:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L2987:
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L3013
.L2989:
	movq	%r13, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L2981
	.p2align 4,,10
	.p2align 3
.L2993:
	movl	$1, %r12d
	jmp	.L2981
	.p2align 4,,10
	.p2align 3
.L3010:
	movsd	%xmm0, -120(%rbp)
	call	*%rax
	movsd	-120(%rbp), %xmm0
	testb	%al, %al
	je	.L2985
	jmp	.L2984
	.p2align 4,,10
	.p2align 3
.L3013:
	movq	-64(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L2990
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L2990:
	testq	%r14, %r14
	je	.L2989
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L2989
	.p2align 4,,10
	.p2align 3
.L3012:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L2987
.L3009:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7698:
	.size	napi_create_date, .-napi_create_date
	.p2align 4
	.globl	napi_is_date
	.type	napi_is_date, @function
napi_is_date:
.LFB7699:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3018
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L3017
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L3017
	movq	%rsi, %rdi
	call	_ZNK2v85Value6IsDateEv@PLT
	movb	%al, (%r12)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3017:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3018:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7699:
	.size	napi_is_date, .-napi_is_date
	.p2align 4
	.globl	napi_get_date_value
	.type	napi_get_date_value, @function
napi_get_date_value:
.LFB7700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3039
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L3028
.L3030:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L3026:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3058
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3028:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3059
.L3029:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L3033
	testq	%r12, %r12
	je	.L3033
	movq	%r13, %rdi
	call	_ZNK2v85Value6IsDateEv@PLT
	testb	%al, %al
	jne	.L3034
	movabsq	$77309411328, %rax
	movq	$0, 88(%rbx)
	movl	$18, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L3032
	.p2align 4,,10
	.p2align 3
.L3033:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L3032:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L3060
.L3035:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3039:
	movl	$1, %r12d
	jmp	.L3026
	.p2align 4,,10
	.p2align 3
.L3059:
	call	*%rax
	testb	%al, %al
	je	.L3030
	jmp	.L3029
	.p2align 4,,10
	.p2align 3
.L3060:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L3036
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L3036:
	testq	%r13, %r13
	je	.L3035
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L3035
	.p2align 4,,10
	.p2align 3
.L3034:
	movq	%r13, %rdi
	call	_ZNK2v84Date7ValueOfEv@PLT
	movq	%r14, %rdi
	movsd	%xmm0, (%r12)
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L3032
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L3032
.L3058:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7700:
	.size	napi_get_date_value, .-napi_get_date_value
	.p2align 4
	.globl	napi_run_script
	.type	napi_run_script, @function
napi_run_script:
.LFB7701:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3077
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L3063
.L3065:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L3061:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3099
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3063:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r12
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3100
.L3064:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -64(%rbp)
	testq	%r13, %r13
	je	.L3068
	testq	%r12, %r12
	je	.L3068
	movq	0(%r13), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L3069
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L3069
	movq	16(%rbx), %r15
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Script7CompileENS_5LocalINS_7ContextEEENS1_INS_6StringEEEPNS_12ScriptOriginE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L3072
	movq	%r15, %rsi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L3072
	movq	%rax, (%r12)
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L3067
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L3067
	.p2align 4,,10
	.p2align 3
.L3069:
	movabsq	$12884901888, %rax
	movq	$0, 88(%rbx)
	movl	$3, %r12d
	movq	%rax, 96(%rbx)
.L3067:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L3101
.L3073:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3068:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L3067
	.p2align 4,,10
	.p2align 3
.L3077:
	movl	$1, %r12d
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3100:
	call	*%rax
	testb	%al, %al
	je	.L3065
	jmp	.L3064
	.p2align 4,,10
	.p2align 3
.L3101:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L3074
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L3074:
	testq	%r13, %r13
	je	.L3073
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L3073
	.p2align 4,,10
	.p2align 3
.L3072:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L3067
.L3099:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7701:
	.size	napi_run_script, .-napi_run_script
	.p2align 4
	.globl	napi_add_finalizer
	.type	napi_add_finalizer, @function
napi_add_finalizer:
.LFB7702:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3120
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L3104
.L3106:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
.L3102:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3148
	addq	$104, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3104:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r12
	movq	%r9, %r15
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3149
.L3105:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	$0, 96(%rbx)
	movq	%r14, %rdi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r12, %r12
	movq	%rbx, -64(%rbp)
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	je	.L3109
	movq	%r12, %rdi
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3109
	movq	-128(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L3109
	leaq	56(%rbx), %rdx
	testq	%r15, %r15
	movq	-136(%rbp), %r8
	movq	%rcx, -144(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%r13, %xmm0
	je	.L3110
	movq	%r8, %xmm1
	movl	$80, %edi
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movdqa	-128(%rbp), %xmm0
	xorl	%edi, %edi
	movq	-136(%rbp), %rdx
	movq	%rbx, 24(%rax)
	movq	%rax, %r13
	movq	%rcx, 32(%rax)
	movw	%di, 56(%rax)
	movl	$0, 60(%rax)
	movb	$0, 64(%rax)
	movq	%rdx, 16(%rax)
	movups	%xmm0, 40(%rax)
	movq	64(%rbx), %rax
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L3111
	movq	%r13, 16(%rax)
.L3111:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%r13, 64(%rbx)
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, 0(%r13)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movl	60(%r13), %esi
	movq	%rax, 72(%r13)
	testl	%esi, %esi
	je	.L3150
.L3112:
	movq	%r13, (%r15)
.L3113:
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L3108
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r12d
	movq	%rax, 96(%rbx)
	jmp	.L3108
	.p2align 4,,10
	.p2align 3
.L3109:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r12d
	movq	%rax, 96(%rbx)
.L3108:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L3151
.L3116:
	movq	%r14, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L3102
	.p2align 4,,10
	.p2align 3
.L3120:
	movl	$1, %r12d
	jmp	.L3102
	.p2align 4,,10
	.p2align 3
.L3149:
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	testb	%al, %al
	je	.L3106
	jmp	.L3105
	.p2align 4,,10
	.p2align 3
.L3151:
	movq	-64(%rbp), %rbx
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r13
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L3117
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L3117:
	testq	%r13, %r13
	je	.L3116
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L3116
.L3110:
	movq	%r8, %xmm2
	movl	$80, %edi
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -128(%rbp)
	call	_Znwm@PLT
	movq	-144(%rbp), %rcx
	movdqa	-128(%rbp), %xmm0
	movq	-136(%rbp), %rdx
	movq	%rbx, 24(%rax)
	movq	%rax, %r13
	movq	%rcx, 32(%rax)
	xorl	%ecx, %ecx
	movw	%cx, 56(%rax)
	movl	$0, 60(%rax)
	movb	$1, 64(%rax)
	movq	%rdx, 16(%rax)
	movups	%xmm0, 40(%rax)
	movq	64(%rbx), %rax
	movq	%rax, 8(%r13)
	testq	%rax, %rax
	je	.L3114
	movq	%r13, 16(%rax)
.L3114:
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_19ReferenceE(%rip), %rax
	movq	%r13, 64(%rbx)
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, 0(%r13)
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movl	60(%r13), %edx
	movq	%rax, 72(%r13)
	testl	%edx, %edx
	jne	.L3113
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L3113
.L3150:
	xorl	%ecx, %ecx
	leaq	_ZN6v8impl12_GLOBAL__N_19Reference16FinalizeCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L3112
.L3148:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7702:
	.size	napi_add_finalizer, .-napi_add_finalizer
	.p2align 4
	.globl	napi_adjust_external_memory
	.type	napi_adjust_external_memory, @function
napi_adjust_external_memory:
.LFB7703:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3158
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L3166
	movq	8(%rdi), %r14
	movq	%rsi, %r12
	movq	32(%r14), %r15
	addq	%rsi, %r15
	movq	%r15, %rax
	subq	48(%r14), %rax
	movq	%r15, 32(%r14)
	cmpq	$33554432, %rax
	jg	.L3167
	testq	%r12, %r12
	js	.L3168
.L3156:
	je	.L3157
	cmpq	40(%r14), %r15
	jle	.L3157
	movq	%r14, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
.L3157:
	movq	32(%r14), %rax
	movq	%rax, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3168:
	.cfi_restore_state
	addq	40(%r14), %r12
	cmpq	$67108864, %r12
	jle	.L3157
	movq	%r12, 40(%r14)
	jmp	.L3157
	.p2align 4,,10
	.p2align 3
.L3166:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3167:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	testq	%r12, %r12
	jns	.L3156
	jmp	.L3168
	.p2align 4,,10
	.p2align 3
.L3158:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7703:
	.size	napi_adjust_external_memory, .-napi_adjust_external_memory
	.p2align 4
	.globl	napi_set_instance_data
	.type	napi_set_instance_data, @function
napi_set_instance_data:
.LFB7704:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, -32(%rbp)
	movq	%rcx, -40(%rbp)
	testq	%rdi, %rdi
	je	.L3178
	movq	%rdi, %rbx
	movq	120(%rdi), %rdi
	movq	%rdx, %r12
	testq	%rdi, %rdi
	je	.L3171
	movq	16(%rdi), %rdx
	movq	8(%rdi), %rax
	testq	%rdx, %rdx
	je	.L3172
	movq	%rax, 8(%rdx)
	movq	8(%rdi), %rax
.L3172:
	testq	%rax, %rax
	je	.L3173
	movq	%rdx, 16(%rax)
.L3173:
	movl	60(%rdi), %ecx
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rdi)
	testl	%ecx, %ecx
	jne	.L3174
	cmpb	$0, 64(%rdi)
	jne	.L3174
	cmpb	$0, 56(%rdi)
	jne	.L3174
	movb	$1, 64(%rdi)
	.p2align 4,,10
	.p2align 3
.L3171:
	movq	-32(%rbp), %xmm0
	movl	$72, %edi
	movhps	-40(%rbp), %xmm0
	movaps	%xmm0, -32(%rbp)
	call	_Znwm@PLT
	xorl	%edx, %edx
	leaq	32(%rbx), %rcx
	testq	%r12, %r12
	movw	%dx, 56(%rax)
	leaq	56(%rbx), %rdx
	movdqa	-32(%rbp), %xmm0
	leaq	16+_ZTVN6v8impl12_GLOBAL__N_17RefBaseE(%rip), %rsi
	cmove	%rcx, %rdx
	movq	%rbx, 24(%rax)
	movq	%r12, 32(%rax)
	movq	8(%rdx), %rcx
	movq	%rsi, (%rax)
	movl	$0, 60(%rax)
	movb	$1, 64(%rax)
	movq	%rdx, 16(%rax)
	movq	%rcx, 8(%rax)
	movups	%xmm0, 40(%rax)
	testq	%rcx, %rcx
	je	.L3177
	movq	%rax, 16(%rcx)
.L3177:
	movq	%rax, 8(%rdx)
	movq	%rax, 120(%rbx)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3174:
	.cfi_restore_state
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L3171
	.p2align 4,,10
	.p2align 3
.L3178:
	addq	$32, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7704:
	.size	napi_set_instance_data, .-napi_set_instance_data
	.p2align 4
	.globl	napi_get_instance_data
	.type	napi_get_instance_data, @function
napi_get_instance_data:
.LFB7705:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3196
	testq	%rsi, %rsi
	je	.L3200
	movq	120(%rdi), %rax
	testq	%rax, %rax
	je	.L3195
	movq	40(%rax), %rax
.L3195:
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L3200:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3196:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7705:
	.size	napi_get_instance_data, .-napi_get_instance_data
	.p2align 4
	.globl	napi_detach_arraybuffer
	.type	napi_detach_arraybuffer, @function
napi_detach_arraybuffer:
.LFB7706:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L3207
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L3215
	movq	%rsi, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L3204
	movq	$0, 88(%rbx)
	movabsq	$81604378624, %rax
	movq	%rax, 96(%rbx)
	movl	$19, %eax
.L3201:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3215:
	.cfi_restore_state
	movabsq	$4294967296, %rax
	movq	$0, 88(%rdi)
	movq	%rax, 96(%rdi)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3204:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v811ArrayBuffer10IsExternalEv@PLT
	testb	%al, %al
	jne	.L3205
.L3206:
	movabsq	$85899345920, %rax
	movq	$0, 88(%rbx)
	movq	%rax, 96(%rbx)
	movl	$20, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3205:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZNK2v811ArrayBuffer12IsDetachableEv@PLT
	testb	%al, %al
	je	.L3206
	movq	%r12, %rdi
	call	_ZN2v811ArrayBuffer6DetachEv@PLT
	movq	$0, 88(%rbx)
	xorl	%eax, %eax
	movq	$0, 96(%rbx)
	jmp	.L3201
	.p2align 4,,10
	.p2align 3
.L3207:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE7706:
	.size	napi_detach_arraybuffer, .-napi_detach_arraybuffer
	.p2align 4
	.globl	napi_is_detached_arraybuffer
	.type	napi_is_detached_arraybuffer, @function
napi_is_detached_arraybuffer:
.LFB7707:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3222
	movq	%rdi, %rbx
	movq	%rsi, %r12
	testq	%rsi, %rsi
	je	.L3219
	movq	%rdx, %r13
	testq	%rdx, %rdx
	je	.L3219
	movq	%rsi, %rdi
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L3230
.L3220:
	movb	%al, 0(%r13)
	xorl	%eax, %eax
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3219:
	movq	$0, 88(%rbx)
	movabsq	$4294967296, %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L3216:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L3231
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3222:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3230:
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	cmpq	$0, -96(%rbp)
	sete	%al
	jmp	.L3220
.L3231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7707:
	.size	napi_is_detached_arraybuffer, .-napi_is_detached_arraybuffer
	.section	.rodata._ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_.str1.1,"aMS",@progbits,1
.LC21:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,"axG",@progbits,_ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.type	_ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, @function
_ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_:
.LFB9001:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	movabsq	$144115188075855871, %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r10
	movq	(%rdi), %r9
	movq	%r10, %rax
	subq	%r9, %rax
	sarq	$6, %rax
	cmpq	%rdx, %rax
	je	.L3246
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	%rsi, %r8
	subq	%r9, %r12
	testq	%rax, %rax
	je	.L3242
	movabsq	$9223372036854775744, %r15
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L3247
.L3234:
	movq	%r15, %rdi
	movq	%r8, -72(%rbp)
	movq	%r10, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r15
.L3241:
	movdqu	(%r14), %xmm1
	movdqu	16(%r14), %xmm2
	subq	%r8, %r10
	leaq	64(%rbx,%r12), %r11
	movdqu	32(%r14), %xmm3
	movdqu	48(%r14), %xmm4
	leaq	(%r11,%r10), %rax
	movq	%r10, %r14
	movq	%rax, -56(%rbp)
	movups	%xmm1, (%rbx,%r12)
	movups	%xmm2, 16(%rbx,%r12)
	movups	%xmm3, 32(%rbx,%r12)
	movups	%xmm4, 48(%rbx,%r12)
	testq	%r12, %r12
	jg	.L3248
	testq	%r10, %r10
	jg	.L3237
	testq	%r9, %r9
	jne	.L3240
.L3238:
	movq	%rbx, %xmm0
	movq	%r15, 16(%r13)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3248:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%r8, -80(%rbp)
	movq	%r11, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r14, %r14
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r8
	jg	.L3237
.L3240:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L3238
	.p2align 4,,10
	.p2align 3
.L3247:
	testq	%rcx, %rcx
	jne	.L3235
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3237:
	movq	%r14, %rdx
	movq	%r8, %rsi
	movq	%r11, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L3238
	jmp	.L3240
	.p2align 4,,10
	.p2align 3
.L3242:
	movl	$64, %r15d
	jmp	.L3234
.L3246:
	leaq	.LC21(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3235:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	movq	%rdx, %r15
	salq	$6, %r15
	jmp	.L3234
	.cfi_endproc
.LFE9001:
	.size	_ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_, .-_ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	.section	.rodata.str1.1
.LC22:
	.string	"vector::reserve"
	.text
	.p2align 4
	.globl	napi_define_class
	.type	napi_define_class, @function
napi_define_class:
.LFB7583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rbp), %rax
	movq	24(%rbp), %r14
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L3302
	cmpq	$0, 24(%rdi)
	movq	%rdi, %rbx
	je	.L3251
.L3253:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r13d
	movq	%rax, 96(%rbx)
.L3249:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3359
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3251:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdx, %r13
	leaq	_ZNK10napi_env__16can_call_into_jsEv(%rip), %rdx
	movq	%rsi, %r10
	movq	%rcx, %r15
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L3360
.L3252:
	movq	$0, 88(%rbx)
	movq	8(%rbx), %rsi
	leaq	-112(%rbp), %r12
	movq	$0, 96(%rbx)
	movq	%r12, %rdi
	movq	%r8, -208(%rbp)
	movq	%r10, -200(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	testq	%r14, %r14
	movq	%rbx, -64(%rbp)
	movq	-200(%rbp), %r10
	movq	-208(%rbp), %r8
	je	.L3256
	testq	%r15, %r15
	je	.L3256
	cmpq	$0, -184(%rbp)
	je	.L3308
	cmpq	$0, -192(%rbp)
	je	.L3256
.L3308:
	leaq	-144(%rbp), %rax
	movq	8(%rbx), %rsi
	movq	%r10, -216(%rbp)
	movq	%rax, %rdi
	movq	%r8, -224(%rbp)
	movq	%rsi, -200(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-224(%rbp), %r8
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	movq	-216(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L3358
	subq	$8, %rsp
	movq	-200(%rbp), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%r10, -224(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	leaq	-2147483648(%r13), %rdx
	popq	%r9
	popq	%r10
	movq	%rax, -216(%rbp)
	movq	-224(%rbp), %r10
	movabsq	$-2147483650, %rax
	cmpq	%rax, %rdx
	jbe	.L3261
	testq	%r10, %r10
	je	.L3261
	movq	8(%rbx), %rdi
	movq	%r10, %rsi
	movl	%r13d, %ecx
	movl	$1, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3358
	movq	-216(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	cmpq	$0, -184(%rbp)
	je	.L3263
	movq	-192(%rbp), %r15
	xorl	%eax, %eax
	movq	%r12, -248(%rbp)
	movq	$0, -240(%rbp)
	movq	%rax, %r12
	movq	%r15, -264(%rbp)
	movq	%r14, -256(%rbp)
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3362:
	addq	$1, -240(%rbp)
.L3265:
	addq	$1, %r12
	addq	$64, %r15
	cmpq	%r12, -184(%rbp)
	je	.L3361
.L3279:
	testb	$4, 49(%r15)
	jne	.L3362
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L3266
	movq	8(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3303
.L3356:
	movq	24(%r15), %rsi
	movl	48(%r15), %eax
	movq	%r13, -232(%rbp)
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	je	.L3363
.L3270:
	movl	%r13d, %edx
	orl	$2, %edx
	testb	$2, %al
	cmove	%edx, %r13d
	movl	%r13d, %edx
	orl	$4, %edx
	testb	$4, %al
	cmove	%edx, %r13d
	testq	%rsi, %rsi
	je	.L3364
	movq	56(%r15), %rdx
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movq	%rax, %rdx
	movq	-200(%rbp), %rdi
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$1, %r9d
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	32(%r15), %rsi
	movq	%rax, -224(%rbp)
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	jne	.L3298
.L3299:
	movq	-216(%rbp), %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%r9d, %r9d
	movl	%r13d, %r8d
	movq	%r14, %rcx
	movq	-224(%rbp), %rdx
	movq	-232(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3256:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r13d
	movq	%rax, 96(%rbx)
.L3255:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L3365
.L3295:
	movq	%r12, %rdi
	call	_ZN2v88TryCatchD2Ev@PLT
	jmp	.L3249
	.p2align 4,,10
	.p2align 3
.L3302:
	movl	$1, %r13d
	jmp	.L3249
	.p2align 4,,10
	.p2align 3
.L3360:
	movq	%r8, -208(%rbp)
	movq	%rsi, -200(%rbp)
	call	*%rax
	movq	-200(%rbp), %r10
	movq	-208(%rbp), %r8
	testb	%al, %al
	je	.L3253
	jmp	.L3252
	.p2align 4,,10
	.p2align 3
.L3365:
	movq	-64(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	movq	-64(%rbp), %rax
	movq	8(%rax), %r15
	testq	%rdi, %rdi
	je	.L3296
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 24(%rbx)
.L3296:
	testq	%r14, %r14
	je	.L3295
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 24(%rbx)
	jmp	.L3295
.L3366:
	movq	-248(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L3358:
	movabsq	$38654705664, %rax
	movq	$0, 88(%rbx)
	movl	$9, %r13d
	movq	%rax, 96(%rbx)
.L3259:
	movq	-208(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3261:
	movabsq	$4294967296, %rax
	movq	$0, 88(%rbx)
	movl	$1, %r13d
	movq	%rax, 96(%rbx)
	jmp	.L3259
	.p2align 4,,10
	.p2align 3
.L3298:
	movq	56(%r15), %rdx
	movq	%rbx, %rdi
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movq	-200(%rbp), %rdi
	movq	%rax, %rdx
	movl	$1, %r9d
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	jmp	.L3299
	.p2align 4,,10
	.p2align 3
.L3266:
	movq	8(%r15), %r13
	movq	%r13, %rdi
	call	_ZNK2v85Value6IsNameEv@PLT
	testb	%al, %al
	jne	.L3356
	movq	-248(%rbp), %r12
	movl	$4, %r13d
.L3267:
	movl	%r13d, 100(%rbx)
	movl	$0, 96(%rbx)
	movq	$0, 88(%rbx)
	jmp	.L3259
	.p2align 4,,10
	.p2align 3
.L3364:
	movq	$0, -224(%rbp)
	movq	32(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L3298
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.L3277
	movq	56(%r15), %rdx
	movq	%rbx, %rdi
	call	_ZN6v8impl12_GLOBAL__N_1L26CreateFunctionCallbackDataEP10napi_env__PFP12napi_value__S2_P20napi_callback_info__EPv
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L3366
	movq	-216(%rbp), %rsi
	movq	-200(%rbp), %rdi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	movq	%r14, %rdx
	xorl	%r8d, %r8d
	pushq	$0
	movq	-200(%rbp), %rdi
	movq	%rax, %rcx
	movl	$1, %r9d
	leaq	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper6InvokeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	-216(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-232(%rbp), %rsi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3363:
	cmpq	$0, 32(%r15)
	jne	.L3270
	movl	%eax, %r11d
	notl	%r11d
	andl	$1, %r11d
	movl	%r11d, %r13d
	jmp	.L3270
.L3303:
	movq	-248(%rbp), %r12
	movl	$9, %r13d
	jmp	.L3267
.L3361:
	movq	16(%rbx), %rsi
	movq	-216(%rbp), %rdi
	movq	%r12, -184(%rbp)
	movq	-256(%rbp), %r14
	movq	-248(%rbp), %r12
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3367
.L3280:
	movq	-208(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-240(%rbp), %rcx
	movq	%rax, (%r14)
	testq	%rcx, %rcx
	je	.L3281
	movabsq	$144115188075855871, %rax
	pxor	%xmm0, %xmm0
	movq	$0, -160(%rbp)
	movaps	%xmm0, -176(%rbp)
	cmpq	%rax, %rcx
	ja	.L3368
	movq	-240(%rbp), %r13
	salq	$6, %r13
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	-176(%rbp), %r15
	movq	-168(%rbp), %rdx
	movq	%rax, %r8
	subq	%r15, %rdx
	testq	%rdx, %rdx
	jg	.L3369
	testq	%r15, %r15
	jne	.L3284
.L3285:
	addq	%r8, %r13
	movq	%r8, %xmm0
	leaq	-176(%rbp), %r15
	movq	%r8, %rsi
	movq	%r13, -160(%rbp)
	movq	-184(%rbp), %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, -184(%rbp)
	movq	%rbx, %r12
	movq	-264(%rbp), %rbx
	salq	$6, %r13
	movaps	%xmm0, -176(%rbp)
	addq	-192(%rbp), %r13
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3286:
	addq	$64, %rbx
	cmpq	%r13, %rbx
	je	.L3370
.L3288:
	testb	$4, 49(%rbx)
	je	.L3286
	cmpq	-160(%rbp), %rsi
	je	.L3287
	movdqu	(%rbx), %xmm1
	addq	$64, %rsi
	movups	%xmm1, -64(%rsi)
	movdqu	16(%rbx), %xmm2
	movups	%xmm2, -48(%rsi)
	movdqu	32(%rbx), %xmm3
	movups	%xmm3, -32(%rsi)
	movdqu	48(%rbx), %xmm4
	movups	%xmm4, -16(%rsi)
	movq	%rsi, -168(%rbp)
	jmp	.L3286
.L3289:
	movq	(%rbx), %rax
	movq	(%r14), %r15
	movq	%rsi, -184(%rbp)
	movq	%rbx, %rdi
	movq	-176(%rbp), %r13
	call	*16(%rax)
	testb	%al, %al
	je	.L3291
	movq	-184(%rbp), %r8
	movq	%r13, %rcx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%r8, %rdx
	subq	%r13, %rdx
	sarq	$6, %rdx
	call	napi_define_properties.part.0
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L3290
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3281
	call	_ZdlPv@PLT
.L3281:
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L3259
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r13d
	movq	%rax, 96(%rbx)
	jmp	.L3259
.L3370:
	movq	%r12, %rbx
	movq	-184(%rbp), %r12
	cmpq	$0, 24(%rbx)
	je	.L3289
.L3291:
	movabsq	$42949672960, %rax
	movq	$0, 88(%rbx)
	movl	$10, %r13d
	movq	%rax, 96(%rbx)
.L3290:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3259
	call	_ZdlPv@PLT
	jmp	.L3259
.L3277:
	movq	-216(%rbp), %rdi
	movq	40(%r15), %r14
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-232(%rbp), %rsi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	jmp	.L3265
.L3287:
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	_ZNSt6vectorI24napi_property_descriptorSaIS0_EE17_M_realloc_insertIJRKS0_EEEvN9__gnu_cxx17__normal_iteratorIPS0_S2_EEDpOT_
	movq	-168(%rbp), %rsi
	jmp	.L3286
.L3359:
	call	__stack_chk_fail@PLT
.L3367:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L3280
.L3369:
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	memmove@PLT
	movq	%rax, %r8
.L3284:
	movq	%r15, %rdi
	movq	%r8, -200(%rbp)
	call	_ZdlPv@PLT
	movq	-200(%rbp), %r8
	jmp	.L3285
.L3263:
	movq	16(%rbx), %rsi
	movq	-216(%rbp), %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L3371
.L3357:
	movq	-208(%rbp), %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, (%r14)
	jmp	.L3281
.L3368:
	leaq	.LC22(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3371:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%esi, %esi
	jmp	.L3357
	.cfi_endproc
.LFE7583:
	.size	napi_define_class, .-napi_define_class
	.weak	_ZTV10napi_env__
	.section	.data.rel.ro.local._ZTV10napi_env__,"awG",@progbits,_ZTV10napi_env__,comdat
	.align 8
	.type	_ZTV10napi_env__, @object
	.size	_ZTV10napi_env__, 48
_ZTV10napi_env__:
	.quad	0
	.quad	0
	.quad	_ZN10napi_env__D1Ev
	.quad	_ZN10napi_env__D0Ev
	.quad	_ZNK10napi_env__16can_call_into_jsEv
	.quad	_ZNK10napi_env__34mark_arraybuffer_as_untransferableEN2v85LocalINS0_11ArrayBufferEEE
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN6v8impl12_GLOBAL__N_17RefBaseE, @object
	.size	_ZTVN6v8impl12_GLOBAL__N_17RefBaseE, 40
_ZTVN6v8impl12_GLOBAL__N_17RefBaseE:
	.quad	0
	.quad	0
	.quad	_ZN6v8impl12_GLOBAL__N_17RefBaseD1Ev
	.quad	_ZN6v8impl12_GLOBAL__N_17RefBaseD0Ev
	.quad	_ZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEb
	.align 8
	.type	_ZTVN6v8impl12_GLOBAL__N_19ReferenceE, @object
	.size	_ZTVN6v8impl12_GLOBAL__N_19ReferenceE, 40
_ZTVN6v8impl12_GLOBAL__N_19ReferenceE:
	.quad	0
	.quad	0
	.quad	_ZN6v8impl12_GLOBAL__N_19ReferenceD1Ev
	.quad	_ZN6v8impl12_GLOBAL__N_19ReferenceD0Ev
	.quad	_ZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEb
	.align 8
	.type	_ZTVN6v8impl12_GLOBAL__N_120ArrayBufferReferenceE, @object
	.size	_ZTVN6v8impl12_GLOBAL__N_120ArrayBufferReferenceE, 40
_ZTVN6v8impl12_GLOBAL__N_120ArrayBufferReferenceE:
	.quad	0
	.quad	0
	.quad	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD1Ev
	.quad	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReferenceD0Ev
	.quad	_ZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEb
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEEE, @object
	.size	_ZTVN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEEE, 40
_ZTVN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS2_5ValueEEEXadL_ZNS0_14CallbackBundle18function_or_getterEEEE12GetNewTargetEv
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local
	.align 8
	.type	_ZTVN6v8impl12_GLOBAL__N_123FunctionCallbackWrapperE, @object
	.size	_ZTVN6v8impl12_GLOBAL__N_123FunctionCallbackWrapperE, 40
_ZTVN6v8impl12_GLOBAL__N_123FunctionCallbackWrapperE:
	.quad	0
	.quad	0
	.quad	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper12GetNewTargetEv
	.quad	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper4ArgsEPP12napi_value__m
	.quad	_ZN6v8impl12_GLOBAL__N_123FunctionCallbackWrapper14SetReturnValueEP12napi_value__
	.section	.rodata.str1.1
.LC23:
	.string	"../src/js_native_api_v8.h:97"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"(open_callback_scopes) == (open_callback_scopes_before)"
	.align 8
.LC25:
	.ascii	"void napi_env__::CallIntoModule(T&&, U&&) [with T = v8impl::"
	.ascii	"{anonymous}::CallbackWrapperBase<Info, FunctionField>::Invok"
	.ascii	"eCallback() [with Info = v8::Fun"
	.string	"ctionCallbackInfo<v8::Value>; napi_value__* (* v8impl::{anonymous}::CallbackBundle::* FunctionField)(napi_env, napi_callback_info) = &v8impl::{anonymous}::CallbackBundle::function_or_getter]::<lambda(napi_env)>; U = void(napi_env__*, v8::Local<v8::Value>)]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args_0, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args_0, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args_0:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.section	.rodata.str1.1
.LC26:
	.string	"../src/js_native_api_v8.h:96"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"(open_handle_scopes) == (open_handle_scopes_before)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_119CallbackWrapperBaseIN2v820FunctionCallbackInfoINS4_5ValueEEEXadL_ZNS2_14CallbackBundle18function_or_getterEEEE14InvokeCallbackEvEUlPS_E_FvSA_NS4_5LocalIS6_EEEEEvOT_OT0_E4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC25
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"../src/js_native_api_v8.cc:712"
	.align 8
.LC29:
	.string	"obj->SetPrivate(context, (node::Environment::GetCurrent((context))->napi_wrapper()), v8::External::New(env->isolate, reference)).FromJust()"
	.align 8
.LC30:
	.ascii	"napi_status v8impl::{anonymous}::Wrap(napi_env, napi_va"
	.string	"lue, void*, napi_finalize, void*, napi_ref__**) [with v8impl::{anonymous}::WrapType wrap_type = v8impl::<unnamed>::retrievable; napi_env = napi_env__*; napi_value = napi_value__*; napi_finalize = void (*)(napi_env__*, void*, void*); napi_ref = napi_ref__*]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6v8impl12_GLOBAL__N_14WrapILNS0_8WrapTypeE0EEE11napi_statusP10napi_env__P12napi_value__PvPFvS5_S8_S8_ES8_PP10napi_ref__E4args, @object
	.size	_ZZN6v8impl12_GLOBAL__N_14WrapILNS0_8WrapTypeE0EEE11napi_statusP10napi_env__P12napi_value__PvPFvS5_S8_S8_ES8_PP10napi_ref__E4args, 24
_ZZN6v8impl12_GLOBAL__N_14WrapILNS0_8WrapTypeE0EEE11napi_statusP10napi_env__P12napi_value__PvPFvS5_S8_S8_ES8_PP10napi_ref__E4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"void napi_env__::CallIntoModule(T&&, U&&) [with T = v8impl::{anonymous}::RefBase::Finalize(bool)::<lambda(napi_env)>; U = void(napi_env__*, v8::Local<v8::Value>)]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args_0:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC31
	.align 16
	.type	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args, @object
	.size	_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args, 24
_ZZN10napi_env__14CallIntoModuleIZN6v8impl12_GLOBAL__N_17RefBase8FinalizeEbEUlPS_E_FvS4_N2v85LocalINS6_5ValueEEEEEEvOT_OT0_E4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC31
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"../src/js_native_api_v8.cc:762"
	.align 8
.LC33:
	.string	"(env->last_error.error_code) <= (last_status)"
	.align 8
.LC34:
	.string	"napi_status napi_get_last_error_info(napi_env, const napi_extended_error_info**)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZ24napi_get_last_error_infoE4args, @object
	.size	_ZZ24napi_get_last_error_infoE4args, 24
_ZZ24napi_get_last_error_infoE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"Invalid argument"
.LC36:
	.string	"An object was expected"
.LC37:
	.string	"A string was expected"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"A string or symbol was expected"
	.section	.rodata.str1.1
.LC39:
	.string	"A function was expected"
.LC40:
	.string	"A number was expected"
.LC41:
	.string	"A boolean was expected"
.LC42:
	.string	"An array was expected"
.LC43:
	.string	"Unknown failure"
.LC44:
	.string	"An exception is pending"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"The async work item was cancelled"
	.align 8
.LC46:
	.string	"napi_escape_handle already called on scope"
	.section	.rodata.str1.1
.LC47:
	.string	"Invalid handle scope usage"
.LC48:
	.string	"Invalid callback scope usage"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"Thread-safe function queue is full"
	.align 8
.LC50:
	.string	"Thread-safe function handle is closing"
	.section	.rodata.str1.1
.LC51:
	.string	"A bigint was expected"
.LC52:
	.string	"A date was expected"
.LC53:
	.string	"An arraybuffer was expected"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"A detachable arraybuffer was expected"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZL14error_messages, @object
	.size	_ZL14error_messages, 168
_ZL14error_messages:
	.quad	0
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"../src/js_native_api_v8.cc:441"
	.align 8
.LC56:
	.string	"obj->DeletePrivate(context, (node::Environment::GetCurrent((context))->napi_wrapper())) .FromJust()"
	.align 8
.LC57:
	.string	"napi_status v8impl::{anonymous}::Unwrap(napi_env, napi_value, void**, v8impl::{anonymous}::UnwrapAction)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6v8impl12_GLOBAL__N_1L6UnwrapEP10napi_env__P12napi_value__PPvNS0_12UnwrapActionEE4args, @object
	.size	_ZZN6v8impl12_GLOBAL__N_1L6UnwrapEP10napi_env__P12napi_value__PPvNS0_12UnwrapActionEE4args, 24
_ZZN6v8impl12_GLOBAL__N_1L6UnwrapEP10napi_env__P12napi_value__PPvNS0_12UnwrapActionEE4args:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"../src/js_native_api_v8.cc:398"
	.section	.rodata.str1.1
.LC59:
	.string	"obj->IsArrayBuffer()"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"virtual void v8impl::{anonymous}::ArrayBufferReference::Finalize(bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args_0, @object
	.size	_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args_0, 24
_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args_0:
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../src/js_native_api_v8.cc:397"
	.section	.rodata.str1.1
.LC62:
	.string	"!obj.IsEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args, @object
	.size	_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args, 24
_ZZN6v8impl12_GLOBAL__N_120ArrayBufferReference8FinalizeEbE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC60
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
