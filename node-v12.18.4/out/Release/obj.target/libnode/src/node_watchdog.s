	.file	"node_watchdog.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4515:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4515:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4517:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4517:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5801:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5801:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB7708:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE7708:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB7709:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE7709:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB7710:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE7710:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB7712:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L9
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE7712:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZNK4node19TraceSigintWatchdog8SelfSizeEv,"axG",@progbits,_ZNK4node19TraceSigintWatchdog8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node19TraceSigintWatchdog8SelfSizeEv
	.type	_ZNK4node19TraceSigintWatchdog8SelfSizeEv, @function
_ZNK4node19TraceSigintWatchdog8SelfSizeEv:
.LFB7748:
	.cfi_startproc
	endbr64
	movl	$240, %eax
	ret
	.cfi_endproc
.LFE7748:
	.size	_ZNK4node19TraceSigintWatchdog8SelfSizeEv, .-_ZNK4node19TraceSigintWatchdog8SelfSizeEv
	.text
	.p2align 4
	.type	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbENUlP10uv_async_sE_4_FUNES6_, @function
_ZZN4node8WatchdogC4EPN2v87IsolateEmPbENUlP10uv_async_sE_4_FUNES6_:
.LFB7753:
	.cfi_startproc
	endbr64
	subq	$848, %rdi
	jmp	uv_stop@PLT
	.cfi_endproc
.LFE7753:
	.size	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbENUlP10uv_async_sE_4_FUNES6_, .-_ZZN4node8WatchdogC4EPN2v87IsolateEmPbENUlP10uv_async_sE_4_FUNES6_
	.align 2
	.p2align 4
	.globl	_ZN4node8Watchdog3RunEPv
	.type	_ZN4node8Watchdog3RunEPv, @function
_ZN4node8Watchdog3RunEPv:
.LFB7760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	call	uv_run@PLT
	addq	$8, %rsp
	leaq	992(%rbx), %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_close@PLT
	.cfi_endproc
.LFE7760:
	.size	_ZN4node8Watchdog3RunEPv, .-_ZN4node8Watchdog3RunEPv
	.align 2
	.p2align 4
	.globl	_ZN4node14SigintWatchdog12HandleSigintEv
	.type	_ZN4node14SigintWatchdog12HandleSigintEv, @function
_ZN4node14SigintWatchdog12HandleSigintEv:
.LFB7777:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rax
	movq	8(%rdi), %rdi
	movb	$1, (%rax)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v87Isolate18TerminateExecutionEv@PLT
	movl	$1, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7777:
	.size	_ZN4node14SigintWatchdog12HandleSigintEv, .-_ZN4node14SigintWatchdog12HandleSigintEv
	.align 2
	.p2align 4
	.globl	_ZN4node8Watchdog5TimerEP10uv_timer_s
	.type	_ZN4node8Watchdog5TimerEP10uv_timer_s, @function
_ZN4node8Watchdog5TimerEP10uv_timer_s:
.LFB7761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	152(%rdi), %rax
	movq	-992(%rdi), %rdi
	movb	$1, (%rax)
	call	_ZN2v87Isolate18TerminateExecutionEv@PLT
	addq	$8, %rsp
	leaq	-976(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_stop@PLT
	.cfi_endproc
.LFE7761:
	.size	_ZN4node8Watchdog5TimerEP10uv_timer_s, .-_ZN4node8Watchdog5TimerEP10uv_timer_s
	.align 2
	.p2align 4
	.globl	_ZN4node19TraceSigintWatchdog12HandleSigintEv
	.type	_ZN4node19TraceSigintWatchdog12HandleSigintEv, @function
_ZN4node19TraceSigintWatchdog12HandleSigintEv:
.LFB7793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	addq	$104, %rdi
	subq	$8, %rsp
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L21
	movq	16(%r12), %rax
	movq	%r12, %rdx
	leaq	_ZZN4node19TraceSigintWatchdog12HandleSigintEvENUlPN2v87IsolateEPvE_4_FUNES3_S4_(%rip), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate16RequestInterruptEPFvPS0_PvES2_@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	leaq	_ZZN4node19TraceSigintWatchdog12HandleSigintEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7793:
	.size	_ZN4node19TraceSigintWatchdog12HandleSigintEv, .-_ZN4node19TraceSigintWatchdog12HandleSigintEv
	.section	.rodata._ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE.str1.1,"aMS",@progbits,1
.LC1:
	.string	"uv_async_t"
.LC2:
	.string	"handle_"
	.section	.text._ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$72, %edi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movl	$10, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC1(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r12)
	leaq	-32(%rbp), %rsi
	movq	$128, 64(%r12)
	movq	(%rdi), %rax
	movq	%r12, -32(%rbp)
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L24
	movq	88(%rbx), %rdx
	cmpq	72(%rbx), %rax
	je	.L43
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L27
.L29:
	movq	8(%rbx), %rdi
	leaq	.LC2(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L24
	movq	88(%rbx), %rdx
	cmpq	72(%rbx), %rax
	je	.L26
.L27:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L24
	addq	$-128, 64(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
	movq	-8(%rdx), %rax
	movq	504(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L29
.L26:
	movq	-8(%rdx), %rax
	addq	$512, %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	_ZZN4node13MemoryTracker24TrackInlineFieldWithSizeEPKcmS2_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7746:
	.size	_ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10192:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L45
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	ret
	.cfi_endproc
.LFE10192:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10194:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper12HandleSignalEi
	.type	_ZN4node20SigintWatchdogHelper12HandleSignalEi, @function
_ZN4node20SigintWatchdogHelper12HandleSignalEi:
.LFB7799:
	.cfi_startproc
	endbr64
	leaq	128+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	jmp	uv_sem_post@PLT
	.cfi_endproc
.LFE7799:
	.size	_ZN4node20SigintWatchdogHelper12HandleSignalEi, .-_ZN4node20SigintWatchdogHelper12HandleSignalEi
	.section	.text._ZN4node19TraceSigintWatchdogD2Ev,"axG",@progbits,_ZN4node19TraceSigintWatchdogD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node19TraceSigintWatchdogD2Ev
	.type	_ZN4node19TraceSigintWatchdogD2Ev, @function
_ZN4node19TraceSigintWatchdogD2Ev:
.LFB10188:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE10188:
	.size	_ZN4node19TraceSigintWatchdogD2Ev, .-_ZN4node19TraceSigintWatchdogD2Ev
	.weak	_ZN4node19TraceSigintWatchdogD1Ev
	.set	_ZN4node19TraceSigintWatchdogD1Ev,_ZN4node19TraceSigintWatchdogD2Ev
	.section	.text._ZN4node19TraceSigintWatchdogD0Ev,"axG",@progbits,_ZN4node19TraceSigintWatchdogD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node19TraceSigintWatchdogD0Ev
	.type	_ZN4node19TraceSigintWatchdogD0Ev, @function
_ZN4node19TraceSigintWatchdogD0Ev:
.LFB10190:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	56(%rdi), %rdx
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10190:
	.size	_ZN4node19TraceSigintWatchdogD0Ev, .-_ZN4node19TraceSigintWatchdogD0Ev
	.section	.text._ZN4node19TraceSigintWatchdogD2Ev,"axG",@progbits,_ZN4node19TraceSigintWatchdogD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node19TraceSigintWatchdogD1Ev
	.type	_ZThn88_N4node19TraceSigintWatchdogD1Ev, @function
_ZThn88_N4node19TraceSigintWatchdogD1Ev:
.LFB10299:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%rdi), %rdx
	subq	$88, %rdi
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE10299:
	.size	_ZThn88_N4node19TraceSigintWatchdogD1Ev, .-_ZThn88_N4node19TraceSigintWatchdogD1Ev
	.section	.text._ZN4node19TraceSigintWatchdogD0Ev,"axG",@progbits,_ZN4node19TraceSigintWatchdogD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node19TraceSigintWatchdogD0Ev
	.type	_ZThn88_N4node19TraceSigintWatchdogD0Ev, @function
_ZThn88_N4node19TraceSigintWatchdogD0Ev:
.LFB10300:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-88(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	movq	-32(%rdi), %rdx
	movq	-24(%rdi), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$240, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10300:
	.size	_ZThn88_N4node19TraceSigintWatchdogD0Ev, .-_ZThn88_N4node19TraceSigintWatchdogD0Ev
	.text
	.p2align 4
	.globl	_ZThn88_N4node19TraceSigintWatchdog12HandleSigintEv
	.type	_ZThn88_N4node19TraceSigintWatchdog12HandleSigintEv, @function
_ZThn88_N4node19TraceSigintWatchdog12HandleSigintEv:
.LFB10301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-88(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$16, %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L60
	movq	-72(%rbx), %rax
	movq	%r12, %rdx
	leaq	_ZZN4node19TraceSigintWatchdog12HandleSigintEvENUlPN2v87IsolateEPvE_4_FUNES3_S4_(%rip), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate16RequestInterruptEPFvPS0_PvES2_@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	leaq	_ZZN4node19TraceSigintWatchdog12HandleSigintEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10301:
	.size	_ZThn88_N4node19TraceSigintWatchdog12HandleSigintEv, .-_ZThn88_N4node19TraceSigintWatchdog12HandleSigintEv
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper17RunSigintWatchdogEPv
	.type	_ZN4node20SigintWatchdogHelper17RunSigintWatchdogEPv, @function
_ZN4node20SigintWatchdogHelper17RunSigintWatchdogEPv:
.LFB7798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	128+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%r13, %rdi
	call	uv_sem_wait@PLT
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	96+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rbx
	movq	88+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rax
	movzbl	161+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %r12d
	cmpq	%rax, %rbx
	jne	.L65
	testb	%r12b, %r12b
	je	.L77
	cmpq	%rax, %rbx
	jne	.L65
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	subq	$8, %rbx
	cmpq	%rbx, 88+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	je	.L64
.L65:
	movq	-8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpl	$1, %eax
	jne	.L78
.L64:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	testb	%r12b, %r12b
	je	.L67
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movb	$1, 112+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	call	uv_mutex_unlock@PLT
	jmp	.L67
	.cfi_endproc
.LFE7798:
	.size	_ZN4node20SigintWatchdogHelper17RunSigintWatchdogEPv, .-_ZN4node20SigintWatchdogHelper17RunSigintWatchdogEPv
	.section	.text._ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev:
.LFB7747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$19, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC3(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28516, %edx
	movw	%dx, 16(%rax)
	movb	$103, 18(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L82
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L82:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7747:
	.size	_ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev, .-_ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L84
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L91
.L84:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L89
	cmpw	$1040, %cx
	jne	.L85
.L89:
	movq	23(%rdx), %r13
.L87:
	movq	8(%rbx), %r12
	movl	$240, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	movl	$36, %r8d
	leaq	104(%rax), %r14
	addq	$8, %r12
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%r12, %rdx
	movq	%r14, %rcx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node19TraceSigintWatchdogE(%rip), %rax
	movb	$0, 96(%rbx)
	movq	%r14, %rsi
	movq	%rax, (%rbx)
	addq	$120, %rax
	leaq	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_(%rip), %rdx
	movq	%rax, 88(%rbx)
	movq	360(%r13), %rax
	movl	$0, 232(%rbx)
	movq	2360(%rax), %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L92
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L84
	leaq	_ZZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7779:
	.size	_ZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB7533:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L94
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L123
	cmpq	$1, %rax
	jne	.L97
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L98:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L93:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L124
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L98
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L96:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L94:
	cmpb	$37, 1(%rax)
	jne	.L125
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L126
	cmpq	$1, %r13
	jne	.L103
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L104:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L106
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L127
.L106:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L108:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L128
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L110:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L111
	call	_ZdlPv@PLT
.L111:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L93
	call	_ZdlPv@PLT
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L103:
	testq	%r13, %r13
	jne	.L129
	movq	%rbx, %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L102:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L128:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L108
.L125:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L124:
	call	__stack_chk_fail@PLT
.L129:
	movq	%rbx, %rdi
	jmp	.L102
	.cfi_endproc
.LFE7533:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"Failed to initialize uv loop."
.LC5:
	.string	"node::Watchdog::Watchdog()"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node8WatchdogC2EPN2v87IsolateEmPb
	.type	_ZN4node8WatchdogC2EPN2v87IsolateEmPb, @function
_ZN4node8WatchdogC2EPN2v87IsolateEmPb:
.LFB7755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	16(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, (%rdi)
	movq	%rcx, 1144(%rdi)
	movq	%r13, %rdi
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L137
	leaq	864(%r12), %rsi
	leaq	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbENUlP10uv_async_sE_4_FUNES6_(%rip), %rdx
	movq	%r13, %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L138
	leaq	992(%r12), %r15
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	uv_timer_init@PLT
	testl	%eax, %eax
	jne	.L139
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	leaq	_ZN4node8Watchdog5TimerEP10uv_timer_s(%rip), %rsi
	movq	%r15, %rdi
	call	uv_timer_start@PLT
	testl	%eax, %eax
	jne	.L140
	leaq	8(%r12), %rdi
	movq	%r12, %rdx
	leaq	_ZN4node8Watchdog3RunEPv(%rip), %rsi
	call	uv_thread_create@PLT
	testl	%eax, %eax
	jne	.L141
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	leaq	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L137:
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	call	_ZN4node10FatalErrorEPKcS1_@PLT
	.cfi_endproc
.LFE7755:
	.size	_ZN4node8WatchdogC2EPN2v87IsolateEmPb, .-_ZN4node8WatchdogC2EPN2v87IsolateEmPb
	.globl	_ZN4node8WatchdogC1EPN2v87IsolateEmPb
	.set	_ZN4node8WatchdogC1EPN2v87IsolateEmPb,_ZN4node8WatchdogC2EPN2v87IsolateEmPb
	.align 2
	.p2align 4
	.globl	_ZN4node8WatchdogD2Ev
	.type	_ZN4node8WatchdogD2Ev, @function
_ZN4node8WatchdogD2Ev:
.LFB7758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	864(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	uv_async_send@PLT
	leaq	8(%rbx), %rdi
	call	uv_thread_join@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	leaq	16(%rbx), %r12
	call	uv_close@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	uv_run@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node18CheckedUvLoopCloseEP9uv_loop_s@PLT
	.cfi_endproc
.LFE7758:
	.size	_ZN4node8WatchdogD2Ev, .-_ZN4node8WatchdogD2Ev
	.globl	_ZN4node8WatchdogD1Ev
	.set	_ZN4node8WatchdogD1Ev,_ZN4node8WatchdogD2Ev
	.section	.rodata.str1.1
.LC6:
	.string	"TraceSigintWatchdog"
.LC7:
	.string	"start"
.LC8:
	.string	"stop"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node19TraceSigintWatchdog4InitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node19TraceSigintWatchdog4InitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node19TraceSigintWatchdog4InitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r8d, %r8d
	movl	$1, %r9d
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	2680(%rdi), %rdx
	movq	%rsi, -56(%rbp)
	leaq	_ZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rdi), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$19, %ecx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L151
.L145:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L152
.L146:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node19TraceSigintWatchdog4StopERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L153
.L147:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L154
.L148:
	movq	3280(%rbx), %rsi
	movq	-56(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L155
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L152:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L153:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
.LFE7778:
	.size	_ZN4node19TraceSigintWatchdog4InitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node19TraceSigintWatchdog4InitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.p2align 4
	.type	_ZN4node8watchdogL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node8watchdogL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rdx, %rdx
	je	.L159
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L159
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L159
	movq	271(%rax), %rdi
	popq	%rbx
	movq	%r12, %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node19TraceSigintWatchdog4InitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rsi
	xorl	%edi, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node19TraceSigintWatchdog4InitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.cfi_endproc
.LFE7821:
	.size	_ZN4node8watchdogL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node8watchdogL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.align 2
	.p2align 4
	.globl	_ZN4node19TraceSigintWatchdogC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node19TraceSigintWatchdogC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node19TraceSigintWatchdogC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7791:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$36, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	104(%rdi), %r13
	pushq	%r12
	movq	%r13, %rcx
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	16+_ZTVN4node19TraceSigintWatchdogE(%rip), %rax
	movb	$0, 96(%rbx)
	movq	%r13, %rsi
	movq	%rax, (%rbx)
	addq	$120, %rax
	leaq	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_(%rip), %rdx
	movq	%rax, 88(%rbx)
	movq	360(%r12), %rax
	movl	$0, 232(%rbx)
	movq	2360(%rax), %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L164
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	leaq	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7791:
	.size	_ZN4node19TraceSigintWatchdogC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node19TraceSigintWatchdogC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.globl	_ZN4node19TraceSigintWatchdogC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.set	_ZN4node19TraceSigintWatchdogC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE,_ZN4node19TraceSigintWatchdogC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper26InformWatchdogsAboutSignalEv
	.type	_ZN4node20SigintWatchdogHelper26InformWatchdogsAboutSignalEv, @function
_ZN4node20SigintWatchdogHelper26InformWatchdogsAboutSignalEv:
.LFB7800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	call	uv_mutex_lock@PLT
	movq	96+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rbx
	movq	88+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rax
	movzbl	161+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %r12d
	cmpq	%rbx, %rax
	jne	.L169
	testb	%r12b, %r12b
	je	.L175
	cmpq	%rbx, %rax
	jne	.L169
.L167:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	subq	$8, %rbx
	cmpq	%rbx, 88+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	je	.L167
.L169:
	movq	-8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	cmpl	$1, %eax
	jne	.L176
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L175:
	movb	$1, 112+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	jmp	.L167
	.cfi_endproc
.LFE7800:
	.size	_ZN4node20SigintWatchdogHelper26InformWatchdogsAboutSignalEv, .-_ZN4node20SigintWatchdogHelper26InformWatchdogsAboutSignalEv
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper5StartEv
	.type	_ZN4node20SigintWatchdogHelper5StartEv, @function
_ZN4node20SigintWatchdogHelper5StartEv:
.LFB7801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$272, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movl	(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, (%rbx)
	testl	%eax, %eax
	jg	.L183
	cmpb	$0, 160(%rbx)
	jne	.L185
	movb	$0, 112(%rbx)
	leaq	-304(%rbp), %r14
	movb	$0, 161(%rbx)
	movq	%r14, %rdi
	call	sigfillset@PLT
	leaq	-176(%rbp), %rdx
	movq	%r14, %rsi
	movl	$2, %edi
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L186
	movdqa	-128(%rbp), %xmm3
	movdqa	-112(%rbp), %xmm4
	xorl	%esi, %esi
	leaq	120(%rbx), %rdi
	movdqa	-176(%rbp), %xmm0
	movdqa	-160(%rbp), %xmm1
	xorl	%ecx, %ecx
	leaq	_ZN4node20SigintWatchdogHelper17RunSigintWatchdogEPv(%rip), %rdx
	movdqa	-144(%rbp), %xmm2
	movdqa	-96(%rbp), %xmm5
	movaps	%xmm3, -256(%rbp)
	movdqa	-80(%rbp), %xmm6
	movdqa	-64(%rbp), %xmm7
	movaps	%xmm0, -304(%rbp)
	movaps	%xmm1, -288(%rbp)
	movaps	%xmm2, -272(%rbp)
	movaps	%xmm4, -240(%rbp)
	movaps	%xmm5, -224(%rbp)
	movaps	%xmm6, -208(%rbp)
	movaps	%xmm7, -192(%rbp)
	call	pthread_create@PLT
	xorl	%edx, %edx
	movq	%r14, %rsi
	movl	$2, %edi
	movl	%eax, %r12d
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L187
	testl	%r12d, %r12d
	jne	.L178
	movb	$1, 160(%rbx)
	xorl	%edx, %edx
	movl	$2, %edi
	leaq	_ZN4node20SigintWatchdogHelper12HandleSignalEi(%rip), %rsi
	call	_ZN4node21RegisterSignalHandlerEiPFviEb@PLT
.L178:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	addq	$272, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	_ZZN4node20SigintWatchdogHelper5StartEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	_ZZN4node20SigintWatchdogHelper5StartEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	_ZZN4node20SigintWatchdogHelper5StartEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7801:
	.size	_ZN4node20SigintWatchdogHelper5StartEv, .-_ZN4node20SigintWatchdogHelper5StartEv
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper4StopEv
	.type	_ZN4node20SigintWatchdogHelper4StopEv, @function
_ZN4node20SigintWatchdogHelper4StopEv:
.LFB7802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	leaq	48(%rbx), %r12
	call	uv_mutex_lock@PLT
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movl	(%rbx), %eax
	movzbl	112(%rbx), %r14d
	subl	$1, %eax
	movl	%eax, (%rbx)
	testl	%eax, %eax
	jg	.L197
	movb	$1, 161(%rbx)
	movq	88(%rbx), %rax
	cmpq	96(%rbx), %rax
	je	.L192
	movq	%rax, 96(%rbx)
.L192:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	cmpb	$0, 160(%rbx)
	jne	.L193
.L196:
	movb	$0, 112(%rbx)
.L191:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	popq	%rbx
	movl	%r14d, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	leaq	128(%rbx), %rdi
	call	uv_sem_post@PLT
	movq	120(%rbx), %rdi
	xorl	%esi, %esi
	call	pthread_join@PLT
	testl	%eax, %eax
	jne	.L198
	movb	$0, 160(%rbx)
	movl	$1, %edx
	movl	$2, %edi
	movq	_ZN4node10SignalExitEi@GOTPCREL(%rip), %rsi
	call	_ZN4node21RegisterSignalHandlerEiPFviEb@PLT
	movzbl	112(%rbx), %r14d
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L197:
	movb	$0, 112(%rbx)
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	_ZZN4node20SigintWatchdogHelper4StopEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7802:
	.size	_ZN4node20SigintWatchdogHelper4StopEv, .-_ZN4node20SigintWatchdogHelper4StopEv
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelperD2Ev
	.type	_ZN4node20SigintWatchdogHelperD2Ev, @function
_ZN4node20SigintWatchdogHelperD2Ev:
.LFB7819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	$0, (%rdi)
	call	_ZN4node20SigintWatchdogHelper4StopEv
	cmpb	$0, 160(%rbx)
	jne	.L206
	leaq	128(%rbx), %rdi
	call	uv_sem_destroy@PLT
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	leaq	48(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	addq	$8, %rsp
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L206:
	.cfi_restore_state
	leaq	_ZZN4node20SigintWatchdogHelperD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7819:
	.size	_ZN4node20SigintWatchdogHelperD2Ev, .-_ZN4node20SigintWatchdogHelperD2Ev
	.globl	_ZN4node20SigintWatchdogHelperD1Ev
	.set	_ZN4node20SigintWatchdogHelperD1Ev,_ZN4node20SigintWatchdogHelperD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper16HasPendingSignalEv
	.type	_ZN4node20SigintWatchdogHelper16HasPendingSignalEv, @function
_ZN4node20SigintWatchdogHelper16HasPendingSignalEv:
.LFB7803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movzbl	112(%rbx), %r13d
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	popq	%rbx
	movl	%r13d, %eax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7803:
	.size	_ZN4node20SigintWatchdogHelper16HasPendingSignalEv, .-_ZN4node20SigintWatchdogHelper16HasPendingSignalEv
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE
	.type	_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE, @function
_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE:
.LFB7805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	48(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movq	96(%rbx), %rdx
	movq	88(%rbx), %rdi
	movq	%rdx, %rax
	subq	%rdi, %rax
	movq	%rax, %rcx
	sarq	$5, %rax
	sarq	$3, %rcx
	testq	%rax, %rax
	jle	.L210
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L235:
	cmpq	8(%rdi), %r12
	je	.L231
	cmpq	16(%rdi), %r12
	je	.L232
	cmpq	24(%rdi), %r12
	je	.L233
	addq	$32, %rdi
	cmpq	%rax, %rdi
	je	.L234
.L215:
	cmpq	(%rdi), %r12
	jne	.L235
.L211:
	cmpq	%rdi, %rdx
	je	.L220
	leaq	8(%rdi), %rsi
	cmpq	%rsi, %rdx
	je	.L222
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	96(%rbx), %rsi
.L222:
	subq	$8, %rsi
	movq	%r13, %rdi
	movq	%rsi, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	sarq	$3, %rcx
.L210:
	cmpq	$2, %rcx
	je	.L216
	cmpq	$3, %rcx
	je	.L217
	cmpq	$1, %rcx
	je	.L218
.L220:
	leaq	_ZZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	cmpq	(%rdi), %r12
	je	.L211
	addq	$8, %rdi
.L216:
	cmpq	(%rdi), %r12
	je	.L211
	addq	$8, %rdi
.L218:
	cmpq	(%rdi), %r12
	jne	.L220
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L231:
	addq	$8, %rdi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L232:
	addq	$16, %rdi
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L233:
	addq	$24, %rdi
	jmp	.L211
	.cfi_endproc
.LFE7805:
	.size	_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE, .-_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE
	.align 2
	.p2align 4
	.globl	_ZN4node14SigintWatchdogD2Ev
	.type	_ZN4node14SigintWatchdogD2Ev, @function
_ZN4node14SigintWatchdogD2Ev:
.LFB7774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node14SigintWatchdogE(%rip), %rax
	movq	%rdi, %rsi
	movq	%rax, (%rdi)
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE
	leaq	8+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_lock@PLT
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movl	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %eax
	subl	$1, %eax
	movl	%eax, _ZN4node20SigintWatchdogHelper8instanceE(%rip)
	testl	%eax, %eax
	jg	.L244
	movq	88+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rax
	cmpq	96+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rax
	movb	$1, 161+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	je	.L239
	movq	%rax, 96+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
.L239:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	cmpb	$0, 160+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	jne	.L240
.L243:
	leaq	8+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movb	$0, 112+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L240:
	.cfi_restore_state
	leaq	128+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_sem_post@PLT
	movq	120+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	xorl	%esi, %esi
	call	pthread_join@PLT
	testl	%eax, %eax
	jne	.L245
	movq	_ZN4node10SignalExitEi@GOTPCREL(%rip), %rsi
	movl	$1, %edx
	movl	$2, %edi
	movb	$0, 160+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	call	_ZN4node21RegisterSignalHandlerEiPFviEb@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movb	$0, 112+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	call	uv_mutex_unlock@PLT
	leaq	8+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	leaq	_ZZN4node20SigintWatchdogHelper4StopEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7774:
	.size	_ZN4node14SigintWatchdogD2Ev, .-_ZN4node14SigintWatchdogD2Ev
	.globl	_ZN4node14SigintWatchdogD1Ev
	.set	_ZN4node14SigintWatchdogD1Ev,_ZN4node14SigintWatchdogD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node14SigintWatchdogD0Ev
	.type	_ZN4node14SigintWatchdogD0Ev, @function
_ZN4node14SigintWatchdogD0Ev:
.LFB7776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node14SigintWatchdogE(%rip), %rax
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE
	leaq	8+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_lock@PLT
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movl	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %eax
	subl	$1, %eax
	movl	%eax, _ZN4node20SigintWatchdogHelper8instanceE(%rip)
	testl	%eax, %eax
	jg	.L254
	movq	88+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rax
	cmpq	96+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rax
	movb	$1, 161+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	je	.L249
	movq	%rax, 96+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
.L249:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	cmpb	$0, 160+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	jne	.L250
.L253:
	movb	$0, 112+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
.L248:
	leaq	8+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	leaq	128+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_sem_post@PLT
	movq	120+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	xorl	%esi, %esi
	call	pthread_join@PLT
	testl	%eax, %eax
	jne	.L255
	movq	_ZN4node10SignalExitEi@GOTPCREL(%rip), %rsi
	movl	$1, %edx
	movl	$2, %edi
	movb	$0, 160+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	call	_ZN4node21RegisterSignalHandlerEiPFviEb@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movb	$0, 112+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
	call	uv_mutex_unlock@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	_ZZN4node20SigintWatchdogHelper4StopEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7776:
	.size	_ZN4node14SigintWatchdogD0Ev, .-_ZN4node14SigintWatchdogD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node19TraceSigintWatchdog4StopERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node19TraceSigintWatchdog4StopERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node19TraceSigintWatchdog4StopERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L264
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L262
	cmpw	$1040, %cx
	jne	.L258
.L262:
	movq	23(%rdx), %rax
.L260:
	testq	%rax, %rax
	je	.L256
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	leaq	88(%rax), %rsi
	call	_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE
	addq	$8, %rsp
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node20SigintWatchdogHelper4StopEv
	.p2align 4,,10
	.p2align 3
.L256:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L264:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7781:
	.size	_ZN4node19TraceSigintWatchdog4StopERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node19TraceSigintWatchdog4StopERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelperC2Ev
	.type	_ZN4node20SigintWatchdogHelperC2Ev, @function
_ZN4node20SigintWatchdogHelperC2Ev:
.LFB7816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	addq	$8, %rdi
	subq	$8, %rsp
	movl	$0, -8(%rdi)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L267
	leaq	48(%rbx), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L267
	xorl	%eax, %eax
	movq	$0, 104(%rbx)
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movb	$0, 112(%rbx)
	leaq	128(%rbx), %rdi
	movw	%ax, 160(%rbx)
	movups	%xmm0, 88(%rbx)
	call	uv_sem_init@PLT
	testl	%eax, %eax
	jne	.L270
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L267:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	leaq	_ZZN4node20SigintWatchdogHelperC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7816:
	.size	_ZN4node20SigintWatchdogHelperC2Ev, .-_ZN4node20SigintWatchdogHelperC2Ev
	.globl	_ZN4node20SigintWatchdogHelperC1Ev
	.set	_ZN4node20SigintWatchdogHelperC1Ev,_ZN4node20SigintWatchdogHelperC2Ev
	.p2align 4
	.globl	_Z18_register_watchdogv
	.type	_Z18_register_watchdogv, @function
_Z18_register_watchdogv:
.LFB7822:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7822:
	.size	_Z18_register_watchdogv, .-_Z18_register_watchdogv
	.section	.text.unlikely._ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB9044:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L273
	call	__stack_chk_fail@PLT
.L273:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9044:
	.size	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_:
.LFB8583:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L275
	call	_ZdlPv@PLT
.L275:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L277
	call	__stack_chk_fail@PLT
.L277:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8583:
	.size	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"KEYBOARD_INTERRUPT: Script execution was interrupted by `SIGINT`\n"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZN4node19TraceSigintWatchdog15HandleInterruptEv.part.0, @function
_ZN4node19TraceSigintWatchdog15HandleInterruptEv.part.0:
.LFB10296:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC9(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	16(%rdi), %r12
	movq	stderr(%rip), %rdi
	call	_ZN4node7FPrintFIJEEEvP8_IO_FILEPKcDpOT_
	cmpl	$2, 232(%rbx)
	jne	.L280
	movq	352(%r12), %rdi
	movl	$10, %esi
	movl	$127, %edx
	call	_ZN2v810StackTrace17CurrentStackTraceEPNS_7IsolateEiNS0_17StackTraceOptionsE@PLT
	movq	352(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE@PLT
.L280:
	movb	$0, 96(%rbx)
	leaq	88(%rbx), %rsi
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movl	$0, 232(%rbx)
	call	_ZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseE
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	_ZN4node20SigintWatchdogHelper4StopEv
	popq	%rbx
	movl	$2, %edi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	raise@PLT
	.cfi_endproc
.LFE10296:
	.size	_ZN4node19TraceSigintWatchdog15HandleInterruptEv.part.0, .-_ZN4node19TraceSigintWatchdog15HandleInterruptEv.part.0
	.align 2
.LCOLDB10:
	.text
.LHOTB10:
	.align 2
	.p2align 4
	.globl	_ZN4node19TraceSigintWatchdog15HandleInterruptEv
	.type	_ZN4node19TraceSigintWatchdog15HandleInterruptEv, @function
_ZN4node19TraceSigintWatchdog15HandleInterruptEv:
.LFB7797:
	.cfi_startproc
	endbr64
	cmpb	$0, 96(%rdi)
	jne	.L282
	movl	232(%rdi), %eax
	movb	$1, 96(%rdi)
	testl	%eax, %eax
	jne	.L286
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node19TraceSigintWatchdog15HandleInterruptEv.cold, @function
_ZN4node19TraceSigintWatchdog15HandleInterruptEv.cold:
.LFSB7797:
.L286:
	jmp	_ZN4node19TraceSigintWatchdog15HandleInterruptEv.part.0
	.cfi_endproc
.LFE7797:
	.text
	.size	_ZN4node19TraceSigintWatchdog15HandleInterruptEv, .-_ZN4node19TraceSigintWatchdog15HandleInterruptEv
	.section	.text.unlikely
	.size	_ZN4node19TraceSigintWatchdog15HandleInterruptEv.cold, .-_ZN4node19TraceSigintWatchdog15HandleInterruptEv.cold
.LCOLDE10:
	.text
.LHOTE10:
	.section	.text.unlikely
.LCOLDB11:
	.text
.LHOTB11:
	.p2align 4
	.type	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_, @function
_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_:
.LFB7789:
	.cfi_startproc
	endbr64
	movl	$1, 128(%rdi)
	subq	$104, %rdi
	cmpb	$0, 96(%rdi)
	je	.L289
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_.cold, @function
_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_.cold:
.LFSB7789:
.L289:
	movb	$1, 96(%rdi)
	jmp	_ZN4node19TraceSigintWatchdog15HandleInterruptEv.part.0
	.cfi_endproc
.LFE7789:
	.text
	.size	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_, .-_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_
	.section	.text.unlikely
	.size	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_.cold, .-_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEENUlP10uv_async_sE_4_FUNES8_.cold
.LCOLDE11:
	.text
.LHOTE11:
	.p2align 4
	.type	_ZZN4node19TraceSigintWatchdog12HandleSigintEvENUlPN2v87IsolateEPvE_4_FUNES3_S4_, @function
_ZZN4node19TraceSigintWatchdog12HandleSigintEvENUlPN2v87IsolateEPvE_4_FUNES3_S4_:
.LFB7795:
	.cfi_startproc
	endbr64
	movl	232(%rsi), %eax
	testl	%eax, %eax
	jne	.L291
	movl	$2, 232(%rsi)
	cmpb	$0, 96(%rsi)
	je	.L296
.L290:
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	cmpb	$0, 96(%rsi)
	jne	.L290
.L296:
	movb	$1, 96(%rsi)
	movq	%rsi, %rdi
	jmp	_ZN4node19TraceSigintWatchdog15HandleInterruptEv.part.0
	.cfi_endproc
.LFE7795:
	.size	_ZZN4node19TraceSigintWatchdog12HandleSigintEvENUlPN2v87IsolateEPvE_4_FUNES3_S4_, .-_ZZN4node19TraceSigintWatchdog12HandleSigintEvENUlPN2v87IsolateEPvE_4_FUNES3_S4_
	.section	.rodata._ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_.str1.1,"aMS",@progbits,1
.LC12:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,"axG",@progbits,_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.type	_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, @function
_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_:
.LFB9056:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L311
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L307
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L312
.L299:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L306:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L313
	testq	%r13, %r13
	jg	.L302
	testq	%r9, %r9
	jne	.L305
.L303:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L302
.L305:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L312:
	testq	%rsi, %rsi
	jne	.L300
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L302:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L303
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L307:
	movl	$8, %r14d
	jmp	.L299
.L311:
	leaq	.LC12(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L300:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L299
	.cfi_endproc
.LFE9056:
	.size	_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_, .-_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node20SigintWatchdogHelper8RegisterEPNS_18SigintWatchdogBaseE
	.type	_ZN4node20SigintWatchdogHelper8RegisterEPNS_18SigintWatchdogBaseE, @function
_ZN4node20SigintWatchdogHelper8RegisterEPNS_18SigintWatchdogBaseE:
.LFB7804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	48(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	movq	%rsi, -24(%rbp)
	call	uv_mutex_lock@PLT
	movq	96(%rbx), %rsi
	cmpq	104(%rbx), %rsi
	je	.L315
	movq	-24(%rbp), %rax
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 96(%rbx)
.L316:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	leaq	-24(%rbp), %rdx
	leaq	88(%rbx), %rdi
	call	_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L316
	.cfi_endproc
.LFE7804:
	.size	_ZN4node20SigintWatchdogHelper8RegisterEPNS_18SigintWatchdogBaseE, .-_ZN4node20SigintWatchdogHelper8RegisterEPNS_18SigintWatchdogBaseE
	.align 2
	.p2align 4
	.globl	_ZN4node14SigintWatchdogC2EPN2v87IsolateEPb
	.type	_ZN4node14SigintWatchdogC2EPN2v87IsolateEPb, @function
_ZN4node14SigintWatchdogC2EPN2v87IsolateEPb:
.LFB7771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node14SigintWatchdogE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rdi, -16(%rbp)
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	96+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rsi
	cmpq	104+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rsi
	je	.L319
	movq	-16(%rbp), %rax
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 96+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
.L320:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	_ZN4node20SigintWatchdogHelper5StartEv
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L323
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	leaq	-16(%rbp), %rdx
	leaq	88+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L320
.L323:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7771:
	.size	_ZN4node14SigintWatchdogC2EPN2v87IsolateEPb, .-_ZN4node14SigintWatchdogC2EPN2v87IsolateEPb
	.globl	_ZN4node14SigintWatchdogC1EPN2v87IsolateEPb
	.set	_ZN4node14SigintWatchdogC1EPN2v87IsolateEPb,_ZN4node14SigintWatchdogC2EPN2v87IsolateEPb
	.align 2
	.p2align 4
	.globl	_ZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L341
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L333
	cmpw	$1040, %cx
	jne	.L326
.L333:
	movq	23(%rdx), %rax
.L328:
	testq	%rax, %rax
	je	.L324
	addq	$88, %rax
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movq	%rax, -32(%rbp)
	call	uv_mutex_lock@PLT
	movq	96+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rsi
	cmpq	104+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rsi
	je	.L330
	movq	-32(%rbp), %rax
	addq	$8, %rsi
	movq	%rax, -8(%rsi)
	movq	%rsi, 96+_ZN4node20SigintWatchdogHelper8instanceE(%rip)
.L331:
	leaq	48+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	_ZN4node20SigintWatchdogHelper5StartEv
	testl	%eax, %eax
	jne	.L342
.L324:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L343
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	-32(%rbp), %rdx
	leaq	88+_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	call	_ZNSt6vectorIPN4node18SigintWatchdogBaseESaIS2_EE17_M_realloc_insertIJRKS2_EEEvN9__gnu_cxx17__normal_iteratorIPS2_S4_EEDpOT_
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	_ZZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L343:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7780:
	.size	_ZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node8WatchdogC2EPN2v87IsolateEmPb, @function
_GLOBAL__sub_I__ZN4node8WatchdogC2EPN2v87IsolateEmPb:
.LFB10226:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node20SigintWatchdogHelperC1Ev
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rsi
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZN4node20SigintWatchdogHelperD1Ev(%rip), %rdi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE10226:
	.size	_GLOBAL__sub_I__ZN4node8WatchdogC2EPN2v87IsolateEmPb, .-_GLOBAL__sub_I__ZN4node8WatchdogC2EPN2v87IsolateEmPb
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node8WatchdogC2EPN2v87IsolateEmPb
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node14SigintWatchdogE
	.section	.data.rel.ro.local._ZTVN4node14SigintWatchdogE,"awG",@progbits,_ZTVN4node14SigintWatchdogE,comdat
	.align 8
	.type	_ZTVN4node14SigintWatchdogE, @object
	.size	_ZTVN4node14SigintWatchdogE, 40
_ZTVN4node14SigintWatchdogE:
	.quad	0
	.quad	0
	.quad	_ZN4node14SigintWatchdogD1Ev
	.quad	_ZN4node14SigintWatchdogD0Ev
	.quad	_ZN4node14SigintWatchdog12HandleSigintEv
	.weak	_ZTVN4node19TraceSigintWatchdogE
	.section	.data.rel.ro._ZTVN4node19TraceSigintWatchdogE,"awG",@progbits,_ZTVN4node19TraceSigintWatchdogE,comdat
	.align 8
	.type	_ZTVN4node19TraceSigintWatchdogE, @object
	.size	_ZTVN4node19TraceSigintWatchdogE, 160
_ZTVN4node19TraceSigintWatchdogE:
	.quad	0
	.quad	0
	.quad	_ZN4node19TraceSigintWatchdogD1Ev
	.quad	_ZN4node19TraceSigintWatchdogD0Ev
	.quad	_ZNK4node19TraceSigintWatchdog10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node19TraceSigintWatchdog14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node19TraceSigintWatchdog8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node19TraceSigintWatchdog12HandleSigintEv
	.quad	-88
	.quad	0
	.quad	_ZThn88_N4node19TraceSigintWatchdogD1Ev
	.quad	_ZThn88_N4node19TraceSigintWatchdogD0Ev
	.quad	_ZThn88_N4node19TraceSigintWatchdog12HandleSigintEv
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC13:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC15:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/node_watchdog.cc"
.LC17:
	.string	"watchdog"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC16
	.quad	0
	.quad	_ZN4node8watchdogL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC17
	.quad	0
	.quad	0
	.globl	_ZN4node20SigintWatchdogHelper8instanceE
	.bss
	.align 32
	.type	_ZN4node20SigintWatchdogHelper8instanceE, @object
	.size	_ZN4node20SigintWatchdogHelper8instanceE, 168
_ZN4node20SigintWatchdogHelper8instanceE:
	.zero	168
	.section	.rodata.str1.1
.LC18:
	.string	"../src/node_watchdog.cc:416"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"(has_running_thread_) == (false)"
	.align 8
.LC20:
	.string	"node::SigintWatchdogHelper::~SigintWatchdogHelper()"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node20SigintWatchdogHelperD4EvE4args, @object
	.size	_ZZN4node20SigintWatchdogHelperD4EvE4args, 24
_ZZN4node20SigintWatchdogHelperD4EvE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"../src/node_watchdog.cc:404"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"(0) == (uv_sem_init(&sem_, 0))"
	.align 8
.LC23:
	.string	"node::SigintWatchdogHelper::SigintWatchdogHelper()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SigintWatchdogHelperC4EvE4args, @object
	.size	_ZZN4node20SigintWatchdogHelperC4EvE4args, 24
_ZZN4node20SigintWatchdogHelperC4EvE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"../src/node_watchdog.cc:393"
.LC25:
	.string	"(it) != (watchdogs_.end())"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"void node::SigintWatchdogHelper::Unregister(node::SigintWatchdogBase*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseEE4args, @object
	.size	_ZZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseEE4args, 24
_ZZN4node20SigintWatchdogHelper10UnregisterEPNS_18SigintWatchdogBaseEE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"../src/node_watchdog.cc:361"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"(0) == (pthread_join(thread_, nullptr))"
	.align 8
.LC29:
	.string	"bool node::SigintWatchdogHelper::Stop()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SigintWatchdogHelper4StopEvE4args, @object
	.size	_ZZN4node20SigintWatchdogHelper4StopEvE4args, 24
_ZZN4node20SigintWatchdogHelper4StopEvE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/node_watchdog.cc:310"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"(0) == (pthread_sigmask(2, &sigmask, nullptr))"
	.align 8
.LC32:
	.string	"int node::SigintWatchdogHelper::Start()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SigintWatchdogHelper5StartEvE4args_1, @object
	.size	_ZZN4node20SigintWatchdogHelper5StartEvE4args_1, 24
_ZZN4node20SigintWatchdogHelper5StartEvE4args_1:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/node_watchdog.cc:307"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"(0) == (pthread_sigmask(2, &sigmask, &savemask))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SigintWatchdogHelper5StartEvE4args_0, @object
	.size	_ZZN4node20SigintWatchdogHelper5StartEvE4args_0, 24
_ZZN4node20SigintWatchdogHelper5StartEvE4args_0:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC32
	.section	.rodata.str1.1
.LC35:
	.string	"../src/node_watchdog.cc:300"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20SigintWatchdogHelper5StartEvE4args, @object
	.size	_ZZN4node20SigintWatchdogHelper5StartEvE4args, 24
_ZZN4node20SigintWatchdogHelper5StartEvE4args:
	.quad	.LC35
	.quad	.LC19
	.quad	.LC32
	.section	.rodata.str1.1
.LC36:
	.string	"../src/node_watchdog.cc:188"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"(uv_async_send(&handle_)) == (0)"
	.align 8
.LC38:
	.string	"virtual node::SignalPropagation node::TraceSigintWatchdog::HandleSigint()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node19TraceSigintWatchdog12HandleSigintEvE4args, @object
	.size	_ZZN4node19TraceSigintWatchdog12HandleSigintEvE4args, 24
_ZZN4node19TraceSigintWatchdog12HandleSigintEvE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_watchdog.cc:179"
.LC40:
	.string	"(r) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"node::TraceSigintWatchdog::TraceSigintWatchdog(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @object
	.size	_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node19TraceSigintWatchdogC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"../src/node_watchdog.cc:158"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"static void node::TraceSigintWatchdog::Start(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node19TraceSigintWatchdog5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC42
	.quad	.LC40
	.quad	.LC43
	.section	.rodata.str1.1
.LC44:
	.string	"../src/node_watchdog.cc:146"
.LC45:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"static void node::TraceSigintWatchdog::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node19TraceSigintWatchdog3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.section	.rodata.str1.1
.LC47:
	.string	"../src/node_watchdog.cc:65"
.LC48:
	.string	"(0) == (rc)"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"node::Watchdog::Watchdog(v8::Isolate*, uint64_t, bool*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_2, @object
	.size	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_2, 24
_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_2:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.section	.rodata.str1.1
.LC50:
	.string	"../src/node_watchdog.cc:62"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_1, @object
	.size	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_1, 24
_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_1:
	.quad	.LC50
	.quad	.LC48
	.quad	.LC49
	.section	.rodata.str1.1
.LC51:
	.string	"../src/node_watchdog.cc:59"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_0, @object
	.size	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_0, 24
_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args_0:
	.quad	.LC51
	.quad	.LC48
	.quad	.LC49
	.section	.rodata.str1.1
.LC52:
	.string	"../src/node_watchdog.cc:56"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args, @object
	.size	_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args, 24
_ZZN4node8WatchdogC4EPN2v87IsolateEmPbE4args:
	.quad	.LC52
	.quad	.LC48
	.quad	.LC49
	.weak	_ZZN4node13MemoryTracker24TrackInlineFieldWithSizeEPKcmS2_E4args
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"../src/memory_tracker-inl.h:88"
	.section	.rodata.str1.1
.LC54:
	.string	"CurrentNode()"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"void node::MemoryTracker::TrackInlineFieldWithSize(const char*, size_t, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker24TrackInlineFieldWithSizeEPKcmS2_E4args,"awG",@progbits,_ZZN4node13MemoryTracker24TrackInlineFieldWithSizeEPKcmS2_E4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker24TrackInlineFieldWithSizeEPKcmS2_E4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker24TrackInlineFieldWithSizeEPKcmS2_E4args, 24
_ZZN4node13MemoryTracker24TrackInlineFieldWithSizeEPKcmS2_E4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC56:
	.string	"../src/debug_utils-inl.h:67"
.LC57:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC59:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC61:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	7451578753385329236
	.quad	7521983764066758249
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
