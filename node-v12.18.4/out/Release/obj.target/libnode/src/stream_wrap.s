	.file	"stream_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5790:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5790:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7143:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7143:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap7IsAliveEv
	.type	_ZN4node15LibuvStreamWrap7IsAliveEv, @function
_ZN4node15LibuvStreamWrap7IsAliveEv:
.LFB7664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L7
	cmpl	$2, 72(%rbx)
	setne	%al
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7664:
	.size	_ZN4node15LibuvStreamWrap7IsAliveEv, .-_ZN4node15LibuvStreamWrap7IsAliveEv
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.type	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv, @function
_ZN4node15LibuvStreamWrap12GetAsyncWrapEv:
.LFB7666:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE7666:
	.size	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv, .-_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.type	_ZN4node15LibuvStreamWrap9IsIPCPipeEv, @function
_ZN4node15LibuvStreamWrap9IsIPCPipeEv:
.LFB7667:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdx
	xorl	%eax, %eax
	cmpl	$7, 16(%rdx)
	je	.L17
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	248(%rdx), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE7667:
	.size	_ZN4node15LibuvStreamWrap9IsIPCPipeEv, .-_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.section	.text._ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv:
.LFB9785:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9785:
	.size	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9786:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9786:
	.size	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv
	.type	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv, @function
_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv:
.LFB9788:
	.cfi_startproc
	endbr64
	movl	$320, %eax
	ret
	.cfi_endproc
.LFE9788:
	.size	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv, .-_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv
	.section	.text._ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv:
.LFB9790:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE9790:
	.size	_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv:
.LFB9791:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9791:
	.size	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9792:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9792:
	.size	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv
	.type	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv, @function
_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv:
.LFB9794:
	.cfi_startproc
	endbr64
	movl	$184, %eax
	ret
	.cfi_endproc
.LFE9794:
	.size	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv, .-_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv
	.section	.text._ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv:
.LFB9796:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE9796:
	.size	_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7579:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L31:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7579:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7578:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L37:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7578:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i
	.type	_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i, @function
_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i:
.LFB9136:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rdx
	leaq	-88(%rdi), %rcx
	subl	$1, 2156(%rdx)
	js	.L43
	jmp	*80(%rcx)
	.p2align 4,,10
	.p2align 3
.L43:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9136:
	.size	_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i, .-_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i
	.section	.text._ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i
	.type	_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i, @function
_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i:
.LFB9137:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rdx
	leaq	-88(%rdi), %rcx
	subl	$1, 2156(%rdx)
	js	.L49
	jmp	*80(%rcx)
	.p2align 4,,10
	.p2align 3
.L49:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9137:
	.size	_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i, .-_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap5GetFDEv
	.type	_ZN4node15LibuvStreamWrap5GetFDEv, @function
_ZN4node15LibuvStreamWrap5GetFDEv:
.LFB7663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	152(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-1, -12(%rbp)
	movl	$-1, %eax
	testq	%rdi, %rdi
	je	.L50
	leaq	-12(%rbp), %rsi
	call	uv_fileno@PLT
	movl	-12(%rbp), %eax
.L50:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L57
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L57:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7663:
	.size	_ZN4node15LibuvStreamWrap5GetFDEv, .-_ZN4node15LibuvStreamWrap5GetFDEv
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap9IsClosingEv
	.type	_ZN4node15LibuvStreamWrap9IsClosingEv, @function
_ZN4node15LibuvStreamWrap9IsClosingEv:
.LFB7665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	152(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_is_closing@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE7665:
	.size	_ZN4node15LibuvStreamWrap9IsClosingEv, .-_ZN4node15LibuvStreamWrap9IsClosingEv
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap9ReadStartEv
	.type	_ZN4node15LibuvStreamWrap9ReadStartEv, @function
_ZN4node15LibuvStreamWrap9ReadStartEv:
.LFB7668:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdi
	leaq	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_stream_slPK8uv_buf_tE0_4_FUNES2_lS5_(%rip), %rdx
	leaq	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_handle_smP8uv_buf_tE_4_FUNES2_mS4_(%rip), %rsi
	jmp	uv_read_start@PLT
	.cfi_endproc
.LFE7668:
	.size	_ZN4node15LibuvStreamWrap9ReadStartEv, .-_ZN4node15LibuvStreamWrap9ReadStartEv
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap8ReadStopEv
	.type	_ZN4node15LibuvStreamWrap8ReadStopEv, @function
_ZN4node15LibuvStreamWrap8ReadStopEv:
.LFB7675:
	.cfi_startproc
	endbr64
	movq	152(%rdi), %rdi
	jmp	uv_read_stop@PLT
	.cfi_endproc
.LFE7675:
	.size	_ZN4node15LibuvStreamWrap8ReadStopEv, .-_ZN4node15LibuvStreamWrap8ReadStopEv
	.section	.text._ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.type	_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv, @function
_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv:
.LFB9795:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L64
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9795:
	.size	_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv, .-_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.section	.text._ZN4node7ReqWrapI10uv_write_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI10uv_write_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI10uv_write_sE6CancelEv
	.type	_ZN4node7ReqWrapI10uv_write_sE6CancelEv, @function
_ZN4node7ReqWrapI10uv_write_sE6CancelEv:
.LFB9789:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L67
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9789:
	.size	_ZN4node7ReqWrapI10uv_write_sE6CancelEv, .-_ZN4node7ReqWrapI10uv_write_sE6CancelEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE, @function
_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE:
.LFB7683:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	16(%rbx), %rax
	subq	$8, %rsp
	cmpq	$0, 96(%rbx)
	movq	152(%rdi), %rsi
	movq	%rax, 104(%rbx)
	jne	.L72
	leaq	_ZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_si(%rip), %rax
	leaq	104(%rbx), %rdi
	movq	%rax, 96(%rbx)
	leaq	_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i(%rip), %rdx
	call	uv_shutdown@PLT
	testl	%eax, %eax
	js	.L68
	movq	32(%rbx), %rdx
	addl	$1, 2156(%rdx)
.L68:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	leaq	_ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7683:
	.size	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE, .-_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.type	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm, @function
_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm:
.LFB7685:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rbx
	movq	(%rdx), %r12
	movq	152(%rdi), %rdi
	movq	%rbx, %rsi
	movl	%r12d, %edx
	call	uv_try_write@PLT
	cmpl	$-38, %eax
	je	.L78
	cmpl	$-11, %eax
	je	.L78
	testl	%eax, %eax
	js	.L73
	cltq
	testq	%r12, %r12
	jne	.L77
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L76:
	subq	%rdx, %rax
	addq	$16, %rbx
	subq	$1, %r12
	je	.L75
.L77:
	movq	8(%rbx), %rdx
	cmpq	%rax, %rdx
	jbe	.L76
	subq	%rax, %rdx
	addq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
.L75:
	movq	%rbx, (%r14)
	xorl	%eax, %eax
	movq	%r12, 0(%r13)
.L73:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7685:
	.size	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm, .-_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB7686:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	40(%rbx), %rax
	subq	$8, %rsp
	cmpq	$0, 120(%rbx)
	movq	152(%rdi), %rsi
	movq	%rax, 128(%rbx)
	jne	.L88
	leaq	_ZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_si(%rip), %rax
	leaq	128(%rbx), %rdi
	movq	%rax, 120(%rbx)
	leaq	_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i(%rip), %r9
	call	uv_write2@PLT
	testl	%eax, %eax
	js	.L84
	movq	56(%rbx), %rdx
	addl	$1, 2156(%rdx)
.L84:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	leaq	_ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7686:
	.size	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.p2align 4
	.type	_ZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENUlRKNS1_20FunctionCallbackInfoIS5_EEE_4_FUNESD_, @function
_ZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENUlRKNS1_20FunctionCallbackInfoIS5_EEE_4_FUNESD_:
.LFB7648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L90
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L92
.L90:
	movq	8(%rdi), %r12
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %r12
	movq	%r12, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%edx, %edx
	popq	%r12
	movl	$1, %esi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L90
	leaq	_ZZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENKUlRKNS1_20FunctionCallbackInfoIS5_EEE_clESD_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7648:
	.size	_ZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENUlRKNS1_20FunctionCallbackInfoIS5_EEE_4_FUNESD_, .-_ZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENUlRKNS1_20FunctionCallbackInfoIS5_EEE_4_FUNESD_
	.section	.text._ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.type	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv, @function
_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv:
.LFB9859:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE9859:
	.size	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv, .-_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.type	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv, @function
_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv:
.LFB9860:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE9860:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv, .-_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9861:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9861:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv, @function
_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv:
.LFB9862:
	.cfi_startproc
	endbr64
	movl	$320, %eax
	ret
	.cfi_endproc
.LFE9862:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv, .-_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv
	.section	.text._ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv:
.LFB9863:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE9863:
	.size	_ZThn56_N4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.type	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv, @function
_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv:
.LFB9864:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE9864:
	.size	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv, .-_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv, @function
_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv:
.LFB9865:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE9865:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv, .-_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9866:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9866:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv:
.LFB9867:
	.cfi_startproc
	endbr64
	movl	$184, %eax
	ret
	.cfi_endproc
.LFE9867:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv, .-_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv
	.section	.text._ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv:
.LFB9868:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE9868:
	.size	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv
	.text
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.type	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv, @function
_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv:
.LFB9872:
	.cfi_startproc
	endbr64
	leaq	-88(%rdi), %rax
	ret
	.cfi_endproc
.LFE9872:
	.size	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv, .-_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_si
	.type	_ZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_si, @function
_ZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_si:
.LFB7684:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$88, %rdi
	je	.L105
	movq	-72(%rdi), %rax
	movl	%esi, %r14d
	leaq	-64(%rbp), %r15
	movq	%rdi, %r12
	leaq	-88(%rdi), %r13
	movq	%r15, %rdi
	subq	$104, %r12
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	32(%r12), %rax
	movq	3280(%rax), %r8
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-16(%r13), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-16(%r13), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-72(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L109
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	leaq	_ZZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_siE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L109:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7684:
	.size	_ZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_si, .-_ZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_si
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_si
	.type	_ZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_si, @function
_ZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_si:
.LFB7687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$48, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpq	$88, %rdi
	je	.L111
	movq	-72(%rdi), %rax
	movl	%esi, %r14d
	leaq	-64(%rbp), %r15
	movq	%rdi, %r12
	leaq	-88(%rdi), %r13
	movq	%r15, %rdi
	addq	$-128, %r12
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	56(%r12), %rax
	movq	3280(%rax), %r8
	movq	%r8, %rdi
	movq	%r8, -72(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-40(%r13), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-40(%r13), %rax
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-72(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	leaq	_ZZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_siE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7687:
	.size	_ZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_si, .-_ZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_si
	.section	.text._ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev:
.LFB9787:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE9787:
	.size	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7681:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$184, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	leaq	88(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L123
	cmpw	$1040, %cx
	jne	.L118
.L123:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L125
.L121:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	120(%rbx), %rbx
	leaq	16(%r12), %rdi
	movq	%r13, %rdx
	movsd	.LC0(%rip), %xmm0
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movl	$26, %ecx
	movq	%rbx, %rsi
	movq	%rax, (%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 72(%r12)
	leaq	80(%r12), %rax
	movq	%rax, 80(%r12)
	movq	%rax, 88(%r12)
	je	.L126
	movq	2112(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rdx, 80(%r12)
	leaq	2112(%rbx), %rdx
	movq	%rax, 16(%r12)
	addq	$112, %rax
	movq	%rax, 72(%r12)
	movq	%r12, %rax
	movq	%rdx, 88(%r12)
	movq	$0, 96(%r12)
	movq	$0, 104(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7681:
	.size	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.set	.LTHUNK7,_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB9925:
	.cfi_startproc
	endbr64
	subq	$88, %rdi
	jmp	.LTHUNK7
	.cfi_endproc
.LFE9925:
	.size	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN4node7ReqWrapI10uv_write_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI10uv_write_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI10uv_write_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI10uv_write_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI10uv_write_sE6CancelEv:
.LFB9878:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L129
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9878:
	.size	_ZThn56_N4node7ReqWrapI10uv_write_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI10uv_write_sE6CancelEv
	.section	.text._ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI13uv_shutdown_sE6CancelEv:
.LFB9879:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L132
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9879:
	.size	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.text
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.type	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv, @function
_ZThn88_N4node15LibuvStreamWrap8ReadStopEv:
.LFB9886:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	jmp	uv_read_stop@PLT
	.cfi_endproc
.LFE9886:
	.size	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv, .-_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.type	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv, @function
_ZThn88_N4node15LibuvStreamWrap9IsClosingEv:
.LFB9889:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	64(%rdi), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_is_closing@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE9889:
	.size	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv, .-_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.type	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv, @function
_ZThn88_N4node15LibuvStreamWrap9ReadStartEv:
.LFB9893:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdi
	leaq	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_stream_slPK8uv_buf_tE0_4_FUNES2_lS5_(%rip), %rdx
	leaq	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_handle_smP8uv_buf_tE_4_FUNES2_mS4_(%rip), %rsi
	jmp	uv_read_start@PLT
	.cfi_endproc
.LFE9893:
	.size	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv, .-_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.type	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv, @function
_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv:
.LFB9894:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rdx
	xorl	%eax, %eax
	cmpl	$7, 16(%rdx)
	je	.L140
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	movl	248(%rdx), %eax
	testl	%eax, %eax
	setne	%al
	ret
	.cfi_endproc
.LFE9894:
	.size	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv, .-_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.type	_ZThn88_N4node15LibuvStreamWrap5GetFDEv, @function
_ZThn88_N4node15LibuvStreamWrap5GetFDEv:
.LFB9897:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	64(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$-1, -12(%rbp)
	movl	$-1, %eax
	testq	%rdi, %rdi
	je	.L141
	leaq	-12(%rbp), %rsi
	call	uv_fileno@PLT
	movl	-12(%rbp), %eax
.L141:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L148
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L148:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9897:
	.size	_ZThn88_N4node15LibuvStreamWrap5GetFDEv, .-_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.type	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev, @function
_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev:
.LFB9882:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE(%rip), %rax
	movq	%rax, -72(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI13uv_shutdown_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L154
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9882:
	.size	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev, .-_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev:
.LFB9891:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -16(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI13uv_shutdown_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L158
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	leaq	-16(%rdi), %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9891:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.p2align 4
	.weak	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.type	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev, @function
_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev:
.LFB9892:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -72(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI13uv_shutdown_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L162
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	leaq	-72(%rdi), %r12
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L162:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9892:
	.size	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev, .-_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev:
.LFB9883:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE(%rip), %rax
	movq	%rax, -16(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI13uv_shutdown_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L168
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L168:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9883:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev
	.type	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev, @function
_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev:
.LFB9736:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE(%rip), %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node7ReqWrapI13uv_shutdown_sEE(%rip), %rax
	movq	%rax, 16(%rdi)
	addq	$112, %rax
	cmpq	$0, 24(%rdi)
	movq	%rax, 72(%rdi)
	je	.L174
	movq	80(%rdi), %rdx
	movq	88(%rdi), %rax
	leaq	16(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L174:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9736:
	.size	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev, .-_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.set	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED2Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.type	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev, @function
_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev:
.LFB9738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node7ReqWrapI13uv_shutdown_sEE(%rip), %rax
	movq	%rax, 16(%rdi)
	addq	$112, %rax
	cmpq	$0, 24(%rdi)
	movq	%rax, 72(%rdi)
	je	.L178
	movq	80(%r12), %rdx
	movq	88(%r12), %rax
	addq	$16, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$184, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9738:
	.size	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev, .-_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.text
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE, @function
_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE:
.LFB9900:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	16(%rbx), %rax
	subq	$8, %rsp
	cmpq	$0, 96(%rbx)
	movq	64(%rdi), %rsi
	movq	%rax, 104(%rbx)
	jne	.L183
	leaq	_ZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_si(%rip), %rax
	leaq	104(%rbx), %rdi
	movq	%rax, 96(%rbx)
	leaq	_ZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE7WrapperES2_i(%rip), %rdx
	call	uv_shutdown@PLT
	testl	%eax, %eax
	js	.L179
	movq	32(%rbx), %rdx
	addl	$1, 2156(%rdx)
.L179:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	leaq	_ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9900:
	.size	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE, .-_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.section	.text._ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.type	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev, @function
_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev:
.LFB9734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node7ReqWrapI10uv_write_sEE(%rip), %rax
	movq	%rax, 40(%rdi)
	addq	$112, %rax
	cmpq	$0, 48(%rdi)
	movq	%rax, 96(%rdi)
	je	.L192
	movq	104(%r12), %rdx
	movq	112(%r12), %rax
	addq	$40, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	movq	%rax, (%r12)
	call	uv_buf_init@PLT
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	testq	%r13, %r13
	je	.L186
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L193
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L186:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L193:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9734:
	.size	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev, .-_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev:
.LFB9875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L197
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L197:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9875:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev, .-_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev:
.LFB9876:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE9876:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev, .-_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev:
.LFB9793:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L202
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L202:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9793:
	.size	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7682:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$320, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	leaq	88(%rbx), %rax
	movq	%rax, 8(%r12)
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L209
	cmpw	$1040, %cx
	jne	.L204
.L209:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L211
.L207:
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	120(%rbx), %rbx
	leaq	40(%r12), %rdi
	movsd	.LC0(%rip), %xmm0
	movq	%rdx, 32(%r12)
	movl	$39, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rax, 24(%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%rbx)
	movq	%rax, 96(%r12)
	leaq	104(%r12), %rax
	movq	%rax, 104(%r12)
	movq	%rax, 112(%r12)
	je	.L212
	movq	2112(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rbx)
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rdx, 104(%r12)
	leaq	2112(%rbx), %rdx
	movq	%rax, 40(%r12)
	addq	$112, %rax
	movq	%rax, 96(%r12)
	movq	%r12, %rax
	movq	%rdx, 112(%r12)
	movq	$0, 120(%r12)
	movq	$0, 128(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L207
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7682:
	.size	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.set	.LTHUNK8,_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB9927:
	.cfi_startproc
	endbr64
	subq	$88, %rdi
	jmp	.LTHUNK8
	.cfi_endproc
.LFE9927:
	.size	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB9901:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rsi, %rbx
	leaq	40(%rbx), %rax
	subq	$8, %rsp
	cmpq	$0, 120(%rbx)
	movq	64(%rdi), %rsi
	movq	%rax, 128(%rbx)
	jne	.L217
	leaq	_ZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_si(%rip), %rax
	leaq	128(%rbx), %rdi
	movq	%rax, 120(%rbx)
	leaq	_ZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE7WrapperES2_i(%rip), %r9
	call	uv_write2@PLT
	testl	%eax, %eax
	js	.L213
	movq	56(%rbx), %rdx
	addl	$1, 2156(%rdx)
.L213:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	leaq	_ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9901:
	.size	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.type	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv, @function
_ZThn88_N4node15LibuvStreamWrap7IsAliveEv:
.LFB9902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$88, %rdi
	subq	$8, %rsp
	movq	-88(%rbx), %rax
	call	*56(%rax)
	testb	%al, %al
	je	.L218
	cmpl	$2, -16(%rbx)
	setne	%al
.L218:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9902:
	.size	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv, .-_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.p2align 4
	.type	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_handle_smP8uv_buf_tE_4_FUNES2_mS4_, @function
_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_handle_smP8uv_buf_tE_4_FUNES2_mS4_:
.LFB7670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	(%rdi), %r13
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r13), %rax
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r13), %rax
	movq	3280(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	96(%r13), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L227
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L227:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7670:
	.size	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_handle_smP8uv_buf_tE_4_FUNES2_mS4_, .-_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_handle_smP8uv_buf_tE_4_FUNES2_mS4_
	.section	.text._ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev:
.LFB9888:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, -40(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI10uv_write_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L233
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r13
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L228
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L234
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9888:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.type	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev, @function
_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev:
.LFB9896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, -96(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI10uv_write_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L243
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	leaq	-96(%rdi), %r13
	subq	$56, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-72(%rbx), %r12
	movq	-64(%rbx), %r14
	movq	%rax, -96(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbx)
	movq	%rdx, -64(%rbx)
	testq	%r12, %r12
	je	.L237
	movq	-80(%rbx), %rax
	testq	%rax, %rax
	je	.L244
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L237:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$320, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9896:
	.size	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev, .-_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev:
.LFB9895:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rax, -40(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI10uv_write_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L253
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %rbx
	leaq	-40(%rdi), %r13
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r14
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L247
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L254
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L247:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$320, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9895:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED5Ev,comdat
	.p2align 4
	.weak	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.type	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev, @function
_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev:
.LFB9887:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -96(%rdi)
	leaq	16+_ZTVN4node7ReqWrapI10uv_write_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L260
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	subq	$56, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-72(%rbx), %r12
	movq	-64(%rbx), %r13
	movq	%rax, -96(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbx)
	movq	%rdx, -64(%rbx)
	testq	%r12, %r12
	je	.L255
	movq	-80(%rbx), %rax
	testq	%rax, %rax
	je	.L261
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L261:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9887:
	.size	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev, .-_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"ShutdownWrap"
.LC3:
	.string	"callback"
.LC4:
	.string	"handle"
.LC5:
	.string	"WriteWrap"
.LC6:
	.string	"kReadBytesOrError"
.LC8:
	.string	"kArrayBufferOffset"
.LC10:
	.string	"kBytesWritten"
.LC12:
	.string	"kLastWriteWasAsync"
.LC14:
	.string	"streamBaseState"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB15:
	.text
.LHOTB15:
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L263
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L263
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L263
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	xorl	%edx, %edx
	leaq	_ZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENUlRKNS1_20FunctionCallbackInfoIS5_EEE_4_FUNESD_(%rip), %rsi
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$2, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movl	$12, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L306
.L264:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	352(%rbx), %rax
	leaq	104(%rax), %rdx
	movq	360(%rbx), %rax
	movq	1136(%rax), %rsi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	movl	$8, %ecx
	movq	%rax, -64(%rbp)
	leaq	104(%rdi), %r9
	movq	%r9, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L307
.L265:
	movq	%r8, %rdi
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movl	$6, %ecx
	movq	%rax, -64(%rbp)
	leaq	104(%rdi), %r9
	movq	%r9, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L308
.L266:
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	movq	%r9, %rdx
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L309
.L267:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L310
.L268:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	3232(%rbx), %rdi
	movq	352(%rbx), %r15
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L269
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3232(%rbx)
.L269:
	testq	%r13, %r13
	je	.L270
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3232(%rbx)
.L270:
	subq	$8, %rsp
	movq	352(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	xorl	%edx, %edx
	leaq	_ZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENUlRKNS1_20FunctionCallbackInfoIS5_EEE_4_FUNESD_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$2, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$9, %ecx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L311
.L271:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L312
.L272:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L313
.L273:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	3264(%rbx), %rdi
	movq	352(%rbx), %r15
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L274
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3264(%rbx)
.L274:
	testq	%r13, %r13
	je	.L275
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3264(%rbx)
.L275:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L314
.L276:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	movq	%rdx, -56(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-56(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L315
.L277:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L316
.L278:
	movsd	.LC9(%rip), %xmm0
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-56(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L317
.L279:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L318
.L280:
	movsd	.LC11(%rip), %xmm0
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-56(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L319
.L281:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r13, %rdi
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L320
.L282:
	movsd	.LC13(%rip), %xmm0
	movq	%r13, %rdi
	movq	%rdx, -56(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-56(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L321
.L283:
	movq	1856(%rbx), %r13
	testq	%r13, %r13
	je	.L284
	movq	0(%r13), %rsi
	movq	1824(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
.L284:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$15, %ecx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L322
.L285:
	movq	%r13, %rcx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L323
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%r9, -72(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r9, -72(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L313:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L315:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L317:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L319:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L320:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L321:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7646:
.L263:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7646:
	.text
	.size	_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE15:
	.text
.LHOTE15:
	.section	.text._ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev
	.type	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev, @function
_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev:
.LFB9732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node7ReqWrapI10uv_write_sEE(%rip), %rax
	movq	%rax, 40(%rdi)
	addq	$112, %rax
	cmpq	$0, 48(%rdi)
	movq	%rax, 96(%rdi)
	je	.L329
	movq	104(%rbx), %rdx
	movq	112(%rbx), %rax
	addq	$40, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rbx), %r12
	movq	32(%rbx), %r13
	movq	%rax, (%rbx)
	call	uv_buf_init@PLT
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%r12, %r12
	je	.L324
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L330
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9732:
	.size	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev, .-_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev
	.weak	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.set	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev,_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED2Ev
	.text
	.p2align 4
	.globl	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.type	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm, @function
_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm:
.LFB9903:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rbx
	movq	(%rdx), %r12
	movq	64(%rdi), %rdi
	movq	%rbx, %rsi
	movl	%r12d, %edx
	call	uv_try_write@PLT
	cmpl	$-38, %eax
	je	.L336
	cmpl	$-11, %eax
	je	.L336
	testl	%eax, %eax
	js	.L331
	cltq
	testq	%r12, %r12
	jne	.L335
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L334:
	subq	%rdx, %rax
	addq	$16, %rbx
	subq	$1, %r12
	je	.L333
.L335:
	movq	8(%rbx), %rdx
	cmpq	%rax, %rdx
	jbe	.L334
	subq	%rax, %rdx
	addq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
.L333:
	movq	%rbx, (%r14)
	xorl	%eax, %eax
	movq	%r12, 0(%r13)
.L331:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9903:
	.size	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm, .-_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node15LibuvStreamWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node15LibuvStreamWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7679:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L356
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L351
	cmpw	$1040, %cx
	jne	.L344
.L351:
	movq	23(%rdx), %rax
.L346:
	testq	%rax, %rax
	je	.L342
	movq	152(%rax), %rax
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L357
	movq	96(%rax), %rsi
	testl	%esi, %esi
	js	.L349
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L342:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L357:
	movq	$0, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L356:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L349:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L358
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L342
.L358:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L342
	.cfi_endproc
.LFE7679:
	.size	_ZN4node15LibuvStreamWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node15LibuvStreamWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7680:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L380
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L372
	cmpw	$1040, %cx
	jne	.L361
.L372:
	movq	23(%rdx), %r12
.L363:
	testq	%r12, %r12
	je	.L359
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L381
	movq	(%r12), %rax
	leaq	_ZN4node15LibuvStreamWrap7IsAliveEv(%rip), %rcx
	movq	%r12, %rdi
	movq	104(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L366
	call	*56(%rax)
	testb	%al, %al
	je	.L369
	cmpl	$2, 72(%r12)
	je	.L369
.L368:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L370
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L371:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	152(%r12), %rdi
	movq	(%rbx), %rbx
	movzbl	%al, %esi
	call	uv_stream_set_blocking@PLT
	salq	$32, %rax
	movq	%rax, 24(%rbx)
.L359:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	call	*%rdx
	testb	%al, %al
	jne	.L368
.L369:
	movabsq	$-94489280512, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L370:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L371
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L380:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L381:
	leaq	_ZZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7680:
	.size	_ZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7141:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L383
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L383:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L392
.L384:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L392:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L384
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7141:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE
	.type	_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE, @function
_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE:
.LFB7659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	88(%rbx), %rdx
	movq	%r14, 120(%rbx)
	movq	%r12, %rdi
	leaq	16+_ZTVN4node22EmitToJSStreamListenerE(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, 152(%rbx)
	movl	$1, %esi
	movq	%rax, 128(%rbx)
	leaq	128(%rbx), %rax
	movq	%rax, 96(%rbx)
	leaq	16+_ZTVN4node15LibuvStreamWrapE(%rip), %rax
	movq	%rax, (%rbx)
	addq	$208, %rax
	movq	$0, 144(%rbx)
	movq	%rdx, 136(%rbx)
	movq	%rax, 88(%rbx)
	movups	%xmm0, 104(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7659:
	.size	_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE, .-_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE
	.globl	_ZN4node15LibuvStreamWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE
	.set	_ZN4node15LibuvStreamWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE,_ZN4node15LibuvStreamWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_stream_sNS_9AsyncWrap12ProviderTypeE
	.section	.rodata.str1.1
.LC16:
	.string	"LibuvStreamWrap"
.LC17:
	.string	"setBlocking"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE
	.type	_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE, @function
_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE:
.LFB7661:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	3176(%rdi), %r12
	testq	%r12, %r12
	je	.L408
.L396:
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	subq	$8, %rsp
	movq	2680(%rdi), %rdx
	movq	%rdi, %rbx
	xorl	%ecx, %ecx
	movq	352(%rdi), %rdi
	pushq	$0
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	movl	$15, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L409
.L397:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	movl	$1, %r9d
	leaq	_ZN4node15LibuvStreamWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	360(%rbx), %rax
	movl	$5, %r8d
	movq	1952(%rax), %rsi
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L410
.L398:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE@PLT
	movq	3176(%rbx), %rdi
	movq	352(%rbx), %r13
	testq	%rdi, %rdi
	je	.L399
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3176(%rbx)
.L399:
	testq	%r12, %r12
	je	.L396
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3176(%rbx)
	leaq	-32(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rsi
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L410:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L398
	.cfi_endproc
.LFE7661:
	.size	_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE, .-_ZN4node15LibuvStreamWrap22GetConstructorTemplateEPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	3176(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L413
	movq	%rsi, %r12
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L413
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L424
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L418
	cmpw	$1040, %cx
	jne	.L415
.L418:
	movq	23(%rdx), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	leaq	_ZZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7662:
	.size	_ZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap9OnUvAllocEmP8uv_buf_t
	.type	_ZN4node15LibuvStreamWrap9OnUvAllocEmP8uv_buf_t, @function
_ZN4node15LibuvStreamWrap9OnUvAllocEmP8uv_buf_t:
.LFB7676:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	96(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r15, %rdi
	movq	%rax, (%r12)
	movq	%rdx, 8(%r12)
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L428
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L428:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7676:
	.size	_ZN4node15LibuvStreamWrap9OnUvAllocEmP8uv_buf_t, .-_ZN4node15LibuvStreamWrap9OnUvAllocEmP8uv_buf_t
	.align 2
	.p2align 4
	.globl	_ZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_t
	.type	_ZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_t, @function
_ZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_t:
.LFB7678:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r15, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	movq	3280(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	152(%rbx), %rdi
	cmpl	$7, 16(%rdi)
	je	.L430
.L501:
	cmpq	$0, 8(%rbx)
	je	.L435
	testq	%r12, %r12
	jle	.L433
.L469:
	addq	%r12, 104(%rbx)
	movq	96(%rbx), %rdi
.L468:
	movq	(%rdi), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L503
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movl	248(%rdi), %eax
	testl	%eax, %eax
	je	.L501
	call	uv_pipe_pending_count@PLT
	testl	%eax, %eax
	jle	.L501
	movq	152(%rbx), %rdi
	call	uv_pipe_pending_type@PLT
	cmpq	$0, 8(%rbx)
	je	.L435
	testq	%r12, %r12
	jg	.L504
	.p2align 4,,10
	.p2align 3
.L433:
	movq	96(%rbx), %rdi
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L435:
	leaq	_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L504:
	cmpl	$12, %eax
	jne	.L438
	movq	16(%rbx), %r8
	leaq	-160(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -200(%rbp)
	movq	352(%r8), %rsi
	movq	%r8, -208(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-208(%rbp), %r8
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN4node7TCPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE@PLT
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	je	.L460
	movq	%rax, %rdi
	movq	%r9, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jle	.L472
	movq	(%r8), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L479
	cmpw	$1040, %cx
	jne	.L441
.L479:
	movq	23(%rdx), %rax
.L443:
	testq	%rax, %rax
	je	.L505
	movq	80(%rax), %rsi
	testq	%rsi, %rsi
	je	.L506
	movq	152(%rbx), %rdi
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	uv_accept@PLT
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jne	.L457
	movq	%r9, -208(%rbp)
	movq	%r8, %rsi
	movq	%r9, %rdi
.L499:
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-208(%rbp), %r9
	movq	%rax, -200(%rbp)
	movq	%r9, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-200(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L469
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rax
	testq	%rdi, %rdi
	je	.L467
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L507
.L467:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1328(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L469
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L460:
	movq	%r9, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	jmp	.L469
.L438:
	cmpl	$7, %eax
	jne	.L449
	movq	16(%rbx), %r8
	leaq	-128(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -200(%rbp)
	movq	352(%r8), %rsi
	movq	%r8, -208(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-208(%rbp), %r8
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN4node8PipeWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE@PLT
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	je	.L460
	movq	%rax, %rdi
	movq	%r9, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jle	.L472
	movq	(%r8), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L480
	cmpw	$1040, %cx
	jne	.L452
.L480:
	movq	23(%rdx), %rax
.L454:
	testq	%rax, %rax
	je	.L508
	movq	80(%rax), %rsi
	testq	%rsi, %rsi
	je	.L509
.L466:
	movq	152(%rbx), %rdi
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	uv_accept@PLT
	testl	%eax, %eax
	jne	.L457
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %r9
	movq	%r8, %rsi
	movq	%r9, %rdi
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L507:
	movq	352(%rax), %r8
	movq	(%rdi), %rsi
	movq	%rcx, -200(%rbp)
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-200(%rbp), %rcx
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L472:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L441:
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %r8
	jmp	.L443
.L505:
	leaq	_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L506:
	leaq	_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L508:
	leaq	_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L452:
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %r8
	jmp	.L454
.L509:
	leaq	_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L503:
	call	__stack_chk_fail@PLT
.L457:
	call	_ZN4node5AbortEv@PLT
.L449:
	cmpl	$15, %eax
	je	.L510
	testl	%eax, %eax
	je	.L469
	leaq	_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L510:
	movq	16(%rbx), %r8
	leaq	-96(%rbp), %r9
	movq	%r9, %rdi
	movq	%r9, -200(%rbp)
	movq	352(%r8), %rsi
	movq	%r8, -208(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-208(%rbp), %r8
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE@PLT
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	je	.L460
	movq	%rax, %rdi
	movq	%r9, -208(%rbp)
	movq	%rax, -200(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	movq	-200(%rbp), %r8
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jle	.L472
	movq	(%r8), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L481
	cmpw	$1040, %cx
	jne	.L462
.L481:
	movq	23(%rdx), %rax
.L464:
	testq	%rax, %rax
	je	.L511
	movq	80(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L466
	leaq	_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L462:
	movq	%r8, %rdi
	xorl	%esi, %esi
	movq	%r9, -208(%rbp)
	movq	%r8, -200(%rbp)
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	-208(%rbp), %r9
	movq	-200(%rbp), %r8
	jmp	.L464
.L511:
	leaq	_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7678:
	.size	_ZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_t, .-_ZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_t
	.p2align 4
	.type	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_stream_slPK8uv_buf_tE0_4_FUNES2_lS5_, @function
_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_stream_slPK8uv_buf_tE0_4_FUNES2_lS5_:
.LFB7673:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rdi
	jmp	_ZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_t
	.cfi_endproc
.LFE7673:
	.size	_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_stream_slPK8uv_buf_tE0_4_FUNES2_lS5_, .-_ZZN4node15LibuvStreamWrap9ReadStartEvENUlP11uv_stream_slPK8uv_buf_tE0_4_FUNES2_lS5_
	.p2align 4
	.globl	_Z21_register_stream_wrapv
	.type	_Z21_register_stream_wrapv, @function
_Z21_register_stream_wrapv:
.LFB7688:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7688:
	.size	_Z21_register_stream_wrapv, .-_Z21_register_stream_wrapv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8386:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L531
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L532
.L517:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L521
.L514:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L532:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L522
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L518:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L517
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L523
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L519:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L521:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L514
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L523:
	xorl	%edx, %edx
	jmp	.L519
	.cfi_endproc
.LFE8386:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%r12, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	leaq	-32(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L534
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L550
.L534:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L551
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L538
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L552
.L533:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L553
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L550:
	.cfi_restore_state
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L552:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L551:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L541
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L536:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L538:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L541:
	xorl	%edx, %edx
	jmp	.L536
.L553:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7611:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%r12, %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	leaq	-32(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L555
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L571
.L555:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L572
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L559
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L573
.L554:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L574
	addq	$24, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L571:
	.cfi_restore_state
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L573:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L572:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L562
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L557:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L559:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L562:
	xorl	%edx, %edx
	jmp	.L557
.L574:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7609:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node11ReqWrapBaseE
	.section	.data.rel.ro._ZTVN4node11ReqWrapBaseE,"awG",@progbits,_ZTVN4node11ReqWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11ReqWrapBaseE, @object
	.size	_ZTVN4node11ReqWrapBaseE, 48
_ZTVN4node11ReqWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node9StreamReqE
	.section	.data.rel.ro._ZTVN4node9StreamReqE,"awG",@progbits,_ZTVN4node9StreamReqE,comdat
	.align 8
	.type	_ZTVN4node9StreamReqE, @object
	.size	_ZTVN4node9StreamReqE, 48
_ZTVN4node9StreamReqE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node15LibuvStreamWrapE
	.section	.data.rel.ro._ZTVN4node15LibuvStreamWrapE,"awG",@progbits,_ZTVN4node15LibuvStreamWrapE,comdat
	.align 8
	.type	_ZTVN4node15LibuvStreamWrapE, @object
	.size	_ZTVN4node15LibuvStreamWrapE, 368
_ZTVN4node15LibuvStreamWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node15LibuvStreamWrap5GetFDEv
	.quad	_ZN4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZN4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZN4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZN4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZN4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZN4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZN4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZN4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	-88
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn88_N4node15LibuvStreamWrap9ReadStartEv
	.quad	_ZThn88_N4node15LibuvStreamWrap8ReadStopEv
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZThn88_N4node15LibuvStreamWrap10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn88_N4node15LibuvStreamWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.quad	_ZThn88_N4node15LibuvStreamWrap7IsAliveEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsClosingEv
	.quad	_ZThn88_N4node15LibuvStreamWrap9IsIPCPipeEv
	.quad	_ZThn88_N4node15LibuvStreamWrap5GetFDEv
	.quad	_ZThn88_N4node15LibuvStreamWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn88_N4node15LibuvStreamWrap12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.weak	_ZTVN4node7ReqWrapI13uv_shutdown_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI13uv_shutdown_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI13uv_shutdown_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI13uv_shutdown_sEE, @object
	.size	_ZTVN4node7ReqWrapI13uv_shutdown_sEE, 160
_ZTVN4node7ReqWrapI13uv_shutdown_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.quad	_ZN4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE12GetAsyncWrapEv
	.weak	_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE
	.section	.data.rel.ro._ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE,"awG",@progbits,_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE,comdat
	.align 8
	.type	_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE, @object
	.size	_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE, 232
_ZTVN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.quad	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv
	.quad	-16
	.quad	0
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.quad	-72
	.quad	0
	.quad	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED1Ev
	.quad	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEED0Ev
	.quad	_ZThn56_N4node7ReqWrapI13uv_shutdown_sE6CancelEv
	.quad	_ZThn72_N4node18SimpleShutdownWrapINS_7ReqWrapI13uv_shutdown_sEEE12GetAsyncWrapEv
	.weak	_ZTVN4node7ReqWrapI10uv_write_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI10uv_write_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI10uv_write_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI10uv_write_sEE, @object
	.size	_ZTVN4node7ReqWrapI10uv_write_sEE, 160
_ZTVN4node7ReqWrapI10uv_write_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI10uv_write_sE6CancelEv
	.quad	_ZN4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI10uv_write_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI10uv_write_sE12GetAsyncWrapEv
	.weak	_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE
	.section	.data.rel.ro._ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE,"awG",@progbits,_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE,comdat
	.align 8
	.type	_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE, @object
	.size	_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE, 232
_ZTVN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.quad	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.quad	_ZN4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.quad	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv
	.quad	-40
	.quad	0
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI10uv_write_sE6CancelEv
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.quad	-96
	.quad	0
	.quad	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED1Ev
	.quad	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEED0Ev
	.quad	_ZThn56_N4node7ReqWrapI10uv_write_sE6CancelEv
	.quad	_ZThn96_N4node15SimpleWriteWrapINS_7ReqWrapI10uv_write_sEEE12GetAsyncWrapEv
	.weak	_ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args
	.section	.rodata.str1.1
.LC18:
	.string	"../src/req_wrap-inl.h:130"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"(req_wrap->original_callback_) == nullptr"
	.align 8
.LC20:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, void (*)"
	.ascii	"(ReqT*, "
	.string	"Args ...)>::For(node::ReqWrap<T>*, node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_write_s; Args = {int}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_write_s*, int)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI10uv_write_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.weak	_ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args
	.section	.rodata.str1.8
	.align 8
.LC21:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, void (*)"
	.ascii	"(ReqT*, Args ."
	.string	"..)>::For(node::ReqWrap<T>*, node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_shutdown_s; Args = {int}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_shutdown_s*, int)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI13uv_shutdown_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC21
	.weak	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args
	.section	.rodata.str1.1
.LC22:
	.string	"../src/req_wrap-inl.h:28"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"(false) == (persistent().IsEmpty())"
	.align 8
.LC24:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_write_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI10uv_write_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI10uv_write_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI10uv_write_sED4EvE4args, 24
_ZZN4node7ReqWrapI10uv_write_sED4EvE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.weak	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_shutdown_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args, 24
_ZZN4node7ReqWrapI13uv_shutdown_sED4EvE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC25
	.section	.rodata.str1.1
.LC26:
	.string	"../src/stream_wrap.cc:236"
.LC27:
	.string	"(stream) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"v8::MaybeLocal<v8::Object> node::AcceptHandle(node::Environment*, node::LibuvStreamWrap*) [with WrapType = node::UDPWrap]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0, @object
	.size	_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0, 24
_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"../src/stream_wrap.cc:234"
.LC30:
	.string	"(wrap) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args, @object
	.size	_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args, 24
_ZZN4nodeL12AcceptHandleINS_7UDPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC28
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"v8::MaybeLocal<v8::Object> node::AcceptHandle(node::Environment*, node::LibuvStreamWrap*) [with WrapType = node::PipeWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0, @object
	.size	_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0, 24
_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC31
	.align 16
	.type	_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args, @object
	.size	_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args, 24
_ZZN4nodeL12AcceptHandleINS_8PipeWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"v8::MaybeLocal<v8::Object> node::AcceptHandle(node::Environment*, node::LibuvStreamWrap*) [with WrapType = node::TCPWrap]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0, @object
	.size	_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0, 24
_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args_0:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC32
	.align 16
	.type	_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args, @object
	.size	_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args, 24
_ZZN4nodeL12AcceptHandleINS_7TCPWrapEEEN2v810MaybeLocalINS2_6ObjectEEEPNS_11EnvironmentEPNS_15LibuvStreamWrapEE4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/stream_wrap.cc"
.LC34:
	.string	"stream_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC33
	.quad	0
	.quad	_ZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC34
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC35:
	.string	"../src/stream_wrap.cc:398"
.LC36:
	.string	"(req_wrap) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"static void node::LibuvStreamWrap::AfterUvWrite(uv_write_t*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_siE4args, @object
	.size	_ZZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_siE4args, 24
_ZZN4node15LibuvStreamWrap12AfterUvWriteEP10uv_write_siE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.section	.rodata.str1.1
.LC38:
	.string	"../src/stream_wrap.cc:333"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"static void node::LibuvStreamWrap::AfterUvShutdown(uv_shutdown_t*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_siE4args, @object
	.size	_ZZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_siE4args, 24
_ZZN4node15LibuvStreamWrap15AfterUvShutdownEP13uv_shutdown_siE4args:
	.quad	.LC38
	.quad	.LC36
	.quad	.LC39
	.section	.rodata.str1.1
.LC40:
	.string	"../src/stream_wrap.cc:304"
.LC41:
	.string	"(args.Length()) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"static void node::LibuvStreamWrap::SetBlocking(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node15LibuvStreamWrap11SetBlockingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.section	.rodata.str1.1
.LC43:
	.string	"../src/stream_wrap.cc:269"
.LC44:
	.string	"(type) == (UV_UNKNOWN_HANDLE)"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"void node::LibuvStreamWrap::OnUvRead(ssize_t, const uv_buf_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args_0, @object
	.size	_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args_0, 24
_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args_0:
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.section	.rodata.str1.1
.LC46:
	.string	"../src/stream_wrap.cc:257"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"(persistent().IsEmpty()) == (false)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args, @object
	.size	_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args, 24
_ZZN4node15LibuvStreamWrap8OnUvReadElPK8uv_buf_tE4args:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC45
	.section	.rodata.str1.1
.LC48:
	.string	"../src/stream_wrap.cc:160"
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"!sw.IsEmpty() && sw->HasInstance(object)"
	.align 8
.LC50:
	.string	"static node::LibuvStreamWrap* node::LibuvStreamWrap::From(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @object
	.size	_ZZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node15LibuvStreamWrap4FromEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.section	.rodata.str1.1
.LC51:
	.string	"../src/stream_wrap.cc:62"
.LC52:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"node::LibuvStreamWrap::Initialize(v8::Local<v8::Object>, v8::Local<v8::Value>, v8::Local<v8::Context>, void*)::<lambda(const v8::FunctionCallbackInfo<v8::Value>&)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENKUlRKNS1_20FunctionCallbackInfoIS5_EEE_clESD_E4args, @object
	.size	_ZZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENKUlRKNS1_20FunctionCallbackInfoIS5_EEE_clESD_E4args, 24
_ZZZN4node15LibuvStreamWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPvENKUlRKNS1_20FunctionCallbackInfoIS5_EEE_clESD_E4args:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.weak	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC54:
	.string	"../src/req_wrap-inl.h:13"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC56:
	.string	"node::ReqWrapBase::ReqWrapBase(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, 24
_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC57:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC59:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC60:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC60
	.quad	.LC58
	.quad	.LC61
	.weak	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC62:
	.string	"../src/stream_base-inl.h:26"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"(req_wrap_obj->GetAlignedPointerFromInternalField( StreamReq::kStreamReqField)) == (nullptr)"
	.align 8
.LC64:
	.string	"void node::StreamReq::AttachToObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC65:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC67:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC68:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC70:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC71:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC73:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC74:
	.string	"../src/env-inl.h:955"
.LC75:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC77:
	.string	"../src/env-inl.h:399"
.LC78:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	7517463719428581715
	.quad	8239175502546629749
	.section	.rodata.cst8
	.align 8
.LC9:
	.long	0
	.long	1072693248
	.align 8
.LC11:
	.long	0
	.long	1073741824
	.align 8
.LC13:
	.long	0
	.long	1074266112
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
