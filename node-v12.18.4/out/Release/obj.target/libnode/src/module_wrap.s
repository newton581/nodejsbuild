	.file	"module_wrap.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4286:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4288:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node6loader10ModuleWrap8SelfSizeEv,"axG",@progbits,_ZNK4node6loader10ModuleWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6loader10ModuleWrap8SelfSizeEv
	.type	_ZNK4node6loader10ModuleWrap8SelfSizeEv, @function
_ZNK4node6loader10ModuleWrap8SelfSizeEv:
.LFB4624:
	.cfi_startproc
	endbr64
	movl	$144, %eax
	ret
	.cfi_endproc
.LFE4624:
	.size	_ZNK4node6loader10ModuleWrap8SelfSizeEv, .-_ZNK4node6loader10ModuleWrap8SelfSizeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB6044:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE6044:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB6045:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE6045:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB6046:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE6046:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB6048:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L9
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE6048:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10847:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L10
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	ret
	.cfi_endproc
.LFE10847:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10849:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L13
	call	_ZdlPv@PLT
.L13:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10849:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev:
.LFB4623:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$10, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559798001485, %rcx
	movq	%rdx, (%rdi)
	movl	$28769, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE4623:
	.size	_ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L27
	cmpw	$1040, %cx
	jne	.L17
.L27:
	movq	23(%rdx), %r12
.L19:
	cmpl	$1, 16(%rbx)
	movq	352(%r12), %r14
	je	.L36
	leaq	_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L37
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L38
	movq	8(%rbx), %r13
.L22:
	movq	2800(%r12), %rdi
	movq	352(%r12), %r15
	testq	%rdi, %rdi
	je	.L23
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2800(%r12)
.L23:
	testq	%r13, %r13
	je	.L24
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2800(%r12)
.L24:
	addq	$8, %rsp
	movq	%r14, %rdi
	leaq	_ZN4node6loader10ModuleWrap38HostInitializeImportMetaObjectCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEENS3_INS2_6ObjectEEE(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate41SetHostInitializeImportMetaObjectCallbackEPFvNS_5LocalINS_7ContextEEENS1_INS_6ModuleEEENS1_INS_6ObjectEEEE@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7828:
	.size	_ZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	8(%rdi), %r13
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L51
	cmpw	$1040, %cx
	jne	.L40
.L51:
	movq	23(%rdx), %r12
.L42:
	leaq	-80(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpl	$1, 16(%rbx)
	je	.L61
	leaq	_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L61:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L62
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L63
	movq	8(%rbx), %r14
.L45:
	movq	2792(%r12), %rdi
	movq	352(%r12), %rbx
	testq	%rdi, %rdi
	je	.L46
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2792(%r12)
.L46:
	testq	%r14, %r14
	je	.L47
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2792(%r12)
.L47:
	leaq	_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v87Isolate38SetHostImportModuleDynamicallyCallbackEPFNS_10MaybeLocalINS_7PromiseEEENS_5LocalINS_7ContextEEENS4_INS_14ScriptOrModuleEEENS4_INS_6StringEEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L64
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L64:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7826:
	.size	_ZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap9GetStatusERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap9GetStatusERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap9GetStatusERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	movq	%rdi, %rbx
	movq	(%rdi), %rax
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	movq	8(%rax), %r12
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L80
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L72
	cmpw	$1040, %cx
	jne	.L67
.L72:
	movq	23(%rdx), %rax
.L69:
	testq	%rax, %rax
	je	.L65
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L71:
	movq	(%rbx), %rbx
	call	_ZNK2v86Module9GetStatusEv@PLT
	salq	$32, %rax
	movq	%rax, 24(%rbx)
.L65:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7817:
	.size	_ZN4node6loader10ModuleWrap9GetStatusERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap9GetStatusERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap8GetErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap8GetErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap8GetErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	movq	%rdi, %rbx
	movq	(%rdi), %rax
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	movq	8(%rax), %r12
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L97
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L89
	cmpw	$1040, %cx
	jne	.L83
.L89:
	movq	23(%rdx), %rax
.L85:
	testq	%rax, %rax
	je	.L81
	movq	48(%rax), %rdi
	testq	%rdi, %rdi
	je	.L87
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L87:
	movq	(%rbx), %rbx
	call	_ZNK2v86Module12GetExceptionEv@PLT
	testq	%rax, %rax
	je	.L98
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L81:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L81
	.cfi_endproc
.LFE7819:
	.size	_ZN4node6loader10ModuleWrap8GetErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap8GetErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot get namespace, Module has not been instantiated"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap12GetNamespaceERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap12GetNamespaceERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap12GetNamespaceERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L115
	cmpw	$1040, %cx
	jne	.L100
.L115:
	movq	23(%rdx), %r13
.L102:
	movq	8(%rbx), %r12
	movq	8(%rdi), %r14
	leaq	8(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L124
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L116
	cmpw	$1040, %cx
	jne	.L104
.L116:
	movq	23(%rdx), %rax
.L106:
	testq	%rax, %rax
	je	.L99
	movq	48(%rax), %r12
	testq	%r12, %r12
	je	.L108
	movq	(%r12), %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L108:
	movq	%r12, %rdi
	call	_ZNK2v86Module9GetStatusEv@PLT
	subl	$2, %eax
	cmpl	$2, %eax
	ja	.L125
	movq	%r12, %rdi
	call	_ZN2v86Module18GetModuleNamespaceEv@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L126
	movq	(%rax), %rax
.L112:
	movq	%rax, 24(%rdx)
.L99:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	352(%r13), %rsi
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r13), %r13
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L128
.L110:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L100:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r13
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L126:
	movq	16(%rdx), %rax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L110
.L127:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7816:
	.size	_ZN4node6loader10ModuleWrap12GetNamespaceERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap12GetNamespaceERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7830:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r13
	movq	%rdi, %rbx
	movq	(%rdi), %rax
	leaq	8(%r13), %r14
	movq	%r14, %rdi
	movq	8(%rax), %r12
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L147
	movq	8(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L141
	cmpw	$1040, %cx
	jne	.L131
.L141:
	movq	23(%rdx), %rax
.L133:
	testq	%rax, %rax
	je	.L145
	cmpb	$0, 40(%rax)
	je	.L148
	cmpl	$2, 16(%rbx)
	jne	.L149
	movq	8(%rbx), %r13
	movq	0(%r13), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L138
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L138
	movq	48(%rax), %rdi
	leaq	-8(%r13), %r14
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L140:
	popq	%rbx
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Module24SetSyntheticModuleExportEPNS_7IsolateENS_5LocalINS_6StringEEENS3_INS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	leaq	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L131:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L145:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7830:
	.size	_ZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"ERR_EXECUTION_ENVIRONMENT_NOT_AVAILABLE"
	.align 8
.LC3:
	.string	"Context not associated with Node.js environment"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"code"
.LC5:
	.string	"Invalid host defined options"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB6:
	.text
.LHOTB6:
	.p2align 4
	.type	_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE, @function
_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE:
.LFB7821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.L152
	movq	%rbx, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L152
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L152
	movq	271(%rax), %r14
	testq	%r14, %r14
	je	.L152
	leaq	-112(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	2792(%r14), %rax
	movq	%r13, %rdi
	movq	%rax, -128(%rbp)
	call	_ZN2v814ScriptOrModule21GetHostDefinedOptionsEv@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZNK2v814PrimitiveArray6LengthEv@PLT
	cmpl	$10, %eax
	je	.L161
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	_ZN2v87Promise8Resolver3NewENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L165
	movq	%rbx, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$28, %ecx
	xorl	%edx, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L236
.L187:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, %rdx
	call	_ZN2v87Promise8Resolver6RejectENS_5LocalINS_7ContextEEENS2_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L237
.L164:
	movq	%r13, %rdi
	call	_ZN2v87Promise8Resolver10GetPromiseEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L165:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
.L160:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v814PrimitiveArray3GetEPNS_7IsolateEi@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testb	%al, %al
	je	.L239
.L166:
	sarq	$32, %rcx
	movl	$9, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rcx, -136(%rbp)
	call	_ZN2v814PrimitiveArray3GetEPNS_7IsolateEi@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	-136(%rbp), %rcx
	testb	%al, %al
	je	.L240
.L167:
	shrq	$32, %rax
	movl	%eax, %edi
	testl	%ecx, %ecx
	je	.L241
	cmpl	$1, %ecx
	je	.L242
	cmpl	$2, %ecx
	jne	.L179
	movq	280(%r14), %rsi
	xorl	%edx, %edx
	divq	%rsi
	movq	272(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L180
	movq	(%rax), %rcx
	movl	8(%rcx), %r10d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L243:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L180
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %r10
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L180
.L182:
	cmpl	%r10d, %edi
	jne	.L243
.L181:
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L173
.L229:
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L244
.L173:
	movq	%rax, -80(%rbp)
	movq	-128(%rbp), %rdi
	leaq	88(%r12), %rdx
	movq	%rbx, %rsi
	movq	-120(%rbp), %rax
	leaq	-80(%rbp), %r8
	movl	$2, %ecx
	movq	%rax, -72(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L245
	movq	%rax, %rdi
	call	_ZNK2v85Value9IsPromiseEv@PLT
	testb	%al, %al
	je	.L246
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L152:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L247
.L155:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L248
.L156:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L249
.L157:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L250
.L158:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L251
.L159:
	movq	%r12, %rdi
	movq	%r15, %rsi
	xorl	%r12d, %r12d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L241:
	movq	224(%r14), %rcx
	xorl	%edx, %edx
	divq	%rcx
	movq	216(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L175
	movq	(%rax), %rsi
	movl	8(%rsi), %r10d
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L253:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L175
	movl	8(%rsi), %eax
	xorl	%edx, %edx
	movq	%rax, %r10
	divq	%rcx
	cmpq	%rdx, %r8
	jne	.L252
.L171:
	cmpl	%r10d, %edi
	jne	.L253
	movq	16(%rsi), %rcx
	movq	8(%rcx), %rax
	testq	%rax, %rax
	jne	.L229
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L245:
	xorl	%r12d, %r12d
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L244:
	movq	16(%rcx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L242:
	movq	168(%r14), %rsi
	xorl	%edx, %edx
	divq	%rsi
	movq	160(%r14), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L175
	movq	(%rax), %rcx
	movl	8(%rcx), %r10d
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L255:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L175
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %r10
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L254
.L177:
	cmpl	%r10d, %edi
	jne	.L255
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L247:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L249:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L250:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L251:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L237:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L236:
	movq	%rdi, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdi
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L239:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-136(%rbp), %rcx
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%rcx, -144(%rbp)
	movq	%rax, -136(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-144(%rbp), %rcx
	movq	-136(%rbp), %rax
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L246:
	leaq	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L238:
	call	__stack_chk_fail@PLT
.L179:
	leaq	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L254:
	jmp	.L175
.L252:
	jmp	.L175
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE.cold, @function
_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE.cold:
.LFSB7821:
.L175:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	16, %rax
	ud2
	.cfi_endproc
.LFE7821:
	.text
	.size	_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE, .-_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE
	.section	.text.unlikely
	.size	_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE.cold, .-_ZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEE.cold
.LCOLDE6:
	.text
.LHOTE6:
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap29GetStaticDependencySpecifiersERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap29GetStaticDependencySpecifiersERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap29GetStaticDependencySpecifiersERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L280
	cmpw	$1040, %cx
	jne	.L257
.L280:
	movq	23(%rdx), %rax
	movq	%rax, -216(%rbp)
.L259:
	movq	8(%rbx), %r12
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L298
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L281
	cmpw	$1040, %cx
	jne	.L261
.L281:
	movq	23(%rdx), %rax
.L263:
	testq	%rax, %rax
	je	.L256
	movq	48(%rax), %r14
	testq	%r14, %r14
	je	.L265
	movq	-216(%rbp), %rax
	movq	(%r14), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
.L265:
	movq	%r14, %rdi
	call	_ZNK2v86Module23GetModuleRequestsLengthEv@PLT
	movdqa	.LC7(%rip), %xmm0
	movslq	%eax, %r13
	leaq	-184(%rbp), %rax
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r13, %r12
	movups	%xmm0, -184(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	cmpq	$16, %r13
	ja	.L299
	movq	%rax, %r8
.L266:
	movq	%r13, -208(%rbp)
	testl	%r12d, %r12d
	jle	.L271
	subl	$1, %r12d
	xorl	%r15d, %r15d
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-192(%rbp), %r8
	movq	%rax, (%r8,%r15,8)
	leaq	1(%r15), %rax
	cmpq	%r15, %r12
	je	.L271
	movq	%rax, %r15
.L273:
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	_ZNK2v86Module16GetModuleRequestEi@PLT
	cmpq	%r15, -208(%rbp)
	ja	.L272
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L299:
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r13,8), %r15
	andq	%r13, %rax
	cmpq	%rax, %r13
	jne	.L300
	testq	%r15, %r15
	je	.L268
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L301
	movq	%rax, -192(%rbp)
	movq	%r13, -200(%rbp)
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L271:
	movq	-216(%rbp), %rax
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	(%rbx), %rbx
	movq	352(%rax), %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L302
	movq	(%rax), %rax
.L275:
	movq	%rax, 24(%rbx)
	movq	-192(%rbp), %rdi
	cmpq	-224(%rbp), %rdi
	je	.L256
	testq	%rdi, %rdi
	je	.L256
	call	free@PLT
.L256:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L303
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L257:
	.cfi_restore_state
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -216(%rbp)
	jmp	.L259
.L261:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L263
.L298:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L268:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L302:
	movq	16(%rbx), %rax
	jmp	.L275
.L301:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L268
	movq	-208(%rbp), %rax
	movq	%r8, -192(%rbp)
	movq	%r13, -200(%rbp)
	testq	%rax, %rax
	je	.L266
	movq	-224(%rbp), %rsi
	movq	%r8, %rdi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L266
.L300:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L303:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7818:
	.size	_ZN4node6loader10ModuleWrap29GetStaticDependencySpecifiersERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap29GetStaticDependencySpecifiersERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7831:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	8(%rdi), %r12
	movq	%rdi, %rbx
	movq	(%rdi), %rax
	leaq	8(%r12), %r14
	movq	%r14, %rdi
	movq	8(%rax), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L327
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L321
	cmpw	$1040, %cx
	jne	.L306
.L321:
	movq	23(%rdx), %rax
.L308:
	testq	%rax, %rax
	je	.L304
	cmpb	$0, 40(%rax)
	jne	.L328
	movq	48(%rax), %r12
	testq	%r12, %r12
	je	.L311
	movq	(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L311:
	movq	%r12, %rdi
	call	_ZNK2v86Module9GetStatusEv@PLT
	cmpl	$2, %eax
	jg	.L329
	movq	%r12, %rdi
	call	_ZN2v86Module22GetUnboundModuleScriptEv@PLT
	movq	%rax, %rdi
	call	_ZN2v814ScriptCompiler15CreateCodeCacheENS_5LocalINS_19UnboundModuleScriptEEE@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r12
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L322
	cmpw	$1040, %cx
	jne	.L313
.L322:
	movq	23(%rdx), %rdi
.L315:
	testq	%r12, %r12
	je	.L330
	movslq	8(%r12), %rdx
	movq	(%r12), %rsi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L331
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L320:
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	(%rbx), %rbx
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEm@PLT
	testq	%rax, %rax
	je	.L332
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L304:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L313:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L328:
	leaq	_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L329:
	leaq	_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L331:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L332:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L304
	.cfi_endproc
.LFE7831:
	.size	_ZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L356
	cmpw	$1040, %cx
	jne	.L334
.L356:
	movq	23(%rdx), %r14
.L336:
	movq	8(%rbx), %rbx
	movq	8(%rdi), %r13
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L380
	movq	8(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L357
	cmpw	$1040, %cx
	jne	.L338
.L357:
	movq	23(%rdx), %rbx
.L340:
	testq	%rbx, %rbx
	je	.L333
	movq	128(%rbx), %r8
	testq	%r8, %r8
	je	.L342
	movq	(%r8), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r8
.L342:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L343
	movq	(%r12), %rsi
	movq	%r13, %rdi
	movq	%r8, -136(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %r12
.L343:
	movq	352(%r14), %rsi
	leaq	-128(%rbp), %r15
	movq	%r8, -136(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-136(%rbp), %r8
	movq	%r12, %rdi
	movq	%r14, -80(%rbp)
	leaq	_ZN4node6loader10ModuleWrap15ResolveCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEENS3_INS2_6ModuleEEE(%rip), %rdx
	movl	$0, -72(%rbp)
	movq	%r8, %rsi
	call	_ZN2v86Module17InstantiateModuleENS_5LocalINS_7ContextEEEPFNS_10MaybeLocalIS0_EES3_NS1_INS_6StringEEENS1_IS0_EEE@PLT
	movq	88(%rbx), %r12
	testq	%r12, %r12
	jne	.L344
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L349
.L350:
	movq	%r13, %r12
.L344:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L347
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L347:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L381
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L350
.L349:
	movq	80(%rbx), %rax
	movq	72(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 96(%rbx)
	movq	%r15, %rdi
	movq	$0, 88(%rbx)
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L345
.L379:
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
.L333:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L382
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L379
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	testq	%rax, %rax
	je	.L383
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	testq	%rax, %rax
	je	.L384
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%r15, %rdi
	movq	%rax, %r12
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%r14, %rdi
	movl	$2, %ecx
	movq	%r12, %rdx
	movq	%rax, %rsi
	call	_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE@PLT
	movq	%r15, %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L334:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L338:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L380:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L382:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7814:
	.size	_ZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC8:
	.string	"ModuleWrap"
.LC9:
	.string	"link"
.LC10:
	.string	"instantiate"
.LC11:
	.string	"evaluate"
.LC12:
	.string	"setExport"
.LC13:
	.string	"createCachedData"
.LC14:
	.string	"getNamespace"
.LC15:
	.string	"getStatus"
.LC16:
	.string	"getError"
.LC17:
	.string	"getStaticDependencySpecifiers"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"setImportModuleDynamicallyCallback"
	.align 8
.LC19:
	.string	"setInitializeImportMetaObjectCallback"
	.section	.rodata.str1.1
.LC20:
	.string	"kUninstantiated"
.LC21:
	.string	"kInstantiating"
.LC22:
	.string	"kInstantiated"
.LC23:
	.string	"kEvaluating"
.LC24:
	.string	"kEvaluated"
.LC25:
	.string	"kErrored"
	.section	.text.unlikely
	.align 2
.LCOLDB26:
	.text
.LHOTB26:
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.type	_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L386
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L386
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L386
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %r15
	movq	2680(%rbx), %rdx
	pushq	$0
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$10, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L422
.L387:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L423
.L388:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L424
.L389:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L425
.L390:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC12(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L426
.L391:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L427
.L392:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap12GetNamespaceERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L428
.L393:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap9GetStatusERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC15(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %rsi
	popq	%rax
	testq	%rsi, %rsi
	je	.L429
.L394:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap8GetErrorERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC16(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L430
.L395:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	movq	%rax, %rcx
	leaq	_ZN4node6loader10ModuleWrap29GetStaticDependencySpecifiersERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC17(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L431
.L396:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L432
.L397:
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC8(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L433
.L398:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L434
.L399:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L435
.L400:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L436
.L401:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L437
.L402:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L438
.L403:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L439
.L404:
	movq	%r8, %rdx
	movq	%r12, %rcx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L440
.L405:
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$15, %ecx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L441
.L406:
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L442
.L407:
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$14, %ecx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L443
.L408:
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L444
.L409:
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$13, %ecx
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L445
.L410:
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L446
.L411:
	movq	352(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L447
.L412:
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L448
.L413:
	movq	352(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L449
.L414:
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L450
.L415:
	movq	352(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC25(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L451
.L416:
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L452
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L423:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L425:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L428:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L429:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L431:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L432:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L433:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L434:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L435:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L437:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L438:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L439:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L440:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L442:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L443:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L444:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L446:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L448:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L452:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7834:
.L386:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7834:
	.text
	.size	_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE26:
	.text
.LHOTE26:
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7138:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	16(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	2592(%r12), %rcx
	movq	%rax, (%rdi)
	movq	%rdi, %rax
	movq	2584(%r12), %r13
	subq	$1, 2656(%r12)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r8
	testq	%r8, %r8
	je	.L454
	movq	(%r8), %rdi
	movq	%rdx, %r11
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L455:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L454
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L454
	movq	%r9, %rdi
.L457:
	cmpq	%rsi, %rbx
	jne	.L455
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	cmpq	%rax, 8(%rdi)
	jne	.L455
	cmpq	16(%rdi), %rbx
	jne	.L455
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L489
	testq	%rsi, %rsi
	je	.L459
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L459
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L459:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L454:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L490
.L462:
	cmpq	$0, 8(%rbx)
	je	.L453
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r12
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L466
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L491
.L466:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L453
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L453:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L492
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L471
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L459
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L458:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L493
.L460:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L491:
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r10, %rax
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L490:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L494
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L462
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%rsi, 2600(%r12)
	jmp	.L460
.L494:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L492:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7138:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7140:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7140:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap9GetFromIDEPNS_11EnvironmentEj
	.type	_ZN4node6loader10ModuleWrap9GetFromIDEPNS_11EnvironmentEj, @function
_ZN4node6loader10ModuleWrap9GetFromIDEPNS_11EnvironmentEj:
.LFB7807:
	.cfi_startproc
	endbr64
	movq	168(%rdi), %rcx
	movl	%esi, %eax
	xorl	%edx, %edx
	divq	%rcx
	movq	160(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L497
	movq	(%r8), %r8
	movl	8(%r8), %edi
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L509:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L497
	movl	8(%r8), %eax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L508
.L500:
	cmpl	%esi, %edi
	jne	.L509
	movq	16(%r8), %r8
.L497:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L508:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE7807:
	.size	_ZN4node6loader10ModuleWrap9GetFromIDEPNS_11EnvironmentEj, .-_ZN4node6loader10ModuleWrap9GetFromIDEPNS_11EnvironmentEj
	.p2align 4
	.globl	_Z21_register_module_wrapv
	.type	_Z21_register_module_wrapv, @function
_Z21_register_module_wrapv:
.LFB7835:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7835:
	.size	_Z21_register_module_wrapv, .-_Z21_register_module_wrapv
	.section	.text._ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_:
.LFB9206:
	.cfi_startproc
	endbr64
	movslq	(%rsi), %rax
	movq	8(%rdi), %rsi
	xorl	%edx, %edx
	movq	%rax, %r10
	divq	%rsi
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L512
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L533:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L512
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L512
.L514:
	cmpl	%edi, %r10d
	jne	.L533
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	jne	.L516
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L534:
	cmpl	%r10d, %r8d
	jne	.L515
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L515
.L516:
	movslq	8(%rdi), %rax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	cmpq	%r9, %rdx
	je	.L534
.L515:
	movq	%rcx, %rax
	movq	%rdi, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L512:
	xorl	%eax, %eax
	xorl	%edx, %edx
	ret
	.cfi_endproc
.LFE9206:
	.size	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_, .-_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap13GetFromModuleEPNS_11EnvironmentEN2v85LocalINS4_6ModuleEEE
	.type	_ZN4node6loader10ModuleWrap13GetFromModuleEPNS_11EnvironmentEN2v85LocalINS4_6ModuleEEE, @function
_ZN4node6loader10ModuleWrap13GetFromModuleEPNS_11EnvironmentEN2v85LocalINS4_6ModuleEEE:
.LFB7806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK2v86Module15GetIdentityHashEv@PLT
	leaq	-28(%rbp), %rsi
	leaq	104(%r12), %rdi
	movl	%eax, -28(%rbp)
	call	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_
	cmpq	%rax, %rdx
	je	.L543
	testq	%rbx, %rbx
	je	.L537
	.p2align 4,,10
	.p2align 3
.L540:
	movq	16(%rax), %r8
	movq	48(%r8), %rcx
	testq	%rcx, %rcx
	je	.L538
	movq	(%rbx), %rsi
	cmpq	%rsi, (%rcx)
	je	.L535
.L538:
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L540
.L543:
	xorl	%r8d, %r8d
.L535:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L552
	addq	$16, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	je	.L543
.L537:
	movq	16(%rax), %r8
	cmpq	$0, 48(%r8)
	jne	.L553
	jmp	.L535
.L552:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7806:
	.size	_ZN4node6loader10ModuleWrap13GetFromModuleEPNS_11EnvironmentEN2v85LocalINS4_6ModuleEEE, .-_ZN4node6loader10ModuleWrap13GetFromModuleEPNS_11EnvironmentEN2v85LocalINS4_6ModuleEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap38HostInitializeImportMetaObjectCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEENS3_INS2_6ObjectEEE
	.type	_ZN4node6loader10ModuleWrap38HostInitializeImportMetaObjectCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEENS3_INS2_6ObjectEEE, @function
_ZN4node6loader10ModuleWrap38HostInitializeImportMetaObjectCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEENS3_INS2_6ObjectEEE:
.LFB7827:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L554
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L554
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L554
	movq	271(%rax), %r15
	testq	%r15, %r15
	je	.L554
	movq	%rbx, %rdi
	leaq	-144(%rbp), %r14
	call	_ZNK2v86Module15GetIdentityHashEv@PLT
	leaq	104(%r15), %rdi
	movq	%r14, %rsi
	movl	%eax, -144(%rbp)
	call	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_
	cmpq	%rdx, %rax
	je	.L554
	testq	%rbx, %rbx
	je	.L556
	.p2align 4,,10
	.p2align 3
.L559:
	movq	16(%rax), %rcx
	movq	48(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L557
	movq	(%rsi), %rsi
	cmpq	%rsi, (%rbx)
	je	.L558
.L557:
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L559
.L554:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L585
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L586:
	.cfi_restore_state
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	je	.L554
.L556:
	movq	16(%rax), %rcx
	cmpq	$0, 48(%rcx)
	jne	.L586
.L558:
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L560
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L587
.L560:
	movq	352(%r15), %rsi
	movq	%r14, %rdi
	movq	%rax, -80(%rbp)
	movq	2800(%r15), %rbx
	movq	%r13, -72(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	leaq	-80(%rbp), %r8
	movq	%r12, %rsi
	movq	%r15, -96(%rbp)
	movq	352(%r15), %rdx
	movq	%rbx, %rdi
	movl	$2, %ecx
	movl	$0, -88(%rbp)
	addq	$88, %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L588
.L562:
	movq	%r14, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L588:
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L562
	movq	%r14, %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L587:
	movq	16(%rcx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L560
.L585:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7827:
	.size	_ZN4node6loader10ModuleWrap38HostInitializeImportMetaObjectCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEENS3_INS2_6ObjectEEE, .-_ZN4node6loader10ModuleWrap38HostInitializeImportMetaObjectCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEENS3_INS2_6ObjectEEE
	.section	.text.unlikely
	.align 2
.LCOLDB27:
	.text
.LHOTB27:
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE
	.type	_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE, @function
_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE:
.LFB7829:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L590
	movq	%rdi, %r15
	movq	%rsi, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L590
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L590
	movq	271(%rax), %rbx
	movq	%r13, %rdi
	leaq	-128(%rbp), %r12
	movq	352(%rbx), %rax
	movq	%rax, -136(%rbp)
	call	_ZNK2v86Module15GetIdentityHashEv@PLT
	leaq	104(%rbx), %rdi
	movq	%r12, %rsi
	movl	%eax, -128(%rbp)
	call	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_
	cmpq	%rdx, %rax
	je	.L591
	testq	%r13, %r13
	jne	.L595
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L593:
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	je	.L591
.L595:
	movq	16(%rax), %r14
	movq	48(%r14), %rsi
	testq	%rsi, %rsi
	je	.L593
	movq	(%rsi), %rcx
	cmpq	%rcx, 0(%r13)
	jne	.L593
.L594:
	movq	352(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -80(%rbp)
	movq	32(%r14), %rbx
	movl	$0, -72(%rbp)
	testq	%rbx, %rbx
	je	.L596
	movq	(%rbx), %rsi
	movq	-136(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rbx
.L596:
	movq	8(%r14), %rdx
	testq	%rdx, %rdx
	je	.L597
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L631
.L597:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L632
.L598:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L599
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 32(%r14)
.L599:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L633
.L603:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L634
	addq	$104, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L635:
	.cfi_restore_state
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	je	.L591
.L592:
	movq	16(%rax), %r14
	cmpq	$0, 48(%r14)
	jne	.L635
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L633:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L603
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	testq	%rax, %rax
	je	.L636
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	testq	%rax, %rax
	je	.L637
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L631:
	movq	16(%r14), %rax
	movq	(%rdx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdx
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L632:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L598
	leaq	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L636:
	leaq	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L637:
	leaq	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L634:
	call	__stack_chk_fail@PLT
.L591:
	movq	352(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -80(%rbp)
	movq	32, %rax
	movl	$0, -72(%rbp)
	ud2
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE.cold, @function
_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE.cold:
.LFSB7829:
.L590:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7829:
	.text
	.size	_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE, .-_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE
	.section	.text.unlikely
	.size	_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE.cold, .-_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE.cold
.LCOLDE27:
	.text
.LHOTE27:
	.section	.rodata._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_.str1.8,"aMS",@progbits,1
	.align 8
.LC28:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_:
.LFB9254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r12), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	leaq	0(,%rdx,8), %rax
	movq	%rax, -80(%rbp)
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L639
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	48(%rbx), %rsi
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L640:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L639
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%r14
	cmpq	%rdx, %r8
	jne	.L639
.L643:
	cmpq	%rsi, %r13
	jne	.L640
	movq	8(%r15), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L640
	movq	%r8, -72(%rbp)
	testq	%rdx, %rdx
	je	.L693
	movq	8(%rbx), %rsi
	movq	(%r15), %rdi
	call	memcmp@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	jne	.L640
.L693:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	leaq	40(%rbx), %rax
	jne	.L694
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	movl	$56, %edi
	call	_Znwm@PLT
	movq	(%r15), %r8
	movq	8(%r15), %r15
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	%rdi, 8(%rax)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L644
	testq	%r8, %r8
	je	.L695
.L644:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L696
	cmpq	$1, %r15
	jne	.L647
	movzbl	(%r8), %eax
	movb	%al, 24(%rbx)
.L648:
	movq	%r15, 16(%rbx)
	movl	$1, %ecx
	movb	$0, (%rdi,%r15)
	movq	24(%r12), %rdx
	leaq	32(%r12), %rdi
	movq	$0, 40(%rbx)
	movq	8(%r12), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r15
	testb	%al, %al
	jne	.L649
	movq	(%r12), %r8
.L650:
	movq	-80(%rbp), %r14
	movq	%r13, 48(%rbx)
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L659
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L660:
	addq	$1, 24(%r12)
	jmp	.L693
	.p2align 4,,10
	.p2align 3
.L647:
	testq	%r15, %r15
	je	.L648
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L696:
	leaq	8(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%rbx)
.L646:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	8(%rbx), %rdi
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L649:
	cmpq	$1, %rdx
	je	.L697
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L698
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L652:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L654
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L656:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L657:
	testq	%rsi, %rsi
	je	.L654
.L655:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r15
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L656
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L663
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L655
	.p2align 4,,10
	.p2align 3
.L654:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L658
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L658:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r15, 8(%r12)
	divq	%r15
	movq	%r8, (%r12)
	leaq	0(,%rdx,8), %rax
	movq	%rax, -80(%rbp)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L659:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L661
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
.L661:
	leaq	16(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L663:
	movq	%rdx, %rdi
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L697:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L652
.L694:
	call	__stack_chk_fail@PLT
.L695:
	leaq	.LC28(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L698:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9254:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"linking error, expected resolver to return a promise"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1400, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdx), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L744
	cmpw	$1040, %si
	jne	.L700
.L744:
	movq	23(%rcx), %r14
.L702:
	movq	8(%rdx), %rax
	cmpl	$1, 16(%r13)
	movq	%rax, -1408(%rbp)
	je	.L798
	leaq	_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L798:
	movq	8(%r13), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L799
	movq	8(%r13), %rbx
	leaq	8(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -1352(%rbp)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L800
	movq	8(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L745
	cmpw	$1040, %cx
	jne	.L705
.L745:
	movq	23(%rdx), %rax
	movq	%rax, -1336(%rbp)
.L707:
	testq	%rax, %rax
	je	.L699
	cmpb	$0, 64(%rax)
	jne	.L699
	movb	$1, 64(%rax)
	movl	16(%r13), %eax
	testl	%eax, %eax
	jle	.L801
	movq	8(%r13), %rax
	movq	%rax, -1376(%rbp)
.L710:
	movq	-1336(%rbp), %rax
	movq	128(%rax), %rax
	movq	%rax, -1368(%rbp)
	testq	%rax, %rax
	je	.L711
	movq	(%rax), %rsi
	movq	-1408(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, -1368(%rbp)
.L711:
	movq	-1336(%rbp), %rax
	movq	48(%rax), %rax
	movq	%rax, -1344(%rbp)
	testq	%rax, %rax
	je	.L712
	movq	(%rax), %rsi
	movq	-1408(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, -1344(%rbp)
.L712:
	movq	-1344(%rbp), %rdi
	call	_ZNK2v86Module23GetModuleRequestsLengthEv@PLT
	movdqa	.LC7(%rip), %xmm0
	movslq	%eax, %r12
	leaq	-1240(%rbp), %rax
	movaps	%xmm0, -1264(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r12, %rbx
	movups	%xmm0, -1240(%rbp)
	movq	%rax, -1416(%rbp)
	movq	%rax, -1248(%rbp)
	movq	$0, -1240(%rbp)
	movups	%xmm0, -1224(%rbp)
	movups	%xmm0, -1208(%rbp)
	movups	%xmm0, -1192(%rbp)
	movups	%xmm0, -1176(%rbp)
	movups	%xmm0, -1160(%rbp)
	movups	%xmm0, -1144(%rbp)
	movups	%xmm0, -1128(%rbp)
	cmpq	$16, %r12
	jbe	.L742
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r12,8), %rdi
	andq	%r12, %rax
	cmpq	%rax, %r12
	jne	.L802
	testq	%rdi, %rdi
	je	.L715
	movq	%rdi, -1360(%rbp)
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L803
	movq	%rax, -1248(%rbp)
	movq	%r12, -1256(%rbp)
.L713:
	movq	%r12, -1264(%rbp)
	testl	%ebx, %ebx
	jle	.L718
	leal	-1(%rbx), %eax
	leaq	-1328(%rbp), %rcx
	movq	%r13, -1432(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, -1400(%rbp)
	leaq	-1104(%rbp), %rax
	movq	%rax, -1384(%rbp)
	leaq	-1280(%rbp), %rax
	movq	%rax, -1424(%rbp)
	movq	%rax, %r13
	movq	%rcx, -1392(%rbp)
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L720:
	cmpq	$1, %rbx
	jne	.L722
	movzbl	(%r8), %eax
	movb	%al, -1280(%rbp)
	movq	%r13, %rax
.L723:
	movq	%rbx, -1288(%rbp)
	movq	-1352(%rbp), %rdx
	leaq	-1304(%rbp), %r8
	movl	$1, %ecx
	movb	$0, (%rax,%rbx)
	movq	-1368(%rbp), %rsi
	movq	-1376(%rbp), %rdi
	movq	%r15, -1304(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L804
	movq	%rax, %rdi
	call	_ZNK2v85Value9IsPromiseEv@PLT
	testb	%al, %al
	je	.L805
.L728:
	movq	-1336(%rbp), %rax
	leaq	-1296(%rbp), %rsi
	leaq	72(%rax), %rdi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	352(%r14), %r8
	movq	(%rax), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L730
	movq	%r8, -1360(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, (%r15)
	movq	-1360(%rbp), %r8
.L730:
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, (%r15)
	cmpq	%r12, -1264(%rbp)
	jbe	.L806
	movq	-1248(%rbp), %rax
	movq	-1296(%rbp), %rdi
	movq	%rbx, (%rax,%r12,8)
	cmpq	%r13, %rdi
	je	.L732
	call	_ZdlPv@PLT
.L732:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L733
	testq	%rdi, %rdi
	je	.L733
	call	free@PLT
	leaq	1(%r12), %rax
	cmpq	%r12, -1400(%rbp)
	je	.L795
.L735:
	movq	%rax, %r12
.L736:
	movq	-1344(%rbp), %rdi
	movl	%r12d, %esi
	call	_ZNK2v86Module16GetModuleRequestEi@PLT
	movq	352(%r14), %rsi
	movq	-1384(%rbp), %rdi
	movq	%rax, %rdx
	movq	%rax, %r15
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r8
	movq	-1104(%rbp), %rbx
	movq	%r13, -1296(%rbp)
	movq	%r8, %rax
	addq	%rbx, %rax
	je	.L719
	testq	%r8, %r8
	je	.L807
.L719:
	movq	%rbx, -1328(%rbp)
	cmpq	$15, %rbx
	jbe	.L720
	movq	-1392(%rbp), %rsi
	leaq	-1296(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -1360(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1360(%rbp), %r8
	movq	%rax, -1296(%rbp)
	movq	%rax, %rdi
	movq	-1328(%rbp), %rax
	movq	%rax, -1280(%rbp)
.L721:
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-1328(%rbp), %rbx
	movq	-1296(%rbp), %rax
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L733:
	leaq	1(%r12), %rax
	cmpq	%r12, -1400(%rbp)
	jne	.L735
.L795:
	movq	-1432(%rbp), %r13
	movq	-1248(%rbp), %r8
	movq	-1264(%rbp), %r12
.L718:
	movq	-1408(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r8, %rsi
	movq	0(%r13), %rbx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	testq	%rax, %rax
	je	.L808
	movq	(%rax), %rax
.L738:
	movq	%rax, 24(%rbx)
	movq	-1248(%rbp), %rdi
	cmpq	-1416(%rbp), %rdi
	je	.L699
	testq	%rdi, %rdi
	je	.L699
.L797:
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L699:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L809
	addq	$1400, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L801:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -1376(%rbp)
	jmp	.L710
	.p2align 4,,10
	.p2align 3
.L700:
	leaq	32(%rdx), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r14
	jmp	.L702
.L705:
	movq	-1352(%rbp), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -1336(%rbp)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L722:
	testq	%rbx, %rbx
	jne	.L810
	movq	%r13, %rax
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L805:
	movq	352(%r14), %rsi
	movq	-1392(%rbp), %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r14), %r15
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC29(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L811
.L729:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-1392(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L742:
	movq	%rax, %r8
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L806:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L799:
	leaq	_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L800:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L804:
	movq	-1296(%rbp), %rdi
	cmpq	-1424(%rbp), %rdi
	je	.L725
	call	_ZdlPv@PLT
.L725:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L726
	testq	%rdi, %rdi
	je	.L726
	call	free@PLT
.L726:
	movq	-1248(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L699
	cmpq	-1416(%rbp), %rdi
	jne	.L797
	jmp	.L699
.L811:
	movq	%rax, -1360(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1360(%rbp), %rdi
	jmp	.L729
.L715:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L803:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-1360(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L715
	movq	-1264(%rbp), %rax
	movq	%r8, -1248(%rbp)
	movq	%r12, -1256(%rbp)
	testq	%rax, %rax
	je	.L713
	movq	-1416(%rbp), %rsi
	movq	%r8, %rdi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L713
.L802:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L808:
	movq	16(%rbx), %rax
	jmp	.L738
.L810:
	movq	%r13, %rdi
	jmp	.L721
.L807:
	leaq	.LC28(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L809:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7809:
	.size	_ZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_,"axG",@progbits,_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_
	.type	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_, @function
_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_:
.LFB9269:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rsi
	movq	(%r8), %rdi
	movq	%r8, -56(%rbp)
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r12
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r12
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L819
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L819
	movq	48(%rbx), %rcx
	movq	-56(%rbp), %r8
	movq	%rdx, %r15
	xorl	%r14d, %r14d
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L814:
	testq	%r14, %r14
	jne	.L812
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L812
.L828:
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r12
	cmpq	%rdx, %r15
	jne	.L812
.L817:
	cmpq	%r13, %rcx
	jne	.L814
	movq	8(%r8), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L814
	testq	%rdx, %rdx
	je	.L815
	movq	(%r8), %rdi
	movq	8(%rbx), %rsi
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r8
	testl	%eax, %eax
	jne	.L814
.L815:
	movq	(%rbx), %rbx
	addq	$1, %r14
	testq	%rbx, %rbx
	jne	.L828
.L812:
	addq	$24, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L819:
	.cfi_restore_state
	xorl	%r14d, %r14d
	jmp	.L812
	.cfi_endproc
.LFE9269:
	.size	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_, .-_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"linking error, not in local cache"
	.align 8
.LC31:
	.string	"linking error, dependency promises must be resolved on instantiate"
	.align 8
.LC32:
	.string	"linking error, expected a valid module object from resolver"
	.section	.rodata.str1.1
.LC33:
	.string	"linking error, null dep"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap15ResolveCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEENS3_INS2_6ModuleEEE
	.type	_ZN4node6loader10ModuleWrap15ResolveCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEENS3_INS2_6ModuleEEE, @function
_ZN4node6loader10ModuleWrap15ResolveCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEENS3_INS2_6ModuleEEE:
.LFB7820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1160, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L833
	movq	%rsi, %r12
	movq	%rdx, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L833
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L833
	movq	271(%rax), %rbx
	testq	%rbx, %rbx
	je	.L833
	movq	352(%rbx), %rax
	movq	%r14, %rdi
	leaq	-1168(%rbp), %r13
	movq	%rax, -1176(%rbp)
	call	_ZNK2v86Module15GetIdentityHashEv@PLT
	leaq	104(%rbx), %rdi
	movq	%r13, %rsi
	movl	%eax, -1168(%rbp)
	call	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_
	cmpq	%rdx, %rax
	je	.L840
	testq	%r14, %r14
	je	.L841
	.p2align 4,,10
	.p2align 3
.L844:
	movq	16(%rax), %r15
	movq	48(%r15), %rsi
	testq	%rsi, %rsi
	je	.L842
	movq	(%rsi), %rcx
	cmpq	%rcx, (%r14)
	je	.L843
.L842:
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	jne	.L844
.L840:
	movq	352(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r12
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC33(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L906
.L868:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v811HandleScopeD1Ev@PLT
.L839:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L907
	addq	$1160, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	movq	(%rax), %rax
	cmpq	%rax, %rdx
	je	.L840
.L841:
	movq	16(%rax), %r15
	cmpq	$0, 48(%r15)
	jne	.L908
.L843:
	movq	-1176(%rbp), %rsi
	movq	%r12, %rdx
	leaq	-1104(%rbp), %rdi
	leaq	-1120(%rbp), %r12
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r9
	movq	-1104(%rbp), %r8
	movq	%r12, -1136(%rbp)
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L871
	testq	%r9, %r9
	je	.L909
.L871:
	movq	%r8, -1168(%rbp)
	cmpq	$15, %r8
	ja	.L910
	cmpq	$1, %r8
	jne	.L847
	movzbl	(%r9), %eax
	leaq	-1136(%rbp), %r10
	movb	%al, -1120(%rbp)
	movq	%r12, %rax
.L848:
	movq	%r8, -1128(%rbp)
	addq	$72, %r15
	movq	%r10, %rsi
	movb	$0, (%rax,%r8)
	movq	%r15, %rdi
	movq	%r10, -1184(%rbp)
	call	_ZNKSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_N2v86GlobalINS8_7PromiseEEEESaISC_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSE_18_Mod_range_hashingENSE_20_Default_ranged_hashENSE_20_Prime_rehash_policyENSE_17_Hashtable_traitsILb1ELb0ELb1EEEE5countERS7_
	movq	-1184(%rbp), %r10
	cmpq	$1, %rax
	je	.L849
	movq	352(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r14
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC30(%rip), %rsi
.L904:
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L911
.L857:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v811HandleScopeD1Ev@PLT
.L851:
	movq	-1136(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L866
	call	_ZdlPv@PLT
.L866:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L839
	testq	%rdi, %rdi
	je	.L839
	call	free@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L833:
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L912
.L832:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L913
.L835:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L914
.L836:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L915
.L837:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L916
.L838:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L847:
	testq	%r8, %r8
	jne	.L917
	movq	%r12, %rax
	leaq	-1136(%rbp), %r10
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L849:
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_N2v86GlobalINS9_7PromiseEEEESaISD_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	(%rax), %r15
	testq	%r15, %r15
	je	.L852
	movq	(%r15), %rsi
	movq	-1176(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r15
.L852:
	movq	%r15, %rdi
	call	_ZN2v87Promise5StateEv@PLT
	cmpl	$1, %eax
	je	.L853
	movq	352(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r14
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC31(%rip), %rsi
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L910:
	leaq	-1136(%rbp), %r10
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r9, -1200(%rbp)
	movq	%r10, %rdi
	movq	%r8, -1192(%rbp)
	movq	%r10, -1184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1184(%rbp), %r10
	movq	-1192(%rbp), %r8
	movq	%rax, -1136(%rbp)
	movq	%rax, %rdi
	movq	-1168(%rbp), %rax
	movq	-1200(%rbp), %r9
	movq	%rax, -1120(%rbp)
.L846:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r10, -1184(%rbp)
	call	memcpy@PLT
	movq	-1168(%rbp), %r8
	movq	-1136(%rbp), %rax
	movq	-1184(%rbp), %r10
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L853:
	movq	%r15, %rdi
	call	_ZN2v87Promise6ResultEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L859
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L918
.L859:
	movq	352(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r14
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC32(%rip), %rsi
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L906:
	movq	%rdi, -1176(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1176(%rbp), %rdi
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L911:
	movq	%rax, -1176(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1176(%rbp), %rdi
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L912:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%rax, -1176(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1176(%rbp), %rdi
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L916:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L914:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L915:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L918:
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L919
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L874
	cmpw	$1040, %cx
	jne	.L861
.L874:
	movq	23(%rdx), %rax
.L863:
	xorl	%r13d, %r13d
	testq	%rax, %rax
	je	.L851
	movq	48(%rax), %r13
	testq	%r13, %r13
	je	.L851
	movq	0(%r13), %rsi
	movq	-1176(%rbp), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
	jmp	.L851
.L919:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L861:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L863
.L907:
	call	__stack_chk_fail@PLT
.L909:
	leaq	.LC28(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L917:
	movq	%r12, %rdi
	leaq	-1136(%rbp), %r10
	jmp	.L846
	.cfi_endproc
.LFE7820:
	.size	_ZN4node6loader10ModuleWrap15ResolveCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEENS3_INS2_6ModuleEEE, .-_ZN4node6loader10ModuleWrap15ResolveCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6StringEEENS3_INS2_6ModuleEEE
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB9534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L929
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L930
.L922:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L930:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L931
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L932
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L927
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L927:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L925:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L931:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L924
	cmpq	%r14, %rsi
	je	.L925
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L924:
	cmpq	%r14, %rsi
	je	.L925
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L925
.L929:
	leaq	.LC34(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L932:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9534:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata._ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE.str1.1,"aMS",@progbits,1
.LC35:
	.string	"url"
.LC36:
	.string	"resolve_cache"
.LC37:
	.string	"pair"
.LC38:
	.string	"std::basic_string"
.LC39:
	.string	"first"
.LC40:
	.string	"second"
	.section	.text._ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB4621:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	56(%rdi), %rax
	testq	%rax, %rax
	je	.L935
	movq	(%rax), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L935
	movq	8(%r14), %r12
	leaq	-64(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%r14), %rcx
	movq	%rax, %rdx
	cmpq	32(%r14), %rcx
	je	.L970
	cmpq	72(%r14), %rcx
	je	.L1007
.L938:
	movq	-8(%rcx), %rsi
.L937:
	leaq	.LC35(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L935:
	cmpq	$0, 88(%rbx)
	je	.L933
	movq	64(%r14), %rax
	cmpq	32(%r14), %rax
	je	.L940
	cmpq	72(%r14), %rax
	je	.L1008
.L941:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L940
	subq	$56, 64(%rax)
.L940:
	movl	$72, %edi
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r13
	leaq	-64(%rbp), %r12
	call	_Znwm@PLT
	movl	$13, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r13, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC36(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r14), %rdi
	movb	$0, 24(%r15)
	movq	%r12, %rsi
	movq	$56, 64(%r15)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L942
	movq	(%rdi), %rax
	call	*8(%rax)
.L942:
	movq	64(%r14), %rax
	cmpq	32(%r14), %rax
	je	.L943
	movq	%rax, %rdx
	cmpq	72(%r14), %rax
	je	.L1009
.L944:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L943
	movq	8(%r14), %rdi
	leaq	.LC36(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%r14), %rax
.L943:
	leaq	16(%r14), %rcx
	movq	%r15, -64(%rbp)
	movq	%rcx, -88(%rbp)
	movq	80(%r14), %rcx
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L945
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%r14)
.L946:
	movq	88(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L947
	.p2align 4,,10
	.p2align 3
.L966:
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$4, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r13, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC37(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r14), %rdi
	movb	$0, 24(%r15)
	movq	%r12, %rsi
	movq	$40, 64(%r15)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L948
	movq	(%rdi), %rax
	call	*8(%rax)
.L948:
	movq	64(%r14), %rax
	cmpq	32(%r14), %rax
	je	.L949
	movq	%rax, %rdx
	cmpq	72(%r14), %rax
	je	.L1010
.L950:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L949
	movq	8(%r14), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%r14), %rax
.L949:
	movq	80(%r14), %rcx
	movq	%r15, -64(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L951
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%r14)
.L952:
	movq	16(%rbx), %r11
	testq	%r11, %r11
	movq	%r11, -72(%rbp)
	je	.L954
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$17, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r13, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC38(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%r14), %rdi
	movq	-72(%rbp), %r11
	movq	%r12, %rsi
	movb	$0, 24(%r15)
	movq	(%rdi), %rax
	movq	%r11, 64(%r15)
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L955
	movq	(%rdi), %rax
	call	*8(%rax)
.L955:
	movq	64(%r14), %rax
	cmpq	32(%r14), %rax
	je	.L954
	cmpq	72(%r14), %rax
	je	.L1011
.L957:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L954
	movq	8(%r14), %rdi
	leaq	.LC39(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L954:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L959
	movq	(%rax), %rsi
	movq	(%r14), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L959
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	movq	(%rdi), %rdx
	movq	%rdi, -72(%rbp)
	movq	16(%rdx), %r8
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	movq	%r8, -80(%rbp)
	call	*%rdx
	movq	64(%r14), %rcx
	cmpq	32(%r14), %rcx
	movq	-72(%rbp), %rdi
	movq	-80(%rbp), %r8
	movq	%rax, %rdx
	je	.L975
	cmpq	72(%r14), %rcx
	je	.L1012
.L962:
	movq	-8(%rcx), %rsi
.L961:
	leaq	.LC40(%rip), %rcx
	call	*%r8
.L959:
	movq	64(%r14), %rdi
	movq	72(%r14), %r8
	cmpq	%r8, %rdi
	je	.L963
	movq	(%rbx), %rbx
	subq	$8, %rdi
	movq	%rdi, 64(%r14)
	testq	%rbx, %rbx
	jne	.L966
.L967:
	cmpq	%r8, %rdi
	je	.L968
.L1014:
	subq	$8, %rdi
	movq	%rdi, 64(%r14)
.L933:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1013
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	movq	88(%r14), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L963:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	movq	88(%r14), %rax
	movq	-8(%rax), %r8
	subq	$8, %rax
	movq	%rax, %xmm2
	leaq	504(%r8), %rdi
	movq	%r8, %xmm1
	leaq	512(%r8), %rcx
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%r14)
	movq	%rcx, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%r14)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L966
	cmpq	%r8, %rdi
	jne	.L1014
	.p2align 4,,10
	.p2align 3
.L968:
	call	_ZdlPv@PLT
	movq	88(%r14), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm4
	leaq	504(%rax), %rbx
	movq	%rax, %xmm3
	addq	$512, %rax
	movq	%rbx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 64(%r14)
	movq	%rax, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 80(%r14)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	88(%r14), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L951:
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	88(%r14), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L975:
	xorl	%esi, %esi
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	88(%r14), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	88(%r14), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	88(%r14), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L945:
	movq	%r12, %rsi
	leaq	16(%r14), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L947:
	movq	64(%r14), %rdi
	movq	72(%r14), %r8
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L970:
	xorl	%esi, %esi
	jmp	.L937
.L1013:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4621:
	.size	_ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB9648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movl	(%rsi), %eax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r12
	movq	%rax, %r8
	divq	%rdi
	leaq	(%r12,%rdx,8), %r13
	movq	0(%r13), %r11
	testq	%r11, %r11
	je	.L1026
	movq	(%r11), %r14
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r14), %esi
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1033:
	testq	%rcx, %rcx
	je	.L1026
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L1026
	movq	%rcx, %r14
.L1018:
	movq	(%r14), %rcx
	cmpl	%esi, %r8d
	jne	.L1033
	cmpq	%r9, %r11
	je	.L1034
	testq	%rcx, %rcx
	je	.L1020
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L1020
	movq	%r9, (%r12,%rdx,8)
	movq	(%r14), %rcx
.L1020:
	movq	%rcx, (%r9)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1034:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L1027
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L1020
	movq	%r9, (%r12,%rdx,8)
	movq	0(%r13), %rax
.L1019:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1035
.L1021:
	movq	$0, 0(%r13)
	movq	(%r14), %rcx
	jmp	.L1020
.L1027:
	movq	%r9, %rax
	jmp	.L1019
.L1035:
	movq	%rcx, 16(%rbx)
	jmp	.L1021
	.cfi_endproc
.LFE9648:
	.size	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.section	.text._ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE
	.type	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE, @function
_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE:
.LFB9673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movslq	8(%rsi), %rax
	movq	%rsi, %rdi
	movq	8(%rbx), %r10
	movq	(%rbx), %r8
	divq	%r10
	leaq	(%r8,%rdx,8), %r9
	movq	%rdx, %rsi
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %rdi
	jne	.L1037
	movq	(%rdi), %r12
	cmpq	%rcx, %rax
	je	.L1048
	testq	%r12, %r12
	je	.L1040
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	divq	%r10
	cmpq	%rdx, %rsi
	je	.L1040
	movq	%rcx, (%r8,%rdx,8)
	movq	(%rdi), %r12
.L1040:
	movq	%r12, (%rcx)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1048:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1042
	movslq	8(%r12), %rax
	xorl	%edx, %edx
	divq	%r10
	cmpq	%rdx, %rsi
	je	.L1040
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r9), %rax
.L1039:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L1049
.L1041:
	movq	$0, (%r9)
	movq	(%rdi), %r12
	jmp	.L1040
.L1042:
	movq	%rcx, %rax
	jmp	.L1039
.L1049:
	movq	%r12, 16(%rbx)
	jmp	.L1041
	.cfi_endproc
.LFE9673:
	.size	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE, .-_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrapD2Ev
	.type	_ZN4node6loader10ModuleWrapD2Ev, @function
_ZN4node6loader10ModuleWrapD2Ev:
.LFB7803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node6loader10ModuleWrapE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	48(%r13), %r14
	movq	16(%r13), %rdi
	testq	%r14, %r14
	je	.L1051
	movq	352(%rdi), %rdi
	movq	(%r14), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r13), %rdi
	movq	%rax, %r14
.L1051:
	leaq	136(%r13), %rsi
	addq	$160, %rdi
	call	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	movq	16(%r13), %rbx
	movq	%r14, %rdi
	call	_ZNK2v86Module15GetIdentityHashEv@PLT
	leaq	-68(%rbp), %rsi
	leaq	104(%rbx), %rdi
	movl	%eax, -68(%rbp)
	call	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE11equal_rangeERS1_
	movq	%rax, %rsi
	cmpq	%rax, %rdx
	jne	.L1054
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	(%rsi), %rsi
	cmpq	%rsi, %rdx
	je	.L1052
.L1054:
	cmpq	%r13, 16(%rsi)
	jne	.L1053
	movq	16(%r13), %rax
	leaq	104(%rax), %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE5eraseENS8_20_Node_const_iteratorIS6_Lb0ELb0EEE
.L1052:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	128(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1055
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1055:
	movq	88(%r13), %r12
	testq	%r12, %r12
	jne	.L1056
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1095:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L1061
.L1062:
	movq	%rbx, %r12
.L1056:
	movq	40(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	je	.L1059
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1059:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L1095
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L1062
.L1061:
	movq	80(%r13), %rax
	movq	72(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	72(%r13), %rdi
	leaq	120(%r13), %rax
	movq	$0, 96(%r13)
	movq	$0, 88(%r13)
	cmpq	%rax, %rdi
	je	.L1057
	call	_ZdlPv@PLT
.L1057:
	movq	56(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1063
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1063:
	movq	48(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1064
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1064:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1065
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1065:
	movq	%r13, %rdi
	call	_ZN4node10BaseObjectD2Ev
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1096
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1096:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7803:
	.size	_ZN4node6loader10ModuleWrapD2Ev, .-_ZN4node6loader10ModuleWrapD2Ev
	.globl	_ZN4node6loader10ModuleWrapD1Ev
	.set	_ZN4node6loader10ModuleWrapD1Ev,_ZN4node6loader10ModuleWrapD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrapD0Ev
	.type	_ZN4node6loader10ModuleWrapD0Ev, @function
_ZN4node6loader10ModuleWrapD0Ev:
.LFB7805:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node6loader10ModuleWrapD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$144, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7805:
	.size	_ZN4node6loader10ModuleWrapD0Ev, .-_ZN4node6loader10ModuleWrapD0Ev
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node6loader10ModuleWrapD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1100
	call	_ZN4node6loader10ModuleWrapD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$144, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1100:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
.LFE7152:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB10083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1103
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1113
.L1129:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L1114:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1103:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1127
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1128
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L1106:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1108
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1111:
	testq	%rsi, %rsi
	je	.L1108
.L1109:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movl	8(%rcx), %eax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L1110
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L1116
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1109
	.p2align 4,,10
	.p2align 3
.L1108:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L1112
	call	_ZdlPv@PLT
.L1112:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L1129
.L1113:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1115
	movl	8(%rax), %eax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L1115:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	%rdx, %rdi
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1127:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L1106
.L1128:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10083:
	.size	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrapC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE
	.type	_ZN4node6loader10ModuleWrapC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE, @function
_ZN4node6loader10ModuleWrapC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE:
.LFB7796:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	movq	%rcx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1131
	movq	%rdx, %rsi
	movq	%rdx, %r15
	movq	%r8, %r14
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r15, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1190
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %r13
	movl	$40, %edi
	leaq	1(%r13), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	2592(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r15
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r13, 24(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r10
	testq	%r8, %r8
	je	.L1133
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1134:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1133
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1133
	movq	%rdi, %rax
.L1136:
	cmpq	%rcx, %r12
	jne	.L1134
	cmpq	%r11, 8(%rax)
	jne	.L1134
	cmpq	16(%rax), %r12
	jne	.L1134
	cmpq	$0, (%r8)
	je	.L1133
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1133:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	movq	%r10, -64(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L1137
	movq	2584(%rbx), %r9
	movq	-64(%rbp), %r10
	movq	%r12, 32(%r15)
	addq	%r9, %r10
	movq	(%r10), %rax
	testq	%rax, %rax
	je	.L1147
.L1194:
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	(%r10), %rax
	movq	%r15, (%rax)
.L1148:
	movq	-56(%rbp), %rsi
	leaq	16+_ZTVN4node6loader10ModuleWrapE(%rip), %rax
	addq	$1, 2656(%rbx)
	addq	$1, 2608(%rbx)
	movq	352(%rbx), %rdi
	movq	%rax, (%r12)
	movq	$0, 32(%r12)
	movb	$0, 40(%r12)
	testq	%rsi, %rsi
	je	.L1150
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	352(%rbx), %rdi
	movq	%rax, %rsi
.L1150:
	movq	%rsi, 48(%r12)
	movq	%r14, %rsi
	testq	%r14, %r14
	je	.L1151
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %rsi
.L1151:
	leaq	120(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%rsi, 56(%r12)
	movl	$24, %edi
	movq	%rax, 72(%r12)
	movups	%xmm0, 120(%r12)
	movl	1752(%rbx), %eax
	movq	$1, 80(%r12)
	leal	1(%rax), %edx
	movb	$0, 64(%r12)
	movq	$0, 88(%r12)
	movq	$0, 96(%r12)
	movl	$0x3f800000, 104(%r12)
	movq	$0, 112(%r12)
	movl	%eax, 136(%r12)
	movl	%edx, 1752(%rbx)
	call	_Znwm@PLT
	movl	136(%r12), %r10d
	movq	168(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	%r10d, 8(%rax)
	movq	%r10, %r11
	movq	%r12, 16(%rax)
	movq	%r10, %rax
	divq	%rsi
	movq	160(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1152
	movq	(%rax), %rcx
	movl	8(%rcx), %r8d
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1152
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L1152
.L1154:
	cmpl	%r11d, %r8d
	jne	.L1191
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1152:
	.cfi_restore_state
	addq	$40, %rsp
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r10, %rdx
	leaq	160(%rbx), %r11
	movq	%r9, %rsi
	popq	%rbx
	popq	%r12
	movq	%r11, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIjSt4pairIKjPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.p2align 4,,10
	.p2align 3
.L1137:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L1192
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1193
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -72(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r9
	leaq	2632(%rbx), %rax
	movq	%rax, %r13
.L1140:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L1142
	xorl	%r10d, %r10d
	leaq	2600(%rbx), %r11
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1145:
	testq	%rsi, %rsi
	je	.L1142
.L1143:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L1144
	movq	2600(%rbx), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L1156
	movq	%rcx, (%r9,%r10,8)
	movq	%rdx, %r10
	testq	%rsi, %rsi
	jne	.L1143
	.p2align 4,,10
	.p2align 3
.L1142:
	movq	2584(%rbx), %rdi
	cmpq	%r13, %rdi
	je	.L1146
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
.L1146:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r15)
	divq	%r8
	movq	%r8, 2592(%rbx)
	movq	%r9, 2584(%rbx)
	leaq	0(,%rdx,8), %r10
	addq	%r9, %r10
	movq	(%r10), %rax
	testq	%rax, %rax
	jne	.L1194
.L1147:
	movq	2600(%rbx), %rax
	movq	%r15, 2600(%rbx)
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.L1149
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r15, (%r9,%rdx,8)
.L1149:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r10)
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1156:
	movq	%rdx, %r10
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1131:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1190:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1192:
	leaq	2632(%rbx), %r9
	movq	$0, 2632(%rbx)
	movq	%r9, %r13
	jmp	.L1140
.L1193:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7796:
	.size	_ZN4node6loader10ModuleWrapC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE, .-_ZN4node6loader10ModuleWrapC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE
	.globl	_ZN4node6loader10ModuleWrapC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE
	.set	_ZN4node6loader10ModuleWrapC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE,_ZN4node6loader10ModuleWrapC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE
	.section	.text._ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	.type	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE, @function
_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE:
.LFB10729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	cmpq	$1, %rsi
	je	.L1223
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L1224
	leaq	0(,%rsi,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memset@PLT
	leaq	48(%r12), %rax
	movq	%rax, -64(%rbp)
.L1197:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L1200
	leaq	16(%r12), %rax
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rax, -56(%rbp)
	xorl	%r15d, %r15d
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	(%r8), %rax
	movl	%edi, %r14d
	movq	%rax, (%rsi)
	movq	%rsi, (%r8)
.L1203:
	movq	%rsi, %r8
	testq	%r10, %r10
	je	.L1201
.L1227:
	movq	%r10, %rsi
.L1199:
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	movq	%r9, %r11
	movq	(%rsi), %r10
	divq	%rbx
	testq	%r8, %r8
	setne	%dil
	cmpq	%r11, %rdx
	movq	%rdx, %rcx
	movq	%rdx, %r9
	sete	%al
	andb	%al, %dil
	jne	.L1225
	testb	%r14b, %r14b
	je	.L1204
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L1204
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%r11, %rdx
	je	.L1204
	movq	%r8, 0(%r13,%rdx,8)
.L1204:
	leaq	0(%r13,%rcx,8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1226
	movq	(%rdx), %rdx
	xorl	%r14d, %r14d
	movq	%rsi, %r8
	movq	%rdx, (%rsi)
	movq	(%rax), %rax
	movq	%rsi, (%rax)
	testq	%r10, %r10
	jne	.L1227
.L1201:
	testb	%dil, %dil
	je	.L1200
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L1200
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%rdx, %rcx
	je	.L1200
	movq	%rsi, 0(%r13,%rdx,8)
.L1200:
	movq	(%r12), %rdi
	cmpq	-64(%rbp), %rdi
	je	.L1207
	call	_ZdlPv@PLT
.L1207:
	movq	%rbx, 8(%r12)
	movq	%r13, (%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	.cfi_restore_state
	movq	16(%r12), %rdx
	movq	%rdx, (%rsi)
	movq	-56(%rbp), %rdx
	movq	%rsi, 16(%r12)
	movq	%rdx, (%rax)
	cmpq	$0, (%rsi)
	je	.L1209
	movq	%rsi, 0(%r13,%r15,8)
	xorl	%r14d, %r14d
	movq	%rcx, %r15
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1209:
	xorl	%r14d, %r14d
	movq	%rcx, %r15
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1223:
	leaq	48(%rdi), %r13
	movq	$0, 48(%rdi)
	movq	%r13, -64(%rbp)
	jmp	.L1197
.L1224:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10729:
	.size	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE, .-_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"ERR_VM_MODULE_CACHED_DATA_REJECTED"
	.align 8
.LC44:
	.string	"cachedData buffer was rejected"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1229
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1387
.L1229:
	movl	16(%r15), %eax
	cmpl	$2, %eax
	jle	.L1388
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rdx
	movzwl	11(%rdx), %edx
	movl	%edx, %esi
	subl	$1056, %edx
	cmpl	$1, %edx
	jbe	.L1340
	cmpw	$1040, %si
	jne	.L1231
.L1340:
	movq	23(%rcx), %rbx
	movq	8(%r15), %rcx
	movq	%rcx, -200(%rbp)
	leaq	8(%rcx), %rsi
	movq	352(%rbx), %r13
	movq	%rcx, -208(%rbp)
	movq	%rsi, -216(%rbp)
	movq	%rcx, %rsi
.L1233:
	movq	(%rsi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1234
.L1398:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1234
	movq	-208(%rbp), %rsi
	leaq	-8(%rsi), %rdi
	cmpl	$1, %eax
	jg	.L1237
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1237:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1238
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1389
.L1238:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1390
	cmpl	$1, 16(%r15)
	jg	.L1241
	movq	(%r15), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L1242:
	leaq	-128(%rbp), %r8
	movq	%rbx, %rdi
	movq	%rax, -128(%rbp)
	movq	%r8, %rsi
	call	_ZN4node10contextify17ContextifyContext30ContextFromContextifiedSandboxEPNS_11EnvironmentERKN2v85LocalINS4_6ObjectEEE@PLT
	testq	%rax, %rax
	je	.L1391
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L1239
	movzbl	11(%r12), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L1392
.L1239:
	cmpl	$2, 16(%r15)
	jg	.L1245
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1246:
	call	_ZNK2v85Value7IsArrayEv@PLT
	movb	%al, -224(%rbp)
	testb	%al, %al
	je	.L1247
	cmpl	$3, 16(%r15)
	jle	.L1393
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
.L1249:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	$0, -256(%rbp)
	movq	$0, -248(%rbp)
	testb	%al, %al
	je	.L1394
.L1250:
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN2v814PrimitiveArray3NewEPNS_7IsolateEi@PLT
	movsd	.LC42(%rip), %xmm0
	movq	%r13, %rdi
	movq	%rax, %r14
	movq	%rax, -240(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v814PrimitiveArray3SetEPNS_7IsolateEiNS_5LocalINS_9PrimitiveEEE@PLT
	addl	$1, 1808(%rbx)
	leaq	-192(%rbp), %rax
	movq	352(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -232(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rbx, -144(%rbp)
	movl	$0, -136(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	cmpb	$0, -224(%rbp)
	je	.L1264
	cmpl	$2, 16(%r15)
	jg	.L1265
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1266:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L1395
	cmpl	$2, 16(%r15)
	jg	.L1268
	movq	(%r15), %rax
	movq	8(%rax), %r14
	leaq	88(%r14), %rax
	movq	%rax, -248(%rbp)
	movq	%rax, %rdi
.L1269:
	call	_ZNK2v85Array6LengthEv@PLT
	pxor	%xmm0, %xmm0
	movq	$0, -112(%rbp)
	movl	%eax, %esi
	movaps	%xmm0, -128(%rbp)
	movq	%rsi, %r14
	testq	%rsi, %rsi
	je	.L1270
	leaq	0(,%rsi,8), %rdi
	movq	%rsi, -264(%rbp)
	movq	%rdi, -256(%rbp)
	call	_Znwm@PLT
	movq	-256(%rbp), %rdi
	movq	%rax, -128(%rbp)
	addq	%rax, %rdi
	movq	%rdi, -112(%rbp)
	cmpl	$1, %r14d
	je	.L1271
	movq	-264(%rbp), %rsi
	movq	%rax, %rdx
	pxor	%xmm0, %xmm0
	movq	%rsi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L1272:
	movups	%xmm0, (%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1272
	movq	%rsi, %rdx
	andq	$-2, %rdx
	leaq	(%rax,%rdx,8), %rax
	cmpq	%rdx, %rsi
	je	.L1273
.L1271:
	movq	$0, (%rax)
.L1273:
	movq	%rdi, -120(%rbp)
	leal	-1(%r14), %eax
	xorl	%r14d, %r14d
	movq	%rbx, -256(%rbp)
	movq	-248(%rbp), %rbx
	movq	%r13, -264(%rbp)
	movq	%r14, %r13
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L1277:
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	testq	%rax, %rax
	je	.L1396
.L1274:
	movq	(%rax), %rdx
	movq	%rdx, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	je	.L1397
.L1275:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1387:
	cmpl	$5, 43(%rax)
	jne	.L1229
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1231:
	xorl	%esi, %esi
	addq	$32, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	8(%r15), %rcx
	movq	%rax, %rbx
	movl	16(%r15), %eax
	leaq	8(%rcx), %rsi
	movq	%rcx, -208(%rbp)
	movq	352(%rbx), %r13
	movq	%rsi, -216(%rbp)
	testl	%eax, %eax
	jg	.L1336
	movq	(%r15), %rdx
	movq	8(%rdx), %rdx
	leaq	88(%rdx), %rsi
	movq	(%rsi), %rdx
	movq	%rsi, -200(%rbp)
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1398
.L1234:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1241:
	movq	8(%r15), %rax
	subq	$8, %rax
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1247:
	movl	16(%r15), %edx
	cmpl	$2, %edx
	jle	.L1399
	movq	8(%r15), %rax
	subq	$16, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1400
.L1253:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1245:
	movq	8(%r15), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1389:
	cmpl	$5, 43(%rax)
	jne	.L1238
	movq	-216(%rbp), %rdi
	call	_ZN2v86Object15CreationContextEv@PLT
	movq	%rax, %r12
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	(%r15), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1253
.L1400:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1253
	cmpl	$3, %edx
	jg	.L1255
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1256:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1401
	movl	16(%r15), %eax
	cmpl	$3, %eax
	jg	.L1258
	movq	(%r15), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -248(%rbp)
	movq	%rax, %rdi
.L1261:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1402
	cmpl	$4, 16(%r15)
	jg	.L1263
	movq	(%r15), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -256(%rbp)
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1397:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L1275
	movq	-128(%rbp), %rdx
	movq	%rax, (%rdx,%r13,8)
	leaq	1(%r13), %rax
	cmpq	%r14, %r13
	je	.L1403
	movq	%rax, %r13
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1264:
	movl	16(%r15), %eax
	cmpl	$5, %eax
	jg	.L1280
	movq	(%r15), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1281:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1282
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1282
	cmpl	$5, 43(%rdx)
	je	.L1339
	.p2align 4,,10
	.p2align 3
.L1282:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1404
	cmpl	$5, 16(%r15)
	jg	.L1285
	movq	(%r15), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r9
.L1286:
	movq	%r9, %rdi
	movq	%r9, -264(%rbp)
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-128(%rbp), %r8
	movq	%r8, %rdi
	movq	%rax, %rsi
	movq	%r8, -280(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-264(%rbp), %r9
	movq	-128(%rbp), %r14
	movq	%r9, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	-264(%rbp), %r9
	addq	%rax, %r14
	movq	%r9, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movl	$24, %edi
	movq	%rax, -272(%rbp)
	call	_Znwm@PLT
	movq	-272(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v814ScriptCompiler10CachedDataC1EPKhiNS1_12BufferPolicyE@PLT
	movl	16(%r15), %eax
	movl	$1, %edx
	movq	-280(%rbp), %r8
.L1283:
	cmpl	$2, %eax
	jg	.L1287
	movq	(%r15), %rax
	movq	8(%rax), %r14
	leaq	88(%r14), %rax
	movq	%rax, -272(%rbp)
.L1288:
	movq	%r8, -296(%rbp)
	leaq	120(%r13), %r14
	movl	%edx, -284(%rbp)
	cmpq	$-112, %r13
	je	.L1289
	leaq	112(%r13), %r11
	movq	%r11, %rdi
	movq	%r11, -280(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpq	$-120, %r13
	movq	-280(%rbp), %r11
	movl	-284(%rbp), %edx
	movq	-296(%rbp), %r8
	jne	.L1290
	movq	%r11, %rdi
	movl	%edx, -280(%rbp)
	movb	%al, -284(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	-280(%rbp), %edx
	movzbl	-284(%rbp), %ecx
	movq	-296(%rbp), %r8
	movzbl	%al, %eax
.L1291:
	movl	%eax, %esi
	movq	%r13, %rdi
	movq	-272(%rbp), %xmm0
	movq	$0, -88(%rbp)
	orl	$8, %esi
	testb	%cl, %cl
	cmovne	%esi, %eax
	movhps	-200(%rbp), %xmm0
	xorl	%ecx, %ecx
	movq	%r8, %rsi
	movaps	%xmm0, -128(%rbp)
	movq	-248(%rbp), %xmm0
	movl	%eax, -96(%rbp)
	movq	-240(%rbp), %rax
	movhps	-256(%rbp), %xmm0
	movq	%rax, -80(%rbp)
	movq	-264(%rbp), %rax
	movaps	%xmm0, -112(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN2v814ScriptCompiler13CompileModuleEPNS_7IsolateEPNS0_6SourceENS0_14CompileOptionsENS0_13NoCacheReasonE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1405
	cmpq	$0, -264(%rbp)
	movq	-72(%rbp), %rdi
	jne	.L1406
	testq	%rdi, %rdi
	jne	.L1301
.L1279:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	360(%rbx), %rax
	movq	-200(%rbp), %rcx
	movq	%r12, %rsi
	movq	-216(%rbp), %rdi
	movq	1848(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1309
	shrw	$8, %ax
	je	.L1309
	movl	$144, %edi
	call	_Znwm@PLT
	movq	-200(%rbp), %r8
	movq	%r14, %rcx
	movq	%rbx, %rsi
	movq	-216(%rbp), %rdx
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN4node6loader10ModuleWrapC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEENS5_INS4_6ModuleEEENS5_INS4_6StringEEE
	cmpb	$0, -224(%rbp)
	je	.L1312
	movq	-200(%rbp), %rax
	cmpl	$3, 16(%r15)
	movq	352(%rbx), %r8
	movb	$1, 40(%rax)
	movq	32(%rax), %rdi
	jg	.L1313
	movq	(%r15), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	testq	%rdi, %rdi
	je	.L1314
.L1315:
	movq	%rsi, -248(%rbp)
	movq	%r8, -224(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-200(%rbp), %rax
	movq	-248(%rbp), %rsi
	movq	-224(%rbp), %r8
	movq	$0, 32(%rax)
.L1314:
	testq	%rsi, %rsi
	je	.L1312
.L1316:
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, 32(%rsi)
.L1312:
	movq	-200(%rbp), %rax
	movq	128(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1318
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-200(%rbp), %rax
	movq	$0, 128(%rax)
.L1318:
	testq	%r12, %r12
	je	.L1319
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-200(%rbp), %rsi
	movq	%rax, 128(%rsi)
.L1319:
	movq	%r14, %rdi
	call	_ZNK2v86Module15GetIdentityHashEv@PLT
	movl	$24, %edi
	movslq	%eax, %r14
	call	_Znwm@PLT
	movq	128(%rbx), %rdx
	movq	112(%rbx), %rsi
	leaq	136(%rbx), %rdi
	movq	%rax, %r9
	movl	%r14d, 8(%rax)
	movl	$1, %ecx
	movq	$0, (%rax)
	movq	-200(%rbp), %rax
	movq	%r9, -224(%rbp)
	movq	%rax, 16(%r9)
	leaq	104(%rbx), %rax
	movq	%rax, -248(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	-224(%rbp), %r9
	testb	%al, %al
	jne	.L1407
.L1320:
	movq	112(%rbx), %rdi
	movq	%r14, %rax
	xorl	%edx, %edx
	movq	104(%rbx), %rcx
	divq	%rdi
	leaq	(%rcx,%rdx,8), %rax
	movq	%rdx, %r10
	movq	(%rax), %r8
	movq	%rax, -224(%rbp)
	testq	%r8, %r8
	je	.L1321
	movq	(%r8), %r14
	movl	8(%r9), %r11d
	movl	8(%r14), %esi
	movq	%r14, %rax
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1323
	movl	8(%rcx), %esi
	movq	%rax, %r8
	xorl	%edx, %edx
	movslq	%esi, %rax
	divq	%rdi
	cmpq	%rdx, %r10
	jne	.L1323
	movq	%rcx, %rax
.L1324:
	cmpl	%r11d, %esi
	jne	.L1408
	movq	%rax, (%r9)
	movq	%r9, (%r8)
.L1328:
	movq	-200(%rbp), %rax
	pxor	%xmm0, %xmm0
	addq	$1, 128(%rbx)
	movq	%r13, %rdi
	movl	136(%rax), %eax
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-240(%rbp), %rdi
	movl	$9, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	call	_ZN2v814PrimitiveArray3SetEPNS_7IsolateEiNS_5LocalINS_9PrimitiveEEE@PLT
	movq	-216(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v86Object17SetIntegrityLevelENS_5LocalINS_7ContextEEENS_14IntegrityLevelE@PLT
	movq	-208(%rbp), %rax
	movq	-232(%rbp), %rdi
	movq	8(%rax), %rdx
	movq	(%r15), %rax
	movq	%rdx, 24(%rax)
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	subl	$1, 1808(%rbx)
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1406:
	cmpb	$0, 12(%rdi)
	je	.L1301
	movq	352(%rbx), %r13
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC43(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1409
.L1303:
	movq	%r13, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC44(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1410
.L1304:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1411
.L1305:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1412
.L1306:
	movq	%r13, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-200(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1413
.L1307:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-232(%rbp), %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
.L1299:
	movq	-72(%rbp), %r13
	testq	%r13, %r13
	je	.L1308
	movq	%r13, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1308:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
.L1309:
	movq	-232(%rbp), %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	subl	$1, 1808(%rbx)
.L1228:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1414
	addq	$280, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1392:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	(%r12), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	%rax, -248(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-248(%rbp), %rax
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1265:
	movq	8(%r15), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1280:
	movq	8(%r15), %rsi
	leaq	-40(%rsi), %rdi
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	8(%r15), %rdi
	leaq	-24(%rdi), %rsi
	movq	%rsi, -248(%rbp)
	cmpl	$4, %eax
	je	.L1415
	subq	$32, %rdi
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1287:
	movq	8(%r15), %rax
	subq	$16, %rax
	movq	%rax, -272(%rbp)
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1268:
	movq	8(%r15), %rax
	subq	$16, %rax
	movq	%rax, -248(%rbp)
	movq	%rax, %rdi
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	8(%r15), %rax
	subq	$32, %rax
	movq	%rax, -256(%rbp)
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	%r14, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	%r14, %rdi
	movb	%al, -280(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	-280(%rbp), %r10d
	movl	-284(%rbp), %edx
	xorl	%ecx, %ecx
	movq	-296(%rbp), %r8
	movl	%eax, %esi
	xorl	%eax, %eax
.L1334:
	movl	%eax, %edi
	orl	$4, %edi
	testb	%r10b, %r10b
	cmovne	%edi, %eax
	movl	%eax, %edi
	orl	$2, %edi
	testb	%sil, %sil
	cmovne	%edi, %eax
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	8(%r15), %rax
	leaq	-40(%rax), %r9
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1301:
	movq	%rdi, -248(%rbp)
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movq	-248(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1388:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	-256(%rbp), %rbx
	movq	-264(%rbp), %r13
.L1270:
	movq	-200(%rbp), %rsi
	movq	%r13, %rdi
	leaq	-128(%rbp), %rdx
	leaq	_ZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEE(%rip), %rcx
	call	_ZN2v86Module21CreateSyntheticModuleEPNS_7IsolateENS_5LocalINS_6StringEEERKSt6vectorIS5_SaIS5_EEPFNS_10MaybeLocalINS_5ValueEEENS3_INS_7ContextEEENS3_IS0_EEE@PLT
	movq	-128(%rbp), %rdi
	movq	%rax, %r14
	testq	%rdi, %rdi
	je	.L1279
	call	_ZdlPv@PLT
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1390:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1391:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	$0, -264(%rbp)
	xorl	%edx, %edx
	leaq	-128(%rbp), %r8
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	%rdx, %rsi
	leaq	104(%rbx), %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiPN4node6loader10ModuleWrapEESaIS6_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb0EEEE13_M_rehash_auxEmSt17integral_constantIbLb0EE
	movq	-224(%rbp), %r9
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	-232(%rbp), %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1299
	movq	-232(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L1299
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	testq	%rax, %rax
	je	.L1416
	movq	-232(%rbp), %rdi
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	testq	%rax, %rax
	je	.L1417
	movq	-232(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNK2v88TryCatch7MessageEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZNK2v88TryCatch9ExceptionEv@PLT
	movq	%rbx, %rdi
	movl	$2, %ecx
	movq	%r13, %rdx
	movq	%rax, %rsi
	call	_ZN4node19AppendExceptionLineEPNS_11EnvironmentEN2v85LocalINS2_5ValueEEENS3_INS2_7MessageEEENS_17ErrorHandlingModeE@PLT
	movq	%r14, %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1394:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1401:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1395:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1402:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1404:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1313:
	movq	8(%r15), %rax
	leaq	-24(%rax), %rsi
	testq	%rdi, %rdi
	jne	.L1315
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1290:
	movq	%r14, %rdi
	movq	%r8, -312(%rbp)
	movl	%edx, -288(%rbp)
	movq	%r11, -296(%rbp)
	movb	%al, -297(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	%r14, %rdi
	movb	%al, -284(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	-296(%rbp), %r11
	movb	%al, -280(%rbp)
	movq	%r11, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	-280(%rbp), %esi
	movzbl	-284(%rbp), %r10d
	movl	-288(%rbp), %edx
	movzbl	-297(%rbp), %ecx
	movzbl	%al, %eax
	movq	-312(%rbp), %r8
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	-224(%rbp), %rax
	movq	%r14, (%r9)
	movq	(%rax), %rax
	movq	%r9, (%rax)
	jmp	.L1328
.L1321:
	movq	120(%rbx), %rax
	movq	%rax, (%r9)
	movq	%r9, 120(%rbx)
	movq	(%r9), %rax
	testq	%rax, %rax
	je	.L1333
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	%r9, (%rcx,%rdx,8)
.L1333:
	movq	-224(%rbp), %rsi
	leaq	120(%rbx), %rax
	movq	%rax, (%rsi)
	jmp	.L1328
.L1409:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1303
.L1413:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1307
.L1412:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdx
	jmp	.L1306
.L1411:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1305
.L1410:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L1304
.L1417:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1416:
	leaq	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1414:
	call	__stack_chk_fail@PLT
.L1336:
	movq	-208(%rbp), %rsi
	movq	%rsi, -200(%rbp)
	jmp	.L1233
.L1415:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1261
	.cfi_endproc
.LFE7808:
	.size	_ZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"Script execution timed out after "
	.section	.rodata.str1.1
.LC46:
	.string	"ms"
.LC47:
	.string	"ERR_SCRIPT_EXECUTION_TIMEOUT"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"ERR_SCRIPT_EXECUTION_INTERRUPTED"
	.align 8
.LC49:
	.string	"Script execution was interrupted by `SIGINT`"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7815:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1384, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1474
	cmpw	$1040, %cx
	jne	.L1419
.L1474:
	movq	23(%rdx), %r14
.L1421:
	movq	8(%rbx), %r12
	movq	352(%r14), %r15
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1494
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1475
	cmpw	$1040, %cx
	jne	.L1423
.L1475:
	movq	23(%rdx), %r13
.L1425:
	testq	%r13, %r13
	je	.L1418
	movq	128(%r13), %r12
	testq	%r12, %r12
	je	.L1427
	movq	(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
.L1427:
	movq	48(%r13), %r13
	testq	%r13, %r13
	je	.L1428
	movq	0(%r13), %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
.L1428:
	cmpl	$2, 16(%rbx)
	je	.L1495
	leaq	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1495:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1496
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L1497
	movq	8(%rbx), %rdi
.L1431:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, -1376(%rbp)
	testb	%al, %al
	je	.L1498
.L1432:
	cmpl	$1, 16(%rbx)
	jg	.L1433
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1434:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1499
	cmpl	$1, 16(%rbx)
	jg	.L1436
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1437:
	call	_ZNK2v85Value6IsTrueEv@PLT
	addl	$1, 1808(%r14)
	movq	352(%r14), %rsi
	movb	%al, -1384(%rbp)
	leaq	-1312(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -1368(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-1376(%rbp), %rdx
	movq	%r14, -1264(%rbp)
	movb	$0, -1346(%rbp)
	movzbl	-1384(%rbp), %eax
	cmpq	$-1, %rdx
	movb	$0, -1345(%rbp)
	movl	$0, -1256(%rbp)
	je	.L1438
	testb	%al, %al
	jne	.L1500
.L1440:
	movq	-1376(%rbp), %rdx
	cmpq	$-1, %rdx
	je	.L1441
	leaq	-1216(%rbp), %r8
	leaq	-1346(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -1384(%rbp)
	call	_ZN4node8WatchdogC1EPN2v87IsolateEmPb@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v86Module8EvaluateENS_5LocalINS_7ContextEEE@PLT
	movq	-1384(%rbp), %r8
	movq	%rax, %r15
	movq	%r8, %rdi
	call	_ZN4node8WatchdogD1Ev@PLT
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1500:
	leaq	-1216(%rbp), %r8
	leaq	-1346(%rbp), %rcx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -1408(%rbp)
	call	_ZN4node8WatchdogC1EPN2v87IsolateEmPb@PLT
	leaq	-1344(%rbp), %r9
	leaq	-1345(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r9, %rdi
	movq	%r9, -1384(%rbp)
	call	_ZN4node14SigintWatchdogC1EPN2v87IsolateEPb@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Module8EvaluateENS_5LocalINS_7ContextEEE@PLT
	movq	-1384(%rbp), %r9
	movq	%rax, %r15
	movq	%r9, %rdi
	call	_ZN4node14SigintWatchdogD1Ev@PLT
	movq	-1408(%rbp), %r8
	movq	%r8, %rdi
	call	_ZN4node8WatchdogD1Ev@PLT
.L1439:
	testq	%r15, %r15
	je	.L1501
.L1442:
	cmpb	$0, -1346(%rbp)
	jne	.L1443
	cmpb	$0, -1345(%rbp)
	je	.L1444
.L1443:
	testb	$1, 1932(%r14)
	jne	.L1449
	movzbl	2664(%r14), %eax
	testb	%al, %al
	jne	.L1467
.L1449:
	movq	352(%r14), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	cmpb	$0, -1346(%rbp)
	jne	.L1502
	cmpb	$0, -1345(%rbp)
	jne	.L1503
.L1444:
	movq	-1368(%rbp), %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1504
	movq	(%rbx), %rbx
	testq	%r15, %r15
	je	.L1505
	movq	(%r15), %rax
	movq	%rax, 24(%rbx)
.L1467:
	movq	-1368(%rbp), %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	subl	$1, 1808(%r14)
.L1418:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1506
	addq	$1384, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1437
	.p2align 4,,10
	.p2align 3
.L1419:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1425
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Module8EvaluateENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1438:
	testb	%al, %al
	je	.L1440
	leaq	-1344(%rbp), %r8
	leaq	-1345(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r8, %rdi
	movq	%r8, -1384(%rbp)
	call	_ZN4node14SigintWatchdogC1EPN2v87IsolateEPb@PLT
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v86Module8EvaluateENS_5LocalINS_7ContextEEE@PLT
	movq	-1384(%rbp), %r8
	movq	%rax, %r15
	movq	%r8, %rdi
	call	_ZN4node14SigintWatchdogD1Ev@PLT
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	.LC50(%rip), %xmm1
	leaq	-1104(%rbp), %r13
	leaq	-1216(%rbp), %r12
	movq	%r13, %rdi
	movq	%r13, -1384(%rbp)
	movhps	.LC51(%rip), %xmm1
	movaps	%xmm1, -1408(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -1104(%rbp)
	xorl	%eax, %eax
	movw	%ax, -880(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -872(%rbp)
	movups	%xmm0, -856(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -1216(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -888(%rbp)
	addq	%r12, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1408(%rbp), %xmm1
	movq	%rax, -1104(%rbp)
	leaq	-1152(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -1216(%rbp)
	movaps	%xmm0, -1200(%rbp)
	movaps	%xmm0, -1184(%rbp)
	movaps	%xmm0, -1168(%rbp)
	movq	%rax, -1392(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r13, %rdi
	leaq	-1208(%rbp), %rsi
	movq	%rax, -1208(%rbp)
	leaq	-1120(%rbp), %rax
	movq	%rax, -1416(%rbp)
	movq	%rax, -1136(%rbp)
	movl	$16, -1144(%rbp)
	movq	$0, -1128(%rbp)
	movb	$0, -1120(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$33, %edx
	leaq	.LC45(%rip), %rsi
	movq	%r12, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-1376(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC46(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1232(%rbp), %rax
	movq	$0, -1240(%rbp)
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1408(%rbp)
	movq	%rax, -1248(%rbp)
	movq	-1168(%rbp), %rax
	movb	$0, -1232(%rbp)
	testq	%rax, %rax
	je	.L1450
	movq	-1184(%rbp), %r8
	movq	-1176(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L1507
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1452:
	movq	352(%r14), %r13
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rsi
	movq	-1248(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -1376(%rbp)
	testq	%rax, %rax
	je	.L1508
.L1453:
	movq	%r13, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1509
.L1454:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1510
.L1455:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1511
.L1456:
	movq	%r13, %rdi
	movq	%rdx, -1424(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1376(%rbp), %rcx
	movq	-1424(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1512
.L1457:
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-1248(%rbp), %rdi
	cmpq	-1408(%rbp), %rdi
	je	.L1458
	call	_ZdlPv@PLT
.L1458:
	movq	.LC50(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-1136(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	movhps	.LC52(%rip), %xmm0
	movaps	%xmm0, -1216(%rbp)
	cmpq	-1416(%rbp), %rdi
	je	.L1459
	call	_ZdlPv@PLT
.L1459:
	movq	-1392(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1208(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	-1384(%rbp), %rdi
	movq	%rax, -1216(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1216(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1104(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1494:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	-1368(%rbp), %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1442
	leaq	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	-1368(%rbp), %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L1467
	movq	-1368(%rbp), %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -1376(%rbp)
	testq	%rax, %rax
	je	.L1513
.L1461:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC49(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1514
.L1462:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1515
.L1463:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1516
.L1464:
	movq	%r12, %rdi
	movq	%rdx, -1384(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1376(%rbp), %rcx
	movq	-1384(%rbp), %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1517
.L1465:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1496:
	leaq	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1499:
	leaq	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1498:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1432
	.p2align 4,,10
	.p2align 3
.L1507:
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1505:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1450:
	leaq	-1136(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1452
	.p2align 4,,10
	.p2align 3
.L1508:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1453
	.p2align 4,,10
	.p2align 3
.L1512:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	%rax, -1424(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1424(%rbp), %rdx
	jmp	.L1456
	.p2align 4,,10
	.p2align 3
.L1510:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1509:
	movq	%rax, -1424(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1424(%rbp), %rdi
	jmp	.L1454
.L1514:
	movq	%rax, -1384(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1384(%rbp), %rdi
	jmp	.L1462
.L1513:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1461
.L1517:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1465
.L1516:
	movq	%rax, -1384(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1384(%rbp), %rdx
	jmp	.L1464
.L1515:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1463
.L1506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7815:
	.size	_ZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node6loader10ModuleWrapE
	.section	.data.rel.ro._ZTVN4node6loader10ModuleWrapE,"awG",@progbits,_ZTVN4node6loader10ModuleWrapE,comdat
	.align 8
	.type	_ZTVN4node6loader10ModuleWrapE, @object
	.size	_ZTVN4node6loader10ModuleWrapE, 88
_ZTVN4node6loader10ModuleWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node6loader10ModuleWrapD1Ev
	.quad	_ZN4node6loader10ModuleWrapD0Ev
	.quad	_ZNK4node6loader10ModuleWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6loader10ModuleWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6loader10ModuleWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC53:
	.string	"../src/util-inl.h:374"
.LC54:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC56:
	.string	"../src/util-inl.h:325"
.LC57:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args
	.section	.rodata.str1.1
.LC59:
	.string	"../src/util.h:352"
.LC60:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 16; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.section	.rodata.str1.1
.LC62:
	.string	"../src/module_wrap.cc"
.LC63:
	.string	"module_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC62
	.quad	0
	.quad	_ZN4node6loader10ModuleWrap10InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC63
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC64:
	.string	"../src/module_wrap.cc:665"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"(module->GetStatus()) < (v8::Module::Status::kEvaluating)"
	.align 8
.LC66:
	.string	"static void node::loader::ModuleWrap::CreateCachedData(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.section	.rodata.str1.1
.LC67:
	.string	"../src/module_wrap.cc:661"
.LC68:
	.string	"!obj->synthetic_"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC66
	.section	.rodata.str1.1
.LC69:
	.string	"../src/module_wrap.cc:645"
.LC70:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"static void node::loader::ModuleWrap::SetSyntheticExport(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.section	.rodata.str1.1
.LC72:
	.string	"../src/module_wrap.cc:643"
.LC73:
	.string	"(args.Length()) == (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC71
	.section	.rodata.str1.1
.LC74:
	.string	"../src/module_wrap.cc:641"
.LC75:
	.string	"obj->synthetic_"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap18SetSyntheticExportERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC71
	.section	.rodata.str1.1
.LC76:
	.string	"../src/module_wrap.cc:627"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"!try_catch.Exception().IsEmpty()"
	.align 8
.LC78:
	.string	"static v8::MaybeLocal<v8::Value> node::loader::ModuleWrap::SyntheticModuleEvaluationStepsCallback(v8::Local<v8::Context>, v8::Local<v8::Module>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_1, @object
	.size	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_1, 24
_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_1:
	.quad	.LC76
	.quad	.LC77
	.quad	.LC78
	.section	.rodata.str1.1
.LC79:
	.string	"../src/module_wrap.cc:626"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"!try_catch.Message().IsEmpty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args_0:
	.quad	.LC79
	.quad	.LC80
	.quad	.LC78
	.section	.rodata.str1.1
.LC81:
	.string	"../src/module_wrap.cc:622"
.LC82:
	.string	"try_catch.HasCaught()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args, 24
_ZZN4node6loader10ModuleWrap38SyntheticModuleEvaluationStepsCallbackEN2v85LocalINS2_7ContextEEENS3_INS2_6ModuleEEEE4args:
	.quad	.LC81
	.quad	.LC82
	.quad	.LC78
	.section	.rodata.str1.1
.LC83:
	.string	"../src/module_wrap.cc:601"
.LC84:
	.string	"args[0]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"static void node::loader::ModuleWrap::SetInitializeImportMetaObjectCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC83
	.quad	.LC84
	.quad	.LC85
	.section	.rodata.str1.1
.LC86:
	.string	"../src/module_wrap.cc:600"
.LC87:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap37SetInitializeImportMetaObjectCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC86
	.quad	.LC87
	.quad	.LC85
	.section	.rodata.str1.1
.LC88:
	.string	"../src/module_wrap.cc:565"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"static void node::loader::ModuleWrap::SetImportModuleDynamicallyCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC88
	.quad	.LC84
	.quad	.LC89
	.section	.rodata.str1.1
.LC90:
	.string	"../src/module_wrap.cc:564"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap34SetImportModuleDynamicallyCallbackERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC90
	.quad	.LC87
	.quad	.LC89
	.section	.rodata.str1.1
.LC91:
	.string	"../src/module_wrap.cc:551"
.LC92:
	.string	"result->IsPromise()"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"v8::MaybeLocal<v8::Promise> node::loader::ImportModuleDynamically(v8::Local<v8::Context>, v8::Local<v8::ScriptOrModule>, v8::Local<v8::String>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_1, @object
	.size	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_1, 24
_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_1:
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.section	.rodata.str1.1
.LC94:
	.string	"../src/module_wrap.cc:537"
.LC95:
	.string	"\"Unreachable code reached\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_0, @object
	.size	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_0, 24
_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args_0:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC93
	.section	.rodata.str1.1
.LC96:
	.string	"../src/module_wrap.cc:534"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"(it) != (env->id_to_function_map.end())"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args, @object
	.size	_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args, 24
_ZZN4node6loaderL23ImportModuleDynamicallyEN2v85LocalINS1_7ContextEEENS2_INS1_14ScriptOrModuleEEENS2_INS1_6StringEEEE4args:
	.quad	.LC96
	.quad	.LC97
	.quad	.LC93
	.section	.rodata.str1.1
.LC98:
	.string	"../src/module_wrap.cc:354"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"static void node::loader::ModuleWrap::Evaluate(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC98
	.quad	.LC82
	.quad	.LC99
	.section	.rodata.str1.1
.LC100:
	.string	"../src/module_wrap.cc:330"
.LC101:
	.string	"args[1]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC100
	.quad	.LC101
	.quad	.LC99
	.section	.rodata.str1.1
.LC102:
	.string	"../src/module_wrap.cc:327"
.LC103:
	.string	"args[0]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC102
	.quad	.LC103
	.quad	.LC99
	.section	.rodata.str1.1
.LC104:
	.string	"../src/module_wrap.cc:325"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap8EvaluateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC104
	.quad	.LC73
	.quad	.LC99
	.section	.rodata.str1.1
.LC105:
	.string	"../src/module_wrap.cc:308"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"static void node::loader::ModuleWrap::Instantiate(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC105
	.quad	.LC77
	.quad	.LC106
	.section	.rodata.str1.1
.LC107:
	.string	"../src/module_wrap.cc:307"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap11InstantiateERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC107
	.quad	.LC80
	.quad	.LC106
	.section	.rodata.str1.1
.LC108:
	.string	"../src/module_wrap.cc:244"
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"static void node::loader::ModuleWrap::Link(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC108
	.quad	.LC84
	.quad	.LC109
	.section	.rodata.str1.1
.LC110:
	.string	"../src/module_wrap.cc:243"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap4LinkERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC110
	.quad	.LC87
	.quad	.LC109
	.section	.rodata.str1.1
.LC111:
	.string	"../src/module_wrap.cc:199"
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"static void node::loader::ModuleWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__12_:
	.quad	.LC111
	.quad	.LC77
	.quad	.LC112
	.section	.rodata.str1.1
.LC113:
	.string	"../src/module_wrap.cc:198"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__11_:
	.quad	.LC113
	.quad	.LC80
	.quad	.LC112
	.section	.rodata.str1.1
.LC114:
	.string	"../src/module_wrap.cc:167"
.LC115:
	.string	"args[5]->IsArrayBufferView()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_:
	.quad	.LC114
	.quad	.LC115
	.quad	.LC112
	.section	.rodata.str1.1
.LC116:
	.string	"../src/module_wrap.cc:158"
.LC117:
	.string	"export_name_val->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9:
	.quad	.LC116
	.quad	.LC117
	.quad	.LC112
	.section	.rodata.str1.1
.LC118:
	.string	"../src/module_wrap.cc:150"
.LC119:
	.string	"args[2]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC112
	.section	.rodata.str1.1
.LC120:
	.string	"../src/module_wrap.cc:133"
.LC121:
	.string	"args[4]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7:
	.quad	.LC120
	.quad	.LC121
	.quad	.LC112
	.section	.rodata.str1.1
.LC122:
	.string	"../src/module_wrap.cc:131"
.LC123:
	.string	"args[3]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6:
	.quad	.LC122
	.quad	.LC123
	.quad	.LC112
	.section	.rodata.str1.1
.LC124:
	.string	"../src/module_wrap.cc:130"
.LC125:
	.string	"args[2]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5:
	.quad	.LC124
	.quad	.LC125
	.quad	.LC112
	.section	.rodata.str1.1
.LC126:
	.string	"../src/module_wrap.cc:127"
.LC127:
	.string	"args[3]->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC126
	.quad	.LC127
	.quad	.LC112
	.section	.rodata.str1.1
.LC128:
	.string	"../src/module_wrap.cc:117"
.LC129:
	.string	"(sandbox) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC128
	.quad	.LC129
	.quad	.LC112
	.section	.rodata.str1.1
.LC130:
	.string	"../src/module_wrap.cc:113"
.LC131:
	.string	"args[1]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC130
	.quad	.LC131
	.quad	.LC112
	.section	.rodata.str1.1
.LC132:
	.string	"../src/module_wrap.cc:106"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC132
	.quad	.LC70
	.quad	.LC112
	.section	.rodata.str1.1
.LC133:
	.string	"../src/module_wrap.cc:99"
.LC134:
	.string	"(args.Length()) >= (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC133
	.quad	.LC134
	.quad	.LC112
	.section	.rodata.str1.1
.LC135:
	.string	"../src/module_wrap.cc:98"
.LC136:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6loader10ModuleWrap3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC135
	.quad	.LC136
	.quad	.LC112
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC137:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC139:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC140:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC142:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC140
	.quad	.LC141
	.quad	.LC142
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC143:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC144:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC145:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC143
	.quad	.LC144
	.quad	.LC145
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC146:
	.string	"../src/base_object-inl.h:44"
.LC147:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC146
	.quad	.LC147
	.quad	.LC145
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC148:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC150:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.quad	0
	.quad	16
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC42:
	.long	0
	.long	1072693248
	.section	.data.rel.ro,"aw"
	.align 8
.LC50:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC51:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC52:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
