	.file	"node_contextify.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4858:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4858:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4859:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4859:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7098:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7282:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7282:
	.size	_ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node10contextify16ContextifyScript8SelfSizeEv,"axG",@progbits,_ZNK4node10contextify16ContextifyScript8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10contextify16ContextifyScript8SelfSizeEv
	.type	_ZNK4node10contextify16ContextifyScript8SelfSizeEv, @function
_ZNK4node10contextify16ContextifyScript8SelfSizeEv:
.LFB7284:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE7284:
	.size	_ZNK4node10contextify16ContextifyScript8SelfSizeEv, .-_ZNK4node10contextify16ContextifyScript8SelfSizeEv
	.section	.text._ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7286:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7286:
	.size	_ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node10contextify15CompiledFnEntry8SelfSizeEv,"axG",@progbits,_ZNK4node10contextify15CompiledFnEntry8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10contextify15CompiledFnEntry8SelfSizeEv
	.type	_ZNK4node10contextify15CompiledFnEntry8SelfSizeEv, @function
_ZNK4node10contextify15CompiledFnEntry8SelfSizeEv:
.LFB7288:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE7288:
	.size	_ZNK4node10contextify15CompiledFnEntry8SelfSizeEv, .-_ZNK4node10contextify15CompiledFnEntry8SelfSizeEv
	.text
	.p2align 4
	.type	_ZN4node10contextifyL24WatchdogHasPendingSigintERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10contextifyL24WatchdogHasPendingSigintERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	subq	$8, %rsp
	call	_ZN4node20SigintWatchdogHelper16HasPendingSignalEv@PLT
	movq	(%rbx), %rdx
	cmpb	$1, %al
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7819:
	.size	_ZN4node10contextifyL24WatchdogHasPendingSigintERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10contextifyL24WatchdogHasPendingSigintERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node10contextifyL18StopSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10contextifyL18StopSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	subq	$8, %rsp
	call	_ZN4node20SigintWatchdogHelper4StopEv@PLT
	movq	(%rbx), %rdx
	cmpb	$1, %al
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7818:
	.size	_ZN4node10contextifyL18StopSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10contextifyL18StopSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node10contextifyL19StartSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10contextifyL19StartSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	_ZN4node20SigintWatchdogHelper8instanceE(%rip), %rdi
	subq	$8, %rsp
	call	_ZN4node20SigintWatchdogHelper5StartEv@PLT
	movq	(%rbx), %rdx
	cmpl	$1, %eax
	movq	8(%rdx), %rcx
	sbbq	%rax, %rax
	andq	$-8, %rax
	movq	120(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7817:
	.size	_ZN4node10contextifyL19StartSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10contextifyL19StartSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0, @function
_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0:
.LFB11030:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rsi), %r8
	movq	8(%rdi), %rcx
	movq	(%rdi), %r13
	movq	%r8, %rax
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r10
	testq	%r10, %r10
	je	.L33
	movq	%rdi, %rbx
	movq	(%r10), %rdi
	movq	%rdx, %r12
	movq	%r10, %r11
	movq	32(%rdi), %r9
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%rdi), %r15
	testq	%r15, %r15
	je	.L33
	movq	32(%r15), %r9
	xorl	%edx, %edx
	movq	%rdi, %r11
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r12
	jne	.L33
	movq	%r15, %rdi
.L25:
	cmpq	%r9, %r8
	jne	.L23
	movq	8(%rdi), %rax
	cmpq	%rax, (%rsi)
	jne	.L23
	cmpq	16(%rdi), %r8
	jne	.L23
	movq	(%rdi), %rsi
	cmpq	%r11, %r10
	je	.L40
	testq	%rsi, %rsi
	je	.L27
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r12
	je	.L27
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%rdi), %rsi
.L27:
	movq	%rsi, (%r11)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L34
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r12
	je	.L27
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L26:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L41
.L28:
	movq	$0, (%r14)
	movq	(%rdi), %rsi
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r11, %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rsi, 16(%rbx)
	jmp	.L28
	.cfi_endproc
.LFE11030:
	.size	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0, .-_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	.section	.text._ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev:
.LFB7287:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1850043974, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$7234307576554155843, %rcx
	movq	%rdx, (%rdi)
	movl	$29300, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$121, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE7287:
	.size	_ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev, .-_ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev:
.LFB7283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$16, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L46
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L46:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7283:
	.size	_ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev, .-_ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext11CleanupHookEPv
	.type	_ZN4node10contextify17ContextifyContext11CleanupHookEPv, @function
_ZN4node10contextify17ContextifyContext11CleanupHookEPv:
.LFB7739:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$40, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L48
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L48:
	leaq	_ZN4node10contextify17ContextifyContext11CleanupHookEPv(%rip), %rax
	leaq	-48(%rbp), %rsi
	movq	%r12, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	(%r12), %rax
	movq	$0, -32(%rbp)
	leaq	2584(%rax), %rdi
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L49:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L58
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L58:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7739:
	.size	_ZN4node10contextify17ContextifyContext11CleanupHookEPv, .-_ZN4node10contextify17ContextifyContext11CleanupHookEPv
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE
	.type	_ZN4node10contextify17ContextifyContext12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE, @function
_ZN4node10contextify17ContextifyContext12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE:
.LFB7759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L59
	movq	(%r12), %rdi
	leaq	_ZN4node10contextify17ContextifyContext11CleanupHookEPv(%rip), %rax
	leaq	-48(%rbp), %rsi
	movq	%r12, -40(%rbp)
	movq	%rax, -48(%rbp)
	addq	$2584, %rdi
	movq	$0, -32(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L61:
	movl	$16, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L59:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L70
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L70:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7759:
	.size	_ZN4node10contextify17ContextifyContext12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE, .-_ZN4node10contextify17ContextifyContext12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L83
	cmpw	$1040, %cx
	jne	.L72
.L83:
	movq	23(%rdx), %r12
.L74:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L75
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L85
.L77:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L86
	movq	8(%rbx), %rdi
.L79:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	88(%rax), %rdx
	call	_ZN2v86Object10HasPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	(%rbx), %rbx
	testb	%al, %al
	je	.L87
.L80:
	movzbl	%ah, %eax
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L77
.L85:
	leaq	_ZZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L87:
	movl	%eax, -20(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-20(%rbp), %eax
	jmp	.L80
	.cfi_endproc
.LFE7758:
	.size	_ZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScript16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify16ContextifyScript16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify16ContextifyScript16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7779:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L103
	cmpw	$1040, %cx
	jne	.L89
.L103:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L109
.L92:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L104
	cmpw	$1040, %cx
	jne	.L93
.L104:
	movq	23(%rdx), %rax
.L95:
	testq	%rax, %rax
	je	.L88
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.L97
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L110
.L97:
	call	_ZN2v814ScriptCompiler15CreateCodeCacheENS_5LocalINS_13UnboundScriptEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L111
.L98:
	movslq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	%r13, %rdi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L112
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L102:
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	(%rbx), %rbx
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEm@PLT
	testq	%rax, %rax
	je	.L113
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L88:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L110:
	.cfi_restore_state
	movq	352(%r13), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	call	_ZN2v814ScriptCompiler15CreateCodeCacheENS_5LocalINS_13UnboundScriptEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L98
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L92
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L112:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L88
	.cfi_endproc
.LFE7779:
	.size	_ZN4node10contextify16ContextifyScript16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify16ContextifyScript16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L115
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L115:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L125
.L116:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L117
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L117:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L116
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7096:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext30IndexedPropertyDeleterCallbackEjRKN2v820PropertyCallbackInfoINS2_7BooleanEEE
	.type	_ZN4node10contextify17ContextifyContext30IndexedPropertyDeleterCallbackEjRKN2v820PropertyCallbackInfoINS2_7BooleanEEE, @function
_ZN4node10contextify17ContextifyContext30IndexedPropertyDeleterCallbackEjRKN2v820PropertyCallbackInfoINS2_7BooleanEEE:
.LFB7773:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L135
	cmpw	$1040, %cx
	jne	.L127
.L135:
	movq	23(%rdx), %r13
.L129:
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.L126
	movq	(%rax), %rdi
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L147
.L131:
	movq	55(%rdi), %rax
	movq	279(%rax), %r14
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r13), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L132
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L148
.L132:
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEEj@PLT
	testb	%al, %al
	jne	.L149
.L133:
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 32(%rax)
.L126:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	shrw	$8, %ax
	je	.L133
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%rdi, %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L148:
	movq	0(%r13), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEEj@PLT
	testb	%al, %al
	je	.L133
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L129
	.cfi_endproc
.LFE7773:
	.size	_ZN4node10contextify17ContextifyContext30IndexedPropertyDeleterCallbackEjRKN2v820PropertyCallbackInfoINS2_7BooleanEEE, .-_ZN4node10contextify17ContextifyContext30IndexedPropertyDeleterCallbackEjRKN2v820PropertyCallbackInfoINS2_7BooleanEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext23PropertyDeleterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_7BooleanEEE
	.type	_ZN4node10contextify17ContextifyContext23PropertyDeleterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_7BooleanEEE, @function
_ZN4node10contextify17ContextifyContext23PropertyDeleterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_7BooleanEEE:
.LFB7767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L159
	cmpw	$1040, %cx
	jne	.L151
.L159:
	movq	23(%rdx), %r13
.L153:
	movq	8(%r13), %rax
	testq	%rax, %rax
	je	.L150
	movq	(%rax), %rdi
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L171
.L155:
	movq	55(%rdi), %rax
	movq	279(%rax), %r14
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r13), %rsi
	movq	%rax, %r14
	testq	%rsi, %rsi
	je	.L156
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L172
.L156:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L173
.L157:
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 32(%rax)
.L150:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	shrw	$8, %ax
	je	.L157
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%rdi, %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L172:
	movq	0(%r13), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L157
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L151:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L153
	.cfi_endproc
.LFE7767:
	.size	_ZN4node10contextify17ContextifyContext23PropertyDeleterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_7BooleanEEE, .-_ZN4node10contextify17ContextifyContext23PropertyDeleterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_7BooleanEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext26PropertyEnumeratorCallbackERKN2v820PropertyCallbackInfoINS2_5ArrayEEE
	.type	_ZN4node10contextify17ContextifyContext26PropertyEnumeratorCallbackERKN2v820PropertyCallbackInfoINS2_5ArrayEEE, @function
_ZN4node10contextify17ContextifyContext26PropertyEnumeratorCallbackERKN2v820PropertyCallbackInfoINS2_5ArrayEEE:
.LFB7768:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L182
	cmpw	$1040, %cx
	jne	.L175
.L182:
	movq	23(%rdx), %r12
.L177:
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L174
	movq	(%rax), %rdi
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L190
.L179:
	movq	55(%rdi), %rax
	movq	279(%rax), %r13
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r13, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, %r13
	testq	%rsi, %rsi
	je	.L180
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L191
.L180:
	movq	%r13, %rdi
	call	_ZN2v86Object16GetPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L174
.L192:
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 32(%rax)
.L174:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%rdi, %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L191:
	movq	(%r12), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object16GetPropertyNamesENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	jne	.L192
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L175:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L177
	.cfi_endproc
.LFE7768:
	.size	_ZN4node10contextify17ContextifyContext26PropertyEnumeratorCallbackERKN2v820PropertyCallbackInfoINS2_5ArrayEEE, .-_ZN4node10contextify17ContextifyContext26PropertyEnumeratorCallbackERKN2v820PropertyCallbackInfoINS2_5ArrayEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext26PropertyDescriptorCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext26PropertyDescriptorCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext26PropertyDescriptorCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB7764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %rdi
	movq	%rsi, %rbx
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L205
	cmpw	$1040, %cx
	jne	.L194
.L205:
	movq	23(%rdx), %r14
.L196:
	movq	8(%r14), %r12
	testq	%r12, %r12
	je	.L193
	movq	(%r14), %rax
	movq	(%r12), %rdi
	movq	%r12, %rdx
	movq	352(%rax), %r8
	movzbl	11(%r12), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L210
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L211
.L203:
	movq	55(%rdi), %rax
	movq	279(%rax), %r14
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object14HasOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	testb	%al, %al
	je	.L193
	shrw	$8, %ax
	jne	.L212
.L193:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	.cfi_restore_state
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r14), %rdx
	movq	%rax, %r12
	movq	(%r14), %rax
	movq	(%rdx), %rdi
	movq	352(%rax), %r8
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L203
.L211:
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object24GetOwnPropertyDescriptorENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	testq	%rax, %rax
	je	.L193
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 32(%rax)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L196
	.cfi_endproc
.LFE7764:
	.size	_ZN4node10contextify17ContextifyContext26PropertyDescriptorCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext26PropertyDescriptorCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext33IndexedPropertyDescriptorCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext33IndexedPropertyDescriptorCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext33IndexedPropertyDescriptorCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE:
.LFB7771:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	(%rsi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L220
	cmpw	$1040, %cx
	jne	.L214
.L220:
	movq	23(%rdx), %rax
.L216:
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L213
	movzbl	11(%r12), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L222
.L218:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L223
.L219:
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10contextify17ContextifyContext26PropertyDescriptorCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	(%r12), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L219
	.cfi_endproc
.LFE7771:
	.size	_ZN4node10contextify17ContextifyContext33IndexedPropertyDescriptorCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext33IndexedPropertyDescriptorCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext22PropertySetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIS6_EE
	.type	_ZN4node10contextify17ContextifyContext22PropertySetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIS6_EE, @function
_ZN4node10contextify17ContextifyContext22PropertySetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIS6_EE:
.LFB7763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %rdi
	movq	%rsi, -56(%rbp)
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L251
	cmpw	$1040, %cx
	jne	.L225
.L251:
	movq	23(%rdx), %rbx
.L227:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L224
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L268
.L230:
	call	_ZN2v87Context6GlobalEv@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	testq	%rsi, %rsi
	je	.L231
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L269
.L231:
	movq	%r12, %rdi
	movq	%r13, %rdx
	movl	$0, %r12d
	call	_ZN2v86Object30GetRealNamedPropertyAttributesENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, %r15
	shrq	$32, %rax
	testb	%r15b, %r15b
	cmovne	%eax, %r12d
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L270
.L233:
	movq	55(%rdi), %rax
	movq	279(%rax), %rsi
	movq	%rsi, -64(%rbp)
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r8
	testq	%rsi, %rsi
	je	.L234
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L271
.L234:
	movq	%r13, %rdx
	movq	%r8, %rdi
	call	_ZN2v86Object30GetRealNamedPropertyAttributesENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	movq	%rax, %rdx
	shrq	$32, %rax
	orl	%r12d, %eax
	testb	%dl, %dl
	cmovne	%eax, %r12d
	andl	$1, %r12d
	je	.L272
.L224:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	movq	(%r14), %r12
	testq	%rdi, %rdi
	je	.L237
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L237
	movq	(%rbx), %rax
	movq	(%rdi), %rsi
	movq	%rdx, -64(%rbp)
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-64(%rbp), %rdx
	movq	%rax, %rdi
.L237:
	movq	%rdx, -64(%rbp)
	call	_ZN2v87Context6GlobalEv@PLT
	movq	-64(%rbp), %rdx
	testq	%rax, %rax
	je	.L250
	movq	48(%r12), %rcx
	cmpq	%rcx, (%rax)
	sete	%r12b
.L238:
	movq	-56(%rbp), %rdi
	movq	%rdx, -64(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-64(%rbp), %rdx
	movl	%eax, %ecx
	testb	%dl, %dl
	jne	.L239
	testb	%r15b, %r15b
	jne	.L239
	movabsq	$8589934592, %rsi
	movq	(%r14), %rdx
	movq	(%rdx), %rax
	cmpq	%rsi, %rax
	je	.L240
	movabsq	$4294967296, %rdx
	cmpq	%rdx, %rax
	setne	%al
.L241:
	testb	%al, %al
	je	.L243
	testb	%cl, %cl
	jne	.L243
	testb	%r12b, %r12b
	je	.L224
	.p2align 4,,10
	.p2align 3
.L243:
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L273
.L247:
	movq	55(%rdi), %rax
	movq	279(%rax), %r12
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r12
	testq	%rsi, %rsi
	je	.L248
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L274
.L248:
	movq	-56(%rbp), %rcx
	addq	$24, %rsp
	movq	%r13, %rdx
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	%rdi, %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L269:
	movq	(%rbx), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L271:
	movq	(%rbx), %rax
	movq	(%rsi), %rsi
	movq	%r8, -64(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-64(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L225:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L239:
	cmpb	$1, %r15b
	je	.L243
	testb	%dl, %dl
	je	.L243
	movabsq	$8589934592, %rsi
	movq	(%r14), %rdx
	movq	(%rdx), %rax
	cmpq	%rsi, %rax
	je	.L245
	movabsq	$4294967296, %rdx
	cmpq	%rdx, %rax
	setne	%al
.L246:
	testb	%al, %al
	je	.L243
	testb	%cl, %cl
	jne	.L243
	testb	%r12b, %r12b
	jne	.L243
	movq	(%r14), %rax
	movq	16(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 32(%rax)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L273:
	movq	(%rbx), %rax
	movq	%rdi, %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L274:
	movq	(%rbx), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L250:
	xorl	%r12d, %r12d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L240:
	movq	16(%rdx), %rdi
	movb	%cl, -64(%rbp)
	call	_ZN2v88internal18ShouldThrowOnErrorEPNS0_7IsolateE@PLT
	movzbl	-64(%rbp), %ecx
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L245:
	movq	16(%rdx), %rdi
	movb	%cl, -64(%rbp)
	call	_ZN2v88internal18ShouldThrowOnErrorEPNS0_7IsolateE@PLT
	movzbl	-64(%rbp), %ecx
	jmp	.L246
	.cfi_endproc
.LFE7763:
	.size	_ZN4node10contextify17ContextifyContext22PropertySetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIS6_EE, .-_ZN4node10contextify17ContextifyContext22PropertySetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIS6_EE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext29IndexedPropertySetterCallbackEjN2v85LocalINS2_5ValueEEERKNS2_20PropertyCallbackInfoIS4_EE
	.type	_ZN4node10contextify17ContextifyContext29IndexedPropertySetterCallbackEjN2v85LocalINS2_5ValueEEERKNS2_20PropertyCallbackInfoIS4_EE, @function
_ZN4node10contextify17ContextifyContext29IndexedPropertySetterCallbackEjN2v85LocalINS2_5ValueEEERKNS2_20PropertyCallbackInfoIS4_EE:
.LFB7770:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%rdx), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L282
	cmpw	$1040, %cx
	jne	.L276
.L282:
	movq	23(%rdx), %rax
.L278:
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L275
	movzbl	11(%r12), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L284
.L280:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L285
.L281:
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10contextify17ContextifyContext22PropertySetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIS6_EE
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	(%r12), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L276:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L281
	.cfi_endproc
.LFE7770:
	.size	_ZN4node10contextify17ContextifyContext29IndexedPropertySetterCallbackEjN2v85LocalINS2_5ValueEEERKNS2_20PropertyCallbackInfoIS4_EE, .-_ZN4node10contextify17ContextifyContext29IndexedPropertySetterCallbackEjN2v85LocalINS2_5ValueEEERKNS2_20PropertyCallbackInfoIS4_EE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext22PropertyGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext22PropertyGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext22PropertyGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB7762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rsi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L302
	cmpw	$1040, %cx
	jne	.L287
.L302:
	movq	23(%rdx), %r15
.L289:
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L286
	movq	(%r15), %rax
	movq	(%r12), %rdi
	movq	%r12, %rdx
	movq	352(%rax), %r8
	movzbl	11(%r12), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L317
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L318
.L300:
	movq	55(%rdi), %rax
	movq	279(%rax), %r14
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r14, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	testq	%rax, %rax
	je	.L319
.L292:
	movq	(%rax), %rax
	testq	%r14, %r14
	je	.L295
	cmpq	%rax, (%r14)
	je	.L320
.L295:
	movq	(%rbx), %rdx
.L298:
	movq	%rax, 32(%rdx)
.L286:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	.cfi_restore_state
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	8(%r15), %rdx
	movq	%rax, %r12
	movq	(%r15), %rax
	movq	(%rdx), %rdi
	movq	352(%rax), %r8
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L300
.L318:
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L287:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L320:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L296
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L321
.L296:
	call	_ZN2v87Context6GlobalEv@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L322
	movq	(%rax), %rax
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L319:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L293
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L323
.L293:
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v86Object20GetRealNamedPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	testq	%rax, %rax
	jne	.L292
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	movq	24(%rdx), %rax
	movq	%rax, 32(%rdx)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L321:
	movq	(%r15), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L323:
	movq	(%r15), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L293
	.cfi_endproc
.LFE7762:
	.size	_ZN4node10contextify17ContextifyContext22PropertyGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext22PropertyGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext29IndexedPropertyGetterCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext29IndexedPropertyGetterCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext29IndexedPropertyGetterCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE:
.LFB7769:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%edi, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	(%rsi), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L331
	cmpw	$1040, %cx
	jne	.L325
.L331:
	movq	23(%rdx), %rax
.L327:
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L324
	movzbl	11(%r12), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L333
.L329:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	%r14d, %esi
	movq	%rax, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L334
.L330:
	addq	$24, %rsp
	movq	%r13, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10contextify17ContextifyContext22PropertyGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	(%r12), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L325:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L330
	.cfi_endproc
.LFE7769:
	.size	_ZN4node10contextify17ContextifyContext29IndexedPropertyGetterCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext29IndexedPropertyGetterCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext23PropertyDefinerCallbackEN2v85LocalINS2_4NameEEERKNS2_18PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext23PropertyDefinerCallbackEN2v85LocalINS2_4NameEEERKNS2_18PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext23PropertyDefinerCallbackEN2v85LocalINS2_4NameEEERKNS2_18PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB7765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	(%rdx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L366
	cmpw	$1040, %cx
	jne	.L336
.L366:
	movq	23(%rdx), %rbx
.L338:
	movq	8(%rbx), %r13
	testq	%r13, %r13
	je	.L335
	movzbl	11(%r13), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L402
.L340:
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %r15
	testq	%rdi, %rdi
	je	.L341
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L403
.L341:
	call	_ZN2v87Context6GlobalEv@PLT
	movq	8(%rbx), %rsi
	movq	%rax, %r8
	testq	%rsi, %rsi
	je	.L342
	movzbl	11(%rsi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L404
.L342:
	movq	%r14, %rdx
	movq	%r8, %rdi
	call	_ZN2v86Object30GetRealNamedPropertyAttributesENS_5LocalINS_7ContextEEENS1_INS_4NameEEE@PLT
	testb	%al, %al
	je	.L364
	btq	$32, %rax
	jnc	.L364
.L335:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L405
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L364:
	.cfi_restore_state
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L406
.L344:
	movq	55(%rdi), %rax
	movq	279(%rax), %rbx
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%rbx, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZNK2v818PropertyDescriptor7has_getEv@PLT
	testb	%al, %al
	jne	.L348
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor7has_setEv@PLT
	testb	%al, %al
	jne	.L348
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor9has_valueEv@PLT
	leaq	88(%r15), %rsi
	testb	%al, %al
	jne	.L407
.L357:
	movq	%r12, %rdi
	movq	%rsi, -72(%rbp)
	call	_ZNK2v818PropertyDescriptor12has_writableEv@PLT
	movq	-72(%rbp), %rsi
	testb	%al, %al
	je	.L358
	movq	%r12, %rdi
	leaq	-64(%rbp), %r15
	call	_ZNK2v818PropertyDescriptor8writableEv@PLT
	movq	-72(%rbp), %rsi
	movq	%r15, %rdi
	movzbl	%al, %edx
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor14has_enumerableEv@PLT
	testb	%al, %al
	jne	.L401
	.p2align 4,,10
	.p2align 3
.L362:
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor16has_configurableEv@PLT
	testb	%al, %al
	jne	.L408
.L363:
	movq	%rbx, %rdi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	_ZN2v86Object14DefinePropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEERNS_18PropertyDescriptorE@PLT
	movq	%r15, %rdi
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L348:
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor7has_setEv@PLT
	leaq	88(%r15), %rdx
	testb	%al, %al
	jne	.L409
.L351:
	movq	%r12, %rdi
	movq	%rdx, -72(%rbp)
	call	_ZNK2v818PropertyDescriptor7has_getEv@PLT
	movq	-72(%rbp), %rdx
	leaq	88(%r15), %rsi
	testb	%al, %al
	jne	.L410
.L353:
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEES3_@PLT
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor14has_enumerableEv@PLT
	testb	%al, %al
	je	.L362
.L401:
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor10enumerableEv@PLT
	movq	%r15, %rdi
	movzbl	%al, %esi
	call	_ZN2v818PropertyDescriptor14set_enumerableEb@PLT
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor16has_configurableEv@PLT
	testb	%al, %al
	je	.L363
.L408:
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor12configurableEv@PLT
	movq	%r15, %rdi
	movzbl	%al, %esi
	call	_ZN2v818PropertyDescriptor16set_configurableEb@PLT
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L402:
	movq	(%rbx), %rax
	movq	0(%r13), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r13
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L403:
	movq	(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L404:
	movq	(%rbx), %rax
	movq	(%rsi), %rsi
	movq	%r8, -72(%rbp)
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rsi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L336:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L410:
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor3getEv@PLT
	movq	-72(%rbp), %rdx
	movq	%rax, %rsi
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor3setEv@PLT
	movq	%rax, %rdx
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L406:
	movq	(%rbx), %rax
	movq	%rdi, %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	(%rax), %rdi
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	-64(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor14has_enumerableEv@PLT
	testb	%al, %al
	je	.L362
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r12, %rdi
	call	_ZNK2v818PropertyDescriptor5valueEv@PLT
	movq	%rax, %rsi
	jmp	.L357
.L405:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7765:
	.size	_ZN4node10contextify17ContextifyContext23PropertyDefinerCallbackEN2v85LocalINS2_4NameEEERKNS2_18PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext23PropertyDefinerCallbackEN2v85LocalINS2_4NameEEERKNS2_18PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext30IndexedPropertyDefinerCallbackEjRKN2v818PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext30IndexedPropertyDefinerCallbackEjRKN2v818PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext30IndexedPropertyDefinerCallbackEjRKN2v818PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE:
.LFB7772:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%edi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 12, -48
	movq	(%rdx), %rdi
	movq	40(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L418
	cmpw	$1040, %cx
	jne	.L412
.L418:
	movq	23(%rdx), %rax
.L414:
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.L411
	movzbl	11(%r12), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L420
.L416:
	movq	%r12, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movl	%r15d, %esi
	movq	%rax, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L421
.L417:
	addq	$16, %rsp
	movq	%r13, %rdx
	movq	%r14, %rsi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10contextify17ContextifyContext23PropertyDefinerCallbackEN2v85LocalINS2_4NameEEERKNS2_18PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L420:
	.cfi_restore_state
	movq	(%rax), %rax
	movq	(%r12), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r12
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L412:
	addq	$40, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L417
	.cfi_endproc
.LFE7772:
	.size	_ZN4node10contextify17ContextifyContext30IndexedPropertyDefinerCallbackEjRKN2v818PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext30IndexedPropertyDefinerCallbackEjRKN2v818PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node10BaseObjectD2Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD2Ev
	.type	_ZN4node10BaseObjectD2Ev, @function
_ZN4node10BaseObjectD2Ev:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-48(%rbp), %r12
	movq	%r12, %rsi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %rax
	subq	$1, 2656(%rdi)
	addq	$2584, %rdi
	movq	%rax, -48(%rbp)
	movq	%rbx, -40(%rbp)
	movq	$0, -32(%rbp)
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L438
.L424:
	cmpq	$0, 8(%rbx)
	je	.L422
	movq	16(%rbx), %rax
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L428
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L439
.L428:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L422
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L422:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L440
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	16(%rbx), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L438:
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L441
	movl	4(%rdi), %eax
	movq	$0, 16(%rdi)
	testl	%eax, %eax
	jne	.L424
	movl	$24, %esi
	call	_ZdlPvm@PLT
	jmp	.L424
.L441:
	leaq	_ZZN4node10BaseObjectD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L440:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7084:
	.size	_ZN4node10BaseObjectD2Ev, .-_ZN4node10BaseObjectD2Ev
	.weak	_ZN4node10BaseObjectD1Ev
	.set	_ZN4node10BaseObjectD1Ev,_ZN4node10BaseObjectD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScriptD2Ev
	.type	_ZN4node10contextify16ContextifyScriptD2Ev, @function
_ZN4node10contextify16ContextifyScriptD2Ev:
.LFB7787:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10contextify16ContextifyScriptE(%rip), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%rax, (%rdi)
	movl	40(%rdi), %eax
	movq	224(%rbx), %rcx
	movq	216(%rbx), %r14
	movq	%rax, %r13
	divq	%rcx
	leaq	(%r14,%rdx,8), %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L443
	movq	(%rsi), %rdi
	movq	%rdx, %r11
	movq	%rsi, %r10
	movl	8(%rdi), %r9d
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L467:
	testq	%r8, %r8
	je	.L443
	movl	8(%r8), %eax
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rax, %r9
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L443
	movq	%r8, %rdi
.L445:
	movq	(%rdi), %r8
	cmpl	%r9d, %r13d
	jne	.L467
	cmpq	%r10, %rsi
	je	.L468
	testq	%r8, %r8
	je	.L447
	movl	8(%r8), %eax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L447
	movq	%r10, (%r14,%rdx,8)
	movq	(%rdi), %r8
.L447:
	movq	%r8, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 240(%rbx)
.L443:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L449
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L449:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.p2align 4,,10
	.p2align 3
.L468:
	.cfi_restore_state
	testq	%r8, %r8
	je	.L452
	movl	8(%r8), %eax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L447
	movq	%r10, (%r14,%rdx,8)
	movq	(%r15), %rax
.L446:
	leaq	232(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L469
.L448:
	movq	$0, (%r15)
	movq	(%rdi), %r8
	jmp	.L447
	.p2align 4,,10
	.p2align 3
.L452:
	movq	%r10, %rax
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%r8, 232(%rbx)
	jmp	.L448
	.cfi_endproc
.LFE7787:
	.size	_ZN4node10contextify16ContextifyScriptD2Ev, .-_ZN4node10contextify16ContextifyScriptD2Ev
	.globl	_ZN4node10contextify16ContextifyScriptD1Ev
	.set	_ZN4node10contextify16ContextifyScriptD1Ev,_ZN4node10contextify16ContextifyScriptD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScriptD0Ev
	.type	_ZN4node10contextify16ContextifyScriptD0Ev, @function
_ZN4node10contextify16ContextifyScriptD0Ev:
.LFB7789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10contextify16ContextifyScriptD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7789:
	.size	_ZN4node10contextify16ContextifyScriptD0Ev, .-_ZN4node10contextify16ContextifyScriptD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify15CompiledFnEntryD2Ev
	.type	_ZN4node10contextify15CompiledFnEntryD2Ev, @function
_ZN4node10contextify15CompiledFnEntryD2Ev:
.LFB7814:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10contextify15CompiledFnEntryE(%rip), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%rax, (%rdi)
	movl	32(%rdi), %eax
	movq	280(%rbx), %rcx
	movq	272(%rbx), %r14
	movq	%rax, %r13
	divq	%rcx
	leaq	(%r14,%rdx,8), %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L473
	movq	(%rsi), %rdi
	movq	%rdx, %r11
	movq	%rsi, %r10
	movl	8(%rdi), %r9d
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L497:
	testq	%r8, %r8
	je	.L473
	movl	8(%r8), %eax
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rax, %r9
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L473
	movq	%r8, %rdi
.L475:
	movq	(%rdi), %r8
	cmpl	%r9d, %r13d
	jne	.L497
	cmpq	%r10, %rsi
	je	.L498
	testq	%r8, %r8
	je	.L477
	movl	8(%r8), %eax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L477
	movq	%r10, (%r14,%rdx,8)
	movq	(%rdi), %r8
.L477:
	movq	%r8, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 296(%rbx)
.L473:
	movq	40(%r12), %rdi
	call	_ZN2v82V89ClearWeakEPm@PLT
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L479
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L479:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10BaseObjectD2Ev
	.p2align 4,,10
	.p2align 3
.L498:
	.cfi_restore_state
	testq	%r8, %r8
	je	.L482
	movl	8(%r8), %eax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L477
	movq	%r10, (%r14,%rdx,8)
	movq	(%r15), %rax
.L476:
	leaq	288(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L499
.L478:
	movq	$0, (%r15)
	movq	(%rdi), %r8
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L482:
	movq	%r10, %rax
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%r8, 288(%rbx)
	jmp	.L478
	.cfi_endproc
.LFE7814:
	.size	_ZN4node10contextify15CompiledFnEntryD2Ev, .-_ZN4node10contextify15CompiledFnEntryD2Ev
	.globl	_ZN4node10contextify15CompiledFnEntryD1Ev
	.set	_ZN4node10contextify15CompiledFnEntryD1Ev,_ZN4node10contextify15CompiledFnEntryD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify15CompiledFnEntryD0Ev
	.type	_ZN4node10contextify15CompiledFnEntryD0Ev, @function
_ZN4node10contextify15CompiledFnEntryD0Ev:
.LFB7816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10contextify15CompiledFnEntryD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7816:
	.size	_ZN4node10contextify15CompiledFnEntryD0Ev, .-_ZN4node10contextify15CompiledFnEntryD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify15CompiledFnEntry12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE
	.type	_ZN4node10contextify15CompiledFnEntry12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE, @function
_ZN4node10contextify15CompiledFnEntry12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE:
.LFB7809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	testq	%r12, %r12
	je	.L502
	movq	%r12, %rdi
	call	_ZN4node10contextify15CompiledFnEntryD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L502:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7809:
	.size	_ZN4node10contextify15CompiledFnEntry12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE, .-_ZN4node10contextify15CompiledFnEntry12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE
	.section	.text._ZN4node10BaseObjectD0Ev,"axG",@progbits,_ZN4node10BaseObjectD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObjectD0Ev
	.type	_ZN4node10BaseObjectD0Ev, @function
_ZN4node10BaseObjectD0Ev:
.LFB7086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node10BaseObjectD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7086:
	.size	_ZN4node10BaseObjectD0Ev, .-_ZN4node10BaseObjectD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContextD2Ev
	.type	_ZN4node10contextify17ContextifyContextD2Ev, @function
_ZN4node10contextify17ContextifyContextD2Ev:
.LFB7737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	-48(%rbp), %rsi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	_ZN4node10contextify17ContextifyContext11CleanupHookEPv(%rip), %rax
	movq	%rdi, -40(%rbp)
	movq	%rax, -48(%rbp)
	movq	(%rdi), %rax
	movq	$0, -32(%rbp)
	leaq	2584(%rax), %rdi
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L507
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L507:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L514
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L514:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7737:
	.size	_ZN4node10contextify17ContextifyContextD2Ev, .-_ZN4node10contextify17ContextifyContextD2Ev
	.globl	_ZN4node10contextify17ContextifyContextD1Ev
	.set	_ZN4node10contextify17ContextifyContextD1Ev,_ZN4node10contextify17ContextifyContextD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext17CreateDataWrapperEPNS_11EnvironmentE
	.type	_ZN4node10contextify17ContextifyContext17CreateDataWrapperEPNS_11EnvironmentE, @function
_ZN4node10contextify17ContextifyContext17CreateDataWrapperEPNS_11EnvironmentE:
.LFB7740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	2992(%rsi), %rdi
	movq	3280(%rsi), %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L519
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	%rbx, %rax
.L519:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7740:
	.size	_ZN4node10contextify17ContextifyContext17CreateDataWrapperEPNS_11EnvironmentE, .-_ZN4node10contextify17ContextifyContext17CreateDataWrapperEPNS_11EnvironmentE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext15CreateV8ContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	.type	_ZN4node10contextify17ContextifyContext15CreateV8ContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE, @function
_ZN4node10contextify17ContextifyContext15CreateV8ContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE:
.LFB7741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-2432(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$2456, %rsp
	movq	%rdi, -2456(%rbp)
	movq	352(%rsi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %r9d
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object18GetConstructorNameEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	3280(%rbx), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	2992(%rbx), %rdi
	movq	%rax, %r12
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	popq	%rcx
	popq	%rsi
	testq	%rax, %rax
	je	.L543
	movq	-2456(%rbp), %rdx
	movq	%rax, %rdi
	xorl	%esi, %esi
	movq	%rax, -2456(%rbp)
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	_ZN4node10contextify17ContextifyContext22PropertyGetterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	_ZN4node10contextify17ContextifyContext22PropertySetterCallbackEN2v85LocalINS2_4NameEEENS3_INS2_5ValueEEERKNS2_20PropertyCallbackInfoIS6_EE(%rip), %rdx
	movq	%rax, %xmm0
	leaq	_ZN4node10contextify17ContextifyContext23PropertyDeleterCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_7BooleanEEE(%rip), %rax
	movq	%rdx, %xmm1
	movq	$0, -2384(%rbp)
	movq	%rax, -2376(%rbp)
	leaq	_ZN4node10contextify17ContextifyContext29IndexedPropertySetterCallbackEjN2v85LocalINS2_5ValueEEERKNS2_20PropertyCallbackInfoIS4_EE(%rip), %rcx
	leaq	_ZN4node10contextify17ContextifyContext23PropertyDefinerCallbackEN2v85LocalINS2_4NameEEERKNS2_18PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rax
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, %xmm2
	movq	%rax, -2360(%rbp)
	leaq	_ZN4node10contextify17ContextifyContext29IndexedPropertyGetterCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE(%rip), %rsi
	leaq	_ZN4node10contextify17ContextifyContext26PropertyDescriptorCallbackEN2v85LocalINS2_4NameEEERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rax
	leaq	_ZN4node10contextify17ContextifyContext30IndexedPropertyDeleterCallbackEjRKN2v820PropertyCallbackInfoINS2_7BooleanEEE(%rip), %rcx
	movq	%rax, -2352(%rbp)
	movq	-2456(%rbp), %rax
	leaq	_ZN4node10contextify17ContextifyContext26PropertyEnumeratorCallbackERKN2v820PropertyCallbackInfoINS2_5ArrayEEE(%rip), %rdx
	movq	%rcx, -2296(%rbp)
	leaq	_ZN4node10contextify17ContextifyContext30IndexedPropertyDefinerCallbackEjRKN2v818PropertyDescriptorERKNS2_20PropertyCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movaps	%xmm0, -2400(%rbp)
	movq	%rsi, %xmm0
	leaq	-2400(%rbp), %rsi
	punpcklqdq	%xmm2, %xmm0
	movq	%rcx, -2280(%rbp)
	leaq	_ZN4node10contextify17ContextifyContext33IndexedPropertyDescriptorCallbackEjRKN2v820PropertyCallbackInfoINS2_5ValueEEE(%rip), %rcx
	movq	%rdx, -2368(%rbp)
	movq	%rax, -2344(%rbp)
	movq	%rdx, -2288(%rbp)
	movq	%rcx, -2272(%rbp)
	movq	%rax, -2264(%rbp)
	movaps	%xmm0, -2320(%rbp)
	movl	$8, -2336(%rbp)
	movq	$0, -2304(%rbp)
	movl	$8, -2256(%rbp)
	call	_ZN2v814ObjectTemplate10SetHandlerERKNS_33NamedPropertyHandlerConfigurationE@PLT
	movq	%r12, %rdi
	leaq	-2320(%rbp), %rsi
	call	_ZN2v814ObjectTemplate10SetHandlerERKNS_35IndexedPropertyHandlerConfigurationE@PLT
	subq	$8, %rsp
	movq	%r12, %rdx
	xorl	%r8d, %r8d
	pushq	$0
	movq	352(%rbx), %rdi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L543
	movq	%r12, %rdi
	call	_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE@PLT
	movq	3280(%rbx), %rdi
	call	_ZN2v87Context16GetSecurityTokenEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Context16SetSecurityTokenENS_5LocalINS_5ValueEEE@PLT
	movq	%r14, %rdx
	movl	$33, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context15SetEmbedderDataEiNS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	96(%rax), %rdx
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	movq	(%r15), %rdx
	movq	352(%rbx), %rsi
	leaq	-2160(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	16(%r15), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	%r12, %rdi
	movzbl	%al, %esi
	call	_ZN2v87Context30AllowCodeGenerationFromStringsEb@PLT
	movq	24(%r15), %rdx
	movl	$34, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context15SetEmbedderDataEiNS_5LocalINS_5ValueEEE@PLT
	leaq	-1104(%rbp), %rax
	movq	-2144(%rbp), %r8
	movq	%rax, -2480(%rbp)
	leaq	-1088(%rbp), %rax
	movq	%rax, -2456(%rbp)
	movq	%rax, -1104(%rbp)
	testq	%r8, %r8
	je	.L531
	movq	%r8, %rdi
	movq	%r8, -2464(%rbp)
	call	strlen@PLT
	movq	-2464(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -2440(%rbp)
	movq	%rax, %r14
	ja	.L573
	cmpq	$1, %rax
	jne	.L529
	movzbl	(%r8), %edx
	movb	%dl, -1088(%rbp)
	movq	-2456(%rbp), %rdx
.L530:
	movq	%rax, -1096(%rbp)
	leaq	-2224(%rbp), %r14
	movb	$0, (%rdx,%rax)
	movq	-1104(%rbp), %r9
	movq	-1096(%rbp), %r8
	movq	%r14, -2240(%rbp)
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L548
	testq	%r9, %r9
	je	.L531
.L548:
	movq	%r8, -2440(%rbp)
	cmpq	$15, %r8
	ja	.L574
	cmpq	$1, %r8
	jne	.L535
	leaq	-2240(%rbp), %rcx
	movzbl	(%r9), %eax
	movq	%rcx, -2464(%rbp)
	movb	%al, -2224(%rbp)
	movq	%r14, %rax
.L536:
	movq	%r8, -2232(%rbp)
	movb	$0, (%rax,%r8)
	leaq	-2192(%rbp), %rax
	movq	-1104(%rbp), %rdi
	movq	%rax, -2472(%rbp)
	movq	%rax, -2208(%rbp)
	movq	$0, -2200(%rbp)
	movb	$0, -2192(%rbp)
	movb	$0, -2176(%rbp)
	cmpq	-2456(%rbp), %rdi
	je	.L537
	call	_ZdlPv@PLT
.L537:
	movq	8(%r15), %rdx
	testq	%rdx, %rdx
	je	.L538
	movq	352(%rbx), %rsi
	movq	-2480(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	-2200(%rbp), %rdx
	leaq	-2208(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L538
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L538
	call	free@PLT
.L538:
	movq	%r12, %rdi
	movq	%rbx, %rdx
	movl	$32, %esi
	call	_ZN2v87Context31SetAlignedPointerInEmbedderDataEiPv@PLT
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	%r12, %rdi
	movl	$35, %esi
	call	_ZN2v87Context31SetAlignedPointerInEmbedderDataEiPv@PLT
	movq	2080(%rbx), %rdi
	movq	-2464(%rbp), %rdx
	movq	%r12, %rsi
	call	_ZN4node9inspector5Agent14ContextCreatedEN2v85LocalINS2_7ContextEEERKNS_11ContextInfoE@PLT
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-2208(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-2472(%rbp), %rdi
	je	.L540
	call	_ZdlPv@PLT
.L540:
	movq	-2240(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L525
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L525
	call	free@PLT
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L543:
	xorl	%r12d, %r12d
.L525:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L575
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	.cfi_restore_state
	movq	-2480(%rbp), %rdi
	leaq	-2440(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2464(%rbp), %r8
	movq	%rax, -1104(%rbp)
	movq	%rax, %rdi
	movq	-2440(%rbp), %rax
	movq	%rax, -1088(%rbp)
.L528:
	movq	%r14, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-2440(%rbp), %rax
	movq	-1104(%rbp), %rdx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L529:
	testq	%rax, %rax
	jne	.L576
	movq	-2456(%rbp), %rdx
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L535:
	testq	%r8, %r8
	jne	.L577
	leaq	-2240(%rbp), %rcx
	movq	%r14, %rax
	movq	%rcx, -2464(%rbp)
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L574:
	leaq	-2240(%rbp), %rax
	leaq	-2440(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -2488(%rbp)
	movq	%rax, %rdi
	movq	%r9, -2472(%rbp)
	movq	%rax, -2464(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2472(%rbp), %r9
	movq	-2488(%rbp), %r8
	movq	%rax, -2240(%rbp)
	movq	%rax, %rdi
	movq	-2440(%rbp), %rax
	movq	%rax, -2224(%rbp)
.L534:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-2440(%rbp), %r8
	movq	-2240(%rbp), %rax
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L531:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L575:
	call	__stack_chk_fail@PLT
.L577:
	leaq	-2240(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -2464(%rbp)
	jmp	.L534
.L576:
	movq	-2456(%rbp), %rdi
	jmp	.L528
	.cfi_endproc
.LFE7741:
	.size	_ZN4node10contextify17ContextifyContext15CreateV8ContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE, .-_ZN4node10contextify17ContextifyContext15CreateV8ContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContextC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	.type	_ZN4node10contextify17ContextifyContextC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE, @function
_ZN4node10contextify17ContextifyContextC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE:
.LFB7734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rsi, (%rdi)
	movq	$0, 8(%rdi)
	call	_ZN4node10contextify17ContextifyContext15CreateV8ContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	testq	%rax, %rax
	je	.L578
	movq	8(%r12), %rdi
	movq	352(%rbx), %r14
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L580
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L580:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	leaq	_ZN4node10contextify17ContextifyContext12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	movq	%rax, 8(%r12)
	movq	%rax, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	movq	2640(%rbx), %r14
	movl	$40, %edi
	leaq	1(%r14), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	2592(%rbx), %rsi
	leaq	_ZN4node10contextify17ContextifyContext11CleanupHookEPv(%rip), %r10
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r10, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r14, 24(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%rdi, %rdi
	je	.L581
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L582:
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L581
	movq	32(%r8), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L581
	movq	%r8, %rax
.L584:
	cmpq	%rcx, %r12
	jne	.L582
	cmpq	%r10, 8(%rax)
	jne	.L582
	cmpq	16(%rax), %r12
	jne	.L582
	cmpq	$0, (%rdi)
	je	.L581
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L581:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L585
	movq	2584(%rbx), %r8
	movq	%r12, 32(%r13)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L595
.L624:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r15), %rax
	movq	%r13, (%rax)
.L596:
	addq	$1, 2608(%rbx)
.L578:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L622
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L623
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%rbx), %r10
	movq	%rax, %r8
.L588:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L590
	xorl	%edi, %edi
	leaq	2600(%rbx), %r9
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L592:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L593:
	testq	%rsi, %rsi
	je	.L590
.L591:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L592
	movq	2600(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L599
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L591
	.p2align 4,,10
	.p2align 3
.L590:
	movq	2584(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L594
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L594:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r13)
	divq	%r14
	movq	%r14, 2592(%rbx)
	movq	%r8, 2584(%rbx)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L624
.L595:
	movq	2600(%rbx), %rax
	movq	%r13, 2600(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L597
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r8,%rdx,8)
.L597:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%rdx, %rdi
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	2632(%rbx), %r8
	movq	$0, 2632(%rbx)
	movq	%r8, %r10
	jmp	.L588
.L623:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7734:
	.size	_ZN4node10contextify17ContextifyContextC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE, .-_ZN4node10contextify17ContextifyContextC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	.globl	_ZN4node10contextify17ContextifyContextC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	.set	_ZN4node10contextify17ContextifyContextC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE,_ZN4node10contextify17ContextifyContextC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7752:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L663
	cmpw	$1040, %cx
	jne	.L626
.L663:
	movq	23(%rdx), %r12
.L628:
	cmpl	$5, 16(%rbx)
	je	.L679
	leaq	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L679:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L680
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L681
	movq	8(%rbx), %r14
.L631:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r14, %rdi
	movq	88(%rax), %rdx
	call	_ZN2v86Object10HasPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	testb	%al, %al
	je	.L682
.L632:
	shrw	$8, %ax
	jne	.L683
	movl	16(%rbx), %esi
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	cmpl	$1, %esi
	jg	.L634
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L635:
	movq	(%rax), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L684
.L636:
	leaq	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L681:
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L684:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L636
	movq	%rax, -160(%rbp)
	cmpl	$2, %esi
	jg	.L638
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L639:
	movq	(%rax), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L685
.L640:
	leaq	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L634:
	movq	8(%rbx), %rax
	subq	$8, %rax
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L685:
	movq	-1(%rdx), %rcx
	movzwl	11(%rcx), %ecx
	cmpw	$63, %cx
	ja	.L686
	movq	%rax, -152(%rbp)
.L642:
	cmpl	$3, %esi
	jg	.L643
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L644:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L687
	movl	16(%rbx), %eax
	cmpl	$3, %eax
	jg	.L646
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	movq	%rdi, -144(%rbp)
.L649:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L688
	cmpl	$4, 16(%rbx)
	jg	.L651
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L652:
	movq	352(%r12), %rsi
	leaq	-128(%rbp), %r15
	movq	%rax, -136(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movl	$16, %edi
	movq	%r12, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_Znwm@PLT
	leaq	-160(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN4node10contextify17ContextifyContextC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNS0_14ContextOptionsE
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L689
	movq	8(%r13), %rdx
	testq	%rdx, %rdx
	je	.L655
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L690
.L656:
	movq	352(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v88External3NewEPNS_7IsolateEPv@PLT
	movq	3280(%r12), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%r12), %rax
	movq	88(%rax), %rdx
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
.L625:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L691
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L638:
	.cfi_restore_state
	movq	8(%rbx), %rax
	subq	$16, %rax
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L643:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L646:
	movq	8(%rbx), %rdi
	leaq	-24(%rdi), %rdx
	movq	%rdx, -144(%rbp)
	cmpl	$4, %eax
	je	.L692
	subq	$32, %rdi
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L651:
	movq	8(%rbx), %rax
	subq	$32, %rax
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L626:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L690:
	movq	0(%r13), %rax
	movq	(%rdx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	jne	.L656
	.p2align 4,,10
	.p2align 3
.L655:
	leaq	_ZN4node10contextify17ContextifyContext11CleanupHookEPv(%rip), %rax
	leaq	-192(%rbp), %rsi
	movq	%r13, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	0(%r13), %rax
	movq	$0, -176(%rbp)
	leaq	2584(%rax), %rdi
	call	_ZNSt10_HashtableIN4node19CleanupHookCallbackES1_SaIS1_ENSt8__detail9_IdentityENS1_5EqualENS1_4HashENS3_18_Mod_range_hashingENS3_20_Default_ranged_hashENS3_20_Prime_rehash_policyENS3_17_Hashtable_traitsILb1ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS1_.isra.0.constprop.0
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	je	.L658
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L658:
	movq	%r13, %rdi
	movl	$16, %esi
	call	_ZdlPvm@PLT
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%r15, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L655
	movq	%r15, %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L680:
	leaq	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L682:
	movl	%eax, -196(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-196(%rbp), %eax
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L683:
	leaq	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L687:
	leaq	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L686:
	cmpw	$67, %cx
	jne	.L640
	cmpl	$5, 43(%rdx)
	je	.L642
	jmp	.L640
.L691:
	call	__stack_chk_fail@PLT
.L692:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L649
	.cfi_endproc
.LFE7752:
	.size	_ZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"makeContext"
.LC3:
	.string	"isContext"
.LC4:
	.string	"compileFunction"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.type	_ZN4node10contextify17ContextifyContext4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE, @function
_ZN4node10contextify17ContextifyContext4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE:
.LFB7751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	352(%rdi), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	testq	%rax, %rax
	je	.L715
	movq	2992(%rbx), %rdi
	movq	352(%rbx), %r14
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L706
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2992(%rbx)
.L706:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2992(%rbx)
.L695:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	pushq	$0
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	leaq	_ZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L716
.L697:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L717
.L698:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L718
.L699:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	pushq	$0
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	leaq	_ZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L719
.L700:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L720
.L701:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L721
.L702:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	leaq	_ZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L722
.L703:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L723
.L704:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L724
.L705:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L715:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	2992(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L695
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2992(%rbx)
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L716:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L717:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L718:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L719:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L720:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L721:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L722:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L723:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L724:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L705
	.cfi_endproc
.LFE7751:
	.size	_ZN4node10contextify17ContextifyContext4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE, .-_ZN4node10contextify17ContextifyContext4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext30ContextFromContextifiedSandboxEPNS_11EnvironmentERKN2v85LocalINS4_6ObjectEEE
	.type	_ZN4node10contextify17ContextifyContext30ContextFromContextifiedSandboxEPNS_11EnvironmentERKN2v85LocalINS4_6ObjectEEE, @function
_ZN4node10contextify17ContextifyContext30ContextFromContextifiedSandboxEPNS_11EnvironmentERKN2v85LocalINS4_6ObjectEEE:
.LFB7760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	360(%rdi), %rax
	movq	(%rsi), %r8
	movq	3280(%rdi), %rsi
	movq	88(%rax), %rdx
	movq	%r8, %rdi
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	testq	%rax, %rax
	je	.L730
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value10IsExternalEv@PLT
	testb	%al, %al
	je	.L730
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v88External5ValueEv@PLT
	.p2align 4,,10
	.p2align 3
.L730:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7760:
	.size	_ZN4node10contextify17ContextifyContext30ContextFromContextifiedSandboxEPNS_11EnvironmentERKN2v85LocalINS4_6ObjectEEE, .-_ZN4node10contextify17ContextifyContext30ContextFromContextifiedSandboxEPNS_11EnvironmentERKN2v85LocalINS4_6ObjectEEE
	.section	.rodata.str1.1
.LC5:
	.string	"ContextifyScript"
.LC6:
	.string	"createCachedData"
.LC7:
	.string	"runInContext"
.LC8:
	.string	"runInThisContext"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScript4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.type	_ZN4node10contextify16ContextifyScript4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE, @function
_ZN4node10contextify16ContextifyScript4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE:
.LFB7774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rbp), %rbx
	subq	$56, %rsp
	movq	%rsi, -88(%rbp)
	movq	352(%rdi), %rsi
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r15), %rdi
	xorl	%edx, %edx
	movl	$16, %ecx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L749
.L733:
	subq	$8, %rsp
	movq	2680(%r15), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movq	352(%r15), %rdi
	movl	$1, %r9d
	leaq	_ZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%r15), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%r15), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node10contextify16ContextifyScript16CreateCachedDataERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%r15), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L750
.L734:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%r15), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	352(%r15), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%r15), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L751
.L735:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-96(%rbp), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%r15), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%r15), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	352(%r15), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%r15), %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L752
.L736:
	movq	%r12, %rdi
	movq	%rsi, -96(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-96(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-96(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%r15), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L753
.L737:
	movq	3280(%r15), %rsi
	movq	-88(%rbp), %rdi
	movq	%r13, %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L754
.L738:
	movq	3216(%r15), %rdi
	movq	352(%r15), %r13
	testq	%rdi, %rdi
	je	.L739
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3216(%r15)
.L739:
	testq	%r12, %r12
	je	.L740
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3216(%r15)
.L740:
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L755
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L749:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rsi
	jmp	.L734
	.p2align 4,,10
	.p2align 3
.L751:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rsi
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%rsi, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rsi
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L753:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rcx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L754:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L738
.L755:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7774:
	.size	_ZN4node10contextify16ContextifyScript4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE, .-_ZN4node10contextify16ContextifyScript4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.section	.rodata.str1.1
.LC9:
	.string	"startSigintWatchdog"
.LC10:
	.string	"stopSigintWatchdog"
.LC11:
	.string	"watchdogHasPendingSigint"
.LC12:
	.string	"CompiledFnEntry"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB13:
	.text
.LHOTB13:
	.p2align 4
	.globl	_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7820:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L757
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L757
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L757
	movq	271(%rax), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node10contextify17ContextifyContext4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node10contextify16ContextifyScript4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node10contextifyL19StartSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L780
.L758:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L781
.L759:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L782
.L760:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node10contextifyL18StopSigintWatchdogERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L783
.L761:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L784
.L762:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L785
.L763:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node10contextifyL24WatchdogHasPendingSigintERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L786
.L764:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L787
.L765:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L788
.L766:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	movl	$15, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L789
.L767:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	3080(%rbx), %rdi
	movq	352(%rbx), %r13
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L768
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3080(%rbx)
.L768:
	testq	%r12, %r12
	je	.L756
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3080(%rbx)
.L756:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L781:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L782:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L760
	.p2align 4,,10
	.p2align 3
.L783:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L784:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L785:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L763
	.p2align 4,,10
	.p2align 3
.L786:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L787:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L788:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L789:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L767
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7820:
.L757:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	_ZN4node10contextify17ContextifyContext4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	_ZN4node10contextify16ContextifyScript4InitEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7820:
	.text
	.size	_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE13:
	.text
.LHOTE13:
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScript10InstanceOfEPNS_11EnvironmentERKN2v85LocalINS4_5ValueEEE
	.type	_ZN4node10contextify16ContextifyScript10InstanceOfEPNS_11EnvironmentERKN2v85LocalINS4_5ValueEEE, @function
_ZN4node10contextify16ContextifyScript10InstanceOfEPNS_11EnvironmentERKN2v85LocalINS4_5ValueEEE:
.LFB7778:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	je	.L791
	movq	3216(%rdi), %rdi
	movq	(%rsi), %rsi
	jmp	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L791:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7778:
	.size	_ZN4node10contextify16ContextifyScript10InstanceOfEPNS_11EnvironmentERKN2v85LocalINS4_5ValueEEE, .-_ZN4node10contextify16ContextifyScript10InstanceOfEPNS_11EnvironmentERKN2v85LocalINS4_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify15CompiledFnEntryC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE
	.type	_ZN4node10contextify15CompiledFnEntryC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE, @function
_ZN4node10contextify15CompiledFnEntryC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE:
.LFB7811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	movq	%r8, -56(%rbp)
	testq	%rdx, %rdx
	je	.L793
	movq	%rdx, %rsi
	movq	%rdx, %r14
	movl	%ecx, %r15d
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r14, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L837
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %r13
	movl	$40, %edi
	leaq	1(%r13), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r11
	movq	2592(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r14
	movq	%r11, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r13, 24(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r10
	testq	%r8, %r8
	je	.L795
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L796:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L795
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L795
	movq	%rdi, %rax
.L798:
	cmpq	%rcx, %r12
	jne	.L796
	cmpq	%r11, 8(%rax)
	jne	.L796
	cmpq	16(%rax), %r12
	jne	.L796
	cmpq	$0, (%r8)
	je	.L795
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L795:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	movq	%r10, -64(%rbp)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r8
	testb	%al, %al
	jne	.L799
	movq	2584(%rbx), %r9
	movq	-64(%rbp), %r10
	movq	%r12, 32(%r14)
	addq	%r9, %r10
	movq	(%r10), %rax
	testq	%rax, %rax
	je	.L809
.L840:
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	(%r10), %rax
	movq	%r14, (%rax)
.L810:
	movq	-56(%rbp), %rdi
	leaq	16+_ZTVN4node10contextify15CompiledFnEntryE(%rip), %rax
	addq	$1, 2656(%rbx)
	addq	$1, 2608(%rbx)
	movq	352(%rbx), %r8
	movq	%rax, (%r12)
	movl	%r15d, 32(%r12)
	testq	%rdi, %rdi
	je	.L812
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %rdi
.L812:
	movq	%rdi, 40(%r12)
	addq	$40, %rsp
	movq	%r12, %rsi
	xorl	%ecx, %ecx
	popq	%rbx
	leaq	_ZN4node10contextify15CompiledFnEntry12WeakCallbackERKN2v816WeakCallbackInfoIS1_EE(%rip), %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L838
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L839
	leaq	0(,%rdx,8), %rdx
	movq	%r8, -72(%rbp)
	movq	%rdx, %rdi
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r9
	leaq	2632(%rbx), %rax
	movq	%rax, %r13
.L802:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L804
	xorl	%r10d, %r10d
	leaq	2600(%rbx), %r11
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L806:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L807:
	testq	%rsi, %rsi
	je	.L804
.L805:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r8
	leaq	(%r9,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L806
	movq	2600(%rbx), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r11, (%rax)
	cmpq	$0, (%rcx)
	je	.L814
	movq	%rcx, (%r9,%r10,8)
	movq	%rdx, %r10
	testq	%rsi, %rsi
	jne	.L805
	.p2align 4,,10
	.p2align 3
.L804:
	movq	2584(%rbx), %rdi
	cmpq	%r13, %rdi
	je	.L808
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
.L808:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r14)
	divq	%r8
	movq	%r8, 2592(%rbx)
	movq	%r9, 2584(%rbx)
	leaq	0(,%rdx,8), %r10
	addq	%r9, %r10
	movq	(%r10), %rax
	testq	%rax, %rax
	jne	.L840
.L809:
	movq	2600(%rbx), %rax
	movq	%r14, 2600(%rbx)
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.L811
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r14, (%r9,%rdx,8)
.L811:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r10)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L814:
	movq	%rdx, %r10
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L793:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L837:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L838:
	leaq	2632(%rbx), %r9
	movq	$0, 2632(%rbx)
	movq	%r9, %r13
	jmp	.L802
.L839:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7811:
	.size	_ZN4node10contextify15CompiledFnEntryC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE, .-_ZN4node10contextify15CompiledFnEntryC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE
	.globl	_ZN4node10contextify15CompiledFnEntryC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE
	.set	_ZN4node10contextify15CompiledFnEntryC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE,_ZN4node10contextify15CompiledFnEntryC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE
	.p2align 4
	.globl	_Z20_register_contextifyv
	.type	_Z20_register_contextifyv, @function
_Z20_register_contextifyv:
.LFB7821:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7821:
	.size	_Z20_register_contextifyv, .-_Z20_register_contextifyv
	.section	.text._ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB10061:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L843
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L853
.L869:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L854:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L843:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L867
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L868
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L846:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L848
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L850:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L851:
	testq	%rsi, %rsi
	je	.L848
.L849:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movl	8(%rcx), %eax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L850
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L856
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L849
	.p2align 4,,10
	.p2align 3
.L848:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L852
	call	_ZdlPv@PLT
.L852:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L869
.L853:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L855
	movl	8(%rax), %eax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L855:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L856:
	movq	%rdx, %rdi
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L867:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L846
.L868:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10061:
	.size	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScriptC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.type	_ZN4node10contextify16ContextifyScriptC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE, @function
_ZN4node10contextify16ContextifyScriptC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE:
.LFB7784:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10BaseObjectE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rax, (%rdi)
	movq	352(%rsi), %rdi
	testq	%rdx, %rdx
	je	.L871
	movq	%rdx, %rsi
	movq	%rdx, %r13
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rbx, %xmm1
	movq	%r13, %rdi
	movq	$0, 24(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L921
	movq	%r12, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2640(%rbx), %r14
	movl	$40, %edi
	leaq	1(%r14), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	movq	_ZN4node10BaseObject8DeleteMeEPv@GOTPCREL(%rip), %r10
	movq	2592(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %r13
	movq	%r10, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%r14, 24(%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%r8, %r8
	je	.L873
	movq	(%r8), %rax
	movq	32(%rax), %rcx
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L874:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L873
	movq	32(%rdi), %rcx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L873
	movq	%rdi, %rax
.L876:
	cmpq	%rcx, %r12
	jne	.L874
	cmpq	%r10, 8(%rax)
	jne	.L874
	cmpq	16(%rax), %r12
	jne	.L874
	cmpq	$0, (%r8)
	je	.L873
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L873:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L877
	movq	2584(%rbx), %r8
	movq	%r12, 32(%r13)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L887
.L925:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r15), %rax
	movq	%r13, (%rax)
.L888:
	leaq	16+_ZTVN4node10contextify16ContextifyScriptE(%rip), %rax
	addq	$1, 2656(%rbx)
	movq	%rax, (%r12)
	movl	1756(%rbx), %eax
	addq	$1, 2608(%rbx)
	leal	1(%rax), %edx
	movl	%eax, 40(%r12)
	movq	24(%r12), %rax
	movq	$0, 32(%r12)
	movl	%edx, 1756(%rbx)
	testq	%rax, %rax
	je	.L892
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L891
.L892:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L891:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	40(%r12), %r11d
	movq	224(%rbx), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	%r11d, 8(%rax)
	movq	%r11, %r9
	movq	%r12, 16(%rax)
	movq	%r11, %rax
	divq	%rsi
	movq	216(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L893
	movq	(%rax), %rcx
	movl	8(%rcx), %r8d
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L922:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L893
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %r8
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L893
.L895:
	cmpl	%r8d, %r9d
	jne	.L922
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L893:
	.cfi_restore_state
	addq	$24, %rsp
	movq	%rdi, %rcx
	movl	$1, %r8d
	movq	%r11, %rdx
	leaq	216(%rbx), %r9
	movq	%r10, %rsi
	popq	%rbx
	popq	%r12
	movq	%r9, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify16ContextifyScriptEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.p2align 4,,10
	.p2align 3
.L877:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L923
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L924
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%rbx), %r10
	movq	%rax, %r8
.L880:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L882
	xorl	%edi, %edi
	leaq	2600(%rbx), %r9
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L884:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L885:
	testq	%rsi, %rsi
	je	.L882
.L883:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L884
	movq	2600(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L897
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L883
	.p2align 4,,10
	.p2align 3
.L882:
	movq	2584(%rbx), %rdi
	cmpq	%rdi, %r10
	je	.L886
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L886:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%r12, 32(%r13)
	divq	%r14
	movq	%r14, 2592(%rbx)
	movq	%r8, 2584(%rbx)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L925
.L887:
	movq	2600(%rbx), %rax
	movq	%r13, 2600(%rbx)
	movq	%rax, 0(%r13)
	testq	%rax, %rax
	je	.L889
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r13, (%r8,%rdx,8)
.L889:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L897:
	movq	%rdx, %rdi
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L871:
	movq	$0, 8(%r12)
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	movq	%rsi, 16(%r12)
	movq	$0, 24(%r12)
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L921:
	leaq	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L923:
	leaq	2632(%rbx), %r8
	movq	$0, 2632(%rbx)
	movq	%r8, %r10
	jmp	.L880
.L924:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7784:
	.size	_ZN4node10contextify16ContextifyScriptC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE, .-_ZN4node10contextify16ContextifyScriptC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.globl	_ZN4node10contextify16ContextifyScriptC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.set	_ZN4node10contextify16ContextifyScriptC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE,_ZN4node10contextify16ContextifyScriptC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	.section	.rodata.str1.1
.LC14:
	.string	"node,node.vm,node.vm.script"
.LC15:
	.string	"filename"
.LC16:
	.string	"ContextifyScript::New"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1320, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1034
	cmpw	$1040, %cx
	jne	.L927
.L1034:
	movq	23(%rdx), %r12
.L929:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L930
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1111
.L930:
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L1112
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1112:
	movq	8(%rbx), %rsi
	movq	(%rsi), %rdx
	movq	%rsi, -1272(%rbp)
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L931
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L931
	movq	-8(%rsi), %rdx
	leaq	-8(%rsi), %rcx
	movq	%rcx, -1304(%rbp)
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1113
.L933:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1111:
	cmpl	$5, 43(%rax)
	jne	.L930
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	ja	.L933
	movq	3280(%r12), %rcx
	movq	352(%r12), %r13
	movq	%rcx, -1288(%rbp)
	cmpl	$2, %eax
	je	.L935
	cmpl	$7, %eax
	jne	.L1114
	leaq	-16(%rsi), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1115
	movl	16(%rbx), %eax
	cmpl	$2, %eax
	jle	.L1116
	movq	8(%rbx), %rdi
	leaq	-16(%rdi), %rsi
	movq	%rsi, -1328(%rbp)
	cmpl	$3, %eax
	je	.L1117
	subq	$24, %rdi
.L941:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1118
	movl	16(%rbx), %edx
	cmpl	$3, %edx
	jg	.L943
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -1336(%rbp)
	movq	%rax, %rdi
.L946:
	movq	(%rdi), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L947
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L947
	cmpl	$5, 43(%rax)
	je	.L1023
	.p2align 4,,10
	.p2align 3
.L947:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1119
	movl	16(%rbx), %edx
	cmpl	$4, %edx
	jg	.L950
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r15
	movq	%r15, %rdi
.L953:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1120
	cmpl	$5, 16(%rbx)
	jg	.L955
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L956:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$6, 16(%rbx)
	movb	%al, -1337(%rbp)
	jg	.L957
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L958:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L959
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L959
	cmpl	$5, 43(%rax)
	je	.L960
	.p2align 4,,10
	.p2align 3
.L959:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1121
	cmpl	$6, 16(%rbx)
	jg	.L962
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L963:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	88(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L965
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsExternalEv@PLT
	testb	%al, %al
	je	.L965
	movq	%r14, %rdi
	call	_ZNK2v88External5ValueEv@PLT
	testq	%rax, %rax
	je	.L965
	movq	8(%rax), %rsi
	movq	%rsi, -1288(%rbp)
	testq	%rsi, %rsi
	je	.L960
	movzbl	11(%rsi), %ecx
	movl	%ecx, %edx
	movb	%cl, -1280(%rbp)
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L1122
.L960:
	movq	8(%rbx), %rax
	movl	$48, %edi
	leaq	8(%rax), %r14
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1280(%rbp)
	call	_ZN4node10contextify16ContextifyScriptC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L969
	leaq	-1184(%rbp), %rax
	movq	%rax, -1296(%rbp)
	leaq	-1248(%rbp), %rax
	movq	%rax, -1320(%rbp)
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L935:
	xorl	%esi, %esi
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movl	$48, %edi
	movq	%rax, -1336(%rbp)
	movq	8(%rbx), %rax
	leaq	8(%rax), %r14
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, -1280(%rbp)
	call	_ZN4node10contextify16ContextifyScriptC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEE
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1024
	movb	$0, -1337(%rbp)
.L969:
	movq	(%rdi), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %r14
	movq	16(%rax), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	cmpq	%r14, %rdx
	jne	.L1123
.L971:
	leaq	-1184(%rbp), %rsi
	leaq	-1248(%rbp), %rcx
	cmpb	$0, (%rax)
	movq	%rsi, -1296(%rbp)
	movq	%rcx, -1320(%rbp)
	jne	.L1124
.L970:
	movl	$0, -1312(%rbp)
	testq	%r15, %r15
	je	.L968
	movq	%r15, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	-1296(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r15, %rdi
	movq	-1184(%rbp), %r14
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r15, %rdi
	addq	%rax, %r14
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movl	$24, %edi
	movq	%rax, -1312(%rbp)
	call	_Znwm@PLT
	movq	-1312(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v814ScriptCompiler10CachedDataC1EPKhiNS1_12BufferPolicyE@PLT
	movl	$1, -1312(%rbp)
.L968:
	movl	$10, %esi
	movq	%r13, %rdi
	call	_ZN2v814PrimitiveArray3NewEPNS_7IsolateEi@PLT
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$8, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v814PrimitiveArray3SetEPNS_7IsolateEiNS_5LocalINS_9PrimitiveEEE@PLT
	movq	-1280(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movl	40(%rax), %eax
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdi
	movl	$9, %edx
	movq	%r13, %rsi
	movq	%rax, %rcx
	call	_ZN2v814PrimitiveArray3SetEPNS_7IsolateEiNS_5LocalINS_9PrimitiveEEE@PLT
	leaq	120(%r13), %rax
	leaq	112(%r13), %rdi
	movq	%rax, -1360(%rbp)
	movq	%rdi, -1352(%rbp)
	cmpq	$-120, %r13
	je	.L1125
	leaq	120(%r13), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	leaq	120(%r13), %rdi
	movb	%al, -1338(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	leaq	120(%r13), %rdi
	movb	%al, -1339(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpq	$-112, %r13
	jne	.L1126
.L984:
	movl	%eax, %ecx
	orl	$4, %ecx
	cmpb	$0, -1339(%rbp)
	cmovne	%ecx, %eax
	movl	%eax, %ecx
	orl	$2, %ecx
	testb	%dl, %dl
	cmovne	%ecx, %eax
	movl	%eax, %edx
	orl	$8, %edx
	cmpb	$0, -1338(%rbp)
	cmovne	%edx, %eax
.L983:
	movq	-1272(%rbp), %xmm0
	movq	-1296(%rbp), %rdi
	movl	%eax, -1216(%rbp)
	movq	%r14, -1200(%rbp)
	movhps	-1304(%rbp), %xmm0
	movq	%r15, -1192(%rbp)
	movaps	%xmm0, -1248(%rbp)
	movq	-1328(%rbp), %xmm0
	movq	$0, -1208(%rbp)
	movhps	-1336(%rbp), %xmm0
	movaps	%xmm0, -1232(%rbp)
	movq	352(%r12), %rsi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%r12, -1136(%rbp)
	movq	-1288(%rbp), %rdi
	movl	$0, -1128(%rbp)
	addl	$1, 1808(%r12)
	call	_ZN2v87Context5EnterEv@PLT
	movl	-1312(%rbp), %edx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	-1320(%rbp), %rsi
	call	_ZN2v814ScriptCompiler20CompileUnboundScriptEPNS_7IsolateEPNS0_6SourceENS0_14CompileOptionsENS0_13NoCacheReasonE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1127
	movq	-1280(%rbp), %rax
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.L998
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-1280(%rbp), %rax
	movq	$0, 32(%rax)
.L998:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	-1280(%rbp), %rsi
	movq	%rax, 32(%rsi)
	testq	%r15, %r15
	jne	.L1128
	cmpb	$0, -1337(%rbp)
	jne	.L1129
.L1002:
	movq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic781(%rip), %r13
	testq	%r13, %r13
	je	.L1130
.L1010:
	testb	$5, 0(%r13)
	jne	.L1131
.L1012:
	movq	-1288(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	subl	$1, 1808(%r12)
.L1110:
	movq	-1296(%rbp), %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-1192(%rbp), %r12
	testq	%r12, %r12
	je	.L926
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L926:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1132
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L931:
	.cfi_restore_state
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1116:
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -1328(%rbp)
	movq	%rax, %rdi
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L927:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r12
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L1125:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	%al, %eax
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L943:
	movq	8(%rbx), %rdi
	leaq	-24(%rdi), %rax
	movq	%rax, -1336(%rbp)
	cmpl	$4, %edx
	je	.L1133
	subq	$32, %rdi
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	-1304(%rbp), %rdx
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic697(%rip), %rax
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L1134
.L973:
	leaq	-1184(%rbp), %rax
	movq	-1088(%rbp), %rdi
	movq	%rax, -1296(%rbp)
	leaq	-1248(%rbp), %rax
	movq	%rax, -1320(%rbp)
	testb	$5, (%r10)
	jne	.L1135
.L975:
	testq	%rdi, %rdi
	je	.L970
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L970
	call	free@PLT
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1128:
	movq	-1192(%rbp), %rax
	movq	8(%rbx), %rdi
	movq	3280(%r12), %rsi
	addq	$8, %rdi
	cmpb	$0, 12(%rax)
	movq	-1360(%rbp), %rax
	cmovne	-1352(%rbp), %rax
	movq	%rax, %rcx
	movq	360(%r12), %rax
	movq	272(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1002
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L957:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L955:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1131:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1104(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1013
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1136
.L1013:
	movq	-1096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1014
	movq	(%rdi), %rax
	call	*8(%rax)
.L1014:
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1012
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1012
	.p2align 4,,10
	.p2align 3
.L1130:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1011
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1137
.L1011:
	movq	%r13, _ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic781(%rip)
	mfence
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L950:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %r15
.L948:
	cmpl	$5, %edx
	jle	.L1138
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L962:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1129:
	movq	%r14, %rdi
	call	_ZN2v814ScriptCompiler15CreateCodeCacheENS_5LocalINS_13UnboundScriptEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1004
	movslq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	%r12, %rdi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	movq	%rax, %rcx
	movq	8(%rbx), %rax
	leaq	8(%rax), %rdi
	testq	%rcx, %rcx
	je	.L1139
.L1005:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	280(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1140
.L1006:
	movq	8(%rbx), %rdi
	addq	$8, %rdi
.L1017:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	-1352(%rbp), %rcx
	movq	264(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1141
.L1007:
	testq	%r13, %r13
	je	.L1002
	movq	%r13, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1123:
	leaq	.LC14(%rip), %rsi
	call	*%rdx
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	(%rax), %rax
	movq	(%rsi), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, -1288(%rbp)
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1023:
	xorl	%r15d, %r15d
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1004:
	movq	8(%rbx), %rdi
	movq	-1360(%rbp), %rax
	addq	$8, %rdi
	movq	%rax, -1352(%rbp)
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1024:
	leaq	-1184(%rbp), %rax
	movb	$0, -1337(%rbp)
	movq	%rax, -1296(%rbp)
	leaq	-1248(%rbp), %rax
	movl	$0, -1312(%rbp)
	movq	%rax, -1320(%rbp)
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L1135:
	leaq	.LC15(%rip), %rax
	pxor	%xmm0, %xmm0
	movq	%rdi, -1184(%rbp)
	movq	%r10, -1312(%rbp)
	movq	%rax, -1248(%rbp)
	movb	$7, -1249(%rbp)
	movaps	%xmm0, -1120(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L976
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	-1312(%rbp), %r10
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1142
.L976:
	movq	-1112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L977
	movq	(%rdi), %rax
	call	*8(%rax)
.L977:
	movq	-1120(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L978
	movq	(%rdi), %rax
	call	*8(%rax)
.L978:
	movq	-1088(%rbp), %rdi
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1134:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r10
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L974
	movq	(%rax), %rax
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r10
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L1143
.L974:
	movq	%r10, _ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic697(%rip)
	mfence
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1114:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1127:
	movq	-1296(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE@PLT
	movq	%rbx, %rdi
	subl	$1, 1808(%r12)
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	je	.L1144
.L988:
	movq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic750(%rip), %r12
	testq	%r12, %r12
	je	.L1145
.L990:
	testb	$5, (%r12)
	jne	.L1146
.L992:
	movq	-1288(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1115:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1126:
	movq	-1352(%rbp), %rdi
	movb	%dl, -1340(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	-1340(%rbp), %edx
	movzbl	%al, %eax
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1120:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1119:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1137:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1011
	.p2align 4,,10
	.p2align 3
.L1121:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1136:
	subq	$8, %rsp
	leaq	-1104(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	-1280(%rbp), %r9
	pushq	$6
	leaq	.LC16(%rip), %rcx
	movl	$101, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L965:
	leaq	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1138:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1146:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1104(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L993
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1147
.L993:
	movq	-1096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L994
	movq	(%rdi), %rax
	call	*8(%rax)
.L994:
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L992
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1145:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L991
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1148
.L991:
	movq	%r12, _ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic750(%rip)
	mfence
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1144:
	movq	-1296(%rbp), %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L988
.L1141:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1007
.L1143:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r10
	jmp	.L974
.L1142:
	subq	$8, %rsp
	leaq	-1120(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	-1280(%rbp), %r9
	pushq	$6
	leaq	.LC16(%rip), %rcx
	movl	$98, %esi
	pushq	%rdx
	leaq	-1249(%rbp), %rdx
	pushq	-1296(%rbp)
	pushq	%rdx
	movq	%r10, %rdx
	pushq	-1320(%rbp)
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L976
.L1140:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1006
.L1139:
	movq	%rdi, -1304(%rbp)
	movq	%rcx, -1272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1304(%rbp), %rdi
	movq	-1272(%rbp), %rcx
	jmp	.L1005
.L1148:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L991
.L1147:
	subq	$8, %rsp
	leaq	-1104(%rbp), %rdx
	xorl	%r8d, %r8d
	movq	-1280(%rbp), %r9
	pushq	$6
	leaq	.LC16(%rip), %rcx
	movl	$101, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L993
.L1132:
	call	__stack_chk_fail@PLT
.L1133:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L946
.L1117:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L941
	.cfi_endproc
.LFE7775:
	.size	_ZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.type	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, @function
_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm:
.LFB10129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L1150
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1160
.L1176:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L1161:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1150:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1174
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1175
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L1153:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1155
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1158:
	testq	%rsi, %rsi
	je	.L1155
.L1156:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movl	8(%rcx), %eax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L1157
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L1163
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1156
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L1159
	call	_ZdlPv@PLT
.L1159:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L1176
.L1160:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1162
	movl	8(%rax), %eax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L1162:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	%rdx, %rdi
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1174:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L1153
.L1175:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10129:
	.size	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm, .-_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	.section	.rodata.str1.1
.LC19:
	.string	"vector::_M_realloc_insert"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7790:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1331
	cmpw	$1040, %cx
	jne	.L1178
.L1331:
	movq	23(%rdx), %rax
	movq	%rax, -224(%rbp)
.L1180:
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L1416
	movq	8(%rbx), %r12
	movq	(%r12), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1417
.L1183:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	movq	88(%r12), %rax
	addq	$88, %r12
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1183
.L1417:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1183
	cmpl	$1, %ecx
	jg	.L1185
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -240(%rbp)
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1418
.L1187:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1185:
	movq	8(%rbx), %rax
	subq	$8, %rax
	movq	%rax, -240(%rbp)
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1187
.L1418:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1187
	cmpl	$2, %ecx
	jg	.L1189
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1190:
	movq	-224(%rbp), %rax
	movq	352(%rax), %rsi
	movq	3280(%rax), %rax
	movq	%rsi, -216(%rbp)
	movq	%rax, -232(%rbp)
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1419
	movl	16(%rbx), %eax
	cmpl	$2, %eax
	jg	.L1192
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -256(%rbp)
	movq	%rax, %rdi
.L1195:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1420
	movl	16(%rbx), %eax
	cmpl	$3, %eax
	jg	.L1197
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	leaq	88(%rdx), %rsi
	movq	%rsi, -264(%rbp)
	movq	%rsi, %rdi
.L1200:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1201
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L1421
.L1201:
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1422
	movl	16(%rbx), %eax
	cmpl	$4, %eax
	jg	.L1204
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	movq	%r13, %rdi
.L1207:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1423
	cmpl	$5, 16(%rbx)
	jg	.L1209
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1210:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, -285(%rbp)
	movl	16(%rbx), %eax
	cmpl	$6, %eax
	jg	.L1211
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1212:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1213
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	je	.L1424
.L1213:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1425
	cmpl	$6, 16(%rbx)
	jg	.L1216
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1217:
	movq	-224(%rbp), %rsi
	movq	360(%rsi), %rax
	movq	3280(%rsi), %rsi
	movq	88(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1219
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsExternalEv@PLT
	testb	%al, %al
	je	.L1219
	movq	%r14, %rdi
	call	_ZNK2v88External5ValueEv@PLT
	testq	%rax, %rax
	je	.L1219
	movq	8(%rax), %rsi
	movq	%rsi, -248(%rbp)
	testq	%rsi, %rsi
	je	.L1221
	movzbl	11(%rsi), %esi
	movl	%esi, %edx
	movb	%sil, -272(%rbp)
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L1426
.L1221:
	movl	16(%rbx), %eax
.L1214:
	cmpl	$7, %eax
	jg	.L1222
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1223:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1224
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1224
	cmpl	$5, 43(%rdx)
	jne	.L1224
	xorl	%r15d, %r15d
.L1225:
	cmpl	$8, %eax
	jle	.L1427
	movq	8(%rbx), %rax
	leaq	-64(%rax), %rdi
.L1230:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1231
.L1436:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1231
	cmpl	$5, 43(%rax)
	jne	.L1231
	movq	$0, -296(%rbp)
.L1232:
	testq	%r13, %r13
	je	.L1316
.L1438:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rsi
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -280(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	%r13, %rdi
	movq	-128(%rbp), %r14
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	movq	%r13, %rdi
	addq	%rax, %r14
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movl	$24, %edi
	movq	%rax, -272(%rbp)
	call	_Znwm@PLT
	movq	-272(%rbp), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v814ScriptCompiler10CachedDataC1EPKhiNS1_12BufferPolicyE@PLT
	movl	$1, -300(%rbp)
.L1235:
	movq	-224(%rbp), %rsi
	movq	-216(%rbp), %rdi
	movl	1760(%rsi), %eax
	movl	%eax, -284(%rbp)
	addl	$1, %eax
	movl	%eax, 1760(%rsi)
	movl	$10, %esi
	call	_ZN2v814PrimitiveArray3NewEPNS_7IsolateEi@PLT
	movsd	.LC18(%rip), %xmm0
	movq	-216(%rbp), %rdi
	movq	%rax, %r14
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-216(%rbp), %rsi
	movl	$8, %edx
	movq	%r14, %rdi
	movq	%rax, %rcx
	call	_ZN2v814PrimitiveArray3SetEPNS_7IsolateEiNS_5LocalINS_9PrimitiveEEE@PLT
	movl	-284(%rbp), %ecx
	pxor	%xmm0, %xmm0
	movq	-216(%rbp), %rdi
	cvtsi2sdq	%rcx, %xmm0
	movq	%rcx, -312(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-216(%rbp), %rsi
	movq	%r14, %rdi
	movl	$9, %edx
	movq	%rax, %rcx
	call	_ZN2v814PrimitiveArray3SetEPNS_7IsolateEiNS_5LocalINS_9PrimitiveEEE@PLT
	movq	-216(%rbp), %rax
	leaq	120(%rax), %rsi
	leaq	112(%rax), %rdi
	movq	%rsi, -272(%rbp)
	movq	%rdi, -320(%rbp)
	cmpq	$-120, %rax
	je	.L1428
	movq	%rsi, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	-272(%rbp), %rdi
	movb	%al, -328(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	-272(%rbp), %rdi
	movb	%al, -336(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpq	$-112, -216(%rbp)
	jne	.L1429
.L1240:
	movl	%eax, %ecx
	orl	$4, %ecx
	cmpb	$0, -336(%rbp)
	cmovne	%ecx, %eax
	movl	%eax, %ecx
	orl	$2, %ecx
	testb	%dl, %dl
	cmovne	%ecx, %eax
	movl	%eax, %edx
	orl	$8, %edx
	cmpb	$0, -328(%rbp)
	cmovne	%edx, %eax
.L1239:
	movq	%r12, %xmm0
	movq	%r14, -144(%rbp)
	movq	-224(%rbp), %r14
	movhps	-240(%rbp), %xmm0
	movq	-280(%rbp), %rdi
	movl	%eax, -160(%rbp)
	movaps	%xmm0, -192(%rbp)
	movq	352(%r14), %rsi
	movq	-256(%rbp), %xmm0
	movq	$0, -152(%rbp)
	movhps	-264(%rbp), %xmm0
	movq	%r13, -136(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-248(%rbp), %rdi
	movq	%r14, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	testq	%r15, %r15
	je	.L1318
	movq	%rbx, -264(%rbp)
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	movq	$0, -240(%rbp)
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1430
	cmpq	%r12, -240(%rbp)
	je	.L1246
	movq	%rbx, (%r12)
	addq	$8, %r12
.L1247:
	addl	$1, %r13d
.L1258:
	movq	%r15, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r13d, %eax
	jbe	.L1319
	movq	-232(%rbp), %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L1399
	movq	%r14, %r15
.L1302:
	testq	%r15, %r15
	je	.L1297
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L1297:
	movq	-248(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-280(%rbp), %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L1177
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1177:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1431
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1189:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1192:
	movq	8(%rbx), %rdi
	leaq	-16(%rdi), %rsi
	movq	%rsi, -256(%rbp)
	cmpl	$3, %eax
	je	.L1432
	subq	$24, %rdi
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	8(%rbx), %rdi
	leaq	-24(%rdi), %rsi
	movq	%rsi, -264(%rbp)
	cmpl	$4, %eax
	je	.L1433
	subq	$32, %rdi
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1211:
	movq	8(%rbx), %rsi
	leaq	-48(%rsi), %rdi
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1209:
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	8(%rbx), %rsi
	leaq	-32(%rsi), %r13
.L1202:
	cmpl	$5, %eax
	jle	.L1434
	movq	8(%rbx), %rax
	leaq	-40(%rax), %rdi
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1216:
	movq	8(%rbx), %rax
	leaq	-48(%rax), %rdi
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1421:
	cmpl	$5, 43(%rdx)
	jne	.L1201
	xorl	%r13d, %r13d
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1424:
	cmpl	$5, 43(%rdx)
	jne	.L1213
	movq	-232(%rbp), %rsi
	movq	%rsi, -248(%rbp)
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1178:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -224(%rbp)
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1224:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L1435
	movl	16(%rbx), %eax
	cmpl	$7, %eax
	jg	.L1227
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	leaq	88(%rax), %r15
	movq	%r15, %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L1436
.L1231:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L1437
	cmpl	$8, 16(%rbx)
	jg	.L1234
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	%rax, -296(%rbp)
	testq	%r13, %r13
	jne	.L1438
.L1316:
	movl	$0, -300(%rbp)
	leaq	-128(%rbp), %rax
	movq	%rax, -280(%rbp)
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1246:
	movabsq	$1152921504606846975, %rcx
	movq	%r12, %rsi
	subq	%r14, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1266
	testq	%rax, %rax
	je	.L1439
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L1440
	testq	%rcx, %rcx
	jne	.L1441
	movq	$0, -240(%rbp)
	movl	$8, %ecx
	xorl	%eax, %eax
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1222:
	movq	8(%rbx), %rsi
	leaq	-56(%rsi), %rdi
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	8(%rbx), %rsi
	leaq	-56(%rsi), %r15
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1428:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	%al, %eax
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1234:
	movq	8(%rbx), %rax
	subq	$64, %rax
	movq	%rax, -296(%rbp)
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1430:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1318:
	xorl	%r12d, %r12d
.L1243:
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L1324
	movq	%r15, -256(%rbp)
	xorl	%esi, %esi
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	-232(%rbp), %r15
	movq	%r12, -328(%rbp)
	movq	%rsi, %r12
	movq	$0, -240(%rbp)
	movq	%rbx, -336(%rbp)
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L1276:
	movq	%rbx, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	cmpl	%r14d, %eax
	jbe	.L1442
	movl	%r14d, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1443
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1444
.L1304:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1304
	cmpq	%r13, -240(%rbp)
	je	.L1264
	movq	%rdx, 0(%r13)
	addq	$8, %r13
.L1265:
	addl	$1, %r14d
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	%r12, -296(%rbp)
	movq	-256(%rbp), %r15
.L1278:
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L1302
.L1415:
	movq	%rax, %rdi
	call	_ZdlPv@PLT
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1264:
	movabsq	$1152921504606846975, %rcx
	movq	%r13, %rsi
	subq	%r12, %rsi
	movq	%rsi, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L1266
	testq	%rax, %rax
	je	.L1445
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	ja	.L1446
	testq	%rcx, %rcx
	jne	.L1447
	movq	$0, -240(%rbp)
	movl	$8, %ecx
	xorl	%eax, %eax
.L1269:
	movq	%rdx, (%rax,%rsi)
	cmpq	%r13, %r12
	je	.L1327
	leaq	-8(%r13), %rsi
	leaq	15(%rax), %rdx
	subq	%r12, %rsi
	subq	%r12, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1328
	movabsq	$2305843009213693948, %rcx
	testq	%rcx, %rdi
	je	.L1328
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1272:
	movdqu	(%r12,%rdx), %xmm2
	movups	%xmm2, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	jne	.L1272
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%r12,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %r8
	je	.L1274
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L1274:
	leaq	16(%rax,%rsi), %r13
.L1270:
	testq	%r12, %r12
	je	.L1275
	movq	%r12, %rdi
	movq	%rax, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %rax
.L1275:
	movq	%rax, %r12
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	(%rax), %rax
	movq	352(%rax), %rdi
	movq	-248(%rbp), %rax
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, -248(%rbp)
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1419:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1439:
	movl	$8, %ecx
.L1249:
	movq	%rcx, %rdi
	movq	%rsi, -256(%rbp)
	movq	%rcx, -240(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %rcx
	leaq	(%rax,%rcx), %rsi
	leaq	8(%rax), %rcx
	movq	%rsi, -240(%rbp)
	movq	-256(%rbp), %rsi
.L1251:
	movq	%rbx, (%rax,%rsi)
	cmpq	%r14, %r12
	je	.L1322
	leaq	-8(%r12), %rsi
	leaq	15(%rax), %rdx
	subq	%r14, %rsi
	subq	%r14, %rdx
	movq	%rsi, %rdi
	shrq	$3, %rdi
	cmpq	$30, %rdx
	jbe	.L1323
	movabsq	$2305843009213693948, %rbx
	testq	%rbx, %rdi
	je	.L1323
	addq	$1, %rdi
	xorl	%edx, %edx
	movq	%rdi, %rcx
	shrq	%rcx
	salq	$4, %rcx
	.p2align 4,,10
	.p2align 3
.L1254:
	movdqu	(%r14,%rdx), %xmm1
	movups	%xmm1, (%rax,%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rdx
	jne	.L1254
	movq	%rdi, %r8
	andq	$-2, %r8
	leaq	0(,%r8,8), %rcx
	leaq	(%r14,%rcx), %rdx
	addq	%rax, %rcx
	cmpq	%rdi, %r8
	je	.L1256
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
.L1256:
	leaq	16(%rax,%rsi), %r12
.L1252:
	testq	%r14, %r14
	je	.L1257
	movq	%r14, %rdi
	movq	%rax, -256(%rbp)
	call	_ZdlPv@PLT
	movq	-256(%rbp), %rax
.L1257:
	movq	%rax, %r14
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1420:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1423:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	-264(%rbp), %rbx
	movq	%r14, %r15
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1440:
	movabsq	$9223372036854775800, %rcx
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1422:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1219:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1441:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	salq	$3, %rcx
	jmp	.L1249
.L1447:
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rcx
	cmova	%rax, %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L1267:
	movq	%rcx, %rdi
	movq	%rsi, -296(%rbp)
	movq	%rdx, -264(%rbp)
	movq	%rcx, -240(%rbp)
	call	_Znwm@PLT
	movq	-240(%rbp), %rcx
	movq	-264(%rbp), %rdx
	leaq	(%rax,%rcx), %rsi
	leaq	8(%rax), %rcx
	movq	%rsi, -240(%rbp)
	movq	-296(%rbp), %rsi
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1425:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1445:
	movl	$8, %ecx
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	%rax, %rcx
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%r12, %rdx
	jne	.L1253
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1435:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1437:
	leaq	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	%r12, %rax
	movq	%r13, %rdx
	movq	-256(%rbp), %r15
	movq	-328(%rbp), %r12
	movq	%rax, -296(%rbp)
	subq	%rax, %rdx
	movq	-336(%rbp), %rbx
	movq	%rax, %rcx
	sarq	$3, %rdx
.L1259:
	subq	$8, %rsp
	movq	%r12, %r8
	leaq	-200(%rbp), %rax
	movq	-248(%rbp), %rdi
	pushq	%rax
	movl	-300(%rbp), %eax
	subq	%r15, %r8
	movq	%r15, %r9
	pushq	$0
	sarq	$3, %r8
	leaq	-192(%rbp), %rsi
	pushq	%rax
	movq	$0, -200(%rbp)
	call	_ZN2v814ScriptCompiler24CompileFunctionInContextENS_5LocalINS_7ContextEEEPNS0_6SourceEmPNS1_INS_6StringEEEmPNS1_INS_6ObjectEEENS0_14CompileOptionsENS0_13NoCacheReasonEPNS1_INS_14ScriptOrModuleEEE@PLT
	addq	$32, %rsp
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1448
	movq	-224(%rbp), %rax
	movq	-232(%rbp), %rsi
	movq	3080(%rax), %rdi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1278
	movq	-200(%rbp), %r8
	movl	$48, %edi
	movq	%r8, -232(%rbp)
	call	_Znwm@PLT
	movq	-232(%rbp), %r8
	movq	-224(%rbp), %rsi
	movq	%r13, %rdx
	movl	-284(%rbp), %ecx
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN4node10contextify15CompiledFnEntryC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEjNS5_INS4_14ScriptOrModuleEEE
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-224(%rbp), %rsi
	xorl	%edx, %edx
	movq	$0, (%rax)
	movq	%rax, %rdi
	movl	-284(%rbp), %eax
	movq	280(%rsi), %r8
	movq	%r14, 16(%rdi)
	movl	%eax, 8(%rdi)
	movq	-312(%rbp), %rax
	divq	%r8
	movq	272(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L1282
	movq	(%rax), %rcx
	movl	-284(%rbp), %r10d
	movl	8(%rcx), %esi
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1282
	movl	8(%rcx), %eax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L1282
.L1283:
	cmpl	%esi, %r10d
	jne	.L1449
	call	_ZdlPv@PLT
.L1307:
	movq	-216(%rbp), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	-248(%rbp), %rsi
	movq	%r12, %rcx
	movq	%rax, %r14
	movq	-224(%rbp), %rax
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	784(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1278
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	288(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1278
	cmpb	$0, -285(%rbp)
	jne	.L1450
.L1289:
	movq	(%rbx), %rax
	testq	%r14, %r14
	je	.L1451
	movq	(%r14), %rdx
.L1291:
	movq	%rdx, 24(%rax)
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	jne	.L1415
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1446:
	movabsq	$9223372036854775800, %rcx
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1324:
	xorl	%edx, %edx
	movq	%rax, %rcx
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	-320(%rbp), %rdi
	movb	%dl, -301(%rbp)
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	-301(%rbp), %edx
	movzbl	%al, %eax
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1322:
	movq	%rcx, %r12
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1328:
	movq	%rax, %rcx
	movq	%r12, %rdx
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	(%rdx), %rdi
	addq	$8, %rdx
	addq	$8, %rcx
	movq	%rdi, -8(%rcx)
	cmpq	%rdx, %r13
	jne	.L1271
	jmp	.L1274
.L1327:
	movq	%rcx, %r13
	jmp	.L1270
.L1448:
	movq	-280(%rbp), %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	je	.L1278
	movq	-280(%rbp), %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L1278
	movq	-280(%rbp), %rbx
	movq	-224(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE@PLT
	movq	%rbx, %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1282:
	movq	-224(%rbp), %rax
	movq	-312(%rbp), %rdx
	movq	%rdi, %rcx
	movq	%r9, %rsi
	movl	$1, %r8d
	leaq	272(%rax), %r10
	movq	%r10, %rdi
	call	_ZNSt10_HashtableIjSt4pairIKjPN4node10contextify15CompiledFnEntryEESaIS6_ENSt8__detail10_Select1stESt8equal_toIjESt4hashIjENS8_18_Mod_range_hashingENS8_20_Default_ranged_hashENS8_20_Prime_rehash_policyENS8_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS8_10_Hash_nodeIS6_Lb0EEEm
	jmp	.L1307
.L1450:
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler26CreateCodeCacheForFunctionENS_5LocalINS_8FunctionEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1285
	movslq	8(%rax), %rdx
	movq	(%rax), %rsi
	movq	-224(%rbp), %rdi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1452
.L1286:
	movq	-224(%rbp), %rax
	movq	-248(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	360(%rax), %rax
	movq	280(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1288
	movq	-224(%rbp), %rax
	movq	-320(%rbp), %rcx
	movq	%r14, %rdi
	movq	-248(%rbp), %rsi
	movq	360(%rax), %rax
	movq	264(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1288
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1289
.L1285:
	movq	-224(%rbp), %rax
	movq	-272(%rbp), %rcx
	movq	%r14, %rdi
	movq	-248(%rbp), %rsi
	movq	360(%rax), %rax
	movq	264(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L1289
	jmp	.L1278
.L1288:
	movq	%r12, %rdi
	call	_ZN2v814ScriptCompiler10CachedDataD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L1278
.L1451:
	movq	16(%rax), %rdx
	jmp	.L1291
.L1452:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1286
.L1266:
	leaq	.LC19(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1431:
	call	__stack_chk_fail@PLT
.L1432:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1195
.L1433:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L1200
	.cfi_endproc
.LFE7790:
	.size	_ZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"Script execution timed out after "
	.section	.rodata.str1.1
.LC21:
	.string	"ms"
.LC22:
	.string	"ERR_SCRIPT_EXECUTION_TIMEOUT"
.LC23:
	.string	"code"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"ERR_SCRIPT_EXECUTION_INTERRUPTED"
	.align 8
.LC25:
	.string	"Script execution was interrupted by `SIGINT`"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE.part.0, @function
_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE.part.0:
.LFB11026:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%r8d, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1312(%rbp), %rbx
	subq	$1400, %rsp
	movl	%edx, -1380(%rbp)
	movq	352(%rdi), %rsi
	movq	%rbx, %rdi
	movl	%ecx, -1368(%rbp)
	movq	%r9, -1376(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	(%r15), %r15
	movq	%r12, -1264(%rbp)
	movl	$0, -1256(%rbp)
	movq	%r15, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1504
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1494
	cmpw	$1040, %cx
	jne	.L1455
.L1494:
	movq	23(%rdx), %rax
.L1457:
	testq	%rax, %rax
	je	.L1472
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.L1460
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1505
.L1460:
	call	_ZN2v813UnboundScript20BindToCurrentContextEv@PLT
	movq	%rax, %r15
	testb	%r14b, %r14b
	jne	.L1506
.L1461:
	movb	$0, -1346(%rbp)
	movb	$0, -1345(%rbp)
	cmpq	$-1, %r13
	je	.L1463
	cmpb	$0, -1368(%rbp)
	je	.L1465
	movq	352(%r12), %rsi
	leaq	-1216(%rbp), %r14
	leaq	-1346(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN4node8WatchdogC1EPN2v87IsolateEmPb@PLT
	leaq	-1344(%rbp), %r8
	movq	352(%r12), %rsi
	leaq	-1345(%rbp), %rdx
	movq	%r8, %rdi
	movq	%r8, -1408(%rbp)
	call	_ZN4node14SigintWatchdogC1EPN2v87IsolateEPb@PLT
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	-1408(%rbp), %r8
	movq	%rax, -1368(%rbp)
	movq	%r8, %rdi
	call	_ZN4node14SigintWatchdogD1Ev@PLT
	movq	%r14, %rdi
	call	_ZN4node8WatchdogD1Ev@PLT
.L1464:
	cmpb	$0, -1346(%rbp)
	jne	.L1467
.L1510:
	cmpb	$0, -1345(%rbp)
	jne	.L1467
.L1468:
	movq	%rbx, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L1507
	movq	-1376(%rbp), %rax
	cmpq	$0, -1368(%rbp)
	movq	(%rax), %r12
	je	.L1508
	movq	-1368(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%r12)
	movl	$1, %r12d
.L1459:
	movq	%rbx, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1509
	addq	$1400, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1463:
	.cfi_restore_state
	cmpb	$0, -1368(%rbp)
	je	.L1465
	movq	352(%r12), %rsi
	leaq	-1344(%rbp), %r14
	leaq	-1345(%rbp), %rdx
	movq	%r14, %rdi
	call	_ZN4node14SigintWatchdogC1EPN2v87IsolateEPb@PLT
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -1368(%rbp)
	call	_ZN4node14SigintWatchdogD1Ev@PLT
	cmpb	$0, -1346(%rbp)
	je	.L1510
	.p2align 4,,10
	.p2align 3
.L1467:
	testb	$1, 1932(%r12)
	jne	.L1473
	movzbl	2664(%r12), %eax
	testb	%al, %al
	jne	.L1472
.L1473:
	movq	352(%r12), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	cmpb	$0, -1346(%rbp)
	jne	.L1511
	cmpb	$0, -1345(%rbp)
	je	.L1468
	movq	352(%r12), %r13
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC24(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1512
.L1485:
	movq	%r13, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1513
.L1486:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1514
.L1487:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1515
.L1488:
	movq	%r13, %rdi
	movq	%rdx, -1408(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1408(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1516
.L1489:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	2080(%r12), %rdi
	movl	$29810, %edx
	movabsq	$7957614712154583618, %rax
	leaq	-1216(%rbp), %rsi
	leaq	-1200(%rbp), %r14
	movq	%rax, -1200(%rbp)
	movq	%r14, -1216(%rbp)
	movl	$1635021600, -1192(%rbp)
	movw	%dx, -1188(%rbp)
	movq	$14, -1208(%rbp)
	movb	$0, -1186(%rbp)
	call	_ZN4node9inspector5Agent30PauseOnNextJavascriptStatementERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-1216(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1461
	call	_ZdlPv@PLT
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1472:
	xorl	%r12d, %r12d
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1465:
	cmpq	$-1, %r13
	je	.L1466
	movq	352(%r12), %rsi
	leaq	-1216(%rbp), %r14
	leaq	-1346(%rbp), %rcx
	movq	%r13, %rdx
	movq	%r14, %rdi
	call	_ZN4node8WatchdogC1EPN2v87IsolateEmPb@PLT
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -1368(%rbp)
	call	_ZN4node8WatchdogD1Ev@PLT
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	352(%r12), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	call	_ZN2v813UnboundScript20BindToCurrentContextEv@PLT
	movq	%rax, %r15
	testb	%r14b, %r14b
	je	.L1461
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	call	_ZN2v86Script3RunENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -1368(%rbp)
	jmp	.L1464
	.p2align 4,,10
	.p2align 3
.L1455:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L1457
	.p2align 4,,10
	.p2align 3
.L1507:
	cmpb	$0, -1346(%rbp)
	jne	.L1491
	cmpb	$1, -1345(%rbp)
	je	.L1491
	cmpb	$0, -1380(%rbp)
	jne	.L1517
.L1491:
	movq	%rbx, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	movl	%eax, %r12d
	testb	%al, %al
	jne	.L1472
	movq	%rbx, %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	.LC26(%rip), %xmm1
	leaq	-1104(%rbp), %r15
	leaq	-1216(%rbp), %r14
	movq	%r15, %rdi
	movhps	.LC27(%rip), %xmm1
	movaps	%xmm1, -1408(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, -1104(%rbp)
	xorl	%eax, %eax
	movw	%ax, -880(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, -872(%rbp)
	movups	%xmm0, -856(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -1216(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	$0, -888(%rbp)
	addq	%r14, %rdi
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-1408(%rbp), %xmm1
	movq	%rax, -1104(%rbp)
	leaq	-1152(%rbp), %rax
	movq	%rax, %rdi
	movaps	%xmm1, -1216(%rbp)
	movaps	%xmm0, -1200(%rbp)
	movaps	%xmm0, -1184(%rbp)
	movaps	%xmm0, -1168(%rbp)
	movq	%rax, -1424(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	leaq	-1208(%rbp), %rsi
	movq	%rax, -1208(%rbp)
	leaq	-1120(%rbp), %rax
	movq	%rax, -1392(%rbp)
	movq	%rax, -1136(%rbp)
	movl	$16, -1144(%rbp)
	movq	$0, -1128(%rbp)
	movb	$0, -1120(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movl	$33, %edx
	leaq	.LC20(%rip), %rsi
	movq	%r14, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZNSo9_M_insertIlEERSoT_@PLT
	movl	$2, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	leaq	-1232(%rbp), %rax
	movq	$0, -1240(%rbp)
	leaq	-1248(%rbp), %rdi
	movq	%rax, -1416(%rbp)
	movq	%rax, -1248(%rbp)
	movq	-1168(%rbp), %rax
	movb	$0, -1232(%rbp)
	testq	%rax, %rax
	je	.L1474
	movq	-1184(%rbp), %r8
	movq	-1176(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L1475
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L1476:
	movq	352(%r12), %r13
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC22(%rip), %rsi
	movq	-1248(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -1408(%rbp)
	testq	%rax, %rax
	je	.L1518
.L1477:
	movq	%r13, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1519
.L1478:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1520
.L1479:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC23(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1521
.L1480:
	movq	%r13, %rdi
	movq	%rdx, -1432(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1408(%rbp), %rcx
	movq	-1432(%rbp), %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1522
.L1481:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-1248(%rbp), %rdi
	cmpq	-1416(%rbp), %rdi
	je	.L1482
	call	_ZdlPv@PLT
.L1482:
	movq	.LC26(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-1136(%rbp), %rdi
	movq	%rax, -1104(%rbp)
	movhps	.LC28(%rip), %xmm0
	movaps	%xmm0, -1216(%rbp)
	cmpq	-1392(%rbp), %rdi
	je	.L1483
	call	_ZdlPv@PLT
.L1483:
	movq	-1424(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1208(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r15, %rdi
	movq	%rax, -1216(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -1216(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -1104(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1504:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1475:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN4node6errors18DecorateErrorStackEPNS_11EnvironmentERKNS0_13TryCatchScopeE@PLT
	jmp	.L1491
	.p2align 4,,10
	.p2align 3
.L1508:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r12), %rax
	movq	%rax, 24(%r12)
	movl	$1, %r12d
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1474:
	leaq	-1136(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1518:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1520:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1479
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%rax, -1432(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1432(%rbp), %rdi
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L1522:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	%rax, -1432(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1432(%rbp), %rdx
	jmp	.L1480
.L1516:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1489
.L1515:
	movq	%rax, -1408(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1408(%rbp), %rdx
	jmp	.L1488
.L1514:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1487
.L1513:
	movq	%rax, -1408(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1408(%rbp), %rdi
	jmp	.L1486
.L1512:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1485
.L1509:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11026:
	.size	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE.part.0, .-_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE.part.0
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"Script methods can only be called on script instances."
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.type	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE, @function
_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE:
.LFB7782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	jne	.L1524
.L1536:
	xorl	%eax, %eax
.L1523:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L1537
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1524:
	.cfi_restore_state
	movq	%rdi, %r12
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L1536
	movq	%rsi, %r14
	movq	(%r9), %rsi
	movq	%r9, %rbx
	testq	%rsi, %rsi
	je	.L1530
	movq	3216(%rdi), %rdi
	movl	%ecx, -88(%rbp)
	movl	%edx, %r13d
	movl	%r8d, %r15d
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L1538
.L1530:
	movq	352(%r12), %rsi
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r12), %r12
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC29(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1539
.L1529:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1538:
	movl	-88(%rbp), %ecx
	movzbl	%r13b, %edx
	movq	%rbx, %r9
	movzbl	%r15b, %r8d
	movq	%r14, %rsi
	movq	%r12, %rdi
	movzbl	%cl, %ecx
	call	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE.part.0
	jmp	.L1523
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L1529
.L1537:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7782:
	.size	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE, .-_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	.section	.rodata.str1.1
.LC30:
	.string	"RunInThisContext"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1590
	cmpw	$1040, %cx
	jne	.L1541
.L1590:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1620
.L1544:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1591
	cmpw	$1040, %cx
	jne	.L1545
.L1591:
	movq	23(%rdx), %r14
.L1547:
	testq	%r14, %r14
	je	.L1540
	movq	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic820(%rip), %r12
	testq	%r12, %r12
	je	.L1621
	testb	$5, (%r12)
	jne	.L1622
.L1553:
	cmpl	$4, 16(%rbx)
	je	.L1623
.L1583:
	leaq	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1624
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1625
	movq	8(%rbx), %rdi
.L1559:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %r15
	testb	%al, %al
	je	.L1626
.L1560:
	cmpl	$1, 16(%rbx)
	jg	.L1561
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1562:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1627
	cmpl	$1, 16(%rbx)
	jg	.L1564
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, %r12d
	jg	.L1566
.L1636:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1567:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1628
	cmpl	$2, 16(%rbx)
	jg	.L1569
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$3, 16(%rbx)
	movb	%al, -81(%rbp)
	jg	.L1571
.L1637:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1572:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1629
	cmpl	$3, 16(%rbx)
	jle	.L1630
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
.L1575:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movzbl	-81(%rbp), %ecx
	movzbl	%r12b, %edx
	movq	%rbx, %r9
	movzbl	%al, %r8d
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	movq	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic847(%rip), %r12
	testq	%r12, %r12
	je	.L1631
.L1577:
	testb	$5, (%r12)
	je	.L1540
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1580
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1632
.L1580:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1581
	movq	(%rdi), %rax
	call	*8(%rax)
.L1581:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1540
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1633
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1625:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1559
	.p2align 4,,10
	.p2align 3
.L1622:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1554
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1634
.L1554:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1555
	movq	(%rdi), %rax
	call	*8(%rax)
.L1555:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1553
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpl	$4, 16(%rbx)
	jne	.L1583
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1621:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1552
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1635
.L1552:
	movq	%r12, _ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic820(%rip)
	mfence
	testb	$5, (%r12)
	je	.L1553
	jmp	.L1622
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$2, 16(%rbx)
	movl	%eax, %r12d
	jle	.L1636
.L1566:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1567
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$3, 16(%rbx)
	movb	%al, -81(%rbp)
	jle	.L1637
.L1571:
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1575
	.p2align 4,,10
	.p2align 3
.L1541:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L1544
	.p2align 4,,10
	.p2align 3
.L1620:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1545:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1631:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1578
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1638
.L1578:
	movq	%r12, _ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic847(%rip)
	mfence
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1624:
	leaq	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1627:
	leaq	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1626:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1635:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1552
	.p2align 4,,10
	.p2align 3
.L1634:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	%r14, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC30(%rip), %rcx
	movl	$98, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1554
	.p2align 4,,10
	.p2align 3
.L1628:
	leaq	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1629:
	leaq	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1638:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1632:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	%r14, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC30(%rip), %rcx
	movl	$101, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1580
.L1633:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7780:
	.size	_ZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC31:
	.string	"RunInContext"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1700
	cmpw	$1040, %cx
	jne	.L1640
.L1700:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1741
.L1643:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1701
	cmpw	$1040, %cx
	jne	.L1644
.L1701:
	movq	23(%rdx), %r12
.L1646:
	testq	%r12, %r12
	je	.L1639
	cmpl	$5, 16(%r15)
	jne	.L1742
	movq	8(%r15), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1743
	movl	16(%r15), %eax
	testl	%eax, %eax
	jle	.L1744
	movq	8(%r15), %rdi
.L1651:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	88(%rax), %rdx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1653
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsExternalEv@PLT
	testb	%al, %al
	je	.L1653
	movq	%r14, %rdi
	call	_ZNK2v88External5ValueEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1653
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L1639
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1745
.L1655:
	movq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic869(%rip), %r14
	testq	%r14, %r14
	je	.L1746
.L1657:
	testb	$5, (%r14)
	jne	.L1747
.L1659:
	cmpl	$1, 16(%r15)
	jle	.L1748
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
.L1664:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L1749
	cmpl	$1, 16(%r15)
	jg	.L1666
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1667:
	movq	3280(%r13), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %r13
	testb	%al, %al
	je	.L1750
.L1668:
	cmpl	$2, 16(%r15)
	jg	.L1669
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1670:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1751
	cmpl	$2, 16(%r15)
	jg	.L1672
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1673:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$3, 16(%r15)
	movb	%al, -81(%rbp)
	jg	.L1674
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1675:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1752
	cmpl	$3, 16(%r15)
	jg	.L1677
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1678:
	call	_ZNK2v85Value6IsTrueEv@PLT
	cmpl	$4, 16(%r15)
	movb	%al, -82(%rbp)
	jg	.L1679
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1680:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1753
	cmpl	$4, 16(%r15)
	jg	.L1682
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1683:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	8(%rbx), %r14
	movb	%al, -83(%rbp)
	testq	%r14, %r14
	je	.L1684
	movzbl	11(%r14), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1754
.L1684:
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movzbl	-82(%rbp), %ecx
	movzbl	-81(%rbp), %edx
	movq	%r13, %rsi
	movzbl	-83(%rbp), %r8d
	movq	(%rbx), %rdi
	movq	%r15, %r9
	call	_ZN4node10contextify16ContextifyScript11EvalMachineEPNS_11EnvironmentElbbbRKN2v820FunctionCallbackInfoINS4_5ValueEEE
	movq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic893(%rip), %r13
	testq	%r13, %r13
	je	.L1755
.L1686:
	testb	$5, 0(%r13)
	jne	.L1756
.L1688:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
.L1639:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1757
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1744:
	.cfi_restore_state
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1651
	.p2align 4,,10
	.p2align 3
.L1748:
	movq	(%r15), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1664
	.p2align 4,,10
	.p2align 3
.L1640:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r15), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L1643
	.p2align 4,,10
	.p2align 3
.L1741:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1646
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	8(%r15), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1670
	.p2align 4,,10
	.p2align 3
.L1666:
	movq	8(%r15), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1674:
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1675
	.p2align 4,,10
	.p2align 3
.L1672:
	movq	8(%r15), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1673
	.p2align 4,,10
	.p2align 3
.L1745:
	movq	(%rbx), %rax
	movq	(%rdx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	jne	.L1655
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1679:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1680
	.p2align 4,,10
	.p2align 3
.L1677:
	movq	8(%r15), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1747:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1660
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1758
.L1660:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1661
	movq	(%rdi), %rax
	call	*8(%rax)
.L1661:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1659
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1659
	.p2align 4,,10
	.p2align 3
.L1746:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1658
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1759
.L1658:
	movq	%r14, _ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic869(%rip)
	mfence
	jmp	.L1657
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	8(%r15), %rax
	leaq	-32(%rax), %rdi
	jmp	.L1683
	.p2align 4,,10
	.p2align 3
.L1756:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1689
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1760
.L1689:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1690
	movq	(%rdi), %rax
	call	*8(%rax)
.L1690:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1688
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1755:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1687
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1761
.L1687:
	movq	%r13, _ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic893(%rip)
	mfence
	jmp	.L1686
	.p2align 4,,10
	.p2align 3
.L1742:
	leaq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1743:
	leaq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	(%rbx), %rax
	movq	(%r14), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r14
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1653:
	leaq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1749:
	leaq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1751:
	leaq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1750:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1668
	.p2align 4,,10
	.p2align 3
.L1752:
	leaq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1753:
	leaq	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1759:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L1658
	.p2align 4,,10
	.p2align 3
.L1758:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC31(%rip), %rcx
	movl	$98, %esi
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1660
	.p2align 4,,10
	.p2align 3
.L1761:
	leaq	.LC14(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1760:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC31(%rip), %rcx
	movl	$101, %esi
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1689
.L1757:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7781:
	.size	_ZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.weak	_ZTVN4node10contextify16ContextifyScriptE
	.section	.data.rel.ro._ZTVN4node10contextify16ContextifyScriptE,"awG",@progbits,_ZTVN4node10contextify16ContextifyScriptE,comdat
	.align 8
	.type	_ZTVN4node10contextify16ContextifyScriptE, @object
	.size	_ZTVN4node10contextify16ContextifyScriptE, 88
_ZTVN4node10contextify16ContextifyScriptE:
	.quad	0
	.quad	0
	.quad	_ZN4node10contextify16ContextifyScriptD1Ev
	.quad	_ZN4node10contextify16ContextifyScriptD0Ev
	.quad	_ZNK4node10contextify16ContextifyScript10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10contextify16ContextifyScript14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node10contextify16ContextifyScript8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.weak	_ZTVN4node10contextify15CompiledFnEntryE
	.section	.data.rel.ro._ZTVN4node10contextify15CompiledFnEntryE,"awG",@progbits,_ZTVN4node10contextify15CompiledFnEntryE,comdat
	.align 8
	.type	_ZTVN4node10contextify15CompiledFnEntryE, @object
	.size	_ZTVN4node10contextify15CompiledFnEntryE, 88
_ZTVN4node10contextify15CompiledFnEntryE:
	.quad	0
	.quad	0
	.quad	_ZN4node10contextify15CompiledFnEntryD1Ev
	.quad	_ZN4node10contextify15CompiledFnEntryD0Ev
	.quad	_ZNK4node10contextify15CompiledFnEntry10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10contextify15CompiledFnEntry14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node10contextify15CompiledFnEntry8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.section	.rodata.str1.1
.LC32:
	.string	"../src/node_contextify.cc"
.LC33:
	.string	"contextify"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC32
	.quad	0
	.quad	_ZN4node10contextify10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC33
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"../src/node_contextify.cc:1111"
	.section	.rodata.str1.1
.LC35:
	.string	"val->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"static void node::contextify::ContextifyContext::CompileFunction(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args__10_:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"../src/node_contextify.cc:1100"
	.section	.rodata.str1.1
.LC38:
	.string	"val->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"../src/node_contextify.cc:1046"
	.section	.rodata.str1.1
.LC40:
	.string	"args[8]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"../src/node_contextify.cc:1039"
	.section	.rodata.str1.1
.LC42:
	.string	"args[7]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"../src/node_contextify.cc:1030"
	.section	.rodata.str1.1
.LC44:
	.string	"(sandbox) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6:
	.quad	.LC43
	.quad	.LC44
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"../src/node_contextify.cc:1026"
	.section	.rodata.str1.1
.LC46:
	.string	"args[6]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"../src/node_contextify.cc:1020"
	.section	.rodata.str1.1
.LC48:
	.string	"args[5]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC49:
	.string	"../src/node_contextify.cc:1015"
	.section	.rodata.str1.1
.LC50:
	.string	"args[4]->IsArrayBufferView()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC49
	.quad	.LC50
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"../src/node_contextify.cc:1009"
	.section	.rodata.str1.1
.LC52:
	.string	"args[3]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"../src/node_contextify.cc:1005"
	.section	.rodata.str1.1
.LC54:
	.string	"args[2]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC36
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"../src/node_contextify.cc:1001"
	.section	.rodata.str1.1
.LC56:
	.string	"args[1]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC55
	.quad	.LC56
	.quad	.LC36
	.section	.rodata.str1.1
.LC57:
	.string	"../src/node_contextify.cc:997"
.LC58:
	.string	"args[0]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10contextify17ContextifyContext15CompileFunctionERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC36
	.local	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic893
	.comm	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic893,8,8
	.section	.rodata.str1.1
.LC59:
	.string	"../src/node_contextify.cc:881"
.LC60:
	.string	"args[4]->IsBoolean()"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"static void node::contextify::ContextifyScript::RunInContext(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, @object
	.size	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, 24
_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.section	.rodata.str1.1
.LC62:
	.string	"../src/node_contextify.cc:878"
.LC63:
	.string	"args[3]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC61
	.section	.rodata.str1.1
.LC64:
	.string	"../src/node_contextify.cc:875"
.LC65:
	.string	"args[2]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC64
	.quad	.LC65
	.quad	.LC61
	.section	.rodata.str1.1
.LC66:
	.string	"../src/node_contextify.cc:872"
.LC67:
	.string	"args[1]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC66
	.quad	.LC67
	.quad	.LC61
	.local	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic869
	.comm	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic869,8,8
	.section	.rodata.str1.1
.LC68:
	.string	"../src/node_contextify.cc:864"
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"(contextify_context) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC61
	.section	.rodata.str1.1
.LC70:
	.string	"../src/node_contextify.cc:859"
.LC71:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC61
	.section	.rodata.str1.1
.LC72:
	.string	"../src/node_contextify.cc:857"
.LC73:
	.string	"(args.Length()) == (5)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10contextify16ContextifyScript12RunInContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC61
	.local	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic847
	.comm	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic847,8,8
	.section	.rodata.str1.1
.LC74:
	.string	"../src/node_contextify.cc:836"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"static void node::contextify::ContextifyScript::RunInThisContext(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC74
	.quad	.LC63
	.quad	.LC75
	.section	.rodata.str1.1
.LC76:
	.string	"../src/node_contextify.cc:833"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC76
	.quad	.LC65
	.quad	.LC75
	.section	.rodata.str1.1
.LC77:
	.string	"../src/node_contextify.cc:830"
.LC78:
	.string	"args[1]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC75
	.section	.rodata.str1.1
.LC79:
	.string	"../src/node_contextify.cc:827"
.LC80:
	.string	"args[0]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC79
	.quad	.LC80
	.quad	.LC75
	.section	.rodata.str1.1
.LC81:
	.string	"../src/node_contextify.cc:825"
.LC82:
	.string	"(args.Length()) == (4)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC81
	.quad	.LC82
	.quad	.LC75
	.local	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic820
	.comm	_ZZN4node10contextify16ContextifyScript16RunInThisContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic820,8,8
	.local	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic781
	.comm	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic781,8,8
	.local	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic750
	.comm	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic750,8,8
	.local	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic697
	.comm	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE28trace_event_unique_atomic697,8,8
	.section	.rodata.str1.1
.LC83:
	.string	"../src/node_contextify.cc:683"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"static void node::contextify::ContextifyScript::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_9:
	.quad	.LC83
	.quad	.LC44
	.quad	.LC84
	.section	.rodata.str1.1
.LC85:
	.string	"../src/node_contextify.cc:679"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_8:
	.quad	.LC85
	.quad	.LC46
	.quad	.LC84
	.section	.rodata.str1.1
.LC86:
	.string	"../src/node_contextify.cc:676"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_7:
	.quad	.LC86
	.quad	.LC48
	.quad	.LC84
	.section	.rodata.str1.1
.LC87:
	.string	"../src/node_contextify.cc:673"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_6:
	.quad	.LC87
	.quad	.LC50
	.quad	.LC84
	.section	.rodata.str1.1
.LC88:
	.string	"../src/node_contextify.cc:670"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5:
	.quad	.LC88
	.quad	.LC52
	.quad	.LC84
	.section	.rodata.str1.1
.LC89:
	.string	"../src/node_contextify.cc:668"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC89
	.quad	.LC54
	.quad	.LC84
	.section	.rodata.str1.1
.LC90:
	.string	"../src/node_contextify.cc:667"
.LC91:
	.string	"(argc) == (7)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC90
	.quad	.LC91
	.quad	.LC84
	.section	.rodata.str1.1
.LC92:
	.string	"../src/node_contextify.cc:655"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC92
	.quad	.LC56
	.quad	.LC84
	.section	.rodata.str1.1
.LC93:
	.string	"../src/node_contextify.cc:652"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC93
	.quad	.LC58
	.quad	.LC84
	.section	.rodata.str1.1
.LC94:
	.string	"../src/node_contextify.cc:650"
.LC95:
	.string	"(argc) >= (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC84
	.section	.rodata.str1.1
.LC96:
	.string	"../src/node_contextify.cc:647"
.LC97:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10contextify16ContextifyScript3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC96
	.quad	.LC97
	.quad	.LC84
	.section	.rodata.str1.1
.LC98:
	.string	"../src/node_contextify.cc:295"
	.section	.rodata.str1.8
	.align 8
.LC99:
	.string	"static void node::contextify::ContextifyContext::IsContext(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10contextify17ContextifyContext9IsContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC98
	.quad	.LC71
	.quad	.LC99
	.section	.rodata.str1.1
.LC100:
	.string	"../src/node_contextify.cc:270"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"static void node::contextify::ContextifyContext::MakeContext(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, @object
	.size	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5, 24
_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_5:
	.quad	.LC100
	.quad	.LC60
	.quad	.LC101
	.section	.rodata.str1.1
.LC102:
	.string	"../src/node_contextify.cc:267"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC102
	.quad	.LC63
	.quad	.LC101
	.section	.rodata.str1.1
.LC103:
	.string	"../src/node_contextify.cc:262"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"args[2]->IsString() || args[2]->IsUndefined()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC103
	.quad	.LC104
	.quad	.LC101
	.section	.rodata.str1.1
.LC105:
	.string	"../src/node_contextify.cc:259"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC105
	.quad	.LC56
	.quad	.LC101
	.section	.rodata.str1.1
.LC106:
	.string	"../src/node_contextify.cc:252"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"!sandbox->HasPrivate( env->context(), env->contextify_context_private_symbol()).FromJust()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC106
	.quad	.LC107
	.quad	.LC101
	.section	.rodata.str1.1
.LC108:
	.string	"../src/node_contextify.cc:248"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC108
	.quad	.LC71
	.quad	.LC101
	.section	.rodata.str1.1
.LC109:
	.string	"../src/node_contextify.cc:247"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node10contextify17ContextifyContext11MakeContextERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC109
	.quad	.LC73
	.quad	.LC101
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC110:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC112:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC113:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC115:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.weak	_ZZN4node10BaseObjectD4EvE4args
	.section	.rodata.str1.1
.LC116:
	.string	"../src/base_object-inl.h:59"
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"(metadata->strong_ptr_count) == (0)"
	.align 8
.LC118:
	.string	"virtual node::BaseObject::~BaseObject()"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectD4EvE4args,"awG",@progbits,_ZZN4node10BaseObjectD4EvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectD4EvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectD4EvE4args, 24
_ZZN4node10BaseObjectD4EvE4args:
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0
	.section	.rodata.str1.1
.LC119:
	.string	"../src/base_object-inl.h:45"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"(object->InternalFieldCount()) > (0)"
	.align 8
.LC121:
	.string	"node::BaseObject::BaseObject(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args_0:
	.quad	.LC119
	.quad	.LC120
	.quad	.LC121
	.weak	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC122:
	.string	"../src/base_object-inl.h:44"
.LC123:
	.string	"(false) == (object.IsEmpty())"
	.section	.data.rel.ro.local._ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10BaseObjectC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC122
	.quad	.LC123
	.quad	.LC121
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC124:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC126:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	7598830848421293891
	.quad	8390322045804837222
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC18:
	.long	0
	.long	1073741824
	.section	.data.rel.ro,"aw"
	.align 8
.LC26:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC27:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC28:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
