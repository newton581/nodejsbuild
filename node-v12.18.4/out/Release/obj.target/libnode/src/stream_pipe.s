	.file	"stream_pipe.cc"
	.text
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6037:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6037:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6038:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6038:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6054:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6054:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE:
.LFB6063:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6063:
	.size	_ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node10StreamPipe8SelfSizeEv,"axG",@progbits,_ZNK4node10StreamPipe8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10StreamPipe8SelfSizeEv
	.type	_ZNK4node10StreamPipe8SelfSizeEv, @function
_ZNK4node10StreamPipe8SelfSizeEv:
.LFB6065:
	.cfi_startproc
	endbr64
	movl	$128, %eax
	ret
	.cfi_endproc
.LFE6065:
	.size	_ZNK4node10StreamPipe8SelfSizeEv, .-_ZNK4node10StreamPipe8SelfSizeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7141:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7141:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9873:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE9873:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9874:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9874:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9876:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE9876:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB9877:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9877:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9878:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9878:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9880:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE9880:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7577:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7577:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7576:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L25
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L25:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7576:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16WritableListener13OnStreamAllocEm
	.type	_ZN4node10StreamPipe16WritableListener13OnStreamAllocEm, @function
_ZN4node10StreamPipe16WritableListener13OnStreamAllocEm:
.LFB7643:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L31:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node10StreamPipe16WritableListener13OnStreamAllocEmE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7643:
	.size	_ZN4node10StreamPipe16WritableListener13OnStreamAllocEm, .-_ZN4node10StreamPipe16WritableListener13OnStreamAllocEm
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_t
	.type	_ZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_t, @function
_ZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_t:
.LFB7644:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L37:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_tE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7644:
	.size	_ZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_t, .-_ZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_t
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev:
.LFB9830:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	addq	$16, %rdi
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9830:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB9832:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	16(%rdi), %rdi
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9832:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16WritableListener18OnStreamWantsWriteEm
	.type	_ZN4node10StreamPipe16WritableListener18OnStreamWantsWriteEm, @function
_ZN4node10StreamPipe16WritableListener18OnStreamWantsWriteEm:
.LFB7642:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, -44(%rdi)
	movq	%rsi, -32(%rdi)
	jne	.L41
	cmpb	$0, -42(%rdi)
	je	.L47
.L41:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L48
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	.cfi_restore_state
	movq	-88(%rdi), %rax
	leaq	-112(%rbp), %r12
	leaq	-104(%rdi), %rbx
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	88(%rbx), %rdi
	movb	$1, 60(%rbx)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L41
.L48:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7642:
	.size	_ZN4node10StreamPipe16WritableListener18OnStreamWantsWriteEm, .-_ZN4node10StreamPipe16WritableListener18OnStreamWantsWriteEm
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe8IsClosedERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamPipe8IsClosedERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamPipe8IsClosedERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L62
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L57
	cmpw	$1040, %cx
	jne	.L51
.L57:
	movq	23(%rdx), %rax
.L53:
	testq	%rax, %rax
	je	.L49
	movq	(%rbx), %rdx
	cmpb	$1, 62(%rax)
	sbbq	%rax, %rax
	movq	8(%rdx), %rcx
	andl	$8, %eax
	movq	112(%rcx,%rax), %rax
	movq	%rax, 24(%rdx)
.L49:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L62:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7648:
	.size	_ZN4node10StreamPipe8IsClosedERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamPipe8IsClosedERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16ReadableListener15OnStreamDestroyEv
	.type	_ZN4node10StreamPipe16ReadableListener15OnStreamDestroyEv, @function
_ZN4node10StreamPipe16ReadableListener15OnStreamDestroyEv:
.LFB7640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, -19(%rdi)
	movb	$1, -16(%rdi)
	je	.L67
.L63:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L68
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	(%rdi), %rax
	movq	%rdi, %r12
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rax), %rbx
	call	uv_buf_init@PLT
	movq	$-32, %rsi
	movq	%r12, %rdi
	movq	%rdx, -40(%rbp)
	leaq	-48(%rbp), %rdx
	movq	%rax, -48(%rbp)
	call	*%rbx
	jmp	.L63
.L68:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7640:
	.size	_ZN4node10StreamPipe16ReadableListener15OnStreamDestroyEv, .-_ZN4node10StreamPipe16ReadableListener15OnStreamDestroyEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9939:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9939:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9940:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE9940:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB9941:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9941:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB9942:
	.cfi_startproc
	endbr64
	movl	$72, %eax
	ret
	.cfi_endproc
.LFE9942:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev:
.LFB9943:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9943:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, @function
_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev:
.LFB9947:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-16(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -16(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9947:
	.size	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev, .-_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9944:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L79:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9944:
	.size	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev:
.LFB6064:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$10, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$7588685637200540755, %rcx
	movq	%rdx, (%rdi)
	movl	$25968, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE6064:
	.size	_ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev, .-_ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9945:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE9945:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9875:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE9875:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB9879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$18, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC0(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$28769, %edx
	movw	%dx, 16(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L86
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L86:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9879:
	.size	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_E4CallES2_:
.LFB9871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%rdi, -88(%rbp)
	movq	352(%rsi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	24(%r14), %rdx
	movq	8(%rdx), %r15
	testq	%r15, %r15
	je	.L88
	movzbl	11(%r15), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L130
.L88:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	1256(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L92
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L91
	movq	-88(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	24(%rax), %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L92
.L91:
	movq	352(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	addq	$104, %rax
	movq	%rax, -88(%rbp)
	movq	360(%rbx), %rax
	movq	1640(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L92
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	1608(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L92
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L132
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	.cfi_restore_state
	movq	16(%rdx), %rax
	movq	(%r15), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r15
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-96(%rbp), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L92
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	-88(%rbp), %rcx
	movq	1640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L92
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	-88(%rbp), %rcx
	movq	1608(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L92
	movq	360(%rbx), %rax
	movq	-88(%rbp), %r15
	movq	%r14, %rdi
	movq	3280(%rbx), %rsi
	movq	1360(%rax), %rdx
	movq	%r15, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L92
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rcx
	movq	-96(%rbp), %rdi
	movq	1344(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	jmp	.L92
.L132:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9871:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_E4CallES2_
	.section	.text._ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7604:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$96, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L138
	cmpw	$1040, %cx
	jne	.L134
.L138:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L140
.L137:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	32(%rbx), %rsi
	leaq	40(%r12), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	%rdx, 32(%r12)
	movl	$39, %ecx
	movq	%r13, %rdx
	movq	%rax, 24(%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L137
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7604:
	.size	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB7602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$72, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L146
	cmpw	$1040, %cx
	jne	.L142
.L146:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L148
.L145:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	32(%rbx), %rsi
	leaq	16(%r12), %rdi
	movsd	.LC1(%rip), %xmm0
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movq	%r13, %rdx
	movl	$26, %ecx
	movq	%rax, (%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 16(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L145
	.p2align 4,,10
	.p2align 3
.L148:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7602:
	.size	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe13PendingWritesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamPipe13PendingWritesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamPipe13PendingWritesERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L160
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L155
	cmpw	$1040, %cx
	jne	.L151
.L155:
	movq	23(%rdx), %rax
.L153:
	testq	%rax, %rax
	je	.L149
	movslq	56(%rax), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L149:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L160:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7649:
	.size	_ZN4node10StreamPipe13PendingWritesERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamPipe13PendingWritesERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev:
.LFB9946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r13
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L161
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L165
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L161:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9946:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB9948:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-40(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r14
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L167
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L173
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L167:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$96, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9948:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev:
.LFB9826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rbx), %r12
	movq	32(%rbx), %r13
	movq	%rax, (%rbx)
	call	uv_buf_init@PLT
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%r12, %r12
	je	.L174
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L178
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9826:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB9828:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	movq	%rax, (%r12)
	call	uv_buf_init@PLT
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	testq	%r13, %r13
	je	.L180
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L186
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L180:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9828:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node10StreamPipe16ReadableListenerD2Ev,"axG",@progbits,_ZN4node10StreamPipe16ReadableListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamPipe16ReadableListenerD2Ev
	.type	_ZN4node10StreamPipe16ReadableListenerD2Ev, @function
_ZN4node10StreamPipe16ReadableListenerD2Ev:
.LFB9822:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L187
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L189
	cmpq	%rax, %rdi
	jne	.L191
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	%rax, %rdi
	je	.L207
.L191:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L203
.L189:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L207:
	movq	16(%rdi), %rax
	movq	%rax, 16(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L206:
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
	ret
	.cfi_endproc
.LFE9822:
	.size	_ZN4node10StreamPipe16ReadableListenerD2Ev, .-_ZN4node10StreamPipe16ReadableListenerD2Ev
	.weak	_ZN4node10StreamPipe16ReadableListenerD1Ev
	.set	_ZN4node10StreamPipe16ReadableListenerD1Ev,_ZN4node10StreamPipe16ReadableListenerD2Ev
	.section	.text._ZN4node10StreamPipe16WritableListenerD2Ev,"axG",@progbits,_ZN4node10StreamPipe16WritableListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamPipe16WritableListenerD2Ev
	.type	_ZN4node10StreamPipe16WritableListenerD2Ev, @function
_ZN4node10StreamPipe16WritableListenerD2Ev:
.LFB9818:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L208
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L210
	cmpq	%rax, %rdi
	jne	.L212
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L224:
	cmpq	%rax, %rdi
	je	.L228
.L212:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L224
.L210:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	movq	16(%rdi), %rax
	movq	%rax, 16(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
	ret
	.cfi_endproc
.LFE9818:
	.size	_ZN4node10StreamPipe16WritableListenerD2Ev, .-_ZN4node10StreamPipe16WritableListenerD2Ev
	.weak	_ZN4node10StreamPipe16WritableListenerD1Ev
	.set	_ZN4node10StreamPipe16WritableListenerD1Ev,_ZN4node10StreamPipe16WritableListenerD2Ev
	.section	.text._ZN4node10StreamPipe16WritableListenerD0Ev,"axG",@progbits,_ZN4node10StreamPipe16WritableListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamPipe16WritableListenerD0Ev
	.type	_ZN4node10StreamPipe16WritableListenerD0Ev, @function
_ZN4node10StreamPipe16WritableListenerD0Ev:
.LFB9820:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L230
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L231
	cmpq	%rax, %rdi
	jne	.L233
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L245:
	cmpq	%rax, %rdi
	je	.L249
.L233:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L245
.L231:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L248:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
.L230:
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L249:
	movq	16(%rdi), %rax
	movl	$24, %esi
	movq	%rax, 16(%rdx)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9820:
	.size	_ZN4node10StreamPipe16WritableListenerD0Ev, .-_ZN4node10StreamPipe16WritableListenerD0Ev
	.section	.text._ZN4node10StreamPipe16ReadableListenerD0Ev,"axG",@progbits,_ZN4node10StreamPipe16ReadableListenerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamPipe16ReadableListenerD0Ev
	.type	_ZN4node10StreamPipe16ReadableListenerD0Ev, @function
_ZN4node10StreamPipe16ReadableListenerD0Ev:
.LFB9824:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%rdx, %rdx
	je	.L251
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L252
	cmpq	%rax, %rdi
	jne	.L254
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L266:
	cmpq	%rax, %rdi
	je	.L270
.L254:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L266
.L252:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L269:
	.cfi_def_cfa 7, 8
	.cfi_restore 6
	movq	16(%rdi), %rax
	movq	%rax, 8(%rdx)
.L251:
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L270:
	movq	16(%rdi), %rax
	movl	$24, %esi
	movq	%rax, 16(%rdx)
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9824:
	.size	_ZN4node10StreamPipe16ReadableListenerD0Ev, .-_ZN4node10StreamPipe16ReadableListenerD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16ReadableListener13OnStreamAllocEm
	.type	_ZN4node10StreamPipe16ReadableListener13OnStreamAllocEm, @function
_ZN4node10StreamPipe16ReadableListener13OnStreamAllocEm:
.LFB7633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	cmpq	%rsi, -8(%rdi)
	cmovbe	-8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L279
	movq	-64(%rdi), %r13
	leaq	-80(%rdi), %rax
	movq	%rbx, %rsi
	movq	360(%r13), %rax
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L280
	movl	%ebx, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r15
	movq	%rdx, %r14
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rdx, %rbx
	movq	%rax, %r12
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r12, %r12
	je	.L274
	movq	360(%r13), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L274:
	addq	$40, %rsp
	movq	%r15, %rax
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L279:
	.cfi_restore_state
	leaq	_ZZN4node10StreamPipe16ReadableListener13OnStreamAllocEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L280:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7633:
	.size	_ZN4node10StreamPipe16ReadableListener13OnStreamAllocEm, .-_ZN4node10StreamPipe16ReadableListener13OnStreamAllocEm
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamPipe5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamPipe5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L295
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L290
	cmpw	$1040, %cx
	jne	.L283
.L290:
	movq	23(%rdx), %rbx
.L285:
	testq	%rbx, %rbx
	je	.L281
	cmpb	$0, 60(%rbx)
	movb	$0, 62(%rbx)
	movq	$65536, 72(%rbx)
	jne	.L281
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %r12
	leaq	-80(%rbp), %r13
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$2, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	88(%rbx), %rdi
	movb	$1, 60(%rbx)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L281:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L296
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7646:
	.size	_ZN4node10StreamPipe5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamPipe5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"StreamPipe"
.LC3:
	.string	"unpipe"
.LC4:
	.string	"start"
.LC5:
	.string	"isClosed"
.LC6:
	.string	"pendingWrites"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB7:
	.text
.LHOTB7:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -56(%rbp)
	testq	%rdx, %rdx
	je	.L298
	movq	%rdx, %rdi
	movq	%rdx, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L298
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L298
	movq	271(%rax), %r12
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r15
	movq	%rax, %rbx
	popq	%rax
	testq	%rbx, %rbx
	je	.L310
.L299:
	movq	352(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node10StreamPipe6UnpipeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%r12), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L311
.L300:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node10StreamPipe5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%r12), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L312
.L301:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node10StreamPipe8IsClosedERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%r12), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L313
.L302:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%r12), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%r12), %rdx
	movq	352(%r12), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node10StreamPipe13PendingWritesERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%r12), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L314
.L303:
	movq	%r13, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L315
.L304:
	movq	-56(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L316
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L310:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L311:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L316:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7650:
.L298:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7650:
	.text
	.size	_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE7:
	.text
.LHOTE7:
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7139:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L318
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L318:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L328
.L319:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L320
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L328:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L319
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7139:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED0Ev:
.LFB9492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L331
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L342
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L343
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L344
.L331:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L337
	movq	(%rdi), %rax
	call	*8(%rax)
.L337:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L344:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L335
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L342:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L335:
	cmpb	$0, 8(%rdx)
	je	.L331
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L331
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9492:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED2Ev:
.LFB9490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L347
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L355
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L356
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L347
	cmpb	$0, 9(%rdx)
	je	.L351
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L347:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L345
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L345:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L351:
	cmpb	$0, 8(%rdx)
	je	.L347
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L347
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L356:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9490:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipeC2EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE
	.type	_ZN4node10StreamPipeC2EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE, @function
_ZN4node10StreamPipeC2EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE:
.LFB7620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movsd	.LC1(%rip), %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	movl	$29, %ecx
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	movq	%r14, %rdx
	pushq	%rbx
	.cfi_offset 3, -48
	movq	32(%rsi), %rsi
	movq	%rdi, %rbx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node10StreamPipeE(%rip), %rax
	xorl	%edx, %edx
	movq	$0, 72(%rbx)
	movq	%rax, (%rbx)
	movabsq	$281474976710656, %rax
	movq	%rax, 56(%rbx)
	leaq	16+_ZTVN4node10StreamPipe16ReadableListenerE(%rip), %rax
	movq	%rax, 80(%rbx)
	leaq	16+_ZTVN4node10StreamPipe16WritableListenerE(%rip), %rax
	movq	%rax, 104(%rbx)
	movq	24(%rbx), %rax
	movw	%dx, 64(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
	testq	%rax, %rax
	je	.L360
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L359
.L360:
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rbx, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L359:
	testq	%r12, %r12
	je	.L374
	cmpq	$0, 88(%rbx)
	leaq	80(%rbx), %rax
	jne	.L361
	movq	8(%r13), %rdx
	cmpq	$0, 112(%rbx)
	movq	%rax, 8(%r13)
	leaq	104(%rbx), %rax
	movq	%r13, 88(%rbx)
	movq	%rdx, 96(%rbx)
	jne	.L361
	movq	8(%r12), %rdx
	leaq	_ZNK4node14StreamResource13HasWantsWriteEv(%rip), %rcx
	movq	%rax, 8(%r12)
	movq	(%r12), %rax
	movq	%r12, 112(%rbx)
	movq	%rdx, 120(%rbx)
	movq	56(%rax), %rdx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jne	.L375
.L362:
	movb	%al, 65(%rbx)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*136(%rax)
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1640(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L376
.L363:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*136(%rax)
	movq	%r14, %rcx
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1360(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L377
.L364:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*136(%rax)
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1608(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L378
.L365:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*136(%rax)
	movq	%r14, %rcx
	movq	%rax, %rdi
	movq	16(%rbx), %rax
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1344(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L379
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L375:
	.cfi_restore_state
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L376:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L379:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L361:
	leaq	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	_ZZN4node10StreamPipeC4EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7620:
	.size	_ZN4node10StreamPipeC2EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE, .-_ZN4node10StreamPipeC2EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE
	.globl	_ZN4node10StreamPipeC1EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE
	.set	_ZN4node10StreamPipeC1EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE,_ZN4node10StreamPipeC2EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rdx
	movq	%rdi, %rbx
	movq	40(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L381
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L417
.L381:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L418
	movq	8(%rbx), %rdi
.L383:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L419
	cmpl	$1, 16(%rbx)
	jg	.L385
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L386:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L420
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jg	.L388
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L389:
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L406
	cmpw	$1040, %si
	jne	.L390
.L406:
	movq	23(%rdx), %r12
	testq	%r12, %r12
	je	.L392
.L393:
	movq	31(%rdx), %r12
.L392:
	cmpl	$1, %ecx
	jg	.L397
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L398:
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L408
	cmpw	$1040, %cx
	jne	.L399
.L408:
	cmpq	$0, 23(%rdx)
	je	.L403
.L401:
	movq	31(%rdx), %r14
.L402:
	movq	8(%rbx), %r13
	movl	$128, %edi
	call	_Znwm@PLT
	popq	%rbx
	movq	%r14, %rdx
	movq	%r12, %rsi
	addq	$8, %r13
	popq	%r12
	movq	%rax, %rdi
	movq	%r13, %rcx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10StreamPipeC1EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEE
	.p2align 4,,10
	.p2align 3
.L418:
	.cfi_restore_state
	movq	8(%rdx), %rdi
	addq	$88, %rdi
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L417:
	cmpl	$5, 43(%rax)
	jne	.L381
	leaq	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L385:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L397:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r13
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L388:
	movq	8(%rbx), %r13
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L419:
	leaq	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L420:
	leaq	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L399:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L403
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L401
	cmpw	$1040, %cx
	je	.L401
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L390:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L416
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L407
	cmpw	$1040, %cx
	je	.L407
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
.L416:
	movl	16(%rbx), %ecx
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L403:
	xorl	%r14d, %r14d
	jmp	.L402
.L407:
	movl	16(%rbx), %ecx
	jmp	.L393
	.cfi_endproc
.LFE7645:
	.size	_ZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_Z21_register_stream_pipev
	.type	_Z21_register_stream_pipev, @function
_Z21_register_stream_pipev:
.LFB7651:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7651:
	.size	_Z21_register_stream_pipev, .-_Z21_register_stream_pipev
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8349:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L439
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L440
.L425:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L429
.L422:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L430
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L426:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L425
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L431
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L427:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L429:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L422
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L439:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L431:
	xorl	%edx, %edx
	jmp	.L427
	.cfi_endproc
.LFE8349:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L442
	leaq	16(%r12), %rsi
.L443:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L444
	addq	$16, %r12
.L445:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L446
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L462
.L446:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L463
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L450
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L464
.L441:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L465
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L464:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L463:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L453
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L448:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L450:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L453:
	xorl	%edx, %edx
	jmp	.L448
.L465:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7607:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7609:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L467
	leaq	40(%r12), %rsi
.L468:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L469
	addq	$40, %r12
.L470:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L471
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L487
.L471:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L488
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L475
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L489
.L466:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L490
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L467:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L489:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L488:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L478
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L473:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L475:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L478:
	xorl	%edx, %edx
	jmp	.L473
.L490:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7609:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.section	.text._ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE:
.LFB7599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	32(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-80(%rbp), %rax
	movq	352(%r14), %rsi
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	je	.L535
.L492:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*128(%rax)
	movq	16(%rax), %rbx
	movsd	40(%rax), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L536
.L495:
	movq	1256(%rbx), %rax
	leaq	_ZN4node10StreamBase18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movq	0(%r13), %rax
	movq	112(%rax), %rax
	movsd	%xmm1, -112(%rbp)
	cmpq	%rdx, %rax
	jne	.L496
	movl	$72, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, %r15
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r15)
	movq	-1(%rdx), %rax
	movq	%r13, 8(%r15)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L515
	cmpw	$1040, %cx
	jne	.L497
.L515:
	movq	31(%rdx), %rax
.L499:
	testq	%rax, %rax
	jne	.L537
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	32(%r13), %rsi
	leaq	16(%r15), %rdi
	movq	%r12, %rdx
	leaq	16+_ZTVN4node12ShutdownWrapE(%rip), %rax
	movsd	.LC1(%rip), %xmm0
	movl	$26, %ecx
	movq	%rax, (%r15)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r15)
	addq	$72, %rax
	movq	%rax, 16(%r15)
.L501:
	movq	0(%r13), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*32(%rax)
	movl	%eax, -116(%rbp)
	testl	%eax, %eax
	je	.L502
	testq	%r15, %r15
	jne	.L538
.L502:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*64(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L507
	movq	352(%r14), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L539
.L508:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L540
.L509:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*72(%rax)
.L507:
	movq	1256(%rbx), %rax
	movsd	-112(%rbp), %xmm2
	movsd	%xmm2, 24(%rax)
.L510:
	movq	-104(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L541
	movl	-116(%rbp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L536:
	.cfi_restore_state
	comisd	.LC8(%rip), %xmm0
	jnb	.L495
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L538:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	leaq	-88(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L503
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L542
.L503:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-88(%rbp), %r9
	movq	24(%r9), %rdx
	testq	%rdx, %rdx
	je	.L543
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L511
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L502
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*8(%rax)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L542:
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r9
	movq	%r9, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L497:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r15
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L535:
	movq	3232(%r14), %rdi
	movq	3280(%r14), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L544
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L537:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L543:
	movl	$24, %edi
	movq	%r9, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %r9
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r9), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L514
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L505:
	movb	%dl, 8(%rax)
	movq	%r9, 16(%rax)
	movq	%rax, 24(%r9)
.L511:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L514:
	xorl	%edx, %edx
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L540:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%rax, -128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-128(%rbp), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L544:
	movl	$-16, -116(%rbp)
	jmp	.L510
.L541:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7599:
	.size	_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC2EPS1_:
.LFB8371:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L562
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L563
.L548:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L552
.L545:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L553
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L549:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L548
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L554
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L550:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L552:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L545
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L549
	.p2align 4,,10
	.p2align 3
.L554:
	xorl	%edx, %edx
	jmp	.L550
	.cfi_endproc
.LFE8371:
	.size	_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC2EPS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe6UnpipeEb
	.type	_ZN4node10StreamPipe6UnpipeEb, @function
_ZN4node10StreamPipe6UnpipeEb:
.LFB7628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 62(%rdi)
	jne	.L564
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movq	88(%rdi), %rdi
	cmpb	$0, 64(%rbx)
	je	.L631
.L567:
	movq	8(%rdi), %rax
	movb	$1, 62(%rbx)
	leaq	80(%rbx), %rdx
	movb	$0, 60(%rbx)
	testq	%rax, %rax
	je	.L572
	cmpq	%rdx, %rax
	jne	.L569
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L625:
	cmpq	%rdx, %rax
	je	.L633
.L569:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L625
.L572:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L631:
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	88(%rbx), %rdi
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L633:
	movq	96(%rbx), %rax
	movq	%rax, 16(%rcx)
.L571:
	movl	56(%rbx), %eax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 88(%rbx)
	testl	%eax, %eax
	je	.L634
.L601:
	testb	%r12b, %r12b
	jne	.L564
	movq	16(%rbx), %rax
	leaq	-64(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-88(%rbp), %rdi
	movq	%rbx, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC1EPS1_
	movq	-88(%rbp), %rsi
	leaq	-72(%rbp), %rdi
	movq	16(%rbx), %r12
	movq	%rbx, -80(%rbp)
	call	_ZN4node17BaseObjectPtrImplINS_10StreamPipeELb0EEC1EPS1_
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-80(%rbp), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_EE(%rip), %rcx
	movb	$1, 8(%rax)
	movq	%rdx, 24(%rax)
	movq	-72(%rbp), %rdx
	movq	$0, 16(%rax)
	movq	%rdx, 32(%rax)
	movq	2480(%r12), %rdx
	movq	%rcx, (%rax)
	movq	$0, -72(%rbp)
	lock addq	$1, 2464(%r12)
	movq	%rax, 2480(%r12)
	testq	%rdx, %rdx
	je	.L580
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L582
.L630:
	movq	(%rdi), %rax
	call	*8(%rax)
.L582:
	movq	1312(%r12), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	jne	.L584
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%r12), %rdx
	movl	4(%rdx), %eax
.L584:
	movq	-72(%rbp), %rdi
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	testq	%rdi, %rdi
	je	.L586
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L594
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L595
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L586
	cmpb	$0, 9(%rdx)
	je	.L590
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L593
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L594
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L595
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L593
	cmpb	$0, 9(%rdx)
	je	.L597
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L564:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L635
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	movq	96(%rbx), %rax
	movq	%rax, 8(%rdi)
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L634:
	movq	112(%rbx), %rcx
	leaq	104(%rbx), %rdx
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L572
	cmpq	%rdx, %rax
	jne	.L575
	movq	120(%rbx), %rax
	movq	%rax, 8(%rcx)
.L577:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 112(%rbx)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L626:
	cmpq	%rdx, %rax
	je	.L636
.L575:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L626
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L580:
	movq	2472(%r12), %rdi
	movq	%rax, 2472(%r12)
	testq	%rdi, %rdi
	jne	.L630
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L636:
	movq	120(%rbx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L577
.L594:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L590:
	cmpb	$0, 8(%rdx)
	je	.L586
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L586
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L586
.L597:
	cmpb	$0, 8(%rdx)
	je	.L593
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L593
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L593
.L595:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L635:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7628:
	.size	_ZN4node10StreamPipe6UnpipeEb, .-_ZN4node10StreamPipe6UnpipeEb
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipeD2Ev
	.type	_ZN4node10StreamPipeD2Ev, @function
_ZN4node10StreamPipeD2Ev:
.LFB7623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10StreamPipeE(%rip), %rax
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10StreamPipe6UnpipeEb
	movq	112(%r12), %rcx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rdx
	movq	%rdx, 104(%r12)
	testq	%rcx, %rcx
	je	.L638
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L639
	leaq	104(%r12), %rsi
	cmpq	%rax, %rsi
	jne	.L641
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L666:
	cmpq	%rax, %rsi
	je	.L671
.L641:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L666
.L639:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L670:
	movq	120(%r12), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L638:
	movq	%rdx, 80(%r12)
	movq	88(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L672
.L645:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L639
	leaq	80(%r12), %rcx
	cmpq	%rcx, %rax
	jne	.L647
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L667:
	cmpq	%rcx, %rax
	je	.L674
.L647:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L667
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L671:
	movq	16(%rsi), %rax
	movq	%rax, 16(%rcx)
	movq	%rdx, 80(%r12)
	movq	88(%r12), %rdx
	testq	%rdx, %rdx
	je	.L645
	jmp	.L672
	.p2align 4,,10
	.p2align 3
.L674:
	movq	96(%r12), %rax
	movq	%rax, 16(%rdx)
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L673:
	movq	96(%r12), %rax
	movq	%rax, 8(%rdx)
	jmp	.L645
	.cfi_endproc
.LFE7623:
	.size	_ZN4node10StreamPipeD2Ev, .-_ZN4node10StreamPipeD2Ev
	.globl	_ZN4node10StreamPipeD1Ev
	.set	_ZN4node10StreamPipeD1Ev,_ZN4node10StreamPipeD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-104(%rdi), %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	56(%r12), %eax
	subl	$1, %eax
	cmpb	$0, 62(%r12)
	movl	%eax, 56(%r12)
	jne	.L716
	cmpb	$0, 61(%r12)
	jne	.L717
	movl	%edx, %r15d
	testl	%edx, %edx
	je	.L690
	movq	16(%rdi), %r13
	testq	%r13, %r13
	je	.L718
	movq	%rsi, %r14
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN4node10StreamPipe6UnpipeEb
	movq	0(%r13), %rax
	movl	%r15d, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*32(%rax)
.L675:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L719
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L690:
	.cfi_restore_state
	cmpb	$0, 65(%r12)
	jne	.L675
	movq	(%rdi), %rax
	leaq	_ZN4node10StreamPipe16WritableListener18OnStreamWantsWriteEm(%rip), %rdx
	movq	48(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L692
	movq	$65536, 72(%r12)
	cmpb	$0, 60(%r12)
	jne	.L675
	movq	16(%r12), %rax
	leaq	-128(%rbp), %r13
	leaq	-96(%rbp), %r14
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	88(%r12), %rdi
	movb	$1, 60(%r12)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L716:
	testl	%eax, %eax
	jne	.L675
	movq	16(%r12), %rbx
	leaq	-96(%rbp), %r15
	movq	%r15, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	360(%rbx), %rax
	movq	1136(%rax), %rbx
	testq	%rdi, %rdi
	je	.L678
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L720
.L678:
	movq	3280(%rdx), %rsi
	movq	%rbx, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L695
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L721
	movq	16(%r12), %rax
	movq	352(%rax), %rax
	addq	$88, %rax
.L681:
	testq	%rax, %rax
	je	.L695
.L682:
	movq	8(%r13), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L687
	cmpq	%r13, %rax
	jne	.L684
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L712:
	cmpq	%r13, %rax
	je	.L723
.L684:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L712
.L687:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L717:
	movq	16(%r12), %rax
	leaq	-128(%rbp), %r13
	leaq	-96(%rbp), %r14
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	112(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node10StreamPipe6UnpipeEb
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L721:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L720:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L723:
	movq	16(%r13), %rax
	movq	%rax, 16(%rdx)
.L686:
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movups	%xmm0, 8(%r13)
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L692:
	movl	$65536, %esi
	call	*%rax
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L718:
	leaq	_ZZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L695:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L682
.L722:
	movq	16(%r13), %rax
	movq	%rax, 8(%rdx)
	jmp	.L686
.L719:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7637:
	.size	_ZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	16(%rdi), %r14
	testq	%r14, %r14
	je	.L727
	leaq	-104(%rdi), %r8
	movq	%rsi, %r12
	xorl	%esi, %esi
	movl	%edx, %r13d
	movq	%r8, %rdi
	call	_ZN4node10StreamPipe6UnpipeEb
	movq	(%r14), %rax
	movl	%r13d, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	40(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L727:
	.cfi_restore_state
	leaq	_ZZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7639:
	.size	_ZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16WritableListener15OnStreamDestroyEv
	.type	_ZN4node10StreamPipe16WritableListener15OnStreamDestroyEv, @function
_ZN4node10StreamPipe16WritableListener15OnStreamDestroyEv:
.LFB7641:
	.cfi_startproc
	endbr64
	movb	$1, -41(%rdi)
	subq	$104, %rdi
	xorl	%esi, %esi
	movb	$1, 61(%rdi)
	movl	$0, 56(%rdi)
	jmp	_ZN4node10StreamPipe6UnpipeEb
	.cfi_endproc
.LFE7641:
	.size	_ZN4node10StreamPipe16WritableListener15OnStreamDestroyEv, .-_ZN4node10StreamPipe16WritableListener15OnStreamDestroyEv
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe6UnpipeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node10StreamPipe6UnpipeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node10StreamPipe6UnpipeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L737
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L735
	cmpw	$1040, %cx
	jne	.L731
.L735:
	movq	23(%rdx), %rdi
.L733:
	testq	%rdi, %rdi
	je	.L729
	addq	$8, %rsp
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node10StreamPipe6UnpipeEb
	.p2align 4,,10
	.p2align 3
.L729:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L731:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L737:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7647:
	.size	_ZN4node10StreamPipe6UnpipeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node10StreamPipe6UnpipeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipeD0Ev
	.type	_ZN4node10StreamPipeD0Ev, @function
_ZN4node10StreamPipeD0Ev:
.LFB7625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10StreamPipeE(%rip), %rax
	movl	$1, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node10StreamPipe6UnpipeEb
	movq	112(%r12), %rcx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rdx
	movq	%rdx, 104(%r12)
	testq	%rcx, %rcx
	je	.L739
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L740
	leaq	104(%r12), %rsi
	cmpq	%rax, %rsi
	jne	.L742
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L767:
	cmpq	%rax, %rsi
	je	.L772
.L742:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L767
.L740:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L771:
	movq	120(%r12), %rax
	movq	%rax, 8(%rcx)
	.p2align 4,,10
	.p2align 3
.L739:
	movq	%rdx, 80(%r12)
	movq	88(%r12), %rdx
	testq	%rdx, %rdx
	jne	.L773
.L746:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L773:
	.cfi_restore_state
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L740
	leaq	80(%r12), %rcx
	cmpq	%rcx, %rax
	jne	.L748
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L768:
	cmpq	%rcx, %rax
	je	.L775
.L748:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L768
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L772:
	movq	16(%rsi), %rax
	movq	%rax, 16(%rcx)
	movq	%rdx, 80(%r12)
	movq	88(%r12), %rdx
	testq	%rdx, %rdx
	je	.L746
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L775:
	movq	96(%r12), %rax
	movq	%rax, 16(%rdx)
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L774:
	movq	96(%r12), %rax
	movq	%rax, 8(%rdx)
	jmp	.L746
	.cfi_endproc
.LFE7625:
	.size	_ZN4node10StreamPipeD0Ev, .-_ZN4node10StreamPipeD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferE
	.type	_ZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferE, @function
_ZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferE:
.LFB7636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 65(%rdi)
	je	.L863
.L777:
	movq	8(%rbx), %rdi
	call	uv_buf_init@PLT
	movq	112(%r14), %r15
	leaq	-160(%rbp), %rsi
	movq	$1, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	32(%r15), %rax
	movq	%r15, %rdi
	movq	%rdx, -136(%rbp)
	leaq	-152(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movq	-136(%rbp), %rax
	addq	%rax, 24(%r15)
	movq	(%r15), %rax
	call	*40(%rax)
	movl	%eax, %r13d
	testl	%eax, %eax
	je	.L864
.L778:
	addl	$1, 56(%r14)
.L780:
	movl	56(%r14), %eax
	leaq	104(%r14), %rbx
	subl	$1, %eax
	cmpb	$0, 62(%r14)
	movl	%eax, 56(%r14)
	jne	.L865
	cmpb	$0, 61(%r14)
	jne	.L866
	testl	%r13d, %r13d
	je	.L814
	movq	120(%r14), %r12
	testq	%r12, %r12
	je	.L867
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10StreamPipe6UnpipeEb
	movq	(%r12), %rax
	movl	%r13d, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*32(%rax)
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L864:
	cmpq	$0, -152(%rbp)
	je	.L778
	movq	-168(%rbp), %r13
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	movq	352(%r13), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3264(%r13), %rdi
	movq	3280(%r13), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -176(%rbp)
	testq	%rax, %rax
	je	.L868
	movq	%rax, %r12
	movq	%rax, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*128(%rax)
	movsd	40(%rax), %xmm0
	movq	16(%rax), %rax
	movq	%rax, -184(%rbp)
	movq	1216(%rax), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	je	.L783
	comisd	.LC8(%rip), %xmm0
	jb	.L869
.L783:
	movq	-184(%rbp), %rax
	leaq	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movq	1256(%rax), %rax
	movsd	24(%rax), %xmm2
	movsd	%xmm0, 24(%rax)
	movq	(%r15), %rax
	movq	120(%rax), %rax
	movsd	%xmm2, -200(%rbp)
	cmpq	%rdx, %rax
	jne	.L784
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-176(%rbp), %rax
	movq	%r15, 8(%r12)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L828
	cmpw	$1040, %cx
	jne	.L785
.L828:
	movq	31(%rdx), %rax
.L787:
	testq	%rax, %rax
	jne	.L870
	movq	-176(%rbp), %r13
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	32(%r15), %rsi
	leaq	40(%r12), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	%rax, 24(%r12)
	movl	$39, %ecx
	movq	%rdx, 32(%r12)
	movq	%r13, %rdx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 40(%r12)
.L789:
	movq	(%r15), %rax
	movq	-152(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	movq	-160(%rbp), %rdx
	movq	%r15, %rdi
	call	*48(%rax)
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.L871
.L790:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*64(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L795
	movq	-168(%rbp), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L872
.L796:
	movq	-168(%rbp), %rsi
	movq	-176(%rbp), %rdi
	movq	360(%rsi), %rax
	movq	3280(%rsi), %rsi
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L873
.L797:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*72(%rax)
.L795:
	movq	-184(%rbp), %rax
	movsd	-200(%rbp), %xmm1
	movq	-192(%rbp), %rdi
	movq	1256(%rax), %rax
	movsd	%xmm1, 24(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	addl	$1, 56(%r14)
	testl	%r13d, %r13d
	jne	.L780
	cmpq	$0, 24(%r12)
	movb	$0, 60(%r14)
	jne	.L874
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%rax, 24(%r12)
	movq	(%rbx), %rax
	movq	%rdx, 32(%r12)
	movq	%rax, 16(%r12)
	movq	8(%rbx), %r15
	movq	16(%rbx), %r13
	call	uv_buf_init@PLT
	movq	88(%r14), %rdi
	movq	%rax, 8(%rbx)
	movq	%rdx, 16(%rbx)
	movq	%r15, 24(%r12)
	movq	%r13, 32(%r12)
	testq	%rdi, %rdi
	je	.L776
	movq	(%rdi), %rax
	call	*24(%rax)
.L776:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L875
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L865:
	.cfi_restore_state
	testl	%eax, %eax
	jne	.L776
	movq	16(%r14), %r13
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	movq	352(%r13), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r13), %r12
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	360(%r13), %rax
	movq	8(%r14), %rdi
	movq	16(%r14), %rdx
	movq	1136(%rax), %r13
	testq	%rdi, %rdi
	je	.L802
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L802
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %rdi
.L802:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L820
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L876
	movq	16(%r14), %rax
	movq	352(%rax), %rax
	addq	$88, %rax
.L805:
	testq	%rax, %rax
	je	.L820
.L806:
	movq	112(%r14), %rdx
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L811
	cmpq	%rax, %rbx
	jne	.L808
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L858:
	cmpq	%rax, %rbx
	je	.L878
.L808:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L858
.L811:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L814:
	cmpb	$0, 65(%r14)
	jne	.L776
	cmpb	$0, 60(%r14)
	movq	$65536, 72(%r14)
	jne	.L776
	movq	16(%r14), %rax
	leaq	-128(%rbp), %r12
	leaq	-96(%rbp), %r13
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	88(%r14), %rdi
	movb	$1, 60(%r14)
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L863:
	movl	56(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L777
	leaq	_ZZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L871:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	leaq	-128(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L791
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L879
.L791:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-128(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L880
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L824
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L794
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L794:
	xorl	%r12d, %r12d
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L866:
	movq	16(%r14), %rax
	leaq	-128(%rbp), %r12
	leaq	-96(%rbp), %r13
	movq	%r12, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	112(%r14), %rdi
	xorl	%esi, %esi
	call	_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN4node10StreamPipe6UnpipeEb
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L868:
	movq	-192(%rbp), %rdi
	movl	$-16, %r13d
	call	_ZN2v811HandleScopeD1Ev@PLT
	addl	$1, 56(%r14)
	jmp	.L780
	.p2align 4,,10
	.p2align 3
.L784:
	movq	-176(%rbp), %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L879:
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r11
	movq	%r11, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L820:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L869:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L874:
	leaq	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L785:
	movq	-176(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L870:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L878:
	movq	16(%rbx), %rax
	movq	%rax, 16(%rdx)
.L810:
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movups	%xmm0, 112(%r14)
	call	_ZN2v87Context4ExitEv@PLT
	movq	-192(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L867:
	leaq	_ZZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L880:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L827
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L793:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L824:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L876:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L805
.L872:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rcx
	jmp	.L796
.L873:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L797
.L877:
	movq	120(%r14), %rax
	movq	%rax, 8(%rdx)
	jmp	.L810
.L827:
	xorl	%edx, %edx
	jmp	.L793
.L875:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7636:
	.size	_ZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferE, .-_ZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferE
	.align 2
	.p2align 4
	.globl	_ZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_t
	.type	_ZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_t, @function
_ZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_t:
.LFB7635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%r13), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	movhps	(%rdx), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	testq	%rsi, %rsi
	jns	.L882
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movq	112(%r13), %r15
	movb	$1, 61(%r13)
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	16(%rbx), %r14
	testq	%r14, %r14
	je	.L901
	movq	(%r14), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rax), %rbx
	call	uv_buf_init@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rax, -96(%rbp)
	movq	%rdx, -88(%rbp)
	leaq	-96(%rbp), %rdx
	call	*%rbx
	movl	56(%r13), %eax
	testl	%eax, %eax
	je	.L902
.L884:
	movq	-72(%rbp), %r12
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-64(%rbp), %r13
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r12, %r12
	je	.L881
	movq	-80(%rbp), %rax
	testq	%rax, %rax
	je	.L903
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L881:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L904
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L882:
	.cfi_restore_state
	leaq	-80(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferE
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L902:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN4node10StreamBase8ShutdownEN2v85LocalINS1_6ObjectEEE
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN4node10StreamPipe6UnpipeEb
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L903:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L901:
	leaq	_ZZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L904:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7635:
	.size	_ZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_t, .-_ZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_t
	.weak	_ZTVN4node9StreamReqE
	.section	.data.rel.ro._ZTVN4node9StreamReqE,"awG",@progbits,_ZTVN4node9StreamReqE,comdat
	.align 8
	.type	_ZTVN4node9StreamReqE, @object
	.size	_ZTVN4node9StreamReqE, 48
_ZTVN4node9StreamReqE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE, 168
_ZTVN4node18SimpleShutdownWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-16
	.quad	0
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn16_N4node18SimpleShutdownWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn16_NK4node18SimpleShutdownWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, 168
_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-40
	.quad	0
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node10StreamPipeE
	.section	.data.rel.ro._ZTVN4node10StreamPipeE,"awG",@progbits,_ZTVN4node10StreamPipeE,comdat
	.align 8
	.type	_ZTVN4node10StreamPipeE, @object
	.size	_ZTVN4node10StreamPipeE, 96
_ZTVN4node10StreamPipeE:
	.quad	0
	.quad	0
	.quad	_ZN4node10StreamPipeD1Ev
	.quad	_ZN4node10StreamPipeD0Ev
	.quad	_ZNK4node10StreamPipe10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10StreamPipe14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node10StreamPipe8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node10StreamPipe16ReadableListenerE
	.section	.data.rel.ro.local._ZTVN4node10StreamPipe16ReadableListenerE,"awG",@progbits,_ZTVN4node10StreamPipe16ReadableListenerE,comdat
	.align 8
	.type	_ZTVN4node10StreamPipe16ReadableListenerE, @object
	.size	_ZTVN4node10StreamPipe16ReadableListenerE, 80
_ZTVN4node10StreamPipe16ReadableListenerE:
	.quad	0
	.quad	0
	.quad	_ZN4node10StreamPipe16ReadableListenerD1Ev
	.quad	_ZN4node10StreamPipe16ReadableListenerD0Ev
	.quad	_ZN4node10StreamPipe16ReadableListener13OnStreamAllocEm
	.quad	_ZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_t
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node10StreamPipe16ReadableListener15OnStreamDestroyEv
	.weak	_ZTVN4node10StreamPipe16WritableListenerE
	.section	.data.rel.ro.local._ZTVN4node10StreamPipe16WritableListenerE,"awG",@progbits,_ZTVN4node10StreamPipe16WritableListenerE,comdat
	.align 8
	.type	_ZTVN4node10StreamPipe16WritableListenerE, @object
	.size	_ZTVN4node10StreamPipe16WritableListenerE, 80
_ZTVN4node10StreamPipe16WritableListenerE:
	.quad	0
	.quad	0
	.quad	_ZN4node10StreamPipe16WritableListenerD1Ev
	.quad	_ZN4node10StreamPipe16WritableListenerD0Ev
	.quad	_ZN4node10StreamPipe16WritableListener13OnStreamAllocEm
	.quad	_ZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_t
	.quad	_ZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node10StreamPipe16WritableListener18OnStreamWantsWriteEm
	.quad	_ZN4node10StreamPipe16WritableListener15OnStreamDestroyEv
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_10StreamPipe6UnpipeEbEUlS2_E_E4CallES2_
	.section	.rodata.str1.1
.LC9:
	.string	"../src/stream_pipe.cc"
.LC10:
	.string	"stream_pipe"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC9
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_120InitializeStreamPipeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC10
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC11:
	.string	"../src/stream_pipe.cc:251"
.LC12:
	.string	"args[1]->IsObject()"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC13:
	.string	"static void node::StreamPipe::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.section	.rodata.str1.1
.LC14:
	.string	"../src/stream_pipe.cc:250"
.LC15:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC13
	.section	.rodata.str1.1
.LC16:
	.string	"../src/stream_pipe.cc:249"
.LC17:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node10StreamPipe3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC13
	.section	.rodata.str1.1
.LC18:
	.string	"../src/stream_pipe.cc:244"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC20:
	.string	"virtual void node::StreamPipe::WritableListener::OnStreamRead(ssize_t, const uv_buf_t&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_tE4args, @object
	.size	_ZZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_tE4args, 24
_ZZN4node10StreamPipe16WritableListener12OnStreamReadElRK8uv_buf_tE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"../src/stream_pipe.cc:238"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"virtual uv_buf_t node::StreamPipe::WritableListener::OnStreamAlloc(size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe16WritableListener13OnStreamAllocEmE4args, @object
	.size	_ZZN4node10StreamPipe16WritableListener13OnStreamAllocEmE4args, 24
_ZZN4node10StreamPipe16WritableListener13OnStreamAllocEmE4args:
	.quad	.LC21
	.quad	.LC19
	.quad	.LC22
	.section	.rodata.str1.1
.LC23:
	.string	"../src/stream_pipe.cc:203"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"virtual void node::StreamPipe::WritableListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @object
	.size	_ZZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node10StreamPipe16WritableListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC23
	.quad	.LC19
	.quad	.LC24
	.section	.rodata.str1.1
.LC25:
	.string	"../src/stream_pipe.cc:188"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"virtual void node::StreamPipe::WritableListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @object
	.size	_ZZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node10StreamPipe16WritableListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC25
	.quad	.LC19
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"../src/stream_pipe.cc:149"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"uses_wants_write_ || pending_writes_ == 0"
	.align 8
.LC29:
	.string	"void node::StreamPipe::ProcessData(size_t, node::AllocatedBuffer&&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferEE4args, @object
	.size	_ZZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferEE4args, 24
_ZZN4node10StreamPipe11ProcessDataEmONS_15AllocatedBufferEE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/stream_pipe.cc:134"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"virtual void node::StreamPipe::ReadableListener::OnStreamRead(ssize_t, const uv_buf_t&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_tE4args, @object
	.size	_ZZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_tE4args, 24
_ZZN4node10StreamPipe16ReadableListener12OnStreamReadElRK8uv_buf_tE4args:
	.quad	.LC30
	.quad	.LC19
	.quad	.LC31
	.section	.rodata.str1.1
.LC32:
	.string	"../src/stream_pipe.cc:118"
.LC33:
	.string	"(size) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"virtual uv_buf_t node::StreamPipe::ReadableListener::OnStreamAlloc(size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipe16ReadableListener13OnStreamAllocEmE4args, @object
	.size	_ZZN4node10StreamPipe16ReadableListener13OnStreamAllocEmE4args, 24
_ZZN4node10StreamPipe16ReadableListener13OnStreamAllocEmE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"../src/stream_pipe.cc:22"
.LC36:
	.string	"(sink) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"node::StreamPipe::StreamPipe(node::StreamBase*, node::StreamBase*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node10StreamPipeC4EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEEE4args, @object
	.size	_ZZN4node10StreamPipeC4EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node10StreamPipeC4EPNS_10StreamBaseES2_N2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args
	.section	.rodata.str1.1
.LC38:
	.string	"../src/stream_base-inl.h:289"
.LC39:
	.string	"(storage_.data()) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"void node::WriteWrap::SetAllocatedStorage(node::AllocatedBuffer&&)"
	.section	.data.rel.ro.local._ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args,"awG",@progbits,_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args,comdat
	.align 16
	.type	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args, @gnu_unique_object
	.size	_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args, 24
_ZZN4node9WriteWrap19SetAllocatedStorageEONS_15AllocatedBufferEE4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC41:
	.string	"../src/stream_base-inl.h:103"
.LC42:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.weak	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC44:
	.string	"../src/stream_base-inl.h:85"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"(listener->stream_) == nullptr"
	.align 8
.LC46:
	.string	"void node::StreamResource::PushStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC47
	.quad	.LC19
	.quad	.LC48
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC49:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC49
	.quad	.LC19
	.quad	.LC50
	.weak	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC51:
	.string	"../src/stream_base-inl.h:26"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"(req_wrap_obj->GetAlignedPointerFromInternalField( StreamReq::kStreamReqField)) == (nullptr)"
	.align 8
.LC53:
	.string	"void node::StreamReq::AttachToObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC54:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC56:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC57:
	.string	"../src/base_object-inl.h:195"
.LC58:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC57
	.quad	.LC58
	.quad	.LC56
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC59:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC61:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC62:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC64:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC65:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC67:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC68:
	.string	"../src/env-inl.h:955"
.LC69:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.weak	_ZZN4node11Environment8AllocateEmE4args
	.section	.rodata.str1.1
.LC71:
	.string	"../src/env-inl.h:889"
.LC72:
	.string	"(ret) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"char* node::Environment::Allocate(size_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8AllocateEmE4args,"awG",@progbits,_ZZN4node11Environment8AllocateEmE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8AllocateEmE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8AllocateEmE4args, 24
_ZZN4node11Environment8AllocateEmE4args:
	.quad	.LC71
	.quad	.LC72
	.quad	.LC73
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC74:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC76:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	7517463719428581715
	.quad	8239175502546629749
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-1074790400
	.align 8
.LC8:
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
