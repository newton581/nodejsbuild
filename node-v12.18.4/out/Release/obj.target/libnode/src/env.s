	.file	"env.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4687:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4687:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4689:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4689:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node14MemoryRetainer13WrappedObjectEv,"axG",@progbits,_ZNK4node14MemoryRetainer13WrappedObjectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.type	_ZNK4node14MemoryRetainer13WrappedObjectEv, @function
_ZNK4node14MemoryRetainer13WrappedObjectEv:
.LFB4971:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4971:
	.size	_ZNK4node14MemoryRetainer13WrappedObjectEv, .-_ZNK4node14MemoryRetainer13WrappedObjectEv
	.section	.text._ZNK4node14MemoryRetainer10IsRootNodeEv,"axG",@progbits,_ZNK4node14MemoryRetainer10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.type	_ZNK4node14MemoryRetainer10IsRootNodeEv, @function
_ZNK4node14MemoryRetainer10IsRootNodeEv:
.LFB4972:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4972:
	.size	_ZNK4node14MemoryRetainer10IsRootNodeEv, .-_ZNK4node14MemoryRetainer10IsRootNodeEv
	.section	.text._ZNK4node11IsolateData8SelfSizeEv,"axG",@progbits,_ZNK4node11IsolateData8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11IsolateData8SelfSizeEv
	.type	_ZNK4node11IsolateData8SelfSizeEv, @function
_ZNK4node11IsolateData8SelfSizeEv:
.LFB5969:
	.cfi_startproc
	endbr64
	movl	$2416, %eax
	ret
	.cfi_endproc
.LFE5969:
	.size	_ZNK4node11IsolateData8SelfSizeEv, .-_ZNK4node11IsolateData8SelfSizeEv
	.section	.text._ZNK4node10AsyncHooks8SelfSizeEv,"axG",@progbits,_ZNK4node10AsyncHooks8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10AsyncHooks8SelfSizeEv
	.type	_ZNK4node10AsyncHooks8SelfSizeEv, @function
_ZNK4node10AsyncHooks8SelfSizeEv:
.LFB5974:
	.cfi_startproc
	endbr64
	movl	$136, %eax
	ret
	.cfi_endproc
.LFE5974:
	.size	_ZNK4node10AsyncHooks8SelfSizeEv, .-_ZNK4node10AsyncHooks8SelfSizeEv
	.section	.text._ZNK4node13ImmediateInfo8SelfSizeEv,"axG",@progbits,_ZNK4node13ImmediateInfo8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13ImmediateInfo8SelfSizeEv
	.type	_ZNK4node13ImmediateInfo8SelfSizeEv, @function
_ZNK4node13ImmediateInfo8SelfSizeEv:
.LFB5976:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE5976:
	.size	_ZNK4node13ImmediateInfo8SelfSizeEv, .-_ZNK4node13ImmediateInfo8SelfSizeEv
	.section	.text._ZNK4node8TickInfo8SelfSizeEv,"axG",@progbits,_ZNK4node8TickInfo8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8TickInfo8SelfSizeEv
	.type	_ZNK4node8TickInfo8SelfSizeEv, @function
_ZNK4node8TickInfo8SelfSizeEv:
.LFB5978:
	.cfi_startproc
	endbr64
	movl	$48, %eax
	ret
	.cfi_endproc
.LFE5978:
	.size	_ZNK4node8TickInfo8SelfSizeEv, .-_ZNK4node8TickInfo8SelfSizeEv
	.section	.text._ZNK4node11Environment10IsRootNodeEv,"axG",@progbits,_ZNK4node11Environment10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment10IsRootNodeEv
	.type	_ZNK4node11Environment10IsRootNodeEv, @function
_ZNK4node11Environment10IsRootNodeEv:
.LFB6015:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE6015:
	.size	_ZNK4node11Environment10IsRootNodeEv, .-_ZNK4node11Environment10IsRootNodeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB6063:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE6063:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB6064:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE6064:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB6065:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE6065:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB6067:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L15
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE6067:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN4node24NodeArrayBufferAllocator7GetImplEv,"axG",@progbits,_ZN4node24NodeArrayBufferAllocator7GetImplEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocator7GetImplEv
	.type	_ZN4node24NodeArrayBufferAllocator7GetImplEv, @function
_ZN4node24NodeArrayBufferAllocator7GetImplEv:
.LFB6689:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE6689:
	.size	_ZN4node24NodeArrayBufferAllocator7GetImplEv, .-_ZN4node24NodeArrayBufferAllocator7GetImplEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7879:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7879:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.text
	.p2align 4
	.type	_ZZN4node11Environment18ToggleImmediateRefEbENUlP9uv_idle_sE_4_FUNES2_, @function
_ZZN4node11Environment18ToggleImmediateRefEbENUlP9uv_idle_sE_4_FUNES2_:
.LFB8333:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8333:
	.size	_ZZN4node11Environment18ToggleImmediateRefEbENUlP9uv_idle_sE_4_FUNES2_, .-_ZZN4node11Environment18ToggleImmediateRefEbENUlP9uv_idle_sE_4_FUNES2_
	.section	.text._ZNK4node11Environment8SelfSizeEv,"axG",@progbits,_ZNK4node11Environment8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment8SelfSizeEv
	.type	_ZNK4node11Environment8SelfSizeEv, @function
_ZNK4node11Environment8SelfSizeEv:
.LFB8350:
	.cfi_startproc
	endbr64
	movl	$3056, %eax
	ret
	.cfi_endproc
.LFE8350:
	.size	_ZNK4node11Environment8SelfSizeEv, .-_ZNK4node11Environment8SelfSizeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node10BaseObject18IsDoneInitializingEv
	.type	_ZNK4node10BaseObject18IsDoneInitializingEv, @function
_ZNK4node10BaseObject18IsDoneInitializingEv:
.LFB8382:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE8382:
	.size	_ZNK4node10BaseObject18IsDoneInitializingEv, .-_ZNK4node10BaseObject18IsDoneInitializingEv
	.align 2
	.p2align 4
	.globl	_ZNK4node10BaseObject10IsRootNodeEv
	.type	_ZNK4node10BaseObject10IsRootNodeEv, @function
_ZNK4node10BaseObject10IsRootNodeEv:
.LFB8384:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L23
	movzbl	11(%rax), %eax
	andl	$7, %eax
	cmpb	$2, %al
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE8384:
	.size	_ZNK4node10BaseObject10IsRootNodeEv, .-_ZNK4node10BaseObject10IsRootNodeEv
	.section	.text._ZN4node26TrackingTraceStateObserverD2Ev,"axG",@progbits,_ZN4node26TrackingTraceStateObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node26TrackingTraceStateObserverD2Ev
	.type	_ZN4node26TrackingTraceStateObserverD2Ev, @function
_ZN4node26TrackingTraceStateObserverD2Ev:
.LFB10683:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10683:
	.size	_ZN4node26TrackingTraceStateObserverD2Ev, .-_ZN4node26TrackingTraceStateObserverD2Ev
	.weak	_ZN4node26TrackingTraceStateObserverD1Ev
	.set	_ZN4node26TrackingTraceStateObserverD1Ev,_ZN4node26TrackingTraceStateObserverD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED2Ev:
.LFB12417:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L25
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L25:
	ret
	.cfi_endproc
.LFE12417:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB13427:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE13427:
	.size	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB13431:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE13431:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB13435:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE13435:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB13517:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE13517:
	.size	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB13518:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L31:
	ret
	.cfi_endproc
.LFE13518:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB13520:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE13520:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB13521:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L34
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE13521:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB13523:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE13523:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB13459:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L37
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	ret
	.cfi_endproc
.LFE13459:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.text
	.p2align 4
	.type	_ZZN4node11Environment11CloseHandleI11uv_handle_sZZNS0_22RegisterHandleCleanupsEvENKUlPS0_PS2_PvE_clES3_S4_S5_EUlS4_E_EEvPT_T0_ENUlS4_E_4_FUNES4_, @function
_ZZN4node11Environment11CloseHandleI11uv_handle_sZZNS0_22RegisterHandleCleanupsEvENKUlPS0_PS2_PvE_clES3_S4_S5_EUlS4_E_EEvPT_T0_ENUlS4_E_4_FUNES4_:
.LFB9848:
	.cfi_startproc
	endbr64
	movq	(%rdi), %r8
	movl	$24, %esi
	movq	(%r8), %rax
	subl	$1, 2152(%rax)
	movq	16(%r8), %rax
	movq	%rax, (%rdi)
	movq	%r8, %rdi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9848:
	.size	_ZZN4node11Environment11CloseHandleI11uv_handle_sZZNS0_22RegisterHandleCleanupsEvENKUlPS0_PS2_PvE_clES3_S4_S5_EUlS4_E_EEvPT_T0_ENUlS4_E_4_FUNES4_, .-_ZZN4node11Environment11CloseHandleI11uv_handle_sZZNS0_22RegisterHandleCleanupsEvENKUlPS0_PS2_PvE_clES3_S4_S5_EUlS4_E_EEvPT_T0_ENUlS4_E_4_FUNES4_
	.section	.text._ZN4node26TrackingTraceStateObserverD0Ev,"axG",@progbits,_ZN4node26TrackingTraceStateObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node26TrackingTraceStateObserverD0Ev
	.type	_ZN4node26TrackingTraceStateObserverD0Ev, @function
_ZN4node26TrackingTraceStateObserverD0Ev:
.LFB10685:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10685:
	.size	_ZN4node26TrackingTraceStateObserverD0Ev, .-_ZN4node26TrackingTraceStateObserverD0Ev
	.section	.text._ZN4node22NodeTraceStateObserverD0Ev,"axG",@progbits,_ZN4node22NodeTraceStateObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22NodeTraceStateObserverD0Ev
	.type	_ZN4node22NodeTraceStateObserverD0Ev, @function
_ZN4node22NodeTraceStateObserverD0Ev:
.LFB10475:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10475:
	.size	_ZN4node22NodeTraceStateObserverD0Ev, .-_ZN4node22NodeTraceStateObserverD0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB13429:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13429:
	.size	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB13516:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13516:
	.size	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB13437:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13437:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB13522:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13522:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB13433:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13433:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB13519:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13519:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED0Ev:
.LFB12419:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	(%rdi), %rax
	call	*8(%rax)
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12419:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED0Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB13461:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L55
	call	_ZdlPv@PLT
.L55:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13461:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZN4node7tracing11TracedValueD0Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD0Ev
	.type	_ZN4node7tracing11TracedValueD0Ev, @function
_ZN4node7tracing11TracedValueD0Ev:
.LFB10433:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L58
	call	_ZdlPv@PLT
.L58:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10433:
	.size	_ZN4node7tracing11TracedValueD0Ev, .-_ZN4node7tracing11TracedValueD0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB13515:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L60
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L62
	call	_ZdlPv@PLT
.L62:
	movq	%r12, %rdi
	call	uv_mutex_destroy@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$80, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13515:
	.size	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.text
	.p2align 4
	.globl	_ZN4node19InitThreadLocalOnceEv
	.type	_ZN4node19InitThreadLocalOnceEv, @function
_ZN4node19InitThreadLocalOnceEv:
.LFB7991:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11Environment16thread_local_envE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_key_create@PLT
	testl	%eax, %eax
	jne	.L67
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	_ZZN4node19InitThreadLocalOnceEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7991:
	.size	_ZN4node19InitThreadLocalOnceEv, .-_ZN4node19InitThreadLocalOnceEv
	.section	.text._ZN4node8TickInfoD2Ev,"axG",@progbits,_ZN4node8TickInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8TickInfoD2Ev
	.type	_ZN4node8TickInfoD2Ev, @function
_ZN4node8TickInfoD2Ev:
.LFB13443:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node8TickInfoE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L68
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L68:
	ret
	.cfi_endproc
.LFE13443:
	.size	_ZN4node8TickInfoD2Ev, .-_ZN4node8TickInfoD2Ev
	.weak	_ZN4node8TickInfoD1Ev
	.set	_ZN4node8TickInfoD1Ev,_ZN4node8TickInfoD2Ev
	.section	.text._ZN4node8TickInfoD0Ev,"axG",@progbits,_ZN4node8TickInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8TickInfoD0Ev
	.type	_ZN4node8TickInfoD0Ev, @function
_ZN4node8TickInfoD0Ev:
.LFB13445:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node8TickInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L71:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13445:
	.size	_ZN4node8TickInfoD0Ev, .-_ZN4node8TickInfoD0Ev
	.section	.text._ZN4node13ImmediateInfoD2Ev,"axG",@progbits,_ZN4node13ImmediateInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13ImmediateInfoD2Ev
	.type	_ZN4node13ImmediateInfoD2Ev, @function
_ZN4node13ImmediateInfoD2Ev:
.LFB13447:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node13ImmediateInfoE(%rip), %rax
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L76
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	ret
	.cfi_endproc
.LFE13447:
	.size	_ZN4node13ImmediateInfoD2Ev, .-_ZN4node13ImmediateInfoD2Ev
	.weak	_ZN4node13ImmediateInfoD1Ev
	.set	_ZN4node13ImmediateInfoD1Ev,_ZN4node13ImmediateInfoD2Ev
	.section	.text._ZN4node13ImmediateInfoD0Ev,"axG",@progbits,_ZN4node13ImmediateInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13ImmediateInfoD0Ev
	.type	_ZN4node13ImmediateInfoD0Ev, @function
_ZN4node13ImmediateInfoD0Ev:
.LFB13449:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13ImmediateInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L79
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L79:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13449:
	.size	_ZN4node13ImmediateInfoD0Ev, .-_ZN4node13ImmediateInfoD0Ev
	.section	.text._ZN4node10AsyncHooksD2Ev,"axG",@progbits,_ZN4node10AsyncHooksD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10AsyncHooksD2Ev
	.type	_ZN4node10AsyncHooksD2Ev, @function
_ZN4node10AsyncHooksD2Ev:
.LFB13439:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10AsyncHooksE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L85
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L85:
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L86
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L86:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L87
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L87:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L84
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L84:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13439:
	.size	_ZN4node10AsyncHooksD2Ev, .-_ZN4node10AsyncHooksD2Ev
	.weak	_ZN4node10AsyncHooksD1Ev
	.set	_ZN4node10AsyncHooksD1Ev,_ZN4node10AsyncHooksD2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_:
.LFB13524:
	.cfi_startproc
	endbr64
	movq	360(%rsi), %rax
	movq	2360(%rax), %rdi
	jmp	uv_stop@PLT
	.cfi_endproc
.LFE13524:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_
	.p2align 4
	.type	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP10uv_check_sE0_4_FUNES2_, @function
_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP10uv_check_sE0_4_FUNES2_:
.LFB8308:
	.cfi_startproc
	endbr64
	movq	-528(%rdi), %rdi
	xorl	%esi, %esi
	jmp	_ZN2v87Isolate7SetIdleEb@PLT
	.cfi_endproc
.LFE8308:
	.size	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP10uv_check_sE0_4_FUNES2_, .-_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP10uv_check_sE0_4_FUNES2_
	.p2align 4
	.type	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP12uv_prepare_sE_4_FUNES2_, @function
_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP12uv_prepare_sE_4_FUNES2_:
.LFB8305:
	.cfi_startproc
	endbr64
	movq	-408(%rdi), %rdi
	movl	$1, %esi
	jmp	_ZN2v87Isolate7SetIdleEb@PLT
	.cfi_endproc
.LFE8305:
	.size	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP12uv_prepare_sE_4_FUNES2_, .-_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP12uv_prepare_sE_4_FUNES2_
	.p2align 4
	.type	_ZZN4node11Environment22RegisterHandleCleanupsEvENUlPS0_P11uv_handle_sPvE_4_FUNES1_S3_S4_, @function
_ZZN4node11Environment22RegisterHandleCleanupsEvENUlPS0_P11uv_handle_sPvE_4_FUNES1_S3_S4_:
.LFB8298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rdi, (%rsi)
	addl	$1, 2152(%rdi)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%r12, %rdi
	leaq	_ZZN4node11Environment11CloseHandleI11uv_handle_sZZNS0_22RegisterHandleCleanupsEvENKUlPS0_PS2_PvE_clES3_S4_S5_EUlS4_E_EEvPT_T0_ENUlS4_E_4_FUNES4_(%rip), %rsi
	movq	%rbx, (%rax)
	movq	%rdx, 16(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_close@PLT
	.cfi_endproc
.LFE8298:
	.size	_ZZN4node11Environment22RegisterHandleCleanupsEvENUlPS0_P11uv_handle_sPvE_4_FUNES1_S3_S4_, .-_ZZN4node11Environment22RegisterHandleCleanupsEvENUlPS0_P11uv_handle_sPvE_4_FUNES1_S3_S4_
	.align 2
	.p2align 4
	.globl	_ZNK4node10BaseObject13WrappedObjectEv
	.type	_ZNK4node10BaseObject13WrappedObjectEv, @function
_ZNK4node10BaseObject13WrappedObjectEv:
.LFB8383:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L110
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L113
.L110:
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	16(%rdi), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8383:
	.size	_ZNK4node10BaseObject13WrappedObjectEv, .-_ZNK4node10BaseObject13WrappedObjectEv
	.p2align 4
	.type	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_T0_SH_T1_T2_.isra.0, @function
_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_T0_SH_T1_T2_.isra.0:
.LFB13837:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	-1(%rdx), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	movq	16(%rbp), %r11
	movq	32(%rbp), %r10
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rax, %r13
	andl	$1, %r14d
	shrq	$63, %r13
	pushq	%r12
	addq	%rax, %r13
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	sarq	%r13
	cmpq	%r13, %rsi
	jge	.L115
	movq	%rsi, %r9
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	1(%r9), %rcx
	leaq	(%rcx,%rcx), %rax
	leaq	-1(%rax), %r8
	leaq	(%rax,%rcx,4), %rcx
	leaq	(%r8,%r8,2), %r12
	leaq	(%rdi,%rcx,8), %rcx
	leaq	(%rdi,%r12,8), %r12
	movq	16(%rcx), %r15
	cmpq	%r15, 16(%r12)
	jb	.L116
	movdqu	(%rcx), %xmm1
	leaq	(%r9,%r9,2), %r8
	leaq	(%rdi,%r8,8), %r8
	movups	%xmm1, (%r8)
	movq	16(%rcx), %r9
	movq	%r9, 16(%r8)
	cmpq	%rax, %r13
	jle	.L117
	movq	%rax, %r9
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L116:
	movdqu	(%r12), %xmm2
	leaq	(%r9,%r9,2), %rax
	leaq	(%rdi,%rax,8), %rax
	movups	%xmm2, (%rax)
	movq	16(%r12), %rcx
	movq	%rcx, 16(%rax)
	cmpq	%r8, %r13
	jle	.L124
	movq	%r8, %r9
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r12, %rcx
	movq	%r8, %rax
.L117:
	testq	%r14, %r14
	je	.L123
.L120:
	leaq	-1(%rax), %rdx
	movq	%rdx, %r8
	shrq	$63, %r8
	addq	%rdx, %r8
	sarq	%r8
	cmpq	%rsi, %rax
	jg	.L122
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L130:
	movdqu	(%r9), %xmm0
	leaq	-1(%r8), %rdx
	movups	%xmm0, (%rcx)
	movq	16(%r9), %rax
	movq	%rax, 16(%rcx)
	movq	%rdx, %rax
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	movq	%rax, %rdx
	movq	%r8, %rax
	cmpq	%r8, %rsi
	jge	.L129
	movq	%rdx, %r8
.L122:
	leaq	(%r8,%r8,2), %rdx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rdx,8), %r9
	leaq	(%rdi,%rax,8), %rcx
	cmpq	%r10, 16(%r9)
	ja	.L130
.L121:
	movq	%r11, (%rcx)
	movq	%rbx, 8(%rcx)
	movq	%r10, 16(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rdi,%rax,8), %rcx
	testq	%r14, %r14
	jne	.L121
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	-2(%rdx), %r8
	movq	%r8, %rdx
	shrq	$63, %rdx
	addq	%r8, %rdx
	sarq	%rdx
	cmpq	%rax, %rdx
	jne	.L120
	leaq	1(%rax,%rax), %rax
	leaq	(%rax,%rax,2), %rdx
	leaq	(%rdi,%rdx,8), %rdx
	movdqu	(%rdx), %xmm3
	movups	%xmm3, (%rcx)
	movq	16(%rdx), %r8
	movq	%r8, 16(%rcx)
	movq	%rdx, %rcx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r9, %rcx
	jmp	.L121
	.cfi_endproc
.LFE13837:
	.size	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_T0_SH_T1_T2_.isra.0, .-_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_T0_SH_T1_T2_.isra.0
	.p2align 4
	.type	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_.isra.0, @function
_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_.isra.0:
.LFB14135:
	.cfi_startproc
	cmpq	%rsi, %rdi
	je	.L143
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	24(%rdi), %rbx
	subq	$24, %rsp
	cmpq	%rbx, %rsi
	jne	.L139
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L146:
	cmpq	%rbx, %r12
	je	.L135
	movl	$24, %eax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r8, -56(%rbp)
	subq	%r12, %rdx
	leaq	(%r12,%rax), %rdi
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L135:
	addq	$24, %rbx
	movq	%r8, (%r12)
	movq	%r15, 8(%r12)
	movq	%r13, 16(%r12)
	cmpq	%rbx, %r14
	je	.L131
.L139:
	movq	16(%rbx), %r13
	movq	(%rbx), %r8
	movq	%rbx, %rcx
	movq	8(%rbx), %r15
	cmpq	16(%r12), %r13
	ja	.L146
	cmpq	-8(%rbx), %r13
	jbe	.L137
	leaq	-24(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L138:
	movdqu	(%rax), %xmm0
	movq	16(%rax), %rdx
	movq	%rax, %rcx
	subq	$24, %rax
	movups	%xmm0, 48(%rax)
	movq	%rdx, 64(%rax)
	cmpq	16(%rax), %r13
	ja	.L138
.L137:
	addq	$24, %rbx
	movq	%r8, (%rcx)
	movq	%r15, 8(%rcx)
	movq	%r13, 16(%rcx)
	cmpq	%rbx, %r14
	jne	.L139
.L131:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE14135:
	.size	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_.isra.0, .-_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_.isra.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"fields"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node8TickInfo10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node8TickInfo10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node8TickInfo10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8339:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L147
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L147
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L154
	cmpq	72(%rbx), %rcx
	je	.L159
.L152:
	movq	-8(%rcx), %rsi
.L151:
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L147:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L160
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%esi, %esi
	jmp	.L151
.L160:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8339:
	.size	_ZNK4node8TickInfo10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node8TickInfo10MemoryInfoEPNS_13MemoryTrackerE
	.align 2
	.p2align 4
	.type	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0, @function
_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0:
.LFB14186:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L161
	movq	8(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rsi, %r12
	leaq	-48(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L166
	cmpq	72(%rbx), %rcx
	je	.L171
.L164:
	movq	-8(%rcx), %rsi
.L163:
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	*%r14
.L161:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L172
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	xorl	%esi, %esi
	jmp	.L163
.L172:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE14186:
	.size	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0, .-_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	.p2align 4
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_T1_, @function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_T1_:
.LFB11763:
	.cfi_startproc
	movq	%rsi, %rax
	subq	%rdi, %rax
	cmpq	$384, %rax
	jle	.L202
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	testq	%rdx, %rdx
	je	.L175
	leaq	24(%rdi), %r15
.L177:
	movq	%rsi, %rax
	movq	-8(%rsi), %r9
	movq	(%rbx), %r11
	subq	$1, %r14
	subq	%rbx, %rax
	movq	8(%rbx), %r10
	movq	16(%rbx), %r8
	movabsq	$-6148914691236517205, %rcx
	sarq	$3, %rax
	imulq	%rax, %rcx
	movq	%rcx, %rax
	shrq	$63, %rax
	addq	%rcx, %rax
	movq	%rax, %rcx
	andq	$-2, %rax
	sarq	%rcx
	addq	%rcx, %rax
	movq	40(%rbx), %rcx
	leaq	(%rbx,%rax,8), %rax
	movq	16(%rax), %rdi
	cmpq	%rdi, %rcx
	jbe	.L182
	cmpq	%r9, %rdi
	jbe	.L183
	movdqu	(%rax), %xmm4
	movups	%xmm4, (%rbx)
.L205:
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movq	%r11, (%rax)
	movq	%r10, 8(%rax)
	movq	%r8, 16(%rax)
	movq	-8(%rsi), %r8
.L184:
	movq	16(%rbx), %rdi
	movq	%r15, %r12
	movq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L188:
	movq	16(%r12), %r9
	movq	%r12, %r13
	cmpq	%rdi, %r9
	ja	.L189
	leaq	-24(%rcx), %rax
	cmpq	%rdi, %r8
	jnb	.L190
	subq	$48, %rcx
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rcx, %rax
	subq	$24, %rcx
	cmpq	%rdi, 40(%rcx)
	jb	.L191
.L190:
	cmpq	%rax, %r12
	jnb	.L208
	movdqu	(%rax), %xmm0
	movq	(%r12), %rdi
	movq	8(%r12), %rcx
	movups	%xmm0, (%r12)
	movq	16(%rax), %r8
	movq	%r8, 16(%r12)
	movq	-8(%rax), %r8
	movq	%rcx, 8(%rax)
	movq	%rax, %rcx
	movq	%rdi, (%rax)
	movq	%r9, 16(%rax)
	movq	16(%rbx), %rdi
.L189:
	addq	$24, %r12
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_T1_
	movq	%r12, %rax
	subq	%rbx, %rax
	cmpq	$384, %rax
	jle	.L173
	testq	%r14, %r14
	je	.L175
	movq	%r12, %rsi
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L182:
	cmpq	%r9, %rcx
	jbe	.L186
	movdqu	24(%rbx), %xmm5
	movups	%xmm5, (%rbx)
.L207:
	movq	%rcx, 16(%rbx)
	movq	%r11, 24(%rbx)
	movq	%r10, 32(%rbx)
	movq	%r8, 40(%rbx)
	movq	-8(%rsi), %r8
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L183:
	cmpq	%r9, %rcx
	jbe	.L185
.L206:
	movdqu	-24(%rsi), %xmm6
	movups	%xmm6, (%rbx)
	movq	-8(%rsi), %rax
	movq	%rax, 16(%rbx)
	movq	%r11, -24(%rsi)
	movq	%r10, -16(%rsi)
	movq	%r8, -8(%rsi)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L186:
	cmpq	%r9, %rdi
	ja	.L206
	movdqu	(%rax), %xmm7
	movups	%xmm7, (%rbx)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L175:
	movabsq	$-6148914691236517205, %r12
	sarq	$3, %rax
	imulq	%rax, %r12
	leaq	-2(%r12), %rsi
	sarq	%rsi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L178:
	subq	$1, %rsi
.L180:
	leaq	(%rsi,%rsi,2), %rax
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movdqu	(%rbx,%rax,8), %xmm3
	movq	16(%rbx,%rax,8), %rax
	movaps	%xmm3, -80(%rbp)
	movq	%rax, -64(%rbp)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_T0_SH_T1_T2_.isra.0
	addq	$32, %rsp
	testq	%rsi, %rsi
	jne	.L178
	movabsq	$-6148914691236517205, %r12
	subq	$24, %r13
	.p2align 4,,10
	.p2align 3
.L179:
	movq	16(%r13), %rax
	movdqu	(%rbx), %xmm2
	movq	%r13, %r14
	subq	$8, %rsp
	subq	%rbx, %r14
	movdqu	0(%r13), %xmm1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movups	%xmm2, 0(%r13)
	movq	%r14, %rdx
	subq	$24, %r13
	movq	%rax, -64(%rbp)
	movq	16(%rbx), %rax
	sarq	$3, %rdx
	movaps	%xmm1, -80(%rbp)
	imulq	%r12, %rdx
	movq	%rax, 40(%r13)
	pushq	-64(%rbp)
	pushq	-72(%rbp)
	pushq	-80(%rbp)
	call	_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElS3_NS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_T0_SH_T1_T2_.isra.0
	addq	$32, %rsp
	cmpq	$24, %r14
	jg	.L179
.L173:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	movdqu	24(%rbx), %xmm7
	movups	%xmm7, (%rbx)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE11763:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_T1_, .-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_T1_
	.align 2
	.p2align 4
	.globl	_ZNK4node13ImmediateInfo10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node13ImmediateInfo10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node13ImmediateInfo10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8338:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L209
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L209
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L216
	cmpq	72(%rbx), %rcx
	je	.L221
.L214:
	movq	-8(%rcx), %rsi
.L213:
	leaq	.LC1(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L209:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L216:
	xorl	%esi, %esi
	jmp	.L213
.L222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8338:
	.size	_ZNK4node13ImmediateInfo10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node13ImmediateInfo10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata.str1.1
.LC2:
	.string	"async_ids_stack"
.LC3:
	.string	"async_id_fields"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node10AsyncHooks10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node10AsyncHooks10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node10AsyncHooks10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8340:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L225
	movq	8(%rdi), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L225
	movq	8(%rbx), %r13
	leaq	-48(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L240
	cmpq	72(%rbx), %rcx
	je	.L253
.L228:
	movq	-8(%rcx), %rsi
.L227:
	leaq	.LC2(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L225:
	movq	80(%r12), %rax
	testq	%rax, %rax
	je	.L230
	movq	48(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L230
	movq	8(%rbx), %r13
	leaq	-48(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L241
	cmpq	72(%rbx), %rcx
	je	.L254
.L233:
	movq	-8(%rcx), %rsi
.L232:
	leaq	.LC1(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L230:
	movq	120(%r12), %rax
	testq	%rax, %rax
	je	.L223
	movq	88(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L223
	movq	8(%rbx), %r12
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L242
	cmpq	72(%rbx), %rcx
	je	.L255
.L238:
	movq	-8(%rcx), %rsi
.L237:
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L223:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L256
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L253:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L254:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L240:
	xorl	%esi, %esi
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L241:
	xorl	%esi, %esi
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L242:
	xorl	%esi, %esi
	jmp	.L237
.L256:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8340:
	.size	_ZNK4node10AsyncHooks10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node10AsyncHooks10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZN4node10AsyncHooksD0Ev,"axG",@progbits,_ZN4node10AsyncHooksD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10AsyncHooksD0Ev
	.type	_ZN4node10AsyncHooksD0Ev, @function
_ZN4node10AsyncHooksD0Ev:
.LFB13441:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node10AsyncHooksE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	128(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L258
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L258:
	movq	120(%r12), %rdi
	testq	%rdi, %rdi
	je	.L259
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L259:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L260
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L260:
	movq	40(%r12), %rdi
	testq	%rdi, %rdi
	je	.L261
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L261:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE13441:
	.size	_ZN4node10AsyncHooksD0Ev, .-_ZN4node10AsyncHooksD0Ev
	.text
	.p2align 4
	.type	_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEENUlPvE_4_FUNES6_, @function
_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEENUlPvE_4_FUNES6_:
.LFB8377:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L275
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L277
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L278
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L277
.L278:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	16(%r14), %r13
	testq	%r13, %r13
	je	.L285
	lock subl	$1, 8(%r13)
	jne	.L285
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L285
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L278
	.p2align 4,,10
	.p2align 3
.L277:
	movq	8(%r12), %rax
	movq	(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 24(%r12)
	movq	$0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L287
	call	_ZdlPv@PLT
.L287:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L281
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L277
.L283:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	16(%r14), %r13
	testq	%r13, %r13
	je	.L281
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L281
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.cfi_endproc
.LFE8377:
	.size	_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEENUlPvE_4_FUNES6_, .-_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEENUlPvE_4_FUNES6_
	.section	.text._ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev:
.LFB5973:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$10, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8029716252517299009, %rcx
	movq	%rdx, (%rdi)
	movl	$29547, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movb	$0, 26(%rdi)
	ret
	.cfi_endproc
.LFE5973:
	.size	_ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev, .-_ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev:
.LFB5975:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1718503781, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8386099861059890505, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$111, 28(%rdi)
	movq	$13, 8(%rdi)
	movb	$0, 29(%rdi)
	ret
	.cfi_endproc
.LFE5975:
	.size	_ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev, .-_ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node11Environment14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node11Environment14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11Environment14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node11Environment14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node11Environment14MemoryInfoNameB5cxx11Ev:
.LFB6014:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$116, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$7885362534758641221, %rcx
	movq	%rdx, (%rdi)
	movl	$28261, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE6014:
	.size	_ZNK4node11Environment14MemoryInfoNameB5cxx11Ev, .-_ZNK4node11Environment14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev:
.LFB5968:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$97, 26(%rdi)
	movq	%rdi, %rax
	movabsq	$4928473329006768969, %rcx
	movq	%rdx, (%rdi)
	movl	$29793, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 24(%rdi)
	movq	$11, 8(%rdi)
	movb	$0, 27(%rdi)
	ret
	.cfi_endproc
.LFE5968:
	.size	_ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev, .-_ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev:
.LFB5977:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$8, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8027224647447832916, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE5977:
	.size	_ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev, .-_ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev
	.section	.text._ZN4node11IsolateDataD0Ev,"axG",@progbits,_ZN4node11IsolateDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11IsolateDataD0Ev
	.type	_ZN4node11IsolateDataD0Ev, @function
_ZN4node11IsolateDataD0Ev:
.LFB13453:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11IsolateDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	2408(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L306
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L307
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L320
	.p2align 4,,10
	.p2align 3
.L306:
	movq	24(%r12), %rbx
	testq	%rbx, %rbx
	je	.L312
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L313
.L312:
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	8(%r12), %rdi
	leaq	56(%r12), %rax
	movq	$0, 32(%r12)
	movq	$0, 24(%r12)
	cmpq	%rax, %rdi
	je	.L314
	call	_ZdlPv@PLT
.L314:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$2416, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L307:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L306
.L320:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L310
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L311:
	cmpl	$1, %eax
	jne	.L306
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L310:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L311
	.cfi_endproc
.LFE13453:
	.size	_ZN4node11IsolateDataD0Ev, .-_ZN4node11IsolateDataD0Ev
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB13499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L323
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L324
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L350
	.p2align 4,,10
	.p2align 3
.L323:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L329
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L335
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L351
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L329
.L330:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L337
.L351:
	lock subl	$1, 8(%r13)
	jne	.L337
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L337
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L330
	.p2align 4,,10
	.p2align 3
.L329:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L339
	call	_ZdlPv@PLT
.L339:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L333
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L329
.L335:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L333
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L333
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L324:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L323
.L350:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L327
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L328:
	cmpl	$1, %eax
	jne	.L323
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L327:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L328
	.cfi_endproc
.LFE13499:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.text._ZN4node11IsolateDataD2Ev,"axG",@progbits,_ZN4node11IsolateDataD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11IsolateDataD2Ev
	.type	_ZN4node11IsolateDataD2Ev, @function
_ZN4node11IsolateDataD2Ev:
.LFB13451:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11IsolateDataE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	2408(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L354
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L355
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L368
	.p2align 4,,10
	.p2align 3
.L354:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L360
	.p2align 4,,10
	.p2align 3
.L361:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L361
.L360:
	movq	16(%rbx), %rax
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	addq	$56, %rbx
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-48(%rbx), %rdi
	movq	$0, -24(%rbx)
	movq	$0, -32(%rbx)
	cmpq	%rbx, %rdi
	je	.L352
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L354
.L368:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L358
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L359:
	cmpl	$1, %eax
	jne	.L354
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L352:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L359
	.cfi_endproc
.LFE13451:
	.size	_ZN4node11IsolateDataD2Ev, .-_ZN4node11IsolateDataD2Ev
	.weak	_ZN4node11IsolateDataD1Ev
	.set	_ZN4node11IsolateDataD1Ev,_ZN4node11IsolateDataD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node10BaseObject8DeleteMeEPv
	.type	_ZN4node10BaseObject8DeleteMeEPv, @function
_ZN4node10BaseObject8DeleteMeEPv:
.LFB8381:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L370
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L376
.L370:
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L376:
	movb	$1, 9(%rax)
	ret
	.cfi_endproc
.LFE8381:
	.size	_ZN4node10BaseObject8DeleteMeEPv, .-_ZN4node10BaseObject8DeleteMeEPv
	.section	.rodata._ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv.str1.1,"aMS",@progbits,1
.LC4:
	.string	"node,node.async_hooks"
	.section	.text._ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv,"axG",@progbits,_ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv
	.type	_ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv, @function
_ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv:
.LFB5989:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testb	$2, 1932(%rax)
	jne	.L403
.L377:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L403:
	.cfi_restore_state
	movzbl	1930(%rax), %edx
	testb	%dl, %dl
	je	.L377
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L377
	movq	%rdi, %rbx
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L379
	movq	(%rax), %rax
	leaq	.LC4(%rip), %rsi
	leaq	-160(%rbp), %r13
	call	*16(%rax)
	movq	%r13, %rdi
	movzbl	(%rax), %r14d
	movq	8(%rbx), %rax
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movq	3032(%rax), %r9
	testq	%r9, %r9
	je	.L402
	movq	352(%rax), %rsi
	leaq	-128(%rbp), %r15
	movq	%r9, -176(%rbp)
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-168(%rbp), %rax
	movq	%r15, %rdi
	movl	$1, %esi
	movl	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	testb	%r14b, %r14b
	leaq	112(%r12), %rax
	movq	-176(%rbp), %r9
	je	.L382
.L383:
	movq	%rax, -64(%rbp)
	movq	8(%rbx), %rax
	movq	%r9, %rdi
	leaq	88(%r12), %rdx
	leaq	-64(%rbp), %r8
	movl	$1, %ecx
	movq	3280(%rax), %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
.L402:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L379:
	movq	8(%rbx), %rax
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %r14
	movq	3032(%r14), %r9
	testq	%r9, %r9
	je	.L402
	movq	352(%r14), %rsi
	leaq	-128(%rbp), %r15
	movq	%r9, -168(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r14, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	movq	-168(%rbp), %r9
.L382:
	leaq	120(%r12), %rax
	jmp	.L383
.L404:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5989:
	.size	_ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv, .-_ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv
	.section	.text._ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv,"axG",@progbits,_ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv
	.type	_ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv, @function
_ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv:
.LFB5990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testb	$2, 1932(%rax)
	jne	.L431
.L405:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L432
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	.cfi_restore_state
	movzbl	1930(%rax), %edx
	testb	%dl, %dl
	je	.L405
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L405
	movq	%rdi, %rbx
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L407
	movq	(%rax), %rax
	leaq	.LC4(%rip), %rsi
	leaq	-160(%rbp), %r13
	call	*16(%rax)
	movq	%r13, %rdi
	movzbl	(%rax), %r14d
	movq	8(%rbx), %rax
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movq	3032(%rax), %r9
	testq	%r9, %r9
	je	.L430
	movq	352(%rax), %rsi
	leaq	-128(%rbp), %r15
	movq	%r9, -176(%rbp)
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-168(%rbp), %rax
	movq	%r15, %rdi
	movl	$1, %esi
	movl	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	testb	%r14b, %r14b
	leaq	112(%r12), %rax
	movq	-176(%rbp), %r9
	je	.L410
.L411:
	movq	%rax, -64(%rbp)
	movq	8(%rbx), %rax
	movq	%r9, %rdi
	leaq	88(%r12), %rdx
	leaq	-64(%rbp), %r8
	movl	$1, %ecx
	movq	3280(%rax), %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
.L430:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L407:
	movq	8(%rbx), %rax
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %r14
	movq	3032(%r14), %r9
	testq	%r9, %r9
	je	.L430
	movq	352(%r14), %rsi
	leaq	-128(%rbp), %r15
	movq	%r9, -168(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r14, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	movq	-168(%rbp), %r9
.L410:
	leaq	120(%r12), %rax
	jmp	.L411
.L432:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5990:
	.size	_ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv, .-_ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv
	.section	.rodata.str1.1
.LC5:
	.string	"node,node.environment"
.LC6:
	.string	"RunTimers"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment9RunTimersEP10uv_timer_s
	.type	_ZN4node11Environment9RunTimersEP10uv_timer_s, @function
_ZN4node11Environment9RunTimersEP10uv_timer_s:
.LFB8329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-368(%rdi), %rbx
	subq	$264, %rsp
	movq	%rdi, -296(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip), %r12
	testq	%r12, %r12
	je	.L525
.L435:
	testb	$5, (%r12)
	jne	.L526
.L437:
	leaq	1930(%rbx), %rax
	movq	%rax, -248(%rbp)
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	jne	.L527
.L474:
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L528
	.p2align 4,,10
	.p2align 3
.L466:
	testb	$5, (%r12)
	jne	.L529
.L433:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L530
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L527:
	.cfi_restore_state
	leaq	2664(%rbx), %rax
	movq	%rax, -288(%rbp)
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L474
	leaq	-224(%rbp), %rax
	movq	352(%rbx), %rsi
	leaq	-144(%rbp), %r12
	movq	%rax, %rdi
	movq	%rax, -272(%rbp)
	leaq	-192(%rbp), %r15
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -264(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	pxor	%xmm0, %xmm0
	movq	%rbx, %rsi
	xorl	%r8d, %r8d
	movq	2968(%rbx), %r13
	movq	%r12, %rcx
	movq	%r15, %rdi
	movaps	%xmm0, -144(%rbp)
	movq	%r13, %rdx
	call	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi@PLT
	movq	360(%rbx), %rax
	movq	3016(%rbx), %r14
	movq	2360(%rax), %rdi
	call	uv_update_time@PLT
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_now@PLT
	movq	%rax, %rsi
	movq	1376(%rbx), %rax
	cmpq	%rax, %rsi
	jb	.L531
	subq	%rax, %rsi
	movl	$4294967295, %eax
	movq	352(%rbx), %rdi
	cmpq	%rax, %rsi
	ja	.L445
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
.L446:
	movq	%rax, -232(%rbp)
	leaq	-232(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	%r15, -280(%rbp)
.L451:
	movq	352(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%rbx, -96(%rbp)
	movl	$0, -88(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	movq	%r14, %rdi
	movl	$1, %ecx
	movq	%r13, %rdx
	movq	3280(%rbx), %rsi
	movq	-256(%rbp), %r8
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	testq	%r15, %r15
	jne	.L449
	movq	-248(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L532
.L523:
	movq	-280(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	-264(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-272(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	jne	.L466
.L528:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L467
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L467:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	testb	$5, (%r12)
	je	.L433
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-288(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L451
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L449:
	movq	%r15, %rax
	movq	3280(%rbx), %rsi
	movq	-280(%rbp), %r15
	movq	%rax, %rdi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %r13
	testb	%al, %al
	je	.L533
.L475:
	testq	%r13, %r13
	je	.L452
	movq	360(%rbx), %rax
	movq	1376(%rbx), %r12
	movq	2360(%rax), %rdi
	call	uv_now@PLT
	cmpb	$0, 2648(%rbx)
	jne	.L453
	movq	%r13, %rcx
	leaq	368(%rbx), %rdi
	leaq	_ZN4node11Environment9RunTimersEP10uv_timer_s(%rip), %rsi
	sarq	$63, %rcx
	movq	%rcx, %rdx
	xorq	%r13, %rdx
	subq	%rcx, %rdx
	addq	%r12, %rdx
	subq	%rax, %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	cmovle	%rax, %rdx
	xorl	%ecx, %ecx
	call	uv_timer_start@PLT
.L453:
	testq	%r13, %r13
	jle	.L452
	movq	-296(%rbp), %rdi
	call	uv_ref@PLT
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L452:
	movq	-296(%rbp), %rdi
	call	uv_unref@PLT
.L454:
	movq	%r15, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	-264(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-272(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L534
.L456:
	testb	$5, (%r12)
	je	.L433
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L460
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC6(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L460:
	leaq	-64(%rbp), %rbx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L464:
	movq	-8(%rbx), %r12
	subq	$8, %rbx
	testq	%r12, %r12
	je	.L461
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L462
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L463
	call	_ZdlPv@PLT
.L463:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L461:
	cmpq	%r13, %rbx
	jne	.L464
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L526:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L438
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC6(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$98, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L438:
	leaq	-64(%rbp), %r14
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r12
.L442:
	movq	-8(%r14), %r15
	subq	$8, %r14
	testq	%r15, %r15
	je	.L439
	movq	(%r15), %rax
	movq	8(%rax), %rax
	cmpq	%r12, %rax
	jne	.L440
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L441
	call	_ZdlPv@PLT
.L441:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L439:
	cmpq	%r13, %r14
	jne	.L442
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L525:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L436
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L436:
	movq	%r12, _ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip)
	mfence
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L445:
	testq	%rsi, %rsi
	js	.L447
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L448:
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L529:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L469
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC6(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L469:
	leaq	-64(%rbp), %rbx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L473:
	movq	-8(%rbx), %r12
	subq	$8, %rbx
	testq	%r12, %r12
	je	.L470
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L471
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L472
	call	_ZdlPv@PLT
.L472:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L470:
	cmpq	%r13, %rbx
	jne	.L473
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L534:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L457
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L457:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L440:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L471:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L531:
	leaq	_ZZN4node11Environment6GetNowEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L533:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L475
.L462:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L461
.L530:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8329:
	.size	_ZN4node11Environment9RunTimersEP10uv_timer_s, .-_ZN4node11Environment9RunTimersEP10uv_timer_s
	.section	.text._ZN4node13MemoryTracker7AddNodeEPKcmS2_,"axG",@progbits,_ZN4node13MemoryTracker7AddNodeEPKcmS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13MemoryTracker7AddNodeEPKcmS2_
	.type	_ZN4node13MemoryTracker7AddNodeEPKcmS2_, @function
_ZN4node13MemoryTracker7AddNodeEPKcmS2_:
.LFB6095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movl	$72, %edi
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	32(%r12), %r9
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	movq	%r9, -72(%rbp)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	strlen@PLT
	movq	-72(%rbp), %r9
	movq	%r13, %rcx
	xorl	%edx, %edx
	movq	%rax, %r8
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r15, 64(%r12)
	leaq	-64(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	%r12, -64(%rbp)
	call	*8(%rax)
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L536
	movq	(%rdi), %rax
	call	*8(%rax)
.L536:
	movq	64(%rbx), %rax
	cmpq	%rax, 32(%rbx)
	je	.L535
	cmpq	72(%rbx), %rax
	je	.L547
.L538:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L535
	movq	8(%rbx), %rdi
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L535:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L548
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L538
.L548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6095:
	.size	_ZN4node13MemoryTracker7AddNodeEPKcmS2_, .-_ZN4node13MemoryTracker7AddNodeEPKcmS2_
	.section	.text._ZN4node10V8Platform7DisposeEv,"axG",@progbits,_ZN4node10V8Platform7DisposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10V8Platform7DisposeEv
	.type	_ZN4node10V8Platform7DisposeEv, @function
_ZN4node10V8Platform7DisposeEv:
.LFB7812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L550
	movl	24(%rbx), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L550:
	movq	$0, 16(%rbx)
	movq	32(%rbx), %rdi
	call	_ZN4node12NodePlatform8ShutdownEv@PLT
	movq	32(%rbx), %r13
	testq	%r13, %r13
	je	.L551
	movq	0(%r13), %rax
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L552
	movq	120(%r13), %r12
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%r12, %r12
	je	.L554
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L555
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L556:
	cmpl	$1, %eax
	jne	.L554
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L558
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L559:
	cmpl	$1, %eax
	je	.L592
	.p2align 4,,10
	.p2align 3
.L554:
	movq	64(%r13), %r12
	testq	%r12, %r12
	je	.L560
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L561
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L568:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L560
.L561:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r14
	testq	%r14, %r14
	je	.L568
	lock subl	$1, 8(%r14)
	jne	.L568
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r14)
	jne	.L568
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L561
	.p2align 4,,10
	.p2align 3
.L560:
	movq	56(%r13), %rax
	movq	48(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r13), %rdi
	leaq	96(%r13), %rax
	movq	$0, 72(%r13)
	movq	$0, 64(%r13)
	cmpq	%rax, %rdi
	je	.L570
	call	_ZdlPv@PLT
.L570:
	leaq	8(%r13), %rdi
	call	uv_mutex_destroy@PLT
	movl	$128, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L551:
	movq	8(%rbx), %r12
	movq	$0, 32(%rbx)
	movq	$0, 8(%rbx)
	testq	%r12, %r12
	je	.L571
	movq	%r12, %rdi
	call	_ZN4node7tracing5AgentD1Ev@PLT
	movl	$1312, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L571:
	movq	(%rbx), %rdi
	movq	$0, (%rbx)
	testq	%rdi, %rdi
	je	.L549
	movq	(%rdi), %rax
	leaq	_ZN4node22NodeTraceStateObserverD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L573
	addq	$8, %rsp
	movl	$16, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	cmpl	$1, %eax
	jne	.L564
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L564:
	movq	%r15, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L560
.L566:
	movq	%r12, %r15
	movq	(%r12), %r12
	movq	24(%r15), %r14
	testq	%r14, %r14
	je	.L564
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L564
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L549:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L555:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L573:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L558:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L559
	.cfi_endproc
.LFE7812:
	.size	_ZN4node10V8Platform7DisposeEv, .-_ZN4node10V8Platform7DisposeEv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"Failed to deserialize alpn_buffer_private_symbol\n"
	.align 8
.LC8:
	.string	"Failed to deserialize arraybuffer_untransferable_private_symbol\n"
	.align 8
.LC9:
	.string	"Failed to deserialize arrow_message_private_symbol\n"
	.align 8
.LC10:
	.string	"Failed to deserialize contextify_context_private_symbol\n"
	.align 8
.LC11:
	.string	"Failed to deserialize contextify_global_private_symbol\n"
	.align 8
.LC12:
	.string	"Failed to deserialize decorated_private_symbol\n"
	.align 8
.LC13:
	.string	"Failed to deserialize napi_wrapper\n"
	.align 8
.LC14:
	.string	"Failed to deserialize sab_lifetimepartner_symbol\n"
	.align 8
.LC15:
	.string	"Failed to deserialize handle_onclose_symbol\n"
	.align 8
.LC16:
	.string	"Failed to deserialize no_message_symbol\n"
	.align 8
.LC17:
	.string	"Failed to deserialize oninit_symbol\n"
	.align 8
.LC18:
	.string	"Failed to deserialize owner_symbol\n"
	.align 8
.LC19:
	.string	"Failed to deserialize onpskexchange_symbol\n"
	.align 8
.LC20:
	.string	"Failed to deserialize resource_symbol\n"
	.align 8
.LC21:
	.string	"Failed to deserialize trigger_async_id_symbol\n"
	.align 8
.LC22:
	.string	"Failed to deserialize address_string\n"
	.align 8
.LC23:
	.string	"Failed to deserialize aliases_string\n"
	.align 8
.LC24:
	.string	"Failed to deserialize args_string\n"
	.align 8
.LC25:
	.string	"Failed to deserialize asn1curve_string\n"
	.align 8
.LC26:
	.string	"Failed to deserialize async_ids_stack_string\n"
	.align 8
.LC27:
	.string	"Failed to deserialize bits_string\n"
	.align 8
.LC28:
	.string	"Failed to deserialize buffer_string\n"
	.align 8
.LC29:
	.string	"Failed to deserialize bytes_parsed_string\n"
	.align 8
.LC30:
	.string	"Failed to deserialize bytes_read_string\n"
	.align 8
.LC31:
	.string	"Failed to deserialize bytes_written_string\n"
	.align 8
.LC32:
	.string	"Failed to deserialize cached_data_produced_string\n"
	.align 8
.LC33:
	.string	"Failed to deserialize cached_data_rejected_string\n"
	.align 8
.LC34:
	.string	"Failed to deserialize cached_data_string\n"
	.align 8
.LC35:
	.string	"Failed to deserialize cache_key_string\n"
	.align 8
.LC36:
	.string	"Failed to deserialize change_string\n"
	.align 8
.LC37:
	.string	"Failed to deserialize channel_string\n"
	.align 8
.LC38:
	.string	"Failed to deserialize chunks_sent_since_last_write_string\n"
	.align 8
.LC39:
	.string	"Failed to deserialize clone_unsupported_type_str\n"
	.align 8
.LC40:
	.string	"Failed to deserialize code_string\n"
	.align 8
.LC41:
	.string	"Failed to deserialize commonjs_string\n"
	.align 8
.LC42:
	.string	"Failed to deserialize config_string\n"
	.align 8
.LC43:
	.string	"Failed to deserialize constants_string\n"
	.align 8
.LC44:
	.string	"Failed to deserialize crypto_dh_string\n"
	.align 8
.LC45:
	.string	"Failed to deserialize crypto_dsa_string\n"
	.align 8
.LC46:
	.string	"Failed to deserialize crypto_ec_string\n"
	.align 8
.LC47:
	.string	"Failed to deserialize crypto_ed25519_string\n"
	.align 8
.LC48:
	.string	"Failed to deserialize crypto_ed448_string\n"
	.align 8
.LC49:
	.string	"Failed to deserialize crypto_x25519_string\n"
	.align 8
.LC50:
	.string	"Failed to deserialize crypto_x448_string\n"
	.align 8
.LC51:
	.string	"Failed to deserialize crypto_rsa_string\n"
	.align 8
.LC52:
	.string	"Failed to deserialize crypto_rsa_pss_string\n"
	.align 8
.LC53:
	.string	"Failed to deserialize cwd_string\n"
	.align 8
.LC54:
	.string	"Failed to deserialize data_string\n"
	.align 8
.LC55:
	.string	"Failed to deserialize dest_string\n"
	.align 8
.LC56:
	.string	"Failed to deserialize destroyed_string\n"
	.align 8
.LC57:
	.string	"Failed to deserialize detached_string\n"
	.align 8
.LC58:
	.string	"Failed to deserialize dh_string\n"
	.align 8
.LC59:
	.string	"Failed to deserialize dns_a_string\n"
	.align 8
.LC60:
	.string	"Failed to deserialize dns_aaaa_string\n"
	.align 8
.LC61:
	.string	"Failed to deserialize dns_cname_string\n"
	.align 8
.LC62:
	.string	"Failed to deserialize dns_mx_string\n"
	.align 8
.LC63:
	.string	"Failed to deserialize dns_naptr_string\n"
	.align 8
.LC64:
	.string	"Failed to deserialize dns_ns_string\n"
	.align 8
.LC65:
	.string	"Failed to deserialize dns_ptr_string\n"
	.align 8
.LC66:
	.string	"Failed to deserialize dns_soa_string\n"
	.align 8
.LC67:
	.string	"Failed to deserialize dns_srv_string\n"
	.align 8
.LC68:
	.string	"Failed to deserialize dns_txt_string\n"
	.align 8
.LC69:
	.string	"Failed to deserialize done_string\n"
	.align 8
.LC70:
	.string	"Failed to deserialize duration_string\n"
	.align 8
.LC71:
	.string	"Failed to deserialize ecdh_string\n"
	.align 8
.LC72:
	.string	"Failed to deserialize emit_warning_string\n"
	.align 8
.LC73:
	.string	"Failed to deserialize empty_object_string\n"
	.align 8
.LC74:
	.string	"Failed to deserialize encoding_string\n"
	.align 8
.LC75:
	.string	"Failed to deserialize entries_string\n"
	.align 8
.LC76:
	.string	"Failed to deserialize entry_type_string\n"
	.align 8
.LC77:
	.string	"Failed to deserialize env_pairs_string\n"
	.align 8
.LC78:
	.string	"Failed to deserialize env_var_settings_string\n"
	.align 8
.LC79:
	.string	"Failed to deserialize errno_string\n"
	.align 8
.LC80:
	.string	"Failed to deserialize error_string\n"
	.align 8
.LC81:
	.string	"Failed to deserialize exchange_string\n"
	.align 8
.LC82:
	.string	"Failed to deserialize exit_code_string\n"
	.align 8
.LC83:
	.string	"Failed to deserialize expire_string\n"
	.align 8
.LC84:
	.string	"Failed to deserialize exponent_string\n"
	.align 8
.LC85:
	.string	"Failed to deserialize exports_string\n"
	.align 8
.LC86:
	.string	"Failed to deserialize ext_key_usage_string\n"
	.align 8
.LC87:
	.string	"Failed to deserialize external_stream_string\n"
	.align 8
.LC88:
	.string	"Failed to deserialize family_string\n"
	.align 8
.LC89:
	.string	"Failed to deserialize fatal_exception_string\n"
	.align 8
.LC90:
	.string	"Failed to deserialize fd_string\n"
	.align 8
.LC91:
	.string	"Failed to deserialize fields_string\n"
	.align 8
.LC92:
	.string	"Failed to deserialize file_string\n"
	.align 8
.LC93:
	.string	"Failed to deserialize fingerprint256_string\n"
	.align 8
.LC94:
	.string	"Failed to deserialize fingerprint_string\n"
	.align 8
.LC95:
	.string	"Failed to deserialize flags_string\n"
	.align 8
.LC96:
	.string	"Failed to deserialize fragment_string\n"
	.align 8
.LC97:
	.string	"Failed to deserialize function_string\n"
	.align 8
.LC98:
	.string	"Failed to deserialize get_data_clone_error_string\n"
	.align 8
.LC99:
	.string	"Failed to deserialize get_shared_array_buffer_id_string\n"
	.align 8
.LC100:
	.string	"Failed to deserialize gid_string\n"
	.align 8
.LC101:
	.string	"Failed to deserialize h2_string\n"
	.align 8
.LC102:
	.string	"Failed to deserialize handle_string\n"
	.align 8
.LC103:
	.string	"Failed to deserialize help_text_string\n"
	.align 8
.LC104:
	.string	"Failed to deserialize homedir_string\n"
	.align 8
.LC105:
	.string	"Failed to deserialize host_string\n"
	.align 8
.LC106:
	.string	"Failed to deserialize hostmaster_string\n"
	.align 8
.LC107:
	.string	"Failed to deserialize http_1_1_string\n"
	.align 8
.LC108:
	.string	"Failed to deserialize identity_string\n"
	.align 8
.LC109:
	.string	"Failed to deserialize ignore_string\n"
	.align 8
.LC110:
	.string	"Failed to deserialize infoaccess_string\n"
	.align 8
.LC111:
	.string	"Failed to deserialize inherit_string\n"
	.align 8
.LC112:
	.string	"Failed to deserialize input_string\n"
	.align 8
.LC113:
	.string	"Failed to deserialize internal_binding_string\n"
	.align 8
.LC114:
	.string	"Failed to deserialize internal_string\n"
	.align 8
.LC115:
	.string	"Failed to deserialize ipv4_string\n"
	.align 8
.LC116:
	.string	"Failed to deserialize ipv6_string\n"
	.align 8
.LC117:
	.string	"Failed to deserialize isclosing_string\n"
	.align 8
.LC118:
	.string	"Failed to deserialize issuer_string\n"
	.align 8
.LC119:
	.string	"Failed to deserialize issuercert_string\n"
	.align 8
.LC120:
	.string	"Failed to deserialize kill_signal_string\n"
	.align 8
.LC121:
	.string	"Failed to deserialize kind_string\n"
	.align 8
.LC122:
	.string	"Failed to deserialize library_string\n"
	.align 8
.LC123:
	.string	"Failed to deserialize mac_string\n"
	.align 8
.LC124:
	.string	"Failed to deserialize max_buffer_string\n"
	.align 8
.LC125:
	.string	"Failed to deserialize message_port_constructor_string\n"
	.align 8
.LC126:
	.string	"Failed to deserialize message_port_string\n"
	.align 8
.LC127:
	.string	"Failed to deserialize message_string\n"
	.align 8
.LC128:
	.string	"Failed to deserialize minttl_string\n"
	.align 8
.LC129:
	.string	"Failed to deserialize module_string\n"
	.align 8
.LC130:
	.string	"Failed to deserialize modulus_string\n"
	.align 8
.LC131:
	.string	"Failed to deserialize name_string\n"
	.align 8
.LC132:
	.string	"Failed to deserialize netmask_string\n"
	.align 8
.LC133:
	.string	"Failed to deserialize next_string\n"
	.align 8
.LC134:
	.string	"Failed to deserialize nistcurve_string\n"
	.align 8
.LC135:
	.string	"Failed to deserialize node_string\n"
	.align 8
.LC136:
	.string	"Failed to deserialize nsname_string\n"
	.align 8
.LC137:
	.string	"Failed to deserialize ocsp_request_string\n"
	.align 8
.LC138:
	.string	"Failed to deserialize oncertcb_string\n"
	.align 8
.LC139:
	.string	"Failed to deserialize onchange_string\n"
	.align 8
.LC140:
	.string	"Failed to deserialize onclienthello_string\n"
	.align 8
.LC141:
	.string	"Failed to deserialize oncomplete_string\n"
	.align 8
.LC142:
	.string	"Failed to deserialize onconnection_string\n"
	.align 8
.LC143:
	.string	"Failed to deserialize ondone_string\n"
	.align 8
.LC144:
	.string	"Failed to deserialize onerror_string\n"
	.align 8
.LC145:
	.string	"Failed to deserialize onexit_string\n"
	.align 8
.LC146:
	.string	"Failed to deserialize onhandshakedone_string\n"
	.align 8
.LC147:
	.string	"Failed to deserialize onhandshakestart_string\n"
	.align 8
.LC148:
	.string	"Failed to deserialize onkeylog_string\n"
	.align 8
.LC149:
	.string	"Failed to deserialize onmessage_string\n"
	.align 8
.LC150:
	.string	"Failed to deserialize onnewsession_string\n"
	.align 8
.LC151:
	.string	"Failed to deserialize onocspresponse_string\n"
	.align 8
.LC152:
	.string	"Failed to deserialize onreadstart_string\n"
	.align 8
.LC153:
	.string	"Failed to deserialize onreadstop_string\n"
	.align 8
.LC154:
	.string	"Failed to deserialize onshutdown_string\n"
	.align 8
.LC155:
	.string	"Failed to deserialize onsignal_string\n"
	.align 8
.LC156:
	.string	"Failed to deserialize onunpipe_string\n"
	.align 8
.LC157:
	.string	"Failed to deserialize onwrite_string\n"
	.align 8
.LC158:
	.string	"Failed to deserialize openssl_error_stack\n"
	.align 8
.LC159:
	.string	"Failed to deserialize options_string\n"
	.align 8
.LC160:
	.string	"Failed to deserialize order_string\n"
	.align 8
.LC161:
	.string	"Failed to deserialize output_string\n"
	.align 8
.LC162:
	.string	"Failed to deserialize parse_error_string\n"
	.align 8
.LC163:
	.string	"Failed to deserialize password_string\n"
	.align 8
.LC164:
	.string	"Failed to deserialize path_string\n"
	.align 8
.LC165:
	.string	"Failed to deserialize pending_handle_string\n"
	.align 8
.LC166:
	.string	"Failed to deserialize pid_string\n"
	.align 8
.LC167:
	.string	"Failed to deserialize pipe_source_string\n"
	.align 8
.LC168:
	.string	"Failed to deserialize pipe_string\n"
	.align 8
.LC169:
	.string	"Failed to deserialize pipe_target_string\n"
	.align 8
.LC170:
	.string	"Failed to deserialize port1_string\n"
	.align 8
.LC171:
	.string	"Failed to deserialize port2_string\n"
	.align 8
.LC172:
	.string	"Failed to deserialize port_string\n"
	.align 8
.LC173:
	.string	"Failed to deserialize preference_string\n"
	.align 8
.LC174:
	.string	"Failed to deserialize primordials_string\n"
	.align 8
.LC175:
	.string	"Failed to deserialize priority_string\n"
	.align 8
.LC176:
	.string	"Failed to deserialize process_string\n"
	.align 8
.LC177:
	.string	"Failed to deserialize promise_string\n"
	.align 8
.LC178:
	.string	"Failed to deserialize psk_string\n"
	.align 8
.LC179:
	.string	"Failed to deserialize pubkey_string\n"
	.align 8
.LC180:
	.string	"Failed to deserialize query_string\n"
	.align 8
.LC181:
	.string	"Failed to deserialize raw_string\n"
	.align 8
.LC182:
	.string	"Failed to deserialize read_host_object_string\n"
	.align 8
.LC183:
	.string	"Failed to deserialize readable_string\n"
	.align 8
.LC184:
	.string	"Failed to deserialize reason_string\n"
	.align 8
.LC185:
	.string	"Failed to deserialize refresh_string\n"
	.align 8
.LC186:
	.string	"Failed to deserialize regexp_string\n"
	.align 8
.LC187:
	.string	"Failed to deserialize rename_string\n"
	.align 8
.LC188:
	.string	"Failed to deserialize replacement_string\n"
	.align 8
.LC189:
	.string	"Failed to deserialize require_string\n"
	.align 8
.LC190:
	.string	"Failed to deserialize retry_string\n"
	.align 8
.LC191:
	.string	"Failed to deserialize scheme_string\n"
	.align 8
.LC192:
	.string	"Failed to deserialize scopeid_string\n"
	.align 8
.LC193:
	.string	"Failed to deserialize serial_number_string\n"
	.align 8
.LC194:
	.string	"Failed to deserialize serial_string\n"
	.align 8
.LC195:
	.string	"Failed to deserialize servername_string\n"
	.align 8
.LC196:
	.string	"Failed to deserialize service_string\n"
	.align 8
.LC197:
	.string	"Failed to deserialize session_id_string\n"
	.align 8
.LC198:
	.string	"Failed to deserialize shell_string\n"
	.align 8
.LC199:
	.string	"Failed to deserialize signal_string\n"
	.align 8
.LC200:
	.string	"Failed to deserialize sink_string\n"
	.align 8
.LC201:
	.string	"Failed to deserialize size_string\n"
	.align 8
.LC202:
	.string	"Failed to deserialize sni_context_err_string\n"
	.align 8
.LC203:
	.string	"Failed to deserialize sni_context_string\n"
	.align 8
.LC204:
	.string	"Failed to deserialize source_string\n"
	.align 8
.LC205:
	.string	"Failed to deserialize stack_string\n"
	.align 8
.LC206:
	.string	"Failed to deserialize standard_name_string\n"
	.align 8
.LC207:
	.string	"Failed to deserialize start_time_string\n"
	.align 8
.LC208:
	.string	"Failed to deserialize status_string\n"
	.align 8
.LC209:
	.string	"Failed to deserialize stdio_string\n"
	.align 8
.LC210:
	.string	"Failed to deserialize subject_string\n"
	.align 8
.LC211:
	.string	"Failed to deserialize subjectaltname_string\n"
	.align 8
.LC212:
	.string	"Failed to deserialize syscall_string\n"
	.align 8
.LC213:
	.string	"Failed to deserialize target_string\n"
	.align 8
.LC214:
	.string	"Failed to deserialize thread_id_string\n"
	.align 8
.LC215:
	.string	"Failed to deserialize ticketkeycallback_string\n"
	.align 8
.LC216:
	.string	"Failed to deserialize timeout_string\n"
	.align 8
.LC217:
	.string	"Failed to deserialize tls_ticket_string\n"
	.align 8
.LC218:
	.string	"Failed to deserialize transfer_string\n"
	.align 8
.LC219:
	.string	"Failed to deserialize ttl_string\n"
	.align 8
.LC220:
	.string	"Failed to deserialize type_string\n"
	.align 8
.LC221:
	.string	"Failed to deserialize uid_string\n"
	.align 8
.LC222:
	.string	"Failed to deserialize unknown_string\n"
	.align 8
.LC223:
	.string	"Failed to deserialize url_special_ftp_string\n"
	.align 8
.LC224:
	.string	"Failed to deserialize url_special_file_string\n"
	.align 8
.LC225:
	.string	"Failed to deserialize url_special_gopher_string\n"
	.align 8
.LC226:
	.string	"Failed to deserialize url_special_http_string\n"
	.align 8
.LC227:
	.string	"Failed to deserialize url_special_https_string\n"
	.align 8
.LC228:
	.string	"Failed to deserialize url_special_ws_string\n"
	.align 8
.LC229:
	.string	"Failed to deserialize url_special_wss_string\n"
	.align 8
.LC230:
	.string	"Failed to deserialize url_string\n"
	.align 8
.LC231:
	.string	"Failed to deserialize username_string\n"
	.align 8
.LC232:
	.string	"Failed to deserialize valid_from_string\n"
	.align 8
.LC233:
	.string	"Failed to deserialize valid_to_string\n"
	.align 8
.LC234:
	.string	"Failed to deserialize value_string\n"
	.align 8
.LC235:
	.string	"Failed to deserialize verify_error_string\n"
	.align 8
.LC236:
	.string	"Failed to deserialize version_string\n"
	.align 8
.LC237:
	.string	"Failed to deserialize weight_string\n"
	.align 8
.LC238:
	.string	"Failed to deserialize windows_hide_string\n"
	.align 8
.LC239:
	.string	"Failed to deserialize windows_verbatim_arguments_string\n"
	.align 8
.LC240:
	.string	"Failed to deserialize wrap_string\n"
	.align 8
.LC241:
	.string	"Failed to deserialize writable_string\n"
	.align 8
.LC242:
	.string	"Failed to deserialize write_host_object_string\n"
	.align 8
.LC243:
	.string	"Failed to deserialize write_queue_size_string\n"
	.align 8
.LC244:
	.string	"Failed to deserialize x_forwarded_string\n"
	.align 8
.LC245:
	.string	"Failed to deserialize zero_return_string\n"
	.align 8
.LC246:
	.string	"Failed to deserialize AsyncWrap provider %zu\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11IsolateData21DeserializePropertiesEPKSt6vectorImSaImEE
	.type	_ZN4node11IsolateData21DeserializePropertiesEPKSt6vectorImSaImEE, @function
_ZN4node11IsolateData21DeserializePropertiesEPKSt6vectorImSaImEE:
.LFB7955:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	2352(%rdi), %rsi
	movq	%r15, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r12), %rax
	movq	2352(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L847
.L594:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 64(%rbx)
	movq	(%r12), %rax
	movq	8(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L848
.L595:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 72(%rbx)
	movq	(%r12), %rax
	movq	16(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L849
.L596:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 80(%rbx)
	movq	(%r12), %rax
	movq	24(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L850
.L597:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 88(%rbx)
	movq	(%r12), %rax
	movq	32(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L851
.L598:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 96(%rbx)
	movq	(%r12), %rax
	movq	40(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L852
.L599:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 104(%rbx)
	movq	(%r12), %rax
	movq	48(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L853
.L600:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 112(%rbx)
	movq	(%r12), %rax
	movq	56(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L854
.L601:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 120(%rbx)
	movq	(%r12), %rax
	movq	64(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L855
.L602:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 128(%rbx)
	movq	(%r12), %rax
	movq	72(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L856
.L603:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 136(%rbx)
	movq	(%r12), %rax
	movq	80(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L857
.L604:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 144(%rbx)
	movq	(%r12), %rax
	movq	88(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L858
.L605:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 152(%rbx)
	movq	(%r12), %rax
	movq	96(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L859
.L606:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 160(%rbx)
	movq	(%r12), %rax
	movq	104(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L860
.L607:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 168(%rbx)
	movq	(%r12), %rax
	movq	112(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L861
.L608:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 176(%rbx)
	movq	(%r12), %rax
	movq	120(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L862
.L609:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 184(%rbx)
	movq	(%r12), %rax
	movq	128(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L863
.L610:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 192(%rbx)
	movq	(%r12), %rax
	movq	136(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L864
.L611:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 200(%rbx)
	movq	(%r12), %rax
	movq	144(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L865
.L612:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 208(%rbx)
	movq	(%r12), %rax
	movq	152(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L866
.L613:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 216(%rbx)
	movq	(%r12), %rax
	movq	160(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L867
.L614:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 224(%rbx)
	movq	(%r12), %rax
	movq	168(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L868
.L615:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 232(%rbx)
	movq	(%r12), %rax
	movq	176(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L869
.L616:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 240(%rbx)
	movq	(%r12), %rax
	movq	184(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L870
.L617:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 248(%rbx)
	movq	(%r12), %rax
	movq	192(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L871
.L618:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 256(%rbx)
	movq	(%r12), %rax
	movq	200(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L872
.L619:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 264(%rbx)
	movq	(%r12), %rax
	movq	208(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L873
.L620:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 272(%rbx)
	movq	(%r12), %rax
	movq	216(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L874
.L621:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 280(%rbx)
	movq	(%r12), %rax
	movq	224(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L875
.L622:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 288(%rbx)
	movq	(%r12), %rax
	movq	232(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L876
.L623:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 296(%rbx)
	movq	(%r12), %rax
	movq	240(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L877
.L624:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 304(%rbx)
	movq	(%r12), %rax
	movq	248(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L878
.L625:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 312(%rbx)
	movq	(%r12), %rax
	movq	256(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L879
.L626:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 320(%rbx)
	movq	(%r12), %rax
	movq	264(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L880
.L627:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 328(%rbx)
	movq	(%r12), %rax
	movq	272(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L881
.L628:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 336(%rbx)
	movq	(%r12), %rax
	movq	280(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L882
.L629:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 344(%rbx)
	movq	(%r12), %rax
	movq	288(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L883
.L630:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 352(%rbx)
	movq	(%r12), %rax
	movq	296(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L884
.L631:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 360(%rbx)
	movq	(%r12), %rax
	movq	304(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L885
.L632:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 368(%rbx)
	movq	(%r12), %rax
	movq	312(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L886
.L633:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 376(%rbx)
	movq	(%r12), %rax
	movq	320(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L887
.L634:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 384(%rbx)
	movq	(%r12), %rax
	movq	328(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L888
.L635:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 392(%rbx)
	movq	(%r12), %rax
	movq	336(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L889
.L636:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 400(%rbx)
	movq	(%r12), %rax
	movq	344(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L890
.L637:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 408(%rbx)
	movq	(%r12), %rax
	movq	352(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L891
.L638:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 416(%rbx)
	movq	(%r12), %rax
	movq	360(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L892
.L639:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 424(%rbx)
	movq	(%r12), %rax
	movq	368(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L893
.L640:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 432(%rbx)
	movq	(%r12), %rax
	movq	376(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L894
.L641:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 440(%rbx)
	movq	(%r12), %rax
	movq	384(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L895
.L642:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 448(%rbx)
	movq	(%r12), %rax
	movq	392(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L896
.L643:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 456(%rbx)
	movq	(%r12), %rax
	movq	400(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L897
.L644:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 464(%rbx)
	movq	(%r12), %rax
	movq	408(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L898
.L645:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 472(%rbx)
	movq	(%r12), %rax
	movq	416(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L899
.L646:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 480(%rbx)
	movq	(%r12), %rax
	movq	424(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L900
.L647:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 488(%rbx)
	movq	(%r12), %rax
	movq	432(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L901
.L648:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 496(%rbx)
	movq	(%r12), %rax
	movq	440(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L902
.L649:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 504(%rbx)
	movq	(%r12), %rax
	movq	448(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L903
.L650:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 512(%rbx)
	movq	(%r12), %rax
	movq	456(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L904
.L651:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 520(%rbx)
	movq	(%r12), %rax
	movq	464(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L905
.L652:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 528(%rbx)
	movq	(%r12), %rax
	movq	472(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L906
.L653:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 536(%rbx)
	movq	(%r12), %rax
	movq	480(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L907
.L654:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 544(%rbx)
	movq	(%r12), %rax
	movq	488(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L908
.L655:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 552(%rbx)
	movq	(%r12), %rax
	movq	496(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L909
.L656:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 560(%rbx)
	movq	(%r12), %rax
	movq	504(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L910
.L657:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 568(%rbx)
	movq	(%r12), %rax
	movq	512(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L911
.L658:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 576(%rbx)
	movq	(%r12), %rax
	movq	520(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L912
.L659:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 584(%rbx)
	movq	(%r12), %rax
	movq	528(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L913
.L660:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 592(%rbx)
	movq	(%r12), %rax
	movq	536(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L914
.L661:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 600(%rbx)
	movq	(%r12), %rax
	movq	544(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L915
.L662:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 608(%rbx)
	movq	(%r12), %rax
	movq	552(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L916
.L663:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 616(%rbx)
	movq	(%r12), %rax
	movq	560(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L917
.L664:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 624(%rbx)
	movq	(%r12), %rax
	movq	568(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L918
.L665:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 632(%rbx)
	movq	(%r12), %rax
	movq	576(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L919
.L666:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 640(%rbx)
	movq	(%r12), %rax
	movq	584(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L920
.L667:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 648(%rbx)
	movq	(%r12), %rax
	movq	592(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L921
.L668:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 656(%rbx)
	movq	(%r12), %rax
	movq	600(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L922
.L669:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 664(%rbx)
	movq	(%r12), %rax
	movq	608(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L923
.L670:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 672(%rbx)
	movq	(%r12), %rax
	movq	616(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L924
.L671:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 680(%rbx)
	movq	(%r12), %rax
	movq	624(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L925
.L672:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 688(%rbx)
	movq	(%r12), %rax
	movq	632(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L926
.L673:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 696(%rbx)
	movq	(%r12), %rax
	movq	640(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L927
.L674:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 704(%rbx)
	movq	(%r12), %rax
	movq	648(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L928
.L675:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 712(%rbx)
	movq	(%r12), %rax
	movq	656(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L929
.L676:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 720(%rbx)
	movq	(%r12), %rax
	movq	664(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L930
.L677:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 728(%rbx)
	movq	(%r12), %rax
	movq	672(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L931
.L678:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 736(%rbx)
	movq	(%r12), %rax
	movq	680(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L932
.L679:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 744(%rbx)
	movq	(%r12), %rax
	movq	688(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L933
.L680:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 752(%rbx)
	movq	(%r12), %rax
	movq	696(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L934
.L681:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 760(%rbx)
	movq	(%r12), %rax
	movq	704(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L935
.L682:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 768(%rbx)
	movq	(%r12), %rax
	movq	712(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L936
.L683:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 776(%rbx)
	movq	(%r12), %rax
	movq	720(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L937
.L684:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 784(%rbx)
	movq	(%r12), %rax
	movq	728(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L938
.L685:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 792(%rbx)
	movq	(%r12), %rax
	movq	736(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L939
.L686:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 800(%rbx)
	movq	(%r12), %rax
	movq	744(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L940
.L687:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 808(%rbx)
	movq	(%r12), %rax
	movq	752(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L941
.L688:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 816(%rbx)
	movq	(%r12), %rax
	movq	760(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L942
.L689:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 824(%rbx)
	movq	(%r12), %rax
	movq	768(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L943
.L690:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 832(%rbx)
	movq	(%r12), %rax
	movq	776(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L944
.L691:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 840(%rbx)
	movq	(%r12), %rax
	movq	784(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L945
.L692:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 848(%rbx)
	movq	(%r12), %rax
	movq	792(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L946
.L693:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 856(%rbx)
	movq	(%r12), %rax
	movq	800(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L947
.L694:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 864(%rbx)
	movq	(%r12), %rax
	movq	808(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L948
.L695:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 872(%rbx)
	movq	(%r12), %rax
	movq	816(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L949
.L696:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 880(%rbx)
	movq	(%r12), %rax
	movq	824(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L950
.L697:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 888(%rbx)
	movq	(%r12), %rax
	movq	832(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L951
.L698:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 896(%rbx)
	movq	(%r12), %rax
	movq	840(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L952
.L699:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 904(%rbx)
	movq	(%r12), %rax
	movq	848(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L953
.L700:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 912(%rbx)
	movq	(%r12), %rax
	movq	856(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L954
.L701:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 920(%rbx)
	movq	(%r12), %rax
	movq	864(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L955
.L702:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 928(%rbx)
	movq	(%r12), %rax
	movq	872(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L956
.L703:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 936(%rbx)
	movq	(%r12), %rax
	movq	880(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L957
.L704:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 944(%rbx)
	movq	(%r12), %rax
	movq	888(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L958
.L705:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 952(%rbx)
	movq	(%r12), %rax
	movq	896(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L959
.L706:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 960(%rbx)
	movq	(%r12), %rax
	movq	904(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L960
.L707:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 968(%rbx)
	movq	(%r12), %rax
	movq	912(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L961
.L708:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 976(%rbx)
	movq	(%r12), %rax
	movq	920(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L962
.L709:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 984(%rbx)
	movq	(%r12), %rax
	movq	928(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L963
.L710:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 992(%rbx)
	movq	(%r12), %rax
	movq	936(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L964
.L711:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1000(%rbx)
	movq	(%r12), %rax
	movq	944(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L965
.L712:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1008(%rbx)
	movq	(%r12), %rax
	movq	952(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L966
.L713:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1016(%rbx)
	movq	(%r12), %rax
	movq	960(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L967
.L714:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1024(%rbx)
	movq	(%r12), %rax
	movq	968(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L968
.L715:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1032(%rbx)
	movq	(%r12), %rax
	movq	976(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L969
.L716:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1040(%rbx)
	movq	(%r12), %rax
	movq	984(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L970
.L717:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1048(%rbx)
	movq	(%r12), %rax
	movq	992(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L971
.L718:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1056(%rbx)
	movq	(%r12), %rax
	movq	1000(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L972
.L719:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1064(%rbx)
	movq	(%r12), %rax
	movq	1008(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L973
.L720:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1072(%rbx)
	movq	(%r12), %rax
	movq	1016(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L974
.L721:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1080(%rbx)
	movq	(%r12), %rax
	movq	1024(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L975
.L722:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1088(%rbx)
	movq	(%r12), %rax
	movq	1032(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L976
.L723:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1096(%rbx)
	movq	(%r12), %rax
	movq	1040(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L977
.L724:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1104(%rbx)
	movq	(%r12), %rax
	movq	1048(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L978
.L725:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1112(%rbx)
	movq	(%r12), %rax
	movq	1056(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L979
.L726:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1120(%rbx)
	movq	(%r12), %rax
	movq	1064(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L980
.L727:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1128(%rbx)
	movq	(%r12), %rax
	movq	1072(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L981
.L728:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1136(%rbx)
	movq	(%r12), %rax
	movq	1080(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L982
.L729:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1144(%rbx)
	movq	(%r12), %rax
	movq	1088(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L983
.L730:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1152(%rbx)
	movq	(%r12), %rax
	movq	1096(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L984
.L731:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1160(%rbx)
	movq	(%r12), %rax
	movq	1104(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L985
.L732:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1168(%rbx)
	movq	(%r12), %rax
	movq	1112(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L986
.L733:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1176(%rbx)
	movq	(%r12), %rax
	movq	1120(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L987
.L734:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1184(%rbx)
	movq	(%r12), %rax
	movq	1128(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L988
.L735:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1192(%rbx)
	movq	(%r12), %rax
	movq	1136(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L989
.L736:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1200(%rbx)
	movq	(%r12), %rax
	movq	1144(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L990
.L737:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1208(%rbx)
	movq	(%r12), %rax
	movq	1152(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L991
.L738:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1216(%rbx)
	movq	(%r12), %rax
	movq	1160(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L992
.L739:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1224(%rbx)
	movq	(%r12), %rax
	movq	1168(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L993
.L740:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1232(%rbx)
	movq	(%r12), %rax
	movq	1176(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L994
.L741:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1240(%rbx)
	movq	(%r12), %rax
	movq	1184(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L995
.L742:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1248(%rbx)
	movq	(%r12), %rax
	movq	1192(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L996
.L743:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1256(%rbx)
	movq	(%r12), %rax
	movq	1200(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L997
.L744:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1264(%rbx)
	movq	(%r12), %rax
	movq	1208(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L998
.L745:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1272(%rbx)
	movq	(%r12), %rax
	movq	1216(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L999
.L746:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1280(%rbx)
	movq	(%r12), %rax
	movq	1224(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1000
.L747:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1288(%rbx)
	movq	(%r12), %rax
	movq	1232(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1001
.L748:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1296(%rbx)
	movq	(%r12), %rax
	movq	1240(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1002
.L749:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1304(%rbx)
	movq	(%r12), %rax
	movq	1248(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1003
.L750:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1312(%rbx)
	movq	(%r12), %rax
	movq	1256(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1004
.L751:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1320(%rbx)
	movq	(%r12), %rax
	movq	1264(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1005
.L752:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1328(%rbx)
	movq	(%r12), %rax
	movq	1272(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1006
.L753:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1336(%rbx)
	movq	(%r12), %rax
	movq	1280(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1007
.L754:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1344(%rbx)
	movq	(%r12), %rax
	movq	1288(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1008
.L755:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1352(%rbx)
	movq	(%r12), %rax
	movq	1296(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1009
.L756:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1360(%rbx)
	movq	(%r12), %rax
	movq	1304(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1010
.L757:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1368(%rbx)
	movq	(%r12), %rax
	movq	1312(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1011
.L758:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1376(%rbx)
	movq	(%r12), %rax
	movq	1320(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1012
.L759:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1384(%rbx)
	movq	(%r12), %rax
	movq	1328(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1013
.L760:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1392(%rbx)
	movq	(%r12), %rax
	movq	1336(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1014
.L761:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1400(%rbx)
	movq	(%r12), %rax
	movq	1344(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1015
.L762:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1408(%rbx)
	movq	(%r12), %rax
	movq	1352(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1016
.L763:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1416(%rbx)
	movq	(%r12), %rax
	movq	1360(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1017
.L764:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1424(%rbx)
	movq	(%r12), %rax
	movq	1368(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1018
.L765:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1432(%rbx)
	movq	(%r12), %rax
	movq	1376(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1019
.L766:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1440(%rbx)
	movq	(%r12), %rax
	movq	1384(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1020
.L767:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1448(%rbx)
	movq	(%r12), %rax
	movq	1392(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1021
.L768:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1456(%rbx)
	movq	(%r12), %rax
	movq	1400(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1022
.L769:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1464(%rbx)
	movq	(%r12), %rax
	movq	1408(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1023
.L770:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1472(%rbx)
	movq	(%r12), %rax
	movq	1416(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1024
.L771:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1480(%rbx)
	movq	(%r12), %rax
	movq	1424(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1025
.L772:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1488(%rbx)
	movq	(%r12), %rax
	movq	1432(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1026
.L773:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1496(%rbx)
	movq	(%r12), %rax
	movq	1440(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1027
.L774:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1504(%rbx)
	movq	(%r12), %rax
	movq	1448(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1028
.L775:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1512(%rbx)
	movq	(%r12), %rax
	movq	1456(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1029
.L776:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1520(%rbx)
	movq	(%r12), %rax
	movq	1464(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1030
.L777:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1528(%rbx)
	movq	(%r12), %rax
	movq	1472(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1031
.L778:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1536(%rbx)
	movq	(%r12), %rax
	movq	1480(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1032
.L779:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1544(%rbx)
	movq	(%r12), %rax
	movq	1488(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1033
.L780:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1552(%rbx)
	movq	(%r12), %rax
	movq	1496(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1034
.L781:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1560(%rbx)
	movq	(%r12), %rax
	movq	1504(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1035
.L782:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1568(%rbx)
	movq	(%r12), %rax
	movq	1512(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1036
.L783:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1576(%rbx)
	movq	(%r12), %rax
	movq	1520(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1037
.L784:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1584(%rbx)
	movq	(%r12), %rax
	movq	1528(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1038
.L785:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1592(%rbx)
	movq	(%r12), %rax
	movq	1536(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1039
.L786:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1600(%rbx)
	movq	(%r12), %rax
	movq	1544(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1040
.L787:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1608(%rbx)
	movq	(%r12), %rax
	movq	1552(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1041
.L788:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1616(%rbx)
	movq	(%r12), %rax
	movq	1560(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1042
.L789:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1624(%rbx)
	movq	(%r12), %rax
	movq	1568(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1043
.L790:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1632(%rbx)
	movq	(%r12), %rax
	movq	1576(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1044
.L791:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1640(%rbx)
	movq	(%r12), %rax
	movq	1584(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1045
.L792:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1648(%rbx)
	movq	(%r12), %rax
	movq	1592(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1046
.L793:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1656(%rbx)
	movq	(%r12), %rax
	movq	1600(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1047
.L794:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1664(%rbx)
	movq	(%r12), %rax
	movq	1608(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1048
.L795:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1672(%rbx)
	movq	(%r12), %rax
	movq	1616(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1049
.L796:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1680(%rbx)
	movq	(%r12), %rax
	movq	1624(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1050
.L797:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1688(%rbx)
	movq	(%r12), %rax
	movq	1632(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1051
.L798:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1696(%rbx)
	movq	(%r12), %rax
	movq	1640(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1052
.L799:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1704(%rbx)
	movq	(%r12), %rax
	movq	1648(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1053
.L800:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1712(%rbx)
	movq	(%r12), %rax
	movq	1656(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1054
.L801:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1720(%rbx)
	movq	(%r12), %rax
	movq	1664(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1055
.L802:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1728(%rbx)
	movq	(%r12), %rax
	movq	1672(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1056
.L803:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1736(%rbx)
	movq	(%r12), %rax
	movq	1680(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1057
.L804:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1744(%rbx)
	movq	(%r12), %rax
	movq	1688(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1058
.L805:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1752(%rbx)
	movq	(%r12), %rax
	movq	1696(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1059
.L806:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1760(%rbx)
	movq	(%r12), %rax
	movq	1704(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1060
.L807:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1768(%rbx)
	movq	(%r12), %rax
	movq	1712(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1061
.L808:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1776(%rbx)
	movq	(%r12), %rax
	movq	1720(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1062
.L809:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1784(%rbx)
	movq	(%r12), %rax
	movq	1728(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1063
.L810:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1792(%rbx)
	movq	(%r12), %rax
	movq	1736(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1064
.L811:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1800(%rbx)
	movq	(%r12), %rax
	movq	1744(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1065
.L812:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1808(%rbx)
	movq	(%r12), %rax
	movq	1752(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1066
.L813:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1816(%rbx)
	movq	(%r12), %rax
	movq	1760(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1067
.L814:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1824(%rbx)
	movq	(%r12), %rax
	movq	1768(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1068
.L815:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1832(%rbx)
	movq	(%r12), %rax
	movq	1776(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1069
.L816:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1840(%rbx)
	movq	(%r12), %rax
	movq	1784(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1070
.L817:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1848(%rbx)
	movq	(%r12), %rax
	movq	1792(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1071
.L818:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1856(%rbx)
	movq	(%r12), %rax
	movq	1800(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1072
.L819:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1864(%rbx)
	movq	(%r12), %rax
	movq	1808(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1073
.L820:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1872(%rbx)
	movq	(%r12), %rax
	movq	1816(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1074
.L821:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1880(%rbx)
	movq	(%r12), %rax
	movq	1824(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1075
.L822:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1888(%rbx)
	movq	(%r12), %rax
	movq	1832(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1076
.L823:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1896(%rbx)
	movq	(%r12), %rax
	movq	1840(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1077
.L824:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1904(%rbx)
	movq	(%r12), %rax
	movq	1848(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1078
.L825:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1912(%rbx)
	movq	(%r12), %rax
	movq	1856(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1079
.L826:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1920(%rbx)
	movq	(%r12), %rax
	movq	1864(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1080
.L827:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1928(%rbx)
	movq	(%r12), %rax
	movq	1872(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1081
.L828:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1936(%rbx)
	movq	(%r12), %rax
	movq	1880(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1082
.L829:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1944(%rbx)
	movq	(%r12), %rax
	movq	1888(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1083
.L830:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1952(%rbx)
	movq	(%r12), %rax
	movq	1896(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1084
.L831:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, 1960(%rbx)
	movq	(%r12), %rax
	movq	1904(%rax), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L1085
.L832:
	movq	2352(%rbx), %rdi
	movq	%r13, %rsi
	leaq	.LC246(%rip), %r14
	xorl	%r13d, %r13d
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	%rax, 1968(%rbx)
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L833:
	movq	2352(%rbx), %rdi
.L846:
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	%rax, 1976(%rbx,%r13,8)
	addq	$1, %r13
	cmpq	$47, %r13
	je	.L1086
.L836:
	movq	(%r12), %rax
	movq	2352(%rbx), %rdi
	movq	1912(%rax,%r13,8), %rsi
	call	_ZN2v87Isolate23GetDataFromSnapshotOnceEm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L833
	movq	stderr(%rip), %rdi
	movq	%r13, %rcx
	movq	%r14, %rdx
	xorl	%eax, %eax
	movl	$1, %esi
	call	__fprintf_chk@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	2352(%rbx), %rdi
	xorl	%esi, %esi
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L1086:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1087
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L847:
	.cfi_restore_state
	movq	stderr(%rip), %rcx
	movl	$49, %edx
	movl	$1, %esi
	leaq	.LC7(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L594
.L848:
	movq	stderr(%rip), %rcx
	movl	$64, %edx
	movl	$1, %esi
	leaq	.LC8(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L595
.L849:
	movq	stderr(%rip), %rcx
	movl	$51, %edx
	movl	$1, %esi
	leaq	.LC9(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L596
.L850:
	movq	stderr(%rip), %rcx
	movl	$56, %edx
	movl	$1, %esi
	leaq	.LC10(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L597
.L851:
	movq	stderr(%rip), %rcx
	movl	$55, %edx
	movl	$1, %esi
	leaq	.LC11(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L598
.L852:
	movq	stderr(%rip), %rcx
	movl	$47, %edx
	movl	$1, %esi
	leaq	.LC12(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L599
.L853:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC13(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L600
.L854:
	movq	stderr(%rip), %rcx
	movl	$49, %edx
	movl	$1, %esi
	leaq	.LC14(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L601
.L855:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC15(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L602
.L856:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC16(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L603
.L857:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC17(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L604
.L858:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC18(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L605
.L859:
	movq	stderr(%rip), %rcx
	movl	$43, %edx
	movl	$1, %esi
	leaq	.LC19(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L606
.L860:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC20(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L607
.L861:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC21(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L608
.L862:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC22(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L609
.L863:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC23(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L610
.L864:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC24(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L611
.L865:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC25(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L612
.L866:
	movq	stderr(%rip), %rcx
	movl	$45, %edx
	movl	$1, %esi
	leaq	.LC26(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L613
.L867:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC27(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L614
.L868:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC28(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L615
.L869:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC29(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L616
.L870:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC30(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L617
.L871:
	movq	stderr(%rip), %rcx
	movl	$43, %edx
	movl	$1, %esi
	leaq	.LC31(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L618
.L872:
	movq	stderr(%rip), %rcx
	movl	$50, %edx
	movl	$1, %esi
	leaq	.LC32(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L619
.L873:
	movq	stderr(%rip), %rcx
	movl	$50, %edx
	movl	$1, %esi
	leaq	.LC33(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L620
.L874:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC34(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L621
.L875:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC35(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L622
.L876:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC36(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L623
.L877:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC37(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L624
.L878:
	movq	stderr(%rip), %rcx
	movl	$58, %edx
	movl	$1, %esi
	leaq	.LC38(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L625
.L879:
	movq	stderr(%rip), %rcx
	movl	$49, %edx
	movl	$1, %esi
	leaq	.LC39(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L626
.L880:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC40(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L627
.L881:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC41(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L628
.L882:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC42(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L629
.L883:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC43(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L630
.L884:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC44(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L631
.L885:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC45(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L632
.L886:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC46(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L633
.L887:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC47(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L634
.L888:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC48(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L635
.L889:
	movq	stderr(%rip), %rcx
	movl	$43, %edx
	movl	$1, %esi
	leaq	.LC49(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L636
.L890:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC50(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L637
.L891:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC51(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L638
.L892:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC52(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L639
.L893:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC53(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L640
.L894:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC54(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L641
.L895:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC55(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L642
.L896:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC56(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L643
.L897:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC57(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L644
.L898:
	movq	stderr(%rip), %rcx
	movl	$32, %edx
	movl	$1, %esi
	leaq	.LC58(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L645
.L899:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC59(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L646
.L900:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC60(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L647
.L901:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC61(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L648
.L902:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC62(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L649
.L903:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC63(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L650
.L904:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC64(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L651
.L905:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC65(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L652
.L906:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC66(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L653
.L907:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC67(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L654
.L908:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC68(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L655
.L909:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC69(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L656
.L910:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC70(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L657
.L911:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC71(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L658
.L912:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC72(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L659
.L913:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC73(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L660
.L914:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC74(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L661
.L915:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC75(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L662
.L916:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC76(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L663
.L917:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC77(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L664
.L918:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC78(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L665
.L919:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC79(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L666
.L920:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC80(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L667
.L921:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC81(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L668
.L922:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC82(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L669
.L923:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC83(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L670
.L924:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC84(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L671
.L925:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC85(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L672
.L926:
	movq	stderr(%rip), %rcx
	movl	$43, %edx
	movl	$1, %esi
	leaq	.LC86(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L673
.L927:
	movq	stderr(%rip), %rcx
	movl	$45, %edx
	movl	$1, %esi
	leaq	.LC87(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L674
.L928:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC88(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L675
.L929:
	movq	stderr(%rip), %rcx
	movl	$45, %edx
	movl	$1, %esi
	leaq	.LC89(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L676
.L930:
	movq	stderr(%rip), %rcx
	movl	$32, %edx
	movl	$1, %esi
	leaq	.LC90(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L677
.L931:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC91(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L678
.L932:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC92(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L679
.L933:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC93(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L680
.L934:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC94(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L681
.L935:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC95(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L682
.L936:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC96(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L683
.L937:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC97(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L684
.L938:
	movq	stderr(%rip), %rcx
	movl	$50, %edx
	movl	$1, %esi
	leaq	.LC98(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L685
.L939:
	movq	stderr(%rip), %rcx
	movl	$56, %edx
	movl	$1, %esi
	leaq	.LC99(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L686
.L940:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC100(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L687
.L941:
	movq	stderr(%rip), %rcx
	movl	$32, %edx
	movl	$1, %esi
	leaq	.LC101(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L688
.L942:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC102(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L689
.L943:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC103(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L690
.L944:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC104(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L691
.L945:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC105(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L692
.L946:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC106(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L693
.L947:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC107(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L694
.L948:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC108(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L695
.L949:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC109(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L696
.L950:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC110(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L697
.L951:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC111(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L698
.L952:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC112(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L699
.L953:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC113(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L700
.L954:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC114(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L701
.L955:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC115(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L702
.L956:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC116(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L703
.L957:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC117(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L704
.L958:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC118(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L705
.L959:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC119(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L706
.L960:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC120(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L707
.L961:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC121(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L708
.L962:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC122(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L709
.L963:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC123(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L710
.L964:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC124(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L711
.L965:
	movq	stderr(%rip), %rcx
	movl	$54, %edx
	movl	$1, %esi
	leaq	.LC125(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L712
.L966:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC126(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L713
.L967:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC127(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L714
.L968:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC128(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L715
.L969:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC129(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L716
.L970:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC130(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L717
.L971:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC131(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L718
.L972:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC132(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L719
.L973:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC133(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L720
.L974:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC134(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L721
.L975:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC135(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L722
.L976:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC136(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L723
.L977:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC137(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L724
.L978:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC138(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L725
.L979:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC139(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L726
.L980:
	movq	stderr(%rip), %rcx
	movl	$43, %edx
	movl	$1, %esi
	leaq	.LC140(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L727
.L981:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC141(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L728
.L982:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC142(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L729
.L983:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC143(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L730
.L984:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC144(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L731
.L985:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC145(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L732
.L986:
	movq	stderr(%rip), %rcx
	movl	$45, %edx
	movl	$1, %esi
	leaq	.LC146(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L733
.L987:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC147(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L734
.L988:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC148(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L735
.L989:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC149(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L736
.L990:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC150(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L737
.L991:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC151(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L738
.L992:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC152(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L739
.L993:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC153(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L740
.L994:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC154(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L741
.L995:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC155(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L742
.L996:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC156(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L743
.L997:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC157(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L744
.L998:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC158(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L745
.L999:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC159(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L746
.L1000:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC160(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L747
.L1001:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC161(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L748
.L1002:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC162(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L749
.L1003:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC163(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L750
.L1004:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC164(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L751
.L1005:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC165(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L752
.L1006:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC166(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L753
.L1007:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC167(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L754
.L1008:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC168(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L755
.L1009:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC169(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L756
.L1010:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC170(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L757
.L1011:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC171(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L758
.L1012:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC172(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L759
.L1013:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC173(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L760
.L1014:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC174(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L761
.L1015:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC175(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L762
.L1016:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC176(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L763
.L1017:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC177(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L764
.L1018:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC178(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L765
.L1019:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC179(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L766
.L1020:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC180(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L767
.L1021:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC181(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L768
.L1022:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC182(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L769
.L1023:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC183(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L770
.L1024:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC184(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L771
.L1025:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC185(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L772
.L1026:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC186(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L773
.L1027:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC187(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L774
.L1028:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC188(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L775
.L1029:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC189(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L776
.L1030:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC190(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L777
.L1031:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC191(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L778
.L1032:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC192(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L779
.L1033:
	movq	stderr(%rip), %rcx
	movl	$43, %edx
	movl	$1, %esi
	leaq	.LC193(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L780
.L1034:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC194(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L781
.L1035:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC195(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L782
.L1036:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC196(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L783
.L1037:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC197(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L784
.L1038:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC198(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L785
.L1039:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC199(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L786
.L1040:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC200(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L787
.L1041:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC201(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L788
.L1042:
	movq	stderr(%rip), %rcx
	movl	$45, %edx
	movl	$1, %esi
	leaq	.LC202(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L789
.L1043:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC203(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L790
.L1044:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC204(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L791
.L1045:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC205(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L792
.L1046:
	movq	stderr(%rip), %rcx
	movl	$43, %edx
	movl	$1, %esi
	leaq	.LC206(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L793
.L1047:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC207(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L794
.L1048:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC208(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L795
.L1049:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC209(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L796
.L1050:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC210(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L797
.L1051:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC211(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L798
.L1052:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC212(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L799
.L1053:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC213(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L800
.L1054:
	movq	stderr(%rip), %rcx
	movl	$39, %edx
	movl	$1, %esi
	leaq	.LC214(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L801
.L1055:
	movq	stderr(%rip), %rcx
	movl	$47, %edx
	movl	$1, %esi
	leaq	.LC215(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L802
.L1056:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC216(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L803
.L1057:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC217(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L804
.L1058:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC218(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L805
.L1059:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC219(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L806
.L1060:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC220(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L807
.L1061:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC221(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L808
.L1062:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC222(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L809
.L1063:
	movq	stderr(%rip), %rcx
	movl	$45, %edx
	movl	$1, %esi
	leaq	.LC223(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L810
.L1064:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC224(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L811
.L1065:
	movq	stderr(%rip), %rcx
	movl	$48, %edx
	movl	$1, %esi
	leaq	.LC225(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L812
.L1066:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC226(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L813
.L1067:
	movq	stderr(%rip), %rcx
	movl	$47, %edx
	movl	$1, %esi
	leaq	.LC227(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L814
.L1068:
	movq	stderr(%rip), %rcx
	movl	$44, %edx
	movl	$1, %esi
	leaq	.LC228(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L815
.L1069:
	movq	stderr(%rip), %rcx
	movl	$45, %edx
	movl	$1, %esi
	leaq	.LC229(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L816
.L1070:
	movq	stderr(%rip), %rcx
	movl	$33, %edx
	movl	$1, %esi
	leaq	.LC230(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L817
.L1071:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC231(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L818
.L1072:
	movq	stderr(%rip), %rcx
	movl	$40, %edx
	movl	$1, %esi
	leaq	.LC232(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L819
.L1073:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC233(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L820
.L1074:
	movq	stderr(%rip), %rcx
	movl	$35, %edx
	movl	$1, %esi
	leaq	.LC234(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L821
.L1075:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC235(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L822
.L1076:
	movq	stderr(%rip), %rcx
	movl	$37, %edx
	movl	$1, %esi
	leaq	.LC236(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L823
.L1077:
	movq	stderr(%rip), %rcx
	movl	$36, %edx
	movl	$1, %esi
	leaq	.LC237(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L824
.L1078:
	movq	stderr(%rip), %rcx
	movl	$42, %edx
	movl	$1, %esi
	leaq	.LC238(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L825
.L1079:
	movq	stderr(%rip), %rcx
	movl	$56, %edx
	movl	$1, %esi
	leaq	.LC239(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L826
.L1080:
	movq	stderr(%rip), %rcx
	movl	$34, %edx
	movl	$1, %esi
	leaq	.LC240(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L827
.L1081:
	movq	stderr(%rip), %rcx
	movl	$38, %edx
	movl	$1, %esi
	leaq	.LC241(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L828
.L1082:
	movq	stderr(%rip), %rcx
	movl	$47, %edx
	movl	$1, %esi
	leaq	.LC242(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L829
.L1083:
	movq	stderr(%rip), %rcx
	movl	$46, %edx
	movl	$1, %esi
	leaq	.LC243(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L830
.L1084:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC244(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L831
.L1085:
	movq	stderr(%rip), %rcx
	movl	$41, %edx
	movl	$1, %esi
	leaq	.LC245(%rip), %rdi
	call	fwrite@PLT
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L832
.L1087:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7955:
	.size	_ZN4node11IsolateData21DeserializePropertiesEPKSt6vectorImSaImEE, .-_ZN4node11IsolateData21DeserializePropertiesEPKSt6vectorImSaImEE
	.section	.rodata.str1.1
.LC247:
	.string	"node:alpnBuffer"
.LC248:
	.string	"node:untransferableBuffer"
.LC249:
	.string	"node:arrowMessage"
.LC250:
	.string	"node:contextify:context"
.LC251:
	.string	"node:contextify:global"
.LC252:
	.string	"node:decorated"
.LC253:
	.string	"node:napi:wrapper"
	.section	.rodata.str1.8
	.align 8
.LC254:
	.string	"node:sharedArrayBufferLifetimePartner"
	.section	.rodata.str1.1
.LC255:
	.string	"handle_onclose"
.LC256:
	.string	"no_message_symbol"
.LC257:
	.string	"oninit"
.LC258:
	.string	"owner_symbol"
.LC259:
	.string	"onpskexchange"
.LC260:
	.string	"resource_symbol"
.LC261:
	.string	"trigger_async_id_symbol"
.LC262:
	.string	"address"
.LC263:
	.string	"aliases"
.LC264:
	.string	"args"
.LC265:
	.string	"asn1Curve"
.LC266:
	.string	"bits"
.LC267:
	.string	"buffer"
.LC268:
	.string	"bytesParsed"
.LC269:
	.string	"bytesRead"
.LC270:
	.string	"bytesWritten"
.LC271:
	.string	"cachedDataProduced"
.LC272:
	.string	"cachedDataRejected"
.LC273:
	.string	"cachedData"
.LC274:
	.string	"cacheKey"
.LC275:
	.string	"change"
.LC276:
	.string	"channel"
.LC277:
	.string	"chunksSentSinceLastWrite"
	.section	.rodata.str1.8
	.align 8
.LC278:
	.string	"Cannot transfer object of unsupported type."
	.section	.rodata.str1.1
.LC279:
	.string	"code"
.LC280:
	.string	"commonjs"
.LC281:
	.string	"config"
.LC282:
	.string	"constants"
.LC283:
	.string	"dh"
.LC284:
	.string	"dsa"
.LC285:
	.string	"ec"
.LC286:
	.string	"ed25519"
.LC287:
	.string	"ed448"
.LC288:
	.string	"x25519"
.LC289:
	.string	"x448"
.LC290:
	.string	"rsa"
.LC291:
	.string	"rsa-pss"
.LC292:
	.string	"cwd"
.LC293:
	.string	"data"
.LC294:
	.string	"dest"
.LC295:
	.string	"destroyed"
.LC296:
	.string	"detached"
.LC297:
	.string	"DH"
.LC298:
	.string	"A"
.LC299:
	.string	"AAAA"
.LC300:
	.string	"CNAME"
.LC301:
	.string	"MX"
.LC302:
	.string	"NAPTR"
.LC303:
	.string	"NS"
.LC304:
	.string	"PTR"
.LC305:
	.string	"SOA"
.LC306:
	.string	"SRV"
.LC307:
	.string	"TXT"
.LC308:
	.string	"done"
.LC309:
	.string	"duration"
.LC310:
	.string	"ECDH"
.LC311:
	.string	"emitWarning"
.LC312:
	.string	"{}"
.LC313:
	.string	"encoding"
.LC314:
	.string	"entries"
.LC315:
	.string	"entryType"
.LC316:
	.string	"envPairs"
.LC317:
	.string	"envVarSettings"
.LC318:
	.string	"errno"
.LC319:
	.string	"error"
.LC320:
	.string	"exchange"
.LC321:
	.string	"exitCode"
.LC322:
	.string	"expire"
.LC323:
	.string	"exponent"
.LC324:
	.string	"exports"
.LC325:
	.string	"ext_key_usage"
.LC326:
	.string	"_externalStream"
.LC327:
	.string	"family"
.LC328:
	.string	"_fatalException"
.LC329:
	.string	"fd"
.LC330:
	.string	"file"
.LC331:
	.string	"fingerprint256"
.LC332:
	.string	"fingerprint"
.LC333:
	.string	"flags"
.LC334:
	.string	"fragment"
.LC335:
	.string	"function"
.LC336:
	.string	"_getDataCloneError"
.LC337:
	.string	"_getSharedArrayBufferId"
.LC338:
	.string	"gid"
.LC339:
	.string	"h2"
.LC340:
	.string	"handle"
.LC341:
	.string	"helpText"
.LC342:
	.string	"homedir"
.LC343:
	.string	"host"
.LC344:
	.string	"hostmaster"
.LC345:
	.string	"http/1.1"
.LC346:
	.string	"identity"
.LC347:
	.string	"ignore"
.LC348:
	.string	"infoAccess"
.LC349:
	.string	"inherit"
.LC350:
	.string	"input"
.LC351:
	.string	"internalBinding"
.LC352:
	.string	"internal"
.LC353:
	.string	"IPv4"
.LC354:
	.string	"IPv6"
.LC355:
	.string	"isClosing"
.LC356:
	.string	"issuer"
.LC357:
	.string	"issuerCertificate"
.LC358:
	.string	"killSignal"
.LC359:
	.string	"kind"
.LC360:
	.string	"library"
.LC361:
	.string	"mac"
.LC362:
	.string	"maxBuffer"
.LC363:
	.string	"MessagePort"
.LC364:
	.string	"messagePort"
.LC365:
	.string	"message"
.LC366:
	.string	"minttl"
.LC367:
	.string	"module"
.LC368:
	.string	"modulus"
.LC369:
	.string	"name"
.LC370:
	.string	"netmask"
.LC371:
	.string	"next"
.LC372:
	.string	"nistCurve"
.LC373:
	.string	"node"
.LC374:
	.string	"nsname"
.LC375:
	.string	"OCSPRequest"
.LC376:
	.string	"oncertcb"
.LC377:
	.string	"onchange"
.LC378:
	.string	"onclienthello"
.LC379:
	.string	"oncomplete"
.LC380:
	.string	"onconnection"
.LC381:
	.string	"ondone"
.LC382:
	.string	"onerror"
.LC383:
	.string	"onexit"
.LC384:
	.string	"onhandshakedone"
.LC385:
	.string	"onhandshakestart"
.LC386:
	.string	"onkeylog"
.LC387:
	.string	"onmessage"
.LC388:
	.string	"onnewsession"
.LC389:
	.string	"onocspresponse"
.LC390:
	.string	"onreadstart"
.LC391:
	.string	"onreadstop"
.LC392:
	.string	"onshutdown"
.LC393:
	.string	"onsignal"
.LC394:
	.string	"onunpipe"
.LC395:
	.string	"onwrite"
.LC396:
	.string	"opensslErrorStack"
.LC397:
	.string	"options"
.LC398:
	.string	"order"
.LC399:
	.string	"output"
.LC400:
	.string	"Parse Error"
.LC401:
	.string	"password"
.LC402:
	.string	"path"
.LC403:
	.string	"pendingHandle"
.LC404:
	.string	"pid"
.LC405:
	.string	"pipeSource"
.LC406:
	.string	"pipe"
.LC407:
	.string	"pipeTarget"
.LC408:
	.string	"port1"
.LC409:
	.string	"port2"
.LC410:
	.string	"port"
.LC411:
	.string	"preference"
.LC412:
	.string	"primordials"
.LC413:
	.string	"priority"
.LC414:
	.string	"process"
.LC415:
	.string	"promise"
.LC416:
	.string	"psk"
.LC417:
	.string	"pubkey"
.LC418:
	.string	"query"
.LC419:
	.string	"raw"
.LC420:
	.string	"_readHostObject"
.LC421:
	.string	"readable"
.LC422:
	.string	"reason"
.LC423:
	.string	"refresh"
.LC424:
	.string	"regexp"
.LC425:
	.string	"rename"
.LC426:
	.string	"replacement"
.LC427:
	.string	"require"
.LC428:
	.string	"retry"
.LC429:
	.string	"scheme"
.LC430:
	.string	"scopeid"
.LC431:
	.string	"serialNumber"
.LC432:
	.string	"serial"
.LC433:
	.string	"servername"
.LC434:
	.string	"service"
.LC435:
	.string	"sessionId"
.LC436:
	.string	"shell"
.LC437:
	.string	"signal"
.LC438:
	.string	"sink"
.LC439:
	.string	"size"
.LC440:
	.string	"Invalid SNI context"
.LC441:
	.string	"sni_context"
.LC442:
	.string	"source"
.LC443:
	.string	"stack"
.LC444:
	.string	"standardName"
.LC445:
	.string	"startTime"
.LC446:
	.string	"status"
.LC447:
	.string	"stdio"
.LC448:
	.string	"subject"
.LC449:
	.string	"subjectaltname"
.LC450:
	.string	"syscall"
.LC451:
	.string	"target"
.LC452:
	.string	"threadId"
.LC453:
	.string	"onticketkeycallback"
.LC454:
	.string	"timeout"
.LC455:
	.string	"tlsTicket"
.LC456:
	.string	"transfer"
.LC457:
	.string	"ttl"
.LC458:
	.string	"type"
.LC459:
	.string	"uid"
.LC460:
	.string	"<unknown>"
.LC461:
	.string	"ftp:"
.LC462:
	.string	"file:"
.LC463:
	.string	"gopher:"
.LC464:
	.string	"http:"
.LC465:
	.string	"https:"
.LC466:
	.string	"ws:"
.LC467:
	.string	"wss:"
.LC468:
	.string	"url"
.LC469:
	.string	"username"
.LC470:
	.string	"valid_from"
.LC471:
	.string	"valid_to"
.LC472:
	.string	"value"
.LC473:
	.string	"verifyError"
.LC474:
	.string	"version"
.LC475:
	.string	"weight"
.LC476:
	.string	"windowsHide"
.LC477:
	.string	"windowsVerbatimArguments"
.LC478:
	.string	"wrap"
.LC479:
	.string	"writable"
.LC480:
	.string	"_writeHostObject"
.LC481:
	.string	"writeQueueSize"
.LC482:
	.string	"x-forwarded-for"
.LC483:
	.string	"ZERO_RETURN"
.LC484:
	.string	"NONE"
.LC485:
	.string	"DIRHANDLE"
.LC486:
	.string	"DNSCHANNEL"
.LC487:
	.string	"ELDHISTOGRAM"
.LC488:
	.string	"FILEHANDLE"
.LC489:
	.string	"FILEHANDLECLOSEREQ"
.LC490:
	.string	"FSEVENTWRAP"
.LC491:
	.string	"FSREQCALLBACK"
.LC492:
	.string	"FSREQPROMISE"
.LC493:
	.string	"GETADDRINFOREQWRAP"
.LC494:
	.string	"GETNAMEINFOREQWRAP"
.LC495:
	.string	"HEAPSNAPSHOT"
.LC496:
	.string	"HTTP2SESSION"
.LC497:
	.string	"HTTP2STREAM"
.LC498:
	.string	"HTTP2PING"
.LC499:
	.string	"HTTP2SETTINGS"
.LC500:
	.string	"HTTPINCOMINGMESSAGE"
.LC501:
	.string	"HTTPCLIENTREQUEST"
.LC502:
	.string	"JSSTREAM"
.LC503:
	.string	"MESSAGEPORT"
.LC504:
	.string	"PIPECONNECTWRAP"
.LC505:
	.string	"PIPESERVERWRAP"
.LC506:
	.string	"PIPEWRAP"
.LC507:
	.string	"PROCESSWRAP"
.LC508:
	.string	"PROMISE"
.LC509:
	.string	"QUERYWRAP"
.LC510:
	.string	"SHUTDOWNWRAP"
.LC511:
	.string	"SIGNALWRAP"
.LC512:
	.string	"STATWATCHER"
.LC513:
	.string	"STREAMPIPE"
.LC514:
	.string	"TCPCONNECTWRAP"
.LC515:
	.string	"TCPSERVERWRAP"
.LC516:
	.string	"TCPWRAP"
.LC517:
	.string	"TTYWRAP"
.LC518:
	.string	"UDPSENDWRAP"
.LC519:
	.string	"UDPWRAP"
.LC520:
	.string	"SIGINTWATCHDOG"
.LC521:
	.string	"WORKER"
.LC522:
	.string	"WORKERHEAPSNAPSHOT"
.LC523:
	.string	"WRITEWRAP"
.LC524:
	.string	"ZLIB"
.LC525:
	.string	"PBKDF2REQUEST"
.LC526:
	.string	"KEYPAIRGENREQUEST"
.LC527:
	.string	"RANDOMBYTESREQUEST"
.LC528:
	.string	"SCRYPTREQUEST"
.LC529:
	.string	"TLSWRAP"
.LC530:
	.string	"INSPECTORJSBINDING"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11IsolateData16CreatePropertiesEv
	.type	_ZN4node11IsolateData16CreatePropertiesEv, @function
_ZN4node11IsolateData16CreatePropertiesEv:
.LFB7956:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	subq	$48, %rsp
	movq	2352(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	leaq	.LC247(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1377
.L1089:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$25, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 64(%rbx)
	leaq	.LC248(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1378
.L1090:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$17, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 72(%rbx)
	leaq	.LC249(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1379
.L1091:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$23, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 80(%rbx)
	leaq	.LC250(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1380
.L1092:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$22, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 88(%rbx)
	leaq	.LC251(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1381
.L1093:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 96(%rbx)
	leaq	.LC252(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1382
.L1094:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$17, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 104(%rbx)
	leaq	.LC253(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1383
.L1095:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$37, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 112(%rbx)
	leaq	.LC254(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1384
.L1096:
	movq	2352(%rbx), %rdi
	call	_ZN2v87Private3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 120(%rbx)
	leaq	.LC255(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1385
.L1097:
	movq	2352(%rbx), %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$17, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 128(%rbx)
	leaq	.LC256(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1386
.L1098:
	movq	2352(%rbx), %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 136(%rbx)
	leaq	.LC257(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1387
.L1099:
	movq	2352(%rbx), %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 144(%rbx)
	leaq	.LC258(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1388
.L1100:
	movq	2352(%rbx), %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 152(%rbx)
	leaq	.LC259(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1389
.L1101:
	movq	2352(%rbx), %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 160(%rbx)
	leaq	.LC260(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1390
.L1102:
	movq	2352(%rbx), %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$23, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 168(%rbx)
	leaq	.LC261(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1391
.L1103:
	movq	2352(%rbx), %rdi
	call	_ZN2v86Symbol3NewEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	2352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 176(%rbx)
	leaq	.LC262(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1392
.L1104:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 184(%rbx)
	leaq	.LC263(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1393
.L1105:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 192(%rbx)
	leaq	.LC264(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1394
.L1106:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 200(%rbx)
	leaq	.LC265(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1395
.L1107:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 208(%rbx)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1396
.L1108:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 216(%rbx)
	leaq	.LC266(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1397
.L1109:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 224(%rbx)
	leaq	.LC267(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1398
.L1110:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 232(%rbx)
	leaq	.LC268(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1399
.L1111:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 240(%rbx)
	leaq	.LC269(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1400
.L1112:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 248(%rbx)
	leaq	.LC270(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1401
.L1113:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 256(%rbx)
	leaq	.LC271(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1402
.L1114:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 264(%rbx)
	leaq	.LC272(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1403
.L1115:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 272(%rbx)
	leaq	.LC273(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1404
.L1116:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 280(%rbx)
	leaq	.LC274(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1405
.L1117:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 288(%rbx)
	leaq	.LC275(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1406
.L1118:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 296(%rbx)
	leaq	.LC276(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1407
.L1119:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$24, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 304(%rbx)
	leaq	.LC277(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1408
.L1120:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$43, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 312(%rbx)
	leaq	.LC278(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1409
.L1121:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 320(%rbx)
	leaq	.LC279(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1410
.L1122:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 328(%rbx)
	leaq	.LC280(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1411
.L1123:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 336(%rbx)
	leaq	.LC281(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1412
.L1124:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 344(%rbx)
	leaq	.LC282(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1413
.L1125:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 352(%rbx)
	leaq	.LC283(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1414
.L1126:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 360(%rbx)
	leaq	.LC284(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1415
.L1127:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 368(%rbx)
	leaq	.LC285(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1416
.L1128:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 376(%rbx)
	leaq	.LC286(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1417
.L1129:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 384(%rbx)
	leaq	.LC287(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1418
.L1130:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 392(%rbx)
	leaq	.LC288(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1419
.L1131:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 400(%rbx)
	leaq	.LC289(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1420
.L1132:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 408(%rbx)
	leaq	.LC290(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1421
.L1133:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 416(%rbx)
	leaq	.LC291(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1422
.L1134:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 424(%rbx)
	leaq	.LC292(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1423
.L1135:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 432(%rbx)
	leaq	.LC293(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1424
.L1136:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 440(%rbx)
	leaq	.LC294(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1425
.L1137:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 448(%rbx)
	leaq	.LC295(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1426
.L1138:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 456(%rbx)
	leaq	.LC296(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1427
.L1139:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 464(%rbx)
	leaq	.LC297(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1428
.L1140:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$1, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 472(%rbx)
	leaq	.LC298(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1429
.L1141:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 480(%rbx)
	leaq	.LC299(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1430
.L1142:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 488(%rbx)
	leaq	.LC300(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1431
.L1143:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 496(%rbx)
	leaq	.LC301(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1432
.L1144:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 504(%rbx)
	leaq	.LC302(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1433
.L1145:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 512(%rbx)
	leaq	.LC303(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1434
.L1146:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 520(%rbx)
	leaq	.LC304(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1435
.L1147:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 528(%rbx)
	leaq	.LC305(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1436
.L1148:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 536(%rbx)
	leaq	.LC306(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1437
.L1149:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 544(%rbx)
	leaq	.LC307(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1438
.L1150:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 552(%rbx)
	leaq	.LC308(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1439
.L1151:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 560(%rbx)
	leaq	.LC309(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1440
.L1152:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 568(%rbx)
	leaq	.LC310(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1441
.L1153:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 576(%rbx)
	leaq	.LC311(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1442
.L1154:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 584(%rbx)
	leaq	.LC312(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1443
.L1155:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 592(%rbx)
	leaq	.LC313(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1444
.L1156:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 600(%rbx)
	leaq	.LC314(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1445
.L1157:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 608(%rbx)
	leaq	.LC315(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1446
.L1158:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 616(%rbx)
	leaq	.LC316(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1447
.L1159:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 624(%rbx)
	leaq	.LC317(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1448
.L1160:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 632(%rbx)
	leaq	.LC318(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1449
.L1161:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 640(%rbx)
	leaq	.LC319(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1450
.L1162:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 648(%rbx)
	leaq	.LC320(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1451
.L1163:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 656(%rbx)
	leaq	.LC321(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1452
.L1164:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 664(%rbx)
	leaq	.LC322(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1453
.L1165:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 672(%rbx)
	leaq	.LC323(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1454
.L1166:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 680(%rbx)
	leaq	.LC324(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1455
.L1167:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 688(%rbx)
	leaq	.LC325(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1456
.L1168:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 696(%rbx)
	leaq	.LC326(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1457
.L1169:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 704(%rbx)
	leaq	.LC327(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1458
.L1170:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 712(%rbx)
	leaq	.LC328(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1459
.L1171:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 720(%rbx)
	leaq	.LC329(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1460
.L1172:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 728(%rbx)
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1461
.L1173:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 736(%rbx)
	leaq	.LC330(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1462
.L1174:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 744(%rbx)
	leaq	.LC331(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1463
.L1175:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 752(%rbx)
	leaq	.LC332(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1464
.L1176:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 760(%rbx)
	leaq	.LC333(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1465
.L1177:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 768(%rbx)
	leaq	.LC334(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1466
.L1178:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 776(%rbx)
	leaq	.LC335(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1467
.L1179:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 784(%rbx)
	leaq	.LC336(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1468
.L1180:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$23, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 792(%rbx)
	leaq	.LC337(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1469
.L1181:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 800(%rbx)
	leaq	.LC338(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1470
.L1182:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$2, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 808(%rbx)
	leaq	.LC339(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1471
.L1183:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 816(%rbx)
	leaq	.LC340(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1472
.L1184:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 824(%rbx)
	leaq	.LC341(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1473
.L1185:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 832(%rbx)
	leaq	.LC342(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1474
.L1186:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 840(%rbx)
	leaq	.LC343(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1475
.L1187:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 848(%rbx)
	leaq	.LC344(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1476
.L1188:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 856(%rbx)
	leaq	.LC345(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1477
.L1189:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 864(%rbx)
	leaq	.LC346(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1478
.L1190:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 872(%rbx)
	leaq	.LC347(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1479
.L1191:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 880(%rbx)
	leaq	.LC348(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1480
.L1192:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 888(%rbx)
	leaq	.LC349(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1481
.L1193:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 896(%rbx)
	leaq	.LC350(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1482
.L1194:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 904(%rbx)
	leaq	.LC351(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1483
.L1195:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 912(%rbx)
	leaq	.LC352(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1484
.L1196:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 920(%rbx)
	leaq	.LC353(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1485
.L1197:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 928(%rbx)
	leaq	.LC354(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1486
.L1198:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 936(%rbx)
	leaq	.LC355(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1487
.L1199:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 944(%rbx)
	leaq	.LC356(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1488
.L1200:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$17, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 952(%rbx)
	leaq	.LC357(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1489
.L1201:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 960(%rbx)
	leaq	.LC358(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1490
.L1202:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 968(%rbx)
	leaq	.LC359(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1491
.L1203:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 976(%rbx)
	leaq	.LC360(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1492
.L1204:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 984(%rbx)
	leaq	.LC361(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1493
.L1205:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 992(%rbx)
	leaq	.LC362(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1494
.L1206:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1000(%rbx)
	leaq	.LC363(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1495
.L1207:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1008(%rbx)
	leaq	.LC364(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1496
.L1208:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1016(%rbx)
	leaq	.LC365(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1497
.L1209:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1024(%rbx)
	leaq	.LC366(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1498
.L1210:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1032(%rbx)
	leaq	.LC367(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1499
.L1211:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1040(%rbx)
	leaq	.LC368(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1500
.L1212:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1048(%rbx)
	leaq	.LC369(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1501
.L1213:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1056(%rbx)
	leaq	.LC370(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1502
.L1214:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1064(%rbx)
	leaq	.LC371(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1503
.L1215:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1072(%rbx)
	leaq	.LC372(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1504
.L1216:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1080(%rbx)
	leaq	.LC373(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1505
.L1217:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1088(%rbx)
	leaq	.LC374(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1506
.L1218:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1096(%rbx)
	leaq	.LC375(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1507
.L1219:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1104(%rbx)
	leaq	.LC376(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1508
.L1220:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1112(%rbx)
	leaq	.LC377(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1509
.L1221:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1120(%rbx)
	leaq	.LC378(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1510
.L1222:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1128(%rbx)
	leaq	.LC379(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1511
.L1223:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1136(%rbx)
	leaq	.LC380(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1512
.L1224:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1144(%rbx)
	leaq	.LC381(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1513
.L1225:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1152(%rbx)
	leaq	.LC382(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1514
.L1226:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1160(%rbx)
	leaq	.LC383(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1515
.L1227:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1168(%rbx)
	leaq	.LC384(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1516
.L1228:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$16, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1176(%rbx)
	leaq	.LC385(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1517
.L1229:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1184(%rbx)
	leaq	.LC386(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1518
.L1230:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1192(%rbx)
	leaq	.LC387(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1519
.L1231:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1200(%rbx)
	leaq	.LC388(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1520
.L1232:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1208(%rbx)
	leaq	.LC389(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1521
.L1233:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1216(%rbx)
	leaq	.LC390(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1522
.L1234:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1224(%rbx)
	leaq	.LC391(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1523
.L1235:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1232(%rbx)
	leaq	.LC392(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1524
.L1236:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1240(%rbx)
	leaq	.LC393(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1525
.L1237:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1248(%rbx)
	leaq	.LC394(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1526
.L1238:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1256(%rbx)
	leaq	.LC395(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1527
.L1239:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$17, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1264(%rbx)
	leaq	.LC396(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1528
.L1240:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1272(%rbx)
	leaq	.LC397(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1529
.L1241:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1280(%rbx)
	leaq	.LC398(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1530
.L1242:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1288(%rbx)
	leaq	.LC399(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1531
.L1243:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1296(%rbx)
	leaq	.LC400(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1532
.L1244:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1304(%rbx)
	leaq	.LC401(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1533
.L1245:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1312(%rbx)
	leaq	.LC402(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1534
.L1246:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1320(%rbx)
	leaq	.LC403(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1535
.L1247:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1328(%rbx)
	leaq	.LC404(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1536
.L1248:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1336(%rbx)
	leaq	.LC405(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1537
.L1249:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1344(%rbx)
	leaq	.LC406(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1538
.L1250:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1352(%rbx)
	leaq	.LC407(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1539
.L1251:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1360(%rbx)
	leaq	.LC408(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1540
.L1252:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1368(%rbx)
	leaq	.LC409(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1541
.L1253:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1376(%rbx)
	leaq	.LC410(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1542
.L1254:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1384(%rbx)
	leaq	.LC411(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1543
.L1255:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1392(%rbx)
	leaq	.LC412(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1544
.L1256:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1400(%rbx)
	leaq	.LC413(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1545
.L1257:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1408(%rbx)
	leaq	.LC414(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1546
.L1258:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1416(%rbx)
	leaq	.LC415(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1547
.L1259:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1424(%rbx)
	leaq	.LC416(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1548
.L1260:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1432(%rbx)
	leaq	.LC417(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1549
.L1261:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1440(%rbx)
	leaq	.LC418(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1550
.L1262:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1448(%rbx)
	leaq	.LC419(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1551
.L1263:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1456(%rbx)
	leaq	.LC420(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1552
.L1264:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1464(%rbx)
	leaq	.LC421(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1553
.L1265:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1472(%rbx)
	leaq	.LC422(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1554
.L1266:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1480(%rbx)
	leaq	.LC423(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1555
.L1267:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1488(%rbx)
	leaq	.LC424(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1556
.L1268:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1496(%rbx)
	leaq	.LC425(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1557
.L1269:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1504(%rbx)
	leaq	.LC426(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1558
.L1270:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1512(%rbx)
	leaq	.LC427(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1559
.L1271:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1520(%rbx)
	leaq	.LC428(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1560
.L1272:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1528(%rbx)
	leaq	.LC429(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1561
.L1273:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1536(%rbx)
	leaq	.LC430(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1562
.L1274:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1544(%rbx)
	leaq	.LC431(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1563
.L1275:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1552(%rbx)
	leaq	.LC432(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1564
.L1276:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1560(%rbx)
	leaq	.LC433(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1565
.L1277:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1568(%rbx)
	leaq	.LC434(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1566
.L1278:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1576(%rbx)
	leaq	.LC435(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1567
.L1279:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1584(%rbx)
	leaq	.LC436(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1568
.L1280:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1592(%rbx)
	leaq	.LC437(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1569
.L1281:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1600(%rbx)
	leaq	.LC438(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1570
.L1282:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1608(%rbx)
	leaq	.LC439(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1571
.L1283:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$19, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1616(%rbx)
	leaq	.LC440(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1572
.L1284:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1624(%rbx)
	leaq	.LC441(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1573
.L1285:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1632(%rbx)
	leaq	.LC442(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1574
.L1286:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1640(%rbx)
	leaq	.LC443(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1575
.L1287:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1648(%rbx)
	leaq	.LC444(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1576
.L1288:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1656(%rbx)
	leaq	.LC445(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1577
.L1289:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1664(%rbx)
	leaq	.LC446(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1578
.L1290:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1672(%rbx)
	leaq	.LC447(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1579
.L1291:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1680(%rbx)
	leaq	.LC448(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1580
.L1292:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1688(%rbx)
	leaq	.LC449(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1581
.L1293:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1696(%rbx)
	leaq	.LC450(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1582
.L1294:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1704(%rbx)
	leaq	.LC451(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1583
.L1295:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1712(%rbx)
	leaq	.LC452(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1584
.L1296:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$19, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1720(%rbx)
	leaq	.LC453(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1585
.L1297:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1728(%rbx)
	leaq	.LC454(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1586
.L1298:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1736(%rbx)
	leaq	.LC455(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1587
.L1299:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1744(%rbx)
	leaq	.LC456(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1588
.L1300:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1752(%rbx)
	leaq	.LC457(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1589
.L1301:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1760(%rbx)
	leaq	.LC458(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1590
.L1302:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1768(%rbx)
	leaq	.LC459(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1591
.L1303:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1776(%rbx)
	leaq	.LC460(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1592
.L1304:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1784(%rbx)
	leaq	.LC461(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1593
.L1305:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1792(%rbx)
	leaq	.LC462(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1594
.L1306:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1800(%rbx)
	leaq	.LC463(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1595
.L1307:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1808(%rbx)
	leaq	.LC464(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1596
.L1308:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1816(%rbx)
	leaq	.LC465(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1597
.L1309:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1824(%rbx)
	leaq	.LC466(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1598
.L1310:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1832(%rbx)
	leaq	.LC467(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1599
.L1311:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$3, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1840(%rbx)
	leaq	.LC468(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1600
.L1312:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1848(%rbx)
	leaq	.LC469(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1601
.L1313:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1856(%rbx)
	leaq	.LC470(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1602
.L1314:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1864(%rbx)
	leaq	.LC471(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1603
.L1315:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$5, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1872(%rbx)
	leaq	.LC472(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1604
.L1316:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1880(%rbx)
	leaq	.LC473(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1605
.L1317:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1888(%rbx)
	leaq	.LC474(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1606
.L1318:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1896(%rbx)
	leaq	.LC475(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1607
.L1319:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1904(%rbx)
	leaq	.LC476(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1608
.L1320:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$24, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1912(%rbx)
	leaq	.LC477(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1609
.L1321:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1920(%rbx)
	leaq	.LC478(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1610
.L1322:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1928(%rbx)
	leaq	.LC479(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1611
.L1323:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$16, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1936(%rbx)
	leaq	.LC480(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1612
.L1324:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1944(%rbx)
	leaq	.LC481(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1613
.L1325:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1952(%rbx)
	leaq	.LC482(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1614
.L1326:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1960(%rbx)
	leaq	.LC483(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1615
.L1327:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1968(%rbx)
	leaq	.LC484(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1616
.L1328:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1976(%rbx)
	leaq	.LC485(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1617
.L1329:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1984(%rbx)
	leaq	.LC486(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1618
.L1330:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 1992(%rbx)
	leaq	.LC487(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1619
.L1331:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2000(%rbx)
	leaq	.LC488(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1620
.L1332:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2008(%rbx)
	leaq	.LC489(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1621
.L1333:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2016(%rbx)
	leaq	.LC490(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1622
.L1334:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2024(%rbx)
	leaq	.LC491(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1623
.L1335:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2032(%rbx)
	leaq	.LC492(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1624
.L1336:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2040(%rbx)
	leaq	.LC493(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1625
.L1337:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2048(%rbx)
	leaq	.LC494(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1626
.L1338:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2056(%rbx)
	leaq	.LC495(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1627
.L1339:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2064(%rbx)
	leaq	.LC496(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1628
.L1340:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2072(%rbx)
	leaq	.LC497(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1629
.L1341:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2080(%rbx)
	leaq	.LC498(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1630
.L1342:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2088(%rbx)
	leaq	.LC499(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1631
.L1343:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$19, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2096(%rbx)
	leaq	.LC500(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1632
.L1344:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$17, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2104(%rbx)
	leaq	.LC501(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1633
.L1345:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2112(%rbx)
	leaq	.LC502(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1634
.L1346:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2120(%rbx)
	leaq	.LC503(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1635
.L1347:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$15, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2128(%rbx)
	leaq	.LC504(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1636
.L1348:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2136(%rbx)
	leaq	.LC505(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1637
.L1349:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$8, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2144(%rbx)
	leaq	.LC506(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1638
.L1350:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2152(%rbx)
	leaq	.LC507(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1639
.L1351:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2160(%rbx)
	leaq	.LC508(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1640
.L1352:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2168(%rbx)
	leaq	.LC509(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1641
.L1353:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$12, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2176(%rbx)
	leaq	.LC510(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1642
.L1354:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2184(%rbx)
	leaq	.LC511(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1643
.L1355:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2192(%rbx)
	leaq	.LC512(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1644
.L1356:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$10, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2200(%rbx)
	leaq	.LC513(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1645
.L1357:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2208(%rbx)
	leaq	.LC514(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1646
.L1358:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2216(%rbx)
	leaq	.LC515(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1647
.L1359:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2224(%rbx)
	leaq	.LC516(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1648
.L1360:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2232(%rbx)
	leaq	.LC517(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1649
.L1361:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$11, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2240(%rbx)
	leaq	.LC518(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1650
.L1362:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2248(%rbx)
	leaq	.LC519(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1651
.L1363:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$14, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2256(%rbx)
	leaq	.LC520(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1652
.L1364:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$6, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2264(%rbx)
	leaq	.LC521(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1653
.L1365:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2272(%rbx)
	leaq	.LC522(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1654
.L1366:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$9, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2280(%rbx)
	leaq	.LC523(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1655
.L1367:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$4, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2288(%rbx)
	leaq	.LC524(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1656
.L1368:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2296(%rbx)
	leaq	.LC525(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1657
.L1369:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$17, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2304(%rbx)
	leaq	.LC526(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1658
.L1370:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2312(%rbx)
	leaq	.LC527(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1659
.L1371:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$13, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2320(%rbx)
	leaq	.LC528(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1660
.L1372:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$7, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2328(%rbx)
	leaq	.LC529(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1661
.L1373:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movl	$18, %ecx
	movl	$1, %edx
	movq	2352(%rbx), %rdi
	movq	%rax, 2336(%rbx)
	leaq	.LC530(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1662
.L1374:
	movq	2352(%rbx), %rdi
	call	_ZN2v82V810EternalizeEPNS_7IsolateEPNS_5ValueE@PLT
	movq	%r12, %rdi
	movq	%rax, 2344(%rbx)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1663
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1377:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1090
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1380:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1092
	.p2align 4,,10
	.p2align 3
.L1381:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1093
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1383:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1096
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1097
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1387:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1388:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1389:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1390:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1102
	.p2align 4,,10
	.p2align 3
.L1391:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1103
	.p2align 4,,10
	.p2align 3
.L1392:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1393:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1394:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1106
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1396:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1108
	.p2align 4,,10
	.p2align 3
.L1397:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1399:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1113
	.p2align 4,,10
	.p2align 3
.L1402:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1410:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1411:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1123
	.p2align 4,,10
	.p2align 3
.L1412:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1124
	.p2align 4,,10
	.p2align 3
.L1413:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1126
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1127
	.p2align 4,,10
	.p2align 3
.L1416:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1417:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1130
	.p2align 4,,10
	.p2align 3
.L1419:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1131
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1132
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1133
	.p2align 4,,10
	.p2align 3
.L1422:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1423:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1424:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1136
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1137
	.p2align 4,,10
	.p2align 3
.L1426:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1427:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1429:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1430:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1142
	.p2align 4,,10
	.p2align 3
.L1431:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1432:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1434:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1146
	.p2align 4,,10
	.p2align 3
.L1435:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1436:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1148
	.p2align 4,,10
	.p2align 3
.L1437:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1149
	.p2align 4,,10
	.p2align 3
.L1438:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1152
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1442:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1443:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1155
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1156
	.p2align 4,,10
	.p2align 3
.L1445:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1446:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1158
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1159
	.p2align 4,,10
	.p2align 3
.L1448:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1161
	.p2align 4,,10
	.p2align 3
.L1450:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1162
	.p2align 4,,10
	.p2align 3
.L1451:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1163
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1164
	.p2align 4,,10
	.p2align 3
.L1453:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1165
	.p2align 4,,10
	.p2align 3
.L1454:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1167
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1168
	.p2align 4,,10
	.p2align 3
.L1457:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1169
	.p2align 4,,10
	.p2align 3
.L1458:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1459:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1171
	.p2align 4,,10
	.p2align 3
.L1460:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1461:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1173
	.p2align 4,,10
	.p2align 3
.L1462:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1175
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1465:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1177
	.p2align 4,,10
	.p2align 3
.L1466:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1179
	.p2align 4,,10
	.p2align 3
.L1468:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1180
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1181
	.p2align 4,,10
	.p2align 3
.L1470:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1182
	.p2align 4,,10
	.p2align 3
.L1471:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1472:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1184
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1185
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1475:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1189
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1479:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1191
	.p2align 4,,10
	.p2align 3
.L1480:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1192
	.p2align 4,,10
	.p2align 3
.L1481:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1193
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1194
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1195
	.p2align 4,,10
	.p2align 3
.L1484:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1196
	.p2align 4,,10
	.p2align 3
.L1485:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1197
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1200
	.p2align 4,,10
	.p2align 3
.L1489:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1201
	.p2align 4,,10
	.p2align 3
.L1490:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1202
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1492:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1204
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1206
	.p2align 4,,10
	.p2align 3
.L1495:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1207
	.p2align 4,,10
	.p2align 3
.L1496:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1209
	.p2align 4,,10
	.p2align 3
.L1498:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1499:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1501:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1213
	.p2align 4,,10
	.p2align 3
.L1502:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1214
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1504:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1505:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1506:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1220
	.p2align 4,,10
	.p2align 3
.L1509:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1510:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1511:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1512:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1513:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1225
	.p2align 4,,10
	.p2align 3
.L1514:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1515:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1228
	.p2align 4,,10
	.p2align 3
.L1517:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1229
	.p2align 4,,10
	.p2align 3
.L1518:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1519:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1231
	.p2align 4,,10
	.p2align 3
.L1520:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1232
	.p2align 4,,10
	.p2align 3
.L1521:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1522:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1523:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1524:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1525:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1237
	.p2align 4,,10
	.p2align 3
.L1526:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1238
	.p2align 4,,10
	.p2align 3
.L1527:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1239
	.p2align 4,,10
	.p2align 3
.L1528:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1529:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1241
	.p2align 4,,10
	.p2align 3
.L1530:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1242
	.p2align 4,,10
	.p2align 3
.L1531:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1243
	.p2align 4,,10
	.p2align 3
.L1532:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1244
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1246
	.p2align 4,,10
	.p2align 3
.L1535:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1247
	.p2align 4,,10
	.p2align 3
.L1536:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1537:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1538:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1250
	.p2align 4,,10
	.p2align 3
.L1539:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1251
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1255
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1545:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1257
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1258
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1259
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1260
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1261
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1262
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1264
	.p2align 4,,10
	.p2align 3
.L1553:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1554:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1555:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1267
	.p2align 4,,10
	.p2align 3
.L1556:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1557:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1269
	.p2align 4,,10
	.p2align 3
.L1558:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1270
	.p2align 4,,10
	.p2align 3
.L1559:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1271
	.p2align 4,,10
	.p2align 3
.L1560:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1272
	.p2align 4,,10
	.p2align 3
.L1561:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1562:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1274
	.p2align 4,,10
	.p2align 3
.L1563:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1564:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1565:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1566:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1278
	.p2align 4,,10
	.p2align 3
.L1567:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1568:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1569:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1570:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1282
	.p2align 4,,10
	.p2align 3
.L1571:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1572:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1573:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1285
	.p2align 4,,10
	.p2align 3
.L1574:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1575:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1576:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1578:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1290
	.p2align 4,,10
	.p2align 3
.L1579:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1291
	.p2align 4,,10
	.p2align 3
.L1580:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1292
	.p2align 4,,10
	.p2align 3
.L1581:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1582:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1294
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1295
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1585:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1586:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1298
	.p2align 4,,10
	.p2align 3
.L1587:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1299
	.p2align 4,,10
	.p2align 3
.L1588:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1300
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1301
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1593:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1594:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1596:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1598:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1599:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1311
	.p2align 4,,10
	.p2align 3
.L1600:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1312
	.p2align 4,,10
	.p2align 3
.L1601:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1313
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1603:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1315
	.p2align 4,,10
	.p2align 3
.L1604:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1605:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1606:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1607:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1319
	.p2align 4,,10
	.p2align 3
.L1608:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1320
	.p2align 4,,10
	.p2align 3
.L1609:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1321
	.p2align 4,,10
	.p2align 3
.L1610:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1611:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1612:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1324
	.p2align 4,,10
	.p2align 3
.L1613:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1614:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1615:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1327
	.p2align 4,,10
	.p2align 3
.L1616:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1617:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1618:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1330
	.p2align 4,,10
	.p2align 3
.L1619:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1331
	.p2align 4,,10
	.p2align 3
.L1620:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1332
	.p2align 4,,10
	.p2align 3
.L1621:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1622:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1334
	.p2align 4,,10
	.p2align 3
.L1623:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1624:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1336
	.p2align 4,,10
	.p2align 3
.L1625:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1337
	.p2align 4,,10
	.p2align 3
.L1626:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1338
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1339
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1341
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1342
	.p2align 4,,10
	.p2align 3
.L1631:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1343
	.p2align 4,,10
	.p2align 3
.L1632:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1633:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1345
	.p2align 4,,10
	.p2align 3
.L1634:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1635:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1636:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1637:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1349
	.p2align 4,,10
	.p2align 3
.L1638:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1639:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1354
	.p2align 4,,10
	.p2align 3
.L1643:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1356
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1357
	.p2align 4,,10
	.p2align 3
.L1646:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1647:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1359
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1649:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1361
	.p2align 4,,10
	.p2align 3
.L1650:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1362
	.p2align 4,,10
	.p2align 3
.L1651:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1364
	.p2align 4,,10
	.p2align 3
.L1653:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1365
	.p2align 4,,10
	.p2align 3
.L1654:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1655:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1656:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1368
	.p2align 4,,10
	.p2align 3
.L1657:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1369
	.p2align 4,,10
	.p2align 3
.L1658:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1370
	.p2align 4,,10
	.p2align 3
.L1659:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1660:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1372
	.p2align 4,,10
	.p2align 3
.L1661:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1373
	.p2align 4,,10
	.p2align 3
.L1662:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L1374
.L1663:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7956:
	.size	_ZN4node11IsolateData16CreatePropertiesEv, .-_ZN4node11IsolateData16CreatePropertiesEv
	.section	.rodata.str1.8
	.align 8
.LC532:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11IsolateDataC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE
	.type	_ZN4node11IsolateDataC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE, @function
_ZN4node11IsolateDataC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE:
.LFB7988:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rdx, %xmm1
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	leaq	2344(%r12), %rdx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node11IsolateDataE(%rip), %rax
	movups	%xmm0, 56(%r12)
	movq	%rax, (%r12)
	leaq	56(%r12), %rax
	movq	%rax, 8(%r12)
	leaq	1976(%r12), %rax
	movq	$1, 16(%r12)
	movq	$0, 24(%r12)
	movq	$0, 32(%r12)
	movl	$0x3f800000, 40(%r12)
	movq	$0, 48(%r12)
	movups	%xmm0, 72(%r12)
	movups	%xmm0, 88(%r12)
	movups	%xmm0, 104(%r12)
	movups	%xmm0, 120(%r12)
	movups	%xmm0, 136(%r12)
	movups	%xmm0, 152(%r12)
	movups	%xmm0, 168(%r12)
	movups	%xmm0, 184(%r12)
	movups	%xmm0, 200(%r12)
	movups	%xmm0, 216(%r12)
	movups	%xmm0, 232(%r12)
	movups	%xmm0, 248(%r12)
	movups	%xmm0, 264(%r12)
	movups	%xmm0, 280(%r12)
	movups	%xmm0, 296(%r12)
	movups	%xmm0, 312(%r12)
	movups	%xmm0, 328(%r12)
	movups	%xmm0, 344(%r12)
	movups	%xmm0, 360(%r12)
	movups	%xmm0, 376(%r12)
	movups	%xmm0, 392(%r12)
	movups	%xmm0, 408(%r12)
	movups	%xmm0, 424(%r12)
	movups	%xmm0, 440(%r12)
	movups	%xmm0, 456(%r12)
	movups	%xmm0, 472(%r12)
	movups	%xmm0, 488(%r12)
	movups	%xmm0, 504(%r12)
	movups	%xmm0, 520(%r12)
	movups	%xmm0, 536(%r12)
	movups	%xmm0, 552(%r12)
	movups	%xmm0, 568(%r12)
	movups	%xmm0, 584(%r12)
	movups	%xmm0, 600(%r12)
	movups	%xmm0, 616(%r12)
	movups	%xmm0, 632(%r12)
	movups	%xmm0, 648(%r12)
	movups	%xmm0, 664(%r12)
	movups	%xmm0, 680(%r12)
	movups	%xmm0, 696(%r12)
	movups	%xmm0, 712(%r12)
	movups	%xmm0, 728(%r12)
	movups	%xmm0, 744(%r12)
	movups	%xmm0, 760(%r12)
	movups	%xmm0, 776(%r12)
	movups	%xmm0, 792(%r12)
	movups	%xmm0, 808(%r12)
	movups	%xmm0, 824(%r12)
	movups	%xmm0, 840(%r12)
	movups	%xmm0, 856(%r12)
	movups	%xmm0, 872(%r12)
	movups	%xmm0, 888(%r12)
	movups	%xmm0, 904(%r12)
	movups	%xmm0, 920(%r12)
	movups	%xmm0, 936(%r12)
	movups	%xmm0, 952(%r12)
	movups	%xmm0, 968(%r12)
	movups	%xmm0, 984(%r12)
	movups	%xmm0, 1000(%r12)
	movups	%xmm0, 1016(%r12)
	movups	%xmm0, 1032(%r12)
	movups	%xmm0, 1048(%r12)
	movups	%xmm0, 1064(%r12)
	movups	%xmm0, 1080(%r12)
	movups	%xmm0, 1096(%r12)
	movups	%xmm0, 1112(%r12)
	movups	%xmm0, 1128(%r12)
	movups	%xmm0, 1144(%r12)
	movups	%xmm0, 1160(%r12)
	movups	%xmm0, 1176(%r12)
	movups	%xmm0, 1192(%r12)
	movups	%xmm0, 1208(%r12)
	movups	%xmm0, 1224(%r12)
	movups	%xmm0, 1240(%r12)
	movups	%xmm0, 1256(%r12)
	movups	%xmm0, 1272(%r12)
	movups	%xmm0, 1288(%r12)
	movups	%xmm0, 1304(%r12)
	movups	%xmm0, 1320(%r12)
	movups	%xmm0, 1336(%r12)
	movups	%xmm0, 1352(%r12)
	movups	%xmm0, 1368(%r12)
	movups	%xmm0, 1384(%r12)
	movups	%xmm0, 1400(%r12)
	movups	%xmm0, 1416(%r12)
	movups	%xmm0, 1432(%r12)
	movups	%xmm0, 1448(%r12)
	movups	%xmm0, 1464(%r12)
	movups	%xmm0, 1480(%r12)
	movups	%xmm0, 1496(%r12)
	movups	%xmm0, 1512(%r12)
	movups	%xmm0, 1528(%r12)
	movups	%xmm0, 1544(%r12)
	movups	%xmm0, 1560(%r12)
	movups	%xmm0, 1576(%r12)
	movups	%xmm0, 1592(%r12)
	movups	%xmm0, 1608(%r12)
	movups	%xmm0, 1624(%r12)
	movups	%xmm0, 1640(%r12)
	movups	%xmm0, 1656(%r12)
	movups	%xmm0, 1672(%r12)
	movups	%xmm0, 1688(%r12)
	movups	%xmm0, 1704(%r12)
	movups	%xmm0, 1720(%r12)
	movups	%xmm0, 1736(%r12)
	movups	%xmm0, 1752(%r12)
	movups	%xmm0, 1768(%r12)
	movups	%xmm0, 1784(%r12)
	movups	%xmm0, 1800(%r12)
	movups	%xmm0, 1816(%r12)
	movups	%xmm0, 1832(%r12)
	movups	%xmm0, 1848(%r12)
	movups	%xmm0, 1864(%r12)
	movups	%xmm0, 1880(%r12)
	movups	%xmm0, 1896(%r12)
	movups	%xmm0, 1912(%r12)
	movups	%xmm0, 1928(%r12)
	movups	%xmm0, 1944(%r12)
	movups	%xmm0, 1960(%r12)
	.p2align 4,,10
	.p2align 3
.L1665:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1665
	movq	%rdi, %xmm0
	movq	$0, 2344(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 2352(%r12)
	call	_ZN2v87Isolate23GetArrayBufferAllocatorEv@PLT
	movq	%rax, 2368(%r12)
	testq	%r13, %r13
	je	.L1666
	movq	0(%r13), %rdx
	leaq	_ZN4node24NodeArrayBufferAllocator7GetImplEv(%rip), %rcx
	movq	40(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L1702
	.p2align 4,,10
	.p2align 3
.L1666:
	cmpq	%rax, %r13
	movq	%r13, 2376(%r12)
	movq	%rbx, 2392(%r12)
	movq	$0, 2400(%r12)
	movq	$0, 2408(%r12)
	sete	2384(%r12)
	testq	%rax, %rax
	je	.L1703
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	movl	$64, %edi
	movq	8(%rax), %r13
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN4node17PerIsolateOptionsE(%rip), %rax
	movq	%rax, (%rbx)
	movq	8(%r13), %rax
	movq	%rax, 8(%rbx)
	movq	16(%r13), %rax
	movq	%rax, 16(%rbx)
	testq	%rax, %rax
	je	.L1668
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1669
	lock addl	$1, 8(%rax)
.L1668:
	leaq	48(%rbx), %rdi
	movl	24(%r13), %eax
	movq	%rdi, 32(%rbx)
	movq	32(%r13), %r15
	movq	40(%r13), %r13
	movl	%eax, 24(%rbx)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L1670
	testq	%r15, %r15
	je	.L1704
.L1670:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L1705
	cmpq	$1, %r13
	jne	.L1673
	movzbl	(%r15), %eax
	movb	%al, 48(%rbx)
.L1674:
	movq	%r13, 40(%rbx)
	movb	$0, (%rdi,%r13)
	movl	$24, %edi
	call	_Znwm@PLT
	movq	2408(%r12), %r13
	movq	%rbx, %xmm0
	movabsq	$4294967297, %rcx
	movq	%rax, %xmm2
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rsi
	movq	%rcx, 8(%rax)
	punpcklqdq	%xmm2, %xmm0
	movq	%rsi, (%rax)
	movq	%rbx, 16(%rax)
	movups	%xmm0, 2400(%r12)
	testq	%r13, %r13
	je	.L1676
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1677
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L1706
	.p2align 4,,10
	.p2align 3
.L1676:
	testq	%r14, %r14
	je	.L1707
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node11IsolateData21DeserializePropertiesEPKSt6vectorImSaImEE
.L1664:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1708
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1673:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L1674
	jmp	.L1672
	.p2align 4,,10
	.p2align 3
.L1669:
	addl	$1, 8(%rax)
	jmp	.L1668
.L1705:
	leaq	32(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rbx)
.L1672:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	32(%rbx), %rdi
	jmp	.L1674
.L1707:
	movq	%r12, %rdi
	call	_ZN4node11IsolateData16CreatePropertiesEv
	jmp	.L1664
.L1677:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1676
.L1706:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1680
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1681:
	cmpl	$1, %eax
	jne	.L1676
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1676
.L1702:
	movq	%r13, %rdi
	call	*%rdx
	movq	%rax, %r13
	movq	2368(%r12), %rax
	jmp	.L1666
.L1703:
	leaq	_ZZN4node11IsolateDataC4EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1680:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1681
.L1704:
	leaq	.LC532(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1708:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7988:
	.size	_ZN4node11IsolateDataC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE, .-_ZN4node11IsolateDataC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE
	.globl	_ZN4node11IsolateDataC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE
	.set	_ZN4node11IsolateDataC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE,_ZN4node11IsolateDataC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE
	.align 2
	.p2align 4
	.globl	_ZN4node26TrackingTraceStateObserver24UpdateTraceCategoryStateEv
	.type	_ZN4node26TrackingTraceStateObserver24UpdateTraceCategoryStateEv, @function
_ZN4node26TrackingTraceStateObserver24UpdateTraceCategoryStateEv:
.LFB7992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	testb	$2, 1932(%rax)
	jne	.L1735
.L1709:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1736
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1735:
	.cfi_restore_state
	movzbl	1930(%rax), %edx
	testb	%dl, %dl
	je	.L1709
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L1709
	movq	%rdi, %rbx
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1711
	movq	(%rax), %rax
	leaq	.LC4(%rip), %rsi
	leaq	-160(%rbp), %r13
	call	*16(%rax)
	movq	%r13, %rdi
	movzbl	(%rax), %r14d
	movq	8(%rbx), %rax
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rax
	movq	3032(%rax), %r9
	testq	%r9, %r9
	je	.L1734
	movq	352(%rax), %rsi
	leaq	-128(%rbp), %r15
	movq	%r9, -176(%rbp)
	movq	%r15, %rdi
	movq	%rax, -168(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-168(%rbp), %rax
	movq	%r15, %rdi
	movl	$1, %esi
	movl	$0, -72(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	testb	%r14b, %r14b
	leaq	112(%r12), %rax
	movq	-176(%rbp), %r9
	je	.L1714
.L1715:
	movq	%rax, -64(%rbp)
	movq	8(%rbx), %rax
	movq	%r9, %rdi
	leaq	88(%r12), %rdx
	leaq	-64(%rbp), %r8
	movl	$1, %ecx
	movq	3280(%rax), %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r15, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
.L1734:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1709
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	8(%rbx), %rax
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %r14
	movq	3032(%r14), %r9
	testq	%r9, %r9
	je	.L1734
	movq	352(%r14), %rsi
	leaq	-128(%rbp), %r15
	movq	%r9, -168(%rbp)
	movq	%r15, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r14, -80(%rbp)
	movl	$0, -72(%rbp)
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	movq	-168(%rbp), %r9
.L1714:
	leaq	120(%r12), %rax
	jmp	.L1715
.L1736:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7992:
	.size	_ZN4node26TrackingTraceStateObserver24UpdateTraceCategoryStateEv, .-_ZN4node26TrackingTraceStateObserver24UpdateTraceCategoryStateEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment16AllocateThreadIdEv
	.type	_ZN4node11Environment16AllocateThreadIdEv, @function
_ZN4node11Environment16AllocateThreadIdEv:
.LFB7994:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	lock xaddq	%rax, _ZN4nodeL14next_thread_idE(%rip)
	ret
	.cfi_endproc
.LFE7994:
	.size	_ZN4node11Environment16AllocateThreadIdEv, .-_ZN4node11Environment16AllocateThreadIdEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment16CreatePropertiesEv
	.type	_ZN4node11Environment16CreatePropertiesEv, @function
_ZN4node11Environment16CreatePropertiesEv:
.LFB7995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	352(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	movq	352(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	3280(%rbx), %r15
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdx
	popq	%rcx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1788
.L1739:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1789
	movq	%rax, %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2680(%rbx), %rdi
	movq	352(%rbx), %r8
	testq	%rdi, %rdi
	je	.L1756
	movq	%r8, -88(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-88(%rbp), %r8
	movq	$0, 2680(%rbx)
.L1756:
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2680(%rbx)
.L1741:
	movq	3056(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L1743
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3056(%rbx)
.L1743:
	testq	%r12, %r12
	je	.L1744
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3056(%rbx)
.L1744:
	movq	%r15, %rdi
	call	_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1790
.L1745:
	movq	360(%rbx), %rax
	movq	%r15, %rsi
	movq	1400(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1791
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1792
.L1747:
	movq	2976(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L1748
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2976(%rbx)
.L1748:
	testq	%r12, %r12
	je	.L1749
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2976(%rbx)
.L1749:
	movq	%rbx, %rdi
	call	_ZN4node19CreateProcessObjectEPNS_11EnvironmentE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1793
	movq	2968(%rbx), %rdi
	movq	352(%rbx), %r14
	testq	%rdi, %rdi
	je	.L1753
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2968(%rbx)
.L1753:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2968(%rbx)
.L1752:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1794
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1793:
	.cfi_restore_state
	movq	2968(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1752
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2968(%rbx)
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1788:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L1739
	.p2align 4,,10
	.p2align 3
.L1789:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	xorl	%edi, %edi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	2680(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1741
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2680(%rbx)
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1790:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L1745
	.p2align 4,,10
	.p2align 3
.L1791:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1747
	.p2align 4,,10
	.p2align 3
.L1792:
	leaq	_ZZN4node11Environment16CreatePropertiesEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1794:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7995:
	.size	_ZN4node11Environment16CreatePropertiesEv, .-_ZN4node11Environment16CreatePropertiesEv
	.p2align 4
	.globl	_ZN4node11GetExecPathERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EE
	.type	_ZN4node11GetExecPathERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EE, @function
_ZN4node11GetExecPathERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EE:
.LFB7996:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %r14
	movb	$0, 16(%rdi)
	movq	%rdi, %r12
	movq	%r14, (%rdi)
	leaq	-8256(%rbp), %r15
	movq	%rsi, %rbx
	leaq	-8304(%rbp), %rsi
	movq	$0, 8(%rdi)
	movq	%r15, %rdi
	movq	$8192, -8304(%rbp)
	call	uv_exepath@PLT
	testl	%eax, %eax
	jne	.L1796
	movq	-8304(%rbp), %r13
	leaq	-8272(%rbp), %rbx
	leaq	-8288(%rbp), %rdi
	movq	%rbx, -8288(%rbp)
	movq	%r13, -8296(%rbp)
	cmpq	$15, %r13
	ja	.L1818
	cmpq	$1, %r13
	jne	.L1799
	movzbl	-8256(%rbp), %eax
	movb	%al, -8272(%rbp)
	movq	%rbx, %rax
.L1800:
	movq	%r13, -8280(%rbp)
	movb	$0, (%rax,%r13)
	movq	-8288(%rbp), %rdx
	movq	(%r12), %rdi
	cmpq	%rbx, %rdx
	je	.L1819
	movq	-8280(%rbp), %rax
	movq	-8272(%rbp), %rcx
	cmpq	%rdi, %r14
	je	.L1820
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	16(%r12), %rsi
	movq	%rdx, (%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	testq	%rdi, %rdi
	je	.L1806
	movq	%rdi, -8288(%rbp)
	movq	%rsi, -8272(%rbp)
.L1804:
	movq	$0, -8280(%rbp)
	movb	$0, (%rdi)
	movq	-8288(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1795
	call	_ZdlPv@PLT
	jmp	.L1795
	.p2align 4,,10
	.p2align 3
.L1796:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
.L1795:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1821
	addq	$8264, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1799:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L1822
	movq	%rbx, %rax
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1818:
	leaq	-8296(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -8288(%rbp)
	movq	%rax, %rdi
	movq	-8296(%rbp), %rax
	movq	%rax, -8272(%rbp)
.L1798:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-8296(%rbp), %r13
	movq	-8288(%rbp), %rax
	jmp	.L1800
	.p2align 4,,10
	.p2align 3
.L1819:
	movq	-8280(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1802
	cmpq	$1, %rdx
	je	.L1823
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-8280(%rbp), %rdx
	movq	(%r12), %rdi
.L1802:
	movq	%rdx, 8(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-8288(%rbp), %rdi
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1820:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, (%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r12)
.L1806:
	movq	%rbx, -8288(%rbp)
	leaq	-8272(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L1804
	.p2align 4,,10
	.p2align 3
.L1823:
	movzbl	-8272(%rbp), %eax
	movb	%al, (%rdi)
	movq	-8280(%rbp), %rdx
	movq	(%r12), %rdi
	jmp	.L1802
.L1821:
	call	__stack_chk_fail@PLT
.L1822:
	movq	%rbx, %rdi
	jmp	.L1798
	.cfi_endproc
.LFE7996:
	.size	_ZN4node11GetExecPathERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EE, .-_ZN4node11GetExecPathERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EE
	.section	.rodata.str1.1
.LC544:
	.string	"exec_args"
.LC545:
	.string	"Environment"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11EnvironmentC2EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm
	.type	_ZN4node11EnvironmentC2EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm, @function
_ZN4node11EnvironmentC2EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm:
.LFB8278:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-224(%rbp), %rbx
	leaq	1184(%r12), %r13
	subq	$344, %rsp
	movq	%rdx, -328(%rbp)
	movq	%rcx, -344(%rbp)
	movq	%r8, -352(%rbp)
	movl	%r9d, -360(%rbp)
	movq	%rsi, -336(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node11EnvironmentE(%rip), %rax
	movl	$0, 16(%rdi)
	movq	%rax, (%rdi)
	leaq	16(%rdi), %rax
	movq	%rax, 32(%rdi)
	movq	%rax, 40(%rdi)
	leaq	64(%rdi), %rax
	movq	%rax, 80(%rdi)
	movq	%rax, 88(%rdi)
	leaq	152(%rdi), %rax
	movq	%rax, 104(%rdi)
	leaq	208(%rdi), %rax
	movq	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movl	$0, 64(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 96(%rdi)
	movq	$1, 112(%rdi)
	movq	$0, 120(%rdi)
	movq	$0, 128(%rdi)
	movl	$0x3f800000, 136(%rdi)
	movq	$0, 144(%rdi)
	movq	$0, 152(%rdi)
	movq	%rax, 160(%rdi)
	leaq	264(%rdi), %rax
	movq	%rax, 216(%rdi)
	leaq	320(%rdi), %rax
	movq	%rax, 272(%rdi)
	leaq	328(%rdi), %rax
	movq	$1, 168(%rdi)
	movq	$0, 176(%rdi)
	movq	$0, 184(%rdi)
	movl	$0x3f800000, 192(%rdi)
	movq	$0, 200(%rdi)
	movq	$0, 208(%rdi)
	movq	$1, 224(%rdi)
	movq	$0, 232(%rdi)
	movq	$0, 240(%rdi)
	movl	$0x3f800000, 248(%rdi)
	movq	$0, 256(%rdi)
	movq	$0, 264(%rdi)
	movq	$1, 280(%rdi)
	movq	$0, 288(%rdi)
	movq	$0, 296(%rdi)
	movl	$0x3f800000, 304(%rdi)
	movq	$0, 312(%rdi)
	movq	$0, 320(%rdi)
	movq	%rax, 336(%rdi)
	movq	%rax, 328(%rdi)
	movq	$0, 344(%rdi)
	movq	%rdx, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	$0, 1128(%r12)
	movb	$0, 1136(%r12)
	movq	%rax, %xmm0
	leaq	16+_ZTVN4node10AsyncHooksE(%rip), %rax
	movhps	-336(%rbp), %xmm0
	movq	%rax, 1144(%r12)
	leaq	-256(%rbp), %rax
	movq	%xmm0, %r15
	movq	%rax, %rdi
	movups	%xmm0, 352(%r12)
	movq	%xmm0, 1152(%r12)
	movdqa	.LC533(%rip), %xmm0
	movq	%r15, %rsi
	movq	$0, 1184(%r12)
	movups	%xmm0, 1160(%r12)
	movq	%rax, -312(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1152(%r12), %rdi
	movl	$256, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$32, %edx
	movq	%r14, %rdi
	movq	1168(%r12), %rsi
	movq	%rax, 1176(%r12)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	testq	%rax, %rax
	je	.L2497
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1828
	movq	1184(%r12), %r8
	testq	%r8, %r8
	je	.L1829
.L1827:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 1184(%r12)
.L1829:
	testq	%rdi, %rdi
	je	.L2123
	movq	%rdi, 1184(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L1828:
	testq	%rax, %rax
	je	.L2123
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2123:
	movq	-312(%rbp), %r15
	leaq	1224(%r12), %r13
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	352(%r12), %r14
	movdqa	.LC534(%rip), %xmm0
	movq	%r15, %rdi
	movq	$0, 1224(%r12)
	movq	%r14, 1192(%r12)
	movq	%r14, %rsi
	movups	%xmm0, 1200(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1192(%r12), %rdi
	movl	$32, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$8, %edx
	movq	%r15, %rdi
	movq	1208(%r12), %rsi
	movq	%rax, 1216(%r12)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2498
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1833
	movq	1224(%r12), %r8
	testq	%r8, %r8
	je	.L1834
.L1832:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 1224(%r12)
.L1834:
	testq	%rdi, %rdi
	je	.L2122
	movq	%rdi, 1224(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2122:
	movq	-312(%rbp), %r15
	leaq	1264(%r12), %r13
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	352(%r12), %r14
	movdqa	.LC535(%rip), %xmm4
	movq	%r15, %rdi
	movq	$0, 1264(%r12)
	movq	%r14, 1232(%r12)
	movq	%r14, %rsi
	movups	%xmm4, 1240(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1232(%r12), %rdi
	movl	$32, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$4, %edx
	movq	%r15, %rdi
	movq	1248(%r12), %rsi
	movq	%rax, 1256(%r12)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2499
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1838
	movq	1264(%r12), %r8
	testq	%r8, %r8
	je	.L1839
.L1837:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 1264(%r12)
.L1839:
	testq	%rdi, %rdi
	je	.L2121
	movq	%rdi, 1264(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2121:
	movq	-312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	352(%r12), %r15
	movq	%rbx, %rdi
	movq	$0, 1272(%r12)
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%r12), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L1840
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 1272(%r12)
.L1840:
	testq	%r13, %r13
	je	.L1841
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%r12)
.L1841:
	movq	1256(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%rbx, %rdi
	leaq	1320(%r12), %r13
	movups	%xmm0, (%rax)
	movq	1216(%r12), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	1216(%r12), %rax
	movapd	.LC536(%rip), %xmm0
	movq	-328(%rbp), %rdi
	movl	$1, 24(%rax)
	movq	1256(%r12), %rax
	movups	%xmm0, 16(%rax)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movdqa	.LC537(%rip), %xmm0
	movq	$0, 1320(%r12)
	movq	%rax, %r15
	leaq	16+_ZTVN4node13ImmediateInfoE(%rip), %rax
	movq	-312(%rbp), %rdi
	movq	%rax, 1280(%r12)
	movq	%r15, %rsi
	movq	%r15, 1288(%r12)
	movups	%xmm0, 1296(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1288(%r12), %rdi
	movl	$12, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$3, %edx
	movq	%r14, %rdi
	movq	1304(%r12), %rsi
	movq	%rax, 1312(%r12)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2500
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1845
	movq	1320(%r12), %r8
	testq	%r8, %r8
	je	.L1846
.L1844:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 1320(%r12)
.L1846:
	testq	%rdi, %rdi
	je	.L2120
	movq	%rdi, 1320(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2120:
	movq	-312(%rbp), %r14
	leaq	1368(%r12), %r13
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-328(%rbp), %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movdqa	.LC538(%rip), %xmm0
	movq	%r14, %rdi
	movq	$0, 1368(%r12)
	movq	%rax, %r15
	leaq	16+_ZTVN4node8TickInfoE(%rip), %rax
	movq	%rax, 1328(%r12)
	movq	%r15, %rsi
	movq	%r15, 1336(%r12)
	movups	%xmm0, 1344(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1336(%r12), %rdi
	movl	$2, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$2, %edx
	movq	%r14, %rdi
	movq	1352(%r12), %rsi
	movq	%rax, 1360(%r12)
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2501
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1850
	movq	1368(%r12), %r8
	testq	%r8, %r8
	je	.L1851
.L1849:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 1368(%r12)
.L1851:
	testq	%rdi, %rdi
	je	.L2119
	movq	%rdi, 1368(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2119:
	movq	-312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-336(%rbp), %rax
	movq	2360(%rax), %rdi
	call	uv_now@PLT
	pxor	%xmm0, %xmm0
	movb	$0, 1472(%r12)
	movq	%rax, 1376(%r12)
	leaq	1472(%r12), %rax
	movq	%rax, 1456(%r12)
	leaq	1504(%r12), %rax
	movq	%rax, 1488(%r12)
	leaq	1536(%r12), %rax
	movq	%rax, 1520(%r12)
	leaq	1584(%r12), %rax
	movq	%rax, 1568(%r12)
	leaq	1616(%r12), %rax
	movq	%rax, 1600(%r12)
	movq	-352(%rbp), %rax
	movb	$0, 1504(%r12)
	movb	$0, 1536(%r12)
	movb	$0, 1584(%r12)
	movb	$0, 1616(%r12)
	movups	%xmm0, 1384(%r12)
	movups	%xmm0, 1416(%r12)
	movups	%xmm0, 1432(%r12)
	movq	8(%rax), %r13
	subq	(%rax), %r13
	movl	$16842752, 1400(%r12)
	movq	%r13, %rax
	movq	$0, 1408(%r12)
	sarq	$5, %rax
	movq	$0, 1448(%r12)
	movq	$0, 1464(%r12)
	movq	$0, 1496(%r12)
	movq	$0, 1528(%r12)
	movq	$0, 1560(%r12)
	movq	$0, 1576(%r12)
	movq	$0, 1608(%r12)
	movq	$0, 1688(%r12)
	movups	%xmm0, 1640(%r12)
	movups	%xmm0, 1656(%r12)
	movups	%xmm0, 1672(%r12)
	je	.L2502
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1864
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
.L1853:
	movq	-352(%rbp), %rax
	movq	%r14, %xmm0
	addq	%r14, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 1688(%r12)
	movups	%xmm0, 1672(%r12)
	movq	8(%rax), %rcx
	movq	(%rax), %r8
	movq	%rcx, -320(%rbp)
	cmpq	%r8, %rcx
	je	.L1855
	movq	%r12, -368(%rbp)
	movq	%r8, %r12
	jmp	.L1861
	.p2align 4,,10
	.p2align 3
.L1857:
	cmpq	$1, %r15
	jne	.L1859
	movzbl	0(%r13), %eax
	movb	%al, 16(%r14)
.L1860:
	movq	%r15, 8(%r14)
	addq	$32, %r12
	addq	$32, %r14
	movb	$0, (%rdi,%r15)
	cmpq	%r12, -320(%rbp)
	je	.L2503
.L1861:
	leaq	16(%r14), %rdi
	movq	8(%r12), %r15
	movq	%rdi, (%r14)
	movq	(%r12), %r13
	movq	%r13, %rax
	addq	%r15, %rax
	je	.L1856
	testq	%r13, %r13
	je	.L1866
.L1856:
	movq	%r15, -224(%rbp)
	cmpq	$15, %r15
	jbe	.L1857
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, 16(%r14)
.L1858:
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-224(%rbp), %r15
	movq	(%r14), %rdi
	jmp	.L1860
	.p2align 4,,10
	.p2align 3
.L1833:
	testq	%rax, %rax
	je	.L2122
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L1859:
	testq	%r15, %r15
	je	.L1860
	jmp	.L1858
	.p2align 4,,10
	.p2align 3
.L2503:
	movq	-368(%rbp), %r12
.L1855:
	movq	-344(%rbp), %rax
	movq	%r14, 1680(%r12)
	pxor	%xmm0, %xmm0
	movq	8(%rax), %r13
	subq	(%rax), %r13
	movq	$0, 1712(%r12)
	movups	%xmm0, 1696(%r12)
	movq	%r13, %rax
	sarq	$5, %rax
	je	.L2504
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1864
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %r14
.L1863:
	movq	-344(%rbp), %rax
	movq	%r14, %xmm0
	addq	%r14, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 1712(%r12)
	movups	%xmm0, 1696(%r12)
	movq	8(%rax), %rcx
	movq	(%rax), %r8
	movq	%rcx, -320(%rbp)
	cmpq	%r8, %rcx
	je	.L1865
	movq	%r12, -368(%rbp)
	movq	%r8, %r12
	jmp	.L1872
	.p2align 4,,10
	.p2align 3
.L2507:
	movzbl	0(%r13), %eax
	movb	%al, 16(%r14)
.L1871:
	movq	%r15, 8(%r14)
	addq	$32, %r12
	addq	$32, %r14
	movb	$0, (%rdi,%r15)
	cmpq	%r12, -320(%rbp)
	je	.L2505
.L1872:
	leaq	16(%r14), %rdi
	movq	8(%r12), %r15
	movq	%rdi, (%r14)
	movq	(%r12), %r13
	movq	%r13, %rax
	addq	%r15, %rax
	je	.L2128
	testq	%r13, %r13
	je	.L1866
.L2128:
	movq	%r15, -224(%rbp)
	cmpq	$15, %r15
	ja	.L2506
	cmpq	$1, %r15
	je	.L2507
	testq	%r15, %r15
	je	.L1871
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L2506:
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, 16(%r14)
.L1869:
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-224(%rbp), %r15
	movq	(%r14), %rdi
	jmp	.L1871
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	-368(%rbp), %r12
.L1865:
	movq	%r14, 1704(%r12)
	movq	-344(%rbp), %rsi
	leaq	1720(%r12), %rdi
	leaq	1800(%r12), %r13
	call	_ZN4node11GetExecPathERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS6_EE
	movq	352(%r12), %r8
	movdqa	.LC539(%rip), %xmm0
	movq	$0, 1752(%r12)
	movl	$0, 1760(%r12)
	movq	-312(%rbp), %rdi
	movq	%r8, 1768(%r12)
	movq	%r8, %rsi
	movq	$0, 1800(%r12)
	movups	%xmm0, 1776(%r12)
	movq	%r8, -320(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1768(%r12), %rdi
	movl	$4, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$1, %edx
	movq	%r15, %rdi
	movq	1784(%r12), %rsi
	movq	%rax, 1792(%r12)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-320(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L2508
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1876
	movq	1800(%r12), %r8
	testq	%r8, %r8
	je	.L1877
.L1875:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 1800(%r12)
.L1877:
	testq	%rdi, %rdi
	je	.L2118
	movq	%rdi, 1800(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2118:
	movq	-312(%rbp), %r14
	leaq	1856(%r12), %r13
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	352(%r12), %r8
	movdqa	.LC535(%rip), %xmm5
	movq	%r14, %rdi
	movl	$0, 1808(%r12)
	movq	%r8, 1824(%r12)
	movq	%r8, %rsi
	movq	$0, 1816(%r12)
	movq	$0, 1856(%r12)
	movups	%xmm5, 1832(%r12)
	movq	%r8, -320(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1824(%r12), %rdi
	movl	$16, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r15
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$4, %edx
	movq	%r15, %rdi
	movq	1840(%r12), %rsi
	movq	%rax, 1848(%r12)
	call	_ZN2v810Int32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	-320(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L2509
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1881
	movq	1856(%r12), %r8
	testq	%r8, %r8
	je	.L1882
.L1880:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 1856(%r12)
.L1882:
	testq	%rdi, %rdi
	je	.L2117
	movq	%rdi, 1856(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2117:
	movq	-312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	1920(%r12), %rax
	movq	$0, 1864(%r12)
	movq	%rax, 1872(%r12)
	xorl	%eax, %eax
	cmpq	$-1, 16(%rbp)
	movw	%ax, 1928(%r12)
	movl	-360(%rbp), %eax
	movq	$1, 1880(%r12)
	movq	$0, 1888(%r12)
	movq	$0, 1896(%r12)
	movl	$0x3f800000, 1904(%r12)
	movq	$0, 1912(%r12)
	movq	$0, 1920(%r12)
	movb	$1, 1930(%r12)
	movl	%eax, 1932(%r12)
	je	.L2510
.L1883:
	movq	16(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	$1, 1952(%r12)
	movl	$64, %edi
	movq	$0, 1960(%r12)
	movq	%rax, 1936(%r12)
	leaq	1992(%r12), %rax
	movq	%rax, 1944(%r12)
	movq	$0, 1968(%r12)
	movl	$0x3f800000, 1976(%r12)
	movq	$0, 1984(%r12)
	movq	$8, 2008(%r12)
	movups	%xmm0, 1992(%r12)
	movups	%xmm0, 2016(%r12)
	movups	%xmm0, 2032(%r12)
	movups	%xmm0, 2048(%r12)
	movups	%xmm0, 2064(%r12)
	call	_Znwm@PLT
	movq	2008(%r12), %rdx
	movl	$512, %edi
	movq	%rax, 2000(%r12)
	leaq	-4(,%rdx,4), %r13
	andq	$-8, %r13
	addq	%rax, %r13
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	352(%r12), %r15
	movdqa	.LC540(%rip), %xmm6
	movq	%rax, %xmm1
	leaq	512(%rax), %rdx
	movq	%rax, 0(%r13)
	movq	-312(%rbp), %rdi
	movq	%rax, 2056(%r12)
	punpcklqdq	%xmm1, %xmm1
	movq	%r15, %rsi
	movq	%rax, 2048(%r12)
	leaq	2128(%r12), %rax
	movups	%xmm1, 2016(%r12)
	movq	%rax, %xmm1
	leaq	2112(%r12), %rax
	movq	%rax, %xmm2
	leaq	2096(%r12), %rax
	movq	%rdx, 2032(%r12)
	punpcklqdq	%xmm1, %xmm1
	movq	%rdx, 2064(%r12)
	movq	%rax, %xmm3
	punpcklqdq	%xmm2, %xmm2
	movq	%r13, 2040(%r12)
	punpcklqdq	%xmm3, %xmm3
	movq	%r13, 2072(%r12)
	leaq	2296(%r12), %r13
	movq	$0, 2080(%r12)
	movb	$0, 2088(%r12)
	movq	$0, 2144(%r12)
	movq	$0, 2152(%r12)
	movb	$0, 2192(%r12)
	movq	$0, 2200(%r12)
	movl	$0, 2256(%r12)
	movq	%r15, 2264(%r12)
	movq	$0, 2296(%r12)
	movups	%xmm0, 2160(%r12)
	movups	%xmm0, 2176(%r12)
	pxor	%xmm0, %xmm0
	movups	%xmm3, 2096(%r12)
	movups	%xmm2, 2112(%r12)
	movups	%xmm1, 2128(%r12)
	movups	%xmm0, 2208(%r12)
	movups	%xmm0, 2224(%r12)
	movups	%xmm0, 2240(%r12)
	movups	%xmm6, 2272(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	2264(%r12), %rdi
	movl	$288, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$36, %edx
	movq	%r14, %rdi
	movq	2280(%r12), %rsi
	movq	%rax, 2288(%r12)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2511
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1887
	movq	2296(%r12), %r8
	testq	%r8, %r8
	je	.L1888
.L1886:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 2296(%r12)
.L1888:
	testq	%rdi, %rdi
	je	.L2116
	movq	%rdi, 2296(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2116:
	movq	-312(%rbp), %r14
	leaq	2336(%r12), %r13
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	352(%r12), %r15
	movdqa	.LC540(%rip), %xmm7
	movq	%r14, %rdi
	movq	$0, 2336(%r12)
	movq	%r15, 2304(%r12)
	movq	%r15, %rsi
	movups	%xmm7, 2312(%r12)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	2304(%r12), %rdi
	movl	$288, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, %r14
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movl	$36, %edx
	movq	%r14, %rdi
	movq	2320(%r12), %rsi
	movq	%rax, 2328(%r12)
	call	_ZN2v814BigUint64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2512
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	cmpq	%rbx, %r13
	je	.L1892
	movq	2336(%r12), %r8
	testq	%r8, %r8
	je	.L1893
.L1891:
	movq	%r8, %rdi
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-224(%rbp), %rdi
	movq	$0, 2336(%r12)
.L1893:
	testq	%rdi, %rdi
	je	.L2115
	movq	%rdi, 2336(%r12)
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2115:
	movq	-312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	leaq	2376(%r12), %rax
	movq	$0, 2392(%r12)
	pxor	%xmm0, %xmm0
	movq	%rax, %xmm1
	leaq	2400(%r12), %rdi
	movups	%xmm0, 2344(%r12)
	movups	%xmm0, 2360(%r12)
	punpcklqdq	%xmm1, %xmm1
	movups	%xmm1, 2376(%r12)
	call	uv_mutex_init@PLT
	pxor	%xmm0, %xmm0
	testl	%eax, %eax
	jne	.L1895
	leaq	2440(%r12), %rax
	leaq	2488(%r12), %rdi
	movups	%xmm0, 2456(%r12)
	movups	%xmm0, 2472(%r12)
	movq	%rax, %xmm1
	punpcklqdq	%xmm1, %xmm1
	movups	%xmm1, 2440(%r12)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1895
	pxor	%xmm0, %xmm0
	leaq	2632(%r12), %rax
	movq	$0, 2528(%r12)
	movq	%rax, 2584(%r12)
	movups	%xmm0, 2536(%r12)
	movups	%xmm0, 2560(%r12)
	movups	%xmm0, 2672(%r12)
	movups	%xmm0, 2688(%r12)
	movups	%xmm0, 2704(%r12)
	movups	%xmm0, 2720(%r12)
	movups	%xmm0, 2736(%r12)
	movups	%xmm0, 2752(%r12)
	movups	%xmm0, 2768(%r12)
	movups	%xmm0, 2784(%r12)
	movups	%xmm0, 2800(%r12)
	movups	%xmm0, 2816(%r12)
	movups	%xmm0, 2832(%r12)
	movups	%xmm0, 2848(%r12)
	movups	%xmm0, 2864(%r12)
	movups	%xmm0, 2880(%r12)
	movups	%xmm0, 2896(%r12)
	movups	%xmm0, 2912(%r12)
	movq	$0, 2552(%r12)
	movq	$0, 2576(%r12)
	movq	$1, 2592(%r12)
	movq	$0, 2600(%r12)
	movq	$0, 2608(%r12)
	movl	$0x3f800000, 2616(%r12)
	movq	$0, 2624(%r12)
	movq	$0, 2632(%r12)
	movq	$0, 2640(%r12)
	movb	$0, 2648(%r12)
	movq	$0, 2656(%r12)
	movb	$0, 2664(%r12)
	movups	%xmm0, 2928(%r12)
	movq	-328(%rbp), %r14
	movups	%xmm0, 2944(%r12)
	movups	%xmm0, 2960(%r12)
	movq	%r14, %rdi
	movups	%xmm0, 2976(%r12)
	movups	%xmm0, 2992(%r12)
	movups	%xmm0, 3008(%r12)
	movups	%xmm0, 3024(%r12)
	movups	%xmm0, 3040(%r12)
	movups	%xmm0, 3056(%r12)
	movups	%xmm0, 3072(%r12)
	movups	%xmm0, 3088(%r12)
	movups	%xmm0, 3104(%r12)
	movups	%xmm0, 3120(%r12)
	movups	%xmm0, 3136(%r12)
	movups	%xmm0, 3152(%r12)
	movups	%xmm0, 3168(%r12)
	movups	%xmm0, 3184(%r12)
	movups	%xmm0, 3200(%r12)
	movups	%xmm0, 3216(%r12)
	movups	%xmm0, 3232(%r12)
	movups	%xmm0, 3248(%r12)
	movups	%xmm0, 3264(%r12)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	testq	%r14, %r14
	je	.L2125
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
.L1896:
	movq	%rax, 3280(%r12)
	movq	352(%r12), %rsi
	leaq	-288(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -384(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-328(%rbp), %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8+_ZN4node11per_process18system_environmentE(%rip), %r13
	movq	_ZN4node11per_process18system_environmentE(%rip), %rdx
	testq	%r13, %r13
	je	.L1897
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	8(%r13), %rax
	testq	%r14, %r14
	je	.L1898
	lock addl	$1, (%rax)
.L1899:
	movq	1392(%r12), %r15
	movq	%rdx, 1384(%r12)
	cmpq	%r13, %r15
	je	.L2108
	testq	%r14, %r14
	je	.L1900
	lock addl	$1, (%rax)
	movq	1392(%r12), %r15
.L1901:
	testq	%r15, %r15
	jne	.L1902
	movq	%r13, 1392(%r12)
.L2108:
	testq	%r14, %r14
	je	.L1912
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L1913:
	cmpl	$1, %eax
	je	.L2513
.L1911:
	leaq	2208(%r12), %rdi
	movq	%r12, %rsi
	call	_ZN4node16EnabledDebugList5ParseEPNS_11EnvironmentE@PLT
	movq	-336(%rbp), %rcx
	movq	2400(%rcx), %rax
	movq	2408(%rcx), %rcx
	movq	%rcx, -336(%rbp)
	testq	%rcx, %rcx
	je	.L1917
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1918
	lock addl	$1, 8(%rcx)
.L1917:
	movl	$768, %edi
	movq	8(%rax), %r13
	call	_Znwm@PLT
	movq	%rax, %r9
	leaq	16+_ZTVN4node18EnvironmentOptionsE(%rip), %rax
	movq	24(%r13), %r14
	leaq	32(%r9), %rdi
	movq	%rax, (%r9)
	movl	8(%r13), %eax
	movq	%rdi, 16(%r9)
	movq	16(%r13), %r15
	movl	%eax, 8(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2129
	testq	%r15, %r15
	je	.L1866
.L2129:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2514
	cmpq	$1, %r14
	jne	.L1922
	movzbl	(%r15), %eax
	movb	%al, 32(%r9)
.L1923:
	movq	%r14, 24(%r9)
	movb	$0, (%rdi,%r14)
	leaq	64(%r9), %rdi
	movq	56(%r13), %r14
	movq	%rdi, 48(%r9)
	movq	48(%r13), %r15
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2130
	testq	%r15, %r15
	je	.L1866
.L2130:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2515
	cmpq	$1, %r14
	jne	.L1927
	movzbl	(%r15), %eax
	movb	%al, 64(%r9)
.L1928:
	movq	%r14, 56(%r9)
	movb	$0, (%rdi,%r14)
	leaq	104(%r9), %rdi
	movzwl	80(%r13), %eax
	movq	%rdi, 88(%r9)
	movq	88(%r13), %r15
	movq	96(%r13), %r14
	movw	%ax, 80(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2131
	testq	%r15, %r15
	je	.L1866
.L2131:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2516
	cmpq	$1, %r14
	jne	.L1932
	movzbl	(%r15), %eax
	movb	%al, 104(%r9)
.L1933:
	movq	%r14, 96(%r9)
	movb	$0, (%rdi,%r14)
	leaq	136(%r9), %rdi
	movq	128(%r13), %r14
	movq	%rdi, 120(%r9)
	movq	120(%r13), %r15
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2132
	testq	%r15, %r15
	je	.L1866
.L2132:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2517
	cmpq	$1, %r14
	jne	.L1937
	movzbl	(%r15), %eax
	movb	%al, 136(%r9)
.L1938:
	movq	%r14, 128(%r9)
	movb	$0, (%rdi,%r14)
	leaq	168(%r9), %rdi
	movq	160(%r13), %r14
	movq	%rdi, 152(%r9)
	movq	152(%r13), %r15
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2133
	testq	%r15, %r15
	je	.L1866
.L2133:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2518
	cmpq	$1, %r14
	jne	.L1942
	movzbl	(%r15), %eax
	movb	%al, 168(%r9)
.L1943:
	movq	%r14, 160(%r9)
	movb	$0, (%rdi,%r14)
	leaq	208(%r9), %rdi
	movzbl	188(%r13), %eax
	movl	184(%r13), %edx
	movq	%rdi, 192(%r9)
	movq	192(%r13), %r15
	movq	200(%r13), %r14
	movb	%al, 188(%r9)
	movl	%edx, 184(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2134
	testq	%r15, %r15
	je	.L1866
.L2134:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2519
	cmpq	$1, %r14
	jne	.L1947
	movzbl	(%r15), %eax
	movb	%al, 208(%r9)
.L1948:
	movq	%r14, 200(%r9)
	movb	$0, (%rdi,%r14)
	leaq	240(%r9), %rdi
	movq	232(%r13), %r14
	movq	%rdi, 224(%r9)
	movq	224(%r13), %r15
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2135
	testq	%r15, %r15
	je	.L1866
.L2135:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2520
	cmpq	$1, %r14
	jne	.L1952
	movzbl	(%r15), %eax
	movb	%al, 240(%r9)
.L1953:
	movq	%r14, 232(%r9)
	movb	$0, (%rdi,%r14)
	movq	256(%r13), %rax
	leaq	288(%r9), %rdi
	movq	%rax, 256(%r9)
	movq	264(%r13), %rax
	movq	%rdi, 272(%r9)
	movq	272(%r13), %r15
	movq	280(%r13), %r14
	movq	%rax, 264(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2136
	testq	%r15, %r15
	je	.L1866
.L2136:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2521
	cmpq	$1, %r14
	jne	.L1957
	movzbl	(%r15), %eax
	movb	%al, 288(%r9)
.L1958:
	movq	%r14, 280(%r9)
	movb	$0, (%rdi,%r14)
	leaq	328(%r9), %rdi
	movq	304(%r13), %rax
	movq	%rdi, 312(%r9)
	movq	312(%r13), %r15
	movq	%rax, 304(%r9)
	movq	320(%r13), %r14
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2137
	testq	%r15, %r15
	je	.L1866
.L2137:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2522
	cmpq	$1, %r14
	jne	.L1962
	movzbl	(%r15), %eax
	movb	%al, 328(%r9)
.L1963:
	movq	%r14, 320(%r9)
	movb	$0, (%rdi,%r14)
	leaq	368(%r9), %rdi
	movzbl	344(%r13), %eax
	movq	%rdi, 352(%r9)
	movq	352(%r13), %r15
	movq	360(%r13), %r14
	movb	%al, 344(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2138
	testq	%r15, %r15
	je	.L1866
.L2138:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2523
	cmpq	$1, %r14
	jne	.L1967
	movzbl	(%r15), %eax
	movb	%al, 368(%r9)
.L1968:
	movq	%r14, 360(%r9)
	movb	$0, (%rdi,%r14)
	leaq	400(%r9), %rdi
	movq	392(%r13), %r14
	movq	%rdi, 384(%r9)
	movq	384(%r13), %r15
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2139
	testq	%r15, %r15
	je	.L1866
.L2139:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2524
	cmpq	$1, %r14
	jne	.L1972
	movzbl	(%r15), %eax
	movb	%al, 400(%r9)
.L1973:
	movq	%r14, 392(%r9)
	movb	$0, (%rdi,%r14)
	movq	416(%r13), %rax
	leaq	448(%r9), %rdi
	movq	%rax, 416(%r9)
	movzbl	424(%r13), %eax
	movq	%rdi, 432(%r9)
	movq	432(%r13), %r15
	movq	440(%r13), %r14
	movb	%al, 424(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2140
	testq	%r15, %r15
	je	.L1866
.L2140:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2525
	cmpq	$1, %r14
	jne	.L1977
	movzbl	(%r15), %eax
	movb	%al, 448(%r9)
.L1978:
	movq	%r14, 440(%r9)
	movb	$0, (%rdi,%r14)
	leaq	488(%r9), %rdi
	movq	464(%r13), %rax
	movq	%rdi, 472(%r9)
	movq	472(%r13), %r15
	movq	480(%r13), %r14
	movq	%rax, 464(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2141
	testq	%r15, %r15
	je	.L1866
.L2141:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2526
	cmpq	$1, %r14
	jne	.L1982
	movzbl	(%r15), %eax
	movb	%al, 488(%r9)
.L1983:
	movq	%r14, 480(%r9)
	movb	$0, (%rdi,%r14)
	leaq	520(%r9), %rdi
	movq	512(%r13), %r14
	movq	%rdi, 504(%r9)
	movq	504(%r13), %r15
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2142
	testq	%r15, %r15
	je	.L1866
.L2142:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2527
	cmpq	$1, %r14
	jne	.L1987
	movzbl	(%r15), %eax
	movb	%al, 520(%r9)
.L1988:
	movq	%r14, 512(%r9)
	movb	$0, (%rdi,%r14)
	leaq	560(%r9), %rdi
	movzbl	538(%r13), %eax
	movzwl	536(%r13), %edx
	movq	%rdi, 544(%r9)
	movq	544(%r13), %r15
	movq	552(%r13), %r14
	movb	%al, 538(%r9)
	movw	%dx, 536(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2143
	testq	%r15, %r15
	je	.L1866
.L2143:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2528
	cmpq	$1, %r14
	jne	.L1992
	movzbl	(%r15), %eax
	movb	%al, 560(%r9)
.L1993:
	movq	%r14, 552(%r9)
	movb	$0, (%rdi,%r14)
	leaq	608(%r9), %rdi
	movzbl	584(%r13), %eax
	movq	576(%r13), %rdx
	movq	%rdi, 592(%r9)
	movq	592(%r13), %r15
	movq	600(%r13), %r14
	movb	%al, 584(%r9)
	movq	%rdx, 576(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2144
	testq	%r15, %r15
	je	.L1866
.L2144:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2529
	cmpq	$1, %r14
	jne	.L1997
	movzbl	(%r15), %eax
	movb	%al, 608(%r9)
.L1998:
	movq	%r14, 600(%r9)
	pxor	%xmm0, %xmm0
	movb	$0, (%rdi,%r14)
	movq	632(%r13), %r14
	subq	624(%r13), %r14
	movups	%xmm0, 624(%r9)
	movq	$0, 640(%r9)
	movq	%r14, %rax
	sarq	$5, %rax
	je	.L2530
	movabsq	$288230376151711743, %rdx
	movq	%r9, -320(%rbp)
	cmpq	%rdx, %rax
	ja	.L1864
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %r15
.L2000:
	movq	%r15, %xmm0
	addq	%r15, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 640(%r9)
	movups	%xmm0, 624(%r9)
	movq	632(%r13), %r14
	movq	624(%r13), %rcx
	cmpq	%rcx, %r14
	je	.L2001
	movq	%r13, -368(%rbp)
	movq	%r15, %r13
	movq	%r12, -376(%rbp)
	movq	%rcx, %r12
	movq	%r9, -360(%rbp)
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L2003:
	cmpq	$1, %r15
	jne	.L2005
	movzbl	(%r10), %eax
	movb	%al, 16(%r13)
.L2006:
	addq	$32, %r12
	movq	%r15, 8(%r13)
	addq	$32, %r13
	movb	$0, (%rdi,%r15)
	cmpq	%r12, %r14
	je	.L2531
.L2007:
	leaq	16(%r13), %rdi
	movq	8(%r12), %r15
	movq	%rdi, 0(%r13)
	movq	(%r12), %r10
	movq	%r10, %rax
	addq	%r15, %rax
	je	.L2145
	testq	%r10, %r10
	je	.L1866
.L2145:
	movq	%r15, -224(%rbp)
	cmpq	$15, %r15
	jbe	.L2003
	movq	%r13, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r10, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r10
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, 16(%r13)
.L2004:
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-224(%rbp), %r15
	movq	0(%r13), %rdi
	jmp	.L2006
	.p2align 4,,10
	.p2align 3
.L1838:
	testq	%rax, %rax
	je	.L2121
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L1845:
	testq	%rax, %rax
	je	.L2120
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2120
	.p2align 4,,10
	.p2align 3
.L1850:
	testq	%rax, %rax
	je	.L2119
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L1876:
	testq	%rax, %rax
	je	.L2118
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L1881:
	testq	%rax, %rax
	je	.L2117
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L2497:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2123
	movq	1184(%r12), %r8
	testq	%r8, %r8
	jne	.L1827
	jmp	.L2123
	.p2align 4,,10
	.p2align 3
.L1887:
	testq	%rax, %rax
	je	.L2116
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2498:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2122
	movq	1224(%r12), %r8
	testq	%r8, %r8
	jne	.L1832
	jmp	.L2122
	.p2align 4,,10
	.p2align 3
.L2504:
	xorl	%r14d, %r14d
	jmp	.L1863
	.p2align 4,,10
	.p2align 3
.L2502:
	xorl	%r14d, %r14d
	jmp	.L1853
	.p2align 4,,10
	.p2align 3
.L1892:
	testq	%rax, %rax
	je	.L2115
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2499:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2121
	movq	1264(%r12), %r8
	testq	%r8, %r8
	jne	.L1837
	jmp	.L2121
	.p2align 4,,10
	.p2align 3
.L2500:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2120
	movq	1320(%r12), %r8
	testq	%r8, %r8
	jne	.L1844
	jmp	.L2120
	.p2align 4,,10
	.p2align 3
.L2501:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2119
	movq	1368(%r12), %r8
	testq	%r8, %r8
	jne	.L1849
	jmp	.L2119
	.p2align 4,,10
	.p2align 3
.L2508:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2118
	movq	1800(%r12), %r8
	testq	%r8, %r8
	jne	.L1875
	jmp	.L2118
	.p2align 4,,10
	.p2align 3
.L2510:
	movl	$1, %eax
	lock xaddq	%rax, _ZN4nodeL14next_thread_idE(%rip)
	movq	%rax, 16(%rbp)
	jmp	.L1883
	.p2align 4,,10
	.p2align 3
.L1912:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1913
	.p2align 4,,10
	.p2align 3
.L1900:
	addl	$1, 8(%r13)
	jmp	.L1901
	.p2align 4,,10
	.p2align 3
.L2005:
	testq	%r15, %r15
	je	.L2006
	jmp	.L2004
	.p2align 4,,10
	.p2align 3
.L2531:
	movq	%r13, %r15
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r13
	movq	-376(%rbp), %r12
.L2001:
	movq	%r15, 632(%r9)
	movq	656(%r13), %r14
	pxor	%xmm0, %xmm0
	subq	648(%r13), %r14
	movups	%xmm0, 648(%r9)
	movq	$0, 664(%r9)
	movq	%r14, %rax
	sarq	$5, %rax
	je	.L2532
	movabsq	$288230376151711743, %rdx
	movq	%r9, -320(%rbp)
	cmpq	%rdx, %rax
	ja	.L1864
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %r15
.L2009:
	movq	%r15, %xmm0
	addq	%r15, %r14
	punpcklqdq	%xmm0, %xmm0
	movq	%r14, 664(%r9)
	movups	%xmm0, 648(%r9)
	movq	656(%r13), %r14
	movq	648(%r13), %rcx
	cmpq	%rcx, %r14
	je	.L2010
	movq	%r13, -368(%rbp)
	movq	%rcx, %r13
	movq	%r12, -376(%rbp)
	movq	%r15, %r12
	movq	%r9, -360(%rbp)
	jmp	.L2016
	.p2align 4,,10
	.p2align 3
.L2012:
	cmpq	$1, %r15
	jne	.L2014
	movzbl	(%r10), %eax
	movb	%al, 16(%r12)
.L2015:
	addq	$32, %r13
	movq	%r15, 8(%r12)
	addq	$32, %r12
	movb	$0, (%rdi,%r15)
	cmpq	%r13, %r14
	je	.L2533
.L2016:
	leaq	16(%r12), %rdi
	movq	8(%r13), %r15
	movq	%rdi, (%r12)
	movq	0(%r13), %r10
	movq	%r10, %rax
	addq	%r15, %rax
	je	.L2146
	testq	%r10, %r10
	je	.L1866
.L2146:
	movq	%r15, -224(%rbp)
	cmpq	$15, %r15
	jbe	.L2012
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r10, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r10
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, 16(%r12)
.L2013:
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-224(%rbp), %r15
	movq	(%r12), %rdi
	jmp	.L2015
	.p2align 4,,10
	.p2align 3
.L2014:
	testq	%r15, %r15
	je	.L2015
	jmp	.L2013
	.p2align 4,,10
	.p2align 3
.L2533:
	movq	%r12, %r15
	movq	-360(%rbp), %r9
	movq	-368(%rbp), %r13
	movq	-376(%rbp), %r12
.L2010:
	leaq	16+_ZTVN4node12DebugOptionsE(%rip), %rax
	leaq	704(%r9), %rdi
	movq	%r15, 656(%r9)
	movq	696(%r13), %r14
	movq	%rax, 672(%r9)
	movl	680(%r13), %eax
	movq	%rdi, 688(%r9)
	movq	688(%r13), %r15
	movl	%eax, 680(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2147
	testq	%r15, %r15
	je	.L1866
.L2147:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2534
	cmpq	$1, %r14
	jne	.L2020
	movzbl	(%r15), %eax
	movb	%al, 704(%r9)
.L2021:
	movq	%r14, 696(%r9)
	movb	$0, (%rdi,%r14)
	leaq	744(%r9), %rdi
	movzwl	720(%r13), %eax
	movq	%rdi, 728(%r9)
	movq	728(%r13), %r15
	movq	736(%r13), %r14
	movw	%ax, 720(%r9)
	movq	%r15, %rax
	addq	%r14, %rax
	je	.L2148
	testq	%r15, %r15
	je	.L1866
.L2148:
	movq	%r14, -224(%rbp)
	cmpq	$15, %r14
	ja	.L2535
	cmpq	$1, %r14
	jne	.L2025
	movzbl	(%r15), %eax
	movb	%al, 744(%r9)
.L2026:
	movq	%r14, 736(%r9)
	movb	$0, (%rdi,%r14)
	movl	760(%r13), %eax
	movl	$24, %edi
	movq	%r9, -320(%rbp)
	movl	%eax, 760(%r9)
	call	_Znwm@PLT
	movq	-320(%rbp), %r9
	movabsq	$4294967297, %rcx
	movq	1648(%r12), %r13
	movq	%rax, %xmm7
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%r9, %xmm0
	movq	%rcx, (%rax)
	punpcklqdq	%xmm7, %xmm0
	movq	%r9, 16(%rax)
	movups	%xmm0, 1640(%r12)
	testq	%r13, %r13
	je	.L2028
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L2029
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L2030:
	cmpl	$1, %eax
	je	.L2536
.L2028:
	movq	-336(%rbp), %rax
	testq	%rax, %rax
	je	.L2035
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L2036
	movl	$-1, %ecx
	lock xaddl	%ecx, 8(%rax)
	movl	%ecx, %eax
.L2037:
	cmpl	$1, %eax
	je	.L2537
.L2035:
	movl	$80, %edi
	movq	1640(%r12), %r14
	call	_Znwm@PLT
	movq	%rax, %rdi
	movq	%rax, %r15
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1895
	leaq	56(%r15), %rdi
	movq	736(%r14), %r13
	movq	%rdi, 40(%r15)
	movq	728(%r14), %r8
	movq	%r8, %rax
	addq	%r13, %rax
	je	.L2149
	testq	%r8, %r8
	je	.L1866
.L2149:
	movq	%r13, -224(%rbp)
	cmpq	$15, %r13
	ja	.L2538
	cmpq	$1, %r13
	jne	.L2044
	movzbl	(%r8), %eax
	movb	%al, 56(%r15)
.L2045:
	movq	%r13, 48(%r15)
	movb	$0, (%rdi,%r13)
	movl	760(%r14), %eax
	movl	$24, %edi
	movl	%eax, 72(%r15)
	call	_Znwm@PLT
	movq	1664(%r12), %r13
	movq	%r15, %xmm0
	movabsq	$4294967297, %rcx
	movq	%rax, %xmm6
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	punpcklqdq	%xmm6, %xmm0
	movq	%rcx, (%rax)
	movq	%r15, 16(%rax)
	movups	%xmm0, 1656(%r12)
	testq	%r13, %r13
	je	.L2047
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L2048
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L2049:
	cmpl	$1, %eax
	je	.L2539
.L2047:
	movl	$208, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN4node9inspector5AgentC1EPNS_11EnvironmentE@PLT
	movq	2080(%r12), %r13
	movq	%r14, 2080(%r12)
	testq	%r13, %r13
	je	.L2053
	movq	%r13, %rdi
	call	_ZN4node9inspector5AgentD1Ev@PLT
	movl	$208, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2053:
	leaq	-128(%rbp), %rax
	movq	%r12, %rdx
	movl	$32, %esi
	movb	$0, -112(%rbp)
	movq	-328(%rbp), %rdi
	leaq	-112(%rbp), %r14
	leaq	-80(%rbp), %r13
	movb	$0, -80(%rbp)
	leaq	-144(%rbp), %r15
	movq	%rax, -320(%rbp)
	movq	%r15, -160(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%r14, -128(%rbp)
	movq	$0, -120(%rbp)
	movq	%r13, -96(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -64(%rbp)
	call	_ZN2v87Context31SetAlignedPointerInEmbedderDataEiPv@PLT
	movq	-328(%rbp), %rdi
	movl	$35, %esi
	leaq	_ZN4node11Environment15kNodeContextTagE(%rip), %rdx
	call	_ZN2v87Context31SetAlignedPointerInEmbedderDataEiPv@PLT
	movq	2080(%r12), %rdi
	movq	-320(%rbp), %rdx
	movq	-328(%rbp), %rsi
	call	_ZN4node9inspector5Agent14ContextCreatedEN2v85LocalINS2_7ContextEEERKNS_11ContextInfoE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L2054
	call	_ZdlPv@PLT
.L2054:
	movq	-128(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L2055
	call	_ZdlPv@PLT
.L2055:
	movq	-160(%rbp), %rdi
	cmpq	%r15, %rdi
	je	.L2056
	call	_ZdlPv@PLT
.L2056:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	1816(%r12), %rdi
	leaq	16+_ZTVN4node26TrackingTraceStateObserverE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%r12, 8(%rax)
	movq	%rax, 1816(%r12)
	testq	%rdi, %rdi
	je	.L2057
	movq	(%rdi), %rax
	call	*8(%rax)
.L2057:
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rax
	testq	%rax, %rax
	je	.L2058
	movq	976(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2540
	movq	(%rdi), %rax
	movq	1816(%r12), %rsi
	call	*48(%rax)
.L2058:
	movq	1416(%r12), %rdx
	movq	1432(%r12), %rax
	subq	%rdx, %rax
	cmpq	$4095, %rax
	jbe	.L2541
.L2060:
	movq	352(%r12), %r13
	movl	$128, %edi
	call	_Znwm@PLT
	movdqa	.LC541(%rip), %xmm0
	movq	-312(%rbp), %rdi
	movq	%r13, (%rax)
	movq	%rax, %r15
	movq	%r13, %rsi
	movq	$0, 32(%rax)
	movups	%xmm0, 8(%rax)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r15), %rdi
	movl	$80, %esi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	movq	%rax, -336(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-224(%rbp), %rax
	movq	16(%r15), %rsi
	movl	$80, %edx
	movq	-336(%rbp), %r8
	movq	%rax, 24(%r15)
	movq	%r8, %rdi
	call	_ZN2v810Uint8Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2542
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	32(%r15), %rdi
	movq	%rax, -224(%rbp)
	testq	%rdi, %rdi
	je	.L2067
.L2066:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 32(%r15)
	movq	-224(%rbp), %rax
.L2067:
	testq	%rax, %rax
	je	.L2065
	movq	%rax, 32(%r15)
	leaq	32(%r15), %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2065:
	movq	-312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r13, 40(%r15)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	.LC547(%rip), %xmm0
	movq	$0, 72(%r15)
	movups	%xmm0, 48(%r15)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2113
	movq	(%rdi), %rsi
	movq	(%r15), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2113:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-336(%rbp), %rdi
	cmpq	$47, %rax
	jbe	.L2543
	movq	24(%r15), %rax
	xorl	%esi, %esi
	movl	$6, %edx
	movq	%rax, 64(%r15)
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2544
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	72(%r15), %rdi
	movq	%rax, -256(%rbp)
	testq	%rdi, %rdi
	je	.L2072
.L2071:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 72(%r15)
	movq	-256(%rbp), %rax
.L2072:
	testq	%rax, %rax
	je	.L2070
	movq	%rax, 72(%r15)
	movq	-312(%rbp), %rdi
	leaq	72(%r15), %rsi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2070:
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r13, 80(%r15)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movdqa	.LC546(%rip), %xmm0
	movq	$0, 112(%r15)
	movups	%xmm0, 88(%r15)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2111
	movq	(%rdi), %rsi
	movq	(%r15), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
.L2111:
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%rax, %rdi
	movq	%rax, -336(%rbp)
	call	_ZNK2v811ArrayBuffer10ByteLengthEv@PLT
	movq	-336(%rbp), %rdi
	subq	$48, %rax
	cmpq	$27, %rax
	jbe	.L2545
	movq	24(%r15), %rax
	movl	$48, %esi
	movl	$7, %edx
	addq	$48, %rax
	movq	%rax, 104(%r15)
	call	_ZN2v811Uint32Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2546
	movq	%r13, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	112(%r15), %rdi
	movq	%rax, -256(%rbp)
	testq	%rdi, %rdi
	je	.L2077
.L2076:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 112(%r15)
	movq	-256(%rbp), %rax
.L2077:
	testq	%rax, %rax
	je	.L2075
	movq	%rax, 112(%r15)
	movq	-312(%rbp), %rdi
	leaq	112(%r15), %rsi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2075:
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	48(%r15), %rsi
	movq	$0, 120(%r15)
	testq	%rsi, %rsi
	je	.L2110
	movq	64(%r15), %rcx
	cmpq	$1, %rsi
	je	.L2126
	movq	%rsi, %rdx
	movapd	.LC542(%rip), %xmm0
	movq	%rcx, %rax
	shrq	%rdx
	salq	$4, %rdx
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L2079:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L2079
	movq	%rsi, %rax
	andq	$-2, %rax
	andl	$1, %esi
	je	.L2110
.L2078:
	movq	.LC543(%rip), %rdx
	movq	%rdx, (%rcx,%rax,8)
.L2110:
	movq	1864(%r12), %r13
	movq	%r15, 1864(%r12)
	testq	%r13, %r13
	je	.L2081
	movq	112(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2082
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2082:
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2083
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2083:
	movq	32(%r13), %rdi
	testq	%rdi, %rdi
	je	.L2084
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L2084:
	movl	$128, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
	movq	1864(%r12), %r15
.L2081:
	call	uv_hrtime@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %rdx
	call	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	movq	1864(%r12), %rdi
	movl	$1, %esi
	movq	_ZN4node11per_process15node_start_timeE(%rip), %rdx
	call	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	movq	1864(%r12), %rdi
	movl	$2, %esi
	movq	_ZN4node11performance20performance_v8_startE(%rip), %rdx
	call	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2086
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	cmpb	$0, (%rax)
	jne	.L2547
.L2086:
	movq	1792(%r12), %rax
	movl	$1, (%rax)
	movq	1640(%r12), %rax
	cmpb	$0, 265(%rax)
	je	.L2106
	movq	1216(%r12), %rax
	subl	$1, 24(%rax)
.L2106:
	movq	%r12, %rdi
	call	_ZN4node11Environment16CreatePropertiesEv
	movq	-328(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-384(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2548
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1897:
	.cfi_restore_state
	movq	1392(%r12), %r15
	movq	%rdx, 1384(%r12)
	testq	%r15, %r15
	je	.L1911
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
.L1902:
	testq	%r14, %r14
	je	.L1905
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
.L1906:
	cmpl	$1, %eax
	je	.L2549
.L1904:
	movq	%r13, 1392(%r12)
	testq	%r13, %r13
	je	.L1911
	leaq	8(%r13), %rax
	jmp	.L2108
	.p2align 4,,10
	.p2align 3
.L2509:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2117
	movq	1856(%r12), %r8
	testq	%r8, %r8
	jne	.L1880
	jmp	.L2117
	.p2align 4,,10
	.p2align 3
.L1922:
	testq	%r14, %r14
	je	.L1923
	.p2align 4,,10
	.p2align 3
.L1921:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	16(%r9), %rdi
	jmp	.L1923
	.p2align 4,,10
	.p2align 3
.L1932:
	testq	%r14, %r14
	je	.L1933
	.p2align 4,,10
	.p2align 3
.L1931:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	88(%r9), %rdi
	jmp	.L1933
	.p2align 4,,10
	.p2align 3
.L1927:
	testq	%r14, %r14
	je	.L1928
	.p2align 4,,10
	.p2align 3
.L1926:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	48(%r9), %rdi
	jmp	.L1928
	.p2align 4,,10
	.p2align 3
.L1942:
	testq	%r14, %r14
	je	.L1943
	.p2align 4,,10
	.p2align 3
.L1941:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	152(%r9), %rdi
	jmp	.L1943
	.p2align 4,,10
	.p2align 3
.L1937:
	testq	%r14, %r14
	je	.L1938
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	120(%r9), %rdi
	jmp	.L1938
	.p2align 4,,10
	.p2align 3
.L1962:
	testq	%r14, %r14
	je	.L1963
	.p2align 4,,10
	.p2align 3
.L1961:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	312(%r9), %rdi
	jmp	.L1963
	.p2align 4,,10
	.p2align 3
.L1957:
	testq	%r14, %r14
	je	.L1958
	.p2align 4,,10
	.p2align 3
.L1956:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	272(%r9), %rdi
	jmp	.L1958
	.p2align 4,,10
	.p2align 3
.L1952:
	testq	%r14, %r14
	je	.L1953
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	224(%r9), %rdi
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1947:
	testq	%r14, %r14
	je	.L1948
	.p2align 4,,10
	.p2align 3
.L1946:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	192(%r9), %rdi
	jmp	.L1948
	.p2align 4,,10
	.p2align 3
.L1997:
	testq	%r14, %r14
	je	.L1998
	.p2align 4,,10
	.p2align 3
.L1996:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	592(%r9), %rdi
	jmp	.L1998
	.p2align 4,,10
	.p2align 3
.L1992:
	testq	%r14, %r14
	je	.L1993
	.p2align 4,,10
	.p2align 3
.L1991:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	544(%r9), %rdi
	jmp	.L1993
	.p2align 4,,10
	.p2align 3
.L1987:
	testq	%r14, %r14
	je	.L1988
	.p2align 4,,10
	.p2align 3
.L1986:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	504(%r9), %rdi
	jmp	.L1988
	.p2align 4,,10
	.p2align 3
.L1982:
	testq	%r14, %r14
	je	.L1983
	.p2align 4,,10
	.p2align 3
.L1981:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	472(%r9), %rdi
	jmp	.L1983
	.p2align 4,,10
	.p2align 3
.L1977:
	testq	%r14, %r14
	je	.L1978
	.p2align 4,,10
	.p2align 3
.L1976:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	432(%r9), %rdi
	jmp	.L1978
	.p2align 4,,10
	.p2align 3
.L1972:
	testq	%r14, %r14
	je	.L1973
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	384(%r9), %rdi
	jmp	.L1973
	.p2align 4,,10
	.p2align 3
.L1967:
	testq	%r14, %r14
	je	.L1968
	.p2align 4,,10
	.p2align 3
.L1966:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	352(%r9), %rdi
	jmp	.L1968
	.p2align 4,,10
	.p2align 3
.L2020:
	testq	%r14, %r14
	je	.L2021
	.p2align 4,,10
	.p2align 3
.L2019:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	688(%r9), %rdi
	jmp	.L2021
	.p2align 4,,10
	.p2align 3
.L2025:
	testq	%r14, %r14
	je	.L2026
	.p2align 4,,10
	.p2align 3
.L2024:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r9, -320(%rbp)
	call	memcpy@PLT
	movq	-320(%rbp), %r9
	movq	-224(%rbp), %r14
	movq	728(%r9), %rdi
	jmp	.L2026
	.p2align 4,,10
	.p2align 3
.L1895:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2514:
	leaq	16(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 16(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 32(%r9)
	jmp	.L1921
	.p2align 4,,10
	.p2align 3
.L2516:
	leaq	88(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 88(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 104(%r9)
	jmp	.L1931
	.p2align 4,,10
	.p2align 3
.L2515:
	leaq	48(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 48(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 64(%r9)
	jmp	.L1926
	.p2align 4,,10
	.p2align 3
.L2518:
	leaq	152(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 152(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 168(%r9)
	jmp	.L1941
	.p2align 4,,10
	.p2align 3
.L2517:
	leaq	120(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 120(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 136(%r9)
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L2522:
	leaq	312(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 312(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 328(%r9)
	jmp	.L1961
	.p2align 4,,10
	.p2align 3
.L2528:
	leaq	544(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 544(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 560(%r9)
	jmp	.L1991
	.p2align 4,,10
	.p2align 3
.L2527:
	leaq	504(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 504(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 520(%r9)
	jmp	.L1986
	.p2align 4,,10
	.p2align 3
.L2521:
	leaq	272(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 272(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 288(%r9)
	jmp	.L1956
	.p2align 4,,10
	.p2align 3
.L2520:
	leaq	224(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 224(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 240(%r9)
	jmp	.L1951
	.p2align 4,,10
	.p2align 3
.L2519:
	leaq	192(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 192(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 208(%r9)
	jmp	.L1946
	.p2align 4,,10
	.p2align 3
.L2529:
	leaq	592(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 592(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 608(%r9)
	jmp	.L1996
	.p2align 4,,10
	.p2align 3
.L2524:
	leaq	384(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 384(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 400(%r9)
	jmp	.L1971
	.p2align 4,,10
	.p2align 3
.L2523:
	leaq	352(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 352(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 368(%r9)
	jmp	.L1966
	.p2align 4,,10
	.p2align 3
.L2526:
	leaq	472(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 472(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 488(%r9)
	jmp	.L1981
	.p2align 4,,10
	.p2align 3
.L2525:
	leaq	432(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 432(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 448(%r9)
	jmp	.L1976
	.p2align 4,,10
	.p2align 3
.L2534:
	leaq	688(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 688(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 704(%r9)
	jmp	.L2019
	.p2align 4,,10
	.p2align 3
.L2535:
	leaq	728(%r9), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r9, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r9
	movq	%rax, %rdi
	movq	%rax, 728(%r9)
	movq	-224(%rbp), %rax
	movq	%rax, 744(%r9)
	jmp	.L2024
	.p2align 4,,10
	.p2align 3
.L1918:
	addl	$1, 8(%rcx)
	jmp	.L1917
	.p2align 4,,10
	.p2align 3
.L1898:
	addl	$1, 8(%r13)
	jmp	.L1899
	.p2align 4,,10
	.p2align 3
.L2044:
	testq	%r13, %r13
	je	.L2045
	.p2align 4,,10
	.p2align 3
.L2043:
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-224(%rbp), %r13
	movq	40(%r15), %rdi
	jmp	.L2045
	.p2align 4,,10
	.p2align 3
.L2538:
	leaq	40(%r15), %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r8, -320(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-320(%rbp), %r8
	movq	%rax, 40(%r15)
	movq	%rax, %rdi
	movq	-224(%rbp), %rax
	movq	%rax, 56(%r15)
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2511:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2116
	movq	2296(%r12), %r8
	testq	%r8, %r8
	jne	.L1886
	jmp	.L2116
	.p2align 4,,10
	.p2align 3
.L2530:
	xorl	%r15d, %r15d
	jmp	.L2000
	.p2align 4,,10
	.p2align 3
.L2532:
	xorl	%r15d, %r15d
	jmp	.L2009
	.p2align 4,,10
	.p2align 3
.L2541:
	movq	1424(%r12), %r15
	movl	$4096, %edi
	subq	%rdx, %r15
	call	_Znwm@PLT
	movq	1416(%r12), %r8
	movq	1424(%r12), %rdx
	movq	%rax, %r13
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L2550
	testq	%r8, %r8
	jne	.L2062
.L2063:
	leaq	0(%r13,%r15), %rax
	movq	%r13, 1416(%r12)
	addq	$4096, %r13
	movq	%rax, 1424(%r12)
	movq	%r13, 1432(%r12)
	jmp	.L2060
	.p2align 4,,10
	.p2align 3
.L2036:
	movq	%rax, %rcx
	movl	8(%rax), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rcx)
	jmp	.L2037
	.p2align 4,,10
	.p2align 3
.L2029:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L2030
	.p2align 4,,10
	.p2align 3
.L2512:
	movq	$0, -224(%rbp)
	cmpq	%rbx, %r13
	je	.L2115
	movq	2336(%r12), %r8
	testq	%r8, %r8
	jne	.L1891
	jmp	.L2115
	.p2align 4,,10
	.p2align 3
.L2048:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L2049
.L2537:
	movq	-336(%rbp), %r15
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L2039
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L2040:
	cmpl	$1, %eax
	jne	.L2035
	movq	-336(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2035
.L2536:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L2032
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2033:
	cmpl	$1, %eax
	jne	.L2028
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2028
.L2513:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L1915
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1916:
	cmpl	$1, %eax
	jne	.L1911
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1911
.L1905:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L1906
.L2539:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L2051
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L2052:
	cmpl	$1, %eax
	jne	.L2047
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2047
.L2125:
	xorl	%eax, %eax
	jmp	.L1896
.L2550:
	movq	%r8, %rsi
	movq	%rax, %rdi
	movq	%r8, -336(%rbp)
	call	memmove@PLT
	movq	-336(%rbp), %r8
.L2062:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L2063
.L2547:
	leaq	-296(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue6CreateEv@PLT
	movq	-296(%rbp), %rdi
	leaq	.LC264(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10BeginArrayEPKc@PLT
	movq	-344(%rbp), %rax
	movq	8(%rax), %r15
	movq	(%rax), %r13
	cmpq	%r15, %r13
	je	.L2092
	.p2align 4,,10
	.p2align 3
.L2091:
	movq	0(%r13), %rsi
	movq	-296(%rbp), %rdi
	addq	$32, %r13
	call	_ZN4node7tracing11TracedValue12AppendStringEPKc@PLT
	cmpq	%r13, %r15
	jne	.L2091
.L2092:
	movq	-296(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue8EndArrayEv@PLT
	movq	-296(%rbp), %rdi
	leaq	.LC544(%rip), %rsi
	call	_ZN4node7tracing11TracedValue10BeginArrayEPKc@PLT
	movq	-352(%rbp), %rax
	movq	8(%rax), %r15
	movq	(%rax), %r13
	cmpq	%r15, %r13
	je	.L2090
	.p2align 4,,10
	.p2align 3
.L2095:
	movq	0(%r13), %rsi
	movq	-296(%rbp), %rdi
	addq	$32, %r13
	call	_ZN4node7tracing11TracedValue12AppendStringEPKc@PLT
	cmpq	%r13, %r15
	jne	.L2095
.L2090:
	movq	-296(%rbp), %rdi
	call	_ZN4node7tracing11TracedValue8EndArrayEv@PLT
	movq	_ZZN4node11EnvironmentC4EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEmE28trace_event_unique_atomic389(%rip), %r13
	testq	%r13, %r13
	je	.L2093
.L2094:
	movq	-296(%rbp), %r15
	testb	$5, 0(%r13)
	jne	.L2551
.L2097:
	testq	%r15, %r15
	je	.L2086
	movq	(%r15), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2104
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	movq	8(%r15), %rdi
	movq	%rax, (%r15)
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2105
	call	_ZdlPv@PLT
.L2105:
	movl	$48, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
	jmp	.L2086
.L2542:
	movq	$0, -224(%rbp)
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L2066
	jmp	.L2065
.L2549:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L1908
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1909:
	cmpl	$1, %eax
	jne	.L1904
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1904
.L1915:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1916
.L2544:
	movq	$0, -256(%rbp)
	movq	72(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L2071
	jmp	.L2070
.L2032:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2033
.L2039:
	movq	-336(%rbp), %rcx
	movl	12(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rcx)
	jmp	.L2040
.L2051:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L2052
.L2540:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2546:
	movq	$0, -256(%rbp)
	movq	112(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L2076
	jmp	.L2075
.L2543:
	leaq	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1908:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1909
.L2545:
	leaq	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2551:
	leaq	.LC264(%rip), %rax
	movb	$8, -297(%rbp)
	movq	%rax, -256(%rbp)
	movq	$0, -296(%rbp)
	movq	%r15, -224(%rbp)
	movq	$0, -120(%rbp)
	movq	%r15, -128(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2098
	subq	$8, %rsp
	movq	(%rax), %rax
	movq	%r12, %r9
	xorl	%r8d, %r8d
	leaq	-297(%rbp), %rdx
	pushq	$6
	movl	$98, %esi
	leaq	.LC545(%rip), %rcx
	pushq	-320(%rbp)
	pushq	%rbx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	-312(%rbp)
	pushq	$1
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2098:
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r15
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rbx
.L2102:
	movq	-8(%r14), %r13
	subq	$8, %r14
	testq	%r13, %r13
	je	.L2099
	movq	0(%r13), %rax
	movq	8(%rax), %rax
	cmpq	%r15, %rax
	jne	.L2100
	movq	8(%r13), %rdi
	leaq	24(%r13), %rax
	movq	%rbx, 0(%r13)
	cmpq	%rax, %rdi
	je	.L2101
	call	_ZdlPv@PLT
.L2101:
	movl	$48, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L2099:
	cmpq	-320(%rbp), %r14
	jne	.L2102
	movq	-296(%rbp), %r15
	jmp	.L2097
.L2093:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2096
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r13
.L2096:
	movq	%r13, _ZZN4node11EnvironmentC4EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEmE28trace_event_unique_atomic389(%rip)
	mfence
	jmp	.L2094
.L2126:
	xorl	%eax, %eax
	jmp	.L2078
.L2104:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L2086
.L2100:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2099
.L1866:
	leaq	.LC532(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1864:
	call	_ZSt17__throw_bad_allocv@PLT
.L2548:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8278:
	.size	_ZN4node11EnvironmentC2EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm, .-_ZN4node11EnvironmentC2EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm
	.globl	_ZN4node11EnvironmentC1EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm
	.set	_ZN4node11EnvironmentC1EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm,_ZN4node11EnvironmentC2EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment7ExitEnvEv
	.type	_ZN4node11Environment7ExitEnvEv, @function
_ZN4node11Environment7ExitEnvEv:
.LFB8289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	2488(%rbx), %r13
	subq	$8, %rsp
	movb	$0, 1930(%rdi)
	mfence
	movb	$1, 2664(%rdi)
	mfence
	movq	352(%rdi), %rdi
	call	_ZN2v87Isolate18TerminateExecutionEv@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%r13, %rdi
	movb	$1, 8(%rax)
	movq	%rax, %r12
	movq	$0, 16(%rax)
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_EE(%rip), %rax
	movq	%rax, (%r12)
	call	uv_mutex_lock@PLT
	movq	2544(%rbx), %rax
	lock addq	$1, 2528(%rbx)
	movq	%r12, 2544(%rbx)
	testq	%rax, %rax
	je	.L2553
	movq	16(%rax), %rdi
	movq	%r12, 16(%rax)
	testq	%rdi, %rdi
	je	.L2555
.L2561:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2555:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	leaq	1000(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_async_send@PLT
	.p2align 4,,10
	.p2align 3
.L2553:
	.cfi_restore_state
	movq	2536(%rbx), %rdi
	movq	%r12, 2536(%rbx)
	testq	%rdi, %rdi
	jne	.L2561
	jmp	.L2555
	.cfi_endproc
.LFE8289:
	.size	_ZN4node11Environment7ExitEnvEv, .-_ZN4node11Environment7ExitEnvEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment22RegisterHandleCleanupsEv
	.type	_ZN4node11Environment22RegisterHandleCleanupsEv, @function
_ZN4node11Environment22RegisterHandleCleanupsEv:
.LFB8293:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	_ZZN4node11Environment22RegisterHandleCleanupsEvENUlPS0_P11uv_handle_sPvE_4_FUNES1_S3_S4_(%rip), %r13
	pushq	%r12
	.cfi_offset 12, -32
	leaq	2128(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$40, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	368(%rbx), %rax
	movq	%rax, 16(%rdi)
	movq	%r13, 24(%rdi)
	movq	$0, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2144(%rbx)
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	520(%rbx), %rax
	movq	%rax, 16(%rdi)
	movq	%r13, 24(%rdi)
	movq	$0, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2144(%rbx)
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	640(%rbx), %rax
	movq	%rax, 16(%rdi)
	movq	%r13, 24(%rdi)
	movq	$0, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2144(%rbx)
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	760(%rbx), %rax
	movq	%rax, 16(%rdi)
	movq	%r13, 24(%rdi)
	movq	$0, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2144(%rbx)
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	880(%rbx), %rax
	movq	%rax, 16(%rdi)
	movq	%r13, 24(%rdi)
	movq	$0, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2144(%rbx)
	movl	$40, %edi
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	leaq	1000(%rbx), %rax
	movq	%r13, 24(%rdi)
	movq	%rax, 16(%rdi)
	movq	$0, 32(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2144(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8293:
	.size	_ZN4node11Environment22RegisterHandleCleanupsEv, .-_ZN4node11Environment22RegisterHandleCleanupsEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment15InitializeLibuvEb
	.type	_ZN4node11Environment15InitializeLibuvEb, @function
_ZN4node11Environment15InitializeLibuvEb:
.LFB8284:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	leaq	368(%r12), %r13
	subq	$56, %rsp
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%r12), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	360(%r12), %rax
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	call	uv_timer_init@PLT
	testl	%eax, %eax
	jne	.L2569
	movq	%r13, %rdi
	leaq	520(%r12), %r13
	leaq	760(%r12), %rbx
	call	uv_unref@PLT
	movq	360(%r12), %rax
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	call	uv_check_init@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	movq	360(%r12), %rax
	leaq	640(%r12), %rsi
	movq	2360(%rax), %rdi
	call	uv_idle_init@PLT
	movq	%r13, %rdi
	leaq	_ZN4node11Environment14CheckImmediateEP10uv_check_s(%rip), %rsi
	leaq	880(%r12), %r13
	call	uv_check_start@PLT
	movq	360(%r12), %rax
	movq	%rbx, %rsi
	movq	2360(%rax), %rdi
	call	uv_prepare_init@PLT
	movq	360(%r12), %rax
	movq	%r13, %rsi
	movq	2360(%rax), %rdi
	call	uv_check_init@PLT
	movq	360(%r12), %rax
	leaq	1000(%r12), %r9
	leaq	_ZZN4node11Environment15InitializeLibuvEbENUlP10uv_async_sE_4_FUNES2_(%rip), %rdx
	movq	%r9, %rsi
	movq	%r9, -96(%rbp)
	movq	2360(%rax), %rdi
	call	uv_async_init@PLT
	movq	%rbx, %rdi
	call	uv_unref@PLT
	movq	%r13, %rdi
	call	uv_unref@PLT
	movq	-96(%rbp), %r9
	movq	%r9, %rdi
	call	uv_unref@PLT
	movq	%r12, %rdi
	call	_ZN4node11Environment22RegisterHandleCleanupsEv
	cmpb	$0, -84(%rbp)
	je	.L2566
	cmpb	$0, 1136(%r12)
	jne	.L2566
	movb	$1, 1136(%r12)
	leaq	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP12uv_prepare_sE_4_FUNES2_(%rip), %rsi
	movq	%rbx, %rdi
	call	uv_prepare_start@PLT
	leaq	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP10uv_check_sE0_4_FUNES2_(%rip), %rsi
	movq	%r13, %rdi
	call	uv_check_start@PLT
.L2566:
	leaq	_ZN4node19InitThreadLocalOnceEv(%rip), %rsi
	leaq	_ZZN4node11Environment15InitializeLibuvEbE9init_once(%rip), %rdi
	call	uv_once@PLT
	movq	%r12, %rsi
	leaq	_ZN4node11Environment16thread_local_envE(%rip), %rdi
	call	uv_key_set@PLT
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2570
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2569:
	.cfi_restore_state
	leaq	_ZZN4node11Environment15InitializeLibuvEbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2570:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8284:
	.size	_ZN4node11Environment15InitializeLibuvEb, .-_ZN4node11Environment15InitializeLibuvEb
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment25StartProfilerIdleNotifierEv
	.type	_ZN4node11Environment25StartProfilerIdleNotifierEv, @function
_ZN4node11Environment25StartProfilerIdleNotifierEv:
.LFB8302:
	.cfi_startproc
	endbr64
	cmpb	$0, 1136(%rdi)
	je	.L2576
	ret
	.p2align 4,,10
	.p2align 3
.L2576:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP12uv_prepare_sE_4_FUNES2_(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	760(%rdi), %rdi
	subq	$8, %rsp
	movb	$1, 376(%rdi)
	call	uv_prepare_start@PLT
	addq	$8, %rsp
	leaq	880(%rbx), %rdi
	leaq	_ZZN4node11Environment25StartProfilerIdleNotifierEvENUlP10uv_check_sE0_4_FUNES2_(%rip), %rsi
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uv_check_start@PLT
	.cfi_endproc
.LFE8302:
	.size	_ZN4node11Environment25StartProfilerIdleNotifierEv, .-_ZN4node11Environment25StartProfilerIdleNotifierEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment24StopProfilerIdleNotifierEv
	.type	_ZN4node11Environment24StopProfilerIdleNotifierEv, @function
_ZN4node11Environment24StopProfilerIdleNotifierEv:
.LFB8310:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	leaq	760(%rdi), %rdi
	subq	$8, %rsp
	movb	$0, 376(%rdi)
	call	uv_prepare_stop@PLT
	addq	$8, %rsp
	leaq	880(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_check_stop@PLT
	.cfi_endproc
.LFE8310:
	.size	_ZN4node11Environment24StopProfilerIdleNotifierEv, .-_ZN4node11Environment24StopProfilerIdleNotifierEv
	.section	.rodata.str1.8
	.align 8
.LC548:
	.string	"(node:%d) WARNING: Detected use of sync API\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node11Environment14PrintSyncTraceEv
	.type	_ZNK4node11Environment14PrintSyncTraceEv, @function
_ZNK4node11Environment14PrintSyncTraceEv:
.LFB8311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 1401(%rdi)
	je	.L2579
	movq	352(%rdi), %rsi
	leaq	-48(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	call	uv_os_getpid@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC548(%rip), %rdx
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	movl	$127, %edx
	movl	$10, %esi
	movq	352(%rbx), %rdi
	call	_ZN2v810StackTrace17CurrentStackTraceEPNS_7IsolateEiNS0_17StackTraceOptionsE@PLT
	movq	352(%rbx), %rdi
	movq	%rax, %rsi
	call	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2579:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2584
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2584:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8311:
	.size	_ZNK4node11Environment14PrintSyncTraceEv, .-_ZNK4node11Environment14PrintSyncTraceEv
	.section	.rodata.str1.1
.LC549:
	.string	"AtExit"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment18RunAtExitCallbacksEv
	.type	_ZN4node11Environment18RunAtExitCallbacksEv, @function
_ZN4node11Environment18RunAtExitCallbacksEv:
.LFB8316:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip), %r12
	testq	%r12, %r12
	je	.L2635
	testb	$5, (%r12)
	jne	.L2636
.L2589:
	movq	2440(%r13), %rbx
	leaq	2440(%r13), %r12
	cmpq	%rbx, %r12
	je	.L2595
	.p2align 4,,10
	.p2align 3
.L2596:
	movq	24(%rbx), %rdi
	call	*16(%rbx)
	movq	(%rbx), %rbx
	cmpq	%rbx, %r12
	jne	.L2596
	movq	2440(%r13), %rbx
	cmpq	%rbx, %r12
	je	.L2595
	.p2align 4,,10
	.p2align 3
.L2597:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	jne	.L2597
.L2595:
	movq	$0, 2456(%r13)
	movq	%r12, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 2440(%r13)
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L2598
.L2599:
	testb	$5, (%r12)
	jne	.L2637
.L2585:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2638
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2635:
	.cfi_restore_state
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2588
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L2588:
	movq	%r12, _ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip)
	mfence
	testb	$5, (%r12)
	je	.L2589
.L2636:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2590
	subq	$8, %rsp
	leaq	-80(%rbp), %rbx
	movq	(%rax), %rax
	movq	%r13, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC549(%rip), %rcx
	movq	%r12, %rdx
	pushq	%rbx
	movl	$98, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2590:
	leaq	-64(%rbp), %r15
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
.L2594:
	movq	-8(%r15), %r12
	subq	$8, %r15
	testq	%r12, %r12
	je	.L2591
	movq	(%r12), %rdx
	movq	8(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L2592
	movq	8(%r12), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L2593
	call	_ZdlPv@PLT
.L2593:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2591:
	cmpq	%rbx, %r15
	jne	.L2594
	jmp	.L2589
	.p2align 4,,10
	.p2align 3
.L2637:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2602
	subq	$8, %rsp
	leaq	-80(%rbp), %rbx
	movq	(%rax), %rax
	movq	%r13, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC549(%rip), %rcx
	movq	%r12, %rdx
	pushq	%rbx
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2602:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L2606:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L2603
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2604
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L2605
	call	_ZdlPv@PLT
.L2605:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2603:
	cmpq	%rbx, %r13
	jne	.L2606
	jmp	.L2585
	.p2align 4,,10
	.p2align 3
.L2598:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2600
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L2600:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L2599
	.p2align 4,,10
	.p2align 3
.L2592:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2591
	.p2align 4,,10
	.p2align 3
.L2604:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2603
.L2638:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8316:
	.size	_ZN4node11Environment18RunAtExitCallbacksEv, .-_ZN4node11Environment18RunAtExitCallbacksEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment6AtExitEPFvPvES1_
	.type	_ZN4node11Environment6AtExitEPFvPvES1_, @function
_ZN4node11Environment6AtExitEPFvPvES1_:
.LFB8317:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	2440(%rdi), %r14
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%r14, %rsi
	movq	%r13, 16(%rax)
	movq	%rax, %rdi
	movq	%r12, 24(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2456(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8317:
	.size	_ZN4node11Environment6AtExitEPFvPvES1_, .-_ZN4node11Environment6AtExitEPFvPvES1_
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment21RunAndClearInterruptsEv
	.type	_ZN4node11Environment21RunAndClearInterruptsEv, @function
_ZN4node11Environment21RunAndClearInterruptsEv:
.LFB8318:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	2552(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_(%rip), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2651:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L2641
.L2672:
	leaq	2488(%rbx), %r12
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	movq	%r12, %rdi
	movups	%xmm0, -56(%rbp)
	call	uv_mutex_lock@PLT
	movq	(%r14), %rax
	lock addq	%rax, -64(%rbp)
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	je	.L2643
	movq	2560(%rbx), %rdx
	movq	$0, 2560(%rbx)
	movq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	testq	%rdi, %rdi
	je	.L2645
.L2668:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2645:
	movq	2568(%rbx), %rax
	movq	%r12, %rdi
	movq	$0, 2568(%rbx)
	movq	%rax, -48(%rbp)
	movq	$0, (%r14)
	mfence
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %r12
	movq	$0, -56(%rbp)
	testq	%r12, %r12
	je	.L2647
	.p2align 4,,10
	.p2align 3
.L2650:
	movq	16(%r12), %rax
	movq	$0, 16(%r12)
	movq	%rax, -56(%rbp)
	testq	%rax, %rax
	je	.L2670
.L2648:
	lock subq	$1, -64(%rbp)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%r13, %rax
	jne	.L2671
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_stop@PLT
.L2669:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-56(%rbp), %r12
	movq	$0, -56(%rbp)
	testq	%r12, %r12
	jne	.L2650
.L2647:
	lock subq	$1, -64(%rbp)
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2651
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L2672
.L2641:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2673
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2670:
	.cfi_restore_state
	movq	$0, -48(%rbp)
	jmp	.L2648
	.p2align 4,,10
	.p2align 3
.L2671:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2669
	.p2align 4,,10
	.p2align 3
.L2643:
	movq	2560(%rbx), %rax
	movq	-56(%rbp), %rdi
	movq	$0, 2560(%rbx)
	movq	%rax, -56(%rbp)
	testq	%rdi, %rdi
	jne	.L2668
	jmp	.L2645
.L2673:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8318:
	.size	_ZN4node11Environment21RunAndClearInterruptsEv, .-_ZN4node11Environment21RunAndClearInterruptsEv
	.p2align 4
	.type	_ZZN4node11Environment22RequestInterruptFromV8EvENUlPN2v87IsolateEPvE_4_FUNES3_S4_, @function
_ZZN4node11Environment22RequestInterruptFromV8EvENUlPN2v87IsolateEPvE_4_FUNES3_S4_:
.LFB8325:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	subq	$8, %rsp
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L2677
	movq	$0, 2576(%rdi)
	call	_ZN4node11Environment21RunAndClearInterruptsEv
.L2677:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$8, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8325:
	.size	_ZZN4node11Environment22RequestInterruptFromV8EvENUlPN2v87IsolateEPvE_4_FUNES3_S4_, .-_ZZN4node11Environment22RequestInterruptFromV8EvENUlPN2v87IsolateEPvE_4_FUNES3_S4_
	.section	.rodata.str1.1
.LC550:
	.string	"RunAndClearNativeImmediates"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment27RunAndClearNativeImmediatesEb
	.type	_ZN4node11Environment27RunAndClearNativeImmediatesEb, @function
_ZN4node11Environment27RunAndClearNativeImmediatesEb:
.LFB8319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip), %r12
	testq	%r12, %r12
	je	.L2791
.L2680:
	testb	$5, (%r12)
	jne	.L2792
.L2682:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	leaq	-144(%rbp), %r12
	call	_ZN4node11Environment21RunAndClearInterruptsEv
	leaq	2464(%rbx), %rax
	movq	%rax, -184(%rbp)
	.p2align 4,,10
	.p2align 3
.L2697:
	movq	352(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -96(%rbp)
	movl	$0, -88(%rbp)
	jmp	.L2789
	.p2align 4,,10
	.p2align 3
.L2735:
	addq	$1, %r14
.L2733:
	movq	0(%r13), %rax
	leaq	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_(%rip), %rcx
	movq	16(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L2691
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_stop@PLT
.L2692:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2793
.L2789:
	movq	2472(%rbx), %r13
	movq	$0, 2472(%rbx)
	testq	%r13, %r13
	je	.L2688
	movq	16(%r13), %rax
	movq	$0, 16(%r13)
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	je	.L2689
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	2472(%rbx), %rax
.L2689:
	testq	%rax, %rax
	je	.L2794
.L2690:
	movq	-184(%rbp), %rax
	lock subq	$1, (%rax)
	cmpb	$0, 8(%r13)
	jne	.L2735
	testb	%r15b, %r15b
	jne	.L2692
	jmp	.L2733
	.p2align 4,,10
	.p2align 3
.L2794:
	movq	$0, 2480(%rbx)
	jmp	.L2690
	.p2align 4,,10
	.p2align 3
.L2691:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2692
	.p2align 4,,10
	.p2align 3
.L2793:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	je	.L2795
.L2695:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	jmp	.L2697
	.p2align 4,,10
	.p2align 3
.L2688:
	movq	-184(%rbp), %rax
	lock subq	$1, (%rax)
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	1312(%rbx), %rdx
	subl	%r14d, 4(%rdx)
	jne	.L2699
	cmpb	$0, 2648(%rbx)
	jne	.L2699
	leaq	640(%rbx), %rdi
	call	uv_idle_stop@PLT
	.p2align 4,,10
	.p2align 3
.L2699:
	movq	$0, -176(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -168(%rbp)
	movq	2528(%rbx), %rax
	testq	%rax, %rax
	jne	.L2796
.L2700:
	leaq	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L2720:
	movq	352(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	-168(%rbp), %r13
	movq	%rbx, -96(%rbp)
	movl	$0, -88(%rbp)
	movq	$0, -168(%rbp)
	testq	%r13, %r13
	je	.L2705
	testb	%r15b, %r15b
	jne	.L2706
	.p2align 4,,10
	.p2align 3
.L2711:
	movq	16(%r13), %rax
	movq	$0, 16(%r13)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L2797
.L2707:
	lock subq	$1, -176(%rbp)
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2798
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_stop@PLT
.L2709:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2710
	movq	-168(%rbp), %r13
	movq	$0, -168(%rbp)
	testq	%r13, %r13
	jne	.L2711
.L2705:
	lock subq	$1, -176(%rbp)
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	-168(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2732
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L2732:
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L2799
.L2722:
	testb	$5, (%r12)
	jne	.L2800
.L2678:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2801
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2706:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	$0, 16(%r13)
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L2802
.L2713:
	lock subq	$1, -176(%rbp)
	cmpb	$0, 8(%r13)
	je	.L2715
	movq	0(%r13), %rax
	movq	16(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2714
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_stop@PLT
.L2715:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L2710
	movq	-168(%rbp), %r13
	movq	$0, -168(%rbp)
	testq	%r13, %r13
	jne	.L2706
	jmp	.L2705
	.p2align 4,,10
	.p2align 3
.L2802:
	movq	$0, -160(%rbp)
	jmp	.L2713
	.p2align 4,,10
	.p2align 3
.L2797:
	movq	$0, -160(%rbp)
	jmp	.L2707
	.p2align 4,,10
	.p2align 3
.L2710:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	je	.L2803
.L2718:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L2714:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2715
	.p2align 4,,10
	.p2align 3
.L2798:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2709
	.p2align 4,,10
	.p2align 3
.L2795:
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L2695
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L2695
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2803:
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	je	.L2718
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L2718
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L2792:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2683
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC550(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$98, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2683:
	leaq	-64(%rbp), %r14
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r12
.L2687:
	movq	-8(%r14), %r8
	subq	$8, %r14
	testq	%r8, %r8
	je	.L2684
	movq	(%r8), %rdx
	movq	8(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L2685
	movq	8(%r8), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r8), %rdx
	movq	%rax, (%r8)
	cmpq	%rdx, %rdi
	je	.L2686
	movq	%r8, -184(%rbp)
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r8
.L2686:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L2684:
	cmpq	%r13, %r14
	jne	.L2687
	jmp	.L2682
	.p2align 4,,10
	.p2align 3
.L2791:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2681
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L2681:
	movq	%r12, _ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip)
	mfence
	jmp	.L2680
	.p2align 4,,10
	.p2align 3
.L2800:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2725
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC550(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2725:
	leaq	-64(%rbp), %rbx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L2729:
	movq	-8(%rbx), %r12
	subq	$8, %rbx
	testq	%r12, %r12
	je	.L2726
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2727
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L2728
	call	_ZdlPv@PLT
.L2728:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2726:
	cmpq	%r13, %rbx
	jne	.L2729
	jmp	.L2678
	.p2align 4,,10
	.p2align 3
.L2799:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2723
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L2723:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L2722
	.p2align 4,,10
	.p2align 3
.L2796:
	leaq	2488(%rbx), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	2528(%rbx), %rax
	lock addq	%rax, -176(%rbp)
	movq	-160(%rbp), %rax
	testq	%rax, %rax
	je	.L2701
	movq	2536(%rbx), %rdx
	movq	$0, 2536(%rbx)
	movq	16(%rax), %rdi
	movq	%rdx, 16(%rax)
	testq	%rdi, %rdi
	je	.L2703
.L2790:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2703:
	movq	2544(%rbx), %rax
	movq	%r13, %rdi
	movq	$0, 2544(%rbx)
	movq	%rax, -160(%rbp)
	movq	$0, 2528(%rbx)
	mfence
	call	uv_mutex_unlock@PLT
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L2701:
	movq	2536(%rbx), %rax
	movq	-168(%rbp), %rdi
	movq	$0, 2536(%rbx)
	movq	%rax, -168(%rbp)
	testq	%rdi, %rdi
	jne	.L2790
	jmp	.L2703
	.p2align 4,,10
	.p2align 3
.L2727:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2726
	.p2align 4,,10
	.p2align 3
.L2685:
	movq	%r8, %rdi
	call	*%rdx
	jmp	.L2684
.L2801:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8319:
	.size	_ZN4node11Environment27RunAndClearNativeImmediatesEb, .-_ZN4node11Environment27RunAndClearNativeImmediatesEb
	.section	.rodata.str1.1
.LC551:
	.string	"CheckImmediate"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment14CheckImmediateEP10uv_check_s
	.type	_ZN4node11Environment14CheckImmediateEP10uv_check_s, @function
_ZN4node11Environment14CheckImmediateEP10uv_check_s:
.LFB8330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-520(%rdi), %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip), %r12
	testq	%r12, %r12
	je	.L2882
.L2806:
	testb	$5, (%r12)
	jne	.L2883
.L2808:
	movq	352(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment27RunAndClearNativeImmediatesEb
	movq	1312(%rbx), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L2817
	leaq	1930(%rbx), %r12
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	jne	.L2884
.L2817:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %rdx
	movq	%rdx, %r12
	testq	%rdx, %rdx
	je	.L2885
.L2816:
	testb	$5, (%r12)
	jne	.L2886
.L2804:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2887
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2884:
	.cfi_restore_state
	leaq	2664(%rbx), %r15
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L2817
	xorl	%eax, %eax
	movq	%rax, %xmm1
	.p2align 4,,10
	.p2align 3
.L2818:
	movq	2912(%rbx), %rdx
	movapd	%xmm1, %xmm0
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	2968(%rbx), %rsi
	movq	352(%rbx), %rdi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	je	.L2888
.L2827:
	movq	1312(%rbx), %rax
	cmpl	$1, 8(%rax)
	jne	.L2828
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.L2829
.L2881:
	movq	1312(%rbx), %rax
.L2828:
	movl	4(%rax), %eax
	testl	%eax, %eax
	jne	.L2830
	cmpb	$0, 2648(%rbx)
	jne	.L2830
	leaq	640(%rbx), %rdi
	call	uv_idle_stop@PLT
.L2830:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L2889
.L2832:
	testb	$5, (%r12)
	je	.L2804
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2835
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC551(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2835:
	leaq	-64(%rbp), %rbx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L2839:
	movq	-8(%rbx), %r12
	subq	$8, %rbx
	testq	%r12, %r12
	je	.L2836
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2837
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L2838
	call	_ZdlPv@PLT
.L2838:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2836:
	cmpq	%r13, %rbx
	jne	.L2839
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L2883:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2809
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC551(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$98, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2809:
	leaq	-64(%rbp), %r15
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
.L2813:
	movq	-8(%r15), %r12
	subq	$8, %r15
	testq	%r12, %r12
	je	.L2810
	movq	(%r12), %rdx
	movq	8(%rdx), %rdx
	cmpq	%r14, %rdx
	jne	.L2811
	movq	8(%r12), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r12), %rdx
	movq	%rax, (%r12)
	cmpq	%rdx, %rdi
	je	.L2812
	call	_ZdlPv@PLT
.L2812:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2810:
	cmpq	%r13, %r15
	jne	.L2813
	jmp	.L2808
	.p2align 4,,10
	.p2align 3
.L2882:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2807
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L2807:
	movq	%r12, _ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip)
	mfence
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2886:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r13
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2822
	subq	$8, %rsp
	leaq	-80(%rbp), %r13
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC551(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r13
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L2822:
	leaq	-64(%rbp), %rbx
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L2826:
	movq	-8(%rbx), %r12
	subq	$8, %rbx
	testq	%r12, %r12
	je	.L2823
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L2824
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L2825
	call	_ZdlPv@PLT
.L2825:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L2823:
	cmpq	%r13, %rbx
	jne	.L2826
	jmp	.L2804
	.p2align 4,,10
	.p2align 3
.L2829:
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L2818
	jmp	.L2881
	.p2align 4,,10
	.p2align 3
.L2888:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	pxor	%xmm1, %xmm1
	jmp	.L2827
	.p2align 4,,10
	.p2align 3
.L2885:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2819
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L2819:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L2816
	.p2align 4,,10
	.p2align 3
.L2889:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2833
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L2833:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L2811:
	movq	%r12, %rdi
	call	*%rdx
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L2824:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2823
.L2837:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2836
.L2887:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8330:
	.size	_ZN4node11Environment14CheckImmediateEP10uv_check_s, .-_ZN4node11Environment14CheckImmediateEP10uv_check_s
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment22RequestInterruptFromV8Ev
	.type	_ZN4node11Environment22RequestInterruptFromV8Ev, @function
_ZN4node11Environment22RequestInterruptFromV8Ev:
.LFB8321:
	.cfi_startproc
	endbr64
	cmpq	$0, 2576(%rdi)
	je	.L2895
	ret
	.p2align 4,,10
	.p2align 3
.L2895:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	movl	$8, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	352(%rbx), %rdi
	leaq	_ZZN4node11Environment22RequestInterruptFromV8EvENUlPN2v87IsolateEPvE_4_FUNES3_S4_(%rip), %rsi
	movq	%rbx, (%rax)
	movq	%rax, %rdx
	movq	%rax, 2576(%rbx)
	addq	$8, %rsp
	popq	%rbx
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate16RequestInterruptEPFvPS0_PvES2_@PLT
	.cfi_endproc
.LFE8321:
	.size	_ZN4node11Environment22RequestInterruptFromV8Ev, .-_ZN4node11Environment22RequestInterruptFromV8Ev
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment13ScheduleTimerEl
	.type	_ZN4node11Environment13ScheduleTimerEl, @function
_ZN4node11Environment13ScheduleTimerEl:
.LFB8327:
	.cfi_startproc
	endbr64
	cmpb	$0, 2648(%rdi)
	movq	%rsi, %rdx
	je	.L2898
	ret
	.p2align 4,,10
	.p2align 3
.L2898:
	addq	$368, %rdi
	xorl	%ecx, %ecx
	leaq	_ZN4node11Environment9RunTimersEP10uv_timer_s(%rip), %rsi
	jmp	uv_timer_start@PLT
	.cfi_endproc
.LFE8327:
	.size	_ZN4node11Environment13ScheduleTimerEl, .-_ZN4node11Environment13ScheduleTimerEl
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment14ToggleTimerRefEb
	.type	_ZN4node11Environment14ToggleTimerRefEb, @function
_ZN4node11Environment14ToggleTimerRefEb:
.LFB8328:
	.cfi_startproc
	endbr64
	cmpb	$0, 2648(%rdi)
	jne	.L2899
	addq	$368, %rdi
	testb	%sil, %sil
	je	.L2901
	jmp	uv_ref@PLT
	.p2align 4,,10
	.p2align 3
.L2901:
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L2899:
	ret
	.cfi_endproc
.LFE8328:
	.size	_ZN4node11Environment14ToggleTimerRefEb, .-_ZN4node11Environment14ToggleTimerRefEb
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment18ToggleImmediateRefEb
	.type	_ZN4node11Environment18ToggleImmediateRefEb, @function
_ZN4node11Environment18ToggleImmediateRefEb:
.LFB8331:
	.cfi_startproc
	endbr64
	cmpb	$0, 2648(%rdi)
	jne	.L2902
	addq	$640, %rdi
	testb	%sil, %sil
	je	.L2904
	leaq	_ZZN4node11Environment18ToggleImmediateRefEbENUlP9uv_idle_sE_4_FUNES2_(%rip), %rsi
	jmp	uv_idle_start@PLT
	.p2align 4,,10
	.p2align 3
.L2904:
	jmp	uv_idle_stop@PLT
	.p2align 4,,10
	.p2align 3
.L2902:
	ret
	.cfi_endproc
.LFE8331:
	.size	_ZN4node11Environment18ToggleImmediateRefEb, .-_ZN4node11Environment18ToggleImmediateRefEb
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment6GetNowEv
	.type	_ZN4node11Environment6GetNowEv, @function
_ZN4node11Environment6GetNowEv:
.LFB8335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	360(%rdi), %rax
	movq	2360(%rax), %rdi
	call	uv_update_time@PLT
	movq	360(%rbx), %rax
	movq	2360(%rax), %rdi
	call	uv_now@PLT
	movq	%rax, %rsi
	movq	1376(%rbx), %rax
	cmpq	%rax, %rsi
	jb	.L2912
	subq	%rax, %rsi
	movl	$4294967295, %eax
	movq	352(%rbx), %rdi
	cmpq	%rax, %rsi
	jbe	.L2913
	testq	%rsi, %rsi
	js	.L2909
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
.L2910:
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2913:
	.cfi_restore_state
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2909:
	.cfi_restore_state
	movq	%rsi, %rax
	andl	$1, %esi
	pxor	%xmm0, %xmm0
	shrq	%rax
	orq	%rsi, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L2910
	.p2align 4,,10
	.p2align 3
.L2912:
	leaq	_ZZN4node11Environment6GetNowEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8335:
	.size	_ZN4node11Environment6GetNowEv, .-_ZN4node11Environment6GetNowEv
	.p2align 4
	.globl	_ZN4node20CollectExceptionInfoEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEEiPKcS7_S7_S7_S7_
	.type	_ZN4node20CollectExceptionInfoEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEEiPKcS7_S7_S7_S7_, @function
_ZN4node20CollectExceptionInfoEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEEiPKcS7_S7_S7_S7_:
.LFB8336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	%edx, %esi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	16(%rbp), %rbx
	movq	24(%rbp), %r15
	movq	%rcx, -64(%rbp)
	movq	352(%rdi), %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%r13), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r11
	testb	%al, %al
	je	.L2943
.L2915:
	movq	352(%r13), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r11, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2944
.L2916:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	movq	328(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2945
.L2917:
	testq	%r14, %r14
	je	.L2918
	movq	352(%r13), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2946
.L2919:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	movq	1024(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2947
.L2918:
	testq	%rbx, %rbx
	je	.L2921
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	352(%r13), %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	call	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2948
.L2922:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	movq	1320(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2949
.L2921:
	testq	%r15, %r15
	je	.L2924
	movq	%r15, %rdi
	call	strlen@PLT
	movq	352(%r13), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2950
.L2925:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	movq	448(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2951
.L2924:
	movq	-56(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L2914
	movq	352(%r13), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2952
.L2928:
	movq	360(%r13), %rax
	movq	3280(%r13), %rsi
	movq	%r12, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2953
.L2914:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2953:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L2946:
	.cfi_restore_state
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L2919
	.p2align 4,,10
	.p2align 3
.L2947:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2918
	.p2align 4,,10
	.p2align 3
.L2948:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L2922
	.p2align 4,,10
	.p2align 3
.L2949:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2921
	.p2align 4,,10
	.p2align 3
.L2950:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L2925
	.p2align 4,,10
	.p2align 3
.L2951:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2924
	.p2align 4,,10
	.p2align 3
.L2952:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L2928
	.p2align 4,,10
	.p2align 3
.L2945:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2917
	.p2align 4,,10
	.p2align 3
.L2943:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-64(%rbp), %r11
	jmp	.L2915
	.p2align 4,,10
	.p2align 3
.L2944:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rcx
	jmp	.L2916
	.cfi_endproc
.LFE8336:
	.size	_ZN4node20CollectExceptionInfoEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEEiPKcS7_S7_S7_S7_, .-_ZN4node20CollectExceptionInfoEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEEiPKcS7_S7_S7_S7_
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_
	.type	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_, @function
_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_:
.LFB8337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	%r9, -56(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	testl	%r12d, %r12d
	je	.L2954
	cmpb	$1, %al
	je	.L2964
.L2954:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2964:
	.cfi_restore_state
	movl	%r12d, %edi
	call	uv_err_name@PLT
	movq	%rax, %rcx
	testq	%rbx, %rbx
	je	.L2956
	cmpb	$0, (%rbx)
	jne	.L2959
.L2956:
	movl	%r12d, %edi
	movq	%rcx, -64(%rbp)
	call	uv_strerror@PLT
	movq	-64(%rbp), %rcx
	movq	%rax, %rbx
.L2959:
	pushq	16(%rbp)
	movl	%r12d, %edx
	movq	%rbx, %r9
	movq	%r14, %r8
	pushq	-56(%rbp)
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN4node20CollectExceptionInfoEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEEiPKcS7_S7_S7_S7_
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8337:
	.size	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_, .-_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_
	.align 2
	.p2align 4
	.globl	_ZN4node10AsyncHooks20grow_async_ids_stackEv
	.type	_ZN4node10AsyncHooks20grow_async_ids_stackEv, @function
_ZN4node10AsyncHooks20grow_async_ids_stackEv:
.LFB8341:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-144(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	8(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	leaq	(%rax,%rax,2), %r12
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%rbx), %rax
	leaq	0(,%rax,8), %rdx
	movabsq	$2305843009213693951, %rax
	andq	%r12, %rax
	cmpq	%rax, %r12
	jne	.L2989
	movq	8(%rbx), %rdi
	leaq	0(,%r12,8), %rsi
	movq	%rdx, -160(%rbp)
	leaq	-112(%rbp), %r15
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, -152(%rbp)
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %r14
	movq	32(%rbx), %rsi
	movq	-160(%rbp), %rdx
	movq	%r14, %rdi
	call	memcpy@PLT
	movq	-152(%rbp), %r8
	movq	24(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r8, %rdi
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2990
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	40(%rbx), %rdi
	movq	%rax, -112(%rbp)
	testq	%rdi, %rdi
	je	.L2970
.L2969:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 40(%rbx)
	movq	-112(%rbp), %rax
.L2970:
	testq	%rax, %rax
	je	.L2968
	movq	%rax, 40(%rbx)
	leaq	40(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN2v82V819MoveGlobalReferenceEPPmS2_@PLT
.L2968:
	movq	%r12, 16(%rbx)
	movq	%r13, %rdi
	leaq	-1144(%rbx), %r12
	movq	%r14, 32(%rbx)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	40(%rbx), %rcx
	movq	2712(%r12), %r13
	testq	%rcx, %rcx
	je	.L2972
	movq	(%rcx), %rsi
	movq	8(%rbx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
.L2972:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	216(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2991
.L2965:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2992
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2990:
	.cfi_restore_state
	movq	40(%rbx), %rdi
	movq	$0, -112(%rbp)
	testq	%rdi, %rdi
	jne	.L2969
	jmp	.L2968
	.p2align 4,,10
	.p2align 3
.L2989:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2991:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2965
.L2992:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8341:
	.size	_ZN4node10AsyncHooks20grow_async_ids_stackEv, .-_ZN4node10AsyncHooks20grow_async_ids_stackEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment24stop_sub_worker_contextsEv
	.type	_ZN4node11Environment24stop_sub_worker_contextsEv, @function
_ZN4node11Environment24stop_sub_worker_contextsEv:
.LFB8343:
	.cfi_startproc
	endbr64
	cmpq	$0, 1968(%rdi)
	je	.L3018
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	1960(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L2994:
	movq	1960(%rbx), %rax
	movq	1952(%rbx), %rcx
	xorl	%edx, %edx
	movq	1944(%rbx), %r14
	movq	8(%rax), %r12
	movq	%r12, %rax
	divq	%rcx
	leaq	(%r14,%rdx,8), %r15
	movq	%rdx, %r11
	movq	(%r15), %r9
	testq	%r9, %r9
	je	.L2995
	movq	(%r9), %rdi
	movq	%r9, %r10
	movq	8(%rdi), %rsi
	jmp	.L2997
	.p2align 4,,10
	.p2align 3
.L3021:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L2995
	movq	8(%r8), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L2995
	movq	%r8, %rdi
.L2997:
	cmpq	%rsi, %r12
	jne	.L3021
	movq	(%rdi), %rsi
	cmpq	%r10, %r9
	je	.L3022
	testq	%rsi, %rsi
	je	.L2999
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L2999
	movq	%r10, (%r14,%rdx,8)
	movq	(%rdi), %rsi
.L2999:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 1968(%rbx)
.L2995:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN4node6worker6Worker4ExitEi@PLT
	movq	%r12, %rdi
	call	_ZN4node6worker6Worker10JoinThreadEv@PLT
	cmpq	$0, 1968(%rbx)
	jne	.L2994
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3022:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L3004
	movq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L2999
	movq	%r10, (%r14,%rdx,8)
	movq	(%r15), %rax
	cmpq	%r13, %rax
	je	.L3023
.L3000:
	movq	$0, (%r15)
	movq	(%rdi), %rsi
	jmp	.L2999
	.p2align 4,,10
	.p2align 3
.L3004:
	movq	%r10, %rax
	cmpq	%r13, %rax
	jne	.L3000
.L3023:
	movq	%rsi, 1960(%rbx)
	jmp	.L3000
	.p2align 4,,10
	.p2align 3
.L3018:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.cfi_endproc
.LFE8343:
	.size	_ZN4node11Environment24stop_sub_worker_contextsEv, .-_ZN4node11Environment24stop_sub_worker_contextsEv
	.section	.rodata.str1.1
.LC552:
	.string	"(node:%d) "
.LC553:
	.string	"(node:%d, thread:%lu) "
	.section	.rodata.str1.8
	.align 8
.LC554:
	.string	"WARNING: Exited the environment with code %d\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment4ExitEi
	.type	_ZN4node11Environment4ExitEi, @function
_ZN4node11Environment4ExitEi:
.LFB8342:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	1648(%rdi), %r13
	movq	1640(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L3025
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	leaq	8(%r13), %rax
	testq	%rbx, %rbx
	je	.L3026
	lock addl	$1, (%rax)
	movzbl	467(%rdx), %r15d
	testq	%rbx, %rbx
	je	.L3045
.L3036:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L3046
.L3029:
	testb	%r15b, %r15b
	jne	.L3047
.L3032:
	testb	$1, 1932(%r12)
	jne	.L3048
	movq	2368(%r12), %rdi
	movl	%r14d, %esi
	call	_ZN4node6worker6Worker4ExitEi@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3049
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3026:
	.cfi_restore_state
	addl	$1, 8(%r13)
	movzbl	467(%rdx), %r15d
	testq	%rbx, %rbx
	jne	.L3036
.L3045:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L3029
.L3046:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L3030
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L3031:
	cmpl	$1, %eax
	jne	.L3029
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L3029
	.p2align 4,,10
	.p2align 3
.L3047:
	leaq	-80(%rbp), %r13
	movq	352(%r12), %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testb	$1, 1932(%r12)
	jne	.L3050
	movq	1936(%r12), %rbx
	call	uv_os_getpid@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC553(%rip), %rdx
	movl	%eax, %ecx
	movq	%rbx, %r8
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
.L3034:
	movq	stderr(%rip), %rdi
	movl	%r14d, %ecx
	movl	$1, %esi
	xorl	%eax, %eax
	leaq	.LC554(%rip), %rdx
	call	__fprintf_chk@PLT
	movl	$127, %edx
	movl	$10, %esi
	movq	352(%r12), %rdi
	call	_ZN2v810StackTrace17CurrentStackTraceEPNS_7IsolateEiNS0_17StackTraceOptionsE@PLT
	movq	352(%r12), %rdi
	movq	%rax, %rsi
	call	_ZN4node15PrintStackTraceEPN2v87IsolateENS0_5LocalINS0_10StackTraceEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L3032
	.p2align 4,,10
	.p2align 3
.L3025:
	movzbl	467(%rdx), %r15d
	jmp	.L3029
	.p2align 4,,10
	.p2align 3
.L3050:
	call	uv_os_getpid@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC552(%rip), %rdx
	movl	%eax, %ecx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	jmp	.L3034
	.p2align 4,,10
	.p2align 3
.L3030:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L3031
.L3049:
	call	__stack_chk_fail@PLT
.L3048:
	movq	%r12, %rdi
	movb	$0, 1930(%r12)
	mfence
	call	_ZN4node11Environment24stop_sub_worker_contextsEv
	leaq	_ZN4node11per_process11v8_platformE(%rip), %rdi
	call	_ZN4node10V8Platform7DisposeEv
	movl	%r14d, %edi
	call	exit@PLT
	.cfi_endproc
.LFE8342:
	.size	_ZN4node11Environment4ExitEi, .-_ZN4node11Environment4ExitEi
	.align 2
	.p2align 4
	.globl	_ZNK4node11Environment17worker_parent_envEv
	.type	_ZNK4node11Environment17worker_parent_envEv, @function
_ZNK4node11Environment17worker_parent_envEv:
.LFB8344:
	.cfi_startproc
	endbr64
	movq	2368(%rdi), %rax
	testq	%rax, %rax
	je	.L3051
	movq	16(%rax), %rax
.L3051:
	ret
	.cfi_endproc
.LFE8344:
	.size	_ZNK4node11Environment17worker_parent_envEv, .-_ZNK4node11Environment17worker_parent_envEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment10ReallocateEPcmm
	.type	_ZN4node11Environment10ReallocateEPcmm, @function
_ZN4node11Environment10ReallocateEPcmm:
.LFB8352:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	cmpq	%rcx, %rdx
	je	.L3060
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movq	%rdx, %r14
	movq	%rcx, %r12
	cmpb	$0, 2384(%rax)
	jne	.L3065
	movq	2368(%rax), %rdi
	movq	%rcx, %rsi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L3056
	cmpq	%r12, %r14
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	cmovbe	%r14, %rdx
	call	memcpy@PLT
	cmpq	%r12, %r14
	jb	.L3066
.L3059:
	movq	360(%rbx), %rax
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L3056:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3065:
	.cfi_restore_state
	movq	2376(%rax), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3060:
	.cfi_restore_state
	movq	%rsi, %r13
	jmp	.L3056
	.p2align 4,,10
	.p2align 3
.L3066:
	movq	%r12, %rdx
	leaq	0(%r13,%r14), %rdi
	xorl	%esi, %esi
	subq	%r14, %rdx
	call	memset@PLT
	jmp	.L3059
	.cfi_endproc
.LFE8352:
	.size	_ZN4node11Environment10ReallocateEPcmm, .-_ZN4node11Environment10ReallocateEPcmm
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment17RunWeakRefCleanupEv
	.type	_ZN4node11Environment17RunWeakRefCleanupEv, @function
_ZN4node11Environment17RunWeakRefCleanupEv:
.LFB8379:
	.cfi_startproc
	endbr64
	movq	352(%rdi), %rdi
	jmp	_ZN2v87Isolate16ClearKeptObjectsEv@PLT
	.cfi_endproc
.LFE8379:
	.size	_ZN4node11Environment17RunWeakRefCleanupEv, .-_ZN4node11Environment17RunWeakRefCleanupEv
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment25CleanupFinalizationGroupsEv
	.type	_ZN4node11Environment25CleanupFinalizationGroupsEv, @function
_ZN4node11Environment25CleanupFinalizationGroupsEv:
.LFB8380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-128(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%rbx, -80(%rbp)
	movq	2016(%rbx), %rax
	movl	$0, -72(%rbp)
	cmpq	%rax, 2048(%rbx)
	je	.L3097
	leaq	1930(%rbx), %rax
	movq	%rax, -168(%rbp)
.L3083:
	movq	-168(%rbp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	jne	.L3098
.L3097:
	movq	%r12, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3099
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3098:
	.cfi_restore_state
	movzbl	2664(%rbx), %eax
	testb	%al, %al
	jne	.L3097
	movq	2016(%rbx), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L3100
	movq	352(%rbx), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	2016(%rbx), %rdx
	movq	%rax, %r15
	movq	2032(%rbx), %rax
	movq	(%rdx), %rdi
	subq	$8, %rax
	cmpq	%rax, %rdx
	je	.L3073
	testq	%rdi, %rdi
	je	.L3074
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	2016(%rbx), %rdx
.L3074:
	addq	$8, %rdx
	movq	%rdx, 2016(%rbx)
.L3075:
	movq	%r15, %rdi
	call	_ZN2v817FinalizationGroup7CleanupENS_5LocalIS0_EE@PLT
	testb	%al, %al
	je	.L3078
	shrw	$8, %ax
	je	.L3078
	movq	2016(%rbx), %rax
	cmpq	%rax, 2048(%rbx)
	jne	.L3083
	jmp	.L3097
	.p2align 4,,10
	.p2align 3
.L3078:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L3101
.L3081:
	leaq	1000(%rbx), %rdi
	call	uv_async_send@PLT
	jmp	.L3097
	.p2align 4,,10
	.p2align 3
.L3100:
	movq	2032(%rbx), %rax
	xorl	%r15d, %r15d
	subq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L3074
.L3076:
	movq	2024(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	2040(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 2016(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 2032(%rbx)
	jmp	.L3075
	.p2align 4,,10
	.p2align 3
.L3101:
	movq	%r12, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L3081
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	jmp	.L3081
.L3099:
	call	__stack_chk_fail@PLT
.L3073:
	testq	%rdi, %rdi
	je	.L3076
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	jmp	.L3076
	.cfi_endproc
.LFE8380:
	.size	_ZN4node11Environment25CleanupFinalizationGroupsEv, .-_ZN4node11Environment25CleanupFinalizationGroupsEv
	.p2align 4
	.type	_ZZN4node11Environment15InitializeLibuvEbENUlP10uv_async_sE_4_FUNES2_, @function
_ZZN4node11Environment15InitializeLibuvEbENUlP10uv_async_sE_4_FUNES2_:
.LFB8287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-1000(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN4node11Environment25CleanupFinalizationGroupsEv
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11Environment27RunAndClearNativeImmediatesEb
	.cfi_endproc
.LFE8287:
	.size	_ZZN4node11Environment15InitializeLibuvEbENUlP10uv_async_sE_4_FUNES2_, .-_ZZN4node11Environment15InitializeLibuvEbENUlP10uv_async_sE_4_FUNES2_
	.section	.text._ZN4node7tracing11TracedValueD2Ev,"axG",@progbits,_ZN4node7tracing11TracedValueD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing11TracedValueD2Ev
	.type	_ZN4node7tracing11TracedValueD2Ev, @function
_ZN4node7tracing11TracedValueD2Ev:
.LFB10431:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	addq	$24, %rdi
	movq	%rax, -24(%rdi)
	cmpq	%rdi, %r8
	je	.L3104
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L3104:
	ret
	.cfi_endproc
.LFE10431:
	.size	_ZN4node7tracing11TracedValueD2Ev, .-_ZN4node7tracing11TracedValueD2Ev
	.weak	_ZN4node7tracing11TracedValueD1Ev
	.set	_ZN4node7tracing11TracedValueD1Ev,_ZN4node7tracing11TracedValueD2Ev
	.section	.text._ZN4node22NodeTraceStateObserverD2Ev,"axG",@progbits,_ZN4node22NodeTraceStateObserverD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node22NodeTraceStateObserverD2Ev
	.type	_ZN4node22NodeTraceStateObserverD2Ev, @function
_ZN4node22NodeTraceStateObserverD2Ev:
.LFB10473:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10473:
	.size	_ZN4node22NodeTraceStateObserverD2Ev, .-_ZN4node22NodeTraceStateObserverD2Ev
	.weak	_ZN4node22NodeTraceStateObserverD1Ev
	.set	_ZN4node22NodeTraceStateObserverD1Ev,_ZN4node22NodeTraceStateObserverD2Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB10492:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L3122
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L3111:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L3109
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L3107
.L3110:
	movq	%rbx, %r12
	jmp	.L3111
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3110
.L3107:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3122:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE10492:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB11234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L3126
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L3136
.L3152:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L3137:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3126:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L3150
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L3151
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L3129:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L3131
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L3132
	.p2align 4,,10
	.p2align 3
.L3133:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L3134:
	testq	%rsi, %rsi
	je	.L3131
.L3132:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L3133
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L3139
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L3132
	.p2align 4,,10
	.p2align 3
.L3131:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L3135
	call	_ZdlPv@PLT
.L3135:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L3152
.L3136:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L3138
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L3138:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L3137
	.p2align 4,,10
	.p2align 3
.L3139:
	movq	%rdx, %rdi
	jmp	.L3134
	.p2align 4,,10
	.p2align 3
.L3150:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L3129
.L3151:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11234:
	.size	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC555:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB11238:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L3162
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L3163
.L3155:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3163:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L3164
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L3165
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L3160
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L3160:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L3158:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L3155
	.p2align 4,,10
	.p2align 3
.L3164:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L3157
	cmpq	%r14, %rsi
	je	.L3158
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L3158
	.p2align 4,,10
	.p2align 3
.L3157:
	cmpq	%r14, %rsi
	je	.L3158
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L3158
.L3162:
	leaq	.LC555(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L3165:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE11238:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata.str1.1
.LC556:
	.string	"handle_onclose_symbol"
.LC557:
	.string	"oninit_symbol"
.LC558:
	.string	"onpskexchange_symbol"
.LC559:
	.string	"address_string"
.LC560:
	.string	"aliases_string"
.LC561:
	.string	"args_string"
.LC562:
	.string	"asn1curve_string"
.LC563:
	.string	"async_ids_stack_string"
.LC564:
	.string	"bits_string"
.LC565:
	.string	"buffer_string"
.LC566:
	.string	"bytes_parsed_string"
.LC567:
	.string	"bytes_read_string"
.LC568:
	.string	"bytes_written_string"
.LC569:
	.string	"cached_data_produced_string"
.LC570:
	.string	"cached_data_rejected_string"
.LC571:
	.string	"cached_data_string"
.LC572:
	.string	"cache_key_string"
.LC573:
	.string	"change_string"
.LC574:
	.string	"channel_string"
	.section	.rodata.str1.8
	.align 8
.LC575:
	.string	"chunks_sent_since_last_write_string"
	.section	.rodata.str1.1
.LC576:
	.string	"clone_unsupported_type_str"
.LC577:
	.string	"code_string"
.LC578:
	.string	"commonjs_string"
.LC579:
	.string	"config_string"
.LC580:
	.string	"constants_string"
.LC581:
	.string	"crypto_dh_string"
.LC582:
	.string	"crypto_dsa_string"
.LC583:
	.string	"crypto_ec_string"
.LC584:
	.string	"crypto_ed25519_string"
.LC585:
	.string	"crypto_ed448_string"
.LC586:
	.string	"crypto_x25519_string"
.LC587:
	.string	"crypto_x448_string"
.LC588:
	.string	"crypto_rsa_string"
.LC589:
	.string	"crypto_rsa_pss_string"
.LC590:
	.string	"cwd_string"
.LC591:
	.string	"data_string"
.LC592:
	.string	"dest_string"
.LC593:
	.string	"destroyed_string"
.LC594:
	.string	"detached_string"
.LC595:
	.string	"dh_string"
.LC596:
	.string	"dns_a_string"
.LC597:
	.string	"dns_aaaa_string"
.LC598:
	.string	"dns_cname_string"
.LC599:
	.string	"dns_mx_string"
.LC600:
	.string	"dns_naptr_string"
.LC601:
	.string	"dns_ns_string"
.LC602:
	.string	"dns_ptr_string"
.LC603:
	.string	"dns_soa_string"
.LC604:
	.string	"dns_srv_string"
.LC605:
	.string	"dns_txt_string"
.LC606:
	.string	"done_string"
.LC607:
	.string	"duration_string"
.LC608:
	.string	"ecdh_string"
.LC609:
	.string	"emit_warning_string"
.LC610:
	.string	"empty_object_string"
.LC611:
	.string	"encoding_string"
.LC612:
	.string	"entries_string"
.LC613:
	.string	"entry_type_string"
.LC614:
	.string	"env_pairs_string"
.LC615:
	.string	"env_var_settings_string"
.LC616:
	.string	"errno_string"
.LC617:
	.string	"error_string"
.LC618:
	.string	"exchange_string"
.LC619:
	.string	"exit_code_string"
.LC620:
	.string	"expire_string"
.LC621:
	.string	"exponent_string"
.LC622:
	.string	"exports_string"
.LC623:
	.string	"ext_key_usage_string"
.LC624:
	.string	"external_stream_string"
.LC625:
	.string	"family_string"
.LC626:
	.string	"fatal_exception_string"
.LC627:
	.string	"fd_string"
.LC628:
	.string	"fields_string"
.LC629:
	.string	"file_string"
.LC630:
	.string	"fingerprint256_string"
.LC631:
	.string	"fingerprint_string"
.LC632:
	.string	"flags_string"
.LC633:
	.string	"fragment_string"
.LC634:
	.string	"function_string"
.LC635:
	.string	"get_data_clone_error_string"
	.section	.rodata.str1.8
	.align 8
.LC636:
	.string	"get_shared_array_buffer_id_string"
	.section	.rodata.str1.1
.LC637:
	.string	"gid_string"
.LC638:
	.string	"h2_string"
.LC639:
	.string	"handle_string"
.LC640:
	.string	"help_text_string"
.LC641:
	.string	"homedir_string"
.LC642:
	.string	"host_string"
.LC643:
	.string	"hostmaster_string"
.LC644:
	.string	"http_1_1_string"
.LC645:
	.string	"identity_string"
.LC646:
	.string	"ignore_string"
.LC647:
	.string	"infoaccess_string"
.LC648:
	.string	"inherit_string"
.LC649:
	.string	"input_string"
.LC650:
	.string	"internal_binding_string"
.LC651:
	.string	"internal_string"
.LC652:
	.string	"ipv4_string"
.LC653:
	.string	"ipv6_string"
.LC654:
	.string	"isclosing_string"
.LC655:
	.string	"issuer_string"
.LC656:
	.string	"issuercert_string"
.LC657:
	.string	"kill_signal_string"
.LC658:
	.string	"kind_string"
.LC659:
	.string	"library_string"
.LC660:
	.string	"mac_string"
.LC661:
	.string	"max_buffer_string"
	.section	.rodata.str1.8
	.align 8
.LC662:
	.string	"message_port_constructor_string"
	.section	.rodata.str1.1
.LC663:
	.string	"message_port_string"
.LC664:
	.string	"message_string"
.LC665:
	.string	"minttl_string"
.LC666:
	.string	"module_string"
.LC667:
	.string	"modulus_string"
.LC668:
	.string	"name_string"
.LC669:
	.string	"netmask_string"
.LC670:
	.string	"next_string"
.LC671:
	.string	"nistcurve_string"
.LC672:
	.string	"node_string"
.LC673:
	.string	"nsname_string"
.LC674:
	.string	"ocsp_request_string"
.LC675:
	.string	"oncertcb_string"
.LC676:
	.string	"onchange_string"
.LC677:
	.string	"onclienthello_string"
.LC678:
	.string	"oncomplete_string"
.LC679:
	.string	"onconnection_string"
.LC680:
	.string	"ondone_string"
.LC681:
	.string	"onerror_string"
.LC682:
	.string	"onexit_string"
.LC683:
	.string	"onhandshakedone_string"
.LC684:
	.string	"onhandshakestart_string"
.LC685:
	.string	"onkeylog_string"
.LC686:
	.string	"onmessage_string"
.LC687:
	.string	"onnewsession_string"
.LC688:
	.string	"onocspresponse_string"
.LC689:
	.string	"onreadstart_string"
.LC690:
	.string	"onreadstop_string"
.LC691:
	.string	"onshutdown_string"
.LC692:
	.string	"onsignal_string"
.LC693:
	.string	"onunpipe_string"
.LC694:
	.string	"onwrite_string"
.LC695:
	.string	"openssl_error_stack"
.LC696:
	.string	"options_string"
.LC697:
	.string	"order_string"
.LC698:
	.string	"output_string"
.LC699:
	.string	"parse_error_string"
.LC700:
	.string	"password_string"
.LC701:
	.string	"path_string"
.LC702:
	.string	"pending_handle_string"
.LC703:
	.string	"pid_string"
.LC704:
	.string	"pipe_source_string"
.LC705:
	.string	"pipe_string"
.LC706:
	.string	"pipe_target_string"
.LC707:
	.string	"port1_string"
.LC708:
	.string	"port2_string"
.LC709:
	.string	"port_string"
.LC710:
	.string	"preference_string"
.LC711:
	.string	"primordials_string"
.LC712:
	.string	"priority_string"
.LC713:
	.string	"process_string"
.LC714:
	.string	"promise_string"
.LC715:
	.string	"psk_string"
.LC716:
	.string	"pubkey_string"
.LC717:
	.string	"query_string"
.LC718:
	.string	"raw_string"
.LC719:
	.string	"read_host_object_string"
.LC720:
	.string	"readable_string"
.LC721:
	.string	"reason_string"
.LC722:
	.string	"refresh_string"
.LC723:
	.string	"regexp_string"
.LC724:
	.string	"rename_string"
.LC725:
	.string	"replacement_string"
.LC726:
	.string	"require_string"
.LC727:
	.string	"retry_string"
.LC728:
	.string	"scheme_string"
.LC729:
	.string	"scopeid_string"
.LC730:
	.string	"serial_number_string"
.LC731:
	.string	"serial_string"
.LC732:
	.string	"servername_string"
.LC733:
	.string	"service_string"
.LC734:
	.string	"session_id_string"
.LC735:
	.string	"shell_string"
.LC736:
	.string	"signal_string"
.LC737:
	.string	"sink_string"
.LC738:
	.string	"size_string"
.LC739:
	.string	"sni_context_err_string"
.LC740:
	.string	"sni_context_string"
.LC741:
	.string	"source_string"
.LC742:
	.string	"stack_string"
.LC743:
	.string	"standard_name_string"
.LC744:
	.string	"start_time_string"
.LC745:
	.string	"status_string"
.LC746:
	.string	"stdio_string"
.LC747:
	.string	"subject_string"
.LC748:
	.string	"subjectaltname_string"
.LC749:
	.string	"syscall_string"
.LC750:
	.string	"target_string"
.LC751:
	.string	"thread_id_string"
.LC752:
	.string	"ticketkeycallback_string"
.LC753:
	.string	"timeout_string"
.LC754:
	.string	"tls_ticket_string"
.LC755:
	.string	"transfer_string"
.LC756:
	.string	"ttl_string"
.LC757:
	.string	"type_string"
.LC758:
	.string	"uid_string"
.LC759:
	.string	"unknown_string"
.LC760:
	.string	"url_special_ftp_string"
.LC761:
	.string	"url_special_file_string"
.LC762:
	.string	"url_special_gopher_string"
.LC763:
	.string	"url_special_http_string"
.LC764:
	.string	"url_special_https_string"
.LC765:
	.string	"url_special_ws_string"
.LC766:
	.string	"url_special_wss_string"
.LC767:
	.string	"url_string"
.LC768:
	.string	"username_string"
.LC769:
	.string	"valid_from_string"
.LC770:
	.string	"valid_to_string"
.LC771:
	.string	"value_string"
.LC772:
	.string	"verify_error_string"
.LC773:
	.string	"version_string"
.LC774:
	.string	"weight_string"
.LC775:
	.string	"windows_hide_string"
	.section	.rodata.str1.8
	.align 8
.LC776:
	.string	"windows_verbatim_arguments_string"
	.section	.rodata.str1.1
.LC777:
	.string	"wrap_string"
.LC778:
	.string	"writable_string"
.LC779:
	.string	"write_host_object_string"
.LC780:
	.string	"write_queue_size_string"
.LC781:
	.string	"x_forwarded_string"
.LC782:
	.string	"zero_return_string"
.LC783:
	.string	"async_wrap_providers"
.LC784:
	.string	"NodeArrayBufferAllocator"
.LC785:
	.string	"node_allocator"
.LC786:
	.string	"allocator"
.LC787:
	.string	"v8::ArrayBuffer::Allocator"
.LC788:
	.string	"MultiIsolatePlatform"
.LC789:
	.string	"platform"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node11IsolateData10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node11IsolateData10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node11IsolateData10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	128(%rdi), %rax
	testq	%rax, %rax
	je	.L3167
	movq	8(%rsi), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3362
	cmpq	72(%rbx), %rcx
	je	.L3623
.L3169:
	movq	-8(%rcx), %rsi
.L3168:
	leaq	.LC556(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3167:
	movq	136(%r12), %rax
	testq	%rax, %rax
	je	.L3170
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3363
	cmpq	72(%rbx), %rcx
	je	.L3624
.L3172:
	movq	-8(%rcx), %rsi
.L3171:
	leaq	.LC256(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3170:
	movq	144(%r12), %rax
	testq	%rax, %rax
	je	.L3173
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3364
	cmpq	72(%rbx), %rcx
	je	.L3625
.L3175:
	movq	-8(%rcx), %rsi
.L3174:
	leaq	.LC557(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3173:
	movq	152(%r12), %rax
	testq	%rax, %rax
	je	.L3176
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3365
	cmpq	72(%rbx), %rcx
	je	.L3626
.L3178:
	movq	-8(%rcx), %rsi
.L3177:
	leaq	.LC258(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3176:
	movq	160(%r12), %rax
	testq	%rax, %rax
	je	.L3179
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3366
	cmpq	72(%rbx), %rcx
	je	.L3627
.L3181:
	movq	-8(%rcx), %rsi
.L3180:
	leaq	.LC558(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3179:
	movq	168(%r12), %rax
	testq	%rax, %rax
	je	.L3182
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3367
	cmpq	72(%rbx), %rcx
	je	.L3628
.L3184:
	movq	-8(%rcx), %rsi
.L3183:
	leaq	.LC260(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3182:
	movq	176(%r12), %rax
	testq	%rax, %rax
	je	.L3185
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3368
	cmpq	72(%rbx), %rcx
	je	.L3629
.L3187:
	movq	-8(%rcx), %rsi
.L3186:
	leaq	.LC261(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3185:
	movq	184(%r12), %rax
	testq	%rax, %rax
	je	.L3188
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3369
	cmpq	72(%rbx), %rcx
	je	.L3630
.L3190:
	movq	-8(%rcx), %rsi
.L3189:
	leaq	.LC559(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3188:
	movq	192(%r12), %rax
	testq	%rax, %rax
	je	.L3191
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3370
	cmpq	72(%rbx), %rcx
	je	.L3631
.L3193:
	movq	-8(%rcx), %rsi
.L3192:
	leaq	.LC560(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3191:
	movq	200(%r12), %rax
	testq	%rax, %rax
	je	.L3194
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3371
	cmpq	72(%rbx), %rcx
	je	.L3632
.L3196:
	movq	-8(%rcx), %rsi
.L3195:
	leaq	.LC561(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3194:
	movq	208(%r12), %rax
	testq	%rax, %rax
	je	.L3197
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3372
	cmpq	72(%rbx), %rcx
	je	.L3633
.L3199:
	movq	-8(%rcx), %rsi
.L3198:
	leaq	.LC562(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3197:
	movq	216(%r12), %rax
	testq	%rax, %rax
	je	.L3200
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3373
	cmpq	72(%rbx), %rcx
	je	.L3634
.L3202:
	movq	-8(%rcx), %rsi
.L3201:
	leaq	.LC563(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3200:
	movq	224(%r12), %rax
	testq	%rax, %rax
	je	.L3203
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3374
	cmpq	72(%rbx), %rcx
	je	.L3635
.L3205:
	movq	-8(%rcx), %rsi
.L3204:
	leaq	.LC564(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3203:
	movq	232(%r12), %rax
	testq	%rax, %rax
	je	.L3206
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3375
	cmpq	72(%rbx), %rcx
	je	.L3636
.L3208:
	movq	-8(%rcx), %rsi
.L3207:
	leaq	.LC565(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3206:
	movq	240(%r12), %rax
	testq	%rax, %rax
	je	.L3209
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3376
	cmpq	72(%rbx), %rcx
	je	.L3637
.L3211:
	movq	-8(%rcx), %rsi
.L3210:
	leaq	.LC566(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3209:
	movq	248(%r12), %rax
	testq	%rax, %rax
	je	.L3212
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3377
	cmpq	72(%rbx), %rcx
	je	.L3638
.L3214:
	movq	-8(%rcx), %rsi
.L3213:
	leaq	.LC567(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3212:
	movq	256(%r12), %rax
	testq	%rax, %rax
	je	.L3215
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3378
	cmpq	72(%rbx), %rcx
	je	.L3639
.L3217:
	movq	-8(%rcx), %rsi
.L3216:
	leaq	.LC568(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3215:
	movq	264(%r12), %rax
	testq	%rax, %rax
	je	.L3218
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3379
	cmpq	72(%rbx), %rcx
	je	.L3640
.L3220:
	movq	-8(%rcx), %rsi
.L3219:
	leaq	.LC569(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3218:
	movq	272(%r12), %rax
	testq	%rax, %rax
	je	.L3221
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3380
	cmpq	72(%rbx), %rcx
	je	.L3641
.L3223:
	movq	-8(%rcx), %rsi
.L3222:
	leaq	.LC570(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3221:
	movq	280(%r12), %rax
	testq	%rax, %rax
	je	.L3224
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3381
	cmpq	72(%rbx), %rcx
	je	.L3642
.L3226:
	movq	-8(%rcx), %rsi
.L3225:
	leaq	.LC571(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3224:
	movq	288(%r12), %rax
	testq	%rax, %rax
	je	.L3227
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3382
	cmpq	72(%rbx), %rcx
	je	.L3643
.L3229:
	movq	-8(%rcx), %rsi
.L3228:
	leaq	.LC572(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3227:
	movq	296(%r12), %rax
	testq	%rax, %rax
	je	.L3230
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3383
	cmpq	72(%rbx), %rcx
	je	.L3644
.L3232:
	movq	-8(%rcx), %rsi
.L3231:
	leaq	.LC573(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3230:
	movq	304(%r12), %rax
	testq	%rax, %rax
	je	.L3233
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3384
	cmpq	72(%rbx), %rcx
	je	.L3645
.L3235:
	movq	-8(%rcx), %rsi
.L3234:
	leaq	.LC574(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3233:
	movq	312(%r12), %rax
	testq	%rax, %rax
	je	.L3236
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3385
	cmpq	72(%rbx), %rcx
	je	.L3646
.L3238:
	movq	-8(%rcx), %rsi
.L3237:
	leaq	.LC575(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3236:
	movq	320(%r12), %rax
	testq	%rax, %rax
	je	.L3239
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3386
	cmpq	72(%rbx), %rcx
	je	.L3647
.L3241:
	movq	-8(%rcx), %rsi
.L3240:
	leaq	.LC576(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3239:
	movq	328(%r12), %rax
	testq	%rax, %rax
	je	.L3242
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3387
	cmpq	72(%rbx), %rcx
	je	.L3648
.L3244:
	movq	-8(%rcx), %rsi
.L3243:
	leaq	.LC577(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3242:
	movq	336(%r12), %rax
	testq	%rax, %rax
	je	.L3245
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3388
	cmpq	72(%rbx), %rcx
	je	.L3649
.L3247:
	movq	-8(%rcx), %rsi
.L3246:
	leaq	.LC578(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3245:
	movq	344(%r12), %rax
	testq	%rax, %rax
	je	.L3248
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3389
	cmpq	72(%rbx), %rcx
	je	.L3650
.L3250:
	movq	-8(%rcx), %rsi
.L3249:
	leaq	.LC579(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3248:
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L3251
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3390
	cmpq	72(%rbx), %rcx
	je	.L3651
.L3253:
	movq	-8(%rcx), %rsi
.L3252:
	leaq	.LC580(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3251:
	movq	360(%r12), %rax
	testq	%rax, %rax
	je	.L3254
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3391
	cmpq	72(%rbx), %rcx
	je	.L3652
.L3256:
	movq	-8(%rcx), %rsi
.L3255:
	leaq	.LC581(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3254:
	movq	368(%r12), %rax
	testq	%rax, %rax
	je	.L3257
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3392
	cmpq	72(%rbx), %rcx
	je	.L3653
.L3259:
	movq	-8(%rcx), %rsi
.L3258:
	leaq	.LC582(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3257:
	movq	376(%r12), %rax
	testq	%rax, %rax
	je	.L3260
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3393
	cmpq	72(%rbx), %rcx
	je	.L3654
.L3262:
	movq	-8(%rcx), %rsi
.L3261:
	leaq	.LC583(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3260:
	movq	384(%r12), %rax
	testq	%rax, %rax
	je	.L3263
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3394
	cmpq	72(%rbx), %rcx
	je	.L3655
.L3265:
	movq	-8(%rcx), %rsi
.L3264:
	leaq	.LC584(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3263:
	movq	392(%r12), %rax
	testq	%rax, %rax
	je	.L3266
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3395
	cmpq	72(%rbx), %rcx
	je	.L3656
.L3268:
	movq	-8(%rcx), %rsi
.L3267:
	leaq	.LC585(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3266:
	movq	400(%r12), %rax
	testq	%rax, %rax
	je	.L3269
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3396
	cmpq	72(%rbx), %rcx
	je	.L3657
.L3271:
	movq	-8(%rcx), %rsi
.L3270:
	leaq	.LC586(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3269:
	movq	408(%r12), %rax
	testq	%rax, %rax
	je	.L3272
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3397
	cmpq	72(%rbx), %rcx
	je	.L3658
.L3274:
	movq	-8(%rcx), %rsi
.L3273:
	leaq	.LC587(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3272:
	movq	416(%r12), %rax
	testq	%rax, %rax
	je	.L3275
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3398
	cmpq	72(%rbx), %rcx
	je	.L3659
.L3277:
	movq	-8(%rcx), %rsi
.L3276:
	leaq	.LC588(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3275:
	movq	424(%r12), %rax
	testq	%rax, %rax
	je	.L3278
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3399
	cmpq	72(%rbx), %rcx
	je	.L3660
.L3280:
	movq	-8(%rcx), %rsi
.L3279:
	leaq	.LC589(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3278:
	movq	432(%r12), %rax
	testq	%rax, %rax
	je	.L3281
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3400
	cmpq	72(%rbx), %rcx
	je	.L3661
.L3283:
	movq	-8(%rcx), %rsi
.L3282:
	leaq	.LC590(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3281:
	movq	440(%r12), %rax
	testq	%rax, %rax
	je	.L3284
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3401
	cmpq	72(%rbx), %rcx
	je	.L3662
.L3286:
	movq	-8(%rcx), %rsi
.L3285:
	leaq	.LC591(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3284:
	movq	448(%r12), %rax
	testq	%rax, %rax
	je	.L3287
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3402
	cmpq	72(%rbx), %rcx
	je	.L3663
.L3289:
	movq	-8(%rcx), %rsi
.L3288:
	leaq	.LC592(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3287:
	movq	456(%r12), %rax
	testq	%rax, %rax
	je	.L3290
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3403
	cmpq	72(%rbx), %rcx
	je	.L3664
.L3292:
	movq	-8(%rcx), %rsi
.L3291:
	leaq	.LC593(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3290:
	movq	464(%r12), %rax
	testq	%rax, %rax
	je	.L3293
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3404
	cmpq	72(%rbx), %rcx
	je	.L3665
.L3295:
	movq	-8(%rcx), %rsi
.L3294:
	leaq	.LC594(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3293:
	movq	472(%r12), %rax
	testq	%rax, %rax
	je	.L3296
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3405
	cmpq	72(%rbx), %rcx
	je	.L3666
.L3298:
	movq	-8(%rcx), %rsi
.L3297:
	leaq	.LC595(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3296:
	movq	480(%r12), %rax
	testq	%rax, %rax
	je	.L3299
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3406
	cmpq	72(%rbx), %rcx
	je	.L3667
.L3301:
	movq	-8(%rcx), %rsi
.L3300:
	leaq	.LC596(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3299:
	movq	488(%r12), %rax
	testq	%rax, %rax
	je	.L3302
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3407
	cmpq	72(%rbx), %rcx
	je	.L3668
.L3304:
	movq	-8(%rcx), %rsi
.L3303:
	leaq	.LC597(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3302:
	movq	496(%r12), %rax
	testq	%rax, %rax
	je	.L3305
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3408
	cmpq	72(%rbx), %rcx
	je	.L3669
.L3307:
	movq	-8(%rcx), %rsi
.L3306:
	leaq	.LC598(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3305:
	movq	504(%r12), %rax
	testq	%rax, %rax
	je	.L3308
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3409
	cmpq	72(%rbx), %rcx
	je	.L3670
.L3310:
	movq	-8(%rcx), %rsi
.L3309:
	leaq	.LC599(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3308:
	movq	512(%r12), %rax
	testq	%rax, %rax
	je	.L3311
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3410
	cmpq	72(%rbx), %rcx
	je	.L3671
.L3313:
	movq	-8(%rcx), %rsi
.L3312:
	leaq	.LC600(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3311:
	movq	520(%r12), %rax
	testq	%rax, %rax
	je	.L3314
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3411
	cmpq	72(%rbx), %rcx
	je	.L3672
.L3316:
	movq	-8(%rcx), %rsi
.L3315:
	leaq	.LC601(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3314:
	movq	528(%r12), %rax
	testq	%rax, %rax
	je	.L3317
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3412
	cmpq	72(%rbx), %rcx
	je	.L3673
.L3319:
	movq	-8(%rcx), %rsi
.L3318:
	leaq	.LC602(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3317:
	movq	536(%r12), %rax
	testq	%rax, %rax
	je	.L3320
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3413
	cmpq	72(%rbx), %rcx
	je	.L3674
.L3322:
	movq	-8(%rcx), %rsi
.L3321:
	leaq	.LC603(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3320:
	movq	544(%r12), %rax
	testq	%rax, %rax
	je	.L3323
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3414
	cmpq	72(%rbx), %rcx
	je	.L3675
.L3325:
	movq	-8(%rcx), %rsi
.L3324:
	leaq	.LC604(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3323:
	movq	552(%r12), %rax
	testq	%rax, %rax
	je	.L3326
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3415
	cmpq	72(%rbx), %rcx
	je	.L3676
.L3328:
	movq	-8(%rcx), %rsi
.L3327:
	leaq	.LC605(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3326:
	movq	560(%r12), %rax
	movq	%r13, %rdx
	leaq	.LC606(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	568(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC607(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	576(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC608(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	584(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC609(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	592(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC610(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	600(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC611(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	608(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC612(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	616(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC613(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	624(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC614(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	632(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC615(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	640(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC616(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	648(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC617(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	656(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC618(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	664(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC619(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	672(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC620(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	680(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC621(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	688(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC622(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	696(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC623(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	704(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC624(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	712(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC625(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	720(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC626(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	728(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC627(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	736(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC628(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	744(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC629(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	752(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC630(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	760(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC631(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	768(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC632(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	776(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC633(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	784(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC634(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	792(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC635(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	800(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC636(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	808(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC637(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	816(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC638(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	824(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC639(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	832(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC640(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	840(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC641(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	848(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC642(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	856(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC643(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	864(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC644(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	872(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC645(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	880(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC646(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	888(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC647(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	896(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC648(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	904(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC649(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	912(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC650(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	920(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC651(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	928(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC652(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	936(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC653(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	944(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC654(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	952(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC655(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	960(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC656(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	968(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC657(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	976(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC658(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	984(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC659(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	992(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC660(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1000(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC661(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1008(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC662(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1016(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC663(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1024(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC664(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1032(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC665(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1040(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC666(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1048(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC667(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1056(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC668(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1064(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC669(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1072(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC670(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1080(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC671(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1088(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC672(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1096(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC673(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1104(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC674(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1112(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC675(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1120(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC676(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1128(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC677(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1136(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC678(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1144(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC679(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1152(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC680(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1160(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC681(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1168(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC682(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1176(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC683(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1184(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC684(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1192(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC685(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1200(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC686(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1208(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC687(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1216(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC688(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1224(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC689(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1232(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC690(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1240(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC691(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1248(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC692(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1256(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC693(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1264(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC694(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1272(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC695(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1280(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC696(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1288(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC697(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1296(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC698(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1304(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC699(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1312(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC700(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1320(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC701(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1328(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC702(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1336(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC703(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1344(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC704(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1352(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC705(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1360(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC706(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1368(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC707(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1376(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC708(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1384(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC709(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1392(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC710(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1400(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC711(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1408(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC712(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1416(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC713(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1424(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC714(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1432(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC715(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1440(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC716(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1448(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC717(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1456(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC718(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1464(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC719(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1472(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC720(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1480(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC721(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1488(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC722(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1496(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC723(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1504(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC724(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1512(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC725(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1520(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC726(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1528(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC727(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1536(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC728(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1544(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC729(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1552(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC730(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1560(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC731(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1568(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC732(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1576(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC733(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1584(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC734(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1592(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC735(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1600(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC736(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1608(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC737(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1616(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC738(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1624(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC739(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1632(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC740(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1640(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC741(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1648(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC742(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1656(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC743(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1664(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC744(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1672(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC745(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1680(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC746(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1688(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC747(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1696(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC748(%rip), %rsi
	leaq	-64(%rbp), %r13
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1704(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC749(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1712(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC750(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1720(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC751(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1728(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC752(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1736(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC753(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1744(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC754(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1752(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC755(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1760(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC756(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1768(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC757(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1776(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC758(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1784(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC759(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1792(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC760(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1800(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC761(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1808(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC762(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1816(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC763(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1824(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC764(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1832(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC765(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1840(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC766(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1848(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC767(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1856(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC768(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1864(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC769(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1872(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC770(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1880(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC771(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1888(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC772(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1896(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC773(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1904(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC774(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1912(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC775(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1920(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC776(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1928(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC777(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1936(%r12), %rax
	movq	%r13, %rdx
	movq	%rbx, %rdi
	leaq	.LC778(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN4node13MemoryTracker10TrackFieldIN2v86StringEEEvPKcRKNS2_5LocalIT_EES5_.constprop.0
	movq	1944(%r12), %rax
	testq	%rax, %rax
	je	.L3329
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3416
	cmpq	72(%rbx), %rcx
	je	.L3677
.L3331:
	movq	-8(%rcx), %rsi
.L3330:
	leaq	.LC779(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3329:
	movq	1952(%r12), %rax
	testq	%rax, %rax
	je	.L3332
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3417
	cmpq	72(%rbx), %rcx
	je	.L3678
.L3334:
	movq	-8(%rcx), %rsi
.L3333:
	leaq	.LC780(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3332:
	movq	1960(%r12), %rax
	testq	%rax, %rax
	je	.L3335
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3418
	cmpq	72(%rbx), %rcx
	je	.L3679
.L3337:
	movq	-8(%rcx), %rsi
.L3336:
	leaq	.LC781(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3335:
	movq	1968(%r12), %rax
	testq	%rax, %rax
	je	.L3338
	movq	8(%rbx), %r14
	movq	%r13, %rsi
	movq	(%r14), %rdx
	movq	%r14, %rdi
	movq	16(%rdx), %r15
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L3419
	cmpq	72(%rbx), %rcx
	je	.L3680
.L3340:
	movq	-8(%rcx), %rsi
.L3339:
	leaq	.LC782(%rip), %rcx
	movq	%r14, %rdi
	call	*%r15
.L3338:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3341
	cmpq	72(%rbx), %rax
	je	.L3681
.L3342:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L3341
	subq	$376, 64(%rax)
.L3341:
	movl	$72, %edi
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r15
	call	_Znwm@PLT
	movl	$20, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r14
	movq	%r15, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC783(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r14)
	movq	$0, 40(%r14)
	movb	$0, 48(%r14)
	movq	$0, 64(%r14)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r14)
	movq	%r13, %rsi
	movq	$376, 64(%r14)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r14, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3343
	movq	(%rdi), %rax
	call	*8(%rax)
.L3343:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3344
	movq	%rax, %rdx
	cmpq	72(%rbx), %rax
	je	.L3682
.L3345:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L3344
	movq	8(%rbx), %rdi
	leaq	.LC783(%rip), %rcx
	movq	%r14, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rax
.L3344:
	movq	80(%rbx), %rcx
	movq	%r14, -64(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L3346
	movq	%r14, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L3347:
	leaq	2352(%r12), %rax
	leaq	1976(%r12), %r14
	movq	%rax, -72(%rbp)
	jmp	.L3351
	.p2align 4,,10
	.p2align 3
.L3350:
	movq	-8(%rcx), %rsi
.L3349:
	xorl	%ecx, %ecx
	call	*%r9
.L3348:
	addq	$8, %r14
	cmpq	-72(%rbp), %r14
	je	.L3683
.L3351:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L3348
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	movq	(%rdi), %rdx
	movq	%rdi, -80(%rbp)
	movq	16(%rdx), %r9
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	movq	%r9, -88(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	cmpq	32(%rbx), %rcx
	movq	-80(%rbp), %rdi
	movq	-88(%rbp), %r9
	movq	%rax, %rdx
	je	.L3422
	cmpq	72(%rbx), %rcx
	jne	.L3350
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3350
	.p2align 4,,10
	.p2align 3
.L3422:
	xorl	%esi, %esi
	jmp	.L3349
	.p2align 4,,10
	.p2align 3
.L3683:
	movq	64(%rbx), %rdi
	cmpq	72(%rbx), %rdi
	je	.L3352
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3353:
	cmpq	$0, 2376(%r12)
	je	.L3354
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$24, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%r15, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC784(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	$24, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3355
	movq	(%rdi), %rax
	call	*8(%rax)
.L3355:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3356
	cmpq	72(%rbx), %rax
	je	.L3684
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3356
.L3687:
	movq	8(%rbx), %rdi
	leaq	.LC785(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3356:
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$20, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	movq	%r15, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC788(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r12)
	movq	%r13, %rsi
	movq	$8, 64(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3358
	movq	(%rdi), %rax
	call	*8(%rax)
.L3358:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3166
	cmpq	72(%rbx), %rax
	je	.L3685
.L3360:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3166
	movq	8(%rbx), %rdi
	leaq	.LC789(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3166:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3686
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3354:
	.cfi_restore_state
	leaq	.LC786(%rip), %rcx
	movl	$8, %edx
	leaq	.LC787(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker7AddNodeEPKcmS2_
	jmp	.L3356
	.p2align 4,,10
	.p2align 3
.L3685:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3360
	.p2align 4,,10
	.p2align 3
.L3682:
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L3345
	.p2align 4,,10
	.p2align 3
.L3681:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3342
	.p2align 4,,10
	.p2align 3
.L3680:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3340
	.p2align 4,,10
	.p2align 3
.L3637:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3211
	.p2align 4,,10
	.p2align 3
.L3638:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3214
	.p2align 4,,10
	.p2align 3
.L3639:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3217
	.p2align 4,,10
	.p2align 3
.L3640:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3220
	.p2align 4,,10
	.p2align 3
.L3641:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3223
	.p2align 4,,10
	.p2align 3
.L3642:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3226
	.p2align 4,,10
	.p2align 3
.L3643:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3229
	.p2align 4,,10
	.p2align 3
.L3644:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3232
	.p2align 4,,10
	.p2align 3
.L3645:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3235
	.p2align 4,,10
	.p2align 3
.L3646:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3238
	.p2align 4,,10
	.p2align 3
.L3647:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3241
	.p2align 4,,10
	.p2align 3
.L3648:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3244
	.p2align 4,,10
	.p2align 3
.L3649:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3247
	.p2align 4,,10
	.p2align 3
.L3650:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3250
	.p2align 4,,10
	.p2align 3
.L3651:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3652:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3256
	.p2align 4,,10
	.p2align 3
.L3629:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3187
	.p2align 4,,10
	.p2align 3
.L3630:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3190
	.p2align 4,,10
	.p2align 3
.L3631:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3193
	.p2align 4,,10
	.p2align 3
.L3632:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3196
	.p2align 4,,10
	.p2align 3
.L3633:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3199
	.p2align 4,,10
	.p2align 3
.L3634:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3202
	.p2align 4,,10
	.p2align 3
.L3635:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3205
	.p2align 4,,10
	.p2align 3
.L3636:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3208
	.p2align 4,,10
	.p2align 3
.L3625:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3175
	.p2align 4,,10
	.p2align 3
.L3626:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3178
	.p2align 4,,10
	.p2align 3
.L3627:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3181
	.p2align 4,,10
	.p2align 3
.L3628:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3184
	.p2align 4,,10
	.p2align 3
.L3623:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3169
	.p2align 4,,10
	.p2align 3
.L3624:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3684:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %rsi
	addq	$512, %rax
	testq	%rsi, %rsi
	jne	.L3687
	jmp	.L3356
	.p2align 4,,10
	.p2align 3
.L3653:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3259
	.p2align 4,,10
	.p2align 3
.L3654:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3262
	.p2align 4,,10
	.p2align 3
.L3655:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3265
	.p2align 4,,10
	.p2align 3
.L3656:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3268
	.p2align 4,,10
	.p2align 3
.L3657:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3271
	.p2align 4,,10
	.p2align 3
.L3658:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3274
	.p2align 4,,10
	.p2align 3
.L3659:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3277
	.p2align 4,,10
	.p2align 3
.L3660:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3280
	.p2align 4,,10
	.p2align 3
.L3661:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3283
	.p2align 4,,10
	.p2align 3
.L3662:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3286
	.p2align 4,,10
	.p2align 3
.L3663:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3289
	.p2align 4,,10
	.p2align 3
.L3664:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3292
	.p2align 4,,10
	.p2align 3
.L3665:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3295
	.p2align 4,,10
	.p2align 3
.L3666:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3298
	.p2align 4,,10
	.p2align 3
.L3667:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3301
	.p2align 4,,10
	.p2align 3
.L3668:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3304
	.p2align 4,,10
	.p2align 3
.L3669:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3307
	.p2align 4,,10
	.p2align 3
.L3670:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3310
	.p2align 4,,10
	.p2align 3
.L3671:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3313
	.p2align 4,,10
	.p2align 3
.L3672:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3316
	.p2align 4,,10
	.p2align 3
.L3673:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3319
	.p2align 4,,10
	.p2align 3
.L3674:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3322
	.p2align 4,,10
	.p2align 3
.L3675:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3325
	.p2align 4,,10
	.p2align 3
.L3676:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3328
	.p2align 4,,10
	.p2align 3
.L3677:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3331
	.p2align 4,,10
	.p2align 3
.L3678:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3334
	.p2align 4,,10
	.p2align 3
.L3679:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3337
	.p2align 4,,10
	.p2align 3
.L3346:
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L3347
	.p2align 4,,10
	.p2align 3
.L3352:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3353
	.p2align 4,,10
	.p2align 3
.L3406:
	xorl	%esi, %esi
	jmp	.L3300
	.p2align 4,,10
	.p2align 3
.L3407:
	xorl	%esi, %esi
	jmp	.L3303
	.p2align 4,,10
	.p2align 3
.L3404:
	xorl	%esi, %esi
	jmp	.L3294
	.p2align 4,,10
	.p2align 3
.L3405:
	xorl	%esi, %esi
	jmp	.L3297
	.p2align 4,,10
	.p2align 3
.L3402:
	xorl	%esi, %esi
	jmp	.L3288
	.p2align 4,,10
	.p2align 3
.L3403:
	xorl	%esi, %esi
	jmp	.L3291
	.p2align 4,,10
	.p2align 3
.L3371:
	xorl	%esi, %esi
	jmp	.L3195
	.p2align 4,,10
	.p2align 3
.L3372:
	xorl	%esi, %esi
	jmp	.L3198
	.p2align 4,,10
	.p2align 3
.L3363:
	xorl	%esi, %esi
	jmp	.L3171
	.p2align 4,,10
	.p2align 3
.L3376:
	xorl	%esi, %esi
	jmp	.L3210
	.p2align 4,,10
	.p2align 3
.L3400:
	xorl	%esi, %esi
	jmp	.L3282
	.p2align 4,,10
	.p2align 3
.L3401:
	xorl	%esi, %esi
	jmp	.L3285
	.p2align 4,,10
	.p2align 3
.L3398:
	xorl	%esi, %esi
	jmp	.L3276
	.p2align 4,,10
	.p2align 3
.L3399:
	xorl	%esi, %esi
	jmp	.L3279
	.p2align 4,,10
	.p2align 3
.L3396:
	xorl	%esi, %esi
	jmp	.L3270
	.p2align 4,,10
	.p2align 3
.L3397:
	xorl	%esi, %esi
	jmp	.L3273
	.p2align 4,,10
	.p2align 3
.L3394:
	xorl	%esi, %esi
	jmp	.L3264
	.p2align 4,,10
	.p2align 3
.L3395:
	xorl	%esi, %esi
	jmp	.L3267
	.p2align 4,,10
	.p2align 3
.L3392:
	xorl	%esi, %esi
	jmp	.L3258
	.p2align 4,,10
	.p2align 3
.L3393:
	xorl	%esi, %esi
	jmp	.L3261
	.p2align 4,,10
	.p2align 3
.L3414:
	xorl	%esi, %esi
	jmp	.L3324
	.p2align 4,,10
	.p2align 3
.L3415:
	xorl	%esi, %esi
	jmp	.L3327
	.p2align 4,,10
	.p2align 3
.L3412:
	xorl	%esi, %esi
	jmp	.L3318
	.p2align 4,,10
	.p2align 3
.L3413:
	xorl	%esi, %esi
	jmp	.L3321
	.p2align 4,,10
	.p2align 3
.L3410:
	xorl	%esi, %esi
	jmp	.L3312
	.p2align 4,,10
	.p2align 3
.L3411:
	xorl	%esi, %esi
	jmp	.L3315
	.p2align 4,,10
	.p2align 3
.L3408:
	xorl	%esi, %esi
	jmp	.L3306
	.p2align 4,,10
	.p2align 3
.L3409:
	xorl	%esi, %esi
	jmp	.L3309
	.p2align 4,,10
	.p2align 3
.L3418:
	xorl	%esi, %esi
	jmp	.L3336
	.p2align 4,,10
	.p2align 3
.L3419:
	xorl	%esi, %esi
	jmp	.L3339
	.p2align 4,,10
	.p2align 3
.L3416:
	xorl	%esi, %esi
	jmp	.L3330
	.p2align 4,,10
	.p2align 3
.L3417:
	xorl	%esi, %esi
	jmp	.L3333
	.p2align 4,,10
	.p2align 3
.L3365:
	xorl	%esi, %esi
	jmp	.L3177
	.p2align 4,,10
	.p2align 3
.L3366:
	xorl	%esi, %esi
	jmp	.L3180
	.p2align 4,,10
	.p2align 3
.L3367:
	xorl	%esi, %esi
	jmp	.L3183
	.p2align 4,,10
	.p2align 3
.L3362:
	xorl	%esi, %esi
	jmp	.L3168
	.p2align 4,,10
	.p2align 3
.L3389:
	xorl	%esi, %esi
	jmp	.L3249
	.p2align 4,,10
	.p2align 3
.L3390:
	xorl	%esi, %esi
	jmp	.L3252
	.p2align 4,,10
	.p2align 3
.L3391:
	xorl	%esi, %esi
	jmp	.L3255
	.p2align 4,,10
	.p2align 3
.L3368:
	xorl	%esi, %esi
	jmp	.L3186
	.p2align 4,,10
	.p2align 3
.L3385:
	xorl	%esi, %esi
	jmp	.L3237
	.p2align 4,,10
	.p2align 3
.L3386:
	xorl	%esi, %esi
	jmp	.L3240
	.p2align 4,,10
	.p2align 3
.L3387:
	xorl	%esi, %esi
	jmp	.L3243
	.p2align 4,,10
	.p2align 3
.L3388:
	xorl	%esi, %esi
	jmp	.L3246
	.p2align 4,,10
	.p2align 3
.L3381:
	xorl	%esi, %esi
	jmp	.L3225
	.p2align 4,,10
	.p2align 3
.L3382:
	xorl	%esi, %esi
	jmp	.L3228
	.p2align 4,,10
	.p2align 3
.L3383:
	xorl	%esi, %esi
	jmp	.L3231
	.p2align 4,,10
	.p2align 3
.L3384:
	xorl	%esi, %esi
	jmp	.L3234
	.p2align 4,,10
	.p2align 3
.L3377:
	xorl	%esi, %esi
	jmp	.L3213
	.p2align 4,,10
	.p2align 3
.L3378:
	xorl	%esi, %esi
	jmp	.L3216
	.p2align 4,,10
	.p2align 3
.L3379:
	xorl	%esi, %esi
	jmp	.L3219
	.p2align 4,,10
	.p2align 3
.L3380:
	xorl	%esi, %esi
	jmp	.L3222
	.p2align 4,,10
	.p2align 3
.L3373:
	xorl	%esi, %esi
	jmp	.L3201
	.p2align 4,,10
	.p2align 3
.L3374:
	xorl	%esi, %esi
	jmp	.L3204
	.p2align 4,,10
	.p2align 3
.L3375:
	xorl	%esi, %esi
	jmp	.L3207
	.p2align 4,,10
	.p2align 3
.L3364:
	xorl	%esi, %esi
	jmp	.L3174
	.p2align 4,,10
	.p2align 3
.L3369:
	xorl	%esi, %esi
	jmp	.L3189
	.p2align 4,,10
	.p2align 3
.L3370:
	xorl	%esi, %esi
	jmp	.L3192
.L3686:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7990:
	.size	_ZNK4node11IsolateData10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node11IsolateData10MemoryInfoEPNS_13MemoryTrackerE
	.section	.rodata._ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc.str1.1,"aMS",@progbits,1
.LC790:
	.string	"wrapped"
.LC791:
	.string	"wrapper"
	.section	.text._ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc,"axG",@progbits,_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	.type	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc, @function
_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc:
.LFB6089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3689
	movq	(%rax), %rsi
	movq	%rdx, %r10
	movq	8(%rsi), %rdi
	movq	%rsi, %r8
	movq	%rdi, %r9
	jmp	.L3692
	.p2align 4,,10
	.p2align 3
.L3767:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L3696
	movq	8(%r8), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L3696
.L3692:
	cmpq	%r9, %r12
	jne	.L3767
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3694
	cmpq	72(%rbx), %rax
	je	.L3768
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3694
.L3779:
	movq	8(%rbx), %rdi
	movq	16(%r8), %rdx
	movq	%r13, %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3694:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3769
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3770:
	.cfi_restore_state
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L3689
	movq	8(%rsi), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	cmpq	%rdx, %r10
	jne	.L3689
.L3696:
	cmpq	%rdi, %r12
	jne	.L3770
	movq	16(%rsi), %r15
.L3711:
	movq	80(%rbx), %rcx
	movq	64(%rbx), %rax
	movq	%r15, -128(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L3712
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L3713:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-128(%rbp), %r13
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L3724
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L3771
.L3715:
	movq	-8(%rax), %rax
.L3714:
	cmpq	%r13, %rax
	jne	.L3772
	cmpq	$0, 64(%rax)
	je	.L3773
	cmpq	72(%rbx), %rdi
	je	.L3718
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L3694
	.p2align 4,,10
	.p2align 3
.L3689:
	movl	$72, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	48(%r15), %rdx
	movq	%rax, (%r15)
	movq	%r12, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rdx, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	testq	%r12, %r12
	je	.L3774
	leaq	-128(%rbp), %rax
	movq	(%rbx), %rsi
	movq	%rdx, -192(%rbp)
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	-192(%rbp), %rdx
	testq	%rax, %rax
	je	.L3697
	movq	8(%rbx), %rdi
	leaq	-168(%rbp), %rsi
	movq	(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	%rax, -168(%rbp)
	call	*%rcx
	movq	-192(%rbp), %rdx
	movq	%rax, 16(%r15)
.L3697:
	movq	8(%r15), %rsi
	movq	%rdx, -192(%rbp)
	leaq	-96(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rsi
	leaq	-80(%rbp), %rcx
	movq	32(%r15), %rdi
	movq	-192(%rbp), %rdx
	cmpq	%rcx, %rsi
	je	.L3775
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %r8
	cmpq	%rdi, %rdx
	je	.L3776
	movq	%rax, %xmm0
	movq	%r8, %xmm3
	movq	48(%r15), %rdx
	movq	%rsi, 32(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 40(%r15)
	testq	%rdi, %rdi
	je	.L3703
	movq	%rdi, -96(%rbp)
	movq	%rdx, -80(%rbp)
.L3701:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L3704
	call	_ZdlPv@PLT
.L3704:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	-184(%rbp), %rdi
	movq	%rax, 64(%r15)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	-184(%rbp), %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3705
	movq	(%rdi), %rax
	call	*8(%rax)
.L3705:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3706
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3708
	.p2align 4,,10
	.p2align 3
.L3777:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3706
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3706
.L3708:
	cmpq	%rsi, %r12
	jne	.L3777
	addq	$16, %rcx
.L3719:
	movq	%r15, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3709
	cmpq	72(%rbx), %rax
	je	.L3778
.L3710:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3709
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3709:
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L3711
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC790(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r15), %rsi
	movq	%r15, %rdx
	leaq	.LC791(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L3711
	.p2align 4,,10
	.p2align 3
.L3768:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	movq	504(%rax), %rsi
	addq	$512, %rax
	testq	%rsi, %rsi
	jne	.L3779
	jmp	.L3694
	.p2align 4,,10
	.p2align 3
.L3771:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3715
	.p2align 4,,10
	.p2align 3
.L3718:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3694
	.p2align 4,,10
	.p2align 3
.L3712:
	leaq	-128(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L3713
	.p2align 4,,10
	.p2align 3
.L3772:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3773:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3724:
	xorl	%eax, %eax
	jmp	.L3714
	.p2align 4,,10
	.p2align 3
.L3775:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L3699
	cmpq	$1, %rdx
	je	.L3780
	movq	%rcx, %rsi
	movq	%rcx, -192(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	movq	-192(%rbp), %rcx
.L3699:
	movq	%rdx, 40(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L3701
	.p2align 4,,10
	.p2align 3
.L3778:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3710
.L3776:
	movq	%rax, %xmm0
	movq	%r8, %xmm4
	movq	%rsi, 32(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 40(%r15)
.L3703:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L3701
	.p2align 4,,10
	.p2align 3
.L3706:
	movl	$24, %edi
	movq	%r9, -184(%rbp)
	call	_Znwm@PLT
	movq	-184(%rbp), %r9
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	%r9, %rsi
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L3719
	.p2align 4,,10
	.p2align 3
.L3774:
	leaq	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3780:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	jmp	.L3699
.L3769:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6089:
	.size	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc, .-_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv
	.type	_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv, @function
_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv:
.LFB8345:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %xmm1
	movq	%rdi, %xmm0
	movl	$64, %edi
	punpcklqdq	%xmm1, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	_ZN4node10BaseObject8DeleteMeEPv(%rip), %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-208(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movq	$0, -192(%rbp)
	movq	$8, -184(%rbp)
	call	_Znwm@PLT
	movq	-184(%rbp), %rdx
	movl	$512, %edi
	movq	%rax, -192(%rbp)
	leaq	-4(,%rdx,4), %rbx
	andq	$-8, %rbx
	addq	%rax, %rbx
	call	_Znwm@PLT
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rbx, -120(%rbp)
	movq	%rax, (%rbx)
	leaq	512(%rax), %rdx
	movq	%rax, %xmm0
	movq	%rdx, -160(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rdx, -128(%rbp)
	xorl	%edx, %edx
	movq	%rbx, -152(%rbp)
	movq	%rax, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	%r13, -112(%rbp)
	movq	$1, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movl	$0x3f800000, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -176(%rbp)
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	movq	2600(%r12), %rbx
	testq	%rbx, %rbx
	je	.L3788
	.p2align 4,,10
	.p2align 3
.L3782:
	cmpq	%r15, 8(%rbx)
	jne	.L3786
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L3786
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*56(%rax)
	testb	%al, %al
	jne	.L3817
	.p2align 4,,10
	.p2align 3
.L3786:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L3782
.L3788:
	movq	-96(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L3783
	.p2align 4,,10
	.p2align 3
.L3784:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L3784
.L3783:
	movq	-104(%rbp), %rax
	movq	-112(%rbp), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	-112(%rbp), %rdi
	movq	$0, -88(%rbp)
	movq	$0, -96(%rbp)
	cmpq	%r13, %rdi
	je	.L3789
	call	_ZdlPv@PLT
.L3789:
	movq	-192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3781
	movq	-120(%rbp), %rax
	movq	-152(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L3792
	.p2align 4,,10
	.p2align 3
.L3793:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L3793
	movq	-192(%rbp), %rdi
.L3792:
	call	_ZdlPv@PLT
.L3781:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3818
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L3817:
	.cfi_restore_state
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	jmp	.L3786
.L3818:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8345:
	.size	_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv, .-_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv
	.section	.rodata.str1.1
.LC792:
	.string	"isolate_data"
.LC793:
	.string	"native_modules_with_cache"
.LC794:
	.string	"std::basic_string"
.LC795:
	.string	"native_modules_without_cache"
.LC796:
	.string	"destroy_async_id_list"
.LC797:
	.string	"exec_argv"
	.section	.rodata.str1.8
	.align 8
.LC798:
	.string	"should_abort_on_uncaught_toggle"
	.section	.rodata.str1.1
.LC799:
	.string	"stream_base_state"
.LC800:
	.string	"fs_stats_field_array"
.LC801:
	.string	"fs_stats_field_bigint_array"
.LC802:
	.string	"cleanup_hooks"
.LC803:
	.string	"async_hooks"
.LC804:
	.string	"immediate_info"
.LC805:
	.string	"tick_info"
.LC806:
	.string	"as_callback_data"
.LC807:
	.string	"async_hooks_after_function"
.LC808:
	.string	"async_hooks_before_function"
	.section	.rodata.str1.8
	.align 8
.LC809:
	.string	"async_hooks_callback_trampoline"
	.section	.rodata.str1.1
.LC810:
	.string	"async_hooks_binding"
.LC811:
	.string	"async_hooks_destroy_function"
.LC812:
	.string	"async_hooks_init_function"
	.section	.rodata.str1.8
	.align 8
.LC813:
	.string	"async_hooks_promise_resolve_function"
	.section	.rodata.str1.1
.LC814:
	.string	"buffer_prototype_object"
.LC815:
	.string	"crypto_key_object_constructor"
.LC816:
	.string	"domexception_function"
	.section	.rodata.str1.8
	.align 8
.LC817:
	.string	"enhance_fatal_stack_after_inspector"
	.align 8
.LC818:
	.string	"enhance_fatal_stack_before_inspector"
	.section	.rodata.str1.1
.LC819:
	.string	"fs_use_promises_symbol"
	.section	.rodata.str1.8
	.align 8
.LC820:
	.string	"host_import_module_dynamically_callback"
	.align 8
.LC821:
	.string	"host_initialize_import_meta_object_callback"
	.align 8
.LC822:
	.string	"http2session_on_altsvc_function"
	.align 8
.LC823:
	.string	"http2session_on_error_function"
	.align 8
.LC824:
	.string	"http2session_on_frame_error_function"
	.align 8
.LC825:
	.string	"http2session_on_goaway_data_function"
	.align 8
.LC826:
	.string	"http2session_on_headers_function"
	.align 8
.LC827:
	.string	"http2session_on_origin_function"
	.section	.rodata.str1.1
.LC828:
	.string	"http2session_on_ping_function"
	.section	.rodata.str1.8
	.align 8
.LC829:
	.string	"http2session_on_priority_function"
	.align 8
.LC830:
	.string	"http2session_on_select_padding_function"
	.align 8
.LC831:
	.string	"http2session_on_settings_function"
	.align 8
.LC832:
	.string	"http2session_on_stream_close_function"
	.align 8
.LC833:
	.string	"http2session_on_stream_trailers_function"
	.section	.rodata.str1.1
.LC834:
	.string	"internal_binding_loader"
.LC835:
	.string	"immediate_callback_function"
	.section	.rodata.str1.8
	.align 8
.LC836:
	.string	"inspector_console_extension_installer"
	.section	.rodata.str1.1
.LC837:
	.string	"message_port"
.LC838:
	.string	"native_module_require"
.LC839:
	.string	"performance_entry_callback"
.LC840:
	.string	"performance_entry_template"
.LC841:
	.string	"prepare_stack_trace_callback"
.LC842:
	.string	"process_object"
.LC843:
	.string	"promise_reject_callback"
	.section	.rodata.str1.8
	.align 8
.LC844:
	.string	"script_data_constructor_function"
	.section	.rodata.str1.1
.LC845:
	.string	"source_map_cache_getter"
.LC846:
	.string	"tick_callback_function"
.LC847:
	.string	"timers_callback_function"
.LC848:
	.string	"tls_wrap_constructor_function"
.LC849:
	.string	"trace_category_state_function"
.LC850:
	.string	"udp_constructor_function"
.LC851:
	.string	"url_constructor_function"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB852:
	.text
.LHOTB852:
	.align 2
	.p2align 4
	.globl	_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8351:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	360(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L3820
	movq	104(%rsi), %rdi
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3821
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3823
	.p2align 4,,10
	.p2align 3
.L4411:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3821
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3821
.L3823:
	cmpq	%rsi, %r8
	jne	.L4411
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L4412
	cmpq	72(%rbx), %rax
	je	.L4413
.L3824:
	movq	-8(%rax), %rsi
.L4080:
	leaq	.LC792(%rip), %rcx
	call	*%r8
.L3820:
	leaq	16(%r12), %rax
	movq	%rax, -72(%rbp)
	cmpq	32(%r12), %rax
	je	.L3825
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3826
	cmpq	72(%rbx), %rax
	je	.L4414
.L3827:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L3826
	subq	$48, 64(%rax)
.L3826:
	movl	$72, %edi
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r14
	leaq	-64(%rbp), %r13
	call	_Znwm@PLT
	movl	$25, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC793(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r15)
	movq	%r13, %rsi
	movq	$48, 64(%r15)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3828
	movq	(%rdi), %rax
	call	*8(%rax)
.L3828:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3829
	movq	%rax, %rdx
	cmpq	72(%rbx), %rax
	je	.L4415
.L3830:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L3829
	movq	8(%rbx), %rdi
	leaq	.LC793(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rax
.L3829:
	movq	80(%rbx), %rcx
	movq	%r15, -64(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L3831
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L3832:
	movq	32(%r12), %r15
	cmpq	%r15, -72(%rbp)
	jne	.L3833
	jmp	.L3841
	.p2align 4,,10
	.p2align 3
.L3840:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3837
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3837:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -72(%rbp)
	je	.L3841
.L3833:
	movq	40(%r15), %r11
	testq	%r11, %r11
	movq	%r11, -88(%rbp)
	je	.L3837
	movl	$72, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$17, %r8d
	movq	%rax, %r9
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC794(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r9)
	movq	$0, 40(%r9)
	movb	$0, 48(%r9)
	movq	$0, 64(%r9)
	movq	%r9, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	-80(%rbp), %r9
	movq	%r13, %rsi
	movq	-88(%rbp), %r11
	movq	(%rdi), %rax
	movb	$0, 24(%r9)
	movq	%r11, 64(%r9)
	movq	8(%rax), %rax
	movq	%r9, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	movq	-80(%rbp), %r9
	testq	%rdi, %rdi
	je	.L3838
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-80(%rbp), %r9
.L3838:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3837
	cmpq	72(%rbx), %rax
	jne	.L3840
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3840
	.p2align 4,,10
	.p2align 3
.L3841:
	movq	64(%rbx), %rdi
	cmpq	72(%rbx), %rdi
	je	.L4416
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3825:
	leaq	64(%r12), %rax
	movq	%rax, -72(%rbp)
	cmpq	80(%r12), %rax
	je	.L3842
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3843
	cmpq	72(%rbx), %rax
	je	.L4417
.L3844:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L3843
	subq	$48, 64(%rax)
.L3843:
	movl	$72, %edi
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r14
	leaq	-64(%rbp), %r13
	call	_Znwm@PLT
	movl	$28, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC795(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r15)
	movq	%r13, %rsi
	movq	$48, 64(%r15)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3845
	movq	(%rdi), %rax
	call	*8(%rax)
.L3845:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3846
	movq	%rax, %rdx
	cmpq	72(%rbx), %rax
	je	.L4418
.L3847:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L3846
	movq	8(%rbx), %rdi
	leaq	.LC795(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rax
.L3846:
	movq	80(%rbx), %rdi
	movq	%r15, -64(%rbp)
	leaq	-8(%rdi), %rdx
	cmpq	%rdx, %rax
	je	.L3848
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L3849:
	movq	80(%r12), %r15
	cmpq	%r15, -72(%rbp)
	jne	.L3850
	jmp	.L3858
	.p2align 4,,10
	.p2align 3
.L3857:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3854
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3854:
	movq	%r15, %rdi
	call	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base@PLT
	movq	%rax, %r15
	cmpq	%rax, -72(%rbp)
	je	.L3858
.L3850:
	movq	40(%r15), %r11
	testq	%r11, %r11
	movq	%r11, -88(%rbp)
	je	.L3854
	movl	$72, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$17, %r8d
	movq	%rax, %r9
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC794(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r9)
	movq	$0, 40(%r9)
	movb	$0, 48(%r9)
	movq	$0, 64(%r9)
	movq	%r9, -80(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	-80(%rbp), %r9
	movq	%r13, %rsi
	movq	-88(%rbp), %r11
	movq	(%rdi), %rax
	movb	$0, 24(%r9)
	movq	%r11, 64(%r9)
	movq	8(%rax), %rax
	movq	%r9, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	movq	-80(%rbp), %r9
	testq	%rdi, %rdi
	je	.L3855
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-80(%rbp), %r9
.L3855:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3854
	cmpq	72(%rbx), %rax
	jne	.L3857
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3857
	.p2align 4,,10
	.p2align 3
.L3858:
	movq	64(%rbx), %rdi
	cmpq	72(%rbx), %rdi
	je	.L4419
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3842:
	movq	1416(%r12), %rax
	cmpq	%rax, 1424(%r12)
	je	.L3859
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3860
	cmpq	72(%rbx), %rax
	je	.L4420
.L3861:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L3860
	subq	$24, 64(%rax)
.L3860:
	movl	$72, %edi
	leaq	-64(%rbp), %r13
	call	_Znwm@PLT
	movl	$21, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r14
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC796(%rip), %rcx
	movq	%rax, (%r14)
	leaq	48(%r14), %rax
	leaq	32(%r14), %rdi
	movq	$0, 8(%r14)
	movq	$0, 16(%r14)
	movb	$0, 24(%r14)
	movq	%rax, 32(%r14)
	movq	$0, 40(%r14)
	movb	$0, 48(%r14)
	movq	$0, 64(%r14)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r14)
	movq	%r13, %rsi
	movq	$24, 64(%r14)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r14, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3862
	movq	(%rdi), %rax
	call	*8(%rax)
.L3862:
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L3863
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L4421
.L3864:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3863
	movq	8(%rbx), %rdi
	leaq	.LC796(%rip), %rcx
	movq	%r14, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rdi
.L3863:
	movq	80(%rbx), %rax
	movq	%r14, -64(%rbp)
	subq	$8, %rax
	cmpq	%rax, %rdi
	je	.L3865
	movq	%r14, (%rdi)
	addq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3866:
	movq	1416(%r12), %rax
	movq	1424(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L3867
	cmpq	%rdi, 32(%rbx)
	je	.L3868
	movq	72(%rbx), %rsi
	.p2align 4,,10
	.p2align 3
.L3873:
	cmpq	%rdi, %rsi
	je	.L4422
	movq	-8(%rdi), %rcx
	addq	$8, %rax
	addq	$8, 64(%rcx)
	cmpq	%rax, %rdx
	jne	.L3873
.L3872:
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3859:
	movq	1672(%r12), %rax
	cmpq	%rax, 1680(%r12)
	je	.L3874
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3875
	cmpq	72(%rbx), %rax
	je	.L4423
.L3876:
	movq	-8(%rax), %rax
	testq	%rax, %rax
	je	.L3875
	subq	$24, 64(%rax)
.L3875:
	movl	$72, %edi
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %r14
	leaq	-64(%rbp), %r13
	call	_Znwm@PLT
	movl	$9, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC797(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movb	$0, 24(%r15)
	movq	%r13, %rsi
	movq	$24, 64(%r15)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3877
	movq	(%rdi), %rax
	call	*8(%rax)
.L3877:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3878
	movq	%rax, %rdx
	cmpq	72(%rbx), %rax
	je	.L4424
.L3879:
	movq	-8(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L3878
	movq	8(%rbx), %rdi
	leaq	.LC797(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	64(%rbx), %rax
.L3878:
	movq	80(%rbx), %rcx
	movq	%r15, -64(%rbp)
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	je	.L3880
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L3881:
	movq	1672(%r12), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rdi
	movq	1680(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3882
	jmp	.L3890
	.p2align 4,,10
	.p2align 3
.L3888:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L4410
	movq	8(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L4410:
	movq	1680(%r12), %rax
.L3885:
	addq	$32, -72(%rbp)
	movq	-72(%rbp), %rdi
	cmpq	%rax, %rdi
	je	.L3890
.L3882:
	movq	-72(%rbp), %rcx
	movq	8(%rcx), %r10
	testq	%r10, %r10
	movq	%r10, -80(%rbp)
	je	.L3885
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$17, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r15
	movq	%r14, (%rax)
	leaq	32(%rax), %rdi
	leaq	48(%rax), %rax
	movq	$0, -40(%rax)
	leaq	.LC794(%rip), %rcx
	movq	$0, -32(%rax)
	movb	$0, -24(%rax)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	-80(%rbp), %r10
	movq	%r13, %rsi
	movb	$0, 24(%r15)
	movq	(%rdi), %rax
	movq	%r10, 64(%r15)
	movq	8(%rax), %rax
	movq	%r15, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3886
	movq	(%rdi), %rax
	call	*8(%rax)
.L3886:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L4410
	cmpq	72(%rbx), %rax
	jne	.L3888
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3888
	.p2align 4,,10
	.p2align 3
.L3890:
	movq	64(%rbx), %rdi
	cmpq	72(%rbx), %rdi
	je	.L4425
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
.L3874:
	movq	1800(%r12), %rax
	testq	%rax, %rax
	je	.L3892
	movq	1768(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L3892
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4092
	cmpq	72(%rbx), %rcx
	je	.L4426
.L3895:
	movq	-8(%rcx), %rsi
.L3894:
	leaq	.LC798(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3892:
	movq	1856(%r12), %rax
	testq	%rax, %rax
	je	.L3897
	movq	1824(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L3897
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4093
	cmpq	72(%rbx), %rcx
	je	.L4427
.L3900:
	movq	-8(%rcx), %rsi
.L3899:
	leaq	.LC799(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3897:
	movq	2296(%r12), %rax
	testq	%rax, %rax
	je	.L3902
	movq	2264(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L3902
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4094
	cmpq	72(%rbx), %rcx
	je	.L4428
.L3905:
	movq	-8(%rcx), %rsi
.L3904:
	leaq	.LC800(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3902:
	movq	2336(%r12), %rax
	testq	%rax, %rax
	je	.L3907
	movq	2304(%r12), %rdi
	movq	(%rax), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	je	.L3907
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4095
	cmpq	72(%rbx), %rcx
	je	.L4429
.L3910:
	movq	-8(%rcx), %rsi
.L3909:
	leaq	.LC801(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3907:
	movq	2608(%r12), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$3, %rax
	movq	%rax, %r14
	je	.L3914
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$13, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r13
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC802(%rip), %rcx
	movq	%rax, 0(%r13)
	leaq	48(%r13), %rax
	leaq	32(%r13), %rdi
	movq	$0, 8(%r13)
	movq	$0, 16(%r13)
	movb	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movq	$0, 40(%r13)
	movb	$0, 48(%r13)
	movq	$0, 64(%r13)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r14, 64(%r13)
	leaq	-64(%rbp), %rsi
	movb	$0, 24(%r13)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r13, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3912
	movq	(%rdi), %rax
	call	*8(%rax)
.L3912:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L3914
	cmpq	72(%rbx), %rax
	je	.L4430
.L3913:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L3914
	movq	8(%rbx), %rdi
	leaq	.LC802(%rip), %rcx
	movq	%r13, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L3914:
	movq	104(%rbx), %rdi
	leaq	1144(%r12), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3916
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3917
	.p2align 4,,10
	.p2align 3
.L4431:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3916
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3916
.L3917:
	cmpq	%r8, %rsi
	jne	.L4431
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L4432
	cmpq	72(%rbx), %rax
	je	.L4433
.L3918:
	movq	-8(%rax), %rsi
.L4077:
	leaq	.LC803(%rip), %rcx
	call	*%r8
.L3919:
	movq	104(%rbx), %rdi
	leaq	1280(%r12), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3921
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3922
	.p2align 4,,10
	.p2align 3
.L4434:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3921
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3921
.L3922:
	cmpq	%r8, %rsi
	jne	.L4434
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L4435
	cmpq	72(%rbx), %rax
	je	.L4436
.L3923:
	movq	-8(%rax), %rsi
.L4074:
	leaq	.LC804(%rip), %rcx
	call	*%r8
.L3924:
	movq	104(%rbx), %rdi
	leaq	1328(%r12), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L3926
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L3927
	.p2align 4,,10
	.p2align 3
.L4437:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L3926
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L3926
.L3927:
	cmpq	%r8, %rsi
	jne	.L4437
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L4438
	cmpq	72(%rbx), %rax
	je	.L4439
.L3928:
	movq	-8(%rax), %rsi
.L4071:
	leaq	.LC805(%rip), %rcx
	call	*%r8
.L3929:
	movq	2680(%r12), %rax
	testq	%rax, %rax
	je	.L3930
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4096
	cmpq	72(%rbx), %rcx
	je	.L4440
.L3932:
	movq	-8(%rcx), %rsi
.L3931:
	leaq	.LC806(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3930:
	movq	2688(%r12), %rax
	testq	%rax, %rax
	je	.L3933
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4097
	cmpq	72(%rbx), %rcx
	je	.L4441
.L3935:
	movq	-8(%rcx), %rsi
.L3934:
	leaq	.LC807(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3933:
	movq	2696(%r12), %rax
	testq	%rax, %rax
	je	.L3936
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4098
	cmpq	72(%rbx), %rcx
	je	.L4442
.L3938:
	movq	-8(%rcx), %rsi
.L3937:
	leaq	.LC808(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3936:
	movq	2704(%r12), %rax
	testq	%rax, %rax
	je	.L3939
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4099
	cmpq	72(%rbx), %rcx
	je	.L4443
.L3941:
	movq	-8(%rcx), %rsi
.L3940:
	leaq	.LC809(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3939:
	movq	2712(%r12), %rax
	testq	%rax, %rax
	je	.L3942
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4100
	cmpq	72(%rbx), %rcx
	je	.L4444
.L3944:
	movq	-8(%rcx), %rsi
.L3943:
	leaq	.LC810(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3942:
	movq	2720(%r12), %rax
	testq	%rax, %rax
	je	.L3945
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4101
	cmpq	72(%rbx), %rcx
	je	.L4445
.L3947:
	movq	-8(%rcx), %rsi
.L3946:
	leaq	.LC811(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3945:
	movq	2728(%r12), %rax
	testq	%rax, %rax
	je	.L3948
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4102
	cmpq	72(%rbx), %rcx
	je	.L4446
.L3950:
	movq	-8(%rcx), %rsi
.L3949:
	leaq	.LC812(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3948:
	movq	2736(%r12), %rax
	testq	%rax, %rax
	je	.L3951
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4103
	cmpq	72(%rbx), %rcx
	je	.L4447
.L3953:
	movq	-8(%rcx), %rsi
.L3952:
	leaq	.LC813(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3951:
	movq	2744(%r12), %rax
	testq	%rax, %rax
	je	.L3954
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4104
	cmpq	72(%rbx), %rcx
	je	.L4448
.L3956:
	movq	-8(%rcx), %rsi
.L3955:
	leaq	.LC814(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3954:
	movq	2752(%r12), %rax
	testq	%rax, %rax
	je	.L3957
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4105
	cmpq	72(%rbx), %rcx
	je	.L4449
.L3959:
	movq	-8(%rcx), %rsi
.L3958:
	leaq	.LC815(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3957:
	movq	2760(%r12), %rax
	testq	%rax, %rax
	je	.L3960
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4106
	cmpq	72(%rbx), %rcx
	je	.L4450
.L3962:
	movq	-8(%rcx), %rsi
.L3961:
	leaq	.LC816(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3960:
	movq	2768(%r12), %rax
	testq	%rax, %rax
	je	.L3963
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4107
	cmpq	72(%rbx), %rcx
	je	.L4451
.L3965:
	movq	-8(%rcx), %rsi
.L3964:
	leaq	.LC817(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3963:
	movq	2776(%r12), %rax
	testq	%rax, %rax
	je	.L3966
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4108
	cmpq	72(%rbx), %rcx
	je	.L4452
.L3968:
	movq	-8(%rcx), %rsi
.L3967:
	leaq	.LC818(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3966:
	movq	2784(%r12), %rax
	testq	%rax, %rax
	je	.L3969
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4109
	cmpq	72(%rbx), %rcx
	je	.L4453
.L3971:
	movq	-8(%rcx), %rsi
.L3970:
	leaq	.LC819(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3969:
	movq	2792(%r12), %rax
	testq	%rax, %rax
	je	.L3972
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4110
	cmpq	72(%rbx), %rcx
	je	.L4454
.L3974:
	movq	-8(%rcx), %rsi
.L3973:
	leaq	.LC820(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3972:
	movq	2800(%r12), %rax
	testq	%rax, %rax
	je	.L3975
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4111
	cmpq	72(%rbx), %rcx
	je	.L4455
.L3977:
	movq	-8(%rcx), %rsi
.L3976:
	leaq	.LC821(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3975:
	movq	2808(%r12), %rax
	testq	%rax, %rax
	je	.L3978
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4112
	cmpq	72(%rbx), %rcx
	je	.L4456
.L3980:
	movq	-8(%rcx), %rsi
.L3979:
	leaq	.LC822(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3978:
	movq	2816(%r12), %rax
	testq	%rax, %rax
	je	.L3981
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4113
	cmpq	72(%rbx), %rcx
	je	.L4457
.L3983:
	movq	-8(%rcx), %rsi
.L3982:
	leaq	.LC823(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3981:
	movq	2824(%r12), %rax
	testq	%rax, %rax
	je	.L3984
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4114
	cmpq	72(%rbx), %rcx
	je	.L4458
.L3986:
	movq	-8(%rcx), %rsi
.L3985:
	leaq	.LC824(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3984:
	movq	2832(%r12), %rax
	testq	%rax, %rax
	je	.L3987
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4115
	cmpq	72(%rbx), %rcx
	je	.L4459
.L3989:
	movq	-8(%rcx), %rsi
.L3988:
	leaq	.LC825(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3987:
	movq	2840(%r12), %rax
	testq	%rax, %rax
	je	.L3990
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4116
	cmpq	72(%rbx), %rcx
	je	.L4460
.L3992:
	movq	-8(%rcx), %rsi
.L3991:
	leaq	.LC826(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3990:
	movq	2848(%r12), %rax
	testq	%rax, %rax
	je	.L3993
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4117
	cmpq	72(%rbx), %rcx
	je	.L4461
.L3995:
	movq	-8(%rcx), %rsi
.L3994:
	leaq	.LC827(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3993:
	movq	2856(%r12), %rax
	testq	%rax, %rax
	je	.L3996
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4118
	cmpq	72(%rbx), %rcx
	je	.L4462
.L3998:
	movq	-8(%rcx), %rsi
.L3997:
	leaq	.LC828(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3996:
	movq	2864(%r12), %rax
	testq	%rax, %rax
	je	.L3999
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4119
	cmpq	72(%rbx), %rcx
	je	.L4463
.L4001:
	movq	-8(%rcx), %rsi
.L4000:
	leaq	.LC829(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L3999:
	movq	2872(%r12), %rax
	testq	%rax, %rax
	je	.L4002
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4120
	cmpq	72(%rbx), %rcx
	je	.L4464
.L4004:
	movq	-8(%rcx), %rsi
.L4003:
	leaq	.LC830(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4002:
	movq	2880(%r12), %rax
	testq	%rax, %rax
	je	.L4005
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4121
	cmpq	72(%rbx), %rcx
	je	.L4465
.L4007:
	movq	-8(%rcx), %rsi
.L4006:
	leaq	.LC831(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4005:
	movq	2888(%r12), %rax
	testq	%rax, %rax
	je	.L4008
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4122
	cmpq	72(%rbx), %rcx
	je	.L4466
.L4010:
	movq	-8(%rcx), %rsi
.L4009:
	leaq	.LC832(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4008:
	movq	2896(%r12), %rax
	testq	%rax, %rax
	je	.L4011
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4123
	cmpq	72(%rbx), %rcx
	je	.L4467
.L4013:
	movq	-8(%rcx), %rsi
.L4012:
	leaq	.LC833(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4011:
	movq	2904(%r12), %rax
	testq	%rax, %rax
	je	.L4014
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4124
	cmpq	72(%rbx), %rcx
	je	.L4468
.L4016:
	movq	-8(%rcx), %rsi
.L4015:
	leaq	.LC834(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4014:
	movq	2912(%r12), %rax
	testq	%rax, %rax
	je	.L4017
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4125
	cmpq	72(%rbx), %rcx
	je	.L4469
.L4019:
	movq	-8(%rcx), %rsi
.L4018:
	leaq	.LC835(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4017:
	movq	2920(%r12), %rax
	testq	%rax, %rax
	je	.L4020
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4126
	cmpq	72(%rbx), %rcx
	je	.L4470
.L4022:
	movq	-8(%rcx), %rsi
.L4021:
	leaq	.LC836(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4020:
	movq	2928(%r12), %rax
	testq	%rax, %rax
	je	.L4023
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4127
	cmpq	72(%rbx), %rcx
	je	.L4471
.L4025:
	movq	-8(%rcx), %rsi
.L4024:
	leaq	.LC837(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4023:
	movq	2936(%r12), %rax
	testq	%rax, %rax
	je	.L4026
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4128
	cmpq	72(%rbx), %rcx
	je	.L4472
.L4028:
	movq	-8(%rcx), %rsi
.L4027:
	leaq	.LC838(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4026:
	movq	2944(%r12), %rax
	testq	%rax, %rax
	je	.L4029
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4129
	cmpq	72(%rbx), %rcx
	je	.L4473
.L4031:
	movq	-8(%rcx), %rsi
.L4030:
	leaq	.LC839(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4029:
	movq	2952(%r12), %rax
	testq	%rax, %rax
	je	.L4032
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4130
	cmpq	72(%rbx), %rcx
	je	.L4474
.L4034:
	movq	-8(%rcx), %rsi
.L4033:
	leaq	.LC840(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4032:
	movq	2960(%r12), %rax
	testq	%rax, %rax
	je	.L4035
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4131
	cmpq	72(%rbx), %rcx
	je	.L4475
.L4037:
	movq	-8(%rcx), %rsi
.L4036:
	leaq	.LC841(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4035:
	movq	2968(%r12), %rax
	testq	%rax, %rax
	je	.L4038
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4132
	cmpq	72(%rbx), %rcx
	je	.L4476
.L4040:
	movq	-8(%rcx), %rsi
.L4039:
	leaq	.LC842(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4038:
	movq	2976(%r12), %rax
	testq	%rax, %rax
	je	.L4041
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4133
	cmpq	72(%rbx), %rcx
	je	.L4477
.L4043:
	movq	-8(%rcx), %rsi
.L4042:
	leaq	.LC412(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4041:
	movq	2984(%r12), %rax
	testq	%rax, %rax
	je	.L4044
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4134
	cmpq	72(%rbx), %rcx
	je	.L4478
.L4046:
	movq	-8(%rcx), %rsi
.L4045:
	leaq	.LC843(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4044:
	movq	2992(%r12), %rax
	testq	%rax, %rax
	je	.L4047
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4135
	cmpq	72(%rbx), %rcx
	je	.L4479
.L4049:
	movq	-8(%rcx), %rsi
.L4048:
	leaq	.LC844(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4047:
	movq	3000(%r12), %rax
	testq	%rax, %rax
	je	.L4050
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4136
	cmpq	72(%rbx), %rcx
	je	.L4480
.L4052:
	movq	-8(%rcx), %rsi
.L4051:
	leaq	.LC845(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4050:
	movq	3008(%r12), %rax
	testq	%rax, %rax
	je	.L4053
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4137
	cmpq	72(%rbx), %rcx
	je	.L4481
.L4055:
	movq	-8(%rcx), %rsi
.L4054:
	leaq	.LC846(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4053:
	movq	3016(%r12), %rax
	testq	%rax, %rax
	je	.L4056
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4138
	cmpq	72(%rbx), %rcx
	je	.L4482
.L4058:
	movq	-8(%rcx), %rsi
.L4057:
	leaq	.LC847(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4056:
	movq	3024(%r12), %rax
	testq	%rax, %rax
	je	.L4059
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4139
	cmpq	72(%rbx), %rcx
	je	.L4483
.L4061:
	movq	-8(%rcx), %rsi
.L4060:
	leaq	.LC848(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4059:
	movq	3032(%r12), %rax
	testq	%rax, %rax
	je	.L4062
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4140
	cmpq	72(%rbx), %rcx
	je	.L4484
.L4064:
	movq	-8(%rcx), %rsi
.L4063:
	leaq	.LC849(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4062:
	movq	3040(%r12), %rax
	testq	%rax, %rax
	je	.L4065
	movq	8(%rbx), %r13
	leaq	-64(%rbp), %rsi
	movq	0(%r13), %rdx
	movq	%r13, %rdi
	movq	16(%rdx), %r14
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4141
	cmpq	72(%rbx), %rcx
	je	.L4485
.L4067:
	movq	-8(%rcx), %rsi
.L4066:
	leaq	.LC850(%rip), %rcx
	movq	%r13, %rdi
	call	*%r14
.L4065:
	movq	3048(%r12), %rax
	testq	%rax, %rax
	je	.L3819
	movq	8(%rbx), %r12
	leaq	-64(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -64(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L4142
	cmpq	72(%rbx), %rcx
	je	.L4486
.L4070:
	movq	-8(%rcx), %rsi
.L4069:
	leaq	.LC851(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L3819:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4487
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4422:
	.cfi_restore_state
	movq	88(%rbx), %rcx
	addq	$8, %rax
	movq	-8(%rcx), %rcx
	movq	504(%rcx), %rcx
	addq	$8, 64(%rcx)
	cmpq	%rax, %rdx
	jne	.L3873
.L3871:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm6
	leaq	504(%rax), %rdi
	movq	%rax, %xmm5
	addq	$512, %rax
	movq	%rdi, %xmm0
	punpcklqdq	%xmm5, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3859
	.p2align 4,,10
	.p2align 3
.L4414:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3827
	.p2align 4,,10
	.p2align 3
.L4424:
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L3879
	.p2align 4,,10
	.p2align 3
.L4423:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3876
	.p2align 4,,10
	.p2align 3
.L4421:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3864
	.p2align 4,,10
	.p2align 3
.L4420:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3861
	.p2align 4,,10
	.p2align 3
.L4418:
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L3847
	.p2align 4,,10
	.p2align 3
.L4417:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3844
	.p2align 4,,10
	.p2align 3
.L4415:
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rdx
	addq	$512, %rdx
	jmp	.L3830
	.p2align 4,,10
	.p2align 3
.L3867:
	cmpq	72(%rbx), %rdi
	jne	.L3872
	jmp	.L3871
	.p2align 4,,10
	.p2align 3
.L4440:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3932
	.p2align 4,,10
	.p2align 3
.L4441:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3935
	.p2align 4,,10
	.p2align 3
.L4442:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3938
	.p2align 4,,10
	.p2align 3
.L4443:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3941
	.p2align 4,,10
	.p2align 3
.L4444:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3944
	.p2align 4,,10
	.p2align 3
.L4445:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L4446:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3950
	.p2align 4,,10
	.p2align 3
.L4447:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3953
	.p2align 4,,10
	.p2align 3
.L4448:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3956
	.p2align 4,,10
	.p2align 3
.L4449:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3959
	.p2align 4,,10
	.p2align 3
.L4450:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3962
	.p2align 4,,10
	.p2align 3
.L4451:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3965
	.p2align 4,,10
	.p2align 3
.L4452:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3968
	.p2align 4,,10
	.p2align 3
.L4453:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3971
	.p2align 4,,10
	.p2align 3
.L4454:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3974
	.p2align 4,,10
	.p2align 3
.L4455:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3977
	.p2align 4,,10
	.p2align 3
.L4456:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3980
	.p2align 4,,10
	.p2align 3
.L4457:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3983
	.p2align 4,,10
	.p2align 3
.L4458:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3986
	.p2align 4,,10
	.p2align 3
.L4459:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3989
	.p2align 4,,10
	.p2align 3
.L4460:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3992
	.p2align 4,,10
	.p2align 3
.L4461:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3995
	.p2align 4,,10
	.p2align 3
.L4462:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3998
	.p2align 4,,10
	.p2align 3
.L4463:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4001
	.p2align 4,,10
	.p2align 3
.L4464:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4004
	.p2align 4,,10
	.p2align 3
.L4465:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4007
	.p2align 4,,10
	.p2align 3
.L4466:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4010
	.p2align 4,,10
	.p2align 3
.L4467:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4013
	.p2align 4,,10
	.p2align 3
.L4468:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4016
	.p2align 4,,10
	.p2align 3
.L4469:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4019
	.p2align 4,,10
	.p2align 3
.L4470:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4022
	.p2align 4,,10
	.p2align 3
.L4471:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4025
	.p2align 4,,10
	.p2align 3
.L4472:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4028
	.p2align 4,,10
	.p2align 3
.L4473:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4031
	.p2align 4,,10
	.p2align 3
.L4474:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4034
	.p2align 4,,10
	.p2align 3
.L4475:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4037
	.p2align 4,,10
	.p2align 3
.L4476:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4040
	.p2align 4,,10
	.p2align 3
.L4477:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4043
	.p2align 4,,10
	.p2align 3
.L4478:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4046
	.p2align 4,,10
	.p2align 3
.L4479:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4049
	.p2align 4,,10
	.p2align 3
.L4480:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4052
	.p2align 4,,10
	.p2align 3
.L4481:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4055
	.p2align 4,,10
	.p2align 3
.L4482:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4058
	.p2align 4,,10
	.p2align 3
.L4426:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3895
	.p2align 4,,10
	.p2align 3
.L4483:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4061
	.p2align 4,,10
	.p2align 3
.L4484:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4064
	.p2align 4,,10
	.p2align 3
.L4485:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4067
	.p2align 4,,10
	.p2align 3
.L4486:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L4070
	.p2align 4,,10
	.p2align 3
.L4429:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3910
	.p2align 4,,10
	.p2align 3
.L4427:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3900
	.p2align 4,,10
	.p2align 3
.L4428:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L3905
	.p2align 4,,10
	.p2align 3
.L4439:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3928
	.p2align 4,,10
	.p2align 3
.L4413:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3824
	.p2align 4,,10
	.p2align 3
.L4433:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3918
	.p2align 4,,10
	.p2align 3
.L4436:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3923
	.p2align 4,,10
	.p2align 3
.L3831:
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L3832
	.p2align 4,,10
	.p2align 3
.L4425:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm1
	leaq	504(%rax), %rdi
	movq	%rax, %xmm7
	addq	$512, %rax
	movq	%rdi, %xmm0
	punpcklqdq	%xmm7, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3874
	.p2align 4,,10
	.p2align 3
.L4419:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm4
	leaq	504(%rax), %rdi
	movq	%rax, %xmm3
	addq	$512, %rax
	movq	%rdi, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3842
	.p2align 4,,10
	.p2align 3
.L4416:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rdi
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rdi, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L3825
	.p2align 4,,10
	.p2align 3
.L3880:
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L3881
	.p2align 4,,10
	.p2align 3
.L3848:
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L3849
	.p2align 4,,10
	.p2align 3
.L3865:
	leaq	16(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	movq	64(%rbx), %rdi
	jmp	.L3866
	.p2align 4,,10
	.p2align 3
.L4430:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L3913
	.p2align 4,,10
	.p2align 3
.L3821:
	leaq	.LC792(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	jmp	.L3820
	.p2align 4,,10
	.p2align 3
.L3926:
	leaq	.LC805(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	jmp	.L3929
	.p2align 4,,10
	.p2align 3
.L3916:
	leaq	.LC803(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	jmp	.L3919
	.p2align 4,,10
	.p2align 3
.L3921:
	leaq	.LC804(%rip), %rdx
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKc
	jmp	.L3924
	.p2align 4,,10
	.p2align 3
.L4110:
	xorl	%esi, %esi
	jmp	.L3973
	.p2align 4,,10
	.p2align 3
.L4111:
	xorl	%esi, %esi
	jmp	.L3976
	.p2align 4,,10
	.p2align 3
.L4108:
	xorl	%esi, %esi
	jmp	.L3967
	.p2align 4,,10
	.p2align 3
.L4109:
	xorl	%esi, %esi
	jmp	.L3970
	.p2align 4,,10
	.p2align 3
.L4106:
	xorl	%esi, %esi
	jmp	.L3961
	.p2align 4,,10
	.p2align 3
.L4107:
	xorl	%esi, %esi
	jmp	.L3964
	.p2align 4,,10
	.p2align 3
.L4104:
	xorl	%esi, %esi
	jmp	.L3955
	.p2align 4,,10
	.p2align 3
.L4105:
	xorl	%esi, %esi
	jmp	.L3958
	.p2align 4,,10
	.p2align 3
.L4118:
	xorl	%esi, %esi
	jmp	.L3997
	.p2align 4,,10
	.p2align 3
.L4119:
	xorl	%esi, %esi
	jmp	.L4000
	.p2align 4,,10
	.p2align 3
.L4116:
	xorl	%esi, %esi
	jmp	.L3991
	.p2align 4,,10
	.p2align 3
.L4117:
	xorl	%esi, %esi
	jmp	.L3994
	.p2align 4,,10
	.p2align 3
.L4114:
	xorl	%esi, %esi
	jmp	.L3985
	.p2align 4,,10
	.p2align 3
.L4115:
	xorl	%esi, %esi
	jmp	.L3988
	.p2align 4,,10
	.p2align 3
.L4112:
	xorl	%esi, %esi
	jmp	.L3979
	.p2align 4,,10
	.p2align 3
.L4113:
	xorl	%esi, %esi
	jmp	.L3982
	.p2align 4,,10
	.p2align 3
.L4126:
	xorl	%esi, %esi
	jmp	.L4021
	.p2align 4,,10
	.p2align 3
.L4127:
	xorl	%esi, %esi
	jmp	.L4024
	.p2align 4,,10
	.p2align 3
.L4124:
	xorl	%esi, %esi
	jmp	.L4015
	.p2align 4,,10
	.p2align 3
.L4125:
	xorl	%esi, %esi
	jmp	.L4018
	.p2align 4,,10
	.p2align 3
.L4122:
	xorl	%esi, %esi
	jmp	.L4009
	.p2align 4,,10
	.p2align 3
.L4094:
	xorl	%esi, %esi
	jmp	.L3904
	.p2align 4,,10
	.p2align 3
.L4095:
	xorl	%esi, %esi
	jmp	.L3909
	.p2align 4,,10
	.p2align 3
.L4136:
	xorl	%esi, %esi
	jmp	.L4051
	.p2align 4,,10
	.p2align 3
.L4137:
	xorl	%esi, %esi
	jmp	.L4054
	.p2align 4,,10
	.p2align 3
.L4141:
	xorl	%esi, %esi
	jmp	.L4066
	.p2align 4,,10
	.p2align 3
.L4142:
	xorl	%esi, %esi
	jmp	.L4069
	.p2align 4,,10
	.p2align 3
.L4099:
	xorl	%esi, %esi
	jmp	.L3940
	.p2align 4,,10
	.p2align 3
.L4096:
	xorl	%esi, %esi
	jmp	.L3931
	.p2align 4,,10
	.p2align 3
.L4097:
	xorl	%esi, %esi
	jmp	.L3934
	.p2align 4,,10
	.p2align 3
.L4093:
	xorl	%esi, %esi
	jmp	.L3899
	.p2align 4,,10
	.p2align 3
.L4140:
	xorl	%esi, %esi
	jmp	.L4063
	.p2align 4,,10
	.p2align 3
.L4092:
	xorl	%esi, %esi
	jmp	.L3894
	.p2align 4,,10
	.p2align 3
.L4138:
	xorl	%esi, %esi
	jmp	.L4057
	.p2align 4,,10
	.p2align 3
.L4139:
	xorl	%esi, %esi
	jmp	.L4060
	.p2align 4,,10
	.p2align 3
.L4123:
	xorl	%esi, %esi
	jmp	.L4012
	.p2align 4,,10
	.p2align 3
.L4120:
	xorl	%esi, %esi
	jmp	.L4003
	.p2align 4,,10
	.p2align 3
.L4121:
	xorl	%esi, %esi
	jmp	.L4006
	.p2align 4,,10
	.p2align 3
.L4134:
	xorl	%esi, %esi
	jmp	.L4045
	.p2align 4,,10
	.p2align 3
.L4135:
	xorl	%esi, %esi
	jmp	.L4048
	.p2align 4,,10
	.p2align 3
.L4132:
	xorl	%esi, %esi
	jmp	.L4039
	.p2align 4,,10
	.p2align 3
.L4133:
	xorl	%esi, %esi
	jmp	.L4042
	.p2align 4,,10
	.p2align 3
.L4130:
	xorl	%esi, %esi
	jmp	.L4033
	.p2align 4,,10
	.p2align 3
.L4131:
	xorl	%esi, %esi
	jmp	.L4036
	.p2align 4,,10
	.p2align 3
.L4128:
	xorl	%esi, %esi
	jmp	.L4027
	.p2align 4,,10
	.p2align 3
.L4129:
	xorl	%esi, %esi
	jmp	.L4030
	.p2align 4,,10
	.p2align 3
.L4102:
	xorl	%esi, %esi
	jmp	.L3949
	.p2align 4,,10
	.p2align 3
.L4103:
	xorl	%esi, %esi
	jmp	.L3952
	.p2align 4,,10
	.p2align 3
.L4100:
	xorl	%esi, %esi
	jmp	.L3943
	.p2align 4,,10
	.p2align 3
.L4101:
	xorl	%esi, %esi
	jmp	.L3946
	.p2align 4,,10
	.p2align 3
.L4098:
	xorl	%esi, %esi
	jmp	.L3937
	.p2align 4,,10
	.p2align 3
.L4435:
	xorl	%esi, %esi
	jmp	.L4074
	.p2align 4,,10
	.p2align 3
.L4438:
	xorl	%esi, %esi
	jmp	.L4071
	.p2align 4,,10
	.p2align 3
.L4412:
	xorl	%esi, %esi
	jmp	.L4080
	.p2align 4,,10
	.p2align 3
.L4432:
	xorl	%esi, %esi
	jmp	.L4077
.L4487:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE.cold, @function
_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE.cold:
.LFSB8351:
.L3868:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	64, %rax
	ud2
	.cfi_endproc
.LFE8351:
	.text
	.size	_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text.unlikely
	.size	_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE.cold, .-_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE.cold
.LCOLDE852:
	.text
.LHOTE852:
	.section	.rodata._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC853:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.type	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, @function
_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_:
.LFB11531:
	.cfi_startproc
	endbr64
	movabsq	$1152921504606846975, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$3, %rax
	cmpq	%rcx, %rax
	je	.L4502
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L4498
	movabsq	$9223372036854775800, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L4503
.L4490:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L4497:
	movq	(%r15), %rax
	subq	%r8, %r13
	leaq	8(%rbx,%rdx), %r15
	movq	%rax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L4504
	testq	%r13, %r13
	jg	.L4493
	testq	%r9, %r9
	jne	.L4496
.L4494:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4504:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L4493
.L4496:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L4494
	.p2align 4,,10
	.p2align 3
.L4503:
	testq	%rsi, %rsi
	jne	.L4491
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L4497
	.p2align 4,,10
	.p2align 3
.L4493:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L4494
	jmp	.L4496
	.p2align 4,,10
	.p2align 3
.L4498:
	movl	$8, %r14d
	jmp	.L4490
.L4502:
	leaq	.LC853(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L4491:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$3, %r14
	jmp	.L4490
	.cfi_endproc
.LFE11531:
	.size	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_, .-_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11IsolateData9SerializeEPN2v815SnapshotCreatorE
	.type	_ZN4node11IsolateData9SerializeEPN2v815SnapshotCreatorE, @function
_ZN4node11IsolateData9SerializeEPN2v815SnapshotCreatorE:
.LFB7939:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rdx, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v815SnapshotCreator10GetIsolateEv@PLT
	pxor	%xmm0, %xmm0
	movq	$0, 16(%r12)
	movq	%rax, %rsi
	leaq	-80(%rbp), %rax
	movups	%xmm0, (%r12)
	movq	%rax, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	64(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4506
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4507:
	movq	72(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4508
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4509:
	movq	80(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4510
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4511:
	movq	88(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4512
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4513:
	movq	96(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4514
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4515:
	movq	104(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4516
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4517:
	movq	112(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4518
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4519:
	movq	120(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4520
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4521:
	movq	128(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4522
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4523:
	movq	136(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4524
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4525:
	movq	144(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4526
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4527:
	movq	152(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4528
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4529:
	movq	160(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4530
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4531:
	movq	168(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4532
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4533:
	movq	176(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4534
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4535:
	movq	184(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4536
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4537:
	movq	192(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4538
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4539:
	movq	200(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4540
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4541:
	movq	208(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4542
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4543:
	movq	216(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4544
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4545:
	movq	224(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4546
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4547:
	movq	232(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4548
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4549:
	movq	240(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4550
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4551:
	movq	248(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4552
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4553:
	movq	256(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4554
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4555:
	movq	264(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4556
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4557:
	movq	272(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4558
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4559:
	movq	280(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4560
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4561:
	movq	288(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4562
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4563:
	movq	296(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4564
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4565:
	movq	304(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4566
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4567:
	movq	312(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4568
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4569:
	movq	320(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4570
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4571:
	movq	328(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4572
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4573:
	movq	336(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4574
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4575:
	movq	344(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4576
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4577:
	movq	352(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4578
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4579:
	movq	360(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4580
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4581:
	movq	368(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4582
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4583:
	movq	376(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4584
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4585:
	movq	384(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4586
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4587:
	movq	392(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4588
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4589:
	movq	400(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4590
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4591:
	movq	408(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4592
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4593:
	movq	416(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4594
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4595:
	movq	424(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4596
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4597:
	movq	432(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4598
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4599:
	movq	440(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4600
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4601:
	movq	448(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4602
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4603:
	movq	456(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4604
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4605:
	movq	464(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4606
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4607:
	movq	472(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4608
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4609:
	movq	480(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4610
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4611:
	movq	488(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4612
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4613:
	movq	496(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4614
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4615:
	movq	504(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4616
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4617:
	movq	512(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4618
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4619:
	movq	520(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4620
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4621:
	movq	528(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4622
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4623:
	movq	536(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4624
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4625:
	movq	544(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4626
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4627:
	movq	552(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4628
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4629:
	movq	560(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4630
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4631:
	movq	568(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4632
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4633:
	movq	576(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4634
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4635:
	movq	584(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4636
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4637:
	movq	592(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4638
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4639:
	movq	600(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4640
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4641:
	movq	608(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4642
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4643:
	movq	616(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4644
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4645:
	movq	624(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4646
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4647:
	movq	632(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4648
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4649:
	movq	640(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4650
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4651:
	movq	648(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4652
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4653:
	movq	656(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4654
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4655:
	movq	664(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4656
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4657:
	movq	672(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4658
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4659:
	movq	680(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4660
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4661:
	movq	688(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4662
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4663:
	movq	696(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4664
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4665:
	movq	704(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4666
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4667:
	movq	712(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4668
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4669:
	movq	720(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4670
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4671:
	movq	728(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4672
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4673:
	movq	736(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4674
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4675:
	movq	744(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4676
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4677:
	movq	752(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4678
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4679:
	movq	760(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4680
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4681:
	movq	768(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4682
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4683:
	movq	776(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4684
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4685:
	movq	784(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4686
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4687:
	movq	792(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4688
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4689:
	movq	800(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4690
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4691:
	movq	808(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4692
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4693:
	movq	816(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4694
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4695:
	movq	824(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4696
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4697:
	movq	832(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4698
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4699:
	movq	840(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4700
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4701:
	movq	848(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4702
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4703:
	movq	856(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4704
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4705:
	movq	864(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4706
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4707:
	movq	872(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4708
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4709:
	movq	880(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4710
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4711:
	movq	888(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4712
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4713:
	movq	896(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4714
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4715:
	movq	904(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4716
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4717:
	movq	912(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4718
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4719:
	movq	920(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4720
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4721:
	movq	928(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4722
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4723:
	movq	936(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4724
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4725:
	movq	944(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4726
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4727:
	movq	952(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4728
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4729:
	movq	960(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4730
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4731:
	movq	968(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4732
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4733:
	movq	976(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4734
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4735:
	movq	984(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4736
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4737:
	movq	992(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4738
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4739:
	movq	1000(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4740
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4741:
	movq	1008(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4742
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4743:
	movq	1016(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4744
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4745:
	movq	1024(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4746
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4747:
	movq	1032(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4748
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4749:
	movq	1040(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4750
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4751:
	movq	1048(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4752
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4753:
	movq	1056(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4754
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4755:
	movq	1064(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4756
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4757:
	movq	1072(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4758
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4759:
	movq	1080(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4760
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4761:
	movq	1088(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4762
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4763:
	movq	1096(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4764
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4765:
	movq	1104(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4766
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4767:
	movq	1112(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4768
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4769:
	movq	1120(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4770
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4771:
	movq	1128(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4772
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4773:
	movq	1136(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4774
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4775:
	movq	1144(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4776
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4777:
	movq	1152(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4778
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4779:
	movq	1160(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4780
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4781:
	movq	1168(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4782
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4783:
	movq	1176(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4784
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4785:
	movq	1184(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4786
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4787:
	movq	1192(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4788
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4789:
	movq	1200(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4790
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4791:
	movq	1208(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4792
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4793:
	movq	1216(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4794
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4795:
	movq	1224(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4796
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4797:
	movq	1232(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4798
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4799:
	movq	1240(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4800
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4801:
	movq	1248(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4802
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4803:
	movq	1256(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4804
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4805:
	movq	1264(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4806
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4807:
	movq	1272(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4808
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4809:
	movq	1280(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4810
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4811:
	movq	1288(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4812
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4813:
	movq	1296(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4814
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4815:
	movq	1304(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4816
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4817:
	movq	1312(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4818
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4819:
	movq	1320(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4820
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4821:
	movq	1328(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4822
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4823:
	movq	1336(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4824
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4825:
	movq	1344(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4826
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4827:
	movq	1352(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4828
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4829:
	movq	1360(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4830
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4831:
	movq	1368(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4832
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4833:
	movq	1376(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4834
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4835:
	movq	1384(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4836
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4837:
	movq	1392(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4838
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4839:
	movq	1400(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4840
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4841:
	movq	1408(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4842
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4843:
	movq	1416(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4844
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4845:
	movq	1424(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4846
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4847:
	movq	1432(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4848
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4849:
	movq	1440(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4850
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4851:
	movq	1448(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4852
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4853:
	movq	1456(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4854
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4855:
	movq	1464(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4856
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4857:
	movq	1472(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4858
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4859:
	movq	1480(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4860
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4861:
	movq	1488(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4862
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4863:
	movq	1496(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4864
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4865:
	movq	1504(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4866
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4867:
	movq	1512(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4868
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4869:
	movq	1520(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4870
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4871:
	movq	1528(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4872
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4873:
	movq	1536(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4874
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4875:
	movq	1544(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4876
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4877:
	movq	1552(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4878
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4879:
	movq	1560(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4880
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4881:
	movq	1568(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4882
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4883:
	movq	1576(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4884
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4885:
	movq	1584(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4886
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4887:
	movq	1592(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4888
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4889:
	movq	1600(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4890
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4891:
	movq	1608(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4892
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4893:
	movq	1616(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4894
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4895:
	movq	1624(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4896
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4897:
	movq	1632(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4898
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4899:
	movq	1640(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4900
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4901:
	movq	1648(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4902
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4903:
	movq	1656(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4904
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4905:
	movq	1664(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4906
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4907:
	movq	1672(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4908
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4909:
	movq	1680(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4910
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4911:
	movq	1688(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4912
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4913:
	movq	1696(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4914
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4915:
	movq	1704(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4916
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4917:
	movq	1712(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4918
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4919:
	movq	1720(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4920
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4921:
	movq	1728(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4922
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4923:
	movq	1736(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4924
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4925:
	movq	1744(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4926
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4927:
	movq	1752(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4928
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4929:
	movq	1760(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4930
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4931:
	movq	1768(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4932
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4933:
	movq	1776(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4934
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4935:
	movq	1784(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4936
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4937:
	movq	1792(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4938
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4939:
	movq	1800(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4940
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4941:
	movq	1808(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4942
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4943:
	movq	1816(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4944
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4945:
	movq	1824(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4946
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4947:
	movq	1832(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4948
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4949:
	movq	1840(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4950
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4951:
	movq	1848(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4952
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4953:
	movq	1856(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4954
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4955:
	movq	1864(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4956
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4957:
	movq	1872(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4958
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4959:
	movq	1880(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4960
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4961:
	movq	1888(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4962
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4963:
	movq	1896(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4964
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4965:
	movq	1904(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4966
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4967:
	movq	1912(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4968
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4969:
	movq	1920(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4970
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4971:
	movq	1928(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4972
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4973:
	movq	1936(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4974
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4975:
	movq	1944(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4976
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4977:
	movq	1952(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4978
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4979:
	movq	1960(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4980
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4981:
	movq	1968(%rbx), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	je	.L4982
	movq	%rax, (%rsi)
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
.L4983:
	leaq	1976(%rbx), %r15
	leaq	-88(%rbp), %r14
	addq	$2352, %rbx
	jmp	.L4987
	.p2align 4,,10
	.p2align 3
.L4990:
	movq	%rax, (%rsi)
	addq	$8, %r15
	addq	$8, %rsi
	movq	%rsi, 8(%r12)
	cmpq	%rbx, %r15
	je	.L4985
.L4987:
	movq	(%r15), %rax
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	_ZN2v815SnapshotCreator7AddDataEm@PLT
	movq	8(%r12), %rsi
	movq	%rax, -88(%rbp)
	cmpq	16(%r12), %rsi
	jne	.L4990
	movq	%r14, %rdx
	movq	%r12, %rdi
	addq	$8, %r15
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	cmpq	%rbx, %r15
	jne	.L4987
	.p2align 4,,10
	.p2align 3
.L4985:
	movq	-104(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L4991
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L4506:
	.cfi_restore_state
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4507
.L4508:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4509
.L4510:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4511
.L4512:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4513
.L4514:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4515
.L4516:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4517
.L4518:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4519
.L4520:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4521
.L4522:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4523
.L4524:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4525
.L4526:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4527
.L4528:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4529
.L4530:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4531
.L4532:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4533
.L4534:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4535
.L4536:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4537
.L4538:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4539
.L4540:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4541
.L4542:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4543
.L4544:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4545
.L4546:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4547
.L4548:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4549
.L4550:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4551
.L4552:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4553
.L4554:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4555
.L4556:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4557
.L4558:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4559
.L4560:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4561
.L4562:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4563
.L4564:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4565
.L4566:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4567
.L4568:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4569
.L4570:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4571
.L4572:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4573
.L4574:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4575
.L4576:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4577
.L4578:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4579
.L4580:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4581
.L4582:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4583
.L4584:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4585
.L4586:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4587
.L4588:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4589
.L4590:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4591
.L4592:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4593
.L4594:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4595
.L4596:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4597
.L4598:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4599
.L4600:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4601
.L4602:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4603
.L4604:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4605
.L4606:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4607
.L4608:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4609
.L4610:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4611
.L4612:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4613
.L4614:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4615
.L4616:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4617
.L4618:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4619
.L4620:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4621
.L4622:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4623
.L4624:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4625
.L4626:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4627
.L4628:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4629
.L4630:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4631
.L4632:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4633
.L4634:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4635
.L4636:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4637
.L4638:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4639
.L4640:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4641
.L4642:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4643
.L4644:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4645
.L4646:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4647
.L4648:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4649
.L4650:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4651
.L4652:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4653
.L4654:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4655
.L4656:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4657
.L4658:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4659
.L4660:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4661
.L4662:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4663
.L4664:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4665
.L4666:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4667
.L4668:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4669
.L4670:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4671
.L4672:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4673
.L4674:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4675
.L4676:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4677
.L4678:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4679
.L4680:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4681
.L4682:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4683
.L4684:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4685
.L4686:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4687
.L4688:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4689
.L4690:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4691
.L4692:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4693
.L4694:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4695
.L4696:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4697
.L4698:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4699
.L4700:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4701
.L4702:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4703
.L4704:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4705
.L4706:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4707
.L4708:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4709
.L4710:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4711
.L4712:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4713
.L4714:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4715
.L4716:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4717
.L4718:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4719
.L4720:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4721
.L4722:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4723
.L4724:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4725
.L4726:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4727
.L4728:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4729
.L4730:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4731
.L4732:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4733
.L4734:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4735
.L4736:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4737
.L4738:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4739
.L4740:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4741
.L4742:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4743
.L4744:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4745
.L4746:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4747
.L4748:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4749
.L4750:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4751
.L4752:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4753
.L4754:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4755
.L4756:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4757
.L4758:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4759
.L4760:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4761
.L4762:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4763
.L4764:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4765
.L4766:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4767
.L4768:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4769
.L4770:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4771
.L4772:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4773
.L4774:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4775
.L4776:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4777
.L4778:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4779
.L4780:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4781
.L4782:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4783
.L4784:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4785
.L4786:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4787
.L4788:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4789
.L4790:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4791
.L4792:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4793
.L4794:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4795
.L4796:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4797
.L4798:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4799
.L4800:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4801
.L4802:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4803
.L4804:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4805
.L4806:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4807
.L4808:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4809
.L4810:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4811
.L4812:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4813
.L4814:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4815
.L4816:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4817
.L4818:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4819
.L4820:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4821
.L4822:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4823
.L4824:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4825
.L4826:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4827
.L4828:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4829
.L4830:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4831
.L4832:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4833
.L4834:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4835
.L4836:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4837
.L4838:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4839
.L4840:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4841
.L4842:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4843
.L4844:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4845
.L4846:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4847
.L4848:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4849
.L4850:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4851
.L4852:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4853
.L4854:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4855
.L4856:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4857
.L4858:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4859
.L4860:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4861
.L4862:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4863
.L4864:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4865
.L4866:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4867
.L4868:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4869
.L4870:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4871
.L4872:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4873
.L4874:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4875
.L4876:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4877
.L4878:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4879
.L4880:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4881
.L4882:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4883
.L4884:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4885
.L4886:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4887
.L4888:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4889
.L4890:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4891
.L4892:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4893
.L4894:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4895
.L4896:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4897
.L4898:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4899
.L4900:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4901
.L4902:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4903
.L4904:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4905
.L4906:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4907
.L4908:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4909
.L4910:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4911
.L4912:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4913
.L4914:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4915
.L4916:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4917
.L4918:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4919
.L4920:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4921
.L4922:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4923
.L4924:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4925
.L4926:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4927
.L4928:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4929
.L4930:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4931
.L4932:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4933
.L4934:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4935
.L4936:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4937
.L4938:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4939
.L4940:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4941
.L4942:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4943
.L4944:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4945
.L4946:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4947
.L4948:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4949
.L4950:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4951
.L4952:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4953
.L4954:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4955
.L4956:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4957
.L4958:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4959
.L4960:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4961
.L4962:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4963
.L4964:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4965
.L4966:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4967
.L4968:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4969
.L4970:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4971
.L4972:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4973
.L4974:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4975
.L4976:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4977
.L4978:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4979
.L4980:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4981
.L4982:
	leaq	-88(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZNSt6vectorImSaImEE17_M_realloc_insertIJmEEEvN9__gnu_cxx17__normal_iteratorIPmS1_EEDpOT_
	jmp	.L4983
.L4991:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7939:
	.size	_ZN4node11IsolateData9SerializeEPN2v815SnapshotCreatorE, .-_ZN4node11IsolateData9SerializeEPN2v815SnapshotCreatorE
	.section	.text._ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.type	_ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, @function
_ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm:
.LFB12528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L4993
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L5003
.L5019:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L5004:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L4993:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L5017
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L5018
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L4996:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L4998
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L4999
	.p2align 4,,10
	.p2align 3
.L5000:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L5001:
	testq	%rsi, %rsi
	je	.L4998
.L4999:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L5000
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L5006
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L4999
	.p2align 4,,10
	.p2align 3
.L4998:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L5002
	call	_ZdlPv@PLT
.L5002:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L5019
.L5003:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L5005
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L5005:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L5004
	.p2align 4,,10
	.p2align 3
.L5006:
	movq	%rdx, %rdi
	jmp	.L5001
	.p2align 4,,10
	.p2align 3
.L5017:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L4996
.L5018:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE12528:
	.size	_ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm, .-_ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEE
	.type	_ZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEE, @function
_ZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEE:
.LFB8353:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	2672(%rdi), %r12
	testq	%r12, %r12
	je	.L5039
.L5021:
	movq	0(%r13), %r14
	movq	8(%r12), %rdi
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r15
	testq	%rax, %rax
	je	.L5023
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L5025
	.p2align 4,,10
	.p2align 3
.L5040:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5023
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r15
	jne	.L5023
.L5025:
	cmpq	%rsi, %r14
	jne	.L5040
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5039:
	.cfi_restore_state
	movq	360(%rdi), %rax
	movq	%rdi, %rbx
	movq	2392(%rax), %r12
	testq	%r12, %r12
	je	.L5041
	movl	$56, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdi
	movq	352(%rbx), %rsi
	movq	%rax, %rcx
	movups	%xmm0, 32(%rax)
	leaq	48(%rax), %rax
	leaq	_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEENUlPvE_4_FUNES6_(%rip), %rdx
	movq	$0, (%rax)
	movq	%rax, (%rcx)
	movq	(%r12), %rax
	movq	$1, 8(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 24(%rcx)
	movl	$0x3f800000, 32(%rcx)
	movq	$0, 40(%rcx)
	movq	%rcx, 2672(%rbx)
	call	*208(%rax)
	movq	2672(%rbx), %r12
	jmp	.L5021
	.p2align 4,,10
	.p2align 3
.L5023:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	0(%r13), %rax
	movq	%rax, 8(%rcx)
	movq	8(%r13), %rax
	movq	%rax, 16(%rcx)
	testq	%rax, %rax
	je	.L5026
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L5027
	lock addl	$1, 8(%rax)
.L5026:
	addq	$8, %rsp
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	popq	%rbx
	movl	$1, %r8d
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableISt10shared_ptrIN2v811ArrayBuffer9AllocatorEES4_SaIS4_ENSt8__detail9_IdentityESt8equal_toIS4_ESt4hashIS4_ENS6_18_Mod_range_hashingENS6_20_Default_ranged_hashENS6_20_Prime_rehash_policyENS6_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS6_10_Hash_nodeIS4_Lb0EEEm
	.p2align 4,,10
	.p2align 3
.L5027:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L5026
	.p2align 4,,10
	.p2align 3
.L5041:
	leaq	_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8353:
	.size	_ZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEE, .-_ZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEE
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB13497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L5044
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L5045
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L5071
	.p2align 4,,10
	.p2align 3
.L5044:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L5050
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L5056
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L5072
	.p2align 4,,10
	.p2align 3
.L5058:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L5050
.L5051:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L5058
.L5072:
	lock subl	$1, 8(%r13)
	jne	.L5058
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L5058
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L5051
	.p2align 4,,10
	.p2align 3
.L5050:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L5060
	call	_ZdlPv@PLT
.L5060:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L5055:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L5054
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L5054:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L5050
.L5056:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L5054
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L5054
	jmp	.L5055
	.p2align 4,,10
	.p2align 3
.L5045:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L5044
.L5071:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L5048
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L5049:
	cmpl	$1, %eax
	jne	.L5044
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L5044
	.p2align 4,,10
	.p2align 3
.L5048:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L5049
	.cfi_endproc
.LFE13497:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11Environment15kNodeContextTagE, @function
_GLOBAL__sub_I__ZN4node11Environment15kNodeContextTagE:
.LFB13578:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZStL8__ioinit(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNSt8ios_base4InitC1Ev@PLT
	movq	_ZNSt8ios_base4InitD1Ev@GOTPCREL(%rip), %rdi
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_def_cfa 7, 8
	leaq	_ZStL8__ioinit(%rip), %rsi
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE13578:
	.size	_GLOBAL__sub_I__ZN4node11Environment15kNodeContextTagE, .-_GLOBAL__sub_I__ZN4node11Environment15kNodeContextTagE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11Environment15kNodeContextTagE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11EnvironmentD2Ev
	.type	_ZN4node11EnvironmentD2Ev, @function
_ZN4node11EnvironmentD2Ev:
.LFB8281:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node11EnvironmentE(%rip), %rax
	movq	%rax, (%rdi)
	movq	2576(%rdi), %rax
	testq	%rax, %rax
	je	.L5076
	movq	$0, (%rax)
.L5076:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movq	%rbx, %rdx
	leaq	_ZN4node11Environment18BuildEmbedderGraphEPN2v87IsolateEPNS1_13EmbedderGraphEPv(%rip), %rsi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler32RemoveBuildEmbedderGraphCallbackEPFvPNS_7IsolateEPNS_13EmbedderGraphEPvES5_@PLT
	movq	2344(%rbx), %rax
	cmpq	%rax, 2352(%rbx)
	jne	.L5771
	movq	352(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	2080(%rbx), %r12
	movq	$0, 2080(%rbx)
	testq	%r12, %r12
	je	.L5078
	movq	%r12, %rdi
	call	_ZN4node9inspector5AgentD1Ev@PLT
	movl	$208, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5078:
	movq	3280(%rbx), %rdi
	movl	$32, %esi
	xorl	%edx, %edx
	call	_ZN2v87Context31SetAlignedPointerInEmbedderDataEiPv@PLT
	movq	1816(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L5079
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %rax
	testq	%rax, %rax
	je	.L5079
	movq	976(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5772
	movq	(%rdi), %rax
	call	*56(%rax)
.L5079:
	movq	2160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5081
	call	_ZdaPv@PLT
.L5081:
	movq	2168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5082
	call	_ZdaPv@PLT
.L5082:
	movq	2184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5083
	call	_ZdaPv@PLT
.L5083:
	movq	2176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5084
	call	_ZdaPv@PLT
.L5084:
	movq	_ZZN4node11EnvironmentD4EvE28trace_event_unique_atomic441(%rip), %r12
	testq	%r12, %r12
	je	.L5773
.L5086:
	testb	$5, (%r12)
	jne	.L5774
.L5088:
	testb	$1, 1932(%rbx)
	je	.L5094
.L5097:
	cmpq	$0, 2656(%rbx)
	jne	.L5775
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	3280(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5099
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5099:
	movq	3272(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5100
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5100:
	movq	3264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5101
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5101:
	movq	3256(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5102
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5102:
	movq	3248(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5103
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5103:
	movq	3240(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5104
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5104:
	movq	3232(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5105
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5105:
	movq	3224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5106
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5106:
	movq	3216(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5107
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5107:
	movq	3208(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5108
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5108:
	movq	3200(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5109
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5109:
	movq	3192(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5110
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5110:
	movq	3184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5111
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5111:
	movq	3176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5112
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5112:
	movq	3168(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5113
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5113:
	movq	3160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5114
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5114:
	movq	3152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5115
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5115:
	movq	3144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5116
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5116:
	movq	3136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5117
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5117:
	movq	3128(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5118
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5118:
	movq	3120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5119
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5119:
	movq	3112(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5120
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5120:
	movq	3104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5121
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5121:
	movq	3096(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5122
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5122:
	movq	3088(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5123
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5123:
	movq	3080(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5124
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5124:
	movq	3072(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5125
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5125:
	movq	3064(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5126
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5126:
	movq	3056(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5127
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5127:
	movq	3048(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5128
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5128:
	movq	3040(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5129
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5129:
	movq	3032(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5130
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5130:
	movq	3024(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5131
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5131:
	movq	3016(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5132
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5132:
	movq	3008(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5133
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5133:
	movq	3000(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5134
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5134:
	movq	2992(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5135
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5135:
	movq	2984(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5136
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5136:
	movq	2976(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5137
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5137:
	movq	2968(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5138
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5138:
	movq	2960(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5139
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5139:
	movq	2952(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5140
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5140:
	movq	2944(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5141
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5141:
	movq	2936(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5142
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5142:
	movq	2928(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5143
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5143:
	movq	2920(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5144
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5144:
	movq	2912(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5145
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5145:
	movq	2904(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5146
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5146:
	movq	2896(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5147
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5147:
	movq	2888(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5148
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5148:
	movq	2880(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5149
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5149:
	movq	2872(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5150
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5150:
	movq	2864(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5151
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5151:
	movq	2856(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5152
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5152:
	movq	2848(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5153
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5153:
	movq	2840(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5154
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5154:
	movq	2832(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5155
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5155:
	movq	2824(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5156
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5156:
	movq	2816(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5157
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5157:
	movq	2808(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5158
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5158:
	movq	2800(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5159
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5159:
	movq	2792(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5160
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5160:
	movq	2784(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5161
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5161:
	movq	2776(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5162
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5162:
	movq	2768(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5163
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5163:
	movq	2760(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5164
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5164:
	movq	2752(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5165
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5165:
	movq	2744(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5166
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5166:
	movq	2736(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5167
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5167:
	movq	2728(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5168
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5168:
	movq	2720(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5169
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5169:
	movq	2712(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5170
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5170:
	movq	2704(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5171
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5171:
	movq	2696(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5172
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5172:
	movq	2688(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5173
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5173:
	movq	2680(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5174
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5174:
	movq	2600(%rbx), %r12
	testq	%r12, %r12
	je	.L5178
	.p2align 4,,10
	.p2align 3
.L5175:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L5175
.L5178:
	movq	2592(%rbx), %rax
	movq	2584(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	2584(%rbx), %rdi
	leaq	2632(%rbx), %rax
	movq	$0, 2608(%rbx)
	movq	$0, 2600(%rbx)
	cmpq	%rax, %rdi
	je	.L5176
	call	_ZdlPv@PLT
.L5176:
	movq	2560(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5179
	movq	(%rdi), %rax
	call	*8(%rax)
.L5179:
	movq	2536(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5180
	movq	(%rdi), %rax
	call	*8(%rax)
.L5180:
	leaq	2488(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	movq	2472(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5181
	movq	(%rdi), %rax
	call	*8(%rax)
.L5181:
	movq	2440(%rbx), %r12
	leaq	2440(%rbx), %r13
	cmpq	%r13, %r12
	je	.L5185
	.p2align 4,,10
	.p2align 3
.L5182:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L5182
.L5185:
	leaq	2400(%rbx), %rdi
	leaq	2376(%rbx), %r13
	call	uv_mutex_destroy@PLT
	movq	2376(%rbx), %r12
	cmpq	%r13, %r12
	je	.L5183
	.p2align 4,,10
	.p2align 3
.L5184:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L5184
.L5183:
	movq	2352(%rbx), %r13
	movq	2344(%rbx), %r12
	cmpq	%r12, %r13
	je	.L5186
	.p2align 4,,10
	.p2align 3
.L5187:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5188
	call	_ZN4node2fs18FileHandleReadWrapD0Ev@PLT
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L5187
.L5189:
	movq	2344(%rbx), %r12
.L5186:
	testq	%r12, %r12
	je	.L5191
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5191:
	movq	2336(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5192
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5192:
	movq	2296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5193
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5193:
	movq	2200(%rbx), %r12
	testq	%r12, %r12
	je	.L5194
	movq	312(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5195
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5195:
	movq	272(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5196
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5196:
	movq	232(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5197
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5197:
	movq	192(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5198
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5198:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5199
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5199:
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5200
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5200:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5201
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5201:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5202
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5202:
	movl	$320, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5194:
	movq	2128(%rbx), %r12
	leaq	2128(%rbx), %r13
	cmpq	%r13, %r12
	je	.L5206
	.p2align 4,,10
	.p2align 3
.L5203:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r13, %r12
	jne	.L5203
.L5206:
	movq	2112(%rbx), %rax
	leaq	2112(%rbx), %rsi
	cmpq	%rax, %rsi
	je	.L5205
	.p2align 4,,10
	.p2align 3
.L5204:
	movq	2120(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rcx, (%rdx)
	movq	%rax, (%rax)
	movq	%rax, 8(%rax)
	movq	2112(%rbx), %rax
	cmpq	%rax, %rsi
	jne	.L5204
.L5205:
	movq	2120(%rbx), %rdx
	leaq	2096(%rbx), %rsi
	movq	%rdx, 8(%rax)
	movq	%rax, (%rdx)
	movq	2096(%rbx), %rax
	cmpq	%rax, %rsi
	je	.L5207
	.p2align 4,,10
	.p2align 3
.L5208:
	movq	2104(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, 8(%rcx)
	movq	%rcx, (%rdx)
	movq	%rax, (%rax)
	movq	%rax, 8(%rax)
	movq	2096(%rbx), %rax
	cmpq	%rax, %rsi
	jne	.L5208
.L5207:
	movq	2104(%rbx), %rdx
	movq	2080(%rbx), %r12
	movq	%rdx, 8(%rax)
	movq	%rax, (%rdx)
	testq	%r12, %r12
	je	.L5209
	movq	%r12, %rdi
	call	_ZN4node9inspector5AgentD1Ev@PLT
	movl	$208, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5209:
	movq	2032(%rbx), %rcx
	movq	2048(%rbx), %rax
	movq	2056(%rbx), %r12
	movq	2016(%rbx), %r15
	movq	%rcx, -144(%rbp)
	movq	2040(%rbx), %rcx
	movq	%rax, -136(%rbp)
	movq	2072(%rbx), %rax
	leaq	8(%rcx), %r14
	movq	%rcx, -152(%rbp)
	movq	%rax, -128(%rbp)
	cmpq	%r14, %rax
	jbe	.L5217
	.p2align 4,,10
	.p2align 3
.L5210:
	movq	(%r14), %rax
	leaq	512(%rax), %r13
	.p2align 4,,10
	.p2align 3
.L5216:
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5213
	movq	%rax, -120(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-120(%rbp), %rax
	addq	$8, %rax
	cmpq	%r13, %rax
	jne	.L5216
	addq	$8, %r14
	cmpq	%r14, -128(%rbp)
	ja	.L5210
.L5217:
	movq	-152(%rbp), %rcx
	cmpq	%rcx, -128(%rbp)
	je	.L5768
	cmpq	-144(%rbp), %r15
	je	.L5223
	.p2align 4,,10
	.p2align 3
.L5218:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5222
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	addq	$8, %r15
	cmpq	%r15, -144(%rbp)
	jne	.L5218
.L5223:
	cmpq	%r12, -136(%rbp)
	je	.L5226
	.p2align 4,,10
	.p2align 3
.L5219:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5225
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	addq	$8, %r12
	cmpq	%r12, -136(%rbp)
	jne	.L5219
.L5226:
	movq	2000(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5221
	movq	2072(%rbx), %rax
	movq	2040(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L5231
	.p2align 4,,10
	.p2align 3
.L5232:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L5232
	movq	2000(%rbx), %rdi
.L5231:
	call	_ZdlPv@PLT
.L5221:
	movq	1960(%rbx), %r12
	testq	%r12, %r12
	je	.L5236
	.p2align 4,,10
	.p2align 3
.L5233:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L5233
.L5236:
	movq	1952(%rbx), %rax
	movq	1944(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	1944(%rbx), %rdi
	leaq	1992(%rbx), %rax
	movq	$0, 1968(%rbx)
	movq	$0, 1960(%rbx)
	cmpq	%rax, %rdi
	je	.L5234
	call	_ZdlPv@PLT
.L5234:
	movq	1888(%rbx), %r12
	testq	%r12, %r12
	jne	.L5237
	jmp	.L5241
	.p2align 4,,10
	.p2align 3
.L5776:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L5241
.L5242:
	movq	%r13, %r12
.L5237:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	jne	.L5776
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L5242
.L5241:
	movq	1880(%rbx), %rax
	movq	1872(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	1872(%rbx), %rdi
	leaq	1920(%rbx), %rax
	movq	$0, 1896(%rbx)
	movq	$0, 1888(%rbx)
	cmpq	%rax, %rdi
	je	.L5238
	call	_ZdlPv@PLT
.L5238:
	movq	1864(%rbx), %r12
	testq	%r12, %r12
	je	.L5243
	movq	112(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5244
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5244:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5245
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5245:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5246
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5246:
	movl	$128, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5243:
	movq	1856(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5247
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5247:
	movq	1816(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5248
	movq	(%rdi), %rax
	call	*8(%rax)
.L5248:
	movq	1800(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5249
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5249:
	movq	1720(%rbx), %rdi
	leaq	1736(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L5250
	call	_ZdlPv@PLT
.L5250:
	movq	1704(%rbx), %r13
	movq	1696(%rbx), %r12
	cmpq	%r12, %r13
	je	.L5251
	.p2align 4,,10
	.p2align 3
.L5255:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5252
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L5255
.L5253:
	movq	1696(%rbx), %r12
.L5251:
	testq	%r12, %r12
	je	.L5256
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5256:
	movq	1680(%rbx), %r13
	movq	1672(%rbx), %r12
	cmpq	%r12, %r13
	je	.L5257
	.p2align 4,,10
	.p2align 3
.L5261:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L5258
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L5261
.L5259:
	movq	1672(%rbx), %r12
.L5257:
	testq	%r12, %r12
	je	.L5262
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L5262:
	movq	1664(%rbx), %r12
	testq	%r12, %r12
	je	.L5264
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L5265
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L5266:
	cmpl	$1, %eax
	je	.L5777
.L5264:
	movq	1648(%rbx), %r12
	testq	%r12, %r12
	je	.L5271
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L5272
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L5273:
	cmpl	$1, %eax
	je	.L5778
.L5271:
	movq	1600(%rbx), %rdi
	leaq	1616(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L5277
	call	_ZdlPv@PLT
.L5277:
	movq	1568(%rbx), %rdi
	leaq	1584(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L5278
	call	_ZdlPv@PLT
.L5278:
	movq	1560(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5279
	movq	(%rdi), %rax
	call	*8(%rax)
.L5279:
	movq	1520(%rbx), %rdi
	leaq	1536(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L5280
	call	_ZdlPv@PLT
.L5280:
	movq	1488(%rbx), %rdi
	leaq	1504(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L5281
	call	_ZdlPv@PLT
.L5281:
	movq	1456(%rbx), %rdi
	leaq	1472(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L5282
	call	_ZdlPv@PLT
.L5282:
	movq	1448(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5283
	movq	(%rdi), %rax
	call	*8(%rax)
.L5283:
	movq	1440(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5284
	movq	(%rdi), %rax
	call	*8(%rax)
.L5284:
	movq	1416(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5285
	call	_ZdlPv@PLT
.L5285:
	movq	1392(%rbx), %r12
	testq	%r12, %r12
	je	.L5287
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L5288
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L5289:
	cmpl	$1, %eax
	je	.L5779
.L5287:
	movq	1368(%rbx), %rdi
	leaq	16+_ZTVN4node8TickInfoE(%rip), %rax
	movq	%rax, 1328(%rbx)
	testq	%rdi, %rdi
	je	.L5293
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5293:
	movq	1320(%rbx), %rdi
	leaq	16+_ZTVN4node13ImmediateInfoE(%rip), %rax
	movq	%rax, 1280(%rbx)
	testq	%rdi, %rdi
	je	.L5294
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5294:
	movq	1272(%rbx), %rdi
	leaq	16+_ZTVN4node10AsyncHooksE(%rip), %rax
	movq	%rax, 1144(%rbx)
	testq	%rdi, %rdi
	je	.L5295
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5295:
	movq	1264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5296
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5296:
	movq	1224(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5297
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5297:
	movq	1184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L5298
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5298:
	movq	328(%rbx), %r12
	leaq	328(%rbx), %r14
	cmpq	%r12, %r14
	jne	.L5299
	jmp	.L5304
	.p2align 4,,10
	.p2align 3
.L5780:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	je	.L5304
.L5305:
	movq	%r13, %r12
.L5299:
	movq	56(%r12), %rdi
	leaq	72(%r12), %rax
	movq	(%r12), %r13
	cmpq	%rax, %rdi
	je	.L5302
	call	_ZdlPv@PLT
.L5302:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L5780
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	cmpq	%r13, %r14
	jne	.L5305
.L5304:
	movq	288(%rbx), %r12
	testq	%r12, %r12
	je	.L5300
	.p2align 4,,10
	.p2align 3
.L5301:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L5301
.L5300:
	movq	280(%rbx), %rax
	movq	272(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	272(%rbx), %rdi
	leaq	320(%rbx), %rax
	movq	$0, 296(%rbx)
	movq	$0, 288(%rbx)
	cmpq	%rax, %rdi
	je	.L5306
	call	_ZdlPv@PLT
.L5306:
	movq	232(%rbx), %r12
	testq	%r12, %r12
	je	.L5311
	.p2align 4,,10
	.p2align 3
.L5308:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L5308
.L5311:
	movq	224(%rbx), %rax
	movq	216(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	216(%rbx), %rdi
	leaq	264(%rbx), %rax
	movq	$0, 240(%rbx)
	movq	$0, 232(%rbx)
	cmpq	%rax, %rdi
	je	.L5309
	call	_ZdlPv@PLT
.L5309:
	movq	176(%rbx), %r12
	testq	%r12, %r12
	je	.L5315
	.p2align 4,,10
	.p2align 3
.L5312:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L5312
.L5315:
	movq	168(%rbx), %rax
	movq	160(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	160(%rbx), %rdi
	leaq	208(%rbx), %rax
	movq	$0, 184(%rbx)
	movq	$0, 176(%rbx)
	cmpq	%rax, %rdi
	je	.L5313
	call	_ZdlPv@PLT
.L5313:
	movq	120(%rbx), %r12
	testq	%r12, %r12
	je	.L5319
	.p2align 4,,10
	.p2align 3
.L5316:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L5316
.L5319:
	movq	112(%rbx), %rax
	movq	104(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	104(%rbx), %rdi
	leaq	152(%rbx), %rax
	movq	$0, 128(%rbx)
	movq	$0, 120(%rbx)
	cmpq	%rax, %rdi
	je	.L5317
	call	_ZdlPv@PLT
.L5317:
	movq	72(%rbx), %r12
	leaq	56(%rbx), %r14
	testq	%r12, %r12
	je	.L5324
.L5320:
	movq	24(%r12), %rsi
	movq	%r14, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L5323
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L5324
.L5325:
	movq	%r13, %r12
	jmp	.L5320
	.p2align 4,,10
	.p2align 3
.L5230:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L5770
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L5770:
	addq	$8, %r15
.L5768:
	cmpq	%r15, -136(%rbp)
	jne	.L5230
	jmp	.L5226
	.p2align 4,,10
	.p2align 3
.L5213:
	addq	$8, %rax
	cmpq	%r13, %rax
	jne	.L5216
	addq	$8, %r14
	cmpq	%r14, -128(%rbp)
	ja	.L5210
	jmp	.L5217
	.p2align 4,,10
	.p2align 3
.L5188:
	addq	$8, %r12
	cmpq	%r12, %r13
	jne	.L5187
	jmp	.L5189
	.p2align 4,,10
	.p2align 3
.L5252:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L5255
	jmp	.L5253
	.p2align 4,,10
	.p2align 3
.L5258:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L5261
	jmp	.L5259
	.p2align 4,,10
	.p2align 3
.L5222:
	addq	$8, %r15
	cmpq	%r15, -144(%rbp)
	jne	.L5218
	cmpq	%r12, -136(%rbp)
	jne	.L5219
	jmp	.L5226
	.p2align 4,,10
	.p2align 3
.L5225:
	addq	$8, %r12
	cmpq	%r12, -136(%rbp)
	jne	.L5219
	jmp	.L5226
	.p2align 4,,10
	.p2align 3
.L5323:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L5325
.L5324:
	movq	24(%rbx), %r12
	leaq	8(%rbx), %r13
	testq	%r12, %r12
	je	.L5075
.L5322:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L5326
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L5075
.L5327:
	movq	%rbx, %r12
	jmp	.L5322
	.p2align 4,,10
	.p2align 3
.L5326:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L5327
.L5075:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5781
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5094:
	.cfi_restore_state
	movq	328(%rbx), %r12
	leaq	328(%rbx), %r14
	cmpq	%r14, %r12
	je	.L5097
	.p2align 4,,10
	.p2align 3
.L5098:
	leaq	16(%r12), %rdi
	call	_ZN4node7binding4DLib5CloseEv@PLT
	movq	(%r12), %r12
	cmpq	%r12, %r14
	jne	.L5098
	jmp	.L5097
	.p2align 4,,10
	.p2align 3
.L5774:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %r14
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5089
	subq	$8, %rsp
	leaq	-80(%rbp), %r14
	movq	(%rax), %rax
	movq	%rbx, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC545(%rip), %rcx
	movq	%r12, %rdx
	pushq	%r14
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L5089:
	leaq	-64(%rbp), %r15
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r12
.L5093:
	movq	-8(%r15), %r8
	subq	$8, %r15
	testq	%r8, %r8
	je	.L5090
	movq	(%r8), %rdx
	movq	8(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L5091
	movq	8(%r8), %rdi
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %rax
	leaq	24(%r8), %rdx
	movq	%rax, (%r8)
	cmpq	%rdx, %rdi
	je	.L5092
	movq	%r8, -120(%rbp)
	call	_ZdlPv@PLT
	movq	-120(%rbp), %r8
.L5092:
	movl	$48, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L5090:
	cmpq	%r14, %r15
	jne	.L5093
	jmp	.L5088
	.p2align 4,,10
	.p2align 3
.L5773:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5087
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L5087:
	movq	%r12, _ZZN4node11EnvironmentD4EvE28trace_event_unique_atomic441(%rip)
	mfence
	jmp	.L5086
	.p2align 4,,10
	.p2align 3
.L5777:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L5268
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L5269:
	cmpl	$1, %eax
	jne	.L5264
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L5264
	.p2align 4,,10
	.p2align 3
.L5778:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L5275
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L5276:
	cmpl	$1, %eax
	jne	.L5271
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L5271
	.p2align 4,,10
	.p2align 3
.L5779:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L5291
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L5292:
	cmpl	$1, %eax
	jne	.L5287
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L5287
.L5772:
	leaq	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5288:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L5289
	.p2align 4,,10
	.p2align 3
.L5265:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L5266
	.p2align 4,,10
	.p2align 3
.L5272:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L5273
	.p2align 4,,10
	.p2align 3
.L5771:
	leaq	_ZZN4node11EnvironmentD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5775:
	leaq	_ZZN4node11EnvironmentD4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L5091:
	movq	%r8, %rdi
	call	*%rdx
	jmp	.L5090
.L5275:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L5276
.L5291:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L5292
.L5268:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L5269
.L5781:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8281:
	.size	_ZN4node11EnvironmentD2Ev, .-_ZN4node11EnvironmentD2Ev
	.globl	_ZN4node11EnvironmentD1Ev
	.set	_ZN4node11EnvironmentD1Ev,_ZN4node11EnvironmentD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node11EnvironmentD0Ev
	.type	_ZN4node11EnvironmentD0Ev, @function
_ZN4node11EnvironmentD0Ev:
.LFB8283:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node11EnvironmentD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$3288, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8283:
	.size	_ZN4node11EnvironmentD0Ev, .-_ZN4node11EnvironmentD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment14CleanupHandlesEv
	.type	_ZN4node11Environment14CleanupHandlesEv, @function
_ZN4node11Environment14CleanupHandlesEv:
.LFB8301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	2112(%rbx), %r14
	subq	$40, %rsp
	movq	352(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeC1EPS0_NS1_9OnFailureE@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment27RunAndClearNativeImmediatesEb
	movq	2120(%rbx), %r12
	cmpq	%r12, %r14
	je	.L5788
	.p2align 4,,10
	.p2align 3
.L5785:
	movq	-8(%r12), %rax
	leaq	-8(%r12), %rdi
	call	*16(%rax)
	movq	8(%r12), %r12
	cmpq	%r14, %r12
	jne	.L5785
.L5788:
	movq	2104(%rbx), %r12
	leaq	2096(%rbx), %r14
	cmpq	%r14, %r12
	je	.L5787
	.p2align 4,,10
	.p2align 3
.L5786:
	movq	-56(%r12), %rax
	leaq	-56(%r12), %rdi
	xorl	%esi, %esi
	call	*80(%rax)
	movq	8(%r12), %r12
	cmpq	%r12, %r14
	jne	.L5786
.L5787:
	movq	2128(%rbx), %r12
	leaq	2128(%rbx), %r15
	cmpq	%r15, %r12
	je	.L5790
	.p2align 4,,10
	.p2align 3
.L5789:
	movq	32(%r12), %rdx
	movq	16(%r12), %rsi
	movq	%rbx, %rdi
	call	*24(%r12)
	movq	(%r12), %r12
	cmpq	%r12, %r15
	jne	.L5789
	movq	2128(%rbx), %r12
	cmpq	%r12, %r15
	je	.L5790
	.p2align 4,,10
	.p2align 3
.L5791:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r15
	jne	.L5791
.L5790:
	movq	$0, 2144(%rbx)
	movq	%r15, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 2128(%rbx)
	jmp	.L5792
	.p2align 4,,10
	.p2align 3
.L5793:
	movq	360(%rbx), %rax
	movl	$1, %esi
	movq	2360(%rax), %rdi
	call	uv_run@PLT
.L5792:
	cmpq	$0, 2152(%rbx)
	jne	.L5793
	cmpq	2096(%rbx), %r14
	jne	.L5793
	movq	2344(%rbx), %r14
	movq	2352(%rbx), %r15
	cmpq	%r15, %r14
	je	.L5795
	movq	%r14, %r12
	.p2align 4,,10
	.p2align 3
.L5799:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L5796
	call	_ZN4node2fs18FileHandleReadWrapD0Ev@PLT
	addq	$8, %r12
	cmpq	%r12, %r15
	jne	.L5799
.L5797:
	movq	%r14, 2352(%rbx)
.L5795:
	movq	%r13, %rdi
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5809
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5796:
	.cfi_restore_state
	addq	$8, %r12
	cmpq	%r12, %r15
	jne	.L5799
	jmp	.L5797
.L5809:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8301:
	.size	_ZN4node11Environment14CleanupHandlesEv, .-_ZN4node11Environment14CleanupHandlesEv
	.section	.rodata.str1.1
.LC854:
	.string	"RunCleanup"
	.section	.rodata.str1.8
	.align 8
.LC855:
	.string	"cannot create std::vector larger than max_size()"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11Environment10RunCleanupEv
	.type	_ZN4node11Environment10RunCleanupEv, @function
_ZN4node11Environment10RunCleanupEv:
.LFB8312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	$1, 2648(%rdi)
	movq	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip), %r12
	testq	%r12, %r12
	je	.L5919
	testb	$5, (%r12)
	jne	.L5920
.L5814:
	movq	%r15, %rdi
	call	_ZN4node11Environment14CleanupHandlesEv
	leaq	2600(%r15), %rax
	cmpq	$0, 2608(%r15)
	movq	%rax, -104(%rbp)
	je	.L5827
	.p2align 4,,10
	.p2align 3
.L5820:
	movq	2600(%r15), %rbx
	testq	%rbx, %rbx
	je	.L5823
	movq	%rbx, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L5824:
	movq	(%rax), %rax
	addq	$1, %rdx
	testq	%rax, %rax
	jne	.L5824
	movabsq	$384307168202282325, %rax
	cmpq	%rax, %rdx
	jg	.L5921
	leaq	(%rdx,%rdx,2), %rdi
	salq	$3, %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L5828:
	movdqu	8(%rbx), %xmm2
	addq	$24, %r14
	movups	%xmm2, -24(%r14)
	movq	24(%rbx), %rax
	movq	%rax, -8(%r14)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L5828
	movq	-96(%rbp), %rcx
	cmpq	%rcx, %r14
	je	.L5860
	movq	%r14, %rbx
	movl	$63, %edx
	movq	%rcx, %rdi
	movq	%rcx, %r13
	movabsq	$-6148914691236517205, %rsi
	subq	%rcx, %rbx
	movq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rsi, %rax
	movq	%r14, %rsi
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	%eax, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rdx
	call	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEElNS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_T1_
	cmpq	$384, %rbx
	jle	.L5830
	leaq	384(%r13), %rbx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_.isra.0
	movq	%rbx, %rdx
	cmpq	%rbx, %r14
	je	.L5832
	.p2align 4,,10
	.p2align 3
.L5835:
	movq	(%rdx), %r9
	movq	8(%rdx), %r8
	movq	%rdx, %rsi
	movq	16(%rdx), %rdi
	cmpq	%rdi, -8(%rdx)
	jnb	.L5833
	leaq	-24(%rdx), %rax
	.p2align 4,,10
	.p2align 3
.L5834:
	movdqu	(%rax), %xmm1
	movq	16(%rax), %rcx
	movq	%rax, %rsi
	subq	$24, %rax
	movups	%xmm1, 48(%rax)
	movq	%rcx, 64(%rax)
	cmpq	16(%rax), %rdi
	ja	.L5834
.L5833:
	addq	$24, %rdx
	movq	%r9, (%rsi)
	movq	%r8, 8(%rsi)
	movq	%rdi, 16(%rsi)
	cmpq	%rdx, %r14
	jne	.L5835
.L5832:
	movq	%r14, -88(%rbp)
	movq	-96(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L5852:
	movq	8(%rbx), %rdi
	movq	2592(%r15), %rsi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	movq	2584(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r10
	testq	%rax, %rax
	je	.L5838
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L5838
	movq	32(%rcx), %r8
	xorl	%r9d, %r9d
	jmp	.L5844
	.p2align 4,,10
	.p2align 3
.L5840:
	testq	%r9, %r9
	jne	.L5842
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L5843
.L5922:
	movq	32(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r10
	jne	.L5843
.L5844:
	cmpq	%r8, %rdi
	jne	.L5840
	movq	8(%rcx), %rax
	cmpq	%rax, (%rbx)
	jne	.L5840
	cmpq	16(%rcx), %rdi
	jne	.L5840
	movq	(%rcx), %rcx
	addq	$1, %r9
	testq	%rcx, %rcx
	jne	.L5922
	.p2align 4,,10
	.p2align 3
.L5843:
	testq	%r9, %r9
	je	.L5838
.L5842:
	call	*(%rbx)
	movq	8(%rbx), %r9
	movq	2592(%r15), %rsi
	xorl	%edx, %edx
	movq	2584(%r15), %r13
	movq	%r9, %rax
	divq	%rsi
	leaq	0(%r13,%rdx,8), %r14
	movq	%rdx, %r12
	movq	(%r14), %r10
	testq	%r10, %r10
	je	.L5838
	movq	(%r10), %rdi
	movq	%r10, %r11
	movq	32(%rdi), %rcx
	jmp	.L5848
	.p2align 4,,10
	.p2align 3
.L5845:
	movq	(%rdi), %r8
	testq	%r8, %r8
	je	.L5838
	movq	32(%r8), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r11
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r12
	jne	.L5838
	movq	%r8, %rdi
.L5848:
	cmpq	%r9, %rcx
	jne	.L5845
	movq	8(%rdi), %rax
	cmpq	%rax, (%rbx)
	jne	.L5845
	cmpq	16(%rdi), %rcx
	jne	.L5845
	movq	(%rdi), %rcx
	cmpq	%r11, %r10
	je	.L5923
	testq	%rcx, %rcx
	je	.L5850
	movq	32(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r12
	je	.L5850
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L5850:
	movq	%rcx, (%r11)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r15)
.L5838:
	addq	$24, %rbx
	cmpq	%rbx, -88(%rbp)
	jne	.L5852
.L5860:
	movq	%r15, %rdi
	call	_ZN4node11Environment14CleanupHandlesEv
	movq	-96(%rbp), %rdi
	call	_ZdlPv@PLT
	cmpq	$0, 2608(%r15)
	jne	.L5820
.L5827:
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L5821
.L5822:
	testb	$5, (%r12)
	jne	.L5924
.L5810:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L5925
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L5923:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L5863
	movq	32(%rcx), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r12
	je	.L5850
	movq	%r11, 0(%r13,%rdx,8)
	movq	(%r14), %rax
	cmpq	-104(%rbp), %rax
	je	.L5926
.L5851:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L5850
	.p2align 4,,10
	.p2align 3
.L5823:
	movq	%r15, %rdi
	call	_ZN4node11Environment14CleanupHandlesEv
	cmpq	$0, 2608(%r15)
	jne	.L5820
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	jne	.L5822
.L5821:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5853
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L5853:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	testb	$5, (%r12)
	je	.L5810
.L5924:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5855
	subq	$8, %rsp
	leaq	-80(%rbp), %rbx
	movq	(%rax), %rax
	movq	%r15, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC854(%rip), %rcx
	movq	%r12, %rdx
	pushq	%rbx
	movl	$101, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L5855:
	leaq	-64(%rbp), %r13
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %r14
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r15
.L5859:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L5856
	movq	(%r12), %rax
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.L5857
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r15, (%r12)
	cmpq	%rax, %rdi
	je	.L5858
	call	_ZdlPv@PLT
.L5858:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5856:
	cmpq	%rbx, %r13
	jne	.L5859
	jmp	.L5810
	.p2align 4,,10
	.p2align 3
.L5863:
	movq	%r11, %rax
	cmpq	-104(%rbp), %rax
	jne	.L5851
.L5926:
	movq	%rcx, 2600(%r15)
	jmp	.L5851
	.p2align 4,,10
	.p2align 3
.L5830:
	movq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPN4node19CleanupHookCallbackESt6vectorIS3_SaIS3_EEEENS0_5__ops15_Iter_comp_iterIZNS2_11Environment10RunCleanupEvEUlRKS3_SD_E_EEEvT_SG_T0_.isra.0
	jmp	.L5832
.L5919:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5813
	movq	(%rax), %rax
	leaq	.LC5(%rip), %rsi
	call	*16(%rax)
	movq	%rax, %r12
.L5813:
	movq	%r12, _ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip)
	mfence
	testb	$5, (%r12)
	je	.L5814
.L5920:
	pxor	%xmm0, %xmm0
	leaq	-80(%rbp), %rbx
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L5815
	subq	$8, %rsp
	leaq	-80(%rbp), %rbx
	movq	(%rax), %rax
	movq	%r15, %r9
	pushq	$6
	xorl	%r8d, %r8d
	leaq	.LC854(%rip), %rcx
	movq	%r12, %rdx
	pushq	%rbx
	movl	$98, %esi
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*24(%rax)
	addq	$64, %rsp
.L5815:
	leaq	-64(%rbp), %r13
	leaq	16+_ZTVN4node7tracing11TracedValueE(%rip), %r14
.L5819:
	movq	-8(%r13), %r12
	subq	$8, %r13
	testq	%r12, %r12
	je	.L5816
	movq	(%r12), %rax
	leaq	_ZN4node7tracing11TracedValueD0Ev(%rip), %rsi
	movq	8(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L5817
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%r14, (%r12)
	cmpq	%rax, %rdi
	je	.L5818
	call	_ZdlPv@PLT
.L5818:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L5816:
	cmpq	%rbx, %r13
	jne	.L5819
	jmp	.L5814
.L5817:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L5816
.L5857:
	movq	%r12, %rdi
	call	*%rax
	jmp	.L5856
.L5921:
	leaq	.LC855(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L5925:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8312:
	.size	_ZN4node11Environment10RunCleanupEv, .-_ZN4node11Environment10RunCleanupEv
	.weak	_ZTVN4node26TrackingTraceStateObserverE
	.section	.data.rel.ro.local._ZTVN4node26TrackingTraceStateObserverE,"awG",@progbits,_ZTVN4node26TrackingTraceStateObserverE,comdat
	.align 8
	.type	_ZTVN4node26TrackingTraceStateObserverE, @object
	.size	_ZTVN4node26TrackingTraceStateObserverE, 48
_ZTVN4node26TrackingTraceStateObserverE:
	.quad	0
	.quad	0
	.quad	_ZN4node26TrackingTraceStateObserverD1Ev
	.quad	_ZN4node26TrackingTraceStateObserverD0Ev
	.quad	_ZN4node26TrackingTraceStateObserver14OnTraceEnabledEv
	.quad	_ZN4node26TrackingTraceStateObserver15OnTraceDisabledEv
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node11IsolateDataE
	.section	.data.rel.ro.local._ZTVN4node11IsolateDataE,"awG",@progbits,_ZTVN4node11IsolateDataE,comdat
	.align 8
	.type	_ZTVN4node11IsolateDataE, @object
	.size	_ZTVN4node11IsolateDataE, 72
_ZTVN4node11IsolateDataE:
	.quad	0
	.quad	0
	.quad	_ZN4node11IsolateDataD1Ev
	.quad	_ZN4node11IsolateDataD0Ev
	.quad	_ZNK4node11IsolateData10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node11IsolateData14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node11IsolateData8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node13ImmediateInfoE
	.section	.data.rel.ro.local._ZTVN4node13ImmediateInfoE,"awG",@progbits,_ZTVN4node13ImmediateInfoE,comdat
	.align 8
	.type	_ZTVN4node13ImmediateInfoE, @object
	.size	_ZTVN4node13ImmediateInfoE, 72
_ZTVN4node13ImmediateInfoE:
	.quad	0
	.quad	0
	.quad	_ZN4node13ImmediateInfoD1Ev
	.quad	_ZN4node13ImmediateInfoD0Ev
	.quad	_ZNK4node13ImmediateInfo10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node13ImmediateInfo14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node13ImmediateInfo8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node8TickInfoE
	.section	.data.rel.ro.local._ZTVN4node8TickInfoE,"awG",@progbits,_ZTVN4node8TickInfoE,comdat
	.align 8
	.type	_ZTVN4node8TickInfoE, @object
	.size	_ZTVN4node8TickInfoE, 72
_ZTVN4node8TickInfoE:
	.quad	0
	.quad	0
	.quad	_ZN4node8TickInfoD1Ev
	.quad	_ZN4node8TickInfoD0Ev
	.quad	_ZNK4node8TickInfo10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node8TickInfo14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node8TickInfo8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node10AsyncHooksE
	.section	.data.rel.ro.local._ZTVN4node10AsyncHooksE,"awG",@progbits,_ZTVN4node10AsyncHooksE,comdat
	.align 8
	.type	_ZTVN4node10AsyncHooksE, @object
	.size	_ZTVN4node10AsyncHooksE, 72
_ZTVN4node10AsyncHooksE:
	.quad	0
	.quad	0
	.quad	_ZN4node10AsyncHooksD1Ev
	.quad	_ZN4node10AsyncHooksD0Ev
	.quad	_ZNK4node10AsyncHooks10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node10AsyncHooks14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node10AsyncHooks8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node14MemoryRetainer10IsRootNodeEv
	.weak	_ZTVN4node11EnvironmentE
	.section	.data.rel.ro.local._ZTVN4node11EnvironmentE,"awG",@progbits,_ZTVN4node11EnvironmentE,comdat
	.align 8
	.type	_ZTVN4node11EnvironmentE, @object
	.size	_ZTVN4node11EnvironmentE, 72
_ZTVN4node11EnvironmentE:
	.quad	0
	.quad	0
	.quad	_ZN4node11EnvironmentD1Ev
	.quad	_ZN4node11EnvironmentD0Ev
	.quad	_ZNK4node11Environment10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node11Environment14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node11Environment8SelfSizeEv
	.quad	_ZNK4node14MemoryRetainer13WrappedObjectEv
	.quad	_ZNK4node11Environment10IsRootNodeEv
	.weak	_ZTVN4node10BaseObjectE
	.section	.data.rel.ro._ZTVN4node10BaseObjectE,"awG",@progbits,_ZTVN4node10BaseObjectE,comdat
	.align 8
	.type	_ZTVN4node10BaseObjectE, @object
	.size	_ZTVN4node10BaseObjectE, 88
_ZTVN4node10BaseObjectE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node10BaseObject18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS1_7ExitEnvEvEUlS2_E_E4CallES2_
	.weak	_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node15ExclusiveAccessINS0_8HostPortENS0_9MutexBaseINS0_16LibuvMutexTraitsEEEEELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC856:
	.string	"../src/util-inl.h:325"
.LC857:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC858:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC856
	.quad	.LC857
	.quad	.LC858
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC859:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC860:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC861:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC859
	.quad	.LC860
	.quad	.LC861
	.weak	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0
	.section	.rodata.str1.1
.LC862:
	.string	"../src/aliased_buffer.h:74"
	.section	.rodata.str1.8
	.align 8
.LC863:
	.string	"(MultiplyWithOverflowCheck(sizeof(NativeT), count)) <= (ab->ByteLength() - byte_offset)"
	.align 8
.LC864:
	.ascii	"node::AliasedBufferBase<NativeT, V"
	.string	"8T, <template-parameter-1-3> >::AliasedBufferBase(v8::Isolate*, size_t, size_t, const node::AliasedBufferBase<unsigned char, v8::Uint8Array>&) [with NativeT = unsigned int; V8T = v8::Uint32Array; <template-parameter-1-3> = void; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,"awG",@progbits,_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, 24
_ZZN4node17AliasedBufferBaseIjN2v811Uint32ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0:
	.quad	.LC862
	.quad	.LC863
	.quad	.LC864
	.weak	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0
	.section	.rodata.str1.8
	.align 8
.LC865:
	.ascii	"node::AliasedBufferBase<Nativ"
	.string	"eT, V8T, <template-parameter-1-3> >::AliasedBufferBase(v8::Isolate*, size_t, size_t, const node::AliasedBufferBase<unsigned char, v8::Uint8Array>&) [with NativeT = double; V8T = v8::Float64Array; <template-parameter-1-3> = void; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,"awG",@progbits,_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0,comdat
	.align 16
	.type	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, @gnu_unique_object
	.size	_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0, 24
_ZZN4node17AliasedBufferBaseIdN2v812Float64ArrayEvEC4EPNS1_7IsolateEmmRKNS0_IhNS1_10Uint8ArrayEvEEE4args_0:
	.quad	.LC862
	.quad	.LC863
	.quad	.LC865
	.section	.rodata.str1.1
.LC866:
	.string	"../src/env.cc:1073"
.LC867:
	.string	"(platform) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC868:
	.string	"void node::Environment::AddArrayBufferAllocatorToKeepAliveUntilIsolateDispose(std::shared_ptr<v8::ArrayBuffer::Allocator>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEE4args, @object
	.size	_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEE4args, 24
_ZZN4node11Environment53AddArrayBufferAllocatorToKeepAliveUntilIsolateDisposeESt10shared_ptrIN2v811ArrayBuffer9AllocatorEEE4args:
	.quad	.LC866
	.quad	.LC867
	.quad	.LC868
	.globl	_ZN4node11Environment16thread_local_envE
	.bss
	.align 4
	.type	_ZN4node11Environment16thread_local_envE, @object
	.size	_ZN4node11Environment16thread_local_envE, 4
_ZN4node11Environment16thread_local_envE:
	.zero	4
	.section	.rodata.str1.1
.LC869:
	.string	"../src/env.cc:855"
.LC870:
	.string	"(now) >= (timer_base())"
	.section	.rodata.str1.8
	.align 8
.LC871:
	.string	"v8::Local<v8::Value> node::Environment::GetNow()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment6GetNowEvE4args, @object
	.size	_ZZN4node11Environment6GetNowEvE4args, 24
_ZZN4node11Environment6GetNowEvE4args:
	.quad	.LC869
	.quad	.LC870
	.quad	.LC871
	.local	_ZZN4node11Environment15InitializeLibuvEbE9init_once
	.comm	_ZZN4node11Environment15InitializeLibuvEbE9init_once,4,4
	.section	.rodata.str1.1
.LC872:
	.string	"../src/env.cc:463"
	.section	.rodata.str1.8
	.align 8
.LC873:
	.string	"(0) == (uv_timer_init(event_loop(), timer_handle()))"
	.align 8
.LC874:
	.string	"void node::Environment::InitializeLibuv(bool)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment15InitializeLibuvEbE4args, @object
	.size	_ZZN4node11Environment15InitializeLibuvEbE4args, 24
_ZZN4node11Environment15InitializeLibuvEbE4args:
	.quad	.LC872
	.quad	.LC873
	.quad	.LC874
	.section	.rodata.str1.1
.LC875:
	.string	"../src/env.cc:456"
.LC876:
	.string	"(base_object_count()) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC877:
	.string	"virtual node::Environment::~Environment()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11EnvironmentD4EvE4args_1, @object
	.size	_ZZN4node11EnvironmentD4EvE4args_1, 24
_ZZN4node11EnvironmentD4EvE4args_1:
	.quad	.LC875
	.quad	.LC876
	.quad	.LC877
	.local	_ZZN4node11EnvironmentD4EvE28trace_event_unique_atomic441
	.comm	_ZZN4node11EnvironmentD4EvE28trace_event_unique_atomic441,8,8
	.section	.rodata.str1.1
.LC878:
	.string	"../src/env.cc:416"
	.section	.rodata.str1.8
	.align 8
.LC879:
	.string	"file_handle_read_wrap_freelist_.empty()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11EnvironmentD4EvE4args, @object
	.size	_ZZN4node11EnvironmentD4EvE4args, 24
_ZZN4node11EnvironmentD4EvE4args:
	.quad	.LC878
	.quad	.LC879
	.quad	.LC877
	.local	_ZZN4node11EnvironmentC4EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEmE28trace_event_unique_atomic389
	.comm	_ZZN4node11EnvironmentC4EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEmE28trace_event_unique_atomic389,8,8
	.section	.rodata.str1.1
.LC880:
	.string	"../src/env.cc:285"
.LC881:
	.string	"primordials->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC882:
	.string	"void node::Environment::CreateProperties()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11Environment16CreatePropertiesEvE4args, @object
	.size	_ZZN4node11Environment16CreatePropertiesEvE4args, 24
_ZZN4node11Environment16CreatePropertiesEvE4args:
	.quad	.LC880
	.quad	.LC881
	.quad	.LC882
	.local	_ZN4nodeL14next_thread_idE
	.comm	_ZN4nodeL14next_thread_idE,8,8
	.section	.rodata.str1.1
.LC883:
	.string	"../src/env.cc:233"
	.section	.rodata.str1.8
	.align 8
.LC884:
	.string	"(0) == (uv_key_create(&Environment::thread_local_env))"
	.align 8
.LC885:
	.string	"void node::InitThreadLocalOnce()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node19InitThreadLocalOnceEvE4args, @object
	.size	_ZZN4node19InitThreadLocalOnceEvE4args, 24
_ZZN4node19InitThreadLocalOnceEvE4args:
	.quad	.LC883
	.quad	.LC884
	.quad	.LC885
	.section	.rodata.str1.1
.LC886:
	.string	"../src/env.cc:195"
.LC887:
	.string	"(allocator_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC888:
	.string	"node::IsolateData::IsolateData(v8::Isolate*, uv_loop_t*, node::MultiIsolatePlatform*, node::ArrayBufferAllocator*, const std::vector<long unsigned int>*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11IsolateDataC4EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEEE4args, @object
	.size	_ZZN4node11IsolateDataC4EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEEE4args, 24
_ZZN4node11IsolateDataC4EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEEE4args:
	.quad	.LC886
	.quad	.LC887
	.quad	.LC888
	.globl	_ZN4node11Environment18kNodeContextTagPtrE
	.align 8
	.type	_ZN4node11Environment18kNodeContextTagPtrE, @object
	.size	_ZN4node11Environment18kNodeContextTagPtrE, 8
_ZN4node11Environment18kNodeContextTagPtrE:
	.quad	_ZN4node11Environment15kNodeContextTagE
	.globl	_ZN4node11Environment15kNodeContextTagE
	.section	.rodata
	.align 4
	.type	_ZN4node11Environment15kNodeContextTagE, @object
	.size	_ZN4node11Environment15kNodeContextTagE, 4
_ZN4node11Environment15kNodeContextTagE:
	.long	7237476
	.weak	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381
	.section	.bss._ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381,"awG",@nobits,_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381,comdat
	.align 8
	.type	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381, @gnu_unique_object
	.size	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381, 8
_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381:
	.zero	8
	.weak	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378
	.section	.bss._ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378,"awG",@nobits,_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378,comdat
	.align 8
	.type	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378, @gnu_unique_object
	.size	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378, 8
_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378:
	.zero	8
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args
	.section	.rodata.str1.1
.LC889:
	.string	"../src/tracing/agent.h:91"
.LC890:
	.string	"(controller) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC891:
	.string	"node::tracing::TracingController* node::tracing::Agent::GetTracingController()"
	.section	.data.rel.ro.local._ZZN4node7tracing5Agent20GetTracingControllerEvE4args,"awG",@progbits,_ZZN4node7tracing5Agent20GetTracingControllerEvE4args,comdat
	.align 16
	.type	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, @gnu_unique_object
	.size	_ZZN4node7tracing5Agent20GetTracingControllerEvE4args, 24
_ZZN4node7tracing5Agent20GetTracingControllerEvE4args:
	.quad	.LC889
	.quad	.LC890
	.quad	.LC891
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0
	.section	.rodata.str1.8
	.align 8
.LC892:
	.string	"../src/memory_tracker-inl.h:269"
	.section	.rodata.str1.1
.LC893:
	.string	"(n->size_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC894:
	.string	"void node::MemoryTracker::Track(const node::MemoryRetainer*, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0:
	.quad	.LC892
	.quad	.LC893
	.quad	.LC894
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args
	.section	.rodata.str1.8
	.align 8
.LC895:
	.string	"../src/memory_tracker-inl.h:268"
	.section	.rodata.str1.1
.LC896:
	.string	"(CurrentNode()) == (n)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args:
	.quad	.LC895
	.quad	.LC896
	.quad	.LC894
	.weak	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args
	.section	.rodata.str1.8
	.align 8
.LC897:
	.string	"../src/memory_tracker-inl.h:27"
	.section	.rodata.str1.1
.LC898:
	.string	"(retainer_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC899:
	.string	"node::MemoryRetainerNode::MemoryRetainerNode(node::MemoryTracker*, const node::MemoryRetainer*)"
	.section	.data.rel.ro.local._ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,"awG",@progbits,_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,comdat
	.align 16
	.type	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, @gnu_unique_object
	.size	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, 24
_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args:
	.quad	.LC897
	.quad	.LC898
	.quad	.LC899
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC533:
	.quad	32
	.quad	0
	.align 16
.LC534:
	.quad	8
	.quad	0
	.align 16
.LC535:
	.quad	4
	.quad	0
	.align 16
.LC536:
	.long	0
	.long	1072693248
	.long	0
	.long	-1074790400
	.align 16
.LC537:
	.quad	3
	.quad	0
	.align 16
.LC538:
	.quad	2
	.quad	0
	.align 16
.LC539:
	.quad	1
	.quad	0
	.align 16
.LC540:
	.quad	36
	.quad	0
	.align 16
.LC541:
	.quad	80
	.quad	0
	.align 16
.LC542:
	.long	0
	.long	-1074790400
	.long	0
	.long	-1074790400
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC543:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16
	.align 16
.LC546:
	.quad	7
	.quad	48
	.align 16
.LC547:
	.quad	6
	.quad	0
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
