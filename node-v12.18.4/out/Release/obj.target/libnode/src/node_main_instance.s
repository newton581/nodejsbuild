	.file	"node_main_instance.cc"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node16NodeMainInstanceC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_
	.type	_ZN4node16NodeMainInstanceC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_, @function
_ZN4node16NodeMainInstanceC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_:
.LFB7649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$136, %rsp
	movq	%rsi, -160(%rbp)
	movq	8(%r8), %r15
	movq	%rdx, -168(%rbp)
	subq	(%r8), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rax
	movq	$0, 16(%rdi)
	sarq	$5, %rax
	movups	%xmm0, (%rdi)
	je	.L48
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L14
	movq	%r15, %rdi
	movq	%r8, -136(%rbp)
	call	_Znwm@PLT
	movq	-136(%rbp), %r8
	movq	%rax, %r12
.L3:
	movq	%r12, %xmm0
	addq	%r12, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 16(%r14)
	movups	%xmm0, (%r14)
	movq	8(%r8), %rax
	movq	(%r8), %r9
	movq	%rax, -152(%rbp)
	cmpq	%r9, %rax
	je	.L5
	leaq	-128(%rbp), %rax
	movq	%r9, %r15
	movq	%rax, -176(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L50:
	movzbl	(%r11), %eax
	movb	%al, 16(%r12)
.L10:
	movq	%r8, 8(%r12)
	addq	$32, %r15
	addq	$32, %r12
	movb	$0, (%rdi,%r8)
	cmpq	%r15, -152(%rbp)
	je	.L5
.L11:
	leaq	16(%r12), %rdi
	movq	8(%r15), %r8
	movq	%rdi, (%r12)
	movq	(%r15), %r11
	movq	%r11, %rax
	addq	%r8, %rax
	je	.L6
	testq	%r11, %r11
	je	.L16
.L6:
	movq	%r8, -128(%rbp)
	cmpq	$15, %r8
	ja	.L49
	cmpq	$1, %r8
	je	.L50
	testq	%r8, %r8
	je	.L10
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-176(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r8, -144(%rbp)
	movq	%r11, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r11
	movq	-144(%rbp), %r8
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-128(%rbp), %rax
	movq	%rax, 16(%r12)
.L8:
	movq	%r8, %rdx
	movq	%r11, %rsi
	call	memcpy@PLT
	movq	-128(%rbp), %r8
	movq	(%r12), %rdi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r12, 8(%r14)
	movq	8(%rbx), %r15
	pxor	%xmm0, %xmm0
	subq	(%rbx), %r15
	movq	$0, 40(%r14)
	movq	%r15, %rax
	movups	%xmm0, 24(%r14)
	sarq	$5, %rax
	je	.L51
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L14
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, %r12
.L13:
	movq	%r12, %xmm0
	addq	%r12, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 40(%r14)
	movups	%xmm0, 24(%r14)
	movq	8(%rbx), %r15
	movq	(%rbx), %r9
	cmpq	%r9, %r15
	je	.L15
	leaq	-128(%rbp), %rax
	movq	%r9, %rbx
	movq	%rax, -152(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L18:
	cmpq	$1, %r8
	jne	.L20
	movzbl	(%r11), %eax
	movb	%al, 16(%r12)
.L21:
	addq	$32, %rbx
	movq	%r8, 8(%r12)
	addq	$32, %r12
	movb	$0, (%rdi,%r8)
	cmpq	%rbx, %r15
	je	.L15
.L22:
	leaq	16(%r12), %rdi
	movq	8(%rbx), %r8
	movq	%rdi, (%r12)
	movq	(%rbx), %r11
	movq	%r11, %rax
	addq	%r8, %rax
	je	.L25
	testq	%r11, %r11
	je	.L16
.L25:
	movq	%r8, -128(%rbp)
	cmpq	$15, %r8
	jbe	.L18
	movq	-152(%rbp), %rsi
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r11, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %r11
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-128(%rbp), %rax
	movq	%rax, 16(%r12)
.L19:
	movq	%r8, %rdx
	movq	%r11, %rsi
	call	memcpy@PLT
	movq	-128(%rbp), %r8
	movq	(%r12), %rdi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L20:
	testq	%r8, %r8
	je	.L21
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L15:
	movq	-160(%rbp), %r15
	xorl	%eax, %eax
	movq	%r12, 32(%r14)
	movl	$2416, %edi
	movq	$0, 48(%r14)
	movq	%r15, 56(%r14)
	movq	%r13, 64(%r14)
	movq	$0, 72(%r14)
	movw	%ax, 80(%r14)
	call	_Znwm@PLT
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	-168(%rbp), %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	movq	%rax, %rbx
	call	_ZN4node11IsolateDataC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE@PLT
	movq	72(%r14), %rdi
	movq	%rbx, 72(%r14)
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	movq	56(%r14), %rdi
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	$0, -72(%rbp)
	movq	$3, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN4node22SetIsolateMiscHandlersEPN2v87IsolateERKNS_15IsolateSettingsE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L52
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%r12d, %r12d
	jmp	.L13
.L16:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L14:
	call	_ZSt17__throw_bad_allocv@PLT
.L52:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7649:
	.size	_ZN4node16NodeMainInstanceC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_, .-_ZN4node16NodeMainInstanceC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_
	.globl	_ZN4node16NodeMainInstanceC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_
	.set	_ZN4node16NodeMainInstanceC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_,_ZN4node16NodeMainInstanceC2EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_
	.align 2
	.p2align 4
	.globl	_ZN4node16NodeMainInstance6CreateEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_
	.type	_ZN4node16NodeMainInstance6CreateEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_, @function
_ZN4node16NodeMainInstance6CreateEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$88, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %r8
	movq	%r15, %rcx
	movq	%rax, %rbx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN4node16NodeMainInstanceC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_
	movq	%rbx, (%r12)
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7653:
	.size	_ZN4node16NodeMainInstance6CreateEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_, .-_ZN4node16NodeMainInstance6CreateEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISE_EESI_
	.align 2
	.p2align 4
	.globl	_ZN4node16NodeMainInstanceC2EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE
	.type	_ZN4node16NodeMainInstanceC2EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE, @function
_ZN4node16NodeMainInstanceC2EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE:
.LFB7658:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$152, %rsp
	movq	16(%rbp), %rax
	movq	8(%r8), %r15
	movq	%rdx, -136(%rbp)
	subq	(%r8), %r15
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, %rax
	movq	$0, 16(%rdi)
	sarq	$5, %rax
	movups	%xmm0, (%rdi)
	je	.L107
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L68
	movq	%r15, %rdi
	movq	%r8, -152(%rbp)
	call	_Znwm@PLT
	movq	-152(%rbp), %r8
	movq	%rax, %rcx
.L57:
	movq	%rcx, %xmm0
	addq	%rcx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 16(%r14)
	movups	%xmm0, (%r14)
	movq	8(%r8), %rax
	movq	(%r8), %r9
	movq	%rax, -152(%rbp)
	cmpq	%r9, %rax
	je	.L59
	leaq	-128(%rbp), %rax
	movq	%rbx, -184(%rbp)
	movq	%rcx, %r15
	movq	%r9, %rbx
	movq	%rax, -160(%rbp)
	movq	%r13, -168(%rbp)
	movq	%r12, -176(%rbp)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L110:
	movzbl	0(%r13), %eax
	movb	%al, 16(%r15)
.L64:
	movq	%r12, 8(%r15)
	addq	$32, %rbx
	addq	$32, %r15
	movb	$0, (%rdi,%r12)
	cmpq	%rbx, -152(%rbp)
	je	.L108
.L65:
	leaq	16(%r15), %rdi
	movq	8(%rbx), %r12
	movq	%rdi, (%r15)
	movq	(%rbx), %r13
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L60
	testq	%r13, %r13
	je	.L70
.L60:
	movq	%r12, -128(%rbp)
	cmpq	$15, %r12
	ja	.L109
	cmpq	$1, %r12
	je	.L110
	testq	%r12, %r12
	je	.L64
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-160(%rbp), %rsi
	movq	%r15, %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-128(%rbp), %rax
	movq	%rax, 16(%r15)
.L62:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-128(%rbp), %r12
	movq	(%r15), %rdi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L108:
	movq	-168(%rbp), %r13
	movq	-176(%rbp), %r12
	movq	%r15, %rcx
	movq	-184(%rbp), %rbx
.L59:
	movq	%rcx, 8(%r14)
	movq	8(%rbx), %r15
	pxor	%xmm0, %xmm0
	subq	(%rbx), %r15
	movq	$0, 40(%r14)
	movq	%r15, %rax
	movups	%xmm0, 24(%r14)
	sarq	$5, %rax
	je	.L111
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L68
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%rax, %rcx
.L67:
	movq	%rcx, %xmm0
	addq	%rcx, %r15
	punpcklqdq	%xmm0, %xmm0
	movq	%r15, 40(%r14)
	movups	%xmm0, 24(%r14)
	movq	8(%rbx), %r15
	movq	(%rbx), %r9
	cmpq	%r9, %r15
	je	.L69
	leaq	-128(%rbp), %rax
	movq	%r15, -160(%rbp)
	movq	%rcx, %rbx
	movq	%r9, %r15
	movq	%rax, -152(%rbp)
	movq	%r13, -168(%rbp)
	movq	%r12, -176(%rbp)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L72:
	cmpq	$1, %r12
	jne	.L74
	movzbl	0(%r13), %eax
	movb	%al, 16(%rbx)
.L75:
	movq	%r12, 8(%rbx)
	addq	$32, %r15
	addq	$32, %rbx
	movb	$0, (%rdi,%r12)
	cmpq	%r15, -160(%rbp)
	je	.L112
.L76:
	leaq	16(%rbx), %rdi
	movq	8(%r15), %r12
	movq	%rdi, (%rbx)
	movq	(%r15), %r13
	movq	%r13, %rax
	addq	%r12, %rax
	je	.L82
	testq	%r13, %r13
	je	.L70
.L82:
	movq	%r12, -128(%rbp)
	cmpq	$15, %r12
	jbe	.L72
	movq	-152(%rbp), %rsi
	movq	%rbx, %rdi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-128(%rbp), %rax
	movq	%rax, 16(%rbx)
.L73:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-128(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L74:
	testq	%r12, %r12
	je	.L75
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-168(%rbp), %r13
	movq	-176(%rbp), %r12
	movq	%rbx, %rcx
.L69:
	movq	%rcx, 32(%r14)
	xorl	%esi, %esi
	leaq	48(%r14), %rdi
	call	_ZN4node20ArrayBufferAllocator6CreateEb@PLT
	movl	$1, %eax
	movq	%r12, 64(%r14)
	movw	%ax, 80(%r14)
	movq	48(%r14), %rax
	movq	$0, 72(%r14)
	movq	$0, 56(%r14)
	movq	%rax, 80(%r13)
	call	_ZN2v87Isolate8AllocateEv@PLT
	movq	%rax, 56(%r14)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L113
	movq	(%r12), %rax
	movq	-136(%rbp), %rdx
	movq	%r12, %rdi
	call	*192(%rax)
	movq	%r13, %rdi
	call	_ZN4node29SetIsolateCreateParamsForNodeEPN2v87Isolate12CreateParamsE@PLT
	movq	56(%r14), %rdi
	movq	%r13, %rsi
	call	_ZN2v87Isolate10InitializeEPS0_RKNS0_12CreateParamsE@PLT
	movq	-144(%rbp), %rax
	testq	%rax, %rax
	setne	81(%r14)
	jne	.L114
.L78:
	movq	48(%r14), %r15
	movq	56(%r14), %r13
	movl	$2416, %edi
	call	_Znwm@PLT
	movq	-144(%rbp), %r9
	movq	-136(%rbp), %rdx
	movq	%r12, %rcx
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r13, %rsi
	movq	%rax, %rbx
	call	_ZN4node11IsolateDataC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE@PLT
	movq	72(%r14), %rdi
	movq	%rbx, 72(%r14)
	testq	%rdi, %rdi
	je	.L79
	movq	(%rdi), %rax
	call	*8(%rax)
.L79:
	movq	56(%r14), %rdi
	leaq	-128(%rbp), %r12
	pxor	%xmm0, %xmm0
	movq	$0, -72(%rbp)
	movq	%r12, %rsi
	movq	$3, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN4node22SetIsolateMiscHandlersEPN2v87IsolateERKNS_15IsolateSettingsE@PLT
	cmpb	$0, 81(%r14)
	je	.L115
.L55:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L116
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	56(%r14), %rdi
	movq	%r12, %rsi
	call	_ZN4node23SetIsolateErrorHandlersEPN2v87IsolateERKNS_15IsolateSettingsE@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%ecx, %ecx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L111:
	xorl	%ecx, %ecx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	cmpq	$0, 88(%r13)
	jne	.L78
	leaq	_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L70:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L68:
	call	_ZSt17__throw_bad_allocv@PLT
.L116:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7658:
	.size	_ZN4node16NodeMainInstanceC2EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE, .-_ZN4node16NodeMainInstanceC2EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE
	.globl	_ZN4node16NodeMainInstanceC1EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE
	.set	_ZN4node16NodeMainInstanceC1EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE,_ZN4node16NodeMainInstanceC2EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEE
	.align 2
	.p2align 4
	.globl	_ZN4node16NodeMainInstance7DisposeEv
	.type	_ZN4node16NodeMainInstance7DisposeEv, @function
_ZN4node16NodeMainInstance7DisposeEv:
.LFB7660:
	.cfi_startproc
	endbr64
	cmpb	$0, 80(%rdi)
	jne	.L122
	movq	64(%rdi), %r8
	movq	56(%rdi), %rsi
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	176(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L122:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node16NodeMainInstance7DisposeEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7660:
	.size	_ZN4node16NodeMainInstance7DisposeEv, .-_ZN4node16NodeMainInstance7DisposeEv
	.align 2
	.p2align 4
	.globl	_ZN4node16NodeMainInstanceD2Ev
	.type	_ZN4node16NodeMainInstanceD2Ev, @function
_ZN4node16NodeMainInstanceD2Ev:
.LFB7662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpb	$0, 80(%rdi)
	je	.L179
	movq	64(%rdi), %rdi
	movq	56(%rbx), %rsi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	56(%rbx), %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L140
	movq	(%rdi), %rax
	call	*8(%rax)
.L140:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	movq	32(%rbx), %r13
	movq	24(%rbx), %r12
	cmpq	%r12, %r13
	je	.L142
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L143
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L146
.L144:
	movq	24(%rbx), %r12
.L142:
	testq	%r12, %r12
	je	.L147
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L147:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L148
	.p2align 4,,10
	.p2align 3
.L152:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L149
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L152
.L150:
	movq	(%rbx), %r12
.L148:
	testq	%r12, %r12
	je	.L123
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L149:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L152
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L143:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L146
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L123:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L125
	movq	(%rdi), %rax
	call	*8(%rax)
.L125:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L126
	movq	(%rdi), %rax
	call	*8(%rax)
.L126:
	movq	32(%rbx), %r13
	movq	24(%rbx), %r12
	cmpq	%r12, %r13
	je	.L127
	.p2align 4,,10
	.p2align 3
.L131:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L128
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L131
.L129:
	movq	24(%rbx), %r12
.L127:
	testq	%r12, %r12
	je	.L132
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L132:
	movq	8(%rbx), %r13
	movq	(%rbx), %r12
	cmpq	%r12, %r13
	je	.L148
.L137:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L134
.L180:
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r13
	je	.L150
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L180
.L134:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L137
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$32, %r12
	cmpq	%r12, %r13
	jne	.L131
	jmp	.L129
	.cfi_endproc
.LFE7662:
	.size	_ZN4node16NodeMainInstanceD2Ev, .-_ZN4node16NodeMainInstanceD2Ev
	.globl	_ZN4node16NodeMainInstanceD1Ev
	.set	_ZN4node16NodeMainInstanceD1Ev,_ZN4node16NodeMainInstanceD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node16NodeMainInstance21CreateMainEnvironmentEPi
	.type	_ZN4node16NodeMainInstance21CreateMainEnvironmentEPi, @function
_ZN4node16NodeMainInstance21CreateMainEnvironmentEPi:
.LFB7667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	movq	%rdi, %r14
	pushq	%r13
	movq	%r15, %rdi
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$136, %rsp
	movq	56(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, (%rdx)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	72(%r12), %rax
	movq	2408(%rax), %r13
	movq	2400(%rax), %rcx
	testq	%r13, %r13
	je	.L182
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	leaq	8(%r13), %rax
	testq	%rdx, %rdx
	je	.L183
	lock addl	$1, (%rax)
	movzbl	24(%rcx), %esi
	testq	%rdx, %rdx
	je	.L210
.L198:
	movl	$-1, %ecx
	lock xaddl	%ecx, (%rax)
	movl	%ecx, %eax
	cmpl	$1, %eax
	je	.L211
.L186:
	movq	56(%r12), %rdi
	testb	%sil, %sil
	jne	.L212
.L189:
	cmpb	$0, 81(%r12)
	je	.L190
	subq	$8, %rsp
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	call	_ZN2v87Context12FromSnapshotEPNS_7IsolateEmNS_33DeserializeInternalFieldsCallbackEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_5ValueEEEPNS_14MicrotaskQueueE@PLT
	popq	%rsi
	popq	%rdi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L213
.L191:
	movq	%r13, %rdi
	call	_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE@PLT
	movq	56(%r12), %rdi
	pxor	%xmm0, %xmm0
	leaq	-128(%rbp), %rsi
	movq	$0, -72(%rbp)
	movq	$3, -128(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	call	_ZN4node23SetIsolateErrorHandlersEPN2v87IsolateERKNS_15IsolateSettingsE@PLT
	testq	%r13, %r13
	je	.L214
.L193:
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	72(%r12), %rsi
	movl	$3288, %edi
	movq	%rsi, -168(%rbp)
	call	_Znwm@PLT
	subq	$8, %rsp
	movq	%r12, %rcx
	movq	%r13, %rdx
	pushq	$-1
	movq	%rax, %rdi
	leaq	24(%r12), %r8
	movl	$7, %r9d
	movq	-168(%rbp), %rsi
	movq	%rax, -168(%rbp)
	call	_ZN4node11EnvironmentC1EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm@PLT
	movq	-168(%rbp), %rdi
	movzbl	_ZN4node11per_process15v8_is_profilingE(%rip), %esi
	movq	%rdi, (%r14)
	call	_ZN4node11Environment15InitializeLibuvEb@PLT
	movq	(%r14), %rdi
	call	_ZN4node11Environment21InitializeDiagnosticsEv@PLT
	movq	(%r14), %rdi
	leaq	-128(%rbp), %rsi
	movq	$0, -128(%rbp)
	call	_ZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EE@PLT
	movq	-128(%rbp), %r12
	popq	%rdx
	movl	%eax, (%rbx)
	popq	%rcx
	testq	%r12, %r12
	je	.L194
	movq	%r12, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movl	(%rbx), %eax
.L194:
	testl	%eax, %eax
	je	.L215
.L196:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L216
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	.cfi_restore_state
	addl	$1, 8(%r13)
	movzbl	24(%rcx), %esi
	testq	%rdx, %rdx
	jne	.L198
.L210:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L186
.L211:
	movq	0(%r13), %rax
	movb	%sil, -168(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movzbl	-168(%rbp), %esi
	testq	%rdx, %rdx
	je	.L187
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L188:
	cmpl	$1, %eax
	jne	.L186
	movq	0(%r13), %rax
	movb	%sil, -168(%rbp)
	movq	%r13, %rdi
	call	*24(%rax)
	movzbl	-168(%rbp), %esi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L190:
	xorl	%esi, %esi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.L193
.L214:
	leaq	_ZZN4node16NodeMainInstance21CreateMainEnvironmentEPiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler24StartTrackingHeapObjectsEb@PLT
	movq	56(%r12), %rdi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L182:
	movzbl	24(%rcx), %esi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L215:
	movq	(%r14), %rdi
	call	_ZN4node11Environment16RunBootstrappingEv@PLT
	testq	%rax, %rax
	jne	.L196
	movl	$1, (%rbx)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L187:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L213:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L191
.L216:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7667:
	.size	_ZN4node16NodeMainInstance21CreateMainEnvironmentEPi, .-_ZN4node16NodeMainInstance21CreateMainEnvironmentEPi
	.align 2
	.p2align 4
	.globl	_ZN4node16NodeMainInstance3RunEv
	.type	_ZN4node16NodeMainInstance3RunEv, @function
_ZN4node16NodeMainInstance3RunEv:
.LFB7664:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-272(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-288(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$296, %rsp
	movq	56(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v86Locker10InitializeEPNS_7IsolateE@PLT
	movq	56(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -312(%rbp)
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	56(%rbx), %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-296(%rbp), %rdi
	leaq	-300(%rbp), %rdx
	movq	%rbx, %rsi
	movl	$0, -300(%rbp)
	call	_ZN4node16NodeMainInstance21CreateMainEnvironmentEPi
	movq	-296(%rbp), %rax
	testq	%rax, %rax
	je	.L254
	movq	3280(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -320(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movl	-300(%rbp), %eax
	testl	%eax, %eax
	je	.L255
.L219:
	movq	-296(%rbp), %rax
	leaq	-208(%rbp), %r12
	movl	$1, %r15d
	movb	$0, 1930(%rax)
	mfence
	movq	-296(%rbp), %rdi
	call	_ZN4node11Environment24stop_sub_worker_contextsEv@PLT
	call	_ZN4node10ResetStdioEv@PLT
	movq	-296(%rbp), %rdi
	call	_ZN4node11Environment10RunCleanupEv@PLT
	movl	$19, %ecx
	xorl	%eax, %eax
	movq	%r12, %rdi
	rep stosq
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L236:
	addl	$1, %r15d
	cmpl	$32, %r15d
	je	.L256
.L237:
	movl	%r15d, %eax
	andl	$-9, %eax
	cmpl	$19, %eax
	je	.L236
	cmpl	$9, %r15d
	je	.L236
	xorl	%eax, %eax
	cmpl	$13, %r15d
	movq	%r12, %rsi
	movl	%r15d, %edi
	sete	%al
	xorl	%edx, %edx
	movq	%rax, -208(%rbp)
	call	sigaction@PLT
	testl	%eax, %eax
	je	.L236
	leaq	_ZZN4node16NodeMainInstance3RunEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	movq	-296(%rbp), %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %rdi
	movq	56(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	-320(%rbp), %rdi
	movl	-300(%rbp), %ebx
	call	_ZN2v87Context4ExitEv@PLT
	movq	-296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L238
	movq	(%rdi), %rax
	call	*8(%rax)
.L238:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-312(%rbp), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v86LockerD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L257
	addq	$296, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movq	-296(%rbp), %rdi
	call	_ZN4node15LoadEnvironmentEPNS_11EnvironmentE@PLT
	movq	-296(%rbp), %rax
	movq	1648(%rax), %rdi
	movq	1640(%rax), %rsi
	testq	%rdi, %rdi
	je	.L220
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rcx
	leaq	8(%rdi), %rdx
	testq	%rcx, %rcx
	je	.L221
	lock addl	$1, (%rdx)
.L222:
	movzbl	468(%rsi), %esi
	movb	%sil, 1401(%rax)
	testq	%rcx, %rcx
	je	.L258
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
.L223:
	cmpl	$1, %eax
	je	.L259
.L225:
	movq	56(%rbx), %rsi
	leaq	-240(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v815SealHandleScopeC1EPNS_7IsolateE@PLT
	movq	-296(%rbp), %rax
	movq	1864(%rax), %r15
	call	uv_hrtime@PLT
	movl	$3, %esi
	movq	%rax, %rdx
	movq	%r15, %rdi
	call	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L231:
	movq	-296(%rbp), %rax
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	je	.L234
.L235:
	movq	-296(%rbp), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L234
.L233:
	movq	-296(%rbp), %rax
	xorl	%esi, %esi
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_run@PLT
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %rdi
	movq	56(%rbx), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	-296(%rbp), %rax
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	je	.L232
	movq	-296(%rbp), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	je	.L235
.L232:
	movq	-296(%rbp), %rax
	movq	360(%rax), %rax
	movq	2360(%rax), %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	jne	.L231
	movq	-296(%rbp), %rdi
	call	_ZN4node14EmitBeforeExitEPNS_11EnvironmentE@PLT
	jmp	.L231
.L221:
	addl	$1, 8(%rdi)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L234:
	movq	-296(%rbp), %rax
	movq	1864(%rax), %r15
	call	uv_hrtime@PLT
	movl	$4, %esi
	movq	%rax, %rdx
	movq	%r15, %rdi
	call	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	movq	%r12, %rdi
	call	_ZN2v815SealHandleScopeD1Ev@PLT
	movq	-296(%rbp), %rdi
	movb	$0, 1401(%rdi)
	call	_ZN4node8EmitExitEPNS_11EnvironmentE@PLT
	movl	%eax, -300(%rbp)
	jmp	.L219
.L254:
	leaq	_ZZN4node16NodeMainInstance3RunEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L220:
	movzbl	468(%rsi), %edx
	movb	%dl, 1401(%rax)
	jmp	.L225
.L258:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L223
.L259:
	movq	(%rdi), %rax
	movq	%rdi, -328(%rbp)
	call	*16(%rax)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rcx
	movq	-328(%rbp), %rdi
	testq	%rcx, %rcx
	je	.L226
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L227:
	cmpl	$1, %eax
	jne	.L225
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L225
.L226:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L227
.L257:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7664:
	.size	_ZN4node16NodeMainInstance3RunEv, .-_ZN4node16NodeMainInstance3RunEv
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"../src/node_main_instance.cc:205"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"!context.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"std::unique_ptr<node::Environment> node::NodeMainInstance::CreateMainEnvironment(int*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node16NodeMainInstance21CreateMainEnvironmentEPiE4args, @object
	.size	_ZZN4node16NodeMainInstance21CreateMainEnvironmentEPiE4args, 24
_ZZN4node16NodeMainInstance21CreateMainEnvironmentEPiE4args:
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"../src/node_main_instance.cc:166"
	.align 8
.LC5:
	.string	"(0) == (sigaction(nr, &act, nullptr))"
	.align 8
.LC6:
	.string	"int node::NodeMainInstance::Run()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node16NodeMainInstance3RunEvE4args_0, @object
	.size	_ZZN4node16NodeMainInstance3RunEvE4args_0, 24
_ZZN4node16NodeMainInstance3RunEvE4args_0:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"../src/node_main_instance.cc:115"
	.section	.rodata.str1.1
.LC8:
	.string	"(env) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node16NodeMainInstance3RunEvE4args, @object
	.size	_ZZN4node16NodeMainInstance3RunEvE4args, 24
_ZZN4node16NodeMainInstance3RunEvE4args:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC6
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"../src/node_main_instance.cc:95"
	.section	.rodata.str1.1
.LC10:
	.string	"!owns_isolate_"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"void node::NodeMainInstance::Dispose()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node16NodeMainInstance7DisposeEvE4args, @object
	.size	_ZZN4node16NodeMainInstance7DisposeEvE4args, 24
_ZZN4node16NodeMainInstance7DisposeEvE4args:
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"../src/node_main_instance.cc:79"
	.align 8
.LC13:
	.string	"!(deserialize_mode_) || (params->external_references != nullptr)"
	.align 8
.LC14:
	.ascii	"no"
	.string	"de::NodeMainInstance::NodeMainInstance(v8::Isolate::CreateParams*, uv_loop_t*, node::MultiIsolatePlatform*, const std::vector<std::__cxx11::basic_string<char> >&, const std::vector<std::__cxx11::basic_string<char> >&, const std::vector<long unsigned int>*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args_0, @object
	.size	_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args_0, 24
_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args_0:
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"../src/node_main_instance.cc:70"
	.section	.rodata.str1.1
.LC16:
	.string	"(isolate_) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args, @object
	.size	_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args, 24
_ZZN4node16NodeMainInstanceC4EPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISF_EESJ_PKS9_ImSaImEEE4args:
	.quad	.LC15
	.quad	.LC16
	.quad	.LC14
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
