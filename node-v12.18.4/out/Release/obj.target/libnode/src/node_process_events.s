	.file	"node_process_events.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"emit"
	.text
	.p2align 4
	.globl	_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE
	.type	_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE, @function
_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE:
.LFB7080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdx, %r12
	xorl	%edx, %edx
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	352(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L4
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm1
	leaq	-64(%rbp), %r8
	movq	%r13, %rdi
	movq	2968(%rbx), %rsi
	movq	%rax, %xmm0
	movl	$2, %ecx
	leaq	.LC0(%rip), %rdx
	movq	%r12, -56(%rbp)
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEENS_13async_contextE@PLT
.L4:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L11
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7080:
	.size	_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE, .-_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE
	.p2align 4
	.globl	_ZN4node25ProcessEmitWarningGenericEPNS_11EnvironmentEPKcS3_S3_
	.type	_ZN4node25ProcessEmitWarningGenericEPNS_11EnvironmentEPKcS3_S3_, @function
_ZN4node25ProcessEmitWarningGenericEPNS_11EnvironmentEPKcS3_S3_:
.LFB7082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	jne	.L42
.L13:
	movl	$1, %eax
.L16:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L43
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%rsi, -120(%rbp)
	movq	%rdi, %rbx
	movzbl	2664(%rdi), %eax
	testb	%al, %al
	jne	.L13
	movq	352(%rdi), %rsi
	leaq	-112(%rbp), %r14
	movq	%rdx, %r12
	movq	%rcx, %r13
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	360(%rbx), %rax
	movq	2968(%rbx), %rdi
	movq	3280(%rbx), %rsi
	movq	584(%rax), %rdx
	movq	%rdi, -128(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-120(%rbp), %r8
	testq	%rax, %rax
	je	.L44
	movq	%rax, %rdi
	movq	%r8, -136(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-120(%rbp), %r10
	movq	-136(%rbp), %r8
	testb	%al, %al
	jne	.L29
	movl	$1, %eax
.L19:
	movq	%r15, %rdi
	movl	%eax, -120(%rbp)
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	-120(%rbp), %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L29:
	movq	352(%rbx), %rdi
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r8, %rsi
	movl	$-1, %ecx
	movq	%r10, -120(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-120(%rbp), %r10
	testq	%rax, %rax
	je	.L45
	movq	%rax, -80(%rbp)
	movl	$1, %ecx
	testq	%r12, %r12
	je	.L28
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r10, -120(%rbp)
	movl	$-1, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-120(%rbp), %r10
	testq	%rax, %rax
	je	.L46
	movq	%rax, -72(%rbp)
	movl	$2, %ecx
	testq	%r13, %r13
	je	.L28
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r10, -120(%rbp)
	movl	$-1, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-120(%rbp), %r10
	testq	%rax, %rax
	je	.L47
	movq	%rax, -64(%rbp)
	movl	$3, %ecx
.L28:
	movq	3280(%rbx), %rsi
	movq	-128(%rbp), %rdx
	leaq	-80(%rbp), %r8
	movq	%r10, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r8
	movl	$257, %eax
	testq	%r8, %r8
	jne	.L19
	xorb	%al, %al
	movb	$0, %ah
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%eax, %eax
	movb	$0, %ah
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
	movq	$0, -80(%rbp)
	movb	$0, %ah
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L46:
	xorl	%eax, %eax
	movq	$0, -72(%rbp)
	movb	$0, %ah
	jmp	.L19
.L47:
	xorl	%eax, %eax
	movq	$0, -64(%rbp)
	movb	$0, %ah
	jmp	.L19
.L43:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7082:
	.size	_ZN4node25ProcessEmitWarningGenericEPNS_11EnvironmentEPKcS3_S3_, .-_ZN4node25ProcessEmitWarningGenericEPNS_11EnvironmentEPKcS3_S3_
	.p2align 4
	.globl	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz
	.type	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz, @function
_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz:
.LFB7083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1336, %rsp
	movq	%rdx, -208(%rbp)
	movq	%rcx, -200(%rbp)
	movq	%r8, -192(%rbp)
	movq	%r9, -184(%rbp)
	testb	%al, %al
	je	.L49
	movaps	%xmm0, -176(%rbp)
	movaps	%xmm1, -160(%rbp)
	movaps	%xmm2, -144(%rbp)
	movaps	%xmm3, -128(%rbp)
	movaps	%xmm4, -112(%rbp)
	movaps	%xmm5, -96(%rbp)
	movaps	%xmm6, -80(%rbp)
	movaps	%xmm7, -64(%rbp)
.L49:
	movq	%fs:40, %rax
	movq	%rax, -232(%rbp)
	xorl	%eax, %eax
	leaq	-1264(%rbp), %r13
	leaq	16(%rbp), %rax
	movq	%rsi, %r8
	movq	%rax, -1280(%rbp)
	leaq	-1288(%rbp), %r9
	movl	$1024, %ecx
	movq	%r13, %rdi
	leaq	-224(%rbp), %rax
	movl	$1, %edx
	movl	$1024, %esi
	movl	$16, -1288(%rbp)
	movl	$48, -1284(%rbp)
	movq	%rax, -1272(%rbp)
	call	__vsnprintf_chk@PLT
	movzbl	1930(%rbx), %eax
	testb	%al, %al
	jne	.L70
.L50:
	xorl	%r12d, %r12d
	movl	$1, %r15d
.L53:
	xorl	%eax, %eax
	movl	%r12d, %ecx
	movq	-232(%rbp), %rdx
	xorq	%fs:40, %rdx
	movb	%r15b, %al
	movb	%cl, %ah
	jne	.L71
	addq	$1336, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	.cfi_restore_state
	movzbl	2664(%rbx), %r15d
	testb	%r15b, %r15b
	jne	.L50
	movq	352(%rbx), %rsi
	leaq	-1344(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r10
	movq	%r10, %rdi
	movq	%r10, -1352(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	360(%rbx), %rax
	movq	2968(%rbx), %rdi
	movq	3280(%rbx), %rsi
	movq	584(%rax), %rdx
	movq	%rdi, -1360(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-1352(%rbp), %r10
	testq	%rax, %rax
	je	.L56
	movq	%rax, %rdi
	movq	%r10, -1368(%rbp)
	movq	%rax, -1352(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-1352(%rbp), %r9
	movq	-1368(%rbp), %r10
	testb	%al, %al
	je	.L72
	movq	352(%rbx), %rdi
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$-1, %ecx
	movq	%r9, -1368(%rbp)
	movq	%r10, -1352(%rbp)
	movq	$0, -1296(%rbp)
	movaps	%xmm0, -1312(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1352(%rbp), %r10
	movq	-1368(%rbp), %r9
	testq	%rax, %rax
	je	.L73
	movq	3280(%rbx), %rsi
	movq	-1360(%rbp), %rdx
	movl	$1, %ecx
	movq	%r9, %rdi
	leaq	-1312(%rbp), %r8
	movq	%r10, -1352(%rbp)
	movq	%rax, -1312(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	-1352(%rbp), %r10
	testq	%rax, %rax
	setne	%r12b
	movl	%r12d, %r15d
.L56:
	movq	%r10, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%r12d, %r12d
	movl	$1, %r15d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L73:
	movq	$0, -1312(%rbp)
	jmp	.L56
.L71:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7083:
	.size	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz, .-_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz
	.section	.rodata.str1.1
.LC1:
	.string	"DeprecationWarning"
	.text
	.p2align 4
	.globl	_ZN4node29ProcessEmitDeprecationWarningEPNS_11EnvironmentEPKcS3_
	.type	_ZN4node29ProcessEmitDeprecationWarningEPNS_11EnvironmentEPKcS3_, @function
_ZN4node29ProcessEmitDeprecationWarningEPNS_11EnvironmentEPKcS3_:
.LFB7094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	1930(%rdi), %eax
	testb	%al, %al
	jne	.L103
.L75:
	xorl	%r13d, %r13d
	movl	$1, %r15d
.L78:
	xorl	%eax, %eax
	movl	%r13d, %ecx
	movb	%r15b, %al
	movb	%cl, %ah
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L104
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	%rsi, -128(%rbp)
	movq	%rdi, %rbx
	movzbl	2664(%rdi), %r15d
	testb	%r15b, %r15b
	jne	.L75
	movq	352(%rdi), %rsi
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	360(%rbx), %rax
	movq	2968(%rbx), %rdi
	movq	3280(%rbx), %rsi
	movq	584(%rax), %rdx
	movq	%rdi, -136(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-128(%rbp), %r11
	testq	%rax, %rax
	je	.L83
	movq	%rax, %rdi
	movq	%r11, -144(%rbp)
	movq	%rax, -128(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-128(%rbp), %r9
	movq	-144(%rbp), %r11
	testb	%al, %al
	je	.L105
	movq	352(%rbx), %rdi
	pxor	%xmm0, %xmm0
	xorl	%edx, %edx
	movq	%r11, %rsi
	movl	$-1, %ecx
	movq	%r9, -128(%rbp)
	movq	$0, -64(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-128(%rbp), %r9
	testq	%rax, %rax
	je	.L106
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%r9, -128(%rbp)
	movq	%rax, -80(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-128(%rbp), %r9
	testq	%rax, %rax
	je	.L107
	cmpq	$0, -120(%rbp)
	movq	%rax, -72(%rbp)
	movl	$2, %ecx
	je	.L84
	movq	-120(%rbp), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r9, -128(%rbp)
	movq	352(%rbx), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-128(%rbp), %r9
	testq	%rax, %rax
	je	.L108
	movq	%rax, -64(%rbp)
	movl	$3, %ecx
.L84:
	movq	3280(%rbx), %rsi
	movq	-136(%rbp), %rdx
	leaq	-80(%rbp), %r8
	movq	%r9, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	setne	%r13b
	movl	%r13d, %r15d
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L105:
	xorl	%r13d, %r13d
	movl	$1, %r15d
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L107:
	movq	$0, -72(%rbp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L106:
	movq	$0, -80(%rbp)
	jmp	.L83
.L108:
	movq	$0, -64(%rbp)
	jmp	.L83
.L104:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7094:
	.size	_ZN4node29ProcessEmitDeprecationWarningEPNS_11EnvironmentEPKcS3_, .-_ZN4node29ProcessEmitDeprecationWarningEPNS_11EnvironmentEPKcS3_
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB8130:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L124
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L113:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L111
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L109
.L112:
	movq	%rbx, %r12
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L112
.L109:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE8130:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.section	.text._ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED2Ev,"axG",@progbits,_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED2Ev
	.type	_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED2Ev, @function
_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED2Ev:
.LFB9057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L127
	movq	%rdi, %rbx
.L131:
	movq	24(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %r13
	cmpq	%rax, %rdi
	je	.L129
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L127
.L130:
	movq	%r13, %r12
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L130
.L127:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9057:
	.size	_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED2Ev, .-_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED2Ev
	.weak	_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED1Ev
	.set	_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED1Ev,_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED2Ev
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_:
.LFB8137:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	8(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	%rax, -56(%rbp)
	testq	%rbx, %rbx
	je	.L143
	movq	8(%rsi), %r15
	movq	(%rsi), %r11
	movabsq	$-2147483649, %r14
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L149:
	movq	16(%rbx), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L145
.L183:
	movq	%rax, %rbx
.L144:
	movq	40(%rbx), %r8
	movq	32(%rbx), %r10
	cmpq	%r8, %r15
	movq	%r8, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L146
	movq	%r10, %rsi
	movq	%r11, %rdi
	movq	%r8, -88(%rbp)
	movq	%rdx, -80(%rbp)
	movq	%r10, -72(%rbp)
	movq	%r11, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r10
	testl	%eax, %eax
	movq	-80(%rbp), %rdx
	movq	-88(%rbp), %r8
	jne	.L147
.L146:
	movq	%r15, %rax
	movl	$2147483648, %ecx
	subq	%r8, %rax
	cmpq	%rcx, %rax
	jge	.L148
	cmpq	%r14, %rax
	jle	.L149
.L147:
	testl	%eax, %eax
	js	.L149
.L148:
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L183
.L145:
	movq	%rbx, %r14
	testb	%sil, %sil
	jne	.L184
.L152:
	testq	%rdx, %rdx
	je	.L153
	movq	%r11, %rsi
	movq	%r10, %rdi
	movq	%r8, -64(%rbp)
	call	memcmp@PLT
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L154
.L153:
	subq	%r15, %r8
	cmpq	$2147483647, %r8
	jg	.L155
	cmpq	$-2147483648, %r8
	jl	.L156
	movl	%r8d, %eax
.L154:
	testl	%eax, %eax
	js	.L156
.L155:
	addq	$56, %rsp
	movq	%rbx, %rax
	xorl	%edx, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	testq	%r14, %r14
	je	.L185
	movl	$1, %r8d
	cmpq	%r14, -56(%rbp)
	jne	.L186
.L157:
	movl	$64, %edi
	movl	%r8d, -64(%rbp)
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movl	-64(%rbp), %r8d
	movq	%rax, %rbx
	leaq	48(%rax), %rax
	movq	%rax, 32(%rbx)
	leaq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L187
	movq	%rdx, 32(%rbx)
	movq	16(%r13), %rdx
	movq	%rdx, 48(%rbx)
.L161:
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	movq	%rbx, %rsi
	movl	%r8d, %edi
	movq	$0, 8(%r13)
	movq	-56(%rbp), %rcx
	movq	%rdx, 40(%rbx)
	movq	%r14, %rdx
	movb	$0, 16(%r13)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, 40(%r12)
	addq	$56, %rsp
	movq	%rbx, %rax
	movl	$1, %edx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	cmpq	%rbx, 24(%r12)
	je	.L188
.L163:
	movq	%rbx, %rdi
	movq	%r11, -64(%rbp)
	movq	%rbx, %r14
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %rdx
	movq	-64(%rbp), %r11
	movq	40(%rax), %r8
	movq	32(%rax), %r10
	movq	%rax, %rbx
	cmpq	%r15, %r8
	cmovbe	%r8, %rdx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L187:
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 48(%rbx)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%rbx, %r14
	movl	$1, %r8d
	cmpq	%r14, -56(%rbp)
	je	.L157
.L186:
	movq	40(%r14), %rbx
	cmpq	%rbx, %r15
	movq	%rbx, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L158
	movq	32(%r14), %rsi
	movq	0(%r13), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L159
.L158:
	movq	%r15, %r9
	xorl	%r8d, %r8d
	subq	%rbx, %r9
	cmpq	$2147483647, %r9
	jg	.L157
	cmpq	$-2147483648, %r9
	jl	.L182
	movl	%r9d, %eax
.L159:
	shrl	$31, %eax
	movl	%eax, %r8d
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L143:
	leaq	8(%rdi), %rax
	cmpq	24(%rdi), %rax
	je	.L169
	movq	8(%rsi), %r15
	movq	(%rsi), %r11
	movq	%rax, %rbx
	jmp	.L163
.L169:
	leaq	8(%r12), %r14
.L182:
	movl	$1, %r8d
	jmp	.L157
.L185:
	xorl	%ebx, %ebx
	jmp	.L155
	.cfi_endproc
.LFE8137:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata.str1.1
.LC3:
	.string	"basic_string::append"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	" is an experimental feature. This feature could change at any time"
	.section	.rodata.str1.1
.LC5:
	.string	"ExperimentalWarning"
	.text
	.p2align 4
	.globl	_ZN4node30ProcessEmitExperimentalWarningEPNS_11EnvironmentEPKc
	.type	_ZN4node30ProcessEmitExperimentalWarningEPNS_11EnvironmentEPKc, @function
_ZN4node30ProcessEmitExperimentalWarningEPNS_11EnvironmentEPKc:
.LFB7093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%r13, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%rsi, %rsi
	je	.L238
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L239
	cmpq	$1, %rax
	jne	.L193
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L194:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	16+_ZN4node21experimental_warningsB5cxx11E(%rip), %rbx
	testq	%rbx, %rbx
	je	.L195
	movq	-96(%rbp), %r14
	movq	-88(%rbp), %r15
	movq	%r12, -128(%rbp)
	leaq	8+_ZN4node21experimental_warningsB5cxx11E(%rip), %r11
	movq	%r13, -136(%rbp)
	movq	%r11, %r12
	movq	%r14, %r13
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L201:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L197
.L196:
	movq	40(%rbx), %r14
	movq	%r15, %rdx
	cmpq	%r15, %r14
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L198
	movq	32(%rbx), %rdi
	movq	%r13, %rsi
	call	memcmp@PLT
	leaq	8+_ZN4node21experimental_warningsB5cxx11E(%rip), %r11
	testl	%eax, %eax
	jne	.L199
.L198:
	movq	%r14, %rax
	movl	$2147483648, %ecx
	subq	%r15, %rax
	cmpq	%rcx, %rax
	jge	.L200
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L201
.L199:
	testl	%eax, %eax
	js	.L201
.L200:
	movq	%rbx, %r12
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L196
.L197:
	movq	%r12, %r9
	movq	%r13, %r14
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	cmpq	%r11, %r9
	je	.L203
	movq	40(%r9), %rbx
	cmpq	%rbx, %r15
	movq	%rbx, %rdx
	cmovbe	%r15, %rdx
	testq	%rdx, %rdx
	je	.L204
	movq	32(%r9), %rsi
	movq	%r14, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L205
.L204:
	movq	%r15, %rcx
	subq	%rbx, %rcx
	cmpq	$2147483647, %rcx
	jg	.L206
	cmpq	$-2147483648, %rcx
	jl	.L203
	movl	%ecx, %eax
.L205:
	testl	%eax, %eax
	js	.L203
.L206:
	cmpq	%r13, %r14
	jne	.L240
.L219:
	xorl	%eax, %eax
	movb	$0, %ah
.L207:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L241
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L242
	movq	%r13, %rdx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L195:
	movq	-96(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	%r13, %r14
	je	.L222
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L243
	cmpq	$1, %rax
	jne	.L210
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L211:
	movq	%rax, -88(%rbp)
	movq	-120(%rbp), %rsi
	leaq	_ZN4node21experimental_warningsB5cxx11E(%rip), %rdi
	movb	$0, (%rdx,%rax)
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE16_M_insert_uniqueIS5_EESt4pairISt17_Rb_tree_iteratorIS5_EbEOT_
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L212
	call	_ZdlPv@PLT
.L212:
	movq	%r12, %rdi
	movq	%r13, -96(%rbp)
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L244
	cmpq	$1, %rax
	jne	.L214
	movzbl	(%r12), %edx
	movb	%dl, -80(%rbp)
	movq	%r13, %rdx
.L215:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	$65, %rax
	jbe	.L245
	movq	-120(%rbp), %rdi
	movl	$66, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-144(%rbp), %rdi
	movq	-96(%rbp), %rsi
	xorl	%ecx, %ecx
	leaq	.LC5(%rip), %rdx
	call	_ZN4node25ProcessEmitWarningGenericEPNS_11EnvironmentEPKcS3_S3_
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L207
	movl	%eax, -120(%rbp)
	call	_ZdlPv@PLT
	movl	-120(%rbp), %eax
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L239:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L192:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L243:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L209:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L214:
	testq	%r14, %r14
	jne	.L246
	movq	%r13, %rdx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L210:
	testq	%rax, %rax
	jne	.L247
	movq	%r13, %rdx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L244:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L213:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L215
.L241:
	call	__stack_chk_fail@PLT
.L245:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L242:
	movq	%r13, %rdi
	jmp	.L192
.L247:
	movq	%r13, %rdi
	jmp	.L209
.L246:
	movq	%r13, %rdi
	jmp	.L213
	.cfi_endproc
.LFE7093:
	.size	_ZN4node30ProcessEmitExperimentalWarningEPNS_11EnvironmentEPKc, .-_ZN4node30ProcessEmitExperimentalWarningEPNS_11EnvironmentEPKc
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE, @function
_GLOBAL__sub_I__ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE:
.LFB9077:
	.cfi_startproc
	endbr64
	leaq	8+_ZN4node21experimental_warningsB5cxx11E(%rip), %rax
	leaq	__dso_handle(%rip), %rdx
	movl	$0, 8+_ZN4node21experimental_warningsB5cxx11E(%rip)
	leaq	-8(%rax), %rsi
	leaq	_ZNSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS5_ESaIS5_EED1Ev(%rip), %rdi
	movq	$0, 16+_ZN4node21experimental_warningsB5cxx11E(%rip)
	movq	%rax, 24+_ZN4node21experimental_warningsB5cxx11E(%rip)
	movq	%rax, 32+_ZN4node21experimental_warningsB5cxx11E(%rip)
	movq	$0, 40+_ZN4node21experimental_warningsB5cxx11E(%rip)
	jmp	__cxa_atexit@PLT
	.cfi_endproc
.LFE9077:
	.size	_GLOBAL__sub_I__ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE, .-_GLOBAL__sub_I__ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE
	.globl	_ZN4node21experimental_warningsB5cxx11E
	.bss
	.align 32
	.type	_ZN4node21experimental_warningsB5cxx11E, @object
	.size	_ZN4node21experimental_warningsB5cxx11E, 48
_ZN4node21experimental_warningsB5cxx11E:
	.zero	48
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
