	.file	"node_credentials.cc"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE, @function
_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE:
.LFB7485:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1136, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L2
	movq	%r12, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
.L1:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L18
	addq	$9328, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	leaq	-9280(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	call	__errno_location@PLT
	movq	-9264(%rbp), %rdi
	leaq	-9320(%rbp), %r8
	leaq	-8224(%rbp), %rdx
	movl	$0, (%rax)
	movl	$8192, %ecx
	leaq	-9312(%rbp), %rsi
	movq	$0, -9320(%rbp)
	call	getgrnam_r@PLT
	movl	%eax, %r8d
	movl	$-1, %eax
	testl	%r8d, %r8d
	jne	.L4
	movq	-9320(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L4
	movl	16(%rdx), %eax
.L4:
	movq	-9264(%rbp), %rdi
	leaq	-9256(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L1
	testq	%rdi, %rdi
	je	.L1
	movl	%eax, -9332(%rbp)
	call	free@PLT
	movl	-9332(%rbp), %eax
	jmp	.L1
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7485:
	.size	_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE, .-_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11credentialsL11uid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE, @function
_ZN4node11credentialsL11uid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE:
.LFB7484:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1152, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %r13
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L20
	movq	%r12, %rdi
	call	_ZNK2v86Uint325ValueEv@PLT
.L19:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L35
	addq	$9344, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	leaq	-9280(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	call	__errno_location@PLT
	movq	-9264(%rbp), %rdi
	leaq	-9336(%rbp), %r8
	leaq	-8224(%rbp), %rdx
	movl	$0, (%rax)
	movl	$8192, %ecx
	leaq	-9328(%rbp), %rsi
	movq	$0, -9336(%rbp)
	call	getpwnam_r@PLT
	movl	%eax, %r8d
	movl	$-1, %eax
	testl	%r8d, %r8d
	jne	.L22
	movq	-9336(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L22
	movl	16(%rdx), %eax
.L22:
	movq	-9264(%rbp), %rdi
	leaq	-9256(%rbp), %rdx
	cmpq	%rdx, %rdi
	je	.L19
	testq	%rdi, %rdi
	je	.L19
	movl	%eax, -9348(%rbp)
	call	free@PLT
	movl	-9348(%rbp), %eax
	jmp	.L19
.L35:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7484:
	.size	_ZN4node11credentialsL11uid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE, .-_ZN4node11credentialsL11uid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"initgroups"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1144, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L79
	cmpw	$1040, %cx
	jne	.L37
.L79:
	movq	23(%rdx), %r13
.L39:
	cmpl	$2, 16(%rbx)
	je	.L101
	leaq	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	movl	16(%rbx), %eax
	je	.L102
.L44:
	cmpl	$1, %eax
	jle	.L103
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L46:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	movl	16(%rbx), %eax
	je	.L47
.L48:
	testl	%eax, %eax
	jg	.L104
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L54:
	movq	352(%r13), %rsi
	leaq	-9312(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L55
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L56:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	movl	%eax, %r12d
	testb	%al, %al
	je	.L57
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L58
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L59:
	call	_ZNK2v86Uint325ValueEv@PLT
	movl	%eax, %r15d
	call	__errno_location@PLT
	leaq	-8256(%rbp), %rdx
	movl	$8192, %ecx
	movl	%r15d, %edi
	movl	$0, (%rax)
	movq	%rax, %r14
	leaq	-9360(%rbp), %rsi
	leaq	-9368(%rbp), %r8
	movq	$0, -9368(%rbp)
	call	getpwuid_r@PLT
	testl	%eax, %eax
	je	.L105
.L63:
	movabsq	$4294967296, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
.L65:
	movq	-9296(%rbp), %rdi
	leaq	-9288(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L36
.L100:
	testq	%rdi, %rdi
	je	.L36
	call	free@PLT
.L36:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L106
	addq	$9336, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-9296(%rbp), %r14
.L62:
	testq	%r14, %r14
	je	.L63
	cmpl	$1, 16(%rbx)
	jg	.L66
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L67:
	movq	352(%r13), %rdi
	call	_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	movl	%eax, %esi
	cmpl	$-1, %eax
	je	.L107
	movq	%r14, %rdi
	call	initgroups@PLT
	movl	%eax, %r15d
	testb	%r12b, %r12b
	jne	.L108
.L70:
	testl	%r15d, %r15d
	jne	.L109
	movq	(%rbx), %rax
	movq	-9296(%rbp), %rdi
	movq	$0, 24(%rax)
	leaq	-9288(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L100
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L55:
	movq	8(%rbx), %rdi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L104:
	movq	8(%rbx), %rdx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L66:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rsi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L107:
	testb	%r12b, %r12b
	jne	.L110
.L69:
	movabsq	$8589934592, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L58:
	movq	8(%rbx), %rdi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-9368(%rbp), %rax
	testq	%rax, %rax
	je	.L61
	movq	(%rax), %rdi
	call	strdup@PLT
	movq	%rax, %r14
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$2, (%r14)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L37:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r14, %rdi
	call	free@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%r14, %rdi
	call	free@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L109:
	call	__errno_location@PLT
	movq	352(%r13), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC0(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L102:
	testl	%eax, %eax
	jle	.L111
	movq	8(%rbx), %rdx
.L42:
	movq	(%rdx), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L112
.L43:
	leaq	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L47:
	cmpl	$1, %eax
	jle	.L113
	movq	8(%rbx), %rcx
	leaq	-8(%rcx), %rdx
.L50:
	movq	(%rdx), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L114
.L51:
	leaq	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L112:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L44
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L48
	jmp	.L51
.L106:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7496:
	.size	_ZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L123
	cmpw	$1040, %cx
	jne	.L116
.L123:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L125
.L119:
	movq	(%rbx), %rbx
	call	getuid@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L120
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L115:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L119
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	_ZZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L120:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L126
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L115
.L126:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L115
	.cfi_endproc
.LFE7486:
	.size	_ZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L135
	cmpw	$1040, %cx
	jne	.L128
.L135:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L137
.L131:
	movq	(%rbx), %rbx
	call	getgid@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L132
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L127:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	_ZZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L132:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L138
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L127
.L138:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L127
	.cfi_endproc
.LFE7487:
	.size	_ZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7488:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L147
	cmpw	$1040, %cx
	jne	.L140
.L147:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L149
.L143:
	movq	(%rbx), %rbx
	call	geteuid@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L144
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L139:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	_ZZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L144:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L150
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L139
.L150:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L139
	.cfi_endproc
.LFE7488:
	.size	_ZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7489:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L159
	cmpw	$1040, %cx
	jne	.L152
.L159:
	movq	23(%rdx), %rax
	cmpb	$0, 1928(%rax)
	je	.L161
.L155:
	movq	(%rbx), %rbx
	call	getegid@PLT
	movl	%eax, %esi
	testl	%eax, %eax
	js	.L156
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L151:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	cmpb	$0, 1928(%rax)
	jne	.L155
	.p2align 4,,10
	.p2align 3
.L161:
	leaq	_ZZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L162
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L151
.L162:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L151
	.cfi_endproc
.LFE7489:
	.size	_ZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC1:
	.string	"setgid"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L179
	cmpw	$1040, %cx
	jne	.L164
.L179:
	movq	23(%rdx), %r12
	testb	$2, 1932(%r12)
	je	.L181
.L167:
	cmpl	$1, 16(%rbx)
	jne	.L182
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	movl	16(%rbx), %eax
	je	.L183
.L173:
	testl	%eax, %eax
	jle	.L184
	movq	8(%rbx), %rsi
.L175:
	movq	352(%r12), %rdi
	call	_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	movl	%eax, %edi
	cmpl	$-1, %eax
	jne	.L176
	movabsq	$4294967296, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L176:
	call	setgid@PLT
	testl	%eax, %eax
	jne	.L185
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	testb	$2, 1932(%r12)
	jne	.L167
	.p2align 4,,10
	.p2align 3
.L181:
	leaq	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L185:
	call	__errno_location@PLT
	movq	352(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC1(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L182:
	.cfi_restore_state
	leaq	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	testl	%eax, %eax
	jle	.L186
	movq	8(%rbx), %rdx
.L171:
	movq	(%rdx), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L187
.L172:
	leaq	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L187:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L173
	jmp	.L172
	.cfi_endproc
.LFE7490:
	.size	_ZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC2:
	.string	"setegid"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L204
	cmpw	$1040, %cx
	jne	.L189
.L204:
	movq	23(%rdx), %r12
	testb	$2, 1932(%r12)
	je	.L206
.L192:
	cmpl	$1, 16(%rbx)
	jne	.L207
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	movl	16(%rbx), %eax
	je	.L208
.L198:
	testl	%eax, %eax
	jle	.L209
	movq	8(%rbx), %rsi
.L200:
	movq	352(%r12), %rdi
	call	_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	movl	%eax, %edi
	cmpl	$-1, %eax
	jne	.L201
	movabsq	$4294967296, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L201:
	call	setegid@PLT
	testl	%eax, %eax
	jne	.L210
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	testb	$2, 1932(%r12)
	jne	.L192
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L210:
	call	__errno_location@PLT
	movq	352(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC2(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	.cfi_restore_state
	leaq	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L208:
	testl	%eax, %eax
	jle	.L211
	movq	8(%rbx), %rdx
.L196:
	movq	(%rdx), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L212
.L197:
	leaq	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L212:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L198
	jmp	.L197
	.cfi_endproc
.LFE7491:
	.size	_ZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC3:
	.string	"setuid"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L229
	cmpw	$1040, %cx
	jne	.L214
.L229:
	movq	23(%rdx), %r12
	testb	$2, 1932(%r12)
	je	.L231
.L217:
	cmpl	$1, 16(%rbx)
	jne	.L232
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	movl	16(%rbx), %eax
	je	.L233
.L223:
	testl	%eax, %eax
	jle	.L234
	movq	8(%rbx), %rsi
.L225:
	movq	352(%r12), %rdi
	call	_ZN4node11credentialsL11uid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	movl	%eax, %edi
	cmpl	$-1, %eax
	jne	.L226
	movabsq	$4294967296, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L226:
	call	setuid@PLT
	testl	%eax, %eax
	jne	.L235
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	testb	$2, 1932(%r12)
	jne	.L217
	.p2align 4,,10
	.p2align 3
.L231:
	leaq	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L235:
	call	__errno_location@PLT
	movq	352(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC3(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	leaq	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L233:
	testl	%eax, %eax
	jle	.L236
	movq	8(%rbx), %rdx
.L221:
	movq	(%rdx), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L237
.L222:
	leaq	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L236:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L237:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L223
	jmp	.L222
	.cfi_endproc
.LFE7492:
	.size	_ZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC4:
	.string	"seteuid"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L254
	cmpw	$1040, %cx
	jne	.L239
.L254:
	movq	23(%rdx), %r12
	testb	$2, 1932(%r12)
	je	.L256
.L242:
	cmpl	$1, 16(%rbx)
	jne	.L257
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	movl	16(%rbx), %eax
	je	.L258
.L248:
	testl	%eax, %eax
	jle	.L259
	movq	8(%rbx), %rsi
.L250:
	movq	352(%r12), %rdi
	call	_ZN4node11credentialsL11uid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	movl	%eax, %edi
	cmpl	$-1, %eax
	jne	.L251
	movabsq	$4294967296, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L259:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L251:
	call	seteuid@PLT
	testl	%eax, %eax
	jne	.L260
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	testb	$2, 1932(%r12)
	jne	.L242
	.p2align 4,,10
	.p2align 3
.L256:
	leaq	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L260:
	call	__errno_location@PLT
	movq	352(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC4(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	movq	%rax, %rsi
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L257:
	.cfi_restore_state
	leaq	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L258:
	testl	%eax, %eax
	jle	.L261
	movq	8(%rbx), %rdx
.L246:
	movq	(%rdx), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L262
.L247:
	leaq	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L261:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L262:
	movq	-1(%rdx), %rdx
	cmpw	$63, 11(%rdx)
	jbe	.L248
	jmp	.L247
	.cfi_endproc
.LFE7493:
	.size	_ZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC5:
	.string	"setgroups"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7495:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$328, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L293
	cmpw	$1040, %cx
	jne	.L264
.L293:
	movq	23(%rdx), %rbx
.L266:
	cmpl	$1, 16(%r12)
	je	.L311
	leaq	_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L311:
	movq	8(%r12), %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L312
	movl	16(%r12), %eax
	testl	%eax, %eax
	jle	.L313
	movq	8(%r12), %r13
.L269:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movdqa	.LC6(%rip), %xmm0
	leaq	-312(%rbp), %rcx
	movl	$0, -312(%rbp)
	movq	%rcx, -352(%rbp)
	movl	%eax, %r14d
	movq	%rcx, -320(%rbp)
	movaps	%xmm0, -336(%rbp)
	cmpl	$64, %eax
	ja	.L314
	movq	%r14, -336(%rbp)
	testq	%r14, %r14
	je	.L292
.L290:
	xorl	%r15d, %r15d
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L273:
	movq	352(%rbx), %rdi
	call	_ZN4node11credentialsL11gid_by_nameEPN2v87IsolateENS1_5LocalINS1_5ValueEEE
	cmpl	$-1, %eax
	je	.L315
	cmpq	-336(%rbp), %r15
	jnb	.L316
	movq	-320(%rbp), %rsi
	movl	%eax, (%rsi,%r15,4)
	addq	$1, %r15
	cmpq	%r14, %r15
	jnb	.L272
.L279:
	movq	3280(%rbx), %rsi
	movl	%r15d, %edx
	movq	%r13, %rdi
	movl	%r15d, -340(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L273
	movq	%rax, -360(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-360(%rbp), %rsi
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L313:
	movq	(%r12), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L316:
	leaq	_ZZN4node16MaybeStackBufferIjLm64EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L314:
	leaq	0(,%r14,4), %r15
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L285
	movq	%rax, -320(%rbp)
	movq	%r14, -328(%rbp)
.L271:
	movq	%r14, -336(%rbp)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L264:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L266
.L292:
	movq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L272:
	movq	%r14, %rdi
	call	setgroups@PLT
	cmpl	$-1, %eax
	je	.L317
	movq	(%r12), %rax
	movq	-320(%rbp), %rdi
	movq	$0, 24(%rax)
	testq	%rdi, %rdi
	je	.L263
	cmpq	-352(%rbp), %rdi
	je	.L263
.L310:
	call	free@PLT
.L263:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L318
	addq	$328, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	movl	-340(%rbp), %esi
	movq	(%r12), %rbx
	addl	$1, %esi
	js	.L275
	salq	$32, %rsi
	movq	%rsi, 24(%rbx)
.L276:
	movq	-320(%rbp), %rdi
	cmpq	-352(%rbp), %rdi
	je	.L263
	testq	%rdi, %rdi
	jne	.L310
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L312:
	leaq	_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L285:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L319
	movq	-336(%rbp), %rax
	movq	%rdi, -320(%rbp)
	movq	%r14, -328(%rbp)
	testq	%rax, %rax
	je	.L271
	movq	-352(%rbp), %rsi
	leaq	0(,%rax,4), %rdx
	call	memcpy@PLT
	jmp	.L271
.L319:
	leaq	_ZZN4node7ReallocIjEEPT_S2_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L317:
	call	__errno_location@PLT
	movq	352(%rbx), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC5(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L276
.L275:
	movq	8(%rbx), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L320
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L276
.L320:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L276
.L318:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7495:
	.size	_ZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC7:
	.string	"safeGetenv"
.LC8:
	.string	"implementsPosixCredentials"
.LC9:
	.string	"getuid"
.LC10:
	.string	"geteuid"
.LC11:
	.string	"getgid"
.LC12:
	.string	"getegid"
.LC13:
	.string	"getgroups"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB14:
	.text
.LHOTB14:
	.p2align 4
	.type	_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L322
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L322
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L322
	movq	271(%rax), %rbx
	movq	352(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, -64(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L366
.L323:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L367
.L324:
	movq	-64(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%r8, -56(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-56(%rbp), %r8
	testb	%al, %al
	je	.L368
.L325:
	movq	%r8, %rsi
	movq	%r14, %rdi
	leaq	112(%r15), %r14
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	xorl	%edx, %edx
	movl	$26, %ecx
	movq	%r15, %rdi
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L369
.L326:
	movl	$1, %r8d
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L370
.L327:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L371
.L328:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L372
.L329:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L373
.L330:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L374
.L331:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L375
.L332:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L376
.L333:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L377
.L334:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L378
.L335:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L379
.L336:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L380
.L337:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L381
.L338:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L382
.L339:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L383
.L340:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L384
.L341:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L385
.L342:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	testb	$2, 1932(%rbx)
	jne	.L386
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L387
.L344:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L388
.L345:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L389
.L346:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L390
.L347:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L391
.L348:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L392
.L349:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L393
.L350:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L394
.L351:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L395
.L352:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L396
.L353:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L397
.L354:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L398
.L355:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L399
.L356:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L400
.L357:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L401
.L358:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L402
.L359:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L403
.L360:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L404
.L361:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L385:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L366:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L367:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L368:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r8
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L370:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L371:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L372:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L373:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L374:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L375:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L376:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L377:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L378:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L379:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L380:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L381:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L382:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L383:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L384:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L404:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L391:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L392:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L393:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L394:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L395:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L396:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L397:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L398:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L387:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L388:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L389:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L390:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L399:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L400:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L401:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L402:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L403:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L360
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7497:
.L322:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7497:
	.text
	.size	_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE14:
	.text
.LHOTE14:
	.p2align 4
	.globl	_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE
	.type	_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE, @function
_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE:
.LFB7476:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, _ZN4node11per_process15linux_at_secureE(%rip)
	je	.L458
.L407:
	movq	(%r14), %rax
	movq	$0, 8(%r14)
	movb	$0, (%rax)
	xorl	%eax, %eax
.L405:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L459
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L458:
	.cfi_restore_state
	movq	%rdi, %r12
	movq	%rdx, %rbx
	call	getuid@PLT
	movl	%eax, %r13d
	call	geteuid@PLT
	cmpl	%eax, %r13d
	jne	.L407
	call	getgid@PLT
	movl	%eax, %r13d
	call	getegid@PLT
	cmpl	%eax, %r13d
	jne	.L407
	testq	%rbx, %rbx
	je	.L409
	movq	352(%rbx), %rsi
	leaq	-176(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-144(%rbp), %rax
	movq	352(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	1392(%rbx), %r8
	movq	1384(%rbx), %r15
	testq	%r8, %r8
	je	.L410
	leaq	8(%r8), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -240(%rbp)
	je	.L411
	lock addl	$1, (%rax)
.L412:
	movq	(%r15), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$-1, %ecx
	movq	%r8, -224(%rbp)
	movq	16(%rax), %rax
	movq	%rax, -232(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-224(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L460
	movq	%r8, -224(%rbp)
	movq	352(%rbx), %rsi
.L457:
	movq	-232(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-224(%rbp), %r8
	movq	%rax, %r12
	je	.L414
	movq	-240(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
.L415:
	cmpl	$1, %eax
	je	.L461
.L413:
	testq	%r12, %r12
	je	.L419
	movq	352(%rbx), %rsi
	leaq	-192(%rbp), %r15
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-192(%rbp), %r12
	testq	%r12, %r12
	je	.L462
	movslq	-184(%rbp), %r8
	leaq	-80(%rbp), %rbx
	leaq	-96(%rbp), %rdi
	movq	%rbx, -96(%rbp)
	movq	%r8, -200(%rbp)
	cmpq	$15, %r8
	ja	.L463
	cmpq	$1, %r8
	jne	.L423
	movzbl	(%r12), %eax
	movb	%al, -80(%rbp)
	movq	%rbx, %rax
.L424:
	movq	%r8, -88(%rbp)
	movb	$0, (%rax,%r8)
	movq	-96(%rbp), %rdx
	movq	(%r14), %rdi
	cmpq	%rbx, %rdx
	je	.L464
	leaq	16(%r14), %rcx
	movq	-88(%rbp), %rax
	movq	-80(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L465
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	16(%r14), %rcx
	movq	%rdx, (%r14)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r14)
	testq	%rdi, %rdi
	je	.L430
	movq	%rdi, -96(%rbp)
	movq	%rcx, -80(%rbp)
.L428:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L431
	call	_ZdlPv@PLT
.L431:
	movq	%r15, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	-216(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$1, %eax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L410:
	movq	(%r15), %rax
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$-1, %ecx
	movq	16(%rax), %rax
	movq	%rax, -224(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L466
	movq	352(%rbx), %rsi
	movq	-224(%rbp), %rax
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r15, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
.L419:
	movq	-216(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	%r12, %rdi
	call	getenv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L433
	movq	%rax, %rdi
	call	strlen@PLT
	movq	8(%r14), %rdx
	movq	%r12, %rcx
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movl	$1, %eax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L411:
	addl	$1, 8(%r8)
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L414:
	movl	8(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r8)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L433:
	leaq	_ZN4node11per_process13env_var_mutexE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L461:
	movq	(%r8), %rax
	movq	%r8, -224(%rbp)
	movq	%r8, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-224(%rbp), %r8
	je	.L417
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r8)
.L418:
	cmpl	$1, %eax
	jne	.L413
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*24(%rax)
	jmp	.L413
.L423:
	testq	%r8, %r8
	jne	.L467
	movq	%rbx, %rax
	jmp	.L424
.L463:
	leaq	-200(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-224(%rbp), %r8
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-200(%rbp), %rax
	movq	%rax, -80(%rbp)
.L422:
	movq	%r8, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-200(%rbp), %r8
	movq	-96(%rbp), %rax
	jmp	.L424
.L464:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L426
	cmpq	$1, %rdx
	je	.L468
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	(%r14), %rdi
.L426:
	movq	%rdx, 8(%r14)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L428
.L465:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, (%r14)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%r14)
.L430:
	movq	%rbx, -96(%rbp)
	leaq	-80(%rbp), %rbx
	movq	%rbx, %rdi
	jmp	.L428
.L417:
	movl	12(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r8)
	jmp	.L418
.L468:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	(%r14), %rdi
	jmp	.L426
.L459:
	call	__stack_chk_fail@PLT
.L466:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	352(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	-224(%rbp), %rax
	call	*%rax
	movq	%rax, %r12
	jmp	.L413
.L460:
	movq	%r8, -224(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	352(%rbx), %rsi
	xorl	%edx, %edx
	jmp	.L457
.L467:
	movq	%rbx, %rdi
	jmp	.L422
	.cfi_endproc
.LFE7476:
	.size	_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE, .-_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE
	.p2align 4
	.type	_ZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$1088, %rsp
	movl	16(%rdi), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testl	%edx, %edx
	jg	.L470
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L503
.L472:
	leaq	_ZZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L470:
	movq	8(%rdi), %rax
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L472
.L503:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L472
	movq	(%rbx), %rdi
	movq	32(%rdi), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L491
	cmpw	$1040, %si
	jne	.L474
.L491:
	movq	23(%rcx), %r12
.L476:
	movq	352(%r12), %r14
	testl	%edx, %edx
	jle	.L504
	movq	8(%rbx), %rdx
.L478:
	leaq	-1088(%rbp), %rdi
	movq	%r14, %rsi
	leaq	-1104(%rbp), %r13
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1072(%rbp), %rdi
	movq	%r12, %rdx
	leaq	-1120(%rbp), %rsi
	movq	%r13, -1120(%rbp)
	movq	$0, -1112(%rbp)
	movb	$0, -1104(%rbp)
	call	_ZN4node11credentials10SafeGetenvEPKcPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPNS_11EnvironmentE
	testb	%al, %al
	jne	.L479
	movq	-1120(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L480
	call	_ZdlPv@PLT
.L480:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L469
	testq	%rdi, %rdi
	je	.L469
.L502:
	call	free@PLT
.L469:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	addq	$1088, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L479:
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-1112(%rbp), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L506
	movq	-1120(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L485
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L489:
	movq	-1120(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L487
	call	_ZdlPv@PLT
.L487:
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L469
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L502
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L474:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movl	16(%rbx), %edx
	movq	%rax, %r12
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L506:
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
.L485:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L489
.L505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7477:
	.size	_ZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_Z21_register_credentialsv
	.type	_Z21_register_credentialsv, @function
_Z21_register_credentialsv:
.LFB7498:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7498:
	.size	_Z21_register_credentialsv, .-_Z21_register_credentialsv
	.section	.text._ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE,"axG",@progbits,_ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE,comdat
	.p2align 4
	.weak	_ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE
	.type	_ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE, @function
_ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE:
.LFB8210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1128, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L557
.L509:
	leaq	-1136(%rbp), %r13
	movq	%r14, %rsi
	leaq	-1080(%rbp), %r12
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r15), %rcx
	movq	8(%r15), %r9
	movq	%r12, %rax
	movdqa	.LC15(%rip), %xmm0
	movq	%r12, -1088(%rbp)
	leaq	-56(%rbp), %rdx
	subq	%rcx, %r9
	sarq	$2, %r9
	movaps	%xmm0, -1104(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L510:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L510
	movq	$0, -1080(%rbp)
	cmpq	$128, %r9
	jbe	.L517
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r9,8), %rdi
	andq	%r9, %rax
	cmpq	%rax, %r9
	jne	.L558
	movq	%r9, -1160(%rbp)
	movq	%r8, -1152(%rbp)
	movq	%rcx, -1144(%rbp)
	testq	%rdi, %rdi
	je	.L513
	movq	%rdi, -1168(%rbp)
	call	malloc@PLT
	movq	-1168(%rbp), %rdi
	movq	-1144(%rbp), %rcx
	testq	%rax, %rax
	movq	-1152(%rbp), %r8
	movq	-1160(%rbp), %r9
	je	.L559
	movq	%rax, -1088(%rbp)
	movq	%r9, -1096(%rbp)
.L517:
	movq	%r8, -1104(%rbp)
	testq	%r8, %r8
	je	.L518
	testq	%r14, %r14
	je	.L531
	xorl	%ebx, %ebx
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L560:
	movq	%rax, (%rcx)
	movq	(%r15), %rcx
	addq	$1, %rbx
	movq	8(%r15), %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	cmpq	%rbx, %rax
	jbe	.L528
.L522:
	movl	(%rcx,%rbx,4), %esi
	movq	%r14, %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-1104(%rbp), %r8
	cmpq	%rbx, %r8
	jbe	.L520
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%rbx,8), %rcx
	testq	%rax, %rax
	jne	.L560
.L521:
	movq	$0, (%rcx)
	xorl	%r14d, %r14d
.L527:
	testq	%rsi, %rsi
	je	.L526
	cmpq	%r12, %rsi
	je	.L526
	movq	%rsi, %rdi
	call	free@PLT
.L526:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L561
	addq	$1128, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L520:
	.cfi_restore_state
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L557:
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r14
	jmp	.L509
.L518:
	movq	-1088(%rbp), %rsi
.L528:
	movq	%r14, %rdi
	movq	%r8, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-1088(%rbp), %rsi
	movq	%rax, %r14
	jmp	.L527
.L531:
	xorl	%edx, %edx
	jmp	.L519
.L553:
	movq	%rax, (%rcx)
	movq	(%r15), %rcx
	addq	$1, %rdx
	movq	8(%r15), %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	cmpq	%rax, %rdx
	jnb	.L528
.L519:
	leaq	(%rcx,%rdx,4), %rcx
	movq	%rbx, %rdi
	movq	%rdx, -1152(%rbp)
	movq	%rcx, -1144(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-1144(%rbp), %rcx
	movq	%rax, %rdi
	movl	(%rcx), %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-1104(%rbp), %r8
	movq	-1152(%rbp), %rdx
	cmpq	%rdx, %r8
	jbe	.L520
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%rdx,8), %rcx
	testq	%rax, %rax
	jne	.L553
	jmp	.L521
.L513:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L559:
	movq	%r9, -1152(%rbp)
	movq	%rdi, -1144(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-1144(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L513
	movq	(%r15), %rcx
	movq	8(%r15), %r8
	movq	%rdi, -1088(%rbp)
	movq	-1152(%rbp), %r9
	movq	-1104(%rbp), %rax
	subq	%rcx, %r8
	movq	%r9, -1096(%rbp)
	sarq	$2, %r8
	testq	%rax, %rax
	je	.L516
	leaq	0(,%rax,8), %rdx
	movq	%r12, %rsi
	movq	%r8, -1160(%rbp)
	movq	%r9, -1152(%rbp)
	movq	%rcx, -1144(%rbp)
	call	memcpy@PLT
	movq	-1160(%rbp), %r8
	movq	-1152(%rbp), %r9
	movq	-1144(%rbp), %rcx
.L516:
	movq	%r9, -1104(%rbp)
	cmpq	%r8, %r9
	jnb	.L517
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L558:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L561:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8210:
	.size	_ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE, .-_ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE
	.section	.rodata._ZNSt6vectorIjSaIjEE17_M_default_appendEm.str1.1,"aMS",@progbits,1
.LC16:
	.string	"vector::_M_default_append"
	.section	.text._ZNSt6vectorIjSaIjEE17_M_default_appendEm,"axG",@progbits,_ZNSt6vectorIjSaIjEE17_M_default_appendEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIjSaIjEE17_M_default_appendEm
	.type	_ZNSt6vectorIjSaIjEE17_M_default_appendEm, @function
_ZNSt6vectorIjSaIjEE17_M_default_appendEm:
.LFB8610:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L581
	movabsq	$2305843009213693951, %rdx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rcx
	movq	16(%r12), %rax
	movq	%rcx, %rsi
	subq	(%rdi), %rsi
	subq	%rcx, %rax
	movq	%rdx, %rdi
	movq	%rsi, %r13
	sarq	$2, %rax
	sarq	$2, %r13
	subq	%r13, %rdi
	cmpq	%rbx, %rax
	jb	.L564
	salq	$2, %rbx
	movq	%rcx, %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	memset@PLT
	movq	%rax, %rcx
	addq	%rbx, %rcx
	movq	%rcx, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L581:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	cmpq	%rbx, %rdi
	jb	.L584
	cmpq	%rbx, %r13
	movq	%rbx, %r14
	movq	%rsi, -56(%rbp)
	cmovnb	%r13, %r14
	addq	%r13, %r14
	cmpq	%rdx, %r14
	cmova	%rdx, %r14
	salq	$2, %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	leaq	0(,%rbx,4), %rdx
	movq	%rax, %r15
	leaq	(%rax,%rsi), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	(%r12), %r8
	movq	8(%r12), %rdx
	subq	%r8, %rdx
	testq	%rdx, %rdx
	jg	.L585
	testq	%r8, %r8
	jne	.L568
.L569:
	addq	%r13, %rbx
	addq	%r15, %r14
	movq	%r15, (%r12)
	leaq	(%r15,%rbx,4), %rax
	movq	%r14, 16(%r12)
	movq	%rax, 8(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L585:
	.cfi_restore_state
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
.L568:
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L569
.L584:
	leaq	.LC16(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8610:
	.size	_ZNSt6vectorIjSaIjEE17_M_default_appendEm, .-_ZNSt6vectorIjSaIjEE17_M_default_appendEm
	.section	.rodata._ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_.str1.1,"aMS",@progbits,1
.LC17:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,"axG",@progbits,_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.type	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, @function
_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_:
.LFB8622:
	.cfi_startproc
	endbr64
	movabsq	$2305843009213693951, %rcx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	(%rdi), %r9
	movq	%r13, %rax
	subq	%r9, %rax
	sarq	$2, %rax
	cmpq	%rcx, %rax
	je	.L600
	movq	%rdx, %r15
	movq	%rsi, %rdx
	movq	%rdi, %r12
	movq	%rsi, %r8
	subq	%r9, %rdx
	testq	%rax, %rax
	je	.L596
	movabsq	$9223372036854775804, %r14
	leaq	(%rax,%rax), %rsi
	cmpq	%rsi, %rax
	jbe	.L601
.L588:
	movq	%r14, %rdi
	movq	%r8, -72(%rbp)
	movq	%rdx, -64(%rbp)
	movq	%r9, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %r8
	movq	%rax, %rbx
	addq	%rax, %r14
.L595:
	movl	(%r15), %eax
	subq	%r8, %r13
	leaq	4(%rbx,%rdx), %r15
	movl	%eax, (%rbx,%rdx)
	leaq	(%r15,%r13), %rax
	movq	%rax, -56(%rbp)
	testq	%rdx, %rdx
	jg	.L602
	testq	%r13, %r13
	jg	.L591
	testq	%r9, %r9
	jne	.L594
.L592:
	movq	%rbx, %xmm0
	movq	%r14, 16(%r12)
	movhps	-56(%rbp), %xmm0
	movups	%xmm0, (%r12)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L602:
	.cfi_restore_state
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	movq	%r9, -64(%rbp)
	call	memmove@PLT
	testq	%r13, %r13
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r8
	jg	.L591
.L594:
	movq	%r9, %rdi
	call	_ZdlPv@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L601:
	testq	%rsi, %rsi
	jne	.L589
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%r13, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r9
	testq	%r9, %r9
	je	.L592
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L596:
	movl	$4, %r14d
	jmp	.L588
.L600:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L589:
	cmpq	%rcx, %rsi
	cmovbe	%rsi, %rcx
	movq	%rcx, %r14
	salq	$2, %r14
	jmp	.L588
	.cfi_endproc
.LFE8622:
	.size	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_, .-_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"cannot create std::vector larger than max_size()"
	.text
	.p2align 4
	.type	_ZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L633
	cmpw	$1040, %cx
	jne	.L604
.L633:
	movq	23(%rdx), %r12
	cmpb	$0, 1928(%r12)
	je	.L651
.L607:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	getgroups@PLT
	movl	%eax, %r13d
	cmpl	$-1, %eax
	je	.L652
	movabsq	$2305843009213693951, %rdx
	cltq
	cmpq	%rdx, %rax
	ja	.L653
	pxor	%xmm0, %xmm0
	movq	$0, -64(%rbp)
	leaq	0(,%rax,4), %r14
	movaps	%xmm0, -80(%rbp)
	testq	%rax, %rax
	je	.L632
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%r14), %r15
	movq	%rax, %rdi
	movq	%rax, -80(%rbp)
	movq	%r15, -64(%rbp)
	call	memset@PLT
	movq	%rax, %r8
.L611:
	movq	%r8, %rsi
	movl	%r13d, %edi
	movq	%r15, -72(%rbp)
	call	getgroups@PLT
	cmpl	$-1, %eax
	je	.L654
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %rsi
	cltq
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	cmpq	%rdx, %rax
	ja	.L655
	leaq	-80(%rbp), %r13
	jb	.L656
.L615:
	call	getegid@PLT
	movq	-72(%rbp), %rsi
	movq	-80(%rbp), %rdx
	movl	%eax, -84(%rbp)
	movq	%rsi, %rcx
	subq	%rdx, %rcx
	movq	%rcx, %rdi
	sarq	$4, %rcx
	sarq	$2, %rdi
	testq	%rcx, %rcx
	jle	.L616
	salq	$4, %rcx
	addq	%rdx, %rcx
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L661:
	cmpl	4(%rdx), %eax
	je	.L657
	cmpl	8(%rdx), %eax
	je	.L658
	cmpl	12(%rdx), %eax
	je	.L659
	addq	$16, %rdx
	cmpq	%rdx, %rcx
	je	.L660
.L621:
	cmpl	(%rdx), %eax
	jne	.L661
.L617:
	cmpq	%rdx, %rsi
	je	.L626
.L627:
	movq	3280(%r12), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN4node9ToV8ValueIjEEN2v810MaybeLocalINS1_5ValueEEENS1_5LocalINS1_7ContextEEERKSt6vectorIT_SaIS9_EEPNS1_7IsolateE
	testq	%rax, %rax
	je	.L629
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L629:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L603
.L650:
	call	_ZdlPv@PLT
.L603:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L662
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L632:
	.cfi_restore_state
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L656:
	leaq	(%rsi,%rax,4), %rax
	cmpq	%rax, %rcx
	je	.L615
	movq	%rax, -72(%rbp)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L655:
	subq	%rdx, %rax
	leaq	-80(%rbp), %r13
	movq	%rax, %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_default_appendEm
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L604:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	cmpb	$0, 1928(%r12)
	jne	.L607
	.p2align 4,,10
	.p2align 3
.L651:
	leaq	_ZZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L652:
	call	__errno_location@PLT
	movq	352(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC13(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L654:
	call	__errno_location@PLT
	movq	352(%r12), %r12
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	(%rax), %esi
	leaq	.LC13(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L650
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L660:
	movq	%rsi, %rdi
	subq	%rdx, %rdi
	sarq	$2, %rdi
.L616:
	cmpq	$2, %rdi
	je	.L622
	cmpq	$3, %rdi
	je	.L623
	cmpq	$1, %rdi
	je	.L624
.L626:
	cmpq	-64(%rbp), %rsi
	je	.L628
	movl	%eax, (%rsi)
	addq	$4, %rsi
	movq	%rsi, -72(%rbp)
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L657:
	addq	$4, %rdx
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L658:
	addq	$8, %rdx
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L659:
	addq	$12, %rdx
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L623:
	cmpl	(%rdx), %eax
	je	.L617
	addq	$4, %rdx
.L622:
	cmpl	(%rdx), %eax
	je	.L617
	addq	$4, %rdx
.L624:
	cmpl	(%rdx), %eax
	jne	.L626
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L628:
	leaq	-84(%rbp), %rdx
	movq	%r13, %rdi
	call	_ZNSt6vectorIjSaIjEE17_M_realloc_insertIJRKjEEEvN9__gnu_cxx17__normal_iteratorIPjS1_EEDpOT_
	jmp	.L627
.L662:
	call	__stack_chk_fail@PLT
.L653:
	leaq	.LC18(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7494:
	.size	_ZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.weak	_ZZN4node7ReallocIjEEPT_S2_mE4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/util-inl.h:374"
.LC20:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"T* node::Realloc(T*, size_t) [with T = unsigned int; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIjEEPT_S2_mE4args,"awG",@progbits,_ZZN4node7ReallocIjEEPT_S2_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIjEEPT_S2_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIjEEPT_S2_mE4args, 24
_ZZN4node7ReallocIjEEPT_S2_mE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args
	.section	.rodata.str1.1
.LC22:
	.string	"../src/util.h:352"
.LC23:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC25:
	.string	"../src/util.h:391"
.LC26:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC28
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC29:
	.string	"../src/util-inl.h:325"
.LC30:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.weak	_ZZN4node16MaybeStackBufferIjLm64EEixEmE4args
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = unsigned int; long unsigned int kStackStorageSize = 64; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIjLm64EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIjLm64EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIjLm64EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIjLm64EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIjLm64EEixEmE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC32
	.section	.rodata.str1.1
.LC33:
	.string	"../src/node_credentials.cc"
.LC34:
	.string	"credentials"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC33
	.quad	0
	.quad	_ZN4node11credentialsL10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC34
	.quad	0
	.quad	0
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"../src/node_credentials.cc:337"
	.align 8
.LC36:
	.string	"args[1]->IsUint32() || args[1]->IsString()"
	.align 8
.LC37:
	.string	"void node::credentials::InitGroups(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"../src/node_credentials.cc:336"
	.align 8
.LC39:
	.string	"args[0]->IsUint32() || args[0]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC37
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"../src/node_credentials.cc:335"
	.section	.rodata.str1.1
.LC41:
	.string	"(args.Length()) == (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL10InitGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC40
	.quad	.LC41
	.quad	.LC37
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"../src/node_credentials.cc:306"
	.section	.rodata.str1.1
.LC43:
	.string	"args[0]->IsArray()"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"void node::credentials::SetGroups(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"../src/node_credentials.cc:305"
	.section	.rodata.str1.1
.LC46:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL9SetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC44
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"../src/node_credentials.cc:282"
	.align 8
.LC48:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC49:
	.string	"void node::credentials::GetGroups(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL9GetGroupsERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"../src/node_credentials.cc:266"
	.align 8
.LC51:
	.string	"void node::credentials::SetEUid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC50
	.quad	.LC39
	.quad	.LC51
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"../src/node_credentials.cc:265"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC52
	.quad	.LC46
	.quad	.LC51
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"../src/node_credentials.cc:263"
	.section	.rodata.str1.1
.LC54:
	.string	"env->owns_process_state()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL7SetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC51
	.section	.rodata.str1.8
	.align 8
.LC55:
	.string	"../src/node_credentials.cc:247"
	.align 8
.LC56:
	.string	"void node::credentials::SetUid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC55
	.quad	.LC39
	.quad	.LC56
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"../src/node_credentials.cc:246"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC57
	.quad	.LC46
	.quad	.LC56
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"../src/node_credentials.cc:244"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL6SetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC58
	.quad	.LC54
	.quad	.LC56
	.section	.rodata.str1.8
	.align 8
.LC59:
	.string	"../src/node_credentials.cc:228"
	.align 8
.LC60:
	.string	"void node::credentials::SetEGid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC59
	.quad	.LC39
	.quad	.LC60
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../src/node_credentials.cc:227"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC61
	.quad	.LC46
	.quad	.LC60
	.section	.rodata.str1.8
	.align 8
.LC62:
	.string	"../src/node_credentials.cc:225"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL7SetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC62
	.quad	.LC54
	.quad	.LC60
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"../src/node_credentials.cc:209"
	.align 8
.LC64:
	.string	"void node::credentials::SetGid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC63
	.quad	.LC39
	.quad	.LC64
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"../src/node_credentials.cc:208"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC65
	.quad	.LC46
	.quad	.LC64
	.section	.rodata.str1.8
	.align 8
.LC66:
	.string	"../src/node_credentials.cc:206"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL6SetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC66
	.quad	.LC54
	.quad	.LC64
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"../src/node_credentials.cc:199"
	.align 8
.LC68:
	.string	"void node::credentials::GetEGid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL7GetEGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC67
	.quad	.LC48
	.quad	.LC68
	.section	.rodata.str1.8
	.align 8
.LC69:
	.string	"../src/node_credentials.cc:192"
	.align 8
.LC70:
	.string	"void node::credentials::GetEUid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL7GetEUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC69
	.quad	.LC48
	.quad	.LC70
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"../src/node_credentials.cc:185"
	.align 8
.LC72:
	.string	"void node::credentials::GetGid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL6GetGidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC71
	.quad	.LC48
	.quad	.LC72
	.section	.rodata.str1.8
	.align 8
.LC73:
	.string	"../src/node_credentials.cc:178"
	.align 8
.LC74:
	.string	"void node::credentials::GetUid(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL6GetUidERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC73
	.quad	.LC48
	.quad	.LC74
	.section	.rodata.str1.1
.LC75:
	.string	"../src/node_credentials.cc:73"
.LC76:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"void node::credentials::SafeGetenv(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11credentialsL10SafeGetenvERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.globl	_ZN4node11per_process15linux_at_secureE
	.bss
	.type	_ZN4node11per_process15linux_at_secureE, @object
	.size	_ZN4node11per_process15linux_at_secureE, 1
_ZN4node11per_process15linux_at_secureE:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.quad	0
	.quad	64
	.align 16
.LC15:
	.quad	0
	.quad	128
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
