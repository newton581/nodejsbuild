	.file	"node_postmortem_metadata.cc"
	.text
	.p2align 4
	.globl	_ZN4node15GenDebugSymbolsEv
	.type	_ZN4node15GenDebugSymbolsEv, @function
_ZN4node15GenDebugSymbolsEv:
.LFB7131:
	.cfi_startproc
	endbr64
	movl	$32, nodedbg_const_ContextEmbedderIndex__kEnvironment__int(%rip)
	movl	$1, %eax
	movq	$8, nodedbg_offset_ExternalString__data__uintptr_t(%rip)
	movq	$64, nodedbg_offset_ReqWrap__req_wrap_queue___ListNode_ReqWrapQueue(%rip)
	movq	$8, nodedbg_offset_BaseObject__persistent_handle___v8_Persistent_v8_Object(%rip)
	movq	$2096, nodedbg_offset_Environment__handle_wrap_queue___Environment_HandleWrapQueue(%rip)
	movq	$2112, nodedbg_offset_Environment__req_wrap_queue___Environment_ReqWrapQueue(%rip)
	movq	$56, nodedbg_offset_HandleWrap__handle_wrap_queue___ListNode_HandleWrap(%rip)
	movq	$0, nodedbg_offset_Environment_HandleWrapQueue__head___ListNode_HandleWrap(%rip)
	movq	$0, nodedbg_offset_ListNode_HandleWrap__prev___uintptr_t(%rip)
	movq	$8, nodedbg_offset_ListNode_HandleWrap__next___uintptr_t(%rip)
	movq	$0, nodedbg_offset_Environment_ReqWrapQueue__head___ListNode_ReqWrapQueue(%rip)
	movq	$0, nodedbg_offset_ListNode_ReqWrap__prev___uintptr_t(%rip)
	movq	$8, nodedbg_offset_ListNode_ReqWrap__next___uintptr_t(%rip)
	ret
	.cfi_endproc
.LFE7131:
	.size	_ZN4node15GenDebugSymbolsEv, .-_ZN4node15GenDebugSymbolsEv
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I_nodedbg_const_ContextEmbedderIndex__kEnvironment__int, @function
_GLOBAL__sub_I_nodedbg_const_ContextEmbedderIndex__kEnvironment__int:
.LFB8981:
	.cfi_startproc
	endbr64
	movl	$32, nodedbg_const_ContextEmbedderIndex__kEnvironment__int(%rip)
	movq	$8, nodedbg_offset_ExternalString__data__uintptr_t(%rip)
	movq	$64, nodedbg_offset_ReqWrap__req_wrap_queue___ListNode_ReqWrapQueue(%rip)
	movq	$8, nodedbg_offset_BaseObject__persistent_handle___v8_Persistent_v8_Object(%rip)
	movq	$2096, nodedbg_offset_Environment__handle_wrap_queue___Environment_HandleWrapQueue(%rip)
	movq	$2112, nodedbg_offset_Environment__req_wrap_queue___Environment_ReqWrapQueue(%rip)
	movq	$56, nodedbg_offset_HandleWrap__handle_wrap_queue___ListNode_HandleWrap(%rip)
	movq	$0, nodedbg_offset_Environment_HandleWrapQueue__head___ListNode_HandleWrap(%rip)
	movq	$0, nodedbg_offset_ListNode_HandleWrap__prev___uintptr_t(%rip)
	movq	$8, nodedbg_offset_ListNode_HandleWrap__next___uintptr_t(%rip)
	movq	$0, nodedbg_offset_Environment_ReqWrapQueue__head___ListNode_ReqWrapQueue(%rip)
	movq	$0, nodedbg_offset_ListNode_ReqWrap__prev___uintptr_t(%rip)
	movq	$8, nodedbg_offset_ListNode_ReqWrap__next___uintptr_t(%rip)
	ret
	.cfi_endproc
.LFE8981:
	.size	_GLOBAL__sub_I_nodedbg_const_ContextEmbedderIndex__kEnvironment__int, .-_GLOBAL__sub_I_nodedbg_const_ContextEmbedderIndex__kEnvironment__int
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I_nodedbg_const_ContextEmbedderIndex__kEnvironment__int
	.globl	nodedbg_offset_ListNode_ReqWrap__next___uintptr_t
	.bss
	.align 8
	.type	nodedbg_offset_ListNode_ReqWrap__next___uintptr_t, @object
	.size	nodedbg_offset_ListNode_ReqWrap__next___uintptr_t, 8
nodedbg_offset_ListNode_ReqWrap__next___uintptr_t:
	.zero	8
	.globl	nodedbg_offset_ListNode_ReqWrap__prev___uintptr_t
	.align 8
	.type	nodedbg_offset_ListNode_ReqWrap__prev___uintptr_t, @object
	.size	nodedbg_offset_ListNode_ReqWrap__prev___uintptr_t, 8
nodedbg_offset_ListNode_ReqWrap__prev___uintptr_t:
	.zero	8
	.globl	nodedbg_offset_Environment_ReqWrapQueue__head___ListNode_ReqWrapQueue
	.align 8
	.type	nodedbg_offset_Environment_ReqWrapQueue__head___ListNode_ReqWrapQueue, @object
	.size	nodedbg_offset_Environment_ReqWrapQueue__head___ListNode_ReqWrapQueue, 8
nodedbg_offset_Environment_ReqWrapQueue__head___ListNode_ReqWrapQueue:
	.zero	8
	.globl	nodedbg_offset_ListNode_HandleWrap__next___uintptr_t
	.align 8
	.type	nodedbg_offset_ListNode_HandleWrap__next___uintptr_t, @object
	.size	nodedbg_offset_ListNode_HandleWrap__next___uintptr_t, 8
nodedbg_offset_ListNode_HandleWrap__next___uintptr_t:
	.zero	8
	.globl	nodedbg_offset_ListNode_HandleWrap__prev___uintptr_t
	.align 8
	.type	nodedbg_offset_ListNode_HandleWrap__prev___uintptr_t, @object
	.size	nodedbg_offset_ListNode_HandleWrap__prev___uintptr_t, 8
nodedbg_offset_ListNode_HandleWrap__prev___uintptr_t:
	.zero	8
	.globl	nodedbg_offset_Environment_HandleWrapQueue__head___ListNode_HandleWrap
	.align 8
	.type	nodedbg_offset_Environment_HandleWrapQueue__head___ListNode_HandleWrap, @object
	.size	nodedbg_offset_Environment_HandleWrapQueue__head___ListNode_HandleWrap, 8
nodedbg_offset_Environment_HandleWrapQueue__head___ListNode_HandleWrap:
	.zero	8
	.globl	nodedbg_offset_HandleWrap__handle_wrap_queue___ListNode_HandleWrap
	.align 8
	.type	nodedbg_offset_HandleWrap__handle_wrap_queue___ListNode_HandleWrap, @object
	.size	nodedbg_offset_HandleWrap__handle_wrap_queue___ListNode_HandleWrap, 8
nodedbg_offset_HandleWrap__handle_wrap_queue___ListNode_HandleWrap:
	.zero	8
	.globl	nodedbg_offset_Environment__req_wrap_queue___Environment_ReqWrapQueue
	.align 8
	.type	nodedbg_offset_Environment__req_wrap_queue___Environment_ReqWrapQueue, @object
	.size	nodedbg_offset_Environment__req_wrap_queue___Environment_ReqWrapQueue, 8
nodedbg_offset_Environment__req_wrap_queue___Environment_ReqWrapQueue:
	.zero	8
	.globl	nodedbg_offset_Environment__handle_wrap_queue___Environment_HandleWrapQueue
	.align 8
	.type	nodedbg_offset_Environment__handle_wrap_queue___Environment_HandleWrapQueue, @object
	.size	nodedbg_offset_Environment__handle_wrap_queue___Environment_HandleWrapQueue, 8
nodedbg_offset_Environment__handle_wrap_queue___Environment_HandleWrapQueue:
	.zero	8
	.globl	nodedbg_offset_BaseObject__persistent_handle___v8_Persistent_v8_Object
	.align 8
	.type	nodedbg_offset_BaseObject__persistent_handle___v8_Persistent_v8_Object, @object
	.size	nodedbg_offset_BaseObject__persistent_handle___v8_Persistent_v8_Object, 8
nodedbg_offset_BaseObject__persistent_handle___v8_Persistent_v8_Object:
	.zero	8
	.globl	nodedbg_offset_ReqWrap__req_wrap_queue___ListNode_ReqWrapQueue
	.align 8
	.type	nodedbg_offset_ReqWrap__req_wrap_queue___ListNode_ReqWrapQueue, @object
	.size	nodedbg_offset_ReqWrap__req_wrap_queue___ListNode_ReqWrapQueue, 8
nodedbg_offset_ReqWrap__req_wrap_queue___ListNode_ReqWrapQueue:
	.zero	8
	.globl	nodedbg_offset_ExternalString__data__uintptr_t
	.align 8
	.type	nodedbg_offset_ExternalString__data__uintptr_t, @object
	.size	nodedbg_offset_ExternalString__data__uintptr_t, 8
nodedbg_offset_ExternalString__data__uintptr_t:
	.zero	8
	.globl	nodedbg_const_ContextEmbedderIndex__kEnvironment__int
	.align 4
	.type	nodedbg_const_ContextEmbedderIndex__kEnvironment__int, @object
	.size	nodedbg_const_ContextEmbedderIndex__kEnvironment__int, 4
nodedbg_const_ContextEmbedderIndex__kEnvironment__int:
	.zero	4
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
