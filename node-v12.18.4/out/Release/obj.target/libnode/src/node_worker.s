	.file	"node_worker.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4160:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4160:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4687:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4687:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4689:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4689:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZNK4node6worker6Worker8SelfSizeEv,"axG",@progbits,_ZNK4node6worker6Worker8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker6Worker8SelfSizeEv
	.type	_ZNK4node6worker6Worker8SelfSizeEv, @function
_ZNK4node6worker6Worker8SelfSizeEv:
.LFB6052:
	.cfi_startproc
	endbr64
	movl	$360, %eax
	ret
	.cfi_endproc
.LFE6052:
	.size	_ZNK4node6worker6Worker8SelfSizeEv, .-_ZNK4node6worker6Worker8SelfSizeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB6094:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE6094:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB6095:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE6095:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB6096:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE6096:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB6098:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L10
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE6098:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7383:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7383:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_,"axG",@progbits,_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_,comdat
	.p2align 4
	.weak	_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_
	.type	_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_, @function
_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_:
.LFB8000:
	.cfi_startproc
	endbr64
	movb	$1, (%rdi)
	ret
	.cfi_endproc
.LFE8000:
	.size	_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_, .-_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_
	.section	.text._ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8080:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE8080:
	.size	_ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv,"axG",@progbits,_ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv
	.type	_ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv, @function
_ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv:
.LFB8082:
	.cfi_startproc
	endbr64
	movl	$56, %eax
	ret
	.cfi_endproc
.LFE8082:
	.size	_ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv, .-_ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv
	.text
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEEUlPKcE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEEUlPKcE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation:
.LFB9780:
	.cfi_startproc
	endbr64
	cmpl	$2, %edx
	je	.L16
	cmpl	$3, %edx
	je	.L17
	cmpl	$1, %edx
	je	.L21
.L17:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rsi, (%rdi)
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9780:
	.size	_ZNSt14_Function_base13_Base_managerIZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEEUlPKcE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEEUlPKcE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11880:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11880:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11884:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11884:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11930:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L24
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L24:
	ret
	.cfi_endproc
.LFE11930:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11932:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11932:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11933:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L27:
	ret
	.cfi_endproc
.LFE11933:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11935:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11935:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11939:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11939:
	.size	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11942:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L31:
	ret
	.cfi_endproc
.LFE11942:
	.size	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11944:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11944:
	.size	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB11900:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L34
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	ret
	.cfi_endproc
.LFE11900:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11943:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE11943:
	.size	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11941:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11941:
	.size	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11882:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11882:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11931:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11931:
	.size	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11886:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11886:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11934:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11934:
	.size	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB11902:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L43
	call	_ZdlPv@PLT
.L43:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11902:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZN4node6worker23WorkerHeapSnapshotTakerD2Ev,"axG",@progbits,_ZN4node6worker23WorkerHeapSnapshotTakerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node6worker23WorkerHeapSnapshotTakerD2Ev
	.type	_ZN4node6worker23WorkerHeapSnapshotTakerD2Ev, @function
_ZN4node6worker23WorkerHeapSnapshotTakerD2Ev:
.LFB11888:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node6worker23WorkerHeapSnapshotTakerE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE11888:
	.size	_ZN4node6worker23WorkerHeapSnapshotTakerD2Ev, .-_ZN4node6worker23WorkerHeapSnapshotTakerD2Ev
	.weak	_ZN4node6worker23WorkerHeapSnapshotTakerD1Ev
	.set	_ZN4node6worker23WorkerHeapSnapshotTakerD1Ev,_ZN4node6worker23WorkerHeapSnapshotTakerD2Ev
	.section	.text._ZN4node6worker23WorkerHeapSnapshotTakerD0Ev,"axG",@progbits,_ZN4node6worker23WorkerHeapSnapshotTakerD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node6worker23WorkerHeapSnapshotTakerD0Ev
	.type	_ZN4node6worker23WorkerHeapSnapshotTakerD0Ev, @function
_ZN4node6worker23WorkerHeapSnapshotTakerD0Ev:
.LFB11890:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker23WorkerHeapSnapshotTakerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11890:
	.size	_ZN4node6worker23WorkerHeapSnapshotTakerD0Ev, .-_ZN4node6worker23WorkerHeapSnapshotTakerD0Ev
	.text
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L54
	cmpw	$1040, %cx
	jne	.L49
.L54:
	movq	23(%rdx), %rax
.L51:
	movq	2928(%rax), %r12
	testq	%r12, %r12
	je	.L48
	movq	%r12, %rdi
	call	_ZN2v86Object15CreationContextEv@PLT
	movq	%rax, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r8
	movq	(%rbx), %rax
	cmpq	%r8, 8(%rax)
	jne	.L59
	movq	(%r12), %rdx
	movq	%rdx, 24(%rax)
.L48:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	_ZZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8096:
	.size	_ZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker3RefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker6Worker3RefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker6Worker3RefERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8071:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L73
	movq	8(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L68
	cmpw	$1040, %cx
	jne	.L62
.L68:
	movq	23(%rdx), %rax
.L64:
	testq	%rax, %rax
	je	.L60
	cmpb	$0, 345(%rax)
	jne	.L60
	cmpb	$0, 216(%rax)
	je	.L74
.L60:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	.cfi_restore_state
	movq	16(%rax), %rdi
	movb	$1, 345(%rax)
	movq	1128(%rdi), %rax
	addq	$1, %rax
	movq	%rax, 1128(%rdi)
	js	.L75
	addq	$1000, %rdi
	testq	%rax, %rax
	jne	.L67
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L67:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_ref@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	leaq	_ZZN4node11Environment8add_refsElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8071:
	.size	_ZN4node6worker6Worker3RefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker6Worker3RefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker5UnrefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker6Worker5UnrefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker6Worker5UnrefERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8072:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	8(%rdi), %rbx
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L89
	movq	8(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L84
	cmpw	$1040, %cx
	jne	.L78
.L84:
	movq	23(%rdx), %rax
.L80:
	testq	%rax, %rax
	je	.L76
	cmpb	$0, 345(%rax)
	je	.L76
	cmpb	$0, 216(%rax)
	je	.L90
.L76:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	.cfi_restore_state
	movq	16(%rax), %rdi
	movb	$0, 345(%rax)
	movq	1128(%rdi), %rax
	subq	$1, %rax
	movq	%rax, 1128(%rdi)
	js	.L91
	addq	$1000, %rdi
	testq	%rax, %rax
	jne	.L83
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_unref@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_ref@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	leaq	_ZZN4node11Environment8add_refsElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8072:
	.size	_ZN4node6worker6Worker5UnrefERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker6Worker5UnrefERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker17GetResourceLimitsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker6Worker17GetResourceLimitsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker6Worker17GetResourceLimitsERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8073:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L105
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L100
	cmpw	$1040, %cx
	jne	.L94
.L100:
	movq	23(%rdx), %r12
.L96:
	testq	%r12, %r12
	je	.L92
	movq	(%rbx), %rbx
	movl	$24, %esi
	movq	8(%rbx), %rdi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	leaq	-96(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-96(%rbp), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movdqu	288(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	304(%r12), %rdx
	movq	%rdx, 16(%rax)
	movl	$3, %edx
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	testq	%rax, %rax
	je	.L106
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L92:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L107
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L92
.L107:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8073:
	.size	_ZN4node6worker6Worker17GetResourceLimitsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker6Worker17GetResourceLimitsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev:
.LFB6051:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1802661719, 16(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$29285, %edx
	movw	%dx, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	ret
	.cfi_endproc
.LFE6051:
	.size	_ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev:
.LFB8081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rsi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	$23, -32(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-32(%rbp), %rdx
	movdqa	.LC1(%rip), %xmm0
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
	movl	$25963, %edx
	movl	$1632924783, 16(%rax)
	movw	%dx, 20(%rax)
	movb	$114, 22(%rax)
	movups	%xmm0, (%rax)
	movq	-32(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L112
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L112:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8081:
	.size	_ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev, .-_ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB12148:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L114
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L115
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L115:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L116
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L117
.L116:
	testq	%rbx, %rbx
	je	.L117
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L117:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L118
	call	__stack_chk_fail@PLT
.L114:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L118:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12148:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"0123456789abcdef"
	.section	.text.unlikely
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB12261:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC3(%rip), %rax
.L125:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L125
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L126
	call	__stack_chk_fail@PLT
.L126:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12261:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB12283:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC3(%rip), %rax
.L130:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L130
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L131
	call	__stack_chk_fail@PLT
.L131:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12283:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7381:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L135
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L135:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L144
.L136:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L136
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7381:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.type	_ZNSt17_Function_handlerIFNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcEZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINSC_5ValueEEEEUlS7_E_E9_M_invokeERKSt9_Any_dataOS7_, @function
_ZNSt17_Function_handlerIFNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcEZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINSC_5ValueEEEEUlS7_E_E9_M_invokeERKSt9_Any_dataOS7_:
.LFB9778:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	leaq	-80(%rbp), %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	leaq	-96(%rbp), %rbx
	subq	$96, %rsp
	movq	(%rdx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	(%rax), %rsi
	movq	(%rsi), %rax
	call	*24(%rax)
	leaq	16(%r12), %rdi
	cmpb	$0, -80(%rbp)
	movq	%rbx, -112(%rbp)
	movq	$0, -104(%rbp)
	movb	$0, -96(%rbp)
	movq	%rdi, (%r12)
	je	.L146
	movq	-72(%rbp), %r14
	movq	-64(%rbp), %r13
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L147
	testq	%r14, %r14
	je	.L165
.L147:
	movq	%r13, -120(%rbp)
	cmpq	$15, %r13
	jbe	.L148
	movq	%r12, %rdi
	leaq	-120(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-120(%rbp), %rax
	movq	%rax, 16(%r12)
.L149:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L146:
	xorl	%r13d, %r13d
.L151:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L152
	call	_ZdlPv@PLT
.L152:
	movq	-72(%rbp), %rdi
	leaq	-56(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L145
	call	_ZdlPv@PLT
.L145:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	.cfi_restore_state
	cmpq	$1, %r13
	jne	.L150
	movzbl	(%r14), %eax
	movq	(%r12), %rdi
	movb	%al, 16(%r12)
	jmp	.L151
.L165:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	testq	%r13, %r13
	je	.L151
	jmp	.L149
.L166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9778:
	.size	_ZNSt17_Function_handlerIFNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcEZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINSC_5ValueEEEEUlS7_E_E9_M_invokeERKSt9_Any_dataOS7_, .-_ZNSt17_Function_handlerIFNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcEZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINSC_5ValueEEEEUlS7_E_E9_M_invokeERKSt9_Any_dataOS7_
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB12292:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L168
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L168:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rdx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rdx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L170
.L184:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L184
.L170:
	andl	$4095, %edx
	subq	%rdx, %rsp
	testq	%rdx, %rdx
	jne	.L185
.L171:
	leaq	15(%rsp), %r14
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movl	$1, %edx
	andq	$-16, %r14
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	leaq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	$-1, %rcx
	movl	$32, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L186
	cmpq	$1, %r13
	jne	.L174
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L175:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L187
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L175
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L173:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L185:
	orq	$0, -8(%rsp,%rdx)
	jmp	.L171
.L187:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12292:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.1
.LC4:
	.string	"startThread"
.LC5:
	.string	"stopThread"
.LC6:
	.string	"ref"
.LC7:
	.string	"unref"
.LC8:
	.string	"getResourceLimits"
.LC9:
	.string	"takeHeapSnapshot"
.LC10:
	.string	"Worker"
.LC11:
	.string	"WorkerHeapSnapshotTaker"
.LC12:
	.string	"getEnvMessagePort"
.LC13:
	.string	"isMainThread"
.LC14:
	.string	"ownsProcessState"
.LC15:
	.string	"resourceLimits"
.LC16:
	.string	"kMaxYoungGenerationSizeMb"
.LC18:
	.string	"kMaxOldGenerationSizeMb"
.LC20:
	.string	"kCodeRangeSizeMb"
.LC22:
	.string	"kTotalResourceLimitCount"
	.section	.text.unlikely
.LCOLDB24:
	.text
.LHOTB24:
	.p2align 4
	.type	_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB8097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L189
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L189
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L189
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L240
.L190:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L241
.L191:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6worker6Worker3RefERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L242
.L192:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6worker6Worker5UnrefERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L243
.L193:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6worker6Worker17GetResourceLimitsERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L244
.L194:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L245
.L195:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L246
.L196:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L247
.L197:
	movq	3280(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L248
.L198:
	subq	$8, %rsp
	movq	352(%rbx), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	pushq	$0
	movl	$1, %r9d
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movl	$23, %ecx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L249
.L199:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movq	3272(%rbx), %rdi
	movq	352(%rbx), %r14
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L200
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3272(%rbx)
.L200:
	testq	%r12, %r12
	je	.L201
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3272(%rbx)
.L201:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L250
.L202:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L251
.L203:
	movq	%r12, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L252
.L204:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	1936(%rbx), %rax
	testq	%rax, %rax
	js	.L205
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L206:
	movq	352(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1720(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L253
.L207:
	movq	352(%rbx), %rdi
	testb	$1, 1932(%rbx)
	movl	$12, %ecx
	leaq	.LC13(%rip), %rsi
	leaq	112(%rdi), %rax
	leaq	120(%rdi), %r12
	cmovne	%rax, %r12
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L254
.L210:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L255
.L211:
	movq	352(%rbx), %rdi
	testb	$2, 1932(%rbx)
	movl	$16, %ecx
	leaq	.LC14(%rip), %rsi
	leaq	112(%rdi), %rax
	leaq	120(%rdi), %r12
	cmovne	%rax, %r12
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L256
.L214:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L257
.L215:
	testb	$1, 1932(%rbx)
	je	.L258
.L216:
	movq	%r13, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L259
.L219:
	movq	%r12, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L260
.L220:
	movq	%r13, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L261
.L221:
	movsd	.LC19(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L262
.L222:
	movq	%r13, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L263
.L223:
	movsd	.LC21(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L264
.L224:
	movq	%r13, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L265
.L225:
	movsd	.LC23(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L266
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L258:
	movq	352(%rbx), %rdi
	movl	$24, %esi
	movq	2368(%rbx), %r14
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	leaq	-112(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-112(%rbp), %rax
	movq	%r12, %rdi
	xorl	%esi, %esi
	movdqu	288(%r14), %xmm1
	movups	%xmm1, (%rax)
	movq	304(%r14), %rdx
	movq	%rdx, 16(%rax)
	movl	$3, %edx
	call	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$14, %ecx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L267
.L217:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	jne	.L216
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L261:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L262:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L263:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L264:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L265:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L266:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L239
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L254:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L255:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L259:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L260:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L257:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L252:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L240:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L241:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L242:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L243:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L244:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L245:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L246:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rcx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L248:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rsi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L250:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L251:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	jmp	.L217
.L239:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB8097:
.L189:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE8097:
	.text
	.size	_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE24:
	.text
.LHOTE24:
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_E4CallES2_:
.LFB11945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	352(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	24(%r12), %rax
	movq	16(%rax), %r13
	movsd	40(%rax), %xmm0
	movq	1216(%r13), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	je	.L269
	comisd	.LC17(%rip), %xmm0
	jb	.L296
.L269:
	movq	1256(%r13), %rax
	leaq	-104(%rbp), %rdi
	leaq	32(%r12), %rdx
	movq	%rbx, %rsi
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -120(%rbp)
	call	_ZN4node4heap24CreateHeapSnapshotStreamEPNS_11EnvironmentEOSt10unique_ptrIKN2v812HeapSnapshotENS_15FunctionDeleterIS6_XadL_ZNS0_18DeleteHeapSnapshotEPS6_EEEEE@PLT
	movq	-104(%rbp), %rcx
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L270
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L297
.L270:
	movq	24(%r12), %r12
	movq	%rax, -64(%rbp)
	movq	360(%rbx), %rax
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	1152(%rax), %rbx
	testq	%rdi, %rdi
	je	.L271
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L298
.L271:
	movq	3280(%rdx), %rsi
	movq	%rbx, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L281
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L299
.L281:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L275
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L300
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L301
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L275
	cmpb	$0, 9(%rdx)
	je	.L279
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L275:
	movq	1256(%r13), %rax
	movsd	-120(%rbp), %xmm2
	movq	%r14, %rdi
	movsd	%xmm2, 24(%rax)
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L302
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L297:
	movq	16(%rcx), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L299:
	leaq	-64(%rbp), %rcx
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L300:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L296:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L279:
	cmpb	$0, 8(%rdx)
	je	.L275
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L275
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L301:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L302:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11945:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_E4CallES2_
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED0Ev:
.LFB11326:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L305
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L316
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L317
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L318
.L305:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L311
	movq	(%rdi), %rax
	call	*8(%rax)
.L311:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L318:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L309
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L316:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L309:
	cmpb	$0, 8(%rdx)
	je	.L305
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L305
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L317:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11326:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED0Ev:
.LFB10945:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L320
	call	_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE@PLT
.L320:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L322
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L336
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L337
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L338
.L322:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L328
	movq	(%rdi), %rax
	call	*8(%rax)
.L328:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L338:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L326
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L336:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L326:
	cmpb	$0, 8(%rdx)
	je	.L322
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L322
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L337:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10945:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED2Ev:
.LFB10943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L340
	call	_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE@PLT
.L340:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L342
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L353
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L354
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L342
	cmpb	$0, 9(%rdx)
	je	.L346
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L342:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L339
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L346:
	cmpb	$0, 8(%rdx)
	je	.L342
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L342
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10943:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED2Ev:
.LFB11324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L357
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L365
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L366
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L357
	cmpb	$0, 9(%rdx)
	je	.L361
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L357:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L355
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L355:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L361:
	cmpb	$0, 8(%rdx)
	je	.L357
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L357
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L366:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11324:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED2Ev
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L367
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L371:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L369
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L371
.L367:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L371
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB6063:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L374
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L403
	cmpq	$1, %rax
	jne	.L377
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L378:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L373:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L404
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L378
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L403:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L376:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L374:
	cmpb	$37, 1(%rax)
	jne	.L405
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L406
	cmpq	$1, %r13
	jne	.L383
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L384:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L386
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L407
.L386:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L388:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L408
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L390:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L391
	call	_ZdlPv@PLT
.L391:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L373
	call	_ZdlPv@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L383:
	testq	%r13, %r13
	jne	.L409
	movq	%rbx, %rax
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L406:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L382:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L408:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L388
.L405:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L404:
	call	__stack_chk_fail@PLT
.L409:
	movq	%rbx, %rdi
	jmp	.L382
	.cfi_endproc
.LFE6063:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node6worker6Worker10is_stoppedEv
	.type	_ZNK4node6worker6Worker10is_stoppedEv, @function
_ZNK4node6worker6Worker10is_stoppedEv:
.LFB7984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	176(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movq	352(%rbx), %rax
	testq	%rax, %rax
	je	.L411
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%r13b
.L412:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movzbl	344(%rbx), %r13d
	jmp	.L412
	.cfi_endproc
.LFE7984:
	.size	_ZNK4node6worker6Worker10is_stoppedEv, .-_ZNK4node6worker6Worker10is_stoppedEv
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker22array_buffer_allocatorEv
	.type	_ZN4node6worker6Worker22array_buffer_allocatorEv, @function
_ZN4node6worker6Worker22array_buffer_allocatorEv:
.LFB7985:
	.cfi_startproc
	endbr64
	movq	128(%rsi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	136(%rsi), %rdx
	movq	%rdx, 8(%rdi)
	testq	%rdx, %rdx
	je	.L414
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L416
	lock addl	$1, 8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	addl	$1, 8(%rdx)
.L414:
	ret
	.cfi_endproc
.LFE7985:
	.size	_ZN4node6worker6Worker22array_buffer_allocatorEv, .-_ZN4node6worker6Worker22array_buffer_allocatorEv
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker25UpdateResourceConstraintsEPN2v819ResourceConstraintsE
	.type	_ZN4node6worker6Worker25UpdateResourceConstraintsEPN2v819ResourceConstraintsE, @function
_ZN4node6worker6Worker25UpdateResourceConstraintsEPN2v819ResourceConstraintsE:
.LFB7992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm2, %xmm2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movsd	288(%rdi), %xmm0
	movq	%rdi, %rbx
	movq	280(%rdi), %rax
	comisd	%xmm2, %xmm0
	movq	%rax, 16(%rsi)
	jbe	.L445
	mulsd	.LC25(%rip), %xmm0
	movsd	.LC26(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L423
	cvttsd2siq	%xmm0, %rsi
.L424:
	movq	%r12, %rdi
	call	_ZN2v819ResourceConstraints38set_max_young_generation_size_in_bytesEm@PLT
.L425:
	movsd	296(%rbx), %xmm0
	pxor	%xmm3, %xmm3
	comisd	%xmm3, %xmm0
	jbe	.L446
	mulsd	.LC25(%rip), %xmm0
	movsd	.LC26(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L430
	cvttsd2siq	%xmm0, %rax
.L431:
	shrq	$20, %rax
	movq	%rax, 8(%r12)
.L432:
	movsd	304(%rbx), %xmm0
	pxor	%xmm4, %xmm4
	comisd	%xmm4, %xmm0
	jbe	.L447
	mulsd	.LC25(%rip), %xmm0
	movsd	.LC26(%rip), %xmm1
	comisd	%xmm1, %xmm0
	jnb	.L437
	cvttsd2siq	%xmm0, %rax
.L438:
	shrq	$20, %rax
	popq	%rbx
	movq	%rax, 24(%r12)
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	.cfi_restore_state
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rsi
	btcq	$63, %rsi
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rsi, %rdi
	call	_ZNK2v819ResourceConstraints34max_young_generation_size_in_bytesEv@PLT
	testq	%rax, %rax
	js	.L426
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L427:
	mulsd	.LC27(%rip), %xmm0
	movsd	%xmm0, 288(%rbx)
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L447:
	movq	24(%r12), %rax
	salq	$20, %rax
	js	.L440
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L441:
	mulsd	.LC27(%rip), %xmm0
	movsd	%xmm0, 304(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L446:
	.cfi_restore_state
	movq	8(%r12), %rax
	salq	$20, %rax
	js	.L433
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L434:
	mulsd	.LC27(%rip), %xmm0
	movsd	%xmm0, 296(%rbx)
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L437:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L438
	.p2align 4,,10
	.p2align 3
.L430:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	btcq	$63, %rax
	jmp	.L431
	.p2align 4,,10
	.p2align 3
.L426:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L433:
	shrq	%rax
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L440:
	shrq	%rax
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L441
	.cfi_endproc
.LFE7992:
	.size	_ZN4node6worker6Worker25UpdateResourceConstraintsEPN2v819ResourceConstraintsE, .-_ZN4node6worker6Worker25UpdateResourceConstraintsEPN2v819ResourceConstraintsE
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker20CreateEnvMessagePortEPNS_11EnvironmentE
	.type	_ZN4node6worker6Worker20CreateEnvMessagePortEPNS_11EnvironmentE, @function
_ZN4node6worker6Worker20CreateEnvMessagePortEPNS_11EnvironmentE:
.LFB8035:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	176(%rbx), %r13
	subq	$56, %rsp
	movq	144(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	312(%rbx), %rax
	movq	%r12, %rdi
	leaq	-88(%rbp), %rdx
	movq	$0, 312(%rbx)
	movq	3280(%r12), %rsi
	movq	%rax, -88(%rbp)
	call	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE@PLT
	movq	-88(%rbp), %rdi
	movq	%rax, %rbx
	testq	%rdi, %rdi
	je	.L450
	movq	(%rdi), %rax
	call	*8(%rax)
.L450:
	testq	%rbx, %rbx
	je	.L451
	movq	8(%rbx), %r15
	testq	%r15, %r15
	je	.L452
	movzbl	11(%r15), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L479
	movq	2928(%r12), %rdi
	movq	352(%r12), %rbx
	testq	%rdi, %rdi
	je	.L456
.L457:
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2928(%r12)
.L454:
	testq	%r15, %r15
	je	.L451
.L456:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2928(%r12)
.L451:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L480
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L452:
	.cfi_restore_state
	movq	2928(%r12), %rdi
	movq	352(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L457
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L479:
	movq	16(%rbx), %rax
	movq	(%r15), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	2928(%r12), %rdi
	movq	352(%r12), %rbx
	movq	%rax, %r15
	testq	%rdi, %rdi
	jne	.L457
	jmp	.L454
.L480:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8035:
	.size	_ZN4node6worker6Worker20CreateEnvMessagePortEPNS_11EnvironmentE, .-_ZN4node6worker6Worker20CreateEnvMessagePortEPNS_11EnvironmentE
	.align 2
	.p2align 4
	.globl	_ZNK4node6worker6Worker17GetResourceLimitsEPN2v87IsolateE
	.type	_ZNK4node6worker6Worker17GetResourceLimitsEPN2v87IsolateE, @function
_ZNK4node6worker6Worker17GetResourceLimitsEPN2v87IsolateE:
.LFB8074:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movl	$24, %esi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEm@PLT
	leaq	-80(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-80(%rbp), %rax
	movdqu	288(%rbx), %xmm0
	movups	%xmm0, (%rax)
	movq	304(%rbx), %rdx
	movq	%rdx, 16(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L485
	addq	$64, %rsp
	movq	%r12, %rdi
	movl	$3, %edx
	xorl	%esi, %esi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v812Float64Array3NewENS_5LocalINS_11ArrayBufferEEEmm@PLT
.L485:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8074:
	.size	_ZNK4node6worker6Worker17GetResourceLimitsEPN2v87IsolateE, .-_ZNK4node6worker6Worker17GetResourceLimitsEPN2v87IsolateE
	.p2align 4
	.globl	_Z16_register_workerv
	.type	_Z16_register_workerv, @function
_Z16_register_workerv:
.LFB8098:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE8098:
	.size	_Z16_register_workerv, .-_Z16_register_workerv
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB8607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L494
	movq	16(%rdi), %r10
.L488:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L489
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L495
	movq	16(%r9), %r10
.L490:
	cmpq	%r10, %rax
	jbe	.L497
.L489:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L491:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L498
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L493:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L497:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L498:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L494:
	movl	$15, %r10d
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L495:
	movl	$15, %r10d
	jmp	.L490
	.cfi_endproc
.LFE8607:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_:
.LFB9144:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	%rdi, %rsi
	je	.L500
	movq	8(%rsi), %rbx
	movq	(%rsi), %r12
	movq	(%rdi), %r14
	movq	16(%rdi), %rdx
	movq	%rbx, %rax
	subq	%r12, %rax
	subq	%r14, %rdx
	movq	%rax, %rcx
	sarq	$5, %rdx
	movq	%rax, -80(%rbp)
	sarq	$5, %rcx
	movq	%rcx, %r15
	cmpq	%rcx, %rdx
	jb	.L570
	movq	8(%rdi), %r8
	movq	%r8, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %r15
	ja	.L519
	cmpq	$0, -80(%rbp)
	movq	%r14, %rbx
	jle	.L569
	.p2align 4,,10
	.p2align 3
.L520:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	addq	$32, %rbx
	subq	$1, %r15
	movq	-72(%rbp), %r8
	jne	.L520
	movq	-80(%rbp), %rax
	movl	$32, %edx
	testq	%rax, %rax
	cmovg	%rax, %rdx
	addq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L569:
	cmpq	%r14, %r8
	je	.L567
.L527:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L524
	movq	%r8, -72(%rbp)
	addq	$32, %r14
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	cmpq	%r14, %r8
	jne	.L527
.L567:
	movq	-80(%rbp), %r14
	addq	0(%r13), %r14
.L518:
	movq	%r14, 8(%r13)
.L500:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L571
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L570:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L538
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rcx
	ja	.L572
	movq	-80(%rbp), %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
.L502:
	movq	%r12, %r15
	movq	-96(%rbp), %r14
	leaq	-64(%rbp), %r12
	cmpq	%rbx, %r15
	jne	.L512
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L508:
	cmpq	$1, %r8
	jne	.L510
	movzbl	(%r10), %eax
	movb	%al, 16(%r14)
.L511:
	addq	$32, %r15
	movq	%r8, 8(%r14)
	addq	$32, %r14
	movb	$0, (%rdi,%r8)
	cmpq	%r15, %rbx
	je	.L513
.L512:
	leaq	16(%r14), %rdi
	movq	8(%r15), %r8
	movq	%rdi, (%r14)
	movq	(%r15), %r10
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L507
	testq	%r10, %r10
	je	.L531
.L507:
	movq	%r8, -64(%rbp)
	cmpq	$15, %r8
	jbe	.L508
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, -88(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r14)
.L509:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	(%r14), %rdi
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L524:
	addq	$32, %r14
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L519:
	testq	%rcx, %rcx
	jle	.L528
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-72(%rbp), %rdx
	addq	$32, %r14
	subq	$1, %rdx
	jne	.L529
	movq	-88(%rbp), %rax
	movq	8(%r13), %r8
	movq	0(%r13), %r14
	movq	%r8, %rcx
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	subq	%r14, %rcx
.L528:
	leaq	-64(%rbp), %rax
	leaq	(%r12,%rcx), %r15
	addq	-80(%rbp), %r14
	movq	%rax, -88(%rbp)
	cmpq	%rbx, %r15
	jne	.L530
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L574:
	movzbl	(%r14), %eax
	movb	%al, 16(%r8)
.L536:
	addq	$32, %r15
	movq	%r12, 8(%r8)
	addq	$32, %r8
	movb	$0, (%rdi,%r12)
	cmpq	%r15, %rbx
	je	.L567
.L530:
	leaq	16(%r8), %rdi
	movq	8(%r15), %r12
	movq	%rdi, (%r8)
	movq	(%r15), %r14
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L540
	testq	%r14, %r14
	je	.L531
.L540:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L573
	cmpq	$1, %r12
	je	.L574
	testq	%r12, %r12
	je	.L536
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L573:
	movq	-88(%rbp), %rsi
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L534:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r12
	movq	(%r8), %rdi
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L538:
	movq	$0, -96(%rbp)
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L510:
	testq	%r8, %r8
	je	.L511
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L513:
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L505
	.p2align 4,,10
	.p2align 3
.L506:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L514
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L506
.L515:
	movq	0(%r13), %r12
.L505:
	testq	%r12, %r12
	je	.L517
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L517:
	movq	-96(%rbp), %r14
	movq	%r14, 0(%r13)
	addq	-80(%rbp), %r14
	movq	%r14, 16(%r13)
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L514:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L506
	jmp	.L515
.L531:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L571:
	call	__stack_chk_fail@PLT
.L572:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9144:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC28:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB9559:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L617
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L597
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L618
	movabsq	$9223372036854775776, %rcx
.L577:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L595:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L579
	testq	%r10, %r10
	je	.L619
.L579:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L620
	cmpq	$1, %r9
	jne	.L582
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L583:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L599
.L624:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L585:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L615:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L621
.L588:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L585
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L618:
	testq	%rcx, %rcx
	jne	.L578
	xorl	%r13d, %r13d
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L584:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L589
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L593:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L622
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L593
.L591:
	subq	%rbx, %r12
	addq	%r12, %r8
.L589:
	testq	%r14, %r14
	je	.L594
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L594:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L623
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L593
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L597:
	movl	$32, %ecx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L582:
	testq	%r9, %r9
	jne	.L581
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L624
.L599:
	movq	%r13, %r8
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L620:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	%rax, %rdi
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L581:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L583
.L619:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L578:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L577
.L623:
	call	__stack_chk_fail@PLT
.L617:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9559:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC2EPS2_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC5EPS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC2EPS2_
	.type	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC2EPS2_, @function
_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC2EPS2_:
.LFB9862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L642
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L643
.L628:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L632
.L625:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L633
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L629:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L628
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L634
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L630:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L632:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L625
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L633:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L634:
	xorl	%edx, %edx
	jmp	.L630
	.cfi_endproc
.LFE9862:
	.size	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC2EPS2_, .-_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC2EPS2_
	.weak	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC1EPS2_
	.set	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC1EPS2_,_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC2EPS2_
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_E4CallES2_:
.LFB11936:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	352(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate15GetHeapProfilerEv@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v812HeapProfiler16TakeHeapSnapshotEPNS_15ActivityControlEPNS0_18ObjectNameResolverE@PLT
	testq	%rax, %rax
	je	.L667
	movq	24(%r13), %rsi
	leaq	-64(%rbp), %rdi
	movq	32(%r13), %r12
	movq	%rax, %rbx
	call	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC1EPS2_
	movl	$40, %edi
	movq	%rbx, -56(%rbp)
	leaq	2488(%r12), %r13
	call	_Znwm@PLT
	movq	-64(%rbp), %rsi
	movq	%rax, %rbx
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_EE(%rip), %rax
	leaq	24(%rbx), %rdi
	movq	%rax, (%rbx)
	call	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC1EPS2_
	movq	-56(%rbp), %rax
	movq	%r13, %rdi
	movq	$0, -56(%rbp)
	movq	%rax, 32(%rbx)
	call	uv_mutex_lock@PLT
	movq	2544(%r12), %rax
	lock addq	$1, 2528(%r12)
	movq	%rbx, 2544(%r12)
	testq	%rax, %rax
	je	.L646
	movq	16(%rax), %rdi
	movq	%rbx, 16(%rax)
	testq	%rdi, %rdi
	je	.L648
.L666:
	movq	(%rdi), %rax
	call	*8(%rax)
.L648:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	leaq	1000(%r12), %rdi
	call	uv_async_send@PLT
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L650
	call	_ZN4node4heap18DeleteHeapSnapshotEPKN2v812HeapSnapshotE@PLT
.L650:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L644
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L668
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L669
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L644
	cmpb	$0, 9(%rdx)
	je	.L656
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L644:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L670
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movq	2536(%r12), %rdi
	movq	%rbx, 2536(%r12)
	testq	%rdi, %rdi
	jne	.L666
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L667:
	leaq	_ZZZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEEENKUlPNS_11EnvironmentEE_clES9_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L656:
	cmpb	$0, 8(%rdx)
	je	.L644
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L644
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L668:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L670:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE11936:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_E4CallES2_
	.section	.text._ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB10131:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L672
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L682
.L698:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L683:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L696
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L697
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L675:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L677
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L679:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L680:
	testq	%rsi, %rsi
	je	.L677
.L678:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L679
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L685
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L678
	.p2align 4,,10
	.p2align 3
.L677:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L681
	call	_ZdlPv@PLT
.L681:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L698
.L682:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L684
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L684:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L685:
	movq	%rdx, %rdi
	jmp	.L680
	.p2align 4,,10
	.p2align 3
.L696:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L675
.L697:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10131:
	.size	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC29:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB10135:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L708
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L709
.L701:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L709:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L710
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L711
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L706
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L706:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L704:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L710:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L703
	cmpq	%r14, %rsi
	je	.L704
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L703:
	cmpq	%r14, %rsi
	je	.L704
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L704
.L708:
	leaq	.LC29(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L711:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10135:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata.str1.1
.LC30:
	.string	"parent_port"
.LC31:
	.string	"wrapped"
.LC32:
	.string	"wrapper"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node6worker6Worker10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node6worker6Worker10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node6worker6Worker10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	336(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L712
	movq	104(%rsi), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rsi, %rbx
	divq	%rdi
	movq	96(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L714
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L803:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L714
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L714
.L716:
	cmpq	%rsi, %r12
	jne	.L803
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L804
	cmpq	72(%rbx), %rax
	je	.L805
.L717:
	movq	-8(%rax), %rsi
.L749:
	leaq	.LC30(%rip), %rcx
	call	*%r8
.L712:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L806
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L805:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L714:
	movq	(%rbx), %rsi
	leaq	-160(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L718
	movq	(%rax), %r8
	movq	8(%r8), %r10
	movq	%r8, %rcx
	movq	%r10, %rsi
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L807:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L725
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L725
.L721:
	cmpq	%rsi, %r12
	jne	.L807
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L723
	cmpq	72(%rbx), %rax
	je	.L808
.L722:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L723
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	leaq	.LC30(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L723:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L809:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L718
	movq	8(%r8), %r10
	xorl	%edx, %edx
	movq	%r10, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L718
.L725:
	cmpq	%r10, %r12
	jne	.L809
	movq	16(%r8), %r15
.L739:
	movq	80(%rbx), %rdx
	movq	64(%rbx), %rax
	movq	%r15, -128(%rbp)
	subq	$8, %rdx
	cmpq	%rdx, %rax
	je	.L740
	movq	%r15, (%rax)
	addq	$8, %rax
	movq	%rax, 64(%rbx)
.L741:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-128(%rbp), %r14
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L754
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L810
.L743:
	movq	-8(%rax), %rax
.L742:
	cmpq	%r14, %rax
	jne	.L811
	cmpq	$0, 64(%rax)
	je	.L812
	cmpq	72(%rbx), %rdi
	je	.L746
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L804:
	xorl	%esi, %esi
	jmp	.L749
	.p2align 4,,10
	.p2align 3
.L718:
	movl	$72, %edi
	leaq	-128(%rbp), %r14
	call	_Znwm@PLT
	movq	(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rax, (%r15)
	leaq	48(%r15), %rax
	movq	%rax, 32(%r15)
	movq	%r12, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	movq	%rax, -184(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testq	%rax, %rax
	je	.L752
	movq	8(%rbx), %rdi
	leaq	-168(%rbp), %rsi
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	%rax, -168(%rbp)
	call	*%rdx
	movq	%rax, 16(%r15)
.L752:
	movq	8(%r15), %rsi
	leaq	-96(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	32(%r15), %rdi
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	%rdi, %rax
	cmpq	%rcx, %rdx
	je	.L813
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rdi
	cmpq	%rax, -184(%rbp)
	je	.L814
	movq	%rsi, %xmm0
	movq	%rdi, %xmm3
	movq	48(%r15), %r8
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 40(%r15)
	testq	%rax, %rax
	je	.L731
	movq	%rax, -96(%rbp)
	movq	%r8, -80(%rbp)
.L729:
	movq	$0, -88(%rbp)
	movb	$0, (%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L732
	call	_ZdlPv@PLT
.L732:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, 64(%r15)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L733
	movq	(%rdi), %rax
	call	*8(%rax)
.L733:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L734
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L736
	.p2align 4,,10
	.p2align 3
.L815:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L734
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L734
.L736:
	cmpq	%rsi, %r12
	jne	.L815
	addq	$16, %rcx
.L747:
	movq	%r15, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L737
	cmpq	72(%rbx), %rax
	je	.L816
.L738:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L737
	movq	8(%rbx), %rdi
	leaq	.LC30(%rip), %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L737:
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L739
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC31(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r15), %rsi
	movq	%r15, %rdx
	leaq	.LC32(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L808:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L722
.L810:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L743
.L740:
	leaq	-128(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L741
.L746:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L723
.L811:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L812:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L754:
	xorl	%eax, %eax
	jmp	.L742
.L813:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L727
	cmpq	$1, %rdx
	je	.L817
	movq	%rcx, %rsi
	movq	%rcx, -184(%rbp)
	call	memcpy@PLT
	movq	32(%r15), %rdi
	movq	-88(%rbp), %rdx
	movq	-184(%rbp), %rcx
.L727:
	movq	%rdx, 40(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rax
	jmp	.L729
.L816:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L738
.L814:
	movq	%rsi, %xmm0
	movq	%rdi, %xmm4
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 40(%r15)
.L731:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rax
	jmp	.L729
.L734:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L747
.L806:
	call	__stack_chk_fail@PLT
.L817:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	32(%r15), %rdi
	movq	-88(%rbp), %rdx
	jmp	.L727
	.cfi_endproc
.LFE8076:
	.size	_ZNK4node6worker6Worker10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node6worker6Worker10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_,"axG",@progbits,_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	.type	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_, @function
_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_:
.LFB10217:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L829
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L821
	.p2align 4,,10
	.p2align 3
.L836:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L829
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L829
	movq	%rsi, %rdi
.L821:
	cmpq	%rcx, %r9
	jne	.L836
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L837
	testq	%rcx, %rcx
	je	.L823
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L823
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L823:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L829:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L837:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L830
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L823
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L822:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L838
.L824:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L823
.L830:
	movq	%r10, %rax
	jmp	.L822
.L838:
	movq	%rcx, 16(%rbx)
	jmp	.L824
	.cfi_endproc
.LFE10217:
	.size	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_, .-_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker10JoinThreadEv
	.type	_ZN4node6worker6Worker10JoinThreadEv, @function
_ZN4node6worker6Worker10JoinThreadEv:
.LFB8036:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$80, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 216(%rdi)
	je	.L871
.L839:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L872
	addq	$80, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L871:
	.cfi_restore_state
	movq	%rdi, %r12
	leaq	160(%rdi), %rdi
	call	uv_thread_join@PLT
	testl	%eax, %eax
	jne	.L873
	movq	16(%r12), %rax
	leaq	-96(%rbp), %r13
	movq	%r12, -96(%rbp)
	movb	$1, 216(%r12)
	movq	%r13, %rsi
	leaq	1944(%rax), %rdi
	call	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKS3_
	movq	16(%r12), %rax
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r12), %rax
	movq	3280(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	16(%r12), %rax
	movq	8(%r12), %rdi
	movq	352(%rax), %r8
	testq	%rdi, %rdi
	je	.L843
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L874
.L843:
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	leaq	88(%r8), %rcx
	movq	1016(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L875
.L844:
	movq	16(%r12), %rax
	movl	264(%r12), %esi
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	224(%r12), %rsi
	movq	%rax, -64(%rbp)
	testq	%rsi, %rsi
	je	.L845
	movq	16(%r12), %rax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L876
.L846:
	movq	16(%r12), %rdx
	movq	%rax, -56(%rbp)
	cmpq	$0, 240(%r12)
	movq	352(%rdx), %r8
	jne	.L877
.L848:
	leaq	104(%r8), %rax
	movq	%rax, -48(%rbp)
.L850:
	movq	360(%rdx), %rax
	movq	8(%r12), %rdi
	movq	1168(%rax), %r15
	testq	%rdi, %rdi
	je	.L851
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L878
.L851:
	movq	3280(%rdx), %rsi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L857
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L879
.L857:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L856
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L839
.L856:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L845:
	cmpq	$0, 240(%r12)
	movq	16(%r12), %rdx
	movq	352(%rdx), %r8
	leaq	104(%r8), %rax
	movq	%rax, -56(%rbp)
	je	.L848
.L877:
	movq	232(%r12), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r8, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L880
.L849:
	movq	16(%r12), %rdx
	movq	%rax, -48(%rbp)
	movq	352(%rdx), %r8
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L879:
	leaq	-64(%rbp), %rcx
	movl	$3, %edx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L878:
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L874:
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%r12), %rax
	movq	352(%rax), %r8
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L873:
	leaq	_ZZN4node6worker6Worker10JoinThreadEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L875:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L876:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L880:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rax
	jmp	.L849
.L872:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8036:
	.size	_ZN4node6worker6Worker10JoinThreadEv, .-_ZN4node6worker6Worker10JoinThreadEv
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_E4CallES2_:
.LFB11946:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	cmpb	$0, 345(%rdi)
	jne	.L886
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node6worker6Worker10JoinThreadEv
	.p2align 4,,10
	.p2align 3
.L886:
	.cfi_restore_state
	subq	$1, 1128(%rsi)
	js	.L887
	leaq	1000(%rsi), %rdi
	jne	.L884
	call	uv_unref@PLT
	movq	24(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node6worker6Worker10JoinThreadEv
	.p2align 4,,10
	.p2align 3
.L884:
	.cfi_restore_state
	call	uv_ref@PLT
	movq	24(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node6worker6Worker10JoinThreadEv
	.p2align 4,,10
	.p2align 3
.L887:
	.cfi_restore_state
	leaq	_ZZN4node11Environment8add_refsElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11946:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_E4CallES2_
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E:
.LFB10308:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	movq	$0, (%rdi)
	je	.L891
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE10308:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	.section	.rodata._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_.str1.1,"aMS",@progbits,1
.LC33:
	.string	"basic_string::append"
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_:
.LFB10311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	subq	$8, %rsp
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L899
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L900
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L897:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L900:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L897
.L899:
	leaq	.LC33(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10311:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB10377:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movabsq	$288230376151711743, %rdi
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdi, %rax
	je	.L928
	movq	%rsi, %rcx
	movq	%rsi, %rbx
	subq	%r15, %rcx
	testq	%rax, %rax
	je	.L919
	movabsq	$9223372036854775776, %rsi
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L929
.L903:
	movq	%rsi, %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%rsi, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rsi
	movq	-64(%rbp), %rcx
	movq	%rax, %r14
	movq	-72(%rbp), %rdx
	leaq	(%rax,%rsi), %rax
	leaq	32(%r14), %r8
.L918:
	addq	%r14, %rcx
	movq	(%rdx), %rdi
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L930
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rdi
	movq	%rdi, 16(%rcx)
.L906:
	movq	8(%rdx), %rdi
	movq	%rsi, (%rdx)
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rcx)
	movb	$0, 16(%rdx)
	cmpq	%r15, %rbx
	je	.L907
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L908:
	movq	%rdi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L927:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %rbx
	je	.L931
.L911:
	leaq	16(%rcx), %rsi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rdi
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	jne	.L908
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L931:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	leaq	32(%r14,%rdx), %r8
.L907:
	cmpq	%r12, %rbx
	je	.L912
	movq	%rbx, %rdx
	movq	%r8, %rcx
	.p2align 4,,10
	.p2align 3
.L916:
	leaq	16(%rcx), %rsi
	movq	(%rdx), %rdi
	movq	%rsi, (%rcx)
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rdi
	je	.L932
	movq	16(%rdx), %rsi
	addq	$32, %rdx
	movq	%rdi, (%rcx)
	addq	$32, %rcx
	movq	%rsi, -16(%rcx)
	movq	-24(%rdx), %rsi
	movq	%rsi, -24(%rcx)
	cmpq	%r12, %rdx
	jne	.L916
.L914:
	subq	%rbx, %r12
	addq	%r12, %r8
.L912:
	testq	%r15, %r15
	je	.L917
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %r8
.L917:
	movq	%r14, %xmm0
	movq	%r8, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	movdqu	16(%rdx), %xmm2
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movups	%xmm2, -16(%rcx)
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	jne	.L916
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L929:
	testq	%r8, %r8
	jne	.L904
	movl	$32, %r8d
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L919:
	movl	$32, %esi
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L930:
	movdqu	16(%rdx), %xmm4
	movups	%xmm4, 16(%rcx)
	jmp	.L906
.L904:
	cmpq	%rdi, %r8
	movq	%rdi, %rax
	cmovbe	%r8, %rax
	salq	$5, %rax
	movq	%rax, %rsi
	jmp	.L903
.L928:
	leaq	.LC28(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE10377:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB10527:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L934
	testq	%rsi, %rsi
	je	.L950
.L934:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L951
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L937
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L938:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L952
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L937:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L938
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L951:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L936:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L938
.L950:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L952:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10527:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB10614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L954
	testq	%r14, %r14
	je	.L970
.L954:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L971
	cmpq	$1, %r13
	jne	.L957
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L958:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L972
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L957:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L958
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L971:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L956:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L958
.L970:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L972:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10614:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB10619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L977
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L975:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L977:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L975
	.cfi_endproc
.LFE10619:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.text._ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB10735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L979
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L989
.L1005:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L990:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L979:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1003
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1004
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L982:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L984
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L986:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L987:
	testq	%rsi, %rsi
	je	.L984
.L985:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L986
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L992
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L985
	.p2align 4,,10
	.p2align 3
.L984:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L988
	call	_ZdlPv@PLT
.L988:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L1005
.L989:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L991
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L991:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L992:
	movq	%rdx, %rdi
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1003:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L982
.L1004:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10735:
	.size	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.section	.rodata.str1.1
.LC34:
	.string	"ERR_WORKER_INIT_FAILED"
.LC35:
	.string	"code"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8060:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1040
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1028
	cmpw	$1040, %cx
	jne	.L1008
.L1028:
	movq	23(%rdx), %r12
.L1010:
	testq	%r12, %r12
	je	.L1006
	leaq	176(%r12), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	leaq	160(%r12), %rdi
	movq	%r12, %rcx
	movb	$0, 344(%r12)
	leaq	-240(%rbp), %rsi
	leaq	_ZZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlPvE_4_FUNES8_(%rip), %rdx
	movl	$1, -240(%rbp)
	movq	$4194304, -232(%rbp)
	call	uv_thread_create_ex@PLT
	movl	%eax, %edi
	testl	%eax, %eax
	je	.L1041
	movb	$1, 344(%r12)
	leaq	-192(%rbp), %r14
	movl	$128, %edx
	leaq	-224(%rbp), %rbx
	movq	%r14, %rsi
	call	uv_err_name_r@PLT
	movq	16(%r12), %rax
	movq	%rbx, %rdi
	movq	352(%rax), %r12
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC34(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1042
.L1022:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1043
.L1023:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1044
.L1024:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1045
.L1025:
	movq	%r12, %rdi
	movq	%rdx, -248(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-248(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1046
.L1026:
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L1021:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L1006:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1047
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1041:
	.cfi_restore_state
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L1014
	movb	$0, 8(%rax)
.L1014:
	movq	8(%r12), %rdi
	call	_ZN2v82V89ClearWeakEPm@PLT
	cmpb	$0, 345(%r12)
	movb	$0, 216(%r12)
	jne	.L1048
.L1015:
	movq	16(%r12), %rbx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	1952(%rbx), %rdi
	divq	%rdi
	movq	1944(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1018
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L1018
.L1020:
	cmpq	%rsi, %r12
	je	.L1021
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L1049
.L1018:
	movl	$16, %edi
	call	_Znwm@PLT
	leaq	1944(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	call	_ZNSt10_HashtableIPN4node6worker6WorkerES3_SaIS3_ENSt8__detail9_IdentityESt8equal_toIS3_ESt4hashIS3_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	jmp	.L1021
	.p2align 4,,10
	.p2align 3
.L1048:
	movq	16(%r12), %rdi
	movq	1128(%rdi), %rax
	addq	$1, %rax
	movq	%rax, 1128(%rdi)
	js	.L1050
	addq	$1000, %rdi
	testq	%rax, %rax
	jne	.L1017
	call	uv_unref@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1008:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1010
	.p2align 4,,10
	.p2align 3
.L1040:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1017:
	call	uv_ref@PLT
	jmp	.L1015
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%rax, -248(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-248(%rbp), %rdx
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1046:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1042:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	%rax, -248(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-248(%rbp), %rdi
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1044:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1024
.L1050:
	leaq	_ZZN4node11Environment8add_refsElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1047:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8060:
	.size	_ZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC36:
	.string	"lz"
.LC37:
	.string	"%lu"
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11737:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1052
	leaq	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1052:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC36(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1053:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1053
	cmpb	$120, %dl
	jg	.L1054
	cmpb	$99, %dl
	jg	.L1055
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L1056
	cmpb	$88, %dl
	je	.L1057
	jmp	.L1054
.L1055:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1054
	leaq	.L1059(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1059:
	.long	.L1060-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1060-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1062-.L1059
	.long	.L1061-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1060-.L1059
	.long	.L1054-.L1059
	.long	.L1060-.L1059
	.long	.L1054-.L1059
	.long	.L1054-.L1059
	.long	.L1058-.L1059
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1056:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1083
	jmp	.L1065
.L1054:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1088
	call	_ZdlPv@PLT
	jmp	.L1088
.L1060:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC37(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L1085
.L1062:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L1070:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1070
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1085
.L1058:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1085:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1082
	jmp	.L1069
.L1057:
	movq	(%r8), %rsi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1073
	call	_ZdlPv@PLT
.L1073:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1069
.L1082:
	call	_ZdlPv@PLT
	jmp	.L1069
.L1061:
	leaq	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1069:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1088:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1065
.L1083:
	call	_ZdlPv@PLT
.L1065:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1051
	call	_ZdlPv@PLT
.L1051:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1077
	call	__stack_chk_fail@PLT
.L1077:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11737:
	.size	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11493:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1090
	call	__stack_chk_fail@PLT
.L1090:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11493:
	.size	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_:
.LFB11230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1092
	call	_ZdlPv@PLT
.L1092:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1094
	call	__stack_chk_fail@PLT
.L1094:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11230:
	.size	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata._ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_.str1.1,"aMS",@progbits,1
.LC38:
	.string	" "
.LC39:
	.string	"\n"
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB9729:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	call	*72(%rax)
	movq	%r14, %rsi
	leaq	.LC38(%rip), %rdx
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	-128(%rbp), %r14
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC39(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1097
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_
.L1097:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1098
	call	_ZdlPv@PLT
.L1098:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1099
	call	_ZdlPv@PLT
.L1099:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1100
	call	_ZdlPv@PLT
.L1100:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1096
	call	_ZdlPv@PLT
.L1096:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1102
	call	__stack_chk_fail@PLT
.L1102:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9729:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.1
.LC40:
	.string	"Worker %llu destroyed"
	.section	.text.unlikely
	.align 2
.LCOLDB41:
	.text
.LHOTB41:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6WorkerD2Ev
	.type	_ZN4node6worker6WorkerD2Ev, @function
_ZN4node6worker6WorkerD2Ev:
.LFB8039:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6worker6WorkerE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	176(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	cmpb	$0, 344(%r12)
	je	.L1159
	cmpq	$0, 352(%r12)
	jne	.L1160
	cmpb	$0, 216(%r12)
	je	.L1161
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1157
.L1108:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	328(%r12), %r14
	testq	%r14, %r14
	je	.L1110
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1111
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L1112:
	cmpl	$1, %eax
	je	.L1162
.L1110:
	movq	312(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1116
	movq	(%rdi), %rax
	call	*8(%rax)
.L1116:
	movq	232(%r12), %rdi
	leaq	248(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1117
	call	_ZdlPv@PLT
.L1117:
	movq	%r13, %rdi
	call	uv_mutex_destroy@PLT
	movq	168(%r12), %r13
	testq	%r13, %r13
	je	.L1118
	movq	%r13, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movl	$64, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1118:
	movq	136(%r12), %r13
	testq	%r13, %r13
	je	.L1120
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1121
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1122:
	cmpl	$1, %eax
	je	.L1163
.L1120:
	movq	104(%r12), %rbx
	movq	96(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1126
	.p2align 4,,10
	.p2align 3
.L1130:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1127
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L1130
.L1128:
	movq	96(%r12), %r13
.L1126:
	testq	%r13, %r13
	je	.L1131
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1131:
	movq	80(%r12), %rbx
	movq	72(%r12), %r13
	cmpq	%r13, %rbx
	je	.L1132
	.p2align 4,,10
	.p2align 3
.L1136:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1133
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1136
.L1134:
	movq	72(%r12), %r13
.L1132:
	testq	%r13, %r13
	je	.L1137
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1137:
	movq	64(%r12), %r13
	testq	%r13, %r13
	je	.L1139
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1140
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1141:
	cmpl	$1, %eax
	je	.L1164
.L1139:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1127:
	.cfi_restore_state
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1130
	jmp	.L1128
	.p2align 4,,10
	.p2align 3
.L1133:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L1136
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1162:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1114
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L1115:
	cmpl	$1, %eax
	jne	.L1110
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L1110
	.p2align 4,,10
	.p2align 3
.L1163:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1124
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1125:
	cmpl	$1, %eax
	jne	.L1120
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1143
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1144:
	cmpl	$1, %eax
	jne	.L1139
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1139
	.p2align 4,,10
	.p2align 3
.L1111:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L1112
	.p2align 4,,10
	.p2align 3
.L1121:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1122
	.p2align 4,,10
	.p2align 3
.L1140:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1141
	.p2align 4,,10
	.p2align 3
.L1159:
	leaq	_ZZN4node6worker6WorkerD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1160:
	leaq	_ZZN4node6worker6WorkerD4EvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1161:
	leaq	_ZZN4node6worker6WorkerD4EvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1124:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1143:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1144
	.p2align 4,,10
	.p2align 3
.L1114:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L1115
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker6WorkerD2Ev.cold, @function
_ZN4node6worker6WorkerD2Ev.cold:
.LFSB8039:
.L1157:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	272(%r12), %rdx
	leaq	.LC40(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1108
	.cfi_endproc
.LFE8039:
	.text
	.size	_ZN4node6worker6WorkerD2Ev, .-_ZN4node6worker6WorkerD2Ev
	.section	.text.unlikely
	.size	_ZN4node6worker6WorkerD2Ev.cold, .-_ZN4node6worker6WorkerD2Ev.cold
.LCOLDE41:
	.text
.LHOTE41:
	.globl	_ZN4node6worker6WorkerD1Ev
	.set	_ZN4node6worker6WorkerD1Ev,_ZN4node6worker6WorkerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6WorkerD0Ev
	.type	_ZN4node6worker6WorkerD0Ev, @function
_ZN4node6worker6WorkerD0Ev:
.LFB8041:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node6worker6WorkerD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$360, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8041:
	.size	_ZN4node6worker6WorkerD0Ev, .-_ZN4node6worker6WorkerD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED2Ev:
.LFB10882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	24(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L1168
	movq	(%r12), %rax
	leaq	_ZN4node6worker6WorkerD0Ev(%rip), %rdx
	movq	%r12, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1169
	call	_ZN4node6worker6WorkerD1Ev
	movl	$360, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1168:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L1167
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	8(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1167:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1169:
	.cfi_restore_state
	call	*%rax
	jmp	.L1168
	.cfi_endproc
.LFE10882:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED0Ev:
.LFB10884:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	24(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L1176
	movq	0(%r13), %rax
	leaq	_ZN4node6worker6WorkerD0Ev(%rip), %rdx
	movq	%r13, %rdi
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1177
	call	_ZN4node6worker6WorkerD1Ev
	movl	$360, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L1176:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L1178
	movq	(%rdi), %rax
	call	*8(%rax)
.L1178:
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L1177:
	.cfi_restore_state
	call	*%rax
	jmp	.L1176
	.cfi_endproc
.LFE10884:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED0Ev
	.section	.rodata.str1.8
	.align 8
.LC42:
	.string	"Worker %llu taking heap snapshot"
	.section	.text.unlikely
	.align 2
.LCOLDB44:
	.text
.LHOTB44:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1255
	movq	8(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1228
	cmpw	$1040, %cx
	jne	.L1188
.L1228:
	movq	23(%rdx), %r12
.L1190:
	testq	%r12, %r12
	je	.L1186
	movq	16(%r12), %rbx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rbx,%rax)
	jne	.L1247
.L1192:
	movq	1216(%rbx), %rax
	movsd	40(%r12), %xmm0
	movl	24(%rax), %esi
	testl	%esi, %esi
	je	.L1193
	comisd	.LC17(%rip), %xmm0
	jb	.L1256
.L1193:
	movq	1256(%rbx), %rax
	movq	3272(%rbx), %rdi
	movq	3280(%rbx), %rsi
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	movsd	%xmm1, -104(%rbp)
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1257
	movl	$56, %edi
	call	_Znwm@PLT
	movl	$38, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movsd	.LC43(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node6worker23WorkerHeapSnapshotTakerE(%rip), %rax
	movq	%r15, %rsi
	leaq	-88(%rbp), %rdi
	movq	%rax, (%r15)
	call	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC1EPS2_
	movq	-88(%rbp), %rsi
	movq	24(%rsi), %rax
	testq	%rax, %rax
	je	.L1222
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L1223
	movb	$1, 9(%rax)
	leaq	-80(%rbp), %rdi
	leaq	176(%r12), %r14
	call	_ZN4node17BaseObjectPtrImplINS_6worker23WorkerHeapSnapshotTakerELb0EEC1EPS2_
	movq	%r14, %rdi
	movq	%rbx, -72(%rbp)
	call	uv_mutex_lock@PLT
	movq	352(%r12), %r12
	testq	%r12, %r12
	je	.L1197
	movl	$40, %edi
	call	_Znwm@PLT
	leaq	2488(%r12), %r8
	movb	$0, 8(%rax)
	movq	%rax, %r15
	movq	%r8, %rdi
	movq	$0, 16(%rax)
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_EE(%rip), %rax
	movq	%rax, (%r15)
	movq	-80(%rbp), %rax
	movq	%r8, -112(%rbp)
	movq	%rax, 24(%r15)
	movq	-72(%rbp), %rax
	movq	$0, -80(%rbp)
	movq	%rax, 32(%r15)
	call	uv_mutex_lock@PLT
	movq	2568(%r12), %rax
	lock addq	$1, 2552(%r12)
	movq	-112(%rbp), %r8
	testq	%rax, %rax
	movq	%r15, 2568(%r12)
	je	.L1198
	movq	16(%rax), %rdi
	movq	%r15, 16(%rax)
	testq	%rdi, %rdi
	je	.L1200
.L1253:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-112(%rbp), %r8
.L1200:
	movq	%r8, %rdi
	call	uv_mutex_unlock@PLT
	leaq	1000(%r12), %rdi
	call	uv_async_send@PLT
	movq	%r12, %rdi
	call	_ZN4node11Environment22RequestInterruptFromV8Ev@PLT
	movq	%r14, %rdi
	movl	$1, %r14d
	call	uv_mutex_unlock@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1258
.L1219:
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1213
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1214
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1205
	cmpb	$0, 9(%rdx)
	jne	.L1259
	cmpb	$0, 8(%rdx)
	je	.L1205
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1205
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1205:
	movq	0(%r13), %r12
	testb	%r14b, %r14b
	jne	.L1221
.L1254:
	movq	-88(%rbp), %rdi
.L1208:
	movq	16(%r12), %rax
	movq	%rax, 24(%r12)
	.p2align 4,,10
	.p2align 3
.L1210:
	testq	%rdi, %rdi
	je	.L1212
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1213
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1214
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L1212
	cmpb	$0, 9(%rdx)
	jne	.L1260
	cmpb	$0, 8(%rdx)
	je	.L1212
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1212
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	1256(%rbx), %rax
	movsd	-104(%rbp), %xmm2
	movsd	%xmm2, 24(%rax)
.L1186:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1261
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore_state
	movq	1256(%rbx), %rax
	movsd	-104(%rbp), %xmm3
	movsd	%xmm3, 24(%rax)
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1198:
	movq	2560(%r12), %rdi
	movq	%r15, 2560(%r12)
	testq	%rdi, %rdi
	je	.L1200
	movq	%r8, -112(%rbp)
	jmp	.L1253
	.p2align 4,,10
	.p2align 3
.L1197:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L1262
	movq	0(%r13), %r12
	jmp	.L1254
	.p2align 4,,10
	.p2align 3
.L1255:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1258:
	movq	0(%r13), %r12
.L1221:
	movq	-88(%rbp), %rdi
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L1208
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L1263
.L1209:
	movq	(%rax), %rax
	movq	%rax, 24(%r12)
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1222:
	movl	$24, %edi
	movq	%rsi, -104(%rbp)
	call	_Znwm@PLT
	movq	-104(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movq	8(%rsi), %rdx
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1226
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1196:
	movb	%dl, 8(%rax)
	movq	%rsi, 16(%rax)
	movq	%rax, 24(%rsi)
.L1223:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1256:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1259:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1205
	.p2align 4,,10
	.p2align 3
.L1260:
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1262:
	xorl	%r14d, %r14d
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	16(%rdi), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-88(%rbp), %rdi
	testq	%rax, %rax
	jne	.L1209
	jmp	.L1208
	.p2align 4,,10
	.p2align 3
.L1213:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1214:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1226:
	xorl	%edx, %edx
	jmp	.L1196
.L1261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB8083:
.L1247:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	272(%r12), %rdx
	leaq	.LC42(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	16(%r12), %rbx
	jmp	.L1192
	.cfi_endproc
.LFE8083:
	.text
	.size	_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE44:
	.text
.LHOTE44:
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"Creating new worker instance with thread id %llu"
	.align 8
.LC46:
	.string	"Preparation for worker %llu finished"
	.section	.text.unlikely
	.align 2
.LCOLDB47:
	.text
.LHOTB47:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE
	.type	_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE, @function
_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE:
.LFB7982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movq	%rcx, -152(%rbp)
	movl	$37, %ecx
	movsd	.LC43(%rip), %xmm0
	movq	%rax, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node6worker6WorkerE(%rip), %rax
	movdqu	0(%r13), %xmm1
	movq	%rax, (%r14)
	movq	8(%r13), %rax
	movups	%xmm1, 56(%r14)
	testq	%rax, %rax
	je	.L1265
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1266
	lock addl	$1, 8(%rax)
.L1265:
	movq	8(%r15), %r13
	subq	(%r15), %r13
	pxor	%xmm0, %xmm0
	movq	$0, 88(%r14)
	movq	%r13, %rax
	movups	%xmm0, 72(%r14)
	sarq	$5, %rax
	je	.L1393
	movabsq	$288230376151711743, %rdx
	cmpq	%rdx, %rax
	ja	.L1394
	movq	%r13, %rdi
	call	_Znwm@PLT
	movq	%rax, %r12
.L1268:
	movq	%r12, %xmm0
	addq	%r12, %r13
	punpcklqdq	%xmm0, %xmm0
	movq	%r13, 88(%r14)
	movups	%xmm0, 72(%r14)
	movq	8(%r15), %rax
	movq	(%r15), %r9
	movq	%rax, -136(%rbp)
	cmpq	%r9, %rax
	je	.L1270
	movq	%r9, %r13
	leaq	-104(%rbp), %r15
	jmp	.L1276
	.p2align 4,,10
	.p2align 3
.L1396:
	movzbl	(%r11), %eax
	movb	%al, 16(%r12)
.L1275:
	movq	%r8, 8(%r12)
	addq	$32, %r13
	addq	$32, %r12
	movb	$0, (%rdi,%r8)
	cmpq	%r13, -136(%rbp)
	je	.L1270
.L1276:
	leaq	16(%r12), %rdi
	movq	8(%r13), %r8
	movq	%rdi, (%r12)
	movq	0(%r13), %r11
	movq	%r11, %rax
	addq	%r8, %rax
	je	.L1271
	testq	%r11, %r11
	je	.L1313
.L1271:
	movq	%r8, -104(%rbp)
	cmpq	$15, %r8
	ja	.L1395
	cmpq	$1, %r8
	je	.L1396
	testq	%r8, %r8
	je	.L1275
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r11, -128(%rbp)
	movq	%r8, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %r8
	movq	-128(%rbp), %r11
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%r12)
.L1273:
	movq	%r8, %rdx
	movq	%r11, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r8
	movq	(%r12), %rdi
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	%r12, 80(%r14)
	pxor	%xmm0, %xmm0
	leaq	-112(%rbp), %r15
	xorl	%esi, %esi
	movq	360(%rbx), %rax
	movq	$0, 112(%r14)
	movq	%r15, %rdi
	leaq	-104(%rbp), %r13
	movups	%xmm0, 96(%r14)
	movq	2392(%rax), %rax
	movq	%rax, 120(%r14)
	call	_ZN4node20ArrayBufferAllocator6CreateEb@PLT
	movq	-112(%rbp), %rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	$0, 136(%r14)
	movq	%rax, 128(%r14)
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN4node20ArrayBufferAllocatorESt14default_deleteIS5_EEEOSt10unique_ptrIT_T0_E
	movq	-104(%rbp), %rax
	movq	136(%r14), %r15
	cmpq	%r15, %rax
	je	.L1277
	testq	%rax, %rax
	je	.L1278
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1279
	lock addl	$1, 8(%rax)
	movq	136(%r14), %r15
.L1278:
	testq	%r15, %r15
	je	.L1281
.L1408:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1282
	movl	$-1, %ecx
	lock xaddl	%ecx, 8(%r15)
.L1283:
	cmpl	$1, %ecx
	jne	.L1281
	movq	(%r15), %rcx
	movq	%rdx, -128(%rbp)
	movq	%r15, %rdi
	movq	%rax, -120(%rbp)
	call	*16(%rcx)
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rax
	testq	%rdx, %rdx
	je	.L1285
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r15)
.L1286:
	cmpl	$1, %edx
	je	.L1397
	.p2align 4,,10
	.p2align 3
.L1281:
	movq	%rax, 136(%r14)
	movq	-104(%rbp), %r15
.L1277:
	testq	%r15, %r15
	je	.L1288
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L1289
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L1398
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1294
	movq	(%rdi), %rax
	call	*8(%rax)
.L1294:
	movzbl	1136(%rbx), %eax
	leaq	176(%r14), %rdi
	movq	$0, 144(%r14)
	movq	$0, 168(%r14)
	movb	%al, 152(%r14)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L1399
	leaq	248(%r14), %rax
	movb	$1, 216(%r14)
	movq	$0, 224(%r14)
	movq	%rax, 232(%r14)
	movq	$0, 240(%r14)
	movb	$0, 248(%r14)
	movl	$0, 264(%r14)
	call	_ZN4node11Environment16AllocateThreadIdEv@PLT
	movq	-144(%rbp), %rcx
	movq	$0, 280(%r14)
	movq	%rax, 272(%r14)
	movq	$0, 312(%r14)
	movq	(%rcx), %rax
	movq	%rax, 320(%r14)
	movq	8(%rcx), %rax
	movq	%rax, 328(%r14)
	testq	%rax, %rax
	je	.L1296
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1297
	lock addl	$1, 8(%rax)
.L1296:
	movl	$257, %edx
	leaq	272(%r14), %rax
	movq	$0, 336(%r14)
	movw	%dx, 344(%r14)
	movq	16(%r14), %rdx
	movq	%rax, -128(%rbp)
	movslq	32(%r14), %rax
	movq	$0, 352(%r14)
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1390
.L1298:
	movq	3280(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%r13, %rdx
	movq	$0, -104(%rbp)
	call	_ZN4node6worker11MessagePort3NewEPNS_11EnvironmentEN2v85LocalINS4_7ContextEEESt10unique_ptrINS0_15MessagePortDataESt14default_deleteIS9_EE@PLT
	movq	-104(%rbp), %rdi
	movq	%rax, 336(%r14)
	testq	%rdi, %rdi
	je	.L1299
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	336(%r14), %rax
.L1299:
	testq	%rax, %rax
	je	.L1264
	movl	$104, %edi
	call	_Znwm@PLT
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN4node6worker15MessagePortDataC1EPNS0_11MessagePortE@PLT
	movq	312(%r14), %rdi
	movq	%r15, 312(%r14)
	testq	%rdi, %rdi
	je	.L1302
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	312(%r14), %r15
.L1302:
	movq	336(%r14), %rdi
	movq	%r15, %rsi
	call	_ZN4node6worker11MessagePort8EntangleEPS1_PNS0_15MessagePortDataE@PLT
	movq	8(%r14), %r15
	testq	%r15, %r15
	je	.L1303
	movzbl	11(%r15), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1400
.L1303:
	movq	336(%r14), %rdx
	movq	8(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L1304
	movzbl	11(%rcx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1401
.L1304:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	1016(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1402
.L1305:
	movq	8(%r14), %r15
	testq	%r15, %r15
	je	.L1306
	movzbl	11(%r15), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1403
.L1306:
	movq	272(%r14), %rax
	testq	%rax, %rax
	js	.L1307
.L1410:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1308:
	movq	352(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	1720(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1404
.L1309:
	movq	2080(%rbx), %rsi
	movq	-152(%rbp), %rcx
	movq	%r13, %rdi
	movl	272(%r14), %edx
	call	_ZN4node9inspector5Agent15GetParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-104(%rbp), %rax
	movq	168(%r14), %r12
	movq	$0, -104(%rbp)
	movq	%rax, 168(%r14)
	testq	%r12, %r12
	je	.L1311
	movq	%r12, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movq	%r12, %rdi
	movl	$64, %esi
	call	_ZdlPvm@PLT
	movq	-104(%rbp), %r12
	testq	%r12, %r12
	je	.L1311
	movq	%r12, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movl	$64, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L1311:
	movq	1696(%rbx), %rax
	leaq	-80(%rbp), %rcx
	movq	%rcx, -96(%rbp)
	movq	(%rax), %r12
	movq	8(%rax), %r15
	movq	%rcx, -120(%rbp)
	movq	%r12, %rax
	addq	%r15, %rax
	je	.L1338
	testq	%r12, %r12
	je	.L1313
.L1338:
	movq	%r15, -104(%rbp)
	cmpq	$15, %r15
	ja	.L1405
	cmpq	$1, %r15
	jne	.L1317
	movzbl	(%r12), %eax
	movb	%al, -80(%rbp)
	movq	-120(%rbp), %rax
.L1318:
	movq	%r15, -88(%rbp)
	movl	$32, %edi
	movb	$0, (%rax,%r15)
	call	_Znwm@PLT
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %r12
	leaq	16(%rax), %rdi
	movq	%rax, %r15
	leaq	32(%rax), %rbx
	movq	%rdi, (%rax)
	movq	%r9, %rax
	addq	%r12, %rax
	je	.L1339
	testq	%r9, %r9
	je	.L1313
.L1339:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L1406
	cmpq	$1, %r12
	je	.L1322
	testq	%r12, %r12
	jne	.L1321
.L1323:
	movq	%r12, 8(%r15)
	movq	%rbx, %xmm2
	movq	%r15, %xmm0
	movb	$0, (%rdi,%r12)
	movq	96(%r14), %r12
	punpcklqdq	%xmm2, %xmm0
	movq	104(%r14), %r13
	movq	%rbx, 112(%r14)
	movups	%xmm0, 96(%r14)
	movq	%r12, %rbx
	cmpq	%r13, %r12
	je	.L1328
	.p2align 4,,10
	.p2align 3
.L1324:
	movq	(%rbx), %rdi
	leaq	16(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L1327
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%rbx, %r13
	jne	.L1324
.L1328:
	testq	%r12, %r12
	je	.L1326
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1326:
	movq	-96(%rbp), %rdi
	cmpq	-120(%rbp), %rdi
	je	.L1330
	call	_ZdlPv@PLT
.L1330:
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L1333
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1332
.L1333:
	movq	8(%r14), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r14, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L1332:
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1391
.L1264:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1407
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1266:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L1265
	.p2align 4,,10
	.p2align 3
.L1297:
	addl	$1, 8(%rax)
	jmp	.L1296
	.p2align 4,,10
	.p2align 3
.L1307:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1308
	.p2align 4,,10
	.p2align 3
.L1289:
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	cmpl	$1, %eax
	jne	.L1288
.L1398:
	movq	(%r15), %rax
	movq	%rdx, -120(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1292
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L1293:
	cmpl	$1, %eax
	jne	.L1288
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L1288
	.p2align 4,,10
	.p2align 3
.L1327:
	addq	$32, %rbx
	cmpq	%rbx, %r13
	jne	.L1324
	jmp	.L1328
	.p2align 4,,10
	.p2align 3
.L1393:
	xorl	%r12d, %r12d
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1279:
	addl	$1, 8(%rax)
	testq	%r15, %r15
	jne	.L1408
	jmp	.L1281
	.p2align 4,,10
	.p2align 3
.L1322:
	movzbl	(%r9), %eax
	movb	%al, 16(%r15)
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1317:
	testq	%r15, %r15
	jne	.L1409
	movq	-120(%rbp), %rax
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1406:
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r9, -136(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-136(%rbp), %r9
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%r15)
.L1321:
	movq	%r12, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	(%r15), %rdi
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1405:
	leaq	-96(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L1316:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r15
	movq	-96(%rbp), %rax
	jmp	.L1318
	.p2align 4,,10
	.p2align 3
.L1282:
	movl	8(%r15), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%r15)
	jmp	.L1283
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	16(%r14), %rax
	movq	(%r15), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r15
	jmp	.L1303
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	16(%rdx), %rax
	movq	(%rcx), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rcx
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1403:
	movq	16(%r14), %rax
	movq	(%r15), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r15
	movq	272(%r14), %rax
	testq	%rax, %rax
	jns	.L1410
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1399:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1292:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L1293
	.p2align 4,,10
	.p2align 3
.L1404:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1309
	.p2align 4,,10
	.p2align 3
.L1402:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1305
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	12(%r15), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r15)
	jmp	.L1286
	.p2align 4,,10
	.p2align 3
.L1397:
	movq	(%r15), %rdx
	movq	%rax, -120(%rbp)
	movq	%r15, %rdi
	call	*24(%rdx)
	movq	-120(%rbp), %rax
	jmp	.L1281
.L1313:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1394:
	call	_ZSt17__throw_bad_allocv@PLT
.L1407:
	call	__stack_chk_fail@PLT
.L1409:
	movq	-120(%rbp), %rdi
	jmp	.L1316
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE.cold, @function
_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE.cold:
.LFSB7982:
.L1391:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	-128(%rbp), %rdx
	leaq	.LC46(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1264
.L1390:
	leaq	272(%r14), %rdx
	leaq	.LC45(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1298
	.cfi_endproc
.LFE7982:
	.text
	.size	_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE, .-_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE
	.section	.text.unlikely
	.size	_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE.cold, .-_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE.cold
.LCOLDE47:
	.text
.LHOTE47:
	.globl	_ZN4node6worker6WorkerC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE
	.set	_ZN4node6worker6WorkerC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE,_ZN4node6worker6WorkerC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"ERR_MISSING_PLATFORM_FOR_WORKER"
	.align 8
.LC49:
	.string	"The V8 platform used by this instance of Node does not support creating Workers"
	.section	.rodata.str1.1
.LC50:
	.string	"NODE_OPTIONS"
.LC52:
	.string	"invalidNodeOptions"
.LC53:
	.string	"invalidExecArgv"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8042:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2600, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1785
	cmpw	$1040, %cx
	jne	.L1412
.L1785:
	movq	23(%rdx), %r14
.L1414:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1415
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2045
.L1415:
	movq	360(%r14), %rax
	cmpq	$0, 2392(%rax)
	je	.L2046
	leaq	-2240(%rbp), %rax
	pxor	%xmm0, %xmm0
	cmpl	$4, 16(%rbx)
	movq	8(%rdi), %r12
	movq	%rax, -2632(%rbp)
	movq	%rax, -2256(%rbp)
	movq	$0, -2248(%rbp)
	movb	$0, -2240(%rbp)
	movq	$0, -2528(%rbp)
	movaps	%xmm0, -2560(%rbp)
	movaps	%xmm0, -2544(%rbp)
	jne	.L2047
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L2048
.L1424:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%r12, %rsi
	leaq	-1104(%rbp), %rdi
	movq	%rax, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %rdx
	movabsq	$4611686018427387903, %rax
	subq	-2248(%rbp), %rax
	movq	-1088(%rbp), %rsi
	cmpq	%rax, %rdx
	ja	.L2049
	leaq	-2256(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1427
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1427
	call	free@PLT
.L1427:
	cmpl	$1, 16(%rbx)
	jg	.L2050
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1429:
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1430
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L2051
.L1430:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1455
	leaq	-2288(%rbp), %rdi
	call	_ZN4node7KVStore16CreateMapKVStoreEv@PLT
	movq	-2552(%rbp), %r13
	pxor	%xmm1, %xmm1
	movdqa	-2288(%rbp), %xmm0
	movq	-2288(%rbp), %r8
	movaps	%xmm1, -2288(%rbp)
	movaps	%xmm0, -2560(%rbp)
	testq	%r13, %r13
	je	.L1456
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1457
	lock subl	$1, 8(%r13)
	je	.L1458
.L1459:
	movq	-2280(%rbp), %r13
	testq	%r13, %r13
	je	.L2007
	testq	%r15, %r15
	je	.L1465
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1466:
	cmpl	$1, %eax
	je	.L1467
.L2007:
	movq	-2560(%rbp), %r8
.L1456:
	movq	(%r8), %rax
	cmpl	$1, 16(%rbx)
	movq	80(%rax), %r15
	jle	.L2052
	movq	8(%rbx), %rax
	leaq	-8(%rax), %r13
.L1472:
	movq	%r12, %rdi
	movq	%r8, -2576(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-2576(%rbp), %r8
	movq	%r13, %rdx
	movq	%rax, %rsi
	movq	%r8, %rdi
	call	*%r15
.L1454:
	cmpl	$1, 16(%rbx)
	jg	.L1482
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1483:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L1489
	cmpl	$2, 16(%rbx)
	jg	.L1487
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1488:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L1489
	movq	$0, -2608(%rbp)
	movq	$0, -2616(%rbp)
.L1490:
	cmpl	$2, 16(%rbx)
	jg	.L1599
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L1601
.L2067:
	cmpl	$2, 16(%rbx)
	jg	.L1602
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L1603:
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	leaq	-2176(%rbp), %r12
	movq	$0, -2184(%rbp)
	movq	%r12, -2192(%rbp)
	movb	$0, -2176(%rbp)
	movq	$0, -2400(%rbp)
	movaps	%xmm0, -2416(%rbp)
	call	_Znwm@PLT
	movq	-2192(%rbp), %r8
	movq	-2184(%rbp), %r15
	leaq	16(%rax), %rdi
	leaq	32(%rax), %rcx
	movq	%rax, -2416(%rbp)
	movq	%rax, %r9
	movq	%rdi, (%rax)
	movq	%r8, %rax
	addq	%r15, %rax
	movq	%rcx, -2400(%rbp)
	je	.L1786
	testq	%r8, %r8
	je	.L1604
.L1786:
	movq	%r15, -2288(%rbp)
	cmpq	$15, %r15
	ja	.L2053
	cmpq	$1, %r15
	jne	.L2054
	movzbl	(%r8), %eax
	movb	%al, 16(%r9)
.L1609:
	movq	%r15, 8(%r9)
	movb	$0, (%rdi,%r15)
	movq	-2192(%rbp), %rdi
	movq	%rcx, -2408(%rbp)
	cmpq	%r12, %rdi
	je	.L1610
	call	_ZdlPv@PLT
.L1610:
	movq	%r13, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, -2576(%rbp)
	testl	%eax, %eax
	je	.L1611
	leaq	-1104(%rbp), %rax
	xorl	%r12d, %r12d
	movq	%rax, -2584(%rbp)
	.p2align 4,,10
	.p2align 3
.L1628:
	movq	3280(%r14), %rsi
	movq	%r13, %rdi
	movl	%r12d, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1629
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1629
	movq	(%rbx), %rax
	movq	-2584(%rbp), %rdi
	movq	8(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r15
	movq	-1104(%rbp), %r8
	leaq	-2144(%rbp), %rcx
	movq	%rcx, -2160(%rbp)
	movq	%r15, %rax
	addq	%r8, %rax
	je	.L1771
	testq	%r15, %r15
	je	.L1604
.L1771:
	movq	%r8, -2288(%rbp)
	cmpq	$15, %r8
	ja	.L2055
	cmpq	$1, %r8
	jne	.L1616
	movzbl	(%r15), %eax
	movb	%al, -2144(%rbp)
	movq	%rcx, %rax
.L1617:
	movq	%r8, -2152(%rbp)
	movb	$0, (%rax,%r8)
	movq	-2408(%rbp), %r15
	cmpq	-2400(%rbp), %r15
	je	.L1618
	leaq	16(%r15), %rdi
	movq	-2152(%rbp), %r9
	movq	%rdi, (%r15)
	movq	-2160(%rbp), %r10
	movq	%r10, %rax
	addq	%r9, %rax
	je	.L1787
	testq	%r10, %r10
	je	.L1604
.L1787:
	movq	%r9, -2288(%rbp)
	cmpq	$15, %r9
	ja	.L2056
	cmpq	$1, %r9
	jne	.L1622
	movzbl	(%r10), %eax
	movb	%al, 16(%r15)
.L1623:
	movq	%r9, 8(%r15)
	movb	$0, (%rdi,%r9)
	addq	$32, -2408(%rbp)
.L1624:
	movq	-2160(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L1625
	call	_ZdlPv@PLT
.L1625:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1626
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1626
	call	free@PLT
	addl	$1, %r12d
	cmpl	%r12d, -2576(%rbp)
	jne	.L1628
.L1611:
	leaq	-2384(%rbp), %rax
	movq	-2616(%rbp), %rcx
	pxor	%xmm0, %xmm0
	leaq	-2352(%rbp), %r12
	leaq	-2544(%rbp), %r13
	movq	%rax, %rdx
	leaq	-2416(%rbp), %rdi
	movq	%r12, %r9
	movl	$1, %r8d
	movq	%r13, %rsi
	movq	%rax, -2576(%rbp)
	movq	$0, -2368(%rbp)
	movq	$0, -2336(%rbp)
	movaps	%xmm0, -2384(%rbp)
	movaps	%xmm0, -2352(%rbp)
	call	_ZN4node14options_parser5ParseINS_17PerIsolateOptionsENS_7OptionsEEEvPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EESD_SD_PT_NS_20OptionEnvvarSettingsESD_@PLT
	movq	-2384(%rbp), %r15
	movq	-2376(%rbp), %rdx
	leaq	32(%r15), %rax
	cmpq	%rax, %rdx
	je	.L1630
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %r8
	sarq	$5, %r8
	testq	%rcx, %rcx
	jle	.L1782
	movq	%r13, -2584(%rbp)
	addq	$48, %r15
	movq	%rbx, %r13
	movq	%r12, -2592(%rbp)
	movq	%r8, %r12
	jmp	.L1637
	.p2align 4,,10
	.p2align 3
.L1631:
	leaq	-32(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2057
	movq	%rdx, -40(%r15)
	movq	(%r15), %rdx
	movq	-32(%r15), %rax
	movq	%rbx, -48(%r15)
	movq	%rdx, -32(%r15)
	testq	%rdi, %rdi
	je	.L1636
	movq	%rdi, -16(%r15)
	movq	%rax, (%r15)
.L1634:
	movq	$0, -8(%r15)
	addq	$32, %r15
	movb	$0, (%rdi)
	subq	$1, %r12
	je	.L2058
.L1637:
	movq	-16(%r15), %rbx
	movq	-48(%r15), %rdi
	movq	-8(%r15), %rdx
	cmpq	%rbx, %r15
	jne	.L1631
	testq	%rdx, %rdx
	je	.L1632
	cmpq	$1, %rdx
	je	.L2059
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-8(%r15), %rdx
	movq	-48(%r15), %rdi
.L1632:
	movq	%rdx, -40(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-16(%rbx), %rdi
	jmp	.L1634
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L1424
	movslq	43(%rax), %rax
	subl	$3, %eax
	andl	$-3, %eax
	je	.L1425
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L2050:
	movq	8(%rbx), %rdi
.L1425:
	subq	$8, %rdi
	jmp	.L1429
	.p2align 4,,10
	.p2align 3
.L2045:
	cmpl	$5, 43(%rax)
	jne	.L1415
	leaq	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	1384(%r14), %xmm0
	movq	1392(%r14), %rax
	testq	%rax, %rax
	je	.L1473
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1474
	lock addl	$1, 8(%rax)
.L1473:
	movq	%rax, %xmm5
	movq	-2552(%rbp), %r13
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -2560(%rbp)
.L2027:
	testq	%r13, %r13
	je	.L1454
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1477
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1478:
	cmpl	$1, %eax
	jne	.L1454
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1480
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1481:
	cmpl	$1, %eax
	jne	.L1454
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1454
	.p2align 4,,10
	.p2align 3
.L1489:
	movl	$64, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movl	$768, %edi
	movq	%rax, %r15
	movups	%xmm0, (%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	leaq	16+_ZTVN4node17PerIsolateOptionsE(%rip), %rax
	movq	%rax, (%r15)
	call	_Znwm@PLT
	movl	$96, %ecx
	movl	$29812, %esi
	movabsq	$3328210909095473713, %rdx
	movq	%rax, %r13
	xorl	%eax, %eax
	movq	%rdx, -1088(%rbp)
	movq	%r13, %rdi
	movb	$49, -1080(%rbp)
	rep stosq
	leaq	16+_ZTVN4node18EnvironmentOptionsE(%rip), %rax
	movl	$28788, %ecx
	movw	%si, 712(%r13)
	movq	%rax, 0(%r13)
	leaq	32(%r13), %rax
	movl	$24, %edi
	movq	%rax, 16(%r13)
	leaq	64(%r13), %rax
	movq	%rax, 48(%r13)
	leaq	104(%r13), %rax
	movq	%rax, 88(%r13)
	leaq	136(%r13), %rax
	movq	%rax, 120(%r13)
	leaq	168(%r13), %rax
	movq	%rax, 152(%r13)
	leaq	208(%r13), %rax
	movq	%rax, 192(%r13)
	leaq	240(%r13), %rax
	movq	%rax, 224(%r13)
	leaq	288(%r13), %rax
	movq	%rax, 272(%r13)
	leaq	328(%r13), %rax
	movq	%rax, 312(%r13)
	leaq	368(%r13), %rax
	movq	%rax, 352(%r13)
	leaq	400(%r13), %rax
	movq	%rax, 384(%r13)
	leaq	448(%r13), %rax
	movq	%rax, 432(%r13)
	leaq	488(%r13), %rax
	movq	%rax, 472(%r13)
	leaq	520(%r13), %rax
	movq	%rax, 504(%r13)
	leaq	560(%r13), %rax
	movq	%rax, 544(%r13)
	leaq	608(%r13), %rax
	movq	%rax, 592(%r13)
	leaq	16+_ZTVN4node12DebugOptionsE(%rip), %rax
	movq	%rax, 672(%r13)
	leaq	704(%r13), %rax
	movw	%cx, 244(%r13)
	movabsq	$7506500514572694643, %rcx
	movq	%rax, 688(%r13)
	leaq	744(%r13), %rax
	movq	%rcx, 704(%r13)
	movl	$1953000556, 240(%r13)
	movq	$6, 232(%r13)
	movq	$120000, 256(%r13)
	movq	$1000, 304(%r13)
	movq	$524288, 416(%r13)
	movb	$112, 714(%r13)
	movq	$11, 696(%r13)
	movq	%rax, 728(%r13)
	movq	%rdx, 744(%r13)
	movq	%r13, 8(%r15)
	movb	$49, 752(%r13)
	movq	$9, 736(%r13)
	movl	$9229, 760(%r13)
	movq	$0, 16(%r15)
	movq	%r15, -2616(%rbp)
	movabsq	$4294967297, %r15
	call	_Znwm@PLT
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movl	$21075, %edi
	movq	%r13, 16(%rax)
	movq	-2616(%rbp), %r13
	movq	%rcx, (%rax)
	movq	%r15, 8(%rax)
	movq	%rax, 16(%r13)
	leaq	48(%r13), %rax
	movw	%di, 52(%r13)
	movl	$24, %edi
	movl	$0, 24(%r13)
	movq	%rax, 32(%r13)
	movl	$1430735187, 48(%r13)
	movb	$50, 54(%r13)
	movq	$7, 40(%r13)
	movb	$0, 55(%r13)
	call	_Znwm@PLT
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	leaq	_ZNSt14_Function_base13_Base_managerIZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS4_5ValueEEEEUlPKcE_E10_M_managerERSt9_Any_dataRKSE_St18_Manager_operation(%rip), %rsi
	movdqu	8(%r13), %xmm3
	movq	%rax, -2608(%rbp)
	movq	%rsi, %xmm0
	movq	%r15, 8(%rax)
	movq	%rcx, (%rax)
	movq	%r13, 16(%rax)
	leaq	-2560(%rbp), %rax
	movq	%rax, -1104(%rbp)
	leaq	_ZNSt17_Function_handlerIFNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcEZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINSC_5ValueEEEEUlS7_E_E9_M_invokeERKSt9_Any_dataOS7_(%rip), %rax
	movq	%rax, %xmm2
	movq	16(%r13), %rax
	movaps	%xmm3, -2576(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm3, -2288(%rbp)
	movaps	%xmm0, -1088(%rbp)
	testq	%rax, %rax
	je	.L1486
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1491
	lock addl	$1, 8(%rax)
.L1486:
	leaq	-1104(%rbp), %rax
	leaq	-2288(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -2584(%rbp)
	call	_ZN4node16HandleEnvOptionsESt10shared_ptrINS_18EnvironmentOptionsEESt8functionIFNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcEE@PLT
	movq	-2280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1493
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1494
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L1495:
	cmpl	$1, %eax
	jne	.L1493
	movq	(%rdi), %rax
	movq	%rdi, -2576(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-2576(%rbp), %rdi
	je	.L1497
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1498:
	cmpl	$1, %eax
	jne	.L1493
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L1493:
	movq	-1088(%rbp), %rax
	testq	%rax, %rax
	je	.L1499
	movq	-2584(%rbp), %rsi
	movl	$3, %edx
	movq	%rsi, %rdi
	call	*%rax
.L1499:
	movq	-2560(%rbp), %r15
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r12, %rdi
	leaq	.LC50(%rip), %rsi
	movq	(%r15), %rax
	movq	16(%rax), %rax
	movq	%rax, -2576(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2060
.L1500:
	movq	-2576(%rbp), %rax
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1490
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-2224(%rbp), %r12
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-2288(%rbp), %r8
	leaq	-2208(%rbp), %rax
	movq	%rax, -2576(%rbp)
	movq	%rax, -2224(%rbp)
	testq	%r8, %r8
	je	.L1604
	movq	%r8, %rdi
	movq	%r8, -2592(%rbp)
	call	strlen@PLT
	movq	-2592(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -2320(%rbp)
	movq	%rax, %r15
	ja	.L2061
	cmpq	$1, %rax
	jne	.L1505
	movzbl	(%r8), %edx
	movb	%dl, -2208(%rbp)
	movq	-2576(%rbp), %rdx
.L1506:
	movq	%rax, -2216(%rbp)
	movq	%r13, %rdi
	leaq	-2480(%rbp), %r13
	movb	$0, (%rdx,%rax)
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	leaq	-2512(%rbp), %r9
	movaps	%xmm0, -2512(%rbp)
	movq	%r9, %rdx
	movq	%r9, -2592(%rbp)
	movq	$0, -2496(%rbp)
	call	_ZN4node22ParseNodeOptionsEnvVarERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPSt6vectorIS5_SaIS5_EE@PLT
	movq	-2472(%rbp), %rax
	leaq	-1088(%rbp), %rcx
	cmpq	-2464(%rbp), %rax
	movq	%rcx, -1104(%rbp)
	movq	-2480(%rbp), %r15
	movb	$0, -1088(%rbp)
	movq	-2592(%rbp), %r9
	movq	$0, -1096(%rbp)
	je	.L1507
	leaq	16(%rax), %rdx
	movq	%rdx, (%rax)
	cmpq	%rax, %r15
	je	.L2062
	movq	-32(%rax), %rsi
	leaq	-16(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L2063
	movq	%rsi, (%rax)
	movq	-16(%rax), %rsi
	movq	%rsi, 16(%rax)
.L1513:
	movq	-24(%rax), %rsi
	movb	$0, -16(%rax)
	movq	%rdx, -32(%rax)
	movq	%rsi, 8(%rax)
	movq	-2472(%rbp), %rsi
	movq	$0, -24(%rax)
	leaq	32(%rsi), %rax
	movq	%rax, -2472(%rbp)
	leaq	-32(%rsi), %rax
	subq	$48, %rsi
	subq	%r15, %rax
	movq	%rax, %r8
	sarq	$5, %r8
	testq	%rax, %rax
	jle	.L1523
	movq	%r13, -2592(%rbp)
	movq	%rsi, %r12
	movq	%r8, %r13
	movq	%rbx, -2584(%rbp)
	jmp	.L1524
	.p2align 4,,10
	.p2align 3
.L1517:
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2064
	movq	%rdx, 24(%r12)
	movq	(%r12), %rdx
	movq	32(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rdx, 32(%r12)
	testq	%rdi, %rdi
	je	.L1522
	movq	%rdi, -16(%r12)
	movq	%rax, (%r12)
.L1520:
	movq	-16(%r12), %rax
	subq	$32, %r12
	movq	$0, 24(%r12)
	movb	$0, (%rax)
	subq	$1, %r13
	je	.L2065
.L1524:
	movq	-16(%r12), %rbx
	movq	16(%r12), %rdi
	movq	-8(%r12), %rdx
	cmpq	%rbx, %r12
	jne	.L1517
	testq	%rdx, %rdx
	je	.L1518
	cmpq	$1, %rdx
	je	.L2066
	movq	%r12, %rsi
	movq	%r9, -2624(%rbp)
	movq	%rcx, -2600(%rbp)
	call	memcpy@PLT
	movq	-2624(%rbp), %r9
	movq	-2600(%rbp), %rcx
.L1518:
	movq	-8(%rbx), %rax
	movq	16(%rbx), %rdx
	movq	%rax, 24(%rbx)
	movb	$0, (%rdx,%rax)
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1482:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1599:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L2067
.L1601:
	leaq	-2544(%rbp), %r13
	leaq	1672(%r14), %rsi
	movq	%r13, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
.L1654:
	movq	-2616(%rbp), %xmm0
	movq	8(%rbx), %rax
	leaq	8(%rax), %r15
	movq	-2608(%rbp), %rax
	movq	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2320(%rbp)
	testq	%rax, %rax
	je	.L1694
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1695
	lock addl	$1, 8(%rax)
.L1694:
	movdqa	-2560(%rbp), %xmm6
	movq	-2552(%rbp), %rax
	movaps	%xmm6, -2288(%rbp)
	testq	%rax, %rax
	je	.L1696
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1697
	lock addl	$1, 8(%rax)
.L1696:
	movl	$360, %edi
	call	_Znwm@PLT
	subq	$8, %rsp
	movq	%r13, %r9
	movq	%r15, %rdx
	movq	%rax, %r12
	leaq	-2288(%rbp), %rax
	leaq	-2256(%rbp), %rcx
	movq	%r14, %rsi
	pushq	%rax
	leaq	-2320(%rbp), %r8
	movq	%r12, %rdi
	call	_ZN4node6worker6WorkerC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS_17PerIsolateOptionsEEOSt6vectorISD_SaISD_EESG_INS_7KVStoreEE
	movq	-2280(%rbp), %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L1699
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1700
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1701:
	cmpl	$1, %eax
	je	.L2068
.L1699:
	movq	-2312(%rbp), %r13
	testq	%r13, %r13
	je	.L1706
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1707
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L1708:
	cmpl	$1, %eax
	je	.L2069
.L1706:
	cmpl	$3, 16(%rbx)
	jle	.L2070
	movq	8(%rbx), %rax
	leaq	-24(%rax), %rdi
.L1713:
	call	_ZNK2v85Value14IsFloat64ArrayEv@PLT
	testb	%al, %al
	je	.L2071
	cmpl	$3, 16(%rbx)
	jg	.L1715
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
.L1716:
	movq	%r13, %rdi
	call	_ZN2v810TypedArray6LengthEv@PLT
	cmpq	$3, %rax
	jne	.L2072
	leaq	288(%r12), %rsi
	movl	$24, %edx
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	-2536(%rbp), %rbx
	movq	-2544(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1739
	.p2align 4,,10
	.p2align 3
.L1722:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1719
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1722
	.p2align 4,,10
	.p2align 3
.L1741:
	movq	-2544(%rbp), %r12
.L1739:
	testq	%r12, %r12
	je	.L1744
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1744:
	movq	-2552(%rbp), %r12
	testq	%r12, %r12
	je	.L1746
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1747
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L1748:
	cmpl	$1, %eax
	je	.L2073
.L1746:
	movq	-2608(%rbp), %rax
	testq	%rax, %rax
	je	.L1753
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1754
	movl	$-1, %ebx
	lock xaddl	%ebx, 8(%rax)
	movl	%ebx, %eax
.L1755:
	cmpl	$1, %eax
	je	.L2074
.L1753:
	movq	-2256(%rbp), %rdi
	cmpq	-2632(%rbp), %rdi
	je	.L1411
	call	_ZdlPv@PLT
.L1411:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2075
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1487:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1602:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %r13
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L2051:
	cmpl	$3, 43(%rax)
	jne	.L1430
	movq	1392(%r14), %r13
	movq	1384(%r14), %rsi
	testq	%r13, %r13
	je	.L1431
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L1432
	lock addl	$1, 8(%r13)
.L1431:
	movq	(%rsi), %rax
	leaq	-2288(%rbp), %rdi
	movq	%r12, %rdx
	call	*72(%rax)
	movdqa	-2288(%rbp), %xmm0
	movq	-2552(%rbp), %rdi
	pxor	%xmm1, %xmm1
	movaps	%xmm1, -2288(%rbp)
	movaps	%xmm0, -2560(%rbp)
	testq	%rdi, %rdi
	je	.L2027
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	testq	%r15, %r15
	je	.L1435
	lock subl	$1, 8(%rdi)
	je	.L1436
.L1437:
	movq	-2280(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2027
	testq	%r15, %r15
	je	.L1443
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L1444:
	cmpl	$1, %eax
	jne	.L2027
	movq	(%rdi), %rax
	movq	%rdi, -2576(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-2576(%rbp), %rdi
	je	.L1445
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1446:
	cmpl	$1, %eax
	jne	.L2027
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L2027
	.p2align 4,,10
	.p2align 3
.L2052:
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1491:
	addl	$1, 8(%rax)
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L2054:
	testq	%r15, %r15
	je	.L1609
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1494:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L1495
	.p2align 4,,10
	.p2align 3
.L2070:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1713
	.p2align 4,,10
	.p2align 3
.L1474:
	addl	$1, 8(%rax)
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1412:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	jmp	.L1414
	.p2align 4,,10
	.p2align 3
.L1477:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1478
	.p2align 4,,10
	.p2align 3
.L2061:
	movq	%r12, %rdi
	leaq	-2320(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2592(%rbp), %r8
	movq	%rax, -2224(%rbp)
	movq	%rax, %rdi
	movq	-2320(%rbp), %rax
	movq	%rax, -2208(%rbp)
.L1504:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-2320(%rbp), %rax
	movq	-2224(%rbp), %rdx
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1604:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1626:
	addl	$1, %r12d
	cmpl	%r12d, -2576(%rbp)
	jne	.L1628
	jmp	.L1611
	.p2align 4,,10
	.p2align 3
.L1616:
	testq	%r8, %r8
	jne	.L2076
	movq	%rcx, %rax
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L2055:
	leaq	-2160(%rbp), %rdi
	leaq	-2288(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -2600(%rbp)
	movq	%r8, -2592(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2592(%rbp), %r8
	movq	-2600(%rbp), %rcx
	movq	%rax, -2160(%rbp)
	movq	%rax, %rdi
	movq	-2288(%rbp), %rax
	movq	%rax, -2144(%rbp)
.L1615:
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%rcx, -2592(%rbp)
	call	memcpy@PLT
	movq	-2288(%rbp), %r8
	movq	-2160(%rbp), %rax
	movq	-2592(%rbp), %rcx
	jmp	.L1617
	.p2align 4,,10
	.p2align 3
.L1618:
	leaq	-2160(%rbp), %rdx
	leaq	-2416(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rcx, -2592(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-2592(%rbp), %rcx
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1622:
	testq	%r9, %r9
	je	.L1623
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L2056:
	movq	%r15, %rdi
	leaq	-2288(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -2624(%rbp)
	movq	%r10, -2600(%rbp)
	movq	%r9, -2592(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2592(%rbp), %r9
	movq	-2600(%rbp), %r10
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-2288(%rbp), %rax
	movq	-2624(%rbp), %rcx
	movq	%rax, 16(%r15)
.L1621:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -2592(%rbp)
	call	memcpy@PLT
	movq	-2288(%rbp), %r9
	movq	(%r15), %rdi
	movq	-2592(%rbp), %rcx
	jmp	.L1623
	.p2align 4,,10
	.p2align 3
.L1719:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1722
	jmp	.L1741
.L1784:
	movq	%rbx, -2616(%rbp)
	movq	-2592(%rbp), %rbx
	movq	%r14, -2600(%rbp)
	jmp	.L1663
	.p2align 4,,10
	.p2align 3
.L1669:
	movq	(%r14), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r10
	cmpq	%r12, %r10
	jbe	.L1574
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r12,8), %rdx
	testq	%rax, %rax
	je	.L1775
	movq	%rax, (%rdx)
	movq	(%r15), %rcx
	addq	$1, %r12
	movq	8(%r15), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%r12, %rax
	jbe	.L2077
.L1663:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	salq	$5, %rsi
	leaq	(%rcx,%rsi), %r14
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	8(%r14), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	jbe	.L1669
	movq	%rax, -2576(%rbp)
	.p2align 4,,10
	.p2align 3
.L1664:
	movq	-2576(%rbp), %rdi
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
	cmpq	-1104(%rbp), %r12
	jnb	.L1574
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r12,8), %rdx
.L1775:
	movq	$0, (%rdx)
	testq	%rsi, %rsi
	je	.L1766
	cmpq	-2584(%rbp), %rsi
	je	.L1766
	movq	%rsi, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
.L1674:
	movq	-2344(%rbp), %rbx
	movq	-2352(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1676
	.p2align 4,,10
	.p2align 3
.L1680:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1677
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1680
.L1678:
	movq	-2352(%rbp), %r12
.L1676:
	testq	%r12, %r12
	je	.L1681
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1681:
	movq	-2376(%rbp), %rbx
	movq	-2384(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1682
	.p2align 4,,10
	.p2align 3
.L1686:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1683
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1686
.L1684:
	movq	-2384(%rbp), %r12
.L1682:
	testq	%r12, %r12
	je	.L1629
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	-2408(%rbp), %rbx
	movq	-2416(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1688
	.p2align 4,,10
	.p2align 3
.L1692:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1689
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1692
.L1690:
	movq	-2416(%rbp), %r12
.L1688:
	testq	%r12, %r12
	je	.L1598
	movq	%r12, %rdi
.L2009:
	call	_ZdlPv@PLT
.L1598:
	movq	-2536(%rbp), %rbx
	movq	-2544(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1739
.L1743:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1740
.L2078:
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	je	.L1741
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L2078
.L1740:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1743
	jmp	.L1741
	.p2align 4,,10
	.p2align 3
.L1689:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1692
	jmp	.L1690
	.p2align 4,,10
	.p2align 3
.L2064:
	movq	(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rdx, 24(%r12)
	movq	%rax, 32(%r12)
.L1522:
	movq	%r12, -16(%r12)
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L2057:
	movq	(%r15), %rax
	movq	%rbx, -48(%r15)
	movq	%rdx, -40(%r15)
	movq	%rax, -32(%r15)
.L1636:
	movq	%r15, -16(%r15)
	movq	%r15, %rdi
	jmp	.L1634
.L1782:
	movq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L1630:
	movq	-32(%rax), %rdi
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, -2376(%rbp)
	cmpq	%rax, %rdi
	je	.L1638
	call	_ZdlPv@PLT
.L1638:
	movq	-2352(%rbp), %rdi
	movq	%r12, %r15
	cmpq	-2344(%rbp), %rdi
	je	.L2079
.L1768:
	movq	3280(%r14), %rax
	leaq	-2288(%rbp), %r13
	movq	%rax, %rdi
	movq	%rax, -2592(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -2576(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r15), %rcx
	movq	8(%r15), %r9
	leaq	-56(%rbp), %rdx
	leaq	-1080(%rbp), %rax
	movdqa	.LC51(%rip), %xmm0
	subq	%rcx, %r9
	movq	%rax, -2584(%rbp)
	movq	%rax, -1088(%rbp)
	sarq	$5, %r9
	movaps	%xmm0, -1104(%rbp)
	movq	%r9, %r12
	movq	%r9, %r10
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L1655:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1655
	movq	$0, -1080(%rbp)
	cmpq	$128, %r12
	jbe	.L1660
	movq	%rcx, -2616(%rbp)
	leaq	0(,%r12,8), %rdi
	movabsq	$2305843009213693951, %rax
	andq	%r12, %rax
	movq	%r10, -2600(%rbp)
	cmpq	%rax, %r12
	jne	.L1657
	testq	%rdi, %rdi
	je	.L1559
	movq	%rdi, -2624(%rbp)
	call	malloc@PLT
	movq	-2624(%rbp), %rdi
	testq	%rax, %rax
	je	.L1658
	movq	%rax, -1088(%rbp)
	movq	-2600(%rbp), %r10
	movq	%r12, -1096(%rbp)
	movq	-2616(%rbp), %rcx
.L1660:
	movq	%r10, -1104(%rbp)
	testq	%r10, %r10
	je	.L1662
	movq	-2576(%rbp), %rax
	xorl	%r12d, %r12d
	testq	%rax, %rax
	je	.L1784
	movq	%rbx, -2592(%rbp)
	movq	%r15, %rbx
	movq	%rax, %r15
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L2081:
	movq	%rax, (%rdx)
	movq	(%rbx), %rcx
	addq	$1, %r12
	movq	8(%rbx), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%r12, %rax
	jbe	.L2080
.L1667:
	movq	%r12, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movq	8(%rax), %rcx
	cmpq	$1073741798, %rcx
	ja	.L1664
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r10
	cmpq	%r10, %r12
	jnb	.L1574
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r12,8), %rdx
	testq	%rax, %rax
	jne	.L2081
	jmp	.L1775
	.p2align 4,,10
	.p2align 3
.L2053:
	movq	%r9, %rdi
	leaq	-2288(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -2592(%rbp)
	movq	%rcx, -2584(%rbp)
	movq	%r9, -2576(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-2576(%rbp), %r9
	movq	-2584(%rbp), %rcx
	movq	%rax, %rdi
	movq	-2592(%rbp), %r8
	movq	%rax, (%r9)
	movq	-2288(%rbp), %rax
	movq	%rax, 16(%r9)
.L1607:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%r9, -2584(%rbp)
	movq	%rcx, -2576(%rbp)
	call	memcpy@PLT
	movq	-2584(%rbp), %r9
	movq	-2288(%rbp), %r15
	movq	-2576(%rbp), %rcx
	movq	(%r9), %rdi
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1754:
	movq	%rax, %rbx
	movl	8(%rax), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rbx)
	jmp	.L1755
	.p2align 4,,10
	.p2align 3
.L1747:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L1748
	.p2align 4,,10
	.p2align 3
.L2065:
	movq	-2584(%rbp), %rbx
	movq	-2592(%rbp), %r13
.L1523:
	movq	-1104(%rbp), %rax
	movq	(%r15), %rdi
	movq	-1096(%rbp), %rdx
	cmpq	%rcx, %rax
	je	.L2082
	leaq	16(%r15), %rsi
	cmpq	%rsi, %rdi
	je	.L2083
	movq	%rax, (%r15)
	movq	16(%r15), %rsi
	movq	%rdx, 8(%r15)
	movq	-1088(%rbp), %rax
	movq	%rax, 16(%r15)
	testq	%rdi, %rdi
	je	.L1529
	movq	%rdi, -1104(%rbp)
	movq	%rsi, -1088(%rbp)
.L1527:
	movq	$0, -1096(%rbp)
	movb	$0, (%rdi)
	movq	-1104(%rbp), %rdi
.L1530:
	cmpq	%rcx, %rdi
	je	.L1511
	movq	%r9, -2584(%rbp)
	call	_ZdlPv@PLT
	movq	-2584(%rbp), %r9
.L1511:
	movq	-2616(%rbp), %rcx
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	-2448(%rbp), %rdx
	movq	%r13, %rdi
	movq	$0, -2432(%rbp)
	movaps	%xmm0, -2448(%rbp)
	call	_ZN4node14options_parser5ParseINS_17PerIsolateOptionsENS_7OptionsEEEvPSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISA_EESD_SD_PT_NS_20OptionEnvvarSettingsESD_@PLT
	movq	-2504(%rbp), %rax
	cmpq	%rax, -2512(%rbp)
	je	.L1537
	cmpl	$1, 16(%rbx)
	jg	.L1534
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1535:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L2084
.L1537:
	movq	-2440(%rbp), %r12
	movq	-2448(%rbp), %r13
	cmpq	%r13, %r12
	je	.L1532
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1538
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r12, %r13
	jne	.L1533
.L1539:
	movq	-2448(%rbp), %r13
.L1532:
	testq	%r13, %r13
	je	.L1541
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1541:
	movq	-2472(%rbp), %r12
	movq	-2480(%rbp), %r13
	cmpq	%r13, %r12
	je	.L1542
	.p2align 4,,10
	.p2align 3
.L1546:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1543
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r12, %r13
	jne	.L1546
.L1544:
	movq	-2480(%rbp), %r13
.L1542:
	testq	%r13, %r13
	je	.L1547
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1547:
	movq	-2504(%rbp), %r12
	movq	-2512(%rbp), %r13
	cmpq	%r13, %r12
	je	.L1548
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L1549
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r12, %r13
	jne	.L1552
.L1550:
	movq	-2512(%rbp), %r13
.L1548:
	testq	%r13, %r13
	je	.L1553
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L1553:
	movq	-2224(%rbp), %rdi
	cmpq	-2576(%rbp), %rdi
	je	.L1490
	call	_ZdlPv@PLT
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1543:
	addq	$32, %r13
	cmpq	%r13, %r12
	jne	.L1546
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1549:
	addq	$32, %r13
	cmpq	%r13, %r12
	jne	.L1552
	jmp	.L1550
	.p2align 4,,10
	.p2align 3
.L1538:
	addq	$32, %r13
	cmpq	%r13, %r12
	jne	.L1533
	jmp	.L1539
	.p2align 4,,10
	.p2align 3
.L1534:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L1535
	.p2align 4,,10
	.p2align 3
.L1715:
	movq	8(%rbx), %r13
	subq	$24, %r13
	jmp	.L1716
	.p2align 4,,10
	.p2align 3
.L2062:
	movq	-1104(%rbp), %rdx
	cmpq	%rcx, %rdx
	je	.L2085
	movq	%rdx, (%rax)
	movq	-1088(%rbp), %rdx
	movq	%rdx, 16(%rax)
.L1510:
	movq	-1096(%rbp), %rdx
	movq	%rdx, 8(%rax)
	addq	$32, -2472(%rbp)
	jmp	.L1511
	.p2align 4,,10
	.p2align 3
.L2074:
	movq	-2608(%rbp), %rbx
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1757
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L1758:
	cmpl	$1, %eax
	jne	.L1753
	movq	-2608(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1753
	.p2align 4,,10
	.p2align 3
.L2073:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1750
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L1751:
	cmpl	$1, %eax
	jne	.L1746
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L1746
	.p2align 4,,10
	.p2align 3
.L2058:
	movq	%r13, %rbx
	movq	-2592(%rbp), %r12
	movq	-2584(%rbp), %r13
	movq	-2376(%rbp), %rax
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1574:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2046:
	movq	352(%r14), %r12
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC48(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L2086
.L1417:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC49(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2087
.L1418:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2088
.L1419:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC35(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L2089
.L1420:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2090
.L1421:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L1411
	.p2align 4,,10
	.p2align 3
.L1505:
	testq	%rax, %rax
	jne	.L2091
	movq	-2576(%rbp), %rdx
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1457:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L1456
.L1458:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L2092
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1462:
	cmpl	$1, %eax
	jne	.L1459
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1465:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L2047:
	leaq	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1697:
	addl	$1, 8(%rax)
	jmp	.L1696
	.p2align 4,,10
	.p2align 3
.L1695:
	addl	$1, 8(%rax)
	jmp	.L1694
	.p2align 4,,10
	.p2align 3
.L2084:
	movq	3280(%r14), %r13
	movq	%r13, %rdi
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r12
	leaq	-2320(%rbp), %rax
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rax, -2584(%rbp)
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	-2512(%rbp), %rcx
	movq	-2504(%rbp), %r8
	leaq	-2136(%rbp), %rax
	movdqa	.LC51(%rip), %xmm0
	movq	%rax, -2592(%rbp)
	leaq	-1112(%rbp), %rdx
	subq	%rcx, %r8
	movq	%rax, -2144(%rbp)
	sarq	$5, %r8
	movaps	%xmm0, -2160(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r8, %r15
	movq	%r8, %r9
	.p2align 4,,10
	.p2align 3
.L1556:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1556
	movq	$0, -2136(%rbp)
	cmpq	$128, %r15
	jbe	.L1563
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r15,8), %rdi
	andq	%r15, %rax
	cmpq	%rax, %r15
	jne	.L1657
	movq	%rcx, -2616(%rbp)
	movq	%r9, -2600(%rbp)
	testq	%rdi, %rdi
	je	.L1559
	movq	%rdi, -2624(%rbp)
	call	malloc@PLT
	movq	-2624(%rbp), %rdi
	movq	-2600(%rbp), %r9
	testq	%rax, %rax
	movq	-2616(%rbp), %rcx
	je	.L2093
	movq	%rax, -2144(%rbp)
	movq	%r15, -2152(%rbp)
.L1563:
	movq	%r9, -2160(%rbp)
	testq	%r9, %r9
	je	.L1564
	xorl	%r15d, %r15d
	testq	%r12, %r12
	jne	.L1569
	jmp	.L1780
	.p2align 4,,10
	.p2align 3
.L2094:
	movq	%rax, (%rdx)
	movq	-2512(%rbp), %rcx
	addq	$1, %r15
	movq	-2504(%rbp), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%r15, %rax
	jbe	.L1763
.L1569:
	movq	%r15, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movq	8(%rax), %rcx
	cmpq	$1073741798, %rcx
	ja	.L1566
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-2160(%rbp), %r9
	cmpq	%r15, %r9
	jbe	.L1574
	movq	-2144(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	testq	%rax, %rax
	jne	.L2094
.L1776:
	movq	$0, (%rdx)
	testq	%rsi, %rsi
	je	.L1762
	cmpq	-2592(%rbp), %rsi
	je	.L1762
	movq	%rsi, %rdi
	call	free@PLT
	movq	-2584(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
.L1577:
	movq	-2440(%rbp), %rbx
	movq	-2448(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1579
	.p2align 4,,10
	.p2align 3
.L1583:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1580
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L1583
.L1581:
	movq	-2448(%rbp), %r12
.L1579:
	testq	%r12, %r12
	je	.L1584
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1584:
	movq	-2472(%rbp), %rbx
	movq	-2480(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1585
	.p2align 4,,10
	.p2align 3
.L1589:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1586
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1589
.L1587:
	movq	-2480(%rbp), %r12
.L1585:
	testq	%r12, %r12
	je	.L1590
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1590:
	movq	-2504(%rbp), %rbx
	movq	-2512(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L1591
	.p2align 4,,10
	.p2align 3
.L1595:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1592
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1595
.L1593:
	movq	-2512(%rbp), %r12
.L1591:
	testq	%r12, %r12
	je	.L1596
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1596:
	movq	-2224(%rbp), %rdi
	cmpq	-2576(%rbp), %rdi
	jne	.L2009
	jmp	.L1598
	.p2align 4,,10
	.p2align 3
.L1467:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1468
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1469:
	cmpl	$1, %eax
	jne	.L2007
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L2007
	.p2align 4,,10
	.p2align 3
.L1707:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1708
	.p2align 4,,10
	.p2align 3
.L1700:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L1701
.L2068:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1703
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1704:
	cmpl	$1, %eax
	jne	.L1699
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1699
.L1497:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1498
.L2069:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1710
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L1711:
	cmpl	$1, %eax
	jne	.L1706
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L1706
.L2092:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1462
.L1480:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1481
.L1468:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1469
.L1559:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1766:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	jmp	.L1674
.L1564:
	movq	-2144(%rbp), %rsi
.L1763:
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-2584(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-2144(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L1576
	cmpq	-2592(%rbp), %rdi
	je	.L1576
	call	free@PLT
.L1576:
	movq	-2584(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	testq	%r12, %r12
	je	.L1577
	movq	352(%r14), %rdi
	movl	$18, %ecx
	xorl	%edx, %edx
	leaq	.LC52(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L2095
.L1761:
	movq	8(%rbx), %rdi
	movq	3280(%r14), %rsi
	movq	%r12, %rcx
	movq	%r13, %rdx
	addq	$8, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	jmp	.L1577
	.p2align 4,,10
	.p2align 3
.L1677:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1680
	jmp	.L1678
	.p2align 4,,10
	.p2align 3
.L1683:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1686
	jmp	.L1684
	.p2align 4,,10
	.p2align 3
.L1592:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1595
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1586:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1589
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1580:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1583
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L2060:
	movq	%rax, -2592(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2592(%rbp), %rdx
	jmp	.L1500
	.p2align 4,,10
	.p2align 3
.L2066:
	movzbl	(%r12), %eax
	movb	%al, (%rdi)
	jmp	.L1518
	.p2align 4,,10
	.p2align 3
.L1757:
	movq	-2608(%rbp), %rbx
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L1758
	.p2align 4,,10
	.p2align 3
.L2079:
	movq	-2576(%rbp), %r15
	movq	-2376(%rbp), %rax
	cmpq	%rax, -2384(%rbp)
	jne	.L1768
	testq	%rdi, %rdi
	je	.L1646
	call	_ZdlPv@PLT
	movq	-2376(%rbp), %r12
	movq	-2384(%rbp), %r15
	cmpq	%r15, %r12
	je	.L1646
	.p2align 4,,10
	.p2align 3
.L1642:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1645
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r15, %r12
	jne	.L1642
.L1646:
	movq	-2384(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1644
	call	_ZdlPv@PLT
.L1644:
	movq	-2408(%rbp), %r12
	movq	-2416(%rbp), %r15
	cmpq	%r15, %r12
	je	.L1652
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L1651
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r12, %r15
	jne	.L1648
.L1652:
	movq	-2416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1654
	call	_ZdlPv@PLT
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1750:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L1751
	.p2align 4,,10
	.p2align 3
.L1507:
	movq	-2584(%rbp), %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r9, -2600(%rbp)
	movq	%rcx, -2592(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-1104(%rbp), %rdi
	movq	-2600(%rbp), %r9
	movq	-2592(%rbp), %rcx
	jmp	.L1530
	.p2align 4,,10
	.p2align 3
.L2059:
	movzbl	(%r15), %eax
	movb	%al, (%rdi)
	movq	-8(%r15), %rdx
	movq	-48(%r15), %rdi
	jmp	.L1632
.L2063:
	movdqu	-16(%rax), %xmm7
	movups	%xmm7, 16(%rax)
	jmp	.L1513
.L2082:
	testq	%rdx, %rdx
	je	.L1525
	cmpq	$1, %rdx
	je	.L2096
	movq	%rcx, %rsi
	movq	%r9, -2592(%rbp)
	movq	%rcx, -2584(%rbp)
	call	memcpy@PLT
	movq	-1096(%rbp), %rdx
	movq	(%r15), %rdi
	movq	-2592(%rbp), %r9
	movq	-2584(%rbp), %rcx
.L1525:
	movq	%rdx, 8(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-1104(%rbp), %rdi
	jmp	.L1527
.L2100:
	movq	%r12, %r15
	movq	%rax, %r12
.L1566:
	movq	%r12, %rdi
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
	cmpq	%r15, -2160(%rbp)
	jbe	.L1574
	movq	-2144(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	jmp	.L1776
.L2085:
	movdqa	-1088(%rbp), %xmm7
	movups	%xmm7, 16(%rax)
	jmp	.L1510
.L1645:
	addq	$32, %r15
	cmpq	%r15, %r12
	jne	.L1642
	jmp	.L1646
.L1651:
	addq	$32, %r15
	cmpq	%r15, %r12
	jne	.L1648
	jmp	.L1652
.L2083:
	movq	%rax, (%r15)
	movq	%rdx, 8(%r15)
	movq	-1088(%rbp), %rax
	movq	%rax, 16(%r15)
.L1529:
	movq	%rcx, -1104(%rbp)
	leaq	-1088(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L1527
.L2071:
	leaq	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2072:
	leaq	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1432:
	addl	$1, 8(%r13)
	jmp	.L1431
.L1710:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1711
.L1703:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L1704
.L2080:
	movq	-2592(%rbp), %rbx
.L1767:
	movq	-2576(%rbp), %rdi
	movq	%r10, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-1088(%rbp), %rdi
	movq	%rax, %r12
	cmpq	-2584(%rbp), %rdi
	je	.L1673
	testq	%rdi, %rdi
	je	.L1673
	call	free@PLT
.L1673:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	testq	%r12, %r12
	je	.L1674
	movq	352(%r14), %rdi
	xorl	%edx, %edx
	movl	$15, %ecx
	leaq	.LC53(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L2097
.L1765:
	movq	8(%rbx), %rdi
	movq	3280(%r14), %rsi
	movq	%r12, %rcx
	addq	$8, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	jmp	.L1674
.L1443:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L1444
.L1435:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	cmpl	$1, %eax
	jne	.L2027
.L1436:
	movq	(%rdi), %rax
	movq	%rdi, -2576(%rbp)
	call	*16(%rax)
	testq	%r15, %r15
	movq	-2576(%rbp), %rdi
	je	.L2098
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L1441:
	cmpl	$1, %eax
	jne	.L1437
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L1437
.L1657:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1658:
	movq	%rdi, -2600(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2600(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1559
	movq	(%r15), %rcx
	movq	8(%r15), %r10
	movq	%rdi, -1088(%rbp)
	movq	-1104(%rbp), %rax
	movq	%r12, -1096(%rbp)
	subq	%rcx, %r10
	sarq	$5, %r10
	movq	%r10, %r8
	testq	%rax, %rax
	je	.L1659
	movq	-2584(%rbp), %rsi
	leaq	0(,%rax,8), %rdx
	movq	%rcx, -2600(%rbp)
	movq	%r10, -2616(%rbp)
	call	memcpy@PLT
	movq	-2616(%rbp), %r8
	movq	-2600(%rbp), %rcx
.L1659:
	cmpq	%r12, %r8
	jbe	.L1783
	movq	%r12, -1104(%rbp)
.L1661:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2086:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1417
.L2090:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1421
.L2089:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1420
.L2088:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1419
.L2087:
	movq	%rax, -2576(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2576(%rbp), %rdi
	jmp	.L1418
.L1780:
	movq	%r12, -2600(%rbp)
	movq	%r15, %r12
	movq	%rbx, %r15
	jmp	.L1565
.L1991:
	movq	%rax, (%rdx)
	movq	-2512(%rbp), %rcx
	addq	$1, %r12
	movq	-2504(%rbp), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%r12, %rax
	jbe	.L2099
.L1565:
	movq	%r12, %rbx
	movq	%r13, %rdi
	salq	$5, %rbx
	addq	%rcx, %rbx
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	8(%rbx), %rcx
	movq	%rax, %rdi
	cmpq	$1073741798, %rcx
	ja	.L2100
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-2160(%rbp), %r9
	cmpq	%r12, %r9
	jbe	.L1574
	movq	-2144(%rbp), %rsi
	leaq	(%rsi,%r12,8), %rdx
	testq	%rax, %rax
	jne	.L1991
	jmp	.L1776
.L2096:
	movzbl	-1088(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1096(%rbp), %rdx
	movq	(%r15), %rdi
	jmp	.L1525
.L2098:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1441
.L1445:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L1446
.L1762:
	movq	-2584(%rbp), %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	jmp	.L1577
.L2093:
	movq	%rdi, -2600(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-2600(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1559
	movq	-2512(%rbp), %rcx
	movq	-2504(%rbp), %r9
	movq	%rdi, -2144(%rbp)
	movq	-2160(%rbp), %rax
	movq	%r15, -2152(%rbp)
	subq	%rcx, %r9
	sarq	$5, %r9
	movq	%r9, %r8
	testq	%rax, %rax
	je	.L1562
	movq	-2592(%rbp), %rsi
	leaq	0(,%rax,8), %rdx
	movq	%rcx, -2600(%rbp)
	movq	%r9, -2616(%rbp)
	call	memcpy@PLT
	movq	-2616(%rbp), %r8
	movq	-2600(%rbp), %rcx
.L1562:
	cmpq	%r15, %r8
	jbe	.L1779
	movq	%r15, -2160(%rbp)
	jmp	.L1661
.L1662:
	movq	-1088(%rbp), %rsi
	jmp	.L1767
.L2077:
	movq	-2600(%rbp), %r14
	movq	-2616(%rbp), %rbx
	jmp	.L1767
.L2097:
	movq	%rdx, -2576(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2576(%rbp), %rdx
	jmp	.L1765
.L2099:
	movq	-2600(%rbp), %r12
	movq	%r15, %rbx
	jmp	.L1763
.L2095:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1761
.L2049:
	leaq	.LC33(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2075:
	call	__stack_chk_fail@PLT
.L2091:
	movq	-2576(%rbp), %rdi
	jmp	.L1504
.L1779:
	movq	%r8, %r9
	jmp	.L1563
.L1783:
	movq	%r8, %r10
	jmp	.L1660
.L2076:
	movq	%rcx, %rdi
	jmp	.L1615
	.cfi_endproc
.LFE8042:
	.size	_ZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata._ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC54:
	.string	"%d"
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11836:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2102
	leaq	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2102:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC36(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2103:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2103
	cmpb	$120, %dl
	jg	.L2104
	cmpb	$99, %dl
	jg	.L2105
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L2106
	cmpb	$88, %dl
	je	.L2107
	jmp	.L2104
.L2105:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2104
	leaq	.L2109(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2109:
	.long	.L2110-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2110-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2112-.L2109
	.long	.L2111-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2110-.L2109
	.long	.L2104-.L2109
	.long	.L2110-.L2109
	.long	.L2104-.L2109
	.long	.L2104-.L2109
	.long	.L2108-.L2109
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2106:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2113
	call	_ZdlPv@PLT
.L2113:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2133
	jmp	.L2115
.L2104:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2138
	call	_ZdlPv@PLT
	jmp	.L2138
.L2110:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC54(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L2135
.L2112:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2120:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2120
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2135
.L2108:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2135:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2132
	jmp	.L2119
.L2107:
	movl	(%r8), %esi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2123
	call	_ZdlPv@PLT
.L2123:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2119
.L2132:
	call	_ZdlPv@PLT
	jmp	.L2119
.L2111:
	leaq	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2119:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2138:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2115
.L2133:
	call	_ZdlPv@PLT
.L2115:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2101
	call	_ZdlPv@PLT
.L2101:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2127
	call	__stack_chk_fail@PLT
.L2127:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11836:
	.size	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2140
	leaq	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2140:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC36(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2141:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2141
	cmpb	$120, %dl
	jg	.L2142
	cmpb	$99, %dl
	jg	.L2143
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L2144
	cmpb	$88, %dl
	je	.L2145
	jmp	.L2142
.L2143:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2142
	leaq	.L2147(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2147:
	.long	.L2148-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2148-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2150-.L2147
	.long	.L2149-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2148-.L2147
	.long	.L2142-.L2147
	.long	.L2148-.L2147
	.long	.L2142-.L2147
	.long	.L2142-.L2147
	.long	.L2146-.L2147
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2144:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2151
	call	_ZdlPv@PLT
.L2151:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2171
	jmp	.L2153
.L2142:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2176
	call	_ZdlPv@PLT
	jmp	.L2176
.L2148:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC37(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L2173
.L2150:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2158:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2158
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2173
.L2146:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2173:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2170
	jmp	.L2157
.L2145:
	movq	(%r8), %rsi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2161
	call	_ZdlPv@PLT
.L2161:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2157
.L2170:
	call	_ZdlPv@PLT
	jmp	.L2157
.L2149:
	leaq	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2157:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2176:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2153
.L2171:
	call	_ZdlPv@PLT
.L2153:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2139
	call	_ZdlPv@PLT
.L2139:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2165
	call	__stack_chk_fail@PLT
.L2165:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11738:
	.size	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11494:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2178
	call	__stack_chk_fail@PLT
.L2178:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11494:
	.size	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_:
.LFB11233:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2180
	call	_ZdlPv@PLT
.L2180:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2182
	call	__stack_chk_fail@PLT
.L2182:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11233:
	.size	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB9764:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r15, %rdi
	call	*72(%rax)
	leaq	-160(%rbp), %r8
	movq	%r15, %rsi
	leaq	.LC38(%rip), %rdx
	movq	%r8, %rdi
	movq	%r8, -200(%rbp)
	leaq	-128(%rbp), %r15
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	-200(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC39(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2185
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_
.L2185:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2186
	call	_ZdlPv@PLT
.L2186:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2187
	call	_ZdlPv@PLT
.L2187:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2188
	call	_ZdlPv@PLT
.L2188:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2184
	call	_ZdlPv@PLT
.L2184:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2190
	call	__stack_chk_fail@PLT
.L2190:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9764:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.1
.LC55:
	.string	"Worker %llu called Exit(%d)"
	.section	.text.unlikely
	.align 2
.LCOLDB56:
	.text
.LHOTB56:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker4ExitEi
	.type	_ZN4node6worker6Worker4ExitEi, @function
_ZN4node6worker6Worker4ExitEi:
.LFB8075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	176(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$16, %rsp
	movl	%esi, -20(%rbp)
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2196
.L2193:
	movq	352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2194
	movl	-20(%rbp), %eax
	movl	%eax, 264(%rbx)
	call	_ZN4node4StopEPNS_11EnvironmentE@PLT
.L2195:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2194:
	.cfi_restore_state
	movb	$1, 344(%rbx)
	jmp	.L2195
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker6Worker4ExitEi.cold, @function
_ZN4node6worker6Worker4ExitEi.cold:
.LFSB8075:
.L2196:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	-20(%rbp), %rcx
	leaq	272(%rbx), %rdx
	movq	%rbx, %rdi
	leaq	.LC55(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2193
	.cfi_endproc
.LFE8075:
	.text
	.size	_ZN4node6worker6Worker4ExitEi, .-_ZN4node6worker6Worker4ExitEi
	.section	.text.unlikely
	.size	_ZN4node6worker6Worker4ExitEi.cold, .-_ZN4node6worker6Worker4ExitEi.cold
.LCOLDE56:
	.text
.LHOTE56:
	.section	.rodata.str1.1
.LC57:
	.string	"__metadata"
.LC58:
	.string	"name"
.LC59:
	.string	"thread_name"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"Creating isolate for worker with id %llu"
	.section	.rodata.str1.1
.LC61:
	.string	"ERR_WORKER_OUT_OF_MEMORY"
.LC62:
	.string	"Failed to create new Isolate"
.LC63:
	.string	"Starting worker with id %llu"
.LC64:
	.string	"Failed to create new Context"
	.section	.rodata.str1.8
	.align 8
.LC65:
	.string	"Created Environment for worker with id %llu"
	.align 8
.LC66:
	.string	"Created message port for worker %llu"
	.section	.rodata.str1.1
.LC67:
	.string	"internal/main/worker_thread"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"Loaded environment for worker %llu"
	.align 8
.LC69:
	.string	"Exiting thread for worker %llu with exit code %d"
	.section	.rodata.str1.1
.LC70:
	.string	"Worker %llu thread stops"
.LC71:
	.string	"Worker %llu dispose isolate"
	.section	.text.unlikely
	.align 2
.LCOLDB72:
	.text
.LHOTB72:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker3RunEv
	.type	_ZN4node6worker6Worker3RunEv, @function
_ZN4node6worker6Worker3RunEv:
.LFB8006:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC37(%rip), %rcx
	movl	$32, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-1104(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-1072(%rbp), %rbx
	subq	$1336, %rsp
	movq	272(%rdi), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-1088(%rbp), %rax
	movb	$32, -1076(%rbp)
	movq	%rax, -1320(%rbp)
	movq	%rax, -1104(%rbp)
	movabsq	$7517759457908125527, %rax
	movq	%rax, -1088(%rbp)
	xorl	%eax, %eax
	movl	$1684104562, -1080(%rbp)
	movq	$13, -1096(%rbp)
	movb	$0, -1075(%rbp)
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	-1064(%rbp), %rdx
	movq	-1072(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-1072(%rbp), %rdi
	leaq	-1056(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2199
	call	_ZdlPv@PLT
.L2199:
	movq	_ZZN4node6worker6Worker3RunEvE28trace_event_unique_atomic256(%rip), %r13
	testq	%r13, %r13
	je	.L2381
.L2201:
	testb	$5, 0(%r13)
	jne	.L2382
.L2203:
	cmpq	$0, 120(%r12)
	je	.L2383
.L2206:
	leaq	272(%r12), %rax
	movq	16(%r12), %rdx
	movq	%rax, -1352(%rbp)
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2362
.L2207:
	leaq	-1064(%rbp), %r13
	movq	%r12, -1072(%rbp)
	movq	%r13, %rdi
	movb	$1, -216(%rbp)
	movq	$0, -208(%rbp)
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L2384
	leaq	-1208(%rbp), %r15
	leaq	-1216(%rbp), %r14
	movb	$0, -216(%rbp)
	movq	$0, -1216(%rbp)
	movq	%r15, %rdi
	call	_ZN2v819ResourceConstraintsC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %esi
	movq	%r14, %rdi
	movups	%xmm0, -1160(%rbp)
	movaps	%xmm0, -1136(%rbp)
	movw	%si, -1120(%rbp)
	movq	$0, -1168(%rbp)
	movq	$0, -1144(%rbp)
	call	_ZN4node29SetIsolateCreateParamsForNodeEPN2v87Isolate12CreateParamsE@PLT
	movq	128(%r12), %rax
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, -1136(%rbp)
	call	_ZN4node6worker6Worker25UpdateResourceConstraintsEPN2v819ResourceConstraintsE
	call	_ZN2v87Isolate8AllocateEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L2385
	movq	120(%r12), %rdi
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	(%rdi), %rax
	call	*192(%rax)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_ZN2v87Isolate10InitializeEPS0_RKNS0_12CreateParamsE@PLT
	movq	%rbx, %rdi
	call	_ZN4node19SetIsolateUpForNodeEPN2v87IsolateE@PLT
	movq	%r12, %rdx
	leaq	_ZN4node6worker6Worker13NearHeapLimitEPvmm(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v87Isolate24AddNearHeapLimitCallbackEPFmPvmmES1_@PLT
	leaq	-1280(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1304(%rbp)
	call	_ZN2v86Locker10InitializeEPNS_7IsolateE@PLT
	movq	%rbx, %rdi
	call	_ZN2v87Isolate5EnterEv@PLT
	movq	280(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN2v87Isolate13SetStackLimitEm@PLT
	leaq	-1248(%rbp), %rax
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, -1312(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-1072(%rbp), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	128(%r12), %rcx
	movq	120(%rax), %rdx
	call	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE@PLT
	movq	-208(%rbp), %rdi
	movq	%rax, -208(%rbp)
	testq	%rdi, %rdi
	je	.L2213
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
	movq	-208(%rbp), %rax
.L2213:
	testq	%rax, %rax
	je	.L2386
	movq	-1072(%rbp), %rdx
	movq	56(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L2216
	pxor	%xmm0, %xmm0
	movq	64(%rdx), %rsi
	movups	%xmm0, 56(%rdx)
	movq	2408(%rax), %r14
	movq	%rcx, 2400(%rax)
	movq	%rsi, 2408(%rax)
	testq	%r14, %r14
	je	.L2216
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L2218
	movl	$-1, %edx
	lock xaddl	%edx, 8(%r14)
.L2219:
	cmpl	$1, %edx
	jne	.L2216
	movq	(%r14), %rdx
	movq	%rax, -1328(%rbp)
	movq	%r14, %rdi
	call	*16(%rdx)
	movq	-1328(%rbp), %rax
	testq	%rax, %rax
	je	.L2220
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L2221:
	cmpl	$1, %eax
	jne	.L2216
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2216:
	movq	-1312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%rbx, %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-1304(%rbp), %rdi
	call	_ZN2v86LockerD1Ev@PLT
	movq	-1072(%rbp), %rax
	leaq	176(%rax), %r14
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	-1072(%rbp), %rax
	movq	%r14, %rdi
	movq	%rbx, 144(%rax)
	call	uv_mutex_unlock@PLT
.L2222:
	movq	144(%r12), %rsi
	testq	%rsi, %rsi
	je	.L2223
	cmpb	$0, -216(%rbp)
	jne	.L2387
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2363
.L2225:
	leaq	-1296(%rbp), %rax
	leaq	176(%r12), %r14
	movq	%rax, %rdi
	movq	%rax, -1344(%rbp)
	call	_ZN2v86Locker10InitializeEPNS_7IsolateE@PLT
	movq	144(%r12), %rax
	movq	%rax, %rdi
	movq	%rax, -1336(%rbp)
	call	_ZN2v87Isolate5EnterEv@PLT
	leaq	-1280(%rbp), %rax
	movq	144(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -1304(%rbp)
	call	_ZN2v815SealHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2226
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2227:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	je	.L2388
	movq	144(%r12), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
.L2286:
	movq	-1304(%rbp), %rdi
	call	_ZN2v815SealHandleScopeD1Ev@PLT
	movq	-1336(%rbp), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-1344(%rbp), %rdi
	call	_ZN2v86LockerD1Ev@PLT
.L2223:
	movq	-1072(%rbp), %r12
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2370
.L2300:
	addq	$176, %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	-1072(%rbp), %rax
	movq	%r12, %rdi
	movq	144(%rax), %r14
	movq	$0, 144(%rax)
	call	uv_mutex_unlock@PLT
	testq	%r14, %r14
	je	.L2301
	cmpb	$0, -216(%rbp)
	jne	.L2302
	movq	-208(%rbp), %rdi
	movb	$0, -1216(%rbp)
	movq	$0, -208(%rbp)
	testq	%rdi, %rdi
	je	.L2303
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
.L2303:
	movq	-1072(%rbp), %rax
	leaq	-1216(%rbp), %rcx
	leaq	_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_(%rip), %rdx
	movq	%r14, %rsi
	movq	120(%rax), %rdi
	movq	(%rdi), %rax
	call	*208(%rax)
	movq	-1072(%rbp), %rax
	movq	%r14, %rsi
	movq	120(%rax), %rdi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	%r14, %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	cmpb	$0, -1216(%rbp)
	jne	.L2301
	.p2align 4,,10
	.p2align 3
.L2304:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -1216(%rbp)
	je	.L2304
.L2301:
	cmpb	$0, -216(%rbp)
	je	.L2389
.L2306:
	movq	-208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2307
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
.L2307:
	movq	-1104(%rbp), %rdi
	cmpq	-1320(%rbp), %rdi
	je	.L2198
	call	_ZdlPv@PLT
.L2198:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2390
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2382:
	.cfi_restore_state
	leaq	.LC58(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$7, -1280(%rbp)
	movq	%rax, -1248(%rbp)
	movq	-1104(%rbp), %rax
	movaps	%xmm0, -1072(%rbp)
	movq	%rax, -1216(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	subq	$8, %rsp
	movl	$1, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	leaq	-1216(%rbp), %rax
	pushq	$0
	leaq	-1280(%rbp), %r9
	pushq	%rbx
	leaq	-1248(%rbp), %r8
	leaq	.LC59(%rip), %rdx
	pushq	%rax
	call	_ZN4node7tracing17TracingController16AddMetadataEventEPKhPKciPS5_S3_PKmPSt10unique_ptrIN2v824ConvertableToTraceFormatESt14default_deleteISB_EEj@PLT
	movq	-1064(%rbp), %rdi
	addq	$32, %rsp
	testq	%rdi, %rdi
	je	.L2204
	movq	(%rdi), %rax
	call	*8(%rax)
.L2204:
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2203
	movq	(%rdi), %rax
	call	*8(%rax)
	cmpq	$0, 120(%r12)
	jne	.L2206
.L2383:
	leaq	_ZZN4node6worker6Worker3RunEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2381:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2202
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2391
.L2202:
	movq	%r13, _ZZN4node6worker6Worker3RunEvE28trace_event_unique_atomic256(%rip)
	mfence
	jmp	.L2201
	.p2align 4,,10
	.p2align 3
.L2384:
	leaq	-192(%rbp), %r14
	movl	$128, %edx
	movl	%eax, %edi
	movq	%r14, %rsi
	call	uv_err_name_r@PLT
	leaq	.LC34(%rip), %rax
	movq	%r14, %r8
	movq	%rax, 224(%r12)
.L2209:
	movl	(%r8), %edx
	addq	$4, %r8
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L2209
	movl	%eax, %edx
	leaq	232(%r12), %rdi
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r8), %rdx
	cmove	%rdx, %r8
	movq	240(%r12), %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	movq	%r14, %rcx
	sbbq	$3, %r8
	xorl	%esi, %esi
	subq	%r14, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movb	$1, 344(%r12)
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2388:
	leaq	-1248(%rbp), %rax
	movq	144(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -1312(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-1216(%rbp), %rax
	movq	144(%r12), %rsi
	movq	%rax, %rdi
	movq	%rax, -1328(%rbp)
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movq	144(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE@PLT
	movq	%rax, -1360(%rbp)
	testq	%rax, %rax
	je	.L2392
	movq	-1328(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2231
	movq	%r14, %rdi
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	je	.L2233
.L2393:
	movq	-1312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	144(%r12), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2226:
	movzbl	344(%r12), %ebx
	jmp	.L2227
	.p2align 4,,10
	.p2align 3
.L2389:
	movq	%r13, %rdi
	call	_ZN4node18CheckedUvLoopCloseEP9uv_loop_s@PLT
	jmp	.L2306
	.p2align 4,,10
	.p2align 3
.L2231:
	movzbl	344(%r12), %ebx
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	jne	.L2393
.L2233:
	movq	-1360(%rbp), %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movl	$3288, %edi
	movq	-208(%rbp), %rbx
	call	_Znwm@PLT
	subq	$8, %rsp
	leaq	96(%r12), %rcx
	xorl	%r9d, %r9d
	movq	-1360(%rbp), %rdx
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%rbx, %rsi
	pushq	272(%r12)
	leaq	72(%r12), %r8
	call	_ZN4node11EnvironmentC1EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm@PLT
	movq	1392(%r15), %rdi
	pxor	%xmm0, %xmm0
	movq	320(%r12), %rax
	movq	328(%r12), %rbx
	movups	%xmm0, 320(%r12)
	movq	%rax, 1384(%r15)
	popq	%rdx
	popq	%rcx
	cmpq	%rdi, %rbx
	je	.L2234
	testq	%rbx, %rbx
	je	.L2235
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L2236
	lock addl	$1, 8(%rbx)
	movq	1392(%r15), %rdi
.L2235:
	testq	%rdi, %rdi
	je	.L2238
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L2239
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
.L2240:
	cmpl	$1, %edx
	jne	.L2238
	movq	(%rdi), %rdx
	movq	%rax, -1376(%rbp)
	movq	%rdi, -1368(%rbp)
	call	*16(%rdx)
	movq	-1376(%rbp), %rax
	movq	-1368(%rbp), %rdi
	testq	%rax, %rax
	je	.L2242
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L2243:
	cmpl	$1, %eax
	jne	.L2238
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2238:
	movq	%rbx, 1392(%r15)
.L2234:
	testq	%rbx, %rbx
	je	.L2245
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L2246
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rbx)
.L2247:
	cmpl	$1, %edx
	jne	.L2245
	movq	(%rbx), %rdx
	movq	%rax, -1368(%rbp)
	movq	%rbx, %rdi
	call	*16(%rdx)
	movq	-1368(%rbp), %rax
	testq	%rax, %rax
	je	.L2249
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L2250:
	cmpl	$1, %eax
	jne	.L2245
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L2245:
	movq	1640(%r15), %rax
	cmpq	$0, 2368(%r15)
	movb	$0, 8(%rax)
	jne	.L2394
	movq	%r12, 2368(%r15)
	movq	%r15, %rdi
	movzbl	152(%r12), %esi
	call	_ZN4node11Environment15InitializeLibuvEb@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	cmpb	$0, 344(%r12)
	jne	.L2395
	movq	%r15, 352(%r12)
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2364
.L2254:
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2255
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2256:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	jne	.L2253
	movq	%r15, %rdi
	call	_ZN4node11Environment21InitializeDiagnosticsEv@PLT
	movq	168(%r12), %rax
	movq	-1328(%rbp), %rsi
	movq	%r15, %rdi
	movq	$0, 168(%r12)
	movq	%rax, -1216(%rbp)
	call	_ZN4node11Environment19InitializeInspectorESt10unique_ptrINS_9inspector21ParentInspectorHandleESt14default_deleteIS3_EE@PLT
	movq	-1216(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L2258
	movq	%rbx, %rdi
	call	_ZN4node9inspector21ParentInspectorHandleD1Ev@PLT
	movl	$64, %esi
	movq	%rbx, %rdi
	call	_ZdlPvm@PLT
.L2258:
	movq	-1328(%rbp), %rdi
	movq	144(%r12), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN4node11Environment16RunBootstrappingEv@PLT
	testq	%rax, %rax
	je	.L2259
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node6worker6Worker20CreateEnvMessagePortEPNS_11EnvironmentE
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2260
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2261:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	jne	.L2396
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2365
.L2263:
	leaq	.LC67(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node14StartExecutionEPNS_11EnvironmentEPKc@PLT
.L2259:
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2366
.L2264:
	movq	-1328(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2265
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2266:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	jne	.L2253
	movq	144(%r12), %rsi
	movq	-1328(%rbp), %rdi
	call	_ZN2v815SealHandleScopeC1EPNS_7IsolateE@PLT
	movq	1864(%r15), %rbx
	call	uv_hrtime@PLT
	movl	$3, %esi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	call	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	.p2align 4,,10
	.p2align 3
.L2282:
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2267
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2268:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	jne	.L2274
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2272
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2273:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	jne	.L2274
	movq	120(%r12), %rdi
	movq	144(%r12), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	%r13, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	je	.L2279
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2277
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2278:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	je	.L2276
.L2279:
	movq	%r15, %rdi
	call	_ZN4node14EmitBeforeExitEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	call	uv_loop_alive@PLT
	testl	%eax, %eax
	je	.L2274
.L2276:
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2280
	movq	%r14, %rdi
	movzbl	2664(%rax), %ebx
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	je	.L2282
	.p2align 4,,10
	.p2align 3
.L2274:
	movq	1864(%r15), %rbx
	call	uv_hrtime@PLT
	movl	$4, %esi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	call	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm@PLT
	movq	-1328(%rbp), %rdi
	call	_ZN2v815SealHandleScopeD1Ev@PLT
	movq	-1360(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-1312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	352(%r12), %rax
	testq	%rax, %rax
	je	.L2397
	movzbl	2664(%rax), %eax
	testb	%al, %al
	setne	%bl
.L2283:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	je	.L2398
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
.L2287:
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2367
.L2288:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	144(%r12), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	movl	$1, %edx
	movb	$0, 1930(%r15)
	mfence
	movq	144(%r12), %rsi
	movq	-1328(%rbp), %rdi
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeC1EPS0_NS1_9OnFailureE@PLT
	movq	3280(%r15), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	%r14, %rdi
	movb	$1, 344(%r12)
	movq	$0, 352(%r12)
	call	uv_mutex_unlock@PLT
	movq	%r15, %rdi
	movb	$1, 2664(%r15)
	mfence
	call	_ZN4node11Environment24stop_sub_worker_contextsEv@PLT
	movq	%r15, %rdi
	call	_ZN4node11Environment10RunCleanupEv@PLT
	movq	%r15, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movq	120(%r12), %rdi
	movq	144(%r12), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-1328(%rbp), %rdi
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	movq	-1304(%rbp), %rdi
	call	_ZN2v815SealHandleScopeD1Ev@PLT
	movq	-1336(%rbp), %rdi
	call	_ZN2v87Isolate4ExitEv@PLT
	movq	-1344(%rbp), %rdi
	call	_ZN2v86LockerD1Ev@PLT
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2368
.L2289:
	movq	-1072(%rbp), %rdi
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2369
.L2290:
	leaq	176(%rdi), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	-1072(%rbp), %rax
	movq	%r12, %rdi
	movq	144(%rax), %r14
	movq	$0, 144(%rax)
	call	uv_mutex_unlock@PLT
	testq	%r14, %r14
	je	.L2301
	cmpb	$0, -216(%rbp)
	jne	.L2302
	movq	-208(%rbp), %rdi
	movb	$0, -1216(%rbp)
	movq	$0, -208(%rbp)
	testq	%rdi, %rdi
	je	.L2293
	call	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE@PLT
.L2293:
	movq	-1072(%rbp), %rax
	movq	-1328(%rbp), %rcx
	leaq	_ZZN4node6worker16WorkerThreadDataD4EvENUlPvE_4_FUNES2_(%rip), %rdx
	movq	%r14, %rsi
	movq	120(%rax), %rdi
	movq	(%rdi), %rax
	call	*208(%rax)
	movq	-1072(%rbp), %rax
	movq	%r14, %rsi
	movq	120(%rax), %rdi
	movq	(%rdi), %rax
	call	*200(%rax)
	movq	%r14, %rdi
	call	_ZN2v87Isolate7DisposeEv@PLT
	cmpb	$0, -1216(%rbp)
	jne	.L2301
	.p2align 4,,10
	.p2align 3
.L2294:
	movl	$1, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	cmpb	$0, -1216(%rbp)
	je	.L2294
	jmp	.L2301
	.p2align 4,,10
	.p2align 3
.L2386:
	leaq	_ZZN4node6worker16WorkerThreadDataC4EPNS0_6WorkerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2218:
	movl	8(%r14), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r14)
	jmp	.L2219
	.p2align 4,,10
	.p2align 3
.L2387:
	leaq	_ZZN4node6worker6Worker3RunEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2391:
	leaq	.LC57(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L2202
	.p2align 4,,10
	.p2align 3
.L2385:
	leaq	.LC61(%rip), %rax
	movq	240(%r12), %rdx
	xorl	%esi, %esi
	leaq	232(%r12), %rdi
	movq	%rax, 224(%r12)
	movl	$28, %r8d
	leaq	.LC62(%rip), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movb	$1, 344(%r12)
	jmp	.L2222
	.p2align 4,,10
	.p2align 3
.L2392:
	leaq	.LC61(%rip), %rax
	movl	$28, %r8d
	leaq	.LC64(%rip), %rcx
	xorl	%esi, %esi
	movq	%rax, 224(%r12)
	movq	240(%r12), %rdx
	leaq	232(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1328(%rbp), %rdi
	call	_ZN2v88TryCatchD1Ev@PLT
	movq	-1312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	144(%r12), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2302:
	leaq	_ZZN4node6worker16WorkerThreadDataD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2255:
	movzbl	344(%r12), %ebx
	jmp	.L2256
	.p2align 4,,10
	.p2align 3
.L2220:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L2221
	.p2align 4,,10
	.p2align 3
.L2246:
	movl	8(%rbx), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rbx)
	jmp	.L2247
	.p2align 4,,10
	.p2align 3
.L2236:
	addl	$1, 8(%rbx)
	jmp	.L2235
	.p2align 4,,10
	.p2align 3
.L2395:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
.L2253:
	movq	-1360(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-1312(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	144(%r12), %rdi
	call	_ZN2v87Isolate24CancelTerminateExecutionEv@PLT
	movl	$1, %edx
	movb	$0, 1930(%r15)
	mfence
	movq	144(%r12), %rsi
	movq	-1328(%rbp), %rdi
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeC1EPS0_NS1_9OnFailureE@PLT
	movq	3280(%r15), %rbx
	movq	%rbx, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	%r14, %rdi
	movb	$1, 344(%r12)
	movq	$0, 352(%r12)
	call	uv_mutex_unlock@PLT
	movq	%r15, %rdi
	movb	$1, 2664(%r15)
	mfence
	call	_ZN4node11Environment24stop_sub_worker_contextsEv@PLT
	movq	%r15, %rdi
	call	_ZN4node11Environment10RunCleanupEv@PLT
	movq	%r15, %rdi
	call	_ZN4node9RunAtExitEPNS_11EnvironmentE@PLT
	movq	120(%r12), %rdi
	movq	144(%r12), %rsi
	movq	(%rdi), %rax
	call	*176(%rax)
	movq	%rbx, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-1328(%rbp), %rdi
	call	_ZN2v87Isolate32DisallowJavascriptExecutionScopeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE@PLT
	jmp	.L2286
	.p2align 4,,10
	.p2align 3
.L2267:
	movzbl	344(%r12), %ebx
	jmp	.L2268
	.p2align 4,,10
	.p2align 3
.L2272:
	movzbl	344(%r12), %ebx
	jmp	.L2273
	.p2align 4,,10
	.p2align 3
.L2280:
	movzbl	344(%r12), %ebx
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testb	%bl, %bl
	je	.L2282
	jmp	.L2274
	.p2align 4,,10
	.p2align 3
.L2277:
	movzbl	344(%r12), %ebx
	jmp	.L2278
	.p2align 4,,10
	.p2align 3
.L2239:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	jmp	.L2240
	.p2align 4,,10
	.p2align 3
.L2265:
	movzbl	344(%r12), %ebx
	jmp	.L2266
	.p2align 4,,10
	.p2align 3
.L2394:
	leaq	_ZZN4node11Environment18set_worker_contextEPNS_6worker6WorkerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2249:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L2250
.L2260:
	movzbl	344(%r12), %ebx
	jmp	.L2261
.L2242:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L2243
.L2397:
	movzbl	344(%r12), %ebx
	jmp	.L2283
.L2398:
	movq	%r15, %rdi
	call	_ZN4node8EmitExitEPNS_11EnvironmentE@PLT
	movq	%r14, %rdi
	movl	%eax, %ebx
	call	uv_mutex_lock@PLT
	movl	264(%r12), %eax
	testl	%eax, %eax
	jne	.L2287
	movl	%ebx, 264(%r12)
	jmp	.L2287
.L2396:
	movq	-1328(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2253
.L2390:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker6Worker3RunEv.cold, @function
_ZN4node6worker6Worker3RunEv.cold:
.LFSB8006:
.L2362:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	272(%r12), %rdx
	leaq	.LC60(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2207
.L2368:
	movq	-1352(%rbp), %rdx
	leaq	.LC70(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2289
.L2367:
	movq	-1352(%rbp), %rdx
	leaq	264(%r12), %rcx
	leaq	.LC69(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2288
.L2370:
	leaq	272(%r12), %rdx
	movq	%r12, %rdi
	leaq	.LC71(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-1072(%rbp), %r12
	jmp	.L2300
.L2363:
	movq	-1352(%rbp), %rdx
	leaq	.LC63(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	144(%r12), %rsi
	jmp	.L2225
.L2366:
	movq	-1352(%rbp), %rdx
	leaq	.LC68(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2264
.L2364:
	movq	-1352(%rbp), %rdx
	leaq	.LC65(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2254
.L2369:
	leaq	272(%rdi), %rdx
	leaq	.LC71(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-1072(%rbp), %rdi
	jmp	.L2290
.L2365:
	movq	-1352(%rbp), %rdx
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2263
	.cfi_endproc
.LFE8006:
	.text
	.size	_ZN4node6worker6Worker3RunEv, .-_ZN4node6worker6Worker3RunEv
	.section	.text.unlikely
	.size	_ZN4node6worker6Worker3RunEv.cold, .-_ZN4node6worker6Worker3RunEv.cold
.LCOLDE72:
	.text
.LHOTE72:
	.p2align 4
	.type	_ZZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlPvE_4_FUNES8_, @function
_ZZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlPvE_4_FUNES8_:
.LFB8068:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	176(%r14), %r13
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	-3997744(%rbp), %rax
	movq	%rdi, -48(%rbp)
	movq	%rax, 280(%rdi)
	call	_ZN4node6worker6Worker3RunEv
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movl	$32, %edi
	movq	16(%r14), %rbx
	call	_Znwm@PLT
	movb	$1, 8(%rax)
	movq	%rax, %r12
	movq	$0, 16(%rax)
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_EE(%rip), %rax
	movq	%r14, 24(%r12)
	leaq	2488(%rbx), %r14
	movq	%r14, %rdi
	movq	%rax, (%r12)
	call	uv_mutex_lock@PLT
	movq	2544(%rbx), %rax
	lock addq	$1, 2528(%rbx)
	movq	%r12, 2544(%rbx)
	testq	%rax, %rax
	je	.L2400
	movq	16(%rax), %rdi
	movq	%r12, 16(%rax)
	testq	%rdi, %rdi
	je	.L2402
.L2409:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2402:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	leaq	1000(%rbx), %rdi
	call	uv_async_send@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2410
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2400:
	.cfi_restore_state
	movq	2536(%rbx), %rdi
	movq	%r12, 2536(%rbx)
	testq	%rdi, %rdi
	jne	.L2409
	jmp	.L2402
.L2410:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8068:
	.size	_ZZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlPvE_4_FUNES8_, .-_ZZN4node6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEEENUlPvE_4_FUNES8_
	.section	.rodata.str1.1
.LC73:
	.string	"JS heap out of memory"
	.section	.text.unlikely
	.align 2
.LCOLDB74:
	.text
.LHOTB74:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker13NearHeapLimitEPvmm
	.type	_ZN4node6worker6Worker13NearHeapLimitEPvmm, @function
_ZN4node6worker6Worker13NearHeapLimitEPvmm:
.LFB8005:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$21, %r8d
	leaq	.LC73(%rip), %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	xorl	%esi, %esi
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$232, %rdi
	leaq	176(%rbx), %r13
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	.LC61(%rip), %rax
	movq	%rax, -8(%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r13, %rdi
	movl	$1, -44(%rbp)
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2416
.L2412:
	movq	352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2413
	movl	-44(%rbp), %eax
	movl	%eax, 264(%rbx)
	call	_ZN4node4StopEPNS_11EnvironmentE@PLT
.L2414:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	leaq	16777216(%r12), %rax
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L2418
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2413:
	.cfi_restore_state
	movb	$1, 344(%rbx)
	jmp	.L2414
.L2418:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker6Worker13NearHeapLimitEPvmm.cold, @function
_ZN4node6worker6Worker13NearHeapLimitEPvmm.cold:
.LFSB8005:
.L2416:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	-44(%rbp), %rcx
	leaq	272(%rbx), %rdx
	movq	%rbx, %rdi
	leaq	.LC55(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2412
	.cfi_endproc
.LFE8005:
	.text
	.size	_ZN4node6worker6Worker13NearHeapLimitEPvmm, .-_ZN4node6worker6Worker13NearHeapLimitEPvmm
	.section	.text.unlikely
	.size	_ZN4node6worker6Worker13NearHeapLimitEPvmm.cold, .-_ZN4node6worker6Worker13NearHeapLimitEPvmm.cold
.LCOLDE74:
	.text
.LHOTE74:
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"Worker %llu is getting stopped by parent"
	.section	.text.unlikely
	.align 2
.LCOLDB76:
	.text
.LHOTB76:
	.align 2
	.p2align 4
	.globl	_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8070:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	8(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2437
	movq	8(%rbx), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2430
	cmpw	$1040, %cx
	jne	.L2421
.L2430:
	movq	23(%rdx), %rbx
.L2423:
	testq	%rbx, %rbx
	je	.L2419
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	leaq	272(%rbx), %r13
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2434
.L2425:
	leaq	176(%rbx), %r12
	movl	$1, -44(%rbp)
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2435
.L2426:
	movq	352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2427
	movl	-44(%rbp), %eax
	movl	%eax, 264(%rbx)
	call	_ZN4node4StopEPNS_11EnvironmentE@PLT
.L2428:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
.L2419:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2438
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2427:
	.cfi_restore_state
	movb	$1, 344(%rbx)
	jmp	.L2428
	.p2align 4,,10
	.p2align 3
.L2421:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L2423
	.p2align 4,,10
	.p2align 3
.L2437:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2438:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, @function
_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold:
.LFSB8070:
.L2435:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	-44(%rbp), %rcx
	movq	%r13, %rdx
	leaq	.LC55(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2426
.L2434:
	movq	%r13, %rdx
	leaq	.LC75(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2425
	.cfi_endproc
.LFE8070:
	.text
	.size	_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold, .-_ZN4node6worker6Worker10StopThreadERKN2v820FunctionCallbackInfoINS2_5ValueEEE.cold
.LCOLDE76:
	.text
.LHOTE76:
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node6worker6WorkerE
	.section	.data.rel.ro._ZTVN4node6worker6WorkerE,"awG",@progbits,_ZTVN4node6worker6WorkerE,comdat
	.align 8
	.type	_ZTVN4node6worker6WorkerE, @object
	.size	_ZTVN4node6worker6WorkerE, 96
_ZTVN4node6worker6WorkerE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker6WorkerD1Ev
	.quad	_ZN4node6worker6WorkerD0Ev
	.quad	_ZNK4node6worker6Worker10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6worker6Worker14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6worker6Worker8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node6worker23WorkerHeapSnapshotTakerE
	.section	.data.rel.ro._ZTVN4node6worker23WorkerHeapSnapshotTakerE,"awG",@progbits,_ZTVN4node6worker23WorkerHeapSnapshotTakerE,comdat
	.align 8
	.type	_ZTVN4node6worker23WorkerHeapSnapshotTakerE, @object
	.size	_ZTVN4node6worker23WorkerHeapSnapshotTakerE, 96
_ZTVN4node6worker23WorkerHeapSnapshotTakerE:
	.quad	0
	.quad	0
	.quad	_ZN4node6worker23WorkerHeapSnapshotTakerD1Ev
	.quad	_ZN4node6worker23WorkerHeapSnapshotTakerD0Ev
	.quad	_ZNK4node6worker23WorkerHeapSnapshotTaker10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node6worker23WorkerHeapSnapshotTaker14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node6worker23WorkerHeapSnapshotTaker8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker11StartThreadERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlPvE_clESD_EUlS2_E_E4CallES2_
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEENKUlS2_E_clES2_EUlS2_E_E4CallES2_
	.weak	_ZTVSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN4node20ArrayBufferAllocatorESt14default_deleteIS1_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.data.rel.ro.local
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS7_5ValueEEEEUlS2_E_E4CallES2_
	.weak	_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node18EnvironmentOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node17PerIsolateOptionsELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC77:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC79:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.weak	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC80:
	.string	"../src/debug_utils-inl.h:76"
.LC81:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC80
	.quad	.LC81
	.quad	.LC79
	.weak	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC82
	.weak	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC80
	.quad	.LC81
	.quad	.LC82
	.weak	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC83
	.weak	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC80
	.quad	.LC81
	.quad	.LC83
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args
	.section	.rodata.str1.1
.LC84:
	.string	"../src/util.h:352"
.LC85:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args:
	.quad	.LC84
	.quad	.LC85
	.quad	.LC86
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC87:
	.string	"../src/util.h:391"
.LC88:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args:
	.quad	.LC87
	.quad	.LC88
	.quad	.LC89
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC90:
	.string	"../src/util-inl.h:374"
.LC91:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC92:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC90
	.quad	.LC91
	.quad	.LC92
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC93:
	.string	"../src/util-inl.h:325"
.LC94:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC96:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC97:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC98:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.section	.rodata.str1.1
.LC99:
	.string	"../src/node_worker.cc"
.LC100:
	.string	"worker"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC99
	.quad	0
	.quad	_ZN4node6worker12_GLOBAL__N_110InitWorkerEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC100
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC101:
	.string	"../src/node_worker.cc:777"
	.section	.rodata.str1.8
	.align 8
.LC102:
	.string	"(port->CreationContext()->GetIsolate()) == (args.GetIsolate())"
	.align 8
.LC103:
	.string	"void node::worker::{anonymous}::GetEnvMessagePort(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6worker12_GLOBAL__N_117GetEnvMessagePortERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC101
	.quad	.LC102
	.quad	.LC103
	.section	.rodata.str1.1
.LC104:
	.string	"../src/node_worker.cc:753"
.LC105:
	.string	"snapshot"
	.section	.rodata.str1.8
	.align 8
.LC106:
	.string	"node::worker::Worker::TakeHeapSnapshot(const v8::FunctionCallbackInfo<v8::Value>&)::<lambda(node::Environment*)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEEENKUlPNS_11EnvironmentEE_clES9_E4args, @object
	.size	_ZZZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEEENKUlPNS_11EnvironmentEE_clES9_E4args, 24
_ZZZN4node6worker6Worker16TakeHeapSnapshotERKN2v820FunctionCallbackInfoINS2_5ValueEEEENKUlPNS_11EnvironmentEE_clES9_E4args:
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.section	.rodata.str1.1
.LC107:
	.string	"../src/node_worker.cc:608"
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"(limit_info->Length()) == (kTotalResourceLimitCount)"
	.align 8
.LC109:
	.string	"static void node::worker::Worker::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC107
	.quad	.LC108
	.quad	.LC109
	.section	.rodata.str1.1
.LC110:
	.string	"../src/node_worker.cc:606"
.LC111:
	.string	"args[3]->IsFloat64Array()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC109
	.section	.rodata.str1.1
.LC112:
	.string	"../src/node_worker.cc:485"
.LC113:
	.string	"(args.Length()) == (4)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC112
	.quad	.LC113
	.quad	.LC109
	.section	.rodata.str1.1
.LC114:
	.string	"../src/node_worker.cc:472"
.LC115:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node6worker6Worker3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC114
	.quad	.LC115
	.quad	.LC109
	.section	.rodata.str1.1
.LC116:
	.string	"../src/node_worker.cc:463"
.LC117:
	.string	"thread_joined_"
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"virtual node::worker::Worker::~Worker()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6WorkerD4EvE4args_1, @object
	.size	_ZZN4node6worker6WorkerD4EvE4args_1, 24
_ZZN4node6worker6WorkerD4EvE4args_1:
	.quad	.LC116
	.quad	.LC117
	.quad	.LC118
	.section	.rodata.str1.1
.LC119:
	.string	"../src/node_worker.cc:462"
.LC120:
	.string	"(env_) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6WorkerD4EvE4args_0, @object
	.size	_ZZN4node6worker6WorkerD4EvE4args_0, 24
_ZZN4node6worker6WorkerD4EvE4args_0:
	.quad	.LC119
	.quad	.LC120
	.quad	.LC118
	.section	.rodata.str1.1
.LC121:
	.string	"../src/node_worker.cc:461"
.LC122:
	.string	"stopped_"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6WorkerD4EvE4args, @object
	.size	_ZZN4node6worker6WorkerD4EvE4args, 24
_ZZN4node6worker6WorkerD4EvE4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC118
	.section	.rodata.str1.1
.LC123:
	.string	"../src/node_worker.cc:425"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"(uv_thread_join(&tid_)) == (0)"
	.align 8
.LC125:
	.string	"void node::worker::Worker::JoinThread()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6Worker10JoinThreadEvE4args, @object
	.size	_ZZN4node6worker6Worker10JoinThreadEvE4args, 24
_ZZN4node6worker6Worker10JoinThreadEvE4args:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.section	.rodata.str1.1
.LC126:
	.string	"../src/node_worker.cc:265"
.LC127:
	.string	"data.loop_is_usable()"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"void node::worker::Worker::Run()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6Worker3RunEvE4args_0, @object
	.size	_ZZN4node6worker6Worker3RunEvE4args_0, 24
_ZZN4node6worker6Worker3RunEvE4args_0:
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.section	.rodata.str1.1
.LC129:
	.string	"../src/node_worker.cc:259"
.LC130:
	.string	"(platform_) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6worker6Worker3RunEvE4args, @object
	.size	_ZZN4node6worker6Worker3RunEvE4args, 24
_ZZN4node6worker6Worker3RunEvE4args:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC128
	.local	_ZZN4node6worker6Worker3RunEvE28trace_event_unique_atomic256
	.comm	_ZZN4node6worker6Worker3RunEvE28trace_event_unique_atomic256,8,8
	.weak	_ZZN4node6worker16WorkerThreadDataD4EvE4args
	.section	.rodata.str1.1
.LC131:
	.string	"../src/node_worker.cc:203"
.LC132:
	.string	"!loop_init_failed_"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"node::worker::WorkerThreadData::~WorkerThreadData()"
	.section	.data.rel.ro.local._ZZN4node6worker16WorkerThreadDataD4EvE4args,"awG",@progbits,_ZZN4node6worker16WorkerThreadDataD4EvE4args,comdat
	.align 16
	.type	_ZZN4node6worker16WorkerThreadDataD4EvE4args, @gnu_unique_object
	.size	_ZZN4node6worker16WorkerThreadDataD4EvE4args, 24
_ZZN4node6worker16WorkerThreadDataD4EvE4args:
	.quad	.LC131
	.quad	.LC132
	.quad	.LC133
	.weak	_ZZN4node6worker16WorkerThreadDataC4EPNS0_6WorkerEE4args
	.section	.rodata.str1.1
.LC134:
	.string	"../src/node_worker.cc:184"
.LC135:
	.string	"isolate_data_"
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"node::worker::WorkerThreadData::WorkerThreadData(node::worker::Worker*)"
	.section	.data.rel.ro.local._ZZN4node6worker16WorkerThreadDataC4EPNS0_6WorkerEE4args,"awG",@progbits,_ZZN4node6worker16WorkerThreadDataC4EPNS0_6WorkerEE4args,comdat
	.align 16
	.type	_ZZN4node6worker16WorkerThreadDataC4EPNS0_6WorkerEE4args, @gnu_unique_object
	.size	_ZZN4node6worker16WorkerThreadDataC4EPNS0_6WorkerEE4args, 24
_ZZN4node6worker16WorkerThreadDataC4EPNS0_6WorkerEE4args:
	.quad	.LC134
	.quad	.LC135
	.quad	.LC136
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC137:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC139:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC137
	.quad	.LC138
	.quad	.LC139
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC140:
	.string	"../src/base_object-inl.h:195"
.LC141:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC140
	.quad	.LC141
	.quad	.LC139
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC142:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC144:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC145:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC147:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC148:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC149:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC150:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC148
	.quad	.LC149
	.quad	.LC150
	.weak	_ZZN4node11Environment8add_refsElE4args
	.section	.rodata.str1.1
.LC151:
	.string	"../src/env-inl.h:841"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"(task_queues_async_refs_) >= (0)"
	.align 8
.LC153:
	.string	"void node::Environment::add_refs(int64_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8add_refsElE4args,"awG",@progbits,_ZZN4node11Environment8add_refsElE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8add_refsElE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8add_refsElE4args, 24
_ZZN4node11Environment8add_refsElE4args:
	.quad	.LC151
	.quad	.LC152
	.quad	.LC153
	.weak	_ZZN4node11Environment18set_worker_contextEPNS_6worker6WorkerEE4args
	.section	.rodata.str1.1
.LC154:
	.string	"../src/env-inl.h:822"
.LC155:
	.string	"(worker_context_) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC156:
	.string	"void node::Environment::set_worker_context(node::worker::Worker*)"
	.section	.data.rel.ro.local._ZZN4node11Environment18set_worker_contextEPNS_6worker6WorkerEE4args,"awG",@progbits,_ZZN4node11Environment18set_worker_contextEPNS_6worker6WorkerEE4args,comdat
	.align 16
	.type	_ZZN4node11Environment18set_worker_contextEPNS_6worker6WorkerEE4args, @gnu_unique_object
	.size	_ZZN4node11Environment18set_worker_contextEPNS_6worker6WorkerEE4args, 24
_ZZN4node11Environment18set_worker_contextEPNS_6worker6WorkerEE4args:
	.quad	.LC154
	.quad	.LC155
	.quad	.LC156
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC157:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC158:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC159:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC157
	.quad	.LC158
	.quad	.LC159
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"../src/memory_tracker-inl.h:269"
	.section	.rodata.str1.1
.LC161:
	.string	"(n->size_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC162:
	.string	"void node::MemoryTracker::Track(const node::MemoryRetainer*, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0:
	.quad	.LC160
	.quad	.LC161
	.quad	.LC162
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args
	.section	.rodata.str1.8
	.align 8
.LC163:
	.string	"../src/memory_tracker-inl.h:268"
	.section	.rodata.str1.1
.LC164:
	.string	"(CurrentNode()) == (n)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args:
	.quad	.LC163
	.quad	.LC164
	.quad	.LC162
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC165:
	.string	"../src/debug_utils-inl.h:67"
.LC166:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC167:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.quad	7298208976073813847
	.quad	7526482966031331425
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC17:
	.long	0
	.long	0
	.align 8
.LC19:
	.long	0
	.long	1072693248
	.align 8
.LC21:
	.long	0
	.long	1073741824
	.align 8
.LC23:
	.long	0
	.long	1074266112
	.align 8
.LC25:
	.long	0
	.long	1093664768
	.align 8
.LC26:
	.long	0
	.long	1138753536
	.align 8
.LC27:
	.long	0
	.long	1051721728
	.align 8
.LC43:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16
	.align 16
.LC51:
	.quad	0
	.quad	128
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
