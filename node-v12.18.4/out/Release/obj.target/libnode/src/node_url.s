	.file	"node_url.cc"
	.text
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_17ToASCIIERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_, @function
_ZN4node3url12_GLOBAL__N_17ToASCIIERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_:
.LFB7448:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	leaq	-1088(%rbp), %r8
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-1064(%rbp), %rbx
	subq	$1064, %rsp
	movq	8(%rdi), %rdx
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movdqa	.LC0(%rip), %xmm0
	movq	%rbx, -1072(%rbp)
	movb	$0, -1064(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_ZN4node4i18n7ToASCIIEPNS_16MaybeStackBufferIcLm1024EEEPKcmNS0_9idna_modeE@PLT
	testl	%eax, %eax
	js	.L2
	movq	8(%r12), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r13d
	movq	-1088(%rbp), %r8
	movq	-1072(%rbp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L2:
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1
	cmpq	%rbx, %rdi
	je	.L1
	call	free@PLT
.L1:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L15
	addq	$1064, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L15:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7448:
	.size	_ZN4node3url12_GLOBAL__N_17ToASCIIERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_, .-_ZN4node3url12_GLOBAL__N_17ToASCIIERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_19ToUnicodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_, @function
_ZN4node3url12_GLOBAL__N_19ToUnicodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_:
.LFB7447:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	xorl	%r13d, %r13d
	pushq	%r12
	leaq	-1088(%rbp), %r8
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	-1064(%rbp), %rbx
	subq	$1064, %rsp
	movq	8(%rdi), %rdx
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movdqa	.LC0(%rip), %xmm0
	movq	%rbx, -1072(%rbp)
	movb	$0, -1064(%rbp)
	movaps	%xmm0, -1088(%rbp)
	call	_ZN4node4i18n9ToUnicodeEPNS_16MaybeStackBufferIcLm1024EEEPKcm@PLT
	testl	%eax, %eax
	js	.L17
	movq	8(%r12), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movl	$1, %r13d
	movq	-1088(%rbp), %r8
	movq	-1072(%rbp), %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L17:
	movq	-1072(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	cmpq	%rbx, %rdi
	je	.L16
	call	free@PLT
.L16:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L29
	addq	$1064, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L29:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7447:
	.size	_ZN4node3url12_GLOBAL__N_19ToUnicodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_, .-_ZN4node3url12_GLOBAL__N_19ToUnicodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ftp:"
.LC2:
	.string	"file:"
.LC3:
	.string	"gopher:"
.LC4:
	.string	"http:"
.LC5:
	.string	"https:"
.LC6:
	.string	"ws:"
.LC7:
	.string	"wss:"
	.text
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_113NormalizePortERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, @function
_ZN4node3url12_GLOBAL__N_113NormalizePortERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi:
.LFB7445:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%esi, %ebx
	leaq	.LC1(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	cmpl	$21, %ebx
	jne	.L31
	testl	%eax, %eax
	je	.L33
.L31:
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L40
	cmpl	$-1, %ebx
	je	.L33
.L40:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	cmpl	$70, %ebx
	jne	.L41
	testl	%eax, %eax
	je	.L33
.L41:
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	cmpl	$80, %ebx
	sete	%r14b
	testl	%eax, %eax
	jne	.L42
	testb	%r14b, %r14b
	jne	.L33
.L42:
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	cmpl	$443, %ebx
	sete	%r13b
	testl	%eax, %eax
	jne	.L43
	testb	%r13b, %r13b
	jne	.L33
.L43:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L44
	testb	%r14b, %r14b
	jne	.L33
.L44:
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L45
	testb	%r13b, %r13b
	je	.L45
.L33:
	popq	%rbx
	movl	$-1, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7445:
	.size	_ZN4node3url12_GLOBAL__N_113NormalizePortERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi, .-_ZN4node3url12_GLOBAL__N_113NormalizePortERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_124IsForbiddenHostCodePointIcEEbT_, @function
_ZN4node3url12_GLOBAL__N_124IsForbiddenHostCodePointIcEEbT_:
.LFB8252:
	.cfi_startproc
	cmpb	$58, %dil
	jbe	.L72
	leal	-63(%rdi), %edx
	movl	$1, %eax
	cmpb	$1, %dl
	ja	.L74
.L71:
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movabsq	$288371289733735937, %rdx
	movl	$1, %eax
	btq	%rdi, %rdx
	jc	.L71
.L74:
	subl	$91, %edi
	cmpb	$2, %dil
	setbe	%al
	ret
	.cfi_endproc
.LFE8252:
	.size	_ZN4node3url12_GLOBAL__N_124IsForbiddenHostCodePointIcEEbT_, .-_ZN4node3url12_GLOBAL__N_124IsForbiddenHostCodePointIcEEbT_
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB7443:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC1(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L79
.L81:
	movl	$1, %eax
.L78:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L81
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L81
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L81
	leaq	.LC5(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L81
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L81
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	sete	%al
	jmp	.L78
	.cfi_endproc
.LFE7443:
	.size	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_114ShortenUrlPathEPNS0_8url_dataE, @function
_ZN4node3url12_GLOBAL__N_114ShortenUrlPathEPNS0_8url_dataE:
.LFB7483:
	.cfi_startproc
	movq	208(%rdi), %rax
	movq	200(%rdi), %rdx
	cmpq	%rax, %rdx
	je	.L111
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rax, %rcx
	subq	%rdx, %rcx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$32, %rcx
	je	.L114
.L101:
	movq	-32(%rax), %rdi
	leaq	-32(%rax), %rdx
	subq	$16, %rax
	movq	%rdx, 208(%rbx)
	cmpq	%rax, %rdi
	je	.L98
	addq	$8, %rsp
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
	leaq	8(%rdi), %rdi
	leaq	.LC2(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L103
	movq	200(%rbx), %rax
	cmpq	$1, 8(%rax)
	jbe	.L103
	movq	(%rax), %rdx
	movzbl	(%rdx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L103
	cmpb	$58, 1(%rdx)
	jne	.L103
.L98:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	208(%rbx), %rax
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7483:
	.size	_ZN4node3url12_GLOBAL__N_114ShortenUrlPathEPNS0_8url_dataE, .-_ZN4node3url12_GLOBAL__N_114ShortenUrlPathEPNS0_8url_dataE
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L126
	cmpw	$1040, %cx
	jne	.L116
.L126:
	movq	23(%rdx), %r13
.L118:
	cmpl	$1, 16(%rbx)
	je	.L135
	leaq	_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L135:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L136
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L137
	movq	8(%rbx), %r12
.L121:
	movq	3048(%r13), %rdi
	movq	352(%r13), %r14
	testq	%rdi, %rdi
	je	.L122
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3048(%r13)
.L122:
	testq	%r12, %r12
	je	.L115
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3048(%r13)
.L115:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L116:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L136:
	leaq	_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7498:
	.size	_ZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7492:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2096, %rsp
	.cfi_offset 3, -32
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L166
	cmpw	$1040, %cx
	jne	.L139
.L166:
	movq	23(%rdx), %rbx
.L141:
	cmpl	$1, 16(%r12)
	jg	.L177
	leaq	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L177:
	movq	8(%r12), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L142
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L142
	subq	$8, %rdi
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L178
	movl	16(%r12), %edi
	testl	%edi, %edi
	jg	.L145
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L146:
	movq	352(%rbx), %rsi
	leaq	-2096(%rbp), %rdi
	call	_ZN4node12TwoByteValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%r12)
	jg	.L147
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L148:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L179
.L149:
	testq	%rdx, %rdx
	js	.L180
	movq	-2096(%rbp), %rcx
	movq	-2080(%rbp), %r10
	leaq	-1(%rcx), %r11
	cmpq	%rcx, %rdx
	jb	.L151
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L183:
	testw	$1024, %si
	jne	.L155
	cmpq	%r11, %rdx
	je	.L155
	cmpq	%rcx, %rax
	jnb	.L181
	movzwl	2(%r10,%r8), %esi
	andw	$-1024, %si
	cmpw	$-9216, %si
	je	.L182
	movl	$-3, %edx
	movw	%dx, (%rdi)
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rax, %rdx
.L151:
	leaq	(%rdx,%rdx), %r8
	leaq	1(%rdx), %rax
	leaq	(%r10,%r8), %rdi
	movzwl	(%rdi), %esi
	movl	%esi, %r9d
	andw	$-2048, %r9w
	cmpw	$-10240, %r9w
	je	.L183
	cmpq	%rcx, %rax
	jb	.L159
.L160:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r10, %rsi
	movq	(%r12), %r12
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L184
	movq	(%rax), %rax
	movq	%rax, 24(%r12)
.L162:
	movq	-2080(%rbp), %rdi
	leaq	-2072(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L138
	testq	%rdi, %rdi
	je	.L138
	call	free@PLT
.L138:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L185
	addq	$2096, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L155:
	.cfi_restore_state
	movl	$-3, %esi
	movw	%si, (%rdi)
	cmpq	%rcx, %rax
	jb	.L159
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L182:
	leaq	2(%rdx), %rax
	cmpq	%rcx, %rax
	jb	.L159
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L147:
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L145:
	movq	8(%r12), %rdx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L181:
	leaq	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L178:
	leaq	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L180:
	leaq	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L179:
	movq	%rdx, -2104(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-2104(%rbp), %rdx
	jmp	.L149
.L184:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r12), %rax
	movq	%rax, 24(%r12)
	jmp	.L162
.L185:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7492:
	.size	_ZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC8:
	.string	"basic_string::append"
	.text
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7491:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1144, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%rdi, -1160(%rbp)
	movq	32(%rsi), %rdx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L209
	cmpw	$1040, %cx
	jne	.L187
.L209:
	movq	23(%rdx), %rax
	movq	%rax, -1168(%rbp)
.L189:
	movq	-1160(%rbp), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jg	.L221
	leaq	_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L221:
	movq	8(%rax), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L190
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L190
	movq	-1168(%rbp), %rax
	leaq	-1104(%rbp), %rdi
	xorl	%r13d, %r13d
	leaq	-1136(%rbp), %r14
	leaq	_ZN4node3url12_GLOBAL__N_1L19USERINFO_ENCODE_SETE(%rip), %r12
	movq	352(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %rbx
	movq	%r14, %rdi
	leaq	-1120(%rbp), %rax
	movq	%rax, -1152(%rbp)
	movq	%rbx, %rsi
	movq	%rax, -1136(%rbp)
	movq	$0, -1128(%rbp)
	movb	$0, -1120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	testq	%rbx, %rbx
	jne	.L192
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L223:
	movslq	%edx, %rdx
	leaq	_ZN4node3url12_GLOBAL__N_13hexE(%rip), %rax
	movq	(%rax,%rdx,8), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -1144(%rbp)
	call	strlen@PLT
	movq	-1144(%rbp), %rsi
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	%r15, %rax
	cmpq	%rax, %rdx
	ja	.L222
	movq	%r14, %rdi
	addq	$1, %r13
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	cmpq	%rbx, %r13
	je	.L200
.L192:
	movq	-1088(%rbp), %rax
	movq	-1128(%rbp), %r15
	movzbl	(%rax,%r13), %edx
	movl	%edx, %eax
	movl	%edx, %r9d
	sarl	$3, %eax
	cltq
	movzbl	(%r12,%rax), %ecx
	movl	%edx, %eax
	andl	$7, %eax
	btl	%eax, %ecx
	jc	.L223
	movq	-1136(%rbp), %rax
	movl	$15, %edx
	leaq	1(%r15), %r10
	cmpq	-1152(%rbp), %rax
	cmovne	-1120(%rbp), %rdx
	cmpq	%rdx, %r10
	ja	.L224
.L199:
	movb	%r9b, (%rax,%r15)
	movq	-1136(%rbp), %rax
	addq	$1, %r13
	movq	%r10, -1128(%rbp)
	movb	$0, 1(%rax,%r15)
	cmpq	%rbx, %r13
	jne	.L192
.L200:
	movq	-1160(%rbp), %rax
	movq	-1136(%rbp), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%rax), %rbx
	movq	-1168(%rbp), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L225
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L203:
	movq	-1136(%rbp), %rdi
	cmpq	-1152(%rbp), %rdi
	je	.L201
	call	_ZdlPv@PLT
.L201:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L186
	testq	%rdi, %rdi
	je	.L186
	call	free@PLT
.L186:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L226
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%r10, -1176(%rbp)
	movb	%r9b, -1144(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-1136(%rbp), %rax
	movq	-1176(%rbp), %r10
	movzbl	-1144(%rbp), %r9d
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L190:
	leaq	_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -1168(%rbp)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L225:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L203
.L222:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7491:
	.size	_ZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh, @function
_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh:
.LFB7440:
	.cfi_startproc
	movq	%rdx, %r8
	movzbl	%sil, %edx
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %eax
	sarl	$3, %eax
	cltq
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movzbl	(%r8,%rax), %ecx
	movl	%esi, %eax
	andl	$7, %eax
	btl	%eax, %ecx
	jnc	.L228
	leaq	_ZN4node3url12_GLOBAL__N_13hexE(%rip), %rax
	movq	(%rax,%rdx,8), %r13
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	%rax, %rdx
	ja	.L234
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	.p2align 4,,10
	.p2align 3
.L228:
	.cfi_restore_state
	movq	8(%rdi), %r13
	movq	(%rdi), %rax
	leaq	16(%rdi), %rdx
	movl	%esi, %ebx
	leaq	1(%r13), %r14
	cmpq	%rdx, %rax
	je	.L232
	movq	16(%rdi), %rdx
.L230:
	cmpq	%rdx, %r14
	ja	.L235
.L231:
	movb	%bl, (%rax,%r13)
	movq	(%r12), %rax
	movq	%r14, 8(%r12)
	movb	$0, 1(%rax,%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movl	$15, %edx
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rax
	jmp	.L231
.L234:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7440:
	.size	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh, .-_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_113PercentDecodeEPKcm, @function
_ZN4node3url12_GLOBAL__N_113PercentDecodeEPKcm:
.LFB7442:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%r15, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	testq	%rdx, %rdx
	jne	.L271
.L236:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	%rsi, %rbx
	movq	%rdx, %r14
	movq	%rdx, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	addq	%rbx, %r14
	cmpq	%r14, %rbx
	jb	.L238
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L276:
	movl	%eax, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$5, %dl
	ja	.L239
	movzbl	2(%rbx), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L259
	movl	%edx, %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpb	$5, %sil
	ja	.L239
.L259:
	leal	-65(%rax), %esi
	movsbl	%al, %r13d
	cmpb	$5, %sil
	jbe	.L272
	subl	$97, %eax
	subl	$87, %r13d
	sall	$4, %r13d
	cmpb	$6, %al
	movl	$-16, %eax
	cmovnb	%eax, %r13d
	movsbl	%dl, %eax
	cmpb	$9, %cl
	jbe	.L273
.L247:
	leal	-65(%rdx), %ecx
	cmpb	$5, %cl
	jbe	.L274
	subl	$97, %edx
	subl	$87, %eax
	cmpb	$6, %dl
	movl	$-1, %edx
	cmovnb	%edx, %eax
.L248:
	addl	%eax, %r13d
	movq	(%r12), %rdx
	movq	8(%r12), %rax
	leaq	1(%rax), %r9
	cmpq	%rdx, %r15
	je	.L263
	movq	16(%r12), %rcx
.L250:
	cmpq	%rcx, %r9
	ja	.L275
.L251:
	movb	%r13b, (%rdx,%rax)
	movq	(%r12), %rdx
	addq	$3, %rbx
	movq	%r9, 8(%r12)
	movb	$0, 1(%rdx,%rax)
.L244:
	cmpq	%rbx, %r14
	jbe	.L236
.L238:
	movq	%r14, %rax
	movzbl	(%rbx), %r13d
	subq	%rbx, %rax
	subq	$1, %rax
	cmpq	$1, %rax
	jbe	.L239
	cmpb	$37, %r13b
	jne	.L239
	movzbl	1(%rbx), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	ja	.L276
	movzbl	2(%rbx), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	ja	.L277
.L258:
	movsbl	%al, %r13d
.L270:
	sall	$4, %r13d
	movsbl	%dl, %eax
	cmpb	$9, %cl
	ja	.L247
.L273:
	subl	$48, %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L277:
	movl	%edx, %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpb	$5, %sil
	jbe	.L258
.L239:
	movq	8(%r12), %rax
	movq	(%r12), %rdx
	leaq	1(%rax), %r9
	cmpq	%rdx, %r15
	je	.L260
	movq	16(%r12), %rcx
.L242:
	cmpq	%rcx, %r9
	ja	.L278
.L243:
	movb	%r13b, (%rdx,%rax)
	movq	(%r12), %rdx
	addq	$1, %rbx
	movq	%r9, 8(%r12)
	movb	$0, 1(%rdx,%rax)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L274:
	subl	$55, %eax
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L272:
	subl	$55, %r13d
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L278:
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	(%r12), %rdx
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L263:
	movl	$15, %ecx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$15, %ecx
	jmp	.L242
	.cfi_endproc
.LFE7442:
	.size	_ZN4node3url12_GLOBAL__N_113PercentDecodeEPKcm, .-_ZN4node3url12_GLOBAL__N_113PercentDecodeEPKcm
	.align 2
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_17URLHost9ParseHostEPKcmbb.constprop.0, @function
_ZN4node3url12_GLOBAL__N_17URLHost9ParseHostEPKcmbb.constprop.0:
.LFB9935:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L279
	cmpb	$91, (%rsi)
	movq	%rdi, %rbx
	movq	%rsi, %r12
	je	.L477
	testb	%cl, %cl
	je	.L478
	leaq	-128(%rbp), %r14
	movl	%r8d, %r13d
	movq	%r14, %rdi
	call	_ZN4node3url12_GLOBAL__N_113PercentDecodeEPKcm
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	_ZN4node3url12_GLOBAL__N_17ToASCIIERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_
	testb	%al, %al
	je	.L331
	movq	-120(%rbp), %r12
	testq	%r12, %r12
	je	.L332
	movq	-128(%rbp), %r8
	movq	%r8, %rcx
	addq	%r8, %r12
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L480:
	addq	$1, %rcx
	cmpq	%rcx, %r12
	je	.L479
.L334:
	movsbl	(%rcx), %edi
	call	_ZN4node3url12_GLOBAL__N_124IsForbiddenHostCodePointIcEEbT_
	testb	%al, %al
	je	.L480
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L479:
	movl	32(%rbx), %eax
	movl	%eax, -144(%rbp)
	testl	%eax, %eax
	jne	.L376
	cmpq	%r12, %r8
	ja	.L375
	movq	%rbx, -136(%rbp)
	xorl	%ecx, %ecx
	movq	%r8, %r15
	movq	%r8, %rdi
	movq	%r14, -160(%rbp)
	movl	%r13d, -148(%rbp)
	movl	%ecx, %r13d
	.p2align 4,,10
	.p2align 3
.L350:
	xorl	%r14d, %r14d
	cmpq	%r12, %r15
	jb	.L481
.L336:
	leal	1(%r13), %ebx
	cmpq	%rdi, %r15
	je	.L471
	cmpl	$4, %ebx
	jg	.L471
	movq	%r15, %rax
	movl	$10, %r11d
	movl	$10, %esi
	subq	%rdi, %rax
	cmpq	$1, %rax
	jle	.L340
	cmpb	$48, (%rdi)
	je	.L482
.L340:
	cmpq	%rdi, %r15
	jbe	.L343
	movq	%rdi, %rdx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L484:
	cmpl	$8, %esi
	je	.L483
.L346:
	addq	$1, %rdx
	cmpq	%rdx, %r15
	je	.L343
.L347:
	movzbl	(%rdx), %eax
	cmpl	$10, %esi
	je	.L344
	cmpl	$16, %esi
	jne	.L484
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L346
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$5, %al
	jbe	.L346
.L471:
	movq	-136(%rbp), %rbx
	movl	-148(%rbp), %r13d
	movq	-160(%rbp), %r14
.L378:
	testb	%r13b, %r13b
	je	.L363
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	_ZN4node3url12_GLOBAL__N_19ToUnicodeERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_
	testb	%al, %al
	je	.L331
.L363:
	movl	32(%rbx), %eax
	leaq	16(%rbx), %r12
	cmpl	$1, %eax
	je	.L364
	cmpl	$4, %eax
	je	.L364
.L365:
	movq	-128(%rbp), %rax
	leaq	-112(%rbp), %rdx
	movl	$1, 32(%rbx)
	movq	%r12, (%rbx)
	cmpq	%rdx, %rax
	je	.L485
	movq	%rax, (%rbx)
	movq	-112(%rbp), %rax
	movq	%rax, 16(%rbx)
.L368:
	movq	-120(%rbp), %rax
	movq	%rax, 8(%rbx)
	.p2align 4,,10
	.p2align 3
.L279:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L477:
	.cfi_restore_state
	leaq	-1(%rsi,%rdx), %rax
	cmpb	$93, (%rax)
	jne	.L279
	leaq	1(%rsi), %rcx
	movl	32(%rdi), %esi
	testl	%esi, %esi
	jne	.L487
	pxor	%xmm0, %xmm0
	leaq	16(%rdi), %r9
	movaps	%xmm0, (%rdi)
	cmpq	%rcx, %rax
	ja	.L488
.L285:
	cmpq	%rdi, %r9
	jne	.L279
.L288:
	movl	$3, 32(%rbx)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L331:
	movq	-128(%rbp), %r8
.L333:
	leaq	-112(%rbp), %rax
	cmpq	%rax, %r8
	je	.L279
	movq	%r8, %rdi
.L475:
	call	_ZdlPv@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L478:
	movl	32(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L489
	leaq	-96(%rbp), %r13
	leaq	-80(%rbp), %rax
	movq	%rdx, %rsi
	movb	$0, -80(%rbp)
	movq	%r13, %rdi
	movq	%rdx, -136(%rbp)
	movq	%r12, %r15
	leaq	_ZN4node3url12_GLOBAL__N_1L21C0_CONTROL_ENCODE_SETE(%rip), %r14
	movq	%rax, -144(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	-136(%rbp), %rdx
	leaq	(%r12,%rdx), %r12
	.p2align 4,,10
	.p2align 3
.L325:
	movzbl	(%r15), %esi
	cmpb	$37, %sil
	je	.L321
	movsbl	%sil, %edi
	call	_ZN4node3url12_GLOBAL__N_124IsForbiddenHostCodePointIcEEbT_
	testb	%al, %al
	jne	.L490
.L321:
	movzbl	%sil, %esi
	movq	%r14, %rdx
	movq	%r13, %rdi
	addq	$1, %r15
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	cmpq	%r15, %r12
	jne	.L325
	movl	32(%rbx), %eax
	leaq	16(%rbx), %r12
	cmpl	$1, %eax
	je	.L326
	cmpl	$4, %eax
	je	.L326
.L327:
	movl	$4, 32(%rbx)
	movq	-96(%rbp), %rax
	movq	%r12, (%rbx)
	cmpq	-144(%rbp), %rax
	je	.L491
	movq	%rax, (%rbx)
	movq	-80(%rbp), %rax
	movq	%rax, 16(%rbx)
.L330:
	movq	-88(%rbp), %rax
	movq	%rax, 8(%rbx)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L344:
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L471
	addq	$1, %rdx
	cmpq	%rdx, %r15
	jne	.L347
.L343:
	xorl	%esi, %esi
	movl	%r11d, %edx
	call	strtoll@PLT
	testq	%rax, %rax
	js	.L471
	xorl	%edx, %edx
	cmpq	$255, %rax
	setg	%dl
	addl	%edx, -144(%rbp)
.L342:
	movslq	%r13d, %rcx
	movq	%r12, %rdx
	movq	%rax, -96(%rbp,%rcx,8)
	subq	%r15, %rdx
	leaq	1(%r15), %rax
	movq	%rax, %rdi
	cmpl	$1, %edx
	jne	.L391
	testb	%r14b, %r14b
	jne	.L389
.L391:
	movl	%ebx, %r13d
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L483:
	subl	$48, %eax
	cmpb	$7, %al
	jbe	.L346
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L481:
	movzbl	(%r15), %eax
	cmpb	$46, %al
	sete	%r14b
	cmpb	$-1, %al
	je	.L336
	testb	%r14b, %r14b
	jne	.L336
	leaq	1(%r15), %rax
.L337:
	movq	%rax, %r15
	cmpq	%r12, %rax
	jbe	.L350
	movq	-136(%rbp), %rbx
	movl	%r13d, %ecx
	testl	%r13d, %r13d
	je	.L375
.L348:
	movl	-144(%rbp), %edx
	cmpl	$1, %edx
	jg	.L331
	leal	-1(%rcx), %r12d
	movslq	%r12d, %rax
	movq	-96(%rbp,%rax,8), %r14
	jne	.L352
	cmpq	$255, %r14
	jbe	.L331
.L352:
	movl	$5, %eax
	pxor	%xmm1, %xmm1
	subl	%ecx, %eax
	cvtsi2sdl	%eax, %xmm1
	movq	.LC9(%rip), %rax
	movq	%rax, %xmm0
	call	pow@PLT
	testq	%r14, %r14
	js	.L355
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%r14, %xmm1
.L356:
	comisd	%xmm0, %xmm1
	jnb	.L331
	movl	$2, 32(%rbx)
	testl	%r12d, %r12d
	je	.L357
	xorl	%r15d, %r15d
	leaq	-96(%rbp), %r13
.L362:
	movq	0(%r13,%r15,8), %rax
	testq	%rax, %rax
	js	.L358
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%rax, %xmm3
	movsd	%xmm3, -136(%rbp)
.L359:
	movl	$3, %eax
	pxor	%xmm1, %xmm1
	movl	%r14d, %r14d
	subl	%r15d, %eax
	addq	$1, %r15
	cvtsi2sdl	%eax, %xmm1
	movq	.LC9(%rip), %rax
	movq	%rax, %xmm0
	call	pow@PLT
	movsd	-136(%rbp), %xmm1
	mulsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r14, %xmm0
	addsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %r14
	cmpl	%r15d, %r12d
	jg	.L362
.L357:
	movl	%r14d, (%rbx)
	movq	-128(%rbp), %r8
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L482:
	movzbl	1(%rdi), %eax
	orl	$32, %eax
	cmpb	$120, %al
	je	.L492
	addq	$1, %rdi
	movl	$8, %r11d
	movl	$8, %esi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L490:
	movq	-96(%rbp), %rdi
	cmpq	-144(%rbp), %rdi
	jne	.L475
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L326:
	movq	(%rbx), %rdi
	cmpq	%r12, %rdi
	je	.L327
	call	_ZdlPv@PLT
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L491:
	movdqa	-80(%rbp), %xmm2
	movaps	%xmm2, 16(%rbx)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L492:
	addq	$2, %rdi
	cmpq	%rdi, %r15
	je	.L388
	movl	$16, %r11d
	movl	$16, %esi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rbx), %rdi
	cmpq	%r12, %rdi
	je	.L365
	call	_ZdlPv@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L488:
	movzbl	1(%r12), %esi
	cmpb	$58, %sil
	je	.L286
	cmpb	$-1, %sil
	je	.L285
	movq	%rdi, %rdx
	xorl	%r10d, %r10d
.L474:
	cmpq	%rdx, %r9
	jbe	.L279
.L494:
	movq	%rdx, %r8
	cmpb	$58, %sil
	jne	.L493
	testq	%r10, %r10
	jne	.L279
	leaq	1(%rcx), %r8
	cmpq	%r8, %rax
	jbe	.L288
	movzbl	1(%rcx), %esi
	leaq	2(%rdx), %rdi
	cmpb	$-1, %sil
	je	.L308
	movq	%r8, %rcx
	movq	%rdi, %r10
.L309:
	movq	%rdi, %rdx
	cmpq	%rdx, %r9
	ja	.L494
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L493:
	leal	-48(%rsi), %r11d
	movsbl	%sil, %edi
	cmpb	$9, %r11b
	ja	.L495
	subl	$48, %edi
.L373:
	leaq	1(%rcx), %rsi
	cmpq	%rsi, %rax
	ja	.L496
.L293:
	movw	%di, (%rdx)
	leaq	2(%rdx), %r8
.L306:
	testq	%r10, %r10
	je	.L384
	movq	%r8, %rax
	subq	%r10, %rax
	sarq	%rax
	testl	%eax, %eax
	je	.L288
	movl	%eax, %edx
	movzwl	14(%rbx), %ecx
	addq	%rdx, %rdx
	leaq	-2(%r10,%rdx), %rdx
	movzwl	(%rdx), %esi
	movw	%si, 14(%rbx)
	movw	%cx, (%rdx)
	movl	%eax, %edx
	subl	$1, %edx
	je	.L288
	movl	%edx, %edx
	movzwl	12(%rbx), %ecx
	addq	%rdx, %rdx
	leaq	-2(%r10,%rdx), %rdx
	movzwl	(%rdx), %esi
	movw	%si, 12(%rbx)
	movw	%cx, (%rdx)
	movl	%eax, %edx
	subl	$2, %edx
	je	.L288
	movl	%edx, %edx
	movzwl	10(%rbx), %ecx
	addq	%rdx, %rdx
	leaq	-2(%r10,%rdx), %rdx
	movzwl	(%rdx), %esi
	movw	%si, 10(%rbx)
	movw	%cx, (%rdx)
	movl	%eax, %edx
	subl	$3, %edx
	je	.L288
	movl	%edx, %edx
	movzwl	8(%rbx), %ecx
	addq	%rdx, %rdx
	leaq	-2(%r10,%rdx), %rdx
	movzwl	(%rdx), %esi
	movw	%si, 8(%rbx)
	movw	%cx, (%rdx)
	movl	%eax, %edx
	subl	$4, %edx
	je	.L288
	movl	%edx, %edx
	movzwl	6(%rbx), %ecx
	addq	%rdx, %rdx
	leaq	-2(%r10,%rdx), %rdx
	movzwl	(%rdx), %esi
	movw	%si, 6(%rbx)
	movw	%cx, (%rdx)
	movl	%eax, %edx
	subl	$5, %edx
	je	.L288
	movl	%edx, %edx
	movzwl	4(%rbx), %ecx
	addq	%rdx, %rdx
	leaq	-2(%r10,%rdx), %rdx
	movzwl	(%rdx), %esi
	movw	%si, 4(%rbx)
	movw	%cx, (%rdx)
	subl	$6, %eax
	je	.L288
	movl	%eax, %eax
	movzwl	2(%rbx), %edx
	addq	%rax, %rax
	leaq	-2(%r10,%rax), %rax
	movzwl	(%rax), %ecx
	movw	%cx, 2(%rbx)
	movw	%dx, (%rax)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	_ZZN4node3url12_GLOBAL__N_17URLHost15ParseOpaqueHostEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L485:
	movdqa	-112(%rbp), %xmm4
	movaps	%xmm4, 16(%rbx)
	jmp	.L368
.L358:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -136(%rbp)
	jmp	.L359
.L496:
	movsbl	1(%rcx), %r13d
	movl	%edi, %r14d
	sall	$4, %r14d
	leal	-48(%r13), %r15d
	movl	%r13d, %r11d
	leal	-48(%r13), %r12d
	cmpb	$9, %r15b
	ja	.L497
.L372:
	leaq	2(%rcx), %rsi
	leal	(%r12,%r14), %edi
	cmpq	%rsi, %rax
	jbe	.L293
	movsbl	2(%rcx), %r13d
	movl	%edi, %r14d
	sall	$4, %r14d
	leal	-48(%r13), %r15d
	movl	%r13d, %r11d
	leal	-48(%r13), %r12d
	cmpb	$9, %r15b
	ja	.L498
.L371:
	leaq	3(%rcx), %rsi
	leal	(%r12,%r14), %edi
	cmpq	%rsi, %rax
	jbe	.L293
	movsbl	3(%rcx), %r13d
	movl	%edi, %r14d
	sall	$4, %r14d
	leal	-48(%r13), %r15d
	movl	%r13d, %r11d
	leal	-48(%r13), %r12d
	cmpb	$9, %r15b
	ja	.L499
.L370:
	leaq	4(%rcx), %rsi
	leal	(%r12,%r14), %edi
	cmpq	%rsi, %rax
	jbe	.L293
	movzbl	4(%rcx), %r11d
	movq	$-4, %rcx
.L297:
	cmpb	$46, %r11b
	jne	.L500
	addq	%rcx, %rsi
	cmpq	%rsi, %rax
	jbe	.L279
	leaq	12(%rbx), %rdi
	movzbl	(%rsi), %ecx
	cmpq	%rdi, %rdx
	ja	.L279
	cmpb	$-1, %cl
	je	.L279
	xorl	%r12d, %r12d
.L312:
	leal	-48(%rcx), %edx
	cmpb	$9, %dl
	ja	.L279
.L502:
	movsbl	%cl, %edx
	subl	$48, %edx
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rsi, %r13
	addq	$1, %rsi
	cmpq	%rsi, %rax
	ja	.L501
	movzwl	(%r8), %ecx
	sall	$8, %ecx
	addl	%ecx, %edx
	movw	%dx, (%r8)
	leal	1(%r12), %edx
	andl	$-3, %r12d
	cmpl	$1, %r12d
	jne	.L313
	movl	$-1, %ecx
.L379:
	addq	$2, %r8
	movl	%edx, %r12d
.L318:
	cmpb	$-1, %cl
	je	.L313
	testl	%edx, %edx
	je	.L312
	cmpb	$46, %cl
	jne	.L279
	cmpl	$3, %edx
	ja	.L279
	addq	$2, %r13
	cmpq	%r13, %rax
	jbe	.L279
	movzbl	1(%rsi), %ecx
	movq	%r13, %rsi
	leal	-48(%rcx), %edx
	cmpb	$9, %dl
	jbe	.L502
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L501:
	movsbl	(%rsi), %edi
	leal	-48(%rdi), %r11d
	movl	%edi, %ecx
	cmpb	$9, %r11b
	ja	.L503
	subl	$48, %edi
	testl	%edx, %edx
	je	.L279
	leal	(%rdx,%rdx,4), %edx
	leal	(%rdi,%rdx,2), %edx
	cmpl	$255, %edx
	jbe	.L315
	jmp	.L279
.L332:
	movl	32(%rbx), %eax
	testl	%eax, %eax
	je	.L378
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L389:
	movl	%ebx, %r10d
	movq	-136(%rbp), %rbx
	movl	%r10d, %ecx
	jmp	.L348
.L495:
	leal	-65(%rsi), %r11d
	cmpb	$5, %r11b
	jbe	.L292
	subl	$97, %esi
	cmpb	$5, %sil
	ja	.L279
	subl	$87, %edi
	jmp	.L373
.L500:
	cmpb	$58, %r11b
	jne	.L504
	leaq	1(%rsi), %rcx
	cmpq	%rcx, %rax
	jbe	.L279
	movzbl	1(%rsi), %esi
	cmpb	$-1, %sil
	je	.L279
	movw	%di, (%rdx)
	leaq	2(%rdx), %rdi
	jmp	.L309
.L504:
	cmpb	$-1, %r11b
	jne	.L279
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L286:
	subq	$2, %rdx
	cmpq	$1, %rdx
	jbe	.L279
	cmpb	$58, 2(%r12)
	jne	.L279
	leaq	3(%r12), %rcx
	leaq	2(%rdi), %rdx
	cmpq	%rcx, %rax
	jbe	.L288
	movzbl	3(%r12), %esi
	cmpb	$-1, %sil
	je	.L288
	movq	%rdx, %r10
	jmp	.L474
.L388:
	xorl	%eax, %eax
	jmp	.L342
.L355:
	movq	%r14, %rax
	movq	%r14, %rdx
	pxor	%xmm1, %xmm1
	shrq	%rax
	andl	$1, %edx
	orq	%rdx, %rax
	cvtsi2sdq	%rax, %xmm1
	addsd	%xmm1, %xmm1
	jmp	.L356
.L497:
	leal	-65(%r13), %r12d
	cmpb	$5, %r12b
	ja	.L505
	leal	-55(%r13), %r12d
	jmp	.L372
.L498:
	leal	-65(%r13), %r12d
	cmpb	$5, %r12b
	ja	.L506
	leal	-55(%r13), %r12d
	jmp	.L371
.L487:
	leaq	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv6HostEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L292:
	subl	$55, %edi
	jmp	.L373
.L499:
	leal	-65(%r13), %r12d
	cmpb	$5, %r12b
	ja	.L507
	leal	-55(%r13), %r12d
	jmp	.L370
.L375:
	leaq	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L486:
	call	__stack_chk_fail@PLT
.L313:
	cmpl	$4, %edx
	je	.L306
	jmp	.L279
.L505:
	leal	-97(%r13), %r12d
	cmpb	$5, %r12b
	jbe	.L296
	orq	$-1, %rcx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L296:
	leal	-87(%r13), %r12d
	jmp	.L372
.L507:
	leal	-97(%r13), %r12d
	cmpb	$5, %r12b
	jbe	.L304
	movq	$-3, %rcx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L304:
	leal	-87(%r13), %r12d
	jmp	.L370
.L384:
	movq	%r8, %rdi
	jmp	.L285
.L503:
	movzwl	(%r8), %edi
	sall	$8, %edi
	addl	%edi, %edx
	movl	%r12d, %edi
	andl	$-3, %edi
	movw	%dx, (%r8)
	leal	1(%r12), %edx
	subl	$1, %edi
	movl	%edx, %r12d
	jne	.L318
	jmp	.L379
.L308:
	cmpq	$-2, %rdx
	jne	.L288
	jmp	.L285
.L506:
	leal	-97(%r13), %r12d
	cmpb	$5, %r12b
	jbe	.L301
	movq	$-2, %rcx
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	leal	-87(%r13), %r12d
	jmp	.L371
	.cfi_endproc
.LFE9935:
	.size	_ZN4node3url12_GLOBAL__N_17URLHost9ParseHostEPKcmbb.constprop.0, .-_ZN4node3url12_GLOBAL__N_17URLHost9ParseHostEPKcmbb.constprop.0
	.section	.rodata.str1.1
.LC10:
	.string	"%d"
.LC11:
	.string	":"
.LC12:
	.string	"%x"
.LC13:
	.string	"::"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_17URLHost12ToStringMoveEv, @function
_ZN4node3url12_GLOBAL__N_17URLHost12ToStringMoveEv:
.LFB7456:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movb	$0, 16(%rdi)
	movq	%rax, -144(%rbp)
	movq	%rax, (%rdi)
	movl	32(%rsi), %eax
	movq	$0, 8(%rdi)
	cmpl	$1, %eax
	je	.L509
	cmpl	$4, %eax
	je	.L509
	leaq	-80(%rbp), %rcx
	movq	$0, -88(%rbp)
	leaq	-96(%rbp), %r12
	movq	%rcx, -152(%rbp)
	movq	%rcx, -96(%rbp)
	movb	$0, -80(%rbp)
	cmpl	$2, %eax
	je	.L516
	cmpl	$3, %eax
	je	.L666
	leaq	-112(%rbp), %r12
	xorl	%edx, %edx
	movq	%r12, -128(%rbp)
.L586:
	movdqa	-80(%rbp), %xmm2
	movq	(%rbx), %rdi
	movq	%rdx, -120(%rbp)
	movaps	%xmm2, -112(%rbp)
.L571:
	testq	%rdx, %rdx
	je	.L573
	cmpq	$1, %rdx
	je	.L667
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-120(%rbp), %rdx
	movq	(%rbx), %rdi
.L573:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L509:
	movq	0(%r13), %rsi
	leaq	16(%r13), %rax
	movq	8(%r13), %rdx
	cmpq	%rax, %rsi
	je	.L668
	movq	%rsi, (%rbx)
	movq	%rdx, 8(%rbx)
	movq	16(%r13), %rdx
	movq	%rdx, 16(%rbx)
	movq	%rax, 0(%r13)
.L514:
	movq	$0, 8(%r13)
	movb	$0, (%rax)
.L515:
	movl	32(%r13), %eax
	cmpl	$1, %eax
	je	.L579
	cmpl	$4, %eax
	je	.L579
.L580:
	movl	$0, 32(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L669
	addq	$120, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L579:
	.cfi_restore_state
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L580
	call	_ZdlPv@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L516:
	movl	$15, %esi
	movq	%r12, %rdi
	leaq	-61(%rbp), %r14
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movl	0(%r13), %r15d
	movl	$4, %ecx
	xorl	%eax, %eax
	leaq	.LC10(%rip), %r8
	movl	$1, %edx
	movl	$4, %esi
	movq	%r14, %rdi
	movzbl	%r15b, %r9d
	call	__snprintf_chk@PLT
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r14, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	$3, %eax
	movq	%rbx, -136(%rbp)
	movl	%eax, %ebx
.L519:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$46, %r8d
	movl	$1, %ecx
	movq	%r12, %rdi
	shrl	$8, %r15d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movl	$4, %ecx
	movzbl	%r15b, %r9d
	xorl	%eax, %eax
	leaq	.LC10(%rip), %r8
	movl	$1, %edx
	movl	$4, %esi
	movq	%r14, %rdi
	call	__snprintf_chk@PLT
	movq	%r14, %rdi
	call	strlen@PLT
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rcx
	movq	%rax, %r8
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	subl	$1, %ebx
	jne	.L519
	movq	-136(%rbp), %rbx
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
.L583:
	leaq	-112(%rbp), %r12
	movq	%r12, -128(%rbp)
	cmpq	-152(%rbp), %rax
	je	.L586
	movq	-80(%rbp), %rcx
	movq	(%rbx), %rdi
	movq	%rax, -128(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rcx, -112(%rbp)
	cmpq	%r12, %rax
	je	.L571
	cmpq	%rdi, -144(%rbp)
	je	.L670
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	16(%rbx), %rsi
	movq	%rax, (%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L577
	movq	%rdi, -128(%rbp)
	movq	%rsi, -112(%rbp)
.L575:
	movq	$0, -120(%rbp)
	movb	$0, (%rdi)
	movq	-128(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L515
	call	_ZdlPv@PLT
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L668:
	testq	%rdx, %rdx
	je	.L512
	cmpq	$1, %rdx
	je	.L671
	movq	-144(%rbp), %rdi
	call	memcpy@PLT
	movq	8(%r13), %rdx
.L512:
	movq	%rdx, 8(%rbx)
	movb	$0, 16(%rbx,%rdx)
	movq	0(%r13), %rax
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L666:
	movl	$41, %esi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %rdx
	movl	$15, %eax
	cmpq	-152(%rbp), %rdx
	cmovne	-80(%rbp), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r15
	ja	.L672
.L522:
	movb	$91, (%rdx,%r14)
	movq	-96(%rbp), %rax
	leaq	2(%r13), %rcx
	movq	%r15, -88(%rbp)
	movb	$0, 1(%rax,%r14)
	cmpw	$0, 0(%r13)
	movq	%r13, %rax
	je	.L523
	cmpw	$0, 2(%r13)
	jne	.L525
	movq	%rcx, %rsi
	xorl	%edx, %edx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L523:
	cmpw	$0, 2(%r13)
	je	.L592
.L525:
	leaq	2(%rcx), %rsi
	cmpw	$0, 2(%rcx)
	movq	%rsi, -136(%rbp)
	je	.L593
	movq	%rsi, %rcx
.L527:
	cmpw	$0, 2(%rcx)
	leaq	2(%rcx), %rdi
	movl	$1, %edx
	jne	.L664
	xorl	%ecx, %ecx
.L531:
	movl	%edx, %r8d
	movq	%rdi, -136(%rbp)
	xorl	%edx, %edx
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, (%rbx)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 8(%rbx)
.L577:
	movq	%r12, -128(%rbp)
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%rcx, %rsi
	movl	$1, %edx
	movq	%r13, %rcx
.L524:
	addl	$1, %edx
	cmpw	$0, 2(%rsi)
	leaq	2(%rsi), %r8
	jne	.L673
	movq	%rcx, -136(%rbp)
.L526:
	addl	$1, %edx
	cmpw	$0, 2(%r8)
	leaq	2(%r8), %rdi
	jne	.L674
	movl	$1, %r8d
	xorl	%ecx, %ecx
.L529:
	addl	$1, %edx
	cmpw	$0, 2(%rdi)
	leaq	2(%rdi), %rsi
	je	.L535
	cmpl	%r8d, %edx
	jbe	.L675
	cmpw	$0, 4(%rdi)
	leaq	4(%rdi), %r9
	jne	.L546
.L543:
	movq	-136(%rbp), %rcx
	movq	%r9, -136(%rbp)
	movl	%edx, %r8d
	xorl	%edx, %edx
.L541:
	addl	$1, %edx
	cmpw	$0, 2(%r9)
	leaq	2(%r9), %rdi
	jne	.L665
.L547:
	leal	1(%rdx), %esi
	addl	$2, %edx
	cmpw	$0, 2(%rdi)
	je	.L665
	cmpl	%r8d, %esi
	cmova	-136(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	.p2align 4,,10
	.p2align 3
.L555:
	xorl	%r15d, %r15d
.L558:
	cmpq	%rax, -136(%rbp)
	je	.L676
.L559:
	movslq	%r15d, %rax
	leaq	-61(%rbp), %r14
	movl	$1, %edx
	movl	$5, %ecx
	movzwl	0(%r13,%rax,2), %r9d
	leaq	.LC12(%rip), %r8
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	$5, %esi
	call	__snprintf_chk@PLT
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	%rax, %rdx
	ja	.L564
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	cmpl	$7, %r15d
	je	.L582
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %rdx
	movl	$15, %eax
	cmpq	-152(%rbp), %rdx
	cmovne	-80(%rbp), %rax
	leaq	1(%r14), %r9
	cmpq	%rax, %r9
	ja	.L677
.L567:
	movb	$58, (%rdx,%r14)
	movq	-96(%rbp), %rax
	addl	$1, %r15d
	movq	%r9, -88(%rbp)
	movb	$0, 1(%rax,%r14)
	movslq	%r15d, %rax
	leaq	0(%r13,%rax,2), %rax
	cmpq	%rax, -136(%rbp)
	jne	.L559
.L676:
	movabsq	$4611686018427387903, %rax
	testl	%r15d, %r15d
	je	.L560
	cmpq	%rax, -88(%rbp)
	je	.L564
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leal	1(%r15), %edx
	cmpl	$7, %r15d
	je	.L582
	movslq	%r15d, %rax
	leaq	0(%r13,%rax,2), %rax
	cmpw	$0, 2(%rax)
	je	.L678
.L602:
	movl	%edx, %r15d
.L563:
	movslq	%r15d, %rax
	leaq	0(%r13,%rax,2), %rax
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%rsi, %r8
	xorl	%edx, %edx
	jmp	.L526
	.p2align 4,,10
	.p2align 3
.L671:
	movzbl	16(%r13), %eax
	movb	%al, 16(%rbx)
	movq	8(%r13), %rdx
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L667:
	movzbl	-112(%rbp), %eax
	movb	%al, (%rdi)
	movq	-120(%rbp), %rdx
	movq	(%rbx), %rdi
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L678:
	addl	$2, %r15d
	cmpl	$8, %r15d
	jne	.L587
	.p2align 4,,10
	.p2align 3
.L582:
	movq	-88(%rbp), %r14
	movq	-96(%rbp), %rdx
	movl	$15, %eax
	cmpq	-152(%rbp), %rdx
	cmovne	-80(%rbp), %rax
	leaq	1(%r14), %r15
	cmpq	%rax, %r15
	ja	.L679
.L569:
	movb	$93, (%rdx,%r14)
	movq	-96(%rbp), %rax
	movq	%r15, -88(%rbp)
	movb	$0, 1(%rax,%r14)
	movq	-96(%rbp), %rax
	movq	-88(%rbp), %rdx
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L677:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	%r9, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-96(%rbp), %rdx
	movq	-160(%rbp), %r9
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L665:
	cmpl	%r8d, %edx
	cmova	-136(%rbp), %rcx
	movq	%rcx, -136(%rbp)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L560:
	subq	-88(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L564
	movl	$2, %edx
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r15d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	cmpw	$0, 2(%r13)
	movq	%r13, %rax
	jne	.L563
	movl	$2, %r15d
.L587:
	cmpw	$0, 4(%rax)
	jne	.L563
	leal	1(%r15), %edx
	cmpl	$7, %r15d
	je	.L582
	cmpw	$0, 6(%rax)
	jne	.L602
	leal	2(%r15), %edx
	cmpl	$6, %r15d
	je	.L582
	cmpw	$0, 8(%rax)
	jne	.L602
	leal	3(%r15), %edx
	cmpl	$5, %r15d
	je	.L582
	cmpw	$0, 10(%rax)
	jne	.L602
	leal	4(%r15), %edx
	cmpl	$4, %r15d
	je	.L582
	cmpw	$0, 12(%rax)
	jne	.L602
	cmpl	$3, %r15d
	je	.L582
	cmpw	$0, 14(%rax)
	je	.L582
	movl	$7, %r15d
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L673:
	cmpl	$2, %edx
	jne	.L680
	cmpw	$0, 4(%rsi)
	leaq	4(%rsi), %rdi
	je	.L531
	movq	%rcx, -136(%rbp)
.L534:
	cmpw	$0, 2(%rdi)
	leaq	2(%rdi), %rsi
	jne	.L540
.L537:
	movq	-136(%rbp), %rcx
	movq	%rsi, -136(%rbp)
	movl	%edx, %r8d
	xorl	%edx, %edx
.L535:
	addl	$1, %edx
	cmpw	$0, 2(%rsi)
	leaq	2(%rsi), %r9
	je	.L541
	cmpl	%r8d, %edx
	jbe	.L681
	cmpw	$0, 4(%rsi)
	leaq	4(%rsi), %rdi
	jne	.L555
.L549:
	movq	-136(%rbp), %rcx
	movl	%edx, %r8d
	xorl	%edx, %edx
	movq	%rdi, -136(%rbp)
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L672:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-96(%rbp), %rdx
	jmp	.L522
	.p2align 4,,10
	.p2align 3
.L679:
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-96(%rbp), %rdx
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L674:
	cmpl	$1, %edx
	je	.L664
	cmpw	$0, 4(%r8)
	leaq	4(%r8), %rsi
	je	.L537
.L540:
	cmpw	$0, 2(%rsi)
	leaq	2(%rsi), %r9
	je	.L543
.L546:
	cmpw	$0, 2(%r9)
	leaq	2(%r9), %rdi
	je	.L549
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%r8, -136(%rbp)
	movq	%r8, %rcx
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L664:
	movq	$0, -136(%rbp)
	jmp	.L534
.L669:
	call	__stack_chk_fail@PLT
.L564:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L681:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %edx
	jmp	.L546
.L675:
	movq	%rcx, -136(%rbp)
	movl	%r8d, %edx
	jmp	.L540
	.cfi_endproc
.LFE7456:
	.size	_ZN4node3url12_GLOBAL__N_17URLHost12ToStringMoveEv, .-_ZN4node3url12_GLOBAL__N_17URLHost12ToStringMoveEv
	.section	.rodata.str1.1
.LC14:
	.string	""
	.text
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$1144, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L707
	cmpw	$1040, %cx
	jne	.L683
.L707:
	movq	23(%rdx), %r12
.L685:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L733
	leaq	_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L733:
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L686
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L686
	movq	352(%r12), %rsi
	leaq	-1088(%rbp), %rdi
	leaq	-1136(%rbp), %r13
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %rdx
	movl	$1, %r8d
	movq	%r13, %rdi
	movq	-1072(%rbp), %rsi
	movl	$1, %ecx
	movl	$0, -1136(%rbp)
	movl	$0, -1104(%rbp)
	call	_ZN4node3url12_GLOBAL__N_17URLHost9ParseHostEPKcmbb.constprop.0
	movl	-1104(%rbp), %edx
	testl	%edx, %edx
	je	.L734
	leaq	-1168(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node3url12_GLOBAL__N_17URLHost12ToStringMoveEv
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%rbx), %rbx
	movq	352(%r12), %rdi
	movq	-1168(%rbp), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L735
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L703:
	movq	-1168(%rbp), %rdi
	leaq	-1152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L697
	call	_ZdlPv@PLT
.L697:
	movl	-1104(%rbp), %eax
	cmpl	$1, %eax
	je	.L698
	cmpl	$4, %eax
	je	.L698
.L699:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L682
	testq	%rdi, %rdi
	je	.L682
	call	free@PLT
.L682:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L736
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L698:
	.cfi_restore_state
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L699
	call	_ZdlPv@PLT
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L734:
	movq	352(%r12), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	(%rbx), %rbx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L737
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L686:
	leaq	_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L683:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L735:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L703
	.p2align 4,,10
	.p2align 3
.L737:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L697
.L736:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7497:
	.size	_ZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7496:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$1144, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L763
	cmpw	$1040, %cx
	jne	.L739
.L763:
	movq	23(%rdx), %r12
.L741:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L789
	leaq	_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L789:
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L742
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L742
	movq	352(%r12), %rsi
	leaq	-1088(%rbp), %rdi
	leaq	-1136(%rbp), %r13
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movq	%r13, %rdi
	movq	-1088(%rbp), %rdx
	movq	-1072(%rbp), %rsi
	movl	$0, -1136(%rbp)
	movl	$0, -1104(%rbp)
	call	_ZN4node3url12_GLOBAL__N_17URLHost9ParseHostEPKcmbb.constprop.0
	movl	-1104(%rbp), %edx
	testl	%edx, %edx
	je	.L790
	leaq	-1168(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node3url12_GLOBAL__N_17URLHost12ToStringMoveEv
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%rbx), %rbx
	movq	352(%r12), %rdi
	movq	-1168(%rbp), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L791
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L759:
	movq	-1168(%rbp), %rdi
	leaq	-1152(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L753
	call	_ZdlPv@PLT
.L753:
	movl	-1104(%rbp), %eax
	cmpl	$1, %eax
	je	.L754
	cmpl	$4, %eax
	je	.L754
.L755:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L738
	testq	%rdi, %rdi
	je	.L738
	call	free@PLT
.L738:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L792
	addq	$1144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L754:
	.cfi_restore_state
	movq	-1136(%rbp), %rdi
	leaq	-1120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L755
	call	_ZdlPv@PLT
	jmp	.L755
	.p2align 4,,10
	.p2align 3
.L790:
	movq	352(%r12), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	(%rbx), %rbx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L793
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L742:
	leaq	_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L739:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L741
	.p2align 4,,10
	.p2align 3
.L791:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L759
	.p2align 4,,10
	.p2align 3
.L793:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L753
.L792:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7496:
	.size	_ZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0, @function
_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0:
.LFB9936:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$80, %rsp
	movq	8(%rdi), %r9
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%r9, %r9
	jne	.L795
	movq	(%rsi), %rax
	movq	$0, 8(%rsi)
	movb	$0, (%rax)
	movl	$1, %eax
.L794:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L819
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L795:
	.cfi_restore_state
	movq	(%rdi), %rsi
	leaq	-64(%rbp), %r12
	movzbl	%dl, %ecx
	xorl	%r8d, %r8d
	movq	%r9, %rdx
	movq	%r12, %rdi
	movl	$0, -64(%rbp)
	movl	$0, -32(%rbp)
	call	_ZN4node3url12_GLOBAL__N_17URLHost9ParseHostEPKcmbb.constprop.0
	movl	-32(%rbp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L794
	leaq	-96(%rbp), %rdi
	movq	%r12, %rsi
	leaq	-80(%rbp), %r12
	call	_ZN4node3url12_GLOBAL__N_17URLHost12ToStringMoveEv
	movq	-96(%rbp), %rdx
	movq	(%rbx), %rdi
	cmpq	%r12, %rdx
	je	.L820
	leaq	16(%rbx), %rsi
	movq	-80(%rbp), %rcx
	movq	-88(%rbp), %rax
	cmpq	%rsi, %rdi
	je	.L821
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	16(%rbx), %rsi
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%rdi, %rdi
	je	.L802
	movq	%rdi, -96(%rbp)
	movq	%rsi, -80(%rbp)
.L800:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L803
	call	_ZdlPv@PLT
.L803:
	movl	-32(%rbp), %eax
	cmpl	$1, %eax
	je	.L804
	cmpl	$4, %eax
	jne	.L808
.L804:
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L808
	call	_ZdlPv@PLT
.L808:
	movl	$1, %eax
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L820:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L798
	cmpq	$1, %rdx
	je	.L822
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	(%rbx), %rdi
.L798:
	movq	%rdx, 8(%rbx)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L821:
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	%rdx, (%rbx)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 8(%rbx)
.L802:
	movq	%r12, -96(%rbp)
	leaq	-80(%rbp), %r12
	movq	%r12, %rdi
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L822:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	(%rbx), %rdi
	jmp	.L798
.L819:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9936:
	.size	_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0, .-_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0
	.section	.rodata.str1.1
.LC15:
	.string	"parse"
.LC16:
	.string	"encodeAuth"
.LC17:
	.string	"toUSVString"
.LC18:
	.string	"domainToASCII"
.LC19:
	.string	"domainToUnicode"
.LC20:
	.string	"setURLConstructor"
.LC21:
	.string	"URL_FLAGS_NONE"
.LC23:
	.string	"URL_FLAGS_FAILED"
.LC25:
	.string	"URL_FLAGS_CANNOT_BE_BASE"
.LC27:
	.string	"URL_FLAGS_INVALID_PARSE_STATE"
.LC29:
	.string	"URL_FLAGS_TERMINATED"
.LC31:
	.string	"URL_FLAGS_SPECIAL"
.LC33:
	.string	"URL_FLAGS_HAS_USERNAME"
.LC35:
	.string	"URL_FLAGS_HAS_PASSWORD"
.LC37:
	.string	"URL_FLAGS_HAS_HOST"
.LC39:
	.string	"URL_FLAGS_HAS_PATH"
.LC40:
	.string	"URL_FLAGS_HAS_QUERY"
.LC42:
	.string	"URL_FLAGS_HAS_FRAGMENT"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC44:
	.string	"URL_FLAGS_IS_DEFAULT_SCHEME_PORT"
	.section	.rodata.str1.1
.LC46:
	.string	"kSchemeStart"
.LC47:
	.string	"kScheme"
.LC48:
	.string	"kNoScheme"
.LC49:
	.string	"kSpecialRelativeOrAuthority"
.LC51:
	.string	"kPathOrAuthority"
.LC52:
	.string	"kRelative"
.LC54:
	.string	"kRelativeSlash"
.LC56:
	.string	"kSpecialAuthoritySlashes"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"kSpecialAuthorityIgnoreSlashes"
	.section	.rodata.str1.1
.LC59:
	.string	"kAuthority"
.LC61:
	.string	"kHost"
.LC63:
	.string	"kHostname"
.LC65:
	.string	"kPort"
.LC67:
	.string	"kFile"
.LC69:
	.string	"kFileSlash"
.LC71:
	.string	"kFileHost"
.LC73:
	.string	"kPathStart"
.LC74:
	.string	"kPath"
.LC76:
	.string	"kCannotBeBase"
.LC78:
	.string	"kQuery"
.LC80:
	.string	"kFragment"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB82:
	.text
.LHOTB82:
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, @function
_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv:
.LFB7499:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L824
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L824
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L824
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L915
.L825:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L916
.L826:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L917
.L827:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L918
.L828:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L919
.L829:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L920
.L830:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L921
.L831:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L922
.L832:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L923
.L833:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L924
.L834:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L925
.L835:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L926
.L836:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L927
.L837:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L928
.L838:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L929
.L839:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L930
.L840:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L931
.L841:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L932
.L842:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L933
.L843:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L934
.L844:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L935
.L845:
	movq	.LC24(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L936
.L846:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC25(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L937
.L847:
	movq	.LC26(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L938
.L848:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC27(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L939
.L849:
	movq	.LC28(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L940
.L850:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC29(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L941
.L851:
	movq	.LC30(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L942
.L852:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC31(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L943
.L853:
	movq	.LC32(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L944
.L854:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC33(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L945
.L855:
	movsd	.LC34(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L946
.L856:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC35(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L947
.L857:
	movsd	.LC36(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L948
.L858:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC37(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L949
.L859:
	movsd	.LC38(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L950
.L860:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC39(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L951
.L861:
	movsd	.LC9(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L952
.L862:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC40(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L953
.L863:
	movsd	.LC41(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L954
.L864:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC42(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L955
.L865:
	movsd	.LC43(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L956
.L866:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC44(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L957
.L867:
	movsd	.LC45(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L958
.L868:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC46(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L959
.L869:
	movq	%r13, %rdi
	pxor	%xmm0, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L960
.L870:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC47(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L961
.L871:
	movq	.LC24(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L962
.L872:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC48(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L963
.L873:
	movq	.LC26(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L964
.L874:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC49(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L965
.L875:
	movsd	.LC50(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L966
.L876:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC51(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L967
.L877:
	movq	.LC28(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L968
.L878:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC52(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L969
.L879:
	movsd	.LC53(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L970
.L880:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC54(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L971
.L881:
	movsd	.LC55(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L972
.L882:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC56(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L973
.L883:
	movsd	.LC57(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L974
.L884:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC58(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L975
.L885:
	movq	.LC30(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L976
.L886:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC59(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L977
.L887:
	movsd	.LC60(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L978
.L888:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC61(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L979
.L889:
	movsd	.LC62(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L980
.L890:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC63(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L981
.L891:
	movsd	.LC64(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L982
.L892:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC65(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L983
.L893:
	movsd	.LC66(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L984
.L894:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC67(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L985
.L895:
	movsd	.LC68(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L986
.L896:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC69(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L987
.L897:
	movsd	.LC70(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L988
.L898:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC71(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L989
.L899:
	movsd	.LC72(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L990
.L900:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC73(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L991
.L901:
	movq	.LC32(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L992
.L902:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC74(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L993
.L903:
	movsd	.LC75(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L994
.L904:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC76(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L995
.L905:
	movsd	.LC77(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L996
.L906:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC78(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L997
.L907:
	movsd	.LC79(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L998
.L908:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r13
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	leaq	.LC80(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L999
.L909:
	movsd	.LC81(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1000
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L915:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L916:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L826
	.p2align 4,,10
	.p2align 3
.L917:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L827
	.p2align 4,,10
	.p2align 3
.L918:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L919:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L920:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L830
	.p2align 4,,10
	.p2align 3
.L921:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L831
	.p2align 4,,10
	.p2align 3
.L922:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L923:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L924:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L834
	.p2align 4,,10
	.p2align 3
.L925:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L926:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L836
	.p2align 4,,10
	.p2align 3
.L927:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L928:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L929:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L839
	.p2align 4,,10
	.p2align 3
.L930:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L931:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L841
	.p2align 4,,10
	.p2align 3
.L932:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L933:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L843
	.p2align 4,,10
	.p2align 3
.L934:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L935:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L845
	.p2align 4,,10
	.p2align 3
.L936:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L846
	.p2align 4,,10
	.p2align 3
.L937:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L938:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L848
	.p2align 4,,10
	.p2align 3
.L939:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L940:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L941:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L942:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L943:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L944:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L945:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L946:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L947:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L948:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L949:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L950:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L951:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L952:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L953:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L954:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L955:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L956:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L866
	.p2align 4,,10
	.p2align 3
.L957:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L958:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L868
	.p2align 4,,10
	.p2align 3
.L959:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L960:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L961:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L871
	.p2align 4,,10
	.p2align 3
.L962:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L872
	.p2align 4,,10
	.p2align 3
.L963:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L873
	.p2align 4,,10
	.p2align 3
.L964:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L874
	.p2align 4,,10
	.p2align 3
.L965:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L875
	.p2align 4,,10
	.p2align 3
.L966:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L967:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L968:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L878
	.p2align 4,,10
	.p2align 3
.L969:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L879
	.p2align 4,,10
	.p2align 3
.L970:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L880
	.p2align 4,,10
	.p2align 3
.L971:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L881
	.p2align 4,,10
	.p2align 3
.L972:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L882
	.p2align 4,,10
	.p2align 3
.L973:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L974:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L884
	.p2align 4,,10
	.p2align 3
.L975:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L885
	.p2align 4,,10
	.p2align 3
.L976:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L886
	.p2align 4,,10
	.p2align 3
.L977:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L978:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L979:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L889
	.p2align 4,,10
	.p2align 3
.L980:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L890
	.p2align 4,,10
	.p2align 3
.L981:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L891
	.p2align 4,,10
	.p2align 3
.L982:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L983:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L893
	.p2align 4,,10
	.p2align 3
.L984:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L985:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L986:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L896
	.p2align 4,,10
	.p2align 3
.L987:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L988:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L898
	.p2align 4,,10
	.p2align 3
.L989:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L899
	.p2align 4,,10
	.p2align 3
.L990:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L900
	.p2align 4,,10
	.p2align 3
.L991:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L901
	.p2align 4,,10
	.p2align 3
.L992:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L902
	.p2align 4,,10
	.p2align 3
.L993:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L994:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L904
	.p2align 4,,10
	.p2align 3
.L995:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L905
	.p2align 4,,10
	.p2align 3
.L996:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L906
	.p2align 4,,10
	.p2align 3
.L997:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L998:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L908
	.p2align 4,,10
	.p2align 3
.L999:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L1000:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, @function
_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold:
.LFSB7499:
.L824:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7499:
	.text
	.size	_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv, .-_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold, .-_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv.cold
.LCOLDE82:
	.text
.LHOTE82:
	.p2align 4
	.globl	_ZN4node10Utf8StringEPN2v87IsolateERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node10Utf8StringEPN2v87IsolateERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node10Utf8StringEPN2v87IsolateERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB7409:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	8(%rsi), %ecx
	movq	(%rsi), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1004
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1004:
	.cfi_restore_state
	movq	%rax, -8(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-8(%rbp), %rax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7409:
	.size	_ZN4node10Utf8StringEPN2v87IsolateERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node10Utf8StringEPN2v87IsolateERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.rodata.str1.1
.LC83:
	.string	"/"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node3url3URL10ToFilePathB5cxx11Ev
	.type	_ZNK4node3url3URL10ToFilePathB5cxx11Ev, @function
_ZNK4node3url3URL10ToFilePathB5cxx11Ev:
.LFB7500:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	leaq	8(%rsi), %rdi
	leaq	.LC2(%rip), %rsi
	pushq	%rbx
	subq	$136, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1030
	testb	$-128, (%r14)
	jne	.L1031
.L1008:
	movq	208(%r14), %rax
	movq	200(%r14), %rbx
	leaq	-144(%rbp), %r13
	movq	$0, -152(%rbp)
	movq	%r13, -160(%rbp)
	movb	$0, -144(%rbp)
	movq	%rax, -168(%rbp)
	cmpq	%rbx, %rax
	je	.L1009
	leaq	-128(%rbp), %r15
.L1020:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node3url12_GLOBAL__N_113PercentDecodeEPKcm
	movq	-128(%rbp), %rdi
	movq	-120(%rbp), %rsi
	leaq	(%rdi,%rsi), %rdx
	cmpq	%rdi, %rdx
	je	.L1010
	movq	%rdi, %rax
	jmp	.L1014
	.p2align 4,,10
	.p2align 3
.L1011:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L1010
.L1014:
	cmpb	$47, (%rax)
	jne	.L1011
	leaq	16(%r12), %rax
	movb	$0, 16(%r12)
	movq	%rax, (%r12)
	leaq	-112(%rbp), %rax
	movq	$0, 8(%r12)
	cmpq	%rax, %rdi
	je	.L1012
	call	_ZdlPv@PLT
.L1012:
	movq	-160(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L1005
	call	_ZdlPv@PLT
.L1005:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1032
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1010:
	.cfi_restore_state
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %rax
	addq	$1, %rsi
	movb	$0, -80(%rbp)
	movq	%r14, %rdi
	movq	%rax, -176(%rbp)
	movq	%rax, -96(%rbp)
	movq	$0, -88(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -88(%rbp)
	je	.L1033
	movl	$1, %edx
	leaq	.LC83(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rsi
	leaq	-160(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rdi
	cmpq	-176(%rbp), %rdi
	je	.L1016
	call	_ZdlPv@PLT
.L1016:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1017
	call	_ZdlPv@PLT
	addq	$32, %rbx
	cmpq	%rbx, -168(%rbp)
	jne	.L1020
.L1019:
	movq	-160(%rbp), %rax
	leaq	16(%r12), %rcx
	movq	-152(%rbp), %rdx
	movq	%rcx, (%r12)
	cmpq	%r13, %rax
	je	.L1023
	movq	%rax, (%r12)
	movq	-144(%rbp), %rax
	movq	%rax, 16(%r12)
.L1022:
	movq	%rdx, 8(%r12)
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1031:
	cmpq	$0, 112(%r14)
	je	.L1008
.L1030:
	leaq	16(%r12), %rax
	movb	$0, 16(%r12)
	movq	%rax, (%r12)
	movq	$0, 8(%r12)
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1017:
	addq	$32, %rbx
	cmpq	%rbx, -168(%rbp)
	jne	.L1020
	jmp	.L1019
.L1009:
	leaq	16(%r12), %rax
	xorl	%edx, %edx
	movq	%rax, (%r12)
.L1023:
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L1022
.L1033:
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1032:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7500:
	.size	_ZNK4node3url3URL10ToFilePathB5cxx11Ev, .-_ZNK4node3url3URL10ToFilePathB5cxx11Ev
	.p2align 4
	.globl	_Z13_register_urlv
	.type	_Z13_register_urlv, @function
_Z13_register_urlv:
.LFB7509:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7509:
	.size	_Z13_register_urlv, .-_Z13_register_urlv
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_.str1.8,"aMS",@progbits,1
	.align 8
.LC84:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_:
.LFB8291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	cmpq	%rdi, %rsi
	je	.L1036
	movq	8(%rsi), %rbx
	movq	(%rsi), %r12
	movq	(%rdi), %r14
	movq	16(%rdi), %rdx
	movq	%rbx, %rax
	subq	%r12, %rax
	subq	%r14, %rdx
	movq	%rax, %rcx
	sarq	$5, %rdx
	movq	%rax, -80(%rbp)
	sarq	$5, %rcx
	movq	%rcx, %r15
	cmpq	%rcx, %rdx
	jb	.L1106
	movq	8(%rdi), %r8
	movq	%r8, %rcx
	subq	%r14, %rcx
	movq	%rcx, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %r15
	ja	.L1055
	cmpq	$0, -80(%rbp)
	movq	%r14, %rbx
	jle	.L1105
	.p2align 4,,10
	.p2align 3
.L1056:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r8, -72(%rbp)
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	addq	$32, %rbx
	subq	$1, %r15
	movq	-72(%rbp), %r8
	jne	.L1056
	movq	-80(%rbp), %rax
	movl	$32, %edx
	testq	%rax, %rax
	cmovg	%rax, %rdx
	addq	%rdx, %r14
	.p2align 4,,10
	.p2align 3
.L1105:
	cmpq	%r14, %r8
	je	.L1103
.L1063:
	movq	(%r14), %rdi
	leaq	16(%r14), %rax
	cmpq	%rax, %rdi
	je	.L1060
	movq	%r8, -72(%rbp)
	addq	$32, %r14
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
	cmpq	%r14, %r8
	jne	.L1063
.L1103:
	movq	-80(%rbp), %r14
	addq	0(%r13), %r14
.L1054:
	movq	%r14, 8(%r13)
.L1036:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1107
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L1074
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rcx
	ja	.L1108
	movq	-80(%rbp), %rdi
	call	_Znwm@PLT
	movq	%rax, -96(%rbp)
.L1038:
	movq	%r12, %r15
	movq	-96(%rbp), %r14
	leaq	-64(%rbp), %r12
	cmpq	%rbx, %r15
	jne	.L1048
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1044:
	cmpq	$1, %r8
	jne	.L1046
	movzbl	(%r10), %eax
	movb	%al, 16(%r14)
.L1047:
	addq	$32, %r15
	movq	%r8, 8(%r14)
	addq	$32, %r14
	movb	$0, (%rdi,%r8)
	cmpq	%r15, %rbx
	je	.L1049
.L1048:
	leaq	16(%r14), %rdi
	movq	8(%r15), %r8
	movq	%rdi, (%r14)
	movq	(%r15), %r10
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L1043
	testq	%r10, %r10
	je	.L1067
.L1043:
	movq	%r8, -64(%rbp)
	cmpq	$15, %r8
	jbe	.L1044
	movq	%r14, %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r8, -88(%rbp)
	movq	%r10, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r10
	movq	-88(%rbp), %r8
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r14)
.L1045:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	(%r14), %rdi
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1060:
	addq	$32, %r14
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1055:
	testq	%rcx, %rcx
	jle	.L1064
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	%rdx, -72(%rbp)
	addq	$32, %r12
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-72(%rbp), %rdx
	addq	$32, %r14
	subq	$1, %rdx
	jne	.L1065
	movq	-88(%rbp), %rax
	movq	8(%r13), %r8
	movq	0(%r13), %r14
	movq	%r8, %rcx
	movq	8(%rax), %rbx
	movq	(%rax), %r12
	subq	%r14, %rcx
.L1064:
	leaq	-64(%rbp), %rax
	leaq	(%r12,%rcx), %r15
	addq	-80(%rbp), %r14
	movq	%rax, -88(%rbp)
	cmpq	%rbx, %r15
	jne	.L1066
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1110:
	movzbl	(%r14), %eax
	movb	%al, 16(%r8)
.L1072:
	addq	$32, %r15
	movq	%r12, 8(%r8)
	addq	$32, %r8
	movb	$0, (%rdi,%r12)
	cmpq	%r15, %rbx
	je	.L1103
.L1066:
	leaq	16(%r8), %rdi
	movq	8(%r15), %r12
	movq	%rdi, (%r8)
	movq	(%r15), %r14
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L1076
	testq	%r14, %r14
	je	.L1067
.L1076:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L1109
	cmpq	$1, %r12
	je	.L1110
	testq	%r12, %r12
	je	.L1072
	jmp	.L1070
	.p2align 4,,10
	.p2align 3
.L1109:
	movq	-88(%rbp), %rsi
	movq	%r8, %rdi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %rdi
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L1070:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r12
	movq	(%r8), %rdi
	jmp	.L1072
	.p2align 4,,10
	.p2align 3
.L1074:
	movq	$0, -96(%rbp)
	jmp	.L1038
	.p2align 4,,10
	.p2align 3
.L1046:
	testq	%r8, %r8
	je	.L1047
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	8(%r13), %rbx
	movq	0(%r13), %r12
	cmpq	%r12, %rbx
	je	.L1041
	.p2align 4,,10
	.p2align 3
.L1042:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1050
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1042
.L1051:
	movq	0(%r13), %r12
.L1041:
	testq	%r12, %r12
	je	.L1053
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L1053:
	movq	-96(%rbp), %r14
	movq	%r14, 0(%r13)
	addq	-80(%rbp), %r14
	movq	%r14, 16(%r13)
	jmp	.L1054
	.p2align 4,,10
	.p2align 3
.L1050:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L1042
	jmp	.L1051
.L1067:
	leaq	.LC84(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1107:
	call	__stack_chk_fail@PLT
.L1108:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8291:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_.str1.1,"aMS",@progbits,1
.LC85:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_:
.LFB8298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	cmpq	16(%rdi), %r12
	je	.L1112
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
	movq	(%rsi), %rdx
	leaq	16(%rsi), %rax
	cmpq	%rax, %rdx
	je	.L1136
	movq	%rdx, (%r12)
	movq	16(%rsi), %rdx
	movq	%rdx, 16(%r12)
.L1114:
	movq	8(%rbx), %rdx
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	movq	%rdx, 8(%r12)
	movb	$0, 16(%rbx)
	addq	$32, 8(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1136:
	.cfi_restore_state
	movdqu	16(%rsi), %xmm2
	movups	%xmm2, 16(%r12)
	jmp	.L1114
	.p2align 4,,10
	.p2align 3
.L1112:
	movabsq	$288230376151711743, %rsi
	movq	(%rdi), %r15
	movq	%r12, %rdx
	subq	%r15, %rdx
	movq	%rdx, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L1137
	testq	%rax, %rax
	je	.L1127
	movabsq	$9223372036854775776, %rcx
	leaq	(%rax,%rax), %rdi
	cmpq	%rdi, %rax
	jbe	.L1138
.L1117:
	movq	%rcx, %rdi
	movq	%rdx, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %rdx
	movq	%rax, %r14
	leaq	(%rax,%rcx), %rax
	leaq	32(%r14), %rcx
.L1118:
	addq	%r14, %rdx
	movq	(%rbx), %rdi
	leaq	16(%rdx), %rsi
	movq	%rsi, (%rdx)
	leaq	16(%rbx), %rsi
	cmpq	%rsi, %rdi
	je	.L1139
	movq	%rdi, (%rdx)
	movq	16(%rbx), %rdi
	movq	%rdi, 16(%rdx)
.L1120:
	movq	8(%rbx), %rdi
	movq	%rsi, (%rbx)
	movq	$0, 8(%rbx)
	movq	%rdi, 8(%rdx)
	movb	$0, 16(%rbx)
	cmpq	%r15, %r12
	je	.L1121
	movq	%r14, %rcx
	movq	%r15, %rdx
	jmp	.L1125
	.p2align 4,,10
	.p2align 3
.L1122:
	movq	%rsi, (%rcx)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rcx)
.L1135:
	movq	8(%rdx), %rsi
	addq	$32, %rdx
	addq	$32, %rcx
	movq	%rsi, -24(%rcx)
	cmpq	%rdx, %r12
	je	.L1140
.L1125:
	leaq	16(%rcx), %rsi
	leaq	16(%rdx), %rdi
	movq	%rsi, (%rcx)
	movq	(%rdx), %rsi
	cmpq	%rdi, %rsi
	jne	.L1122
	movdqu	16(%rdx), %xmm1
	movups	%xmm1, 16(%rcx)
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1140:
	subq	%r15, %r12
	leaq	32(%r14,%r12), %rcx
.L1121:
	testq	%r15, %r15
	je	.L1126
	movq	%r15, %rdi
	movq	%rax, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rcx
.L1126:
	movq	%r14, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 16(%r13)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 0(%r13)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1138:
	.cfi_restore_state
	testq	%rdi, %rdi
	jne	.L1141
	movl	$32, %ecx
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1127:
	movl	$32, %ecx
	jmp	.L1117
	.p2align 4,,10
	.p2align 3
.L1139:
	movdqu	16(%rbx), %xmm4
	movups	%xmm4, 16(%rdx)
	jmp	.L1120
.L1137:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1141:
	cmpq	%rsi, %rdi
	movq	%rsi, %rax
	cmovbe	%rdi, %rax
	salq	$5, %rax
	movq	%rax, %rcx
	jmp	.L1117
	.cfi_endproc
.LFE8298:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	.section	.text._ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE,"axG",@progbits,_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE,comdat
	.p2align 4
	.weak	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	.type	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE, @function
_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE:
.LFB8307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$1128, %rsp
	movq	%rdi, -1144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1196
.L1143:
	leaq	-1136(%rbp), %r14
	movq	%r12, %rsi
	leaq	-1080(%rbp), %r13
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	(%rbx), %rcx
	movq	8(%rbx), %r15
	movq	%r13, %rax
	movdqa	.LC86(%rip), %xmm0
	movq	%r13, -1088(%rbp)
	leaq	-56(%rbp), %rdx
	subq	%rcx, %r15
	sarq	$5, %r15
	movaps	%xmm0, -1104(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r15, %r9
	.p2align 4,,10
	.p2align 3
.L1144:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L1144
	movq	$0, -1080(%rbp)
	cmpq	$128, %r15
	jbe	.L1151
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r15,8), %rdi
	andq	%r15, %rax
	cmpq	%rax, %r15
	jne	.L1197
	movq	%rcx, -1160(%rbp)
	movq	%r9, -1152(%rbp)
	testq	%rdi, %rdi
	je	.L1147
	movq	%rdi, -1168(%rbp)
	call	malloc@PLT
	movq	-1168(%rbp), %rdi
	movq	-1152(%rbp), %r9
	testq	%rax, %rax
	movq	-1160(%rbp), %rcx
	je	.L1198
	movq	%rax, -1088(%rbp)
	movq	%r15, -1096(%rbp)
.L1151:
	movq	%r9, -1104(%rbp)
	testq	%r9, %r9
	je	.L1152
	xorl	%r15d, %r15d
	testq	%r12, %r12
	jne	.L1157
	jmp	.L1153
	.p2align 4,,10
	.p2align 3
.L1199:
	movq	%rax, (%rdx)
	movq	(%rbx), %rcx
	addq	$1, %r15
	movq	8(%rbx), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%r15, %rax
	jbe	.L1166
.L1157:
	movq	%r15, %rax
	salq	$5, %rax
	addq	%rcx, %rax
	movq	8(%rax), %rcx
	cmpq	$1073741798, %rcx
	ja	.L1154
	movq	(%rax), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r9
	cmpq	%r15, %r9
	jbe	.L1162
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	testq	%rax, %rax
	jne	.L1199
	jmp	.L1167
.L1192:
	movq	%rax, (%rdx)
	movq	(%rbx), %rcx
	addq	$1, %r15
	movq	8(%rbx), %rax
	subq	%rcx, %rax
	sarq	$5, %rax
	cmpq	%rax, %r15
	jnb	.L1166
.L1153:
	movq	%r15, %rsi
	movq	-1144(%rbp), %rdi
	salq	$5, %rsi
	addq	%rcx, %rsi
	movq	%rsi, -1152(%rbp)
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	-1152(%rbp), %rsi
	movq	%rax, %rdi
	movq	8(%rsi), %rcx
	cmpq	$1073741798, %rcx
	ja	.L1200
	movq	(%rsi), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-1104(%rbp), %r9
	cmpq	%r15, %r9
	jbe	.L1162
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
	testq	%rax, %rax
	jne	.L1192
	jmp	.L1167
.L1162:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1200:
	movq	%rax, %r12
.L1154:
	movq	%r12, %rdi
	call	_ZN4node21ThrowErrStringTooLongEPN2v87IsolateE@PLT
	cmpq	-1104(%rbp), %r15
	jnb	.L1162
	movq	-1088(%rbp), %rsi
	leaq	(%rsi,%r15,8), %rdx
.L1167:
	movq	$0, (%rdx)
	xorl	%r12d, %r12d
.L1165:
	testq	%rsi, %rsi
	je	.L1164
	cmpq	%r13, %rsi
	je	.L1164
	movq	%rsi, %rdi
	call	free@PLT
.L1164:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1201
	addq	$1128, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1152:
	.cfi_restore_state
	movq	-1088(%rbp), %rsi
.L1166:
	movq	%r12, %rdi
	movq	%r9, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	-1088(%rbp), %rsi
	movq	%rax, %r12
	jmp	.L1165
.L1196:
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%rax, %r12
	jmp	.L1143
.L1147:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1198:
	movq	%rdi, -1152(%rbp)
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-1152(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1147
	movq	(%rbx), %rcx
	movq	8(%rbx), %r9
	movq	%rdi, -1088(%rbp)
	movq	-1104(%rbp), %rax
	movq	%r15, -1096(%rbp)
	subq	%rcx, %r9
	sarq	$5, %r9
	testq	%rax, %rax
	je	.L1150
	leaq	0(,%rax,8), %rdx
	movq	%r13, %rsi
	movq	%r9, -1160(%rbp)
	movq	%rcx, -1152(%rbp)
	call	memcpy@PLT
	movq	-1160(%rbp), %r9
	movq	-1152(%rbp), %rcx
.L1150:
	movq	%r15, -1104(%rbp)
	cmpq	%r9, %r15
	jnb	.L1151
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1197:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8307:
	.size	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE, .-_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	.text
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_17SetArgsEPNS_11EnvironmentEPN2v85LocalINS4_5ValueEEERKNS0_8url_dataE, @function
_ZN4node3url12_GLOBAL__N_17SetArgsEPNS_11EnvironmentEPN2v85LocalINS4_5ValueEEERKNS0_8url_dataE:
.LFB7485:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$24, %rsp
	movq	352(%rdi), %r14
	movl	(%rdx), %esi
	movq	%r14, %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	%rax, (%r12)
	testb	$16, (%rbx)
	je	.L1203
	leaq	8(%rbx), %r15
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1204
	movq	360(%r13), %rax
	movq	1792(%rax), %rax
.L1212:
	movq	%rax, 8(%r12)
	movl	(%rbx), %eax
	testb	$32, %al
	jne	.L1246
	testb	$64, %al
	jne	.L1247
.L1216:
	testb	$-128, %al
	jne	.L1248
.L1218:
	testb	$2, %ah
	jne	.L1249
.L1220:
	testb	$4, %ah
	jne	.L1250
.L1222:
	movl	4(%rbx), %esi
	testl	%esi, %esi
	js	.L1224
	movq	%r14, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, 40(%r12)
.L1224:
	testl	$256, (%rbx)
	jne	.L1251
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1203:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L1212
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	3280(%r13), %rdi
	xorl	%edx, %edx
	leaq	200(%rbx), %rsi
	call	_ZN4node9ToV8ValueINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEEN2v810MaybeLocalINS7_5ValueEEENS7_5LocalINS7_7ContextEEERKSt6vectorIT_SaISF_EEPNS7_7IsolateE
	testq	%rax, %rax
	je	.L1252
.L1226:
	movq	%rax, 48(%r12)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1246:
	.cfi_restore_state
	movq	40(%rbx), %rsi
	movl	48(%rbx), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1253
.L1215:
	movq	%rax, 16(%r12)
	movl	(%rbx), %eax
	testb	$64, %al
	je	.L1216
.L1247:
	movq	72(%rbx), %rsi
	movl	80(%rbx), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1254
.L1217:
	movq	%rax, 24(%r12)
	movl	(%rbx), %eax
	testb	$-128, %al
	je	.L1218
.L1248:
	movq	104(%rbx), %rsi
	movl	112(%rbx), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1255
.L1219:
	movq	%rax, 32(%r12)
	movl	(%rbx), %eax
	testb	$2, %ah
	je	.L1220
.L1249:
	movq	136(%rbx), %rsi
	movl	144(%rbx), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1256
.L1221:
	movq	%rax, 56(%r12)
	movl	(%rbx), %eax
	testb	$4, %ah
	je	.L1222
.L1250:
	movq	168(%rbx), %rsi
	movl	176(%rbx), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L1257
.L1223:
	movq	%rax, 64(%r12)
	jmp	.L1222
	.p2align 4,,10
	.p2align 3
.L1204:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1206
	movq	360(%r13), %rax
	movq	1800(%rax), %rax
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1206:
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1207
	movq	360(%r13), %rax
	movq	1808(%rax), %rax
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1207:
	leaq	.LC4(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1208
	movq	360(%r13), %rax
	movq	1816(%rax), %rax
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1257:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1252:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1226
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1255:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1254:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1217
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rax
	jmp	.L1221
.L1208:
	leaq	.LC5(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1209
	movq	360(%r13), %rax
	movq	1824(%rax), %rax
	jmp	.L1212
.L1209:
	leaq	.LC6(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1210
	movq	360(%r13), %rax
	movq	1832(%rax), %rax
	jmp	.L1212
.L1210:
	leaq	.LC7(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L1211
	movq	360(%r13), %rax
	movq	1840(%rax), %rax
	jmp	.L1212
.L1211:
	leaq	_ZZN4node3url12_GLOBAL__N_110GetSpecialEPNS_11EnvironmentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7485:
	.size	_ZN4node3url12_GLOBAL__N_17SetArgsEPNS_11EnvironmentEPN2v85LocalINS4_5ValueEEERKNS0_8url_dataE, .-_ZN4node3url12_GLOBAL__N_17SetArgsEPNS_11EnvironmentEPN2v85LocalINS4_5ValueEEERKNS0_8url_dataE
	.align 2
	.p2align 4
	.globl	_ZNK4node3url3URL8ToObjectEPNS_11EnvironmentE
	.type	_ZNK4node3url3URL8ToObjectEPNS_11EnvironmentE, @function
_ZNK4node3url3URL8ToObjectEPNS_11EnvironmentE:
.LFB7508:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$168, %rsp
	movq	3280(%rsi), %r14
	movq	352(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	testb	$1, (%r12)
	je	.L1259
	xorl	%r12d, %r12d
.L1260:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1263
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1259:
	.cfi_restore_state
	leaq	-128(%rbp), %r8
	leaq	88(%r13), %r15
	movq	%r12, %rdx
	addq	$104, %r13
	movq	%r8, %rsi
	movq	%rbx, %rdi
	movq	%r8, -200(%rbp)
	movq	%r13, -96(%rbp)
	movq	%r13, -88(%rbp)
	movq	%r13, -72(%rbp)
	movq	%r13, -64(%rbp)
	leaq	-192(%rbp), %r13
	movq	%r15, -128(%rbp)
	movq	%r15, -120(%rbp)
	movq	%r15, -112(%rbp)
	movq	%r15, -104(%rbp)
	movq	%r15, -80(%rbp)
	call	_ZN4node3url12_GLOBAL__N_17SetArgsEPNS_11EnvironmentEPN2v85LocalINS4_5ValueEEERKNS0_8url_dataE
	movq	352(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	3048(%rbx), %rdi
	movl	$9, %ecx
	movq	%r15, %rdx
	movq	3280(%rbx), %rsi
	movq	-200(%rbp), %r8
	movq	%rbx, -144(%rbp)
	movl	$1, -136(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
	jmp	.L1260
.L1263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7508:
	.size	_ZNK4node3url3URL8ToObjectEPNS_11EnvironmentE, .-_ZNK4node3url3URL8ToObjectEPNS_11EnvironmentE
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8747:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L1306
	movq	%rbx, %r9
	movq	%rdi, %r15
	subq	%r14, %r9
	testq	%rax, %rax
	je	.L1286
	leaq	(%rax,%rax), %r8
	cmpq	%r8, %rax
	jbe	.L1307
	movabsq	$9223372036854775776, %r8
.L1266:
	movq	%r8, %rdi
	movq	%rcx, -96(%rbp)
	movq	%rdx, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %rdx
	movq	-96(%rbp), %rcx
	movq	%rax, %r13
.L1284:
	movq	(%rdx), %r11
	movq	(%rcx), %r10
	addq	%r13, %r9
	leaq	16(%r9), %rdi
	movq	%r11, %rax
	movq	%rdi, (%r9)
	addq	%r10, %rax
	je	.L1268
	testq	%r11, %r11
	je	.L1308
.L1268:
	movq	%r10, -64(%rbp)
	cmpq	$15, %r10
	ja	.L1309
	cmpq	$1, %r10
	jne	.L1271
	movzbl	(%r11), %eax
	movb	%al, 16(%r9)
.L1272:
	movq	%r10, 8(%r9)
	movb	$0, (%rdi,%r10)
	cmpq	%r14, %rbx
	je	.L1288
.L1313:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L1277
	.p2align 4,,10
	.p2align 3
.L1274:
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L1304:
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L1310
.L1277:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	jne	.L1274
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1307:
	testq	%r8, %r8
	jne	.L1267
	xorl	%r13d, %r13d
	jmp	.L1284
	.p2align 4,,10
	.p2align 3
.L1310:
	movq	%rbx, %r9
	subq	%r14, %r9
	addq	%r13, %r9
.L1273:
	addq	$32, %r9
	cmpq	%r12, %rbx
	je	.L1278
	movq	%rbx, %rax
	movq	%r9, %rdx
	.p2align 4,,10
	.p2align 3
.L1282:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	je	.L1311
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -16(%rdx)
	movq	-24(%rax), %rcx
	movq	%rcx, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L1282
.L1280:
	subq	%rbx, %r12
	addq	%r12, %r9
.L1278:
	testq	%r14, %r14
	je	.L1283
	movq	%r14, %rdi
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %r8
	movq	-72(%rbp), %r9
.L1283:
	movq	%r13, %xmm0
	addq	%r8, %r13
	movq	%r9, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1312
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1311:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rcx, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L1282
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1286:
	movl	$32, %r8d
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1271:
	testq	%r10, %r10
	jne	.L1270
	movq	%r10, 8(%r9)
	movb	$0, (%rdi,%r10)
	cmpq	%r14, %rbx
	jne	.L1313
.L1288:
	movq	%r13, %r9
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1309:
	movq	%r9, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r11
	movq	%rax, %rdi
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %r8
	movq	%rax, (%r9)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r9)
.L1270:
	movq	%r10, %rdx
	movq	%r11, %rsi
	movq	%r8, -80(%rbp)
	movq	%r9, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r10
	movq	-80(%rbp), %r8
	movq	(%r9), %rdi
	jmp	.L1272
.L1308:
	leaq	.LC84(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1267:
	cmpq	%rsi, %r8
	cmova	%rsi, %r8
	salq	$5, %r8
	jmp	.L1266
.L1312:
	call	__stack_chk_fail@PLT
.L1306:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8747:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r12
	movq	(%rdi), %r15
	movq	%rdx, -72(%rbp)
	movabsq	$288230376151711743, %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$5, %rax
	cmpq	%rdx, %rax
	je	.L1348
	movq	%rsi, %r8
	movq	%rdi, %r13
	movq	%rsi, %rbx
	subq	%r15, %r8
	testq	%rax, %rax
	je	.L1335
	movabsq	$9223372036854775776, %r14
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1349
.L1316:
	movq	%r14, %rdi
	movq	%r8, -88(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %r8
	movq	%rax, %rcx
	leaq	(%rax,%r14), %rax
	movq	%rax, -80(%rbp)
	leaq	32(%rcx), %r14
.L1333:
	addq	%rcx, %r8
	movq	-72(%rbp), %rdi
	movq	%rcx, -104(%rbp)
	leaq	16(%r8), %r11
	movq	%r8, -88(%rbp)
	movq	%r11, (%r8)
	movq	%r11, -96(%rbp)
	call	strlen@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %r11
	cmpq	$15, %rax
	movq	%rax, -64(%rbp)
	movq	-104(%rbp), %rcx
	movq	%rax, %r10
	ja	.L1350
	cmpq	$1, %rax
	jne	.L1320
	movq	-72(%rbp), %rdi
	movzbl	(%rdi), %edx
	movb	%dl, 16(%r8)
.L1321:
	movq	%rax, 8(%r8)
	movb	$0, (%r11,%rax)
	cmpq	%r15, %rbx
	je	.L1322
.L1352:
	movq	%rcx, %rdx
	movq	%r15, %rax
	jmp	.L1326
	.p2align 4,,10
	.p2align 3
.L1323:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L1346:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L1351
.L1326:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L1323
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1320:
	testq	%rax, %rax
	jne	.L1319
	movq	%rax, 8(%r8)
	movb	$0, (%r11,%rax)
	cmpq	%r15, %rbx
	jne	.L1352
	.p2align 4,,10
	.p2align 3
.L1322:
	cmpq	%r12, %rbx
	je	.L1327
	movq	%rbx, %rax
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L1331:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L1353
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L1331
.L1329:
	subq	%rbx, %r12
	addq	%r12, %r14
.L1327:
	testq	%r15, %r15
	je	.L1332
	movq	%r15, %rdi
	movq	%rcx, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %rcx
.L1332:
	movq	-80(%rbp), %rax
	movq	%rcx, %xmm0
	movq	%r14, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movq	%rax, 16(%r13)
	movups	%xmm0, 0(%r13)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1354
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1349:
	.cfi_restore_state
	testq	%rcx, %rcx
	jne	.L1317
	movq	$0, -80(%rbp)
	movl	$32, %r14d
	jmp	.L1333
	.p2align 4,,10
	.p2align 3
.L1353:
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L1331
	jmp	.L1329
	.p2align 4,,10
	.p2align 3
.L1351:
	movq	%rbx, %rax
	subq	%r15, %rax
	leaq	32(%rcx,%rax), %r14
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1335:
	movl	$32, %r14d
	jmp	.L1316
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-88(%rbp), %r8
	movq	-96(%rbp), %rcx
	movq	%rax, %r11
	movq	-104(%rbp), %r10
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L1319:
	movq	-72(%rbp), %rsi
	movq	%r11, %rdi
	movq	%r10, %rdx
	movq	%rcx, -96(%rbp)
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %r8
	movq	-64(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	(%r8), %r11
	jmp	.L1321
.L1317:
	cmpq	%rdx, %rcx
	cmovbe	%rcx, %rdx
	salq	$5, %rdx
	movq	%rdx, %r14
	jmp	.L1316
.L1354:
	call	__stack_chk_fail@PLT
.L1348:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8763:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_:
.LFB8289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	16(%rdi), %r13
	je	.L1356
	leaq	16(%r13), %rbx
	movq	%rsi, %rdi
	movq	%rbx, 0(%r13)
	call	strlen@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L1367
	cmpq	$1, %rax
	jne	.L1359
	movzbl	(%r15), %edx
	movb	%dl, 16(%r13)
.L1360:
	movq	%rax, 8(%r13)
	movb	$0, (%rbx,%rax)
	addq	$32, 8(%r12)
.L1355:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1368
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1367:
	.cfi_restore_state
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rbx
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r13)
.L1358:
	movq	%rbx, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %rax
	movq	0(%r13), %rbx
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1356:
	movq	%rsi, %rdx
	movq	%r13, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRA1_KcEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L1355
	.p2align 4,,10
	.p2align 3
.L1359:
	testq	%rax, %rax
	je	.L1360
	jmp	.L1358
.L1368:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8289:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB8774:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L1411
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L1391
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L1412
	movabsq	$9223372036854775776, %rcx
.L1371:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L1389:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L1373
	testq	%r10, %r10
	je	.L1413
.L1373:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L1414
	cmpq	$1, %r9
	jne	.L1376
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L1377:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L1393
.L1418:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L1382
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L1409:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L1415
.L1382:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L1379
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L1409
	.p2align 4,,10
	.p2align 3
.L1412:
	testq	%rcx, %rcx
	jne	.L1372
	xorl	%r13d, %r13d
	jmp	.L1389
	.p2align 4,,10
	.p2align 3
.L1415:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L1378:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L1383
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L1387:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L1416
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L1387
.L1385:
	subq	%rbx, %r12
	addq	%r12, %r8
.L1383:
	testq	%r14, %r14
	je	.L1388
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L1388:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1417
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1416:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L1387
	jmp	.L1385
	.p2align 4,,10
	.p2align 3
.L1391:
	movl	$32, %ecx
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1376:
	testq	%r9, %r9
	jne	.L1375
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L1418
.L1393:
	movq	%r13, %r8
	jmp	.L1378
	.p2align 4,,10
	.p2align 3
.L1414:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	%rax, %rdi
	movq	-88(%rbp), %r10
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L1375:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L1377
.L1413:
	leaq	.LC84(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1372:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L1371
.L1417:
	call	__stack_chk_fail@PLT
.L1411:
	leaq	.LC85(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8774:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.section	.rodata.str1.1
.LC87:
	.string	"%40"
.LC88:
	.string	"localhost"
.LC89:
	.string	".."
.LC90:
	.string	"."
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b
	.type	_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b, @function
_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b:
.LFB7484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r10
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	(%rdi,%rsi), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	subq	$248, %rsp
	.cfi_offset 3, -56
	movl	16(%rbp), %eax
	movl	%edx, -196(%rbp)
	movq	%r9, -216(%rbp)
	movl	%eax, -200(%rbp)
	movb	%al, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	%r8b, %r8b
	jne	.L1420
	cmpq	%r13, %rdi
	jb	.L1422
	.p2align 4,,10
	.p2align 3
.L1421:
	leaq	-1(%r13), %rax
	cmpq	%rax, %r10
	jbe	.L1424
	jmp	.L1423
	.p2align 4,,10
	.p2align 3
.L2402:
	subq	$1, %r13
	cmpq	%r10, %r13
	je	.L1423
.L1424:
	cmpb	$32, -1(%r13)
	jbe	.L2402
.L1423:
	movq	%r13, %rsi
	subq	%r10, %rsi
.L1420:
	leaq	-144(%rbp), %rax
	movb	$0, -144(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -160(%rbp)
	movq	$0, -152(%rbp)
	cmpq	%r13, %r10
	jnb	.L1859
	movq	%r10, %r14
	movzbl	(%r14), %eax
	leaq	1(%r14), %rbx
	leal	-9(%rax), %edx
	cmpb	$1, %dl
	jbe	.L1426
.L2403:
	cmpb	$13, %al
	je	.L1426
	cmpq	%rbx, %r13
	je	.L1860
	movq	%rbx, %r14
	movzbl	(%r14), %eax
	leaq	1(%r14), %rbx
	leal	-9(%rax), %edx
	cmpb	$1, %dl
	ja	.L2403
.L1426:
	subq	$1, %rsi
	leaq	-160(%rbp), %rdi
	movq	%r10, -224(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	-224(%rbp), %r10
	movq	%r14, %r8
	xorl	%esi, %esi
	movq	-152(%rbp), %rdx
	movq	-208(%rbp), %rdi
	subq	%r10, %r8
	movq	%r10, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	cmpq	%rbx, %r13
	jbe	.L1428
	movq	-152(%rbp), %rsi
	movq	-160(%rbp), %r8
	movq	%r12, -224(%rbp)
	movl	$15, %eax
	movq	-208(%rbp), %rdi
	movq	%rsi, %r12
	.p2align 4,,10
	.p2align 3
.L1433:
	movzbl	(%rbx), %r14d
	leal	-9(%r14), %edx
	cmpb	$1, %dl
	jbe	.L1429
	cmpb	$13, %r14b
	je	.L1429
	cmpq	-192(%rbp), %r8
	movq	%rax, %rdx
	leaq	1(%r12), %r15
	cmovne	-144(%rbp), %rdx
	cmpq	%rdx, %r15
	ja	.L2404
.L1431:
	movb	%r14b, (%r8,%r12)
	movq	-160(%rbp), %rdx
	movq	%r15, -152(%rbp)
	movb	$0, (%rdx,%r15)
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %r12
.L1429:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	jne	.L1433
	movq	%r12, %rsi
	movq	-224(%rbp), %r12
.L1432:
	leaq	(%r8,%rsi), %r13
	movq	%r8, %r10
.L1425:
	leaq	-112(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	%rax, -224(%rbp)
	movq	%rax, -128(%rbp)
	movl	-196(%rbp), %eax
	movb	$0, -112(%rbp)
	cmpl	$-1, %eax
	je	.L1862
	movl	%eax, %r14d
	cmpl	$20, %eax
	jbe	.L1434
	orl	$4, (%r12)
.L1438:
	movq	-160(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1419
	call	_ZdlPv@PLT
.L1419:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2405
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2407:
	.cfi_restore_state
	addq	$1, %r10
	cmpq	%r10, %r13
	je	.L2406
.L1422:
	cmpb	$32, (%r10)
	jbe	.L2407
	jmp	.L1421
	.p2align 4,,10
	.p2align 3
.L1862:
	xorl	%r14d, %r14d
.L1434:
	cmpq	%r13, %r10
	ja	.L1436
	leaq	72(%r12), %rax
	movb	$0, -235(%rbp)
	cmpl	$-1, -196(%rbp)
	movq	%r10, %r9
	movb	$0, -233(%rbp)
	movb	$0, -232(%rbp)
	movq	%rax, -248(%rbp)
	movq	%r10, -256(%rbp)
	setne	-234(%rbp)
.L1437:
	movl	(%r12), %esi
	movl	%esi, %r15d
	movl	%esi, %ecx
	andl	$16, %r15d
	setne	-208(%rbp)
	movzbl	-208(%rbp), %eax
	cmpq	%r9, %r13
	jbe	.L1463
	movzbl	(%r9), %ebx
	cmpb	$92, %bl
	sete	%r11b
	andl	%r11d, %eax
	movl	%eax, %edx
	cmpl	$20, %r14d
	ja	.L1464
	leaq	.L1466(%rip), %r8
	movl	%r14d, %edi
	movslq	(%r8,%rdi,4), %rax
	addq	%r8, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1466:
	.long	.L1464-.L1466
	.long	.L1477-.L1466
	.long	.L1458-.L1466
	.long	.L1893-.L1466
	.long	.L1475-.L1466
	.long	.L2051-.L1466
	.long	.L1474-.L1466
	.long	.L1473-.L1466
	.long	.L1808-.L1466
	.long	.L1894-.L1466
	.long	.L1472-.L1466
	.long	.L1472-.L1466
	.long	.L1471-.L1466
	.long	.L2052-.L1466
	.long	.L1895-.L1466
	.long	.L1449-.L1466
	.long	.L2343-.L1466
	.long	.L2329-.L1466
	.long	.L1468-.L1466
	.long	.L2303-.L1466
	.long	.L1897-.L1466
	.text
	.p2align 4,,10
	.p2align 3
.L1463:
	cmpl	$20, %r14d
	ja	.L1478
	leaq	.L1842(%rip), %rdi
	movl	%r14d, %edx
	movslq	(%rdi,%rdx,4), %rax
	addq	%rdi, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1842:
	.long	.L1478-.L1842
	.long	.L1953-.L1842
	.long	.L1954-.L1842
	.long	.L1844-.L1842
	.long	.L1453-.L1842
	.long	.L2359-.L1842
	.long	.L1843-.L1842
	.long	.L1462-.L1842
	.long	.L1955-.L1842
	.long	.L1818-.L1842
	.long	.L1956-.L1842
	.long	.L1956-.L1842
	.long	.L1957-.L1842
	.long	.L2058-.L1842
	.long	.L1665-.L1842
	.long	.L1958-.L1842
	.long	.L1959-.L1842
	.long	.L1962-.L1842
	.long	.L1759-.L1842
	.long	.L1961-.L1842
	.long	.L1815-.L1842
	.text
.L1959:
	movl	$-1, %ebx
.L2343:
	leaq	8(%r12), %r15
.L1448:
	movq	%r15, %rdi
	movq	%r9, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-184(%rbp), %r9
	testb	%al, %al
	jne	.L2408
	cmpl	$-1, -196(%rbp)
	sete	%dl
	movl	%edx, %eax
	cmpb	$63, %bl
	jne	.L1698
	testb	%dl, %dl
	je	.L1698
	orl	$512, (%r12)
.L2399:
	movq	136(%r12), %rax
	movq	$0, 144(%r12)
	movb	$0, (%rax)
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
.L2257:
	movl	(%r12), %ecx
	movl	%ecx, %r8d
	andl	$16, %r8d
	cmpq	%rax, %r13
	jbe	.L1924
	movzbl	1(%r9), %ebx
	movq	%rax, %r9
.L1467:
	leaq	_ZN4node3url12_GLOBAL__N_1L24QUERY_ENCODE_SET_SPECIALE(%rip), %r15
	leaq	_ZN4node3url12_GLOBAL__N_1L27QUERY_ENCODE_SET_NONSPECIALE(%rip), %r14
	leaq	-128(%rbp), %rax
.L1790:
	cmpb	$-1, %bl
	je	.L1767
	cmpl	$-1, -196(%rbp)
	jne	.L1768
	cmpb	$35, %bl
	je	.L1767
.L1768:
	testl	%r8d, %r8d
	movq	%r14, %rdx
	movq	%rax, %rdi
	movzbl	%bl, %esi
	cmovne	%r15, %rdx
	movq	%r9, -208(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	movq	-208(%rbp), %r9
	movq	-184(%rbp), %rax
	addq	$1, %r9
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%r12), %ecx
	movl	%ecx, %r8d
	andl	$16, %r8d
	cmpq	%r9, %r13
	ja	.L2409
.L1961:
	movl	$-1, %ebx
.L1767:
	leaq	152(%r12), %r14
	leaq	-112(%rbp), %r15
.L1812:
	orb	$2, %ch
	movq	136(%r12), %rdi
	movq	-128(%rbp), %rax
	movl	%ecx, (%r12)
	movq	-120(%rbp), %rdx
	cmpq	-224(%rbp), %rax
	je	.L2410
	movq	-112(%rbp), %rcx
	cmpq	%r14, %rdi
	je	.L2411
	movq	%rdx, %xmm0
	movq	%rcx, %xmm1
	movq	152(%r12), %rsi
	movq	%rax, 136(%r12)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 144(%r12)
	testq	%rdi, %rdi
	je	.L1774
	movq	%rdi, -128(%rbp)
	movq	%rsi, -112(%rbp)
.L1772:
	movb	$0, (%rdi)
	movq	-128(%rbp), %rax
	addq	$1, %r9
	movq	$0, -120(%rbp)
	movb	$0, (%rax)
	cmpb	$35, %bl
	je	.L2412
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%r12), %ecx
	movl	$-1, %ebx
	movl	%ecx, %r8d
	andl	$16, %r8d
	cmpq	%r9, %r13
	jbe	.L1812
	movzbl	(%r9), %ebx
	jmp	.L1467
.L1759:
	movq	208(%r12), %rax
.L1813:
	addq	$1, %r9
	movl	$-1, %ebx
	xorl	%r15d, %r15d
	cmpq	200(%r12), %rax
	je	.L1760
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1813
	jmp	.L2344
	.p2align 4,,10
	.p2align 3
.L2463:
	movq	-216(%rbp), %rax
	leaq	8(%rax), %r14
.L1457:
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%r9, -208(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jne	.L2393
	cmpb	$35, %bl
	je	.L1643
	cmpb	$63, %bl
	je	.L1644
	cmpb	$-1, %bl
	je	.L2413
	movq	%r13, %rdx
	subq	%r9, %rdx
	cmpq	$1, %rdx
	jbe	.L1657
	movzbl	(%r9), %ebx
	movl	%ebx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L1657
	movzbl	1(%r9), %eax
	cmpb	$58, %al
	je	.L1965
	cmpb	$124, %al
	je	.L1965
.L1657:
	movq	-216(%rbp), %rax
	movl	(%rax), %eax
	testb	$-128, %al
	jne	.L2414
.L1661:
	testb	$1, %ah
	jne	.L2415
.L1662:
	movq	%r12, %rdi
	movq	%r9, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_114ShortenUrlPathEPNS0_8url_dataE
	movq	-184(%rbp), %r9
.L2393:
	cmpq	%r13, %r9
	ja	.L1439
	jb	.L2331
.L1962:
	movb	$0, -184(%rbp)
.L2330:
	movl	$-1, %ebx
.L1442:
	leaq	-128(%rbp), %rcx
.L1454:
	cmpl	$-1, -196(%rbp)
	movq	%r9, %r15
	jne	.L2416
.L1443:
	movl	%ebx, %eax
	andl	$-17, %eax
	cmpb	$47, %al
	sete	%al
	cmpb	$-1, %bl
	sete	%dl
	orl	%edx, %eax
	orb	-184(%rbp), %al
	movl	%eax, %r14d
	je	.L2417
	movq	%r15, %r9
.L1444:
	movq	-120(%rbp), %rax
	cmpq	$4, %rax
	je	.L1707
	cmpq	$6, %rax
	je	.L1708
	cmpq	$2, %rax
	je	.L2418
.L1709:
	cmpq	$1, %rax
	jne	.L2419
	movq	%rcx, %rdi
	leaq	.LC90(%rip), %rsi
	movq	%r9, -216(%rbp)
	movq	%rcx, -208(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-208(%rbp), %rcx
	movq	-216(%rbp), %r9
	testl	%eax, %eax
	jne	.L1730
	cmpb	$47, %bl
	je	.L1730
	cmpb	$1, -184(%rbp)
	je	.L1730
.L1728:
	orl	$256, (%r12)
	leaq	200(%r12), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rcx, -208(%rbp)
	leaq	8(%r12), %r14
	movq	%r9, -184(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_
	movq	-184(%rbp), %r9
	movq	-208(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L1713:
	movq	-128(%rbp), %rax
	movq	$0, -120(%rbp)
	movq	%r14, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rcx, -208(%rbp)
	movb	$0, (%rax)
	movq	%r9, -184(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-184(%rbp), %r9
	movq	-208(%rbp), %rcx
	testl	%eax, %eax
	leaq	1(%r9), %rdi
	movq	%rdi, -216(%rbp)
	jne	.L1741
	leal	1(%rbx), %eax
	testb	$-65, %al
	sete	%dl
	cmpb	$35, %bl
	sete	%al
	orb	%al, %dl
	movb	%dl, -184(%rbp)
	jne	.L2420
	leaq	1(%r9), %rdi
	cmpq	%rdi, %r13
	jb	.L1439
	movl	(%r12), %eax
	movl	$-1, %ebx
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%rdi, %r13
	jbe	.L1820
.L2323:
	movzbl	1(%r9), %ebx
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	movb	%dl, -184(%rbp)
.L1820:
	movq	-216(%rbp), %r9
	jmp	.L1454
.L1958:
	movl	$-1, %ebx
	xorl	%r11d, %r11d
.L1449:
	movl	$15, %r14d
.L1803:
	movl	%ebx, %edx
	movq	-120(%rbp), %rax
	andl	$-17, %edx
	cmpb	$47, %dl
	je	.L1680
	cmpb	$-1, %bl
	je	.L1680
	cmpb	$35, %bl
	sete	%dl
	orb	%dl, %r11b
	je	.L1681
.L1680:
	cmpl	$-1, -196(%rbp)
	je	.L2421
	testq	%rax, %rax
	jne	.L1684
	movq	104(%r12), %rax
	orb	$-128, %sil
	movq	$0, 112(%r12)
	movl	%esi, (%r12)
	movb	$0, (%rax)
	movq	-128(%rbp), %r8
	jmp	.L1483
.L2058:
	leaq	8(%r12), %r15
.L1816:
	xorl	%r14d, %r14d
	movl	$-1, %ebx
.L1795:
	movq	16(%r12), %rdx
	xorl	%esi, %esi
	movl	$5, %r8d
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rcx
	movq	%r9, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	testb	%r14b, %r14b
	movq	-208(%rbp), %r9
	je	.L1639
.L1456:
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	movl	(%r12), %esi
	movl	%esi, %edx
	shrl	$4, %edx
	movl	%edx, %edi
	andl	$1, %edi
	movb	%dil, -208(%rbp)
	cmpq	%rax, %r13
	jbe	.L1946
	movzbl	1(%r9), %ebx
	cmpb	$92, %bl
	sete	%r11b
	jmp	.L1469
.L1957:
	xorl	%edx, %edx
	movl	$-1, %ebx
.L1471:
	movl	$15, %r14d
.L1845:
	leal	-48(%rbx), %eax
	cmpb	$9, %al
	jbe	.L2422
	cmpb	$-1, %bl
	je	.L1629
	cmpb	$0, -234(%rbp)
	jne	.L1629
	movl	%ebx, %eax
	andl	$-17, %eax
	cmpb	$47, %al
	je	.L1630
	cmpb	$35, %bl
	je	.L1630
	testb	%dl, %dl
	je	.L2252
.L1630:
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2286
.L1836:
	movq	-128(%rbp), %r8
	xorl	%esi, %esi
	movq	%r8, %rax
	addq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L1634:
	cmpq	%rax, %rdx
	je	.L1633
	leal	(%rsi,%rsi,4), %edi
	movsbl	(%rax), %esi
	addq	$1, %rax
	leal	-48(%rsi,%rdi,2), %esi
	cmpl	$65535, %esi
	jbe	.L1634
	cmpl	$10, -196(%rbp)
	je	.L2358
.L2285:
	orl	$1, %ecx
	movl	%ecx, (%r12)
	jmp	.L1483
.L1956:
	xorl	%edx, %edx
	movl	$-1, %ebx
.L1472:
	cmpl	$-1, -196(%rbp)
	jne	.L1806
.L1605:
	cmpb	$58, %bl
	jne	.L1607
	cmpb	$1, -233(%rbp)
	je	.L1607
	cmpq	$0, -120(%rbp)
	movl	(%r12), %eax
	je	.L2423
	orb	$-128, %al
	movzbl	-208(%rbp), %edx
	leaq	104(%r12), %rsi
	leaq	-128(%rbp), %rdi
	movl	%eax, (%r12)
	movq	%r9, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0
	movq	-184(%rbp), %r9
	testb	%al, %al
	je	.L1828
	movq	-128(%rbp), %rax
	cmpl	$11, -196(%rbp)
	movq	$0, -120(%rbp)
	movb	$0, (%rax)
	je	.L1510
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	movl	(%r12), %ecx
	movl	%ecx, %edx
	shrl	$4, %edx
	andl	$1, %edx
	cmpq	%rax, %r13
	jbe	.L1948
	movzbl	1(%r9), %ebx
	movq	%rax, %r9
	cmpb	$92, %bl
	sete	%sil
	andl	%esi, %edx
	jmp	.L1471
.L1955:
	movl	$-1, %ebx
.L1808:
	cmpb	$47, %bl
	je	.L1585
	cmpb	$92, %bl
	je	.L1585
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%r12), %ecx
	jbe	.L1871
	movzbl	(%r9), %ebx
	movl	%ecx, %eax
	movq	%r9, %r15
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	jmp	.L1451
.L1888:
	movq	%r14, %r9
.L2359:
	leaq	8(%r12), %r10
.L1460:
	movq	-216(%rbp), %rax
	xorl	%edx, %edx
	movl	$-1, %ebx
	leaq	8(%rax), %r15
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	-216(%rbp), %rax
	movq	%r10, -272(%rbp)
	movq	%r13, -208(%rbp)
	movq	%r15, %r13
	movq	%r10, %r15
	leaq	40(%rax), %r14
	movq	%r14, -280(%rbp)
	movq	%r9, %r14
.L1441:
	movq	%r13, %rsi
	movq	%r15, %rdi
	movb	%dl, -264(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	%r15, %rdi
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movzbl	-264(%rbp), %edx
	testb	%al, %al
	movl	(%r12), %eax
	je	.L1539
	orl	$16, %eax
.L1540:
	movl	%eax, (%r12)
	cmpb	$47, %bl
	je	.L1541
	jg	.L1542
	cmpb	$-1, %bl
	je	.L1543
	movq	-208(%rbp), %r13
	movq	%r14, %r9
	movq	%r15, %r10
	cmpb	$35, %bl
	jne	.L1545
	movq	-216(%rbp), %rdi
	movl	(%rdi), %edx
	testb	$32, %dl
	jne	.L2424
.L1558:
	testb	$64, %dl
	jne	.L2425
.L1559:
	testb	$-128, %dl
	jne	.L2426
.L1560:
	testb	$2, %dh
	jne	.L2427
.L1561:
	andb	$1, %dh
	jne	.L2428
.L1562:
	movq	-216(%rbp), %rax
	leaq	1(%r9), %r14
	movl	4(%rax), %eax
	movl	%eax, 4(%r12)
	cmpq	%r14, %r13
	jnb	.L2380
	.p2align 4,,10
	.p2align 3
.L1439:
	movq	-128(%rbp), %rdi
	cmpq	-224(%rbp), %rdi
	je	.L2357
	call	_ZdlPv@PLT
.L2357:
	movq	-160(%rbp), %r8
.L1436:
	cmpq	-192(%rbp), %r8
	je	.L1419
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1419
.L1954:
	movl	$-1, %ebx
.L1458:
	cmpb	$0, -184(%rbp)
	je	.L2252
	movq	-216(%rbp), %rax
	testb	$2, (%rax)
	je	.L2429
	cmpb	$35, %bl
	jne	.L2252
	leaq	8(%r12), %r14
	leaq	8(%rax), %rsi
	movq	%r9, -184(%rbp)
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	%r14, %rdi
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-184(%rbp), %r9
	testb	%al, %al
	je	.L2430
	movl	(%r12), %ecx
	orl	$16, %ecx
.L1527:
	movq	-216(%rbp), %rax
	movl	%ecx, (%r12)
	movl	(%rax), %eax
	testb	$1, %ah
	jne	.L2431
.L1528:
	testb	$2, %ah
	jne	.L2432
.L1529:
	testb	$4, %ah
	jne	.L2433
.L1530:
	orl	$2, %ecx
	leaq	1(%r9), %r14
	movl	%ecx, (%r12)
	cmpq	%r14, %r13
	jb	.L1439
	movl	$-1, %ebx
	jbe	.L1465
.L2355:
	movzbl	1(%r9), %ebx
.L1465:
	leaq	-128(%rbp), %r15
	movq	%r15, %rax
.L1791:
	leaq	1(%r14), %r15
	cmpb	$-1, %bl
	je	.L2313
	testb	%bl, %bl
	je	.L1780
	movq	%rax, %rdi
	movzbl	%bl, %esi
	leaq	_ZN4node3url12_GLOBAL__N_1L19FRAGMENT_ENCODE_SETE(%rip), %rdx
	movq	%rax, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	cmpq	%r15, %r13
	movq	-184(%rbp), %rax
	jb	.L1439
	movl	(%r12), %ecx
	jbe	.L2316
.L2345:
	movzbl	(%r15), %ebx
	movq	%r15, %r14
	jmp	.L1791
.L1953:
	movl	$-1, %ebx
.L1477:
	movq	%r9, %r14
	movl	$15, %r15d
	movq	%r12, %r9
.L1811:
	movl	%ebx, %eax
	leal	-48(%rbx), %edx
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$9, %dl
	ja	.L2434
	cmpb	$25, %al
	jbe	.L1485
.L1486:
	movq	-120(%rbp), %r12
	movq	-128(%rbp), %rax
	movq	%r15, %rdx
	cmpq	-224(%rbp), %rax
	cmovne	-112(%rbp), %rdx
	leaq	1(%r12), %r10
	cmpq	%rdx, %r10
	ja	.L2435
.L1490:
	movb	%bl, (%rax,%r12)
	movq	-128(%rbp), %rax
	addq	$1, %r14
	movq	%r10, -120(%rbp)
	movb	$0, 1(%rax,%r12)
	cmpq	%r14, %r13
	jb	.L1439
	movl	(%r9), %ecx
	movl	$-1, %ebx
	movl	%ecx, %eax
	shrl	$4, %eax
	andl	$1, %eax
	movb	%al, -208(%rbp)
	cmpq	%r14, %r13
	jbe	.L1811
	movzbl	(%r14), %ebx
	jmp	.L1811
.L1462:
	cmpq	%r9, %r13
	jb	.L1439
.L1814:
	movl	$-1, %ebx
	cmpq	%r9, %r13
	jbe	.L1808
.L2339:
	movzbl	(%r9), %ebx
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L2404:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rdi, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-160(%rbp), %r8
	movl	$15, %eax
	movq	-208(%rbp), %rdi
	jmp	.L1431
	.p2align 4,,10
	.p2align 3
.L2406:
	movq	%r13, %r10
	jmp	.L1421
.L1478:
	cmpl	$-1, -196(%rbp)
	je	.L1482
	orl	$1, %esi
	movq	-128(%rbp), %r8
	movl	%esi, (%r12)
.L1483:
	cmpq	-224(%rbp), %r8
	je	.L1438
	movq	%r8, %rdi
	call	_ZdlPv@PLT
	jmp	.L1438
.L1844:
	cmpq	%r9, %r13
	jb	.L1439
	leaq	8(%r12), %r10
.L1817:
	shrl	$4, %esi
	andl	$1, %esi
	cmpq	%r9, %r13
	jbe	.L1460
	movzbl	(%r9), %ebx
	cmpb	$92, %bl
	sete	%dl
	andl	%esi, %edx
.L1461:
	movq	-216(%rbp), %rax
	leaq	8(%rax), %r15
	jmp	.L1440
.L1453:
	cmpq	%r9, %r13
	jb	.L1439
.L1796:
	movb	$0, -184(%rbp)
	movl	$-1, %ebx
	cmpq	%r9, %r13
	jbe	.L1442
.L2365:
	movl	(%r12), %esi
.L2327:
	movzbl	(%r9), %ebx
	shrl	$4, %esi
	cmpb	$92, %bl
	sete	%al
	andl	%esi, %eax
	movb	%al, -184(%rbp)
	jmp	.L1442
.L1946:
	movq	%rax, %r9
.L1665:
	cmpb	$0, -200(%rbp)
	jne	.L2436
	cmpq	%r9, %r13
	jb	.L1439
	shrl	$4, %esi
	movl	$-1, %ebx
	andl	$1, %esi
	cmpq	%r9, %r13
	jbe	.L1442
	movzbl	(%r9), %ebx
	cmpb	$92, %bl
	sete	%al
	andl	%esi, %eax
	movb	%al, -184(%rbp)
	jmp	.L1442
.L1843:
	leaq	8(%r12), %rax
	movq	%rax, -272(%rbp)
.L1792:
	movq	-272(%rbp), %rdi
	movq	%r9, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-184(%rbp), %r9
.L1571:
	movq	-216(%rbp), %rax
	movl	(%rax), %eax
	testb	$32, %al
	jne	.L2437
.L1575:
	testb	$64, %al
	jne	.L2438
.L1576:
	testb	$-128, %al
	jne	.L2439
.L1568:
	movq	-216(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, 4(%r12)
.L2397:
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1962
.L2331:
	movzbl	(%r9), %ebx
.L2328:
	movl	(%r12), %eax
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	movb	%dl, -184(%rbp)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L2416:
	movq	%r9, %r14
	movq	%rcx, %r15
.L1446:
	cmpb	$47, %bl
	sete	%al
	cmpb	$-1, %bl
	sete	%dl
	orl	%edx, %eax
	orb	-184(%rbp), %al
	je	.L2440
	movq	%r14, %r9
	movq	%r15, %rcx
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L2434:
	cmpb	$25, %al
	jbe	.L1485
	leal	-45(%rbx), %eax
	cmpb	$1, %al
	jbe	.L1486
	cmpb	$43, %bl
	je	.L1486
	movq	%r9, %r12
	movq	-128(%rbp), %r8
	movq	%r14, %r9
	cmpb	$58, %bl
	je	.L2441
	cmpb	$-1, %bl
	jne	.L1492
	cmpb	$0, -234(%rbp)
	je	.L1492
	movq	-120(%rbp), %rbx
.L1493:
	testq	%rbx, %rbx
	je	.L1638
.L1494:
	cmpq	-224(%rbp), %r8
	movl	$15, %eax
	cmovne	-112(%rbp), %rax
	leaq	1(%rbx), %r14
	leaq	-128(%rbp), %r15
	cmpq	%rax, %r14
	ja	.L2442
.L1497:
	movb	$58, (%r8,%rbx)
	movq	-128(%rbp), %rax
	movq	%r15, %rdi
	movq	%r14, -120(%rbp)
	movb	$0, (%rax,%r14)
	movq	%r9, -264(%rbp)
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	cmpl	$-1, -196(%rbp)
	movq	-264(%rbp), %r9
	movl	%eax, %ebx
	je	.L1499
	cmpb	-208(%rbp), %al
	jne	.L1501
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	movq	%r9, -208(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	jne	.L1499
	movabsq	$-4294967200, %rax
	andq	(%r12), %rax
	movabsq	$-4294967296, %rdx
	cmpq	%rdx, %rax
	jne	.L1501
.L1499:
	movq	8(%r12), %rdi
	movq	-128(%rbp), %rax
	leaq	8(%r12), %r10
	movq	-120(%rbp), %rdx
	cmpq	-224(%rbp), %rax
	je	.L2443
	leaq	24(%r12), %rcx
	movq	-112(%rbp), %xmm0
	cmpq	%rcx, %rdi
	je	.L2444
	movq	%rdx, %xmm5
	movq	24(%r12), %rcx
	movq	%rax, 8(%r12)
	punpcklqdq	%xmm0, %xmm5
	movups	%xmm5, 16(%r12)
	testq	%rdi, %rdi
	je	.L1507
	movq	%rdi, -128(%rbp)
	movq	%rcx, -112(%rbp)
.L1505:
	movq	$0, -120(%rbp)
	movb	$0, (%rdi)
	movl	4(%r12), %esi
	movq	%r10, %rdi
	movq	%r9, -264(%rbp)
	movq	%r10, -208(%rbp)
	call	_ZN4node3url12_GLOBAL__N_113NormalizePortERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	testb	%bl, %bl
	movq	-208(%rbp), %r10
	movq	-264(%rbp), %r9
	movl	%eax, 4(%r12)
	movl	(%r12), %eax
	je	.L1508
	movzbl	-184(%rbp), %r15d
	orl	$16, %eax
.L1509:
	movl	%eax, (%r12)
	movq	-128(%rbp), %rax
	cmpl	$-1, -196(%rbp)
	movq	$0, -120(%rbp)
	movq	%r9, -264(%rbp)
	movb	$0, (%rax)
	jne	.L1510
	movq	%r10, %rdi
	leaq	.LC2(%rip), %rsi
	movq	%r10, -208(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-264(%rbp), %r9
	movq	-208(%rbp), %r10
	testl	%eax, %eax
	leaq	1(%r9), %r14
	je	.L1511
	testb	%r15b, %r15b
	jne	.L2445
	testb	%bl, %bl
	jne	.L1517
	cmpq	%r14, %r13
	jbe	.L1518
	cmpb	$47, 1(%r9)
	je	.L2446
	orl	$258, (%r12)
	leaq	200(%r12), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%r9, -184(%rbp)
	movq	%r14, -208(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_
	movq	-184(%rbp), %r9
	movq	-208(%rbp), %rcx
	movzbl	1(%r9), %ebx
	movq	%rcx, %r9
.L1468:
	leaq	1(%r9), %r14
	cmpb	$35, %bl
	je	.L1762
	cmpb	$63, %bl
	jne	.L1848
	cmpq	%r14, %r13
	jb	.L1439
	movl	(%r12), %ecx
	movl	%ecx, %r8d
	andl	$16, %r8d
	cmpq	%r14, %r13
	ja	.L2348
.L1938:
	movq	%r14, %r9
	movl	$-1, %ebx
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L2408:
	cmpb	$47, %bl
	setne	%dl
	cmpb	$92, %bl
	setne	%al
	andb	%al, %dl
	movb	%dl, -184(%rbp)
	jne	.L2397
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	movl	(%r12), %edx
	shrl	$4, %edx
	andl	$1, %edx
	cmpq	%rax, %r13
	jbe	.L1942
	movzbl	1(%r9), %ebx
	movq	%rax, %r9
	cmpb	$92, %bl
	sete	%cl
	andl	%edx, %ecx
	movb	%cl, -184(%rbp)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1585:
	addq	$1, %r9
.L2387:
	cmpq	%r9, %r13
	jb	.L1439
	movl	$-1, %ebx
	jbe	.L1808
	jmp	.L2339
.L2422:
	movq	-120(%rbp), %r15
	movq	-128(%rbp), %rax
	movq	%r14, %rdx
	cmpq	-224(%rbp), %rax
	cmovne	-112(%rbp), %rdx
	leaq	1(%r15), %r10
	cmpq	%rdx, %r10
	ja	.L2447
.L1627:
	movb	%bl, (%rax,%r15)
	movq	-128(%rbp), %rax
	addq	$1, %r9
	movq	%r10, -120(%rbp)
	movb	$0, 1(%rax,%r15)
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%r12), %ecx
	jbe	.L1925
	movzbl	(%r9), %ebx
	movl	%ecx, %eax
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	jmp	.L1845
	.p2align 4,,10
	.p2align 3
.L1805:
	cmpl	$-1, -196(%rbp)
	je	.L1612
	movl	$10, %r14d
	xorl	%edx, %edx
	movl	$-1, %ebx
.L1806:
	leaq	8(%r12), %r10
	leaq	.LC2(%rip), %rsi
	movb	%dl, -272(%rbp)
	movq	%r10, %rdi
	movq	%r9, -264(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-264(%rbp), %r9
	movzbl	-272(%rbp), %edx
	testl	%eax, %eax
	jne	.L1605
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%r12), %esi
	movl	%esi, %eax
	shrl	$4, %eax
	andl	$1, %eax
	movb	%al, -208(%rbp)
	cmpq	%r9, %r13
	jbe	.L1958
	movzbl	(%r9), %ebx
	cmpb	$92, %bl
	sete	%r11b
	jmp	.L1449
.L1684:
	movzbl	-208(%rbp), %edx
	leaq	-96(%rbp), %r14
	leaq	-80(%rbp), %rbx
	movb	$0, -80(%rbp)
	leaq	-128(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rbx, -96(%rbp)
	movq	%r9, -184(%rbp)
	movq	$0, -88(%rbp)
	call	_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0
	movq	-184(%rbp), %r9
	testb	%al, %al
	je	.L2448
	leaq	.LC88(%rip), %rsi
	movq	%r14, %rdi
	movq	%r9, -184(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	jne	.L1688
	movq	-96(%rbp), %rax
	movq	$0, -88(%rbp)
	movb	$0, (%rax)
.L1688:
	orl	$128, (%r12)
	leaq	104(%r12), %rdi
	movq	%r14, %rsi
	movq	%r9, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	cmpl	$-1, -196(%rbp)
	movq	-184(%rbp), %r9
	jne	.L1687
	movq	-128(%rbp), %rax
	movq	$0, -120(%rbp)
	movb	$0, (%rax)
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L2286
	call	_ZdlPv@PLT
	movq	-184(%rbp), %r9
.L2286:
	cmpq	%r9, %r13
	jb	.L1439
	leaq	8(%r12), %r15
	ja	.L2226
.L1459:
	movq	%r15, %rdi
	movq	%r9, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-184(%rbp), %r9
	testb	%al, %al
	jne	.L2397
	cmpl	$-1, -196(%rbp)
	movl	$-1, %ebx
	sete	%al
.L1698:
	cmpb	$35, %bl
	sete	%dl
	andb	%al, %dl
	movb	%dl, -184(%rbp)
	je	.L1700
	orl	$1024, (%r12)
.L2392:
	movq	168(%r12), %rax
	leaq	1(%r9), %r14
	movq	$0, 176(%r12)
	movb	$0, (%rax)
	cmpq	%r14, %r13
	jb	.L1439
.L2258:
	movl	(%r12), %ecx
	ja	.L2355
	movq	%r14, %r9
.L1815:
	addq	$1, %r9
.L1779:
	orb	$4, %ch
	movq	168(%r12), %rdi
	movq	-128(%rbp), %rax
	movl	%ecx, (%r12)
	movq	-120(%rbp), %rdx
	cmpq	-224(%rbp), %rax
	je	.L2449
	leaq	184(%r12), %rcx
	movq	-112(%rbp), %rsi
	cmpq	%rcx, %rdi
	je	.L2450
	movq	%rdx, %xmm0
	movq	%rsi, %xmm2
	movq	184(%r12), %rcx
	movq	%rax, 168(%r12)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 176(%r12)
	testq	%rdi, %rdi
	je	.L1787
	movq	%rdi, -128(%rbp)
	movq	%rcx, -112(%rbp)
.L1785:
	movq	$0, -120(%rbp)
	movb	$0, (%rdi)
	cmpq	%r9, %r13
	jb	.L1439
.L2281:
	movl	(%r12), %ecx
	jbe	.L1815
	movzbl	(%r9), %ebx
	movq	%r9, %r14
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L2419:
	cmpq	$3, %rax
	jne	.L1715
	movq	-128(%rbp), %rax
	leaq	8(%r12), %r14
	cmpb	$37, (%rax)
	je	.L2451
.L1731:
	leaq	.LC2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rcx, -208(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-184(%rbp), %r9
	movq	-208(%rbp), %rcx
	testl	%eax, %eax
	movl	(%r12), %eax
	jne	.L1734
	movq	200(%r12), %rdi
	cmpq	%rdi, 208(%r12)
	je	.L2452
.L1734:
	orb	$1, %ah
	movq	%rcx, %rsi
	leaq	200(%r12), %rdi
	movq	%r9, -208(%rbp)
	movl	%eax, (%r12)
	movq	%rcx, -184(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJS5_EEEvDpOT_
	movq	-208(%rbp), %r9
	movq	-184(%rbp), %rcx
	jmp	.L1713
.L2445:
	movq	-216(%rbp), %rax
	movq	16(%r12), %rdx
	cmpq	16(%rax), %rdx
	je	.L2453
.L2230:
	cmpq	%r14, %r13
	jb	.L1439
	jbe	.L1940
	movzbl	1(%r9), %ebx
	movq	%r14, %r9
.L1473:
	cmpb	$47, %bl
	jne	.L2387
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jbe	.L1580
.L1857:
	cmpb	$47, 1(%r9)
	jne	.L2454
	leaq	2(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	jbe	.L1949
	movzbl	2(%r9), %ebx
	movq	%rax, %r9
	jmp	.L1808
.L1475:
	cmpb	$47, %bl
	jne	.L2455
.L1537:
	leaq	1(%r9), %r15
	cmpq	%r15, %r13
	jb	.L1439
	movl	(%r12), %ecx
	jbe	.L1450
	movzbl	1(%r9), %ebx
	movl	%ecx, %eax
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
.L1451:
	leaq	-128(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%r12, %rax
	movl	%ebx, %r12d
	movq	%rax, %rbx
.L1819:
	movq	-120(%rbp), %r14
	movq	%r14, %rax
	cmpb	$64, %r12b
	je	.L2456
	movl	%r12d, %esi
	movq	-128(%rbp), %r8
	andl	$-17, %esi
	cmpb	$47, %sil
	je	.L2309
	cmpb	$-1, %r12b
	je	.L2309
	cmpb	$35, %r12b
	je	.L2309
	testb	%dl, %dl
	je	.L1597
.L2309:
	movq	%rbx, %r12
.L1596:
	cmpb	$0, -232(%rbp)
	je	.L1599
	testq	%rax, %rax
	je	.L2285
.L1599:
	notq	%rax
	movq	$0, -120(%rbp)
	leaq	(%r15,%rax), %r10
	movb	$0, (%r8)
	leaq	1(%r10), %r9
	cmpq	%r13, %r9
	ja	.L1439
	movl	(%r12), %r15d
	andl	$16, %r15d
	setne	-208(%rbp)
	movzbl	-208(%rbp), %eax
	cmpq	%r13, %r9
	jnb	.L1805
	movzbl	1(%r10), %ebx
	movl	$10, %r14d
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	jmp	.L1472
.L1474:
	leaq	8(%r12), %r10
.L1793:
	movq	%r10, %rdi
	movq	%r9, -208(%rbp)
	movb	%r11b, -264(%rbp)
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-208(%rbp), %r9
	testb	%al, %al
	je	.L1570
	cmpb	$47, %bl
	je	.L1964
	movzbl	-264(%rbp), %r11d
	testb	%r11b, %r11b
	je	.L1571
.L1964:
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	jbe	.L1949
	movzbl	1(%r9), %ebx
	movq	%rax, %r9
	jmp	.L1808
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	-208(%rbp), %r13
	movq	%r14, %r9
	movq	%r15, %r10
	cmpb	$63, %bl
	jne	.L1545
	movq	-216(%rbp), %rdi
	movl	(%rdi), %edx
	testb	$32, %dl
	jne	.L2457
.L1553:
	testb	$64, %dl
	jne	.L2458
.L1554:
	testb	$-128, %dl
	jne	.L2459
.L1555:
	andb	$1, %dh
	jne	.L2460
.L1556:
	movq	-216(%rbp), %rax
	movl	4(%rax), %eax
	movl	%eax, 4(%r12)
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jnb	.L2257
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1871:
	movq	%r9, %r15
.L1450:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r8
	jmp	.L1596
.L1464:
	movl	%ebx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L1478
	movq	-120(%rbp), %r14
	orl	$32, %ebx
	movq	-128(%rbp), %rdx
	movl	$15, %eax
	cmpq	-224(%rbp), %rdx
	cmovne	-112(%rbp), %rax
	movl	%ebx, %r10d
	leaq	1(%r14), %rbx
	cmpq	%rax, %rbx
	ja	.L2461
.L1480:
	movb	%r10b, (%rdx,%r14)
	movq	-128(%rbp), %rax
	movq	%rbx, -120(%rbp)
	movb	$0, 1(%rax,%r14)
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	movl	(%r12), %ecx
	movl	%ecx, %edx
	shrl	$4, %edx
	movl	%edx, %edi
	andl	$1, %edi
	movb	%dil, -208(%rbp)
	cmpq	%rax, %r13
	jbe	.L1933
	movzbl	1(%r9), %ebx
	movq	%rax, %r9
	jmp	.L1477
	.p2align 4,,10
	.p2align 3
.L1539:
	andl	$-17, %eax
	jmp	.L1540
.L2429:
	leaq	8(%rax), %r15
	leaq	.LC2(%rip), %rsi
	movq	%r9, -208(%rbp)
	movq	%r15, %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	je	.L2462
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1863
	movzbl	(%r9), %ebx
	movl	(%r12), %eax
	leaq	8(%r12), %r10
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	jmp	.L1440
.L1913:
	movq	%r14, %r9
.L1455:
	movq	16(%r12), %rdx
	movl	$5, %r8d
	xorl	%esi, %esi
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rcx
	movq	%r9, -208(%rbp)
	movl	$-1, %ebx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-208(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1639:
	cmpb	$0, -200(%rbp)
	jne	.L2463
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%r12), %eax
	movl	$-1, %ebx
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%r9, %r13
	jbe	.L1442
.L2324:
	movzbl	(%r9), %ebx
.L2326:
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
.L2329:
	movb	%dl, -184(%rbp)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L1485:
	orl	$32, %ebx
	jmp	.L1486
	.p2align 4,,10
	.p2align 3
.L1428:
	movq	-160(%rbp), %r8
	movq	-152(%rbp), %rsi
	jmp	.L1432
.L1570:
	cmpb	$47, %bl
	je	.L1537
	jmp	.L1571
	.p2align 4,,10
	.p2align 3
.L1612:
	leaq	-1(%r9), %rbx
	testl	%r15d, %r15d
	jne	.L1831
	cmpl	$-1, -196(%rbp)
	movl	(%r12), %eax
	je	.L1619
	cmpq	$0, -120(%rbp)
	jne	.L1620
	cmpq	$0, 48(%r12)
	jne	.L1621
	cmpq	$0, 80(%r12)
	jne	.L1621
	cmpl	$-1, 4(%r12)
	jne	.L1621
.L1620:
	orb	$-128, %al
	movzbl	-208(%rbp), %edx
	leaq	104(%r12), %rsi
	leaq	-128(%rbp), %rdi
	movl	%eax, (%r12)
	call	_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0
	testb	%al, %al
	je	.L1828
	movq	-128(%rbp), %rax
	movq	$0, -120(%rbp)
	movb	$0, (%rax)
	movq	-128(%rbp), %r8
	jmp	.L1483
	.p2align 4,,10
	.p2align 3
.L1629:
	movq	-120(%rbp), %rdx
	testq	%rdx, %rdx
	jne	.L1836
	movl	-196(%rbp), %eax
	cmpl	$-1, %eax
	je	.L2394
	movq	-128(%rbp), %r8
	cmpl	$10, %eax
	je	.L2358
.L1638:
	orl	$8, %ecx
	movl	%ecx, (%r12)
	jmp	.L1483
.L1762:
	cmpq	%r14, %r13
	jb	.L1439
.L2380:
	movl	(%r12), %ecx
	movl	$-1, %ebx
	jbe	.L1465
	jmp	.L2355
	.p2align 4,,10
	.p2align 3
.L1780:
	cmpq	%r15, %r13
	jb	.L1439
	movl	(%r12), %ecx
	ja	.L2345
	leaq	2(%r14), %r9
	jmp	.L1779
.L2435:
	leaq	-128(%rbp), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r9, -264(%rbp)
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-128(%rbp), %rax
	movq	-264(%rbp), %r9
	movq	-208(%rbp), %r10
	jmp	.L1490
.L2421:
	cmpq	$2, %rax
	je	.L2464
	testq	%rax, %rax
	jne	.L1684
	movq	104(%r12), %rax
	orb	$-128, %sil
	movq	$0, 112(%r12)
	movl	%esi, (%r12)
	movb	$0, (%rax)
.L2394:
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1447
.L2226:
	movzbl	(%r9), %ebx
	leaq	8(%r12), %r15
	jmp	.L1448
.L1828:
	orl	$1, (%r12)
	movq	-128(%rbp), %r8
	jmp	.L1483
.L1859:
	movq	%rax, %r8
	jmp	.L1425
.L1924:
	movq	%rax, %r9
	movl	$-1, %ebx
	jmp	.L1467
.L2430:
	movl	(%r12), %ecx
	andl	$-17, %ecx
	jmp	.L1527
.L2315:
	movq	%rbx, %r12
.L1818:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %r8
	movq	%r9, %r15
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L2252:
	orl	$1, %ecx
	movq	-128(%rbp), %r8
	movl	%ecx, (%r12)
	jmp	.L1483
.L2409:
	movzbl	(%r9), %ebx
	jmp	.L1790
.L2448:
	orl	$1, (%r12)
.L1687:
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L1510
	call	_ZdlPv@PLT
.L1510:
	movq	-128(%rbp), %r8
	jmp	.L1483
.L2410:
	testq	%rdx, %rdx
	je	.L1770
	cmpq	$1, %rdx
	je	.L2465
	movq	-224(%rbp), %rsi
	movq	%r9, -184(%rbp)
	call	memcpy@PLT
	movq	-120(%rbp), %rdx
	movq	136(%r12), %rdi
	movq	-184(%rbp), %r9
.L1770:
	movq	%rdx, 144(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L1772
.L2454:
	cmpq	%r13, %r9
	ja	.L1439
	movl	$-1, %ebx
	jnb	.L1808
	jmp	.L2339
.L2456:
	cmpb	$0, -232(%rbp)
	jne	.L2466
.L1587:
	movq	-128(%rbp), %rdx
	testq	%r14, %r14
	je	.L1588
	movzbl	(%rdx), %eax
	cmpb	$58, %al
	je	.L1589
	orl	$32, (%rbx)
	movzbl	(%rdx), %eax
.L1589:
	xorl	%r12d, %r12d
	movq	%r13, -232(%rbp)
	movzbl	-235(%rbp), %r13d
	movq	%r15, -264(%rbp)
	movq	%r12, %r15
	movq	-248(%rbp), %r12
	jmp	.L1594
	.p2align 4,,10
	.p2align 3
.L1590:
	movzbl	%al, %esi
	testb	%r13b, %r13b
	jne	.L1591
	leaq	_ZN4node3url12_GLOBAL__N_1L19USERINFO_ENCODE_SETE(%rip), %rdx
	leaq	40(%rbx), %rdi
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	movq	-128(%rbp), %rdx
.L1592:
	addq	$1, %r15
	cmpq	%r14, %r15
	je	.L2306
	movzbl	(%rdx,%r15), %eax
.L1594:
	cmpb	$58, %al
	jne	.L1590
	orl	$64, (%rbx)
	movl	$58, %esi
	testb	%r13b, %r13b
	je	.L2340
.L1591:
	leaq	_ZN4node3url12_GLOBAL__N_1L19USERINFO_ENCODE_SETE(%rip), %rdx
	movq	%r12, %rdi
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	movq	-128(%rbp), %rdx
.L2340:
	movl	$1, %r13d
	jmp	.L1592
.L1933:
	movq	%rax, %r9
	movl	$-1, %ebx
	jmp	.L1477
.L1925:
	xorl	%edx, %edx
	movl	$-1, %ebx
	jmp	.L1845
.L1863:
	xorl	%edx, %edx
	movl	$-1, %ebx
	leaq	8(%r12), %r10
	jmp	.L1440
.L1518:
	orl	$258, (%r12)
	leaq	200(%r12), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%r14, -208(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_
	movq	-208(%rbp), %rcx
	cmpq	%rcx, %r13
	jb	.L1439
	movq	-184(%rbp), %r9
	movl	$-1, %ebx
	leaq	2(%r9), %r14
.L1848:
	cmpb	$-1, %bl
	movq	200(%r12), %rdi
	movq	%r14, %r9
	setne	%r15b
	cmpq	208(%r12), %rdi
	jne	.L1764
.L1760:
	leaq	200(%r12), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%r9, -184(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_
	movq	200(%r12), %rdi
	movq	-184(%rbp), %r9
.L1764:
	cmpq	%rdi, 208(%r12)
	je	.L1765
	testb	%r15b, %r15b
	je	.L1765
	movzbl	%bl, %esi
	leaq	_ZN4node3url12_GLOBAL__N_1L21C0_CONTROL_ENCODE_SETE(%rip), %rdx
	movq	%r9, -184(%rbp)
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	movq	-184(%rbp), %r9
	cmpq	%r9, %r13
	jb	.L1439
.L2277:
	jbe	.L1759
.L2344:
	movzbl	(%r9), %ebx
	jmp	.L1468
	.p2align 4,,10
	.p2align 3
.L1607:
	movl	%ebx, %eax
	andl	$-17, %eax
	cmpb	$47, %al
	je	.L1612
	cmpb	$-1, %bl
	je	.L1612
	cmpb	$35, %bl
	je	.L1612
	testb	%dl, %dl
	je	.L2467
	leaq	-1(%r9), %rbx
.L1831:
	cmpq	$0, -120(%rbp)
	je	.L1828
	cmpl	$-1, -196(%rbp)
	movl	(%r12), %eax
	jne	.L1620
.L1619:
	orb	$-128, %al
	movzbl	-208(%rbp), %edx
	leaq	104(%r12), %rsi
	leaq	-128(%rbp), %rdi
	movl	%eax, (%r12)
	call	_ZN4node3url12_GLOBAL__N_19ParseHostERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPS7_bb.constprop.0
	testb	%al, %al
	je	.L1828
	movq	-128(%rbp), %rax
	leaq	1(%rbx), %r9
	movq	$0, -120(%rbp)
	movb	$0, (%rax)
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1447
	movzbl	1(%rbx), %ebx
	leaq	8(%r12), %r15
	jmp	.L1448
.L2358:
	movl	$-1, 4(%r12)
	jmp	.L1483
.L2447:
	leaq	-128(%rbp), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r9, -208(%rbp)
	movq	%r10, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-128(%rbp), %rax
	movq	-208(%rbp), %r9
	movq	-184(%rbp), %r10
	jmp	.L1627
.L2436:
	movq	-216(%rbp), %rax
	leaq	.LC2(%rip), %rsi
	movq	%r9, -184(%rbp)
	leaq	8(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-184(%rbp), %r9
	testl	%eax, %eax
	jne	.L2401
	movq	%r13, %rdx
	subq	%r9, %rdx
	cmpq	$1, %rdx
	jbe	.L1668
	movzbl	(%r9), %ebx
	movl	%ebx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L1668
	movzbl	1(%r9), %eax
	cmpb	$58, %al
	je	.L1966
	cmpb	$124, %al
	je	.L1966
.L1668:
	movq	-216(%rbp), %rax
	movq	200(%rax), %r8
	movl	(%r12), %eax
	movq	8(%r8), %r15
	cmpq	$1, %r15
	jbe	.L1672
	movq	(%r8), %rcx
	movzbl	(%rcx), %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	ja	.L1672
	cmpb	$58, 1(%rcx)
	je	.L2468
.L1672:
	movq	-216(%rbp), %rdi
	testb	$-128, (%rdi)
	jne	.L2469
	movq	$0, 112(%r12)
	andb	$127, %al
	movl	%eax, (%r12)
	movq	104(%r12), %rax
	movb	$0, (%rax)
.L2401:
	cmpq	%r9, %r13
	jb	.L1439
.L2262:
	movl	(%r12), %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%r9, %r13
	jbe	.L1962
	jmp	.L2324
.L2461:
	leaq	-128(%rbp), %r15
	xorl	%edx, %edx
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	movq	%r15, %rdi
	movq	%r9, -264(%rbp)
	movb	%r10b, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-128(%rbp), %rdx
	movq	-264(%rbp), %r9
	movzbl	-208(%rbp), %r10d
	jmp	.L1480
.L2464:
	movq	-128(%rbp), %rdx
	movzbl	(%rdx), %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	ja	.L1684
	movzbl	1(%rdx), %eax
	cmpb	$58, %al
	je	.L1685
	cmpb	$124, %al
	jne	.L1684
.L1685:
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1962
	jmp	.L2327
.L1895:
	movq	%r9, %rax
.L1469:
	cmpb	$47, %bl
	je	.L1664
	testb	%r11b, %r11b
	je	.L1946
.L1664:
	leaq	1(%rax), %r9
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1958
	movzbl	1(%rax), %ebx
	cmpb	$92, %bl
	sete	%r11b
	jmp	.L1449
.L2303:
	movl	%r15d, %r8d
	jmp	.L1467
.L1897:
	movq	%r9, %r14
	jmp	.L1465
.L1893:
	movq	%r9, %r14
.L1476:
	cmpb	$47, %bl
	jne	.L1533
	leaq	1(%r14), %rax
	cmpq	%rax, %r13
	jbe	.L1534
.L1855:
	cmpb	$47, 1(%r14)
	jne	.L2470
	leaq	2(%r14), %r9
	cmpq	%r9, %r13
	jb	.L1439
	movl	$-1, %ebx
	jbe	.L1808
	movzbl	2(%r14), %ebx
	jmp	.L1808
.L1894:
	movq	%r9, %r15
	jmp	.L1451
.L2052:
	leaq	8(%r12), %r15
.L1470:
	cmpb	$47, %bl
	sete	%r14b
	orl	%r11d, %r14d
	jmp	.L1795
.L1597:
	cmpq	-224(%rbp), %r8
	movl	$15, %edx
	leaq	1(%r14), %r14
	cmovne	-112(%rbp), %rdx
	cmpq	%rdx, %r14
	ja	.L2471
.L1603:
	movb	%r12b, (%r8,%rax)
	movq	-128(%rbp), %rax
	leaq	1(%r15), %r9
	movq	%r14, -120(%rbp)
	movb	$0, (%rax,%r14)
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%r9, %r13
	jbe	.L2315
	movzbl	1(%r15), %r12d
	movq	%r9, %r15
	cmpb	$92, %r12b
	sete	%dl
	andl	%eax, %edx
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L2424:
	orl	$32, %eax
	movq	%rdi, %rbx
	leaq	40(%rdi), %rsi
	movq	%r14, -184(%rbp)
	movl	%eax, (%r12)
	leaq	40(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1558
.L2457:
	orl	$32, %eax
	movq	%rdi, %rbx
	leaq	40(%rdi), %rsi
	movq	%r14, -184(%rbp)
	movl	%eax, (%r12)
	leaq	40(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1553
.L2451:
	cmpb	$50, 1(%rax)
	je	.L2472
.L1726:
	leaq	8(%r12), %r14
.L1732:
	cmpb	$50, 1(%rax)
	jne	.L1731
	movzbl	2(%rax), %eax
	movl	%eax, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	ja	.L1731
	orl	$32, %eax
	cmpb	$101, %al
	je	.L1713
	jmp	.L1731
.L2051:
	leaq	8(%r12), %r10
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L2485:
	movq	%r12, %rbx
.L1745:
	movq	208(%rbx), %rdx
	movq	-32(%rdx), %rdi
	leaq	-32(%rdx), %rax
	subq	$16, %rdx
	movq	%rax, 208(%rbx)
	cmpq	%rdx, %rdi
	je	.L2473
	call	_ZdlPv@PLT
	movq	208(%rbx), %rax
	movq	200(%rbx), %r14
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	$63, %rdx
	ja	.L1754
.L2311:
	movq	%rbx, %r12
	movq	-208(%rbp), %rcx
	movzbl	-184(%rbp), %ebx
	movq	%r15, %r9
.L1741:
	cmpb	$63, %bl
	jne	.L2271
	movl	(%r12), %r8d
	movq	-216(%rbp), %r14
	movl	%r8d, %ecx
	orb	$2, %ch
	movl	%ecx, (%r12)
	cmpq	%r14, %r13
	jb	.L1439
	andl	$16, %r8d
	cmpq	%r14, %r13
	jbe	.L1938
.L2348:
	movzbl	1(%r9), %ebx
	movq	%r14, %r9
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L2271:
	cmpb	$35, %bl
	jne	.L2474
	movq	-216(%rbp), %r14
	cmpq	%r14, %r13
	jnb	.L2258
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	-192(%rbp), %r8
	jmp	.L1425
.L1482:
	cmpq	%r9, %r13
	jb	.L1439
	movl	$-1, %ebx
	jbe	.L1458
	movzbl	(%r9), %ebx
	jmp	.L1458
.L2412:
	cmpq	%r9, %r13
	jnb	.L2281
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1545:
	testb	%dl, %dl
	je	.L2475
.L2389:
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	jbe	.L1929
	movzbl	1(%r9), %ebx
	movq	%rax, %r9
	cmpb	$92, %bl
	sete	%r11b
	jmp	.L1793
.L1541:
	movq	-208(%rbp), %r13
	movq	%r14, %r9
	movq	%r15, %r10
	jmp	.L2389
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	-216(%rbp), %rdi
	movl	(%rdi), %edx
	testb	$32, %dl
	jne	.L2476
.L1547:
	testb	$64, %dl
	jne	.L2477
.L1548:
	testb	$-128, %dl
	jne	.L2478
.L1549:
	testb	$2, %dh
	jne	.L2479
.L1550:
	andb	$1, %dh
	jne	.L2480
.L1551:
	movq	-216(%rbp), %rax
	addq	$1, %r14
	movl	4(%rax), %eax
	movl	%eax, 4(%r12)
	cmpq	%r14, -208(%rbp)
	jb	.L1439
	movl	$0, %edx
	jbe	.L1441
	movzbl	(%r14), %ebx
	movl	(%r12), %eax
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	jmp	.L1441
.L1708:
	movq	-128(%rbp), %rax
	cmpb	$37, (%rax)
	je	.L2481
.L1715:
	leaq	8(%r12), %r14
	jmp	.L1731
.L2418:
	movq	%rcx, %rdi
	leaq	.LC89(%rip), %rsi
	movq	%r9, -216(%rbp)
	movq	%rcx, -208(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-208(%rbp), %rcx
	movq	-216(%rbp), %r9
	testl	%eax, %eax
	jne	.L1710
.L1717:
	movq	%r12, %rdi
	movq	%rcx, -216(%rbp)
	leaq	8(%r12), %r14
	movq	%r9, -208(%rbp)
	call	_ZN4node3url12_GLOBAL__N_114ShortenUrlPathEPNS0_8url_dataE
	cmpb	$47, %bl
	movq	-208(%rbp), %r9
	movq	-216(%rbp), %rcx
	je	.L1713
	cmpb	$1, -184(%rbp)
	je	.L1713
	orl	$256, (%r12)
	leaq	200(%r12), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rcx, -208(%rbp)
	movq	%r9, -184(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE12emplace_backIJRA1_KcEEEvDpOT_
	movq	-184(%rbp), %r9
	movq	-208(%rbp), %rcx
	jmp	.L1713
.L1707:
	movq	-128(%rbp), %rax
	movzbl	(%rax), %edx
	cmpb	$46, %dl
	je	.L2482
	cmpb	$37, %dl
	jne	.L1715
	cmpb	$50, 1(%rax)
	jne	.L1715
	movzbl	2(%rax), %edx
	movl	%edx, %esi
	movl	%edx, %edi
	andl	$-33, %esi
	orl	$32, %edi
	subl	$65, %esi
	cmpb	$26, %sil
	cmovb	%edi, %edx
	cmpb	$101, %dl
	jne	.L1715
	cmpb	$46, 3(%rax)
	jne	.L1715
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1580:
	cmpq	%r9, %r13
	jb	.L1439
	movl	$-1, %eax
	cmovbe	%eax, %ebx
	jmp	.L1808
.L2470:
	cmpq	%r13, %r14
	ja	.L1439
	movl	(%r12), %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%r13, %r14
	jnb	.L1888
	movzbl	(%r14), %ebx
	movq	%r14, %r9
	leaq	8(%r12), %r10
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	jmp	.L1461
	.p2align 4,,10
	.p2align 3
.L1730:
	movq	-120(%rbp), %rax
	leaq	8(%r12), %r14
	cmpq	$1, %rax
	jne	.L2483
	movq	%rcx, %rdi
	leaq	.LC90(%rip), %rsi
	movq	%r9, -208(%rbp)
	movq	%rcx, -184(%rbp)
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	movq	-184(%rbp), %rcx
	movq	-208(%rbp), %r9
	testl	%eax, %eax
	je	.L1713
	jmp	.L1731
	.p2align 4,,10
	.p2align 3
.L1534:
	cmpq	%r14, %r13
	jb	.L1439
	leaq	8(%r12), %r10
.L1856:
	xorl	%edx, %edx
	cmpq	%r13, %r14
	movq	-216(%rbp), %rax
	movq	%r14, %r9
	sbbl	%ebx, %ebx
	andl	$48, %ebx
	leaq	8(%rax), %r15
	subl	$1, %ebx
	jmp	.L1440
.L1929:
	movq	%rax, %r9
	jmp	.L1792
.L2306:
	movb	%r13b, -235(%rbp)
	movq	-264(%rbp), %r15
	movq	-232(%rbp), %r13
.L1588:
	leaq	1(%r15), %r9
	movq	$0, -120(%rbp)
	movb	$0, (%rdx)
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%r9, %r13
	jbe	.L1950
	movzbl	(%r9), %r12d
	movb	$1, -232(%rbp)
	movq	%r9, %r15
	cmpb	$92, %r12b
	sete	%dl
	andl	%eax, %edx
	jmp	.L1819
	.p2align 4,,10
	.p2align 3
.L2462:
	movq	16(%r12), %rdx
	movq	%r15, %r14
	leaq	8(%r12), %r15
	xorl	%esi, %esi
	movl	$5, %r8d
	leaq	.LC2(%rip), %rcx
	movq	%r15, %rdi
	movq	%r9, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-208(%rbp), %r9
	orl	$16, (%r12)
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1455
	movzbl	(%r9), %ebx
	movl	$5, %r8d
	movq	%r15, %rdi
	leaq	.LC2(%rip), %rcx
	movq	%r9, -232(%rbp)
	cmpb	$47, %bl
	sete	%dl
	cmpb	$92, %bl
	sete	%al
	xorl	%esi, %esi
	orl	%eax, %edx
	movb	%dl, -208(%rbp)
	movq	16(%r12), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movzbl	-208(%rbp), %r10d
	movq	-232(%rbp), %r9
	testb	%r10b, %r10b
	jne	.L1456
	jmp	.L1457
.L2433:
	orb	$4, %ch
	movq	-216(%rbp), %rsi
	leaq	168(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%ecx, (%r12)
	addq	$168, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%r12), %ecx
	movq	-184(%rbp), %r9
	jmp	.L1530
.L2431:
	orb	$1, %ch
	movq	-216(%rbp), %rbx
	leaq	200(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%ecx, (%r12)
	leaq	200(%rbx), %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movl	(%rbx), %eax
	movl	(%r12), %ecx
	movq	-184(%rbp), %r9
	jmp	.L1528
.L2432:
	orb	$2, %ch
	movq	-216(%rbp), %rbx
	leaq	136(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%ecx, (%r12)
	leaq	136(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %eax
	movl	(%r12), %ecx
	movq	-184(%rbp), %r9
	jmp	.L1529
.L2420:
	movq	208(%r12), %rax
	movq	200(%r12), %r15
	movq	%rax, %rdx
	subq	%r15, %rdx
	cmpq	$63, %rdx
	jbe	.L1741
	movq	%rcx, -208(%rbp)
	movq	%r15, %r14
	movq	%r9, %r15
	movb	%bl, -184(%rbp)
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L1754:
	cmpq	$0, 8(%r14)
	jne	.L2311
	leaq	32(%r14), %rdx
	cmpq	%rax, %rdx
	je	.L1745
	subq	%rdx, %rax
	movq	%rax, %rcx
	sarq	$5, %rcx
	testq	%rax, %rax
	jle	.L1745
	movq	%rbx, %r12
	addq	$48, %r14
	movq	%rcx, %rbx
	jmp	.L1752
	.p2align 4,,10
	.p2align 3
.L1746:
	leaq	-32(%r14), %rcx
	cmpq	%rcx, %rdi
	je	.L2484
	movq	%rax, -48(%r14)
	movq	(%r14), %rax
	movq	-32(%r14), %rcx
	movq	%rdx, -40(%r14)
	movq	%rax, -32(%r14)
	testq	%rdi, %rdi
	je	.L1751
	movq	%rdi, -16(%r14)
	movq	%rcx, (%r14)
.L1749:
	movq	$0, -8(%r14)
	addq	$32, %r14
	movb	$0, (%rdi)
	subq	$1, %rbx
	je	.L2485
.L1752:
	movq	-16(%r14), %rax
	movq	-48(%r14), %rdi
	movq	-8(%r14), %rdx
	cmpq	%r14, %rax
	jne	.L1746
	testq	%rdx, %rdx
	je	.L1747
	cmpq	$1, %rdx
	je	.L2486
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-8(%r14), %rdx
	movq	-48(%r14), %rdi
.L1747:
	movq	%rdx, -40(%r14)
	movb	$0, (%rdi,%rdx)
	movq	-16(%r14), %rdi
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L2484:
	movq	%rax, -48(%r14)
	movq	(%r14), %rax
	movq	%rdx, -40(%r14)
	movq	%rax, -32(%r14)
.L1751:
	movq	%r14, -16(%r14)
	movq	%r14, %rdi
	jmp	.L1749
	.p2align 4,,10
	.p2align 3
.L2473:
	movq	200(%rbx), %r14
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	$63, %rdx
	ja	.L1754
	jmp	.L2311
	.p2align 4,,10
	.p2align 3
.L2486:
	movzbl	(%r14), %eax
	movb	%al, (%rdi)
	movq	-8(%r14), %rdx
	movq	-48(%r14), %rdi
	jmp	.L1747
.L1447:
	leaq	8(%r12), %r15
	movq	%r9, -184(%rbp)
	movq	%r15, %rdi
	call	_ZN4node3url12_GLOBAL__N_19IsSpecialERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-184(%rbp), %r9
	testb	%al, %al
	jne	.L1962
.L1854:
	leaq	1(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	movl	$-1, %ebx
	jbe	.L2342
	movzbl	1(%r9), %ebx
.L2342:
	movq	%rax, %r9
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L2411:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, 136(%r12)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 144(%r12)
.L1774:
	movq	-224(%rbp), %rax
	movq	%r15, %rdi
	movq	%r15, -224(%rbp)
	movq	%rax, -128(%rbp)
	jmp	.L1772
.L2313:
	movq	%r15, %r9
	jmp	.L1779
.L1681:
	movq	-128(%rbp), %rdx
	movq	%r14, %rcx
	cmpq	-224(%rbp), %rdx
	leaq	1(%rax), %r15
	cmovne	-112(%rbp), %rcx
	cmpq	%rcx, %r15
	ja	.L2487
.L1693:
	movb	%bl, (%rdx,%rax)
	movq	-128(%rbp), %rdx
	addq	$1, %r9
	movq	%r15, -120(%rbp)
	movb	$0, 1(%rdx,%rax)
	cmpq	%r9, %r13
	jb	.L1439
	movl	(%r12), %esi
	movl	$-1, %ebx
	movl	%esi, %eax
	shrl	$4, %eax
	andl	$1, %eax
	movb	%al, -208(%rbp)
	cmpq	%r9, %r13
	jbe	.L1803
	movzbl	(%r9), %ebx
	cmpb	$92, %bl
	sete	%r11b
	jmp	.L1803
.L2440:
	movzbl	%bl, %esi
	leaq	_ZN4node3url12_GLOBAL__N_1L15PATH_ENCODE_SETE(%rip), %rdx
	movq	%r15, %rdi
	addq	$1, %r14
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	cmpq	%r14, %r13
	jb	.L1439
	jbe	.L1865
	movzbl	(%r14), %ebx
	movl	(%r12), %eax
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	movb	%dl, -184(%rbp)
	jmp	.L1446
.L2439:
	orl	$128, (%r12)
	movq	-216(%rbp), %rax
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	104(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-184(%rbp), %r9
	jmp	.L1568
.L2438:
	orl	$64, (%r12)
	movq	-216(%rbp), %rbx
	leaq	72(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	72(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %eax
	movq	-184(%rbp), %r9
	jmp	.L1576
.L2437:
	orl	$32, (%r12)
	movq	-216(%rbp), %rbx
	leaq	40(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	40(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %eax
	movq	-184(%rbp), %r9
	jmp	.L1575
.L1533:
	cmpq	%r14, %r13
	jb	.L1439
	movl	(%r12), %edx
	shrl	$4, %edx
	andl	$1, %edx
	cmpq	%r14, %r13
	jbe	.L1888
	movzbl	(%r14), %ebx
	movq	%r14, %r9
	leaq	8(%r12), %r10
	cmpb	$92, %bl
	sete	%al
	andl	%eax, %edx
	jmp	.L1461
.L2455:
	cmpq	%r9, %r13
	jnb	.L2327
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L2449:
	testq	%rdx, %rdx
	je	.L1783
	cmpq	$1, %rdx
	je	.L2488
	movq	-224(%rbp), %rsi
	movq	%r9, -184(%rbp)
	call	memcpy@PLT
	movq	-120(%rbp), %rdx
	movq	168(%r12), %rdi
	movq	-184(%rbp), %r9
.L1783:
	movq	%rdx, 176(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L1785
.L2474:
	movq	-216(%rbp), %rdi
	cmpq	%rdi, %r13
	jb	.L1439
	movl	(%r12), %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%rdi, %r13
	ja	.L2323
	movb	$0, -184(%rbp)
	movl	$-1, %ebx
	jmp	.L1820
	.p2align 4,,10
	.p2align 3
.L2483:
	cmpq	$3, %rax
	jne	.L1731
	movq	-128(%rbp), %rax
	cmpb	$37, (%rax)
	jne	.L1731
	jmp	.L1732
	.p2align 4,,10
	.p2align 3
.L1949:
	movq	%rax, %r9
	movl	$-1, %ebx
	jmp	.L1808
.L2417:
	cmpb	$35, %bl
	jne	.L1706
	movb	$0, -184(%rbp)
	movq	%r15, %r9
	jmp	.L1444
.L1706:
	movq	%rcx, %rdi
	movzbl	%bl, %esi
	addq	$1, %r15
	movq	%rcx, -184(%rbp)
	leaq	_ZN4node3url12_GLOBAL__N_1L15PATH_ENCODE_SETE(%rip), %rdx
	call	_ZN4node3url12_GLOBAL__N_114AppendOrEscapeEPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEhPKh
	cmpq	%r15, %r13
	movq	-184(%rbp), %rcx
	jb	.L1439
	movl	$-1, %ebx
	jbe	.L1801
	movzbl	(%r15), %ebx
	movl	(%r12), %r14d
	shrl	$4, %r14d
	cmpb	$92, %bl
	sete	%al
	andl	%eax, %r14d
.L1801:
	movb	%r14b, -184(%rbp)
	jmp	.L1443
.L1700:
	cmpb	$-1, %bl
	je	.L1854
	cmpb	$47, %bl
	jne	.L1704
	leaq	1(%r9), %rdx
	cmpq	%rdx, %r13
	jb	.L1439
	movl	(%r12), %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%rdx, %r13
	jbe	.L1934
	movzbl	1(%r9), %ebx
	movq	%rdx, %r9
	cmpb	$92, %bl
	sete	%cl
	andl	%eax, %ecx
	movb	%cl, -184(%rbp)
	jmp	.L1442
	.p2align 4,,10
	.p2align 3
.L2450:
	movq	%rdx, %xmm0
	movq	%rsi, %xmm6
	movq	%rax, 168(%r12)
	punpcklqdq	%xmm6, %xmm0
	movups	%xmm0, 176(%r12)
.L1787:
	movq	-224(%rbp), %rax
	movq	%rax, -128(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	jmp	.L1785
.L2428:
	orl	$256, (%r12)
	movq	-216(%rbp), %rax
	leaq	200(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	200(%rax), %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movq	-184(%rbp), %r9
	jmp	.L1562
.L2427:
	orl	$512, (%r12)
	movq	-216(%rbp), %rbx
	leaq	136(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	136(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1561
.L2426:
	orl	$128, (%r12)
	movq	-216(%rbp), %rbx
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	104(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1560
.L2425:
	orl	$64, (%r12)
	movq	-216(%rbp), %rbx
	leaq	72(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	72(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1559
.L2480:
	orl	$256, (%r12)
	movq	-216(%rbp), %rax
	leaq	200(%r12), %rdi
	leaq	200(%rax), %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	jmp	.L1551
.L2460:
	orl	$256, (%r12)
	movq	-216(%rbp), %rax
	leaq	200(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	200(%rax), %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movq	-184(%rbp), %r9
	jmp	.L1556
.L2475:
	movq	-216(%rbp), %rdi
	movl	(%rdi), %edx
	testb	$32, %dl
	jne	.L2489
.L1565:
	testb	$64, %dl
	jne	.L2490
.L1566:
	testb	$-128, %dl
	jne	.L2491
.L1567:
	andb	$1, %dh
	je	.L1568
	orl	$256, (%r12)
	movq	-216(%rbp), %rax
	leaq	200(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	200(%rax), %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movq	%r12, %rdi
	call	_ZN4node3url12_GLOBAL__N_114ShortenUrlPathEPNS0_8url_dataE
	movq	-184(%rbp), %r9
	jmp	.L1568
.L2479:
	orl	$512, (%r12)
	movq	-216(%rbp), %rax
	leaq	136(%r12), %rdi
	leaq	136(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-216(%rbp), %rax
	movl	(%rax), %edx
	jmp	.L1550
.L2477:
	orl	$64, (%r12)
	movq	-216(%rbp), %rax
	movq	-248(%rbp), %rdi
	leaq	72(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-216(%rbp), %rax
	movl	(%rax), %edx
	jmp	.L1548
.L2476:
	orl	$32, %eax
	movq	-280(%rbp), %rsi
	leaq	40(%r12), %rdi
	movl	%eax, (%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-216(%rbp), %rax
	movl	(%rax), %edx
	jmp	.L1547
.L2459:
	orl	$128, (%r12)
	movq	-216(%rbp), %rbx
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	104(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1555
.L2458:
	orl	$64, (%r12)
	movq	-216(%rbp), %rbx
	leaq	72(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	72(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1554
.L2478:
	orl	$128, (%r12)
	movq	-216(%rbp), %rax
	leaq	104(%r12), %rdi
	leaq	104(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-216(%rbp), %rax
	movl	(%rax), %edx
	jmp	.L1549
.L2481:
	cmpb	$50, 1(%rax)
	jne	.L1715
	movzbl	2(%rax), %edx
	movl	%edx, %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpb	$25, %sil
	ja	.L1715
	orl	$32, %edx
	cmpb	$101, %dl
	jne	.L1715
	cmpb	$37, 3(%rax)
	jne	.L1715
	cmpb	$50, 4(%rax)
	jne	.L1715
	movzbl	5(%rax), %eax
	movl	%eax, %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	ja	.L1719
	orl	$32, %eax
.L1719:
	cmpb	$101, %al
	jne	.L1715
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L1865:
	movb	$0, -184(%rbp)
	movl	$-1, %ebx
	jmp	.L1446
.L1633:
	leaq	8(%r12), %r15
	movq	%r9, -184(%rbp)
	movq	%r15, %rdi
	call	_ZN4node3url12_GLOBAL__N_113NormalizePortERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEi
	movq	-184(%rbp), %r9
	cmpl	$-1, %eax
	movl	%eax, 4(%r12)
	jne	.L1835
	orl	$2048, (%r12)
.L1835:
	movq	-128(%rbp), %rax
	movq	$0, -120(%rbp)
	movb	$0, (%rax)
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1459
	movzbl	(%r9), %ebx
	jmp	.L1448
.L2467:
	cmpb	$91, %bl
	je	.L1903
	movzbl	-233(%rbp), %eax
	cmpb	$93, %bl
	cmove	%edx, %eax
	movb	%al, -233(%rbp)
.L1622:
	movq	-120(%rbp), %rax
	movq	-128(%rbp), %rdx
	movl	$15, %ecx
	cmpq	-224(%rbp), %rdx
	cmovne	-112(%rbp), %rcx
	leaq	1(%rax), %r10
	cmpq	%rcx, %r10
	ja	.L2492
.L1624:
	movb	%bl, (%rdx,%rax)
	movq	-128(%rbp), %rdx
	addq	$1, %r9
	movq	%r10, -120(%rbp)
	movb	$0, 1(%rdx,%rax)
	cmpq	%r9, %r13
	jnb	.L1437
	jmp	.L1439
.L2466:
	leaq	3(%r14), %rsi
	movq	-208(%rbp), %r14
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	%r14, %rdi
	movl	$3, %r8d
	xorl	%edx, %edx
	leaq	.LC87(%rip), %rcx
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-120(%rbp), %r14
	jmp	.L1587
.L1704:
	cmpq	%r9, %r13
	jb	.L1439
	movl	$-1, %ebx
	jbe	.L1442
	jmp	.L2331
.L2487:
	xorl	%edx, %edx
	movq	%rax, %rsi
	leaq	-128(%rbp), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r9, -216(%rbp)
	movb	%r11b, -208(%rbp)
	movq	%rax, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-128(%rbp), %rdx
	movq	-216(%rbp), %r9
	movzbl	-208(%rbp), %r11d
	movq	-184(%rbp), %rax
	jmp	.L1693
.L1643:
	movq	-216(%rbp), %rax
	movl	(%rax), %edx
	movl	(%r12), %eax
	testb	$-128, %dl
	jne	.L2493
.L1653:
	testb	$1, %dh
	jne	.L2494
.L1654:
	andb	$2, %dh
	jne	.L2495
.L1655:
	orb	$4, %ah
	movl	%eax, (%r12)
	jmp	.L2392
.L2482:
	cmpb	$37, 1(%rax)
	jne	.L1715
	cmpb	$50, 2(%rax)
	jne	.L1715
	movzbl	3(%rax), %eax
	movl	%eax, %edx
	movl	%eax, %esi
	andl	$-33, %edx
	orl	$32, %esi
	subl	$65, %edx
	cmpb	$26, %dl
	cmovb	%esi, %eax
	cmpb	$101, %al
	jne	.L1715
	jmp	.L1717
	.p2align 4,,10
	.p2align 3
.L2413:
	movq	-216(%rbp), %rax
	movl	(%rax), %eax
	testb	$-128, %al
	jne	.L2496
.L1646:
	testb	$1, %ah
	jne	.L2497
.L1647:
	leaq	1(%r9), %r14
	testb	$2, %ah
	jne	.L2498
	cmpq	%r14, %r13
	jb	.L1439
.L2256:
	jbe	.L1945
	movzbl	1(%r9), %ebx
	movq	%r14, %r9
	cmpb	$92, %bl
	sete	%r11b
	jmp	.L1470
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	-216(%rbp), %rax
	movl	(%rax), %edx
	movl	(%r12), %eax
	testb	$-128, %dl
	jne	.L2499
.L1650:
	andb	$1, %dh
	jne	.L2500
.L1651:
	orb	$2, %ah
	movl	%eax, (%r12)
	jmp	.L2399
.L2465:
	movzbl	-112(%rbp), %eax
	movb	%al, (%rdi)
	movq	-120(%rbp), %rdx
	movq	136(%r12), %rdi
	jmp	.L1770
.L2441:
	cmpl	$-1, -196(%rbp)
	movq	-120(%rbp), %rbx
	jne	.L1493
	jmp	.L1494
.L1492:
	cmpl	$-1, -196(%rbp)
	jne	.L2285
	movq	$0, -120(%rbp)
	movb	$0, (%r8)
	cmpq	%r13, -256(%rbp)
	jnb	.L2501
	movq	-256(%rbp), %r9
	movl	(%r12), %ecx
	movzbl	(%r9), %ebx
	jmp	.L1458
.L1511:
	movq	%r14, %rcx
	movq	%r10, %r15
	cmpq	%r14, %r13
	jb	.L1439
	jbe	.L1913
	movzbl	1(%r9), %ebx
	movq	%rcx, %r9
	cmpb	$47, %bl
	sete	%r14b
	cmpb	$92, %bl
	sete	%al
	orl	%eax, %r14d
	jmp	.L1795
.L1508:
	andl	$-17, %eax
	xorl	%r15d, %r15d
	jmp	.L1509
.L2471:
	movq	-208(%rbp), %rdi
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%rax, -264(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-128(%rbp), %r8
	movq	-264(%rbp), %rax
	jmp	.L1603
.L2491:
	orl	$128, (%r12)
	movq	-216(%rbp), %rbx
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	104(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1567
.L2490:
	orl	$64, (%r12)
	movq	-216(%rbp), %rbx
	leaq	72(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	72(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1566
.L2489:
	orl	$32, %eax
	movq	%rdi, %rbx
	leaq	40(%rdi), %rsi
	movq	%r9, -184(%rbp)
	movl	%eax, (%r12)
	leaq	40(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movq	-184(%rbp), %r9
	jmp	.L1565
.L2442:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movq	%r9, -264(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-128(%rbp), %r8
	movq	-264(%rbp), %r9
	jmp	.L1497
.L2488:
	movzbl	-112(%rbp), %eax
	movb	%al, (%rdi)
	movq	-120(%rbp), %rdx
	movq	168(%r12), %rdi
	jmp	.L1783
.L2492:
	leaq	-128(%rbp), %r15
	xorl	%edx, %edx
	movq	%rax, %rsi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movq	%r9, -272(%rbp)
	movq	%r10, -264(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-128(%rbp), %rdx
	movq	-272(%rbp), %r9
	movq	-264(%rbp), %r10
	movq	-208(%rbp), %rax
	jmp	.L1624
.L1903:
	movb	$1, -233(%rbp)
	jmp	.L1622
.L2500:
	orb	$1, %ah
	movq	-216(%rbp), %rsi
	leaq	200(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%eax, (%r12)
	addq	$200, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movl	(%r12), %eax
	movq	-184(%rbp), %r9
	jmp	.L1651
.L2499:
	orb	$-128, %al
	movq	-216(%rbp), %rbx
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%eax, (%r12)
	leaq	104(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movl	(%r12), %eax
	movq	-184(%rbp), %r9
	jmp	.L1650
.L2472:
	movzbl	2(%rax), %edx
	movl	%edx, %esi
	andl	$-33, %esi
	subl	$65, %esi
	cmpb	$25, %sil
	ja	.L1726
	orl	$32, %edx
	cmpb	$101, %dl
	jne	.L1726
	cmpb	$1, -184(%rbp)
	je	.L1726
	cmpb	$47, %bl
	jne	.L1728
	jmp	.L1726
	.p2align 4,,10
	.p2align 3
.L2497:
	orl	$256, (%r12)
	movq	-216(%rbp), %rbx
	leaq	200(%r12), %rdi
	movq	%r9, -208(%rbp)
	leaq	200(%rbx), %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movl	(%rbx), %eax
	movq	-208(%rbp), %r9
	jmp	.L1647
.L2496:
	orl	$128, (%r12)
	movq	-216(%rbp), %rbx
	leaq	104(%r12), %rdi
	leaq	104(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %eax
	movq	-208(%rbp), %r9
	jmp	.L1646
.L2498:
	orl	$512, (%r12)
	movq	-216(%rbp), %rax
	leaq	136(%r12), %rdi
	movq	%r9, -208(%rbp)
	leaq	136(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	cmpq	%r14, %r13
	movq	-208(%rbp), %r9
	jnb	.L2256
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L2443:
	testq	%rdx, %rdx
	je	.L1503
	cmpq	$1, %rdx
	je	.L2502
	movq	-224(%rbp), %rsi
	movq	%r10, -264(%rbp)
	movq	%r9, -208(%rbp)
	call	memcpy@PLT
	movq	-120(%rbp), %rdx
	movq	8(%r12), %rdi
	movq	-264(%rbp), %r10
	movq	-208(%rbp), %r9
.L1503:
	movq	%rdx, 16(%r12)
	movb	$0, (%rdi,%rdx)
	movq	-128(%rbp), %rdi
	jmp	.L1505
.L2452:
	cmpq	$2, -120(%rbp)
	jne	.L1734
	movq	-128(%rbp), %rsi
	movzbl	(%rsi), %edx
	andl	$-33, %edx
	subl	$65, %edx
	cmpb	$25, %dl
	ja	.L1734
	movzbl	1(%rsi), %edx
	cmpb	$58, %dl
	je	.L1967
	cmpb	$124, %dl
	jne	.L1734
.L1967:
	testb	$-128, %al
	je	.L1736
	cmpq	$0, 112(%r12)
	je	.L1736
	movq	$0, 112(%r12)
	movq	104(%r12), %rax
	movb	$0, (%rax)
	orl	$128, (%r12)
.L1736:
	movq	-128(%rbp), %rax
	movb	$58, 1(%rax)
	movl	(%r12), %eax
	jmp	.L1734
.L2495:
	orb	$2, %ah
	movq	-216(%rbp), %rsi
	leaq	136(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%eax, (%r12)
	addq	$136, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%r12), %eax
	movq	-184(%rbp), %r9
	jmp	.L1655
.L2494:
	orb	$1, %ah
	movq	-216(%rbp), %rbx
	leaq	200(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%eax, (%r12)
	leaq	200(%rbx), %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movl	(%rbx), %edx
	movl	(%r12), %eax
	movq	-184(%rbp), %r9
	jmp	.L1654
.L2493:
	orb	$-128, %al
	movq	-216(%rbp), %rbx
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%eax, (%r12)
	leaq	104(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %edx
	movl	(%r12), %eax
	movq	-184(%rbp), %r9
	jmp	.L1653
.L2444:
	movq	%rdx, %xmm4
	movq	%rax, 8(%r12)
	punpcklqdq	%xmm0, %xmm4
	movups	%xmm4, 16(%r12)
.L1507:
	movq	-224(%rbp), %rax
	movq	%rax, -128(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -224(%rbp)
	movq	%rax, %rdi
	jmp	.L1505
.L2469:
	orb	$-128, %al
	movq	%rdi, %rsi
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	movl	%eax, (%r12)
	addq	$104, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-184(%rbp), %r9
	jmp	.L2397
.L1965:
	cmpq	$2, %rdx
	je	.L1659
	movzbl	2(%r9), %eax
	leal	-35(%rax), %ecx
	cmpb	$57, %cl
	ja	.L1657
	movabsq	$144115188344295425, %rax
	shrq	%cl, %rax
	notq	%rax
	andb	$1, %al
	movb	%al, -184(%rbp)
	jne	.L1657
	cmpq	%r13, %r9
	ja	.L1439
	movl	(%r12), %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%r13, %r9
	jnb	.L2330
	jmp	.L2326
	.p2align 4,,10
	.p2align 3
.L2415:
	orl	$256, (%r12)
	movq	-216(%rbp), %rsi
	leaq	200(%r12), %rdi
	movq	%r9, -184(%rbp)
	addq	$200, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EEaSERKS7_
	movq	-184(%rbp), %r9
	jmp	.L1662
.L2423:
	orl	$1, %eax
	movq	-128(%rbp), %r8
	movl	%eax, (%r12)
	jmp	.L1483
.L1966:
	cmpq	$2, %rdx
	je	.L1670
	movzbl	2(%r9), %eax
	leal	-35(%rax), %ecx
	cmpb	$57, %cl
	ja	.L1668
	movabsq	$144115188344295425, %rax
	shrq	%cl, %rax
	notq	%rax
	andb	$1, %al
	movb	%al, -184(%rbp)
	jne	.L1668
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L2330
	jmp	.L2328
.L1621:
	orl	$8, %eax
	movq	-128(%rbp), %r8
	movl	%eax, (%r12)
	jmp	.L1483
.L2414:
	orl	$128, (%r12)
	movq	-216(%rbp), %rbx
	leaq	104(%r12), %rdi
	movq	%r9, -184(%rbp)
	leaq	104(%rbx), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	(%rbx), %eax
	movq	-184(%rbp), %r9
	jmp	.L1661
.L1659:
	cmpq	%r13, %r9
	ja	.L1439
	movl	(%r12), %eax
	shrl	$4, %eax
	andl	$1, %eax
	cmpq	%r13, %r9
	jnb	.L1962
	jmp	.L2326
.L2446:
	leaq	2(%r9), %rax
	cmpq	%rax, %r13
	jb	.L1439
	jbe	.L1914
	cmpb	$47, 2(%r9)
	movq	%rax, %r9
	je	.L1537
	jmp	.L2365
.L1670:
	cmpq	%r9, %r13
	jb	.L1439
	jbe	.L1962
	jmp	.L2328
.L2468:
	orb	$1, %ah
	movq	208(%r12), %r14
	movl	%eax, (%r12)
	cmpq	216(%r12), %r14
	je	.L1673
	leaq	16(%r14), %rax
	movq	%rax, (%r14)
	movq	(%r8), %rbx
	testq	%rbx, %rbx
	je	.L2503
	movq	%r15, -168(%rbp)
	cmpq	$15, %r15
	ja	.L2504
.L1675:
	movq	(%r14), %rdi
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r9, -184(%rbp)
	call	memcpy@PLT
	movq	-168(%rbp), %rax
	movq	(%r14), %rdx
	movq	-184(%rbp), %r9
	movq	%rax, 8(%r14)
	movb	$0, (%rdx,%rax)
	addq	$32, 208(%r12)
	jmp	.L2401
.L2501:
	movl	(%r12), %ecx
	movq	-256(%rbp), %r9
	movl	$-1, %ebx
	jmp	.L1458
.L2502:
	movzbl	-112(%rbp), %eax
	movb	%al, (%rdi)
	movq	-120(%rbp), %rdx
	movq	8(%r12), %rdi
	jmp	.L1503
.L2453:
	testq	%rdx, %rdx
	je	.L1514
	movq	8(%rax), %rsi
	movq	8(%r12), %rdi
	movq	%r10, -264(%rbp)
	movq	%r9, -208(%rbp)
	call	memcmp@PLT
	movq	-208(%rbp), %r9
	movq	-264(%rbp), %r10
	testl	%eax, %eax
	jne	.L2230
	cmpq	%r14, %r13
	jb	.L1439
	jbe	.L1944
	movzbl	1(%r9), %ebx
	jmp	.L1476
.L1940:
	movq	%r14, %r9
	jmp	.L1814
.L1944:
	movl	(%r12), %esi
	movq	%r14, %r9
	jmp	.L1817
.L1514:
	cmpq	%r14, %r13
	jb	.L1439
	jbe	.L1932
	cmpb	$47, 1(%r9)
	jne	.L2505
	addq	$2, %r9
	cmpq	%r9, %r13
	ja	.L1855
	jmp	.L1856
.L1950:
	movb	$1, -232(%rbp)
	movq	%rbx, %r12
	jmp	.L1818
.L1914:
	movq	%rax, %r9
	jmp	.L1796
.L1765:
	cmpq	%r9, %r13
	jnb	.L2277
	jmp	.L1439
.L2505:
	movzbl	1(%r9), %ebx
	movl	(%r12), %eax
	movq	%r14, %r9
	shrl	$4, %eax
	cmpb	$92, %bl
	sete	%dl
	andl	%eax, %edx
	jmp	.L1461
.L1932:
	movq	%r14, %r9
	jmp	.L1460
.L1942:
	movq	%rax, %r9
	movl	$-1, %ebx
	jmp	.L1442
.L1948:
	movq	%rax, %r9
	xorl	%edx, %edx
	movl	$-1, %ebx
	jmp	.L1471
.L2504:
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r9, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-184(%rbp), %r9
	movq	%rax, (%r14)
	movq	-168(%rbp), %rax
	movq	%rax, 16(%r14)
	jmp	.L1675
.L2503:
	leaq	.LC84(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1673:
	leaq	200(%r12), %rdi
	movq	%r8, %rdx
	movq	%r14, %rsi
	movq	%r9, -184(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-184(%rbp), %r9
	cmpq	%r9, %r13
	jnb	.L2262
	jmp	.L1439
.L1945:
	movq	%r14, %r9
	jmp	.L1816
.L1517:
	cmpq	%r14, %r13
	jb	.L1439
	jbe	.L1916
	movzbl	1(%r9), %ebx
	cmpb	$47, %bl
	jne	.L2506
	leaq	2(%r9), %rax
	movq	%r14, %r9
	cmpq	%rax, %r13
	ja	.L1857
	jmp	.L1808
.L2405:
	call	__stack_chk_fail@PLT
.L2506:
	movq	%r14, %r9
	jmp	.L1808
.L1916:
	movq	%r14, %r9
	movl	$-1, %ebx
	jmp	.L1808
.L2316:
	movq	%r15, %r9
	jmp	.L1815
.L1501:
	orl	$8, (%r12)
	movq	-128(%rbp), %r8
	jmp	.L1483
.L1934:
	movq	%rdx, %r9
	movl	$-1, %ebx
	jmp	.L1442
.L1710:
	movq	-120(%rbp), %rax
	jmp	.L1709
	.cfi_endproc
.LFE7484:
	.size	_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b, .-_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b
	.section	.rodata.str1.1
.LC91:
	.string	"25"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node3url3URL12FromFilePathERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node3url3URL12FromFilePathERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node3url3URL12FromFilePathERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB7504:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$12090, %ecx
	pxor	%xmm0, %xmm0
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	$-1, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$80, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -104(%rbp)
	movl	$7, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movw	%cx, -76(%rbp)
	movq	%rdi, %rcx
	movabsq	$-4294967296, %rax
	movq	%rax, (%rdi)
	leaq	24(%rdi), %rax
	movq	%rax, 8(%rdi)
	leaq	56(%rdi), %rax
	movq	%rax, 40(%rdi)
	leaq	88(%rdi), %rax
	movq	%rax, 72(%rdi)
	leaq	120(%rdi), %rax
	movq	%rax, 104(%rdi)
	leaq	152(%rdi), %rax
	movq	%rax, 136(%rdi)
	leaq	184(%rdi), %rax
	movq	$0, 16(%rdi)
	movb	$0, 24(%rdi)
	movq	$0, 48(%rdi)
	movb	$0, 56(%rdi)
	movq	$0, 80(%rdi)
	movb	$0, 88(%rdi)
	movq	$0, 112(%rdi)
	movb	$0, 120(%rdi)
	movq	$0, 144(%rdi)
	movb	$0, 152(%rdi)
	movq	%r14, -96(%rbp)
	movl	$1701603686, -80(%rbp)
	movb	$47, -74(%rbp)
	movq	$7, -88(%rbp)
	movb	$0, -73(%rbp)
	movq	%rax, 168(%rdi)
	movq	$0, 176(%rdi)
	movb	$0, 184(%rdi)
	movq	$0, 216(%rdi)
	movups	%xmm0, 200(%rdi)
	movq	%r14, %rdi
	pushq	$0
	call	_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b
	movq	-96(%rbp), %rdi
	popq	%rsi
	movq	-104(%rbp), %r10
	popq	%r8
	cmpq	%r14, %rdi
	je	.L2508
	call	_ZdlPv@PLT
	movq	-104(%rbp), %r10
.L2508:
	movq	8(%r10), %r15
	movq	%r14, -96(%rbp)
	movq	%r14, %rdi
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	testq	%r15, %r15
	je	.L2510
	movq	(%r10), %rax
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	movl	$1, %r13d
	movzbl	(%rax), %eax
	.p2align 4,,10
	.p2align 3
.L2517:
	movb	%al, (%rdi,%r15)
	movq	-96(%rbp), %rax
	movq	%r13, -88(%rbp)
	movb	$0, (%rax,%r13)
	movq	(%r10), %rax
	cmpb	$37, (%rax,%rbx)
	je	.L2521
.L2513:
	movq	-88(%rbp), %r15
	movq	-96(%rbp), %rdi
	addq	$1, %rbx
	cmpq	8(%r10), %rbx
	jnb	.L2510
	movq	(%r10), %rax
	cmpq	%r14, %rdi
	movl	$15, %edx
	cmovne	-80(%rbp), %rdx
	leaq	1(%r15), %r13
	movzbl	(%rax,%rbx), %eax
	cmpq	%r13, %rdx
	jnb	.L2517
	leaq	-96(%rbp), %rdi
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r10, -112(%rbp)
	movb	%al, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_mutateEmmPKcm@PLT
	movq	-96(%rbp), %rdi
	movq	-112(%rbp), %r10
	movzbl	-104(%rbp), %eax
	jmp	.L2517
	.p2align 4,,10
	.p2align 3
.L2521:
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L2522
	leaq	-96(%rbp), %rdi
	movl	$2, %edx
	leaq	.LC91(%rip), %rsi
	movq	%r10, -104(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-104(%rbp), %r10
	jmp	.L2513
	.p2align 4,,10
	.p2align 3
.L2510:
	subq	$8, %rsp
	movl	$16, %edx
	xorl	%r9d, %r9d
	movq	%r12, %rcx
	pushq	$0
	movl	$1, %r8d
	movq	%r15, %rsi
	call	_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b
	movq	-96(%rbp), %rdi
	popq	%rax
	popq	%rdx
	cmpq	%r14, %rdi
	je	.L2507
	call	_ZdlPv@PLT
.L2507:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2523
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2522:
	.cfi_restore_state
	leaq	.LC8(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L2523:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7504:
	.size	_ZN4node3url3URL12FromFilePathERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node3url3URL12FromFilePathERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.type	_ZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7490:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$3160, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2818
	cmpw	$1040, %cx
	jne	.L2525
.L2818:
	movq	23(%rdx), %r12
.L2527:
	cmpl	$4, 16(%rbx)
	jg	.L3071
	leaq	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3071:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2528
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2528
	movq	-16(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L3072
.L2530:
	subq	$16, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L2532
	cmpl	$3, 16(%rbx)
	jg	.L3073
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	movq	88(%rdi), %rax
	addq	$88, %rdi
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L3074
.L2535:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L3075
	cmpl	$4, 16(%rbx)
	jg	.L3076
.L2536:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2539:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L3077
	movl	16(%rbx), %eax
	cmpl	$5, %eax
	jg	.L2541
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L2542:
	movq	(%rdi), %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L3078
.L2543:
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L2545
	movl	16(%rbx), %eax
	jmp	.L2544
	.p2align 4,,10
	.p2align 3
.L3072:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L2530
	movslq	43(%rax), %rax
	subl	$3, %eax
	andl	$-3, %eax
	jne	.L2530
.L2531:
	movq	-24(%rdi), %rax
	subq	$24, %rdi
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2535
.L3074:
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L2535
	movslq	43(%rax), %rax
	subl	$3, %eax
	andl	$-3, %eax
	jne	.L2535
	cmpl	$4, 16(%rbx)
	jle	.L2536
.L3076:
	movq	8(%rbx), %rax
	leaq	-32(%rax), %rdi
	jmp	.L2539
	.p2align 4,,10
	.p2align 3
.L3078:
	movq	-1(%rdx), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L2543
	cmpl	$5, 43(%rdx)
	jne	.L2543
.L2544:
	testl	%eax, %eax
	jg	.L3079
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L2548:
	movq	352(%r12), %rsi
	leaq	-2160(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jg	.L2549
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2550:
	call	_ZNK2v85Value8IsNumberEv@PLT
	movl	$-1, -2952(%rbp)
	testb	%al, %al
	je	.L2551
	cmpl	$1, 16(%rbx)
	jg	.L2552
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L2553:
	movq	3280(%r12), %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L3080
.L2554:
	shrq	$32, %rax
	movq	%rax, -2952(%rbp)
.L2551:
	movl	16(%rbx), %edx
	movq	8(%rbx), %rax
	cmpl	$5, %edx
	jg	.L2555
	movq	(%rbx), %rcx
	movq	8(%rcx), %r13
	addq	$88, %r13
	cmpl	$5, %edx
	je	.L3081
	cmpl	$4, %edx
	je	.L3082
	movq	%r13, %r15
	cmpl	$3, %edx
	je	.L3083
	movq	%r13, -3152(%rbp)
	movq	%r13, -3136(%rbp)
.L2560:
	movq	-2160(%rbp), %rsi
	addq	$8, %rax
	movq	3280(%r12), %r14
	leaq	-2424(%rbp), %rbx
	movq	%rax, -3144(%rbp)
	leaq	-2896(%rbp), %rax
	movq	%rsi, -3112(%rbp)
	movq	-2144(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, -3120(%rbp)
	movq	%rsi, -3104(%rbp)
	movq	352(%r12), %rsi
	movq	%rsi, -3128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	%r15, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	movq	%r13, %rdi
	movb	%al, -2936(%rbp)
	call	_ZNK2v85Value8IsObjectEv@PLT
	leaq	-2808(%rbp), %rcx
	pxor	%xmm0, %xmm0
	leaq	-2712(%rbp), %rsi
	movq	%rcx, -3008(%rbp)
	movq	%rcx, -2824(%rbp)
	leaq	-2776(%rbp), %rcx
	movq	%rcx, -3016(%rbp)
	movq	%rcx, -2792(%rbp)
	leaq	-2744(%rbp), %rcx
	movq	%rcx, -3024(%rbp)
	movq	%rcx, -2760(%rbp)
	leaq	-2680(%rbp), %rcx
	movq	%rsi, -3032(%rbp)
	movq	%rsi, -2728(%rbp)
	leaq	-2648(%rbp), %rsi
	movq	%rcx, -3040(%rbp)
	movq	%rcx, -2696(%rbp)
	leaq	-2584(%rbp), %rcx
	movb	%al, -2937(%rbp)
	movabsq	$-4294967296, %rax
	movq	%rsi, -3048(%rbp)
	movq	%rsi, -2664(%rbp)
	leaq	-2552(%rbp), %rsi
	movq	%rcx, -3056(%rbp)
	movq	%rcx, -2600(%rbp)
	leaq	-2520(%rbp), %rcx
	movq	%rax, -2832(%rbp)
	movq	$0, -2816(%rbp)
	movb	$0, -2808(%rbp)
	movq	$0, -2784(%rbp)
	movb	$0, -2776(%rbp)
	movq	$0, -2752(%rbp)
	movb	$0, -2744(%rbp)
	movq	$0, -2720(%rbp)
	movb	$0, -2712(%rbp)
	movq	$0, -2688(%rbp)
	movb	$0, -2680(%rbp)
	movq	$0, -2656(%rbp)
	movb	$0, -2648(%rbp)
	movq	$0, -2616(%rbp)
	movq	%rax, -2608(%rbp)
	movq	$0, -2592(%rbp)
	movups	%xmm0, -2632(%rbp)
	movb	$0, -2584(%rbp)
	cmpb	$0, -2936(%rbp)
	movq	%rsi, -3064(%rbp)
	movq	%rsi, -2568(%rbp)
	leaq	-2488(%rbp), %rsi
	movq	%rcx, -3072(%rbp)
	movq	%rcx, -2536(%rbp)
	leaq	-2456(%rbp), %rcx
	movq	$0, -2560(%rbp)
	movb	$0, -2552(%rbp)
	movq	$0, -2528(%rbp)
	movb	$0, -2520(%rbp)
	movq	%rsi, -3080(%rbp)
	movq	%rsi, -2504(%rbp)
	movq	$0, -2496(%rbp)
	movb	$0, -2488(%rbp)
	movq	%rcx, -3088(%rbp)
	movq	%rcx, -2472(%rbp)
	movq	$0, -2464(%rbp)
	movb	$0, -2456(%rbp)
	movq	%rbx, -3096(%rbp)
	movq	%rbx, -2440(%rbp)
	movq	$0, -2432(%rbp)
	movb	$0, -2424(%rbp)
	movq	$0, -2392(%rbp)
	movups	%xmm0, -2408(%rbp)
	jne	.L3084
.L2562:
	cmpb	$0, -2937(%rbp)
	jne	.L3085
.L2637:
	movzbl	-2937(%rbp), %eax
	subq	$8, %rsp
	movl	-2952(%rbp), %ebx
	leaq	-2608(%rbp), %r13
	movzbl	-2936(%rbp), %r8d
	movq	-3112(%rbp), %rsi
	movq	%r13, %rcx
	leaq	-2832(%rbp), %r9
	pushq	%rax
	movq	-3104(%rbp), %rdi
	movl	%ebx, %edx
	call	_ZN4node3url3URL5ParseEPKcmNS0_15url_parse_stateEPNS0_8url_dataEbPKS5_b
	movl	-2608(%rbp), %eax
	popq	%rdx
	popq	%rcx
	testb	$4, %al
	jne	.L2753
	cmpl	$-1, %ebx
	je	.L2754
	testb	$8, %al
	jne	.L2753
.L2754:
	movq	-3128(%rbp), %rsi
	leaq	88(%rsi), %rbx
	testb	$1, %al
	jne	.L2780
	movq	%rbx, %xmm0
	leaq	104(%rsi), %rax
	movq	%r13, %rdx
	movq	%r12, %rdi
	punpcklqdq	%xmm0, %xmm0
	movq	%rbx, -1056(%rbp)
	leaq	-1104(%rbp), %rbx
	movaps	%xmm0, -1104(%rbp)
	movaps	%xmm0, -1088(%rbp)
	movq	%rsi, %xmm0
	movq	%rbx, %rsi
	punpcklqdq	%xmm0, %xmm0
	paddq	.LC92(%rip), %xmm0
	movq	%rax, -1048(%rbp)
	movq	%rax, -1040(%rbp)
	movaps	%xmm0, -1072(%rbp)
	call	_ZN4node3url12_GLOBAL__N_17SetArgsEPNS_11EnvironmentEPN2v85LocalINS4_5ValueEEERKNS0_8url_dataE
	movq	%rbx, %r8
	movl	$9, %ecx
	movq	%r14, %rsi
	movq	-3144(%rbp), %rdx
	movq	-3152(%rbp), %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
.L2781:
	movq	-2400(%rbp), %rbx
	movq	-2408(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2783
	.p2align 4,,10
	.p2align 3
.L2787:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2784
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2787
.L2785:
	movq	-2408(%rbp), %r12
.L2783:
	testq	%r12, %r12
	je	.L2788
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2788:
	movq	-2440(%rbp), %rdi
	cmpq	-3096(%rbp), %rdi
	je	.L2789
	call	_ZdlPv@PLT
.L2789:
	movq	-2472(%rbp), %rdi
	cmpq	-3088(%rbp), %rdi
	je	.L2790
	call	_ZdlPv@PLT
.L2790:
	movq	-2504(%rbp), %rdi
	cmpq	-3080(%rbp), %rdi
	je	.L2791
	call	_ZdlPv@PLT
.L2791:
	movq	-2536(%rbp), %rdi
	cmpq	-3072(%rbp), %rdi
	je	.L2792
	call	_ZdlPv@PLT
.L2792:
	movq	-2568(%rbp), %rdi
	cmpq	-3064(%rbp), %rdi
	je	.L2793
	call	_ZdlPv@PLT
.L2793:
	movq	-2600(%rbp), %rdi
	cmpq	-3056(%rbp), %rdi
	je	.L2794
	call	_ZdlPv@PLT
.L2794:
	movq	-2624(%rbp), %rbx
	movq	-2632(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2795
	.p2align 4,,10
	.p2align 3
.L2799:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2796
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2799
.L2797:
	movq	-2632(%rbp), %r12
.L2795:
	testq	%r12, %r12
	je	.L2800
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2800:
	movq	-2664(%rbp), %rdi
	cmpq	-3048(%rbp), %rdi
	je	.L2801
	call	_ZdlPv@PLT
.L2801:
	movq	-2696(%rbp), %rdi
	cmpq	-3040(%rbp), %rdi
	je	.L2802
	call	_ZdlPv@PLT
.L2802:
	movq	-2728(%rbp), %rdi
	cmpq	-3032(%rbp), %rdi
	je	.L2803
	call	_ZdlPv@PLT
.L2803:
	movq	-2760(%rbp), %rdi
	cmpq	-3024(%rbp), %rdi
	je	.L2804
	call	_ZdlPv@PLT
.L2804:
	movq	-2792(%rbp), %rdi
	cmpq	-3016(%rbp), %rdi
	je	.L2805
	call	_ZdlPv@PLT
.L2805:
	movq	-2824(%rbp), %rdi
	cmpq	-3008(%rbp), %rdi
	je	.L2806
	call	_ZdlPv@PLT
.L2806:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-3120(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2524
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2524
	call	free@PLT
.L2524:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L3086
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2541:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	leaq	-40(%rsi), %rdi
	jmp	.L2542
	.p2align 4,,10
	.p2align 3
.L2528:
	leaq	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2525:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2527
	.p2align 4,,10
	.p2align 3
.L2796:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2799
	jmp	.L2797
	.p2align 4,,10
	.p2align 3
.L2784:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2787
	jmp	.L2785
	.p2align 4,,10
	.p2align 3
.L2555:
	leaq	-40(%rax), %rbx
	movq	%rbx, -3136(%rbp)
.L2557:
	leaq	-32(%rax), %rsi
	movq	%rsi, -3152(%rbp)
.L2559:
	leaq	-24(%rax), %r15
.L2561:
	leaq	-16(%rax), %r13
	jmp	.L2560
	.p2align 4,,10
	.p2align 3
.L2549:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2550
	.p2align 4,,10
	.p2align 3
.L3079:
	movq	8(%rbx), %rdx
	jmp	.L2548
	.p2align 4,,10
	.p2align 3
.L2753:
	movq	-2400(%rbp), %rbx
	movq	-2408(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2755
	.p2align 4,,10
	.p2align 3
.L2759:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2756
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2759
.L2757:
	movq	-2408(%rbp), %r12
.L2755:
	testq	%r12, %r12
	je	.L2760
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L2760:
	movq	-2440(%rbp), %rdi
	cmpq	-3096(%rbp), %rdi
	je	.L2761
	call	_ZdlPv@PLT
.L2761:
	movq	-2472(%rbp), %rdi
	cmpq	-3088(%rbp), %rdi
	je	.L2762
	call	_ZdlPv@PLT
.L2762:
	movq	-2504(%rbp), %rdi
	cmpq	-3080(%rbp), %rdi
	je	.L2763
	call	_ZdlPv@PLT
.L2763:
	movq	-2536(%rbp), %rdi
	cmpq	-3072(%rbp), %rdi
	je	.L2764
	call	_ZdlPv@PLT
.L2764:
	movq	-2568(%rbp), %rdi
	cmpq	-3064(%rbp), %rdi
	je	.L2765
	call	_ZdlPv@PLT
.L2765:
	movq	-2600(%rbp), %rdi
	cmpq	-3056(%rbp), %rdi
	je	.L2766
	call	_ZdlPv@PLT
.L2766:
	movq	-2624(%rbp), %rbx
	movq	-2632(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L2795
.L2771:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L2768
.L3087:
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %rbx
	je	.L2797
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L3087
.L2768:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2771
	jmp	.L2797
	.p2align 4,,10
	.p2align 3
.L2756:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L2759
	jmp	.L2757
	.p2align 4,,10
	.p2align 3
.L3084:
	movq	%rax, -2384(%rbp)
	leaq	-2360(%rbp), %rax
	movq	%r15, %rdi
	movq	%rax, -2960(%rbp)
	movq	%rax, -2376(%rbp)
	leaq	-2328(%rbp), %rax
	movq	%rax, -2968(%rbp)
	movq	%rax, -2344(%rbp)
	leaq	-2296(%rbp), %rax
	movq	%rax, -2976(%rbp)
	movq	%rax, -2312(%rbp)
	leaq	-2264(%rbp), %rax
	movq	%rax, -2984(%rbp)
	movq	%rax, -2280(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, -2992(%rbp)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movb	$0, -2360(%rbp)
	movb	$0, -2328(%rbp)
	movb	$0, -2296(%rbp)
	movb	$0, -2264(%rbp)
	movb	$0, -2232(%rbp)
	movb	$0, -2200(%rbp)
	movq	3280(%r12), %rsi
	movq	%rax, -3000(%rbp)
	movq	%rax, -2216(%rbp)
	movq	360(%r12), %rax
	movq	$0, -2368(%rbp)
	movq	768(%rax), %rdx
	movq	$0, -2336(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2272(%rbp)
	movq	$0, -2240(%rbp)
	movq	$0, -2208(%rbp)
	movq	$0, -2168(%rbp)
	movups	%xmm0, -2184(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3088
.L2563:
	movq	%rbx, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L3089
.L2564:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	movq	1536(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3090
.L2565:
	movq	(%rbx), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2566
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3091
.L2566:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r15, %rdi
	movq	1384(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3092
.L2568:
	movq	%rbx, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L3093
.L2569:
	movl	-2384(%rbp), %edx
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	testb	$32, %dl
	jne	.L3094
.L2570:
	andl	$64, %edx
	je	.L2575
	movq	1312(%rax), %rdx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3095
.L2576:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L3096
.L2577:
	leaq	_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3085:
	movb	$0, -2360(%rbp)
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movabsq	$-4294967296, %rax
	movq	%rax, -2384(%rbp)
	leaq	-2360(%rbp), %rax
	movq	%rax, -2960(%rbp)
	movq	%rax, -2376(%rbp)
	leaq	-2328(%rbp), %rax
	movq	%rax, -2968(%rbp)
	movq	%rax, -2344(%rbp)
	leaq	-2296(%rbp), %rax
	movq	%rax, -2976(%rbp)
	movq	%rax, -2312(%rbp)
	leaq	-2264(%rbp), %rax
	movq	%rax, -2984(%rbp)
	movq	%rax, -2280(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, -2992(%rbp)
	movq	%rax, -2248(%rbp)
	leaq	-2200(%rbp), %rax
	movb	$0, -2328(%rbp)
	movb	$0, -2296(%rbp)
	movb	$0, -2264(%rbp)
	movb	$0, -2232(%rbp)
	movb	$0, -2200(%rbp)
	movq	3280(%r12), %r15
	movq	%rax, -3000(%rbp)
	movq	%rax, -2216(%rbp)
	movq	%r15, %rsi
	movq	360(%r12), %rax
	movq	$0, -2368(%rbp)
	movq	768(%rax), %rdx
	movq	$0, -2336(%rbp)
	movq	$0, -2304(%rbp)
	movq	$0, -2272(%rbp)
	movq	$0, -2240(%rbp)
	movq	$0, -2208(%rbp)
	movq	$0, -2168(%rbp)
	movups	%xmm0, -2184(%rbp)
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3097
.L2638:
	movq	%rbx, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L3098
.L2639:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	1384(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L3099
.L2641:
	movq	%rbx, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L3100
.L2642:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	1536(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3101
.L2644:
	movq	352(%r12), %rsi
	leaq	-1104(%rbp), %rbx
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	-2368(%rbp), %rdx
	leaq	-2376(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rax, %r8
	movq	%r15, %rcx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	movq	%rax, -3160(%rbp)
	cmpq	%rax, %rdi
	je	.L2645
	testq	%rdi, %rdi
	je	.L2645
	call	free@PLT
.L2645:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	1856(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3102
.L2646:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2647
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3103
.L2647:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	1312(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3104
.L2650:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2651
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3105
.L2651:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	848(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3106
.L2654:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2655
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3107
.L2655:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	1448(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3108
.L2657:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2658
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3109
.L2658:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	776(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3110
.L2660:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2661
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3111
.L2661:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	movq	%r13, %rdi
	movq	1320(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3112
.L2663:
	movq	%r15, %rdi
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	jne	.L3113
.L2664:
	movq	-2384(%rbp), %rax
	movq	-2824(%rbp), %rdi
	movq	-2368(%rbp), %rdx
	movq	%rax, -2832(%rbp)
	movq	-2376(%rbp), %rax
	cmpq	-2960(%rbp), %rax
	je	.L3114
	movq	-2360(%rbp), %rcx
	cmpq	-3008(%rbp), %rdi
	je	.L3115
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	-2808(%rbp), %rsi
	movq	%rax, -2824(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -2816(%rbp)
	testq	%rdi, %rdi
	je	.L2704
	movq	%rdi, -2376(%rbp)
	movq	%rsi, -2360(%rbp)
.L2702:
	movq	$0, -2368(%rbp)
	movb	$0, (%rdi)
	movq	-2792(%rbp), %rdi
	movq	-2344(%rbp), %rdx
	cmpq	-2968(%rbp), %rdx
	je	.L3116
	movq	-2336(%rbp), %rax
	movq	-2328(%rbp), %rcx
	cmpq	-3016(%rbp), %rdi
	je	.L3117
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	-2776(%rbp), %rsi
	movq	%rdx, -2792(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -2784(%rbp)
	testq	%rdi, %rdi
	je	.L2710
	movq	%rdi, -2344(%rbp)
	movq	%rsi, -2328(%rbp)
.L2708:
	movq	$0, -2336(%rbp)
	movb	$0, (%rdi)
	movq	-2760(%rbp), %rdi
	movq	-2312(%rbp), %rdx
	cmpq	-2976(%rbp), %rdx
	je	.L3118
	movq	-2304(%rbp), %rax
	movq	-2296(%rbp), %rcx
	cmpq	-3024(%rbp), %rdi
	je	.L3119
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	-2744(%rbp), %rsi
	movq	%rdx, -2760(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -2752(%rbp)
	testq	%rdi, %rdi
	je	.L2716
	movq	%rdi, -2312(%rbp)
	movq	%rsi, -2296(%rbp)
.L2714:
	movq	$0, -2304(%rbp)
	movb	$0, (%rdi)
	movq	-2728(%rbp), %rdi
	movq	-2280(%rbp), %rdx
	cmpq	-2984(%rbp), %rdx
	je	.L3120
	movq	-2272(%rbp), %rax
	movq	-2264(%rbp), %rcx
	cmpq	-3032(%rbp), %rdi
	je	.L3121
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	-2712(%rbp), %rsi
	movq	%rdx, -2728(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -2720(%rbp)
	testq	%rdi, %rdi
	je	.L2722
	movq	%rdi, -2280(%rbp)
	movq	%rsi, -2264(%rbp)
.L2720:
	movq	$0, -2272(%rbp)
	movb	$0, (%rdi)
	movq	-2696(%rbp), %rdi
	movq	-2248(%rbp), %rdx
	cmpq	-2992(%rbp), %rdx
	je	.L3122
	movq	-2232(%rbp), %rcx
	movq	-2240(%rbp), %rax
	cmpq	-3040(%rbp), %rdi
	je	.L3123
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	-2680(%rbp), %rsi
	movq	%rdx, -2696(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2688(%rbp)
	testq	%rdi, %rdi
	je	.L2728
	movq	%rdi, -2248(%rbp)
	movq	%rsi, -2232(%rbp)
.L2726:
	movq	$0, -2240(%rbp)
	movb	$0, (%rdi)
	movq	-2664(%rbp), %rdi
	movq	-2216(%rbp), %rdx
	cmpq	-3000(%rbp), %rdx
	je	.L3124
	movq	-2200(%rbp), %rcx
	movq	-2208(%rbp), %rax
	cmpq	-3048(%rbp), %rdi
	je	.L3125
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	-2648(%rbp), %rsi
	movq	%rdx, -2664(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -2656(%rbp)
	testq	%rdi, %rdi
	je	.L2734
	movq	%rdi, -2216(%rbp)
	movq	%rsi, -2200(%rbp)
.L2732:
	movq	$0, -2208(%rbp)
	pxor	%xmm0, %xmm0
	movb	$0, (%rdi)
	movq	-2632(%rbp), %r13
	movdqu	-2184(%rbp), %xmm1
	movq	-2168(%rbp), %rax
	movups	%xmm0, -2184(%rbp)
	movq	-2624(%rbp), %rbx
	movq	%r13, %r15
	movq	$0, -2168(%rbp)
	movq	%rax, -2616(%rbp)
	movups	%xmm1, -2632(%rbp)
	cmpq	%rbx, %r13
	je	.L2739
	.p2align 4,,10
	.p2align 3
.L2735:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2738
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%rbx, %r15
	jne	.L2735
.L2739:
	testq	%r13, %r13
	je	.L2737
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2737:
	movq	-2176(%rbp), %rbx
	movq	-2184(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2741
	.p2align 4,,10
	.p2align 3
.L2745:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2742
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L2745
.L2743:
	movq	-2184(%rbp), %r13
.L2741:
	testq	%r13, %r13
	je	.L2746
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2746:
	movq	-2216(%rbp), %rdi
	cmpq	-3000(%rbp), %rdi
	je	.L2747
	call	_ZdlPv@PLT
.L2747:
	movq	-2248(%rbp), %rdi
	cmpq	-2992(%rbp), %rdi
	je	.L2748
	call	_ZdlPv@PLT
.L2748:
	movq	-2280(%rbp), %rdi
	cmpq	-2984(%rbp), %rdi
	je	.L2749
	call	_ZdlPv@PLT
.L2749:
	movq	-2312(%rbp), %rdi
	cmpq	-2976(%rbp), %rdi
	je	.L2750
	call	_ZdlPv@PLT
.L2750:
	movq	-2344(%rbp), %rdi
	cmpq	-2968(%rbp), %rdi
	je	.L2751
	call	_ZdlPv@PLT
.L2751:
	movq	-2376(%rbp), %rdi
	cmpq	-2960(%rbp), %rdi
	je	.L2637
	call	_ZdlPv@PLT
	jmp	.L2637
	.p2align 4,,10
	.p2align 3
.L2742:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L2745
	jmp	.L2743
	.p2align 4,,10
	.p2align 3
.L2738:
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L2735
	jmp	.L2739
	.p2align 4,,10
	.p2align 3
.L3096:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2577
	movq	352(%r12), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2304(%rbp), %rdx
	leaq	-2312(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2579
	testq	%rdi, %rdi
	je	.L2579
	call	free@PLT
.L2579:
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
.L2575:
	movq	848(%rax), %rdx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L3126
.L2580:
	movq	(%r15), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L2581
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	jbe	.L3127
.L2581:
	movq	-2384(%rbp), %rax
	movq	-2600(%rbp), %rdi
	movq	-2368(%rbp), %rdx
	movq	%rax, -2608(%rbp)
	movq	-2376(%rbp), %rax
	cmpq	-2960(%rbp), %rax
	je	.L3128
	movq	-2360(%rbp), %rcx
	cmpq	-3056(%rbp), %rdi
	je	.L3129
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	-2584(%rbp), %rsi
	movq	%rax, -2600(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -2592(%rbp)
	testq	%rdi, %rdi
	je	.L2588
	movq	%rdi, -2376(%rbp)
	movq	%rsi, -2360(%rbp)
.L2586:
	movq	$0, -2368(%rbp)
	movb	$0, (%rdi)
	movq	-2568(%rbp), %rdi
	movq	-2344(%rbp), %rdx
	cmpq	-2968(%rbp), %rdx
	je	.L3130
	movq	-2328(%rbp), %rcx
	movq	-2336(%rbp), %rax
	cmpq	-3064(%rbp), %rdi
	je	.L3131
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	-2552(%rbp), %rsi
	movq	%rdx, -2568(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -2560(%rbp)
	testq	%rdi, %rdi
	je	.L2594
	movq	%rdi, -2344(%rbp)
	movq	%rsi, -2328(%rbp)
.L2592:
	movq	$0, -2336(%rbp)
	movb	$0, (%rdi)
	movq	-2536(%rbp), %rdi
	movq	-2312(%rbp), %rdx
	cmpq	-2976(%rbp), %rdx
	je	.L3132
	movq	-2304(%rbp), %rax
	movq	-2296(%rbp), %rcx
	cmpq	-3072(%rbp), %rdi
	je	.L3133
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	-2520(%rbp), %rsi
	movq	%rdx, -2536(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -2528(%rbp)
	testq	%rdi, %rdi
	je	.L2600
	movq	%rdi, -2312(%rbp)
	movq	%rsi, -2296(%rbp)
.L2598:
	movq	$0, -2304(%rbp)
	movb	$0, (%rdi)
	movq	-2504(%rbp), %rdi
	movq	-2280(%rbp), %rdx
	cmpq	-2984(%rbp), %rdx
	je	.L3134
	movq	-2272(%rbp), %rax
	movq	-2264(%rbp), %rcx
	cmpq	-3080(%rbp), %rdi
	je	.L3135
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	-2488(%rbp), %rsi
	movq	%rdx, -2504(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2496(%rbp)
	testq	%rdi, %rdi
	je	.L2606
	movq	%rdi, -2280(%rbp)
	movq	%rsi, -2264(%rbp)
.L2604:
	movq	$0, -2272(%rbp)
	movb	$0, (%rdi)
	movq	-2472(%rbp), %rdi
	movq	-2248(%rbp), %rdx
	cmpq	-2992(%rbp), %rdx
	je	.L3136
	movq	-2240(%rbp), %rax
	movq	-2232(%rbp), %rcx
	cmpq	-3088(%rbp), %rdi
	je	.L3137
	movq	%rax, %xmm0
	movq	%rcx, %xmm2
	movq	-2456(%rbp), %rsi
	movq	%rdx, -2472(%rbp)
	punpcklqdq	%xmm2, %xmm0
	movaps	%xmm0, -2464(%rbp)
	testq	%rdi, %rdi
	je	.L2612
	movq	%rdi, -2248(%rbp)
	movq	%rsi, -2232(%rbp)
.L2610:
	movq	$0, -2240(%rbp)
	movb	$0, (%rdi)
	movq	-2440(%rbp), %rdi
	movq	-2216(%rbp), %rdx
	cmpq	-3000(%rbp), %rdx
	je	.L3138
	movq	-2200(%rbp), %rcx
	movq	-2208(%rbp), %rax
	cmpq	-3096(%rbp), %rdi
	je	.L3139
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	-2424(%rbp), %rsi
	movq	%rdx, -2440(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -2432(%rbp)
	testq	%rdi, %rdi
	je	.L2618
	movq	%rdi, -2216(%rbp)
	movq	%rsi, -2200(%rbp)
.L2616:
	movq	$0, -2208(%rbp)
	pxor	%xmm0, %xmm0
	movb	$0, (%rdi)
	movq	-2408(%rbp), %rcx
	movdqu	-2184(%rbp), %xmm2
	movq	-2168(%rbp), %rax
	movups	%xmm0, -2184(%rbp)
	movq	-2400(%rbp), %rbx
	movq	%rcx, -3160(%rbp)
	movq	%rcx, %r15
	movq	%rax, -2392(%rbp)
	movq	$0, -2168(%rbp)
	movups	%xmm2, -2408(%rbp)
	cmpq	%rbx, %rcx
	je	.L2623
	.p2align 4,,10
	.p2align 3
.L2619:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2622
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L2619
.L2623:
	cmpq	$0, -3160(%rbp)
	je	.L2621
	movq	-3160(%rbp), %rdi
	call	_ZdlPv@PLT
.L2621:
	movq	-2176(%rbp), %rbx
	movq	-2184(%rbp), %r15
	cmpq	%r15, %rbx
	je	.L2625
	.p2align 4,,10
	.p2align 3
.L2629:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2626
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L2629
.L2627:
	movq	-2184(%rbp), %r15
.L2625:
	testq	%r15, %r15
	je	.L2630
	movq	%r15, %rdi
	call	_ZdlPv@PLT
.L2630:
	movq	-2216(%rbp), %rdi
	cmpq	-3000(%rbp), %rdi
	je	.L2631
	call	_ZdlPv@PLT
.L2631:
	movq	-2248(%rbp), %rdi
	cmpq	-2992(%rbp), %rdi
	je	.L2632
	call	_ZdlPv@PLT
.L2632:
	movq	-2280(%rbp), %rdi
	cmpq	-2984(%rbp), %rdi
	je	.L2633
	call	_ZdlPv@PLT
.L2633:
	movq	-2312(%rbp), %rdi
	cmpq	-2976(%rbp), %rdi
	je	.L2634
	call	_ZdlPv@PLT
.L2634:
	movq	-2344(%rbp), %rdi
	cmpq	-2968(%rbp), %rdi
	je	.L2635
	call	_ZdlPv@PLT
.L2635:
	movq	-2376(%rbp), %rdi
	cmpq	-2960(%rbp), %rdi
	je	.L2562
	call	_ZdlPv@PLT
	jmp	.L2562
	.p2align 4,,10
	.p2align 3
.L2626:
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L2629
	jmp	.L2627
	.p2align 4,,10
	.p2align 3
.L2622:
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L2619
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2780:
	movq	-3136(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L2781
	movl	-2608(%rbp), %esi
	movq	-3128(%rbp), %rdi
	movq	%rbx, %xmm0
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -1104(%rbp)
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%r12), %rdi
	xorl	%edx, %edx
	movq	-3104(%rbp), %rsi
	movl	$-1, %ecx
	movq	%rax, -1104(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L3140
.L2782:
	movq	-3144(%rbp), %rdx
	movq	-3136(%rbp), %rdi
	movl	$2, %ecx
	movq	%r14, %rsi
	leaq	-1104(%rbp), %r8
	movq	%rax, -1096(%rbp)
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	jmp	.L2781
	.p2align 4,,10
	.p2align 3
.L3073:
	movq	8(%rbx), %rdi
	jmp	.L2531
	.p2align 4,,10
	.p2align 3
.L2552:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L2553
	.p2align 4,,10
	.p2align 3
.L3077:
	leaq	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3094:
	movq	1856(%rax), %rdx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3141
.L2571:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L2572
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2572
	movq	352(%r12), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2336(%rbp), %rdx
	leaq	-2344(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2574
	testq	%rdi, %rdi
	je	.L2574
	call	free@PLT
.L2574:
	movl	-2384(%rbp), %edx
	movq	360(%r12), %rax
	movq	3280(%r12), %rsi
	jmp	.L2570
	.p2align 4,,10
	.p2align 3
.L3093:
	movq	%rbx, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, -2380(%rbp)
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L3089:
	movq	%rbx, %rdi
	call	_ZNK2v85Int325ValueEv@PLT
	andl	$242, %eax
	orl	%eax, -2384(%rbp)
	jmp	.L2564
	.p2align 4,,10
	.p2align 3
.L3098:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L3142
.L2640:
	sarq	$32, %rbx
	movl	%ebx, -2384(%rbp)
	jmp	.L2639
	.p2align 4,,10
	.p2align 3
.L3100:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L3143
.L2643:
	sarq	$32, %rbx
	movl	%ebx, -2380(%rbp)
	jmp	.L2642
	.p2align 4,,10
	.p2align 3
.L3113:
	pxor	%xmm0, %xmm0
	movq	%r15, %rdi
	orl	$256, -2384(%rbp)
	movq	$0, -2848(%rbp)
	movaps	%xmm0, -2864(%rbp)
	call	_ZNK2v85Array6LengthEv@PLT
	testl	%eax, %eax
	jne	.L3144
.L2666:
	leaq	-2912(%rbp), %rax
	xorl	%r13d, %r13d
	movq	%rax, -3176(%rbp)
	jmp	.L2686
	.p2align 4,,10
	.p2align 3
.L2677:
	addq	$1, %r13
.L2686:
	movq	%r15, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movl	%eax, %eax
	cmpq	%r13, %rax
	jbe	.L2675
	movq	3280(%r12), %rsi
	movl	%r13d, %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L3145
.L2676:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L2677
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L2677
	movq	352(%r12), %rsi
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	movq	-1088(%rbp), %r10
	movq	-2856(%rbp), %r9
	movq	%r8, -2912(%rbp)
	movq	%r10, -2920(%rbp)
	cmpq	-2848(%rbp), %r9
	je	.L2678
	movq	%r10, %rax
	leaq	16(%r9), %rdi
	addq	%r8, %rax
	movq	%rdi, (%r9)
	je	.L2679
	testq	%r10, %r10
	jne	.L2679
	leaq	.LC84(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L2679:
	movq	%r8, -2904(%rbp)
	cmpq	$15, %r8
	ja	.L3146
	cmpq	$1, %r8
	jne	.L2682
	movzbl	(%r10), %eax
	movb	%al, 16(%r9)
.L2683:
	movq	%r8, 8(%r9)
	movb	$0, (%rdi,%r8)
	addq	$32, -2856(%rbp)
.L2684:
	movq	-1088(%rbp), %rdi
	cmpq	-3160(%rbp), %rdi
	je	.L2677
	testq	%rdi, %rdi
	je	.L2677
	call	free@PLT
	jmp	.L2677
	.p2align 4,,10
	.p2align 3
.L2675:
	movq	-2184(%rbp), %r13
	movdqa	-2864(%rbp), %xmm5
	pxor	%xmm0, %xmm0
	movq	-2848(%rbp), %rax
	movq	-2176(%rbp), %rbx
	movq	$0, -2848(%rbp)
	movups	%xmm5, -2184(%rbp)
	movq	%r13, %r15
	movq	%rax, -2168(%rbp)
	movaps	%xmm0, -2864(%rbp)
	cmpq	%rbx, %r13
	je	.L2691
	.p2align 4,,10
	.p2align 3
.L2687:
	movq	(%r15), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L2690
	call	_ZdlPv@PLT
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L2687
.L2691:
	testq	%r13, %r13
	je	.L2689
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L2689:
	movq	-2856(%rbp), %rbx
	movq	-2864(%rbp), %r13
	cmpq	%r13, %rbx
	je	.L2693
	.p2align 4,,10
	.p2align 3
.L2697:
	movq	0(%r13), %rdi
	leaq	16(%r13), %rax
	cmpq	%rax, %rdi
	je	.L2694
	call	_ZdlPv@PLT
	addq	$32, %r13
	cmpq	%rbx, %r13
	jne	.L2697
.L2695:
	movq	-2864(%rbp), %r13
.L2693:
	testq	%r13, %r13
	je	.L2664
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L2664
	.p2align 4,,10
	.p2align 3
.L2694:
	addq	$32, %r13
	cmpq	%r13, %rbx
	jne	.L2697
	jmp	.L2695
	.p2align 4,,10
	.p2align 3
.L2690:
	addq	$32, %r15
	cmpq	%r15, %rbx
	jne	.L2687
	jmp	.L2691
	.p2align 4,,10
	.p2align 3
.L3114:
	testq	%rdx, %rdx
	je	.L2700
	cmpq	$1, %rdx
	je	.L3147
	movq	-2960(%rbp), %rsi
	call	memcpy@PLT
	movq	-2368(%rbp), %rdx
	movq	-2824(%rbp), %rdi
.L2700:
	movq	%rdx, -2816(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2376(%rbp), %rdi
	jmp	.L2702
	.p2align 4,,10
	.p2align 3
.L3124:
	movq	-2208(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2730
	cmpq	$1, %rdx
	je	.L3148
	movq	-3000(%rbp), %rsi
	call	memcpy@PLT
	movq	-2208(%rbp), %rdx
	movq	-2664(%rbp), %rdi
.L2730:
	movq	%rdx, -2656(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2216(%rbp), %rdi
	jmp	.L2732
	.p2align 4,,10
	.p2align 3
.L3118:
	movq	-2304(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2712
	cmpq	$1, %rdx
	je	.L3149
	movq	-2976(%rbp), %rsi
	call	memcpy@PLT
	movq	-2304(%rbp), %rdx
	movq	-2760(%rbp), %rdi
.L2712:
	movq	%rdx, -2752(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2312(%rbp), %rdi
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L3116:
	movq	-2336(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2706
	cmpq	$1, %rdx
	je	.L3150
	movq	-2968(%rbp), %rsi
	call	memcpy@PLT
	movq	-2336(%rbp), %rdx
	movq	-2792(%rbp), %rdi
.L2706:
	movq	%rdx, -2784(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2344(%rbp), %rdi
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L3122:
	movq	-2240(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2724
	cmpq	$1, %rdx
	je	.L3151
	movq	-2992(%rbp), %rsi
	call	memcpy@PLT
	movq	-2240(%rbp), %rdx
	movq	-2696(%rbp), %rdi
.L2724:
	movq	%rdx, -2688(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2248(%rbp), %rdi
	jmp	.L2726
	.p2align 4,,10
	.p2align 3
.L3120:
	movq	-2272(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2718
	cmpq	$1, %rdx
	je	.L3152
	movq	-2984(%rbp), %rsi
	call	memcpy@PLT
	movq	-2272(%rbp), %rdx
	movq	-2728(%rbp), %rdi
.L2718:
	movq	%rdx, -2720(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2280(%rbp), %rdi
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L3130:
	movq	-2336(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2590
	cmpq	$1, %rdx
	je	.L3153
	movq	-2968(%rbp), %rsi
	call	memcpy@PLT
	movq	-2336(%rbp), %rdx
	movq	-2568(%rbp), %rdi
.L2590:
	movq	%rdx, -2560(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2344(%rbp), %rdi
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L3128:
	testq	%rdx, %rdx
	je	.L2584
	cmpq	$1, %rdx
	je	.L3154
	movq	-2960(%rbp), %rsi
	call	memcpy@PLT
	movq	-2368(%rbp), %rdx
	movq	-2600(%rbp), %rdi
.L2584:
	movq	%rdx, -2592(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2376(%rbp), %rdi
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L3138:
	movq	-2208(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2614
	cmpq	$1, %rdx
	je	.L3155
	movq	-3000(%rbp), %rsi
	call	memcpy@PLT
	movq	-2208(%rbp), %rdx
	movq	-2440(%rbp), %rdi
.L2614:
	movq	%rdx, -2432(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2216(%rbp), %rdi
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L3136:
	movq	-2240(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2608
	cmpq	$1, %rdx
	je	.L3156
	movq	-2992(%rbp), %rsi
	call	memcpy@PLT
	movq	-2240(%rbp), %rdx
	movq	-2472(%rbp), %rdi
.L2608:
	movq	%rdx, -2464(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2248(%rbp), %rdi
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L3134:
	movq	-2272(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2602
	cmpq	$1, %rdx
	je	.L3157
	movq	-2984(%rbp), %rsi
	call	memcpy@PLT
	movq	-2272(%rbp), %rdx
	movq	-2504(%rbp), %rdi
.L2602:
	movq	%rdx, -2496(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2280(%rbp), %rdi
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L3132:
	movq	-2304(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L2596
	cmpq	$1, %rdx
	je	.L3158
	movq	-2976(%rbp), %rsi
	call	memcpy@PLT
	movq	-2304(%rbp), %rdx
	movq	-2536(%rbp), %rdi
.L2596:
	movq	%rdx, -2528(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-2312(%rbp), %rdi
	jmp	.L2598
	.p2align 4,,10
	.p2align 3
.L3145:
	movq	%rax, -3168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3168(%rbp), %rdx
	jmp	.L2676
	.p2align 4,,10
	.p2align 3
.L2572:
	leaq	_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3146:
	movq	%r9, %rdi
	leaq	-2904(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r10, -3192(%rbp)
	movq	%r8, -3184(%rbp)
	movq	%r9, -3168(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-3168(%rbp), %r9
	movq	-3184(%rbp), %r8
	movq	%rax, %rdi
	movq	-3192(%rbp), %r10
	movq	%rax, (%r9)
	movq	-2904(%rbp), %rax
	movq	%rax, 16(%r9)
.L2681:
	movq	%r8, %rdx
	movq	%r10, %rsi
	movq	%r9, -3168(%rbp)
	call	memcpy@PLT
	movq	-3168(%rbp), %r9
	movq	-2904(%rbp), %r8
	movq	(%r9), %rdi
	jmp	.L2683
.L2545:
	leaq	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L3075:
	leaq	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2532:
	leaq	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L3125:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdx, -2664(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2656(%rbp)
.L2734:
	movq	-3000(%rbp), %rax
	movq	%rax, -2216(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, -3000(%rbp)
	movq	%rax, %rdi
	jmp	.L2732
	.p2align 4,,10
	.p2align 3
.L3123:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, -2696(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -2688(%rbp)
.L2728:
	movq	-2992(%rbp), %rax
	movq	%rax, -2248(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, -2992(%rbp)
	movq	%rax, %rdi
	jmp	.L2726
	.p2align 4,,10
	.p2align 3
.L3121:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%rdx, -2728(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -2720(%rbp)
.L2722:
	movq	-2984(%rbp), %rax
	movq	%rax, -2280(%rbp)
	leaq	-2264(%rbp), %rax
	movq	%rax, -2984(%rbp)
	movq	%rax, %rdi
	jmp	.L2720
	.p2align 4,,10
	.p2align 3
.L3119:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, -2760(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -2752(%rbp)
.L2716:
	movq	-2976(%rbp), %rax
	movq	%rax, -2312(%rbp)
	leaq	-2296(%rbp), %rax
	movq	%rax, -2976(%rbp)
	movq	%rax, %rdi
	jmp	.L2714
	.p2align 4,,10
	.p2align 3
.L3117:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdx, -2792(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2784(%rbp)
.L2710:
	movq	-2968(%rbp), %rax
	movq	%rax, -2344(%rbp)
	leaq	-2328(%rbp), %rax
	movq	%rax, -2968(%rbp)
	movq	%rax, %rdi
	jmp	.L2708
	.p2align 4,,10
	.p2align 3
.L3115:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm6
	movq	%rax, -2824(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -2816(%rbp)
.L2704:
	movq	-2960(%rbp), %rax
	movq	%rax, -2376(%rbp)
	leaq	-2360(%rbp), %rax
	movq	%rax, -2960(%rbp)
	movq	%rax, %rdi
	jmp	.L2702
	.p2align 4,,10
	.p2align 3
.L3129:
	movq	%rdx, %xmm0
	movq	%rcx, %xmm3
	movq	%rax, -2600(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -2592(%rbp)
.L2588:
	movq	-2960(%rbp), %rax
	movq	%rax, -2376(%rbp)
	leaq	-2360(%rbp), %rax
	movq	%rax, -2960(%rbp)
	movq	%rax, %rdi
	jmp	.L2586
	.p2align 4,,10
	.p2align 3
.L3137:
	movq	%rax, %xmm0
	movq	%rcx, %xmm7
	movq	%rdx, -2472(%rbp)
	punpcklqdq	%xmm7, %xmm0
	movaps	%xmm0, -2464(%rbp)
.L2612:
	movq	-2992(%rbp), %rax
	movq	%rax, -2248(%rbp)
	leaq	-2232(%rbp), %rax
	movq	%rax, -2992(%rbp)
	movq	%rax, %rdi
	jmp	.L2610
	.p2align 4,,10
	.p2align 3
.L3135:
	movq	%rax, %xmm0
	movq	%rcx, %xmm6
	movq	%rdx, -2504(%rbp)
	punpcklqdq	%xmm6, %xmm0
	movaps	%xmm0, -2496(%rbp)
.L2606:
	movq	-2984(%rbp), %rax
	movq	%rax, -2280(%rbp)
	leaq	-2264(%rbp), %rax
	movq	%rax, -2984(%rbp)
	movq	%rax, %rdi
	jmp	.L2604
	.p2align 4,,10
	.p2align 3
.L3133:
	movq	%rax, %xmm0
	movq	%rcx, %xmm5
	movq	%rdx, -2536(%rbp)
	punpcklqdq	%xmm5, %xmm0
	movaps	%xmm0, -2528(%rbp)
.L2600:
	movq	-2976(%rbp), %rax
	movq	%rax, -2312(%rbp)
	leaq	-2296(%rbp), %rax
	movq	%rax, -2976(%rbp)
	movq	%rax, %rdi
	jmp	.L2598
	.p2align 4,,10
	.p2align 3
.L3131:
	movq	%rax, %xmm0
	movq	%rcx, %xmm1
	movq	%rdx, -2568(%rbp)
	punpcklqdq	%xmm1, %xmm0
	movaps	%xmm0, -2560(%rbp)
.L2594:
	movq	-2968(%rbp), %rax
	movq	%rax, -2344(%rbp)
	leaq	-2328(%rbp), %rax
	movq	%rax, -2968(%rbp)
	movq	%rax, %rdi
	jmp	.L2592
	.p2align 4,,10
	.p2align 3
.L3139:
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	%rdx, -2440(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movaps	%xmm0, -2432(%rbp)
.L2618:
	movq	-3000(%rbp), %rax
	movq	%rax, -2216(%rbp)
	leaq	-2200(%rbp), %rax
	movq	%rax, -3000(%rbp)
	movq	%rax, %rdi
	jmp	.L2616
	.p2align 4,,10
	.p2align 3
.L3091:
	movq	352(%r12), %rsi
	leaq	-1104(%rbp), %rdi
	movq	%rbx, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2368(%rbp), %rdx
	leaq	-2376(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2566
	testq	%rdi, %rdi
	je	.L2566
	call	free@PLT
	jmp	.L2566
	.p2align 4,,10
	.p2align 3
.L3103:
	movq	352(%r12), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2336(%rbp), %rdx
	leaq	-2344(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r15, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	je	.L2648
	orl	$32, -2384(%rbp)
.L2648:
	movq	-1088(%rbp), %rdi
	cmpq	-3160(%rbp), %rdi
	je	.L2647
	testq	%rdi, %rdi
	je	.L2647
	call	free@PLT
	jmp	.L2647
	.p2align 4,,10
	.p2align 3
.L3111:
	movq	352(%r12), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2208(%rbp), %rdx
	leaq	-2216(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	orl	$1024, -2384(%rbp)
	cmpq	-3160(%rbp), %rdi
	je	.L2661
	testq	%rdi, %rdi
	je	.L2661
	call	free@PLT
	jmp	.L2661
	.p2align 4,,10
	.p2align 3
.L3109:
	movq	352(%r12), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2240(%rbp), %rdx
	leaq	-2248(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	orl	$512, -2384(%rbp)
	cmpq	-3160(%rbp), %rdi
	je	.L2658
	testq	%rdi, %rdi
	je	.L2658
	call	free@PLT
	jmp	.L2658
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	352(%r12), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2272(%rbp), %rdx
	leaq	-2280(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	orl	$128, -2384(%rbp)
	cmpq	-3160(%rbp), %rdi
	je	.L2655
	testq	%rdi, %rdi
	je	.L2655
	call	free@PLT
	jmp	.L2655
	.p2align 4,,10
	.p2align 3
.L3105:
	movq	352(%r12), %rsi
	movq	%r15, %rdx
	movq	%rbx, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2304(%rbp), %rdx
	leaq	-2312(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	%r15, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	testl	%eax, %eax
	je	.L2652
	orl	$64, -2384(%rbp)
.L2652:
	movq	-1088(%rbp), %rdi
	cmpq	-3160(%rbp), %rdi
	je	.L2651
	testq	%rdi, %rdi
	je	.L2651
	call	free@PLT
	jmp	.L2651
	.p2align 4,,10
	.p2align 3
.L3127:
	movq	352(%r12), %rsi
	leaq	-1104(%rbp), %rdi
	movq	%r15, %rdx
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1104(%rbp), %r8
	xorl	%esi, %esi
	movq	-1088(%rbp), %rcx
	movq	-2272(%rbp), %rdx
	leaq	-2280(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2581
	testq	%rdi, %rdi
	je	.L2581
	call	free@PLT
	jmp	.L2581
	.p2align 4,,10
	.p2align 3
.L3080:
	movq	%rax, -2936(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-2936(%rbp), %rax
	jmp	.L2554
	.p2align 4,,10
	.p2align 3
.L2678:
	movq	-3176(%rbp), %rcx
	leaq	-2920(%rbp), %rdx
	leaq	-2864(%rbp), %rdi
	movq	%r9, %rsi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJPcmEEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L2684
	.p2align 4,,10
	.p2align 3
.L3144:
	movq	%r15, %rdi
	call	_ZNK2v85Array6LengthEv@PLT
	movq	-2848(%rbp), %rdx
	movl	%eax, %ecx
	movq	-2864(%rbp), %rax
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %rcx
	jbe	.L2666
	movq	-2856(%rbp), %rdi
	movq	%rcx, %r8
	salq	$5, %r8
	movq	%rdi, %r13
	subq	%rax, %r13
	testq	%rcx, %rcx
	je	.L2814
	movq	%r8, %rdi
	movq	%r8, -3168(%rbp)
	call	_Znwm@PLT
	movq	-2864(%rbp), %r10
	movq	-2856(%rbp), %rdi
	movq	-3168(%rbp), %r8
	movq	%rax, %r9
	movq	%r10, %rax
.L2668:
	movq	%r9, %rdx
	cmpq	%rdi, %rax
	jne	.L2669
	jmp	.L2673
	.p2align 4,,10
	.p2align 3
.L2672:
	movq	%rcx, (%rdx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rdx)
.L3055:
	movq	8(%rax), %rcx
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rcx, -24(%rdx)
	cmpq	%rdi, %rax
	je	.L2673
.L2669:
	leaq	16(%rdx), %rcx
	leaq	16(%rax), %rsi
	movq	%rcx, (%rdx)
	movq	(%rax), %rcx
	cmpq	%rsi, %rcx
	jne	.L2672
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L3055
	.p2align 4,,10
	.p2align 3
.L2682:
	testq	%r8, %r8
	je	.L2683
	jmp	.L2681
	.p2align 4,,10
	.p2align 3
.L3092:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2568
	.p2align 4,,10
	.p2align 3
.L3090:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L3088:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2563
	.p2align 4,,10
	.p2align 3
.L3097:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2638
	.p2align 4,,10
	.p2align 3
.L3104:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2650
	.p2align 4,,10
	.p2align 3
.L3112:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2663
	.p2align 4,,10
	.p2align 3
.L3101:
	movq	%rax, -3160(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3160(%rbp), %rdx
	jmp	.L2644
	.p2align 4,,10
	.p2align 3
.L3099:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2641
	.p2align 4,,10
	.p2align 3
.L3102:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2646
	.p2align 4,,10
	.p2align 3
.L3110:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2660
	.p2align 4,,10
	.p2align 3
.L3108:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2657
	.p2align 4,,10
	.p2align 3
.L3106:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2654
	.p2align 4,,10
	.p2align 3
.L3126:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L2580
	.p2align 4,,10
	.p2align 3
.L3149:
	movzbl	-2296(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2304(%rbp), %rdx
	movq	-2760(%rbp), %rdi
	jmp	.L2712
	.p2align 4,,10
	.p2align 3
.L3148:
	movzbl	-2200(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2208(%rbp), %rdx
	movq	-2664(%rbp), %rdi
	jmp	.L2730
	.p2align 4,,10
	.p2align 3
.L3147:
	movzbl	-2360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2368(%rbp), %rdx
	movq	-2824(%rbp), %rdi
	jmp	.L2700
	.p2align 4,,10
	.p2align 3
.L3150:
	movzbl	-2328(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2336(%rbp), %rdx
	movq	-2792(%rbp), %rdi
	jmp	.L2706
	.p2align 4,,10
	.p2align 3
.L3152:
	movzbl	-2264(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2272(%rbp), %rdx
	movq	-2728(%rbp), %rdi
	jmp	.L2718
	.p2align 4,,10
	.p2align 3
.L3151:
	movzbl	-2232(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2240(%rbp), %rdx
	movq	-2696(%rbp), %rdi
	jmp	.L2724
	.p2align 4,,10
	.p2align 3
.L3095:
	movq	%rax, -3160(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3160(%rbp), %rdx
	jmp	.L2576
	.p2align 4,,10
	.p2align 3
.L3158:
	movzbl	-2296(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2304(%rbp), %rdx
	movq	-2536(%rbp), %rdi
	jmp	.L2596
	.p2align 4,,10
	.p2align 3
.L3154:
	movzbl	-2360(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2368(%rbp), %rdx
	movq	-2600(%rbp), %rdi
	jmp	.L2584
	.p2align 4,,10
	.p2align 3
.L3157:
	movzbl	-2264(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2272(%rbp), %rdx
	movq	-2504(%rbp), %rdi
	jmp	.L2602
	.p2align 4,,10
	.p2align 3
.L3155:
	movzbl	-2200(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2208(%rbp), %rdx
	movq	-2440(%rbp), %rdi
	jmp	.L2614
	.p2align 4,,10
	.p2align 3
.L3156:
	movzbl	-2232(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2240(%rbp), %rdx
	movq	-2472(%rbp), %rdi
	jmp	.L2608
	.p2align 4,,10
	.p2align 3
.L3153:
	movzbl	-2328(%rbp), %eax
	movb	%al, (%rdi)
	movq	-2336(%rbp), %rdx
	movq	-2568(%rbp), %rdi
	jmp	.L2590
.L2673:
	testq	%r10, %r10
	je	.L2671
	movq	%r10, %rdi
	movq	%r8, -3176(%rbp)
	movq	%r9, -3168(%rbp)
	call	_ZdlPv@PLT
	movq	-3176(%rbp), %r8
	movq	-3168(%rbp), %r9
.L2671:
	addq	%r9, %r13
	addq	%r9, %r8
	movq	%r9, -2864(%rbp)
	movq	%r13, -2856(%rbp)
	movq	%r8, -2848(%rbp)
	jmp	.L2666
.L3141:
	movq	%rax, -3160(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3160(%rbp), %rdx
	jmp	.L2571
.L3142:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2640
.L3143:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2643
.L3140:
	movq	%rax, -2936(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2936(%rbp), %rax
	jmp	.L2782
.L2814:
	xorl	%r9d, %r9d
	movq	%rax, %r10
	jmp	.L2668
.L3086:
	call	__stack_chk_fail@PLT
.L3081:
	movq	%r13, -3136(%rbp)
	jmp	.L2557
.L3083:
	movq	%r13, -3152(%rbp)
	movq	%r13, -3136(%rbp)
	jmp	.L2561
.L3082:
	movq	%r13, -3152(%rbp)
	movq	%r13, -3136(%rbp)
	jmp	.L2559
	.cfi_endproc
.LFE7490:
	.size	_ZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args
	.section	.rodata.str1.1
.LC93:
	.string	"../src/util.h:352"
.LC94:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EEixEmE4args:
	.quad	.LC93
	.quad	.LC94
	.quad	.LC95
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args
	.section	.rodata.str1.1
.LC96:
	.string	"../src/util.h:391"
.LC97:
	.string	"(length) <= (capacity())"
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"void node::MaybeStackBuffer<T, kStackStorageSize>::SetLength(size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 128; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm128EE9SetLengthEmE4args:
	.quad	.LC96
	.quad	.LC97
	.quad	.LC98
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC99:
	.string	"../src/util-inl.h:374"
.LC100:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC102:
	.string	"../src/util-inl.h:325"
.LC103:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.weak	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = short unsigned int; long unsigned int kStackStorageSize = 1024; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferItLm1024EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args, 24
_ZZN4node16MaybeStackBufferItLm1024EEixEmE4args:
	.quad	.LC93
	.quad	.LC94
	.quad	.LC105
	.section	.rodata.str1.1
.LC106:
	.string	"../src/node_url.cc"
.LC107:
	.string	"url"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC106
	.quad	0
	.quad	_ZN4node3url12_GLOBAL__N_110InitializeEN2v85LocalINS2_6ObjectEEENS3_INS2_5ValueEEENS3_INS2_7ContextEEEPv
	.quad	.LC107
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC108:
	.string	"../src/node_url.cc:2310"
.LC109:
	.string	"args[0]->IsFunction()"
	.section	.rodata.str1.8
	.align 8
.LC110:
	.string	"void node::url::{anonymous}::SetURLConstructor(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC108
	.quad	.LC109
	.quad	.LC110
	.section	.rodata.str1.1
.LC111:
	.string	"../src/node_url.cc:2309"
.LC112:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_117SetURLConstructorERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC111
	.quad	.LC112
	.quad	.LC110
	.section	.rodata.str1.1
.LC113:
	.string	"../src/node_url.cc:2290"
.LC114:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC115:
	.string	"void node::url::{anonymous}::DomainToUnicode(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC113
	.quad	.LC114
	.quad	.LC115
	.section	.rodata.str1.1
.LC116:
	.string	"../src/node_url.cc:2289"
.LC117:
	.string	"(args.Length()) >= (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_115DomainToUnicodeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC116
	.quad	.LC117
	.quad	.LC115
	.section	.rodata.str1.1
.LC118:
	.string	"../src/node_url.cc:2270"
	.section	.rodata.str1.8
	.align 8
.LC119:
	.string	"void node::url::{anonymous}::DomainToASCII(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC118
	.quad	.LC114
	.quad	.LC119
	.section	.rodata.str1.1
.LC120:
	.string	"../src/node_url.cc:2269"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_113DomainToASCIIERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC120
	.quad	.LC117
	.quad	.LC119
	.section	.rodata.str1.1
.LC121:
	.string	"../src/node_url.cc:2242"
.LC122:
	.string	"(start) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC123:
	.string	"void node::url::{anonymous}::ToUSVString(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.section	.rodata.str1.1
.LC124:
	.string	"../src/node_url.cc:2237"
.LC125:
	.string	"args[1]->IsNumber()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC124
	.quad	.LC125
	.quad	.LC123
	.section	.rodata.str1.1
.LC126:
	.string	"../src/node_url.cc:2236"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC126
	.quad	.LC114
	.quad	.LC123
	.section	.rodata.str1.1
.LC127:
	.string	"../src/node_url.cc:2235"
.LC128:
	.string	"(args.Length()) >= (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_111ToUSVStringERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC127
	.quad	.LC128
	.quad	.LC123
	.section	.rodata.str1.1
.LC129:
	.string	"../src/node_url.cc:2218"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"void node::url::{anonymous}::EncodeAuthSet(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC129
	.quad	.LC114
	.quad	.LC130
	.section	.rodata.str1.1
.LC131:
	.string	"../src/node_url.cc:2217"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_113EncodeAuthSetERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC131
	.quad	.LC117
	.quad	.LC130
	.section	.rodata.str1.1
.LC132:
	.string	"../src/node_url.cc:2197"
	.section	.rodata.str1.8
	.align 8
.LC133:
	.string	"args[5]->IsUndefined() || args[5]->IsFunction()"
	.align 8
.LC134:
	.string	"void node::url::{anonymous}::Parse(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, @object
	.size	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4, 24
_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_4:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.section	.rodata.str1.1
.LC135:
	.string	"../src/node_url.cc:2196"
.LC136:
	.string	"args[4]->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, @object
	.size	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3, 24
_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_3:
	.quad	.LC135
	.quad	.LC136
	.quad	.LC134
	.section	.rodata.str1.1
.LC137:
	.string	"../src/node_url.cc:2193"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"args[3]->IsUndefined() || args[3]->IsNull() || args[3]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC137
	.quad	.LC138
	.quad	.LC134
	.section	.rodata.str1.1
.LC139:
	.string	"../src/node_url.cc:2190"
	.section	.rodata.str1.8
	.align 8
.LC140:
	.string	"args[2]->IsUndefined() || args[2]->IsNull() || args[2]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC139
	.quad	.LC140
	.quad	.LC134
	.section	.rodata.str1.1
.LC141:
	.string	"../src/node_url.cc:2189"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC141
	.quad	.LC114
	.quad	.LC134
	.section	.rodata.str1.1
.LC142:
	.string	"../src/node_url.cc:2188"
.LC143:
	.string	"(args.Length()) >= (5)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_15ParseERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC142
	.quad	.LC143
	.quad	.LC134
	.section	.rodata.str1.1
.LC144:
	.string	"../src/node_url.cc:1289"
.LC145:
	.string	"password->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC146:
	.string	"node::url::url_data node::url::{anonymous}::HarvestContext(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args_0, 24
_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args_0:
	.quad	.LC144
	.quad	.LC145
	.quad	.LC146
	.section	.rodata.str1.1
.LC147:
	.string	"../src/node_url.cc:1281"
.LC148:
	.string	"username->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_114HarvestContextEPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEE4args:
	.quad	.LC147
	.quad	.LC148
	.quad	.LC146
	.section	.rodata.str1.1
.LC149:
	.string	"../src/node_url.cc:1006"
	.section	.rodata.str1.8
	.align 8
.LC150:
	.string	"(type_) == (HostType::H_FAILED)"
	.align 8
.LC151:
	.string	"void node::url::{anonymous}::URLHost::ParseOpaqueHost(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_17URLHost15ParseOpaqueHostEPKcmE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_17URLHost15ParseOpaqueHostEPKcmE4args, 24
_ZZN4node3url12_GLOBAL__N_17URLHost15ParseOpaqueHostEPKcmE4args:
	.quad	.LC149
	.quad	.LC150
	.quad	.LC151
	.section	.rodata.str1.1
.LC152:
	.string	"../src/node_url.cc:983"
.LC153:
	.string	"(parts) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC154:
	.string	"void node::url::{anonymous}::URLHost::ParseIPv4Host(const char*, size_t, bool*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args_0, @object
	.size	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args_0, 24
_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args_0:
	.quad	.LC152
	.quad	.LC153
	.quad	.LC154
	.section	.rodata.str1.1
.LC155:
	.string	"../src/node_url.cc:949"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args, 24
_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv4HostEPKcmPbE4args:
	.quad	.LC155
	.quad	.LC150
	.quad	.LC154
	.section	.rodata.str1.1
.LC156:
	.string	"../src/node_url.cc:798"
	.section	.rodata.str1.8
	.align 8
.LC157:
	.string	"void node::url::{anonymous}::URLHost::ParseIPv6Host(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv6HostEPKcmE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv6HostEPKcmE4args, 24
_ZZN4node3url12_GLOBAL__N_17URLHost13ParseIPv6HostEPKcmE4args:
	.quad	.LC156
	.quad	.LC150
	.quad	.LC157
	.section	.rodata.str1.1
.LC158:
	.string	"../src/node_url.cc:746"
.LC159:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC160:
	.string	"v8::Local<v8::String> node::url::{anonymous}::GetSpecial(node::Environment*, const string&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node3url12_GLOBAL__N_110GetSpecialEPNS_11EnvironmentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZN4node3url12_GLOBAL__N_110GetSpecialEPNS_11EnvironmentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZN4node3url12_GLOBAL__N_110GetSpecialEPNS_11EnvironmentERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.section	.rodata
	.align 32
	.type	_ZN4node3url12_GLOBAL__N_1L24QUERY_ENCODE_SET_SPECIALE, @object
	.size	_ZN4node3url12_GLOBAL__N_1L24QUERY_ENCODE_SET_SPECIALE, 32
_ZN4node3url12_GLOBAL__N_1L24QUERY_ENCODE_SET_SPECIALE:
	.string	"\377\377\377\377\215"
	.string	""
	.string	"P"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\200\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377"
	.align 32
	.type	_ZN4node3url12_GLOBAL__N_1L27QUERY_ENCODE_SET_NONSPECIALE, @object
	.size	_ZN4node3url12_GLOBAL__N_1L27QUERY_ENCODE_SET_NONSPECIALE, 32
_ZN4node3url12_GLOBAL__N_1L27QUERY_ENCODE_SET_NONSPECIALE:
	.string	"\377\377\377\377\r"
	.string	""
	.string	"P"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\200\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377"
	.align 32
	.type	_ZN4node3url12_GLOBAL__N_1L19USERINFO_ENCODE_SETE, @object
	.size	_ZN4node3url12_GLOBAL__N_1L19USERINFO_ENCODE_SETE, 32
_ZN4node3url12_GLOBAL__N_1L19USERINFO_ENCODE_SETE:
	.string	"\377\377\377\377\r\200"
	.string	"\374\001"
	.string	""
	.string	"x\001"
	.string	""
	.ascii	"\270\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377"
	.align 32
	.type	_ZN4node3url12_GLOBAL__N_1L15PATH_ENCODE_SETE, @object
	.size	_ZN4node3url12_GLOBAL__N_1L15PATH_ENCODE_SETE, 32
_ZN4node3url12_GLOBAL__N_1L15PATH_ENCODE_SETE:
	.string	"\377\377\377\377\r"
	.string	""
	.string	"\320"
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.ascii	"\250\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377"
	.align 32
	.type	_ZN4node3url12_GLOBAL__N_1L19FRAGMENT_ENCODE_SETE, @object
	.size	_ZN4node3url12_GLOBAL__N_1L19FRAGMENT_ENCODE_SETE, 32
_ZN4node3url12_GLOBAL__N_1L19FRAGMENT_ENCODE_SETE:
	.string	"\377\377\377\377\005"
	.string	""
	.string	"P"
	.string	""
	.string	""
	.string	""
	.string	"\001"
	.string	""
	.ascii	"\200\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377"
	.align 32
	.type	_ZN4node3url12_GLOBAL__N_1L21C0_CONTROL_ENCODE_SETE, @object
	.size	_ZN4node3url12_GLOBAL__N_1L21C0_CONTROL_ENCODE_SETE, 32
_ZN4node3url12_GLOBAL__N_1L21C0_CONTROL_ENCODE_SETE:
	.string	"\377\377\377\377"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.ascii	"\200\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377"
	.section	.rodata.str1.1
.LC161:
	.string	"%00"
.LC162:
	.string	"%01"
.LC163:
	.string	"%02"
.LC164:
	.string	"%03"
.LC165:
	.string	"%04"
.LC166:
	.string	"%05"
.LC167:
	.string	"%06"
.LC168:
	.string	"%07"
.LC169:
	.string	"%08"
.LC170:
	.string	"%09"
.LC171:
	.string	"%0A"
.LC172:
	.string	"%0B"
.LC173:
	.string	"%0C"
.LC174:
	.string	"%0D"
.LC175:
	.string	"%0E"
.LC176:
	.string	"%0F"
.LC177:
	.string	"%10"
.LC178:
	.string	"%11"
.LC179:
	.string	"%12"
.LC180:
	.string	"%13"
.LC181:
	.string	"%14"
.LC182:
	.string	"%15"
.LC183:
	.string	"%16"
.LC184:
	.string	"%17"
.LC185:
	.string	"%18"
.LC186:
	.string	"%19"
.LC187:
	.string	"%1A"
.LC188:
	.string	"%1B"
.LC189:
	.string	"%1C"
.LC190:
	.string	"%1D"
.LC191:
	.string	"%1E"
.LC192:
	.string	"%1F"
.LC193:
	.string	"%20"
.LC194:
	.string	"%21"
.LC195:
	.string	"%22"
.LC196:
	.string	"%23"
.LC197:
	.string	"%24"
.LC198:
	.string	"%25"
.LC199:
	.string	"%26"
.LC200:
	.string	"%27"
.LC201:
	.string	"%28"
.LC202:
	.string	"%29"
.LC203:
	.string	"%2A"
.LC204:
	.string	"%2B"
.LC205:
	.string	"%2C"
.LC206:
	.string	"%2D"
.LC207:
	.string	"%2E"
.LC208:
	.string	"%2F"
.LC209:
	.string	"%30"
.LC210:
	.string	"%31"
.LC211:
	.string	"%32"
.LC212:
	.string	"%33"
.LC213:
	.string	"%34"
.LC214:
	.string	"%35"
.LC215:
	.string	"%36"
.LC216:
	.string	"%37"
.LC217:
	.string	"%38"
.LC218:
	.string	"%39"
.LC219:
	.string	"%3A"
.LC220:
	.string	"%3B"
.LC221:
	.string	"%3C"
.LC222:
	.string	"%3D"
.LC223:
	.string	"%3E"
.LC224:
	.string	"%3F"
.LC225:
	.string	"%41"
.LC226:
	.string	"%42"
.LC227:
	.string	"%43"
.LC228:
	.string	"%44"
.LC229:
	.string	"%45"
.LC230:
	.string	"%46"
.LC231:
	.string	"%47"
.LC232:
	.string	"%48"
.LC233:
	.string	"%49"
.LC234:
	.string	"%4A"
.LC235:
	.string	"%4B"
.LC236:
	.string	"%4C"
.LC237:
	.string	"%4D"
.LC238:
	.string	"%4E"
.LC239:
	.string	"%4F"
.LC240:
	.string	"%50"
.LC241:
	.string	"%51"
.LC242:
	.string	"%52"
.LC243:
	.string	"%53"
.LC244:
	.string	"%54"
.LC245:
	.string	"%55"
.LC246:
	.string	"%56"
.LC247:
	.string	"%57"
.LC248:
	.string	"%58"
.LC249:
	.string	"%59"
.LC250:
	.string	"%5A"
.LC251:
	.string	"%5B"
.LC252:
	.string	"%5C"
.LC253:
	.string	"%5D"
.LC254:
	.string	"%5E"
.LC255:
	.string	"%5F"
.LC256:
	.string	"%60"
.LC257:
	.string	"%61"
.LC258:
	.string	"%62"
.LC259:
	.string	"%63"
.LC260:
	.string	"%64"
.LC261:
	.string	"%65"
.LC262:
	.string	"%66"
.LC263:
	.string	"%67"
.LC264:
	.string	"%68"
.LC265:
	.string	"%69"
.LC266:
	.string	"%6A"
.LC267:
	.string	"%6B"
.LC268:
	.string	"%6C"
.LC269:
	.string	"%6D"
.LC270:
	.string	"%6E"
.LC271:
	.string	"%6F"
.LC272:
	.string	"%70"
.LC273:
	.string	"%71"
.LC274:
	.string	"%72"
.LC275:
	.string	"%73"
.LC276:
	.string	"%74"
.LC277:
	.string	"%75"
.LC278:
	.string	"%76"
.LC279:
	.string	"%77"
.LC280:
	.string	"%78"
.LC281:
	.string	"%79"
.LC282:
	.string	"%7A"
.LC283:
	.string	"%7B"
.LC284:
	.string	"%7C"
.LC285:
	.string	"%7D"
.LC286:
	.string	"%7E"
.LC287:
	.string	"%7F"
.LC288:
	.string	"%80"
.LC289:
	.string	"%81"
.LC290:
	.string	"%82"
.LC291:
	.string	"%83"
.LC292:
	.string	"%84"
.LC293:
	.string	"%85"
.LC294:
	.string	"%86"
.LC295:
	.string	"%87"
.LC296:
	.string	"%88"
.LC297:
	.string	"%89"
.LC298:
	.string	"%8A"
.LC299:
	.string	"%8B"
.LC300:
	.string	"%8C"
.LC301:
	.string	"%8D"
.LC302:
	.string	"%8E"
.LC303:
	.string	"%8F"
.LC304:
	.string	"%90"
.LC305:
	.string	"%91"
.LC306:
	.string	"%92"
.LC307:
	.string	"%93"
.LC308:
	.string	"%94"
.LC309:
	.string	"%95"
.LC310:
	.string	"%96"
.LC311:
	.string	"%97"
.LC312:
	.string	"%98"
.LC313:
	.string	"%99"
.LC314:
	.string	"%9A"
.LC315:
	.string	"%9B"
.LC316:
	.string	"%9C"
.LC317:
	.string	"%9D"
.LC318:
	.string	"%9E"
.LC319:
	.string	"%9F"
.LC320:
	.string	"%A0"
.LC321:
	.string	"%A1"
.LC322:
	.string	"%A2"
.LC323:
	.string	"%A3"
.LC324:
	.string	"%A4"
.LC325:
	.string	"%A5"
.LC326:
	.string	"%A6"
.LC327:
	.string	"%A7"
.LC328:
	.string	"%A8"
.LC329:
	.string	"%A9"
.LC330:
	.string	"%AA"
.LC331:
	.string	"%AB"
.LC332:
	.string	"%AC"
.LC333:
	.string	"%AD"
.LC334:
	.string	"%AE"
.LC335:
	.string	"%AF"
.LC336:
	.string	"%B0"
.LC337:
	.string	"%B1"
.LC338:
	.string	"%B2"
.LC339:
	.string	"%B3"
.LC340:
	.string	"%B4"
.LC341:
	.string	"%B5"
.LC342:
	.string	"%B6"
.LC343:
	.string	"%B7"
.LC344:
	.string	"%B8"
.LC345:
	.string	"%B9"
.LC346:
	.string	"%BA"
.LC347:
	.string	"%BB"
.LC348:
	.string	"%BC"
.LC349:
	.string	"%BD"
.LC350:
	.string	"%BE"
.LC351:
	.string	"%BF"
.LC352:
	.string	"%C0"
.LC353:
	.string	"%C1"
.LC354:
	.string	"%C2"
.LC355:
	.string	"%C3"
.LC356:
	.string	"%C4"
.LC357:
	.string	"%C5"
.LC358:
	.string	"%C6"
.LC359:
	.string	"%C7"
.LC360:
	.string	"%C8"
.LC361:
	.string	"%C9"
.LC362:
	.string	"%CA"
.LC363:
	.string	"%CB"
.LC364:
	.string	"%CC"
.LC365:
	.string	"%CD"
.LC366:
	.string	"%CE"
.LC367:
	.string	"%CF"
.LC368:
	.string	"%D0"
.LC369:
	.string	"%D1"
.LC370:
	.string	"%D2"
.LC371:
	.string	"%D3"
.LC372:
	.string	"%D4"
.LC373:
	.string	"%D5"
.LC374:
	.string	"%D6"
.LC375:
	.string	"%D7"
.LC376:
	.string	"%D8"
.LC377:
	.string	"%D9"
.LC378:
	.string	"%DA"
.LC379:
	.string	"%DB"
.LC380:
	.string	"%DC"
.LC381:
	.string	"%DD"
.LC382:
	.string	"%DE"
.LC383:
	.string	"%DF"
.LC384:
	.string	"%E0"
.LC385:
	.string	"%E1"
.LC386:
	.string	"%E2"
.LC387:
	.string	"%E3"
.LC388:
	.string	"%E4"
.LC389:
	.string	"%E5"
.LC390:
	.string	"%E6"
.LC391:
	.string	"%E7"
.LC392:
	.string	"%E8"
.LC393:
	.string	"%E9"
.LC394:
	.string	"%EA"
.LC395:
	.string	"%EB"
.LC396:
	.string	"%EC"
.LC397:
	.string	"%ED"
.LC398:
	.string	"%EE"
.LC399:
	.string	"%EF"
.LC400:
	.string	"%F0"
.LC401:
	.string	"%F1"
.LC402:
	.string	"%F2"
.LC403:
	.string	"%F3"
.LC404:
	.string	"%F4"
.LC405:
	.string	"%F5"
.LC406:
	.string	"%F6"
.LC407:
	.string	"%F7"
.LC408:
	.string	"%F8"
.LC409:
	.string	"%F9"
.LC410:
	.string	"%FA"
.LC411:
	.string	"%FB"
.LC412:
	.string	"%FC"
.LC413:
	.string	"%FD"
.LC414:
	.string	"%FE"
.LC415:
	.string	"%FF"
	.section	.data.rel.ro.local
	.align 32
	.type	_ZN4node3url12_GLOBAL__N_13hexE, @object
	.size	_ZN4node3url12_GLOBAL__N_13hexE, 2048
_ZN4node3url12_GLOBAL__N_13hexE:
	.quad	.LC161
	.quad	.LC162
	.quad	.LC163
	.quad	.LC164
	.quad	.LC165
	.quad	.LC166
	.quad	.LC167
	.quad	.LC168
	.quad	.LC169
	.quad	.LC170
	.quad	.LC171
	.quad	.LC172
	.quad	.LC173
	.quad	.LC174
	.quad	.LC175
	.quad	.LC176
	.quad	.LC177
	.quad	.LC178
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.quad	.LC185
	.quad	.LC186
	.quad	.LC187
	.quad	.LC188
	.quad	.LC189
	.quad	.LC190
	.quad	.LC191
	.quad	.LC192
	.quad	.LC193
	.quad	.LC194
	.quad	.LC195
	.quad	.LC196
	.quad	.LC197
	.quad	.LC198
	.quad	.LC199
	.quad	.LC200
	.quad	.LC201
	.quad	.LC202
	.quad	.LC203
	.quad	.LC204
	.quad	.LC205
	.quad	.LC206
	.quad	.LC207
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.quad	.LC217
	.quad	.LC218
	.quad	.LC219
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.quad	.LC223
	.quad	.LC224
	.quad	.LC87
	.quad	.LC225
	.quad	.LC226
	.quad	.LC227
	.quad	.LC228
	.quad	.LC229
	.quad	.LC230
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.quad	.LC240
	.quad	.LC241
	.quad	.LC242
	.quad	.LC243
	.quad	.LC244
	.quad	.LC245
	.quad	.LC246
	.quad	.LC247
	.quad	.LC248
	.quad	.LC249
	.quad	.LC250
	.quad	.LC251
	.quad	.LC252
	.quad	.LC253
	.quad	.LC254
	.quad	.LC255
	.quad	.LC256
	.quad	.LC257
	.quad	.LC258
	.quad	.LC259
	.quad	.LC260
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.quad	.LC267
	.quad	.LC268
	.quad	.LC269
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.quad	.LC279
	.quad	.LC280
	.quad	.LC281
	.quad	.LC282
	.quad	.LC283
	.quad	.LC284
	.quad	.LC285
	.quad	.LC286
	.quad	.LC287
	.quad	.LC288
	.quad	.LC289
	.quad	.LC290
	.quad	.LC291
	.quad	.LC292
	.quad	.LC293
	.quad	.LC294
	.quad	.LC295
	.quad	.LC296
	.quad	.LC297
	.quad	.LC298
	.quad	.LC299
	.quad	.LC300
	.quad	.LC301
	.quad	.LC302
	.quad	.LC303
	.quad	.LC304
	.quad	.LC305
	.quad	.LC306
	.quad	.LC307
	.quad	.LC308
	.quad	.LC309
	.quad	.LC310
	.quad	.LC311
	.quad	.LC312
	.quad	.LC313
	.quad	.LC314
	.quad	.LC315
	.quad	.LC316
	.quad	.LC317
	.quad	.LC318
	.quad	.LC319
	.quad	.LC320
	.quad	.LC321
	.quad	.LC322
	.quad	.LC323
	.quad	.LC324
	.quad	.LC325
	.quad	.LC326
	.quad	.LC327
	.quad	.LC328
	.quad	.LC329
	.quad	.LC330
	.quad	.LC331
	.quad	.LC332
	.quad	.LC333
	.quad	.LC334
	.quad	.LC335
	.quad	.LC336
	.quad	.LC337
	.quad	.LC338
	.quad	.LC339
	.quad	.LC340
	.quad	.LC341
	.quad	.LC342
	.quad	.LC343
	.quad	.LC344
	.quad	.LC345
	.quad	.LC346
	.quad	.LC347
	.quad	.LC348
	.quad	.LC349
	.quad	.LC350
	.quad	.LC351
	.quad	.LC352
	.quad	.LC353
	.quad	.LC354
	.quad	.LC355
	.quad	.LC356
	.quad	.LC357
	.quad	.LC358
	.quad	.LC359
	.quad	.LC360
	.quad	.LC361
	.quad	.LC362
	.quad	.LC363
	.quad	.LC364
	.quad	.LC365
	.quad	.LC366
	.quad	.LC367
	.quad	.LC368
	.quad	.LC369
	.quad	.LC370
	.quad	.LC371
	.quad	.LC372
	.quad	.LC373
	.quad	.LC374
	.quad	.LC375
	.quad	.LC376
	.quad	.LC377
	.quad	.LC378
	.quad	.LC379
	.quad	.LC380
	.quad	.LC381
	.quad	.LC382
	.quad	.LC383
	.quad	.LC384
	.quad	.LC385
	.quad	.LC386
	.quad	.LC387
	.quad	.LC388
	.quad	.LC389
	.quad	.LC390
	.quad	.LC391
	.quad	.LC392
	.quad	.LC393
	.quad	.LC394
	.quad	.LC395
	.quad	.LC396
	.quad	.LC397
	.quad	.LC398
	.quad	.LC399
	.quad	.LC400
	.quad	.LC401
	.quad	.LC402
	.quad	.LC403
	.quad	.LC404
	.quad	.LC405
	.quad	.LC406
	.quad	.LC407
	.quad	.LC408
	.quad	.LC409
	.quad	.LC410
	.quad	.LC411
	.quad	.LC412
	.quad	.LC413
	.quad	.LC414
	.quad	.LC415
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	0
	.quad	1024
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC9:
	.long	0
	.long	1081081856
	.align 8
.LC24:
	.long	0
	.long	1072693248
	.align 8
.LC26:
	.long	0
	.long	1073741824
	.align 8
.LC28:
	.long	0
	.long	1074790400
	.align 8
.LC30:
	.long	0
	.long	1075838976
	.align 8
.LC32:
	.long	0
	.long	1076887552
	.align 8
.LC34:
	.long	0
	.long	1077936128
	.align 8
.LC36:
	.long	0
	.long	1078984704
	.align 8
.LC38:
	.long	0
	.long	1080033280
	.align 8
.LC41:
	.long	0
	.long	1082130432
	.align 8
.LC43:
	.long	0
	.long	1083179008
	.align 8
.LC45:
	.long	0
	.long	1084227584
	.align 8
.LC50:
	.long	0
	.long	1074266112
	.align 8
.LC53:
	.long	0
	.long	1075052544
	.align 8
.LC55:
	.long	0
	.long	1075314688
	.align 8
.LC57:
	.long	0
	.long	1075576832
	.align 8
.LC60:
	.long	0
	.long	1075970048
	.align 8
.LC62:
	.long	0
	.long	1076101120
	.align 8
.LC64:
	.long	0
	.long	1076232192
	.align 8
.LC66:
	.long	0
	.long	1076363264
	.align 8
.LC68:
	.long	0
	.long	1076494336
	.align 8
.LC70:
	.long	0
	.long	1076625408
	.align 8
.LC72:
	.long	0
	.long	1076756480
	.align 8
.LC75:
	.long	0
	.long	1076953088
	.align 8
.LC77:
	.long	0
	.long	1077018624
	.align 8
.LC79:
	.long	0
	.long	1077084160
	.align 8
.LC81:
	.long	0
	.long	1077149696
	.section	.rodata.cst16
	.align 16
.LC86:
	.quad	0
	.quad	128
	.align 16
.LC92:
	.quad	104
	.quad	104
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
