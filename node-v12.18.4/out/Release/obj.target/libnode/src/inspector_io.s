	.file	"inspector_io.cc"
	.text
	.section	.text._ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE,"axG",@progbits,_ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE
	.type	_ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE, @function
_ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE:
.LFB8566:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	movq	%rsi, 144(%rax)
	ret
	.cfi_endproc
.LFE8566:
	.size	_ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE, .-_ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11577:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11577:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.text
	.align 2
	.p2align 4
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB11584:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11584:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.set	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.align 2
	.p2align 4
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11587:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %r8
	movq	16(%rdi), %rax
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE11587:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.align 2
	.p2align 4
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11589:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE11589:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB11580:
	.cfi_startproc
	endbr64
	addq	$24, %rdi
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE11580:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.text
	.align 2
	.p2align 4
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11586:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11586:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB11579:
	.cfi_startproc
	endbr64
	movl	$64, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11579:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11581:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE11581:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.text
	.align 2
	.p2align 4
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB11588:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE11588:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB11582:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L12
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L12:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11582:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::append"
.LC1:
	.string	"file://"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19InspectorIoDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector19InspectorIoDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector19InspectorIoDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	movq	136(%rsi), %rax
	leaq	7(%rax), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movabsq	$4611686018427387903, %rax
	subq	8(%r12), %rax
	cmpq	$6, %rax
	jbe	.L19
	movq	%r12, %rdi
	movl	$7, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	136(%rbx), %rdx
	movq	128(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L19:
	.cfi_restore_state
	leaq	.LC0(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8648:
	.size	_ZN4node9inspector19InspectorIoDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector19InspectorIoDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19InspectorIoDelegate10EndSessionEi
	.type	_ZN4node9inspector19InspectorIoDelegate10EndSessionEi, @function
_ZN4node9inspector19InspectorIoDelegate10EndSessionEi:
.LFB8645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	48(%rdi), %rcx
	movq	%rdi, %rbx
	movq	40(%rdi), %r13
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L20
	movq	(%rdi), %r12
	movq	%rdx, %r11
	movq	%rdi, %r10
	movl	8(%r12), %r9d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L20
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%r12, %r10
	movq	%rax, %r9
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L20
	movq	%r8, %r12
.L23:
	cmpl	%esi, %r9d
	jne	.L45
	movq	(%r12), %rsi
	cmpq	%r10, %rdi
	je	.L46
	testq	%rsi, %rsi
	je	.L25
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L25
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r12), %rsi
.L25:
	movq	16(%r12), %rdi
	movq	%rsi, (%r10)
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	call	*8(%rax)
.L27:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 64(%rbx)
.L20:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L30
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L25
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L24:
	leaq	56(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L47
.L26:
	movq	$0, (%r14)
	movq	(%r12), %rsi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r10, %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%rsi, 56(%rbx)
	jmp	.L26
	.cfi_endproc
.LFE8645:
	.size	_ZN4node9inspector19InspectorIoDelegate10EndSessionEi, .-_ZN4node9inspector19InspectorIoDelegate10EndSessionEi
	.align 2
	.p2align 4
	.type	_ZNSt11_Deque_baseIN4node9inspector12_GLOBAL__N_115RequestToServerESaIS3_EE17_M_initialize_mapEm.constprop.0, @function
_ZNSt11_Deque_baseIN4node9inspector12_GLOBAL__N_115RequestToServerESaIS3_EE17_M_initialize_mapEm.constprop.0:
.LFB11794:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	$8, 8(%rdi)
	movl	$64, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	movl	$512, %edi
	movq	%rax, (%rbx)
	leaq	-4(,%rdx,4), %r12
	andq	$-8, %r12
	addq	%rax, %r12
	call	_Znwm@PLT
	movq	%r12, 40(%rbx)
	movq	%rax, %xmm0
	leaq	512(%rax), %rdx
	movq	%rax, (%r12)
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, 72(%rbx)
	movq	%rdx, 32(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rdx, 64(%rbx)
	movq	%rax, 48(%rbx)
	movups	%xmm0, 16(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11794:
	.size	_ZNSt11_Deque_baseIN4node9inspector12_GLOBAL__N_115RequestToServerESaIS3_EE17_M_initialize_mapEm.constprop.0, .-_ZNSt11_Deque_baseIN4node9inspector12_GLOBAL__N_115RequestToServerESaIS3_EE17_M_initialize_mapEm.constprop.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"%04x%04x-%04x-%04x-%04x-%04x%04x%04x"
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_110GenerateIDEv, @function
_ZN4node9inspector12_GLOBAL__N_110GenerateIDEv:
.LFB8525:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$16, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	-320(%rbp), %rdi
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN4node6crypto13EntropySourceEPhm@PLT
	testb	%al, %al
	je	.L75
	movzwl	-306(%rbp), %eax
	subq	$8, %rsp
	leaq	-304(%rbp), %r13
	movzwl	-320(%rbp), %r9d
	movl	$256, %ecx
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%r13, %rbx
	pushq	%rax
	movzwl	-308(%rbp), %eax
	leaq	.LC2(%rip), %r8
	movl	$256, %esi
	pushq	%rax
	movzwl	-310(%rbp), %eax
	pushq	%rax
	movzwl	-312(%rbp), %eax
	andl	$16383, %eax
	orb	$-128, %ah
	pushq	%rax
	movzwl	-314(%rbp), %eax
	andl	$4095, %eax
	orb	$64, %ah
	pushq	%rax
	movzwl	-316(%rbp), %eax
	pushq	%rax
	movzwl	-318(%rbp), %eax
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	leaq	16(%r12), %rax
	movq	%rax, (%r12)
.L52:
	movl	(%rbx), %ecx
	addq	$4, %rbx
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L52
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rbx), %rcx
	cmove	%rcx, %rbx
	movl	%edx, %esi
	addb	%dl, %sil
	sbbq	$3, %rbx
	addq	$64, %rsp
	subq	%r13, %rbx
	movq	%rbx, -328(%rbp)
	cmpq	$15, %rbx
	ja	.L76
	cmpq	$1, %rbx
	jne	.L56
	movzbl	-304(%rbp), %edx
	movb	%dl, 16(%r12)
.L57:
	movq	%rbx, 8(%r12)
	movb	$0, (%rax,%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L77
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	testq	%rbx, %rbx
	je	.L57
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%edx, %edx
	leaq	-328(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-328(%rbp), %rdx
	movq	%rax, (%r12)
	movq	%rdx, 16(%r12)
.L55:
	movl	%ebx, %ecx
	cmpl	$8, %ebx
	jnb	.L58
	andl	$4, %ebx
	jne	.L78
	testl	%ecx, %ecx
	je	.L59
	movzbl	0(%r13), %edx
	movb	%dl, (%rax)
	testb	$2, %cl
	jne	.L79
.L59:
	movq	-328(%rbp), %rbx
	movq	(%r12), %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-304(%rbp), %rdx
	leaq	8(%rax), %rdi
	movq	%r13, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%r13,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rbx,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	_ZZN4node9inspector12_GLOBAL__N_110GenerateIDEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	movl	0(%r13), %edx
	movl	%edx, (%rax)
	movl	-4(%r13,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L59
.L79:
	movzwl	-2(%r13,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L59
.L77:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8525:
	.size	_ZN4node9inspector12_GLOBAL__N_110GenerateIDEv, .-_ZN4node9inspector12_GLOBAL__N_110GenerateIDEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19InspectorIoDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector19InspectorIoDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector19InspectorIoDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	48(%rdi), %r8
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movslq	%esi, %rax
	divq	%r8
	movq	40(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L80
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movl	8(%rcx), %edi
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L80
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L80
.L83:
	cmpl	%esi, %edi
	jne	.L94
	movq	16(%rcx), %r12
	movq	%r9, %rsi
	leaq	-32(%rbp), %rdi
	movq	(%r12), %rax
	movq	16(%rax), %rbx
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-32(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%rbx
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*8(%rax)
.L80:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L95:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8644:
	.size	_ZN4node9inspector19InspectorIoDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector19InspectorIoDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.type	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sENUlP10uv_async_sE_4_FUNES6_, @function
_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sENUlP10uv_async_sE_4_FUNES6_:
.LFB8540:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 128(%rdi)
	je	.L96
	leaq	216(%rdi), %r12
	leaq	-16(%rdi), %r15
	movq	%r12, %rdi
	leaq	-96(%rbp), %r13
	call	uv_mutex_lock@PLT
	pxor	%xmm0, %xmm0
	leaq	-176(%rbp), %rdi
	movq	$0, -176(%rbp)
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	movq	$0, -168(%rbp)
	call	_ZNSt11_Deque_baseIN4node9inspector12_GLOBAL__N_115RequestToServerESaIS3_EE17_M_initialize_mapEm.constprop.0
	movq	176(%r15), %rdx
	movq	%r12, %rdi
	movq	192(%r15), %rax
	movdqa	-160(%rbp), %xmm2
	movdqa	-128(%rbp), %xmm6
	movq	%rdx, %xmm4
	movq	%rax, %xmm5
	movdqa	-112(%rbp), %xmm7
	movq	208(%r15), %rdx
	movdqa	-144(%rbp), %xmm3
	movq	224(%r15), %rax
	movq	168(%r15), %xmm1
	movups	%xmm2, 168(%r15)
	movq	%rdx, %xmm2
	movq	-176(%rbp), %rdx
	movq	184(%r15), %xmm0
	movups	%xmm3, 184(%r15)
	movq	%rax, %xmm3
	movq	152(%r15), %rax
	movq	%rdx, 152(%r15)
	movq	-168(%rbp), %rdx
	punpcklqdq	%xmm4, %xmm1
	punpcklqdq	%xmm5, %xmm0
	movq	%rax, -176(%rbp)
	movq	160(%r15), %rax
	movaps	%xmm1, -160(%rbp)
	movq	200(%r15), %xmm1
	movaps	%xmm0, -144(%rbp)
	movq	216(%r15), %xmm0
	movq	%rdx, 160(%r15)
	punpcklqdq	%xmm2, %xmm1
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm6, 200(%r15)
	movups	%xmm7, 216(%r15)
	movq	%rax, -168(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	uv_mutex_unlock@PLT
	movq	-144(%rbp), %rax
	movq	-160(%rbp), %rbx
	movq	%rax, -184(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-136(%rbp), %rax
	leaq	8(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L103:
	cmpq	%rbx, -192(%rbp)
	je	.L98
.L143:
	movl	(%rbx), %eax
	movq	144(%r15), %r14
	cmpl	$1, %eax
	je	.L99
	cmpl	$2, %eax
	je	.L100
	testl	%eax, %eax
	je	.L142
.L101:
	addq	$16, %rbx
	cmpq	%rbx, -184(%rbp)
	jne	.L103
	movq	(%r12), %rbx
	addq	$8, %r12
	leaq	512(%rbx), %rax
	movq	%rax, -184(%rbp)
	cmpq	%rbx, -192(%rbp)
	jne	.L143
.L98:
	movq	-136(%rbp), %rax
	movq	-104(%rbp), %rcx
	movq	-128(%rbp), %r13
	movq	-120(%rbp), %r15
	leaq	8(%rax), %rdx
	movq	%rax, -208(%rbp)
	movq	-160(%rbp), %rbx
	movq	-144(%rbp), %r14
	cmpq	%rdx, %rcx
	jbe	.L112
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%rdx), %r12
	leaq	512(%r12), %rsi
	.p2align 4,,10
	.p2align 3
.L111:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L108
	movq	(%rdi), %r8
	movq	%rsi, -200(%rbp)
	addq	$16, %r12
	movq	%rdx, -192(%rbp)
	movq	%rcx, -184(%rbp)
	call	*8(%r8)
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rcx
	movq	-192(%rbp), %rdx
	cmpq	%rsi, %r12
	jne	.L111
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L105
.L112:
	cmpq	-208(%rbp), %rcx
	je	.L139
	cmpq	%r14, %rbx
	je	.L118
	.p2align 4,,10
	.p2align 3
.L113:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L117
	movq	(%rdi), %rdx
	addq	$16, %rbx
	call	*8(%rdx)
	cmpq	%rbx, %r14
	jne	.L113
.L118:
	cmpq	%r15, %r13
	je	.L121
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rdx
	addq	$16, %r15
	call	*8(%rdx)
	cmpq	%r13, %r15
	jne	.L114
.L121:
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
.L115:
	movq	-104(%rbp), %rax
	movq	-136(%rbp), %rbx
	leaq	8(%rax), %r12
	cmpq	%rbx, %r12
	jbe	.L126
	.p2align 4,,10
	.p2align 3
.L127:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r12
	ja	.L127
	movq	-176(%rbp), %rdi
.L126:
	call	_ZdlPv@PLT
.L96:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN4node9inspector21InspectorSocketServer20TerminateConnectionsEv@PLT
.L100:
	movq	%r14, %rdi
	call	_ZN4node9inspector21InspectorSocketServer4StopEv@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L99:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	subq	$8, %rsp
	movq	%r13, %rdi
	pushq	16(%rax)
	pushq	8(%rax)
	pushq	(%rax)
	call	_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE@PLT
	movl	4(%rbx), %esi
	movq	%r14, %rdi
	addq	$32, %rsp
	movq	%r13, %rdx
	call	_ZN4node9inspector21InspectorSocketServer4SendEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L101
	call	_ZdlPv@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L125:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L141
	movq	(%rdi), %rax
	call	*8(%rax)
.L141:
	addq	$16, %rbx
.L139:
	cmpq	%rbx, %r13
	jne	.L125
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L115
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L108:
	addq	$16, %r12
	cmpq	%rsi, %r12
	jne	.L111
	addq	$8, %rdx
	cmpq	%rdx, %rcx
	ja	.L105
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L117:
	addq	$16, %rbx
	cmpq	%rbx, %r14
	jne	.L113
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$16, %r15
	cmpq	%r15, %r13
	jne	.L114
	movq	-176(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L115
	jmp	.L96
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8540:
	.size	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sENUlP10uv_async_sE_4_FUNES6_, .-_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sENUlP10uv_async_sE_4_FUNES6_
	.section	.text._ZN4node9inspector19InspectorIoDelegateD0Ev,"axG",@progbits,_ZN4node9inspector19InspectorIoDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19InspectorIoDelegateD0Ev
	.type	_ZN4node9inspector19InspectorIoDelegateD0Ev, @function
_ZN4node9inspector19InspectorIoDelegateD0Ev:
.LFB10024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector19InspectorIoDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	160(%rdi), %rdi
	leaq	176(%r12), %rax
	cmpq	%rax, %rdi
	je	.L146
	call	_ZdlPv@PLT
.L146:
	movq	128(%r12), %rdi
	leaq	144(%r12), %rax
	cmpq	%rax, %rdi
	je	.L147
	call	_ZdlPv@PLT
.L147:
	movq	96(%r12), %rdi
	leaq	112(%r12), %rax
	cmpq	%rax, %rdi
	je	.L148
	call	_ZdlPv@PLT
.L148:
	movq	56(%r12), %r13
	testq	%r13, %r13
	jne	.L152
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L149
.L151:
	movq	%rbx, %r13
.L152:
	movq	16(%r13), %rdi
	movq	0(%r13), %rbx
	testq	%rdi, %rdi
	jne	.L178
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L151
.L149:
	movq	48(%r12), %rax
	movq	40(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	40(%r12), %rdi
	leaq	88(%r12), %rax
	movq	$0, 64(%r12)
	movq	$0, 56(%r12)
	cmpq	%rax, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L155
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L156
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L179
	.p2align 4,,10
	.p2align 3
.L155:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L162
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L163
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L180
.L162:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L155
.L179:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L159
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L160:
	cmpl	$1, %eax
	jne	.L155
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L163:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L162
.L180:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L166
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L167:
	cmpl	$1, %eax
	jne	.L162
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L159:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L166:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L167
	.cfi_endproc
.LFE10024:
	.size	_ZN4node9inspector19InspectorIoDelegateD0Ev, .-_ZN4node9inspector19InspectorIoDelegateD0Ev
	.section	.text._ZN4node9inspector17IoSessionDelegateD2Ev,"axG",@progbits,_ZN4node9inspector17IoSessionDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector17IoSessionDelegateD2Ev
	.type	_ZN4node9inspector17IoSessionDelegateD2Ev, @function
_ZN4node9inspector17IoSessionDelegateD2Ev:
.LFB11549:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector17IoSessionDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L181
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L184
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L190
.L181:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L181
.L190:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L187
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L188:
	cmpl	$1, %eax
	jne	.L181
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L188
	.cfi_endproc
.LFE11549:
	.size	_ZN4node9inspector17IoSessionDelegateD2Ev, .-_ZN4node9inspector17IoSessionDelegateD2Ev
	.weak	_ZN4node9inspector17IoSessionDelegateD1Ev
	.set	_ZN4node9inspector17IoSessionDelegateD1Ev,_ZN4node9inspector17IoSessionDelegateD2Ev
	.section	.text._ZN4node9inspector17IoSessionDelegateD0Ev,"axG",@progbits,_ZN4node9inspector17IoSessionDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector17IoSessionDelegateD0Ev
	.type	_ZN4node9inspector17IoSessionDelegateD0Ev, @function
_ZN4node9inspector17IoSessionDelegateD0Ev:
.LFB11551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector17IoSessionDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L193
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L194
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L200
.L193:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L193
.L200:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L197
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L198:
	cmpl	$1, %eax
	jne	.L193
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L197:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L198
	.cfi_endproc
.LFE11551:
	.size	_ZN4node9inspector17IoSessionDelegateD0Ev, .-_ZN4node9inspector17IoSessionDelegateD0Ev
	.section	.rodata._ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE,"axG",@progbits,_ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.type	_ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE:
.LFB8565:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	8(%rdi), %r14
	leaq	-64(%rbp), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	leaq	8(%r14), %r12
	movl	24(%rbx), %r13d
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L202
	leaq	232(%rbx), %r15
	movq	-64(%rbp), %r14
	movq	$0, -64(%rbp)
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	216(%rbx), %rax
	movq	200(%rbx), %rcx
	movq	168(%rbx), %r9
	subq	$16, %rax
	cmpq	%rax, %rcx
	je	.L203
	leaq	16(%rcx), %rax
	movl	$1, (%rcx)
	movl	%r13d, 4(%rcx)
	movq	%r14, 8(%rcx)
	movq	%rax, 200(%rbx)
.L204:
	cmpq	%r9, %rcx
	je	.L223
.L212:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
.L202:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L201
	movq	(%rdi), %rax
	call	*8(%rax)
.L201:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L224
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	movq	224(%rbx), %r8
	movq	192(%rbx), %rsi
	movq	%rcx, %rdi
	subq	208(%rbx), %rdi
	movq	%r8, %r10
	sarq	$4, %rdi
	subq	%rsi, %r10
	movq	%r10, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rax
	salq	$5, %rax
	leaq	(%rdi,%rax), %rdi
	movq	184(%rbx), %rax
	subq	%r9, %rax
	sarq	$4, %rax
	addq	%rdi, %rax
	movabsq	$576460752303423487, %rdi
	cmpq	%rdi, %rax
	je	.L225
	movq	152(%rbx), %rdi
	movq	160(%rbx), %rax
	movq	%r8, %r11
	subq	%rdi, %r11
	movq	%rdi, -72(%rbp)
	movq	%rax, %rdi
	sarq	$3, %r11
	subq	%r11, %rdi
	cmpq	$1, %rdi
	jbe	.L226
.L206:
	movl	$512, %edi
	movq	%r9, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %r8
	movq	224(%rbx), %rdx
	movq	-88(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	%rax, 8(%r8)
	movq	200(%rbx), %rax
	addq	$8, %rdx
	movl	$1, (%rax)
	movl	%r13d, 4(%rax)
	movq	%r14, 8(%rax)
	movq	(%rdx), %rax
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 200(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 216(%rbx)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	16(%rbx), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L227
	leaq	272(%rbx), %rdi
	call	uv_cond_broadcast@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L226:
	addq	$2, %rdx
	leaq	(%rdx,%rdx), %r11
	cmpq	%r11, %rax
	jbe	.L207
	subq	%rdx, %rax
	movq	-72(%rbp), %rdx
	shrq	%rax
	leaq	(%rdx,%rax,8), %r11
	leaq	8(%r8), %rax
	movq	%rax, %rdx
	subq	%rsi, %rdx
	cmpq	%r11, %rsi
	jbe	.L208
	cmpq	%rax, %rsi
	je	.L209
	movq	%r11, %rdi
	movq	%r10, -88(%rbp)
	movq	%r9, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r9
	movq	-88(%rbp), %r10
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L209:
	movq	(%r11), %rax
	movq	(%r11), %xmm0
	leaq	(%r11,%r10), %r8
	movq	%r11, 192(%rbx)
	movq	%r8, 224(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 176(%rbx)
	movq	(%r8), %rax
	movq	%rax, 208(%rbx)
	addq	$512, %rax
	movq	%rax, 216(%rbx)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData4PostEiNS1_15TransportActionESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS6_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	testq	%rax, %rax
	movl	$1, %esi
	cmovne	%rax, %rsi
	leaq	2(%rax,%rsi), %rax
	movq	%rax, -72(%rbp)
	movq	%rax, %rsi
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rsi
	ja	.L228
	leaq	0(,%rsi,8), %rdi
	movq	%rdx, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %rdx
	movq	192(%rbx), %rsi
	movq	%rax, %rcx
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r10
	movq	%rax, -80(%rbp)
	movq	-72(%rbp), %rax
	subq	%rdx, %rax
	shrq	%rax
	leaq	(%rcx,%rax,8), %r11
	movq	224(%rbx), %rax
	movq	-88(%rbp), %rcx
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L211
	movq	%r11, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r9
	movq	-88(%rbp), %rcx
	movq	%rax, %r11
.L211:
	movq	152(%rbx), %rdi
	movq	%r11, -112(%rbp)
	movq	%r10, -104(%rbp)
	movq	%r9, -96(%rbp)
	movq	%rcx, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rax
	movq	-112(%rbp), %r11
	movq	-104(%rbp), %r10
	movq	-96(%rbp), %r9
	movq	%rax, 152(%rbx)
	movq	-72(%rbp), %rax
	movq	-88(%rbp), %rcx
	movq	%rax, 160(%rbx)
	jmp	.L209
.L208:
	cmpq	%rax, %rsi
	je	.L209
	leaq	8(%r10), %rdi
	movq	%r9, -96(%rbp)
	subq	%rdx, %rdi
	movq	%rcx, -88(%rbp)
	addq	%r11, %rdi
	movq	%r10, -80(%rbp)
	movq	%r11, -72(%rbp)
	call	memmove@PLT
	movq	-72(%rbp), %r11
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %rcx
	movq	-96(%rbp), %r9
	jmp	.L209
.L224:
	call	__stack_chk_fail@PLT
.L228:
	call	_ZSt17__throw_bad_allocv@PLT
.L225:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8565:
	.size	_ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, .-_ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_, @function
_ZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_:
.LFB8649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r13
	leaq	8(%r13), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	$0, 0(%r13)
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	8(%rbx), %r12
	pxor	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	testq	%r12, %r12
	je	.L231
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L232
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L238
.L231:
	addq	$8, %rsp
	leaq	16(%rbx), %rdi
	leaq	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_ENUlP11uv_handle_sE_4_FUNES5_(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_close@PLT
	.p2align 4,,10
	.p2align 3
.L232:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L231
.L238:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L235
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L236:
	cmpl	$1, %eax
	jne	.L231
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L235:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L236
	.cfi_endproc
.LFE8649:
	.size	_ZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_, .-_ZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_
	.section	.text._ZN4node9inspector19InspectorIoDelegateD2Ev,"axG",@progbits,_ZN4node9inspector19InspectorIoDelegateD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector19InspectorIoDelegateD2Ev
	.type	_ZN4node9inspector19InspectorIoDelegateD2Ev, @function
_ZN4node9inspector19InspectorIoDelegateD2Ev:
.LFB10022:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector19InspectorIoDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	160(%rdi), %rdi
	leaq	176(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L240
	call	_ZdlPv@PLT
.L240:
	movq	128(%rbx), %rdi
	leaq	144(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L241
	call	_ZdlPv@PLT
.L241:
	movq	96(%rbx), %rdi
	leaq	112(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L242
	call	_ZdlPv@PLT
.L242:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	jne	.L246
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L272:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L243
.L245:
	movq	%r13, %r12
.L246:
	movq	16(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L272
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L245
.L243:
	movq	48(%rbx), %rax
	movq	40(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	40(%rbx), %rdi
	leaq	88(%rbx), %rax
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	cmpq	%rax, %rdi
	je	.L247
	call	_ZdlPv@PLT
.L247:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L249
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L250
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L273
	.p2align 4,,10
	.p2align 3
.L249:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L239
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L257
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L274
.L239:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L249
.L273:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L253
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L254:
	cmpl	$1, %eax
	jne	.L249
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L257:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L239
.L274:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L260
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L261:
	cmpl	$1, %eax
	jne	.L239
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L260:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L261
	.cfi_endproc
.LFE10022:
	.size	_ZN4node9inspector19InspectorIoDelegateD2Ev, .-_ZN4node9inspector19InspectorIoDelegateD2Ev
	.weak	_ZN4node9inspector19InspectorIoDelegateD1Ev
	.set	_ZN4node9inspector19InspectorIoDelegateD1Ev,_ZN4node9inspector19InspectorIoDelegateD2Ev
	.text
	.p2align 4
	.type	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_ENUlP11uv_handle_sE_4_FUNES5_, @function
_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_ENUlP11uv_handle_sE_4_FUNES5_:
.LFB8654:
	.cfi_startproc
	endbr64
	cmpq	$16, %rdi
	je	.L275
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-16(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	256(%rdi), %rdi
	subq	$56, %rsp
	call	uv_cond_destroy@PLT
	leaq	216(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	movq	192(%r12), %rax
	movq	224(%r12), %r8
	movq	200(%r12), %r13
	movq	208(%r12), %r15
	leaq	8(%rax), %rcx
	movq	%rax, -88(%rbp)
	movq	168(%r12), %rbx
	movq	184(%r12), %r14
	cmpq	%rcx, %r8
	jbe	.L284
	.p2align 4,,10
	.p2align 3
.L277:
	movq	(%rcx), %rax
	leaq	512(%rax), %rsi
	.p2align 4,,10
	.p2align 3
.L283:
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L280
	movq	(%rdi), %r9
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	*8(%r9)
	movq	-80(%rbp), %rax
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %r8
	movq	-72(%rbp), %rcx
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L283
	addq	$8, %rcx
	cmpq	%rcx, %r8
	ja	.L277
.L284:
	cmpq	-88(%rbp), %r8
	je	.L315
	cmpq	%r14, %rbx
	je	.L290
	.p2align 4,,10
	.p2align 3
.L285:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L289
	movq	(%rdi), %rax
	addq	$16, %rbx
	call	*8(%rax)
	cmpq	%rbx, %r14
	jne	.L285
.L290:
	cmpq	%r15, %r13
	je	.L293
	.p2align 4,,10
	.p2align 3
.L286:
	movq	8(%r15), %rdi
	testq	%rdi, %rdi
	je	.L292
	movq	(%rdi), %rax
	addq	$16, %r15
	call	*8(%rax)
	cmpq	%r15, %r13
	jne	.L286
.L293:
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	je	.L288
.L287:
	movq	224(%r12), %rax
	movq	192(%r12), %rbx
	leaq	8(%rax), %r13
	cmpq	%rbx, %r13
	jbe	.L298
	.p2align 4,,10
	.p2align 3
.L299:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	_ZdlPv@PLT
	cmpq	%rbx, %r13
	ja	.L299
	movq	152(%r12), %rdi
.L298:
	call	_ZdlPv@PLT
.L288:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L301
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L302
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L303:
	cmpl	$1, %eax
	jne	.L301
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L305
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L306:
	cmpl	$1, %eax
	je	.L318
.L301:
	addq	$56, %rsp
	movq	%r12, %rdi
	movl	$320, %esi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L317
	movq	(%rdi), %rax
	call	*8(%rax)
.L317:
	addq	$16, %rbx
.L315:
	cmpq	%rbx, %r13
	jne	.L297
	movq	152(%r12), %rdi
	testq	%rdi, %rdi
	jne	.L287
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L280:
	addq	$16, %rax
	cmpq	%rax, %rsi
	jne	.L283
	addq	$8, %rcx
	cmpq	%rcx, %r8
	ja	.L277
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L289:
	addq	$16, %rbx
	cmpq	%rbx, %r14
	jne	.L285
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L292:
	addq	$16, %r15
	cmpq	%r15, %r13
	jne	.L286
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L275:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L318:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L305:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L306
	.cfi_endproc
.LFE8654:
	.size	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_ENUlP11uv_handle_sE_4_FUNES5_, .-_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_ENUlP11uv_handle_sE_4_FUNES5_
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19InspectorIoDelegate12GetTargetIdsB5cxx11Ev
	.type	_ZN4node9inspector19InspectorIoDelegate12GetTargetIdsB5cxx11Ev, @function
_ZN4node9inspector19InspectorIoDelegate12GetTargetIdsB5cxx11Ev:
.LFB8646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	160(%rsi), %r15
	movq	168(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r14, -96(%rbp)
	movq	%r15, %rax
	addq	%r13, %rax
	je	.L320
	testq	%r15, %r15
	je	.L325
.L320:
	movq	%r13, -104(%rbp)
	cmpq	$15, %r13
	ja	.L348
	cmpq	$1, %r13
	jne	.L323
	movzbl	(%r15), %eax
	movb	%al, -80(%rbp)
	movq	%r14, %rax
.L324:
	movq	%r13, -88(%rbp)
	pxor	%xmm0, %xmm0
	movl	$32, %edi
	movb	$0, (%rax,%r13)
	movq	$0, 16(%r12)
	movups	%xmm0, (%r12)
	call	_Znwm@PLT
	movq	-96(%rbp), %r15
	movq	-88(%rbp), %r13
	leaq	16(%rax), %rdi
	leaq	32(%rax), %rcx
	movq	%rax, (%r12)
	movq	%rax, %rbx
	movq	%rdi, (%rax)
	movq	%r15, %rax
	addq	%r13, %rax
	movq	%rcx, 16(%r12)
	je	.L334
	testq	%r15, %r15
	je	.L325
.L334:
	movq	%r13, -104(%rbp)
	cmpq	$15, %r13
	ja	.L349
	cmpq	$1, %r13
	je	.L329
	testq	%r13, %r13
	jne	.L328
.L330:
	movq	%r13, 8(%rbx)
	movb	$0, (%rdi,%r13)
	movq	-96(%rbp), %rdi
	movq	%rcx, 8(%r12)
	cmpq	%r14, %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L350
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	testq	%r13, %r13
	jne	.L351
	movq	%r14, %rax
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L329:
	movzbl	(%r15), %eax
	movb	%al, 16(%rbx)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L349:
	movq	%rbx, %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -120(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-120(%rbp), %rcx
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, 16(%rbx)
.L328:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rcx, -120(%rbp)
	call	memcpy@PLT
	movq	-104(%rbp), %r13
	movq	(%rbx), %rdi
	movq	-120(%rbp), %rcx
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	-96(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L322:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r13
	movq	-96(%rbp), %rax
	jmp	.L324
.L325:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L350:
	call	__stack_chk_fail@PLT
.L351:
	movq	%r14, %rdi
	jmp	.L322
	.cfi_endproc
.LFE8646:
	.size	_ZN4node9inspector19InspectorIoDelegate12GetTargetIdsB5cxx11Ev, .-_ZN4node9inspector19InspectorIoDelegate12GetTargetIdsB5cxx11Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19InspectorIoDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector19InspectorIoDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector19InspectorIoDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	104(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	jne	.L353
	call	_ZN4node27GetHumanReadableProcessNameB5cxx11Ev@PLT
.L352:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L361
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	leaq	16(%rdi), %rdi
	movq	%rdi, (%r12)
	movq	96(%rsi), %r14
	testq	%r14, %r14
	je	.L362
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L363
	cmpq	$1, %r13
	jne	.L357
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L357:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
.L358:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	jmp	.L352
.L362:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L361:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8647:
	.size	_ZN4node9inspector19InspectorIoDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector19InspectorIoDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector11InspectorIoC2ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE
	.type	_ZN4node9inspector11InspectorIoC2ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE, @function
_ZN4node9inspector11InspectorIoC2ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE:
.LFB8581:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movdqu	(%rsi), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movups	%xmm1, (%rdi)
	testq	%rax, %rax
	je	.L365
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L366
	lock addl	$1, 8(%rax)
.L365:
	movdqu	(%rcx), %xmm2
	movq	8(%rcx), %rax
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%r12)
	movups	%xmm2, 32(%r12)
	testq	%rax, %rax
	je	.L367
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L368
	lock addl	$1, 8(%rax)
.L367:
	movzwl	(%r8), %eax
	leaq	64(%r12), %r14
	movq	$0, 56(%r12)
	movq	%r14, %rdi
	movw	%ax, 48(%r12)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L394
	leaq	104(%r12), %r15
	movq	%r15, %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L395
	leaq	168(%r12), %rdi
	movq	%rdi, 152(%r12)
	movq	0(%r13), %rbx
	movq	8(%r13), %r13
	movq	%rbx, %rax
	addq	%r13, %rax
	je	.L371
	testq	%rbx, %rbx
	je	.L396
.L371:
	movq	%r13, -64(%rbp)
	cmpq	$15, %r13
	ja	.L397
	cmpq	$1, %r13
	jne	.L374
	movzbl	(%rbx), %eax
	movb	%al, 168(%r12)
.L375:
	movq	%r13, 160(%r12)
	movb	$0, (%rdi,%r13)
	leaq	184(%r12), %rdi
	call	_ZN4node9inspector12_GLOBAL__N_110GenerateIDEv
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	leaq	56(%r12), %rdi
	movq	%r12, %rdx
	leaq	_ZN4node9inspector11InspectorIo10ThreadMainEPv(%rip), %rsi
	call	uv_thread_create@PLT
	testl	%eax, %eax
	jne	.L398
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	uv_cond_wait@PLT
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L399
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L368:
	addl	$1, 8(%rax)
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L374:
	testq	%r13, %r13
	je	.L375
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L397:
	leaq	152(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 152(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 168(%r12)
.L373:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r13
	movq	152(%r12), %rdi
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L394:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L395:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L398:
	leaq	_ZZN4node9inspector11InspectorIoC4ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L396:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L399:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8581:
	.size	_ZN4node9inspector11InspectorIoC2ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE, .-_ZN4node9inspector11InspectorIoC2ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE
	.globl	_ZN4node9inspector11InspectorIoC1ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE
	.set	_ZN4node9inspector11InspectorIoC1ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE,_ZN4node9inspector11InspectorIoC2ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector11InspectorIoD2Ev
	.type	_ZN4node9inspector11InspectorIoD2Ev, @function
_ZN4node9inspector11InspectorIoD2Ev:
.LFB8584:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	16(%rdi), %r12
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L401
	leaq	232(%r12), %r15
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	216(%r12), %rax
	movq	200(%r12), %rcx
	movq	168(%r12), %r9
	subq	$16, %rax
	cmpq	%rax, %rcx
	je	.L402
	leaq	16(%rcx), %rax
	movq	$0, (%rcx)
	movq	$0, 8(%rcx)
	movq	%rax, 200(%r12)
.L403:
	cmpq	%r9, %rcx
	je	.L441
.L411:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
.L401:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	leaq	56(%rbx), %rdi
	call	uv_thread_join@PLT
	testl	%eax, %eax
	jne	.L442
	movq	184(%rbx), %rdi
	leaq	200(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L414
	call	_ZdlPv@PLT
.L414:
	movq	152(%rbx), %rdi
	leaq	168(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L415
	call	_ZdlPv@PLT
.L415:
	leaq	104(%rbx), %rdi
	call	uv_cond_destroy@PLT
	leaq	64(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L417
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L418
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L443
	.p2align 4,,10
	.p2align 3
.L417:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L424
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L425
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L444
	.p2align 4,,10
	.p2align 3
.L424:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L400
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L432
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L445
.L400:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L424
.L444:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L428
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L429:
	cmpl	$1, %eax
	jne	.L424
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L418:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L417
.L443:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L421
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L422:
	cmpl	$1, %eax
	jne	.L417
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L432:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L400
.L445:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L435
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L436:
	cmpl	$1, %eax
	jne	.L400
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L402:
	.cfi_restore_state
	movq	224(%r12), %r14
	movq	192(%r12), %r8
	movq	%rcx, %rdx
	subq	208(%r12), %rdx
	movq	%r14, %r10
	movq	%rdx, %rsi
	subq	%r8, %r10
	sarq	$4, %rsi
	movq	%r10, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$5, %rax
	leaq	(%rsi,%rax), %rdx
	movq	184(%r12), %rax
	subq	%r9, %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	je	.L446
	movq	152(%r12), %r11
	movq	160(%r12), %rsi
	movq	%r14, %rax
	subq	%r11, %rax
	movq	%rsi, %rdx
	sarq	$3, %rax
	subq	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L447
.L405:
	movl	$512, %edi
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rcx
	movq	%rax, 8(%r14)
	movq	200(%r12), %rax
	movq	$0, (%rax)
	movq	224(%r12), %rdx
	movq	$0, 8(%rax)
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 200(%r12)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 216(%r12)
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	16(%r12), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L448
	leaq	272(%r12), %rdi
	call	uv_cond_broadcast@PLT
	jmp	.L411
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	_ZZN4node9inspector11InspectorIoD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L428:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L421:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L435:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L447:
	leaq	2(%rdi), %rdx
	leaq	(%rdx,%rdx), %rax
	cmpq	%rax, %rsi
	jbe	.L406
	subq	%rdx, %rsi
	addq	$8, %r14
	shrq	%rsi
	movq	%r14, %rdx
	leaq	(%r11,%rsi,8), %r11
	subq	%r8, %rdx
	cmpq	%r11, %r8
	jbe	.L407
	cmpq	%r14, %r8
	je	.L408
	movq	%r11, %rdi
	movq	%r8, %rsi
	movq	%r10, -72(%rbp)
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %r10
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L408:
	movq	(%r11), %rax
	movq	(%r11), %xmm0
	leaq	(%r11,%r10), %r14
	movq	%r11, 192(%r12)
	movq	%r14, 224(%r12)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 176(%r12)
	movq	(%r14), %rax
	movq	%rax, 208(%r12)
	addq	$512, %rax
	movq	%rax, 216(%r12)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L448:
	leaq	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData4PostEiNS1_15TransportActionESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS6_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L406:
	testq	%rsi, %rsi
	movl	$1, %eax
	cmovne	%rsi, %rax
	leaq	2(%rsi,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L449
	leaq	0(,%r14,8), %rdi
	movq	%rdx, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-88(%rbp), %rdx
	movq	-64(%rbp), %rcx
	movq	%rax, %rsi
	movq	-72(%rbp), %r9
	movq	-80(%rbp), %r10
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%rdx, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r11
	movq	224(%r12), %rax
	movq	192(%r12), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L410
	movq	%r11, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	%rax, %r11
.L410:
	movq	152(%r12), %rdi
	movq	%r11, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	-88(%rbp), %r11
	movq	%r14, 160(%r12)
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	%rax, 152(%r12)
	movq	-64(%rbp), %rcx
	jmp	.L408
.L407:
	cmpq	%r14, %r8
	je	.L408
	leaq	8(%r10), %rdi
	movq	%r8, %rsi
	movq	%r9, -80(%rbp)
	subq	%rdx, %rdi
	movq	%rcx, -72(%rbp)
	addq	%r11, %rdi
	movq	%r10, -64(%rbp)
	movq	%r11, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r11
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r9
	jmp	.L408
.L449:
	call	_ZSt17__throw_bad_allocv@PLT
.L446:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8584:
	.size	_ZN4node9inspector11InspectorIoD2Ev, .-_ZN4node9inspector11InspectorIoD2Ev
	.globl	_ZN4node9inspector11InspectorIoD1Ev
	.set	_ZN4node9inspector11InspectorIoD1Ev,_ZN4node9inspector11InspectorIoD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector11InspectorIo5StartESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE
	.type	_ZN4node9inspector11InspectorIo5StartESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE, @function
_ZN4node9inspector11InspectorIo5StartESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE:
.LFB8567:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%r8, %rbx
	subq	$48, %rsp
	movdqu	(%rsi), %xmm0
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	8(%rsi), %rax
	movaps	%xmm0, -80(%rbp)
	testq	%rax, %rax
	je	.L451
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L452
	lock addl	$1, 8(%rax)
.L451:
	movdqu	(%rcx), %xmm1
	movq	8(%rcx), %rax
	movaps	%xmm1, -64(%rbp)
	testq	%rax, %rax
	je	.L453
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L454
	lock addl	$1, 8(%rax)
.L453:
	movl	$216, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	leaq	-64(%rbp), %rcx
	leaq	-80(%rbp), %rsi
	movq	%rbx, %r8
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node9inspector11InspectorIoC1ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE
	movq	-56(%rbp), %r14
	testq	%r14, %r14
	je	.L456
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L457
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L483
	.p2align 4,,10
	.p2align 3
.L456:
	movq	-72(%rbp), %r14
	testq	%r14, %r14
	je	.L467
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L463
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L484
	.p2align 4,,10
	.p2align 3
.L467:
	movq	16(%r12), %rbx
	leaq	8(%rbx), %r14
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	(%rbx), %rbx
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	testq	%rbx, %rbx
	je	.L485
	movq	%r12, 0(%r13)
.L450:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L486
	addq	$48, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L485:
	.cfi_restore_state
	movq	$0, 0(%r13)
	movq	%r12, %rdi
	call	_ZN4node9inspector11InspectorIoD1Ev
	movl	$216, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L452:
	addl	$1, 8(%rax)
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L454:
	addl	$1, 8(%rax)
	jmp	.L453
	.p2align 4,,10
	.p2align 3
.L457:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L456
.L483:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L460
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L461:
	cmpl	$1, %eax
	jne	.L456
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L463:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L467
.L484:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L465
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L466:
	cmpl	$1, %eax
	jne	.L467
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L465:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L460:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L461
.L486:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8567:
	.size	_ZN4node9inspector11InspectorIo5StartESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE, .-_ZN4node9inspector11InspectorIo5StartESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector11InspectorIo27StopAcceptingNewConnectionsEv
	.type	_ZN4node9inspector11InspectorIo27StopAcceptingNewConnectionsEv, @function
_ZN4node9inspector11InspectorIo27StopAcceptingNewConnectionsEv:
.LFB8586:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	leaq	8(%rbx), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L488
	leaq	232(%rbx), %r15
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	216(%rbx), %rax
	movq	200(%rbx), %r12
	movq	168(%rbx), %rcx
	subq	$16, %rax
	cmpq	%rax, %r12
	je	.L489
	leaq	16(%r12), %rax
	movq	$2, (%r12)
	movq	$0, 8(%r12)
	movq	%rax, 200(%rbx)
.L490:
	cmpq	%rcx, %r12
	je	.L504
.L498:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
.L488:
	addq	$40, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	movq	224(%rbx), %r14
	movq	192(%rbx), %r11
	movq	%r12, %rdx
	subq	208(%rbx), %rdx
	movq	%r14, %r9
	movq	%rdx, %rsi
	subq	%r11, %r9
	sarq	$4, %rsi
	movq	%r9, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$5, %rax
	leaq	(%rsi,%rax), %rdx
	movq	184(%rbx), %rax
	subq	%rcx, %rax
	sarq	$4, %rax
	addq	%rdx, %rax
	movabsq	$576460752303423487, %rdx
	cmpq	%rdx, %rax
	je	.L505
	movq	152(%rbx), %r10
	movq	160(%rbx), %rdx
	movq	%r14, %rax
	subq	%r10, %rax
	movq	%rdx, %rsi
	sarq	$3, %rax
	subq	%rax, %rsi
	cmpq	$1, %rsi
	jbe	.L506
.L492:
	movl	$512, %edi
	movq	%rcx, -56(%rbp)
	call	_Znwm@PLT
	movq	-56(%rbp), %rcx
	movq	%rax, 8(%r14)
	movq	200(%rbx), %rax
	movq	$2, (%rax)
	movq	224(%rbx), %rdx
	movq	$0, 8(%rax)
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 200(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 216(%rbx)
	jmp	.L490
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	16(%rbx), %rdi
	call	uv_async_send@PLT
	testl	%eax, %eax
	jne	.L507
	leaq	272(%rbx), %rdi
	call	uv_cond_broadcast@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L506:
	leaq	2(%rdi), %rsi
	leaq	(%rsi,%rsi), %rax
	cmpq	%rax, %rdx
	jbe	.L493
	subq	%rsi, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r10,%rdx,8), %r10
	movq	%r14, %rdx
	subq	%r11, %rdx
	cmpq	%r10, %r11
	jbe	.L494
	cmpq	%r11, %r14
	je	.L495
	movq	%r10, %rdi
	movq	%r11, %rsi
	movq	%r9, -64(%rbp)
	movq	%rcx, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r9
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L495:
	movq	(%r10), %rax
	movq	(%r10), %xmm0
	leaq	(%r10,%r9), %r14
	movq	%r10, 192(%rbx)
	movq	%r14, 224(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 176(%rbx)
	movq	(%r14), %rax
	movq	%rax, 208(%rbx)
	addq	$512, %rax
	movq	%rax, 216(%rbx)
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData4PostEiNS1_15TransportActionESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS6_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L493:
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %r14
	ja	.L508
	leaq	0(,%r14,8), %rdi
	movq	%rsi, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_Znwm@PLT
	movq	-80(%rbp), %rsi
	movq	-72(%rbp), %r9
	movq	%rax, %rcx
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%rsi, %rax
	movq	192(%rbx), %rsi
	shrq	%rax
	leaq	(%rcx,%rax,8), %r10
	movq	224(%rbx), %rax
	movq	-64(%rbp), %rcx
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L497
	movq	%r10, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	%rax, %r10
.L497:
	movq	152(%rbx), %rdi
	movq	%r10, -80(%rbp)
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 160(%rbx)
	movq	-80(%rbp), %r10
	movq	-72(%rbp), %r9
	movq	%rax, 152(%rbx)
	movq	-64(%rbp), %rcx
	jmp	.L495
.L494:
	cmpq	%r11, %r14
	je	.L495
	leaq	8(%r9), %rdi
	movq	%r11, %rsi
	movq	%rcx, -72(%rbp)
	subq	%rdx, %rdi
	movq	%r9, -64(%rbp)
	addq	%r10, %rdi
	movq	%r10, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r9
	movq	-72(%rbp), %rcx
	jmp	.L495
.L508:
	call	_ZSt17__throw_bad_allocv@PLT
.L505:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8586:
	.size	_ZN4node9inspector11InspectorIo27StopAcceptingNewConnectionsEv, .-_ZN4node9inspector11InspectorIo27StopAcceptingNewConnectionsEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector11InspectorIo10ThreadMainEv
	.type	_ZN4node9inspector11InspectorIo10ThreadMainEv, @function
_ZN4node9inspector11InspectorIo10ThreadMainEv:
.LFB8588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-912(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	subq	$1464, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	$0, -912(%rbp)
	call	uv_loop_init@PLT
	testl	%eax, %eax
	jne	.L707
	movabsq	$4294967297, %r14
	movl	$320, %edi
	call	_Znwm@PLT
	movl	$64, %edi
	movq	$0, (%rax)
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	%rax, %r15
	movq	%r14, 8(%rax)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r15)
	leaq	24(%r15), %rdi
	leaq	16(%r15), %rax
	movq	%rbx, 16(%r15)
	movq	%rax, -1448(%rbp)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L512
	movq	-1448(%rbp), %rax
	movq	%r15, 8(%rbx)
	pxor	%xmm0, %xmm0
	leaq	152(%rbx), %rdi
	movq	$0, 160(%rbx)
	movq	%rax, (%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 168(%rbx)
	movups	%xmm0, 184(%rbx)
	movups	%xmm0, 200(%rbx)
	movups	%xmm0, 216(%rbx)
	call	_ZNSt11_Deque_baseIN4node9inspector12_GLOBAL__N_115RequestToServerESaIS3_EE17_M_initialize_mapEm.constprop.0
	leaq	232(%rbx), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L512
	leaq	272(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L708
	leaq	16(%rbx), %rsi
	leaq	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sENUlP10uv_async_sE_4_FUNES6_(%rip), %rdx
	movq	%r13, %rdi
	call	uv_async_init@PLT
	testl	%eax, %eax
	jne	.L709
	movl	$32, %edi
	call	_Znwm@PLT
	movb	$0, -1408(%rbp)
	cmpq	$0, 160(%r12)
	movq	%r14, 8(%rax)
	movq	%rax, %r15
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r15)
	leaq	_ZN4node9inspector12_GLOBAL__N_116RequestQueueData12CloseAndFreeEPS2_(%rip), %rax
	movq	%rax, 16(%r15)
	leaq	-1408(%rbp), %rax
	movq	%rbx, 24(%r15)
	movq	%rax, -1472(%rbp)
	movq	%rax, -1424(%rbp)
	movq	$0, -1416(%rbp)
	jne	.L710
.L515:
	leaq	8(%r15), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -1496(%rbp)
	je	.L711
	leaq	8(%r15), %rax
	lock addl	$1, (%rax)
.L529:
	movq	8(%r12), %r8
	movq	(%r12), %r14
	testq	%r8, %r8
	je	.L530
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L531
	lock addl	$1, 8(%r8)
.L530:
	movl	$192, %edi
	movq	%r8, -1448(%rbp)
	call	_Znwm@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-1448(%rbp), %r8
	movq	%rax, %rcx
	leaq	16+_ZTVN4node9inspector19InspectorIoDelegateE(%rip), %rax
	movq	%rax, (%rcx)
	movq	%rbx, 8(%rcx)
	movq	%r15, 16(%rcx)
	je	.L712
	leaq	8(%r15), %rax
	lock addl	$1, (%rax)
.L532:
	movq	%r14, %xmm0
	movq	%r8, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rcx)
	testq	%r8, %r8
	je	.L533
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L534
	lock addl	$1, 8(%r8)
.L533:
	movq	152(%r12), %r9
	leaq	88(%rcx), %rax
	leaq	112(%rcx), %rdi
	movq	160(%r12), %r14
	movq	%rax, 40(%rcx)
	movq	%r9, %rax
	movq	$1, 48(%rcx)
	addq	%r14, %rax
	movq	$0, 56(%rcx)
	movq	$0, 64(%rcx)
	movl	$0x3f800000, 72(%rcx)
	movq	$0, 80(%rcx)
	movq	$0, 88(%rcx)
	movq	%rdi, 96(%rcx)
	je	.L535
	testq	%r9, %r9
	je	.L540
.L535:
	movq	%r14, -1432(%rbp)
	cmpq	$15, %r14
	ja	.L713
	cmpq	$1, %r14
	jne	.L538
	movzbl	(%r9), %eax
	movb	%al, 112(%rcx)
.L539:
	movq	%r14, 104(%rcx)
	movb	$0, (%rdi,%r14)
	movq	-1424(%rbp), %r9
	leaq	144(%rcx), %rdi
	movq	-1416(%rbp), %r14
	movq	%rdi, 128(%rcx)
	movq	%r9, %rax
	addq	%r14, %rax
	je	.L638
	testq	%r9, %r9
	je	.L540
.L638:
	movq	%r14, -1432(%rbp)
	cmpq	$15, %r14
	ja	.L714
	cmpq	$1, %r14
	jne	.L544
	movzbl	(%r9), %eax
	movb	%al, 144(%rcx)
.L545:
	movq	%r14, 136(%rcx)
	movb	$0, (%rdi,%r14)
	movq	184(%r12), %r9
	leaq	176(%rcx), %rdi
	movq	192(%r12), %r14
	movq	%rdi, 160(%rcx)
	movq	%r9, %rax
	addq	%r14, %rax
	je	.L639
	testq	%r9, %r9
	je	.L540
.L639:
	movq	%r14, -1432(%rbp)
	cmpq	$15, %r14
	ja	.L715
	cmpq	$1, %r14
	jne	.L549
	movzbl	(%r9), %eax
	movb	%al, 176(%rcx)
.L550:
	movq	%r14, 168(%rcx)
	movb	$0, (%rdi,%r14)
	testq	%r8, %r8
	je	.L556
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L552
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r8)
	cmpl	$1, %eax
	je	.L716
	.p2align 4,,10
	.p2align 3
.L556:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L717
	leaq	8(%r15), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, (%rsi)
	cmpl	$1, %eax
	je	.L718
.L559:
	leaq	-1392(%rbp), %rax
	movq	40(%r12), %r14
	movq	$0, -1384(%rbp)
	movq	%rax, -1456(%rbp)
	leaq	-1376(%rbp), %rax
	movq	%rax, -1464(%rbp)
	movq	%rax, -1392(%rbp)
	movb	$0, -1376(%rbp)
	testq	%r14, %r14
	je	.L563
	leaq	8(%r14), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -1504(%rbp)
	je	.L564
	lock addl	$1, (%rax)
.L565:
	movq	32(%r12), %r10
	movq	%rcx, -1488(%rbp)
	movq	%r10, %rdi
	movq	%r10, -1480(%rbp)
	call	uv_mutex_lock@PLT
	movq	32(%r12), %rax
	movq	-1456(%rbp), %rdi
	leaq	40(%rax), %rsi
	movq	%rax, -1448(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-1448(%rbp), %rax
	movq	-1480(%rbp), %r10
	movq	-1488(%rbp), %rcx
	movl	72(%rax), %r8d
	testl	%r8d, %r8d
	js	.L633
	movq	%r10, %rdi
	movq	%rcx, -1480(%rbp)
	movl	%r8d, -1448(%rbp)
	call	uv_mutex_unlock@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movl	-1448(%rbp), %r8d
	movq	-1480(%rbp), %rcx
	je	.L719
	movq	-1504(%rbp), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, (%rsi)
.L566:
	cmpl	$1, %eax
	je	.L720
.L567:
	subq	$8, %rsp
	pushq	stderr(%rip)
	leaq	-1360(%rbp), %rax
	movq	%r13, %rdx
	movq	%rcx, -1432(%rbp)
	movq	%rax, %rdi
	leaq	48(%r12), %r9
	leaq	-1432(%rbp), %rsi
	movq	-1456(%rbp), %rcx
	movq	%rax, -1448(%rbp)
	call	_ZN4node9inspector21InspectorSocketServerC1ESt10unique_ptrINS0_20SocketServerDelegateESt14default_deleteIS3_EEP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiRKNS_17InspectPublishUidEP8_IO_FILE@PLT
	movq	-1432(%rbp), %r14
	popq	%rax
	popq	%rdx
	testq	%r14, %r14
	je	.L570
	movq	(%r14), %rax
	leaq	_ZN4node9inspector19InspectorIoDelegateD0Ev(%rip), %rdx
	movq	64(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L571
	leaq	16+_ZTVN4node9inspector19InspectorIoDelegateE(%rip), %rax
	movq	160(%r14), %rdi
	movq	%rax, (%r14)
	leaq	176(%r14), %rax
	cmpq	%rax, %rdi
	je	.L572
	call	_ZdlPv@PLT
.L572:
	movq	128(%r14), %rdi
	leaq	144(%r14), %rax
	cmpq	%rax, %rdi
	je	.L573
	call	_ZdlPv@PLT
.L573:
	movq	96(%r14), %rdi
	leaq	112(%r14), %rax
	cmpq	%rax, %rdi
	je	.L574
	call	_ZdlPv@PLT
.L574:
	movq	56(%r14), %r9
	testq	%r9, %r9
	je	.L579
	movq	%r12, -1480(%rbp)
	movq	%r9, %r12
	movq	%rbx, -1456(%rbp)
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L721:
	movq	(%rdi), %rdx
	call	*8(%rdx)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L705
.L580:
	movq	%rbx, %r12
.L575:
	movq	16(%r12), %rdi
	movq	(%r12), %rbx
	testq	%rdi, %rdi
	jne	.L721
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L580
.L705:
	movq	-1456(%rbp), %rbx
	movq	-1480(%rbp), %r12
.L579:
	movq	48(%r14), %rax
	movq	40(%r14), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	40(%r14), %rdi
	leaq	88(%r14), %rax
	movq	$0, 64(%r14)
	movq	$0, 56(%r14)
	cmpq	%rax, %rdi
	je	.L576
	call	_ZdlPv@PLT
.L576:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L582
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L583
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L584:
	cmpl	$1, %eax
	je	.L722
.L582:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L589
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L590
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L591:
	cmpl	$1, %eax
	je	.L723
.L589:
	movl	$192, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L570:
	movq	8(%rbx), %rax
	movq	(%rbx), %xmm0
	testq	%rax, %rax
	je	.L595
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L596
	lock addl	$1, 8(%rax)
.L595:
	movq	24(%r12), %rbx
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 16(%r12)
	testq	%rbx, %rbx
	je	.L602
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L598
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rbx)
.L599:
	cmpl	$1, %eax
	je	.L724
.L602:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L725
	movq	-1496(%rbp), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, (%rsi)
.L603:
	cmpl	$1, %eax
	je	.L726
.L605:
	leaq	64(%r12), %r15
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	movq	-1448(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServer5StartEv@PLT
	testb	%al, %al
	jne	.L727
.L609:
	leaq	104(%r12), %rdi
	call	uv_cond_broadcast@PLT
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	uv_run@PLT
	movq	%r13, %rdi
	call	_ZN4node18CheckedUvLoopCloseEP9uv_loop_s@PLT
	movq	-1448(%rbp), %rdi
	call	_ZN4node9inspector21InspectorSocketServerD1Ev@PLT
	movq	-1392(%rbp), %rdi
	cmpq	-1464(%rbp), %rdi
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	-1424(%rbp), %rdi
	cmpq	-1472(%rbp), %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L728
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L710:
	.cfi_restore_state
	leaq	-1360(%rbp), %rax
	movq	152(%r12), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -1448(%rbp)
	movq	$0, -1264(%rbp)
	call	uv_fs_realpath@PLT
	testl	%eax, %eax
	jne	.L516
	movq	-1264(%rbp), %r14
	testq	%r14, %r14
	je	.L729
	leaq	-1392(%rbp), %rax
	movq	%r14, %rdi
	movq	%rax, -1456(%rbp)
	leaq	-1376(%rbp), %rax
	movq	%rax, -1464(%rbp)
	movq	%rax, -1392(%rbp)
	call	strlen@PLT
	movq	%rax, -1432(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L730
	cmpq	$1, %rax
	jne	.L520
	movzbl	(%r14), %edx
	movb	%dl, -1376(%rbp)
	movq	-1464(%rbp), %rdx
.L521:
	movq	%rax, -1384(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-1424(%rbp), %rdi
	movq	-1392(%rbp), %rdx
	cmpq	-1464(%rbp), %rdx
	je	.L731
	movq	-1384(%rbp), %rax
	movq	-1376(%rbp), %rcx
	cmpq	-1472(%rbp), %rdi
	je	.L732
	movq	%rax, %xmm0
	movq	%rcx, %xmm3
	movq	-1408(%rbp), %rsi
	movq	%rdx, -1424(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, -1416(%rbp)
	testq	%rdi, %rdi
	je	.L527
	movq	%rdi, -1392(%rbp)
	movq	%rsi, -1376(%rbp)
.L525:
	movq	$0, -1384(%rbp)
	movb	$0, (%rdi)
	movq	-1392(%rbp), %rdi
	cmpq	-1464(%rbp), %rdi
	je	.L516
	call	_ZdlPv@PLT
.L516:
	movq	-1448(%rbp), %rdi
	call	uv_fs_req_cleanup@PLT
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L538:
	testq	%r14, %r14
	je	.L539
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L544:
	testq	%r14, %r14
	je	.L545
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L549:
	testq	%r14, %r14
	je	.L550
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L713:
	leaq	96(%rcx), %rdi
	leaq	-1432(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -1464(%rbp)
	movq	%r9, -1456(%rbp)
	movq	%rcx, -1448(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1448(%rbp), %rcx
	movq	-1456(%rbp), %r9
	movq	%rax, %rdi
	movq	-1464(%rbp), %r8
	movq	%rax, 96(%rcx)
	movq	-1432(%rbp), %rax
	movq	%rax, 112(%rcx)
.L537:
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%rcx, -1456(%rbp)
	movq	%r8, -1448(%rbp)
	call	memcpy@PLT
	movq	-1456(%rbp), %rcx
	movq	-1432(%rbp), %r14
	movq	-1448(%rbp), %r8
	movq	96(%rcx), %rdi
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L715:
	leaq	160(%rcx), %rdi
	leaq	-1432(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -1464(%rbp)
	movq	%r9, -1456(%rbp)
	movq	%rcx, -1448(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1448(%rbp), %rcx
	movq	-1456(%rbp), %r9
	movq	%rax, %rdi
	movq	-1464(%rbp), %r8
	movq	%rax, 160(%rcx)
	movq	-1432(%rbp), %rax
	movq	%rax, 176(%rcx)
.L548:
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%rcx, -1456(%rbp)
	movq	%r8, -1448(%rbp)
	call	memcpy@PLT
	movq	-1456(%rbp), %rcx
	movq	-1432(%rbp), %r14
	movq	-1448(%rbp), %r8
	movq	160(%rcx), %rdi
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	128(%rcx), %rdi
	leaq	-1432(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -1464(%rbp)
	movq	%r9, -1456(%rbp)
	movq	%rcx, -1448(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1448(%rbp), %rcx
	movq	-1456(%rbp), %r9
	movq	%rax, %rdi
	movq	-1464(%rbp), %r8
	movq	%rax, 128(%rcx)
	movq	-1432(%rbp), %rax
	movq	%rax, 144(%rcx)
.L543:
	movq	%r14, %rdx
	movq	%r9, %rsi
	movq	%rcx, -1456(%rbp)
	movq	%r8, -1448(%rbp)
	call	memcpy@PLT
	movq	-1456(%rbp), %rcx
	movq	-1432(%rbp), %r14
	movq	-1448(%rbp), %r8
	movq	128(%rcx), %rdi
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L534:
	addl	$1, 8(%r8)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L712:
	addl	$1, 8(%r15)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L531:
	addl	$1, 8(%r8)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L711:
	addl	$1, 8(%r15)
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L564:
	addl	$1, 8(%r14)
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L563:
	movq	32(%r12), %r9
	movq	%rcx, -1480(%rbp)
	movq	%r9, %rdi
	movq	%r9, -1448(%rbp)
	call	uv_mutex_lock@PLT
	movq	32(%r12), %r14
	movq	-1456(%rbp), %rdi
	leaq	40(%r14), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movl	72(%r14), %r8d
	movq	-1448(%rbp), %r9
	movq	-1480(%rbp), %rcx
	testl	%r8d, %r8d
	js	.L633
	movq	%r9, %rdi
	movq	%rcx, -1480(%rbp)
	movl	%r8d, -1448(%rbp)
	call	uv_mutex_unlock@PLT
	movl	-1448(%rbp), %r8d
	movq	-1480(%rbp), %rcx
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L727:
	movq	40(%r12), %rbx
	testq	%rbx, %rbx
	je	.L610
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	leaq	8(%rbx), %rdx
	je	.L611
	lock addl	$1, (%rdx)
.L612:
	movq	32(%r12), %r8
	movq	%rdx, -1488(%rbp)
	movq	%r8, %rdi
	movq	%r8, -1480(%rbp)
	call	uv_mutex_lock@PLT
	movq	32(%r12), %rcx
	movq	-1448(%rbp), %rdi
	movq	%rcx, -1456(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	movq	-1456(%rbp), %rcx
	movq	-1480(%rbp), %r8
	movl	%eax, 72(%rcx)
	movq	%r8, %rdi
	call	uv_mutex_unlock@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-1488(%rbp), %rdx
	je	.L733
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
.L613:
	cmpl	$1, %eax
	jne	.L609
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L616
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L617:
	cmpl	$1, %eax
	jne	.L609
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L512:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L596:
	addl	$1, 8(%rax)
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L717:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L559
.L718:
	movq	(%r15), %rax
	movq	%rcx, -1448(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-1448(%rbp), %rcx
	je	.L560
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L561:
	cmpl	$1, %eax
	jne	.L559
	movq	(%r15), %rax
	movq	%rcx, -1448(%rbp)
	movq	%r15, %rdi
	call	*24(%rax)
	movq	-1448(%rbp), %rcx
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L552:
	movl	8(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r8)
	cmpl	$1, %eax
	jne	.L556
.L716:
	movq	(%r8), %rax
	movq	%rcx, -1456(%rbp)
	movq	%r8, %rdi
	movq	%r8, -1448(%rbp)
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-1448(%rbp), %r8
	movq	-1456(%rbp), %rcx
	je	.L554
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r8)
.L555:
	cmpl	$1, %eax
	jne	.L556
	movq	(%r8), %rax
	movq	%rcx, -1448(%rbp)
	movq	%r8, %rdi
	call	*24(%rax)
	movq	-1448(%rbp), %rcx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L725:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L719:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L598:
	movl	8(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rbx)
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	_ZZNK4node8HostPort4portEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L707:
	leaq	_ZZN4node9inspector11InspectorIo10ThreadMainEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L724:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L600
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L601:
	cmpl	$1, %eax
	jne	.L602
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*24(%rax)
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L726:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L606
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L607:
	cmpl	$1, %eax
	jne	.L605
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L720:
	movq	(%r14), %rax
	movq	%rcx, -1480(%rbp)
	movq	%r14, %rdi
	movl	%r8d, -1448(%rbp)
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movl	-1448(%rbp), %r8d
	movq	-1480(%rbp), %rcx
	je	.L568
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L569:
	cmpl	$1, %eax
	jne	.L567
	movq	(%r14), %rax
	movq	%rcx, -1480(%rbp)
	movq	%r14, %rdi
	movl	%r8d, -1448(%rbp)
	call	*24(%rax)
	movq	-1480(%rbp), %rcx
	movl	-1448(%rbp), %r8d
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L708:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L571:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L583:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L590:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L709:
	leaq	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L611:
	addl	$1, 8(%rbx)
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L722:
	movq	(%rdi), %rax
	movq	%rdi, -1456(%rbp)
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-1456(%rbp), %rdi
	je	.L586
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L587:
	cmpl	$1, %eax
	jne	.L582
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L723:
	movq	(%rdi), %rax
	movq	%rdi, -1456(%rbp)
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	-1456(%rbp), %rdi
	je	.L593
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L594:
	cmpl	$1, %eax
	jne	.L589
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L610:
	movq	32(%r12), %r14
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	32(%r12), %rdx
	movq	-1448(%rbp), %rdi
	movq	%rdx, -1456(%rbp)
	call	_ZNK4node9inspector21InspectorSocketServer4PortEv@PLT
	movq	-1456(%rbp), %rdx
	movq	%r14, %rdi
	movl	%eax, 72(%rdx)
	call	uv_mutex_unlock@PLT
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L733:
	movl	8(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rbx)
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L520:
	testq	%rax, %rax
	jne	.L734
	movq	-1464(%rbp), %rdx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L586:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L593:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L554:
	movl	12(%r8), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r8)
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L560:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L606:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L568:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L600:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L730:
	movq	-1456(%rbp), %rdi
	leaq	-1432(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -1480(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1480(%rbp), %r8
	movq	%rax, -1392(%rbp)
	movq	%rax, %rdi
	movq	-1432(%rbp), %rax
	movq	%rax, -1376(%rbp)
.L519:
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-1432(%rbp), %rax
	movq	-1392(%rbp), %rdx
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L731:
	movq	-1384(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L523
	cmpq	$1, %rdx
	je	.L735
	movq	-1464(%rbp), %rsi
	call	memcpy@PLT
	movq	-1384(%rbp), %rdx
	movq	-1424(%rbp), %rdi
.L523:
	movq	%rdx, -1416(%rbp)
	movb	$0, (%rdi,%rdx)
	movq	-1392(%rbp), %rdi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L732:
	movq	%rax, %xmm0
	movq	%rcx, %xmm4
	movq	%rdx, -1424(%rbp)
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, -1416(%rbp)
.L527:
	movq	-1464(%rbp), %rax
	movq	%rax, -1392(%rbp)
	leaq	-1376(%rbp), %rax
	movq	%rax, -1464(%rbp)
	movq	%rax, %rdi
	jmp	.L525
	.p2align 4,,10
	.p2align 3
.L616:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L729:
	leaq	_ZZN4node9inspector12_GLOBAL__N_110ScriptPathEP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L735:
	movzbl	-1376(%rbp), %eax
	movb	%al, (%rdi)
	movq	-1384(%rbp), %rdx
	movq	-1424(%rbp), %rdi
	jmp	.L523
.L540:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L728:
	call	__stack_chk_fail@PLT
.L734:
	movq	-1464(%rbp), %rdi
	jmp	.L519
	.cfi_endproc
.LFE8588:
	.size	_ZN4node9inspector11InspectorIo10ThreadMainEv, .-_ZN4node9inspector11InspectorIo10ThreadMainEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector11InspectorIo10ThreadMainEPv
	.type	_ZN4node9inspector11InspectorIo10ThreadMainEPv, @function
_ZN4node9inspector11InspectorIo10ThreadMainEPv:
.LFB8587:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9inspector11InspectorIo10ThreadMainEv
	.cfi_endproc
.LFE8587:
	.size	_ZN4node9inspector11InspectorIo10ThreadMainEPv, .-_ZN4node9inspector11InspectorIo10ThreadMainEPv
	.align 2
	.p2align 4
	.globl	_ZNK4node9inspector11InspectorIo8GetWsUrlB5cxx11Ev
	.type	_ZNK4node9inspector11InspectorIo8GetWsUrlB5cxx11Ev, @function
_ZNK4node9inspector11InspectorIo8GetWsUrlB5cxx11Ev:
.LFB8608:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	184(%rsi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	40(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L738
	leaq	8(%r13), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -72(%rbp)
	je	.L739
	lock addl	$1, (%rax)
.L740:
	movq	32(%rbx), %r14
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	32(%rbx), %rsi
	movl	72(%rsi), %edx
	testl	%edx, %edx
	js	.L749
	addq	$40, %rsi
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN4node9inspector15FormatWsAddressERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiS8_b@PLT
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L757
	movq	-72(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L758
.L737:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L759
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	.cfi_restore_state
	addl	$1, 8(%r13)
	jmp	.L740
	.p2align 4,,10
	.p2align 3
.L738:
	movq	32(%rsi), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	32(%rbx), %rax
	movl	72(%rax), %edx
	leaq	40(%rax), %rsi
	testl	%edx, %edx
	js	.L749
	movq	%r12, %rdi
	movl	$1, %r8d
	movq	%r15, %rcx
	call	_ZN4node9inspector15FormatWsAddressERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEiS8_b@PLT
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L757:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L737
.L758:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L744
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L745:
	cmpl	$1, %eax
	jne	.L737
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L749:
	leaq	_ZZNK4node8HostPort4portEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L744:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L745
.L759:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8608:
	.size	_ZNK4node9inspector11InspectorIo8GetWsUrlB5cxx11Ev, .-_ZNK4node9inspector11InspectorIo8GetWsUrlB5cxx11Ev
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB10589:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L761
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L771
.L787:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L772:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L761:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L785
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L786
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L764:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L766
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L768:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L769:
	testq	%rsi, %rsi
	je	.L766
.L767:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L768
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L774
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L767
	.p2align 4,,10
	.p2align 3
.L766:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L770
	call	_ZdlPv@PLT
.L770:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L787
.L771:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L773
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L773:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%rdx, %rdi
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L785:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L764
.L786:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10589:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.rodata.str1.1
.LC6:
	.string	"Debugger attached.\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19InspectorIoDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector19InspectorIoDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector19InspectorIoDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	24(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %r12
	movq	(%rax), %r14
	testq	%r12, %r12
	je	.L789
	leaq	8(%r12), %rax
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rax, -88(%rbp)
	je	.L790
	lock addl	$1, (%rax)
.L791:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector17IoSessionDelegateE(%rip), %rsi
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	movq	%rsi, (%rax)
	movq	%r14, 8(%rax)
	movq	%r12, 16(%rax)
	je	.L831
	movq	-88(%rbp), %rdx
	lock addl	$1, (%rdx)
.L792:
	movl	%r13d, 24(%rax)
	leaq	-72(%rbp), %rdi
	leaq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movl	$1, %ecx
	movq	%rax, -64(%rbp)
	call	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L808
	movq	(%rdi), %rax
	call	*8(%rax)
.L808:
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L793
	movq	-88(%rbp), %rdx
	movl	$-1, %eax
	lock xaddl	%eax, (%rdx)
	cmpl	$1, %eax
	je	.L832
.L796:
	movq	-72(%rbp), %r8
	testq	%r8, %r8
	je	.L788
	movq	48(%rbx), %rsi
	movslq	%r13d, %r14
	xorl	%edx, %edx
	movq	%r14, %rax
	divq	%rsi
	movq	40(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r12
	testq	%rax, %rax
	je	.L802
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L833:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L802
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r12
	jne	.L802
.L804:
	cmpl	%edi, %r13d
	jne	.L833
	addq	$16, %rcx
.L807:
	movq	$0, -72(%rbp)
	movq	(%rcx), %rdi
	movq	%r8, (%rcx)
	testq	%rdi, %rdi
	je	.L805
	movq	(%rdi), %rax
	call	*8(%rax)
.L805:
	movq	stderr(%rip), %rcx
	movl	$19, %edx
	movl	$1, %esi
	leaq	.LC6(%rip), %rdi
	call	fwrite@PLT
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L788
	movq	(%rdi), %rax
	call	*8(%rax)
.L788:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L834
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L790:
	.cfi_restore_state
	addl	$1, 8(%r12)
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L831:
	addl	$1, 8(%r12)
	jmp	.L792
	.p2align 4,,10
	.p2align 3
.L789:
	movl	$32, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector17IoSessionDelegateE(%rip), %rsi
	leaq	-72(%rbp), %rdi
	movl	$1, %ecx
	movq	%rsi, (%rax)
	leaq	-64(%rbp), %rdx
	movq	%r15, %rsi
	movq	%r14, 8(%rax)
	movq	$0, 16(%rax)
	movl	%r13d, 24(%rax)
	movq	%rax, -64(%rbp)
	call	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L796
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L793:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L796
.L832:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L797
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L798:
	cmpl	$1, %eax
	jne	.L796
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L802:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	40(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movl	%r13d, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	movq	-72(%rbp), %r8
	leaq	16(%rax), %rcx
	jmp	.L807
	.p2align 4,,10
	.p2align 3
.L797:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L798
.L834:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8640:
	.size	_ZN4node9inspector19InspectorIoDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector19InspectorIoDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.weak	_ZTVN4node9inspector17IoSessionDelegateE
	.section	.data.rel.ro.local._ZTVN4node9inspector17IoSessionDelegateE,"awG",@progbits,_ZTVN4node9inspector17IoSessionDelegateE,comdat
	.align 8
	.type	_ZTVN4node9inspector17IoSessionDelegateE, @object
	.size	_ZTVN4node9inspector17IoSessionDelegateE, 40
_ZTVN4node9inspector17IoSessionDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector17IoSessionDelegateD1Ev
	.quad	_ZN4node9inspector17IoSessionDelegateD0Ev
	.quad	_ZN4node9inspector17IoSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.weak	_ZTVN4node9inspector19InspectorIoDelegateE
	.section	.data.rel.ro.local._ZTVN4node9inspector19InspectorIoDelegateE,"awG",@progbits,_ZTVN4node9inspector19InspectorIoDelegateE,comdat
	.align 8
	.type	_ZTVN4node9inspector19InspectorIoDelegateE, @object
	.size	_ZTVN4node9inspector19InspectorIoDelegateE, 88
_ZTVN4node9inspector19InspectorIoDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector19InspectorIoDelegate12AssignServerEPNS0_21InspectorSocketServerE
	.quad	_ZN4node9inspector19InspectorIoDelegate12StartSessionEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector19InspectorIoDelegate10EndSessionEi
	.quad	_ZN4node9inspector19InspectorIoDelegate15MessageReceivedEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector19InspectorIoDelegate12GetTargetIdsB5cxx11Ev
	.quad	_ZN4node9inspector19InspectorIoDelegate14GetTargetTitleERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector19InspectorIoDelegate12GetTargetUrlERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.quad	_ZN4node9inspector19InspectorIoDelegateD1Ev
	.quad	_ZN4node9inspector19InspectorIoDelegateD0Ev
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector12_GLOBAL__N_116RequestQueueDataEPFvS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector12RequestQueueESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC7:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC9:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC10:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC12:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.section	.rodata.str1.1
.LC13:
	.string	"../src/inspector_io.cc:289"
.LC14:
	.string	"(err) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"void node::inspector::InspectorIo::ThreadMain()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector11InspectorIo10ThreadMainEvE4args, @object
	.size	_ZZN4node9inspector11InspectorIo10ThreadMainEvE4args, 24
_ZZN4node9inspector11InspectorIo10ThreadMainEvE4args:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/inspector_io.cc:273"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"node::inspector::InspectorIo::~InspectorIo()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector11InspectorIoD4EvE4args, @object
	.size	_ZZN4node9inspector11InspectorIoD4EvE4args, 24
_ZZN4node9inspector11InspectorIoD4EvE4args:
	.quad	.LC16
	.quad	.LC14
	.quad	.LC17
	.section	.rodata.str1.1
.LC18:
	.string	"../src/inspector_io.cc:266"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"(uv_thread_create(&thread_, InspectorIo::ThreadMain, this)) == (0)"
	.align 8
.LC20:
	.string	"node::inspector::InspectorIo::InspectorIo(std::shared_ptr<node::inspector::MainThreadHandle>, const string&, std::shared_ptr<node::ExclusiveAccess<node::HostPort> >, const node::InspectPublishUid&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector11InspectorIoC4ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidEE4args, @object
	.size	_ZZN4node9inspector11InspectorIoC4ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidEE4args, 24
_ZZN4node9inspector11InspectorIoC4ESt10shared_ptrINS0_16MainThreadHandleEERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_INS_15ExclusiveAccessINS_8HostPortENS_9MutexBaseINS_16LibuvMutexTraitsEEEEEERKNS_17InspectPublishUidEE4args:
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"../src/inspector_io.cc:119"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"(0) == (uv_async_send(&async_))"
	.align 8
.LC23:
	.string	"void node::inspector::{anonymous}::RequestQueueData::Post(int, node::inspector::{anonymous}::TransportAction, std::unique_ptr<v8_inspector::StringBuffer>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData4PostEiNS1_15TransportActionESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS6_EEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData4PostEiNS1_15TransportActionESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS6_EEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_116RequestQueueData4PostEiNS1_15TransportActionESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS6_EEE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.section	.rodata.str1.1
.LC24:
	.string	"../src/inspector_io.cc:107"
.LC25:
	.string	"(0) == (err)"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"node::inspector::{anonymous}::RequestQueueData::RequestQueueData(uv_loop_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sE4args, 24
_ZZN4node9inspector12_GLOBAL__N_116RequestQueueDataC4EP9uv_loop_sE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.section	.rodata.str1.1
.LC27:
	.string	"../src/inspector_io.cc:49"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"crypto::EntropySource(reinterpret_cast<unsigned char*>(buffer), sizeof(buffer))"
	.align 8
.LC29:
	.string	"std::string node::inspector::{anonymous}::GenerateID()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_110GenerateIDEvE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_110GenerateIDEvE4args, 24
_ZZN4node9inspector12_GLOBAL__N_110GenerateIDEvE4args:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/inspector_io.cc:36"
.LC31:
	.string	"(req.ptr) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"std::string node::inspector::{anonymous}::ScriptPath(uv_loop_t*, const string&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector12_GLOBAL__N_110ScriptPathEP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZN4node9inspector12_GLOBAL__N_110ScriptPathEP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZN4node9inspector12_GLOBAL__N_110ScriptPathEP9uv_loop_sRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.weak	_ZZNK4node8HostPort4portEvE4args
	.section	.rodata.str1.1
.LC33:
	.string	"../src/node_options.h:33"
.LC34:
	.string	"(port_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"int node::HostPort::port() const"
	.section	.data.rel.ro.local._ZZNK4node8HostPort4portEvE4args,"awG",@progbits,_ZZNK4node8HostPort4portEvE4args,comdat
	.align 16
	.type	_ZZNK4node8HostPort4portEvE4args, @gnu_unique_object
	.size	_ZZNK4node8HostPort4portEvE4args, 24
_ZZNK4node8HostPort4portEvE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
