	.file	"node_binding.cc"
	.text
	.section	.text._ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev,"axG",@progbits,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.type	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, @function
_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev:
.LFB7998:
	.cfi_startproc
	endbr64
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE7998:
	.size	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev, .-_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.weak	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev
	.set	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev,_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED2Ev
	.section	.text._ZN4node7binding19global_handle_map_tD2Ev,"axG",@progbits,_ZN4node7binding19global_handle_map_tD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7binding19global_handle_map_tD2Ev
	.type	_ZN4node7binding19global_handle_map_tD2Ev, @function
_ZN4node7binding19global_handle_map_tD2Ev:
.LFB9418:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	56(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L5
.L4:
	movq	48(%r12), %rax
	movq	40(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	40(%r12), %rdi
	leaq	88(%r12), %rax
	movq	$0, 64(%r12)
	movq	$0, 56(%r12)
	cmpq	%rax, %rdi
	je	.L6
	call	_ZdlPv@PLT
.L6:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.cfi_endproc
.LFE9418:
	.size	_ZN4node7binding19global_handle_map_tD2Ev, .-_ZN4node7binding19global_handle_map_tD2Ev
	.weak	_ZN4node7binding19global_handle_map_tD1Ev
	.set	_ZN4node7binding19global_handle_map_tD1Ev,_ZN4node7binding19global_handle_map_tD2Ev
	.text
	.p2align 4
	.type	_ZNSt14_Function_base13_Base_managerIZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS2_4DLibEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, @function
_ZNSt14_Function_base13_Base_managerIZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS2_4DLibEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation:
.LFB8428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	cmpl	$2, %edx
	je	.L13
	cmpl	$3, %edx
	je	.L14
	cmpl	$1, %edx
	je	.L20
.L15:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	.cfi_restore_state
	movq	(%rsi), %r12
	movl	$40, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%r12), %xmm1
	movups	%xmm1, 16(%rax)
	movq	32(%r12), %rdx
	movq	%rax, (%rbx)
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	movl	$40, %esi
	call	_ZdlPvm@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8428:
	.size	_ZNSt14_Function_base13_Base_managerIZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS2_4DLibEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation, .-_ZNSt14_Function_base13_Base_managerIZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS2_4DLibEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation
	.p2align 4
	.globl	node_module_register
	.type	node_module_register, @function
node_module_register:
.LFB7272:
	.cfi_startproc
	endbr64
	testb	$4, 4(%rdi)
	je	.L22
	movq	_ZN4nodeL16modlist_internalE(%rip), %rax
	movq	%rdi, _ZN4nodeL16modlist_internalE(%rip)
	movq	%rax, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	cmpb	$0, _ZN4node19node_is_initializedE(%rip)
	jne	.L24
	movq	_ZN4nodeL14modlist_linkedE(%rip), %rax
	movl	$2, 4(%rdi)
	movq	%rdi, _ZN4nodeL14modlist_linkedE(%rip)
	movq	%rax, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rdi, %fs:_ZN4nodeL23thread_local_modpendingE@tpoff
	ret
	.cfi_endproc
.LFE7272:
	.size	node_module_register, .-node_module_register
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7binding4DLibC2EPKci
	.type	_ZN4node7binding4DLibC2EPKci, @function
_ZN4node7binding4DLibC2EPKci:
.LFB7302:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	16(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r15, (%rdi)
	testq	%rsi, %rsi
	je	.L36
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %r13
	movl	%edx, %r14d
	call	strlen@PLT
	movq	%rax, -64(%rbp)
	movq	%rax, %r12
	cmpq	$15, %rax
	ja	.L37
	cmpq	$1, %rax
	jne	.L29
	movzbl	0(%r13), %edx
	movb	%dl, 16(%rbx)
.L30:
	movq	%rax, 8(%rbx)
	movb	$0, (%r15,%rax)
	leaq	56(%rbx), %rax
	movl	%r14d, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	$0, 48(%rbx)
	movb	$0, 56(%rbx)
	movq	$0, 72(%rbx)
	movb	$0, 80(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L30
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %r15
	movq	-64(%rbp), %rax
	movq	%rax, 16(%rbx)
.L28:
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %rax
	movq	(%rbx), %r15
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L38:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7302:
	.size	_ZN4node7binding4DLibC2EPKci, .-_ZN4node7binding4DLibC2EPKci
	.globl	_ZN4node7binding4DLibC1EPKci
	.set	_ZN4node7binding4DLibC1EPKci,_ZN4node7binding4DLibC2EPKci
	.align 2
	.p2align 4
	.globl	_ZN4node7binding4DLib4OpenEv
	.type	_ZN4node7binding4DLib4OpenEv, @function
_ZN4node7binding4DLib4OpenEv:
.LFB7304:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	32(%rdi), %esi
	movq	(%rdi), %rdi
	call	dlopen@PLT
	movl	$1, %r8d
	movq	%rax, 72(%rbx)
	testq	%rax, %rax
	je	.L44
	popq	%rbx
	movl	%r8d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
	call	dlerror@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strlen@PLT
	movq	48(%rbx), %rdx
	leaq	40(%rbx), %rdi
	movq	%r12, %rcx
	movq	%rax, %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%r12
	movl	%r8d, %eax
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7304:
	.size	_ZN4node7binding4DLib4OpenEv, .-_ZN4node7binding4DLib4OpenEv
	.align 2
	.p2align 4
	.globl	_ZN4node7binding4DLib16GetSymbolAddressEPKc
	.type	_ZN4node7binding4DLib16GetSymbolAddressEPKc, @function
_ZN4node7binding4DLib16GetSymbolAddressEPKc:
.LFB7306:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	jmp	dlsym@PLT
	.cfi_endproc
.LFE7306:
	.size	_ZN4node7binding4DLib16GetSymbolAddressEPKc, .-_ZN4node7binding4DLib16GetSymbolAddressEPKc
	.align 2
	.p2align 4
	.globl	_ZN4node7binding4DLib33GetSavedModuleFromGlobalHandleMapEv
	.type	_ZN4node7binding4DLib33GetSavedModuleFromGlobalHandleMapEv, @function
_ZN4node7binding4DLib33GetSavedModuleFromGlobalHandleMapEv:
.LFB7308:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	72(%rdi), %rbx
	movb	$1, 80(%rdi)
	testq	%rbx, %rbx
	je	.L60
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	48+_ZN4node7bindingL17global_handle_mapE(%rip), %rsi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rsi
	movq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rax
	movq	(%rax,%rdx,8), %r12
	movq	%rdx, %rdi
	testq	%r12, %r12
	je	.L48
	movq	(%r12), %r12
	movq	8(%r12), %rcx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L62:
	movq	(%r12), %r12
	testq	%r12, %r12
	je	.L48
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %rdi
	jne	.L61
.L50:
	cmpq	%rcx, %rbx
	jne	.L62
	addl	$1, 16(%r12)
	movq	24(%r12), %r12
.L48:
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	_ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7308:
	.size	_ZN4node7binding4DLib33GetSavedModuleFromGlobalHandleMapEv, .-_ZN4node7binding4DLib33GetSavedModuleFromGlobalHandleMapEv
	.section	.rodata.str1.8
	.align 8
.LC1:
	.string	"process.dlopen needs at least 2 arguments."
	.align 8
.LC2:
	.string	"flag argument must be an integer."
	.text
	.p2align 4
	.globl	_ZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1192, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L96
	cmpw	$1040, %cx
	jne	.L64
.L96:
	movq	23(%rdx), %r12
.L66:
	movq	3280(%r12), %rsi
	movq	%r12, -1200(%rbp)
	movq	%rsi, -1192(%rbp)
	cmpq	$0, %fs:_ZN4nodeL23thread_local_modpendingE@tpoff
	jne	.L120
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jle	.L121
	cmpl	$2, %eax
	jne	.L122
	movq	$0, -1184(%rbp)
	movl	$1, %r13d
	movq	$0, -1176(%rbp)
.L74:
	movq	8(%rbx), %rdi
.L75:
	movq	-1192(%rbp), %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L63
	movq	%rax, -1184(%rbp)
	movq	-1200(%rbp), %rax
	movq	-1192(%rbp), %rsi
	movq	360(%rax), %rax
	movq	688(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L63
	movq	-1192(%rbp), %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	testq	%rax, %rax
	je	.L63
	cmpl	$1, 16(%rbx)
	movq	%rax, -1176(%rbp)
	jle	.L88
	movq	8(%rbx), %rdx
	subq	$8, %rdx
.L77:
	leaq	-1200(%rbp), %rax
	leaq	-1104(%rbp), %r12
	movq	%rax, %xmm1
	leaq	-1176(%rbp), %rax
	movq	%r12, %rdi
	movq	%rax, %xmm2
	leaq	-1184(%rbp), %rax
	movq	%rax, %xmm0
	leaq	-1192(%rbp), %rax
	punpcklqdq	%xmm2, %xmm1
	movq	%rax, %xmm3
	movq	-1200(%rbp), %rax
	movaps	%xmm1, -1232(%rbp)
	punpcklqdq	%xmm3, %xmm0
	movq	352(%rax), %rsi
	movaps	%xmm0, -1216(%rbp)
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movl	$40, %edi
	movq	$0, -1120(%rbp)
	movq	-1200(%rbp), %rbx
	call	_Znwm@PLT
	movdqa	-1216(%rbp), %xmm0
	movl	$104, %edi
	movdqa	-1232(%rbp), %xmm1
	movq	%r12, 32(%rax)
	leaq	_ZNSt14_Function_base13_Base_managerIZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS3_5ValueEEEEUlPNS2_4DLibEE_E10_M_managerERSt9_Any_dataRKSD_St18_Manager_operation(%rip), %rcx
	movq	-1088(%rbp), %r14
	movups	%xmm0, 16(%rax)
	movq	%rcx, %xmm0
	movq	%rax, -1136(%rbp)
	movups	%xmm1, (%rax)
	leaq	_ZNSt17_Function_handlerIFbPN4node7binding4DLibEEZNS1_6DLOpenERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS3_E_E9_M_invokeERKSt9_Any_dataOS3_(%rip), %rax
	movq	%rax, %xmm4
	punpcklqdq	%xmm4, %xmm0
	movaps	%xmm0, -1120(%rbp)
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16(%rax), %rax
	leaq	32(%r12), %r15
	movq	%rax, -1216(%rbp)
	movq	%r15, 16(%r12)
	testq	%r14, %r14
	je	.L123
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -1168(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L124
	cmpq	$1, %rax
	jne	.L81
	movzbl	(%r14), %edx
	movb	%dl, 32(%r12)
.L82:
	movq	%rax, 24(%r12)
	leaq	328(%rbx), %rsi
	movq	%r12, %rdi
	movb	$0, (%r15,%rax)
	leaq	72(%r12), %rax
	movq	%rax, 56(%r12)
	movl	%r13d, 48(%r12)
	movq	$0, 64(%r12)
	movb	$0, 72(%r12)
	movq	$0, 88(%r12)
	movb	$0, 96(%r12)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	336(%rbx), %rax
	addq	$1, 344(%rbx)
	addq	$16, %rax
	cmpq	$0, -1120(%rbp)
	movq	%rax, -1168(%rbp)
	je	.L125
	leaq	-1136(%rbp), %r12
	leaq	-1168(%rbp), %rsi
	movq	%r12, %rdi
	call	*-1112(%rbp)
	testb	%al, %al
	je	.L126
.L84:
	movq	-1120(%rbp), %rax
	testq	%rax, %rax
	je	.L87
	movl	$3, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	*%rax
.L87:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L63
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L63
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L127
	addq	$1192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testb	%al, %al
	je	.L72
	movl	16(%rbx), %eax
	shrq	$32, %r13
	movq	$0, -1184(%rbp)
	movq	$0, -1176(%rbp)
	testl	%eax, %eax
	jg	.L74
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L121:
	movq	352(%r12), %rsi
	leaq	-1168(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r12), %r12
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L128
.L69:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
.L119:
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	testq	%rax, %rax
	je	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L126:
	subq	$1, 344(%rbx)
	movq	336(%rbx), %r13
	movq	%r13, %rdi
	call	_ZNSt8__detail15_List_node_base9_M_unhookEv@PLT
	movq	56(%r13), %rdi
	leaq	72(%r13), %rax
	cmpq	%rax, %rdi
	je	.L85
	call	_ZdlPv@PLT
.L85:
	movq	16(%r13), %rdi
	leaq	32(%r13), %rax
	cmpq	%rax, %rdi
	je	.L86
	call	_ZdlPv@PLT
.L86:
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	-1168(%rbp), %rsi
	xorl	%edx, %edx
	leaq	16(%r12), %rdi
	movq	%rax, -1216(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1216(%rbp), %r8
	movq	%rax, 16(%r12)
	movq	%rax, %r15
	movq	-1168(%rbp), %rax
	movq	%rax, 32(%r12)
.L80:
	movq	%r15, %rdi
	movq	%r8, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-1168(%rbp), %rax
	movq	16(%r12), %r15
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-1200(%rbp), %rbx
	leaq	-1168(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r12
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L129
.L94:
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rax, -1216(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1216(%rbp), %rdi
	jmp	.L69
.L129:
	movq	%rdi, -1216(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1216(%rbp), %rdi
	jmp	.L94
.L127:
	call	__stack_chk_fail@PLT
.L125:
	call	_ZSt25__throw_bad_function_callv@PLT
	.cfi_endproc
.LFE7311:
	.size	_ZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"natives"
.LC4:
	.string	"No such module: %s"
.LC5:
	.string	"constants"
	.text
	.p2align 4
	.globl	_ZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7322:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$2152, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L160
	cmpw	$1040, %cx
	jne	.L131
.L160:
	movq	23(%rdx), %r13
.L133:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L134
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L135:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L182
.L136:
	leaq	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L136
	movq	352(%r13), %rsi
	leaq	-2144(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	_ZN4nodeL16modlist_internalE(%rip), %rbx
	movq	-2128(%rbp), %r14
	testq	%rbx, %rbx
	jne	.L140
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L183:
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L138
.L140:
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L183
	testb	$4, 4(%rbx)
	je	.L184
	movq	352(%r13), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	cmpq	$0, 24(%rbx)
	movq	%rax, %r14
	jne	.L185
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L186
	movq	352(%r13), %rsi
	movq	48(%rbx), %rcx
	movq	%r14, %rdi
	movq	%r14, %rbx
	movq	3280(%r13), %rdx
	addq	$88, %rsi
	call	*%rax
.L143:
	movq	(%r12), %rax
	testq	%rbx, %rbx
	je	.L187
	movq	(%rbx), %rdx
.L153:
	movq	-2128(%rbp), %rdi
	movq	%rdx, 24(%rax)
	testq	%rdi, %rdi
	je	.L130
.L181:
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L130
	call	free@PLT
.L130:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	addq	$2152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	.cfi_restore_state
	movq	8(%r12), %rdx
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L138:
	movl	$10, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L189
	movl	$8, %ecx
	leaq	.LC3(%rip), %rdi
	movq	%r14, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L146
	movq	3280(%r13), %rdi
	call	_ZN4node13native_module15NativeModuleEnv15GetSourceObjectEN2v85LocalINS2_7ContextEEE@PLT
	movq	352(%r13), %rdi
	movq	%rax, %rbx
	call	_ZN4node13native_module15NativeModuleEnv15GetConfigStringEPN2v87IsolateE@PLT
	movq	3280(%r13), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	360(%r13), %rax
	movq	344(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L190
.L147:
	shrw	$8, %ax
	jne	.L143
	leaq	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%r14, %r9
	movl	$1024, %ecx
	movl	$1, %edx
	xorl	%eax, %eax
	leaq	-1088(%rbp), %r15
	leaq	.LC4(%rip), %r8
	movl	$1024, %esi
	movq	%r15, %rdi
	leaq	-2176(%rbp), %r12
	call	__snprintf_chk@PLT
	movq	352(%r13), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	352(%r13), %r13
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L191
.L148:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-2128(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L181
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L189:
	movq	352(%r13), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	3280(%r13), %rsi
	movq	%rax, %r14
	movq	%rax, %rbx
	movq	352(%r13), %rax
	movq	%r14, %rdi
	leaq	104(%rax), %rdx
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L192
.L144:
	shrw	$8, %ax
	je	.L193
	movq	352(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN4node15DefineConstantsEPN2v87IsolateENS0_5LocalINS0_6ObjectEEE@PLT
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L187:
	movq	16(%rax), %rdx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L185:
	leaq	_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	movl	%eax, -2184(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-2184(%rbp), %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L193:
	leaq	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L192:
	movl	%eax, -2184(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-2184(%rbp), %eax
	jmp	.L144
.L191:
	movq	%rax, -2184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2184(%rbp), %rdi
	jmp	.L148
.L184:
	leaq	_ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L188:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7322:
	.size	_ZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"Linked module has no declared entry point."
	.section	.rodata.str1.1
.LC7:
	.string	"exports"
.LC8:
	.string	"No such module was linked: %s"
	.text
	.p2align 4
	.globl	_ZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7323:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$2152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%rdi, -2184(%rbp)
	movq	32(%rsi), %rdx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L229
	cmpw	$1040, %cx
	jne	.L195
.L229:
	movq	23(%rdx), %r14
.L197:
	movq	-2184(%rbp), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	jg	.L198
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rax
	addq	$88, %rdx
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L256
.L200:
	leaq	_ZZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L198:
	movq	8(%rax), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L200
.L256:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L200
	movq	352(%r14), %rsi
	leaq	-2144(%rbp), %rdi
	movq	%r14, %r15
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-2128(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	2400(%r15), %r12
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	cmpq	$0, 2392(%r15)
	je	.L202
	movq	2376(%r15), %rax
	leaq	16(%rax), %rbx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L257:
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L202
.L204:
	movq	40(%rbx), %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L257
	testb	$2, 4(%rbx)
	je	.L224
	movq	%r15, %rdi
	call	_ZNK4node11Environment17worker_parent_envEv@PLT
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
.L225:
	movq	352(%r14), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	352(%r14), %rdi
	movq	%rax, %r12
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	352(%r14), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L258
.L223:
	movq	3280(%r14), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L259
.L211:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L212
	movq	48(%rbx), %rcx
	movq	3280(%r14), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
.L213:
	movq	3280(%r14), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L260
	movq	(%rax), %rdx
	movq	-2184(%rbp), %rax
	movq	(%rax), %rax
	movq	%rdx, 24(%rax)
.L221:
	movq	-2128(%rbp), %rdi
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L194
	testq	%rdi, %rdi
	je	.L194
.L255:
	call	free@PLT
.L194:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L261
	addq	$2152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZNK4node11Environment17worker_parent_envEv@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	uv_mutex_unlock@PLT
	testq	%r15, %r15
	jne	.L206
	movq	_ZN4nodeL14modlist_linkedE(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L209
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L262:
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L208
.L209:
	movq	40(%rbx), %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L262
	testb	$2, 4(%rbx)
	jne	.L225
.L224:
	leaq	_ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L212:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L214
	movq	48(%rbx), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	*%rax
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L195:
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L197
.L208:
	movq	-2128(%rbp), %r9
	leaq	-1088(%rbp), %r15
	movl	$1024, %ecx
	xorl	%eax, %eax
	leaq	.LC8(%rip), %r8
	movl	$1, %edx
	movl	$1024, %esi
	movq	%r15, %rdi
	call	__snprintf_chk@PLT
	leaq	-2176(%rbp), %r12
	movq	352(%r14), %rsi
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	352(%r14), %r13
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L263
.L227:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L210:
	movq	-2128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L194
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L255
	jmp	.L194
.L258:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L223
.L259:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L211
.L260:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2184(%rbp), %rax
	movq	(%rax), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L221
.L214:
	movq	352(%r14), %rsi
	leaq	-2176(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r14), %r14
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L264
.L215:
	movq	%r13, %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L210
.L263:
	movq	%rdi, -2184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2184(%rbp), %rdi
	jmp	.L227
.L264:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L215
.L261:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7323:
	.size	_ZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node7binding22RegisterBuiltinModulesEv
	.type	_ZN4node7binding22RegisterBuiltinModulesEv, @function
_ZN4node7binding22RegisterBuiltinModulesEv:
.LFB7324:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_Z20_register_async_wrapv@PLT
	call	_Z16_register_bufferv@PLT
	call	_Z20_register_cares_wrapv@PLT
	call	_Z16_register_configv@PLT
	call	_Z20_register_contextifyv@PLT
	call	_Z21_register_credentialsv@PLT
	call	_Z16_register_errorsv@PLT
	call	_Z12_register_fsv@PLT
	call	_Z16_register_fs_dirv@PLT
	call	_Z23_register_fs_event_wrapv@PLT
	call	_Z20_register_heap_utilsv@PLT
	call	_Z15_register_http2v@PLT
	call	_Z21_register_http_parserv@PLT
	call	_Z28_register_http_parser_llhttpv@PLT
	call	_Z19_register_inspectorv@PLT
	call	_Z19_register_js_streamv@PLT
	call	_Z19_register_messagingv@PLT
	call	_Z21_register_module_wrapv@PLT
	call	_Z23_register_native_modulev@PLT
	call	_Z17_register_optionsv@PLT
	call	_Z12_register_osv@PLT
	call	_Z21_register_performancev@PLT
	call	_Z19_register_pipe_wrapv@PLT
	call	_Z22_register_process_wrapv@PLT
	call	_Z25_register_process_methodsv@PLT
	call	_Z16_register_reportv@PLT
	call	_Z16_register_serdesv@PLT
	call	_Z21_register_signal_wrapv@PLT
	call	_Z20_register_spawn_syncv@PLT
	call	_Z21_register_stream_pipev@PLT
	call	_Z21_register_stream_wrapv@PLT
	call	_Z24_register_string_decoderv@PLT
	call	_Z17_register_symbolsv@PLT
	call	_Z20_register_task_queuev@PLT
	call	_Z18_register_tcp_wrapv@PLT
	call	_Z16_register_timersv@PLT
	call	_Z22_register_trace_eventsv@PLT
	call	_Z18_register_tty_wrapv@PLT
	call	_Z15_register_typesv@PLT
	call	_Z18_register_udp_wrapv@PLT
	call	_Z13_register_urlv@PLT
	call	_Z14_register_utilv@PLT
	call	_Z12_register_uvv@PLT
	call	_Z12_register_v8v@PLT
	call	_Z14_register_wasiv@PLT
	call	_Z16_register_workerv@PLT
	call	_Z18_register_watchdogv@PLT
	call	_Z14_register_zlibv@PLT
	call	_Z16_register_cryptov@PLT
	call	_Z18_register_tls_wrapv@PLT
	call	_Z13_register_icuv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_Z18_register_profilerv@PLT
	.cfi_endproc
.LFE7324:
	.size	_ZN4node7binding22RegisterBuiltinModulesEv, .-_ZN4node7binding22RegisterBuiltinModulesEv
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, @function
_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm:
.LFB8711:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L268
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L278
.L294:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L279:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L292
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L293
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L271:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L273
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L275:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L276:
	testq	%rsi, %rsi
	je	.L273
.L274:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L275
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L281
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L274
	.p2align 4,,10
	.p2align 3
.L273:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L277
	call	_ZdlPv@PLT
.L277:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L294
.L278:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L280
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L280:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L281:
	movq	%rdx, %rdi
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L292:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L271
.L293:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8711:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm, .-_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_,"axG",@progbits,_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	.type	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_, @function
_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_:
.LFB8402:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rsi), %r13
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r13, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L296
	movq	(%rax), %rcx
	movq	8(%rcx), %r8
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L296
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L296
.L298:
	cmpq	%r8, %r13
	jne	.L308
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	.cfi_restore_state
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	(%rbx), %rax
	movl	$1, %r8d
	movl	$0, 16(%rcx)
	movq	%rax, 8(%rcx)
	movb	$0, 20(%rcx)
	movq	$0, 24(%rcx)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS9_10_Hash_nodeIS7_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8402:
	.size	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_, .-_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7binding4DLib21SaveInGlobalHandleMapEPNS_11node_moduleE
	.type	_ZN4node7binding4DLib21SaveInGlobalHandleMapEPNS_11node_moduleE, @function
_ZN4node7binding4DLib21SaveInGlobalHandleMapEPNS_11node_moduleE:
.LFB7307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	72(%rdi), %rax
	movb	$1, 80(%rdi)
	movq	%rax, -32(%rbp)
	testq	%rax, %rax
	je	.L313
	leaq	-32(%rbp), %r12
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	movq	%rsi, %rbx
	call	uv_mutex_lock@PLT
	movq	%r12, %rsi
	leaq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	movq	%r12, %rsi
	leaq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	movq	%rbx, 8(%rax)
	movl	4(%rbx), %ebx
	andl	$8, %ebx
	call	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	testl	%ebx, %ebx
	movq	%r12, %rsi
	leaq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	setne	4(%rax)
	call	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	addl	$1, (%rax)
	call	uv_mutex_unlock@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	leaq	_ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7307:
	.size	_ZN4node7binding4DLib21SaveInGlobalHandleMapEPNS_11node_moduleE, .-_ZN4node7binding4DLib21SaveInGlobalHandleMapEPNS_11node_moduleE
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_, @function
_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_:
.LFB8718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rsi), %r9
	movq	8(%rdi), %r8
	movq	(%rdi), %r13
	movq	%r9, %rax
	divq	%r8
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L326
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	%rdx, %r11
	movq	%r12, %r10
	movq	8(%rdi), %rcx
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L333:
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L326
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rcx, %rax
	divq	%r8
	cmpq	%r11, %rdx
	jne	.L326
	movq	%rsi, %rdi
.L318:
	cmpq	%rcx, %r9
	jne	.L333
	movq	(%rdi), %rcx
	cmpq	%r10, %r12
	je	.L334
	testq	%rcx, %rcx
	je	.L320
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L320
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%rdi), %rcx
.L320:
	movq	%rcx, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L327
	movq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%r8
	cmpq	%rdx, %r11
	je	.L320
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L319:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L335
.L321:
	movq	$0, (%r14)
	movq	(%rdi), %rcx
	jmp	.L320
.L327:
	movq	%r10, %rax
	jmp	.L319
.L335:
	movq	%rcx, 16(%rbx)
	jmp	.L321
	.cfi_endproc
.LFE8718:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_, .-_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_
	.section	.rodata.str1.1
.LC9:
	.string	"gnu_get_libc_version"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7binding4DLib5CloseEv
	.type	_ZN4node7binding4DLib5CloseEv, @function
_ZN4node7binding4DLib5CloseEv:
.LFB7305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 72(%rdi)
	je	.L336
	movzbl	_ZGVZL16libc_may_be_muslvE6retval(%rip), %eax
	movq	%rdi, %rbx
	testb	%al, %al
	je	.L371
.L340:
	movzbl	_ZZL16libc_may_be_muslvE17has_cached_retval(%rip), %eax
	testb	%al, %al
	je	.L342
	movzbl	_ZZL16libc_may_be_muslvE6retval(%rip), %eax
	testb	%al, %al
	setne	%al
	testb	%al, %al
	je	.L372
	.p2align 4,,10
	.p2align 3
.L336:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L373
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	.cfi_restore_state
	xorl	%edi, %edi
	leaq	.LC9(%rip), %rsi
	call	dlsym@PLT
	testq	%rax, %rax
	sete	%al
	movb	%al, _ZZL16libc_may_be_muslvE6retval(%rip)
	mfence
	movb	$1, _ZZL16libc_may_be_muslvE17has_cached_retval(%rip)
	mfence
	movzbl	_ZZL16libc_may_be_muslvE6retval(%rip), %eax
	testb	%al, %al
	setne	%al
	testb	%al, %al
	jne	.L336
.L372:
	movq	72(%rbx), %rdi
	call	dlclose@PLT
	testl	%eax, %eax
	jne	.L345
	cmpb	$0, 80(%rbx)
	je	.L345
	movq	72(%rbx), %rax
	movq	%rax, -32(%rbp)
	testq	%rax, %rax
	je	.L374
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	-32(%rbp), %r8
	movq	48+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rdi
	movq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L351
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L375:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L351
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L351
.L349:
	cmpq	%r8, %rsi
	jne	.L375
	movl	16(%rcx), %eax
	testl	%eax, %eax
	je	.L376
	subl	$1, %eax
	movl	%eax, 16(%rcx)
	jne	.L351
	cmpb	$0, 20(%rcx)
	je	.L350
	movq	24(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L350
	movl	$64, %esi
	call	_ZdlPvm@PLT
.L350:
	leaq	-32(%rbp), %rsi
	leaq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_N4node7binding19global_handle_map_t5EntryEESaIS7_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS9_18_Mod_range_hashingENS9_20_Default_ranged_hashENS9_20_Prime_rehash_policyENS9_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS2_
.L351:
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L345:
	movq	$0, 72(%rbx)
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L371:
	leaq	_ZGVZL16libc_may_be_muslvE6retval(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L340
	leaq	_ZGVZL16libc_may_be_muslvE6retval(%rip), %rdi
	call	__cxa_guard_release@PLT
	jmp	.L340
.L374:
	leaq	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L376:
	leaq	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L373:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7305:
	.size	_ZN4node7binding4DLib5CloseEv, .-_ZN4node7binding4DLib5CloseEv
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"ERR_NON_CONTEXT_AWARE_DISABLED"
	.align 8
.LC11:
	.string	"Loading non context-aware native modules has been disabled"
	.section	.rodata.str1.1
.LC12:
	.string	"code"
.LC13:
	.string	"node_register_module_v72"
.LC14:
	.string	"napi_register_module_v1"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"Module did not self-register: '%s'."
	.align 8
.LC16:
	.ascii	"T"
	.string	"he module '%s'\nwas compiled against a different Node.js version using\nNODE_MODULE_VERSION %d. This version of Node.js requires\nNODE_MODULE_VERSION %d. Please try re-compiling or re-installing\nthe module (for instance, using `npm rebuild` or `npm install`)."
	.align 8
.LC17:
	.string	"Module has no declared entry point."
	.text
	.align 2
	.p2align 4
	.type	_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_, @function
_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_:
.LFB7315:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$1096, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movzbl	_ZGVZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %eax
	testb	%al, %al
	je	.L443
.L379:
	leaq	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rdi
	call	uv_mutex_lock@PLT
	movl	32(%r12), %esi
	movq	(%r12), %rdi
	call	dlopen@PLT
	movq	%rax, 72(%r12)
	testq	%rax, %rax
	je	.L382
	movq	%fs:_ZN4nodeL23thread_local_modpendingE@tpoff, %rbx
	movq	$0, %fs:_ZN4nodeL23thread_local_modpendingE@tpoff
	testq	%rbx, %rbx
	je	.L444
	cmpq	$0, 32(%rbx)
	je	.L445
	movq	%rax, 8(%rbx)
	movb	$1, 80(%r12)
	movq	%rax, -1120(%rbp)
.L401:
	leaq	-1120(%rbp), %r14
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	%r14, %rsi
	leaq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	movl	4(%rbx), %r15d
	movq	%r14, %rsi
	leaq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	movq	%rbx, 8(%rax)
	andl	$8, %r15d
	call	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	testl	%r15d, %r15d
	leaq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	movq	%r14, %rsi
	setne	4(%rax)
	call	_ZNSt8__detail9_Map_baseIPvSt4pairIKS1_N4node7binding19global_handle_map_t5EntryEESaIS8_ENS_10_Select1stESt8equal_toIS1_ESt4hashIS1_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS3_
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	addl	$1, (%rax)
	call	uv_mutex_unlock@PLT
.L402:
	movl	(%rbx), %eax
	cmpl	$-1, %eax
	setne	%r14b
	cmpl	$72, %eax
	setne	%al
	andb	%al, %r14b
	jne	.L446
	movzbl	4(%rbx), %r14d
	andl	$1, %r14d
	jne	.L447
	leaq	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L416
	movq	24(%r13), %rdx
	movq	16(%r13), %rsi
	movl	$1, %r14d
	movq	8(%r13), %rdi
	movq	48(%rbx), %rcx
	movq	(%rsi), %rsi
	movq	(%rdx), %rdx
	movq	(%rdi), %rdi
	call	*%rax
.L417:
	leaq	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rdi
	call	uv_mutex_lock@PLT
.L386:
	leaq	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L448
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	leaq	_ZGVZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rdi
	call	__cxa_guard_acquire@PLT
	testl	%eax, %eax
	je	.L379
	leaq	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L449
	leaq	_ZGVZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rdi
	call	__cxa_guard_release@PLT
	leaq	__dso_handle(%rip), %rdx
	leaq	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex(%rip), %rsi
	leaq	_ZN4node9MutexBaseINS_16LibuvMutexTraitsEED1Ev(%rip), %rdi
	call	__cxa_atexit@PLT
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L382:
	call	dlerror@PLT
	movq	%rax, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	movq	48(%r12), %rdx
	movq	%r14, %rcx
	xorl	%esi, %esi
	movq	%rax, %r8
	leaq	40(%r12), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	$0, %fs:_ZN4nodeL23thread_local_modpendingE@tpoff
	movq	0(%r13), %rax
	movq	40(%r12), %rsi
	movq	(%rax), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L450
.L385:
	movq	%r12, %rdi
	call	_ZN4node7binding4DLib5CloseEv
	movq	0(%r13), %rax
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	movq	(%rax), %rax
	movq	352(%rax), %r12
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L445:
	movq	0(%r13), %rax
	movq	(%rax), %rax
	movq	1648(%rax), %r14
	movq	1640(%rax), %rdx
	testq	%r14, %r14
	je	.L388
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	leaq	8(%r14), %rax
	testq	%r15, %r15
	je	.L389
	lock addl	$1, (%rax)
.L390:
	movzbl	267(%rdx), %ecx
	testq	%r15, %r15
	je	.L451
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L391:
	cmpl	$1, %eax
	je	.L452
.L392:
	testb	%cl, %cl
	jne	.L453
	movq	72(%r12), %rax
	movq	%rax, 8(%rbx)
	movb	$1, 80(%r12)
	movq	%rax, -1120(%rbp)
	testq	%rax, %rax
	jne	.L401
	leaq	_ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L446:
	movq	72(%r12), %rdi
	leaq	.LC13(%rip), %rsi
	call	dlsym@PLT
	testq	%rax, %rax
	je	.L412
	movq	16(%r13), %rcx
	movq	24(%r13), %rdx
	movq	(%rcx), %rsi
	movq	8(%r13), %rcx
	movq	(%rdx), %rdx
	movq	(%rcx), %rdi
	call	*%rax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L444:
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdi
	call	dlsym@PLT
	testq	%rax, %rax
	je	.L403
	movq	16(%r13), %rcx
	movq	24(%r13), %rdx
	movl	$1, %r14d
	movq	(%rcx), %rsi
	movq	8(%r13), %rcx
	movq	(%rdx), %rdx
	movq	(%rcx), %rdi
	call	*%rax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L416:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L418
	movq	16(%r13), %rcx
	movq	48(%rbx), %rdx
	movl	$1, %r14d
	movq	(%rcx), %rsi
	movq	8(%r13), %rcx
	movq	(%rcx), %rdi
	call	*%rax
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L389:
	addl	$1, 8(%r14)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L388:
	movzbl	267(%rdx), %ecx
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L451:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L403:
	movq	72(%r12), %rdi
	leaq	.LC14(%rip), %rsi
	call	dlsym@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L404
	movq	16(%r13), %rax
	movq	24(%r13), %rdx
	movl	$1, %r14d
	movq	(%rax), %rsi
	movq	8(%r13), %rax
	movq	(%rdx), %rdx
	movq	(%rax), %rdi
	call	_Z30napi_module_register_by_symbolN2v85LocalINS_6ObjectEEENS0_INS_5ValueEEENS0_INS_7ContextEEEPFP12napi_value__P10napi_env__S8_E@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%r12, %rdi
	call	_ZN4node7binding4DLib5CloseEv
	movq	0(%r13), %rax
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC10(%rip), %rsi
	movq	(%rax), %rax
	movq	352(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L454
.L396:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L455
.L397:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L456
.L398:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L457
.L399:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L458
.L400:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L447:
	leaq	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L452:
	movq	(%r14), %rax
	movb	%cl, -1128(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	movzbl	-1128(%rbp), %ecx
	je	.L393
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L394:
	cmpl	$1, %eax
	jne	.L392
	movq	(%r14), %rax
	movb	%cl, -1128(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movzbl	-1128(%rbp), %ecx
	jmp	.L392
	.p2align 4,,10
	.p2align 3
.L412:
	movq	32(%r13), %rax
	movl	$1024, %ecx
	movl	$1, %edx
	leaq	-1088(%rbp), %r14
	leaq	.LC16(%rip), %r8
	movl	$1024, %esi
	movq	%r14, %rdi
	movq	16(%rax), %r9
	pushq	$72
	movl	(%rbx), %eax
	pushq	%rax
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	%r12, %rdi
	leaq	-1120(%rbp), %r12
	call	_ZN4node7binding4DLib5CloseEv
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r14, %rsi
	movq	352(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	popq	%rax
	popq	%rdx
	testq	%rdi, %rdi
	je	.L442
.L414:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L404:
	movq	72(%r12), %rbx
	movb	$1, 80(%r12)
	testq	%rbx, %rbx
	je	.L459
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_lock@PLT
	movq	48+_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	movq	%rbx, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	40+_ZN4node7bindingL17global_handle_mapE(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L406
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L460:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L406
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L406
.L408:
	cmpq	%rsi, %rbx
	jne	.L460
	addl	$1, 16(%rcx)
	movq	24(%rcx), %rbx
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	testq	%rbx, %rbx
	je	.L409
	cmpq	$0, 32(%rbx)
	jne	.L402
.L409:
	movq	%r12, %rdi
	leaq	-1088(%rbp), %r14
	leaq	-1120(%rbp), %r12
	call	_ZN4node7binding4DLib5CloseEv
	movq	32(%r13), %rax
	movl	$1024, %ecx
	movq	%r14, %rdi
	leaq	.LC15(%rip), %r8
	movl	$1, %edx
	movl	$1024, %esi
	movq	16(%rax), %r9
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	352(%rbx), %r13
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L414
.L442:
	movq	%rdi, -1128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1128(%rbp), %rdi
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L393:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L449:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L418:
	movq	%r12, %rdi
	leaq	-1120(%rbp), %r12
	call	_ZN4node7binding4DLib5CloseEv
	movq	0(%r13), %rax
	movq	%r12, %rdi
	movq	(%rax), %rbx
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%rbx), %r13
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC17(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L461
.L419:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L417
.L458:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L400
.L457:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L399
.L456:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L398
.L455:
	movq	%rax, -1128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1128(%rbp), %rdi
	jmp	.L397
.L454:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L396
.L459:
	leaq	_ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L406:
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L409
.L461:
	movq	%rax, -1128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1128(%rbp), %rdi
	jmp	.L419
.L448:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7315:
	.size	_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_, .-_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_
	.p2align 4
	.type	_ZNSt17_Function_handlerIFbPN4node7binding4DLibEEZNS1_6DLOpenERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS3_E_E9_M_invokeERKSt9_Any_dataOS3_, @function
_ZNSt17_Function_handlerIFbPN4node7binding4DLibEEZNS1_6DLOpenERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS3_E_E9_M_invokeERKSt9_Any_dataOS3_:
.LFB8427:
	.cfi_startproc
	endbr64
	movq	(%rsi), %rsi
	movq	(%rdi), %rdi
	jmp	_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_
	.cfi_endproc
.LFE8427:
	.size	_ZNSt17_Function_handlerIFbPN4node7binding4DLibEEZNS1_6DLOpenERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS3_E_E9_M_invokeERKSt9_Any_dataOS3_, .-_ZNSt17_Function_handlerIFbPN4node7binding4DLibEEZNS1_6DLOpenERKN2v820FunctionCallbackInfoINS5_5ValueEEEEUlS3_E_E9_M_invokeERKSt9_Any_dataOS3_
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node19node_is_initializedE, @function
_GLOBAL__sub_I__ZN4node19node_is_initializedE:
.LFB9430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node7bindingL17global_handle_mapE(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L466
	leaq	88+_ZN4node7bindingL17global_handle_mapE(%rip), %rax
	leaq	__dso_handle(%rip), %rdx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	$1, 48+_ZN4node7bindingL17global_handle_mapE(%rip)
	leaq	-88(%rax), %rsi
	leaq	_ZN4node7binding19global_handle_map_tD1Ev(%rip), %rdi
	movq	%rax, 40+_ZN4node7bindingL17global_handle_mapE(%rip)
	movq	$0, 56+_ZN4node7bindingL17global_handle_mapE(%rip)
	movq	$0, 64+_ZN4node7bindingL17global_handle_mapE(%rip)
	movl	$0x3f800000, 72+_ZN4node7bindingL17global_handle_mapE(%rip)
	movq	$0, 80+_ZN4node7bindingL17global_handle_mapE(%rip)
	movq	$0, 88+_ZN4node7bindingL17global_handle_mapE(%rip)
	jmp	__cxa_atexit@PLT
.L466:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9430:
	.size	_GLOBAL__sub_I__ZN4node19node_is_initializedE, .-_GLOBAL__sub_I__ZN4node19node_is_initializedE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node19node_is_initializedE
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC19:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC21:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.section	.rodata.str1.1
.LC22:
	.string	"../src/node_binding.cc:611"
.LC23:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"void node::binding::GetLinkedBinding(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7binding16GetLinkedBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.section	.rodata.str1.1
.LC25:
	.string	"../src/node_binding.cc:595"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"exports ->Set(env->context(), env->config_string(), native_module::NativeModuleEnv::GetConfigString( env->isolate())) .FromJust()"
	.align 8
.LC27:
	.string	"void node::binding::GetInternalBinding(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.section	.rodata.str1.1
.LC28:
	.string	"../src/node_binding.cc:588"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"exports->SetPrototype(env->context(), Null(env->isolate())).FromJust()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC27
	.section	.rodata.str1.1
.LC30:
	.string	"../src/node_binding.cc:577"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7binding18GetInternalBindingERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC30
	.quad	.LC23
	.quad	.LC27
	.section	.rodata.str1.1
.LC31:
	.string	"../src/node_binding.cc:562"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"(mod->nm_context_register_func) != nullptr"
	.align 8
.LC33:
	.string	"v8::Local<v8::Object> node::binding::InitModule(node::Environment*, node::node_module*, v8::Local<v8::String>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args_0, @object
	.size	_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args_0, 24
_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args_0:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.section	.rodata.str1.1
.LC34:
	.string	"../src/node_binding.cc:561"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"(mod->nm_register_func) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args, @object
	.size	_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args, 24
_ZZN4node7bindingL10InitModuleEPNS_11EnvironmentEPNS_11node_moduleEN2v85LocalINS5_6StringEEEE4args:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC33
	.weak	_ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args
	.section	.rodata.str1.1
.LC36:
	.string	"../src/node_binding.cc:552"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"mp == nullptr || (mp->nm_flags & flag) != 0"
	.align 8
.LC38:
	.string	"node::node_module* node::binding::FindModule(node::node_module*, const char*, int)"
	.section	.data.rel.ro.local._ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args,"awG",@progbits,_ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args,comdat
	.align 16
	.type	_ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args, @gnu_unique_object
	.size	_ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args, 24
_ZZN4node7binding10FindModuleEPNS_11node_moduleEPKciE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_binding.cc:522"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"(mp->nm_flags & NM_F_BUILTIN) == (0)"
	.align 8
.LC41:
	.string	"node::binding::DLOpen(const v8::FunctionCallbackInfo<v8::Value>&)::<lambda(node::binding::DLib*)>"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E4args, @object
	.size	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E4args, 24
_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E4args:
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.local	_ZGVZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex
	.comm	_ZGVZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex,8,8
	.local	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex
	.comm	_ZZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEENKUlPNS0_4DLibEE_clES8_E15dlib_load_mutex,40,32
	.section	.rodata.str1.1
.LC42:
	.string	"../src/node_binding.cc:415"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"(thread_local_modpending) == nullptr"
	.align 8
.LC44:
	.string	"void node::binding::DLOpen(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7binding6DLOpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.local	_ZN4node7bindingL17global_handle_mapE
	.comm	_ZN4node7bindingL17global_handle_mapE,96,32
	.weak	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args_0
	.section	.rodata.str1.1
.LC45:
	.string	"../src/node_binding.cc:300"
.LC46:
	.string	"(it->second.refcount) >= (1)"
	.section	.rodata.str1.8
	.align 8
.LC47:
	.string	"void node::binding::global_handle_map_t::erase(void*)"
	.section	.data.rel.ro.local._ZZN4node7binding19global_handle_map_t5eraseEPvE4args_0,"awG",@progbits,_ZZN4node7binding19global_handle_map_t5eraseEPvE4args_0,comdat
	.align 16
	.type	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args_0, @gnu_unique_object
	.size	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args_0, 24
_ZZN4node7binding19global_handle_map_t5eraseEPvE4args_0:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.weak	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args
	.section	.rodata.str1.1
.LC48:
	.string	"../src/node_binding.cc:295"
.LC49:
	.string	"(handle) != (nullptr)"
	.section	.data.rel.ro.local._ZZN4node7binding19global_handle_map_t5eraseEPvE4args,"awG",@progbits,_ZZN4node7binding19global_handle_map_t5eraseEPvE4args,comdat
	.align 16
	.type	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args, @gnu_unique_object
	.size	_ZZN4node7binding19global_handle_map_t5eraseEPvE4args, 24
_ZZN4node7binding19global_handle_map_t5eraseEPvE4args:
	.quad	.LC48
	.quad	.LC49
	.quad	.LC47
	.weak	_ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args
	.section	.rodata.str1.1
.LC50:
	.string	"../src/node_binding.cc:285"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"node::node_module* node::binding::global_handle_map_t::get_and_increase_refcount(void*)"
	.section	.data.rel.ro.local._ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args,"awG",@progbits,_ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args,comdat
	.align 16
	.type	_ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args, @gnu_unique_object
	.size	_ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args, 24
_ZZN4node7binding19global_handle_map_t25get_and_increase_refcountEPvE4args:
	.quad	.LC50
	.quad	.LC49
	.quad	.LC51
	.weak	_ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args
	.section	.rodata.str1.1
.LC52:
	.string	"../src/node_binding.cc:270"
	.section	.rodata.str1.8
	.align 8
.LC53:
	.string	"void node::binding::global_handle_map_t::set(void*, node::node_module*)"
	.section	.data.rel.ro.local._ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args,"awG",@progbits,_ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args,comdat
	.align 16
	.type	_ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args, @gnu_unique_object
	.size	_ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args, 24
_ZZN4node7binding19global_handle_map_t3setEPvPNS_11node_moduleEE4args:
	.quad	.LC52
	.quad	.LC49
	.quad	.LC53
	.globl	_ZN4node19node_is_initializedE
	.bss
	.type	_ZN4node19node_is_initializedE, @object
	.size	_ZN4node19node_is_initializedE, 1
_ZN4node19node_is_initializedE:
	.zero	1
	.section	.tbss,"awT",@nobits
	.align 8
	.type	_ZN4nodeL23thread_local_modpendingE, @object
	.size	_ZN4nodeL23thread_local_modpendingE, 8
_ZN4nodeL23thread_local_modpendingE:
	.zero	8
	.local	_ZN4nodeL14modlist_linkedE
	.comm	_ZN4nodeL14modlist_linkedE,8,8
	.local	_ZN4nodeL16modlist_internalE
	.comm	_ZN4nodeL16modlist_internalE,8,8
	.local	_ZZL16libc_may_be_muslvE17has_cached_retval
	.comm	_ZZL16libc_may_be_muslvE17has_cached_retval,1,1
	.local	_ZGVZL16libc_may_be_muslvE6retval
	.comm	_ZGVZL16libc_may_be_muslvE6retval,8,8
	.local	_ZZL16libc_may_be_muslvE6retval
	.comm	_ZZL16libc_may_be_muslvE6retval,1,1
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.hidden	__dso_handle
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
