	.file	"node_types.cc"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L14IsNumberObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L14IsNumberObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L2
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L3:
	call	_ZNK2v85Value14IsNumberObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L3
	.cfi_endproc
.LFE7085:
	.size	_ZN4node12_GLOBAL__N_1L14IsNumberObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L14IsNumberObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L14IsStringObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L14IsStringObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L9
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L10:
	call	_ZNK2v85Value14IsStringObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L10
	.cfi_endproc
.LFE7086:
	.size	_ZN4node12_GLOBAL__N_1L14IsStringObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L14IsStringObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L15IsBooleanObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L15IsBooleanObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L15
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L16:
	call	_ZNK2v85Value15IsBooleanObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L16
	.cfi_endproc
.LFE7084:
	.size	_ZN4node12_GLOBAL__N_1L15IsBooleanObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L15IsBooleanObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L14IsBigIntObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L14IsBigIntObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L21
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L22:
	call	_ZNK2v85Value14IsBigIntObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L22
	.cfi_endproc
.LFE7083:
	.size	_ZN4node12_GLOBAL__N_1L14IsBigIntObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L14IsBigIntObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L16IsBoxedPrimitiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L16IsBoxedPrimitiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %r12
	movl	16(%rdi), %edi
	testl	%edi, %edi
	jg	.L27
	movq	8(%r12), %rax
	leaq	88(%rax), %rdi
.L28:
	call	_ZNK2v85Value14IsNumberObjectEv@PLT
	testb	%al, %al
	jne	.L33
	movl	16(%rbx), %esi
	testl	%esi, %esi
	jle	.L41
	movq	8(%rbx), %rdi
.L32:
	call	_ZNK2v85Value14IsStringObjectEv@PLT
	testb	%al, %al
	jne	.L33
	movl	16(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L42
	movq	8(%rbx), %rdi
.L35:
	call	_ZNK2v85Value15IsBooleanObjectEv@PLT
	testb	%al, %al
	jne	.L33
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L36
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L37:
	call	_ZNK2v85Value14IsBigIntObjectEv@PLT
	testb	%al, %al
	jne	.L33
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L43
	movq	8(%rbx), %rdi
.L39:
	call	_ZNK2v85Value14IsSymbolObjectEv@PLT
	testb	%al, %al
	jne	.L33
	movl	$64, %edx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$56, %edx
.L30:
	movq	8(%r12), %rax
	movq	56(%rax,%rdx), %rax
	movq	%rax, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L42:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L36:
	movq	8(%rbx), %rdi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L39
	.cfi_endproc
.LFE7107:
	.size	_ZN4node12_GLOBAL__N_1L16IsBoxedPrimitiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L16IsBoxedPrimitiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L14IsSymbolObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L14IsSymbolObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L45
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L46:
	call	_ZNK2v85Value14IsSymbolObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L46
	.cfi_endproc
.LFE7087:
	.size	_ZN4node12_GLOBAL__N_1L14IsSymbolObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L14IsSymbolObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L13IsArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L13IsArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L51
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L52:
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L52
	.cfi_endproc
.LFE7100:
	.size	_ZN4node12_GLOBAL__N_1L13IsArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L13IsArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L16IsAnyArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L16IsAnyArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7106:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movl	16(%rdi), %edx
	movq	%rdi, %rbx
	movq	(%rdi), %r12
	testl	%edx, %edx
	jg	.L57
	movq	8(%r12), %rax
	leaq	88(%rax), %rdi
.L58:
	call	_ZNK2v85Value13IsArrayBufferEv@PLT
	testb	%al, %al
	jne	.L63
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L65
	movq	8(%rbx), %rdi
.L62:
	call	_ZNK2v85Value19IsSharedArrayBufferEv@PLT
	testb	%al, %al
	jne	.L63
	movl	$64, %eax
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$56, %eax
.L60:
	movq	8(%r12), %rdx
	movq	56(%rdx,%rax), %rax
	movq	%rax, 24(%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L65:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L62
	.cfi_endproc
.LFE7106:
	.size	_ZN4node12_GLOBAL__N_1L16IsAnyArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L16IsAnyArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L19IsSharedArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L19IsSharedArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7102:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L67
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L68:
	call	_ZNK2v85Value19IsSharedArrayBufferEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L68
	.cfi_endproc
.LFE7102:
	.size	_ZN4node12_GLOBAL__N_1L19IsSharedArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L19IsSharedArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L23IsModuleNamespaceObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L23IsModuleNamespaceObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L73
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L74:
	call	_ZNK2v85Value23IsModuleNamespaceObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L74
	.cfi_endproc
.LFE7105:
	.size	_ZN4node12_GLOBAL__N_1L23IsModuleNamespaceObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L23IsModuleNamespaceObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L27IsWebAssemblyCompiledModuleERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L27IsWebAssemblyCompiledModuleERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7104:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L79
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L80:
	call	_ZNK2v85Value27IsWebAssemblyCompiledModuleEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L80
	.cfi_endproc
.LFE7104:
	.size	_ZN4node12_GLOBAL__N_1L27IsWebAssemblyCompiledModuleERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L27IsWebAssemblyCompiledModuleERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L7IsProxyERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L7IsProxyERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7103:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L85
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L86:
	call	_ZNK2v85Value7IsProxyEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L86
	.cfi_endproc
.LFE7103:
	.size	_ZN4node12_GLOBAL__N_1L7IsProxyERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L7IsProxyERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L10IsDataViewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L10IsDataViewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7101:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L91
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L92:
	call	_ZNK2v85Value10IsDataViewEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L92
	.cfi_endproc
.LFE7101:
	.size	_ZN4node12_GLOBAL__N_1L10IsDataViewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L10IsDataViewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L9IsWeakSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L9IsWeakSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L97
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L98:
	call	_ZNK2v85Value9IsWeakSetEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L98
	.cfi_endproc
.LFE7099:
	.size	_ZN4node12_GLOBAL__N_1L9IsWeakSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L9IsWeakSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L9IsWeakMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L9IsWeakMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7098:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L103
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L104:
	call	_ZNK2v85Value9IsWeakMapEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L104
	.cfi_endproc
.LFE7098:
	.size	_ZN4node12_GLOBAL__N_1L9IsWeakMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L9IsWeakMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L13IsSetIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L13IsSetIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L109
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L110:
	call	_ZNK2v85Value13IsSetIteratorEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L110
	.cfi_endproc
.LFE7097:
	.size	_ZN4node12_GLOBAL__N_1L13IsSetIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L13IsSetIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L13IsMapIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L13IsMapIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L115
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L116:
	call	_ZNK2v85Value13IsMapIteratorEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L116
	.cfi_endproc
.LFE7096:
	.size	_ZN4node12_GLOBAL__N_1L13IsMapIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L13IsMapIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L5IsSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L5IsSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L121
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L122:
	call	_ZNK2v85Value5IsSetEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L122
	.cfi_endproc
.LFE7095:
	.size	_ZN4node12_GLOBAL__N_1L5IsSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L5IsSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L5IsMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L5IsMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L127
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L128:
	call	_ZNK2v85Value5IsMapEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L128
	.cfi_endproc
.LFE7094:
	.size	_ZN4node12_GLOBAL__N_1L5IsMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L5IsMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L9IsPromiseERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L9IsPromiseERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L133
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L134:
	call	_ZNK2v85Value9IsPromiseEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L134
	.cfi_endproc
.LFE7093:
	.size	_ZN4node12_GLOBAL__N_1L9IsPromiseERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L9IsPromiseERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L17IsGeneratorObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L17IsGeneratorObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L139
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L140:
	call	_ZNK2v85Value17IsGeneratorObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L140
	.cfi_endproc
.LFE7092:
	.size	_ZN4node12_GLOBAL__N_1L17IsGeneratorObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L17IsGeneratorObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L19IsGeneratorFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L19IsGeneratorFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L145
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L146:
	call	_ZNK2v85Value19IsGeneratorFunctionEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L146
	.cfi_endproc
.LFE7091:
	.size	_ZN4node12_GLOBAL__N_1L19IsGeneratorFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L19IsGeneratorFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L15IsAsyncFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L15IsAsyncFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L151
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L152:
	call	_ZNK2v85Value15IsAsyncFunctionEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L152
	.cfi_endproc
.LFE7090:
	.size	_ZN4node12_GLOBAL__N_1L15IsAsyncFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L15IsAsyncFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L8IsRegExpERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L8IsRegExpERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L157
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L158:
	call	_ZNK2v85Value8IsRegExpEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L158
	.cfi_endproc
.LFE7089:
	.size	_ZN4node12_GLOBAL__N_1L8IsRegExpERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L8IsRegExpERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L13IsNativeErrorERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L13IsNativeErrorERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L163
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L164:
	call	_ZNK2v85Value13IsNativeErrorEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L164
	.cfi_endproc
.LFE7088:
	.size	_ZN4node12_GLOBAL__N_1L13IsNativeErrorERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L13IsNativeErrorERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L17IsArgumentsObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L17IsArgumentsObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L169
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L170:
	call	_ZNK2v85Value17IsArgumentsObjectEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L170
	.cfi_endproc
.LFE7082:
	.size	_ZN4node12_GLOBAL__N_1L17IsArgumentsObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L17IsArgumentsObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L6IsDateERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L6IsDateERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L175
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L176:
	call	_ZNK2v85Value6IsDateEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L176
	.cfi_endproc
.LFE7081:
	.size	_ZN4node12_GLOBAL__N_1L6IsDateERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L6IsDateERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_1L10IsExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node12_GLOBAL__N_1L10IsExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movl	16(%rdi), %eax
	movq	(%rdi), %rbx
	testl	%eax, %eax
	jg	.L181
	movq	8(%rbx), %rax
	leaq	88(%rax), %rdi
.L182:
	call	_ZNK2v85Value10IsExternalEv@PLT
	movq	8(%rbx), %rdx
	cmpb	$1, %al
	sbbq	%rax, %rax
	andl	$8, %eax
	movq	112(%rdx,%rax), %rax
	movq	%rax, 24(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	jmp	.L182
	.cfi_endproc
.LFE7080:
	.size	_ZN4node12_GLOBAL__N_1L10IsExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node12_GLOBAL__N_1L10IsExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"isExternal"
.LC1:
	.string	"isDate"
.LC2:
	.string	"isArgumentsObject"
.LC3:
	.string	"isBigIntObject"
.LC4:
	.string	"isBooleanObject"
.LC5:
	.string	"isNumberObject"
.LC6:
	.string	"isStringObject"
.LC7:
	.string	"isSymbolObject"
.LC8:
	.string	"isNativeError"
.LC9:
	.string	"isRegExp"
.LC10:
	.string	"isAsyncFunction"
.LC11:
	.string	"isGeneratorFunction"
.LC12:
	.string	"isGeneratorObject"
.LC13:
	.string	"isPromise"
.LC14:
	.string	"isMap"
.LC15:
	.string	"isSet"
.LC16:
	.string	"isMapIterator"
.LC17:
	.string	"isSetIterator"
.LC18:
	.string	"isWeakMap"
.LC19:
	.string	"isWeakSet"
.LC20:
	.string	"isArrayBuffer"
.LC21:
	.string	"isDataView"
.LC22:
	.string	"isSharedArrayBuffer"
.LC23:
	.string	"isProxy"
.LC24:
	.string	"isWebAssemblyCompiledModule"
.LC25:
	.string	"isModuleNamespaceObject"
.LC26:
	.string	"isAnyArrayBuffer"
.LC27:
	.string	"isBoxedPrimitive"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB28:
	.text
.LHOTB28:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7108:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L187
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L187
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L187
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L10IsExternalERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L276
.L188:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L277
.L189:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L278
.L190:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node12_GLOBAL__N_1L6IsDateERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L279
.L191:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L280
.L192:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L281
.L193:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L17IsArgumentsObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L282
.L194:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L283
.L195:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L284
.L196:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L14IsBigIntObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L285
.L197:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L286
.L198:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L287
.L199:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L15IsBooleanObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L288
.L200:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L289
.L201:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L290
.L202:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L14IsNumberObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L291
.L203:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC5(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L292
.L204:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L293
.L205:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node12_GLOBAL__N_1L14IsStringObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L294
.L206:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L295
.L207:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L296
.L208:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L14IsSymbolObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L297
.L209:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L298
.L210:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L299
.L211:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L13IsNativeErrorERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L300
.L212:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC8(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L301
.L213:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L302
.L214:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L8IsRegExpERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L303
.L215:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L304
.L216:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L305
.L217:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L15IsAsyncFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L306
.L218:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC10(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L307
.L219:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L308
.L220:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node12_GLOBAL__N_1L19IsGeneratorFunctionERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L309
.L221:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L310
.L222:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L311
.L223:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L17IsGeneratorObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L312
.L224:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L313
.L225:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L314
.L226:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L9IsPromiseERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L315
.L227:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L316
.L228:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L317
.L229:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L5IsMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L318
.L230:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L319
.L231:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L320
.L232:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L5IsSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L321
.L233:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L322
.L234:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L323
.L235:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node12_GLOBAL__N_1L13IsMapIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L324
.L236:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L325
.L237:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L326
.L238:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L13IsSetIteratorERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L327
.L239:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L328
.L240:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L329
.L241:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L9IsWeakMapERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L330
.L242:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L331
.L243:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L332
.L244:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L9IsWeakSetERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L333
.L245:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L334
.L246:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L335
.L247:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L13IsArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L336
.L248:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L337
.L249:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L338
.L250:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node12_GLOBAL__N_1L10IsDataViewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L339
.L251:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L340
.L252:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L341
.L253:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L19IsSharedArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L342
.L254:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L343
.L255:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L344
.L256:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L7IsProxyERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r15
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L345
.L257:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L346
.L258:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L347
.L259:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L27IsWebAssemblyCompiledModuleERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L348
.L260:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L349
.L261:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L350
.L262:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L23IsModuleNamespaceObjectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L351
.L263:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L352
.L264:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L353
.L265:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	pushq	$1
	leaq	_ZN4node12_GLOBAL__N_1L16IsAnyArrayBufferERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L354
.L266:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L355
.L267:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L356
.L268:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node12_GLOBAL__N_1L16IsBoxedPrimitiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L357
.L269:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L358
.L270:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L359
.L271:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L276:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L277:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L278:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L279:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L280:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L281:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L282:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L283:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L284:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L285:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L286:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L287:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L288:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L289:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L290:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L291:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L292:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L293:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L294:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L295:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L296:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L297:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L298:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L299:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L300:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L301:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L302:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L303:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L304:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L305:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L306:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L307:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L308:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L309:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L310:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L311:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L312:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L313:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L314:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L315:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L316:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L317:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L318:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L319:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L320:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L321:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L322:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L323:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L324:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L325:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L326:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L327:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L328:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L329:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L330:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L331:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L332:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L333:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L334:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L335:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L336:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L337:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L338:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L339:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L340:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L341:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L342:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L343:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L344:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L345:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L346:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L347:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L348:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L349:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L350:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L351:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L352:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L353:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L354:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L355:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L356:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L357:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L358:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L359:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L271
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7108:
.L187:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7108:
	.text
	.size	_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE28:
	.text
.LHOTE28:
	.p2align 4
	.globl	_Z15_register_typesv
	.type	_Z15_register_typesv, @function
_Z15_register_typesv:
.LFB7109:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7109:
	.size	_Z15_register_typesv, .-_Z15_register_typesv
	.section	.rodata.str1.1
.LC29:
	.string	"../src/node_types.cc"
.LC30:
	.string	"types"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC29
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_115InitializeTypesEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC30
	.quad	0
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
