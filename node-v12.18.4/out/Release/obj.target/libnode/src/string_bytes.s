	.file	"string_bytes.cc"
	.text
	.section	.text._ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.type	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, @function
_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv:
.LFB2390:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE2390:
	.size	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv, .-_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.section	.text._ZN2v86String26ExternalStringResourceBase7DisposeEv,"axG",@progbits,_ZN2v86String26ExternalStringResourceBase7DisposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.type	_ZN2v86String26ExternalStringResourceBase7DisposeEv, @function
_ZN2v86String26ExternalStringResourceBase7DisposeEv:
.LFB2391:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE2391:
	.size	_ZN2v86String26ExternalStringResourceBase7DisposeEv, .-_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase4LockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase4LockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.type	_ZNK2v86String26ExternalStringResourceBase4LockEv, @function
_ZNK2v86String26ExternalStringResourceBase4LockEv:
.LFB2392:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2392:
	.size	_ZNK2v86String26ExternalStringResourceBase4LockEv, .-_ZNK2v86String26ExternalStringResourceBase4LockEv
	.section	.text._ZNK2v86String26ExternalStringResourceBase6UnlockEv,"axG",@progbits,_ZNK2v86String26ExternalStringResourceBase6UnlockEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.type	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, @function
_ZNK2v86String26ExternalStringResourceBase6UnlockEv:
.LFB2393:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE2393:
	.size	_ZNK2v86String26ExternalStringResourceBase6UnlockEv, .-_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv, @function
_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv:
.LFB8628:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE8628:
	.size	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv, .-_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE6lengthEv, @function
_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE6lengthEv:
.LFB8630:
	.cfi_startproc
	endbr64
	movq	24(%rdi), %rax
	ret
	.cfi_endproc
.LFE8630:
	.size	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE6lengthEv, .-_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE6lengthEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE4dataEv, @function
_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE4dataEv:
.LFB9240:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9240:
	.size	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE4dataEv, .-_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE4dataEv
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv, @function
_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv:
.LFB9241:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE9241:
	.size	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv, .-_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED2Ev, @function
_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED2Ev:
.LFB8364:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	8(%rbx), %r12
	movq	24(%rbx), %r13
	movq	32(%r12), %rbx
	subq	%r13, %rbx
	movq	%rbx, %rax
	subq	48(%r12), %rax
	movq	%rbx, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L18
.L11:
	testq	%r13, %r13
	jg	.L19
	je	.L10
	cmpq	40(%r12), %rbx
	jg	.L20
.L10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movq	40(%r12), %rax
	subq	%r13, %rax
	cmpq	$67108864, %rax
	jle	.L10
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.cfi_endproc
.LFE8364:
	.size	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED2Ev, .-_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED2Ev
	.set	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED1Ev,_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED2Ev, @function
_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED2Ev:
.LFB8380:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	8(%rbx), %r12
	movq	24(%rbx), %rbx
	movq	32(%r12), %r13
	addq	%rbx, %rbx
	subq	%rbx, %r13
	movq	%r13, %rax
	subq	48(%r12), %rax
	movq	%r13, 32(%r12)
	cmpq	$33554432, %rax
	jg	.L29
.L22:
	testq	%rbx, %rbx
	jg	.L30
	je	.L21
	cmpq	40(%r12), %r13
	jg	.L31
.L21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	.cfi_restore_state
	movq	40(%r12), %rax
	subq	%rbx, %rax
	cmpq	$67108864, %rax
	jle	.L21
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	.cfi_endproc
.LFE8380:
	.size	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED2Ev, .-_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED2Ev
	.set	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED1Ev,_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED0Ev, @function
_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED0Ev:
.LFB8366:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	8(%r12), %r13
	movq	24(%r12), %r14
	movq	32(%r13), %rbx
	subq	%r14, %rbx
	movq	%rbx, %rax
	subq	48(%r13), %rax
	movq	%rbx, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L40
.L33:
	testq	%r14, %r14
	jg	.L41
	je	.L35
	cmpq	40(%r13), %rbx
	jg	.L42
.L35:
	popq	%rbx
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
	movq	40(%r13), %rax
	subq	%r14, %rax
	cmpq	$67108864, %rax
	jle	.L35
	movq	%rax, 40(%r13)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L35
	.cfi_endproc
.LFE8366:
	.size	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED0Ev, .-_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED0Ev, @function
_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED0Ev:
.LFB8382:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	8(%r12), %r13
	movq	24(%r12), %rax
	movq	32(%r13), %r14
	leaq	(%rax,%rax), %rbx
	subq	%rbx, %r14
	movq	%r14, %rax
	subq	48(%r13), %rax
	movq	%r14, 32(%r13)
	cmpq	$33554432, %rax
	jg	.L51
.L44:
	testq	%rbx, %rbx
	jg	.L52
	je	.L46
	cmpq	40(%r13), %r14
	jg	.L53
.L46:
	popq	%rbx
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
	movq	40(%r13), %rax
	subq	%rbx, %rax
	cmpq	$67108864, %rax
	jle	.L46
	movq	%rax, 40(%r13)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%r13, %rdi
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r13, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L46
	.cfi_endproc
.LFE8382:
	.size	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED0Ev, .-_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED0Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Cannot create a string longer than 0x%x characters"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"ERR_STRING_TOO_LONG"
.LC2:
	.string	"code"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE3NewEPNS2_7IsolateEPcmPNS2_5LocalINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE3NewEPNS2_7IsolateEPcmPNS2_5LocalINS2_5ValueEEE:
.LFB7951:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	128(%rdi), %rax
	testq	%rdx, %rdx
	je	.L56
	movq	%rsi, %r13
	movq	%rdx, %rbx
	cmpq	$1031912, %rdx
	ja	.L57
	movl	%edx, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L84
.L58:
	movq	%r13, %rdi
	call	free@PLT
	movq	%rbx, %rax
.L56:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L85
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	.cfi_restore_state
	movl	$32, %edi
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcEE(%rip), %r14
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%rbx, 24(%rax)
	movq	%rax, %rsi
	movq	%rax, %r15
	movq	%r14, (%rax)
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	call	_ZN2v86String18NewExternalOneByteEPNS_7IsolateEPNS0_29ExternalOneByteStringResourceE@PLT
	movq	32(%r12), %rdx
	movq	%rax, %rbx
	movq	24(%r15), %rax
	addq	%rax, %rdx
	movq	%rdx, %rcx
	subq	48(%r12), %rcx
	movq	%rdx, 32(%r12)
	cmpq	$33554432, %rcx
	jg	.L86
.L64:
	testq	%rax, %rax
	js	.L87
	je	.L66
	cmpq	40(%r12), %rdx
	jle	.L66
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L87:
	addq	40(%r12), %rax
	cmpq	$67108864, %rax
	jle	.L66
	movq	%rax, 40(%r12)
.L66:
	testq	%rbx, %rbx
	je	.L88
.L67:
	movq	%rbx, %rax
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r12, %rdi
	movq	%rdx, -216(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	-192(%rbp), %r14
	movl	$1073741799, %r9d
	movl	$128, %ecx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %r8
	movl	$1, %edx
	movl	$128, %esi
	movq	%r14, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L89
.L59:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L90
.L60:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L91
.L61:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L92
.L62:
	movq	%r12, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-208(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L93
.L63:
	movq	-200(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r14, (%r15)
	movq	16(%r15), %rdi
	call	free@PLT
	movq	8(%r15), %r14
	movq	24(%r15), %rdx
	movq	32(%r14), %rax
	subq	%rdx, %rax
	movq	%rax, %rcx
	subq	48(%r14), %rcx
	movq	%rax, 32(%r14)
	cmpq	$33554432, %rcx
	jg	.L94
.L68:
	testq	%rdx, %rdx
	jg	.L95
	je	.L70
	cmpq	40(%r14), %rax
	jg	.L96
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r15, %rdi
	movl	$32, %esi
	leaq	-192(%rbp), %r14
	call	_ZdlPvm@PLT
	movl	$1073741799, %r9d
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	movl	$1, %edx
	movl	$128, %esi
	leaq	.LC0(%rip), %r8
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L97
.L71:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L98
.L72:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L99
.L73:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L100
.L74:
	movq	%r12, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-208(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L101
.L75:
	movq	-200(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L95:
	movq	40(%r14), %rax
	subq	%rdx, %rax
	cmpq	$67108864, %rax
	jle	.L70
	movq	%rax, 40(%r14)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r14, %rdi
	movq	%rax, -216(%rbp)
	movq	%rdx, -208(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %rdx
	jmp	.L68
.L100:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdx
	jmp	.L74
.L99:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L73
.L98:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdi
	jmp	.L72
.L97:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L71
.L101:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L75
.L93:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L63
.L92:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdx
	jmp	.L62
.L91:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L61
.L90:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdi
	jmp	.L60
.L89:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L59
.L96:
	movq	%r14, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L70
.L85:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7951:
	.size	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE3NewEPNS2_7IsolateEPcmPNS2_5LocalINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE3NewEPNS2_7IsolateEPcmPNS2_5LocalINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE3NewEPNS2_7IsolateEPtmPNS2_5LocalINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE3NewEPNS2_7IsolateEPtmPNS2_5LocalINS2_5ValueEEE:
.LFB7954:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rcx, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	128(%rdi), %rax
	testq	%rdx, %rdx
	je	.L104
	movq	%rsi, %r13
	movq	%rdx, %rbx
	cmpq	$1031912, %rdx
	ja	.L105
	movl	%edx, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L132
.L106:
	movq	%r13, %rdi
	call	free@PLT
	movq	%rbx, %rax
.L104:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L133
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	$32, %edi
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtEE(%rip), %r14
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%rbx, 24(%rax)
	movq	%rax, %rsi
	movq	%rax, %r15
	movq	%r14, (%rax)
	movq	%r12, 8(%rax)
	movq	%r13, 16(%rax)
	call	_ZN2v86String18NewExternalTwoByteEPNS_7IsolateEPNS0_22ExternalStringResourceE@PLT
	movq	32(%r12), %rdx
	movq	%rax, %rbx
	movq	24(%r15), %rax
	addq	%rax, %rax
	addq	%rax, %rdx
	movq	%rdx, %rcx
	subq	48(%r12), %rcx
	movq	%rdx, 32(%r12)
	cmpq	$33554432, %rcx
	jg	.L134
.L112:
	testq	%rax, %rax
	js	.L135
	je	.L114
	cmpq	40(%r12), %rdx
	jle	.L114
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L135:
	addq	40(%r12), %rax
	cmpq	$67108864, %rax
	jle	.L114
	movq	%rax, 40(%r12)
.L114:
	testq	%rbx, %rbx
	je	.L136
.L115:
	movq	%rbx, %rax
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L134:
	movq	%r12, %rdi
	movq	%rdx, -216(%rbp)
	movq	%rax, -208(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-216(%rbp), %rdx
	movq	-208(%rbp), %rax
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	-192(%rbp), %r14
	movl	$1073741799, %r9d
	movl	$128, %ecx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %r8
	movl	$1, %edx
	movl	$128, %esi
	movq	%r14, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L137
.L107:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L138
.L108:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L139
.L109:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L140
.L110:
	movq	%r12, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-208(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L141
.L111:
	movq	-200(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r14, (%r15)
	movq	16(%r15), %rdi
	call	free@PLT
	movq	8(%r15), %rdi
	movq	24(%r15), %rax
	leaq	(%rax,%rax), %r14
	movq	32(%rdi), %rax
	subq	%r14, %rax
	movq	%rax, %rdx
	subq	48(%rdi), %rdx
	movq	%rax, 32(%rdi)
	cmpq	$33554432, %rdx
	jg	.L142
.L116:
	testq	%r14, %r14
	jg	.L143
	je	.L118
	cmpq	40(%rdi), %rax
	jg	.L144
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r15, %rdi
	movl	$32, %esi
	leaq	-192(%rbp), %r14
	call	_ZdlPvm@PLT
	movl	$1073741799, %r9d
	movq	%r14, %rdi
	xorl	%eax, %eax
	movl	$128, %ecx
	movl	$1, %edx
	movl	$128, %esi
	leaq	.LC0(%rip), %r8
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L145
.L119:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L146
.L120:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L147
.L121:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L148
.L122:
	movq	%r12, %rdi
	movq	%rdx, -208(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-208(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L149
.L123:
	movq	-200(%rbp), %rax
	movq	%r14, (%rax)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L143:
	movq	40(%rdi), %rax
	subq	%r14, %rax
	cmpq	$67108864, %rax
	jle	.L118
	movq	%rax, 40(%rdi)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%rax, -216(%rbp)
	movq	%rdi, -208(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %rdi
	jmp	.L116
.L148:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdx
	jmp	.L122
.L147:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L121
.L146:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdi
	jmp	.L120
.L145:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L119
.L149:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L123
.L141:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L111
.L140:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdx
	jmp	.L110
.L139:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L109
.L138:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rdi
	jmp	.L108
.L137:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L107
.L144:
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L118
.L133:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7954:
	.size	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE3NewEPNS2_7IsolateEPtmPNS2_5LocalINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE3NewEPNS2_7IsolateEPtmPNS2_5LocalINS2_5ValueEEE
	.section	.text._ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE,"axG",@progbits,_ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE,comdat
	.p2align 4
	.weak	_ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE
	.type	_ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE, @function
_ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE:
.LFB7234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$128, %ecx
	movl	$1, %edx
	movl	$128, %esi
	movl	$1073741799, %r9d
	leaq	.LC0(%rip), %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L158
.L151:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L159
.L152:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L160
.L153:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L161
.L154:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L162
.L155:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	addq	$160, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rax, -184(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-184(%rbp), %rdi
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L160:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L161:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L162:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L155
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7234:
	.size	_ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE, .-_ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE
	.section	.rodata.str1.1
.LC3:
	.string	"Failed to allocate memory"
.LC4:
	.string	"ERR_MEMORY_ALLOCATION_FAILED"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE.part.0, @function
_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE.part.0:
.LFB9258:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpl	$6, %ecx
	ja	.L165
	movq	%rdx, %r15
	movl	%ecx, %ecx
	leaq	.L167(%rip), %rdx
	movq	%rdi, %r12
	movslq	(%rdx,%rcx,4), %rax
	movq	%rsi, %r14
	movq	%r8, %r13
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L167:
	.long	.L173-.L167
	.long	.L172-.L167
	.long	.L171-.L167
	.long	.L266-.L167
	.long	.L169-.L167
	.long	.L168-.L167
	.long	.L166-.L167
	.text
	.p2align 4,,10
	.p2align 3
.L168:
	addq	%r15, %r15
	movl	$1, %ebx
	cmovne	%r15, %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L542
.L257:
	testq	%r15, %r15
	je	.L265
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE3hex(%rip), %r9
	xorl	%edi, %edi
	.p2align 4,,10
	.p2align 3
.L259:
	movl	%edx, %ecx
	addl	$1, %edx
	movzbl	(%r14,%rcx), %ecx
	movq	%rcx, %r8
	andl	$15, %ecx
	shrq	$4, %r8
	movzbl	(%r9,%rcx), %ecx
	andl	$15, %r8d
	movzbl	(%r9,%r8), %r8d
	movb	%r8b, (%rsi,%rax)
	leal	1(%rdi), %eax
	movb	%cl, (%rsi,%rax)
	leal	2(%rdi), %eax
	movq	%rax, %rdi
	cmpq	%rax, %r15
	ja	.L259
.L265:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE3NewEPNS2_7IsolateEPcmPNS2_5LocalINS2_5ValueEEE
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%r15, %rdx
	call	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L536
.L288:
	movq	%rbx, %rax
.L180:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L543
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	cmpq	$15, %r15
	ja	.L181
	testq	%r15, %r15
	je	.L466
	cmpb	$0, (%rsi)
	js	.L183
	cmpq	$1, %r15
	je	.L184
	cmpb	$0, 1(%rsi)
	js	.L183
	cmpq	$2, %r15
	je	.L184
	cmpb	$0, 2(%rsi)
	js	.L183
	cmpq	$3, %r15
	je	.L184
	cmpb	$0, 3(%rsi)
	js	.L183
	cmpq	$4, %r15
	je	.L184
	cmpb	$0, 4(%rsi)
	js	.L183
	cmpq	$5, %r15
	je	.L184
	cmpb	$0, 5(%rsi)
	js	.L183
	cmpq	$6, %r15
	je	.L184
	cmpb	$0, 6(%rsi)
	js	.L183
	cmpq	$7, %r15
	je	.L184
	cmpb	$0, 7(%rsi)
	js	.L183
	cmpq	$8, %r15
	je	.L184
	cmpb	$0, 8(%rsi)
	js	.L183
	cmpq	$9, %r15
	je	.L184
	cmpb	$0, 9(%rsi)
	js	.L183
	cmpq	$10, %r15
	je	.L184
	cmpb	$0, 10(%rsi)
	js	.L183
	cmpq	$11, %r15
	je	.L184
	cmpb	$0, 11(%rsi)
	js	.L183
	cmpq	$12, %r15
	je	.L184
	cmpb	$0, 12(%rsi)
	js	.L183
	cmpq	$13, %r15
	je	.L184
	cmpb	$0, 13(%rsi)
	js	.L183
	cmpq	$14, %r15
	je	.L184
	cmpb	$0, 14(%rsi)
	js	.L183
.L184:
	movl	%r15d, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L288
.L532:
	leaq	-192(%rbp), %r14
	movl	$1073741799, %r9d
	movl	$128, %ecx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %r8
	movl	$1, %edx
	movl	$128, %esi
	movq	%r14, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L544
.L309:
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
.L519:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L545
.L310:
	movq	%r14, %rdi
	.p2align 4,,10
	.p2align 3
.L508:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L546
.L311:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L547
.L312:
	movq	%r12, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-200(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L548
.L313:
	movq	%r14, 0(%r13)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L172:
	movl	%r15d, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L288
.L540:
	leaq	-192(%rbp), %r14
	movl	$1073741799, %r9d
	movl	$128, %ecx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %r8
	movl	$1, %edx
	movl	$128, %esi
	movq	%r14, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L549
.L235:
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
.L513:
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	jne	.L508
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	2(%r15), %rdx
	movl	$1, %ebx
	movabsq	$-6148914691236517205, %rcx
	movq	%rdx, %rax
	mulq	%rcx
	shrq	%rdx
	salq	$2, %rdx
	cmovne	%rdx, %rbx
	movq	%rdx, -200(%rbp)
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L550
.L246:
	movabsq	$-6148914691236517205, %rdx
	movq	%r15, %rax
	mulq	%rdx
	shrq	%rdx
	leal	(%rdx,%rdx,2), %r11d
	testl	%r11d, %r11d
	je	.L328
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	leaq	_ZZN4nodeL13base64_encodeEPKcmPcmE5table(%rip), %r9
	.p2align 4,,10
	.p2align 3
.L253:
	movzbl	(%r14,%rdx), %edi
	leal	1(%rdx), %eax
	movl	%r8d, %ebx
	leal	2(%rdx), %ecx
	movzbl	(%r14,%rax), %eax
	movzbl	(%r14,%rcx), %ecx
	movl	%edi, %r10d
	sall	$4, %edi
	shrl	$2, %r10d
	andl	$48, %edi
	movzbl	(%r9,%r10), %r10d
	movb	%r10b, (%rsi,%rbx)
	movl	%eax, %ebx
	leal	1(%r8), %r10d
	sall	$2, %eax
	shrl	$4, %ebx
	andl	$60, %eax
	orl	%ebx, %edi
	movzbl	(%r9,%rdi), %edi
	movb	%dil, (%rsi,%r10)
	movl	%ecx, %r10d
	andl	$63, %ecx
	leal	2(%r8), %edi
	shrl	$6, %r10d
	movzbl	(%r9,%rcx), %ecx
	andl	$3, %r10d
	orl	%r10d, %eax
	movzbl	(%r9,%rax), %eax
	movb	%al, (%rsi,%rdi)
	leal	3(%r8), %eax
	addl	$4, %r8d
	movb	%cl, (%rsi,%rax)
	leal	3(%rdx), %ecx
	addq	$3, %rdx
	cmpl	%edx, %r11d
	ja	.L253
.L252:
	subq	%r11, %r15
	cmpq	$1, %r15
	je	.L254
	cmpq	$2, %r15
	je	.L255
.L256:
	movq	-200(%rbp), %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE3NewEPNS2_7IsolateEPcmPNS2_5LocalINS2_5ValueEEE
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L169:
	testq	%r15, %r15
	je	.L466
	cmpq	$1031912, %r15
	jbe	.L551
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L552
.L240:
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	%rbx, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE3NewEPNS2_7IsolateEPcmPNS2_5LocalINS2_5ValueEEE
	movq	%rax, %rbx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L266:
	testb	$1, %sil
	jne	.L553
	shrq	%r15
	je	.L466
	cmpq	$1031912, %r15
	ja	.L289
	movl	%r15d, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L288
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	128(%r12), %rbx
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L181:
	movl	%esi, %ebx
	andl	$7, %ebx
	jne	.L554
	movq	%r15, %rdi
.L185:
	movabsq	$-9187201950435737472, %rcx
	movq	%rdi, %rdx
	movq	%rsi, %rax
	andq	$-8, %rdx
	addq	%rsi, %rdx
	.p2align 4,,10
	.p2align 3
.L189:
	testq	%rcx, (%rax)
	jne	.L186
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L189
	movl	%edi, %eax
	andl	$7, %eax
	je	.L190
	movl	%eax, %eax
	movq	%rdi, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	leaq	(%rsi,%rdi), %rdx
	addq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L191:
	cmpb	$0, (%rax)
	js	.L186
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L191
.L190:
	cmpq	$1031912, %r15
	jbe	.L184
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L555
.L215:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memcpy@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcEE(%rip), %rax
	movq	%rbx, 16(%r14)
	movq	%r14, %rsi
	movq	%r15, 24(%r14)
	movq	%rax, (%r14)
	movq	%r12, 8(%r14)
	call	_ZN2v86String18NewExternalOneByteEPNS_7IsolateEPNS0_29ExternalOneByteStringResourceE@PLT
	movq	32(%r12), %r15
	movq	%rax, %rbx
	movq	24(%r14), %rax
	addq	%rax, %r15
	movq	%r15, %rdx
	subq	48(%r12), %rdx
	movq	%r15, 32(%r12)
	cmpq	$33554432, %rdx
	jg	.L556
.L221:
	testq	%rax, %rax
	js	.L557
	je	.L223
	cmpq	40(%r12), %r15
	jg	.L558
.L223:
	testq	%rbx, %rbx
	jne	.L288
	movq	%r14, %rdi
	call	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED1Ev
	movl	$32, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	%r12, %rdi
	call	_ZN4node19ERR_STRING_TOO_LONGEPN2v87IsolateE
	movq	%rax, 0(%r13)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L255:
	movl	%ecx, %eax
	leal	1(%rcx), %edx
	leaq	_ZZN4nodeL13base64_encodeEPKcmPcmE5table(%rip), %rcx
	movl	%r8d, %r9d
	movzbl	(%r14,%rax), %eax
	movzbl	(%r14,%rdx), %edi
	movl	%eax, %edx
	sall	$4, %eax
	shrl	$2, %edx
	andl	$48, %eax
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, (%rsi,%r9)
	movl	%edi, %r9d
	leal	1(%r8), %edx
	shrl	$4, %r9d
	orl	%r9d, %eax
	movzbl	(%rcx,%rax), %eax
	movb	%al, (%rsi,%rdx)
	leal	0(,%rdi,4), %eax
	leal	2(%r8), %edx
	andl	$60, %eax
	movzbl	(%rcx,%rax), %eax
	movb	%al, (%rsi,%rdx)
	leal	3(%r8), %eax
	movb	$61, (%rsi,%rax)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L553:
	testq	%r15, %r15
	movl	$1, %ebx
	cmovne	%r15, %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L559
.L281:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r8, %rdi
	call	memcpy@PLT
	movq	%r15, %rdx
	movq	%r13, %rcx
	movq	%r12, %rdi
	shrq	%rdx
	movq	%rax, %rsi
	call	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE3NewEPNS2_7IsolateEPtmPNS2_5LocalINS2_5ValueEEE
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L551:
	movl	%r15d, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L288
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L554:
	movl	$8, %ecx
	movq	%rsi, %rax
	subl	%ebx, %ecx
	leaq	(%rcx,%rsi), %rdx
	.p2align 4,,10
	.p2align 3
.L188:
	cmpb	$0, (%rax)
	js	.L186
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L188
	movq	%r15, %rdi
	movq	%rdx, %rsi
	subq	%rcx, %rdi
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L186:
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L560
.L199:
	testl	%ebx, %ebx
	jne	.L561
	movq	%r15, %rdi
	movq	%r15, %rdx
	movq	%r15, %rcx
	xorl	%eax, %eax
	movdqa	.LC6(%rip), %xmm1
	shrq	$3, %rdi
	shrq	$4, %rdx
	andq	$-16, %rcx
	.p2align 4,,10
	.p2align 3
.L203:
	movdqu	(%r14,%rax), %xmm0
	pand	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L203
	leaq	(%rdx,%rdx), %rax
	cmpq	%rax, %rdi
	je	.L204
	movq	%rdx, %rax
	movabsq	$9187201950435737471, %rdx
	salq	$4, %rax
	andq	(%r14,%rax), %rdx
	movq	%rdx, (%rsi,%rax)
.L204:
	movl	%r15d, %ecx
	andl	$7, %ecx
	je	.L265
	movl	%ecx, %edx
	movq	%r15, %rax
	subq	%rdx, %rax
	movzbl	(%r14,%rax), %edi
	andl	$127, %edi
	movb	%dil, (%rsi,%rax)
	cmpl	$1, %ecx
	je	.L265
	movzbl	1(%r14,%rax), %ecx
	andl	$127, %ecx
	movb	%cl, 1(%rsi,%rax)
	cmpq	$2, %rdx
	je	.L265
	movzbl	2(%r14,%rax), %ecx
	andl	$127, %ecx
	movb	%cl, 2(%rsi,%rax)
	cmpq	$3, %rdx
	je	.L265
	movzbl	3(%r14,%rax), %ecx
	andl	$127, %ecx
	movb	%cl, 3(%rsi,%rax)
	cmpq	$4, %rdx
	je	.L265
	movzbl	4(%r14,%rax), %ecx
	andl	$127, %ecx
	movb	%cl, 4(%rsi,%rax)
	cmpq	$5, %rdx
	je	.L265
	movzbl	5(%r14,%rax), %ecx
	andl	$127, %ecx
	movb	%cl, 5(%rsi,%rax)
	cmpq	$6, %rdx
	je	.L265
	movzbl	6(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 6(%rsi,%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L550:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	jne	.L246
.L528:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L562
.L282:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L563
.L283:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L564
.L284:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L565
.L285:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L566
.L286:
	movq	%rbx, 0(%r13)
	xorl	%eax, %eax
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L542:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L528
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L552:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L240
	.p2align 4,,10
	.p2align 3
.L536:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L567
.L241:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L254:
	movzbl	(%r14,%rcx), %eax
	leaq	_ZZN4nodeL13base64_encodeEPKcmPcmE5table(%rip), %rcx
	movl	%r8d, %edi
	movl	%eax, %edx
	sall	$4, %eax
	shrl	$2, %edx
	andl	$48, %eax
	movzbl	(%rcx,%rdx), %edx
	movzbl	(%rcx,%rax), %eax
	movb	%dl, (%rsi,%rdi)
	leal	1(%r8), %edx
	movb	%al, (%rsi,%rdx)
	leal	2(%r8), %eax
	movb	$61, (%rsi,%rax)
	leal	3(%r8), %eax
	movb	$61, (%rsi,%rax)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L560:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L528
	movl	%r14d, %ebx
	andl	$7, %ebx
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L183:
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L568
.L319:
	movzbl	(%r14), %eax
	andl	$127, %eax
	movb	%al, (%rsi)
	cmpq	$1, %r15
	je	.L265
	movzbl	1(%r14), %eax
	andl	$127, %eax
	movb	%al, 1(%rsi)
	cmpq	$2, %r15
	je	.L265
	movzbl	2(%r14), %eax
	andl	$127, %eax
	movb	%al, 2(%rsi)
	cmpq	$3, %r15
	je	.L265
	movzbl	3(%r14), %eax
	andl	$127, %eax
	movb	%al, 3(%rsi)
	cmpq	$4, %r15
	je	.L265
	movzbl	4(%r14), %eax
	andl	$127, %eax
	movb	%al, 4(%rsi)
	cmpq	$5, %r15
	je	.L265
	movzbl	5(%r14), %eax
	andl	$127, %eax
	movb	%al, 5(%rsi)
	cmpq	$6, %r15
	je	.L265
	movzbl	6(%r14), %eax
	andl	$127, %eax
	movb	%al, 6(%rsi)
	cmpq	$7, %r15
	je	.L265
	movzbl	7(%r14), %eax
	andl	$127, %eax
	movb	%al, 7(%rsi)
	cmpq	$8, %r15
	je	.L265
	movzbl	8(%r14), %eax
	andl	$127, %eax
	movb	%al, 8(%rsi)
	cmpq	$9, %r15
	je	.L265
	movzbl	9(%r14), %eax
	andl	$127, %eax
	movb	%al, 9(%rsi)
	cmpq	$10, %r15
	je	.L265
	movzbl	10(%r14), %eax
	andl	$127, %eax
	movb	%al, 10(%rsi)
	cmpq	$11, %r15
	je	.L265
	movzbl	11(%r14), %eax
	andl	$127, %eax
	movb	%al, 11(%rsi)
	cmpq	$12, %r15
	je	.L265
	movzbl	12(%r14), %eax
	andl	$127, %eax
	movb	%al, 12(%rsi)
	cmpq	$13, %r15
	je	.L265
	movzbl	13(%r14), %eax
	andl	$127, %eax
	movb	%al, 13(%rsi)
	cmpq	$14, %r15
	je	.L265
	movzbl	14(%r14), %eax
	andl	$127, %eax
	movb	%al, 14(%rsi)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L561:
	testq	%r15, %r15
	movl	$1, %edx
	movdqa	.LC5(%rip), %xmm1
	cmovne	%r15, %rdx
	xorl	%eax, %eax
	movq	%rdx, %rcx
	andq	$-16, %rcx
.L201:
	movdqu	(%r14,%rax), %xmm0
	pand	%xmm1, %xmm0
	movups	%xmm0, (%rsi,%rax)
	addq	$16, %rax
	cmpq	%rax, %rcx
	jne	.L201
	movq	%rdx, %rax
	andq	$-16, %rax
	andl	$15, %edx
	je	.L265
	movzbl	(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, (%rsi,%rax)
	leaq	1(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	1(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 1(%rsi,%rax)
	leaq	2(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	2(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 2(%rsi,%rax)
	leaq	3(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	3(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 3(%rsi,%rax)
	leaq	4(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	4(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 4(%rsi,%rax)
	leaq	5(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	5(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 5(%rsi,%rax)
	leaq	6(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	6(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 6(%rsi,%rax)
	leaq	7(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	7(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 7(%rsi,%rax)
	leaq	8(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	8(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 8(%rsi,%rax)
	leaq	9(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	9(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 9(%rsi,%rax)
	leaq	10(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	10(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 10(%rsi,%rax)
	leaq	11(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	11(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 11(%rsi,%rax)
	leaq	12(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	12(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 12(%rsi,%rax)
	leaq	13(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	13(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 13(%rsi,%rax)
	leaq	14(%rax), %rdx
	cmpq	%rdx, %r15
	jbe	.L265
	movzbl	14(%r14,%rax), %edx
	andl	$127, %edx
	movb	%dl, 14(%rsi,%rax)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L565:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L564:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L563:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L562:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L566:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L286
.L559:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %r8
	testq	%rax, %rax
	je	.L528
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L328:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L252
.L289:
	leaq	(%r15,%r15), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -200(%rbp)
	call	malloc@PLT
	movq	-200(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L569
.L296:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	leaq	16+_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtEE(%rip), %r14
	call	memcpy@PLT
	movl	$32, %edi
	call	_Znwm@PLT
	movq	%r12, %rdi
	movq	%rbx, 16(%rax)
	movq	%rax, %rsi
	movq	%r15, 24(%rax)
	movq	%r14, (%rax)
	movq	%r12, 8(%rax)
	movq	%rax, -200(%rbp)
	call	_ZN2v86String18NewExternalTwoByteEPNS_7IsolateEPNS0_22ExternalStringResourceE@PLT
	movq	-200(%rbp), %r8
	movq	%rax, %rbx
	movq	24(%r8), %rax
	leaq	(%rax,%rax), %r15
	movq	32(%r12), %rax
	addq	%r15, %rax
	movq	%rax, %rdx
	subq	48(%r12), %rdx
	movq	%rax, 32(%r12)
	cmpq	$33554432, %rdx
	jg	.L570
.L302:
	testq	%r15, %r15
	js	.L571
	je	.L304
	cmpq	40(%r12), %rax
	jg	.L572
.L304:
	testq	%rbx, %rbx
	jne	.L288
	movq	%r14, (%r8)
	movq	16(%r8), %rdi
	movq	%r8, -200(%rbp)
	call	free@PLT
	movq	-200(%rbp), %r8
	movq	8(%r8), %r14
	movq	24(%r8), %r15
	movq	32(%r14), %rax
	addq	%r15, %r15
	subq	%r15, %rax
	movq	%rax, %rdx
	subq	48(%r14), %rdx
	movq	%rax, 32(%r14)
	cmpq	$33554432, %rdx
	jg	.L573
.L306:
	testq	%r15, %r15
	jg	.L574
	je	.L308
	cmpq	40(%r14), %rax
	jg	.L575
.L308:
	movl	$32, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdx
	jmp	.L312
.L546:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L311
.L548:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L313
.L555:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L536
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L567:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L241
.L549:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L235
.L557:
	addq	40(%r12), %rax
	cmpq	$67108864, %rax
	jle	.L223
	movq	%rax, 40(%r12)
	jmp	.L223
.L556:
	movq	%r12, %rdi
	movq	%rax, -200(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-200(%rbp), %rax
	jmp	.L221
.L568:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L528
	jmp	.L319
.L569:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-200(%rbp), %rdx
	movq	%rdx, %rdi
	call	malloc@PLT
	movq	-200(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L296
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L576
.L297:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L519
.L545:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L310
.L571:
	addq	40(%r12), %r15
	cmpq	$67108864, %r15
	jle	.L304
	movq	%r15, 40(%r12)
	jmp	.L304
.L165:
	leaq	_ZZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L570:
	movq	%r12, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %r8
	jmp	.L302
.L544:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L309
.L543:
	call	__stack_chk_fail@PLT
.L558:
	movq	%r12, %rdi
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L223
.L576:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L297
.L572:
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	movq	-200(%rbp), %r8
	jmp	.L304
.L574:
	movq	40(%r14), %rax
	subq	%r15, %rax
	cmpq	$67108864, %rax
	jle	.L308
	movq	%rax, 40(%r14)
	jmp	.L308
.L573:
	movq	%r14, %rdi
	movq	%rax, -208(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-208(%rbp), %rax
	movq	-200(%rbp), %r8
	jmp	.L306
.L575:
	movq	%r14, %rdi
	movq	%r8, -200(%rbp)
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	movq	-200(%rbp), %r8
	jmp	.L308
	.cfi_endproc
.LFE9258:
	.size	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE.part.0, .-_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE.part.0
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPm
	.type	_ZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPm, @function
_ZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPm:
.LFB7252:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	%r9, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	shrq	%rbx
	je	.L583
	movq	%rdi, %r15
	movq	%rsi, %r12
	movq	%rcx, %r13
	movl	%r8d, %r14d
	testb	$1, %sil
	je	.L591
	leaq	1(%rsi), %rax
	movq	%rax, -80(%rbp)
	testb	$1, %sil
	je	.L592
	movq	%rcx, %rdi
	call	_ZNK2v86String6LengthEv@PLT
	cltq
	cmpq	%rax, %rbx
	ja	.L593
.L582:
	xorl	%ecx, %ecx
	movl	%r14d, %r9d
	leal	-1(%rbx), %r8d
	movq	%r15, %rsi
	leaq	1(%r12), %rdx
	movq	%r13, %rdi
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	leaq	-1(%rbx), %r11
	movl	%eax, %ecx
	cltq
	cmpq	%rax, %r11
	jne	.L594
	leaq	(%r11,%r11), %r10
	leaq	1(%r12), %rsi
	movq	%r12, %rdi
	movq	%r11, -88(%rbp)
	movq	%r10, %rdx
	movq	%r10, -80(%rbp)
	movl	%ecx, -92(%rbp)
	call	memmove@PLT
	movl	-92(%rbp), %ecx
	leaq	-58(%rbp), %rdx
	movl	%r14d, %r9d
	movl	$1, %r8d
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %r11
	cmpl	$1, %eax
	jne	.L595
	movzwl	-58(%rbp), %eax
	movw	%ax, (%r12,%r11,2)
	movq	-72(%rbp), %rax
	movq	%rbx, (%rax)
	leaq	2(%r10), %rax
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L593:
	testq	%rax, %rax
	jne	.L596
.L583:
	xorl	%eax, %eax
.L577:
	movq	-56(%rbp), %rsi
	xorq	%fs:40, %rsi
	jne	.L597
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	leaq	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L591:
	movq	%rsi, %rdx
	movl	%r8d, %r9d
	movq	%rdi, %rsi
	movl	%ebx, %r8d
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZNK2v86String5WriteEPNS_7IsolateEPtiii@PLT
	movq	-72(%rbp), %rdx
	cltq
	movq	%rax, (%rdx)
	addq	%rax, %rax
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L595:
	leaq	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L594:
	leaq	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L597:
	call	__stack_chk_fail@PLT
.L596:
	movq	%rax, %rbx
	jmp	.L582
	.cfi_endproc
.LFE7252:
	.size	_ZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPm, .-_ZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPm
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes13IsValidStringEN2v85LocalINS1_6StringEEENS_8encodingE
	.type	_ZN4node11StringBytes13IsValidStringEN2v85LocalINS1_6StringEEENS_8encodingE, @function
_ZN4node11StringBytes13IsValidStringEN2v85LocalINS1_6StringEEENS_8encodingE:
.LFB7254:
	.cfi_startproc
	endbr64
	cmpl	$5, %esi
	je	.L606
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L606:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZNK2v86String6LengthEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	notl	%eax
	andl	$1, %eax
	ret
	.cfi_endproc
.LFE7254:
	.size	_ZN4node11StringBytes13IsValidStringEN2v85LocalINS1_6StringEEENS_8encodingE, .-_ZN4node11StringBytes13IsValidStringEN2v85LocalINS1_6StringEEENS_8encodingE
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE
	.type	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE, @function
_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE:
.LFB7255:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L608
	movl	%ebx, %eax
	andl	$-3, %eax
	cmpl	$4, %eax
	je	.L629
.L608:
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L630
	cmpl	$6, %ebx
	ja	.L619
	leaq	.L620(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L620:
	.long	.L612-.L620
	.long	.L614-.L620
	.long	.L616-.L620
	.long	.L615-.L620
	.long	.L612-.L620
	.long	.L617-.L620
	.long	.L614-.L620
	.text
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%r12, %rdi
	movl	$1, %ebx
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r12
.L609:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L631
	addq	$48, %rsp
	movl	%ebx, %eax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L630:
	.cfi_restore_state
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L614:
	call	_ZNK2v86String6LengthEv@PLT
	leal	(%rax,%rax,2), %r12d
	movslq	%r12d, %r12
.L613:
	movl	$1, %ebx
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L612:
	call	_ZNK2v86String6LengthEv@PLT
	movslq	%eax, %r12
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L617:
	movq	%rdi, -72(%rbp)
	call	_ZNK2v86String6LengthEv@PLT
	movq	-72(%rbp), %rdi
	testb	$1, %al
	jne	.L632
	call	_ZNK2v86String6LengthEv@PLT
	movl	%eax, %edx
	shrl	$31, %edx
	leal	(%rdx,%rax), %r12d
	sarl	%r12d
	movslq	%r12d, %r12
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L616:
	call	_ZNK2v86String6LengthEv@PLT
	xorl	%r12d, %r12d
	movslq	%eax, %rdx
	cmpq	$1, %rdx
	jbe	.L613
	andl	$3, %eax
	shrq	$2, %rdx
	leaq	1(%rax), %r12
	movq	%r12, %rax
	leaq	(%rdx,%rdx,2), %r12
	shrq	%rax
	addq	%rax, %r12
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L615:
	call	_ZNK2v86String6LengthEv@PLT
	movslq	%eax, %r12
	addq	%r12, %r12
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L631:
	call	__stack_chk_fail@PLT
.L619:
	leaq	_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7255:
	.size	_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE, .-_ZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE
	.type	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE, @function
_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE:
.LFB7256:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movq	%rdi, %rsi
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%edx, %ebx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN4node6Buffer11HasInstanceEN2v85LocalINS1_5ValueEEE@PLT
	testb	%al, %al
	je	.L634
	movl	%ebx, %eax
	andl	$-3, %eax
	cmpl	$4, %eax
	je	.L657
.L634:
	movq	%r13, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L658
	cmpl	$6, %ebx
	ja	.L646
	leaq	.L647(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L647:
	.long	.L638-.L647
	.long	.L639-.L647
	.long	.L641-.L647
	.long	.L640-.L647
	.long	.L638-.L647
	.long	.L645-.L647
	.long	.L639-.L647
	.text
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%r12, %rdi
	movl	$1, %ebx
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%rax, %r12
.L635:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L659
	addq	$56, %rsp
	movl	%ebx, %eax
	movq	%r12, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L658:
	.cfi_restore_state
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L639:
	movq	%r13, %rsi
	movl	$1, %ebx
	call	_ZNK2v86String10Utf8LengthEPNS_7IsolateE@PLT
	movslq	%eax, %r12
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L638:
	call	_ZNK2v86String6LengthEv@PLT
	movl	$1, %ebx
	movslq	%eax, %r12
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L645:
	call	_ZNK2v86String6LengthEv@PLT
	movl	$1, %ebx
	movl	%eax, %edx
	shrl	$31, %edx
	leal	(%rdx,%rax), %r12d
	sarl	%r12d
	movslq	%r12d, %r12
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L641:
	leaq	-96(%rbp), %r15
	movq	%rdi, %rdx
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	movq	%r15, %rdi
	call	_ZN2v86String5ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movslq	-88(%rbp), %rax
	cmpq	$1, %rax
	jbe	.L642
	movq	-96(%rbp), %rdx
	cmpw	$61, -2(%rdx,%rax,2)
	je	.L660
.L643:
	movq	%rax, %r12
	shrq	$2, %rax
	andl	$3, %r12d
	leaq	(%rax,%rax,2), %rdx
	addq	$1, %r12
	shrq	%r12
	addq	%rdx, %r12
.L642:
	movq	%r15, %rdi
	movl	$1, %ebx
	call	_ZN2v86String5ValueD1Ev@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L640:
	call	_ZNK2v86String6LengthEv@PLT
	movl	$1, %ebx
	cltq
	leaq	(%rax,%rax), %r12
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L660:
	cmpw	$61, -4(%rdx,%rax,2)
	leaq	-1(%rax), %rcx
	jne	.L644
	leaq	-2(%rax), %rcx
.L644:
	cmpq	$1, %rcx
	ja	.L661
	xorl	%r12d, %r12d
	jmp	.L642
.L659:
	call	__stack_chk_fail@PLT
.L646:
	leaq	_ZZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L661:
	movq	%rcx, %rax
	jmp	.L643
	.cfi_endproc
.LFE7256:
	.size	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE, .-_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes10hex_encodeEPKcmPcm
	.type	_ZN4node11StringBytes10hex_encodeEPKcmPcm, @function
_ZN4node11StringBytes10hex_encodeEPKcmPcm:
.LFB7261:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	(%rsi,%rsi), %r12
	subq	$8, %rsp
	cmpq	%rcx, %r12
	ja	.L671
	testq	%r12, %r12
	je	.L662
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	leaq	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE3hex(%rip), %r11
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L665:
	movl	%r10d, %eax
	addl	$1, %r10d
	movzbl	(%rdi,%rax), %eax
	movq	%rax, %r8
	andl	$15, %eax
	shrq	$4, %r8
	movzbl	(%r11,%rax), %eax
	andl	$15, %r8d
	movzbl	(%r11,%r8), %r8d
	movb	%r8b, (%rdx,%r9)
	leal	1(%rcx), %r8d
	leal	2(%rcx), %r9d
	movb	%al, (%rdx,%r8)
	movq	%r9, %rcx
	cmpq	%r9, %r12
	ja	.L665
.L662:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L671:
	.cfi_restore_state
	leaq	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7261:
	.size	_ZN4node11StringBytes10hex_encodeEPKcmPcm, .-_ZN4node11StringBytes10hex_encodeEPKcmPcm
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes10hex_encodeB5cxx11EPKcm
	.type	_ZN4node11StringBytes10hex_encodeB5cxx11EPKcm, @function
_ZN4node11StringBytes10hex_encodeB5cxx11EPKcm:
.LFB7262:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	leaq	(%rdx,%rdx), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	movq	(%r12), %r8
	testq	%rbx, %rbx
	je	.L672
	xorl	%edi, %edi
	xorl	%esi, %esi
	leaq	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE3hex(%rip), %r9
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L674:
	movl	%edi, %eax
	addl	$1, %edi
	movzbl	0(%r13,%rax), %eax
	movq	%rax, %rcx
	andl	$15, %eax
	shrq	$4, %rcx
	movzbl	(%r9,%rax), %eax
	andl	$15, %ecx
	movzbl	(%r9,%rcx), %ecx
	movb	%cl, (%r8,%rsi)
	leal	1(%rdx), %ecx
	leal	2(%rdx), %esi
	movb	%al, (%r8,%rcx)
	movq	%rsi, %rdx
	cmpq	%rsi, %rbx
	ja	.L674
.L672:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7262:
	.size	_ZN4node11StringBytes10hex_encodeB5cxx11EPKcm, .-_ZN4node11StringBytes10hex_encodeB5cxx11EPKcm
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"Cannot create a Buffer larger than 0x%zx bytes"
	.section	.rodata.str1.1
.LC9:
	.string	"ERR_BUFFER_TOO_LARGE"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE
	.type	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE, @function
_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE:
.LFB7263:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$2147483647, %rdx
	ja	.L697
	testq	%rdx, %rdx
	jne	.L688
	leaq	128(%rdi), %rax
	cmpl	$6, %ecx
	je	.L688
.L687:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L698
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	movq	%rbx, %r8
	movq	%r12, %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE.part.0
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L697:
	movl	$2147483647, %r9d
	movl	$128, %ecx
	movl	$1, %edx
	movl	$128, %esi
	leaq	-192(%rbp), %r13
	leaq	.LC8(%rip), %r8
	movq	%r13, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L699
.L682:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L700
.L683:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L701
.L684:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L702
.L685:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L703
.L686:
	movq	%r13, (%rbx)
	xorl	%eax, %eax
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L699:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L700:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L683
	.p2align 4,,10
	.p2align 3
.L701:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L702:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L685
	.p2align 4,,10
	.p2align 3
.L703:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L686
.L698:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7263:
	.size	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE, .-_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKtmPNS1_5LocalINS1_5ValueEEE
	.type	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKtmPNS1_5LocalINS1_5ValueEEE, @function
_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKtmPNS1_5LocalINS1_5ValueEEE:
.LFB7264:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$2147483647, %rdx
	ja	.L772
	movq	%rdx, %r13
	leaq	128(%rdi), %r14
	testq	%rdx, %rdx
	jne	.L773
.L745:
	movq	%r14, %rax
.L711:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L774
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	.cfi_restore_state
	movl	$2147483647, %r9d
	movl	$128, %ecx
	movl	$1, %edx
	movl	$128, %esi
	leaq	-192(%rbp), %r13
	leaq	.LC8(%rip), %r8
	movq	%r13, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L775
.L706:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L776
.L707:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L777
.L708:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L778
.L709:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L779
.L710:
	movq	%r13, (%r15)
	xorl	%eax, %eax
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L773:
	cmpq	$1031912, %rdx
	ja	.L746
	movl	%edx, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromTwoByteEPNS_7IsolateEPKtNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L745
	leaq	-192(%rbp), %r13
	movl	$1073741799, %r9d
	movl	$128, %ecx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %r8
	movl	$1, %edx
	movl	$128, %esi
	movq	%r13, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L780
.L748:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L781
.L749:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L782
.L750:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L783
.L751:
	movq	%r12, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-200(%rbp), %rdx
	movq	%rbx, %rcx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L784
.L752:
	movq	%r13, (%r15)
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L746:
	leaq	(%rdx,%rdx), %rbx
	movq	%rsi, -200(%rbp)
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	-200(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L785
.L753:
	movq	%r14, %rdi
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	%r14, %rsi
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE3NewEPNS2_7IsolateEPtmPNS2_5LocalINS2_5ValueEEE
	movq	%rax, %r14
	jmp	.L745
	.p2align 4,,10
	.p2align 3
.L775:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L776:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L777:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L708
	.p2align 4,,10
	.p2align 3
.L778:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L779:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L710
.L785:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	-200(%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L753
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L786
.L754:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L787
.L755:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L788
.L756:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L789
.L757:
	movq	%r12, %rdi
	movq	%rdx, -200(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-200(%rbp), %rdx
	movq	%r13, %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L790
.L758:
	movq	%rbx, (%r15)
	jmp	.L745
.L786:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L754
.L790:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L758
.L789:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdx
	jmp	.L757
.L788:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L756
.L787:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L755
.L784:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L752
.L783:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdx
	jmp	.L751
.L782:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L750
.L781:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L749
.L780:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L748
.L774:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7264:
	.size	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKtmPNS1_5LocalINS1_5ValueEEE, .-_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKtmPNS1_5LocalINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE
	.type	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE, @function
_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE:
.LFB7265:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%rsi, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strlen@PLT
	cmpq	$2147483647, %rax
	ja	.L808
	testq	%rax, %rax
	jne	.L799
	cmpl	$6, %r13d
	je	.L799
	leaq	128(%r12), %rax
.L798:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L809
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L799:
	.cfi_restore_state
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE.part.0
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L808:
	leaq	-192(%rbp), %r13
	movl	$2147483647, %r9d
	movl	$128, %ecx
	xorl	%eax, %eax
	leaq	.LC8(%rip), %r8
	movl	$1, %edx
	movl	$128, %esi
	movq	%r13, %rdi
	call	__snprintf_chk@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	.LC9(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L810
.L793:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L811
.L794:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L812
.L795:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L813
.L796:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L814
.L797:
	movq	%r13, (%rbx)
	xorl	%eax, %eax
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L810:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L793
	.p2align 4,,10
	.p2align 3
.L811:
	movq	%rax, -200(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-200(%rbp), %rdi
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L812:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L813:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L814:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L797
.L809:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7265:
	.size	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE, .-_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcNS_8encodingEPNS1_5LocalINS1_5ValueEEE
	.section	.text._ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_,"axG",@progbits,_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_,comdat
	.p2align 4
	.weak	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
	.type	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_, @function
_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_:
.LFB8626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%r8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	_ZN4node14unbase64_tableE(%rip), %rbx
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L830:
	cmpq	%rcx, %rax
	jnb	.L820
	cmpb	$61, %r10b
	je	.L820
.L817:
	movzbl	(%rdx,%rax), %r11d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r11, %r10
	movzbl	(%rbx,%r11), %r11d
	cmpb	$63, %r11b
	ja	.L830
	cmpq	%rcx, %rax
	jnb	.L820
	cmpq	%rsi, (%r9)
	jb	.L822
	.p2align 4,,10
	.p2align 3
.L820:
	xorl	%eax, %eax
.L815:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	cmpq	%rcx, %rax
	jnb	.L820
	cmpb	$61, %r10b
	je	.L820
.L822:
	movzbl	(%rdx,%rax), %r12d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r12, %r10
	movzbl	(%rbx,%r12), %r12d
	cmpb	$63, %r12b
	ja	.L831
	movq	(%r9), %rax
	sall	$2, %r11d
	leaq	1(%rax), %r10
	movq	%r10, (%r9)
	movl	%r12d, %r10d
	sarl	$4, %r10d
	orl	%r10d, %r11d
	movb	%r11b, (%rdi,%rax)
	movq	(%r8), %rax
	cmpq	%rcx, %rax
	jnb	.L820
	cmpq	%rsi, (%r9)
	jb	.L824
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L832:
	cmpb	$61, %r10b
	je	.L820
	cmpq	%rcx, %rax
	jnb	.L820
.L824:
	movzbl	(%rdx,%rax), %r11d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r11, %r10
	movzbl	(%rbx,%r11), %r11d
	cmpb	$63, %r11b
	ja	.L832
	movq	(%r9), %r10
	sall	$4, %r12d
	leaq	1(%r10), %rax
	movq	%rax, (%r9)
	movl	%r11d, %eax
	sarl	$2, %eax
	orl	%r12d, %eax
	movb	%al, (%rdi,%r10)
	movq	(%r8), %rax
	cmpq	%rcx, %rax
	jnb	.L820
	cmpq	%rsi, (%r9)
	jb	.L826
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L833:
	cmpb	$61, %r12b
	je	.L820
	cmpq	%rcx, %rax
	jnb	.L820
.L826:
	movzbl	(%rdx,%rax), %r10d
	addq	$1, %rax
	movq	%rax, (%r8)
	movq	%r10, %r12
	movzbl	(%rbx,%r10), %r10d
	cmpb	$63, %r10b
	ja	.L833
	movq	(%r9), %rax
	sall	$6, %r11d
	orl	%r11d, %r10d
	leaq	1(%rax), %rdx
	movq	%rdx, (%r9)
	movb	%r10b, (%rdi,%rax)
	cmpq	%rcx, (%r8)
	jnb	.L820
	cmpq	%rsi, (%r9)
	setb	%al
	jmp	.L815
	.cfi_endproc
.LFE8626:
	.size	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_, .-_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
	.section	.text._ZN4node18base64_decode_fastIcEEmPcmPKT_mm,"axG",@progbits,_ZN4node18base64_decode_fastIcEEmPcmPKT_mm,comdat
	.p2align 4
	.weak	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	.type	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm, @function
_ZN4node18base64_decode_fastIcEEmPcmPKT_mm:
.LFB8336:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZN4node14unbase64_tableE(%rip), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rsi
	movq	$0, -72(%rbp)
	cmovbe	%rsi, %r8
	movq	$0, -64(%rbp)
	movq	%r8, %rax
	xorl	%r8d, %r8d
	mulq	%rdx
	movq	%rdx, %r14
	andq	$-2, %rdx
	shrq	%r14
	addq	%rdx, %r14
	xorl	%edx, %edx
	andq	$-4, %rcx
	jne	.L836
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L837:
	movl	%eax, %edi
	movl	%eax, %r9d
	addq	$4, %rdx
	shrl	$22, %edi
	shrl	$20, %r9d
	movq	%rdx, -72(%rbp)
	andl	$3, %r9d
	andl	$-4, %edi
	orl	%r9d, %edi
	movl	%eax, %r9d
	movb	%dil, 0(%r13,%r8)
	movl	%eax, %edi
	shrl	$10, %r9d
	shrl	$12, %edi
	andl	$15, %r9d
	andl	$-16, %edi
	orl	%r9d, %edi
	movb	%dil, 1(%r13,%r8)
	movl	%eax, %edi
	andl	$63, %eax
	shrl	$2, %edi
	andl	$-64, %edi
	orl	%edi, %eax
	movb	%al, 2(%r13,%r8)
	addq	$3, %r8
	movq	%r8, -64(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L835
.L836:
	cmpq	%r14, %r8
	jnb	.L835
	movzbl	(%r12,%rdx), %eax
	movzbl	3(%r12,%rdx), %edi
	movsbl	(%rbx,%rax), %eax
	movsbl	(%rbx,%rdi), %edi
	sall	$24, %eax
	orl	%edi, %eax
	movzbl	1(%r12,%rdx), %edi
	movsbl	(%rbx,%rdi), %edi
	sall	$16, %edi
	orl	%edi, %eax
	movzbl	2(%r12,%rdx), %edi
	movsbl	(%rbx,%rdi), %edi
	sall	$8, %edi
	orl	%edi, %eax
	testl	$-2139062144, %eax
	je	.L837
	leaq	-64(%rbp), %r9
	leaq	-72(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
	movq	-88(%rbp), %rsi
	testb	%al, %al
	je	.L845
	movq	-72(%rbp), %rdx
	movq	%r15, %rcx
	movq	-64(%rbp), %r8
	subq	%rdx, %rcx
	andq	$-4, %rcx
	addq	%rdx, %rcx
	cmpq	%rdx, %rcx
	ja	.L836
	.p2align 4,,10
	.p2align 3
.L835:
	cmpq	%rdx, %r15
	ja	.L846
.L834:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L847
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L846:
	.cfi_restore_state
	cmpq	%r8, %rsi
	jbe	.L834
	leaq	-64(%rbp), %r9
	leaq	-72(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node24base64_decode_group_slowIcEEbPcmPKT_mPmS5_
.L845:
	movq	-64(%rbp), %r8
	jmp	.L834
.L847:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8336:
	.size	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm, .-_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	.section	.text._ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_,"axG",@progbits,_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_,comdat
	.p2align 4
	.weak	_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_
	.type	_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_, @function
_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_:
.LFB8627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	(%r8), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	_ZN4node14unbase64_tableE(%rip), %rbx
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L878:
	cmpq	%rcx, %rax
	jnb	.L867
	cmpb	$61, %r10b
	je	.L867
.L850:
	movzbl	(%rdx,%rax,2), %r11d
	addq	$1, %rax
	movq	%r11, %r10
	movzbl	(%rbx,%r11), %r11d
	cmpb	$63, %r11b
	ja	.L878
	movq	%rax, (%r8)
	cmpq	%rcx, %rax
	jnb	.L877
	cmpq	%rsi, (%r9)
	jb	.L856
	.p2align 4,,10
	.p2align 3
.L877:
	xorl	%eax, %eax
.L848:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	.cfi_restore_state
	movq	%rax, (%r8)
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L879:
	cmpq	%rcx, %rax
	jnb	.L867
	cmpb	$61, %r10b
	je	.L867
.L856:
	movzbl	(%rdx,%rax,2), %r12d
	addq	$1, %rax
	movq	%r12, %r10
	movzbl	(%rbx,%r12), %r12d
	cmpb	$63, %r12b
	ja	.L879
	movq	%rax, (%r8)
	movq	(%r9), %rax
	sall	$2, %r11d
	leaq	1(%rax), %r10
	movq	%r10, (%r9)
	movl	%r12d, %r10d
	sarl	$4, %r10d
	orl	%r10d, %r11d
	movb	%r11b, (%rdi,%rax)
	movq	(%r8), %rax
	cmpq	%rcx, %rax
	jnb	.L877
	cmpq	%rsi, (%r9)
	jb	.L859
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L880:
	cmpb	$61, %r10b
	je	.L867
	cmpq	%rcx, %rax
	jnb	.L867
.L859:
	movzbl	(%rdx,%rax,2), %r11d
	addq	$1, %rax
	movq	%r11, %r10
	movzbl	(%rbx,%r11), %r11d
	cmpb	$63, %r11b
	ja	.L880
	movq	%rax, (%r8)
	movq	(%r9), %r10
	sall	$4, %r12d
	leaq	1(%r10), %rax
	movq	%rax, (%r9)
	movl	%r11d, %eax
	sarl	$2, %eax
	orl	%r12d, %eax
	movb	%al, (%rdi,%r10)
	movq	(%r8), %rax
	cmpq	%rcx, %rax
	jnb	.L877
	cmpq	%rsi, (%r9)
	jb	.L862
	jmp	.L877
	.p2align 4,,10
	.p2align 3
.L881:
	cmpb	$61, %r12b
	je	.L867
	cmpq	%rcx, %rax
	jnb	.L867
.L862:
	movzbl	(%rdx,%rax,2), %r10d
	addq	$1, %rax
	movq	%r10, %r12
	movzbl	(%rbx,%r10), %r10d
	cmpb	$63, %r10b
	ja	.L881
	movq	%rax, (%r8)
	movq	(%r9), %rax
	sall	$6, %r11d
	orl	%r11d, %r10d
	leaq	1(%rax), %rdx
	movq	%rdx, (%r9)
	movb	%r10b, (%rdi,%rax)
	cmpq	%rcx, (%r8)
	jnb	.L877
	cmpq	%rsi, (%r9)
	setb	%al
	jmp	.L848
	.cfi_endproc
.LFE8627:
	.size	_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_, .-_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_
	.section	.text._ZN4node18base64_decode_fastItEEmPcmPKT_mm,"axG",@progbits,_ZN4node18base64_decode_fastItEEmPcmPKT_mm,comdat
	.p2align 4
	.weak	_ZN4node18base64_decode_fastItEEmPcmPKT_mm
	.type	_ZN4node18base64_decode_fastItEEmPcmPKT_mm, @function
_ZN4node18base64_decode_fastItEEmPcmPKT_mm:
.LFB8337:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	movabsq	$-6148914691236517205, %rdx
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	_ZN4node14unbase64_tableE(%rip), %rbx
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	%r8, %rsi
	movq	$0, -72(%rbp)
	cmovbe	%rsi, %r8
	movq	$0, -64(%rbp)
	movq	%r8, %rax
	xorl	%r8d, %r8d
	mulq	%rdx
	movq	%rdx, %r14
	andq	$-2, %rdx
	shrq	%r14
	addq	%rdx, %r14
	xorl	%edx, %edx
	andq	$-4, %rcx
	jne	.L884
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L885:
	movl	%eax, %edi
	movl	%eax, %r9d
	addq	$4, %rdx
	shrl	$22, %edi
	shrl	$20, %r9d
	movq	%rdx, -72(%rbp)
	andl	$3, %r9d
	andl	$-4, %edi
	orl	%r9d, %edi
	movl	%eax, %r9d
	movb	%dil, 0(%r13,%r8)
	movl	%eax, %edi
	shrl	$10, %r9d
	shrl	$12, %edi
	andl	$15, %r9d
	andl	$-16, %edi
	orl	%r9d, %edi
	movb	%dil, 1(%r13,%r8)
	movl	%eax, %edi
	andl	$63, %eax
	shrl	$2, %edi
	andl	$-64, %edi
	orl	%edi, %eax
	movb	%al, 2(%r13,%r8)
	addq	$3, %r8
	movq	%r8, -64(%rbp)
	cmpq	%rdx, %rcx
	jbe	.L883
.L884:
	cmpq	%r14, %r8
	jnb	.L883
	movzbl	(%r12,%rdx,2), %eax
	movzbl	6(%r12,%rdx,2), %edi
	movsbl	(%rbx,%rax), %eax
	movsbl	(%rbx,%rdi), %edi
	sall	$24, %eax
	orl	%edi, %eax
	movzbl	2(%r12,%rdx,2), %edi
	movsbl	(%rbx,%rdi), %edi
	sall	$16, %edi
	orl	%edi, %eax
	movzbl	4(%r12,%rdx,2), %edi
	movsbl	(%rbx,%rdi), %edi
	sall	$8, %edi
	orl	%edi, %eax
	testl	$-2139062144, %eax
	je	.L885
	leaq	-64(%rbp), %r9
	leaq	-72(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rsi, -88(%rbp)
	call	_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_
	movq	-88(%rbp), %rsi
	testb	%al, %al
	je	.L893
	movq	-72(%rbp), %rdx
	movq	%r15, %rcx
	movq	-64(%rbp), %r8
	subq	%rdx, %rcx
	andq	$-4, %rcx
	addq	%rdx, %rcx
	cmpq	%rdx, %rcx
	ja	.L884
	.p2align 4,,10
	.p2align 3
.L883:
	cmpq	%rdx, %r15
	ja	.L894
.L882:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L895
	addq	$56, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L894:
	.cfi_restore_state
	cmpq	%r8, %rsi
	jbe	.L882
	leaq	-64(%rbp), %r9
	leaq	-72(%rbp), %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	_ZN4node24base64_decode_group_slowItEEbPcmPKT_mPmS5_
.L893:
	movq	-64(%rbp), %r8
	jmp	.L882
.L895:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8337:
	.size	_ZN4node18base64_decode_fastItEEmPcmPKT_mm, .-_ZN4node18base64_decode_fastItEEmPcmPKT_mm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi
	.type	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi, @function
_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi:
.LFB7253:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movl	%r8d, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r9, %rbx
	subq	$88, %rsp
	movq	%rcx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, -128(%rbp)
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-120(%rbp), %r10
	leaq	-100(%rbp), %rax
	testq	%rbx, %rbx
	cmove	%rax, %rbx
	movq	(%r10), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L975
.L898:
	leaq	_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L975:
	movq	-1(%rax), %rax
	movq	-128(%rbp), %r11
	cmpw	$63, 11(%rax)
	ja	.L898
	cmpl	$6, %r12d
	ja	.L900
	leaq	.L902(%rip), %rdx
	movslq	(%rdx,%r12,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L902:
	.long	.L904-.L902
	.long	.L901-.L902
	.long	.L906-.L902
	.long	.L905-.L902
	.long	.L904-.L902
	.long	.L903-.L902
	.long	.L901-.L902
	.text
.L905:
	leaq	-96(%rbp), %r9
	movl	$11, %r8d
	movq	%r10, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r11, %rdi
	call	_ZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPm
	movq	%rax, %r12
	movq	-96(%rbp), %rax
	movl	%eax, (%rbx)
	jmp	.L913
.L903:
	movq	%r10, %rdi
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZNK2v86String17IsExternalOneByteEv@PLT
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r11
	testb	%al, %al
	je	.L935
	movq	%r10, %rdi
	call	_ZNK2v86String32GetExternalOneByteStringResourceEv@PLT
	leaq	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv(%rip), %rcx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L936
	movq	24(%r12), %rdx
.L937:
	movq	48(%rax), %rax
	leaq	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L938
	movq	16(%r12), %rax
.L939:
	testq	%r13, %r13
	je	.L959
	cmpq	$1, %rdx
	jbe	.L976
	leaq	-2(%rdx), %rdi
	xorl	%r12d, %r12d
	leaq	_ZN4nodeL11unhex_tableE(%rip), %rcx
	shrq	%rdi
	leaq	1(%rdi), %r8
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L945:
	cmpq	%rdi, %r12
	je	.L977
	movq	%rdx, %r12
.L942:
	movzbl	(%rax,%r12,2), %edx
	movzbl	1(%rax,%r12,2), %esi
	movsbl	(%rcx,%rdx), %edx
	movsbl	(%rcx,%rsi), %esi
	cmpl	$-1, %edx
	je	.L965
	cmpl	$-1, %esi
	je	.L965
	sall	$4, %edx
	orl	%esi, %edx
	movb	%dl, (%r14,%r12)
	leaq	1(%r12), %rdx
	cmpq	%r13, %rdx
	jne	.L945
	movl	%r13d, %r14d
	movq	%r13, %r12
	jmp	.L940
.L901:
	movl	$11, %r9d
	movq	%rbx, %r8
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	_ZNK2v86String9WriteUtf8EPNS_7IsolateEPciPii@PLT
	movslq	%eax, %r12
	.p2align 4,,10
	.p2align 3
.L913:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L978
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L904:
	.cfi_restore_state
	movq	%r10, %rdi
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZNK2v86String17IsExternalOneByteEv@PLT
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r11
	testb	%al, %al
	je	.L907
	movq	%r10, %rdi
	call	_ZNK2v86String32GetExternalOneByteStringResourceEv@PLT
	leaq	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv(%rip), %rcx
	movq	(%rax), %rdx
	movq	%rax, %rdi
	movq	56(%rdx), %rax
	cmpq	%rcx, %rax
	jne	.L908
	movq	24(%rdi), %rax
.L909:
	cmpq	%r13, %rax
	cmovbe	%rax, %r13
	movq	48(%rdx), %rax
	leaq	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv(%rip), %rdx
	movq	%r13, %r12
	cmpq	%rdx, %rax
	jne	.L910
	movq	16(%rdi), %rsi
.L911:
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	memcpy@PLT
	movl	%r12d, %eax
	movl	%eax, (%rbx)
	jmp	.L913
.L906:
	movq	%r10, %rdi
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	_ZNK2v86String17IsExternalOneByteEv@PLT
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r11
	testb	%al, %al
	je	.L923
	movq	%r10, %rdi
	call	_ZNK2v86String32GetExternalOneByteStringResourceEv@PLT
	leaq	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv(%rip), %rcx
	movq	%rax, %r12
	movq	(%rax), %rax
	movq	56(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L924
	movq	24(%r12), %rcx
.L925:
	movq	48(%rax), %rax
	leaq	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L926
	movq	16(%r12), %r9
.L927:
	xorl	%r8d, %r8d
	cmpq	$1, %rcx
	jbe	.L928
	cmpb	$61, -1(%r9,%rcx)
	leaq	-1(%rcx), %rdx
	je	.L979
	movq	%rcx, %rdx
.L929:
	movq	%rdx, %r8
	shrq	$2, %rdx
	andl	$3, %r8d
	leaq	(%rdx,%rdx,2), %rax
	addq	$1, %r8
	shrq	%r8
	addq	%rax, %r8
.L928:
	movq	%r9, %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN4node18base64_decode_fastIcEEmPcmPKT_mm
	movq	%rax, %r12
.L931:
	movl	%r12d, (%rbx)
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L935:
	leaq	-96(%rbp), %r9
	movq	%r11, %rsi
	movq	%r10, %rdx
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v86String5ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	testq	%r13, %r13
	movslq	-88(%rbp), %rdi
	movq	-96(%rbp), %rsi
	movq	-120(%rbp), %r9
	je	.L962
	cmpq	$1, %rdi
	jbe	.L980
	subq	$2, %rdi
	xorl	%r12d, %r12d
	leaq	_ZN4nodeL11unhex_tableE(%rip), %rcx
	shrq	%rdi
	leaq	1(%rdi), %r8
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L951:
	cmpq	%rdi, %r12
	je	.L981
	movq	%rax, %r12
.L948:
	movzbl	(%rsi,%r12,4), %eax
	movzbl	2(%rsi,%r12,4), %edx
	movsbl	(%rcx,%rax), %eax
	movsbl	(%rcx,%rdx), %edx
	cmpl	$-1, %eax
	je	.L966
	cmpl	$-1, %edx
	je	.L966
	sall	$4, %eax
	orl	%edx, %eax
	movb	%al, (%r14,%r12)
	leaq	1(%r12), %rax
	cmpq	%r13, %rax
	jne	.L951
	movl	%r13d, %r14d
	movq	%r13, %r12
.L946:
	movq	%r9, %rdi
	call	_ZN2v86String5ValueD1Ev@PLT
.L940:
	movl	%r14d, (%rbx)
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L907:
	movl	$11, %r9d
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdx
	movq	%r11, %rsi
	movq	%r10, %rdi
	call	_ZNK2v86String12WriteOneByteEPNS_7IsolateEPhiii@PLT
	movl	%eax, (%rbx)
	movslq	%eax, %r12
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L923:
	leaq	-96(%rbp), %r9
	movq	%r10, %rdx
	movq	%r11, %rsi
	movq	%r9, %rdi
	movq	%r9, -120(%rbp)
	call	_ZN2v86String5ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movslq	-88(%rbp), %rcx
	xorl	%r8d, %r8d
	movq	-96(%rbp), %rdx
	movq	-120(%rbp), %r9
	cmpq	$1, %rcx
	jbe	.L932
	cmpw	$61, -2(%rdx,%rcx,2)
	movq	%rcx, %rax
	je	.L982
.L933:
	movq	%rax, %r8
	shrq	$2, %rax
	andl	$3, %r8d
	leaq	(%rax,%rax,2), %rax
	addq	$1, %r8
	shrq	%r8
	addq	%rax, %r8
.L932:
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	%r9, -120(%rbp)
	call	_ZN4node18base64_decode_fastItEEmPcmPKT_mm
	movq	-120(%rbp), %r9
	movq	%rax, %r12
	movq	%r9, %rdi
	call	_ZN2v86String5ValueD1Ev@PLT
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L979:
	leaq	-2(%rcx), %rax
	cmpb	$61, -2(%r9,%rcx)
	cmove	%rax, %rdx
	cmpq	$1, %rdx
	jbe	.L928
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L982:
	cmpw	$61, -4(%rdx,%rcx,2)
	leaq	-1(%rcx), %rax
	jne	.L934
	leaq	-2(%rcx), %rax
.L934:
	cmpq	$1, %rax
	ja	.L933
	xorl	%r8d, %r8d
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L966:
	movl	%r12d, %r14d
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L965:
	movl	%r12d, %r14d
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L977:
	movl	%r8d, %r14d
	movq	%r8, %r12
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L981:
	movl	%r8d, %r14d
	movq	%r8, %r12
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%rdx, -120(%rbp)
	movq	%r12, %rdi
	call	*%rax
	movq	-120(%rbp), %rdx
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L936:
	movq	%r12, %rdi
	call	*%rdx
	movq	%rax, %rdx
	movq	(%r12), %rax
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L926:
	movq	%rcx, -120(%rbp)
	movq	%r12, %rdi
	call	*%rax
	movq	-120(%rbp), %rcx
	movq	%rax, %r9
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L924:
	movq	%r12, %rdi
	call	*%rdx
	movq	%rax, %rcx
	movq	(%r12), %rax
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L910:
	call	*%rax
	movq	%rax, %rsi
	jmp	.L911
	.p2align 4,,10
	.p2align 3
.L908:
	movq	%rdi, -120(%rbp)
	call	*%rax
	movq	-120(%rbp), %rdi
	movq	(%rdi), %rdx
	jmp	.L909
.L959:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L940
.L962:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.L946
.L976:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L940
.L980:
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.L946
.L900:
	leaq	_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L978:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7253:
	.size	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi, .-_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcEE, @object
	.size	_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcEE, 80
_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcEE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED1Ev
	.quad	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcED0Ev
	.quad	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.quad	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.quad	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.quad	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.quad	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE4dataEv
	.quad	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String29ExternalOneByteStringResourceEcE6lengthEv
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtEE, @object
	.size	_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtEE, 80
_ZTVN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtEE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED1Ev
	.quad	_ZN4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtED0Ev
	.quad	_ZNK2v86String26ExternalStringResourceBase11IsCacheableEv
	.quad	_ZN2v86String26ExternalStringResourceBase7DisposeEv
	.quad	_ZNK2v86String26ExternalStringResourceBase4LockEv
	.quad	_ZNK2v86String26ExternalStringResourceBase6UnlockEv
	.quad	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE4dataEv
	.quad	_ZNK4node12_GLOBAL__N_112ExternStringIN2v86String22ExternalStringResourceEtE6lengthEv
	.section	.rodata.str1.1
.LC10:
	.string	"../src/string_bytes.cc:750"
.LC11:
	.string	"0 && \"unknown encoding\""
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"static v8::MaybeLocal<v8::Value> node::StringBytes::Encode(v8::Isolate*, const char*, size_t, node::encoding, v8::Local<v8::Value>*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEEE4args_1, 24
_ZZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEEE4args_1:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.section	.rodata
	.align 16
	.type	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE3hex, @object
	.size	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE3hex, 17
_ZZN4node11StringBytes10hex_encodeEPKcmPcmE3hex:
	.string	"0123456789abcdef"
	.section	.rodata.str1.1
.LC13:
	.string	"../src/string_bytes.cc:604"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"dlen >= slen * 2 && \"not enough space provided for hex encode\""
	.align 8
.LC15:
	.string	"static size_t node::StringBytes::hex_encode(const char*, size_t, char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE4args, @object
	.size	_ZZN4node11StringBytes10hex_encodeEPKcmPcmE4args, 24
_ZZN4node11StringBytes10hex_encodeEPKcmPcmE4args:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/string_bytes.cc:489"
.LC17:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"static v8::Maybe<long unsigned int> node::StringBytes::Size(v8::Isolate*, v8::Local<v8::Value>, node::encoding)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args, @object
	.size	_ZZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args, 24
_ZZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"../src/string_bytes.cc:449"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"static v8::Maybe<long unsigned int> node::StringBytes::StorageSize(v8::Isolate*, v8::Local<v8::Value>, node::encoding)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args_0, @object
	.size	_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args_0, 24
_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args_0:
	.quad	.LC19
	.quad	.LC11
	.quad	.LC20
	.section	.rodata.str1.1
.LC21:
	.string	"../src/string_bytes.cc:444"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"str->Length() % 2 == 0 && \"invalid hex string length\""
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args, @object
	.size	_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args, 24
_ZZN4node11StringBytes11StorageSizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingEE4args:
	.quad	.LC21
	.quad	.LC22
	.quad	.LC20
	.section	.rodata.str1.1
.LC23:
	.string	"../src/string_bytes.cc:386"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"static size_t node::StringBytes::Write(v8::Isolate*, char*, size_t, v8::Local<v8::Value>, node::encoding, int*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args_0, @object
	.size	_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args_0, 24
_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args_0:
	.quad	.LC23
	.quad	.LC11
	.quad	.LC24
	.section	.rodata.str1.1
.LC25:
	.string	"../src/string_bytes.cc:321"
.LC26:
	.string	"val->IsString() == true"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args, @object
	.size	_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args, 24
_ZZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPiE4args:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC24
	.section	.rodata.str1.1
.LC27:
	.string	"../src/string_bytes.cc:299"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"(str->Write(isolate, &last, nchars, 1, flags)) == (1)"
	.align 8
.LC29:
	.string	"static size_t node::StringBytes::WriteUCS2(v8::Isolate*, char*, size_t, v8::Local<v8::String>, int, size_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_1, @object
	.size	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_1, 24
_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_1:
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.section	.rodata.str1.1
.LC30:
	.string	"../src/string_bytes.cc:292"
.LC31:
	.string	"(nchars) == (max_chars - 1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_0, @object
	.size	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_0, 24
_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args_0:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC29
	.section	.rodata.str1.1
.LC32:
	.string	"../src/string_bytes.cc:286"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"(reinterpret_cast<uintptr_t>(aligned_dst) % sizeof(*dst)) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args, @object
	.size	_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args, 24
_ZZN4node11StringBytes9WriteUCS2EPN2v87IsolateEPcmNS1_5LocalINS1_6StringEEEiPmE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC29
	.section	.rodata
	.align 32
	.type	_ZN4nodeL11unhex_tableE, @object
	.size	_ZN4nodeL11unhex_tableE, 256
_ZN4nodeL11unhex_tableE:
	.string	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\001\002\003\004\005\006\007\b\t\377\377\377\377\377\377\377"
	.ascii	"\n\013\f\r\016\017\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\n\013\f\r\016\017\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377"
	.globl	_ZN4node14unbase64_tableE
	.align 32
	.type	_ZN4node14unbase64_tableE, @object
	.size	_ZN4node14unbase64_tableE, 256
_ZN4node14unbase64_tableE:
	.string	"\377\377\377\377\377\377\377\377\377\377\376\377\377\376\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\376\377\377\377\377\377\377\377\377\377\377>\377>\377?456789:;<=\377\377\377\377\377\377\377"
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022"
	.ascii	"\023\024\025\026\027\030\031\377\377\377\377?\377\032\033\034"
	.ascii	"\035\036\037 !\"#$%&'()*+,-./0123\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"
	.ascii	"\377\377\377\377\377\377"
	.align 32
	.type	_ZZN4nodeL13base64_encodeEPKcmPcmE5table, @object
	.size	_ZZN4nodeL13base64_encodeEPKcmPcmE5table, 65
_ZZN4nodeL13base64_encodeEPKcmPcmE5table:
	.string	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	.weak	_ZZN4node11SwapBytes16EPcmE4args
	.section	.rodata.str1.1
.LC34:
	.string	"../src/util-inl.h:208"
.LC35:
	.string	"(nbytes % 2) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"void node::SwapBytes16(char*, size_t)"
	.section	.data.rel.ro.local._ZZN4node11SwapBytes16EPcmE4args,"awG",@progbits,_ZZN4node11SwapBytes16EPcmE4args,comdat
	.align 16
	.type	_ZZN4node11SwapBytes16EPcmE4args, @gnu_unique_object
	.size	_ZZN4node11SwapBytes16EPcmE4args, 24
_ZZN4node11SwapBytes16EPcmE4args:
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.byte	127
	.align 16
.LC6:
	.quad	9187201950435737471
	.quad	9187201950435737471
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
