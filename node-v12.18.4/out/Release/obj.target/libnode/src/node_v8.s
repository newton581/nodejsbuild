	.file	"node_v8.cc"
	.text
	.p2align 4
	.globl	_ZN4node31UpdateHeapStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node31UpdateHeapStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node31UpdateHeapStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$112, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L27
	cmpw	$1040, %cx
	jne	.L2
.L27:
	movq	23(%rdx), %rbx
.L4:
	leaq	-128(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v814HeapStatisticsC1Ev@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v87Isolate17GetHeapStatisticsEPNS_14HeapStatisticsE@PLT
	movq	2160(%rbx), %rax
	testq	%rax, %rax
	je	.L30
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	js	.L6
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L7:
	movq	-120(%rbp), %rdx
	movsd	%xmm0, (%rax)
	testq	%rdx, %rdx
	js	.L8
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L9:
	movq	-112(%rbp), %rdx
	movsd	%xmm0, 8(%rax)
	testq	%rdx, %rdx
	js	.L10
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L11:
	movq	-104(%rbp), %rdx
	movsd	%xmm0, 16(%rax)
	testq	%rdx, %rdx
	js	.L12
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L13:
	movq	-96(%rbp), %rdx
	movsd	%xmm0, 24(%rax)
	testq	%rdx, %rdx
	js	.L14
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L15:
	movq	-88(%rbp), %rdx
	movsd	%xmm0, 32(%rax)
	testq	%rdx, %rdx
	js	.L16
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L17:
	movq	-80(%rbp), %rdx
	movsd	%xmm0, 40(%rax)
	testq	%rdx, %rdx
	js	.L18
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L19:
	movq	-64(%rbp), %rdx
	movsd	%xmm0, 48(%rax)
	testq	%rdx, %rdx
	js	.L20
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L21:
	movzbl	-56(%rbp), %edx
	movsd	%xmm0, 56(%rax)
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movq	-48(%rbp), %rdx
	movsd	%xmm0, 64(%rax)
	testq	%rdx, %rdx
	js	.L22
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L23:
	movq	-40(%rbp), %rdx
	movsd	%xmm0, 72(%rax)
	testq	%rdx, %rdx
	js	.L24
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rdx, %xmm0
.L25:
	movsd	%xmm0, 80(%rax)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L31
	addq	$112, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rdx, %rcx
	andl	$1, %edx
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rdx, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	_ZZNK4node11Environment22heap_statistics_bufferEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L31:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7081:
	.size	_ZN4node31UpdateHeapStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node31UpdateHeapStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jg	.L33
	movq	(%rdi), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rax
	addq	$88, %rdx
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L39
.L35:
	leaq	_ZZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	movq	8(%rdi), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L35
.L39:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L35
	movq	(%rdi), %rax
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	movq	8(%rax), %rsi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movl	-40(%rbp), %esi
	movq	-48(%rbp), %rdi
	call	_ZN2v82V818SetFlagsFromStringEPKci@PLT
	movq	%r12, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7084:
	.size	_ZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node20CachedDataVersionTagERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node20CachedDataVersionTagERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node20CachedDataVersionTagERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7080:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L47
	cmpw	$1040, %cx
	jne	.L42
.L47:
	movq	23(%rdx), %r12
.L44:
	call	_ZN2v814ScriptCompiler20CachedDataVersionTagEv@PLT
	movq	352(%r12), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	(%rbx), %rdx
	testq	%rax, %rax
	je	.L49
	movq	(%rax), %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movq	16(%rdx), %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7080:
	.size	_ZN4node20CachedDataVersionTagERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node20CachedDataVersionTagERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node35UpdateHeapCodeStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node35UpdateHeapCodeStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node35UpdateHeapCodeStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L62
	cmpw	$1040, %cx
	jne	.L51
.L62:
	movq	23(%rdx), %rbx
.L53:
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v818HeapCodeStatisticsC1Ev@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v87Isolate32GetHeapCodeAndMetadataStatisticsEPNS_18HeapCodeStatisticsE@PLT
	movq	2176(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L64
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	js	.L55
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L56:
	movq	-40(%rbp), %rax
	movsd	%xmm0, (%rdx)
	testq	%rax, %rax
	js	.L57
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L58:
	movq	-32(%rbp), %rax
	movsd	%xmm0, 8(%rdx)
	testq	%rax, %rax
	js	.L59
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L60:
	movsd	%xmm0, 16(%rdx)
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	.cfi_restore_state
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rax, %rcx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rcx
	orq	%rax, %rcx
	cvtsi2sdq	%rcx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L51:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	_ZZNK4node11Environment27heap_code_statistics_bufferEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L65:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7083:
	.size	_ZN4node35UpdateHeapCodeStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node35UpdateHeapCodeStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node31UpdateHeapSpaceStatisticsBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node31UpdateHeapSpaceStatisticsBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node31UpdateHeapSpaceStatisticsBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7082:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rsi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L82
	cmpw	$1040, %cx
	jne	.L67
.L82:
	movq	23(%rdx), %rbx
.L69:
	leaq	-96(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v819HeapSpaceStatisticsC1Ev@PLT
	movq	2168(%rbx), %r14
	movq	352(%rbx), %r13
	testq	%r14, %r14
	je	.L88
	movq	%r13, %rdi
	call	_ZN2v87Isolate18NumberOfHeapSpacesEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L66
	xorl	%r15d, %r15d
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L89:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movq	-80(%rbp), %rax
	movsd	%xmm0, (%r14)
	testq	%rax, %rax
	js	.L74
.L90:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movq	-72(%rbp), %rax
	movsd	%xmm0, 8(%r14)
	testq	%rax, %rax
	js	.L76
.L91:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movq	-64(%rbp), %rax
	movsd	%xmm0, 16(%r14)
	testq	%rax, %rax
	js	.L78
.L92:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L79:
	addq	$1, %r15
	movsd	%xmm0, 24(%r14)
	addq	$32, %r14
	cmpq	%r15, %rbx
	je	.L66
.L80:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v87Isolate22GetHeapSpaceStatisticsEPNS_19HeapSpaceStatisticsEm@PLT
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	jns	.L89
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	movq	-80(%rbp), %rax
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, (%r14)
	testq	%rax, %rax
	jns	.L90
.L74:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	movq	-72(%rbp), %rax
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 8(%r14)
	testq	%rax, %rax
	jns	.L91
.L76:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	movq	-64(%rbp), %rax
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	%xmm0, 16(%r14)
	testq	%rax, %rax
	jns	.L92
.L78:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L93
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	leaq	32(%rsi), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L88:
	leaq	_ZZNK4node11Environment28heap_space_statistics_bufferEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L93:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7082:
	.size	_ZN4node31UpdateHeapSpaceStatisticsBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node31UpdateHeapSpaceStatisticsBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"cachedDataVersionTag"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"updateHeapStatisticsArrayBuffer"
	.section	.rodata.str1.1
.LC2:
	.string	"heapStatisticsArrayBuffer"
.LC3:
	.string	"kTotalHeapSizeIndex"
.LC4:
	.string	"kTotalHeapSizeExecutableIndex"
.LC5:
	.string	"kTotalPhysicalSizeIndex"
.LC6:
	.string	"kTotalAvailableSize"
.LC7:
	.string	"kUsedHeapSizeIndex"
.LC8:
	.string	"kHeapSizeLimitIndex"
.LC9:
	.string	"kMallocedMemoryIndex"
.LC10:
	.string	"kPeakMallocedMemoryIndex"
.LC11:
	.string	"kDoesZapGarbageIndex"
.LC12:
	.string	"kNumberOfNativeContextsIndex"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"kNumberOfDetachedContextsIndex"
	.align 8
.LC14:
	.string	"updateHeapCodeStatisticsArrayBuffer"
	.section	.rodata.str1.1
.LC15:
	.string	"heapCodeStatisticsArrayBuffer"
.LC16:
	.string	"kCodeAndMetadataSizeIndex"
.LC17:
	.string	"kBytecodeAndMetadataSizeIndex"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"kExternalScriptSourceSizeIndex"
	.align 8
.LC19:
	.string	"kHeapSpaceStatisticsPropertiesCount"
	.section	.rodata.str1.1
.LC21:
	.string	"kHeapSpaces"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"updateHeapSpaceStatisticsArrayBuffer"
	.align 8
.LC23:
	.string	"heapSpaceStatisticsArrayBuffer"
	.section	.rodata.str1.1
.LC24:
	.string	"kSpaceSizeIndex"
.LC25:
	.string	"kSpaceUsedSizeIndex"
.LC26:
	.string	"kSpaceAvailableSizeIndex"
.LC27:
	.string	"kPhysicalSpaceSizeIndex"
.LC28:
	.string	"setFlagsFromString"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB29:
	.text
.LHOTB29:
	.p2align 4
	.globl	_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.type	_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, @function
_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv:
.LFB7085:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L95
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L95
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L95
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$1
	xorl	%ecx, %ecx
	leaq	_ZN4node20CachedDataVersionTagERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	popq	%r13
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L193
.L96:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L194
.L97:
	movq	%r15, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L195
.L98:
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node31UpdateHeapStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L196
.L99:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L197
.L100:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L198
.L101:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movl	$88, %edi
	call	_Znam@PLT
	cmpq	$0, 2160(%rbx)
	movq	%rax, %rsi
	jne	.L199
	movq	%rax, 2160(%rbx)
	movl	$1, %ecx
	movl	$88, %edx
	movq	352(%rbx), %rdi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$25, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L200
.L103:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L201
.L104:
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$19, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L202
.L105:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L203
.L106:
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$29, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L204
.L107:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L205
.L108:
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$23, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L206
.L109:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L207
.L110:
	movq	352(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$19, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L208
.L111:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L209
.L112:
	movq	352(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$18, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L210
.L113:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L211
.L114:
	movq	352(%rbx), %rdi
	movl	$5, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$19, %ecx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L212
.L115:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L213
.L116:
	movq	352(%rbx), %rdi
	movl	$6, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$20, %ecx
	leaq	.LC9(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L214
.L117:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L215
.L118:
	movq	352(%rbx), %rdi
	movl	$7, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$24, %ecx
	leaq	.LC10(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L216
.L119:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L217
.L120:
	movq	352(%rbx), %rdi
	movl	$8, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$20, %ecx
	leaq	.LC11(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L218
.L121:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L219
.L122:
	movq	352(%rbx), %rdi
	movl	$9, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$28, %ecx
	leaq	.LC12(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L220
.L123:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L221
.L124:
	movq	352(%rbx), %rdi
	movl	$10, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$30, %ecx
	leaq	.LC13(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L222
.L125:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L223
.L126:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node35UpdateHeapCodeStatisticsArrayBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L224
.L127:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC14(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L225
.L128:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L226
.L129:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movl	$24, %edi
	call	_Znam@PLT
	cmpq	$0, 2176(%rbx)
	movq	%rax, %rsi
	jne	.L227
	movq	%rax, 2176(%rbx)
	movl	$1, %ecx
	movl	$24, %edx
	movq	352(%rbx), %rdi
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$29, %ecx
	leaq	.LC15(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L228
.L131:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L229
.L132:
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$25, %ecx
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L230
.L133:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L231
.L134:
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$29, %ecx
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L232
.L135:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L233
.L136:
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$30, %ecx
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L234
.L137:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L235
.L138:
	movq	352(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$35, %ecx
	leaq	.LC19(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L236
.L139:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L237
.L140:
	movq	352(%rbx), %rdi
	leaq	-256(%rbp), %r14
	call	_ZN2v87Isolate18NumberOfHeapSpacesEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	call	_ZN2v819HeapSpaceStatisticsC1Ev@PLT
	movdqa	.LC20(%rip), %xmm0
	leaq	-184(%rbp), %rax
	movq	%rax, -264(%rbp)
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	movups	%xmm0, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	cmpq	$16, %r13
	jbe	.L141
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r13,8), %r15
	andq	%r13, %rax
	cmpq	%rax, %r13
	jne	.L238
	testq	%r15, %r15
	je	.L143
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L239
	movq	%rax, -192(%rbp)
	movq	%r13, -200(%rbp)
.L146:
	movq	%r13, -208(%rbp)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r13, -208(%rbp)
	testq	%r13, %r13
	je	.L174
.L172:
	xorl	%r15d, %r15d
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L148:
	cmpq	%r15, -208(%rbp)
	jbe	.L240
.L149:
	movq	-192(%rbp), %rsi
	movq	%rax, (%rsi,%r15,8)
	addq	$1, %r15
	cmpq	%r15, %r13
	jbe	.L147
.L150:
	movq	352(%rbx), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZN2v87Isolate22GetHeapSpaceStatisticsEPNS_19HeapSpaceStatisticsEm@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movq	-256(%rbp), %rsi
	movl	$-1, %ecx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L148
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %rax
	cmpq	%r15, -208(%rbp)
	ja	.L149
	.p2align 4,,10
	.p2align 3
.L240:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L174:
	movq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L147:
	movq	352(%rbx), %rdi
	movq	%r13, %rdx
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L241
.L151:
	movq	3280(%rbx), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L242
.L152:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r15
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node31UpdateHeapSpaceStatisticsBufferERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L243
.L153:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L244
.L154:
	movq	%r9, %rdx
	movq	%r14, %rcx
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%r9, -272(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-272(%rbp), %r9
	testb	%al, %al
	je	.L245
.L155:
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	leaq	0(,%r13,4), %rdx
	salq	$5, %r13
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	movq	$-1, %rdi
	cmovbe	%r13, %rdi
	call	_Znam@PLT
	cmpq	$0, 2168(%rbx)
	movq	%rax, %rsi
	jne	.L246
	movq	%rax, 2168(%rbx)
	movq	352(%rbx), %rdi
	movq	%r13, %rdx
	movl	$1, %ecx
	call	_ZN2v811ArrayBuffer3NewEPNS_7IsolateEPvmNS_23ArrayBufferCreationModeE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$30, %ecx
	leaq	.LC23(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L247
.L158:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L248
.L159:
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$15, %ecx
	leaq	.LC24(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L249
.L160:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L250
.L161:
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$19, %ecx
	leaq	.LC25(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L251
.L162:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L252
.L163:
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$24, %ecx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L253
.L164:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L254
.L165:
	movq	352(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$23, %ecx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %r13
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L255
.L166:
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L256
.L167:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L257
.L168:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L258
.L169:
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L259
.L170:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	-192(%rbp), %rdi
	cmpq	-264(%rbp), %rdi
	je	.L94
	testq	%rdi, %rdi
	je	.L94
	call	free@PLT
.L94:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L193:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L96
.L194:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L97
.L195:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L98
.L196:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L99
.L197:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L100
.L198:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L101
.L199:
	leaq	_ZZN4node11Environment26set_heap_statistics_bufferEPdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L208:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L111
.L209:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L112
.L210:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L113
.L211:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L114
.L204:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L107
.L205:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L108
.L206:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L109
.L207:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L110
.L224:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L127
.L225:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L128
.L226:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L129
.L227:
	leaq	_ZZN4node11Environment31set_heap_code_statistics_bufferEPdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L212:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L115
.L213:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L116
.L214:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L117
.L215:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L118
.L216:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L119
.L217:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L120
.L218:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L121
.L219:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L122
.L220:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L123
.L221:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L124
.L222:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L125
.L223:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L126
.L200:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L103
.L201:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L104
.L202:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L105
.L203:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L106
.L234:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L137
.L235:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L138
.L236:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L139
.L237:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L140
.L228:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L131
.L229:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L132
.L230:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L133
.L231:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L134
.L232:
	movq	%rax, -264(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-264(%rbp), %rdx
	jmp	.L135
.L233:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L136
.L143:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L239:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L143
	movq	-208(%rbp), %rax
	movq	%rdi, -192(%rbp)
	movq	%r13, -200(%rbp)
	testq	%rax, %rax
	je	.L146
	movq	-264(%rbp), %rsi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	jmp	.L146
.L247:
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %rdx
	jmp	.L158
.L248:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L159
.L249:
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %rdx
	jmp	.L160
.L250:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L161
.L251:
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %rdx
	jmp	.L162
.L252:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L163
.L253:
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %rdx
	jmp	.L164
.L254:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L165
.L255:
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %rdx
	jmp	.L166
.L256:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L167
.L257:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L168
.L258:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L169
.L259:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L170
.L238:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L246:
	leaq	_ZZN4node11Environment32set_heap_space_statistics_bufferEPdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L241:
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %rdx
	jmp	.L151
.L242:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L152
.L243:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L153
.L244:
	movq	%rax, -272(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-272(%rbp), %r9
	jmp	.L154
.L245:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-272(%rbp), %r9
	jmp	.L155
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, @function
_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold:
.LFSB7085:
.L95:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7085:
	.text
	.size	_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, .-_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, .-_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold
.LCOLDE29:
	.text
.LHOTE29:
	.p2align 4
	.globl	_Z12_register_v8v
	.type	_Z12_register_v8v, @function
_Z12_register_v8v:
.LFB7086:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7086:
	.size	_Z12_register_v8v, .-_Z12_register_v8v
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC30:
	.string	"../src/util-inl.h:374"
.LC31:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC33:
	.string	"../src/util-inl.h:325"
.LC34:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args
	.section	.rodata.str1.1
.LC36:
	.string	"../src/util.h:352"
.LC37:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC38:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 16; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args:
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.section	.rodata.str1.1
.LC39:
	.string	"../src/node_v8.cc"
.LC40:
	.string	"v8"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC39
	.quad	0
	.quad	_ZN4node10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.quad	.LC40
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC41:
	.string	"../src/node_v8.cc:140"
.LC42:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::SetFlagsFromString(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, @object
	.size	_ZZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args, 24
_ZZN4node18SetFlagsFromStringERKN2v820FunctionCallbackInfoINS0_5ValueEEEE4args:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.weak	_ZZN4node11Environment31set_heap_code_statistics_bufferEPdE4args
	.section	.rodata.str1.1
.LC44:
	.string	"../src/env-inl.h:569"
	.section	.rodata.str1.8
	.align 8
.LC45:
	.string	"(heap_code_statistics_buffer_) == nullptr"
	.align 8
.LC46:
	.string	"void node::Environment::set_heap_code_statistics_buffer(double*)"
	.section	.data.rel.ro.local._ZZN4node11Environment31set_heap_code_statistics_bufferEPdE4args,"awG",@progbits,_ZZN4node11Environment31set_heap_code_statistics_bufferEPdE4args,comdat
	.align 16
	.type	_ZZN4node11Environment31set_heap_code_statistics_bufferEPdE4args, @gnu_unique_object
	.size	_ZZN4node11Environment31set_heap_code_statistics_bufferEPdE4args, 24
_ZZN4node11Environment31set_heap_code_statistics_bufferEPdE4args:
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.weak	_ZZNK4node11Environment27heap_code_statistics_bufferEvE4args
	.section	.rodata.str1.1
.LC47:
	.string	"../src/env-inl.h:564"
	.section	.rodata.str1.8
	.align 8
.LC48:
	.string	"(heap_code_statistics_buffer_) != nullptr"
	.align 8
.LC49:
	.string	"double* node::Environment::heap_code_statistics_buffer() const"
	.section	.data.rel.ro.local._ZZNK4node11Environment27heap_code_statistics_bufferEvE4args,"awG",@progbits,_ZZNK4node11Environment27heap_code_statistics_bufferEvE4args,comdat
	.align 16
	.type	_ZZNK4node11Environment27heap_code_statistics_bufferEvE4args, @gnu_unique_object
	.size	_ZZNK4node11Environment27heap_code_statistics_bufferEvE4args, 24
_ZZNK4node11Environment27heap_code_statistics_bufferEvE4args:
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.weak	_ZZN4node11Environment32set_heap_space_statistics_bufferEPdE4args
	.section	.rodata.str1.1
.LC50:
	.string	"../src/env-inl.h:559"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"(heap_space_statistics_buffer_) == nullptr"
	.align 8
.LC52:
	.string	"void node::Environment::set_heap_space_statistics_buffer(double*)"
	.section	.data.rel.ro.local._ZZN4node11Environment32set_heap_space_statistics_bufferEPdE4args,"awG",@progbits,_ZZN4node11Environment32set_heap_space_statistics_bufferEPdE4args,comdat
	.align 16
	.type	_ZZN4node11Environment32set_heap_space_statistics_bufferEPdE4args, @gnu_unique_object
	.size	_ZZN4node11Environment32set_heap_space_statistics_bufferEPdE4args, 24
_ZZN4node11Environment32set_heap_space_statistics_bufferEPdE4args:
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.weak	_ZZNK4node11Environment28heap_space_statistics_bufferEvE4args
	.section	.rodata.str1.1
.LC53:
	.string	"../src/env-inl.h:554"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"(heap_space_statistics_buffer_) != nullptr"
	.align 8
.LC55:
	.string	"double* node::Environment::heap_space_statistics_buffer() const"
	.section	.data.rel.ro.local._ZZNK4node11Environment28heap_space_statistics_bufferEvE4args,"awG",@progbits,_ZZNK4node11Environment28heap_space_statistics_bufferEvE4args,comdat
	.align 16
	.type	_ZZNK4node11Environment28heap_space_statistics_bufferEvE4args, @gnu_unique_object
	.size	_ZZNK4node11Environment28heap_space_statistics_bufferEvE4args, 24
_ZZNK4node11Environment28heap_space_statistics_bufferEvE4args:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.weak	_ZZN4node11Environment26set_heap_statistics_bufferEPdE4args
	.section	.rodata.str1.1
.LC56:
	.string	"../src/env-inl.h:549"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"(heap_statistics_buffer_) == nullptr"
	.align 8
.LC58:
	.string	"void node::Environment::set_heap_statistics_buffer(double*)"
	.section	.data.rel.ro.local._ZZN4node11Environment26set_heap_statistics_bufferEPdE4args,"awG",@progbits,_ZZN4node11Environment26set_heap_statistics_bufferEPdE4args,comdat
	.align 16
	.type	_ZZN4node11Environment26set_heap_statistics_bufferEPdE4args, @gnu_unique_object
	.size	_ZZN4node11Environment26set_heap_statistics_bufferEPdE4args, 24
_ZZN4node11Environment26set_heap_statistics_bufferEPdE4args:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.weak	_ZZNK4node11Environment22heap_statistics_bufferEvE4args
	.section	.rodata.str1.1
.LC59:
	.string	"../src/env-inl.h:544"
	.section	.rodata.str1.8
	.align 8
.LC60:
	.string	"(heap_statistics_buffer_) != nullptr"
	.align 8
.LC61:
	.string	"double* node::Environment::heap_statistics_buffer() const"
	.section	.data.rel.ro.local._ZZNK4node11Environment22heap_statistics_bufferEvE4args,"awG",@progbits,_ZZNK4node11Environment22heap_statistics_bufferEvE4args,comdat
	.align 16
	.type	_ZZNK4node11Environment22heap_statistics_bufferEvE4args, @gnu_unique_object
	.size	_ZZNK4node11Environment22heap_statistics_bufferEvE4args, 24
_ZZNK4node11Environment22heap_statistics_bufferEvE4args:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC20:
	.quad	0
	.quad	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
