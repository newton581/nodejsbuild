	.file	"node_config.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"isDebugBuild"
.LC1:
	.string	"hasOpenSSL"
.LC2:
	.string	"hasIntl"
.LC3:
	.string	"hasSmallICU"
.LC4:
	.string	"hasTracing"
.LC5:
	.string	"hasNodeOptions"
.LC6:
	.string	"hasInspector"
.LC7:
	.string	"noBrowserGlobals"
.LC9:
	.string	"bits"
.LC10:
	.string	"hasCachedBuiltins"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB11:
	.text
.LHOTB11:
	.p2align 4
	.type	_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, @function
_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv:
.LFB7213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L2
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L2
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L2
	movq	271(%rax), %rax
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	.LC0(%rip), %rsi
	movq	352(%rax), %r13
	movq	%r13, %rdi
	leaq	120(%r13), %rbx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L29
.L3:
	movl	$1, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L30
.L4:
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	112(%r13), %r15
	movq	%r13, %rdi
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L31
.L5:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L32
.L6:
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L33
.L7:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L34
.L8:
	xorl	%edx, %edx
	movl	$11, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L35
.L9:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L36
.L10:
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L37
.L11:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L38
.L12:
	xorl	%edx, %edx
	movl	$14, %ecx
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L39
.L13:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L40
.L14:
	xorl	%edx, %edx
	movl	$12, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L41
.L15:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L42
.L16:
	xorl	%edx, %edx
	movl	$16, %ecx
	leaq	.LC7(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L43
.L17:
	movl	$1, %r8d
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L44
.L18:
	movsd	.LC8(%rip), %xmm0
	movq	%r13, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	xorl	%edx, %edx
	movl	$4, %ecx
	movq	%r13, %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L45
.L19:
	movl	$1, %r8d
	movq	%r9, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L46
.L20:
	cmpb	$0, _ZN4node13native_module14has_code_cacheE(%rip)
	movl	$17, %ecx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	cmove	%rbx, %r15
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L47
.L22:
	movl	$1, %r8d
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L48
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L30:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L32:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L34:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L36:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L38:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L40:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L42:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L44:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rdx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L46:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, @function
_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold:
.LFSB7213:
.L2:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7213:
	.text
	.size	_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv, .-_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold, .-_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv.cold
.LCOLDE11:
	.text
.LHOTE11:
	.p2align 4
	.globl	_Z16_register_configv
	.type	_Z16_register_configv, @function
_Z16_register_configv:
.LFB7214:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7214:
	.size	_Z16_register_configv, .-_Z16_register_configv
	.section	.rodata.str1.1
.LC12:
	.string	"../src/node_config.cc"
.LC13:
	.string	"config"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC12
	.quad	0
	.quad	_ZN4nodeL10InitializeEN2v85LocalINS0_6ObjectEEENS1_INS0_5ValueEEENS1_INS0_7ContextEEEPv
	.quad	.LC13
	.quad	0
	.quad	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	0
	.long	1078984704
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
