	.file	"tracing_agent.cc"
	.text
	.section	.text._ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s,"axG",@progbits,_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s
	.type	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s, @function
_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s:
.LFB5551:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5551:
	.size	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s, .-_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD2Ev:
.LFB10133:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10133:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB12621:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12621:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB12624:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12624:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN4node9inspector8protocol11NodeTracing7Backend7disableEv,"axG",@progbits,_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv
	.type	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv, @function
_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv:
.LFB4923:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L9
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L9:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4923:
	.size	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv, .-_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD0Ev:
.LFB10135:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10135:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB12623:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12623:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE:
.LFB7966:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movl	8(%r8), %esi
	jmp	_ZN4node9inspector19MainThreadInterface12RemoveObjectEi@PLT
	.cfi_endproc
.LFE7966:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD2Ev:
.LFB10098:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %r8
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestE(%rip), %rax
	addq	$32, %rdi
	movq	%rax, -32(%rdi)
	cmpq	%rdi, %r8
	je	.L13
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	ret
	.cfi_endproc
.LFE10098:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD0Ev:
.LFB10100:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10100:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB12625:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE12625:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB12626:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L19
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L19:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12626:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE:
.LFB7962:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	8(%rdi), %esi
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	$0, 16(%rdi)
	leaq	-16(%rbp), %rdx
	movq	%r8, %rdi
	movq	%rax, -16(%rbp)
	call	_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE@PLT
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L23
	movq	(%rdi), %rax
	call	*8(%rax)
.L23:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L30
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7962:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12TracingAgent4stopEv
	.type	_ZN4node9inspector8protocol12TracingAgent4stopEv, @function
_ZN4node9inspector8protocol12TracingAgent4stopEv:
.LFB8029:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	32(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L32
	movl	40(%rsi), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L32:
	movq	$0, 32(%rbx)
	movq	56(%rbx), %rdi
	call	_ZN4node9inspector8protocol11NodeTracing8Frontend15tracingCompleteEv@PLT
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L38
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L38:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8029:
	.size	_ZN4node9inspector8protocol12TracingAgent4stopEv, .-_ZN4node9inspector8protocol12TracingAgent4stopEv
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD2Ev:
.LFB10022:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L39
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L42
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L45
.L39:
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L39
.L45:
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE10022:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD0Ev:
.LFB10024:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L49
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L53
.L48:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L48
.L53:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L48
	.cfi_endproc
.LFE10024:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD2Ev:
.LFB10173:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L54
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L57
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L58
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L59:
	cmpl	$1, %eax
	jne	.L57
	movq	(%rdi), %rax
	call	*24(%rax)
.L57:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L59
	.cfi_endproc
.LFE10173:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD0Ev:
.LFB10175:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	16(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L63
	movq	16(%r13), %rdi
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdi, %rdi
	je	.L65
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L66
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L67:
	cmpl	$1, %eax
	jne	.L65
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L63:
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L66:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L67
	.cfi_endproc
.LFE10175:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD0Ev:
.LFB10230:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	408(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L75
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L76
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L87
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	96(%r12), %rdi
	movq	%rax, 128(%r12)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	leaq	112(%r12), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%r12)
	cmpq	%rax, %rdi
	je	.L81
	call	_ZdlPv@PLT
.L81:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	80(%r12), %rdi
	movq	%rax, 24(%r12)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	128(%r12), %rdi
	movq	%rax, 16(%r12)
	movq	-24(%rax), %rax
	movq	%rdx, 16(%r12,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 128(%r12)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L82
	movq	(%rdi), %rax
	call	*8(%rax)
.L82:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$416, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L76:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L75
.L87:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L79
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L80:
	cmpl	$1, %eax
	jne	.L75
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L79:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L80
	.cfi_endproc
.LFE10230:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD2Ev:
.LFB10228:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	408(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L90
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L91
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L99
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	96(%rbx), %rdi
	movq	%rax, 128(%rbx)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rcx, %xmm0
	movq	%rax, %xmm1
	leaq	112(%rbx), %rax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 16(%rbx)
	cmpq	%rax, %rdi
	je	.L96
	call	_ZdlPv@PLT
.L96:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	80(%rbx), %rdi
	movq	%rax, 24(%rbx)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	leaq	128(%rbx), %rdi
	movq	%rax, 16(%rbx)
	movq	-24(%rax), %rax
	movq	%rdx, 16(%rbx,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, 128(%rbx)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L88
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L90
.L99:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L94
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L95:
	cmpl	$1, %eax
	jne	.L90
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L88:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L95
	.cfi_endproc
.LFE10228:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12TracingAgentD2Ev
	.type	_ZN4node9inspector8protocol12TracingAgentD2Ev, @function
_ZN4node9inspector8protocol12TracingAgentD2Ev:
.LFB8011:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9inspector8protocol12TracingAgentE(%rip), %rax
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L101
	movl	40(%rbx), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L101:
	movl	48(%rbx), %r13d
	movl	$16, %edi
	movq	$0, 32(%rbx)
	movq	16(%rbx), %r12
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestE(%rip), %rcx
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	movq	%rcx, (%rax)
	movl	%r13d, 8(%rax)
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L102
	movq	(%rdi), %rax
	call	*16(%rax)
.L102:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L104
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L105
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L129
	.p2align 4,,10
	.p2align 3
.L104:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L110
	movl	40(%rbx), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L110:
	movq	24(%rbx), %r12
	testq	%r12, %r12
	je	.L100
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L113
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L130
	.p2align 4,,10
	.p2align 3
.L100:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L131
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L104
.L129:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L108
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L109:
	cmpl	$1, %eax
	jne	.L104
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L113:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L100
.L130:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L116
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L117:
	cmpl	$1, %eax
	jne	.L100
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L108:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L116:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L117
.L131:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8011:
	.size	_ZN4node9inspector8protocol12TracingAgentD2Ev, .-_ZN4node9inspector8protocol12TracingAgentD2Ev
	.globl	_ZN4node9inspector8protocol12TracingAgentD1Ev
	.set	_ZN4node9inspector8protocol12TracingAgentD1Ev,_ZN4node9inspector8protocol12TracingAgentD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12TracingAgentD0Ev
	.type	_ZN4node9inspector8protocol12TracingAgentD0Ev, @function
_ZN4node9inspector8protocol12TracingAgentD0Ev:
.LFB8013:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node9inspector8protocol12TracingAgentD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8013:
	.size	_ZN4node9inspector8protocol12TracingAgentD0Ev, .-_ZN4node9inspector8protocol12TracingAgentD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE:
.LFB7993:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L143
.L135:
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*16(%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L144
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	.cfi_restore_state
	leaq	16(%rbx), %rdi
	leaq	-80(%rbp), %rsi
	movb	$101, -60(%rbp)
	leaq	-64(%rbp), %r13
	movl	$1970037110, -64(%rbp)
	movq	%r13, -80(%rbp)
	movq	$5, -72(%rbp)
	movb	$0, -59(%rbp)
	call	_ZN2v88platform7tracing11TraceWriter21CreateJSONTraceWriterERSoRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	8(%rbx), %rdi
	movq	%rax, 8(%rbx)
	testq	%rdi, %rdi
	je	.L136
	movq	(%rdi), %rax
	call	*8(%rax)
.L136:
	movq	-80(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	8(%rbx), %rdi
	jmp	.L135
.L144:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7993:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE, .-_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"basic_string::_M_construct null not valid"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"}"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter5FlushEb, @function
_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter5FlushEb:
.LFB7994:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$536, %rsp
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L145
	movq	.LC4(%rip), %xmm1
	movq	(%rdi), %rax
	movq	$0, 8(%rbx)
	leaq	-480(%rbp), %r12
	leaq	-320(%rbp), %r14
	leaq	-432(%rbp), %r13
	movhps	.LC5(%rip), %xmm1
	movaps	%xmm1, -528(%rbp)
	call	*8(%rax)
	leaq	-504(%rbp), %rax
	xorl	%edx, %edx
	leaq	-496(%rbp), %rdi
	movq	%rax, %rsi
	movq	%r12, -496(%rbp)
	movq	$47, -504(%rbp)
	movq	%rax, -536(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-504(%rbp), %rdx
	movdqa	.LC2(%rip), %xmm0
	movq	%r14, %rdi
	movabsq	$8080069207680971875, %rcx
	movq	%rax, -496(%rbp)
	movq	%rdx, -480(%rbp)
	movl	$8819, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC3(%rip), %xmm0
	movq	%rcx, 32(%rax)
	movw	%dx, 44(%rax)
	movq	-496(%rbp), %rdx
	movups	%xmm0, 16(%rax)
	movl	$1835102817, 40(%rax)
	movb	$58, 46(%rax)
	movq	-504(%rbp), %rax
	movq	%rax, -488(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rax, -320(%rbp)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movw	%cx, -96(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	-24(%rax), %rdi
	movq	%rax, -432(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	addq	%r13, %rdi
	movq	$0, -104(%rbp)
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	pxor	%xmm0, %xmm0
	movdqa	-528(%rbp), %xmm1
	movq	%rax, -320(%rbp)
	leaq	-368(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -560(%rbp)
	movaps	%xmm1, -432(%rbp)
	movaps	%xmm0, -416(%rbp)
	movaps	%xmm0, -400(%rbp)
	movaps	%xmm0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-496(%rbp), %r8
	movq	-488(%rbp), %r15
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -528(%rbp)
	movq	%rax, -352(%rbp)
	movq	%r8, %rax
	movl	$0, -360(%rbp)
	addq	%r15, %rax
	je	.L148
	testq	%r8, %r8
	je	.L161
.L148:
	movq	%r15, -504(%rbp)
	cmpq	$15, %r15
	ja	.L193
	cmpq	$1, %r15
	jne	.L151
	movzbl	(%r8), %eax
	movb	%al, -336(%rbp)
	movq	-528(%rbp), %rax
.L152:
	movq	%r15, -344(%rbp)
	xorl	%edx, %edx
	movb	$0, (%rax,%r15)
	leaq	-424(%rbp), %r15
	movq	-344(%rbp), %rcx
	movq	-352(%rbp), %rsi
	movq	%r15, %rdi
	movl	$18, -360(%rbp)
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-496(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L153
	call	_ZdlPv@PLT
.L153:
	leaq	24(%rbx), %rax
	leaq	-448(%rbp), %r12
	movq	$0, -456(%rbp)
	movq	%rax, -552(%rbp)
	leaq	96(%rbx), %rax
	leaq	-464(%rbp), %r15
	movq	%rax, -544(%rbp)
	movq	64(%rbx), %rax
	movq	%r12, -464(%rbp)
	movb	$0, -448(%rbp)
	testq	%rax, %rax
	je	.L154
	movq	48(%rbx), %r8
	movq	56(%rbx), %rcx
	cmpq	%r8, %rax
	jbe	.L155
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L156:
	movq	-456(%rbp), %rdx
	movq	-464(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-464(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L157
	call	_ZdlPv@PLT
.L157:
	movq	%r13, %rdi
	movl	$1, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l@PLT
	movq	-384(%rbp), %rax
	movq	%r12, -464(%rbp)
	movb	$0, -448(%rbp)
	movq	400(%rbx), %r13
	movq	$0, -456(%rbp)
	testq	%rax, %rax
	je	.L158
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	jbe	.L159
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L160:
	movl	392(%rbx), %edx
	movl	$48, %edi
	movl	%edx, -544(%rbp)
	call	_Znwm@PLT
	movq	-464(%rbp), %r9
	movq	-456(%rbp), %r8
	movq	%rax, %r15
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestE(%rip), %rax
	movl	-544(%rbp), %edx
	movq	%rax, (%r15)
	movq	%r9, %rax
	leaq	32(%r15), %rdi
	addq	%r8, %rax
	movl	%edx, 8(%r15)
	movq	%rdi, 16(%r15)
	je	.L175
	testq	%r9, %r9
	je	.L161
.L175:
	movq	%r8, -504(%rbp)
	cmpq	$15, %r8
	ja	.L194
	cmpq	$1, %r8
	jne	.L165
	movzbl	(%r9), %eax
	movb	%al, 32(%r15)
.L166:
	movq	%r8, 24(%r15)
	movq	-536(%rbp), %rsi
	movb	$0, (%rdi,%r8)
	movq	%r13, %rdi
	movq	%r15, -504(%rbp)
	call	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE@PLT
	movq	-504(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L167
	movq	(%rdi), %rax
	call	*16(%rax)
.L167:
	movq	-464(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L168
	call	_ZdlPv@PLT
.L168:
	movq	104(%rbx), %rdx
	movq	%r12, %rcx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	leaq	96(%rbx), %rdi
	movq	%r12, -464(%rbp)
	movq	$0, -456(%rbp)
	movb	$0, -448(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	xorl	%ecx, %ecx
	testb	$3, 88(%rbx)
	je	.L169
	movq	104(%rbx), %rcx
.L169:
	movq	-552(%rbp), %rdi
	movq	96(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	-464(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L170
	call	_ZdlPv@PLT
.L170:
	movq	.LC4(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC6(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-528(%rbp), %rdi
	je	.L171
	call	_ZdlPv@PLT
.L171:
	movq	-560(%rbp), %rdi
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	movq	%r14, %rdi
	movq	%rax, -432(%rbp)
	movq	-24(%rax), %rax
	movq	%rbx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
.L145:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L195
	addq	$536, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L196
	movq	-528(%rbp), %rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L159:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L155:
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L165:
	testq	%r8, %r8
	je	.L166
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L193:
	movq	-536(%rbp), %rsi
	leaq	-352(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -544(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-544(%rbp), %r8
	movq	%rax, -352(%rbp)
	movq	%rax, %rdi
	movq	-504(%rbp), %rax
	movq	%rax, -336(%rbp)
.L150:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-504(%rbp), %r15
	movq	-352(%rbp), %rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-536(%rbp), %rsi
	leaq	16(%r15), %rdi
	xorl	%edx, %edx
	movq	%r8, -568(%rbp)
	movq	%r9, -544(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-544(%rbp), %r9
	movq	-568(%rbp), %r8
	movq	%rax, 16(%r15)
	movq	%rax, %rdi
	movq	-504(%rbp), %rax
	movq	%rax, 32(%r15)
.L164:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-504(%rbp), %r8
	movq	16(%r15), %rdi
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	-352(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	96(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L156
.L161:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L195:
	call	__stack_chk_fail@PLT
.L196:
	movq	-528(%rbp), %rdi
	jmp	.L150
	.cfi_endproc
.LFE7994:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter5FlushEb, .-_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter5FlushEb
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequest4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequest4CallEPNS0_19MainThreadInterfaceE:
.LFB7970:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$72, %rsp
	movl	8(%rbx), %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector19MainThreadInterface17GetObjectIfExistsEi@PLT
	testq	%rax, %rax
	je	.L197
	movq	16(%rax), %r12
	movq	%rax, %rdx
	testq	%r12, %r12
	je	.L197
	movl	8(%r12), %eax
	leaq	8(%r12), %rcx
.L202:
	testl	%eax, %eax
	je	.L197
	leal	1(%rax), %esi
	lock cmpxchgl	%esi, (%rcx)
	jne	.L202
	movl	8(%r12), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jne	.L239
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L240
.L217:
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L241
.L206:
	testq	%r15, %r15
	je	.L197
	movq	16(%rbx), %r14
	movq	24(%rbx), %r12
	leaq	-80(%rbp), %r13
	movq	%r13, -96(%rbp)
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L211
	testq	%r14, %r14
	je	.L242
.L211:
	movq	%r12, -104(%rbp)
	cmpq	$15, %r12
	ja	.L243
	cmpq	$1, %r12
	jne	.L214
	movzbl	(%r14), %eax
	leaq	-96(%rbp), %rbx
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L215:
	movq	%r12, -88(%rbp)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movb	$0, (%rax,%r12)
	call	_ZN4node9inspector8protocol11NodeTracing8Frontend23sendRawJSONNotificationENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L197
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L197:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L244
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	.cfi_restore_state
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	movq	8(%rdx), %r15
	testq	%r13, %r13
	jne	.L217
.L240:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L206
.L241:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L207
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L208:
	cmpl	$1, %eax
	jne	.L206
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L214:
	testq	%r12, %r12
	jne	.L245
	movq	%r13, %rax
	leaq	-96(%rbp), %rbx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L243:
	leaq	-96(%rbp), %rbx
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L213:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L207:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L208
.L244:
	call	__stack_chk_fail@PLT
.L242:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L245:
	movq	%r13, %rdi
	leaq	-96(%rbp), %rbx
	jmp	.L213
	.cfi_endproc
.LFE7970:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequest4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequest4CallEPNS0_19MainThreadInterfaceE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12TracingAgentC2EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE
	.type	_ZN4node9inspector8protocol12TracingAgentC2EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE, @function
_ZN4node9inspector8protocol12TracingAgentC2EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE:
.LFB8006:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol12TracingAgentE(%rip), %rax
	movq	%rsi, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rdx), %rax
	movq	%rax, 16(%rdi)
	movq	8(%rdx), %rax
	movq	%rax, 24(%rdi)
	testq	%rax, %rax
	je	.L247
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L248
	lock addl	$1, 8(%rax)
.L247:
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	movups	%xmm0, 56(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	addl	$1, 8(%rax)
	jmp	.L247
	.cfi_endproc
.LFE8006:
	.size	_ZN4node9inspector8protocol12TracingAgentC2EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE, .-_ZN4node9inspector8protocol12TracingAgentC2EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE
	.globl	_ZN4node9inspector8protocol12TracingAgentC1EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE
	.set	_ZN4node9inspector8protocol12TracingAgentC1EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE,_ZN4node9inspector8protocol12TracingAgentC2EPNS_11EnvironmentESt10shared_ptrINS0_16MainThreadHandleEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12TracingAgent4WireEPNS1_14UberDispatcherE
	.type	_ZN4node9inspector8protocol12TracingAgent4WireEPNS1_14UberDispatcherE, @function
_ZN4node9inspector8protocol12TracingAgent4WireEPNS1_14UberDispatcherE:
.LFB8014:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%rsi, -88(%rbp)
	movq	8(%rsi), %rbx
	movq	%fs:40, %rcx
	movq	%rcx, -56(%rbp)
	xorl	%ecx, %ecx
	call	_Znwm@PLT
	movq	64(%r12), %r14
	movabsq	$4294967297, %rdx
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rdx, 8(%rax)
	leaq	16(%rax), %rdx
	movq	%rcx, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rdx, 56(%r12)
	movq	%rax, 64(%r12)
	testq	%r14, %r14
	je	.L254
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L255
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L302
	.p2align 4,,10
	.p2align 3
.L254:
	movq	16(%r12), %rax
	movl	$1, %r14d
	lock xaddl	%r14d, 68(%rax)
	movq	16(%r12), %rax
	movq	64(%r12), %r15
	movq	%rax, -80(%rbp)
	movq	56(%r12), %rax
	addl	$1, %r14d
	movl	%r14d, 48(%r12)
	movq	%rax, -72(%rbp)
	testq	%r15, %r15
	je	.L260
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	leaq	12(%r15), %rax
	movq	%rax, -96(%rbp)
	testq	%r13, %r13
	je	.L261
	lock addl	$1, (%rax)
.L262:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestE(%rip), %rax
	movq	%rax, (%rbx)
	movl	%r14d, 8(%rbx)
	movq	$0, 16(%rbx)
	testq	%r13, %r13
	je	.L303
	movq	-96(%rbp), %rax
	lock addl	$1, (%rax)
.L263:
	movl	$24, %edi
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE(%rip), %rsi
	movq	%rsi, (%rax)
	movq	%rdx, 8(%rax)
	movq	%r15, 16(%rax)
	testq	%r13, %r13
	je	.L304
	movq	-96(%rbp), %rdx
	lock addl	$1, (%rdx)
	movl	$-1, %ecx
	lock xaddl	%ecx, (%rdx)
.L265:
	cmpl	$1, %ecx
	jne	.L266
	movq	(%r15), %rcx
	movq	%rax, -72(%rbp)
	movq	%r15, %rdi
	call	*24(%rcx)
	movq	-72(%rbp), %rax
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE(%rip), %rsi
.L266:
	movq	16(%rbx), %r14
	movq	%rax, 16(%rbx)
	testq	%r14, %r14
	je	.L274
	movq	16(%r14), %rdi
	movq	%rsi, (%r14)
	testq	%rdi, %rdi
	je	.L305
	testq	%r13, %r13
	je	.L267
.L307:
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L268:
	cmpl	$1, %eax
	jne	.L269
	movq	(%rdi), %rax
	call	*24(%rax)
.L269:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	testq	%r15, %r15
	je	.L270
	leaq	12(%r15), %rax
	movq	%rax, -96(%rbp)
.L274:
	testq	%r13, %r13
	je	.L271
	movq	-96(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
.L272:
	cmpl	$1, %eax
	jne	.L270
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L270:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rsi
	movq	%rbx, -64(%rbp)
	call	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L273
	movq	(%rdi), %rax
	call	*16(%rax)
.L273:
	movq	-88(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node9inspector8protocol11NodeTracing10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L306
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	.cfi_restore_state
	addl	$1, 12(%r15)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L255:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L254
.L302:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L258
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L259:
	cmpl	$1, %eax
	jne	.L254
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L260:
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$24, %edi
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestE(%rip), %rax
	movl	%r14d, 8(%rbx)
	movq	%rax, (%rbx)
	movq	$0, 16(%rbx)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	16(%rbx), %r14
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rax, 16(%rbx)
	testq	%r14, %r14
	je	.L270
	movq	16(%r14), %rdi
	movq	%rcx, (%r14)
	testq	%rdi, %rdi
	je	.L299
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	jne	.L307
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L304:
	addl	$1, 12(%r15)
	movl	12(%r15), %ecx
	leal	-1(%rcx), %edi
	movl	%edi, 12(%r15)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L303:
	addl	$1, 12(%r15)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L271:
	movl	12(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 12(%r15)
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L267:
	movl	12(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 12(%rdi)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L299:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L258:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L259
.L306:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8014:
	.size	_ZN4node9inspector8protocol12TracingAgent4WireEPNS1_14UberDispatcherE, .-_ZN4node9inspector8protocol12TracingAgent4WireEPNS1_14UberDispatcherE
	.section	.text._ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,"axG",@progbits,_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.type	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, @function
_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E:
.LFB9996:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L323
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
.L312:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L310
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L308
.L311:
	movq	%rbx, %r12
	jmp	.L312
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L311
.L308:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	ret
	.cfi_endproc
.LFE9996:
	.size	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E, .-_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12TracingAgent5startESt10unique_ptrINS1_11NodeTracing11TraceConfigESt14default_deleteIS5_EE
	.type	_ZN4node9inspector8protocol12TracingAgent5startESt10unique_ptrINS1_11NodeTracing11TraceConfigESt14default_deleteIS5_EE, @function
_ZN4node9inspector8protocol12TracingAgent5startESt10unique_ptrINS1_11NodeTracing11TraceConfigESt14default_deleteIS5_EE:
.LFB8017:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -240(%rbp)
	movq	%rsi, -248(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 32(%rsi)
	je	.L327
	leaq	-96(%rbp), %r12
	movq	%rdi, %r15
	leaq	-144(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	-80(%rbp), %rbx
	movq	$64, -144(%rbp)
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-144(%rbp), %rdx
	movq	%r15, %rdi
	movq	%r12, %rsi
	movdqa	.LC7(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC8(%rip), %xmm0
	movups	%xmm0, 16(%rax)
	movdqa	.LC9(%rip), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	.LC10(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movq	-144(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L326
.L423:
	call	_ZdlPv@PLT
.L326:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L424
	movq	-240(%rbp), %rax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	.cfi_restore_state
	movq	8(%rsi), %rax
	testb	$2, 1932(%rax)
	je	.L425
	leaq	-136(%rbp), %rax
	leaq	-96(%rbp), %rcx
	xorl	%ebx, %ebx
	movl	$0, -136(%rbp)
	movq	%rax, -224(%rbp)
	leaq	-80(%rbp), %r13
	movq	%rax, -120(%rbp)
	movq	%rax, -112(%rbp)
	movq	(%rdx), %rax
	movq	%rcx, -256(%rbp)
	leaq	-160(%rbp), %rcx
	movq	48(%rax), %rdi
	movq	$0, -128(%rbp)
	movq	$0, -104(%rbp)
	movq	%rdi, -232(%rbp)
	movq	(%rdi), %rax
	movq	%rcx, -264(%rbp)
	cmpq	%rax, 8(%rdi)
	je	.L363
	.p2align 4,,10
	.p2align 3
.L332:
	movq	%rbx, %rdx
	movq	%r13, -96(%rbp)
	salq	$5, %rdx
	addq	%rdx, %rax
	movq	(%rax), %r14
	movq	8(%rax), %r12
	movq	%r14, %rax
	addq	%r12, %rax
	setne	%dl
	testq	%r14, %r14
	sete	%al
	testb	%al, %dl
	jne	.L426
	movq	%r12, -160(%rbp)
	cmpq	$15, %r12
	ja	.L427
	cmpq	$1, %r12
	jne	.L338
	movzbl	(%r14), %eax
	movb	%al, -80(%rbp)
	movq	%r13, %rax
.L339:
	movq	%r12, -88(%rbp)
	movb	$0, (%rax,%r12)
	movq	-128(%rbp), %r12
	testq	%r12, %r12
	je	.L340
	movq	-96(%rbp), %r15
	movq	-88(%rbp), %r14
	movq	%rbx, -192(%rbp)
	movq	%r13, -216(%rbp)
	movq	%r15, -208(%rbp)
	movq	%r15, %rbx
	movq	%r14, %r13
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L346:
	movq	16(%r12), %rax
	movl	$1, %esi
	testq	%rax, %rax
	je	.L342
.L428:
	movq	%rax, %r12
.L341:
	movq	40(%r12), %r15
	movq	32(%r12), %r14
	cmpq	%r15, %r13
	movq	%r15, %rdx
	cmovbe	%r13, %rdx
	testq	%rdx, %rdx
	je	.L343
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rdx, -184(%rbp)
	call	memcmp@PLT
	movq	-184(%rbp), %rdx
	testl	%eax, %eax
	jne	.L344
.L343:
	movq	%r13, %rax
	movl	$2147483648, %ecx
	subq	%r15, %rax
	cmpq	%rcx, %rax
	jge	.L345
	movabsq	$-2147483649, %rcx
	cmpq	%rcx, %rax
	jle	.L346
.L344:
	testl	%eax, %eax
	js	.L346
.L345:
	movq	24(%r12), %rax
	xorl	%esi, %esi
	testq	%rax, %rax
	jne	.L428
.L342:
	movq	%r15, %rcx
	movq	%r14, %r11
	movq	%rbx, %r15
	movq	%r13, %r14
	movq	-208(%rbp), %r10
	movq	-192(%rbp), %rbx
	movq	-216(%rbp), %r13
	testb	%sil, %sil
	jne	.L429
.L349:
	testq	%rdx, %rdx
	je	.L350
	movq	%r15, %rsi
	movq	%r11, %rdi
	movq	%r10, -208(%rbp)
	movq	%rcx, -184(%rbp)
	call	memcmp@PLT
	movq	-184(%rbp), %rcx
	movq	-208(%rbp), %r10
	testl	%eax, %eax
	jne	.L351
.L350:
	subq	%r14, %rcx
	movl	$2147483648, %eax
	cmpq	%rax, %rcx
	jge	.L352
	movabsq	$-2147483649, %rax
	cmpq	%rax, %rcx
	jle	.L353
	movl	%ecx, %eax
.L351:
	testl	%eax, %eax
	jns	.L352
.L353:
	testq	%r12, %r12
	je	.L430
.L354:
	movl	$1, %r10d
	cmpq	-224(%rbp), %r12
	jne	.L431
.L355:
	movl	$64, %edi
	movl	%r10d, -184(%rbp)
	call	_Znwm@PLT
	movl	-184(%rbp), %r10d
	movq	%rax, %rsi
	leaq	48(%rax), %rax
	movq	%rax, 32(%rsi)
	movq	-96(%rbp), %rax
	cmpq	%r13, %rax
	je	.L432
	movq	%rax, 32(%rsi)
	movq	-80(%rbp), %rax
	movq	%rax, 48(%rsi)
.L359:
	movq	-88(%rbp), %rax
	movl	%r10d, %edi
	movq	%r12, %rdx
	movq	%r13, -96(%rbp)
	movq	-224(%rbp), %rcx
	movq	$0, -88(%rbp)
	movq	%rax, 40(%rsi)
	movb	$0, -80(%rbp)
	call	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_@PLT
	addq	$1, -104(%rbp)
	movq	-96(%rbp), %r10
.L352:
	cmpq	%r13, %r10
	je	.L360
	movq	%r10, %rdi
	addq	$1, %rbx
	call	_ZdlPv@PLT
	movq	-232(%rbp), %rcx
	movq	8(%rcx), %rdi
	movq	(%rcx), %rax
	movq	%rdi, %rdx
	movq	%rdi, -184(%rbp)
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rbx, %rdx
	ja	.L332
	cmpq	$0, -104(%rbp)
	je	.L363
.L419:
	movq	-248(%rbp), %rax
	movq	16+_ZN4node11per_process11v8_platformE(%rip), %r14
	movq	24(%rax), %r12
	movl	48(%rax), %r15d
	movq	16(%rax), %xmm1
	testq	%r12, %r12
	je	.L366
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L367
	lock addl	$1, 8(%r12)
.L366:
	movq	%r12, %xmm4
	movl	$416, %edi
	punpcklqdq	%xmm4, %xmm1
	movaps	%xmm1, -208(%rbp)
	call	_Znwm@PLT
	movq	%rax, %rbx
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterE(%rip), %rax
	movq	%rax, (%rbx)
	leaq	128(%rbx), %r13
	movq	$0, 8(%rbx)
	movq	%r13, %rdi
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	movq	%rax, 128(%rbx)
	xorl	%eax, %eax
	movw	%ax, 352(%rbx)
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movups	%xmm0, 360(%rbx)
	movups	%xmm0, 376(%rbx)
	movq	%rax, 16(%rbx)
	movq	-24(%rax), %rax
	movq	$0, 344(%rbx)
	leaq	16(%rbx,%rax), %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	leaq	24+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	pxor	%xmm0, %xmm0
	movq	%rax, 128(%rbx)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rcx, %xmm2
	leaq	24(%rbx), %rsi
	movq	%rax, %xmm5
	movups	%xmm0, 32(%rbx)
	leaq	80(%rbx), %rdi
	punpcklqdq	%xmm5, %xmm2
	movups	%xmm0, 48(%rbx)
	movups	%xmm2, 16(%rbx)
	movups	%xmm0, 64(%rbx)
	movq	%rsi, -184(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movb	$0, 112(%rbx)
	movq	%r13, %rdi
	movq	%rax, 24(%rbx)
	leaq	112(%rbx), %rax
	movq	-184(%rbp), %rsi
	movl	$16, 88(%rbx)
	movq	%rax, 96(%rbx)
	movq	$0, 104(%rbx)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-208(%rbp), %xmm1
	movl	%r15d, 392(%rbx)
	movups	%xmm1, 400(%rbx)
	testq	%r12, %r12
	je	.L369
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	leaq	8(%r12), %rax
	testq	%r13, %r13
	je	.L370
	lock addl	$1, (%rax)
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
.L372:
	cmpl	$1, %eax
	je	.L433
.L369:
	leaq	-144(%rbp), %r13
	leaq	-160(%rbp), %rdi
	movl	$1, %r8d
	movq	%r14, %rsi
	leaq	-168(%rbp), %rcx
	movq	%r13, %rdx
	movq	%rbx, -168(%rbp)
	call	_ZN4node7tracing5Agent9AddClientERKSt3setINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4lessIS8_ESaIS8_EESt10unique_ptrINS0_16AsyncTraceWriterESt14default_deleteISG_EENS1_22UseDefaultCategoryModeE@PLT
	movq	-248(%rbp), %rax
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.L376
	movl	40(%rax), %esi
	call	_ZN4node7tracing5Agent10DisconnectEi@PLT
.L376:
	movq	-160(%rbp), %rax
	movq	-248(%rbp), %rbx
	movq	-168(%rbp), %rdi
	movq	%rax, 32(%rbx)
	movl	-152(%rbp), %eax
	movl	%eax, 40(%rbx)
	testq	%rdi, %rdi
	je	.L377
	movq	(%rdi), %rax
	call	*8(%rax)
.L377:
	movq	-240(%rbp), %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
.L365:
	movq	-128(%rbp), %r12
	leaq	-144(%rbp), %r13
	testq	%r12, %r12
	je	.L326
.L378:
	movq	24(%r12), %rsi
	movq	%r13, %rdi
	call	_ZNSt8_Rb_treeINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES5_St9_IdentityIS5_ESt4lessIS5_ESaIS5_EE8_M_eraseEPSt13_Rb_tree_nodeIS5_E
	movq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	16(%r12), %rbx
	cmpq	%rax, %rdi
	je	.L379
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L326
.L381:
	movq	%rbx, %r12
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L360:
	movq	-232(%rbp), %rdi
	addq	$1, %rbx
	movq	8(%rdi), %rcx
	movq	(%rdi), %rax
	movq	%rcx, %rdx
	movq	%rcx, -184(%rbp)
	subq	%rax, %rdx
	sarq	$5, %rdx
	cmpq	%rdx, %rbx
	jb	.L332
	cmpq	$0, -104(%rbp)
	jne	.L419
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L429:
	cmpq	%r12, -120(%rbp)
	je	.L354
.L384:
	movq	%r12, %rdi
	call	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base@PLT
	movq	%r15, %r10
	movq	40(%rax), %rcx
	movq	32(%rax), %r11
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L338:
	testq	%r12, %r12
	jne	.L434
	movq	%r13, %rax
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L427:
	movq	-256(%rbp), %rdi
	movq	-264(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-160(%rbp), %rax
	movq	%rax, -80(%rbp)
.L337:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-160(%rbp), %r12
	movq	-96(%rbp), %rax
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L432:
	movdqa	-80(%rbp), %xmm3
	movups	%xmm3, 48(%rsi)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L431:
	movq	40(%r12), %rcx
	cmpq	%rcx, %r14
	movq	%rcx, %rdx
	cmovbe	%r14, %rdx
	testq	%rdx, %rdx
	je	.L356
	movq	32(%r12), %rsi
	movq	%r15, %rdi
	movq	%rcx, -184(%rbp)
	call	memcmp@PLT
	movq	-184(%rbp), %rcx
	testl	%eax, %eax
	movl	%eax, %r10d
	jne	.L357
.L356:
	movq	%r14, %r8
	movl	$2147483648, %eax
	xorl	%r10d, %r10d
	subq	%rcx, %r8
	cmpq	%rax, %r8
	jge	.L355
	movabsq	$-2147483649, %rax
	cmpq	%rax, %r8
	jle	.L422
	movl	%r8d, %r10d
.L357:
	shrl	$31, %r10d
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L340:
	movq	-224(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	je	.L391
	movq	-96(%rbp), %r15
	movq	-88(%rbp), %r14
	movq	%rax, %r12
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L363:
	movq	-256(%rbp), %rbx
	leaq	-160(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r13, -96(%rbp)
	movq	$39, -160(%rbp)
	movq	%rbx, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-160(%rbp), %rdx
	movdqa	.LC15(%rip), %xmm0
	movq	%rbx, %rsi
	movq	%rax, -96(%rbp)
	movq	-240(%rbp), %rdi
	movq	%rdx, -80(%rbp)
	movl	$25964, %edx
	movups	%xmm0, (%rax)
	movdqa	.LC16(%rip), %xmm0
	movl	$1650552421, 32(%rax)
	movw	%dx, 36(%rax)
	movb	$100, 38(%rax)
	movups	%xmm0, 16(%rax)
	movq	-160(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L365
	call	_ZdlPv@PLT
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L381
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L425:
	leaq	-96(%rbp), %r12
	leaq	-144(%rbp), %rsi
	xorl	%edx, %edx
	movq	$67, -144(%rbp)
	movq	%r12, %rdi
	leaq	-80(%rbp), %rbx
	movq	%rbx, -96(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-144(%rbp), %rdx
	movq	%r12, %rsi
	movdqa	.LC11(%rip), %xmm0
	movq	%rax, -96(%rbp)
	movl	$28271, %ecx
	movq	-240(%rbp), %rdi
	movq	%rdx, -80(%rbp)
	movups	%xmm0, (%rax)
	movdqa	.LC12(%rip), %xmm0
	movw	%cx, 64(%rax)
	movups	%xmm0, 16(%rax)
	movdqa	.LC13(%rip), %xmm0
	movb	$115, 66(%rax)
	movups	%xmm0, 32(%rax)
	movdqa	.LC14(%rip), %xmm0
	movups	%xmm0, 48(%rax)
	movq	-144(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	call	_ZN4node9inspector8protocol16DispatchResponse5ErrorERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-96(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L423
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L370:
	addl	$1, 8(%r12)
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L372
	.p2align 4,,10
	.p2align 3
.L367:
	addl	$1, 8(%r12)
	jmp	.L366
.L433:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L374
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L375:
	cmpl	$1, %eax
	jne	.L369
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L369
.L374:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L375
.L391:
	movq	-224(%rbp), %r12
.L422:
	movl	$1, %r10d
	jmp	.L355
.L426:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L424:
	call	__stack_chk_fail@PLT
.L434:
	movq	%r13, %rdi
	jmp	.L337
.L430:
	movq	%r15, %r10
	jmp	.L352
	.cfi_endproc
.LFE8017:
	.size	_ZN4node9inspector8protocol12TracingAgent5startESt10unique_ptrINS1_11NodeTracing11TraceConfigESt14default_deleteIS5_EE, .-_ZN4node9inspector8protocol12TracingAgent5startESt10unique_ptrINS1_11NodeTracing11TraceConfigESt14default_deleteIS5_EE
	.section	.rodata._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_.str1.1,"aMS",@progbits,1
.LC17:
	.string	"vector::_M_realloc_insert"
	.section	.text._ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,"axG",@progbits,_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.type	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, @function
_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_:
.LFB11291:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	movabsq	$288230376151711743, %rsi
	subq	$56, %rsp
	movq	8(%rdi), %r12
	movq	(%rdi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	subq	%r14, %rax
	sarq	$5, %rax
	cmpq	%rsi, %rax
	je	.L477
	movq	%rbx, %r8
	movq	%rdi, %r15
	subq	%r14, %r8
	testq	%rax, %rax
	je	.L457
	leaq	(%rax,%rax), %rcx
	cmpq	%rcx, %rax
	jbe	.L478
	movabsq	$9223372036854775776, %rcx
.L437:
	movq	%rcx, %rdi
	movq	%rdx, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %r13
.L455:
	movq	(%rdx), %r10
	movq	8(%rdx), %r9
	addq	%r13, %r8
	leaq	16(%r8), %rdi
	movq	%r10, %rax
	movq	%rdi, (%r8)
	addq	%r9, %rax
	je	.L439
	testq	%r10, %r10
	je	.L479
.L439:
	movq	%r9, -64(%rbp)
	cmpq	$15, %r9
	ja	.L480
	cmpq	$1, %r9
	jne	.L442
	movzbl	(%r10), %eax
	movb	%al, 16(%r8)
.L443:
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	je	.L459
.L484:
	movq	%r13, %rdx
	movq	%r14, %rax
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
.L475:
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %rbx
	je	.L481
.L448:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	jne	.L445
	movdqu	16(%rax), %xmm1
	movups	%xmm1, 16(%rdx)
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L478:
	testq	%rcx, %rcx
	jne	.L438
	xorl	%r13d, %r13d
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L481:
	movq	%rbx, %r8
	subq	%r14, %r8
	addq	%r13, %r8
.L444:
	addq	$32, %r8
	cmpq	%r12, %rbx
	je	.L449
	movq	%rbx, %rax
	movq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L453:
	leaq	16(%rdx), %rsi
	leaq	16(%rax), %rdi
	movq	%rsi, (%rdx)
	movq	(%rax), %rsi
	cmpq	%rdi, %rsi
	je	.L482
	movq	%rsi, (%rdx)
	movq	16(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movq	%rsi, -16(%rdx)
	movq	-24(%rax), %rsi
	movq	%rsi, -24(%rdx)
	cmpq	%r12, %rax
	jne	.L453
.L451:
	subq	%rbx, %r12
	addq	%r12, %r8
.L449:
	testq	%r14, %r14
	je	.L454
	movq	%r14, %rdi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
.L454:
	movq	%r13, %xmm0
	addq	%rcx, %r13
	movq	%r8, %xmm3
	movq	%r13, 16(%r15)
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, (%r15)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L483
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L482:
	.cfi_restore_state
	movdqu	16(%rax), %xmm2
	movq	8(%rax), %rsi
	addq	$32, %rax
	addq	$32, %rdx
	movups	%xmm2, -16(%rdx)
	movq	%rsi, -24(%rdx)
	cmpq	%rax, %r12
	jne	.L453
	jmp	.L451
	.p2align 4,,10
	.p2align 3
.L457:
	movl	$32, %ecx
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L442:
	testq	%r9, %r9
	jne	.L441
	movq	%r9, 8(%r8)
	movb	$0, (%rdi,%r9)
	cmpq	%r14, %rbx
	jne	.L484
.L459:
	movq	%r13, %r8
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L480:
	movq	%r8, %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rcx, -96(%rbp)
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r10
	movq	%rax, %rdi
	movq	-88(%rbp), %r9
	movq	-96(%rbp), %rcx
	movq	%rax, (%r8)
	movq	-64(%rbp), %rax
	movq	%rax, 16(%r8)
.L441:
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%rcx, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	-64(%rbp), %r9
	movq	-80(%rbp), %rcx
	movq	(%r8), %rdi
	jmp	.L443
.L479:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L438:
	cmpq	%rsi, %rcx
	cmova	%rsi, %rcx
	salq	$5, %rcx
	jmp	.L437
.L483:
	call	__stack_chk_fail@PLT
.L477:
	leaq	.LC17(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE11291:
	.size	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_, .-_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12TracingAgent13getCategoriesEPSt10unique_ptrINS1_5ArrayINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEESt14default_deleteISB_EE
	.type	_ZN4node9inspector8protocol12TracingAgent13getCategoriesEPSt10unique_ptrINS1_5ArrayINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEESt14default_deleteISB_EE, @function
_ZN4node9inspector8protocol12TracingAgent13getCategoriesEPSt10unique_ptrINS1_5ArrayINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEESt14default_deleteISB_EE:
.LFB8030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$24, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$328, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%rbx), %r14
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rax, %r12
	movq	%rax, (%rbx)
	movups	%xmm0, (%rax)
	testq	%r14, %r14
	je	.L486
	movq	8(%r14), %r15
	movq	(%r14), %r12
	cmpq	%r12, %r15
	je	.L491
	.p2align 4,,10
	.p2align 3
.L487:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L490
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%r12, %r15
	jne	.L487
.L491:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L489
.L488:
	call	_ZdlPv@PLT
.L489:
	movl	$24, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
	movq	(%rbx), %r12
.L486:
	leaq	-304(%rbp), %rcx
	movl	$1701080942, -304(%rbp)
	leaq	-320(%rbp), %rdx
	movb	$0, -300(%rbp)
	movq	8(%r12), %r15
	movq	%rcx, -320(%rbp)
	movq	$4, -312(%rbp)
	cmpq	16(%r12), %r15
	je	.L493
	leaq	16(%r15), %rdi
	movq	-312(%rbp), %r8
	movq	%rdi, (%r15)
	movq	-320(%rbp), %r9
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L494
	testq	%r9, %r9
	je	.L502
.L494:
	movq	%r8, -328(%rbp)
	cmpq	$15, %r8
	ja	.L622
	cmpq	$1, %r8
	jne	.L497
	movzbl	(%r9), %eax
	movb	%al, 16(%r15)
.L498:
	movq	%r8, 8(%r15)
	movb	$0, (%rdi,%r8)
	addq	$32, 8(%r12)
.L499:
	movq	-320(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L500
	call	_ZdlPv@PLT
.L500:
	movq	(%rbx), %r8
	leaq	-272(%rbp), %rcx
	movabsq	$8751445352808476526, %rax
	movl	$25454, %edi
	movq	%rax, -272(%rbp)
	leaq	-288(%rbp), %rdx
	movw	%di, -264(%rbp)
	movb	$0, -262(%rbp)
	movq	8(%r8), %r12
	movq	%rcx, -288(%rbp)
	movq	$10, -280(%rbp)
	cmpq	16(%r8), %r12
	je	.L501
	leaq	16(%r12), %rdi
	movq	-280(%rbp), %r15
	movq	%rdi, (%r12)
	movq	-288(%rbp), %r9
	movq	%r9, %rax
	addq	%r15, %rax
	je	.L559
	testq	%r9, %r9
	je	.L502
.L559:
	movq	%r15, -328(%rbp)
	cmpq	$15, %r15
	ja	.L623
	cmpq	$1, %r15
	jne	.L506
	movzbl	(%r9), %eax
	movb	%al, 16(%r12)
.L507:
	movq	%r15, 8(%r12)
	movb	$0, (%rdi,%r15)
	addq	$32, 8(%r8)
.L508:
	movq	-288(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L509
	call	_ZdlPv@PLT
.L509:
	movq	(%rbx), %r8
	leaq	-240(%rbp), %rcx
	movabsq	$8029744612033982318, %rax
	movl	$28769, %esi
	movq	%rax, -240(%rbp)
	leaq	-256(%rbp), %rdx
	movl	$1920234356, -232(%rbp)
	movw	%si, -228(%rbp)
	movb	$0, -226(%rbp)
	movq	8(%r8), %r12
	movq	%rcx, -256(%rbp)
	movq	$14, -248(%rbp)
	cmpq	16(%r8), %r12
	je	.L510
	leaq	16(%r12), %rdi
	movq	-248(%rbp), %r15
	movq	%rdi, (%r12)
	movq	-256(%rbp), %r9
	movq	%r9, %rax
	addq	%r15, %rax
	je	.L560
	testq	%r9, %r9
	je	.L502
.L560:
	movq	%r15, -328(%rbp)
	cmpq	$15, %r15
	ja	.L624
	cmpq	$1, %r15
	jne	.L514
	movzbl	(%r9), %eax
	movb	%al, 16(%r12)
.L515:
	movq	%r15, 8(%r12)
	movb	$0, (%rdi,%r15)
	addq	$32, 8(%r8)
.L516:
	movq	-256(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L517
	call	_ZdlPv@PLT
.L517:
	movq	(%rbx), %r8
	leaq	-208(%rbp), %rcx
	movabsq	$3347131297522020206, %rax
	movl	$1668184435, -200(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-224(%rbp), %rdx
	movb	$0, -196(%rbp)
	movq	8(%r8), %r12
	movq	%rcx, -224(%rbp)
	movq	$12, -216(%rbp)
	cmpq	16(%r8), %r12
	je	.L518
	leaq	16(%r12), %rdi
	movq	-216(%rbp), %r15
	movq	%rdi, (%r12)
	movq	-224(%rbp), %r9
	movq	%r9, %rax
	addq	%r15, %rax
	je	.L561
	testq	%r9, %r9
	je	.L502
.L561:
	movq	%r15, -328(%rbp)
	cmpq	$15, %r15
	ja	.L625
	cmpq	$1, %r15
	jne	.L522
	movzbl	(%r9), %eax
	movb	%al, 16(%r12)
.L523:
	movq	%r15, 8(%r12)
	movb	$0, (%rdi,%r15)
	addq	$32, 8(%r8)
.L524:
	movq	-224(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L525
	call	_ZdlPv@PLT
.L525:
	movq	(%rbx), %r8
	leaq	-176(%rbp), %rcx
	movabsq	$8243118037543448430, %rax
	movb	$102, -168(%rbp)
	movq	%rax, -176(%rbp)
	leaq	-192(%rbp), %rdx
	movb	$0, -167(%rbp)
	movq	8(%r8), %r12
	movq	%rcx, -192(%rbp)
	movq	$9, -184(%rbp)
	cmpq	16(%r8), %r12
	je	.L526
	leaq	16(%r12), %rdi
	movq	-184(%rbp), %r15
	movq	%rdi, (%r12)
	movq	-192(%rbp), %r9
	movq	%r9, %rax
	addq	%r15, %rax
	je	.L562
	testq	%r9, %r9
	je	.L502
.L562:
	movq	%r15, -328(%rbp)
	cmpq	$15, %r15
	ja	.L626
	cmpq	$1, %r15
	jne	.L530
	movzbl	(%r9), %eax
	leaq	-328(%rbp), %r14
	movb	%al, 16(%r12)
.L531:
	movq	%r15, 8(%r12)
	movb	$0, (%rdi,%r15)
	addq	$32, 8(%r8)
.L532:
	movq	-192(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L533
	call	_ZdlPv@PLT
.L533:
	leaq	-160(%rbp), %r8
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	(%rbx), %r12
	leaq	-144(%rbp), %rax
	movq	%r8, %rdi
	movq	%r8, -352(%rbp)
	movq	%rax, -344(%rbp)
	movq	%rax, -160(%rbp)
	movq	$20, -328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-328(%rbp), %rdx
	movdqa	.LC18(%rip), %xmm0
	movq	%rax, -160(%rbp)
	movq	-352(%rbp), %r8
	movq	%rdx, -144(%rbp)
	movups	%xmm0, (%rax)
	movq	-160(%rbp), %rdx
	movl	$1735289197, 16(%rax)
	movq	-328(%rbp), %rax
	movq	%rax, -152(%rbp)
	movb	$0, (%rdx,%rax)
	movq	8(%r12), %r15
	cmpq	16(%r12), %r15
	je	.L534
	leaq	16(%r15), %rdi
	movq	-152(%rbp), %r8
	movq	%rdi, (%r15)
	movq	-160(%rbp), %r10
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L563
	testq	%r10, %r10
	je	.L502
.L563:
	movq	%r8, -328(%rbp)
	cmpq	$15, %r8
	ja	.L627
	cmpq	$1, %r8
	jne	.L538
	movzbl	(%r10), %eax
	movb	%al, 16(%r15)
.L539:
	movq	%r8, 8(%r15)
	movb	$0, (%rdi,%r8)
	addq	$32, 8(%r12)
.L540:
	movq	-160(%rbp), %rdi
	cmpq	-344(%rbp), %rdi
	je	.L541
	call	_ZdlPv@PLT
.L541:
	leaq	-128(%rbp), %r8
	leaq	-112(%rbp), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, %rdi
	movq	(%rbx), %r12
	movq	%rax, -128(%rbp)
	movq	%r8, -352(%rbp)
	movq	%rax, -344(%rbp)
	movq	$18, -328(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-328(%rbp), %rdx
	movdqa	.LC19(%rip), %xmm0
	movq	%rax, -128(%rbp)
	movq	-352(%rbp), %r8
	movq	%rdx, -112(%rbp)
	movl	$31078, %edx
	movups	%xmm0, (%rax)
	movw	%dx, 16(%rax)
	movq	-128(%rbp), %rdx
	movq	-328(%rbp), %rax
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	8(%r12), %r15
	cmpq	16(%r12), %r15
	je	.L542
	leaq	16(%r15), %rdi
	movq	-120(%rbp), %r8
	movq	%rdi, (%r15)
	movq	-128(%rbp), %r10
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L564
	testq	%r10, %r10
	je	.L502
.L564:
	movq	%r8, -328(%rbp)
	cmpq	$15, %r8
	ja	.L628
	cmpq	$1, %r8
	jne	.L546
	movzbl	(%r10), %eax
	movb	%al, 16(%r15)
.L547:
	movq	%r8, 8(%r15)
	movb	$0, (%rdi,%r8)
	addq	$32, 8(%r12)
.L548:
	movq	-128(%rbp), %rdi
	cmpq	-344(%rbp), %rdi
	je	.L549
	call	_ZdlPv@PLT
.L549:
	movq	(%rbx), %rbx
	leaq	-80(%rbp), %rcx
	movl	$14454, %eax
	movb	$0, -78(%rbp)
	movw	%ax, -80(%rbp)
	leaq	-96(%rbp), %rdx
	movq	%rcx, -96(%rbp)
	movq	8(%rbx), %r12
	movq	$2, -88(%rbp)
	cmpq	16(%rbx), %r12
	je	.L550
	leaq	16(%r12), %rdi
	movq	-88(%rbp), %r15
	movq	%rdi, (%r12)
	movq	-96(%rbp), %r8
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L565
	testq	%r8, %r8
	je	.L502
.L565:
	movq	%r15, -328(%rbp)
	cmpq	$15, %r15
	ja	.L629
	cmpq	$1, %r15
	jne	.L554
	movzbl	(%r8), %eax
	movb	%al, 16(%r12)
.L555:
	movq	%r15, 8(%r12)
	movb	$0, (%rdi,%r15)
	addq	$32, 8(%rbx)
.L556:
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L630
	addq	$328, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %r15
	jne	.L487
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L488
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L622:
	leaq	-328(%rbp), %r14
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rcx, -360(%rbp)
	movq	%r14, %rsi
	movq	%r8, -352(%rbp)
	movq	%r9, -344(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-344(%rbp), %r9
	movq	-352(%rbp), %r8
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	-360(%rbp), %rcx
	movq	%rax, 16(%r15)
.L496:
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%rcx, -344(%rbp)
	call	memcpy@PLT
	movq	-328(%rbp), %r8
	movq	(%r15), %rdi
	movq	-344(%rbp), %rcx
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rcx, -344(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-344(%rbp), %rcx
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L501:
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%rcx, -344(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-344(%rbp), %rcx
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L534:
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L526:
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%rcx, -344(%rbp)
	leaq	-328(%rbp), %r14
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-344(%rbp), %rcx
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L518:
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%rcx, -344(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-344(%rbp), %rcx
	jmp	.L524
	.p2align 4,,10
	.p2align 3
.L510:
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%rcx, -344(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-344(%rbp), %rcx
	jmp	.L516
	.p2align 4,,10
	.p2align 3
.L550:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -344(%rbp)
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	movq	-344(%rbp), %rcx
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZNSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaIS5_EE17_M_realloc_insertIJRKS5_EEEvN9__gnu_cxx17__normal_iteratorIPS5_S7_EEDpOT_
	jmp	.L548
	.p2align 4,,10
	.p2align 3
.L497:
	testq	%r8, %r8
	je	.L498
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L530:
	leaq	-328(%rbp), %r14
	testq	%r15, %r15
	je	.L531
	jmp	.L529
	.p2align 4,,10
	.p2align 3
.L522:
	testq	%r15, %r15
	je	.L523
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L506:
	testq	%r15, %r15
	je	.L507
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L514:
	testq	%r15, %r15
	je	.L515
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L546:
	testq	%r8, %r8
	je	.L547
	jmp	.L545
	.p2align 4,,10
	.p2align 3
.L538:
	testq	%r8, %r8
	je	.L539
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L554:
	testq	%r15, %r15
	je	.L555
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L623:
	leaq	-328(%rbp), %r14
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rcx, -360(%rbp)
	movq	%r14, %rsi
	movq	%r9, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-344(%rbp), %r8
	movq	-352(%rbp), %r9
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	-360(%rbp), %rcx
	movq	%rax, 16(%r12)
.L505:
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%rcx, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	memcpy@PLT
	movq	-328(%rbp), %r15
	movq	(%r12), %rdi
	movq	-352(%rbp), %rcx
	movq	-344(%rbp), %r8
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L628:
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r8, -360(%rbp)
	movq	%r10, -352(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-352(%rbp), %r10
	movq	-360(%rbp), %r8
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	%rax, 16(%r15)
.L545:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-328(%rbp), %r8
	movq	(%r15), %rdi
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	-328(%rbp), %r14
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rcx, -360(%rbp)
	movq	%r14, %rsi
	movq	%r9, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-344(%rbp), %r8
	movq	-352(%rbp), %r9
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	-360(%rbp), %rcx
	movq	%rax, 16(%r12)
.L513:
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%rcx, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	memcpy@PLT
	movq	-328(%rbp), %r15
	movq	(%r12), %rdi
	movq	-352(%rbp), %rcx
	movq	-344(%rbp), %r8
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L627:
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%r10, -360(%rbp)
	movq	%r8, -352(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-352(%rbp), %r8
	movq	-360(%rbp), %r10
	movq	%rax, (%r15)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	%rax, 16(%r15)
.L537:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-328(%rbp), %r8
	movq	(%r15), %rdi
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L625:
	leaq	-328(%rbp), %r14
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rcx, -360(%rbp)
	movq	%r14, %rsi
	movq	%r9, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-344(%rbp), %r8
	movq	-352(%rbp), %r9
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	-360(%rbp), %rcx
	movq	%rax, 16(%r12)
.L521:
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%rcx, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	memcpy@PLT
	movq	-328(%rbp), %r15
	movq	(%r12), %rdi
	movq	-352(%rbp), %rcx
	movq	-344(%rbp), %r8
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L626:
	leaq	-328(%rbp), %r14
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%rcx, -360(%rbp)
	movq	%r14, %rsi
	movq	%r9, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-344(%rbp), %r8
	movq	-352(%rbp), %r9
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	-360(%rbp), %rcx
	movq	%rax, 16(%r12)
.L529:
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%rcx, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	memcpy@PLT
	movq	-328(%rbp), %r15
	movq	(%r12), %rdi
	movq	-352(%rbp), %rcx
	movq	-344(%rbp), %r8
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L629:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	%rcx, -352(%rbp)
	movq	%r8, -344(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-344(%rbp), %r8
	movq	-352(%rbp), %rcx
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-328(%rbp), %rax
	movq	%rax, 16(%r12)
.L553:
	movq	%r15, %rdx
	movq	%r8, %rsi
	movq	%rcx, -344(%rbp)
	call	memcpy@PLT
	movq	-328(%rbp), %r15
	movq	(%r12), %rdi
	movq	-344(%rbp), %rcx
	jmp	.L555
.L502:
	leaq	.LC0(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L630:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8030:
	.size	_ZN4node9inspector8protocol12TracingAgent13getCategoriesEPSt10unique_ptrINS1_5ArrayINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEESt14default_deleteISB_EE, .-_ZN4node9inspector8protocol12TracingAgent13getCategoriesEPSt10unique_ptrINS1_5ArrayINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEESt14default_deleteISB_EE
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE, 32
_ZTVN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_124DeletableFrontendWrapperD0Ev
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestE, 40
_ZTVN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_128CreateFrontendWrapperRequestD0Ev
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestE, 40
_ZTVN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequest4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_129DestroyFrontendWrapperRequestD0Ev
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestE, 40
_ZTVN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequest4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_118SendMessageRequestD0Ev
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterE, 56
_ZTVN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriterD0Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter16AppendTraceEventEPN2v88platform7tracing11TraceObjectE
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_120InspectorTraceWriter5FlushEb
	.quad	_ZN4node7tracing16AsyncTraceWriter18InitializeOnThreadEP9uv_loop_s
	.weak	_ZTVN4node9inspector8protocol12TracingAgentE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol12TracingAgentE,"awG",@progbits,_ZTVN4node9inspector8protocol12TracingAgentE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol12TracingAgentE, @object
	.size	_ZTVN4node9inspector8protocol12TracingAgentE, 64
_ZTVN4node9inspector8protocol12TracingAgentE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12TracingAgentD1Ev
	.quad	_ZN4node9inspector8protocol12TracingAgentD0Ev
	.quad	_ZN4node9inspector8protocol12TracingAgent13getCategoriesEPSt10unique_ptrINS1_5ArrayINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEESt14default_deleteISB_EE
	.quad	_ZN4node9inspector8protocol12TracingAgent5startESt10unique_ptrINS1_11NodeTracing11TraceConfigESt14default_deleteIS5_EE
	.quad	_ZN4node9inspector8protocol12TracingAgent4stopEv
	.quad	_ZN4node9inspector8protocol11NodeTracing7Backend7disableEv
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeTracing8FrontendESaIS4_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.quad	7237117975334822523
	.quad	6081377301048736290
	.align 16
.LC3:
	.quad	7218820976633667954
	.quad	7308335520443102305
	.section	.data.rel.ro,"aw"
	.align 8
.LC4:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC5:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC6:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.section	.rodata.cst16
	.align 16
.LC7:
	.quad	7237089027372638531
	.quad	7453010308902311013
	.align 16
.LC8:
	.quad	8031079651229383226
	.quad	8247252484900549408
	.align 16
.LC9:
	.quad	7305437174154486625
	.quad	7237413383185788774
	.align 16
.LC10:
	.quad	7526676505849066593
	.quad	7451599682962399333
	.align 16
.LC11:
	.quad	2334956330732843604
	.quad	7598824251284484720
	.align 16
.LC12:
	.quad	8007521502018696037
	.quad	7142820481135635566
	.align 16
.LC13:
	.quad	8367798494344143208
	.quad	7863399742950044264
	.align 16
.LC14:
	.quad	7310019993145338209
	.quad	7598543875864552545
	.align 16
.LC15:
	.quad	8391157618955023425
	.quad	8386092971932413728
	.align 16
.LC16:
	.quad	7526395108249266021
	.quad	2334379873124775279
	.align 16
.LC18:
	.quad	8243118037543448430
	.quad	7598824251335192166
	.align 16
.LC19:
	.quad	8243118037543448430
	.quad	7598247041922510438
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
