	.file	"runtime_agent.cc"
	.text
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB10319:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE10319:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB10324:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE10324:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv,"axG",@progbits,_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv
	.type	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv, @function
_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv:
.LFB4860:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L7
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L7:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4860:
	.size	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv, .-_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12RuntimeAgent30notifyWhenWaitingForDisconnectEb
	.type	_ZN4node9inspector8protocol12RuntimeAgent30notifyWhenWaitingForDisconnectEb, @function
_ZN4node9inspector8protocol12RuntimeAgent30notifyWhenWaitingForDisconnectEb:
.LFB7430:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	%dl, 24(%rsi)
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L11
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L11:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7430:
	.size	_ZN4node9inspector8protocol12RuntimeAgent30notifyWhenWaitingForDisconnectEb, .-_ZN4node9inspector8protocol12RuntimeAgent30notifyWhenWaitingForDisconnectEb
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB10323:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE10323:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB10321:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10321:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB10322:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L14
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	ret
	.cfi_endproc
.LFE10322:
	.size	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN4node9inspector8protocol12RuntimeAgentD2Ev,"axG",@progbits,_ZN4node9inspector8protocol12RuntimeAgentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol12RuntimeAgentD2Ev
	.type	_ZN4node9inspector8protocol12RuntimeAgentD2Ev, @function
_ZN4node9inspector8protocol12RuntimeAgentD2Ev:
.LFB10299:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12RuntimeAgentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L16
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L19
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L25
.L16:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L16
.L25:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L22
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L23:
	cmpl	$1, %eax
	jne	.L16
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L22:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L23
	.cfi_endproc
.LFE10299:
	.size	_ZN4node9inspector8protocol12RuntimeAgentD2Ev, .-_ZN4node9inspector8protocol12RuntimeAgentD2Ev
	.weak	_ZN4node9inspector8protocol12RuntimeAgentD1Ev
	.set	_ZN4node9inspector8protocol12RuntimeAgentD1Ev,_ZN4node9inspector8protocol12RuntimeAgentD2Ev
	.section	.text._ZN4node9inspector8protocol12RuntimeAgentD0Ev,"axG",@progbits,_ZN4node9inspector8protocol12RuntimeAgentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol12RuntimeAgentD0Ev
	.type	_ZN4node9inspector8protocol12RuntimeAgentD0Ev, @function
_ZN4node9inspector8protocol12RuntimeAgentD0Ev:
.LFB10301:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12RuntimeAgentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L28
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L29
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L35
.L28:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L28
.L35:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L32
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L33:
	cmpl	$1, %eax
	jne	.L28
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L33
	.cfi_endproc
.LFE10301:
	.size	_ZN4node9inspector8protocol12RuntimeAgentD0Ev, .-_ZN4node9inspector8protocol12RuntimeAgentD0Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12RuntimeAgentC2Ev
	.type	_ZN4node9inspector8protocol12RuntimeAgentC2Ev, @function
_ZN4node9inspector8protocol12RuntimeAgentC2Ev:
.LFB7425:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol12RuntimeAgentE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	$0, 16(%rdi)
	movb	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE7425:
	.size	_ZN4node9inspector8protocol12RuntimeAgentC2Ev, .-_ZN4node9inspector8protocol12RuntimeAgentC2Ev
	.globl	_ZN4node9inspector8protocol12RuntimeAgentC1Ev
	.set	_ZN4node9inspector8protocol12RuntimeAgentC1Ev,_ZN4node9inspector8protocol12RuntimeAgentC2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12RuntimeAgent26notifyWaitingForDisconnectEv
	.type	_ZN4node9inspector8protocol12RuntimeAgent26notifyWaitingForDisconnectEv, @function
_ZN4node9inspector8protocol12RuntimeAgent26notifyWaitingForDisconnectEv:
.LFB7431:
	.cfi_startproc
	endbr64
	movzbl	24(%rdi), %eax
	testb	%al, %al
	jne	.L46
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	8(%rdi), %rdi
	movb	%al, -1(%rbp)
	call	_ZN4node9inspector8protocol11NodeRuntime8Frontend20waitingForDisconnectEv@PLT
	movzbl	-1(%rbp), %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7431:
	.size	_ZN4node9inspector8protocol12RuntimeAgent26notifyWaitingForDisconnectEv, .-_ZN4node9inspector8protocol12RuntimeAgent26notifyWaitingForDisconnectEv
	.section	.text._ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E,"axG",@progbits,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC5IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E
	.type	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E, @function
_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E:
.LFB9844:
	.cfi_startproc
	endbr64
	cmpq	$0, (%rsi)
	movq	$0, (%rdi)
	je	.L50
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	call	_Znwm@PLT
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movabsq	$4294967297, %rcx
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, (%r12)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	ret
	.cfi_endproc
.LFE9844:
	.size	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E, .-_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E
	.weak	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E
	.set	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E,_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC2IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol12RuntimeAgent4WireEPNS1_14UberDispatcherE
	.type	_ZN4node9inspector8protocol12RuntimeAgent4WireEPNS1_14UberDispatcherE, @function
_ZN4node9inspector8protocol12RuntimeAgent4WireEPNS1_14UberDispatcherE:
.LFB7427:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	leaq	-72(%rbp), %rsi
	leaq	-64(%rbp), %rdi
	movq	%r14, (%rax)
	movq	%rax, %rbx
	movq	%rax, -72(%rbp)
	call	_ZNSt14__shared_countILN9__gnu_cxx12_Lock_policyE2EEC1IN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS8_EEEOSt10unique_ptrIT_T0_E
	movq	-64(%rbp), %r14
	testq	%r14, %r14
	je	.L54
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L55
	lock addl	$1, 8(%r14)
	movq	-64(%rbp), %r15
	testq	%r15, %r15
	je	.L54
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L78
.L54:
	movq	16(%r12), %r15
	movq	%rbx, %xmm0
	movq	%r14, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%r12)
	testq	%r15, %r15
	je	.L62
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L63
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L79
	.p2align 4,,10
	.p2align 3
.L62:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L68
	movl	$8, %esi
	call	_ZdlPvm@PLT
.L68:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN4node9inspector8protocol11NodeRuntime10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L80
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
	movl	8(%r15), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r15)
	cmpl	$1, %eax
	jne	.L62
.L79:
	movq	(%r15), %rax
	movq	%rdx, -88(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L66
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L67:
	cmpl	$1, %eax
	jne	.L62
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L55:
	addl	$1, 8(%r14)
	movl	8(%r14), %eax
	movq	%r14, %r15
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r14)
	cmpl	$1, %eax
	jne	.L54
.L78:
	movq	(%r15), %rax
	movq	%rdx, -88(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L59
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L60:
	cmpl	$1, %eax
	jne	.L54
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L59:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L66:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L67
.L80:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7427:
	.size	_ZN4node9inspector8protocol12RuntimeAgent4WireEPNS1_14UberDispatcherE, .-_ZN4node9inspector8protocol12RuntimeAgent4WireEPNS1_14UberDispatcherE
	.weak	_ZTVN4node9inspector8protocol12RuntimeAgentE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol12RuntimeAgentE,"awG",@progbits,_ZTVN4node9inspector8protocol12RuntimeAgentE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol12RuntimeAgentE, @object
	.size	_ZTVN4node9inspector8protocol12RuntimeAgentE, 48
_ZTVN4node9inspector8protocol12RuntimeAgentE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12RuntimeAgentD1Ev
	.quad	_ZN4node9inspector8protocol12RuntimeAgentD0Ev
	.quad	_ZN4node9inspector8protocol12RuntimeAgent30notifyWhenWaitingForDisconnectEb
	.quad	_ZN4node9inspector8protocol11NodeRuntime7Backend7disableEv
	.weak	_ZTVSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt19_Sp_counted_deleterIPN4node9inspector8protocol11NodeRuntime8FrontendESt14default_deleteIS4_ESaIvELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
