	.file	"node_string.cc"
	.text
	.section	.rodata._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci.str1.1,"aMS",@progbits,1
.LC0:
	.string	"basic_string::append"
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci:
.LFB6804:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	movslq	%edx, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%rdi), %rax
	cmpq	%rax, %rdx
	ja	.L7
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L7:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE6804:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev:
.LFB5765:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rax, (%rdi)
	jmp	_ZN6icu_678ByteSinkD2Ev@PLT
	.cfi_endproc
.LFE5765:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.set	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED2Ev
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev:
.LFB5767:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN6icu_677UMemorydlEPv@PLT
	.cfi_endproc
.LFE5767:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil9parseJSONEN12v8_inspector10StringViewE
	.type	_ZN4node9inspector8protocol10StringUtil9parseJSONEN12v8_inspector10StringViewE, @function
_ZN4node9inspector8protocol10StringUtil9parseJSONEN12v8_inspector10StringViewE:
.LFB5089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	24(%rbp), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L12
	movq	$0, (%rdi)
.L11:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L17
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	.cfi_restore_state
	cmpb	$0, 16(%rbp)
	movq	32(%rbp), %rsi
	je	.L14
	call	_ZN4node9inspector8protocol19parseJSONCharactersEPKhj@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L14:
	call	_ZN4node9inspector8protocol19parseJSONCharactersEPKtj@PLT
	jmp	.L11
.L17:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5089:
	.size	_ZN4node9inspector8protocol10StringUtil9parseJSONEN12v8_inspector10StringViewE, .-_ZN4node9inspector8protocol10StringUtil9parseJSONEN12v8_inspector10StringViewE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"C"
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil10fromDoubleB5cxx11Ed
	.type	_ZN4node9inspector8protocol10StringUtil10fromDoubleB5cxx11Ed, @function
_ZN4node9inspector8protocol10StringUtil10fromDoubleB5cxx11Ed:
.LFB5091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	leaq	-432(%rbp), %r15
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-368(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-320(%rbp), %r12
	pushq	%rbx
	movq	%r12, %rdi
	subq	$456, %rsp
	.cfi_offset 3, -56
	movsd	%xmm0, -488(%rbp)
	movq	.LC2(%rip), %xmm2
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movhps	.LC3(%rip), %xmm2
	movaps	%xmm2, -480(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	pxor	%xmm1, %xmm1
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movups	%xmm1, -88(%rbp)
	movups	%xmm1, -72(%rbp)
	movw	%ax, -96(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -104(%rbp)
	addq	%r15, %rax
	movq	%rbx, -432(%rbp)
	movq	%rax, %rdi
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, -456(%rbp)
	leaq	-448(%rbp), %r15
	movq	%rax, (%rdi)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movdqa	-480(%rbp), %xmm2
	pxor	%xmm1, %xmm1
	movq	%r13, %rdi
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movaps	%xmm1, -416(%rbp)
	movaps	%xmm2, -432(%rbp)
	movaps	%xmm1, -400(%rbp)
	movaps	%xmm1, -384(%rbp)
	movq	%rax, -320(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r12, %rdi
	leaq	-424(%rbp), %rsi
	movq	%rax, -424(%rbp)
	leaq	-336(%rbp), %rax
	movq	%rax, -480(%rbp)
	movq	%rax, -352(%rbp)
	movl	$16, -360(%rbp)
	movq	$0, -344(%rbp)
	movb	$0, -336(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	_ZNSt6localeC1EPKc@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	leaq	-440(%rbp), %rdi
	movq	%rdi, -464(%rbp)
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5imbueERKSt6locale@PLT
	movq	-464(%rbp), %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	%r15, %rdi
	call	_ZNSt6localeD1Ev@PLT
	movsd	-488(%rbp), %xmm0
	movq	-456(%rbp), %rdi
	call	_ZNSo9_M_insertIdEERSoT_@PLT
	leaq	16(%r14), %rax
	movb	$0, 16(%r14)
	movq	%rax, (%r14)
	movq	-384(%rbp), %rax
	movq	$0, 8(%r14)
	testq	%rax, %rax
	je	.L19
	movq	-400(%rbp), %r8
	movq	-392(%rbp), %rcx
	cmpq	%r8, %rax
	ja	.L25
	subq	%rcx, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L21:
	movq	.LC2(%rip), %xmm0
	leaq	64+_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-352(%rbp), %rdi
	movq	%rax, -320(%rbp)
	movhps	.LC4(%rip), %xmm0
	movaps	%xmm0, -432(%rbp)
	cmpq	-480(%rbp), %rdi
	je	.L22
	call	_ZdlPv@PLT
.L22:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -424(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-24(%rbx), %rax
	movq	16+_ZTTNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rdx
	movq	%r12, %rdi
	movq	%rbx, -432(%rbp)
	movq	%rdx, -432(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L26
	addq	$456, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	.cfi_restore_state
	subq	%rcx, %rax
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	-352(%rbp), %rsi
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	jmp	.L21
.L26:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5091:
	.size	_ZN4node9inspector8protocol10StringUtil10fromDoubleB5cxx11Ed, .-_ZN4node9inspector8protocol10StringUtil10fromDoubleB5cxx11Ed
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"basic_string::_M_construct null not valid"
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil8toDoubleEPKcmPb
	.type	_ZN4node9inspector8protocol10StringUtil8toDoubleEPKcmPb, @function
_ZN4node9inspector8protocol10StringUtil8toDoubleEPKcmPb:
.LFB5092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-464(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -512(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%rdi, %rax
	movq	%r14, -480(%rbp)
	addq	%rsi, %rax
	je	.L28
	testq	%rdi, %rdi
	je	.L33
.L28:
	movq	%r12, -488(%rbp)
	cmpq	$15, %r12
	ja	.L55
	cmpq	$1, %r12
	jne	.L31
	movzbl	(%r15), %eax
	movb	%al, -464(%rbp)
	movq	%r14, %rax
.L32:
	movq	%r12, -472(%rbp)
	leaq	-448(%rbp), %r15
	movb	$0, (%rax,%r12)
	leaq	-328(%rbp), %r12
	movq	%r12, %rdi
	movq	%r15, -520(%rbp)
	call	_ZNSt8ios_baseC2Ev@PLT
	movq	8+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rbx
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movaps	%xmm0, -96(%rbp)
	movq	16+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%rax, -328(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -80(%rbp)
	movw	%ax, -104(%rbp)
	movq	-24(%rbx), %rax
	movq	$0, -112(%rbp)
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	movq	$0, -440(%rbp)
	movq	-24(%rbx), %rdi
	addq	%r15, %rdi
	leaq	-376(%rbp), %r15
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%r15, %rdi
	movq	$0, -424(%rbp)
	movq	%rax, -448(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -432(%rbp)
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	movq	$0, -400(%rbp)
	movq	$0, -392(%rbp)
	movq	$0, -384(%rbp)
	call	_ZNSt6localeC1Ev@PLT
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	-480(%rbp), %r8
	movq	-472(%rbp), %r9
	movq	%rax, -432(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, -504(%rbp)
	movq	%rax, -360(%rbp)
	movq	%r8, %rax
	movl	$0, -368(%rbp)
	addq	%r9, %rax
	je	.L44
	testq	%r8, %r8
	je	.L33
.L44:
	movq	%r9, -488(%rbp)
	cmpq	$15, %r9
	ja	.L56
	cmpq	$1, %r9
	jne	.L37
	movzbl	(%r8), %eax
	leaq	-488(%rbp), %r13
	movb	%al, -344(%rbp)
	movq	-504(%rbp), %rax
.L38:
	movq	%r9, -352(%rbp)
	leaq	-432(%rbp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movb	$0, (%rax,%r9)
	movq	-360(%rbp), %rsi
	movq	%r8, %rdi
	movq	%r8, -528(%rbp)
	movl	$8, -368(%rbp)
	call	_ZNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEE7_M_syncEPcmm@PLT
	movq	-528(%rbp), %r8
	movq	%r12, %rdi
	movq	%r8, %rsi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	-480(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L39
	call	_ZdlPv@PLT
.L39:
	leaq	-496(%rbp), %r14
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	_ZNSt6localeC1EPKc@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5imbueERKSt6locale@PLT
	movq	%r13, %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	%r14, %rdi
	call	_ZNSt6localeD1Ev@PLT
	movq	-520(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZNSi10_M_extractIdEERSiRT_@PLT
	movq	-512(%rbp), %rax
	testb	$5, -296(%rbp)
	movsd	-488(%rbp), %xmm0
	movq	-360(%rbp), %rdi
	sete	(%rax)
	leaq	24+_ZTVNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -448(%rbp)
	addq	$40, %rax
	movq	%rax, -328(%rbp)
	leaq	16+_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE(%rip), %rax
	movq	%rax, -432(%rbp)
	cmpq	-504(%rbp), %rdi
	je	.L40
	movsd	%xmm0, -504(%rbp)
	call	_ZdlPv@PLT
	movsd	-504(%rbp), %xmm0
.L40:
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	movq	%r15, %rdi
	movsd	%xmm0, -504(%rbp)
	movq	%rax, -432(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	-24(%rbx), %rax
	movq	16+_ZTTNSt7__cxx1119basic_istringstreamIcSt11char_traitsIcESaIcEEE(%rip), %rcx
	movq	%r12, %rdi
	movq	%rbx, -448(%rbp)
	movq	%rcx, -448(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -328(%rbp)
	movq	$0, -440(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movsd	-504(%rbp), %xmm0
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L57
	addq	$504, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	testq	%r12, %r12
	jne	.L58
	movq	%r14, %rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	testq	%r9, %r9
	jne	.L59
	movq	-504(%rbp), %rax
	leaq	-488(%rbp), %r13
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	-488(%rbp), %r13
	leaq	-480(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -480(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -464(%rbp)
.L30:
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %r12
	movq	-480(%rbp), %rax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	-488(%rbp), %r13
	leaq	-360(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r8, -536(%rbp)
	movq	%r13, %rsi
	movq	%r9, -528(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-528(%rbp), %r9
	movq	-536(%rbp), %r8
	movq	%rax, -360(%rbp)
	movq	%rax, %rdi
	movq	-488(%rbp), %rax
	movq	%rax, -344(%rbp)
.L36:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-488(%rbp), %r9
	movq	-360(%rbp), %rax
	jmp	.L38
.L33:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L57:
	call	__stack_chk_fail@PLT
.L59:
	movq	-504(%rbp), %rdi
	leaq	-488(%rbp), %r13
	jmp	.L36
.L58:
	movq	%r14, %rdi
	jmp	.L30
	.cfi_endproc
.LFE5092:
	.size	_ZN4node9inspector8protocol10StringUtil8toDoubleEPKcmPb, .-_ZN4node9inspector8protocol10StringUtil8toDoubleEPKcmPb
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil13jsonToMessageENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol10StringUtil13jsonToMessageENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol10StringUtil13jsonToMessageENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5094:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movq	(%rsi), %rcx
	leaq	16(%rsi), %rdx
	cmpq	%rdx, %rcx
	je	.L63
	movq	%rcx, (%rdi)
	movq	16(%rsi), %rcx
	movq	%rcx, 16(%rdi)
.L62:
	movq	8(%rsi), %rcx
	movq	%rdx, (%rsi)
	movq	$0, 8(%rsi)
	movq	%rcx, 8(%rax)
	movb	$0, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movdqu	16(%rsi), %xmm0
	movups	%xmm0, 16(%rdi)
	jmp	.L62
	.cfi_endproc
.LFE5094:
	.size	_ZN4node9inspector8protocol10StringUtil13jsonToMessageENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol10StringUtil13jsonToMessageENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil15binaryToMessageB5cxx11ESt6vectorIhSaIhEE
	.type	_ZN4node9inspector8protocol10StringUtil15binaryToMessageB5cxx11ESt6vectorIhSaIhEE, @function
_ZN4node9inspector8protocol10StringUtil15binaryToMessageB5cxx11ESt6vectorIhSaIhEE:
.LFB5095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdi, %r13
	addq	$16, %rdi
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movq	(%rsi), %r14
	movq	8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, 0(%r13)
	testq	%r14, %r14
	jne	.L65
	testq	%r12, %r12
	jne	.L81
.L65:
	subq	%r14, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L82
	cmpq	$1, %r12
	jne	.L68
	movzbl	(%r14), %eax
	movb	%al, 16(%r13)
.L69:
	movq	%r12, 8(%r13)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L83
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L69
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r13)
.L67:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	0(%r13), %rdi
	jmp	.L69
.L81:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L83:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5095:
	.size	_ZN4node9inspector8protocol10StringUtil15binaryToMessageB5cxx11ESt6vectorIhSaIhEE, .-_ZN4node9inspector8protocol10StringUtil15binaryToMessageB5cxx11ESt6vectorIhSaIhEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil8fromUTF8B5cxx11EPKhm
	.type	_ZN4node9inspector8protocol10StringUtil8fromUTF8B5cxx11EPKhm, @function
_ZN4node9inspector8protocol10StringUtil8fromUTF8B5cxx11EPKhm:
.LFB5096:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdi, %r13
	addq	$16, %rdi
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	movq	%rdi, 0(%r13)
	addq	%rdx, %rax
	je	.L85
	testq	%rsi, %rsi
	je	.L101
.L85:
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L102
	cmpq	$1, %r12
	jne	.L88
	movzbl	(%r14), %eax
	movb	%al, 16(%r13)
.L89:
	movq	%r12, 8(%r13)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L89
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%r13, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 0(%r13)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r13)
.L87:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	0(%r13), %rdi
	jmp	.L89
.L101:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L103:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5096:
	.size	_ZN4node9inspector8protocol10StringUtil8fromUTF8B5cxx11EPKhm, .-_ZN4node9inspector8protocol10StringUtil8fromUTF8B5cxx11EPKhm
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5098:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	ret
	.cfi_endproc
.LFE5098:
	.size	_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol10StringUtil14CharactersUTF8ERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i,"axG",@progbits,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC5EPS6_i,comdat
	.align 2
	.p2align 4
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i
	.type	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i, @function
_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i:
.LFB6121:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rcx
	movq	%rsi, %rdi
	movq	%rcx, (%rax)
	movq	%rsi, 8(%rax)
	testl	%edx, %edx
	jle	.L105
	movslq	%edx, %rdx
	leaq	16(%rsi), %rax
	cmpq	%rax, (%rsi)
	je	.L108
	movq	16(%rsi), %rax
.L107:
	movq	8(%rdi), %rsi
	subq	%rsi, %rax
	cmpq	%rax, %rdx
	ja	.L109
.L105:
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	addq	%rdx, %rsi
	jmp	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$15, %eax
	jmp	.L107
	.cfi_endproc
.LFE6121:
	.size	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i, .-_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i
	.weak	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC1EPS6_i
	.set	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC1EPS6_i,_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC2EPS6_i
	.text
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE
	.type	_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE, @function
_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE:
.LFB5087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	movl	$34, %esi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZNSo3putEc@PLT
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.L122
.L111:
	movl	$34, %esi
	movq	%r12, %rdi
	call	_ZNSo3putEc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L123
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
	movq	(%rbx), %rsi
	leaq	-112(%rbp), %r13
	movl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movzwl	-104(%rbp), %eax
	testw	%ax, %ax
	js	.L112
	movswl	%ax, %esi
	sarl	$5, %esi
.L113:
	testb	$17, %al
	jne	.L116
	leaq	-102(%rbp), %rdi
	testb	$2, %al
	cmove	-88(%rbp), %rdi
.L114:
	movq	%r12, %rdx
	call	_ZN4node9inspector8protocol23escapeWideStringForJSONEPKtjPNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L112:
	movl	-100(%rbp), %esi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L116:
	xorl	%edi, %edi
	jmp	.L114
.L123:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5087:
	.size	_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE, .-_ZN4node9inspector8protocol10StringUtil25builderAppendQuotedStringERNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEERKNS3_12basic_stringIcS6_S7_EE
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil9parseJSONERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol10StringUtil9parseJSONERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol10StringUtil9parseJSONERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	jne	.L125
	movq	$0, (%rdi)
.L124:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L134
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movq	(%rsi), %rsi
	leaq	-96(%rbp), %r13
	movl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movzwl	-88(%rbp), %eax
	testw	%ax, %ax
	js	.L127
	movswl	%ax, %edx
	sarl	$5, %edx
.L128:
	testb	$17, %al
	jne	.L131
	leaq	-86(%rbp), %rsi
	testb	$2, %al
	cmove	-72(%rbp), %rsi
.L129:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol19parseJSONCharactersEPKtj@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L127:
	movl	-84(%rbp), %edx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L131:
	xorl	%esi, %esi
	jmp	.L129
.L134:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5088:
	.size	_ZN4node9inspector8protocol10StringUtil9parseJSONERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol10StringUtil9parseJSONERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil9fromUTF16B5cxx11EPKtm
	.type	_ZN4node9inspector8protocol10StringUtil9fromUTF16B5cxx11EPKtm, @function
_ZN4node9inspector8protocol10StringUtil9fromUTF16B5cxx11EPKtm:
.LFB5097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-128(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-144(%rbp), %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	movswl	-120(%rbp), %edx
	movq	%rbx, -160(%rbp)
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	testw	%dx, %dx
	js	.L136
	sarl	$5, %edx
.L137:
	leaq	-176(%rbp), %r15
	leaq	-160(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEC1EPS6_i
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	leaq	16+_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE(%rip), %rax
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN6icu_678ByteSinkD2Ev@PLT
	movq	-160(%rbp), %r8
	leaq	16(%r12), %rdi
	movq	-152(%rbp), %r13
	movq	%rdi, (%r12)
	movq	%r8, %rax
	addq	%r13, %rax
	je	.L138
	testq	%r8, %r8
	je	.L155
.L138:
	movq	%r13, -176(%rbp)
	cmpq	$15, %r13
	ja	.L156
	cmpq	$1, %r13
	jne	.L141
	movzbl	(%r8), %eax
	movb	%al, 16(%r12)
.L142:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L143
	call	_ZdlPv@PLT
.L143:
	movq	%r14, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L157
	addq	$152, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L142
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L136:
	movl	-116(%rbp), %edx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r8, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-184(%rbp), %r8
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-176(%rbp), %rax
	movq	%rax, 16(%r12)
.L140:
	movq	%r13, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-176(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L142
.L155:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L157:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5097:
	.size	_ZN4node9inspector8protocol10StringUtil9fromUTF16B5cxx11EPKtm, .-_ZN4node9inspector8protocol10StringUtil9fromUTF16B5cxx11EPKtm
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5099:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-96(%rbp), %r12
	subq	$80, %rsp
	movq	(%rdi), %rsi
	movl	8(%rdi), %edx
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	xorl	%esi, %esi
	movl	$2147483647, %edx
	movq	%r12, %rdi
	call	_ZNK6icu_6713UnicodeString11countChar32Eii@PLT
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L161
	addq	$80, %rsp
	movslq	%ebx, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L161:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5099:
	.size	_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol10StringUtil14CharacterCountERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil12parseMessageERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb
	.type	_ZN4node9inspector8protocol10StringUtil12parseMessageERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb, @function
_ZN4node9inspector8protocol10StringUtil12parseMessageERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb:
.LFB5093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$80, %rsp
	movq	8(%rsi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testb	%r8b, %r8b
	je	.L163
	movq	(%rsi), %rsi
	call	_ZN4node9inspector8protocol5Value11parseBinaryEPKhm@PLT
.L162:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L174
	addq	$80, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	testq	%rdx, %rdx
	jne	.L165
	movq	$0, (%rdi)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L165:
	movq	(%rsi), %rsi
	leaq	-96(%rbp), %r13
	movl	%edx, %edx
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movzwl	-88(%rbp), %eax
	testw	%ax, %ax
	js	.L167
	movswl	%ax, %edx
	sarl	$5, %edx
.L168:
	testb	$17, %al
	jne	.L171
	leaq	-86(%rbp), %rsi
	testb	$2, %al
	cmove	-72(%rbp), %rsi
.L169:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol19parseJSONCharactersEPKtj@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L167:
	movl	-84(%rbp), %edx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L171:
	xorl	%esi, %esi
	jmp	.L169
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5093:
	.size	_ZN4node9inspector8protocol10StringUtil12parseMessageERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb, .-_ZN4node9inspector8protocol10StringUtil12parseMessageERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEb
	.p2align 4
	.globl	_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE
	.type	_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE, @function
_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE:
.LFB5090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	24(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, -208(%rbp)
	testq	%rbx, %rbx
	je	.L189
	cmpb	$0, 16(%rbp)
	movq	32(%rbp), %r8
	je	.L178
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%r8, %r8
	je	.L190
	movq	%rbx, -192(%rbp)
	cmpq	$15, %rbx
	ja	.L191
	cmpq	$1, %rbx
	jne	.L181
	movzbl	(%r8), %eax
	movb	%al, 16(%rdi)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	(%rbx,%rbx), %r15
	xorl	%edx, %edx
	movq	%r8, -224(%rbp)
	leaq	-128(%rbp), %r13
	leaq	-144(%rbp), %rax
	leaq	-160(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rdi, -200(%rbp)
	movq	%rax, -216(%rbp)
	movq	%rax, -160(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	movq	-224(%rbp), %r8
	movl	%ebx, %edx
	movq	%r13, %rdi
	leaq	-192(%rbp), %rbx
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeStringC1EPKDsi@PLT
	.p2align 4,,10
	.p2align 3
.L183:
	movq	-160(%rbp), %rsi
	movl	%r15d, %edx
	movq	%rbx, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkC1EPci@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNK6icu_6713UnicodeString6toUTF8ERNS_8ByteSinkE@PLT
	movslq	-168(%rbp), %r15
	movq	-200(%rbp), %rdi
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6resizeEmc@PLT
	movzbl	-164(%rbp), %r14d
	movq	%rbx, %rdi
	call	_ZN6icu_6720CheckedArrayByteSinkD1Ev@PLT
	testb	%r14b, %r14b
	jne	.L183
	movq	-208(%rbp), %rax
	movq	%rax, (%r12)
	movq	-160(%rbp), %rax
	cmpq	-216(%rbp), %rax
	je	.L192
	movq	%rax, (%r12)
	movq	-144(%rbp), %rax
	movq	%rax, 16(%r12)
.L185:
	movq	-152(%rbp), %rax
	movq	-216(%rbp), %rbx
	movq	%r13, %rdi
	movq	$0, -152(%rbp)
	movb	$0, -144(%rbp)
	movq	%rax, 8(%r12)
	movq	%rbx, -160(%rbp)
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-160(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L175
	call	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L193
	addq	$184, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	.cfi_restore_state
	leaq	-192(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -200(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-200(%rbp), %r8
	movq	%rax, -208(%rbp)
	movq	%rax, (%r12)
	movq	-192(%rbp), %rax
	movq	%rax, 16(%r12)
.L181:
	movq	-208(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
.L182:
	movq	-192(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L189:
	movq	%rax, (%rdi)
	movq	$0, 8(%rdi)
	movb	$0, 16(%rdi)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L192:
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L185
.L193:
	call	__stack_chk_fail@PLT
.L190:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE5090:
	.size	_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE, .-_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE
	.weak	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE
	.section	.data.rel.ro._ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,"awG",@progbits,_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE,comdat
	.align 8
	.type	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, @object
	.size	_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE, 56
_ZTVN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEEE:
	.quad	0
	.quad	0
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED1Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEED0Ev
	.quad	_ZN6icu_6714StringByteSinkINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE6AppendEPKci
	.quad	_ZN6icu_678ByteSink15GetAppendBufferEiiPciPi
	.quad	_ZN6icu_678ByteSink5FlushEv
	.globl	_ZN4node9inspector8protocol10StringUtil9kNotFoundE
	.data
	.align 8
	.type	_ZN4node9inspector8protocol10StringUtil9kNotFoundE, @object
	.size	_ZN4node9inspector8protocol10StringUtil9kNotFoundE, 8
_ZN4node9inspector8protocol10StringUtil9kNotFoundE:
	.quad	-1
	.section	.data.rel.ro,"aw"
	.align 8
.LC2:
	.quad	_ZTVNSt7__cxx1119basic_ostringstreamIcSt11char_traitsIcESaIcEEE+24
	.align 8
.LC3:
	.quad	_ZTVSt15basic_streambufIcSt11char_traitsIcEE+16
	.align 8
.LC4:
	.quad	_ZTVNSt7__cxx1115basic_stringbufIcSt11char_traitsIcESaIcEEE+16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
