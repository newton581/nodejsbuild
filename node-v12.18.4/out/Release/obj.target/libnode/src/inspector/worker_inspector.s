	.file	"worker_inspector.cc"
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD2Ev:
.LFB6797:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6797:
	.size	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD2Ev, .-_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD1Ev,_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD0Ev:
.LFB6799:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6799:
	.size	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD0Ev, .-_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD2Ev:
.LFB6801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120WorkerStartedRequestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	88(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L6
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L7
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L15
	.p2align 4,,10
	.p2align 3
.L6:
	movq	48(%rbx), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L12
	call	_ZdlPv@PLT
.L12:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L4
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L6
.L15:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L10
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L11:
	cmpl	$1, %eax
	jne	.L6
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L11
	.cfi_endproc
.LFE6801:
	.size	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD2Ev, .-_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD1Ev,_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD0Ev:
.LFB6803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120WorkerStartedRequestE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	88(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L18
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L19
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L27
	.p2align 4,,10
	.p2align 3
.L18:
	movq	48(%r12), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L25
	call	_ZdlPv@PLT
.L25:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L18
.L27:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L22
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L23:
	cmpl	$1, %eax
	jne	.L18
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L23
	.cfi_endproc
.LFE6803:
	.size	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD0Ev, .-_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.text
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB6979:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L29
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L29:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L31
.L50:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L50
.L31:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L33
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L34:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L35
	testb	$4, %al
	jne	.L52
	testl	%eax, %eax
	je	.L36
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L53
.L36:
	movq	(%r12), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L36
	andl	$-8, %eax
	xorl	%edx, %edx
.L39:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L39
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L52:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L36
.L53:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L36
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6979:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"basic_string::_M_construct null not valid"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector21ParentInspectorHandleC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb
	.type	_ZN4node9inspector21ParentInspectorHandleC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb, @function
_ZN4node9inspector21ParentInspectorHandleC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb:
.LFB4969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$24, %rdi
	subq	$24, %rsp
	movq	8(%rdx), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	%esi, -24(%rdi)
	movq	%rdi, 8(%rbx)
	movq	(%rdx), %r14
	movq	%r14, %rax
	addq	%r12, %rax
	je	.L55
	testq	%r14, %r14
	je	.L76
.L55:
	movq	%r12, -64(%rbp)
	cmpq	$15, %r12
	ja	.L77
	cmpq	$1, %r12
	jne	.L58
	movzbl	(%r14), %eax
	movb	%al, 24(%rbx)
.L59:
	movq	%r12, 16(%rbx)
	movb	$0, (%rdi,%r12)
	movdqu	0(%r13), %xmm0
	movq	8(%r13), %rax
	movups	%xmm0, 40(%rbx)
	testq	%rax, %rax
	je	.L60
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L61
	lock addl	$1, 8(%rax)
.L60:
	movb	%r15b, 56(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L78
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L59
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	8(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%rbx)
.L57:
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r12
	movq	8(%rbx), %rdi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L61:
	addl	$1, 8(%rax)
	jmp	.L60
.L76:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L78:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4969:
	.size	_ZN4node9inspector21ParentInspectorHandleC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb, .-_ZN4node9inspector21ParentInspectorHandleC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb
	.globl	_ZN4node9inspector21ParentInspectorHandleC1EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb
	.set	_ZN4node9inspector21ParentInspectorHandleC1EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb,_ZN4node9inspector21ParentInspectorHandleC2EiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10shared_ptrINS0_16MainThreadHandleEEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector21ParentInspectorHandleD2Ev
	.type	_ZN4node9inspector21ParentInspectorHandleD2Ev, @function
_ZN4node9inspector21ParentInspectorHandleD2Ev:
.LFB4974:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	40(%rdi), %r12
	movl	$16, %edi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movl	(%rbx), %edx
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestE(%rip), %rcx
	movq	%rax, -48(%rbp)
	movq	%rcx, (%rax)
	movl	%edx, 8(%rax)
	call	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE@PLT
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	(%rdi), %rax
	call	*16(%rax)
.L80:
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L82
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L83
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L94
	.p2align 4,,10
	.p2align 3
.L82:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L79
	call	_ZdlPv@PLT
.L79:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L95
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L82
.L94:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L86
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L87:
	cmpl	$1, %eax
	jne	.L82
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L86:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L87
.L95:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4974:
	.size	_ZN4node9inspector21ParentInspectorHandleD2Ev, .-_ZN4node9inspector21ParentInspectorHandleD2Ev
	.globl	_ZN4node9inspector21ParentInspectorHandleD1Ev
	.set	_ZN4node9inspector21ParentInspectorHandleD1Ev,_ZN4node9inspector21ParentInspectorHandleD2Ev
	.section	.rodata.str1.1
.LC2:
	.string	"Worker "
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector21ParentInspectorHandle13WorkerStartedESt10shared_ptrINS0_16MainThreadHandleEEb
	.type	_ZN4node9inspector21ParentInspectorHandle13WorkerStartedESt10shared_ptrINS0_16MainThreadHandleEEb, @function
_ZN4node9inspector21ParentInspectorHandle13WorkerStartedESt10shared_ptrINS0_16MainThreadHandleEEb:
.LFB4976:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movl	%edx, -156(%rbp)
	movq	8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rsi), %rax
	movq	%rax, -152(%rbp)
	testq	%r12, %r12
	je	.L97
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	leaq	8(%r12), %r14
	testq	%rdx, %rdx
	je	.L98
	lock addl	$1, (%r14)
.L99:
	movl	$104, %edi
	movq	%rdx, -168(%rbp)
	call	_Znwm@PLT
	movl	(%rbx), %r8d
	movq	-168(%rbp), %rdx
	movq	%rax, %r13
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120WorkerStartedRequestE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	16(%r13), %r15
	movl	%r8d, 8(%r13)
	testq	%rdx, %rdx
	je	.L163
	lock addl	$1, (%r14)
.L100:
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-96(%rbp), %r14
	movl	$16, %edx
	xorl	%eax, %eax
	leaq	.LC0(%rip), %rcx
	movq	%r14, %rdi
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	leaq	.LC2(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$7, %r8d
	movq	%r14, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	-112(%rbp), %rcx
	movq	%rcx, -168(%rbp)
	leaq	16(%rax), %rdx
	movq	%rcx, -128(%rbp)
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	je	.L164
	movq	%rcx, -128(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -112(%rbp)
.L102:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%rcx, -120(%rbp)
	movq	%rdx, (%rax)
	movq	-96(%rbp), %rdi
	movq	$0, 8(%rax)
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L103
	call	_ZdlPv@PLT
.L103:
	movq	-128(%rbp), %r10
	movq	-120(%rbp), %r8
	leaq	32(%r13), %rdi
	movq	%rdi, 16(%r13)
	movq	%r10, %rax
	addq	%r8, %rax
	je	.L104
	testq	%r10, %r10
	je	.L109
.L104:
	movq	%r8, -136(%rbp)
	cmpq	$15, %r8
	ja	.L165
	cmpq	$1, %r8
	jne	.L107
	movzbl	(%r10), %eax
	movb	%al, 32(%r13)
.L108:
	movq	%r8, 24(%r13)
	movb	$0, (%rdi,%r8)
	movq	8(%rbx), %r8
	leaq	64(%r13), %rdi
	movq	16(%rbx), %r15
	movq	%rdi, 48(%r13)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L142
	testq	%r8, %r8
	je	.L109
.L142:
	movq	%r15, -136(%rbp)
	cmpq	$15, %r15
	ja	.L166
	cmpq	$1, %r15
	jne	.L113
	movzbl	(%r8), %eax
	leaq	-136(%rbp), %r14
	movb	%al, 64(%r13)
.L114:
	movq	-152(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r15, 56(%r13)
	movb	$0, (%rdi,%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 80(%r13)
	testq	%r12, %r12
	je	.L115
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	leaq	8(%r12), %r15
	testq	%rdx, %rdx
	je	.L116
	lock addl	$1, (%r15)
.L117:
	movq	-128(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L133
	movq	%rdx, -152(%rbp)
	call	_ZdlPv@PLT
	movq	-152(%rbp), %rdx
.L133:
	testq	%rdx, %rdx
	je	.L119
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
	cmpl	$1, %eax
	je	.L167
.L124:
	movzbl	-156(%rbp), %eax
	movb	%al, 96(%r13)
	testq	%rdx, %rdx
	je	.L168
	movl	$-1, %eax
	lock xaddl	%eax, (%r15)
	cmpl	$1, %eax
	je	.L169
	.p2align 4,,10
	.p2align 3
.L118:
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	movq	%r13, -136(%rbp)
	call	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE@PLT
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L96
	movq	(%rdi), %rax
	call	*16(%rax)
.L96:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L170
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	addl	$1, 8(%r12)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L107:
	testq	%r8, %r8
	je	.L108
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L113:
	leaq	-136(%rbp), %r14
	testq	%r15, %r15
	je	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	-136(%rbp), %r14
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%r8, -184(%rbp)
	movq	%r14, %rsi
	movq	%r10, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r8
	movq	%rax, 16(%r13)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, 32(%r13)
.L106:
	movq	%r8, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r8
	movq	16(%r13), %rdi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L166:
	leaq	-136(%rbp), %r14
	leaq	48(%r13), %rdi
	xorl	%edx, %edx
	movq	%r8, -176(%rbp)
	movq	%r14, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %r8
	movq	%rax, 48(%r13)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, 64(%r13)
.L112:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %r15
	movq	48(%r13), %rdi
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L163:
	addl	$1, 8(%r12)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L116:
	addl	$1, 8(%r12)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$104, %edi
	call	_Znwm@PLT
	movl	(%rbx), %r8d
	movq	%rax, %r13
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120WorkerStartedRequestE(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	16(%r13), %r15
	movl	%r8d, 8(%r13)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L164:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -112(%rbp)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-128(%rbp), %rdi
	cmpq	-168(%rbp), %rdi
	je	.L136
	call	_ZdlPv@PLT
.L136:
	movzbl	-156(%rbp), %eax
	movb	%al, 96(%r13)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L168:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %eax
	jne	.L118
.L169:
	movq	(%r12), %rax
	movq	%rdx, -152(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-152(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L127
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L128:
	cmpl	$1, %eax
	jne	.L118
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L119:
	movl	8(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %eax
	jne	.L124
.L167:
	movq	(%r12), %rax
	movq	%rdx, -152(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	movq	-152(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L122
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L123:
	cmpl	$1, %eax
	jne	.L124
	movq	(%r12), %rax
	movq	%rdx, -152(%rbp)
	movq	%r12, %rdi
	call	*24(%rax)
	movq	-152(%rbp), %rdx
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L127:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L122:
	movl	12(%r12), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 12(%r12)
	jmp	.L123
.L170:
	call	__stack_chk_fail@PLT
.L109:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
.LFE4976:
	.size	_ZN4node9inspector21ParentInspectorHandle13WorkerStartedESt10shared_ptrINS0_16MainThreadHandleEEb, .-_ZN4node9inspector21ParentInspectorHandle13WorkerStartedESt10shared_ptrINS0_16MainThreadHandleEEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector21ParentInspectorHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.type	_ZN4node9inspector21ParentInspectorHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, @function
_ZN4node9inspector21ParentInspectorHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb:
.LFB4982:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movzbl	%cl, %ecx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	40(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdx), %rax
	movq	$0, (%rdx)
	leaq	-32(%rbp), %rdx
	movq	%rax, -32(%rbp)
	call	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L171
	movq	(%rdi), %rax
	call	*8(%rax)
.L171:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L178
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L178:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4982:
	.size	_ZN4node9inspector21ParentInspectorHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, .-_ZN4node9inspector21ParentInspectorHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector13WorkerManager15NewParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector13WorkerManager15NewParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector13WorkerManager15NewParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4986:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rax
	cmpq	$0, 168(%rsi)
	setne	%r15b
	movq	%rax, -80(%rbp)
	testq	%r12, %r12
	je	.L180
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L181
	lock addl	$1, 8(%r12)
.L180:
	movl	$64, %edi
	movl	%edx, -72(%rbp)
	call	_Znwm@PLT
	movl	-72(%rbp), %edx
	movq	(%r14), %r9
	leaq	24(%rax), %rdi
	movq	8(%r14), %r8
	movq	%rax, %rbx
	movl	%edx, (%rax)
	movq	%rdi, 8(%rax)
	movq	%r9, %rax
	addq	%r8, %rax
	je	.L182
	testq	%r9, %r9
	je	.L213
.L182:
	movq	%r8, -64(%rbp)
	cmpq	$15, %r8
	ja	.L214
	cmpq	$1, %r8
	jne	.L185
	movzbl	(%r9), %eax
	movb	%al, 24(%rbx)
.L186:
	movq	-80(%rbp), %xmm0
	movq	%r12, %xmm1
	movq	%r8, 16(%rbx)
	movb	$0, (%rdi,%r8)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%rbx)
	testq	%r12, %r12
	je	.L187
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	8(%r12), %rax
	testq	%r14, %r14
	je	.L188
	lock addl	$1, (%rax)
	movb	%r15b, 56(%rbx)
	movq	%rbx, 0(%r13)
	testq	%r14, %r14
	je	.L215
.L196:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L216
.L179:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L217
	addq	$56, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	.cfi_restore_state
	testq	%r8, %r8
	je	.L186
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L181:
	addl	$1, 8(%r12)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L214:
	leaq	8(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -88(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-88(%rbp), %r9
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%rbx)
.L184:
	movq	%r8, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r8
	movq	8(%rbx), %rdi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L188:
	addl	$1, 8(%r12)
	movb	%r15b, 56(%rbx)
	movq	%rbx, 0(%r13)
	testq	%r14, %r14
	jne	.L196
.L215:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L179
.L216:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L193
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L194:
	cmpl	$1, %eax
	jne	.L179
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L187:
	movb	%r15b, 56(%rbx)
	movq	%rbx, 0(%r13)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L193:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L194
.L213:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L217:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4986:
	.size	_ZN4node9inspector13WorkerManager15NewParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector13WorkerManager15NewParentHandleEiRKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.type	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, @function
_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_:
.LFB6151:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r12
	movq	%rax, %r8
	divq	%rdi
	leaq	(%r12,%rdx,8), %r13
	movq	0(%r13), %r11
	testq	%r11, %r11
	je	.L238
	movq	(%r11), %r14
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r14), %esi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L245:
	testq	%rcx, %rcx
	je	.L238
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L238
	movq	%rcx, %r14
.L221:
	movq	(%r14), %rcx
	cmpl	%esi, %r8d
	jne	.L245
	cmpq	%r9, %r11
	je	.L246
	testq	%rcx, %rcx
	je	.L223
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L223
	movq	%r9, (%r12,%rdx,8)
	movq	(%r14), %rcx
.L223:
	movq	88(%r14), %r12
	movq	%rcx, (%r9)
	testq	%r12, %r12
	je	.L226
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L227
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L228:
	cmpl	$1, %eax
	jne	.L226
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L230
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L231:
	cmpl	$1, %eax
	je	.L247
	.p2align 4,,10
	.p2align 3
.L226:
	movq	48(%r14), %rdi
	leaq	64(%r14), %rax
	cmpq	%rax, %rdi
	je	.L232
	call	_ZdlPv@PLT
.L232:
	movq	16(%r14), %rdi
	leaq	32(%r14), %rax
	cmpq	%rax, %rdi
	je	.L233
	call	_ZdlPv@PLT
.L233:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L239
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L223
	movq	%r9, (%r12,%rdx,8)
	movq	0(%r13), %rax
.L222:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L248
.L224:
	movq	$0, 0(%r13)
	movq	(%r14), %rcx
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L227:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L228
.L239:
	movq	%r9, %rax
	jmp	.L222
.L230:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L231
.L247:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L226
.L248:
	movq	%rcx, 16(%rbx)
	jmp	.L224
	.cfi_endproc
.LFE6151:
	.size	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_, .-_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector13WorkerManager14WorkerFinishedEi
	.type	_ZN4node9inspector13WorkerManager14WorkerFinishedEi, @function
_ZN4node9inspector13WorkerManager14WorkerFinishedEi:
.LFB4984:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%esi, -4(%rbp)
	leaq	-4(%rbp), %rsi
	call	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4984:
	.size	_ZN4node9inspector13WorkerManager14WorkerFinishedEi, .-_ZN4node9inspector13WorkerManager14WorkerFinishedEi
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequest4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequest4CallEPNS0_19MainThreadInterfaceE:
.LFB4967:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	leaq	-48(%rbp), %rdi
	subq	$48, %rsp
	.cfi_offset 3, -32
	movq	272(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector5Agent16GetWorkerManagerEv@PLT
	movl	8(%r12), %eax
	leaq	-52(%rbp), %rsi
	movl	%eax, -52(%rbp)
	movq	-48(%rbp), %rax
	leaq	32(%rax), %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERS1_
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L251
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L254
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L261
	.p2align 4,,10
	.p2align 3
.L251:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L262
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L251
.L261:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L257
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L258:
	cmpl	$1, %eax
	jne	.L251
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L257:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L258
.L262:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4967:
	.size	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequest4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequest4CallEPNS0_19MainThreadInterfaceE
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi,"axG",@progbits,_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi:
.LFB6161:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movslq	(%rsi), %rax
	movq	8(%rdi), %rdi
	movq	(%rbx), %r12
	movq	%rax, %r8
	divq	%rdi
	leaq	(%r12,%rdx,8), %r13
	movq	0(%r13), %r11
	testq	%r11, %r11
	je	.L274
	movq	(%r11), %r14
	movq	%rdx, %r10
	movq	%r11, %r9
	movl	8(%r14), %esi
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L281:
	testq	%rcx, %rcx
	je	.L274
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%r14, %r9
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%r10, %rdx
	jne	.L274
	movq	%rcx, %r14
.L266:
	movq	(%r14), %rcx
	cmpl	%esi, %r8d
	jne	.L281
	cmpq	%r9, %r11
	je	.L282
	testq	%rcx, %rcx
	je	.L268
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L268
	movq	%r9, (%r12,%rdx,8)
	movq	(%r14), %rcx
.L268:
	movq	%rcx, (%r9)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	subq	$1, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	testq	%rcx, %rcx
	je	.L275
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	divq	%rdi
	cmpq	%rdx, %r10
	je	.L268
	movq	%r9, (%r12,%rdx,8)
	movq	0(%r13), %rax
.L267:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L283
.L269:
	movq	$0, 0(%r13)
	movq	(%r14), %rcx
	jmp	.L268
.L275:
	movq	%r9, %rax
	jmp	.L267
.L283:
	movq	%rcx, 16(%rbx)
	jmp	.L269
	.cfi_endproc
.LFE6161:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector13WorkerManager20RemoveAttachDelegateEi
	.type	_ZN4node9inspector13WorkerManager20RemoveAttachDelegateEi, @function
_ZN4node9inspector13WorkerManager20RemoveAttachDelegateEi:
.LFB4987:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	96(%rdi), %rcx
	movq	88(%rdi), %r13
	movl	%esi, -36(%rbp)
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L285
	movq	(%rdi), %r12
	movq	%rdx, %r11
	movq	%rdi, %r10
	movl	8(%r12), %r9d
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L310:
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L285
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%r12, %r10
	movq	%rax, %r9
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L285
	movq	%r8, %r12
.L287:
	cmpl	%r9d, %esi
	jne	.L310
	movq	(%r12), %rsi
	cmpq	%r10, %rdi
	je	.L311
	testq	%rsi, %rsi
	je	.L289
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L289
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r12), %rsi
.L289:
	movq	16(%r12), %rdi
	movq	%rsi, (%r10)
	testq	%rdi, %rdi
	je	.L291
	movq	(%rdi), %rax
	call	*16(%rax)
.L291:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 112(%rbx)
.L285:
	leaq	-36(%rbp), %rsi
	leaq	144(%rbx), %rdi
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L311:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L294
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L289
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L288:
	leaq	104(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L312
.L290:
	movq	$0, (%r14)
	movq	(%r12), %rsi
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r10, %rax
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L312:
	movq	%rsi, 104(%rbx)
	jmp	.L290
	.cfi_endproc
.LFE4987:
	.size	_ZN4node9inspector13WorkerManager20RemoveAttachDelegateEi, .-_ZN4node9inspector13WorkerManager20RemoveAttachDelegateEi
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector24WorkerManagerEventHandleD2Ev
	.type	_ZN4node9inspector24WorkerManagerEventHandleD2Ev, @function
_ZN4node9inspector24WorkerManagerEventHandleD2Ev:
.LFB4995:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movl	16(%rdi), %esi
	movq	(%rdi), %rdi
	call	_ZN4node9inspector13WorkerManager20RemoveAttachDelegateEi
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L313
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L316
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L322
.L313:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L313
.L322:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L319
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L320:
	cmpl	$1, %eax
	jne	.L313
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L319:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L320
	.cfi_endproc
.LFE4995:
	.size	_ZN4node9inspector24WorkerManagerEventHandleD2Ev, .-_ZN4node9inspector24WorkerManagerEventHandleD2Ev
	.globl	_ZN4node9inspector24WorkerManagerEventHandleD1Ev
	.set	_ZN4node9inspector24WorkerManagerEventHandleD1Ev,_ZN4node9inspector24WorkerManagerEventHandleD2Ev
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB6194:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L324
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L334
.L350:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L335:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L348
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L349
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L327:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L329
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L331:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L332:
	testq	%rsi, %rsi
	je	.L329
.L330:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L331
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L337
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L330
	.p2align 4,,10
	.p2align 3
.L329:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L333
	call	_ZdlPv@PLT
.L333:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L350
.L334:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L336
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L336:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L337:
	movq	%rdx, %rdi
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L327
.L349:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6194:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.section	.text._ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,"axG",@progbits,_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.type	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, @function
_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_:
.LFB5898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movslq	(%rsi), %r14
	movq	%rsi, %rbx
	movq	8(%rdi), %rdi
	movq	%r14, %rax
	divq	%rdi
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r13
	testq	%rax, %rax
	je	.L352
	movq	(%rax), %rcx
	movq	%r14, %r8
	movl	8(%rcx), %esi
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L352
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%rdi
	cmpq	%rdx, %r13
	jne	.L352
.L354:
	cmpl	%esi, %r8d
	jne	.L364
	popq	%rbx
	leaq	16(%rcx), %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L352:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	(%rbx), %eax
	movl	$1, %r8d
	movq	$0, 16(%rcx)
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	popq	%rbx
	popq	%r12
	addq	$16, %rax
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5898:
	.size	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_, .-_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE
	.type	_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE, @function
_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE:
.LFB4990:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-84(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	88(%rsi), %r13
	pushq	%r12
	movq	%r13, %rdi
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	200(%rsi), %eax
	addl	$1, %eax
	movl	%eax, 200(%rsi)
	movq	%r14, %rsi
	movl	%eax, -84(%rbp)
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	(%rbx), %rdx
	movq	$0, (%rbx)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L366
	movq	(%rdi), %rax
	call	*16(%rax)
.L366:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZNSt8__detail9_Map_baseIiSt4pairIKiSt10unique_ptrIN4node9inspector14WorkerDelegateESt14default_deleteIS6_EEESaISA_ENS_10_Select1stESt8equal_toIiESt4hashIiENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb0ELb0ELb1EEELb1EEixERS2_
	movq	48(%r12), %rbx
	movq	%rax, %r13
	testq	%rbx, %rbx
	je	.L367
	leaq	-80(%rbp), %r14
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L412:
	lock addl	$1, 8(%rax)
.L369:
	leaq	16(%rbx), %rsi
	leaq	48(%rbx), %rdx
	movq	%r14, %r8
	xorl	%ecx, %ecx
	call	*%r9
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L368
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	testq	%rdx, %rdx
	je	.L373
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
	cmpl	$1, %eax
	je	.L411
	.p2align 4,,10
	.p2align 3
.L368:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L367
.L378:
	movq	80(%rbx), %rax
	testq	%rax, %rax
	je	.L368
	movq	0(%r13), %rdi
	movq	(%rdi), %rdx
	movq	(%rdx), %r9
	movq	%rax, -80(%rbp)
	movq	88(%rbx), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L369
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L412
	addl	$1, 8(%rax)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L373:
	movl	8(%rdi), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%rdi)
	cmpl	$1, %eax
	jne	.L368
.L411:
	movq	(%rdi), %rax
	movq	%rdx, -112(%rbp)
	movq	%rdi, -104(%rbp)
	call	*16(%rax)
	movq	-112(%rbp), %rdx
	movq	-104(%rbp), %rdi
	testq	%rdx, %rdx
	je	.L376
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L377:
	cmpl	$1, %eax
	jne	.L368
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L378
	.p2align 4,,10
	.p2align 3
.L367:
	movq	8(%r12), %r13
	testq	%r13, %r13
	je	.L379
	movl	8(%r13), %eax
	leaq	8(%r13), %r14
.L381:
	testl	%eax, %eax
	je	.L379
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%r14)
	jne	.L381
	movq	(%r12), %rbx
	movl	$24, %edi
	movl	-84(%rbp), %r12d
	call	_Znwm@PLT
	movq	%r13, %xmm1
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rdx
	movq	%rbx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	testq	%rdx, %rdx
	je	.L382
	lock addl	$1, (%r14)
	movl	%r12d, 16(%rax)
	movq	%rax, (%r15)
	testq	%rdx, %rdx
	je	.L384
.L415:
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
	cmpl	$1, %eax
	je	.L413
.L365:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L414
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L376:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L382:
	addl	$1, 8(%r13)
	movl	%r12d, 16(%rax)
	movq	%rax, (%r15)
	testq	%rdx, %rdx
	jne	.L415
.L384:
	movl	8(%r13), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 8(%r13)
	cmpl	$1, %eax
	jne	.L365
.L413:
	movq	0(%r13), %rax
	movq	%rdx, -104(%rbp)
	movq	%r13, %rdi
	call	*16(%rax)
	movq	-104(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L388
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L389:
	cmpl	$1, %eax
	jne	.L365
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L365
.L388:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L389
.L414:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE.cold, @function
_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE.cold:
.LFSB4990:
.L379:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE4990:
	.text
	.size	_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE, .-_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE
	.section	.text.unlikely
	.size	_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE.cold, .-_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE.cold
.LCOLDE3:
	.text
.LHOTE3:
	.section	.text._ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm:
.LFB6400:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L417
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L427
.L443:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L428:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L441
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L442
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L420:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L422
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L424:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L425:
	testq	%rsi, %rsi
	je	.L422
.L423:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L424
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L430
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L423
	.p2align 4,,10
	.p2align 3
.L422:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L426
	call	_ZdlPv@PLT
.L426:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L443
.L427:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L429
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L429:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L430:
	movq	%rdx, %rdi
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L420
.L442:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6400:
	.size	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	.section	.text._ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.type	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, @function
_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_:
.LFB6152:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$96, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_Znwm@PLT
	movq	(%rbx), %r8
	movq	8(%rbx), %r15
	movq	%rax, %r12
	movq	$0, (%rax)
	movl	(%r14), %eax
	leaq	32(%r12), %r14
	movl	%eax, 8(%r12)
	movq	%r8, %rax
	addq	%r15, %rax
	movq	%r14, 16(%r12)
	je	.L445
	testq	%r8, %r8
	je	.L450
.L445:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L495
	cmpq	$1, %r15
	jne	.L448
	movzbl	(%r8), %eax
	movb	%al, 32(%r12)
	movq	%r14, %rax
.L449:
	movq	%r15, 24(%r12)
	leaq	64(%r12), %r8
	movb	$0, (%rax,%r15)
	movq	32(%rbx), %r9
	movq	40(%rbx), %r15
	movq	%r8, 48(%r12)
	movq	%r9, %rax
	addq	%r15, %rax
	je	.L474
	testq	%r9, %r9
	je	.L450
.L474:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L496
	cmpq	$1, %r15
	jne	.L454
	movzbl	(%r9), %eax
	movb	%al, 64(%r12)
	movq	%r8, %rax
.L455:
	movq	%r15, 56(%r12)
	movb	$0, (%rax,%r15)
	movdqu	64(%rbx), %xmm0
	movq	72(%rbx), %rax
	movups	%xmm0, 80(%r12)
	testq	%rax, %rax
	je	.L456
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L457
	lock addl	$1, 8(%rax)
.L456:
	movslq	8(%r12), %r9
	movq	8(%r13), %rcx
	xorl	%edx, %edx
	movq	%r9, %rax
	movq	%r9, %r10
	divq	%rcx
	movq	0(%r13), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %rsi
	testq	%rax, %rax
	je	.L458
	movq	(%rax), %rbx
	movl	8(%rbx), %edi
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L497:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L458
	movslq	8(%rbx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %rsi
	jne	.L458
.L460:
	cmpl	%edi, %r10d
	jne	.L497
	movq	88(%r12), %r15
	testq	%r15, %r15
	je	.L461
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L462
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L498
	.p2align 4,,10
	.p2align 3
.L461:
	movq	48(%r12), %rdi
	cmpq	%rdi, %r8
	je	.L467
	call	_ZdlPv@PLT
.L467:
	movq	16(%r12), %rdi
	cmpq	%rdi, %r14
	je	.L468
	call	_ZdlPv@PLT
.L468:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	%rbx, %rax
	xorl	%edx, %edx
.L469:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L499
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L448:
	.cfi_restore_state
	testq	%r15, %r15
	jne	.L500
	movq	%r14, %rax
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L454:
	testq	%r15, %r15
	jne	.L501
	movq	%r8, %rax
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	16(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 16(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 32(%r12)
.L447:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	16(%r12), %rax
	jmp	.L449
	.p2align 4,,10
	.p2align 3
.L496:
	leaq	48(%r12), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -80(%rbp)
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	-80(%rbp), %r9
	movq	%rax, 48(%r12)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 64(%r12)
.L453:
	movq	%r15, %rdx
	movq	%r9, %rsi
	movq	%r8, -72(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	48(%r12), %rax
	movq	-72(%rbp), %r8
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L457:
	addl	$1, 8(%rax)
	jmp	.L456
	.p2align 4,,10
	.p2align 3
.L458:
	movq	%r9, %rdx
	movl	$1, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS7_10_Hash_nodeIS5_Lb0EEEm
	movl	$1, %edx
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L462:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L461
.L498:
	movq	(%r15), %rax
	movq	%r8, -72(%rbp)
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	movq	-72(%rbp), %r8
	je	.L465
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L466:
	cmpl	$1, %eax
	jne	.L461
	movq	(%r15), %rax
	movq	%r8, -72(%rbp)
	movq	%r15, %rdi
	call	*24(%rax)
	movq	-72(%rbp), %r8
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L465:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L466
.L450:
	leaq	.LC1(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L499:
	call	__stack_chk_fail@PLT
.L501:
	movq	%r8, %rdi
	jmp	.L453
.L500:
	movq	%r14, %rdi
	jmp	.L447
	.cfi_endproc
.LFE6152:
	.size	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_, .-_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector13WorkerManager13WorkerStartedEiRKNS0_10WorkerInfoEb
	.type	_ZN4node9inspector13WorkerManager13WorkerStartedEiRKNS0_10WorkerInfoEb, @function
_ZN4node9inspector13WorkerManager13WorkerStartedEiRKNS0_10WorkerInfoEb:
.LFB4985:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%ecx, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	%esi, -84(%rbp)
	movq	64(%rdx), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector16MainThreadHandle7ExpiredEv@PLT
	testb	%al, %al
	je	.L528
.L502:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L529
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L528:
	.cfi_restore_state
	leaq	32(%rbx), %rdi
	leaq	-84(%rbp), %rsi
	movq	%r12, %rdx
	call	_ZNSt10_HashtableIiSt4pairIKiN4node9inspector10WorkerInfoEESaIS5_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENS7_18_Mod_range_hashingENS7_20_Default_ranged_hashENS7_20_Prime_rehash_policyENS7_17_Hashtable_traitsILb0ELb0ELb1EEEE10_M_emplaceIJRiRKS4_EEES0_INS7_14_Node_iteratorIS5_Lb0ELb0EEEbESt17integral_constantIbLb1EEDpOT_
	movq	104(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L502
	movzbl	%r13b, %r13d
	leaq	-80(%rbp), %r14
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L531:
	lock addl	$1, 8(%rax)
.L506:
	leaq	32(%r12), %rdx
	movq	%r14, %r8
	movl	%r13d, %ecx
	movq	%r12, %rsi
	call	*%r9
	movq	-72(%rbp), %r15
	testq	%r15, %r15
	je	.L505
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L510
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L530
	.p2align 4,,10
	.p2align 3
.L505:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L502
.L515:
	movq	64(%r12), %rax
	testq	%rax, %rax
	je	.L505
	movq	16(%rbx), %rdi
	movq	(%rdi), %rdx
	movq	(%rdx), %r9
	movq	%rax, -80(%rbp)
	movq	72(%r12), %rax
	movq	%rax, -72(%rbp)
	testq	%rax, %rax
	je	.L506
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L531
	addl	$1, 8(%rax)
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L510:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L505
.L530:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L513
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L514:
	cmpl	$1, %eax
	jne	.L505
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L513:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L514
.L529:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4985:
	.size	_ZN4node9inspector13WorkerManager13WorkerStartedEiRKNS0_10WorkerInfoEb, .-_ZN4node9inspector13WorkerManager13WorkerStartedEiRKNS0_10WorkerInfoEb
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequest4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequest4CallEPNS0_19MainThreadInterfaceE:
.LFB4961:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	leaq	-48(%rbp), %rdi
	subq	$32, %rsp
	movq	272(%rsi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector5Agent16GetWorkerManagerEv@PLT
	movzbl	96(%rbx), %ecx
	movl	8(%rbx), %esi
	leaq	16(%rbx), %rdx
	movq	-48(%rbp), %rdi
	call	_ZN4node9inspector13WorkerManager13WorkerStartedEiRKNS0_10WorkerInfoEb
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L532
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L535
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L542
	.p2align 4,,10
	.p2align 3
.L532:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L543
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L532
.L542:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L538
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L539:
	cmpl	$1, %eax
	jne	.L532
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L538:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L539
.L543:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4961:
	.size	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequest4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequest4CallEPNS0_19MainThreadInterfaceE
	.section	.text._ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.type	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, @function
_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm:
.LFB6469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L545
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L555
.L571:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L556:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L569
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L570
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L548:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L550
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L552:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L553:
	testq	%rsi, %rsi
	je	.L550
.L551:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L552
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L558
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L551
	.p2align 4,,10
	.p2align 3
.L550:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L554
	call	_ZdlPv@PLT
.L554:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L571
.L555:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L557
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L557:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L558:
	movq	%rdx, %rdi
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L548
.L570:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6469:
	.size	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm, .-_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector13WorkerManager25SetWaitOnStartForDelegateEib
	.type	_ZN4node9inspector13WorkerManager25SetWaitOnStartForDelegateEib, @function
_ZN4node9inspector13WorkerManager25SetWaitOnStartForDelegateEib:
.LFB4992:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	144(%rdi), %r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 12, -40
	movl	%esi, -36(%rbp)
	testb	%dl, %dl
	je	.L573
	movslq	%esi, %r14
	movq	152(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r14, %rax
	movq	%r14, %r8
	divq	%rsi
	movq	144(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r12
	testq	%rax, %rax
	je	.L574
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L586:
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r12
	jne	.L574
.L576:
	cmpl	%edi, %r8d
	je	.L572
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.L586
.L574:
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movq	$0, (%rax)
	movl	-36(%rbp), %eax
	movl	$1, %r8d
	movl	%eax, 8(%rcx)
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE21_M_insert_unique_nodeEmmPNS1_10_Hash_nodeIiLb0EEEm
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L573:
	leaq	-36(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt10_HashtableIiiSaIiENSt8__detail9_IdentityESt8equal_toIiESt4hashIiENS1_18_Mod_range_hashingENS1_20_Default_ranged_hashENS1_20_Prime_rehash_policyENS1_17_Hashtable_traitsILb0ELb1ELb1EEEE8_M_eraseESt17integral_constantIbLb1EERKi
.L572:
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4992:
	.size	_ZN4node9inspector13WorkerManager25SetWaitOnStartForDelegateEib, .-_ZN4node9inspector13WorkerManager25SetWaitOnStartForDelegateEib
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector24WorkerManagerEventHandle14SetWaitOnStartEb
	.type	_ZN4node9inspector24WorkerManagerEventHandle14SetWaitOnStartEb, @function
_ZN4node9inspector24WorkerManagerEventHandle14SetWaitOnStartEb:
.LFB4993:
	.cfi_startproc
	endbr64
	movzbl	%sil, %edx
	movl	16(%rdi), %esi
	movq	(%rdi), %rdi
	jmp	_ZN4node9inspector13WorkerManager25SetWaitOnStartForDelegateEib
	.cfi_endproc
.LFE4993:
	.size	_ZN4node9inspector24WorkerManagerEventHandle14SetWaitOnStartEb, .-_ZN4node9inspector24WorkerManagerEventHandle14SetWaitOnStartEb
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_120WorkerStartedRequestE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_120WorkerStartedRequestE, 40
_ZTVN4node9inspector12_GLOBAL__N_120WorkerStartedRequestE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequest4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120WorkerStartedRequestD0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestE, 40
_ZTVN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequest4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_121WorkerFinishedRequestD0Ev
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
