	.file	"worker_agent.cc"
	.text
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB9206:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9206:
	.size	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB9234:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE9234:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB9242:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE9242:
	.size	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB9240:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	movl	$8, %esi
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	ret
	.cfi_endproc
.LFE9240:
	.size	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB9208:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9208:
	.size	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB9241:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9241:
	.size	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB9236:
	.cfi_startproc
	endbr64
	movl	$128, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9236:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB9239:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L10
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L10:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9239:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev:
.LFB4859:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	leaq	120(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L15
	call	_ZdlPv@PLT
.L15:
	movq	72(%rbx), %rdi
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L16
	call	_ZdlPv@PLT
.L16:
	movq	40(%rbx), %rdi
	leaq	56(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L17
	call	_ZdlPv@PLT
.L17:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L14
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4859:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD1Ev
	.set	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD1Ev,_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB9238:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE9238:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv:
.LFB4871:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	call	*24(%rax)
.L21:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L28:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4871:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev:
.LFB4870:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-32(%rbp), %rdi
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node9inspector8protocol10NodeWorker10WorkerInfo7toValueEv@PLT
	movq	-32(%rbp), %rsi
	movq	%r12, %rdi
	movq	(%rsi), %rax
	call	*(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	(%rdi), %rax
	call	*24(%rax)
.L29:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L36
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L36:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE4870:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11WorkerAgent7disableEv
	.type	_ZN4node9inspector8protocol11WorkerAgent7disableEv, @function
_ZN4node9inspector8protocol11WorkerAgent7disableEv:
.LFB6087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movq	40(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	$0, 40(%rsi)
	testq	%r13, %r13
	je	.L38
	movq	%r13, %rdi
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L38:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L44:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6087:
	.size	_ZN4node9inspector8protocol11WorkerAgent7disableEv, .-_ZN4node9inspector8protocol11WorkerAgent7disableEv
	.section	.text._ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev,"axG",@progbits,_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev
	.type	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev, @function
_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev:
.LFB4861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	104(%rdi), %rdi
	leaq	120(%r12), %rax
	cmpq	%rax, %rdi
	je	.L46
	call	_ZdlPv@PLT
.L46:
	movq	72(%r12), %rdi
	leaq	88(%r12), %rax
	cmpq	%rax, %rdi
	je	.L47
	call	_ZdlPv@PLT
.L47:
	movq	40(%r12), %rdi
	leaq	56(%r12), %rax
	cmpq	%rax, %rdi
	je	.L48
	call	_ZdlPv@PLT
.L48:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L49
	call	_ZdlPv@PLT
.L49:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$136, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE4861:
	.size	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev, .-_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB9237:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	80(%rdi), %r12
	testq	%r12, %r12
	jne	.L56
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L88:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L52
.L55:
	movq	%r13, %r12
.L56:
	movq	40(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	je	.L53
	movq	(%rdi), %rax
	call	*8(%rax)
.L53:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	jne	.L88
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L55
.L52:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	64(%rbx), %rdi
	leaq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	cmpq	%rax, %rdi
	je	.L57
	call	_ZdlPv@PLT
.L57:
	movq	56(%rbx), %r12
	testq	%r12, %r12
	je	.L59
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L60
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L89
	.p2align 4,,10
	.p2align 3
.L59:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L66
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L67
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L90
	.p2align 4,,10
	.p2align 3
.L66:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L51
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L72
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L91
.L51:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L51
.L91:
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L66
.L90:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L60:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L59
.L89:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L63
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L64:
	cmpl	$1, %eax
	jne	.L59
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L64
	.cfi_endproc
.LFE9237:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.section	.text._ZN4node9inspector8protocol11WorkerAgentD0Ev,"axG",@progbits,_ZN4node9inspector8protocol11WorkerAgentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11WorkerAgentD0Ev
	.type	_ZN4node9inspector8protocol11WorkerAgentD0Ev, @function
_ZN4node9inspector8protocol11WorkerAgentD0Ev:
.LFB9220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	56(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L94
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L95
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L117
	.p2align 4,,10
	.p2align 3
.L94:
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L100
	movq	%r13, %rdi
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L100:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L102
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L103
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L118
	.p2align 4,,10
	.p2align 3
.L102:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L107
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L108
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L119
.L107:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$64, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L108:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L107
.L119:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L111
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L112:
	cmpl	$1, %eax
	jne	.L107
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L103:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L102
.L118:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L95:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L94
.L117:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L98
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L99:
	cmpl	$1, %eax
	jne	.L94
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L98:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L111:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L112
	.cfi_endproc
.LFE9220:
	.size	_ZN4node9inspector8protocol11WorkerAgentD0Ev, .-_ZN4node9inspector8protocol11WorkerAgentD0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD2Ev:
.LFB7638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	16(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L120
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L123
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L129
.L120:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L120
.L129:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L126
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L127:
	cmpl	$1, %eax
	jne	.L120
	movq	(%r12), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L126:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L127
	.cfi_endproc
.LFE7638:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD0Ev:
.LFB7640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L132
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L133
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L139
.L132:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L132
.L139:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L136
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L137:
	cmpl	$1, %eax
	jne	.L132
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L136:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L137
	.cfi_endproc
.LFE7640:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
	.text
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB9375:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$208, %rsp
	.cfi_offset 3, -32
	movq	%r8, -160(%rbp)
	movq	%r9, -152(%rbp)
	testb	%al, %al
	je	.L141
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm1, -128(%rbp)
	movaps	%xmm2, -112(%rbp)
	movaps	%xmm3, -96(%rbp)
	movaps	%xmm4, -80(%rbp)
	movaps	%xmm5, -64(%rbp)
	movaps	%xmm6, -48(%rbp)
	movaps	%xmm7, -32(%rbp)
.L141:
	movq	%fs:40, %rax
	movq	%rax, -200(%rbp)
	xorl	%eax, %eax
	movq	%rsp, %rax
	cmpq	%rax, %rsp
	je	.L143
.L162:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L162
.L143:
	subq	$32, %rsp
	orq	$0, 24(%rsp)
	movl	$16, %ecx
	movl	$16, %esi
	movl	$1, %edx
	leaq	.LC0(%rip), %r8
	leaq	15(%rsp), %rbx
	leaq	16(%rbp), %rax
	movl	$32, -224(%rbp)
	andq	$-16, %rbx
	movq	%rax, -216(%rbp)
	leaq	-224(%rbp), %r9
	leaq	-192(%rbp), %rax
	movq	%rbx, %rdi
	movq	%rax, -208(%rbp)
	movl	$48, -220(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rcx
	movq	%rcx, (%r12)
	movslq	%eax, %rsi
	cmpl	$1, %eax
	jne	.L145
	movzbl	(%rbx), %eax
	movl	$1, %esi
	movb	%al, 16(%r12)
.L146:
	movq	%rsi, 8(%r12)
	movb	$0, (%rcx,%rsi)
	movq	-200(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L163
	leaq	-16(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	cmpl	$8, %eax
	jnb	.L147
	testb	$4, %al
	jne	.L164
	testl	%eax, %eax
	je	.L148
	movzbl	(%rbx), %edx
	movb	%dl, 16(%r12)
	testb	$2, %al
	jne	.L165
.L148:
	movq	(%r12), %rcx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%rbx), %rdx
	movq	%rdx, 16(%r12)
	movl	%eax, %edx
	movq	-8(%rbx,%rdx), %rdi
	movq	%rdi, -8(%rcx,%rdx)
	leaq	24(%r12), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%ecx, %eax
	subq	%rcx, %rbx
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L148
	andl	$-8, %eax
	xorl	%edx, %edx
.L151:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rbx,%rcx), %r8
	movq	%r8, (%rdi,%rcx)
	cmpl	%eax, %edx
	jb	.L151
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L164:
	movl	(%rbx), %edx
	movl	%edx, 16(%r12)
	movl	%eax, %edx
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L148
.L165:
	movl	%eax, %edx
	movzwl	-2(%rbx,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L148
.L163:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9375:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE:
.LFB6055:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	-80(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$56, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	pushq	16(%rsi)
	pushq	8(%rsi)
	pushq	(%rsi)
	call	_ZN4node9inspector8protocol10StringUtil16StringViewToUtf8B5cxx11EN12v8_inspector10StringViewE@PLT
	movq	40(%rbx), %rdx
	addq	$32, %rsp
	movq	24(%rdx), %r13
	testq	%r13, %r13
	je	.L173
	movl	8(%r13), %eax
	leaq	8(%r13), %r14
.L169:
	testl	%eax, %eax
	je	.L173
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%r14)
	jne	.L169
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.L170
	movq	16(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L170
	leaq	8(%rbx), %rsi
	movq	%r12, %rdx
	call	_ZN4node9inspector8protocol10NodeWorker8Frontend25receivedMessageFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_@PLT
.L170:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L194
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
	cmpl	$1, %eax
	je	.L195
	.p2align 4,,10
	.p2align 3
.L173:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L166
	call	_ZdlPv@PLT
.L166:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L196
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L173
.L195:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L174
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L175:
	cmpl	$1, %eax
	jne	.L173
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L174:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L175
.L196:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6055:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, .-_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.section	.text._ZN4node9inspector8protocol11WorkerAgentD2Ev,"axG",@progbits,_ZN4node9inspector8protocol11WorkerAgentD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9inspector8protocol11WorkerAgentD2Ev
	.type	_ZN4node9inspector8protocol11WorkerAgentD2Ev, @function
_ZN4node9inspector8protocol11WorkerAgentD2Ev:
.LFB9218:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L199
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L200
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L222
	.p2align 4,,10
	.p2align 3
.L199:
	movq	40(%rbx), %r12
	testq	%r12, %r12
	je	.L205
	movq	%r12, %rdi
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L205:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L207
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L208
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L223
	.p2align 4,,10
	.p2align 3
.L207:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L197
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L213
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L224
.L197:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L197
.L224:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L216
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L217:
	cmpl	$1, %eax
	jne	.L197
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L207
.L223:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L200:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L199
.L222:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L203
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L204:
	cmpl	$1, %eax
	jne	.L199
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L203:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L216:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L217
	.cfi_endproc
.LFE9218:
	.size	_ZN4node9inspector8protocol11WorkerAgentD2Ev, .-_ZN4node9inspector8protocol11WorkerAgentD2Ev
	.weak	_ZN4node9inspector8protocol11WorkerAgentD1Ev
	.set	_ZN4node9inspector8protocol11WorkerAgentD1Ev,_ZN4node9inspector8protocol11WorkerAgentD2Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11WorkerAgent6enableEb
	.type	_ZN4node9inspector8protocol11WorkerAgent6enableEb, @function
_ZN4node9inspector8protocol11WorkerAgent6enableEb:
.LFB6083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movq	32(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L226
	leaq	8(%r13), %rax
	movq	%rsi, %rbx
	movl	%edx, %r15d
	movq	%rax, -88(%rbp)
	movl	8(%r13), %eax
.L228:
	testl	%eax, %eax
	je	.L226
	movq	-88(%rbp), %rcx
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rcx)
	jne	.L228
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.L229
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L229
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L274
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
.L230:
	movzbl	%r15b, %esi
	call	_ZN4node9inspector24WorkerManagerEventHandle14SetWaitOnStartEb@PLT
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
.L254:
	testq	%r14, %r14
	je	.L243
	movq	-88(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L277
	.p2align 4,,10
	.p2align 3
.L225:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L278
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L243:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L225
.L277:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L247
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L248:
	cmpl	$1, %eax
	jne	.L225
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L274:
	movq	56(%rbx), %r9
	movq	48(%rbx), %rcx
	testq	%r9, %r9
	je	.L231
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	8(%r9), %rdx
	testq	%r14, %r14
	je	.L232
	lock addl	$1, (%rdx)
.L233:
	movl	$24, %edi
	movq	%rdx, -120(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rcx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_Znwm@PLT
	movq	-112(%rbp), %r9
	testq	%r14, %r14
	movq	-96(%rbp), %rsi
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateE(%rip), %rcx
	movq	-120(%rbp), %rdx
	movq	%rcx, (%rax)
	movq	-104(%rbp), %rcx
	movq	%r9, 16(%rax)
	movq	%rcx, 8(%rax)
	je	.L279
	lock addl	$1, (%rdx)
	movl	$-1, %ecx
	lock xaddl	%ecx, (%rdx)
	movl	%ecx, %edx
.L235:
	cmpl	$1, %edx
	je	.L280
.L236:
	leaq	-64(%rbp), %rdi
	leaq	-72(%rbp), %rdx
	movq	%rax, -72(%rbp)
	call	_ZN4node9inspector13WorkerManager13SetAutoAttachESt10unique_ptrINS0_14WorkerDelegateESt14default_deleteIS3_EE@PLT
	movq	-64(%rbp), %rax
	movq	40(%rbx), %rdi
	movq	$0, -64(%rbp)
	movq	%rax, 40(%rbx)
	testq	%rdi, %rdi
	je	.L240
	movq	%rdi, -96(%rbp)
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L240
	movq	%rdi, -96(%rbp)
	call	_ZN4node9inspector24WorkerManagerEventHandleD1Ev@PLT
	movq	-96(%rbp), %rdi
	movl	$24, %esi
	call	_ZdlPvm@PLT
.L240:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L242
	movq	(%rdi), %rax
	call	*16(%rax)
.L242:
	movq	40(%rbx), %rdi
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L247:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L248
.L229:
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L279:
	addl	$1, 8(%r9)
	movl	8(%r9), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r9)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L232:
	addl	$1, 8(%r9)
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L231:
	movl	$24, %edi
	movq	%rcx, -104(%rbp)
	movq	%rsi, -96(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateE(%rip), %rcx
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	movq	-96(%rbp), %rsi
	movq	%rcx, (%rax)
	movq	-104(%rbp), %rcx
	movq	$0, 16(%rax)
	movq	%rcx, 8(%rax)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L280:
	movq	(%r9), %rdx
	movq	%rax, -112(%rbp)
	movq	%r9, %rdi
	movq	%rsi, -104(%rbp)
	movq	%r9, -96(%rbp)
	call	*16(%rdx)
	testq	%r14, %r14
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %rax
	je	.L237
	movl	$-1, %edx
	lock xaddl	%edx, 12(%r9)
.L238:
	cmpl	$1, %edx
	jne	.L236
	movq	(%r9), %rdx
	movq	%rax, -104(%rbp)
	movq	%r9, %rdi
	movq	%rsi, -96(%rbp)
	call	*24(%rdx)
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rsi
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L237:
	movl	12(%r9), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r9)
	jmp	.L238
.L278:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6083:
	.size	_ZN4node9inspector8protocol11WorkerAgent6enableEb, .-_ZN4node9inspector8protocol11WorkerAgent6enableEb
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11WorkerAgentC2ESt8weak_ptrINS0_13WorkerManagerEE
	.type	_ZN4node9inspector8protocol11WorkerAgentC2ESt8weak_ptrINS0_13WorkerManagerEE, @function
_ZN4node9inspector8protocol11WorkerAgentC2ESt8weak_ptrINS0_13WorkerManagerEE:
.LFB6073:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector8protocol11WorkerAgentE(%rip), %rax
	movq	$0, 8(%rdi)
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	movq	$0, 16(%rdi)
	movq	%rax, 24(%rdi)
	movq	8(%rsi), %rax
	movq	%rax, 32(%rdi)
	testq	%rax, %rax
	je	.L282
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L283
	lock addl	$1, 12(%rax)
.L282:
	pxor	%xmm0, %xmm0
	movq	$0, 56(%rdi)
	movups	%xmm0, 40(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	addl	$1, 12(%rax)
	jmp	.L282
	.cfi_endproc
.LFE6073:
	.size	_ZN4node9inspector8protocol11WorkerAgentC2ESt8weak_ptrINS0_13WorkerManagerEE, .-_ZN4node9inspector8protocol11WorkerAgentC2ESt8weak_ptrINS0_13WorkerManagerEE
	.globl	_ZN4node9inspector8protocol11WorkerAgentC1ESt8weak_ptrINS0_13WorkerManagerEE
	.set	_ZN4node9inspector8protocol11WorkerAgentC1ESt8weak_ptrINS0_13WorkerManagerEE,_ZN4node9inspector8protocol11WorkerAgentC2ESt8weak_ptrINS0_13WorkerManagerEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherE
	.type	_ZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherE, @function
_ZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherE:
.LFB6075:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movl	$8, %edi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %r13
	call	_Znwm@PLT
	movl	$24, %edi
	movq	%r13, (%rax)
	movq	%rax, %rbx
	call	_Znwm@PLT
	movq	16(%r12), %r15
	movq	%rbx, %xmm0
	movabsq	$4294967297, %rcx
	movq	%rax, %xmm1
	movq	%rcx, 8(%rax)
	leaq	16+_ZTVSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rcx
	punpcklqdq	%xmm1, %xmm0
	movq	%rcx, (%rax)
	movq	%rbx, 16(%rax)
	movups	%xmm0, 8(%r12)
	testq	%r15, %r15
	je	.L289
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L290
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r15)
	cmpl	$1, %eax
	je	.L373
	.p2align 4,,10
	.p2align 3
.L289:
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN4node9inspector8protocol10NodeWorker10Dispatcher4wireEPNS1_14UberDispatcherEPNS2_7BackendE@PLT
	movq	32(%r12), %r15
	testq	%r15, %r15
	je	.L298
	leaq	8(%r15), %rax
	movq	%rax, -64(%rbp)
	movl	8(%r15), %eax
.L297:
	testl	%eax, %eax
	je	.L298
	movq	-64(%rbp), %rsi
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rsi)
	jne	.L297
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.L298
	movq	24(%r12), %rax
	testq	%rax, %rax
	je	.L298
	movq	16(%rax), %rcx
	movq	24(%rax), %r14
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	movq	%rcx, -56(%rbp)
	testq	%r14, %r14
	je	.L300
	testq	%r13, %r13
	je	.L301
	lock addl	$1, 8(%r14)
.L300:
	movl	$128, %edi
	call	_Znwm@PLT
	movq	16(%r12), %r9
	movq	%rax, %rbx
	movabsq	$4294967297, %rax
	movq	%rax, 8(%rbx)
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%rbx)
	movq	8(%r12), %rax
	testq	%r9, %r9
	je	.L302
	leaq	12(%r9), %rdx
	testq	%r13, %r13
	je	.L303
	lock addl	$1, (%rdx)
.L304:
	pxor	%xmm0, %xmm0
	movq	%r9, %xmm4
	movups	%xmm0, 16(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 32(%rbx)
	testq	%r13, %r13
	je	.L374
	lock addl	$1, (%rdx)
.L305:
	movq	-56(%rbp), %xmm0
	movq	%r14, %xmm2
	leaq	112(%rbx), %rdx
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 48(%rbx)
	testq	%r14, %r14
	je	.L306
	leaq	8(%r14), %rax
	testq	%r13, %r13
	je	.L307
	lock addl	$1, (%rax)
.L308:
	movq	%rdx, 64(%rbx)
	movq	$1, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movl	$0x3f800000, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movl	$0, 120(%rbx)
	testq	%r13, %r13
	je	.L375
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L376
.L311:
	testq	%r9, %r9
	je	.L318
	testq	%r13, %r13
	je	.L316
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r9)
.L317:
	cmpl	$1, %eax
	jne	.L318
	movq	(%r9), %rax
	movq	%r9, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L318:
	leaq	16(%rbx), %rax
	movq	%rax, %xmm0
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L319
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L319
.L320:
	movq	56(%r12), %r14
	movq	%rbx, %xmm3
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 48(%r12)
	testq	%r14, %r14
	je	.L332
	testq	%r13, %r13
	je	.L328
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
	cmpl	$1, %eax
	je	.L377
	.p2align 4,,10
	.p2align 3
.L332:
	testq	%r13, %r13
	je	.L378
	movq	-64(%rbp), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, (%rsi)
	cmpl	$1, %eax
	je	.L379
.L287:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L290:
	.cfi_restore_state
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L289
.L373:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L293
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L294:
	cmpl	$1, %eax
	jne	.L289
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*24(%rax)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%xmm0, 16(%rbx)
	testq	%r13, %r13
	je	.L380
	lock addl	$1, 12(%rbx)
.L321:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L323
	testq	%r13, %r13
	je	.L324
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L325:
	cmpl	$1, %eax
	jne	.L323
	movq	(%rdi), %rax
	movq	%xmm0, -56(%rbp)
	call	*24(%rax)
	movq	-56(%rbp), %xmm0
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rbx, 24(%rbx)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L307:
	addl	$1, 8(%r14)
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L301:
	addl	$1, 8(%r14)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L374:
	addl	$1, 12(%r9)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L303:
	addl	$1, 12(%r9)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rdx, 64(%rbx)
	movq	$1, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movl	$0x3f800000, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	movl	$0, 120(%rbx)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L302:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movq	%rax, %xmm0
	movups	%xmm0, 32(%rbx)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L375:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L311
.L376:
	movq	(%r14), %rax
	movq	%r9, -56(%rbp)
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	movq	-56(%rbp), %r9
	je	.L312
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L313:
	cmpl	$1, %eax
	jne	.L311
	movq	(%r14), %rax
	movq	%r9, -56(%rbp)
	movq	%r14, %rdi
	call	*24(%rax)
	movq	-56(%rbp), %r9
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L316:
	movl	12(%r9), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r9)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L328:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	cmpl	$1, %eax
	jne	.L332
.L377:
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L330
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L331:
	cmpl	$1, %eax
	jne	.L332
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L378:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L287
.L379:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L336
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L337:
	cmpl	$1, %eax
	jne	.L287
	movq	(%r15), %rax
	movq	%r15, %rdi
	movq	24(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	leaq	_ZZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L380:
	addl	$1, 12(%rbx)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L324:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L293:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L330:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L312:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L336:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L337
	.cfi_endproc
.LFE6075:
	.size	_ZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherE, .-_ZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeWorkers4SendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.type	_ZN4node9inspector8protocol11NodeWorkers4SendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, @function
_ZN4node9inspector8protocol11NodeWorkers4SendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_:
.LFB6093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L381
	movl	8(%r12), %eax
	leaq	8(%r12), %rbx
.L384:
	testl	%eax, %eax
	je	.L381
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L384
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L385
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L385
	call	_ZN4node9inspector8protocol10NodeWorker8Frontend25receivedMessageFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_@PLT
.L385:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L407
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L408
.L381:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L381
.L408:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L389
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L390:
	cmpl	$1, %eax
	jne	.L381
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L389:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L390
	.cfi_endproc
.LFE6093:
	.size	_ZN4node9inspector8protocol11NodeWorkers4SendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, .-_ZN4node9inspector8protocol11NodeWorkers4SendERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeWorkers7ReceiveERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.type	_ZN4node9inspector8protocol11NodeWorkers7ReceiveERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, @function
_ZN4node9inspector8protocol11NodeWorkers7ReceiveERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_:
.LFB6094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	movl	$3339675911, %edx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	56(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	48(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L409
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	48(%rbx), %rcx
.L413:
	cmpq	%rcx, %r13
	je	.L430
.L411:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L409
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r8
	je	.L413
	.p2align 4,,10
	.p2align 3
.L409:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L431
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L411
	movq	%r8, -72(%rbp)
	testq	%rdx, %rdx
	je	.L412
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	movq	-72(%rbp), %r8
	testl	%eax, %eax
	jne	.L411
.L412:
	movq	40(%rbx), %r12
	movq	%r15, %rsi
	leaq	-64(%rbp), %rdi
	movq	(%r12), %rax
	movq	16(%rax), %rbx
	call	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	*%rbx
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L409
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L409
.L431:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6094:
	.size	_ZN4node9inspector8protocol11NodeWorkers7ReceiveERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, .-_ZN4node9inspector8protocol11NodeWorkers7ReceiveERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11WorkerAgent19sendMessageToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.type	_ZN4node9inspector8protocol11WorkerAgent19sendMessageToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, @function
_ZN4node9inspector8protocol11WorkerAgent19sendMessageToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_:
.LFB6076:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rcx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	48(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol11NodeWorkers7ReceiveERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L435
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L435:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6076:
	.size	_ZN4node9inspector8protocol11WorkerAgent19sendMessageToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_, .-_ZN4node9inspector8protocol11WorkerAgent19sendMessageToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6095:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	8(%rsi), %rsi
	movq	0(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	56(%rbx), %r15
	xorl	%edx, %edx
	movq	48(%rbx), %r10
	movq	%rax, %r14
	divq	%r15
	leaq	(%r10,%rdx,8), %r11
	movq	(%r11), %rcx
	testq	%rcx, %rcx
	je	.L436
	movq	(%rcx), %r12
	movq	%rdx, %r9
	movq	%rcx, %r8
	movq	48(%r12), %rsi
	cmpq	%rsi, %r14
	je	.L493
	.p2align 4,,10
	.p2align 3
.L438:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L436
	movq	48(%rdi), %rsi
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%rsi, %rax
	divq	%r15
	cmpq	%rdx, %r9
	jne	.L436
	movq	%rdi, %r12
	cmpq	%rsi, %r14
	jne	.L438
.L493:
	movq	8(%r13), %rdx
	cmpq	16(%r12), %rdx
	jne	.L438
	testq	%rdx, %rdx
	je	.L439
	movq	8(%r12), %rsi
	movq	0(%r13), %rdi
	movq	%r9, -88(%rbp)
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r10, -56(%rbp)
	call	memcmp@PLT
	movq	-56(%rbp), %r10
	movq	-64(%rbp), %r11
	testl	%eax, %eax
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	-88(%rbp), %r9
	jne	.L438
.L439:
	movq	(%r12), %rsi
	cmpq	%r8, %rcx
	je	.L494
	testq	%rsi, %rsi
	je	.L442
	movq	48(%rsi), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r9
	je	.L442
	movq	%r8, (%r10,%rdx,8)
	movq	(%r12), %rsi
.L442:
	movq	40(%r12), %rdi
	movq	%rsi, (%r8)
	testq	%rdi, %rdi
	je	.L444
	movq	(%rdi), %rax
	call	*8(%rax)
.L444:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L445
	call	_ZdlPv@PLT
.L445:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	movq	24(%rbx), %r12
	subq	$1, 72(%rbx)
	testq	%r12, %r12
	je	.L436
	movl	8(%r12), %eax
	leaq	8(%r12), %r14
.L448:
	testl	%eax, %eax
	je	.L436
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%r14)
	jne	.L448
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L449
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L449
	movq	%r13, %rsi
	call	_ZN4node9inspector8protocol10NodeWorker8Frontend18detachedFromWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
.L449:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L495
	movl	$-1, %eax
	lock xaddl	%eax, (%r14)
.L450:
	cmpl	$1, %eax
	jne	.L436
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L453
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L454:
	cmpl	$1, %eax
	jne	.L436
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L436:
	.cfi_restore_state
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L494:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L462
	movq	48(%rsi), %rax
	xorl	%edx, %edx
	divq	%r15
	cmpq	%rdx, %r9
	je	.L442
	movq	%r8, (%r10,%rdx,8)
	movq	(%r11), %rax
.L441:
	leaq	64(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L496
.L443:
	movq	$0, (%r11)
	movq	(%r12), %rsi
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L495:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L450
	.p2align 4,,10
	.p2align 3
.L462:
	movq	%r8, %rax
	jmp	.L441
	.p2align 4,,10
	.p2align 3
.L453:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%rsi, 64(%rbx)
	jmp	.L443
	.cfi_endproc
.LFE6095:
	.size	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11WorkerAgent6detachERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector8protocol11WorkerAgent6detachERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector8protocol11WorkerAgent6detachERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB6088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	48(%r8), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	%r12, %rdi
	call	_ZN4node9inspector8protocol16DispatchResponse2OKEv@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L500
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L500:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6088:
	.size	_ZN4node9inspector8protocol11WorkerAgent6detachERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector8protocol11WorkerAgent6detachERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD2Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD2Ev:
.LFB6052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE(%rip), %rax
	leaq	8(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	call	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	48(%rbx), %r12
	testq	%r12, %r12
	je	.L503
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L504
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L511
	.p2align 4,,10
	.p2align 3
.L503:
	movq	8(%rbx), %rdi
	addq	$24, %rbx
	cmpq	%rbx, %rdi
	je	.L501
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L504:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L503
.L511:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L507
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L508:
	cmpl	$1, %eax
	jne	.L503
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L501:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L507:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L508
	.cfi_endproc
.LFE6052:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD2Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD2Ev
	.set	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD1Ev,_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev, @function
_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev:
.LFB6054:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE(%rip), %rax
	leaq	8(%rdi), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	movq	40(%rdi), %rdi
	call	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	48(%r12), %r13
	testq	%r13, %r13
	je	.L514
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L515
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L522
	.p2align 4,,10
	.p2align 3
.L514:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	cmpq	%rax, %rdi
	je	.L520
	call	_ZdlPv@PLT
.L520:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$56, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L515:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L514
.L522:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L518
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L519:
	cmpl	$1, %eax
	jne	.L514
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L518:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L519
	.cfi_endproc
.LFE6054:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev, .-_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev
	.section	.rodata._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"basic_string::_M_construct null not valid"
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_:
.LFB7687:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	(%r15), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L524
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	48(%rbx), %rsi
.L527:
	cmpq	%rsi, %r13
	je	.L581
.L525:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L524
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L527
.L524:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	(%r15), %r8
	movq	8(%r15), %r15
	leaq	24(%rax), %rdi
	movq	$0, (%rax)
	movq	%rax, %rbx
	movq	%rdi, 8(%rax)
	movq	%r8, %rax
	addq	%r15, %rax
	je	.L528
	testq	%r8, %r8
	je	.L582
.L528:
	movq	%r15, -64(%rbp)
	cmpq	$15, %r15
	ja	.L583
	cmpq	$1, %r15
	jne	.L531
	movzbl	(%r8), %eax
	movb	%al, 24(%rbx)
.L532:
	movq	%r15, 16(%rbx)
	movl	$1, %ecx
	movb	$0, (%rdi,%r15)
	movq	24(%r12), %rdx
	leaq	32(%r12), %rdi
	movq	$0, 40(%rbx)
	movq	8(%r12), %rsi
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r15
	testb	%al, %al
	jne	.L533
	movq	(%r12), %r8
	movq	%r13, 48(%rbx)
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L543
.L587:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L544:
	addq	$1, 24(%r12)
	leaq	40(%rbx), %rax
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L581:
	movq	8(%r15), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L525
	movq	%r8, -80(%rbp)
	movq	%rcx, -72(%rbp)
	testq	%rdx, %rdx
	je	.L526
	movq	8(%rbx), %rsi
	movq	(%r15), %rdi
	call	memcmp@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	testl	%eax, %eax
	jne	.L525
.L526:
	leaq	40(%rbx), %rax
.L523:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L584
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L531:
	.cfi_restore_state
	testq	%r15, %r15
	je	.L532
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L583:
	leaq	8(%rbx), %rdi
	leaq	-64(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r8, -72(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	-64(%rbp), %rax
	movq	%rax, 24(%rbx)
.L530:
	movq	%r15, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-64(%rbp), %r15
	movq	8(%rbx), %rdi
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L533:
	cmpq	$1, %rdx
	je	.L585
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L586
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L536:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L538
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L540:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L541:
	testq	%rsi, %rsi
	je	.L538
.L539:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r15
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L540
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L548
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L539
	.p2align 4,,10
	.p2align 3
.L538:
	movq	(%r12), %rdi
	cmpq	%r10, %rdi
	je	.L542
	movq	%r8, -72(%rbp)
	call	_ZdlPv@PLT
	movq	-72(%rbp), %r8
.L542:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r15, 8(%r12)
	divq	%r15
	movq	%r8, (%r12)
	movq	%r13, 48(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L587
.L543:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L545
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
.L545:
	leaq	16(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L548:
	movq	%rdx, %rdi
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L585:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L536
.L584:
	call	__stack_chk_fail@PLT
.L582:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L586:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7687:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE
	.type	_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE, @function
_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE:
.LFB6089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$216, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -184(%rbp)
	movq	24(%rdi), %r12
	movq	%rdx, -192(%rbp)
	movl	%ecx, -200(%rbp)
	movq	%r8, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L588
	leaq	8(%r12), %rax
	movq	%rdi, %rbx
	movq	%rax, -224(%rbp)
	movl	8(%r12), %eax
.L591:
	testl	%eax, %eax
	je	.L588
	movq	-224(%rbp), %rsi
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rsi)
	jne	.L591
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L592
	movq	16(%rbx), %rax
	movq	%rax, -232(%rbp)
	testq	%rax, %rax
	je	.L592
	movl	104(%rbx), %eax
	leaq	-128(%rbp), %r14
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	.LC0(%rip), %rcx
	movl	$16, %edx
	movq	%r14, %rdi
	leal	1(%rax), %r8d
	xorl	%eax, %eax
	movl	%r8d, 104(%rbx)
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movq	32(%rbx), %rax
	movq	%rax, -216(%rbp)
	movq	8(%rbx), %rax
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L600
	leaq	8(%rax), %rcx
	movq	%rcx, -176(%rbp)
	movl	8(%rax), %eax
.L602:
	testl	%eax, %eax
	je	.L600
	movq	-176(%rbp), %rcx
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rcx)
	jne	.L602
	movl	$56, %edi
	movq	(%rbx), %r13
	call	_Znwm@PLT
	movq	-128(%rbp), %r8
	movq	-120(%rbp), %r9
	movq	%rax, %rcx
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE(%rip), %rax
	movq	%rax, (%rcx)
	movq	%r8, %rax
	leaq	24(%rcx), %rdi
	addq	%r9, %rax
	movq	%rdi, 8(%rcx)
	je	.L603
	testq	%r8, %r8
	je	.L717
.L603:
	movq	%r9, -136(%rbp)
	cmpq	$15, %r9
	ja	.L718
	cmpq	$1, %r9
	jne	.L606
	movzbl	(%r8), %eax
	leaq	-136(%rbp), %r15
	movb	%al, 24(%rcx)
.L607:
	movq	%r13, %xmm0
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	movq	%r9, 16(%rcx)
	movhps	-168(%rbp), %xmm0
	movb	$0, (%rdi,%r9)
	movups	%xmm0, 40(%rcx)
	testq	%r13, %r13
	je	.L608
	movq	-176(%rbp), %rax
	lock addl	$1, (%rax)
.L609:
	movq	-216(%rbp), %rsi
	leaq	-152(%rbp), %rdi
	movq	%r15, %rdx
	movq	%rcx, -136(%rbp)
	call	_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE@PLT
	movq	-136(%rbp), %r8
	testq	%r8, %r8
	je	.L610
	movq	(%r8), %rax
	leaq	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L611
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE(%rip), %rax
	movq	40(%r8), %rdi
	leaq	8(%r8), %rsi
	movq	%r8, -216(%rbp)
	movq	%rax, (%r8)
	call	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-216(%rbp), %r8
	movq	48(%r8), %rdi
	testq	%rdi, %rdi
	je	.L613
	testq	%r13, %r13
	je	.L614
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rdi)
.L615:
	cmpl	$1, %eax
	jne	.L613
	movq	(%rdi), %rax
	movq	%r8, -240(%rbp)
	movq	%rdi, -216(%rbp)
	call	*16(%rax)
	testq	%r13, %r13
	movq	-216(%rbp), %rdi
	movq	-240(%rbp), %r8
	je	.L617
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L618:
	cmpl	$1, %eax
	jne	.L613
	movq	(%rdi), %rax
	movq	%r8, -216(%rbp)
	call	*24(%rax)
	movq	-216(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L613:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L619
	movq	%r8, -216(%rbp)
	call	_ZdlPv@PLT
	movq	-216(%rbp), %r8
.L619:
	movl	$56, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L610:
	testq	%r13, %r13
	je	.L620
	movq	-176(%rbp), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, (%rsi)
	cmpl	$1, %eax
	je	.L719
.L623:
	movq	-208(%rbp), %rax
	leaq	-144(%rbp), %rdx
	movl	$1, %ecx
	movq	%r15, %rdi
	movq	(%rax), %rsi
	movq	-152(%rbp), %rax
	movq	$0, -152(%rbp)
	movq	%rax, -144(%rbp)
	call	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	leaq	48(%rbx), %rdi
	movq	%r14, %rsi
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_St10unique_ptrIN4node9inspector16InspectorSessionESt14default_deleteISC_EEESaISG_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixERS8_
	movq	-136(%rbp), %rdx
	movq	$0, -136(%rbp)
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	testq	%rdi, %rdi
	je	.L627
	movq	(%rdi), %rax
	call	*8(%rax)
.L627:
	movq	-136(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L628
	movq	(%rdi), %rax
	call	*8(%rax)
.L628:
	movq	-144(%rbp), %r8
	testq	%r8, %r8
	je	.L629
	movq	(%r8), %rax
	leaq	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L630
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE(%rip), %rax
	movq	40(%r8), %rdi
	leaq	8(%r8), %rsi
	movq	%r8, -168(%rbp)
	movq	%rax, (%r8)
	call	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-168(%rbp), %r8
	movq	48(%r8), %rbx
	testq	%rbx, %rbx
	je	.L632
	testq	%r13, %r13
	je	.L633
	movl	$-1, %eax
	lock xaddl	%eax, 8(%rbx)
.L634:
	cmpl	$1, %eax
	jne	.L632
	movq	(%rbx), %rax
	movq	%r8, -168(%rbp)
	movq	%rbx, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	movq	-168(%rbp), %r8
	je	.L636
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rbx)
.L637:
	cmpl	$1, %eax
	jne	.L632
	movq	(%rbx), %rax
	movq	%r8, -168(%rbp)
	movq	%rbx, %rdi
	call	*24(%rax)
	movq	-168(%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L632:
	movq	8(%r8), %rdi
	leaq	24(%r8), %rax
	cmpq	%rax, %rdi
	je	.L638
	movq	%r8, -168(%rbp)
	call	_ZdlPv@PLT
	movq	-168(%rbp), %r8
.L638:
	movl	$56, %esi
	movq	%r8, %rdi
	call	_ZdlPvm@PLT
.L629:
	movzbl	-200(%rbp), %ecx
	movl	$136, %edi
	leaq	16+_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE(%rip), %rbx
	movl	%ecx, -216(%rbp)
	call	_Znwm@PLT
	movq	%r14, %rsi
	leaq	24(%rax), %rdx
	movq	%rbx, (%rax)
	leaq	40(%rax), %r8
	movq	%rdx, 8(%rax)
	leaq	56(%rax), %rdx
	leaq	72(%rax), %r10
	movq	%rdx, 40(%rax)
	leaq	88(%rax), %rdx
	leaq	104(%rax), %r9
	movq	%rdx, 72(%rax)
	leaq	120(%rax), %rdx
	leaq	8(%rax), %rdi
	movq	%rdx, 104(%rax)
	movq	$0, 16(%rax)
	movb	$0, 24(%rax)
	movq	$0, 48(%rax)
	movb	$0, 56(%rax)
	movq	$0, 80(%rax)
	movb	$0, 88(%rax)
	movq	$0, 112(%rax)
	movb	$0, 120(%rax)
	movq	%r8, -208(%rbp)
	movq	%rax, -176(%rbp)
	movq	%r9, -168(%rbp)
	movq	%r10, -200(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-200(%rbp), %r10
	movq	-184(%rbp), %rsi
	movq	%r10, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-168(%rbp), %r9
	movq	-192(%rbp), %rsi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-208(%rbp), %r8
	leaq	-80(%rbp), %rdx
	movl	$29285, %eax
	leaq	-96(%rbp), %rsi
	movq	%rdx, -96(%rbp)
	movq	%r8, %rdi
	movq	%rdx, -168(%rbp)
	movw	%ax, -76(%rbp)
	movl	$1802661751, -80(%rbp)
	movq	$6, -88(%rbp)
	movb	$0, -74(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_assignERKS4_@PLT
	movq	-96(%rbp), %rdi
	movq	-168(%rbp), %rdx
	movq	-176(%rbp), %rax
	movl	-216(%rbp), %ecx
	cmpq	%rdx, %rdi
	movq	%rax, -136(%rbp)
	je	.L639
	movl	%ecx, -168(%rbp)
	call	_ZdlPv@PLT
	movl	-168(%rbp), %ecx
.L639:
	movq	-232(%rbp), %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	call	_ZN4node9inspector8protocol10NodeWorker8Frontend16attachedToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt10unique_ptrINS2_10WorkerInfoESt14default_deleteISD_EEb@PLT
	movq	-136(%rbp), %r14
	testq	%r14, %r14
	je	.L640
	movq	(%r14), %rax
	leaq	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L641
	movq	104(%r14), %rdi
	leaq	120(%r14), %rax
	movq	%rbx, (%r14)
	cmpq	%rax, %rdi
	je	.L642
	call	_ZdlPv@PLT
.L642:
	movq	72(%r14), %rdi
	leaq	88(%r14), %rax
	cmpq	%rax, %rdi
	je	.L643
	call	_ZdlPv@PLT
.L643:
	movq	40(%r14), %rdi
	leaq	56(%r14), %rax
	cmpq	%rax, %rdi
	je	.L644
	call	_ZdlPv@PLT
.L644:
	movq	8(%r14), %rdi
	leaq	24(%r14), %rax
	cmpq	%rax, %rdi
	je	.L645
	call	_ZdlPv@PLT
.L645:
	movl	$136, %esi
	movq	%r14, %rdi
	call	_ZdlPvm@PLT
.L640:
	movq	-152(%rbp), %r15
	testq	%r15, %r15
	je	.L646
	movq	(%r15), %rax
	leaq	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev(%rip), %rdx
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L647
	leaq	16+_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE(%rip), %rax
	movq	40(%r15), %rdi
	leaq	8(%r15), %rsi
	movq	%rax, (%r15)
	call	_ZN4node9inspector8protocol11NodeWorkers8DetachedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	48(%r15), %r14
	testq	%r14, %r14
	je	.L649
	testq	%r13, %r13
	je	.L650
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r14)
.L651:
	cmpl	$1, %eax
	jne	.L649
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L653
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r14)
.L654:
	cmpl	$1, %eax
	jne	.L649
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L649:
	movq	8(%r15), %rdi
	leaq	24(%r15), %rax
	cmpq	%rax, %rdi
	je	.L655
	call	_ZdlPv@PLT
.L655:
	movl	$56, %esi
	movq	%r15, %rdi
	call	_ZdlPvm@PLT
.L646:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L656
	call	_ZdlPv@PLT
.L656:
	testq	%r13, %r13
	je	.L709
.L662:
	movq	-224(%rbp), %rbx
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L720
	.p2align 4,,10
	.p2align 3
.L588:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L721
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L592:
	.cfi_restore_state
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	jne	.L662
.L709:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L588
.L720:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L658
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L659:
	cmpl	$1, %eax
	jne	.L588
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L606:
	leaq	-136(%rbp), %r15
	testq	%r9, %r9
	je	.L607
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L718:
	leaq	-136(%rbp), %r15
	leaq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%r8, -256(%rbp)
	movq	%r15, %rsi
	movq	%r9, -248(%rbp)
	movq	%rcx, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-240(%rbp), %rcx
	movq	-248(%rbp), %r9
	movq	%rax, %rdi
	movq	-256(%rbp), %r8
	movq	%rax, 8(%rcx)
	movq	-136(%rbp), %rax
	movq	%rax, 24(%rcx)
.L605:
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%rcx, -240(%rbp)
	call	memcpy@PLT
	movq	-240(%rbp), %rcx
	movq	-136(%rbp), %r9
	movq	8(%rcx), %rdi
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L608:
	movq	-168(%rbp), %rax
	addl	$1, 8(%rax)
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L633:
	movl	8(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rbx)
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L614:
	movl	8(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rdi)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L650:
	movl	8(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r14)
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L620:
	movq	-168(%rbp), %rcx
	movl	8(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%rcx)
	cmpl	$1, %eax
	jne	.L623
.L719:
	movq	-168(%rbp), %rsi
	movq	(%rsi), %rax
	movq	%rsi, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L624
	movq	-168(%rbp), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rsi)
.L625:
	cmpl	$1, %eax
	jne	.L623
	movq	-168(%rbp), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L630:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%r8, %rdi
	call	*%rax
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L647:
	movq	%r15, %rdi
	call	*%rax
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L641:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L658:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L624:
	movq	-168(%rbp), %rcx
	movl	12(%rcx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rcx)
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L636:
	movl	12(%rbx), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rbx)
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L653:
	movl	12(%r14), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r14)
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L617:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L618
.L721:
	call	__stack_chk_fail@PLT
.L717:
	leaq	.LC2(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE.cold, @function
_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE.cold:
.LFSB6089:
.L600:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE6089:
	.text
	.size	_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE, .-_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE
	.section	.text.unlikely
	.size	_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE.cold, .-_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE.cold
.LCOLDE3:
	.text
.LHOTE3:
	.align 2
	.p2align 4
	.type	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegate13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_bSt10shared_ptrINS0_16MainThreadHandleEE, @function
_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegate13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_bSt10shared_ptrINS0_16MainThreadHandleEE:
.LFB6040:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movdqu	(%r8), %xmm0
	movq	8(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%r8), %rax
	movaps	%xmm0, -48(%rbp)
	testq	%rax, %rax
	je	.L723
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L724
	lock addl	$1, 8(%rax)
.L723:
	movzbl	%cl, %ecx
	leaq	-48(%rbp), %r8
	call	_ZN4node9inspector8protocol11NodeWorkers13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_bSt10shared_ptrINS0_16MainThreadHandleEE
	movq	-40(%rbp), %r12
	testq	%r12, %r12
	je	.L722
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L727
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L737
	.p2align 4,,10
	.p2align 3
.L722:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L738
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L724:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L727:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L722
.L737:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L730
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L731:
	cmpl	$1, %eax
	jne	.L722
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L730:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L731
.L738:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6040:
	.size	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegate13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_bSt10shared_ptrINS0_16MainThreadHandleEE, .-_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegate13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_bSt10shared_ptrINS0_16MainThreadHandleEE
	.weak	_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE,"awG",@progbits,_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE, @object
	.size	_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE, 48
_ZTVN4node9inspector8protocol10NodeWorker10WorkerInfoE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo15serializeToJSONB5cxx11Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfo17serializeToBinaryEv
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD1Ev
	.quad	_ZN4node9inspector8protocol10NodeWorker10WorkerInfoD0Ev
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateE, 40
_ZTVN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegate13WorkerCreatedERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESB_bSt10shared_ptrINS0_16MainThreadHandleEE
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_128AgentWorkerInspectorDelegateD0Ev
	.align 8
	.type	_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE, @object
	.size	_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE, 40
_ZTVN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD1Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegateD0Ev
	.quad	_ZN4node9inspector8protocol12_GLOBAL__N_130ParentInspectorSessionDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.weak	_ZTVN4node9inspector8protocol11WorkerAgentE
	.section	.data.rel.ro.local._ZTVN4node9inspector8protocol11WorkerAgentE,"awG",@progbits,_ZTVN4node9inspector8protocol11WorkerAgentE,comdat
	.align 8
	.type	_ZTVN4node9inspector8protocol11WorkerAgentE, @object
	.size	_ZTVN4node9inspector8protocol11WorkerAgentE, 64
_ZTVN4node9inspector8protocol11WorkerAgentE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector8protocol11WorkerAgentD1Ev
	.quad	_ZN4node9inspector8protocol11WorkerAgentD0Ev
	.quad	_ZN4node9inspector8protocol11WorkerAgent19sendMessageToWorkerERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESA_
	.quad	_ZN4node9inspector8protocol11WorkerAgent6enableEb
	.quad	_ZN4node9inspector8protocol11WorkerAgent7disableEv
	.quad	_ZN4node9inspector8protocol11WorkerAgent6detachERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.weak	_ZTVSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt15_Sp_counted_ptrIPN4node9inspector8protocol10NodeWorker8FrontendELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector8protocol11NodeWorkersESaIS3_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"../src/inspector/worker_agent.cc:88"
	.section	.rodata.str1.1
.LC5:
	.string	"(manager) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"void node::inspector::protocol::WorkerAgent::Wire(node::inspector::protocol::UberDispatcher*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherEE4args, @object
	.size	_ZZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherEE4args, 24
_ZZN4node9inspector8protocol11WorkerAgent4WireEPNS1_14UberDispatcherEE4args:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
