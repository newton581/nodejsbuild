	.file	"main_thread_interface.cc"
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEEEvPS3_MS3_FvT_ERSC_, @function
_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEEEvPS3_MS3_FvT_ERSC_:
.LFB6259:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	testb	$1, %al
	je	.L2
	movq	(%rdi), %rdx
	movq	-1(%rdx,%rsi), %rax
.L2:
	movq	(%rcx), %rdx
	leaq	-16(%rbp), %rsi
	movq	$0, (%rcx)
	movq	%rdx, -16(%rbp)
	call	*%rax
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	call	*8(%rax)
.L1:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L13
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L13:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6259:
	.size	_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEEEvPS3_MS3_FvT_ERSC_, .-_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEEEvPS3_MS3_FvT_ERSC_
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEEEvPS3_MS3_FvT_ERSD_, @function
_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEEEvPS3_MS3_FvT_ERSD_:
.LFB6307:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rax
	testb	$1, %al
	je	.L15
	movq	(%rdi), %rdx
	movq	-1(%rdx,%rsi), %rax
.L15:
	movq	(%rcx), %rdx
	leaq	-16(%rbp), %rsi
	movq	$0, (%rcx)
	movq	%rdx, -16(%rbp)
	call	*%rax
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L14
	movq	(%rdi), %rax
	call	*8(%rax)
.L14:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L25
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L25:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6307:
	.size	_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEEEvPS3_MS3_FvT_ERSD_, .-_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEEEvPS3_MS3_FvT_ERSD_
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD2Ev:
.LFB6748:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6748:
	.size	_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD2Ev, .-_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD1Ev,_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED2Ev:
.LFB6821:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_EE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L27:
	ret
	.cfi_endproc
.LFE6821:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED2Ev, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED1Ev,_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED2Ev:
.LFB7016:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L29
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L29:
	ret
	.cfi_endproc
.LFE7016:
	.size	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED2Ev, .-_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED1Ev,_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED2Ev:
.LFB7220:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L31:
	ret
	.cfi_endproc
.LFE7220:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED2Ev, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED1Ev,_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED2Ev:
.LFB7252:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEEE(%rip), %rax
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L33
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L33:
	ret
	.cfi_endproc
.LFE7252:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED2Ev, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED1Ev,_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED2Ev:
.LFB7914:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7914:
	.size	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED2Ev, .-_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED1Ev,_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED2Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev:
.LFB7938:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7938:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.set	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED2Ev
	.text
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState6CreateEPNS0_19MainThreadInterfaceEb, @function
_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState6CreateEPNS0_19MainThreadInterfaceEb:
.LFB5047:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	movl	$24, %edi
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%edx, %ebx
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	%r13, (%rax)
	movb	%bl, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rax, (%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5047:
	.size	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState6CreateEPNS0_19MainThreadInterfaceEb, .-_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState6CreateEPNS0_19MainThreadInterfaceEb
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD0Ev:
.LFB6750:
	.cfi_startproc
	endbr64
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6750:
	.size	_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD0Ev, .-_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED0Ev:
.LFB7916:
	.cfi_startproc
	endbr64
	movl	$32, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7916:
	.size	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED0Ev, .-_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED0Ev:
.LFB7254:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L42
	movq	(%rdi), %rax
	call	*8(%rax)
.L42:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7254:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED0Ev, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED0Ev:
.LFB6823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	movq	(%rdi), %rax
	call	*8(%rax)
.L48:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE6823:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED0Ev, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED0Ev:
.LFB7222:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L54
	movq	(%rdi), %rax
	call	*8(%rax)
.L54:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$48, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7222:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED0Ev, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED0Ev:
.LFB7018:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	(%rdi), %rax
	call	*8(%rax)
.L60:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7018:
	.size	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED0Ev, .-_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev:
.LFB7940:
	.cfi_startproc
	endbr64
	movl	$88, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7940:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv:
.LFB7942:
	.cfi_startproc
	endbr64
	jmp	_ZdlPv@PLT
	.cfi_endproc
.LFE7942:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info:
.LFB7943:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	16(%rdi), %r12
	subq	$8, %rsp
	cmpq	%rax, %rsi
	je	.L67
	movq	%rsi, %rdi
	call	_ZNSt19_Sp_make_shared_tag5_S_eqERKSt9type_info@PLT
	testb	%al, %al
	movl	$0, %eax
	cmove	%rax, %r12
.L67:
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7943:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS4_EE, @function
_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS4_EE:
.LFB5050:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	272(%rax), %r8
	testq	%r8, %r8
	je	.L71
	movq	(%rsi), %rax
	movzbl	8(%rdi), %ecx
	movq	%rdi, %rbx
	leaq	-40(%rbp), %rdx
	movq	$0, (%rsi)
	leaq	-32(%rbp), %rdi
	movq	%r8, %rsi
	movq	%rax, -40(%rbp)
	call	_ZN4node9inspector5Agent7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb@PLT
	movq	-32(%rbp), %rax
	movq	16(%rbx), %rdi
	movq	$0, -32(%rbp)
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L74
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	-32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L74
	movq	(%rdi), %rax
	call	*8(%rax)
.L74:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L71
	movq	(%rdi), %rax
	call	*8(%rax)
.L71:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L88
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L88:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5050:
	.size	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS4_EE, .-_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS4_EE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv.part.0, @function
_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv.part.0:
.LFB8321:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	96(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	leaq	-64(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	184(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	152(%rdi), %rax
	movb	$1, 216(%rdi)
	cmpq	%rax, %rdx
	jne	.L95
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$8, %rax
	movq	%rax, 152(%rbx)
.L99:
	movq	280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v815SealHandleScopeC1EPNS_7IsolateE@PLT
	movq	(%r14), %rax
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*(%rax)
	movq	%r12, %rdi
	call	_ZN2v815SealHandleScopeD1Ev@PLT
	movq	(%r14), %rax
	movq	%r14, %rdi
	call	*16(%rax)
	movq	152(%rbx), %rax
	cmpq	%rax, 184(%rbx)
	je	.L101
.L95:
	movq	168(%rbx), %rcx
	movq	(%rax), %r14
	leaq	-8(%rcx), %rdx
	cmpq	%rdx, %rax
	jne	.L102
	movq	160(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	176(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm2
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 152(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 168(%rbx)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%rax, %rdx
	cmpq	%rax, %rdx
	jne	.L95
.L100:
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	40(%rbx), %rdx
	movq	56(%rbx), %rax
	movq	%r13, %rdi
	movdqu	168(%rbx), %xmm4
	movq	32(%rbx), %xmm1
	movq	%rdx, %xmm5
	movq	%rax, %xmm6
	movq	72(%rbx), %rdx
	movq	88(%rbx), %rax
	movq	48(%rbx), %xmm0
	punpcklqdq	%xmm5, %xmm1
	movups	%xmm4, 48(%rbx)
	movdqu	152(%rbx), %xmm3
	movq	%rdx, %xmm4
	movq	%rax, %xmm5
	movq	136(%rbx), %rdx
	movq	16(%rbx), %rax
	punpcklqdq	%xmm6, %xmm0
	movdqu	184(%rbx), %xmm7
	movups	%xmm1, 152(%rbx)
	movq	64(%rbx), %xmm1
	movq	%rdx, 16(%rbx)
	movq	144(%rbx), %rdx
	movq	%rax, 136(%rbx)
	movq	24(%rbx), %rax
	punpcklqdq	%xmm4, %xmm1
	movups	%xmm0, 168(%rbx)
	movq	80(%rbx), %xmm0
	movq	%rax, 144(%rbx)
	movups	%xmm3, 32(%rbx)
	movdqu	200(%rbx), %xmm3
	punpcklqdq	%xmm5, %xmm0
	movq	%rdx, 24(%rbx)
	movups	%xmm7, 64(%rbx)
	movups	%xmm3, 80(%rbx)
	movups	%xmm1, 184(%rbx)
	movups	%xmm0, 200(%rbx)
	call	uv_mutex_unlock@PLT
	movq	152(%rbx), %rax
	cmpq	%rax, 184(%rbx)
	jne	.L95
	movb	$0, 216(%rbx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L103
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L103:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8321:
	.size	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv.part.0, .-_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv.part.0
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED2Ev, @function
_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED2Ev:
.LFB8105:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L104
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L106
	movq	(%rdi), %rax
	call	*8(%rax)
.L106:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L104:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8105:
	.size	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED2Ev, .-_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED1Ev,_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED0Ev, @function
_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED0Ev:
.LFB8107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	8(%rdi), %r13
	movq	%rdi, %r12
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L112
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L113
	movq	(%rdi), %rax
	call	*8(%rax)
.L113:
	movl	$24, %esi
	movq	%r13, %rdi
	call	_ZdlPvm@PLT
.L112:
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8107:
	.size	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED0Ev, .-_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED0Ev
	.section	.text._ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,"axG",@progbits,_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.type	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, @function
_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv:
.LFB7941:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	40(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	cmpq	$0, 32(%rbx)
	jne	.L129
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	movq	%r12, %rdi
	call	uv_mutex_destroy@PLT
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L121
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L125
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L130
.L121:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L121
.L130:
	movq	(%rdi), %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	movq	24(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	leaq	_ZZN4node9inspector16MainThreadHandleD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7941:
	.size	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv, .-_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD2Ev:
.LFB6424:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskE(%rip), %rax
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L131
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L134
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L137
.L131:
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L131
.L137:
	movq	(%rdi), %rax
	jmp	*24(%rax)
	.cfi_endproc
.LFE6424:
	.size	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD2Ev, .-_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD1Ev,_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD0Ev:
.LFB6426:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L140
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L141
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L145
.L140:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$24, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L140
.L145:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L140
	.cfi_endproc
.LFE6426:
	.size	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD0Ev, .-_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEE4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEE4CallEPNS0_19MainThreadInterfaceE:
.LFB7950:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	movq	320(%rsi), %r8
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%r8
	movq	312(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L147
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movl	8(%rcx), %esi
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L164:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L147
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L147
.L149:
	cmpl	%esi, %r9d
	jne	.L164
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L147
	movq	8(%rax), %r8
	movq	40(%rdi), %rdx
	leaq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	32(%rdi), %rsi
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L147:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector19MainThreadInterface9GetObjectEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7950:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEE4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEE4CallEPNS0_19MainThreadInterfaceE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEE4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEE4CallEPNS0_19MainThreadInterfaceE:
.LFB7951:
	.cfi_startproc
	endbr64
	movslq	8(%rdi), %rax
	movq	320(%rsi), %r8
	xorl	%edx, %edx
	movq	%rax, %r9
	divq	%r8
	movq	312(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L166
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movl	8(%rcx), %esi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L183:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L166
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L166
.L168:
	cmpl	%esi, %r9d
	jne	.L183
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L166
	movq	8(%rax), %r8
	movq	40(%rdi), %rdx
	leaq	24(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	32(%rdi), %rsi
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L166:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector19MainThreadInterface9GetObjectEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7951:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEE4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEE4CallEPNS0_19MainThreadInterfaceE
	.p2align 4
	.type	_ZZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EEENUlPN2v87IsolateEPvE_4_FUNES9_SA_, @function
_ZZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EEENUlPN2v87IsolateEPvE_4_FUNES9_SA_:
.LFB5147:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	8(%rsi), %r13
	testq	%r13, %r13
	je	.L199
	movl	8(%r13), %eax
	leaq	8(%r13), %rbx
.L189:
	testl	%eax, %eax
	je	.L192
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L189
	movl	8(%r13), %eax
	testl	%eax, %eax
	je	.L190
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L190
	cmpb	$0, 216(%rdi)
	jne	.L190
	call	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv.part.0
.L190:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	testq	%r14, %r14
	je	.L214
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L215
.L192:
	movq	8(%r12), %rdi
.L187:
	testq	%rdi, %rdi
	je	.L199
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L196
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L197:
	cmpl	$1, %eax
	jne	.L199
	movq	(%rdi), %rax
	call	*24(%rax)
.L199:
	popq	%rbx
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L192
.L215:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L193
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L194:
	cmpl	$1, %eax
	jne	.L192
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	8(%r12), %rdi
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L196:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L193:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L194
	.cfi_endproc
.LFE5147:
	.size	_ZZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EEENUlPN2v87IsolateEPvE_4_FUNES9_SA_, .-_ZZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EEENUlPN2v87IsolateEPvE_4_FUNES9_SA_
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTask3RunEv, @function
_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTask3RunEv:
.LFB5025:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L216
	movl	8(%r12), %eax
	leaq	8(%r12), %rbx
.L219:
	testl	%eax, %eax
	je	.L216
	leal	1(%rax), %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L219
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L220
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L220
	cmpb	$0, 216(%rdi)
	jne	.L220
	call	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv.part.0
.L220:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L242
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L243
.L216:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L216
.L243:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L224
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L225:
	cmpl	$1, %eax
	jne	.L216
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L224:
	.cfi_restore_state
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L225
	.cfi_endproc
.LFE5025:
	.size	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTask3RunEv, .-_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTask3RunEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv
	.type	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv, @function
_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv:
.LFB5149:
	.cfi_startproc
	endbr64
	movb	$0, 216(%rdi)
	movq	184(%rdi), %rax
	cmpq	%rax, 152(%rdi)
	je	.L255
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	96(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	leaq	224(%rbx), %r13
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	movq	32(%rbx), %rax
	cmpq	%rax, 64(%rbx)
	jne	.L248
	.p2align 4,,10
	.p2align 3
.L247:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	uv_cond_wait@PLT
	movq	32(%rbx), %rax
	cmpq	%rax, 64(%rbx)
	je	.L247
.L248:
	movq	%r12, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5149:
	.size	_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv, .-_ZN4node9inspector19MainThreadInterface20WaitForFrontendEventEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv
	.type	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv, @function
_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv:
.LFB5150:
	.cfi_startproc
	endbr64
	cmpb	$0, 216(%rdi)
	jne	.L256
	jmp	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv.part.0
	.p2align 4,,10
	.p2align 3
.L256:
	ret
	.cfi_endproc
.LFE5150:
	.size	_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv, .-_ZN4node9inspector19MainThreadInterface16DispatchMessagesEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface9GetHandleEv
	.type	_ZN4node9inspector19MainThreadInterface9GetHandleEv, @function
_ZN4node9inspector19MainThreadInterface9GetHandleEv:
.LFB5176:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	296(%rsi), %r13
	testq	%r13, %r13
	je	.L295
.L259:
	movq	304(%rbx), %rax
	movq	%r13, (%r12)
	movq	%rax, 8(%r12)
	testq	%rax, %rax
	je	.L258
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L277
	lock addl	$1, 8(%rax)
.L258:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L277:
	.cfi_restore_state
	addl	$1, 8(%rax)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$88, %edi
	call	_Znwm@PLT
	pxor	%xmm0, %xmm0
	movq	%rax, %r14
	leaq	16+_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE(%rip), %rax
	movq	%rax, (%r14)
	leaq	40(%r14), %rdi
	leaq	16(%r14), %r15
	movabsq	$4294967297, %rax
	movq	%rax, 8(%r14)
	movq	%rbx, 32(%r14)
	movups	%xmm0, 16(%r14)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L296
	movabsq	$4294967296, %rax
	movq	%r15, %r13
	movq	%rax, 80(%r14)
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L261
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L262
.L261:
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	movq	%r15, 16(%r14)
	testq	%rax, %rax
	je	.L297
	lock addl	$1, 12(%r14)
.L263:
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.L265
	testq	%rax, %rax
	je	.L266
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L267:
	cmpl	$1, %eax
	jne	.L265
	movq	(%rdi), %rax
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r14, 24(%r14)
.L262:
	movq	304(%rbx), %rdi
	movq	%r15, %xmm0
	movq	%r14, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 296(%rbx)
	testq	%rdi, %rdi
	je	.L259
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L270
	movl	$-1, %edx
	lock xaddl	%edx, 8(%rdi)
.L271:
	cmpl	$1, %edx
	je	.L272
.L294:
	movq	296(%rbx), %r13
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L270:
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%rdi)
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L272:
	movq	(%rdi), %rdx
	movq	%rax, -64(%rbp)
	movq	%rdi, -56(%rbp)
	call	*16(%rdx)
	movq	-64(%rbp), %rax
	movq	-56(%rbp), %rdi
	testq	%rax, %rax
	je	.L273
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
.L274:
	cmpl	$1, %eax
	jne	.L294
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L297:
	addl	$1, 12(%r14)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L296:
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L266:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L273:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	jmp	.L274
	.cfi_endproc
.LFE5176:
	.size	_ZN4node9inspector19MainThreadInterface9GetHandleEv, .-_ZN4node9inspector19MainThreadInterface9GetHandleEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface12RemoveObjectEi
	.type	_ZN4node9inspector19MainThreadInterface12RemoveObjectEi, @function
_ZN4node9inspector19MainThreadInterface12RemoveObjectEi:
.LFB5179:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	320(%rdi), %rcx
	movq	%rdi, %rbx
	movq	312(%rdi), %r13
	divq	%rcx
	leaq	0(%r13,%rdx,8), %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L301
	movq	(%rdi), %r12
	movq	%rdx, %r11
	movq	%rdi, %r10
	movl	8(%r12), %r9d
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L322:
	movq	(%r12), %r8
	testq	%r8, %r8
	je	.L301
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%r12, %r10
	movq	%rax, %r9
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L301
	movq	%r8, %r12
.L302:
	cmpl	%esi, %r9d
	jne	.L322
	movq	(%r12), %rsi
	cmpq	%r10, %rdi
	je	.L323
	testq	%rsi, %rsi
	je	.L304
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L304
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r12), %rsi
.L304:
	movq	16(%r12), %rdi
	movq	%rsi, (%r10)
	testq	%rdi, %rdi
	je	.L306
	movq	(%rdi), %rax
	call	*8(%rax)
.L306:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 336(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	leaq	_ZZN4node9inspector19MainThreadInterface12RemoveObjectEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L323:
	testq	%rsi, %rsi
	je	.L309
	movslq	8(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L304
	movq	%r10, 0(%r13,%rdx,8)
	movq	(%r14), %rax
.L303:
	leaq	328(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L324
.L305:
	movq	$0, (%r14)
	movq	(%r12), %rsi
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%r10, %rax
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%rsi, 328(%rbx)
	jmp	.L305
	.cfi_endproc
.LFE5179:
	.size	_ZN4node9inspector19MainThreadInterface12RemoveObjectEi, .-_ZN4node9inspector19MainThreadInterface12RemoveObjectEi
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_113DeleteRequest4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector12_GLOBAL__N_113DeleteRequest4CallEPNS0_19MainThreadInterfaceE:
.LFB5000:
	.cfi_startproc
	endbr64
	movq	%rdi, %r8
	movq	%rsi, %rdi
	movl	8(%r8), %esi
	jmp	_ZN4node9inspector19MainThreadInterface12RemoveObjectEi
	.cfi_endproc
.LFE5000:
	.size	_ZN4node9inspector12_GLOBAL__N_113DeleteRequest4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector12_GLOBAL__N_113DeleteRequest4CallEPNS0_19MainThreadInterfaceE
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface9GetObjectEi
	.type	_ZN4node9inspector19MainThreadInterface9GetObjectEi, @function
_ZN4node9inspector19MainThreadInterface9GetObjectEi:
.LFB5180:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %r8
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%r8
	movq	312(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L327
	movq	(%rax), %rcx
	movq	%rdx, %r9
	movl	8(%rcx), %edi
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L343:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L327
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%r8
	cmpq	%rdx, %r9
	jne	.L327
.L329:
	cmpl	%edi, %esi
	jne	.L343
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L327
	ret
	.p2align 4,,10
	.p2align 3
.L327:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node9inspector19MainThreadInterface9GetObjectEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5180:
	.size	_ZN4node9inspector19MainThreadInterface9GetObjectEi, .-_ZN4node9inspector19MainThreadInterface9GetObjectEi
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface17GetObjectIfExistsEi
	.type	_ZN4node9inspector19MainThreadInterface17GetObjectIfExistsEi, @function
_ZN4node9inspector19MainThreadInterface17GetObjectIfExistsEi:
.LFB5181:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rcx
	movslq	%esi, %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	312(%rdi), %rax
	movq	(%rax,%rdx,8), %r8
	movq	%rdx, %r9
	testq	%r8, %r8
	je	.L344
	movq	(%r8), %r8
	movl	8(%r8), %edi
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L356:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L344
	movslq	8(%r8), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rcx
	cmpq	%rdx, %r9
	jne	.L355
.L347:
	cmpl	%esi, %edi
	jne	.L356
	movq	16(%r8), %r8
.L344:
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L355:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.cfi_endproc
.LFE5181:
	.size	_ZN4node9inspector19MainThreadInterface17GetObjectIfExistsEi, .-_ZN4node9inspector19MainThreadInterface17GetObjectIfExistsEi
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector16MainThreadHandle5ResetEv
	.type	_ZN4node9inspector16MainThreadHandle5ResetEv, @function
_ZN4node9inspector16MainThreadHandle5ResetEv:
.LFB5185:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	uv_mutex_lock@PLT
	movq	$0, 16(%rbx)
	movq	%r12, %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE5185:
	.size	_ZN4node9inspector16MainThreadHandle5ResetEv, .-_ZN4node9inspector16MainThreadHandle5ResetEv
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector16MainThreadHandle7ExpiredEv
	.type	_ZN4node9inspector16MainThreadHandle7ExpiredEv, @function
_ZN4node9inspector16MainThreadHandle7ExpiredEv:
.LFB5187:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	24(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	cmpq	$0, 16(%rbx)
	movq	%r12, %rdi
	sete	%r13b
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5187:
	.size	_ZN4node9inspector16MainThreadHandle7ExpiredEv, .-_ZN4node9inspector16MainThreadHandle7ExpiredEv
	.section	.text._ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm
	.type	_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm, @function
_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm:
.LFB6837:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%rsi, %rdi
	pushq	%r12
	shrq	$6, %rdi
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	1(%rdi), %rbx
	addq	$3, %rdi
	subq	$8, %rsp
	cmpq	$8, %rdi
	ja	.L362
	movq	$8, 8(%r13)
	movl	$64, %edi
.L363:
	call	_Znwm@PLT
	movq	8(%r13), %rdx
	movq	%rax, 0(%r13)
	subq	%rbx, %rdx
	shrq	%rdx
	leaq	(%rax,%rdx,8), %r15
	leaq	(%r15,%rbx,8), %r12
	movq	%r15, %rbx
	cmpq	%r12, %r15
	jnb	.L365
	.p2align 4,,10
	.p2align 3
.L364:
	movl	$512, %edi
	addq	$8, %rbx
	call	_Znwm@PLT
	movq	%rax, -8(%rbx)
	cmpq	%rbx, %r12
	ja	.L364
.L365:
	movq	(%r15), %rdx
	andl	$63, %r14d
	subq	$8, %r12
	movq	%r15, 40(%r13)
	leaq	512(%rdx), %rax
	movq	%rdx, %xmm0
	movq	%rax, 32(%r13)
	movq	(%r12), %rax
	punpcklqdq	%xmm0, %xmm0
	movq	%r12, %xmm2
	movups	%xmm0, 16(%r13)
	leaq	(%rax,%r14,8), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 48(%r13)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 64(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	.cfi_restore_state
	movq	%rdi, 8(%r13)
	salq	$3, %rdi
	jmp	.L363
	.cfi_endproc
.LFE6837:
	.size	_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm, .-_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterfaceC2EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE
	.type	_ZN4node9inspector19MainThreadInterfaceC2EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE, @function
_ZN4node9inspector19MainThreadInterfaceC2EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE:
.LFB5129:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%r8, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	addq	$16, %rdi
	subq	$16, %rsp
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	movq	%rsi, -24(%rbp)
	xorl	%esi, %esi
	movq	%rcx, -32(%rbp)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm
	leaq	96(%rbx), %rdi
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L374
	pxor	%xmm0, %xmm0
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	movq	$0, 136(%rbx)
	movq	$0, 144(%rbx)
	movups	%xmm0, 152(%rbx)
	movups	%xmm0, 168(%rbx)
	movups	%xmm0, 184(%rbx)
	movups	%xmm0, 200(%rbx)
	call	_ZNSt11_Deque_baseISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE17_M_initialize_mapEm
	movb	$0, 216(%rbx)
	leaq	224(%rbx), %rdi
	call	uv_cond_init@PLT
	testl	%eax, %eax
	jne	.L375
	movq	-24(%rbp), %xmm0
	leaq	360(%rbx), %rax
	movq	%r12, 288(%rbx)
	movq	$0, 296(%rbx)
	movhps	-32(%rbp), %xmm0
	movq	$0, 304(%rbx)
	movq	%rax, 312(%rbx)
	movq	$1, 320(%rbx)
	movq	$0, 328(%rbx)
	movq	$0, 336(%rbx)
	movl	$0x3f800000, 344(%rbx)
	movq	$0, 352(%rbx)
	movq	$0, 360(%rbx)
	movups	%xmm0, 272(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L374:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L375:
	leaq	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5129:
	.size	_ZN4node9inspector19MainThreadInterfaceC2EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE, .-_ZN4node9inspector19MainThreadInterfaceC2EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE
	.globl	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE
	.set	_ZN4node9inspector19MainThreadInterfaceC1EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE,_ZN4node9inspector19MainThreadInterfaceC2EPNS0_5AgentEP9uv_loop_sPN2v87IsolateEPNS6_8PlatformE
	.section	.text._ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_
	.type	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_, @function
_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_:
.LFB6840:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rsi), %rdx
	movq	24(%r14), %rax
	leaq	8(%rdx), %r13
	cmpq	%rax, %r13
	jnb	.L377
	.p2align 4,,10
	.p2align 3
.L382:
	movq	0(%r13), %rbx
	leaq	512(%rbx), %r12
	.p2align 4,,10
	.p2align 3
.L381:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L378
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*16(%rax)
	cmpq	%rbx, %r12
	jne	.L381
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L382
.L399:
	movq	24(%r15), %rdx
.L377:
	movq	(%r15), %rbx
	cmpq	%rax, %rdx
	je	.L383
	movq	16(%r15), %r12
	cmpq	%r12, %rbx
	je	.L388
	.p2align 4,,10
	.p2align 3
.L384:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L387
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*16(%rax)
	cmpq	%rbx, %r12
	jne	.L384
.L388:
	movq	(%r14), %r12
	movq	8(%r14), %rbx
	cmpq	%rbx, %r12
	je	.L376
	.p2align 4,,10
	.p2align 3
.L386:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L390
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*16(%rax)
	cmpq	%rbx, %r12
	jne	.L386
.L376:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L381
	movq	24(%r14), %rax
	addq	$8, %r13
	cmpq	%r13, %rax
	ja	.L382
	jmp	.L399
	.p2align 4,,10
	.p2align 3
.L390:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L386
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	.cfi_restore_state
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L384
	jmp	.L388
.L383:
	movq	(%r14), %r12
	cmpq	%r12, %rbx
	je	.L376
.L394:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L392
.L400:
	movq	(%rdi), %rax
	addq	$8, %rbx
	call	*16(%rax)
	cmpq	%rbx, %r12
	je	.L376
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L400
.L392:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L394
	jmp	.L376
	.cfi_endproc
.LFE6840:
	.size	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_, .-_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterfaceD2Ev
	.type	_ZN4node9inspector19MainThreadInterfaceD2Ev, @function
_ZN4node9inspector19MainThreadInterfaceD2Ev:
.LFB5132:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	296(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L402
	leaq	24(%r12), %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	%r13, %rdi
	movq	$0, 16(%r12)
	call	uv_mutex_unlock@PLT
.L402:
	movq	328(%rbx), %r12
	testq	%r12, %r12
	jne	.L403
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L448:
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	je	.L407
.L408:
	movq	%r13, %r12
.L403:
	movq	16(%r12), %rdi
	movq	(%r12), %r13
	testq	%rdi, %rdi
	jne	.L448
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%r13, %r13
	jne	.L408
.L407:
	movq	320(%rbx), %rax
	movq	312(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	312(%rbx), %rdi
	leaq	360(%rbx), %rax
	movq	$0, 336(%rbx)
	movq	$0, 328(%rbx)
	cmpq	%rax, %rdi
	je	.L404
	call	_ZdlPv@PLT
.L404:
	movq	304(%rbx), %r12
	testq	%r12, %r12
	je	.L410
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L411
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L449
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	224(%rbx), %rdi
	leaq	-128(%rbp), %r15
	call	uv_cond_destroy@PLT
	leaq	-96(%rbp), %r14
	movq	%r15, %rdx
	movdqu	184(%rbx), %xmm0
	movdqu	200(%rbx), %xmm1
	movdqu	152(%rbx), %xmm2
	movq	%r14, %rsi
	leaq	136(%rbx), %rdi
	movdqu	168(%rbx), %xmm3
	movaps	%xmm0, -128(%rbp)
	movaps	%xmm1, -112(%rbp)
	movaps	%xmm2, -96(%rbp)
	movaps	%xmm3, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L416
	movq	208(%rbx), %rax
	movq	176(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L417
	.p2align 4,,10
	.p2align 3
.L418:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L418
	movq	136(%rbx), %rdi
.L417:
	call	_ZdlPv@PLT
.L416:
	leaq	96(%rbx), %rdi
	call	uv_mutex_destroy@PLT
	movdqu	64(%rbx), %xmm4
	movdqu	80(%rbx), %xmm5
	leaq	16(%rbx), %rdi
	movdqu	32(%rbx), %xmm6
	movdqu	48(%rbx), %xmm7
	movq	%r15, %rdx
	movq	%r14, %rsi
	movaps	%xmm4, -128(%rbp)
	movaps	%xmm5, -112(%rbp)
	movaps	%xmm6, -96(%rbp)
	movaps	%xmm7, -80(%rbp)
	call	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE19_M_destroy_data_auxESt15_Deque_iteratorIS6_RS6_PS6_ESC_
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L419
	movq	88(%rbx), %rax
	movq	56(%rbx), %r12
	leaq	8(%rax), %r13
	cmpq	%r12, %r13
	jbe	.L420
	.p2align 4,,10
	.p2align 3
.L421:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	_ZdlPv@PLT
	cmpq	%r12, %r13
	ja	.L421
	movq	16(%rbx), %rdi
.L420:
	call	_ZdlPv@PLT
.L419:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L401
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L424
	movl	$-1, %eax
	lock xaddl	%eax, 12(%rdi)
	cmpl	$1, %eax
	je	.L450
	.p2align 4,,10
	.p2align 3
.L401:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L451
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L411:
	.cfi_restore_state
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L410
.L449:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L414
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L415:
	cmpl	$1, %eax
	jne	.L410
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L424:
	movl	12(%rdi), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%rdi)
	cmpl	$1, %eax
	jne	.L401
.L450:
	movq	(%rdi), %rax
	call	*24(%rax)
	jmp	.L401
	.p2align 4,,10
	.p2align 3
.L414:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L415
.L451:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5132:
	.size	_ZN4node9inspector19MainThreadInterfaceD2Ev, .-_ZN4node9inspector19MainThreadInterfaceD2Ev
	.globl	_ZN4node9inspector19MainThreadInterfaceD1Ev
	.set	_ZN4node9inspector19MainThreadInterfaceD1Ev,_ZN4node9inspector19MainThreadInterfaceD2Ev
	.section	.rodata._ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_
	.type	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_, @function
_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_:
.LFB6846:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L461
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L462
.L454:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	$0, (%r12)
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L462:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L463
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L464
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L459
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L459:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L457:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L454
	.p2align 4,,10
	.p2align 3
.L463:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L456
	cmpq	%r14, %rsi
	je	.L457
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L456:
	cmpq	%r14, %rsi
	je	.L457
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L457
.L461:
	leaq	.LC1(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L464:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6846:
	.size	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_, .-_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB2:
	.text
.LHOTB2:
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	.type	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE, @function
_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE:
.LFB5134:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	96(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r14, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	80(%rbx), %rax
	movq	64(%rbx), %r13
	movq	32(%rbx), %r15
	subq	$8, %rax
	cmpq	%rax, %r13
	je	.L466
	movq	(%r12), %rax
	movq	$0, (%r12)
	movq	%rax, 0(%r13)
	leaq	8(%r13), %rax
	movq	%rax, 64(%rbx)
.L467:
	cmpq	%r13, %r15
	je	.L518
.L468:
	leaq	224(%rbx), %rdi
	call	uv_cond_broadcast@PLT
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L519
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L518:
	.cfi_restore_state
	movq	280(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L468
	movq	288(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L468
	movq	(%rsi), %rax
	leaq	-80(%rbp), %rdi
	call	*48(%rax)
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L469
	movl	8(%r12), %eax
	leaq	8(%r12), %rdx
.L471:
	testl	%eax, %eax
	je	.L469
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L471
	movq	(%rbx), %r15
	movl	$16, %edi
	movq	%rdx, -104(%rbp)
	call	_Znwm@PLT
	movq	%r12, %xmm1
	leaq	12(%r12), %rcx
	movq	%r15, %xmm0
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r15
	movq	%rax, %r13
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%rax)
	testq	%r15, %r15
	je	.L472
	lock addl	$1, (%rcx)
	movl	$-1, %eax
	movq	-104(%rbp), %rdx
	lock xaddl	%eax, (%rdx)
.L475:
	cmpl	$1, %eax
	je	.L520
.L477:
	movq	-80(%rbp), %r12
	movq	8(%r13), %r8
	movq	0(%r13), %rcx
	movq	(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, -104(%rbp)
	testq	%r8, %r8
	je	.L481
	leaq	12(%r8), %rdx
	testq	%r15, %r15
	je	.L482
	lock addl	$1, (%rdx)
.L483:
	movl	$24, %edi
	movq	%rdx, -128(%rbp)
	movq	%r8, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %r8
	testq	%r15, %r15
	movq	-128(%rbp), %rdx
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	-112(%rbp), %rcx
	movq	%r8, 16(%rax)
	movq	%rcx, 8(%rax)
	je	.L521
	lock addl	$1, (%rdx)
	movl	$-1, %ecx
	lock xaddl	%ecx, (%rdx)
	movl	%ecx, %edx
.L485:
	cmpl	$1, %edx
	jne	.L486
	movq	(%r8), %rdx
	movq	%rax, -112(%rbp)
	movq	%r8, %rdi
	call	*24(%rdx)
	movq	-112(%rbp), %rax
.L486:
	movq	%rax, -88(%rbp)
	movq	%r12, %rdi
	movq	-104(%rbp), %rax
	leaq	-88(%rbp), %rsi
	call	*%rax
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L487
	movq	(%rdi), %rax
	call	*8(%rax)
.L487:
	movq	280(%rbx), %rdi
	movq	%r13, %rdx
	leaq	_ZZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EEENUlPN2v87IsolateEPvE_4_FUNES9_SA_(%rip), %rsi
	call	_ZN2v87Isolate16RequestInterruptEPFvPS0_PvES2_@PLT
	movq	-72(%rbp), %r12
	testq	%r12, %r12
	je	.L468
	testq	%r15, %r15
	je	.L490
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
.L491:
	cmpl	$1, %eax
	jne	.L468
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L493
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L494:
	cmpl	$1, %eax
	jne	.L468
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L472:
	movl	8(%r12), %eax
	addl	$1, 12(%r12)
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L466:
	leaq	16(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZNSt5dequeISt10unique_ptrIN4node9inspector7RequestESt14default_deleteIS3_EESaIS6_EE16_M_push_back_auxIJS6_EEEvDpOT_
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L482:
	addl	$1, 12(%r8)
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L521:
	addl	$1, 12(%r8)
	movl	12(%r8), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 12(%r8)
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L481:
	movl	$24, %edi
	movq	%rcx, -112(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	-112(%rbp), %rcx
	movq	$0, 16(%rax)
	movq	%rcx, 8(%rax)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L490:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	jmp	.L491
	.p2align 4,,10
	.p2align 3
.L520:
	movq	(%r12), %rax
	movq	%rcx, -104(%rbp)
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r15, %r15
	je	.L478
	movq	-104(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
.L479:
	cmpl	$1, %eax
	jne	.L477
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L493:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L494
	.p2align 4,,10
	.p2align 3
.L478:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L479
.L519:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE.cold, @function
_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE.cold:
.LFSB5134:
.L469:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE5134:
	.text
	.size	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE, .-_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	.section	.text.unlikely
	.size	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE.cold, .-_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE.cold
.LCOLDE2:
	.text
.LHOTE2:
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	.type	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE, @function
_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE:
.LFB5184:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	xorl	%r14d, %r14d
	pushq	%r13
	.cfi_offset 13, -32
	leaq	24(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L523
	movq	(%r12), %rax
	movq	$0, (%r12)
	leaq	-48(%rbp), %rsi
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L524
	movq	(%rdi), %rax
	call	*16(%rax)
.L524:
	movl	$1, %r14d
.L523:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L533
	addq	$16, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L533:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5184:
	.size	_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE, .-_ZN4node9inspector16MainThreadHandle4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD2Ev:
.LFB7922:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	8(%rdi), %r14
	movl	24(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_113DeleteRequestE(%rip), %rax
	movl	%r13d, 8(%r12)
	leaq	24(%r14), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L535
	leaq	-48(%rbp), %rsi
	movq	%r12, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L536
	movq	(%rdi), %rax
	call	*16(%rax)
.L536:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L544:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L534
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L539
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L550
	.p2align 4,,10
	.p2align 3
.L534:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L551
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L539:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L534
.L550:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L542
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L543:
	cmpl	$1, %eax
	jne	.L534
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L534
	.p2align 4,,10
	.p2align 3
.L542:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L543
.L551:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7922:
	.size	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD2Ev, .-_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD1Ev,_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD0Ev:
.LFB7920:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	24(%rdi), %rbx
	movl	40(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_113DeleteRequestE(%rip), %rax
	movl	%r14d, 8(%r13)
	leaq	24(%rbx), %r14
	movq	%rax, 0(%r13)
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L553
	leaq	-48(%rbp), %rsi
	movq	%r13, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L554
	movq	(%rdi), %rax
	call	*16(%rax)
.L554:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
.L569:
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L556
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L557
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L576
	.p2align 4,,10
	.p2align 3
.L556:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L563
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L564
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L577
	.p2align 4,,10
	.p2align 3
.L563:
	movl	$48, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L578
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L553:
	.cfi_restore_state
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L557:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L556
.L576:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L560
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L561:
	cmpl	$1, %eax
	jne	.L556
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L564:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L563
.L577:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L567
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L568:
	cmpl	$1, %eax
	jne	.L563
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L560:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L567:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L568
.L578:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7920:
	.size	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD0Ev, .-_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD0Ev, @function
_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD0Ev:
.LFB7924:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -48
	movq	8(%rdi), %rbx
	movl	24(%rdi), %r14d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r13
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_113DeleteRequestE(%rip), %rax
	movl	%r14d, 8(%r13)
	leaq	24(%rbx), %r14
	movq	%rax, 0(%r13)
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L580
	leaq	-48(%rbp), %rsi
	movq	%r13, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L581
	movq	(%rdi), %rax
	call	*16(%rax)
.L581:
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
.L589:
	movq	16(%r12), %r13
	testq	%r13, %r13
	je	.L583
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L584
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L596
	.p2align 4,,10
	.p2align 3
.L583:
	movl	$32, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L597
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	movq	%r14, %rdi
	call	uv_mutex_unlock@PLT
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L584:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L583
.L596:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L587
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L588:
	cmpl	$1, %eax
	jne	.L583
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L587:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L588
.L597:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7924:
	.size	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD0Ev, .-_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD2Ev, @function
_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD2Ev:
.LFB7918:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	24(%rdi), %r14
	movl	40(%rdi), %r13d
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateE(%rip), %rax
	movq	%rax, (%rdi)
	movl	$16, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_113DeleteRequestE(%rip), %rax
	movl	%r13d, 8(%r12)
	leaq	24(%r14), %r13
	movq	%rax, (%r12)
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L599
	leaq	-48(%rbp), %rsi
	movq	%r12, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L600
	movq	(%rdi), %rax
	call	*16(%rax)
.L600:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L615:
	movq	32(%rbx), %r12
	testq	%r12, %r12
	je	.L602
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L603
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L621
	.p2align 4,,10
	.p2align 3
.L602:
	movq	16(%rbx), %r12
	testq	%r12, %r12
	je	.L598
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L610
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L622
	.p2align 4,,10
	.p2align 3
.L598:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L623
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L599:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L603:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L602
.L621:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L606
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L607:
	cmpl	$1, %eax
	jne	.L602
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L610:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L598
.L622:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L613
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L614:
	cmpl	$1, %eax
	jne	.L598
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L606:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L613:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L614
.L623:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7918:
	.size	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD2Ev, .-_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD2Ev
	.set	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD1Ev,_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD2Ev
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE:
.LFB5090:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	movl	$24, %edi
	movq	-56(%rbp), %r13
	movq	24(%rbx), %r14
	movq	$0, -56(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_EE(%rip), %rdx
	movq	%rax, %r12
	movl	40(%rbx), %eax
	movq	%r13, 16(%r12)
	leaq	24(%r14), %r13
	movq	%r13, %rdi
	movq	%rdx, (%r12)
	movl	%eax, 8(%r12)
	call	uv_mutex_lock@PLT
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L625
	leaq	-48(%rbp), %rsi
	movq	%r12, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L626
	movq	(%rdi), %rax
	call	*16(%rax)
.L626:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L628:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L624
	movq	(%rdi), %rax
	call	*8(%rax)
.L624:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L637
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L625:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L628
.L637:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5090:
	.size	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE, .-_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE, @function
_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE:
.LFB5079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	movl	$48, %edi
	movq	-56(%rbp), %r13
	movq	8(%rbx), %r14
	movq	$0, -56(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEEE(%rip), %rdx
	movq	%rax, %r12
	movl	24(%rbx), %eax
	movq	%r13, 24(%r12)
	leaq	24(%r14), %r13
	movl	%eax, 8(%r12)
	leaq	_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEEEvPS3_MS3_FvT_ERSD_(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, 16(%r12)
	leaq	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState8DispatchESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE(%rip), %rax
	movq	%rdx, (%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	call	uv_mutex_lock@PLT
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.L639
	leaq	-48(%rbp), %rsi
	movq	%r12, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L640
	movq	(%rdi), %rax
	call	*16(%rax)
.L640:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L642:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L638
	movq	(%rdi), %rax
	call	*8(%rax)
.L638:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L651
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L642
.L651:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5079:
	.size	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE, .-_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState8DispatchESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE, @function
_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState8DispatchESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE:
.LFB5052:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	16(%rdi), %r13
	movq	(%rsi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	0(%r13), %rax
	movq	16(%rax), %rbx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %rsi
	leaq	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L653
	leaq	-56(%rbp), %rdi
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	movl	$48, %edi
	movq	-56(%rbp), %r14
	movq	8(%r13), %rbx
	movq	$0, -56(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEEE(%rip), %rdx
	movq	%rax, %r12
	movl	24(%r13), %eax
	leaq	24(%rbx), %r13
	movq	%r13, %rdi
	movq	%rdx, (%r12)
	movl	%eax, 8(%r12)
	leaq	_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEEEvPS3_MS3_FvT_ERSD_(%rip), %rax
	movq	%rax, 16(%r12)
	leaq	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState8DispatchESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE(%rip), %rax
	movq	%r14, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L654
	leaq	-48(%rbp), %rsi
	movq	%r12, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L655
	movq	(%rdi), %rax
	call	*16(%rax)
.L655:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
.L658:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L652
	movq	(%rdi), %rax
	call	*8(%rax)
.L652:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L667
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L654:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L653:
	movq	%r13, %rdi
	call	*%rbx
	jmp	.L652
.L667:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5052:
	.size	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState8DispatchESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE, .-_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState8DispatchESt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS5_EE
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_E4CallEPNS0_19MainThreadInterfaceE, @function
_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_E4CallEPNS0_19MainThreadInterfaceE:
.LFB7953:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	320(%rsi), %r8
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	8(%rdi), %rax
	movq	%rax, %r9
	divq	%r8
	movq	312(%rsi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L669
	movq	(%rax), %rcx
	movq	%rdx, %r10
	movl	8(%rcx), %esi
	jmp	.L671
	.p2align 4,,10
	.p2align 3
.L698:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L669
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rsi
	divq	%r8
	cmpq	%rdx, %r10
	jne	.L669
.L671:
	cmpl	%esi, %r9d
	jne	.L698
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L669
	movq	8(%rax), %r13
	movq	16(%rdi), %rdi
	movq	0(%r13), %rax
	movq	16(%rax), %rbx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%rax, %rsi
	leaq	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE(%rip), %rax
	cmpq	%rax, %rbx
	jne	.L672
	leaq	-56(%rbp), %rdi
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	movq	24(%r13), %rbx
	movl	$24, %edi
	movq	-56(%rbp), %r14
	movq	$0, -56(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_EE(%rip), %rdx
	movq	%rax, %r12
	movl	40(%r13), %eax
	leaq	24(%rbx), %r13
	movq	%r13, %rdi
	movq	%rdx, (%r12)
	movl	%eax, 8(%r12)
	movq	%r14, 16(%r12)
	call	uv_mutex_lock@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L673
	leaq	-48(%rbp), %rsi
	movq	%r12, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L674
	movq	(%rdi), %rax
	call	*16(%rax)
.L674:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
.L678:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L668
	movq	(%rdi), %rax
	call	*8(%rax)
.L668:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L699
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L672:
	.cfi_restore_state
	movq	%r13, %rdi
	call	*%rbx
	jmp	.L668
	.p2align 4,,10
	.p2align 3
.L669:
	leaq	_ZZN4node9inspector19MainThreadInterface9GetObjectEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L699:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7953:
	.size	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_E4CallEPNS0_19MainThreadInterfaceE, .-_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_E4CallEPNS0_19MainThreadInterfaceE
	.section	.text.unlikely
	.align 2
.LCOLDB3:
	.text
.LHOTB3:
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.type	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, @function
_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb:
.LFB5183:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -80(%rbp)
	movq	8(%rsi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	addl	$1, 64(%rsi)
	testq	%r12, %r12
	je	.L701
	leaq	8(%r12), %rax
	movl	%ecx, %r15d
	movq	%rax, -72(%rbp)
	movl	8(%r12), %eax
.L703:
	testl	%eax, %eax
	je	.L701
	movq	-72(%rbp), %rbx
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L703
	movq	(%rdx), %rcx
	movq	(%rsi), %r13
	movq	$0, (%rdx)
	movl	$32, %edi
	movq	%rcx, -88(%rbp)
	call	_Znwm@PLT
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r14
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rax, %rbx
	testq	%r14, %r14
	je	.L704
	movq	-72(%rbp), %rax
	lock addl	$1, (%rax)
.L705:
	movl	$1, %eax
	lock xaddl	%eax, 68(%r13)
	addl	$1, %eax
	testq	%r14, %r14
	je	.L706
	movq	-72(%rbp), %rcx
	lock addl	$1, (%rcx)
.L707:
	movq	%r13, %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 8(%rbx)
	testq	%r14, %r14
	je	.L708
	movq	-72(%rbp), %rsi
	lock addl	$1, (%rsi)
	movl	%eax, 24(%rbx)
	testq	%r14, %r14
	je	.L710
.L756:
	movq	-72(%rbp), %rcx
	movl	$-1, %eax
	lock xaddl	%eax, (%rcx)
	cmpl	$1, %eax
	je	.L750
.L713:
	movl	24(%rbx), %edx
	movl	$32, %edi
	movq	8(%rbx), %r13
	movl	%edx, -96(%rbp)
	call	_Znwm@PLT
	movl	-96(%rbp), %edx
	movq	%rax, %r8
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEEE(%rip), %rax
	movq	%rax, (%r8)
	leaq	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState6CreateEPNS0_19MainThreadInterfaceEb(%rip), %rax
	movb	%r15b, 24(%r8)
	leaq	24(%r13), %r15
	movl	%edx, 8(%r8)
	movq	%r15, %rdi
	movq	%rax, 16(%r8)
	movq	%r8, -96(%rbp)
	call	uv_mutex_lock@PLT
	movq	16(%r13), %rdi
	movq	-96(%rbp), %r8
	testq	%rdi, %rdi
	je	.L717
	leaq	-64(%rbp), %rsi
	movq	%r8, -64(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L718
	movq	(%rdi), %rax
	call	*16(%rax)
.L718:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	testq	%r14, %r14
	je	.L719
.L755:
	movq	-72(%rbp), %rsi
	movl	$-1, %eax
	lock xaddl	%eax, (%rsi)
	cmpl	$1, %eax
	je	.L751
.L722:
	movl	$48, %edi
	movq	8(%rbx), %r13
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEEE(%rip), %rcx
	movq	%rax, %r15
	movl	24(%rbx), %eax
	movq	%rcx, (%r15)
	movl	%eax, 8(%r15)
	leaq	_ZN4node9inspector12_GLOBAL__N_128AnotherThreadObjectReferenceINS1_22MainThreadSessionStateEE5ApplyISt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEEEvPS3_MS3_FvT_ERSC_(%rip), %rax
	movq	%rax, 16(%r15)
	movq	-88(%rbp), %rax
	movq	$0, 40(%r15)
	movq	%rax, 24(%r15)
	leaq	_ZN4node9inspector12_GLOBAL__N_122MainThreadSessionState7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS4_EE(%rip), %rax
	movq	%rax, 32(%r15)
	movq	%r13, -88(%rbp)
	addq	$24, %r13
	movq	%r13, %rdi
	call	uv_mutex_lock@PLT
	movq	-88(%rbp), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L726
	leaq	-64(%rbp), %rsi
	movq	%r15, -64(%rbp)
	call	_ZN4node9inspector19MainThreadInterface4PostESt10unique_ptrINS0_7RequestESt14default_deleteIS3_EE
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L727
	movq	(%rdi), %rax
	call	*16(%rax)
.L727:
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	-80(%rbp), %rax
	movq	%rbx, (%rax)
	testq	%r14, %r14
	je	.L728
.L754:
	movq	-72(%rbp), %rbx
	movl	$-1, %eax
	lock xaddl	%eax, (%rbx)
	cmpl	$1, %eax
	je	.L752
.L700:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L753
	movq	-80(%rbp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L726:
	.cfi_restore_state
	movq	%r13, %rdi
	call	uv_mutex_unlock@PLT
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movq	-80(%rbp), %rax
	movq	%rbx, (%rax)
	testq	%r14, %r14
	jne	.L754
.L728:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L700
	jmp	.L752
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%r15, %rdi
	movq	%r8, -96(%rbp)
	call	uv_mutex_unlock@PLT
	movq	-96(%rbp), %r8
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	jne	.L755
.L719:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L722
	jmp	.L751
	.p2align 4,,10
	.p2align 3
.L708:
	addl	$1, 8(%r12)
	movl	%eax, 24(%rbx)
	testq	%r14, %r14
	jne	.L756
.L710:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L713
	jmp	.L750
	.p2align 4,,10
	.p2align 3
.L706:
	addl	$1, 8(%r12)
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L704:
	addl	$1, 8(%r12)
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L750:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L714
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L715:
	cmpl	$1, %eax
	jne	.L713
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L713
	.p2align 4,,10
	.p2align 3
.L752:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L732
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L733:
	cmpl	$1, %eax
	jne	.L700
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L700
	.p2align 4,,10
	.p2align 3
.L751:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r14, %r14
	je	.L723
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L724:
	cmpl	$1, %eax
	jne	.L722
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L714:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L732:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L723:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L724
.L753:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb.cold, @function
_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb.cold:
.LFSB5183:
.L701:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE5183:
	.text
	.size	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb, .-_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb
	.section	.text.unlikely
	.size	_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb.cold, .-_ZN4node9inspector16MainThreadHandle7ConnectESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EEb.cold
.LCOLDE3:
	.text
.LHOTE3:
	.section	.text._ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.type	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, @function
_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm:
.LFB6960:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L758
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L768
.L784:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L769:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L758:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L782
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L783
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L761:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L763
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L765:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L766:
	testq	%rsi, %rsi
	je	.L763
.L764:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movslq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L765
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L771
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L764
	.p2align 4,,10
	.p2align 3
.L763:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L767
	call	_ZdlPv@PLT
.L767:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L784
.L768:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L770
	movslq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L770:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%rdx, %rdi
	jmp	.L766
	.p2align 4,,10
	.p2align 3
.L782:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L761
.L783:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE6960:
	.size	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm, .-_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE
	.type	_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE, @function
_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE:
.LFB5177:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdx), %r8
	testq	%r8, %r8
	je	.L800
	movslq	%esi, %r15
	movl	%esi, %r13d
	movq	320(%rdi), %rsi
	movq	%rdx, %r12
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%rdi, %rbx
	divq	%rsi
	movq	312(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L787
	movq	(%rax), %rcx
	movl	8(%rcx), %edi
	jmp	.L789
	.p2align 4,,10
	.p2align 3
.L801:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L787
	movslq	8(%rcx), %rax
	xorl	%edx, %edx
	movq	%rax, %rdi
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L787
.L789:
	cmpl	%r13d, %edi
	jne	.L801
	addq	$16, %rcx
.L791:
	movq	(%rcx), %rdi
	movq	$0, (%r12)
	movq	%r8, (%rcx)
	testq	%rdi, %rdi
	je	.L785
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L785:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L787:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movl	$1, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	%r13d, 8(%rax)
	movq	%rax, %rcx
	leaq	312(%rbx), %rdi
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIiSt4pairIKiSt10unique_ptrIN4node9inspector9DeletableESt14default_deleteIS5_EEESaIS9_ENSt8__detail10_Select1stESt8equal_toIiESt4hashIiENSB_18_Mod_range_hashingENSB_20_Default_ranged_hashENSB_20_Prime_rehash_policyENSB_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSB_10_Hash_nodeIS9_Lb0EEEm
	movq	(%r12), %r8
	leaq	16(%rax), %rcx
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L800:
	leaq	_ZZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE5177:
	.size	_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE, .-_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE
	.section	.text.unlikely
	.align 2
.LCOLDB4:
	.text
.LHOTB4:
	.align 2
	.p2align 4
	.globl	_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE
	.type	_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE, @function
_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE:
.LFB5186:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	$1, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	lock xaddl	%r13d, 68(%rsi)
	movl	$16, %edi
	movq	16(%rsi), %r12
	movq	(%rdx), %r15
	movq	$0, (%rdx)
	addl	$1, %r13d
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEEE(%rip), %rsi
	movq	%r12, %rdi
	leaq	-64(%rbp), %rdx
	movq	%rsi, (%rax)
	movl	%r13d, %esi
	movq	%r15, 8(%rax)
	movq	%rax, -64(%rbp)
	call	_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L803
	movq	(%rdi), %rax
	call	*8(%rax)
.L803:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.L804
	movl	8(%r12), %eax
	leaq	8(%r12), %r15
.L806:
	testl	%eax, %eax
	je	.L804
	leal	1(%rax), %ecx
	lock cmpxchgl	%ecx, (%r15)
	jne	.L806
	movq	(%rbx), %rax
	movl	$48, %edi
	movq	%rax, -72(%rbp)
	call	_Znwm@PLT
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateE(%rip), %rcx
	movq	%rcx, (%rax)
	movq	%rax, %rbx
	movq	-72(%rbp), %rax
	movq	%r12, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L807
	lock addl	$1, (%r15)
	lock addl	$1, (%r15)
.L810:
	movq	-72(%rbp), %xmm0
	movq	%r12, %xmm1
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 24(%rbx)
	testq	%rax, %rax
	je	.L811
	lock addl	$1, (%r15)
	movl	%r13d, 40(%rbx)
	testq	%rax, %rax
	je	.L813
.L841:
	movl	$-1, %ecx
	lock xaddl	%ecx, (%r15)
	cmpl	$1, %ecx
	je	.L838
.L816:
	movq	%rbx, (%r14)
	testq	%rax, %rax
	je	.L820
	movl	$-1, %ecx
	lock xaddl	%ecx, (%r15)
	movl	%ecx, %edx
	cmpl	$1, %edx
	je	.L839
.L802:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L840
	addq	$40, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L811:
	.cfi_restore_state
	addl	$1, 8(%r12)
	movl	%r13d, 40(%rbx)
	testq	%rax, %rax
	jne	.L841
.L813:
	movl	8(%r12), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 8(%r12)
	cmpl	$1, %ecx
	jne	.L816
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L807:
	addl	$1, 8(%r12)
	addl	$1, 8(%r12)
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L820:
	movl	8(%r12), %edx
	leal	-1(%rdx), %ecx
	movl	%ecx, 8(%r12)
	cmpl	$1, %edx
	jne	.L802
.L839:
	movq	(%r12), %rdx
	movq	%r12, %rdi
	call	*16(%rdx)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L824
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L825:
	cmpl	$1, %eax
	jne	.L802
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L838:
	movq	(%r12), %rcx
	movq	%r12, %rdi
	call	*16(%rcx)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L817
	movl	$-1, %ecx
	lock xaddl	%ecx, 12(%r12)
.L818:
	cmpl	$1, %ecx
	jne	.L816
	movq	(%r12), %rcx
	movq	%r12, %rdi
	call	*24(%rcx)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rax
	jmp	.L816
	.p2align 4,,10
	.p2align 3
.L824:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L817:
	movl	12(%r12), %ecx
	leal	-1(%rcx), %esi
	movl	%esi, 12(%r12)
	jmp	.L818
.L840:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE.cold, @function
_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE.cold:
.LFSB5186:
.L804:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	call	abort@PLT
	.cfi_endproc
.LFE5186:
	.text
	.size	_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE, .-_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE
	.section	.text.unlikely
	.size	_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE.cold, .-_ZN4node9inspector16MainThreadHandle22MakeDelegateThreadSafeESt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS3_EE.cold
.LCOLDE4:
	.text
.LHOTE4:
	.align 2
	.p2align 4
	.type	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEE4CallESA_, @function
_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEE4CallESA_:
.LFB7952:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	leaq	-56(%rbp), %rdi
	subq	$40, %rsp
	movzbl	24(%rbx), %edx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	*16(%rbx)
	movq	-56(%rbp), %r13
	movl	$16, %edi
	movq	$0, -56(%rbp)
	call	_Znwm@PLT
	movl	8(%rbx), %esi
	movq	%r12, %rdi
	leaq	-48(%rbp), %rdx
	leaq	16+_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEEE(%rip), %rcx
	movq	%r13, 8(%rax)
	movq	%rcx, (%rax)
	movq	%rax, -48(%rbp)
	call	_ZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EE
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L843
	movq	(%rdi), %rax
	call	*8(%rax)
.L843:
	movq	-56(%rbp), %r12
	testq	%r12, %r12
	je	.L842
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L845
	movq	(%rdi), %rax
	call	*8(%rax)
.L845:
	movl	$24, %esi
	movq	%r12, %rdi
	call	_ZdlPvm@PLT
.L842:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L857
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L857:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7952:
	.size	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEE4CallESA_, .-_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEE4CallESA_
	.p2align 4
	.globl	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB5182:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-96(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$112, %rsp
	movq	(%rsi), %r8
	movl	8(%rsi), %edx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r8, %rsi
	call	_ZN6icu_6713UnicodeString8fromUTF8ENS_11StringPieceE@PLT
	movzwl	-88(%rbp), %eax
	testw	%ax, %ax
	js	.L859
	movswl	%ax, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	testb	$17, %al
	jne	.L863
.L867:
	leaq	-86(%rbp), %rcx
	testb	$2, %al
	movq	%rcx, %rax
	cmove	-72(%rbp), %rax
.L861:
	leaq	-128(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movb	$0, -128(%rbp)
	call	_ZN12v8_inspector12StringBuffer6createERKNS_10StringViewE@PLT
	movq	%r13, %rdi
	call	_ZN6icu_6713UnicodeStringD1Ev@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L866
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L859:
	.cfi_restore_state
	movl	-84(%rbp), %edx
	movslq	%edx, %rdx
	testb	$17, %al
	je	.L867
.L863:
	xorl	%eax, %eax
	jmp	.L861
.L866:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE5182:
	.size	_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node9inspector16Utf8ToStringViewERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEEE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEEE, 32
_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS1_22MainThreadSessionStateEED0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_113DeleteRequestE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_113DeleteRequestE, 40
_ZTVN4node9inspector12_GLOBAL__N_113DeleteRequestE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_113DeleteRequest4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_113DeleteRequestD0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskE, 40
_ZTVN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTaskD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_120DispatchMessagesTask3RunEv
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionE, 40
_ZTVN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSessionD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_127CrossThreadInspectorSession8DispatchERKN12v8_inspector10StringViewE
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateE, 40
_ZTVN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegateD0Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_118ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewE
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_EE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_EE, 40
_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_E4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS0_24InspectorSessionDelegateEZNS1_18ThreadSafeDelegate21SendMessageToFrontendERKN12v8_inspector10StringViewEEUlPS3_E_ED0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEEE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEEE, 32
_ZTVN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_116DeletableWrapperINS0_24InspectorSessionDelegateEED0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEEE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEEE, 40
_ZTVN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEE4CallESA_
	.quad	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_119CreateObjectRequestISt5_BindIFPFSt10unique_ptrINS1_22MainThreadSessionStateESt14default_deleteIS5_EEPNS0_19MainThreadInterfaceEbESt12_PlaceholderILi1EEbEEED0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEEE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEEE, 40
_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEE4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrINS0_24InspectorSessionDelegateESt14default_deleteIS7_EEERSA_ESt12_PlaceholderILi1EESC_SA_EEED0Ev
	.align 8
	.type	_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEEE, @object
	.size	_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEEE, 40
_ZTVN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEE4CallEPNS0_19MainThreadInterfaceE
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED1Ev
	.quad	_ZN4node9inspector12_GLOBAL__N_111CallRequestINS1_22MainThreadSessionStateESt5_BindIFPFvPS3_MS3_FvSt10unique_ptrIN12v8_inspector12StringBufferESt14default_deleteIS8_EEERSB_ESt12_PlaceholderILi1EESD_SB_EEED0Ev
	.weak	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE
	.section	.data.rel.ro.local._ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,"awG",@progbits,_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE,comdat
	.align 8
	.type	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, @object
	.size	_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE, 56
_ZTVSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE:
	.quad	0
	.quad	0
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED1Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EED0Ev
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_disposeEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE10_M_destroyEv
	.quad	_ZNSt23_Sp_counted_ptr_inplaceIN4node9inspector16MainThreadHandleESaIS2_ELN9__gnu_cxx12_Lock_policyE2EE14_M_get_deleterERKSt9type_info
	.weak	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"../src/node_mutex.h:174"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"(0) == (Traits::cond_init(&cond_))"
	.align 8
.LC7:
	.string	"node::ConditionVariableBase<Traits>::ConditionVariableBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node21ConditionVariableBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC8:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC10:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"../src/inspector/main_thread_interface.cc:304"
	.section	.rodata.str1.1
.LC12:
	.string	"(pointer) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"node::inspector::Deletable* node::inspector::MainThreadInterface::GetObject(int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector19MainThreadInterface9GetObjectEiE4args, @object
	.size	_ZZN4node9inspector19MainThreadInterface9GetObjectEiE4args, 24
_ZZN4node9inspector19MainThreadInterface9GetObjectEiE4args:
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"../src/inspector/main_thread_interface.cc:297"
	.align 8
.LC15:
	.string	"(1) == (managed_objects_.erase(id))"
	.align 8
.LC16:
	.string	"void node::inspector::MainThreadInterface::RemoveObject(int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector19MainThreadInterface12RemoveObjectEiE4args, @object
	.size	_ZZN4node9inspector19MainThreadInterface12RemoveObjectEiE4args, 24
_ZZN4node9inspector19MainThreadInterface12RemoveObjectEiE4args:
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"../src/inspector/main_thread_interface.cc:292"
	.section	.rodata.str1.1
.LC18:
	.string	"(object) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"void node::inspector::MainThreadInterface::AddObject(int, std::unique_ptr<node::inspector::Deletable>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EEE4args, @object
	.size	_ZZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EEE4args, 24
_ZZN4node9inspector19MainThreadInterface9AddObjectEiSt10unique_ptrINS0_9DeletableESt14default_deleteIS3_EEE4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.weak	_ZZN4node9inspector16MainThreadHandleD4EvE4args
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"../src/inspector/main_thread_interface.h:48"
	.section	.rodata.str1.1
.LC21:
	.string	"(main_thread_) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"node::inspector::MainThreadHandle::~MainThreadHandle()"
	.section	.data.rel.ro.local._ZZN4node9inspector16MainThreadHandleD4EvE4args,"awG",@progbits,_ZZN4node9inspector16MainThreadHandleD4EvE4args,comdat
	.align 16
	.type	_ZZN4node9inspector16MainThreadHandleD4EvE4args, @gnu_unique_object
	.size	_ZZN4node9inspector16MainThreadHandleD4EvE4args, 24
_ZZN4node9inspector16MainThreadHandleD4EvE4args:
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.weak	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag
	.section	.rodata._ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,"aG",@progbits,_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag,comdat
	.align 8
	.type	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, @gnu_unique_object
	.size	_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag, 16
_ZZNSt19_Sp_make_shared_tag5_S_tiEvE5__tag:
	.zero	16
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
