	.file	"tls_wrap.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4692:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4692:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4694:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4694:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB6921:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6921:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB6922:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE6922:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB6938:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE6938:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.section	.text._ZNK4node7TLSWrap8SelfSizeEv,"axG",@progbits,_ZNK4node7TLSWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TLSWrap8SelfSizeEv
	.type	_ZNK4node7TLSWrap8SelfSizeEv, @function
_ZNK4node7TLSWrap8SelfSizeEv:
.LFB6955:
	.cfi_startproc
	endbr64
	movl	$472, %eax
	ret
	.cfi_endproc
.LFE6955:
	.size	_ZNK4node7TLSWrap8SelfSizeEv, .-_ZNK4node7TLSWrap8SelfSizeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB8032:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE8032:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB8495:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE8495:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB8496:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE8496:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB8497:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE8497:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB8499:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L13
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE8499:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap12GetAsyncWrapEv
	.type	_ZN4node7TLSWrap12GetAsyncWrapEv, @function
_ZN4node7TLSWrap12GetAsyncWrapEv:
.LFB8783:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE8783:
	.size	_ZN4node7TLSWrap12GetAsyncWrapEv, .-_ZN4node7TLSWrap12GetAsyncWrapEv
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap9IsIPCPipeEv
	.type	_ZN4node7TLSWrap9IsIPCPipeEv, @function
_ZN4node7TLSWrap9IsIPCPipeEv:
.LFB8784:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.cfi_endproc
.LFE8784:
	.size	_ZN4node7TLSWrap9IsIPCPipeEv, .-_ZN4node7TLSWrap9IsIPCPipeEv
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap5GetFDEv
	.type	_ZN4node7TLSWrap5GetFDEv, @function
_ZN4node7TLSWrap5GetFDEv:
.LFB8785:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*104(%rax)
	.cfi_endproc
.LFE8785:
	.size	_ZN4node7TLSWrap5GetFDEv, .-_ZN4node7TLSWrap5GetFDEv
	.align 2
	.p2align 4
	.globl	_ZNK4node7TLSWrap5ErrorEv
	.type	_ZNK4node7TLSWrap5ErrorEv, @function
_ZNK4node7TLSWrap5ErrorEv:
.LFB8790:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 432(%rdi)
	je	.L17
	movq	424(%rdi), %rax
.L17:
	ret
	.cfi_endproc
.LFE8790:
	.size	_ZNK4node7TLSWrap5ErrorEv, .-_ZNK4node7TLSWrap5ErrorEv
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap10ClearErrorEv
	.type	_ZN4node7TLSWrap10ClearErrorEv, @function
_ZN4node7TLSWrap10ClearErrorEv:
.LFB8791:
	.cfi_startproc
	endbr64
	movq	424(%rdi), %rax
	movq	$0, 432(%rdi)
	movb	$0, (%rax)
	ret
	.cfi_endproc
.LFE8791:
	.size	_ZN4node7TLSWrap10ClearErrorEv, .-_ZN4node7TLSWrap10ClearErrorEv
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB8799:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.cfi_endproc
.LFE8799:
	.size	_ZN4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB11877:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE11877:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB11878:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE11878:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB11880:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE11880:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB11881:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE11881:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB8711:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L31:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8711:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB8710:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L37
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L37:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8710:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,"axG",@progbits,_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.type	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, @function
_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_:
.LFB8036:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %rdi
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE8036:
	.size	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, .-_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB11832:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L39
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	ret
	.cfi_endproc
.LFE11832:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB11834:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L42
	call	_ZdlPv@PLT
.L42:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE11834:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE:
.LFB8738:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movl	$96, %edi
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	%rax, %r12
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r12)
	movq	-1(%rdx), %rax
	movq	%rbx, 8(%r12)
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L49
	cmpw	$1040, %cx
	jne	.L45
.L49:
	movq	31(%rdx), %rax
	testq	%rax, %rax
	jne	.L51
.L48:
	movq	%r12, %rdx
	movq	%r13, %rdi
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r12)
	movq	$0, 16(%r12)
	call	uv_buf_init@PLT
	movq	32(%rbx), %rsi
	leaq	40(%r12), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	%rdx, 32(%r12)
	movl	$39, %ecx
	movq	%r13, %rdx
	movq	%rax, 24(%r12)
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r12)
	addq	$72, %rax
	movq	%rax, 40(%r12)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	.cfi_restore_state
	movl	$1, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L48
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8738:
	.size	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE, .-_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap13OnStreamAllocEm
	.type	_ZN4node7TLSWrap13OnStreamAllocEm, @function
_ZN4node7TLSWrap13OnStreamAllocEm:
.LFB8797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	$0, 88(%rdi)
	je	.L56
	movq	344(%rdi), %rdi
	movq	%rsi, -16(%rbp)
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	leaq	-16(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO12PeekWritableEPm@PLT
	movl	-16(%rbp), %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L57
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leaq	_ZZN4node7TLSWrap13OnStreamAllocEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L57:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8797:
	.size	_ZN4node7TLSWrap13OnStreamAllocEm, .-_ZN4node7TLSWrap13OnStreamAllocEm
	.p2align 4
	.type	_ZZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEEENUliiiPKvmP6ssl_stPvE_4_FUNEiiiS8_mSA_SB_, @function
_ZZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEEENUliiiPKvmP6ssl_stPvE_4_FUNEiiiS8_mSA_SB_:
.LFB8806:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	.cfi_offset 12, -48
	movl	%edi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movq	16(%rbp), %rdx
	movq	%r9, -64(%rbp)
	movq	%rdx, -56(%rbp)
	call	ERR_set_mark@PLT
	movq	-56(%rbp), %rdx
	subq	$8, %rsp
	movq	%rbx, %r8
	movq	-64(%rbp), %r9
	movq	%r15, %rcx
	movl	%r13d, %esi
	movl	%r12d, %edi
	pushq	%rdx
	movl	%r14d, %edx
	call	SSL_trace@PLT
	popq	%rax
	popq	%rdx
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	ERR_pop_to_mark@PLT
	.cfi_endproc
.LFE8806:
	.size	_ZZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEEENUliiiPKvmP6ssl_stPvE_4_FUNEiiiS8_mSA_SB_, .-_ZZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEEENUliiiPKvmP6ssl_stPvE_4_FUNEiiiS8_mSA_SB_
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv:
.LFB12101:
	.cfi_startproc
	endbr64
	movl	$96, %eax
	ret
	.cfi_endproc
.LFE12101:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE:
.LFB12104:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE12104:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.text
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap12GetAsyncWrapEv
	.type	_ZThn248_N4node7TLSWrap12GetAsyncWrapEv, @function
_ZThn248_N4node7TLSWrap12GetAsyncWrapEv:
.LFB12105:
	.cfi_startproc
	endbr64
	leaq	-248(%rdi), %rax
	ret
	.cfi_endproc
.LFE12105:
	.size	_ZThn248_N4node7TLSWrap12GetAsyncWrapEv, .-_ZThn248_N4node7TLSWrap12GetAsyncWrapEv
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap7IsAliveEv
	.type	_ZN4node7TLSWrap7IsAliveEv, @function
_ZN4node7TLSWrap7IsAliveEv:
.LFB8786:
	.cfi_startproc
	endbr64
	cmpq	$0, 88(%rdi)
	je	.L63
	leaq	_ZThn248_N4node7TLSWrap7IsAliveEv(%rip), %rdx
.L66:
	movq	320(%rdi), %r8
	testq	%r8, %r8
	je	.L63
	movq	(%r8), %rax
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L65
	cmpq	$0, -160(%r8)
	leaq	-248(%r8), %rdi
	jne	.L66
.L63:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%r8, %rdi
	jmp	*%rax
	.cfi_endproc
.LFE8786:
	.size	_ZN4node7TLSWrap7IsAliveEv, .-_ZN4node7TLSWrap7IsAliveEv
	.set	.LTHUNK10,_ZN4node7TLSWrap7IsAliveEv
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap9IsClosingEv
	.type	_ZN4node7TLSWrap9IsClosingEv, @function
_ZN4node7TLSWrap9IsClosingEv:
.LFB8787:
	.cfi_startproc
	endbr64
	movq	320(%rdi), %rdi
	leaq	_ZThn248_N4node7TLSWrap9IsClosingEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L72
	.p2align 4,,10
	.p2align 3
.L73:
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	je	.L73
.L72:
	jmp	*%rax
	.cfi_endproc
.LFE8787:
	.size	_ZN4node7TLSWrap9IsClosingEv, .-_ZN4node7TLSWrap9IsClosingEv
	.set	.LTHUNK11,_ZN4node7TLSWrap9IsClosingEv
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap10ClearErrorEv
	.type	_ZThn248_N4node7TLSWrap10ClearErrorEv, @function
_ZThn248_N4node7TLSWrap10ClearErrorEv:
.LFB12106:
	.cfi_startproc
	endbr64
	movq	176(%rdi), %rax
	movq	$0, 184(%rdi)
	movb	$0, (%rax)
	ret
	.cfi_endproc
.LFE12106:
	.size	_ZThn248_N4node7TLSWrap10ClearErrorEv, .-_ZThn248_N4node7TLSWrap10ClearErrorEv
	.p2align 4
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0:
.LFB12132:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L77
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L77:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rdx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rdx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L79
.L93:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L93
.L79:
	andl	$4095, %edx
	subq	%rdx, %rsp
	testq	%rdx, %rdx
	jne	.L94
.L80:
	leaq	15(%rsp), %r14
	leaq	16(%rbp), %rax
	movq	%rcx, %r8
	movl	$1, %edx
	andq	$-16, %r14
	movq	%rax, -232(%rbp)
	leaq	-240(%rbp), %r9
	leaq	-208(%rbp), %rax
	movq	%r14, %rdi
	movq	$-1, %rcx
	movl	$32, -240(%rbp)
	movl	$48, -236(%rbp)
	movq	%rax, -224(%rbp)
	call	__vsnprintf_chk@PLT
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L95
	cmpq	$1, %r13
	jne	.L83
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L84:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L96
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L84
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L82:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L94:
	orq	$0, -8(%rsp,%rdx)
	jmp	.L80
.L96:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12132:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	.p2align 4
	.globl	_ZThn248_NK4node7TLSWrap5ErrorEv
	.type	_ZThn248_NK4node7TLSWrap5ErrorEv, @function
_ZThn248_NK4node7TLSWrap5ErrorEv:
.LFB12110:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	cmpq	$0, 184(%rdi)
	je	.L97
	movq	176(%rdi), %rax
.L97:
	ret
	.cfi_endproc
.LFE12110:
	.size	_ZThn248_NK4node7TLSWrap5ErrorEv, .-_ZThn248_NK4node7TLSWrap5ErrorEv
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap5GetFDEv
	.type	_ZThn248_N4node7TLSWrap5GetFDEv, @function
_ZThn248_N4node7TLSWrap5GetFDEv:
.LFB12114:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*104(%rax)
	.cfi_endproc
.LFE12114:
	.size	_ZThn248_N4node7TLSWrap5GetFDEv, .-_ZThn248_N4node7TLSWrap5GetFDEv
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap9IsIPCPipeEv
	.type	_ZThn248_N4node7TLSWrap9IsIPCPipeEv, @function
_ZThn248_N4node7TLSWrap9IsIPCPipeEv:
.LFB12115:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*96(%rax)
	.cfi_endproc
.LFE12115:
	.size	_ZThn248_N4node7TLSWrap9IsIPCPipeEv, .-_ZThn248_N4node7TLSWrap9IsIPCPipeEv
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.type	_ZThn248_N4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, @function
_ZThn248_N4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE:
.LFB12119:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	jmp	*112(%rax)
	.cfi_endproc
.LFE12119:
	.size	_ZThn248_N4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE, .-_ZThn248_N4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.p2align 4
	.globl	_ZThn312_N4node7TLSWrap13OnStreamAllocEm
	.type	_ZThn312_N4node7TLSWrap13OnStreamAllocEm, @function
_ZThn312_N4node7TLSWrap13OnStreamAllocEm:
.LFB12120:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpq	$0, -224(%rdi)
	je	.L107
	movq	32(%rdi), %rdi
	movq	%rsi, -16(%rbp)
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	leaq	-16(%rbp), %rsi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO12PeekWritableEPm@PLT
	movl	-16(%rbp), %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L108
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
	leaq	_ZZN4node7TLSWrap13OnStreamAllocEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L108:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE12120:
	.size	_ZThn312_N4node7TLSWrap13OnStreamAllocEm, .-_ZThn312_N4node7TLSWrap13OnStreamAllocEm
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap9IsClosingEv
	.type	_ZThn248_N4node7TLSWrap9IsClosingEv, @function
_ZThn248_N4node7TLSWrap9IsClosingEv:
.LFB12121:
	.cfi_startproc
	endbr64
	movq	72(%rdi), %rdi
	leaq	_ZThn248_N4node7TLSWrap9IsClosingEv(%rip), %rdx
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%rdx, %rax
	je	.L111
	jmp	*%rax
.L111:
	subq	$248, %rdi
	jmp	.LTHUNK11
	.cfi_endproc
.LFE12121:
	.size	_ZThn248_N4node7TLSWrap9IsClosingEv, .-_ZThn248_N4node7TLSWrap9IsClosingEv
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev:
.LFB12111:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r13
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L112
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L116
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L112:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE12111:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.p2align 4
	.weak	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB12113:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-40(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	-16(%rbx), %r12
	movq	-8(%rbx), %r14
	movq	%rax, -40(%rbx)
	call	uv_buf_init@PLT
	movq	%rax, -16(%rbx)
	movq	%rdx, -8(%rbx)
	testq	%r12, %r12
	je	.L118
	movq	-24(%rbx), %rax
	testq	%rax, %rax
	je	.L124
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L118:
	popq	%rbx
	movq	%r13, %rdi
	popq	%r12
	movl	$96, %esi
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L124:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE12113:
	.size	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev:
.LFB11824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rbx), %r12
	movq	32(%rbx), %r13
	movq	%rax, (%rbx)
	call	uv_buf_init@PLT
	movq	%rax, 24(%rbx)
	movq	%rdx, 32(%rbx)
	testq	%r12, %r12
	je	.L125
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L129
	movq	360(%rax), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L125:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11824:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.set	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED2Ev
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev:
.LFB11826:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$40, %rdi
	subq	$8, %rsp
	movq	%rax, -40(%rdi)
	addq	$72, %rax
	movq	%rax, (%rdi)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%r12), %r13
	movq	32(%r12), %r14
	movq	%rax, (%r12)
	call	uv_buf_init@PLT
	movq	%rax, 24(%r12)
	movq	%rdx, 32(%r12)
	testq	%r13, %r13
	je	.L131
	movq	16(%r12), %rax
	testq	%rax, %rax
	je	.L137
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L131:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L137:
	.cfi_restore_state
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11826:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.text
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap7IsAliveEv
	.type	_ZThn248_N4node7TLSWrap7IsAliveEv, @function
_ZThn248_N4node7TLSWrap7IsAliveEv:
.LFB12124:
	.cfi_startproc
	endbr64
	cmpq	$0, -160(%rdi)
	je	.L138
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L138
	movq	(%rdi), %rax
	leaq	_ZThn248_N4node7TLSWrap7IsAliveEv(%rip), %rdx
	movq	80(%rax), %rax
	cmpq	%rdx, %rax
	je	.L144
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L138:
	xorl	%eax, %eax
	ret
.L144:
	subq	$248, %rdi
	jmp	.LTHUNK10
	.cfi_endproc
.LFE12124:
	.size	_ZThn248_N4node7TLSWrap7IsAliveEv, .-_ZThn248_N4node7TLSWrap7IsAliveEv
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap12EnableCertCbERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap12EnableCertCbERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap12EnableCertCbERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8809:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L153
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L151
	cmpw	$1040, %cx
	jne	.L147
.L151:
	movq	23(%rdx), %rdx
.L149:
	testq	%rdx, %rdx
	je	.L145
	addq	$8, %rsp
	leaq	56(%rdx), %rdi
	leaq	_ZN4node7TLSWrap21OnClientHelloParseEndEPv(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE13WaitForCertCbEPFvPvES4_@PLT
	.p2align 4,,10
	.p2align 3
.L145:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8809:
	.size	_ZN4node7TLSWrap12EnableCertCbERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap12EnableCertCbERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8803:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L163
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L161
	cmpw	$1040, %cx
	jne	.L156
.L161:
	movq	23(%rdx), %rax
.L158:
	testq	%rax, %rax
	je	.L154
	movq	336(%rax), %rax
	testq	%rax, %rax
	je	.L164
	movq	32(%rax), %rdi
	movq	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE14KeylogCallbackEPK6ssl_stPKc@GOTPCREL(%rip), %rsi
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_CTX_set_keylog_callback@PLT
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L154:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	_ZZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8803:
	.size	_ZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8818:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L174
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L172
	cmpw	$1040, %cx
	jne	.L167
.L172:
	movq	23(%rdx), %rbx
.L169:
	testq	%rbx, %rbx
	je	.L165
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L175
	leaq	_ZN4node7TLSWrap17PskServerCallbackEP6ssl_stPKcPhj(%rip), %rsi
	call	SSL_set_psk_server_callback@PLT
	movq	88(%rbx), %rdi
	leaq	_ZN4node7TLSWrap17PskClientCallbackEP6ssl_stPKcPcjPhj(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_set_psk_client_callback@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L165:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	_ZZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8818:
	.size	_ZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8802:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L187
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L185
	cmpw	$1040, %cx
	jne	.L178
.L185:
	movq	23(%rdx), %rbx
.L180:
	testq	%rbx, %rbx
	je	.L176
	cmpq	$0, 88(%rbx)
	je	.L188
	movl	72(%rbx), %ecx
	movb	$1, 96(%rbx)
	testl	%ecx, %ecx
	jne	.L189
.L176:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	.cfi_restore_state
	movq	344(%rbx), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	cmpl	$3, 128(%rbx)
	movq	$16384, 16(%rax)
	jne	.L176
	pxor	%xmm0, %xmm0
	movl	$-1, %eax
	xorl	%edx, %edx
	movq	$0, 176(%rbx)
	movw	%ax, 216(%rbx)
	leaq	_ZN4node7TLSWrap21OnClientHelloParseEndEPv(%rip), %rax
	movups	%xmm0, 160(%rbx)
	movq	%rax, %xmm1
	movq	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE13OnClientHelloEPvRKNS0_17ClientHelloParser11ClientHelloE@GOTPCREL(%rip), %xmm0
	movb	$0, 184(%rbx)
	punpcklqdq	%xmm1, %xmm0
	movw	%dx, 200(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 208(%rbx)
	movl	$0, 128(%rbx)
	movq	%rbx, 152(%rbx)
	movups	%xmm0, 136(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L187:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L188:
	leaq	_ZZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8802:
	.size	_ZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8804:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L205
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L197
	cmpw	$1040, %cx
	jne	.L192
.L197:
	movq	23(%rdx), %rbx
.L194:
	testq	%rbx, %rbx
	je	.L190
	cmpq	$0, 88(%rbx)
	je	.L190
	movq	stderr(%rip), %rdi
	movl	$16, %esi
	call	BIO_new_fp@PLT
	movq	464(%rbx), %rdi
	movq	%rax, 464(%rbx)
	testq	%rdi, %rdi
	je	.L196
	call	BIO_free_all@PLT
.L196:
	movq	88(%rbx), %rdi
	leaq	_ZZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEEENUliiiPKvmP6ssl_stPvE_4_FUNEiiiS8_mSA_SB_(%rip), %rsi
	call	SSL_set_msg_callback@PLT
	movq	88(%rbx), %rdi
	xorl	%edx, %edx
	movl	$16, %esi
	movq	464(%rbx), %rcx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_ctrl@PLT
	.p2align 4,,10
	.p2align 3
.L190:
	.cfi_restore_state
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8804:
	.size	_ZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8823:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %r12
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L220
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L215
	cmpw	$1040, %cx
	jne	.L208
.L215:
	movq	23(%rdx), %rax
.L210:
	testq	%rax, %rax
	je	.L206
	cmpq	$0, 88(%rax)
	je	.L221
	movq	352(%rax), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	BIO_ctrl@PLT
	movq	(%rbx), %rbx
	testl	%eax, %eax
	js	.L213
	salq	$32, %rax
	movq	%rax, 24(%rbx)
.L206:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	$0, 24(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L213:
	movq	8(%rbx), %rdi
	movl	%eax, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	testq	%rax, %rax
	je	.L222
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L222:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L206
	.cfi_endproc
.LFE8823:
	.size	_ZN4node7TLSWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap17PskClientCallbackEP6ssl_stPKcPcjPhj
	.type	_ZN4node7TLSWrap17PskClientCallbackEP6ssl_stPKcPcjPhj, @function
_ZN4node7TLSWrap17PskClientCallbackEP6ssl_stPKcPcjPhj:
.LFB8821:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%r9d, %ebx
	subq	$136, %rsp
	movq	%rdx, -152(%rbp)
	movl	%ecx, -136(%rbp)
	movq	%r8, -160(%rbp)
	movq	%rsi, -144(%rbp)
	xorl	%esi, %esi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_get_ex_data@PLT
	movq	%r15, %rdi
	movq	16(%rax), %r14
	movq	%rax, %r12
	movq	352(%r14), %r13
	movq	%r13, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	104(%r13), %rax
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movl	-136(%rbp), %esi
	movq	%r13, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	-144(%rbp), %r10
	movq	%rax, -64(%rbp)
	testq	%r10, %r10
	je	.L224
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS0_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L231
	movq	%rax, -80(%rbp)
.L224:
	movq	360(%r14), %rax
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	160(%rax), %r8
	testq	%rdi, %rdi
	je	.L227
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L268
.L227:
	movq	3280(%rdx), %rsi
	movq	%r8, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L226
	movq	%rsi, %rdi
	movq	%rsi, -144(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-144(%rbp), %rsi
	testb	%al, %al
	jne	.L269
	movq	16(%r12), %rax
	movq	352(%rax), %r8
	leaq	88(%r8), %r12
.L230:
	testq	%r12, %r12
	je	.L231
	movq	%r12, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L231
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	1432(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L231
	movq	%rax, -144(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	jne	.L270
	.p2align 4,,10
	.p2align 3
.L231:
	xorl	%eax, %eax
.L226:
	movq	%r15, %rdi
	movl	%eax, -136(%rbp)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movl	-136(%rbp), %eax
	jne	.L271
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	movq	352(%rdx), %r9
	movq	(%rdi), %rsi
	movq	%r8, -144(%rbp)
	movq	%r9, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	-144(%rbp), %r8
	movq	%rax, %rdi
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r12, %rdi
	leaq	-80(%rbp), %rcx
	movl	$3, %edx
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%rax, %r12
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L270:
	movq	-144(%rbp), %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	-144(%rbp), %rdi
	movq	%rax, -168(%rbp)
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	cmpq	%rax, %rbx
	movq	%rax, -144(%rbp)
	jb	.L231
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	%r12, %rdi
	movq	872(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-144(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L231
	movq	(%rax), %rax
	movq	%rax, %rsi
	andl	$3, %esi
	cmpq	$1, %rsi
	jne	.L231
	movq	-1(%rax), %rax
	movq	%rcx, -144(%rbp)
	cmpw	$63, 11(%rax)
	ja	.L231
	leaq	-128(%rbp), %r14
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movslq	-120(%rbp), %rdx
	movl	-136(%rbp), %esi
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	jb	.L235
	movq	-128(%rbp), %rsi
	movq	-152(%rbp), %rdi
	call	memcpy@PLT
	movq	-144(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movq	-160(%rbp), %rdi
	movq	%rcx, %rdx
	movq	%rcx, -136(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %rcx
	movl	%ecx, %eax
.L235:
	movq	%r14, %rdi
	movl	%eax, -136(%rbp)
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
	movl	-136(%rbp), %eax
	jmp	.L226
.L271:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8821:
	.size	_ZN4node7TLSWrap17PskClientCallbackEP6ssl_stPKcPcjPhj, .-_ZN4node7TLSWrap17PskClientCallbackEP6ssl_stPKcPcjPhj
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8811:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L284
	cmpw	$1040, %cx
	jne	.L273
.L284:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L290
.L276:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L285
	cmpw	$1040, %cx
	jne	.L277
.L285:
	movq	23(%rdx), %rax
.L279:
	testq	%rax, %rax
	je	.L272
	movq	88(%rax), %rdi
	testq	%rdi, %rdi
	je	.L291
	xorl	%esi, %esi
	call	SSL_get_servername@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L282
	movq	352(%r13), %rdi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	(%rbx), %rbx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L292
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L272:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	120(%rdx), %rdx
	movq	%rdx, 24(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L276
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L277:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L291:
	leaq	_ZZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L292:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L272
	.cfi_endproc
.LFE8811:
	.size	_ZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.p2align 4
	.weak	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB12109:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE12109:
	.size	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev:
.LFB6954:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 22(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movl	$1465076820, 16(%rdi)
	movw	%dx, 20(%rdi)
	movq	$7, 8(%rdi)
	movb	$0, 23(%rdi)
	ret
	.cfi_endproc
.LFE6954:
	.size	_ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev:
.LFB11879:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1466266729, 24(%rdi)
	movq	%rdi, %rax
	movabsq	$8239165559714703699, %rcx
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movq	%rcx, 16(%rdi)
	movw	%dx, 28(%rdi)
	movb	$112, 30(%rdi)
	movq	$15, 8(%rdi)
	movb	$0, 31(%rdi)
	ret
	.cfi_endproc
.LFE11879:
	.size	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev, .-_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8812:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$1064, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L313
	cmpw	$1040, %cx
	jne	.L297
.L313:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L322
.L300:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L314
	cmpw	$1040, %cx
	jne	.L301
.L314:
	movq	23(%rdx), %r12
.L303:
	testq	%r12, %r12
	je	.L296
	cmpl	$1, 16(%rbx)
	jne	.L323
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L306
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L306
	cmpb	$0, 417(%r12)
	jne	.L324
	movl	72(%r12), %eax
	testl	%eax, %eax
	jne	.L325
	cmpq	$0, 88(%r12)
	je	.L326
	movq	352(%r13), %rsi
	leaq	-1088(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	88(%r12), %rdi
	xorl	%edx, %edx
	movq	-1072(%rbp), %rcx
	movl	$55, %esi
	call	SSL_ctrl@PLT
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L296
	testq	%rdi, %rdi
	je	.L296
	call	free@PLT
.L296:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L327
	addq	$1064, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	leaq	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L300
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L301:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L323:
	leaq	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L324:
	leaq	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L325:
	leaq	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L327:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8812:
	.size	_ZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8801:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L350
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L348
	cmpw	$1040, %cx
	jne	.L330
.L348:
	movq	23(%rdx), %r12
.L332:
	testq	%r12, %r12
	je	.L328
	cmpl	$2, 16(%rbx)
	jne	.L351
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L352
	cmpl	$1, 16(%rbx)
	jle	.L353
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L337:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L354
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L355
	xorl	%esi, %esi
	cmpl	$1, 72(%r12)
	je	.L356
.L340:
	movq	_ZN4node6crypto14VerifyCallbackEiP17x509_store_ctx_st@GOTPCREL(%rip), %rdx
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_set_verify@PLT
	.p2align 4,,10
	.p2align 3
.L353:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L337
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L356:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L341
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L342:
	call	_ZNK2v85Value6IsTrueEv@PLT
	testb	%al, %al
	jne	.L343
	movq	88(%r12), %rdi
	xorl	%esi, %esi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L328:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L350:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L343:
	cmpl	$1, 16(%rbx)
	jle	.L357
	movq	8(%rbx), %rdi
	subq	$8, %rdi
.L345:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	88(%r12), %rdi
	movl	$1, %esi
	testb	%al, %al
	je	.L340
	movl	$3, %esi
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L341:
	movq	8(%rbx), %rdi
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L351:
	leaq	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L352:
	leaq	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L355:
	leaq	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L357:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L345
	.cfi_endproc
.LFE8801:
	.size	_ZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap17PskServerCallbackEP6ssl_stPKcPhj
	.type	_ZN4node7TLSWrap17PskServerCallbackEP6ssl_stPKcPhj, @function
_ZN4node7TLSWrap17PskServerCallbackEP6ssl_stPKcPhj:
.LFB8819:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	xorl	%ebx, %ebx
	subq	$136, %rsp
	movq	%rdx, -160(%rbp)
	movl	%ecx, -140(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_get_ex_data@PLT
	movq	%r14, %rdi
	movq	%rax, -136(%rbp)
	movq	16(%rax), %rax
	movq	352(%rax), %r12
	movq	%rax, -152(%rbp)
	movq	%r12, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L359
	leaq	-128(%rbp), %r15
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	%rax, -168(%rbp)
	movq	%r15, %rdi
	call	_ZN2v86String9Utf8ValueC1EPNS_7IsolateENS_5LocalINS_5ValueEEE@PLT
	movq	-128(%rbp), %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L386
.L367:
	xorl	%ebx, %ebx
.L366:
	movq	%r15, %rdi
	call	_ZN2v86String9Utf8ValueD1Ev@PLT
.L359:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L387
	addq	$136, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	movq	-168(%rbp), %rdx
	movl	-140(%rbp), %esi
	movq	%r12, %rdi
	movq	%rdx, -80(%rbp)
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	%rax, -72(%rbp)
	movq	-152(%rbp), %rax
	movq	360(%rax), %rax
	movq	160(%rax), %r13
	movq	-136(%rbp), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %rdx
	testq	%rdi, %rdi
	je	.L361
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L388
.L361:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	xorl	%ebx, %ebx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L366
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L389
	movq	-136(%rbp), %rax
	movq	16(%rax), %rax
	movq	352(%rax), %r13
	addq	$88, %r13
.L364:
	testq	%r13, %r13
	je	.L367
	movq	%r13, %rdi
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L367
	movq	%r13, %rdi
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movl	-140(%rbp), %ecx
	movq	%rax, %rbx
	cmpq	%rax, %rcx
	jb	.L367
	movq	-160(%rbp), %rdi
	movq	%rax, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L388:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	16(%rax), %rdx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-136(%rbp), %rdi
	movq	%r13, %rsi
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	%rax, %r13
	jmp	.L364
.L387:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8819:
	.size	_ZN4node7TLSWrap17PskServerCallbackEP6ssl_stPKcPhj, .-_ZN4node7TLSWrap17PskServerCallbackEP6ssl_stPKcPhj
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB8030:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L391
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L391:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L401
.L392:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L393
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L393:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L401:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L392
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8030:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"ERR_TLS_PSK_SET_IDENTIY_HINT_FAILED"
	.align 8
.LC3:
	.string	"Failed to set PSK identity hint"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"code"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8817:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1096, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L441
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L426
	cmpw	$1040, %cx
	jne	.L404
.L426:
	movq	23(%rdx), %r12
.L406:
	testq	%r12, %r12
	je	.L402
	cmpq	$0, 88(%r12)
	je	.L442
	movq	16(%r12), %r14
	movl	16(%rbx), %eax
	movq	352(%r14), %r13
	testl	%eax, %eax
	jle	.L443
	movq	8(%rbx), %rdx
.L410:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L444
.L411:
	leaq	_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L444:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L411
	leaq	-1104(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	88(%r12), %rdi
	movq	-1088(%rbp), %rsi
	call	SSL_use_psk_identity_hint@PLT
	testl	%eax, %eax
	je	.L445
.L413:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L402
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L402
	call	free@PLT
.L402:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L446
	addq	$1096, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L443:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L404:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L406
	.p2align 4,,10
	.p2align 3
.L445:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L447
.L414:
	movq	%r13, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC3(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L448
.L415:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L449
.L416:
	xorl	%edx, %edx
	movl	$-1, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L450
.L417:
	movq	%r13, %rdi
	movq	%rdx, -1128(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-1128(%rbp), %rdx
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L451
.L418:
	movq	360(%r14), %rax
	movq	8(%r12), %rdi
	movq	%rbx, -1112(%rbp)
	movq	16(%r12), %rdx
	movq	1160(%rax), %r13
	testq	%rdi, %rdi
	je	.L419
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L452
.L419:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L413
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L413
	leaq	-1112(%rbp), %rcx
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L441:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L452:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L451:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L450:
	movq	%rax, -1128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1128(%rbp), %rdx
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L449:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rax, -1128(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1128(%rbp), %rdi
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L447:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L414
.L446:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8817:
	.size	_ZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB12011:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L454
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L455
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L455:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L456
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L457
.L456:
	testq	%rbx, %rbx
	je	.L457
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L457:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L458
	call	__stack_chk_fail@PLT
.L454:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L458:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12011:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata.str1.1
.LC6:
	.string	"0123456789abcdef"
	.section	.text.unlikely
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB12078:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movslq	%esi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC6(%rip), %rax
.L465:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L465
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L466
	call	__stack_chk_fail@PLT
.L466:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12078:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB12084:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC6(%rip), %rax
.L470:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L470
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L471
	call	__stack_chk_fail@PLT
.L471:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12084:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB12090:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC6(%rip), %rax
.L475:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L475
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L476
	call	__stack_chk_fail@PLT
.L476:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12090:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.section	.rodata.str1.1
.LC7:
	.string	"server ("
.LC8:
	.string	"client ("
.LC9:
	.string	"%ld"
.LC10:
	.string	"basic_string::append"
.LC11:
	.string	")"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node7TLSWrap15diagnostic_nameB5cxx11Ev
	.type	_ZNK4node7TLSWrap15diagnostic_nameB5cxx11Ev, @function
_ZNK4node7TLSWrap15diagnostic_nameB5cxx11Ev:
.LFB8782:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$8, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movb	$0, 24(%rdi)
	movq	%rax, (%rdi)
	movabsq	$2337475350324530260, %rax
	movq	%rax, 16(%rdi)
	cmpl	$1, 72(%rsi)
	movq	$8, 8(%rdi)
	je	.L489
	leaq	.LC8(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L481:
	cvttsd2siq	40(%rbx), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %r13
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rcx
	movl	$32, %edx
	movq	%r13, %rdi
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	movabsq	$4611686018427387903, %rax
	cmpq	%rax, -104(%rbp)
	je	.L490
	movl	$1, %edx
	leaq	.LC11(%rip), %rsi
	leaq	-64(%rbp), %rbx
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%rbx, -80(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L491
	movq	%rcx, -80(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -64(%rbp)
.L484:
	movq	8(%rax), %rcx
	movb	$0, 16(%rax)
	movq	%r12, %rdi
	movq	%rcx, -72(%rbp)
	movq	%rdx, (%rax)
	movq	-80(%rbp), %rsi
	movq	$0, 8(%rax)
	movq	-72(%rbp), %rdx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-80(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L485
	call	_ZdlPv@PLT
.L485:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L479
	call	_ZdlPv@PLT
.L479:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L492
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L489:
	.cfi_restore_state
	leaq	.LC7(%rip), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	jmp	.L481
	.p2align 4,,10
	.p2align 3
.L491:
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -64(%rbp)
	jmp	.L484
.L492:
	call	__stack_chk_fail@PLT
.L490:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE8782:
	.size	_ZNK4node7TLSWrap15diagnostic_nameB5cxx11Ev, .-_ZNK4node7TLSWrap15diagnostic_nameB5cxx11Ev
	.section	.rodata.str1.1
.LC12:
	.string	"wrap"
.LC13:
	.string	"HAVE_SSL_TRACE"
.LC15:
	.string	"TLSWrap"
.LC16:
	.string	"receive"
.LC17:
	.string	"start"
.LC18:
	.string	"setVerifyMode"
.LC19:
	.string	"enableSessionCallbacks"
.LC20:
	.string	"enableKeylogCallback"
.LC21:
	.string	"enableTrace"
.LC22:
	.string	"destroySSL"
.LC23:
	.string	"enableCertCb"
.LC24:
	.string	"setPskIdentityHint"
.LC25:
	.string	"enablePskCallback"
.LC26:
	.string	"getServername"
.LC27:
	.string	"setServername"
	.section	.text.unlikely
	.align 2
.LCOLDB28:
	.text
.LHOTB28:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB8825:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L494
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L494
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L494
	movq	271(%rax), %rbx
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	xorl	%ecx, %ecx
	leaq	_ZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L529
.L495:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L530
.L496:
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L531
.L497:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L532
.L498:
	movsd	.LC14(%rip), %xmm0
	movq	%r12, %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r13, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L533
.L499:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r15
	movq	%rax, %r14
	popq	%rax
	testq	%r14, %r14
	je	.L534
.L500:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$3, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	movl	$1, %r9d
	leaq	_ZN4node7TLSWrap17GetWriteQueueSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	360(%rbx), %rax
	movl	$5, %r8d
	movq	1952(%rax), %rsi
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC16(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L535
.L501:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC17(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L536
.L502:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L537
.L503:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC19(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L538
.L504:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC20(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L539
.L505:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap11EnableTraceERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC21(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L540
.L506:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L541
.L507:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap12EnableCertCbERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L542
.L508:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC24(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L543
.L509:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC25(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L544
.L510:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node10StreamBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE10AddMethodsEPNS_11EnvironmentEN2v85LocalINS6_16FunctionTemplateEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC26(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L545
.L511:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC27(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L546
.L512:
	movq	%r12, %rdi
	movq	%rsi, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-56(%rbp), %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L547
	movq	3024(%rbx), %rdi
	movq	352(%rbx), %r15
	testq	%rdi, %rdi
	je	.L517
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3024(%rbx)
.L517:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3024(%rbx)
.L514:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L548
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L530:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L531:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L532:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L498
	.p2align 4,,10
	.p2align 3
.L533:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L534:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L500
	.p2align 4,,10
	.p2align 3
.L535:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L536:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L537:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L538:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L540:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L545:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L547:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	3024(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L514
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3024(%rbx)
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L548:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB8825:
.L494:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE8825:
	.text
	.size	_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE28:
	.text
.LHOTE28:
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED0Ev:
.LFB11321:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L551
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L562
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L563
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L564
.L551:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L557
	movq	(%rdi), %rax
	call	*8(%rax)
.L557:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L564:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L555
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L562:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L555:
	cmpb	$0, 8(%rdx)
	je	.L551
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L551
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L563:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11321:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED0Ev:
.LFB11289:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L567
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L578
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L579
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L580
.L567:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L573
	movq	(%rdi), %rax
	call	*8(%rax)
.L573:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L580:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L571
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L578:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L571:
	cmpb	$0, 8(%rdx)
	je	.L567
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L567
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L579:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11289:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED0Ev
	.section	.text._ZN4node6crypto7SSLWrapINS_7TLSWrapEED0Ev,"axG",@progbits,_ZN4node6crypto7SSLWrapINS_7TLSWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED0Ev
	.type	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED0Ev, @function
_ZN4node6crypto7SSLWrapINS_7TLSWrapEED0Ev:
.LFB9596:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE10DestroySSLEv@PLT
	movq	184(%r12), %rdi
	testq	%rdi, %rdi
	je	.L583
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L602
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L603
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L604
.L583:
	movq	176(%r12), %rdi
	testq	%rdi, %rdi
	je	.L589
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L589:
	movq	32(%r12), %rdi
	testq	%rdi, %rdi
	je	.L590
	call	SSL_free@PLT
.L590:
	movq	24(%r12), %rdi
	testq	%rdi, %rdi
	je	.L591
	call	SSL_SESSION_free@PLT
.L591:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$192, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L604:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L587
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L602:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L587:
	cmpb	$0, 8(%rdx)
	je	.L583
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L583
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L603:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9596:
	.size	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED0Ev, .-_ZN4node6crypto7SSLWrapINS_7TLSWrapEED0Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED0Ev:
.LFB11236:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L607
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L618
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L619
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L620
.L607:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L613
	movq	(%rdi), %rax
	call	*8(%rax)
.L613:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L620:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L611
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L618:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L611:
	cmpb	$0, 8(%rdx)
	je	.L607
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L607
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L619:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11236:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED0Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED2Ev:
.LFB11287:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L623
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L631
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L632
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L623
	cmpb	$0, 9(%rdx)
	je	.L627
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L623:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L621
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L621:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L631:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L627:
	cmpb	$0, 8(%rdx)
	je	.L623
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L623
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L632:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11287:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED2Ev:
.LFB11234:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L635
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L643
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L644
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L635
	cmpb	$0, 9(%rdx)
	je	.L639
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L635:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L633
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L633:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L643:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L639:
	cmpb	$0, 8(%rdx)
	je	.L635
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L635
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L644:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11234:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED2Ev
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED2Ev:
.LFB11319:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L647
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L655
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L656
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L647
	cmpb	$0, 9(%rdx)
	je	.L651
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L647:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L645
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L645:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L651:
	cmpb	$0, 8(%rdx)
	je	.L647
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L647
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L656:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE11319:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED2Ev
	.section	.text._ZN4node6crypto7SSLWrapINS_7TLSWrapEED2Ev,"axG",@progbits,_ZN4node6crypto7SSLWrapINS_7TLSWrapEED5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED2Ev
	.type	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED2Ev, @function
_ZN4node6crypto7SSLWrapINS_7TLSWrapEED2Ev:
.LFB9594:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE10DestroySSLEv@PLT
	movq	184(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L659
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L675
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L676
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L677
.L659:
	movq	176(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L665
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L665:
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L666
	call	SSL_free@PLT
.L666:
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L657
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	SSL_SESSION_free@PLT
	.p2align 4,,10
	.p2align 3
.L657:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	cmpb	$0, 9(%rdx)
	je	.L663
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L675:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L663:
	cmpb	$0, 8(%rdx)
	je	.L659
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L659
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L676:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9594:
	.size	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED2Ev, .-_ZN4node6crypto7SSLWrapINS_7TLSWrapEED2Ev
	.weak	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED1Ev
	.set	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED1Ev,_ZN4node6crypto7SSLWrapINS_7TLSWrapEED2Ev
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4112:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L678
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L682:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L680
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L682
.L678:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L680:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L682
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4112:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB8467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L685
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L714
	cmpq	$1, %rax
	jne	.L688
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L689:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L684:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L715
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L688:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L689
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L714:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L687:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L689
	.p2align 4,,10
	.p2align 3
.L685:
	cmpb	$37, 1(%rax)
	jne	.L716
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L717
	cmpq	$1, %r13
	jne	.L694
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L695:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L697
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L718
.L697:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L699:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L719
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L701:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L702
	call	_ZdlPv@PLT
.L702:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L684
	call	_ZdlPv@PLT
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L694:
	testq	%r13, %r13
	jne	.L720
	movq	%rbx, %rax
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L717:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L693:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L719:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L701
	.p2align 4,,10
	.p2align 3
.L718:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L699
.L716:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L715:
	call	__stack_chk_fail@PLT
.L720:
	movq	%rbx, %rdi
	jmp	.L693
	.cfi_endproc
.LFE8467:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap7InitSSLEv
	.type	_ZN4node7TLSWrap7InitSSLEv, @function
_ZN4node7TLSWrap7InitSSLEv:
.LFB8759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-32(%rbp), %r12
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rsi
	movq	%r12, %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node6crypto7NodeBIO3NewEPNS_11EnvironmentE@PLT
	movq	-32(%rbp), %rax
	movq	16(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rax, 344(%rbx)
	call	_ZN4node6crypto7NodeBIO3NewEPNS_11EnvironmentE@PLT
	movq	-32(%rbp), %rdx
	movq	344(%rbx), %rsi
	movq	88(%rbx), %rdi
	movq	%rdx, 352(%rbx)
	call	SSL_set_bio@PLT
	movq	88(%rbx), %rdi
	movq	_ZN4node6crypto14VerifyCallbackEiP17x509_store_ctx_st@GOTPCREL(%rip), %rdx
	xorl	%esi, %esi
	call	SSL_set_verify@PLT
	movq	88(%rbx), %rdi
	xorl	%ecx, %ecx
	movl	$16, %edx
	movl	$33, %esi
	call	SSL_ctrl@PLT
	movq	88(%rbx), %rdi
	xorl	%ecx, %ecx
	movl	$4, %edx
	movl	$33, %esi
	call	SSL_ctrl@PLT
	movq	88(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	SSL_set_ex_data@PLT
	movq	88(%rbx), %rdi
	leaq	_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii(%rip), %rsi
	call	SSL_set_info_callback@PLT
	cmpl	$1, 72(%rbx)
	je	.L728
.L722:
	movq	336(%rbx), %rdi
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE22ConfigureSecureContextEPNS0_13SecureContextE@PLT
	movq	88(%rbx), %rdi
	movq	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE15SSLCertCallbackEP6ssl_stPv@GOTPCREL(%rip), %rsi
	movq	%rbx, %rdx
	call	SSL_set_cert_cb@PLT
	movl	72(%rbx), %eax
	cmpl	$1, %eax
	je	.L729
	testl	%eax, %eax
	jne	.L725
	movq	344(%rbx), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	88(%rbx), %rdi
	movq	$4096, 16(%rax)
	call	SSL_set_connect_state@PLT
.L721:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L730
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L728:
	.cfi_restore_state
	movq	336(%rbx), %rax
	leaq	_ZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPv(%rip), %rdx
	movl	$53, %esi
	movq	32(%rax), %rdi
	call	SSL_CTX_callback_ctrl@PLT
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L729:
	movq	88(%rbx), %rdi
	call	SSL_set_accept_state@PLT
	jmp	.L721
.L730:
	call	__stack_chk_fail@PLT
.L725:
	call	_ZN4node5AbortEv@PLT
	.cfi_endproc
.LFE8759:
	.size	_ZN4node7TLSWrap7InitSSLEv, .-_ZN4node7TLSWrap7InitSSLEv
	.section	.rodata.str1.1
.LC34:
	.string	"ERR_SSL_"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB8776:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-160(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rcx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$200, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L738
	movl	%r15d, %esi
	call	SSL_get_error@PLT
	movl	%eax, (%r14)
	cmpl	$6, %eax
	ja	.L734
	leaq	.L736(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L736:
	.long	.L738-.L736
	.long	.L737-.L736
	.long	.L738-.L736
	.long	.L738-.L736
	.long	.L738-.L736
	.long	.L737-.L736
	.long	.L735-.L736
	.text
	.p2align 4,,10
	.p2align 3
.L738:
	xorl	%r12d, %r12d
.L733:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L787
	addq	$200, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L737:
	.cfi_restore_state
	call	ERR_peek_error@PLT
	movq	%rax, -184(%rbp)
	call	BIO_s_mem@PLT
	movq	%rax, %rdi
	call	BIO_new@PLT
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	ERR_print_errors@PLT
	xorl	%edx, %edx
	movl	$115, %esi
	movq	%r14, %rdi
	leaq	-176(%rbp), %rcx
	call	BIO_ctrl@PLT
	movq	16(%rbx), %rax
	movq	352(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rax, %r14
	movq	-176(%rbp), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L788
.L739:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	movq	%rax, -216(%rbp)
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -192(%rbp)
	testq	%rax, %rax
	je	.L789
.L740:
	movq	-184(%rbp), %rdi
	call	ERR_lib_error_string@PLT
	movq	-184(%rbp), %rdi
	movq	%rax, -224(%rbp)
	call	ERR_func_error_string@PLT
	movq	-184(%rbp), %rdi
	movq	%rax, -208(%rbp)
	call	ERR_reason_error_string@PLT
	movq	-224(%rbp), %rsi
	movq	%rax, -184(%rbp)
	testq	%rsi, %rsi
	je	.L741
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L790
.L742:
	movq	16(%rbx), %rax
	movq	-192(%rbp), %rdi
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	984(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L791
.L741:
	movq	-208(%rbp), %rax
	testq	%rax, %rax
	je	.L744
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L792
.L745:
	movq	16(%rbx), %rax
	movq	-192(%rbp), %rdi
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	784(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L793
.L744:
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	je	.L747
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L794
.L748:
	movq	16(%rbx), %rax
	movq	-192(%rbp), %rdi
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	1480(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L795
.L749:
	leaq	-128(%rbp), %rax
	movq	-184(%rbp), %rdi
	movq	%rax, -224(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	%rax, -128(%rbp)
	call	strlen@PLT
	movq	%rax, -168(%rbp)
	movq	%rax, %r8
	cmpq	$15, %rax
	ja	.L796
	cmpq	$1, %rax
	jne	.L752
	movq	-184(%rbp), %rcx
	movzbl	(%rcx), %edx
	movb	%dl, -112(%rbp)
	movq	-208(%rbp), %rdx
.L753:
	movq	%rax, -120(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-128(%rbp), %rax
	movq	-120(%rbp), %rsi
	leaq	(%rax,%rsi), %rdi
	cmpq	%rax, %rdi
	je	.L754
	leaq	-1(%rsi), %rcx
	movq	%rax, %rdx
	cmpq	$14, %rcx
	jbe	.L760
	movq	%rsi, %rcx
	movdqa	.LC31(%rip), %xmm7
	pxor	%xmm4, %xmm4
	movdqa	.LC29(%rip), %xmm9
	andq	$-16, %rcx
	movdqa	.LC32(%rip), %xmm6
	movdqa	.LC30(%rip), %xmm8
	movdqa	.LC33(%rip), %xmm5
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L756:
	movdqu	(%rdx), %xmm2
	addq	$16, %rdx
	movdqa	%xmm2, %xmm0
	paddb	%xmm8, %xmm0
	movdqa	%xmm0, %xmm1
	pminub	%xmm7, %xmm1
	pcmpeqb	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	pcmpeqb	%xmm9, %xmm1
	movdqa	%xmm1, %xmm3
	pcmpeqb	%xmm4, %xmm3
	pand	%xmm3, %xmm0
	movdqa	%xmm2, %xmm3
	paddb	%xmm6, %xmm3
	pand	%xmm0, %xmm3
	pandn	%xmm2, %xmm0
	movdqa	%xmm5, %xmm2
	por	%xmm3, %xmm0
	pand	%xmm1, %xmm2
	pandn	%xmm0, %xmm1
	por	%xmm2, %xmm1
	movups	%xmm1, -16(%rdx)
	cmpq	%rdx, %rcx
	jne	.L756
	movq	%rsi, %rdx
	andq	$-16, %rdx
	addq	%rdx, %rax
	cmpq	%rdx, %rsi
	jne	.L760
	.p2align 4,,10
	.p2align 3
.L757:
	movq	-120(%rbp), %rsi
.L754:
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %rdi
	addq	$8, %rsi
	movb	$0, -80(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -96(%rbp)
	movq	%rdi, -224(%rbp)
	movq	$0, -88(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7reserveEm@PLT
	movq	-224(%rbp), %rdi
	movabsq	$4611686018427387903, %rax
	subq	-88(%rbp), %rax
	cmpq	$7, %rax
	jbe	.L797
	movl	$8, %edx
	leaq	.LC34(%rip), %rsi
	movq	%rdi, -224(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-120(%rbp), %rdx
	movq	-128(%rbp), %rsi
	movq	-224(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-96(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L798
.L764:
	movq	16(%rbx), %rax
	movq	-192(%rbp), %rdi
	movq	%r14, %rsi
	movq	360(%rax), %rax
	movq	328(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L799
.L765:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L766
	call	_ZdlPv@PLT
.L766:
	movq	-128(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L747
	call	_ZdlPv@PLT
.L747:
	testq	%r12, %r12
	je	.L768
	movq	-176(%rbp), %rax
	movq	8(%r12), %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	8(%rax), %rcx
	movq	(%rax), %r8
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
.L768:
	movq	-200(%rbp), %rdi
	call	BIO_free_all@PLT
	movq	-216(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L735:
	movq	16(%rbx), %rax
	movq	%r13, %rdi
	movq	360(%rax), %rax
	movq	1968(%rax), %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L785:
	leal	-97(%rdx), %esi
	leal	-32(%rdx), %ecx
	cmpb	$26, %sil
	cmovb	%ecx, %edx
	movb	%dl, (%rax)
.L761:
	addq	$1, %rax
	cmpq	%rdi, %rax
	je	.L757
.L760:
	movzbl	(%rax), %edx
	cmpb	$32, %dl
	jne	.L785
	movb	$95, (%rax)
	jmp	.L761
	.p2align 4,,10
	.p2align 3
.L752:
	testq	%rax, %rax
	jne	.L800
	movq	-208(%rbp), %rdx
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L796:
	movq	-224(%rbp), %rdi
	leaq	-168(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rax, -232(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-232(%rbp), %r8
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-168(%rbp), %rax
	movq	%rax, -112(%rbp)
.L751:
	movq	-184(%rbp), %rsi
	movq	%r8, %rdx
	call	memcpy@PLT
	movq	-168(%rbp), %rax
	movq	-128(%rbp), %rdx
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L788:
	movq	%rax, -192(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-192(%rbp), %rdi
	jmp	.L739
	.p2align 4,,10
	.p2align 3
.L789:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L740
.L794:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rcx
	jmp	.L748
.L793:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L744
.L795:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L749
.L790:
	movq	%rax, -224(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-224(%rbp), %rcx
	jmp	.L742
.L792:
	movq	%rax, -208(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-208(%rbp), %rcx
	jmp	.L745
.L791:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L741
.L798:
	movq	%rax, -224(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-224(%rbp), %rcx
	jmp	.L764
.L799:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L765
.L787:
	call	__stack_chk_fail@PLT
.L734:
	leaq	_ZZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L797:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L800:
	movq	-208(%rbp), %rdi
	jmp	.L751
	.cfi_endproc
.LFE8776:
	.size	_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.p2align 4
	.globl	_Z18_register_tls_wrapv
	.type	_Z18_register_tls_wrapv, @function
_Z18_register_tls_wrapv:
.LFB8826:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE8826:
	.size	_Z18_register_tls_wrapv, .-_Z18_register_tls_wrapv
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB9335:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L809
	movq	16(%rdi), %r10
.L803:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L804
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L810
	movq	16(%r9), %r10
.L805:
	cmpq	%r10, %rax
	jbe	.L812
.L804:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L806:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L813
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L808:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L812:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L813:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L809:
	movl	$15, %r10d
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L810:
	movl	$15, %r10d
	jmp	.L805
	.cfi_endproc
.LFE9335:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB9569:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L831
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L832
.L817:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L821
.L814:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L832:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L822
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L818:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L817
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L823
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L819:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L821:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L814
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L831:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L822:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L818
	.p2align 4,,10
	.p2align 3
.L823:
	xorl	%edx, %edx
	jmp	.L819
	.cfi_endproc
.LFE9569:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB8741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L834
	leaq	16(%r12), %rsi
.L835:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L836
	addq	$16, %r12
.L837:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L838
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L854
.L838:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L855
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L842
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L856
.L833:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L857
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L854:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L836:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L837
	.p2align 4,,10
	.p2align 3
.L834:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L856:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L833
	.p2align 4,,10
	.p2align 3
.L855:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L845
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L840:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L842:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L845:
	xorl	%edx, %edx
	jmp	.L840
.L857:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8741:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB8743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r12, %rsi
	.cfi_offset 3, -32
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L859
	leaq	40(%r12), %rsi
.L860:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L861
	addq	$40, %r12
.L862:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L863
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L879
.L863:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L880
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L867
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	je	.L881
.L858:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L882
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L879:
	.cfi_restore_state
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L863
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L859:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L881:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L880:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L870
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L865:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L867:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L870:
	xorl	%edx, %edx
	jmp	.L865
.L882:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8743:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.section	.text._ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC2EPS1_:
.LFB9646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L900
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L901
.L886:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L890
.L883:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L901:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L891
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L887:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L886
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L892
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L888:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L890:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L883
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L900:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L891:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L892:
	xorl	%edx, %edx
	jmp	.L888
	.cfi_endproc
.LFE9646:
	.size	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC2EPS1_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_:
.LFB9671:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	movq	%rdx, %rdi
	subq	$8, %rsp
	call	strlen@PLT
	movq	%rax, %rdx
	movabsq	$4611686018427387903, %rax
	subq	8(%r13), %rax
	cmpq	%rax, %rdx
	ja	.L907
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L908
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L905:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L908:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L905
.L907:
	leaq	.LC10(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE9671:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	.section	.text._ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC2EPS2_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC5EPS2_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC2EPS2_
	.type	_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC2EPS2_, @function
_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC2EPS2_:
.LFB9693:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L926
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L927
.L912:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L916
.L909:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L927:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L917
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L913:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L912
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L918
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L914:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L916:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L909
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L926:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L917:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L918:
	xorl	%edx, %edx
	jmp	.L914
	.cfi_endproc
.LFE9693:
	.size	_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC2EPS2_, .-_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC2EPS2_
	.weak	_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC1EPS2_
	.set	_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC1EPS2_,_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC2EPS2_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPv
	.type	_ZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPv, @function
_ZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPv:
.LFB8816:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	SSL_get_ex_data@PLT
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	16(%rax), %rbx
	movq	%rax, %r12
	call	SSL_get_servername@PLT
	testq	%rax, %rax
	je	.L967
	movq	352(%rbx), %rsi
	leaq	-80(%rbp), %r15
	movq	%rax, %r13
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%r12), %r8
	testq	%r8, %r8
	je	.L930
	movzbl	11(%r8), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L987
.L930:
	movq	%r12, %rdi
	movq	%r8, -112(%rbp)
	call	_ZN4node9AsyncWrap8GetOwnerEv@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	352(%rbx), %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	movq	-112(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L988
.L931:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r9, %rdi
	movq	%r8, -104(%rbp)
	movq	1568(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-104(%rbp), %r8
	testb	%al, %al
	jne	.L932
.L935:
	movl	$3, %r12d
.L933:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L928:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L989
	addq	$88, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L932:
	.cfi_restore_state
	shrw	$8, %ax
	je	.L935
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r8, %rdi
	movq	1632(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L935
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L935
	movq	3224(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v816FunctionTemplate11HasInstanceENS_5LocalINS_5ValueEEE@PLT
	testb	%al, %al
	je	.L990
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L991
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L968
	cmpw	$1040, %cx
	jne	.L942
.L968:
	movq	23(%rdx), %r13
.L944:
	testq	%r13, %r13
	je	.L992
	leaq	-88(%rbp), %rbx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN4node17BaseObjectPtrImplINS_6crypto13SecureContextELb0EEC1EPS2_
	leaq	240(%r12), %rax
	cmpq	%rax, %rbx
	je	.L946
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L948
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L956
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L957
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L948
	cmpb	$0, 9(%rdx)
	je	.L952
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L948:
	movq	-88(%rbp), %rax
	movq	%rax, 240(%r12)
.L954:
	movq	%r13, %rdi
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE22ConfigureSecureContextEPNS0_13SecureContextE@PLT
	movq	32(%r13), %rsi
	movq	88(%r12), %rdi
	call	SSL_set_SSL_CTX@PLT
	cmpq	%rax, 32(%r13)
	jne	.L993
	leaq	56(%r12), %rdi
	movq	%r13, %rsi
	xorl	%r12d, %r12d
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE10SetCACertsEPNS0_13SecureContextE@PLT
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L967:
	xorl	%r12d, %r12d
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L987:
	movq	16(%r12), %rax
	movq	(%r8), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r8
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L988:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rcx
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %r9
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L990:
	movq	360(%rbx), %rax
	movq	1624(%rax), %rdi
	call	_ZN2v89Exception9TypeErrorENS_5LocalINS_6StringEEE@PLT
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -88(%rbp)
	movq	360(%rbx), %rax
	movq	1160(%rax), %r13
	testq	%rdi, %rdi
	je	.L938
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L994
.L938:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L935
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L935
	leaq	-88(%rbp), %rcx
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L935
.L946:
	movq	-88(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L954
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L956
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L957
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L954
	cmpb	$0, 9(%rdx)
	je	.L959
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L954
.L994:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L938
.L991:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L942:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L944
.L992:
	leaq	_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L956:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L959:
	cmpb	$0, 8(%rdx)
	je	.L954
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L954
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L954
.L952:
	cmpb	$0, 8(%rdx)
	je	.L948
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L948
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L948
.L993:
	leaq	_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L957:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L989:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8816:
	.size	_ZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPv, .-_ZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPv
	.section	.text._ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.type	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, @function
_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm:
.LFB10587:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L996
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L1006
.L1022:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L1007:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L996:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L1020
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1021
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L999:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L1001
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1003:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1004:
	testq	%rsi, %rsi
	je	.L1001
.L1002:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L1003
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L1009
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1002
	.p2align 4,,10
	.p2align 3
.L1001:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L1005
	call	_ZdlPv@PLT
.L1005:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L1022
.L1006:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L1008
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L1008:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%rdx, %rdi
	jmp	.L1004
	.p2align 4,,10
	.p2align 3
.L1020:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L999
.L1021:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10587:
	.size	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm, .-_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	.section	.rodata._ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc.str1.1,"aMS",@progbits,1
.LC35:
	.string	"wrapped"
.LC36:
	.string	"wrapper"
	.section	.text._ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc,"axG",@progbits,_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	.type	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc, @function
_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc:
.LFB8524:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	104(%rdi), %rsi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	divq	%rsi
	movq	96(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1024
	movq	(%rax), %rcx
	movq	%rdx, %r8
	movq	8(%rcx), %rdi
	jmp	.L1026
	.p2align 4,,10
	.p2align 3
.L1078:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1024
	movq	8(%rcx), %rdi
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L1024
.L1026:
	cmpq	%rdi, %r12
	jne	.L1078
	movq	16(%rcx), %r15
.L1023:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1079
	addq	$120, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1024:
	.cfi_restore_state
	movl	$72, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rax, (%r15)
	leaq	48(%r15), %rax
	movq	%r12, 8(%r15)
	movq	$0, 16(%r15)
	movb	$0, 24(%r15)
	movq	%rax, -152(%rbp)
	movq	%rax, 32(%r15)
	movq	$0, 40(%r15)
	movb	$0, 48(%r15)
	movq	$0, 64(%r15)
	testq	%r12, %r12
	je	.L1080
	movq	(%rbx), %rsi
	leaq	-128(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*40(%rax)
	testq	%rax, %rax
	je	.L1028
	movq	8(%rbx), %rdi
	leaq	-136(%rbp), %rsi
	movq	(%rdi), %rdx
	movq	(%rdx), %rdx
	movq	%rax, -136(%rbp)
	call	*%rdx
	movq	%rax, 16(%r15)
.L1028:
	movq	8(%r15), %rsi
	leaq	-96(%rbp), %rdi
	movq	(%rsi), %rax
	call	*24(%rax)
	movq	-96(%rbp), %rdx
	leaq	-80(%rbp), %rcx
	movq	32(%r15), %rdi
	cmpq	%rcx, %rdx
	je	.L1081
	movq	-80(%rbp), %rsi
	movq	-88(%rbp), %rax
	cmpq	%rdi, -152(%rbp)
	je	.L1082
	movq	%rax, %xmm0
	movq	%rsi, %xmm1
	movq	48(%r15), %r8
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 40(%r15)
	testq	%rdi, %rdi
	je	.L1034
	movq	%rdi, -96(%rbp)
	movq	%r8, -80(%rbp)
.L1032:
	movq	$0, -88(%rbp)
	movb	$0, (%rdi)
	movq	-96(%rbp), %rdi
	cmpq	%rcx, %rdi
	je	.L1035
	call	_ZdlPv@PLT
.L1035:
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	movq	%r14, %rdi
	movq	%rax, 64(%r15)
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	8(%rbx), %rdi
	movq	%r14, %rsi
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r15, -128(%rbp)
	call	*%rax
	movq	-128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1036
	movq	(%rdi), %rax
	call	*8(%rax)
.L1036:
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L1037
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1039
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1037
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r14
	jne	.L1037
.L1039:
	cmpq	%rsi, %r12
	jne	.L1083
	addq	$16, %rcx
.L1043:
	movq	%r15, (%rcx)
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1040
	cmpq	72(%rbx), %rax
	je	.L1084
.L1041:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1040
	movq	8(%rbx), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L1040:
	movq	16(%r15), %rdx
	testq	%rdx, %rdx
	je	.L1023
	movq	8(%rbx), %rdi
	movq	%r15, %rsi
	leaq	.LC35(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	8(%rbx), %rdi
	movq	16(%r15), %rsi
	movq	%r15, %rdx
	leaq	.LC36(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	-88(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L1030
	cmpq	$1, %rdx
	je	.L1085
	movq	%rcx, %rsi
	movq	%rcx, -152(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	movq	-152(%rbp), %rcx
.L1030:
	movq	%rdx, 40(%r15)
	movb	$0, (%rdi,%rdx)
	movq	-96(%rbp), %rdi
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1084:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1041
	.p2align 4,,10
	.p2align 3
.L1082:
	movq	%rax, %xmm0
	movq	%rsi, %xmm2
	movq	%rdx, 32(%r15)
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 40(%r15)
.L1034:
	movq	%rcx, -96(%rbp)
	leaq	-80(%rbp), %rcx
	movq	%rcx, %rdi
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1037:
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	96(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	$0, (%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	%r12, 8(%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPKN4node14MemoryRetainerESt4pairIKS3_PNS0_18MemoryRetainerNodeEESaIS8_ENSt8__detail10_Select1stESt8equal_toIS3_ESt4hashIS3_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNSA_10_Hash_nodeIS8_Lb0EEEm
	leaq	16(%rax), %rcx
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1080:
	leaq	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1085:
	movzbl	-80(%rbp), %eax
	movb	%al, (%rdi)
	movq	-88(%rbp), %rdx
	movq	32(%r15), %rdi
	jmp	.L1030
.L1079:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8524:
	.size	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc, .-_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	.section	.rodata._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_.str1.8,"aMS",@progbits,1
	.align 8
.LC37:
	.string	"cannot create std::deque larger than max_size()"
	.section	.text._ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,"axG",@progbits,_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.type	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, @function
_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_:
.LFB10591:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %r14
	movq	40(%rdi), %rsi
	movq	48(%rbx), %rdx
	subq	56(%rbx), %rdx
	movq	%r14, %r13
	movq	%rdx, %rcx
	subq	%rsi, %r13
	sarq	$3, %rcx
	movq	%r13, %rdi
	sarq	$3, %rdi
	leaq	-1(%rdi), %rax
	salq	$6, %rax
	leaq	(%rcx,%rax), %rdx
	movq	32(%rbx), %rax
	subq	16(%rbx), %rax
	movabsq	$1152921504606846975, %rcx
	sarq	$3, %rax
	addq	%rdx, %rax
	cmpq	%rcx, %rax
	je	.L1095
	movq	(%rbx), %r8
	movq	8(%rbx), %rdx
	movq	%r14, %rax
	subq	%r8, %rax
	movq	%rdx, %r9
	sarq	$3, %rax
	subq	%rax, %r9
	cmpq	$1, %r9
	jbe	.L1096
.L1088:
	movl	$512, %edi
	call	_Znwm@PLT
	movq	(%r12), %rdx
	movq	%rax, 8(%r14)
	movq	48(%rbx), %rax
	movq	%rdx, (%rax)
	movq	72(%rbx), %rdx
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1096:
	.cfi_restore_state
	leaq	2(%rdi), %r15
	leaq	(%r15,%r15), %rax
	cmpq	%rax, %rdx
	ja	.L1097
	testq	%rdx, %rdx
	movl	$1, %eax
	cmovne	%rdx, %rax
	leaq	2(%rdx,%rax), %r14
	cmpq	%rcx, %r14
	ja	.L1098
	leaq	0(,%r14,8), %rdi
	call	_Znwm@PLT
	movq	%rax, %rsi
	movq	%rax, -56(%rbp)
	movq	%r14, %rax
	subq	%r15, %rax
	shrq	%rax
	leaq	(%rsi,%rax,8), %r15
	movq	72(%rbx), %rax
	movq	40(%rbx), %rsi
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L1093
	subq	%rsi, %rdx
	movq	%r15, %rdi
	call	memmove@PLT
.L1093:
	movq	(%rbx), %rdi
	call	_ZdlPv@PLT
	movq	-56(%rbp), %rax
	movq	%r14, 8(%rbx)
	movq	%rax, (%rbx)
.L1091:
	movq	(%r15), %rax
	movq	(%r15), %xmm0
	leaq	(%r15,%r13), %r14
	movq	%r15, 40(%rbx)
	movq	%r14, 72(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 24(%rbx)
	movq	(%r14), %rax
	movq	%rax, 56(%rbx)
	addq	$512, %rax
	movq	%rax, 64(%rbx)
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1097:
	subq	%r15, %rdx
	addq	$8, %r14
	shrq	%rdx
	leaq	(%r8,%rdx,8), %r15
	movq	%r14, %rdx
	subq	%rsi, %rdx
	cmpq	%r15, %rsi
	jbe	.L1090
	cmpq	%r14, %rsi
	je	.L1091
	movq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1090:
	cmpq	%r14, %rsi
	je	.L1091
	leaq	8(%r13), %rdi
	subq	%rdx, %rdi
	addq	%r15, %rdi
	call	memmove@PLT
	jmp	.L1091
.L1095:
	leaq	.LC37(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L1098:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE10591:
	.size	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_, .-_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	.section	.rodata.str1.1
.LC38:
	.string	"std::basic_string"
.LC39:
	.string	"error"
.LC40:
	.string	"AllocatedBuffer"
.LC41:
	.string	"pending_cleartext_input"
.LC42:
	.string	"enc_in"
.LC43:
	.string	"enc_out"
	.text
	.align 2
	.p2align 4
	.globl	_ZNK4node7TLSWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node7TLSWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node7TLSWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB8824:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	addq	$56, %rdi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNK4node6crypto7SSLWrapINS_7TLSWrapEE10MemoryInfoEPNS_13MemoryTrackerE@PLT
	movq	432(%r13), %r14
	testq	%r14, %r14
	je	.L1101
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$17, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC38(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r14, 64(%r12)
	leaq	-80(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -80(%rbp)
	call	*%rax
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1102
	movq	(%rdi), %rax
	call	*8(%rax)
.L1102:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1101
	cmpq	72(%rbx), %rax
	je	.L1218
.L1104:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1101
	movq	8(%rbx), %rdi
	leaq	.LC39(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L1101:
	movq	376(%r13), %r14
	testq	%r14, %r14
	je	.L1106
	movl	$72, %edi
	call	_Znwm@PLT
	movl	$15, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC40(%rip), %rcx
	movq	%rax, (%r12)
	leaq	48(%r12), %rax
	leaq	32(%r12), %rdi
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r14, 64(%r12)
	leaq	-80(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	movq	%r12, -80(%rbp)
	call	*%rax
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1107
	movq	(%rdi), %rax
	call	*8(%rax)
.L1107:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1106
	cmpq	72(%rbx), %rax
	je	.L1219
.L1109:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1106
	movq	8(%rbx), %rdi
	leaq	.LC41(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L1106:
	movq	344(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1111
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1111
	movq	104(%rbx), %rdi
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1113
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1220:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1113
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L1113
.L1115:
	cmpq	%rsi, %r12
	jne	.L1220
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1221
	cmpq	72(%rbx), %rax
	je	.L1222
.L1116:
	movq	-8(%rax), %rsi
.L1154:
	leaq	.LC42(%rip), %rcx
	call	*%r8
.L1111:
	movq	352(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1099
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1099
	movq	104(%rbx), %rdi
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1132
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1134
	.p2align 4,,10
	.p2align 3
.L1223:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1132
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%rdx, %r8
	jne	.L1132
.L1134:
	cmpq	%rsi, %r12
	jne	.L1223
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	movq	(%rdi), %rax
	movq	16(%rax), %r8
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1224
	cmpq	72(%rbx), %rax
	je	.L1225
.L1135:
	movq	-8(%rax), %rsi
.L1150:
	leaq	.LC43(%rip), %rcx
	call	*%r8
.L1099:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1226
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1222:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1135
	.p2align 4,,10
	.p2align 3
.L1219:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1109
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1104
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	(%rbx), %rsi
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1118
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1119
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1118
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%r8, %rdx
	jne	.L1118
.L1119:
	cmpq	%rsi, %r12
	jne	.L1227
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1121
	cmpq	72(%rbx), %rax
	je	.L1228
.L1120:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1121
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	leaq	.LC42(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L1121:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1132:
	movq	(%rbx), %rsi
	leaq	-80(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	104(%rbx), %rdi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rdi
	movq	96(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r8
	testq	%rax, %rax
	je	.L1137
	movq	(%rax), %rcx
	movq	8(%rcx), %rsi
	jmp	.L1138
	.p2align 4,,10
	.p2align 3
.L1229:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L1137
	movq	8(%rcx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rdi
	cmpq	%r8, %rdx
	jne	.L1137
.L1138:
	cmpq	%rsi, %r12
	jne	.L1229
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L1140
	cmpq	72(%rbx), %rax
	je	.L1230
.L1139:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L1140
	movq	8(%rbx), %rdi
	movq	16(%rcx), %rdx
	leaq	.LC43(%rip), %rcx
	movq	(%rdi), %rax
	call	*16(%rax)
.L1140:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1221:
	xorl	%esi, %esi
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1224:
	xorl	%esi, %esi
	jmp	.L1150
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	.LC42(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	movq	80(%rbx), %rcx
	movq	64(%rbx), %rdx
	movq	%rax, -80(%rbp)
	subq	$8, %rcx
	cmpq	%rcx, %rdx
	je	.L1122
	movq	%rax, (%rdx)
	addq	$8, %rdx
	movq	%rdx, 64(%rbx)
.L1123:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-80(%rbp), %r15
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L1157
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L1231
.L1125:
	movq	-8(%rax), %rax
.L1124:
	cmpq	%r15, %rax
	jne	.L1145
	cmpq	$0, 64(%rax)
	je	.L1146
	cmpq	72(%rbx), %rdi
	je	.L1128
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L1121
	.p2align 4,,10
	.p2align 3
.L1137:
	leaq	.LC43(%rip), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node13MemoryTracker7AddNodeEPKNS_14MemoryRetainerEPKc
	movq	80(%rbx), %rcx
	movq	64(%rbx), %rdx
	movq	%rax, -112(%rbp)
	subq	$8, %rcx
	cmpq	%rcx, %rdx
	je	.L1141
	movq	%rax, (%rdx)
	addq	$8, %rdx
	movq	%rdx, 64(%rbx)
.L1142:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	-112(%rbp), %r14
	movq	%rbx, %rsi
	call	*16(%rax)
	movq	64(%rbx), %rdi
	cmpq	32(%rbx), %rdi
	je	.L1159
	movq	%rdi, %rax
	cmpq	72(%rbx), %rdi
	je	.L1232
.L1144:
	movq	-8(%rax), %rax
.L1143:
	cmpq	%r14, %rax
	jne	.L1145
	cmpq	$0, 64(%rax)
	je	.L1146
	cmpq	72(%rbx), %rdi
	je	.L1147
	subq	$8, %rdi
	movq	%rdi, 64(%rbx)
	jmp	.L1140
	.p2align 4,,10
	.p2align 3
.L1228:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1120
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1139
.L1232:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1144
.L1231:
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L1125
.L1145:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1146:
	leaq	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1122:
	leaq	-80(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L1123
.L1141:
	leaq	-112(%rbp), %rsi
	leaq	16(%rbx), %rdi
	call	_ZNSt5dequeIPN4node18MemoryRetainerNodeESaIS2_EE16_M_push_back_auxIJRKS2_EEEvDpOT_
	jmp	.L1142
.L1128:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm2
	leaq	504(%rax), %rcx
	movq	%rax, %xmm1
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L1121
.L1147:
	call	_ZdlPv@PLT
	movq	88(%rbx), %rdx
	movq	-8(%rdx), %rax
	subq	$8, %rdx
	movq	%rdx, %xmm4
	leaq	504(%rax), %rcx
	movq	%rax, %xmm3
	addq	$512, %rax
	movq	%rcx, %xmm0
	punpcklqdq	%xmm3, %xmm0
	movups	%xmm0, 64(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm4, %xmm0
	movups	%xmm0, 80(%rbx)
	jmp	.L1140
.L1159:
	xorl	%eax, %eax
	jmp	.L1143
.L1157:
	xorl	%eax, %eax
	jmp	.L1124
.L1226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8824:
	.size	_ZNK4node7TLSWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node7TLSWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB10677:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L1234
	testq	%rsi, %rsi
	je	.L1250
.L1234:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L1251
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L1237
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L1238:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1252
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1237:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L1238
	jmp	.L1236
	.p2align 4,,10
	.p2align 3
.L1251:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L1236:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L1238
.L1250:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1252:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10677:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB10879:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L1254
	testq	%r14, %r14
	je	.L1270
.L1254:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L1271
	cmpq	$1, %r13
	jne	.L1257
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L1258:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1272
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L1258
	jmp	.L1256
	.p2align 4,,10
	.p2align 3
.L1271:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L1256:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L1258
.L1270:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L1272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10879:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB10883:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L1277
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L1275:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1277:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L1275
	.cfi_endproc
.LFE10883:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC44:
	.string	"(null)"
.LC45:
	.string	"lz"
.LC46:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_:
.LFB10506:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L1279
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1279:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC45(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-184(%rbp), %r9
.L1280:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L1280
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L1281
	cmpb	$99, %dl
	jg	.L1282
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L1283
	cmpb	$88, %dl
	je	.L1284
	jmp	.L1281
.L1282:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1281
	leaq	.L1286(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
	.align 4
	.align 4
.L1286:
	.long	.L1285-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1285-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1285-.L1286
	.long	.L1288-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1285-.L1286
	.long	.L1281-.L1286
	.long	.L1285-.L1286
	.long	.L1281-.L1286
	.long	.L1281-.L1286
	.long	.L1285-.L1286
	.section	.text.unlikely._ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_,comdat
.L1283:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1320
	jmp	.L1317
.L1281:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1317
.L1320:
	call	_ZdlPv@PLT
	jmp	.L1317
.L1285:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L1300
	leaq	.LC44(%rip), %rsi
.L1300:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L1312
	jmp	.L1297
.L1284:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L1302
	leaq	.LC44(%rip), %rsi
.L1302:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1303
	call	_ZdlPv@PLT
.L1303:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1297
.L1312:
	call	_ZdlPv@PLT
	jmp	.L1297
.L1288:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC46(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L1305
	leaq	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1305:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L1297:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1317:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L1292
	call	_ZdlPv@PLT
.L1292:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1278
	call	_ZdlPv@PLT
.L1278:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1308
	call	__stack_chk_fail@PLT
.L1308:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10506:
	.size	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_, .-_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.type	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, @function
_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_:
.LFB10081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1322
	call	__stack_chk_fail@PLT
.L1322:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10081:
	.size	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_, .-_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.type	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, @function
_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_:
.LFB9528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1324
	call	_ZdlPv@PLT
.L1324:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1326
	call	__stack_chk_fail@PLT
.L1326:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9528:
	.size	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_, .-_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
	.section	.rodata._ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_.str1.1,"aMS",@progbits,1
.LC47:
	.string	" "
.LC48:
	.string	"\n"
.LC49:
	.string	"%s"
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB10210:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-144(%rbp), %r14
	leaq	-176(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	movq	%rdi, %rsi
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r13, %rdi
	call	*72(%rax)
	movq	%r13, %rsi
	leaq	.LC47(%rip), %rdx
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	-112(%rbp), %r13
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC48(%rip), %rdx
	leaq	-80(%rbp), %rdi
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	-80(%rbp), %rax
	movq	16(%rbx), %rdx
	movq	%rax, -184(%rbp)
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1329
	movq	stderr(%rip), %rdi
	leaq	-184(%rbp), %rdx
	leaq	.LC49(%rip), %rsi
	call	_ZN4node7FPrintFIJRPKcEEEvP8_IO_FILES2_DpOT_
.L1329:
	movq	-80(%rbp), %rdi
	leaq	-64(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1330
	call	_ZdlPv@PLT
.L1330:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1331
	call	_ZdlPv@PLT
.L1331:
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1332
	call	_ZdlPv@PLT
.L1332:
	movq	-176(%rbp), %rdi
	leaq	-160(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1328
	call	_ZdlPv@PLT
.L1328:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1334
	call	__stack_chk_fail@PLT
.L1334:
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10210:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.1
.LC50:
	.string	"ReadStart()"
	.section	.text.unlikely
	.align 2
.LCOLDB51:
	.text
.LHOTB51:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap9ReadStartEv
	.type	_ZN4node7TLSWrap9ReadStartEv, @function
_ZN4node7TLSWrap9ReadStartEv:
.LFB8788:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZThn248_N4node7TLSWrap9ReadStartEv(%rip), %rbx
	subq	$24, %rsp
.L1340:
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1341
.L1337:
	movq	320(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1338
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1339
	subq	$248, %rdi
	jmp	.L1340
	.p2align 4,,10
	.p2align 3
.L1338:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1339:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap9ReadStartEv.cold, @function
_ZN4node7TLSWrap9ReadStartEv.cold:
.LFSB8788:
.L1341:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	.LC50(%rip), %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-24(%rbp), %rdi
	jmp	.L1337
	.cfi_endproc
.LFE8788:
	.text
	.size	_ZN4node7TLSWrap9ReadStartEv, .-_ZN4node7TLSWrap9ReadStartEv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap9ReadStartEv.cold, .-_ZN4node7TLSWrap9ReadStartEv.cold
.LCOLDE51:
	.text
.LHOTE51:
	.set	.LTHUNK12,_ZN4node7TLSWrap9ReadStartEv
	.section	.rodata.str1.1
.LC52:
	.string	"ReadStop()"
	.section	.text.unlikely
	.align 2
.LCOLDB53:
	.text
.LHOTB53:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap8ReadStopEv
	.type	_ZN4node7TLSWrap8ReadStopEv, @function
_ZN4node7TLSWrap8ReadStopEv:
.LFB8789:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	_ZThn248_N4node7TLSWrap8ReadStopEv(%rip), %rbx
	subq	$24, %rsp
.L1347:
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1348
.L1344:
	movq	320(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1345
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1346
	subq	$248, %rdi
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1345:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1346:
	.cfi_restore_state
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap8ReadStopEv.cold, @function
_ZN4node7TLSWrap8ReadStopEv.cold:
.LFSB8789:
.L1348:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	.LC52(%rip), %rsi
	movq	%rdi, -24(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-24(%rbp), %rdi
	jmp	.L1344
	.cfi_endproc
.LFE8789:
	.text
	.size	_ZN4node7TLSWrap8ReadStopEv, .-_ZN4node7TLSWrap8ReadStopEv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap8ReadStopEv.cold, .-_ZN4node7TLSWrap8ReadStopEv.cold
.LCOLDE53:
	.text
.LHOTE53:
	.set	.LTHUNK13,_ZN4node7TLSWrap8ReadStopEv
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"SSLInfoCallback(SSL_CB_HANDSHAKE_START);"
	.align 8
.LC55:
	.string	"SSLInfoCallback(SSL_CB_HANDSHAKE_DONE);"
	.section	.text.unlikely
	.align 2
.LCOLDB56:
	.text
.LHOTB56:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii
	.type	_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii, @function
_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii:
.LFB8763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testb	$48, %sil
	movl	%esi, -104(%rbp)
	jne	.L1383
.L1350:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1384
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1383:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%rdi, %r12
	leaq	-96(%rbp), %r15
	call	SSL_get_ex_data@PLT
	movq	%r15, %rdi
	movq	16(%rax), %rbx
	movq	%rax, %r13
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	8(%r13), %r9
	movl	-104(%rbp), %r8d
	testq	%r9, %r9
	je	.L1352
	movzbl	11(%r9), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1385
.L1352:
	testb	$16, %r8b
	je	.L1353
	movq	16(%r13), %rdx
	movslq	32(%r13), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1379
.L1354:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r9, %rdi
	movl	%r8d, -112(%rbp)
	movq	%r9, -104(%rbp)
	movq	1184(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	-104(%rbp), %r9
	movl	-112(%rbp), %r8d
	testq	%rax, %rax
	je	.L1353
	movq	%rax, %rdi
	movl	%r8d, -116(%rbp)
	movq	%r9, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-104(%rbp), %rsi
	movq	-112(%rbp), %r9
	testb	%al, %al
	movl	-116(%rbp), %r8d
	jne	.L1386
	.p2align 4,,10
	.p2align 3
.L1353:
	andl	$32, %r8d
	movq	%r9, -104(%rbp)
	je	.L1358
	movq	%r12, %rdi
	call	SSL_renegotiate_pending@PLT
	testl	%eax, %eax
	jne	.L1358
	movq	16(%r13), %rdx
	movslq	32(%r13), %rax
	movq	-104(%rbp), %r9
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1380
.L1360:
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	call	SSL_renegotiate_pending@PLT
	movq	-104(%rbp), %r9
	testl	%eax, %eax
	jne	.L1387
	movq	360(%rbx), %rax
	movb	$1, 418(%r13)
	movq	%r9, %rdi
	movq	3280(%rbx), %rsi
	movq	1176(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1358
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L1388
	.p2align 4,,10
	.p2align 3
.L1358:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1385:
	movq	16(%r13), %rax
	movq	(%r9), %rsi
	movq	352(%rax), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movl	-104(%rbp), %r8d
	movq	%rax, %r9
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1386:
	movq	%rbx, %rdi
	movl	%r8d, -116(%rbp)
	movq	%r9, -104(%rbp)
	movq	%rsi, -112(%rbp)
	call	_ZN4node11Environment6GetNowEv@PLT
	movq	-112(%rbp), %rsi
	leaq	-64(%rbp), %rcx
	movq	%r13, %rdi
	movl	$1, %edx
	movq	%rax, -64(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movl	-116(%rbp), %r8d
	movq	-104(%rbp), %r9
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1388:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1387:
	leaq	_ZZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stiiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1384:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii.cold, @function
_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii.cold:
.LFSB8763:
.L1380:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC55(%rip), %rsi
	movq	%r13, %rdi
	movq	%r9, -104(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-104(%rbp), %r9
	jmp	.L1360
.L1379:
	leaq	.LC54(%rip), %rsi
	movq	%r13, %rdi
	movl	%r8d, -112(%rbp)
	movq	%r9, -104(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-104(%rbp), %r9
	movl	-112(%rbp), %r8d
	jmp	.L1354
	.cfi_endproc
.LFE8763:
	.text
	.size	_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii, .-_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii.cold, .-_ZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stii.cold
.LCOLDE56:
	.text
.LHOTE56:
	.section	.rodata.str1.1
.LC57:
	.string	"Created new TLSWrap"
	.section	.text.unlikely
	.align 2
.LCOLDB58:
	.text
.LHOTB58:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE
	.type	_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE, @function
_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE:
.LFB8751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	movl	$45, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$24, %rsp
	movsd	.LC1(%rip), %xmm0
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE(%rip), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$-1, %esi
	pxor	%xmm0, %xmm0
	movq	$0, 152(%r12)
	movb	$0, 184(%r12)
	movq	%rax, 56(%r12)
	movq	%rbx, 64(%r12)
	movl	%r15d, 72(%r12)
	movw	%dx, 96(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	movb	$0, 120(%r12)
	movl	$3, 128(%r12)
	movq	$0, 176(%r12)
	movq	$0, 192(%r12)
	movw	%cx, 200(%r12)
	movq	$0, 208(%r12)
	movw	%si, 216(%r12)
	movq	$0, 240(%r12)
	movups	%xmm0, 80(%r12)
	movups	%xmm0, 136(%r12)
	movups	%xmm0, 224(%r12)
	movq	32(%r13), %rdi
	movups	%xmm0, 160(%r12)
	call	SSL_new@PLT
	movq	88(%r12), %rdi
	movq	%rax, 88(%r12)
	testq	%rdi, %rdi
	je	.L1390
	call	SSL_free@PLT
	movq	88(%r12), %rax
.L1390:
	testq	%rax, %rax
	je	.L1406
	movq	64(%r12), %rax
	movq	352(%rax), %rdi
	movq	32(%rdi), %rax
	leaq	50272(%rax), %r15
	movq	%r15, %rax
	subq	48(%rdi), %rax
	movq	%r15, 32(%rdi)
	cmpq	$33554432, %rax
	jg	.L1407
.L1392:
	cmpq	40(%rdi), %r15
	jg	.L1408
.L1393:
	leaq	16+_ZTVN4node22EmitToJSStreamListenerE(%rip), %rax
	pxor	%xmm0, %xmm0
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, 288(%r12)
	leaq	288(%r12), %rax
	leaq	248(%r12), %r15
	movq	%rax, 256(%r12)
	leaq	16+_ZTVN4node7TLSWrapE(%rip), %rax
	movq	%rax, (%r12)
	addq	$216, %rax
	movq	%rax, 56(%r12)
	addq	$32, %rax
	movq	%rax, 248(%r12)
	addq	$160, %rax
	movq	%rbx, 280(%r12)
	movq	$0, 304(%r12)
	movq	%r15, 296(%r12)
	movq	$0, 320(%r12)
	movq	$0, 328(%r12)
	movq	%rax, 312(%r12)
	movq	%r13, 336(%r12)
	movq	$0, 344(%r12)
	movq	$0, 352(%r12)
	movq	$0, 360(%r12)
	movups	%xmm0, 264(%r12)
	call	uv_buf_init@PLT
	movq	$0, 384(%r12)
	movq	%rax, 368(%r12)
	leaq	440(%r12), %rax
	movq	%rax, 424(%r12)
	movq	24(%r12), %rax
	movq	%rdx, 376(%r12)
	movq	$0, 392(%r12)
	movb	$0, 400(%r12)
	movq	$0, 408(%r12)
	movl	$0, 416(%r12)
	movq	$0, 432(%r12)
	movb	$0, 440(%r12)
	movl	$0, 456(%r12)
	movb	$0, 460(%r12)
	movq	$0, 464(%r12)
	testq	%rax, %rax
	je	.L1399
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1394
.L1399:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L1394:
	movq	%r15, %rdi
	call	_ZN4node10StreamBase9GetObjectEv@PLT
	movq	%r15, %rdx
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	336(%r12), %rax
	movq	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE18GetSessionCallbackEP6ssl_stPKhiPi@GOTPCREL(%rip), %rsi
	movq	32(%rax), %rdi
	call	SSL_CTX_sess_set_get_cb@PLT
	movq	336(%r12), %rax
	movq	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE18NewSessionCallbackEP6ssl_stP14ssl_session_st@GOTPCREL(%rip), %rsi
	movq	32(%rax), %rdi
	call	SSL_CTX_sess_set_new_cb@PLT
	cmpq	$0, 320(%r12)
	jne	.L1409
	movq	8(%r14), %rax
	movq	%r14, 320(%r12)
	movq	%r12, %rdi
	movq	%rax, 328(%r12)
	leaq	312(%r12), %rax
	movq	%rax, 8(%r14)
	call	_ZN4node7TLSWrap7InitSSLEv
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1403
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1408:
	.cfi_restore_state
	call	_ZN2v87Isolate36ReportExternalAllocationLimitReachedEv@PLT
	jmp	.L1393
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	%rdi, -56(%rbp)
	call	_ZN2v87Isolate19CheckMemoryPressureEv@PLT
	movq	-56(%rbp), %rdi
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1406:
	leaq	_ZZN4node6crypto7SSLWrapINS_7TLSWrapEEC4EPNS_11EnvironmentEPNS0_13SecureContextENS3_4KindEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1409:
	leaq	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE.cold, @function
_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE.cold:
.LFSB8751:
.L1403:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	addq	$24, %rsp
	movq	%r12, %rdi
	leaq	.LC57(%rip), %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	.cfi_endproc
.LFE8751:
	.text
	.size	_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE, .-_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE
	.section	.text.unlikely
	.size	_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE.cold, .-_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE.cold
.LCOLDE58:
	.text
.LHOTE58:
	.globl	_ZN4node7TLSWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE
	.set	_ZN4node7TLSWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE,_ZN4node7TLSWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1447
	cmpw	$1040, %cx
	jne	.L1411
.L1447:
	movq	23(%rdx), %r13
.L1413:
	cmpl	$3, 16(%rbx)
	je	.L1463
	leaq	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1463:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1464
	cmpl	$1, 16(%rbx)
	jle	.L1465
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L1416:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1466
	cmpl	$2, 16(%rbx)
	jg	.L1418
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1419:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1467
	movl	16(%rbx), %eax
	cmpl	$1, %eax
	jg	.L1421
	movq	(%rbx), %rax
	movq	8(%rax), %r14
	addq	$88, %r14
	movq	%r14, %rdi
.L1424:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movb	%al, -49(%rbp)
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1425
	movq	(%rbx), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
.L1426:
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1448
	cmpw	$1040, %cx
	jne	.L1427
.L1448:
	cmpq	$0, 23(%rdx)
	je	.L1430
.L1429:
	movq	31(%rdx), %r15
.L1434:
	testq	%r15, %r15
	je	.L1430
	movq	3024(%r13), %rdi
	movq	3280(%r13), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1410
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1468
	movq	(%r14), %rcx
	movq	-1(%rcx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %esi
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1449
	cmpw	$1040, %si
	jne	.L1438
.L1449:
	movq	23(%rcx), %r9
.L1440:
	movl	$472, %edi
	movq	%r9, -64(%rbp)
	call	_Znwm@PLT
	movzbl	-49(%rbp), %ecx
	movq	-64(%rbp), %r9
	movq	%r15, %r8
	movq	%rax, %r14
	movq	%rax, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZN4node7TLSWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS_6crypto7SSLWrapIS0_E4KindEPNS_10StreamBaseEPNS7_13SecureContextE
	movq	8(%r14), %rax
	movq	(%rbx), %rbx
	testq	%rax, %rax
	je	.L1441
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L1469
.L1442:
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L1410:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1465:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1416
	.p2align 4,,10
	.p2align 3
.L1418:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1419
	.p2align 4,,10
	.p2align 3
.L1425:
	movq	8(%rbx), %r15
	jmp	.L1426
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	8(%rbx), %rdi
	leaq	-8(%rdi), %r14
	cmpl	$2, %eax
	je	.L1470
	subq	$16, %rdi
	jmp	.L1424
	.p2align 4,,10
	.p2align 3
.L1411:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	16(%r14), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	testq	%rax, %rax
	jne	.L1442
	.p2align 4,,10
	.p2align 3
.L1441:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L1410
	.p2align 4,,10
	.p2align 3
.L1464:
	leaq	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1466:
	leaq	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1467:
	leaq	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1430:
	leaq	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1427:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L1430
	movq	(%r15), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1429
	cmpw	$1040, %cx
	je	.L1429
	movq	%r15, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L1434
	.p2align 4,,10
	.p2align 3
.L1438:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r9
	jmp	.L1440
	.p2align 4,,10
	.p2align 3
.L1468:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1470:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1424
	.cfi_endproc
.LFE8760:
	.size	_ZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC59:
	.string	"~TLSWrap()"
	.section	.text.unlikely
	.align 2
.LCOLDB60:
	.text
.LHOTB60:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrapD2Ev
	.type	_ZN4node7TLSWrapD2Ev, @function
_ZN4node7TLSWrapD2Ev:
.LFB8754:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7TLSWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	16(%rdi), %rdx
	movq	%rax, (%rdi)
	addq	$216, %rax
	movq	%rax, 56(%rdi)
	addq	$32, %rax
	movq	%rax, 248(%rdi)
	addq	$160, %rax
	movq	%rax, 312(%rdi)
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1533
.L1472:
	movq	464(%r12), %rdi
	movq	$0, 336(%r12)
	testq	%rdi, %rdi
	je	.L1473
	call	BIO_free_all@PLT
.L1473:
	movq	424(%r12), %rdi
	leaq	440(%r12), %rax
	cmpq	%rax, %rdi
	je	.L1474
	call	_ZdlPv@PLT
.L1474:
	movq	368(%r12), %r13
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	376(%r12), %r14
	call	uv_buf_init@PLT
	movq	%rax, 368(%r12)
	movq	%rdx, 376(%r12)
	testq	%r13, %r13
	je	.L1475
	movq	360(%r12), %rax
	testq	%rax, %rax
	je	.L1539
	movq	360(%rax), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L1475:
	movq	320(%r12), %rcx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rdx
	movq	%rdx, 312(%r12)
	testq	%rcx, %rcx
	jne	.L1540
.L1480:
	movq	%rdx, 288(%r12)
	movq	296(%r12), %rdx
	leaq	16+_ZTVN4node10StreamBaseE(%rip), %rax
	movq	%rax, 248(%r12)
	testq	%rdx, %rdx
	jne	.L1541
.L1484:
	leaq	16+_ZTVN4node14StreamResourceE(%rip), %rax
	movq	256(%r12), %rbx
	movq	%rax, 248(%r12)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1543:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	call	*56(%rax)
	movq	256(%r12), %rax
	cmpq	%rax, %rbx
	je	.L1542
	movq	%rax, %rbx
.L1490:
	testq	%rbx, %rbx
	jne	.L1543
	leaq	16+_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE(%rip), %rax
	leaq	56(%r12), %rdi
	movq	%rax, 56(%r12)
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE10DestroySSLEv@PLT
	movq	240(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1493
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L1544
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1545
	subl	$1, %eax
	movl	%eax, (%rdx)
	je	.L1546
.L1493:
	movq	232(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1499
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L1499:
	movq	88(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1500
	call	SSL_free@PLT
.L1500:
	movq	80(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1501
	call	SSL_SESSION_free@PLT
.L1501:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L1540:
	.cfi_restore_state
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L1481
	leaq	312(%r12), %rsi
	cmpq	%rsi, %rax
	jne	.L1478
	jmp	.L1547
	.p2align 4,,10
	.p2align 3
.L1534:
	cmpq	%rsi, %rax
	je	.L1548
.L1478:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1534
.L1481:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1541:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L1481
	leaq	288(%r12), %rcx
	cmpq	%rcx, %rax
	jne	.L1486
	jmp	.L1549
	.p2align 4,,10
	.p2align 3
.L1535:
	cmpq	%rcx, %rax
	je	.L1550
.L1486:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1535
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1542:
	movq	16(%rbx), %rbx
	pxor	%xmm0, %xmm0
	movq	%rbx, 256(%r12)
	movups	%xmm0, 8(%rax)
	jmp	.L1490
	.p2align 4,,10
	.p2align 3
.L1548:
	movq	328(%r12), %rax
	movq	%rax, 16(%rcx)
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1550:
	movq	304(%r12), %rax
	movq	%rax, 16(%rdx)
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1539:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1546:
	cmpb	$0, 9(%rdx)
	je	.L1497
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	328(%r12), %rax
	movq	%rax, 8(%rcx)
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1549:
	movq	304(%r12), %rax
	movq	%rax, 8(%rdx)
	jmp	.L1484
	.p2align 4,,10
	.p2align 3
.L1497:
	cmpb	$0, 8(%rdx)
	je	.L1493
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L1493
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L1493
	.p2align 4,,10
	.p2align 3
.L1544:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1545:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrapD2Ev.cold, @function
_ZN4node7TLSWrapD2Ev.cold:
.LFSB8754:
.L1533:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	.LC59(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1472
	.cfi_endproc
.LFE8754:
	.text
	.size	_ZN4node7TLSWrapD2Ev, .-_ZN4node7TLSWrapD2Ev
	.section	.text.unlikely
	.size	_ZN4node7TLSWrapD2Ev.cold, .-_ZN4node7TLSWrapD2Ev.cold
.LCOLDE60:
	.text
.LHOTE60:
	.set	.LTHUNK1,_ZN4node7TLSWrapD2Ev
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrapD1Ev
	.type	_ZThn248_N4node7TLSWrapD1Ev, @function
_ZThn248_N4node7TLSWrapD1Ev:
.LFB12156:
	.cfi_startproc
	endbr64
	subq	$248, %rdi
	jmp	.LTHUNK1
	.cfi_endproc
.LFE12156:
	.size	_ZThn248_N4node7TLSWrapD1Ev, .-_ZThn248_N4node7TLSWrapD1Ev
	.set	.LTHUNK2,_ZN4node7TLSWrapD2Ev
	.p2align 4
	.globl	_ZThn56_N4node7TLSWrapD1Ev
	.type	_ZThn56_N4node7TLSWrapD1Ev, @function
_ZThn56_N4node7TLSWrapD1Ev:
.LFB12157:
	.cfi_startproc
	endbr64
	subq	$56, %rdi
	jmp	.LTHUNK2
	.cfi_endproc
.LFE12157:
	.size	_ZThn56_N4node7TLSWrapD1Ev, .-_ZThn56_N4node7TLSWrapD1Ev
	.set	.LTHUNK0,_ZN4node7TLSWrapD2Ev
	.p2align 4
	.globl	_ZThn312_N4node7TLSWrapD1Ev
	.type	_ZThn312_N4node7TLSWrapD1Ev, @function
_ZThn312_N4node7TLSWrapD1Ev:
.LFB12158:
	.cfi_startproc
	endbr64
	subq	$312, %rdi
	jmp	.LTHUNK0
	.cfi_endproc
.LFE12158:
	.size	_ZThn312_N4node7TLSWrapD1Ev, .-_ZThn312_N4node7TLSWrapD1Ev
	.globl	_ZN4node7TLSWrapD1Ev
	.set	_ZN4node7TLSWrapD1Ev,_ZN4node7TLSWrapD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrapD0Ev
	.type	_ZN4node7TLSWrapD0Ev, @function
_ZN4node7TLSWrapD0Ev:
.LFB8756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node7TLSWrapD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$472, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8756:
	.size	_ZN4node7TLSWrapD0Ev, .-_ZN4node7TLSWrapD0Ev
	.p2align 4
	.globl	_ZThn312_N4node7TLSWrapD0Ev
	.type	_ZThn312_N4node7TLSWrapD0Ev, @function
_ZThn312_N4node7TLSWrapD0Ev:
.LFB12116:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-312(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN4node7TLSWrapD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$472, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12116:
	.size	_ZThn312_N4node7TLSWrapD0Ev, .-_ZThn312_N4node7TLSWrapD0Ev
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrapD0Ev
	.type	_ZThn248_N4node7TLSWrapD0Ev, @function
_ZThn248_N4node7TLSWrapD0Ev:
.LFB12117:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-248(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN4node7TLSWrapD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$472, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12117:
	.size	_ZThn248_N4node7TLSWrapD0Ev, .-_ZThn248_N4node7TLSWrapD0Ev
	.p2align 4
	.globl	_ZThn56_N4node7TLSWrapD0Ev
	.type	_ZThn56_N4node7TLSWrapD0Ev, @function
_ZThn56_N4node7TLSWrapD0Ev:
.LFB12118:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	subq	$8, %rsp
	call	_ZN4node7TLSWrapD1Ev
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$472, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE12118:
	.size	_ZThn56_N4node7TLSWrapD0Ev, .-_ZThn56_N4node7TLSWrapD0Ev
	.section	.text.unlikely
.LCOLDB61:
	.text
.LHOTB61:
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap9ReadStartEv
	.type	_ZThn248_N4node7TLSWrap9ReadStartEv, @function
_ZThn248_N4node7TLSWrap9ReadStartEv:
.LFB12122:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-232(%rdi), %rdx
	movslq	-216(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1563
.L1560:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1561
	movq	(%rdi), %rax
	leaq	_ZThn248_N4node7TLSWrap9ReadStartEv(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1565
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1561:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1565:
	.cfi_restore_state
	popq	%rax
	subq	$248, %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	.LTHUNK12
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZThn248_N4node7TLSWrap9ReadStartEv.cold, @function
_ZThn248_N4node7TLSWrap9ReadStartEv.cold:
.LFSB12122:
.L1563:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	-248(%rdi), %rdi
	leaq	.LC50(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1560
	.cfi_endproc
.LFE12122:
	.text
	.size	_ZThn248_N4node7TLSWrap9ReadStartEv, .-_ZThn248_N4node7TLSWrap9ReadStartEv
	.section	.text.unlikely
	.size	_ZThn248_N4node7TLSWrap9ReadStartEv.cold, .-_ZThn248_N4node7TLSWrap9ReadStartEv.cold
.LCOLDE61:
	.text
.LHOTE61:
	.section	.text.unlikely
.LCOLDB62:
	.text
.LHOTB62:
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap8ReadStopEv
	.type	_ZThn248_N4node7TLSWrap8ReadStopEv, @function
_ZThn248_N4node7TLSWrap8ReadStopEv:
.LFB12123:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-232(%rdi), %rdx
	movslq	-216(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1570
.L1567:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1568
	movq	(%rdi), %rax
	leaq	_ZThn248_N4node7TLSWrap8ReadStopEv(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	je	.L1572
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1568:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L1572:
	.cfi_restore_state
	popq	%rax
	subq	$248, %rdi
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	.LTHUNK13
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZThn248_N4node7TLSWrap8ReadStopEv.cold, @function
_ZThn248_N4node7TLSWrap8ReadStopEv.cold:
.LFSB12123:
.L1570:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	-248(%rdi), %rdi
	leaq	.LC52(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1567
	.cfi_endproc
.LFE12123:
	.text
	.size	_ZThn248_N4node7TLSWrap8ReadStopEv, .-_ZThn248_N4node7TLSWrap8ReadStopEv
	.section	.text.unlikely
	.size	_ZThn248_N4node7TLSWrap8ReadStopEv.cold, .-_ZThn248_N4node7TLSWrap8ReadStopEv.cold
.LCOLDE62:
	.text
.LHOTE62:
	.section	.rodata._ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_.str1.1,"aMS",@progbits,1
.LC63:
	.string	"%d"
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	.type	_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_, @function
_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_:
.LFB11598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1574
	leaq	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1574:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC45(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1575:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1575
	cmpb	$120, %dl
	jg	.L1576
	cmpb	$99, %dl
	jg	.L1577
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L1578
	cmpb	$88, %dl
	je	.L1579
	jmp	.L1576
.L1577:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1576
	leaq	.L1581(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,comdat
	.align 4
	.align 4
.L1581:
	.long	.L1582-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1582-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1584-.L1581
	.long	.L1583-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1582-.L1581
	.long	.L1576-.L1581
	.long	.L1582-.L1581
	.long	.L1576-.L1581
	.long	.L1576-.L1581
	.long	.L1580-.L1581
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_,comdat
.L1578:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1585
	call	_ZdlPv@PLT
.L1585:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1605
	jmp	.L1587
.L1576:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1610
	call	_ZdlPv@PLT
	jmp	.L1610
.L1582:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC63(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L1607
.L1584:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L1592:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1592
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1607
.L1580:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1607:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1604
	jmp	.L1591
.L1579:
	movl	(%r8), %esi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1595
	call	_ZdlPv@PLT
.L1595:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1591
.L1604:
	call	_ZdlPv@PLT
	jmp	.L1591
.L1583:
	leaq	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1591:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1610:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1587
.L1605:
	call	_ZdlPv@PLT
.L1587:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1573
	call	_ZdlPv@PLT
.L1573:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1599
	call	__stack_chk_fail@PLT
.L1599:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11598:
	.size	_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_, .-_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_,"axG",@progbits,_ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_,comdat
	.weak	_ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_
	.type	_ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_, @function
_ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_:
.LFB11428:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1612
	call	__stack_chk_fail@PLT
.L1612:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11428:
	.size	_ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_, .-_ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_,"axG",@progbits,_ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_,comdat
	.weak	_ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_
	.type	_ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_, @function
_ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_:
.LFB11212:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRiRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_DpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1614
	call	_ZdlPv@PLT
.L1614:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1616
	call	__stack_chk_fail@PLT
.L1616:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11212:
	.size	_ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_, .-_ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_:
.LFB10211:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r15, %rdi
	call	*72(%rax)
	leaq	-160(%rbp), %r8
	movq	%r15, %rsi
	leaq	.LC47(%rip), %rdx
	movq	%r8, %rdi
	movq	%r8, -200(%rbp)
	leaq	-128(%rbp), %r15
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	-200(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC48(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1619
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJRiRPKcEEEvP8_IO_FILES3_DpOT_
.L1619:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1620
	call	_ZdlPv@PLT
.L1620:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1621
	call	_ZdlPv@PLT
.L1621:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1622
	call	_ZdlPv@PLT
.L1622:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1618
	call	_ZdlPv@PLT
.L1618:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1624
	call	__stack_chk_fail@PLT
.L1624:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10211:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_
	.section	.rodata.str1.1
.LC64:
	.string	"InvokeQueued(%d, %s)"
	.section	.text.unlikely
	.align 2
.LCOLDB65:
	.text
.LHOTB65:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	.type	_ZN4node7TLSWrap12InvokeQueuedEiPKc, @function
_ZN4node7TLSWrap12InvokeQueuedEiPKc:
.LFB8757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -64(%rbp)
	movslq	32(%rdi), %rax
	movq	16(%rdi), %rdx
	movl	%esi, -52(%rbp)
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1645
.L1627:
	movzbl	416(%rdi), %r12d
	testb	%r12b, %r12b
	je	.L1626
	movq	392(%rdi), %r13
	testq	%r13, %r13
	je	.L1626
	movq	-64(%rbp), %rbx
	movq	0(%r13), %rax
	movq	$0, 392(%rdi)
	movq	%r13, %rdi
	movl	-52(%rbp), %r14d
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L1629
	movq	16(%rax), %r8
	movq	8(%rax), %r15
	movq	352(%r8), %rdi
	testq	%r15, %r15
	je	.L1630
	movzbl	11(%r15), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1647
.L1630:
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r8, -72(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-72(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L1648
.L1631:
	movq	360(%r8), %rax
	movq	3280(%r8), %rsi
	movq	%r15, %rdi
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1649
.L1629:
	movq	0(%r13), %rax
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	*24(%rax)
.L1626:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1647:
	.cfi_restore_state
	movq	(%r15), %rsi
	movq	%r8, -72(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-72(%rbp), %r8
	movq	%rax, %r15
	movq	352(%r8), %rdi
	jmp	.L1630
	.p2align 4,,10
	.p2align 3
.L1649:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1648:
	movq	%rax, -80(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %r8
	jmp	.L1631
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap12InvokeQueuedEiPKc.cold, @function
_ZN4node7TLSWrap12InvokeQueuedEiPKc.cold:
.LFSB8757:
.L1645:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-64(%rbp), %rcx
	leaq	-52(%rbp), %rdx
	movq	%rdi, -72(%rbp)
	leaq	.LC64(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_
	movq	-72(%rbp), %rdi
	jmp	.L1627
	.cfi_endproc
.LFE8757:
	.text
	.size	_ZN4node7TLSWrap12InvokeQueuedEiPKc, .-_ZN4node7TLSWrap12InvokeQueuedEiPKc
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap12InvokeQueuedEiPKc.cold, .-_ZN4node7TLSWrap12InvokeQueuedEiPKc.cold
.LCOLDE65:
	.text
.LHOTE65:
	.section	.rodata.str1.1
.LC66:
	.string	"DestroySSL()"
.LC67:
	.string	"DestroySSL() finished"
	.section	.rodata.str1.8
	.align 8
.LC68:
	.string	"Canceled because of SSL destruction"
	.section	.text.unlikely
	.align 2
.LCOLDB69:
	.text
.LHOTB69:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8808:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1680
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1668
	cmpw	$1040, %cx
	jne	.L1652
.L1668:
	movq	23(%rdx), %r12
.L1654:
	testq	%r12, %r12
	je	.L1650
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1675
.L1657:
	leaq	.LC68(%rip), %rdx
	movl	$-125, %esi
	movq	%r12, %rdi
	movb	$1, 416(%r12)
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	leaq	56(%r12), %rdi
	call	_ZN4node6crypto7SSLWrapINS_7TLSWrapEE10DestroySSLEv@PLT
	movq	320(%r12), %rdx
	pxor	%xmm0, %xmm0
	movups	%xmm0, 344(%r12)
	testq	%rdx, %rdx
	jne	.L1681
.L1667:
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1676
.L1650:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1681:
	.cfi_restore_state
	movq	8(%rdx), %rax
	leaq	312(%r12), %rcx
	testq	%rax, %rax
	je	.L1662
	cmpq	%rax, %rcx
	jne	.L1659
	jmp	.L1682
	.p2align 4,,10
	.p2align 3
.L1677:
	cmpq	%rax, %rcx
	je	.L1683
.L1659:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L1677
.L1662:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1652:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1654
	.p2align 4,,10
	.p2align 3
.L1680:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1683:
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
.L1661:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 320(%r12)
	jmp	.L1667
	.p2align 4,,10
	.p2align 3
.L1682:
	movq	328(%r12), %rax
	movq	%rax, 8(%rdx)
	jmp	.L1661
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold, @function
_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold:
.LFSB8808:
.L1676:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	popq	%rax
	movq	%r12, %rdi
	leaq	.LC67(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
.L1675:
	.cfi_restore_state
	leaq	.LC66(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1657
	.cfi_endproc
.LFE8808:
	.text
	.size	_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold, .-_ZN4node7TLSWrap10DestroySSLERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold
.LCOLDE69:
	.text
.LHOTE69:
	.section	.text.unlikely
	.align 2
.LCOLDB70:
	.text
.LHOTB70:
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_:
.LFB11876:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rdi), %rbx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movl	$0, -68(%rbp)
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	movq	$0, -64(%rbp)
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1701
.L1685:
	cmpb	$0, 416(%rbx)
	je	.L1684
	movq	392(%rbx), %r12
	testq	%r12, %r12
	je	.L1684
	movq	-64(%rbp), %r15
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	$0, 392(%rbx)
	movl	-68(%rbp), %r13d
	call	*16(%rax)
	testq	%r15, %r15
	je	.L1687
	movq	16(%rax), %rbx
	movq	8(%rax), %r14
	movq	352(%rbx), %rdi
	testq	%r14, %r14
	je	.L1688
	movzbl	11(%r14), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1703
.L1688:
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1704
.L1689:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1705
.L1687:
	movq	(%r12), %rax
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	*24(%rax)
.L1684:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1706
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1703:
	.cfi_restore_state
	movq	(%r14), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	352(%rbx), %rdi
	movq	%rax, %r14
	jmp	.L1688
	.p2align 4,,10
	.p2align 3
.L1705:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1687
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rcx
	jmp	.L1689
.L1706:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_.cold, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_.cold:
.LFSB11876:
.L1701:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-64(%rbp), %rcx
	leaq	-68(%rbp), %rdx
	movq	%rbx, %rdi
	leaq	.LC64(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiRPKcEEEvPNS_9AsyncWrapES3_DpOT_
	jmp	.L1685
	.cfi_endproc
.LFE11876:
	.text
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_
	.section	.text.unlikely
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_.cold, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_.cold
.LCOLDE70:
	.text
.LHOTE70:
	.section	.rodata._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC71:
	.string	"%lu"
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11599:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1708
	leaq	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1708:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC45(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1709:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1709
	cmpb	$120, %dl
	jg	.L1710
	cmpb	$99, %dl
	jg	.L1711
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L1712
	cmpb	$88, %dl
	je	.L1713
	jmp	.L1710
.L1711:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1710
	leaq	.L1715(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1715:
	.long	.L1716-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1716-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1718-.L1715
	.long	.L1717-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1716-.L1715
	.long	.L1710-.L1715
	.long	.L1716-.L1715
	.long	.L1710-.L1715
	.long	.L1710-.L1715
	.long	.L1714-.L1715
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1712:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1719
	call	_ZdlPv@PLT
.L1719:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1739
	jmp	.L1721
.L1710:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1744
	call	_ZdlPv@PLT
	jmp	.L1744
.L1716:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC71(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L1741
.L1718:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L1726:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1726
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1741
.L1714:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1741:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1738
	jmp	.L1725
.L1713:
	movq	(%r8), %rsi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1729
	call	_ZdlPv@PLT
.L1729:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1725
.L1738:
	call	_ZdlPv@PLT
	jmp	.L1725
.L1717:
	leaq	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1725:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1744:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1721
.L1739:
	call	_ZdlPv@PLT
.L1721:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1707
	call	_ZdlPv@PLT
.L1707:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1733
	call	__stack_chk_fail@PLT
.L1733:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11599:
	.size	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11429:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1746
	call	__stack_chk_fail@PLT
.L1746:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11429:
	.size	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_:
.LFB11213:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1748
	call	_ZdlPv@PLT
.L1748:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1750
	call	__stack_chk_fail@PLT
.L1750:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11213:
	.size	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB10220:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	call	*72(%rax)
	movq	%r14, %rsi
	leaq	.LC47(%rip), %rdx
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	-128(%rbp), %r14
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC48(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1753
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJRmEEEvP8_IO_FILEPKcDpOT_
.L1753:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1754
	call	_ZdlPv@PLT
.L1754:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1755
	call	_ZdlPv@PLT
.L1755:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1756
	call	_ZdlPv@PLT
.L1756:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1752
	call	_ZdlPv@PLT
.L1752:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1758
	call	__stack_chk_fail@PLT
.L1758:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10220:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC72:
	.string	"Receiving %zu bytes injected from JS"
	.section	.text.unlikely
	.align 2
.LCOLDB73:
	.text
.LHOTB73:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$184, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1822
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1796
	cmpw	$1040, %cx
	jne	.L1762
.L1796:
	movq	23(%rdx), %r15
.L1764:
	testq	%r15, %r15
	je	.L1760
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1823
	movq	8(%rbx), %r12
.L1767:
	movq	%r12, %rdi
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1824
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -72(%rbp)
	cmpq	$64, %rax
	ja	.L1771
	movq	%r12, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L1825
.L1771:
	movq	%r12, %rdi
	leaq	-208(%rbp), %r14
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-208(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	addq	%rax, %rbx
	movq	%rbx, -80(%rbp)
.L1770:
	movq	16(%r15), %rcx
	movslq	32(%r15), %rdx
	movq	-72(%rbp), %rax
	cmpb	$0, 2208(%rcx,%rdx)
	movq	%rax, -216(%rbp)
	jne	.L1818
.L1772:
	leaq	_ZThn248_N4node7TLSWrap9IsClosingEv(%rip), %r12
	testq	%rax, %rax
	je	.L1760
	.p2align 4,,10
	.p2align 3
.L1773:
	movq	(%r15), %rax
	leaq	_ZN4node7TLSWrap7IsAliveEv(%rip), %rcx
	movq	80(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1775
	cmpq	$0, 88(%r15)
	je	.L1760
	movq	320(%r15), %rdi
	testq	%rdi, %rdi
	je	.L1760
	movq	(%rdi), %rax
	leaq	_ZThn248_N4node7TLSWrap7IsAliveEv(%rip), %rsi
	movq	80(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1781
	cmpq	$0, -160(%rdi)
	je	.L1760
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1760
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rsi, %rax
	je	.L1826
.L1781:
	call	*%rax
.L1782:
	testb	%al, %al
	je	.L1760
.L1785:
	movq	(%r15), %rax
	leaq	_ZN4node7TLSWrap9IsClosingEv(%rip), %rcx
	movq	88(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1827
	movq	320(%r15), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%r12, %rax
	jne	.L1821
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%r12, %rax
	je	.L1828
.L1821:
	call	*%rax
.L1791:
	testb	%al, %al
	jne	.L1760
	movq	(%r15), %rax
	leaq	_ZN4node7TLSWrap13OnStreamAllocEm(%rip), %rcx
	movq	-216(%rbp), %rsi
	movq	176(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1792
	cmpq	$0, 88(%r15)
	je	.L1829
	movq	344(%r15), %rdi
	movq	%rsi, -208(%rbp)
	leaq	-208(%rbp), %r14
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO12PeekWritableEPm@PLT
	movl	-208(%rbp), %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	movq	%rax, -208(%rbp)
	movq	%rax, %rdi
	movq	%rdx, -200(%rbp)
.L1794:
	cmpq	%rdx, -216(%rbp)
	cmovbe	-216(%rbp), %rdx
	movq	%rbx, %rsi
	movq	%rdx, %r13
	call	memcpy@PLT
	movq	(%r15), %rax
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r13, -200(%rbp)
	movq	%r15, %rdi
	addq	%r13, %rbx
	call	*184(%rax)
	subq	%r13, -216(%rbp)
	jne	.L1773
	.p2align 4,,10
	.p2align 3
.L1760:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1830
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1823:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	jmp	.L1767
	.p2align 4,,10
	.p2align 3
.L1775:
	movq	%r15, %rdi
	call	*%rax
	testb	%al, %al
	jne	.L1785
	jmp	.L1760
	.p2align 4,,10
	.p2align 3
.L1827:
	movq	%r15, %rdi
	jmp	.L1821
	.p2align 4,,10
	.p2align 3
.L1792:
	movq	%r15, %rdi
	leaq	-208(%rbp), %r14
	call	*%rax
	movq	%rax, -208(%rbp)
	movq	-208(%rbp), %rdi
	movq	%rdx, -200(%rbp)
	movq	-200(%rbp), %rdx
	jmp	.L1794
	.p2align 4,,10
	.p2align 3
.L1829:
	leaq	_ZZN4node7TLSWrap13OnStreamAllocEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1762:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L1764
	.p2align 4,,10
	.p2align 3
.L1825:
	leaq	-144(%rbp), %rbx
	movl	$64, %edx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%rbx, -80(%rbp)
	jmp	.L1770
	.p2align 4,,10
	.p2align 3
.L1822:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1824:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1828:
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%r12, %rax
	jne	.L1821
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%r12, %rax
	jne	.L1821
	movq	72(%rdi), %rdi
	movq	(%rdi), %rax
	movq	88(%rax), %rax
	cmpq	%r12, %rax
	jne	.L1821
	subq	$248, %rdi
	call	.LTHUNK11
	jmp	.L1791
.L1826:
	cmpq	$0, -160(%rdi)
	je	.L1760
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1760
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1781
	cmpq	$0, -160(%rdi)
	je	.L1760
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1760
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1781
	cmpq	$0, -160(%rdi)
	je	.L1760
	movq	72(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1760
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1781
	subq	$248, %rdi
	call	.LTHUNK10
	jmp	.L1782
.L1830:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold, @function
_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold:
.LFSB8761:
.L1818:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	-216(%rbp), %rdx
	leaq	.LC72(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-216(%rbp), %rax
	jmp	.L1772
	.cfi_endproc
.LFE8761:
	.text
	.size	_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold, .-_ZN4node7TLSWrap7ReceiveERKN2v820FunctionCallbackInfoINS1_5ValueEEE.cold
.LCOLDE73:
	.text
.LHOTE73:
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11699:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1832
	leaq	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1832:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC45(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1833:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1833
	cmpb	$120, %dl
	jg	.L1834
	cmpb	$99, %dl
	jg	.L1835
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L1836
	cmpb	$88, %dl
	je	.L1837
	jmp	.L1834
.L1835:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1834
	leaq	.L1839(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1839:
	.long	.L1840-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1840-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1842-.L1839
	.long	.L1841-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1840-.L1839
	.long	.L1834-.L1839
	.long	.L1840-.L1839
	.long	.L1834-.L1839
	.long	.L1834-.L1839
	.long	.L1838-.L1839
	.section	.text.unlikely._ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1836:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1843
	call	_ZdlPv@PLT
.L1843:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1863
	jmp	.L1845
.L1834:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1868
	call	_ZdlPv@PLT
	jmp	.L1868
.L1840:
	movl	(%r8), %r8d
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC63(%rip), %rcx
	movl	$16, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L1865
.L1842:
	movb	$0, -57(%rbp)
	movslq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L1850:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1850
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1865
.L1838:
	movl	(%r8), %esi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1865:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1862
	jmp	.L1849
.L1837:
	movl	(%r8), %esi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EiLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1853
	call	_ZdlPv@PLT
.L1853:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1849
.L1862:
	call	_ZdlPv@PLT
	jmp	.L1849
.L1841:
	leaq	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1849:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1868:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1845
.L1863:
	call	_ZdlPv@PLT
.L1845:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1831
	call	_ZdlPv@PLT
.L1831:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1857
	call	__stack_chk_fail@PLT
.L1857:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11699:
	.size	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11497:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1870
	call	__stack_chk_fail@PLT
.L1870:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11497:
	.size	_ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_:
.LFB11297:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1872
	call	_ZdlPv@PLT
.L1872:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1874
	call	__stack_chk_fail@PLT
.L1874:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11297:
	.size	_ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB10229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	call	*72(%rax)
	movq	%r14, %rsi
	leaq	.LC47(%rip), %rdx
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	-128(%rbp), %r14
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC48(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1877
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJRiEEEvP8_IO_FILEPKcDpOT_
.L1877:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1878
	call	_ZdlPv@PLT
.L1878:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1879
	call	_ZdlPv@PLT
.L1879:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1880
	call	_ZdlPv@PLT
.L1880:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1876
	call	_ZdlPv@PLT
.L1876:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1882
	call	__stack_chk_fail@PLT
.L1882:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10229:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11700:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1885
	leaq	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1885:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC45(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1886:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1886
	cmpb	$120, %dl
	jg	.L1887
	cmpb	$99, %dl
	jg	.L1888
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L1889
	cmpb	$88, %dl
	je	.L1890
	jmp	.L1887
.L1888:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1887
	leaq	.L1892(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1892:
	.long	.L1893-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1893-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1895-.L1892
	.long	.L1894-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1893-.L1892
	.long	.L1887-.L1892
	.long	.L1893-.L1892
	.long	.L1887-.L1892
	.long	.L1887-.L1892
	.long	.L1891-.L1892
	.section	.text.unlikely._ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1889:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1896
	call	_ZdlPv@PLT
.L1896:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L1916
	jmp	.L1898
.L1887:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1921
	call	_ZdlPv@PLT
	jmp	.L1921
.L1893:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC71(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L1918
.L1895:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L1903:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L1903
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L1918
.L1891:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L1918:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1915
	jmp	.L1902
.L1890:
	movq	(%r8), %rsi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L1906
	call	_ZdlPv@PLT
.L1906:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1902
.L1915:
	call	_ZdlPv@PLT
	jmp	.L1902
.L1894:
	leaq	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1902:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L1921:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1898
.L1916:
	call	_ZdlPv@PLT
.L1898:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1884
	call	_ZdlPv@PLT
.L1884:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1910
	call	__stack_chk_fail@PLT
.L1910:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11700:
	.size	_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11498:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1923
	call	__stack_chk_fail@PLT
.L1923:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11498:
	.size	_ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_:
.LFB11298:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1925
	call	_ZdlPv@PLT
.L1925:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1927
	call	__stack_chk_fail@PLT
.L1927:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11298:
	.size	_ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB10240:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r15, %rdi
	call	*72(%rax)
	leaq	-160(%rbp), %r8
	movq	%r15, %rsi
	leaq	.LC47(%rip), %rdx
	movq	%r8, %rdi
	movq	%r8, -200(%rbp)
	leaq	-128(%rbp), %r15
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	-200(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC48(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1930
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJmRiEEEvP8_IO_FILEPKcDpOT_
.L1930:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1931
	call	_ZdlPv@PLT
.L1931:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1932
	call	_ZdlPv@PLT
.L1932:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1933
	call	_ZdlPv@PLT
.L1933:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1929
	call	_ZdlPv@PLT
.L1929:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L1935
	call	__stack_chk_fail@PLT
.L1935:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10240:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"Trying to write cleartext input"
	.align 8
.LC75:
	.string	"Returning from ClearIn(), hello_parser_ active"
	.align 8
.LC76:
	.string	"Returning from ClearIn(), ssl_ == nullptr"
	.align 8
.LC77:
	.string	"Returning from ClearIn(), no pending data"
	.align 8
.LC78:
	.string	"Writing %zu bytes, written = %d"
	.align 8
.LC79:
	.string	"Successfully wrote all data to SSL"
	.section	.rodata.str1.1
.LC80:
	.string	"Got SSL error (%d)"
.LC81:
	.string	"Pushing data back"
	.section	.text.unlikely
	.align 2
.LCOLDB82:
	.text
.LHOTB82:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap7ClearInEv
	.type	_ZN4node7TLSWrap7ClearInEv, @function
_ZN4node7TLSWrap7ClearInEv:
.LFB8781:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1976
	cmpl	$3, 128(%rdi)
	jne	.L1937
	cmpq	$0, 88(%rdi)
	je	.L1937
.L1942:
	cmpq	$0, 376(%r14)
	jne	.L1943
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1977
.L1937:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1983
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1943:
	.cfi_restore_state
	leaq	360(%r14), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -184(%rbp)
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %rbx
	call	uv_buf_init@PLT
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	testq	%rbx, %rbx
	je	.L1944
.L1950:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1944:
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	376(%r14), %r12
	movq	360(%r14), %rbx
	movq	368(%r14), %r13
	call	uv_buf_init@PLT
	movq	%rax, 368(%r14)
	movq	%rdx, 376(%r14)
	call	ERR_set_mark@PLT
	movq	352(%r14), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	cmpq	$16383, %r12
	jbe	.L1945
	movq	%r12, %rdx
	shrq	$14, %rdx
	addq	$1, %rdx
	imulq	$16421, %rdx, %rdx
	movq	%rdx, 32(%rax)
.L1945:
	movq	88(%r14), %rdi
	movl	%r12d, %edx
	movq	%r13, %rsi
	movl	%r12d, %r15d
	call	SSL_write@PLT
	movq	16(%r14), %rcx
	movslq	32(%r14), %rdx
	movq	%r12, -128(%rbp)
	movl	%eax, -168(%rbp)
	cmpb	$0, 2208(%rcx,%rdx)
	jne	.L1978
.L1946:
	cmpl	$-1, %eax
	jne	.L1984
	movq	16(%r14), %rax
	movq	352(%rax), %rsi
	leaq	-128(%rbp), %rax
	movq	%rax, %rdi
	movq	%rax, -192(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	16(%r14), %rax
	movq	3280(%rax), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	leaq	-80(%rbp), %rax
	leaq	-96(%rbp), %rcx
	movq	%r14, %rdi
	movl	-168(%rbp), %esi
	leaq	-164(%rbp), %rdx
	movq	%rax, -96(%rbp)
	movq	%rax, -184(%rbp)
	movq	$0, -88(%rbp)
	movb	$0, -80(%rbp)
	movq	%rdx, -200(%rbp)
	call	_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	testq	%rax, %rax
	je	.L1951
	movq	16(%r14), %rcx
	movslq	32(%r14), %rax
	movq	-200(%rbp), %rdx
	cmpb	$0, 2208(%rcx,%rax)
	jne	.L1980
.L1952:
	movb	$1, 416(%r14)
	movq	-96(%rbp), %rdx
	movl	$-71, %esi
	movq	%r14, %rdi
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
.L1953:
	movq	-96(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1956
	call	_ZdlPv@PLT
.L1956:
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-192(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	call	ERR_pop_to_mark@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	testq	%r13, %r13
	je	.L1937
	testq	%rbx, %rbx
	je	.L1950
	movq	360(%rbx), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L1937
	.p2align 4,,10
	.p2align 3
.L1951:
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1981
.L1954:
	movq	368(%r14), %r9
	movq	376(%r14), %r10
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r9, -200(%rbp)
	movq	%r10, -208(%rbp)
	call	uv_buf_init@PLT
	movq	-200(%rbp), %r9
	movq	%rax, 368(%r14)
	movq	%rdx, 376(%r14)
	testq	%r9, %r9
	je	.L1955
	movq	360(%r14), %rax
	testq	%rax, %rax
	je	.L1950
	movq	360(%rax), %rax
	movq	-208(%rbp), %r10
	movq	%r9, %rsi
	movq	2368(%rax), %rdi
	movq	%r10, %rdx
	movq	(%rdi), %rax
	call	*32(%rax)
.L1955:
	movq	%rbx, 360(%r14)
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%r13, 368(%r14)
	movq	%r12, 376(%r14)
	movq	%rax, %r13
	movq	%rdx, %r12
	jmp	.L1953
	.p2align 4,,10
	.p2align 3
.L1984:
	cmpl	%eax, %r15d
	jne	.L1985
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L1979
.L1949:
	call	ERR_pop_to_mark@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	testq	%r13, %r13
	je	.L1937
	testq	%rbx, %rbx
	je	.L1950
	movq	360(%rbx), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L1937
.L1985:
	leaq	_ZZN4node7TLSWrap7ClearInEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1983:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap7ClearInEv.cold, @function
_ZN4node7TLSWrap7ClearInEv.cold:
.LFSB8781:
.L1979:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC79(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1949
.L1977:
	leaq	.LC77(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1937
.L1981:
	leaq	.LC81(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1954
.L1978:
	leaq	-168(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	leaq	.LC78(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	movl	-168(%rbp), %eax
	jmp	.L1946
.L1980:
	leaq	.LC80(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1952
.L1976:
	leaq	.LC74(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	cmpl	$3, 128(%r14)
	je	.L1939
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1937
	leaq	.LC75(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1937
.L1939:
	cmpq	$0, 88(%r14)
	jne	.L1942
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L1937
	leaq	.LC76(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L1937
	.cfi_endproc
.LFE8781:
	.text
	.size	_ZN4node7TLSWrap7ClearInEv, .-_ZN4node7TLSWrap7ClearInEv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap7ClearInEv.cold, .-_ZN4node7TLSWrap7ClearInEv.cold
.LCOLDE82:
	.text
.LHOTE82:
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L1987
	leaq	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1987:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC45(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L1988:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L1988
	cmpb	$120, %dl
	jg	.L1989
	cmpb	$99, %dl
	jg	.L1990
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L1991
	cmpb	$88, %dl
	je	.L1992
	jmp	.L1989
.L1990:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L1989
	leaq	.L1994(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L1994:
	.long	.L1995-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1995-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1997-.L1994
	.long	.L1996-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1995-.L1994
	.long	.L1989-.L1994
	.long	.L1995-.L1994
	.long	.L1989-.L1994
	.long	.L1989-.L1994
	.long	.L1993-.L1994
	.section	.text.unlikely._ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L1991:
	movq	%r8, %rdx
	movq	%r14, %rcx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r10, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-208(%rbp), %r10
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L1998
	call	_ZdlPv@PLT
.L1998:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2018
	jmp	.L2000
.L1989:
	leaq	-112(%rbp), %r15
	movq	%r14, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r15, %rdi
	leaq	-144(%rbp), %r14
	call	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2023
	call	_ZdlPv@PLT
	jmp	.L2023
.L1995:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC71(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L2020
.L1997:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2005:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2005
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2020
.L1993:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2020:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2017
	jmp	.L2004
.L1992:
	movq	(%r8), %rsi
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	-208(%rbp), %r10
	movq	%r15, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2008
	call	_ZdlPv@PLT
.L2008:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2004
.L2017:
	call	_ZdlPv@PLT
	jmp	.L2004
.L1996:
	leaq	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2004:
	leaq	-112(%rbp), %r8
	leaq	2(%rbx), %rsi
	movq	%r14, %rdx
	movq	%r8, %rdi
	movq	%r8, -192(%rbp)
	call	_ZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-192(%rbp), %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r8, %rdx
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2023:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2000
.L2018:
	call	_ZdlPv@PLT
.L2000:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L1986
	call	_ZdlPv@PLT
.L1986:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2012
	call	__stack_chk_fail@PLT
.L2012:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11740:
	.size	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2025
	call	__stack_chk_fail@PLT
.L2025:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11525:
	.size	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_:
.LFB11329:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRmRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2027
	call	_ZdlPv@PLT
.L2027:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2029
	call	__stack_chk_fail@PLT
.L2029:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11329:
	.size	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB10242:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-192(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r15, %rdi
	call	*72(%rax)
	leaq	-160(%rbp), %r8
	movq	%r15, %rsi
	leaq	.LC47(%rip), %rdx
	movq	%r8, %rdi
	movq	%r8, -200(%rbp)
	leaq	-128(%rbp), %r15
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	-200(%rbp), %r8
	movq	%r14, %rdx
	movq	%r15, %rdi
	movq	%r8, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC48(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2032
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJRmRiEEEvP8_IO_FILEPKcDpOT_
.L2032:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2033
	call	_ZdlPv@PLT
.L2033:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2034
	call	_ZdlPv@PLT
.L2034:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2035
	call	_ZdlPv@PLT
.L2035:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2031
	call	_ZdlPv@PLT
.L2031:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2037
	call	__stack_chk_fail@PLT
.L2037:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10242:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.1
.LC83:
	.string	"DoWrite()"
.LC84:
	.string	"Write after DestroySSL"
.LC85:
	.string	"Empty write"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"No pending encrypted output, writing to underlying stream"
	.align 8
.LC87:
	.string	"Got SSL error (%d), returning UV_EPROTO"
	.section	.rodata.str1.1
.LC88:
	.string	"Saving data for later write"
	.section	.text.unlikely
	.align 2
.LCOLDB89:
	.text
.LHOTB89:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB8792:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%r8, %r8
	jne	.L2146
	movslq	32(%rdi), %rax
	movq	%rdx, %r12
	movq	16(%rdi), %rdx
	movq	%rdi, %r14
	movq	%rsi, %r9
	movq	%rcx, %rbx
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2139
.L2041:
	cmpq	$0, 88(%r14)
	je	.L2147
	movq	$0, -128(%rbp)
	testq	%rbx, %rbx
	je	.L2102
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2048:
	movq	%rax, %rdx
	salq	$4, %rdx
	movq	8(%r12,%rdx), %rdx
	addq	%rdx, %rcx
	testq	%rdx, %rdx
	je	.L2047
	addq	$1, %r15
	movq	%rax, %r8
.L2047:
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L2048
	movq	%rcx, -128(%rbp)
	testq	%rcx, %rcx
	je	.L2046
.L2049:
	movq	392(%r14), %rax
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	jne	.L2148
	cmpq	$0, -128(%rbp)
	movq	%r9, 392(%r14)
	je	.L2149
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%r8, -152(%rbp)
	call	uv_buf_init@PLT
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	movq	%rax, -160(%rbp)
	movq	%rdx, -144(%rbp)
	call	ERR_set_mark@PLT
	cmpq	$1, %r15
	movl	$0, -120(%rbp)
	movq	-152(%rbp), %r8
	je	.L2075
	movq	16(%r14), %rax
	movq	-128(%rbp), %r13
	movq	%rax, -136(%rbp)
	movq	360(%rax), %rax
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2087
	movl	%r13d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -152(%rbp)
	movq	%rax, %r13
	movq	%rdx, -144(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -160(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	je	.L2077
.L2088:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2102:
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
.L2046:
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2140
.L2050:
	movq	%r14, %rdi
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZN4node7TLSWrap8ClearOutEv
	movq	352(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	BIO_ctrl@PLT
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %r9
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L2049
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2141
.L2051:
	cmpq	$0, 408(%r14)
	jne	.L2150
	movq	%r9, 408(%r14)
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	%rbx, %rcx
	movq	320(%r14), %rsi
	leaq	-80(%rbp), %rdi
	movq	%r12, %rdx
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	cmpb	$0, -80(%rbp)
	jne	.L2039
	leaq	-120(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_
	movq	-120(%rbp), %rsi
	leaq	-104(%rbp), %rdi
	movq	16(%r14), %rbx
	movq	%r14, -112(%rbp)
	call	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-112(%rbp), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_EE(%rip), %rcx
	movb	$1, 8(%rax)
	movq	%rdx, 24(%rax)
	movq	-104(%rbp), %rdx
	movq	$0, 16(%rax)
	movq	%rdx, 32(%rax)
	movq	2480(%rbx), %rdx
	movq	%rcx, (%rax)
	movq	$0, -104(%rbp)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L2054
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L2056
.L2145:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2056:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L2151
.L2058:
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L2060
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2068
	movl	(%rcx), %edx
	testl	%edx, %edx
	je	.L2069
	subl	$1, %edx
	movl	%edx, (%rcx)
	jne	.L2060
	cmpb	$0, 9(%rcx)
	je	.L2064
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
.L2060:
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L2039
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2068
	movl	(%rcx), %edx
	testl	%edx, %edx
	je	.L2069
	subl	$1, %edx
	movl	%edx, (%rcx)
	jne	.L2039
	cmpb	$0, 9(%rcx)
	je	.L2071
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2149:
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN4node7TLSWrap6EncOutEv
.L2039:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2152
	addq	$120, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L2077:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	movq	%rdx, %rdi
	movq	%rax, -72(%rbp)
	movq	%rax, %r15
	movq	%rdi, -64(%rbp)
	movq	%rdi, -160(%rbp)
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r15, %r15
	je	.L2078
	movq	-136(%rbp), %rax
	movq	-160(%rbp), %r9
	movq	%r15, %rsi
	movq	360(%rax), %rax
	movq	%r9, %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2078:
	testq	%rbx, %rbx
	je	.L2082
	salq	$4, %rbx
	xorl	%r15d, %r15d
	leaq	(%r12,%rbx), %rbx
	.p2align 4,,10
	.p2align 3
.L2083:
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	leaq	0(%r13,%r15), %rdi
	addq	$16, %r12
	call	memcpy@PLT
	addq	-8(%r12), %r15
	cmpq	%r12, %rbx
	jne	.L2083
.L2082:
	movq	352(%r14), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	-128(%rbp), %rdx
	cmpq	$16383, %rdx
	ja	.L2080
.L2081:
	movq	88(%r14), %rdi
	movq	%r13, %rsi
	call	SSL_write@PLT
	movl	%eax, -120(%rbp)
.L2084:
	cmpl	$-1, %eax
	jne	.L2153
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2154
.L2100:
	leaq	-80(%rbp), %r12
	leaq	424(%r14), %rcx
	movl	$-1, %esi
	movq	%r14, %rdi
	movq	%r12, %rdx
	call	_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	16(%r14), %rdx
	testq	%rax, %rax
	movslq	32(%r14), %rax
	je	.L2093
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2142
.L2094:
	movq	$0, 392(%r14)
	movl	$-71, %r13d
	.p2align 4,,10
	.p2align 3
.L2095:
	call	ERR_pop_to_mark@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-152(%rbp), %rsi
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	testq	%rsi, %rsi
	je	.L2039
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L2088
	movq	360(%rax), %rax
	movq	-144(%rbp), %rdx
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L2039
	.p2align 4,,10
	.p2align 3
.L2080:
	movq	%rdx, %rcx
	shrq	$14, %rcx
	addq	$1, %rcx
	imulq	$16421, %rcx, %rcx
	movq	%rcx, 32(%rax)
	jmp	.L2081
	.p2align 4,,10
	.p2align 3
.L2075:
	salq	$4, %r8
	movq	352(%r14), %rdi
	addq	%r8, %r12
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	8(%r12), %rdx
	cmpq	$16383, %rdx
	jbe	.L2085
	shrq	$14, %rdx
	addq	$1, %rdx
	imulq	$16421, %rdx, %rdx
	movq	%rdx, 32(%rax)
	movq	8(%r12), %rdx
.L2085:
	movq	88(%r14), %rdi
	movq	(%r12), %rsi
	call	SSL_write@PLT
	movl	%eax, -120(%rbp)
	cmpl	$-1, %eax
	je	.L2155
.L2086:
	cmpl	-128(%rbp), %eax
	jne	.L2156
	movq	16(%r14), %rdx
	movslq	32(%r14), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2099
	movq	-160(%rbp), %rax
	movq	%rax, -152(%rbp)
	jmp	.L2092
	.p2align 4,,10
	.p2align 3
.L2146:
	leaq	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2148:
	leaq	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2093:
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2143
.L2096:
	cmpq	$0, 376(%r14)
	jne	.L2157
	movq	368(%r14), %r12
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, 368(%r14)
	movq	%rdx, 376(%r14)
	testq	%r12, %r12
	je	.L2098
	movq	360(%r14), %rax
	testq	%rax, %rax
	je	.L2088
	movq	360(%rax), %rax
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2098:
	movq	-136(%rbp), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, 360(%r14)
	call	uv_buf_init@PLT
	movq	-152(%rbp), %rbx
	movq	%rax, -104(%rbp)
	movq	%rbx, 368(%r14)
	movq	-144(%rbp), %rbx
	movq	%rdx, -96(%rbp)
	movq	%rbx, 376(%r14)
	movq	%rdx, -144(%rbp)
	movq	%rax, -152(%rbp)
.L2092:
	movb	$1, 400(%r14)
	movq	%r14, %rdi
	xorl	%r13d, %r13d
	call	_ZN4node7TLSWrap6EncOutEv
	movb	$0, 400(%r14)
	jmp	.L2095
.L2054:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L2145
	jmp	.L2056
.L2157:
	leaq	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2156:
	leaq	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2087:
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2155:
	movq	16(%r14), %rax
	movq	-128(%rbp), %r13
	movq	%rax, -136(%rbp)
	movq	360(%rax), %rax
	movq	%r13, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L2087
	movl	%r13d, %esi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, -152(%rbp)
	movq	%rax, %r13
	movq	%rdx, -144(%rbp)
	call	uv_buf_init@PLT
	cmpq	$0, -160(%rbp)
	movq	%rax, -104(%rbp)
	movq	%rdx, -96(%rbp)
	jne	.L2088
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	xorl	%edi, %edi
	movq	%rdx, %rsi
	movq	%rax, -72(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rax, -160(%rbp)
	call	uv_buf_init@PLT
	movq	-160(%rbp), %r8
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r8, %r8
	je	.L2089
	movq	-136(%rbp), %rax
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	360(%rax), %rax
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L2089:
	movq	(%r12), %rsi
	movq	8(%r12), %rdx
	movq	%r13, %rdi
	call	memcpy@PLT
	movl	-120(%rbp), %eax
	jmp	.L2084
.L2150:
	leaq	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2151:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	jmp	.L2058
.L2064:
	cmpb	$0, 8(%rcx)
	je	.L2060
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2060
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rax, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2060
.L2068:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2069:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2071:
	cmpb	$0, 8(%rcx)
	je	.L2039
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2039
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rax, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2039
.L2147:
	movq	(%r14), %rax
	leaq	_ZN4node7TLSWrap10ClearErrorEv(%rip), %rdx
	movq	168(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2043
	movq	424(%r14), %rax
	movq	$0, 432(%r14)
	movb	$0, (%rax)
.L2044:
	movq	432(%r14), %rdx
	leaq	424(%r14), %rdi
	movl	$22, %r8d
	leaq	.LC84(%rip), %rcx
	xorl	%esi, %esi
	movl	$-71, %r13d
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L2039
.L2043:
	movq	%r14, %rdi
	call	*%rax
	jmp	.L2044
.L2152:
	call	__stack_chk_fail@PLT
.L2153:
	movq	-152(%rbp), %rbx
	movq	%rbx, -160(%rbp)
	jmp	.L2086
.L2154:
	jmp	.L2104
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s.cold, @function
_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s.cold:
.LFSB8792:
.L2143:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC88(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2096
.L2140:
	leaq	.LC85(%rip), %rsi
	movq	%r14, %rdi
	movq	%r9, -144(%rbp)
	movq	%r8, -136(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %r9
	jmp	.L2050
.L2141:
	leaq	.LC86(%rip), %rsi
	movq	%r14, %rdi
	movq	%r9, -136(%rbp)
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-136(%rbp), %r9
	jmp	.L2051
.L2139:
	movq	%rsi, -136(%rbp)
	leaq	.LC83(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-136(%rbp), %r9
	jmp	.L2041
.L2142:
	movq	%r12, %rdx
	leaq	.LC87(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2094
.L2104:
	movq	-152(%rbp), %rax
	movq	%rax, -160(%rbp)
.L2099:
	leaq	-120(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	movq	%r14, %rdi
	leaq	.LC78(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmRiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-160(%rbp), %rax
	cmpl	$-1, -120(%rbp)
	movq	%rax, -152(%rbp)
	jne	.L2092
	jmp	.L2100
	.cfi_endproc
.LFE8792:
	.text
	.size	_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s.cold, .-_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s.cold
.LCOLDE89:
	.text
.LHOTE89:
	.set	.LTHUNK16,_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.type	_ZThn248_N4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, @function
_ZThn248_N4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s:
.LFB12161:
	.cfi_startproc
	endbr64
	subq	$248, %rdi
	jmp	.LTHUNK16
	.cfi_endproc
.LFE12161:
	.size	_ZThn248_N4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s, .-_ZThn248_N4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.section	.text._ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE,"axG",@progbits,_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	.type	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE, @function
_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE:
.LFB8734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%r9, %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$136, %rsp
	movq	%rdx, -104(%rbp)
	movq	32(%rsi), %r14
	movq	%rcx, -112(%rbp)
	movq	%r8, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r9, -120(%rbp)
	testq	%rcx, %rcx
	je	.L2159
	leaq	-1(%rcx), %rcx
	cmpq	$4, %rcx
	jbe	.L2193
	movq	%rcx, %rsi
	movq	%rdx, %rax
	pxor	%xmm0, %xmm0
	shrq	%rsi
	salq	$5, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L2162:
	movdqu	8(%rax), %xmm1
	movdqu	24(%rax), %xmm2
	addq	$32, %rax
	punpcklqdq	%xmm2, %xmm1
	paddq	%xmm1, %xmm0
	cmpq	%rsi, %rax
	jne	.L2162
	movdqa	%xmm0, %xmm1
	andq	$-2, %rcx
	psrldq	$8, %xmm1
	paddq	%xmm1, %xmm0
	movq	%xmm0, %rax
.L2160:
	movq	%rcx, %rsi
	salq	$4, %rsi
	addq	8(%rdx,%rsi), %rax
	leaq	1(%rcx), %rsi
	cmpq	%rbx, %rsi
	jnb	.L2218
	salq	$4, %rsi
	addq	8(%rdx,%rsi), %rax
	leaq	2(%rcx), %rsi
	cmpq	%rbx, %rsi
	jnb	.L2218
	salq	$4, %rsi
	addq	8(%rdx,%rsi), %rax
	leaq	3(%rcx), %rsi
	cmpq	%rbx, %rsi
	jnb	.L2218
	salq	$4, %rsi
	addq	$4, %rcx
	addq	8(%rdx,%rsi), %rax
	cmpq	%rbx, %rcx
	jnb	.L2218
	salq	$4, %rcx
	addq	8(%rdx,%rcx), %rax
.L2218:
	movq	%rax, %rbx
.L2159:
	addq	%rbx, 24(%r13)
	cmpq	$0, -128(%rbp)
	je	.L2219
.L2164:
	leaq	-80(%rbp), %rax
	movq	352(%r14), %rsi
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r15, %r15
	je	.L2220
.L2167:
	movq	0(%r13), %rax
	leaq	_ZThn248_N4node7TLSWrap12GetAsyncWrapEv(%rip), %rdx
	movq	128(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2170
	leaq	-248(%r13), %rax
.L2171:
	movsd	40(%rax), %xmm0
	movq	16(%rax), %rax
	movq	%rax, -136(%rbp)
	movq	1216(%rax), %rax
	movl	24(%rax), %ecx
	testl	%ecx, %ecx
	je	.L2172
	comisd	.LC90(%rip), %xmm0
	jb	.L2221
.L2172:
	movq	-136(%rbp), %rax
	leaq	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE(%rip), %rdx
	movq	1256(%rax), %rax
	movsd	24(%rax), %xmm3
	movsd	%xmm0, 24(%rax)
	movq	0(%r13), %rax
	movq	120(%rax), %rax
	movsd	%xmm3, -152(%rbp)
	cmpq	%rdx, %rax
	jne	.L2173
	movl	$96, %edi
	call	_Znwm@PLT
	movq	%rax, %r15
	leaq	16+_ZTVN4node9StreamReqE(%rip), %rax
	movq	%rax, (%r15)
	movq	-120(%rbp), %rax
	movq	%r13, 8(%r15)
	movq	(%rax), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2199
	cmpw	$1040, %cx
	jne	.L2174
.L2199:
	movq	31(%rdx), %rax
.L2176:
	testq	%rax, %rax
	jne	.L2222
	movq	-120(%rbp), %rdi
	movq	%r15, %rdx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	leaq	16+_ZTVN4node9WriteWrapE(%rip), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, (%r15)
	movq	$0, 16(%r15)
	call	uv_buf_init@PLT
	movq	32(%r13), %rsi
	leaq	40(%r15), %rdi
	movsd	.LC1(%rip), %xmm0
	movq	%rax, 24(%r15)
	movl	$39, %ecx
	movq	%rdx, 32(%r15)
	movq	-120(%rbp), %rdx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE(%rip), %rax
	movq	%rax, (%r15)
	addq	$72, %rax
	movq	%rax, 40(%r15)
.L2178:
	movq	0(%r13), %rax
	leaq	_ZThn248_N4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s(%rip), %rsi
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	48(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L2179
	movq	-128(%rbp), %r8
	leaq	-248(%r13), %rdi
	movq	%r15, %rsi
	call	.LTHUNK16
	movl	%eax, -128(%rbp)
.L2180:
	testl	%eax, %eax
	sete	-153(%rbp)
	jne	.L2223
.L2181:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*64(%rax)
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L2186
	movq	352(%r14), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L2224
.L2187:
	movq	360(%r14), %rax
	movq	3280(%r14), %rsi
	movq	-120(%rbp), %rdi
	movq	648(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L2225
.L2188:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*72(%rax)
.L2186:
	movzbl	-153(%rbp), %eax
	movsd	-152(%rbp), %xmm4
	movq	%r15, 8(%r12)
	movq	%rbx, 16(%r12)
	movb	%al, (%r12)
	movl	-128(%rbp), %eax
	movl	%eax, 4(%r12)
	movq	-136(%rbp), %rax
	movq	1256(%rax), %rax
	movsd	%xmm4, 24(%rax)
.L2189:
	movq	-144(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L2158:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2226
	addq	$136, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2219:
	.cfi_restore_state
	movq	0(%r13), %rax
	leaq	-112(%rbp), %rdx
	leaq	-104(%rbp), %rsi
	movq	%r13, %rdi
	call	*40(%rax)
	testl	%eax, %eax
	jne	.L2165
	cmpq	$0, -112(%rbp)
	jne	.L2164
.L2165:
	movb	$0, (%r12)
	movl	%eax, 4(%r12)
	movq	$0, 8(%r12)
	movq	%rbx, 16(%r12)
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	%r13, %rdi
	call	*%rax
	jmp	.L2171
	.p2align 4,,10
	.p2align 3
.L2223:
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	leaq	-88(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r15), %rax
	movq	%r15, %rdi
	call	*16(%rax)
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2182
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L2227
.L2182:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-88(%rbp), %r8
	movq	24(%r8), %rdx
	testq	%rdx, %rdx
	je	.L2228
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L2190
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L2185
	movq	(%r8), %rax
	movq	%r8, %rdi
	call	*8(%rax)
.L2185:
	xorl	%r15d, %r15d
	jmp	.L2181
	.p2align 4,,10
	.p2align 3
.L2173:
	movq	-120(%rbp), %rsi
	movq	%r13, %rdi
	call	*%rax
	movq	%rax, %r15
	jmp	.L2178
	.p2align 4,,10
	.p2align 3
.L2220:
	movq	3264(%r14), %rdi
	movq	3280(%r14), %rsi
	call	_ZN2v814ObjectTemplate11NewInstanceENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L2229
	movq	%rax, %r15
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	xorl	%edx, %edx
	movl	$1, %esi
	movq	%r15, %rdi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2179:
	movq	-128(%rbp), %r8
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	*%rax
	movl	%eax, -128(%rbp)
	jmp	.L2180
.L2193:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L2160
.L2229:
	movb	$0, (%r12)
	movl	$-16, 4(%r12)
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	jmp	.L2189
.L2227:
	movq	16(%rax), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L2182
.L2174:
	movq	-120(%rbp), %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L2176
.L2222:
	leaq	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2228:
	movl	$24, %edi
	movq	%r8, -120(%rbp)
	call	_Znwm@PLT
	movq	-120(%rbp), %r8
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r8), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L2198
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L2184:
	movb	%dl, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%rax, 24(%r8)
.L2190:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2221:
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2225:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L2188
.L2224:
	movq	%rax, -168(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-168(%rbp), %rcx
	jmp	.L2187
.L2198:
	xorl	%edx, %edx
	jmp	.L2184
.L2226:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8734:
	.size	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE, .-_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"Trying to write encrypted output"
	.align 8
.LC92:
	.string	"Returning from EncOut(), hello_parser_ active"
	.align 8
.LC93:
	.string	"Returning from EncOut(), write currently in progress"
	.align 8
.LC94:
	.string	"Returning from EncOut(), awaiting new session"
	.align 8
.LC95:
	.string	"EncOut() setting write_callback_scheduled_"
	.align 8
.LC96:
	.string	"Returning from EncOut(), ssl_ == nullptr"
	.section	.rodata.str1.1
.LC97:
	.string	"No pending encrypted output"
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"No pending cleartext input, not inside DoWrite()"
	.align 8
.LC99:
	.string	"No pending cleartext input, inside DoWrite()"
	.align 8
.LC100:
	.string	"Writing %zu buffers to the underlying stream"
	.section	.rodata.str1.1
.LC101:
	.string	"Write finished synchronously"
	.section	.text.unlikely
	.align 2
.LCOLDB102:
	.text
.LHOTB102:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap6EncOutEv
	.type	_ZN4node7TLSWrap6EncOutEv, @function
_ZN4node7TLSWrap6EncOutEv:
.LFB8765:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$488, %rsp
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2321
	cmpl	$3, 128(%rdi)
	jne	.L2230
	cmpq	$0, 384(%rdi)
	jne	.L2230
.L2235:
	cmpb	$0, 97(%r12)
	jne	.L2333
	cmpb	$0, 418(%r12)
	je	.L2237
	cmpq	$0, 392(%r12)
	je	.L2237
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2323
.L2238:
	movb	$1, 416(%r12)
.L2237:
	cmpq	$0, 88(%r12)
	je	.L2334
	movq	352(%r12), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L2240
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2325
.L2241:
	cmpq	$0, 376(%r12)
	jne	.L2230
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 400(%r12)
	movzbl	2208(%rdx,%rax), %eax
	jne	.L2242
	testb	%al, %al
	jne	.L2326
.L2243:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	.p2align 4,,10
	.p2align 3
.L2230:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2335
	addq	$488, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2334:
	.cfi_restore_state
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2230
	jmp	.L2324
	.p2align 4,,10
	.p2align 3
.L2333:
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2230
	jmp	.L2322
	.p2align 4,,10
	.p2align 3
.L2240:
	movq	352(%r12), %rdi
	leaq	-304(%rbp), %r14
	movq	$10, -496(%rbp)
	leaq	-384(%rbp), %r15
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	leaq	-496(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -520(%rbp)
	call	_ZN4node6crypto7NodeBIO12PeekMultipleEPPcPmS4_@PLT
	movq	%rax, 384(%r12)
	testq	%rax, %rax
	je	.L2264
	xorl	%ebx, %ebx
	cmpq	$0, -496(%rbp)
	je	.L2264
	.p2align 4,,10
	.p2align 3
.L2265:
	movq	(%r15,%rbx,8), %rdi
	movl	(%r14,%rbx,8), %esi
	movq	%rbx, %r13
	addq	$1, %rbx
	salq	$4, %r13
	call	uv_buf_init@PLT
	movq	-496(%rbp), %rcx
	movq	%rax, -224(%rbp,%r13)
	movq	%rdx, -216(%rbp,%r13)
	cmpq	%rbx, %rcx
	ja	.L2265
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2328
.L2266:
	movq	320(%r12), %rsi
	leaq	-448(%rbp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	leaq	-224(%rbp), %rdx
	call	_ZN4node10StreamBase5WriteEP8uv_buf_tmP11uv_stream_sN2v85LocalINS5_6ObjectEEE
	movl	-444(%rbp), %esi
	testl	%esi, %esi
	jne	.L2336
	cmpb	$0, -448(%rbp)
	jne	.L2230
	movq	16(%r12), %rax
	movslq	32(%r12), %rdx
	cmpb	$0, 2208(%rax,%rdx)
	jne	.L2329
.L2268:
	movq	352(%rax), %rsi
	leaq	-416(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	-488(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_
	movq	-488(%rbp), %rsi
	leaq	-456(%rbp), %rdi
	movq	16(%r12), %rbx
	movq	%r12, -464(%rbp)
	call	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-464(%rbp), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_EE(%rip), %rsi
	movb	$1, 8(%rax)
	movq	%rdx, 24(%rax)
	movq	-456(%rbp), %rdx
	movq	$0, 16(%rax)
	movq	%rdx, 32(%rax)
	movq	2480(%rbx), %rdx
	movq	%rsi, (%rax)
	movq	$0, -456(%rbp)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L2269
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L2271
.L2332:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2271:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L2337
.L2273:
	movq	-456(%rbp), %rdi
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	testq	%rdi, %rdi
	je	.L2275
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2259
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L2260
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L2275
	cmpb	$0, 9(%rdx)
	je	.L2277
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L2275:
	movq	-488(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2280
	movq	24(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L2259
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L2260
	subl	$1, %eax
	movl	%eax, (%rdx)
	jne	.L2280
	cmpb	$0, 9(%rdx)
	je	.L2282
	movq	(%rdi), %rax
	call	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L2280:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2336:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2242:
	testb	%al, %al
	jne	.L2327
.L2244:
	leaq	-504(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_
	movq	-504(%rbp), %rsi
	leaq	-472(%rbp), %rdi
	movq	16(%r12), %rbx
	movq	%r12, -480(%rbp)
	call	_ZN4node17BaseObjectPtrImplINS_7TLSWrapELb0EEC1EPS1_
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-480(%rbp), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_EE(%rip), %rsi
	movb	$1, 8(%rax)
	movq	%rdx, 24(%rax)
	movq	-472(%rbp), %rdx
	movq	$0, 16(%rax)
	movq	%rdx, 32(%rax)
	movq	2480(%rbx), %rdx
	movq	%rsi, (%rax)
	movq	$0, -472(%rbp)
	lock addq	$1, 2464(%rbx)
	movq	%rax, 2480(%rbx)
	testq	%rdx, %rdx
	je	.L2245
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L2247
.L2331:
	movq	(%rdi), %rax
	call	*8(%rax)
.L2247:
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L2338
.L2249:
	addl	$1, %eax
	movl	%eax, 4(%rdx)
	movq	-472(%rbp), %rax
	testq	%rax, %rax
	je	.L2251
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2259
	movl	(%rcx), %edx
	testl	%edx, %edx
	je	.L2260
	subl	$1, %edx
	movl	%edx, (%rcx)
	jne	.L2251
	cmpb	$0, 9(%rcx)
	je	.L2255
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	.p2align 4,,10
	.p2align 3
.L2251:
	movq	-504(%rbp), %rax
	testq	%rax, %rax
	je	.L2230
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2259
	movl	(%rcx), %edx
	testl	%edx, %edx
	je	.L2260
	subl	$1, %edx
	movl	%edx, (%rcx)
	jne	.L2230
	cmpb	$0, 9(%rcx)
	je	.L2262
	movq	(%rax), %rdx
	movq	%rax, %rdi
	call	*8(%rdx)
	jmp	.L2230
	.p2align 4,,10
	.p2align 3
.L2264:
	leaq	_ZZN4node7TLSWrap6EncOutEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2337:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	jmp	.L2273
.L2269:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L2332
	jmp	.L2271
.L2338:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN4node11Environment18ToggleImmediateRefEb@PLT
	movq	1312(%rbx), %rdx
	movl	4(%rdx), %eax
	jmp	.L2249
.L2245:
	movq	2472(%rbx), %rdi
	movq	%rax, 2472(%rbx)
	testq	%rdi, %rdi
	jne	.L2331
	jmp	.L2247
.L2259:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2260:
	leaq	_ZZN4node10BaseObject17decrease_refcountEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2277:
	cmpb	$0, 8(%rdx)
	je	.L2275
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L2275
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2275
.L2282:
	cmpb	$0, 8(%rdx)
	je	.L2280
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L2280
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r8, %rdi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2280
.L2255:
	cmpb	$0, 8(%rcx)
	je	.L2251
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2251
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rax, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2251
.L2262:
	cmpb	$0, 8(%rcx)
	je	.L2230
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.L2230
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%rax, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
	jmp	.L2230
.L2335:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap6EncOutEv.cold, @function
_ZN4node7TLSWrap6EncOutEv.cold:
.LFSB8765:
.L2321:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC91(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	cmpl	$3, 128(%r12)
	je	.L2232
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2230
	leaq	.LC92(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2230
.L2328:
	movq	-520(%rbp), %rdx
	leaq	.LC100(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	-496(%rbp), %rcx
	jmp	.L2266
.L2326:
	leaq	.LC98(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2243
.L2327:
	leaq	.LC99(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2244
.L2322:
	leaq	.LC94(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2230
.L2324:
	leaq	.LC96(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2230
.L2323:
	leaq	.LC95(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2238
.L2329:
	leaq	.LC101(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	16(%r12), %rax
	jmp	.L2268
.L2325:
	leaq	.LC97(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2241
.L2232:
	cmpq	$0, 384(%r12)
	je	.L2235
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2230
	leaq	.LC93(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2230
	.cfi_endproc
.LFE8765:
	.text
	.size	_ZN4node7TLSWrap6EncOutEv, .-_ZN4node7TLSWrap6EncOutEv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap6EncOutEv.cold, .-_ZN4node7TLSWrap6EncOutEv.cold
.LCOLDE102:
	.text
.LHOTE102:
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"OnStreamAfterWrite(status = %d)"
	.section	.rodata.str1.1
.LC104:
	.string	"Had empty write"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"ssl_ == nullptr, marking as cancelled"
	.section	.rodata.str1.1
.LC106:
	.string	"Ignoring error after shutdown"
	.section	.text.unlikely
	.align 2
.LCOLDB107:
	.text
.LHOTB107:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB8775:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$16, %rsp
	movl	%edx, -20(%rbp)
	movslq	32(%rdi), %rax
	movq	16(%rdi), %rdx
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2349
	movq	408(%rdi), %r13
	testq	%r13, %r13
	je	.L2352
.L2342:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	$0, 408(%r12)
	movl	-20(%rbp), %r12d
	call	*16(%rax)
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movl	%r12d, %esi
	call	*24(%rax)
.L2339:
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2352:
	.cfi_restore_state
	cmpq	$0, 88(%rdi)
	je	.L2345
.L2344:
	movl	-20(%rbp), %esi
	testl	%esi, %esi
	je	.L2347
	cmpb	$0, 419(%r12)
	je	.L2348
.L2353:
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2350
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2347:
	.cfi_restore_state
	movq	352(%r12), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	384(%r12), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO4ReadEPcm@PLT
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap7ClearInEv
	movq	%r12, %rdi
	movq	$0, 384(%r12)
	call	_ZN4node7TLSWrap6EncOutEv
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2345:
	.cfi_restore_state
	cmpb	$0, 419(%r12)
	movl	$-125, -20(%rbp)
	movl	$-125, %esi
	jne	.L2353
.L2348:
	movq	%r12, %rdi
	xorl	%edx, %edx
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	addq	$16, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold, @function
_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold:
.LFSB8775:
.L2349:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	-20(%rbp), %rdx
	leaq	.LC103(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	408(%r12), %r13
	testq	%r13, %r13
	je	.L2341
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2342
	leaq	.LC104(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	408(%r12), %r13
	jmp	.L2342
.L2350:
	leaq	.LC106(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2339
.L2341:
	cmpq	$0, 88(%r12)
	jne	.L2344
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2345
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2345
	.cfi_endproc
.LFE8775:
	.text
	.size	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold, .-_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold
.LCOLDE107:
	.text
.LHOTE107:
	.section	.rodata.str1.8
	.align 8
.LC108:
	.string	"Trying to read cleartext output"
	.align 8
.LC109:
	.string	"Returning from ClearOut(), hello_parser_ active"
	.align 8
.LC110:
	.string	"Returning from ClearOut(), EOF reached"
	.align 8
.LC111:
	.string	"Returning from ClearOut(), ssl_ == nullptr"
	.align 8
.LC112:
	.string	"Read %d bytes of cleartext output"
	.align 8
.LC113:
	.string	"Returning from read loop, ssl_ == nullptr"
	.align 8
.LC114:
	.string	"Got SSL error (%d), calling onerror"
	.section	.text.unlikely
	.align 2
.LCOLDB115:
	.text
.LHOTB115:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap8ClearOutEv
	.type	_ZN4node7TLSWrap8ClearOutEv, @function
_ZN4node7TLSWrap8ClearOutEv:
.LFB8780:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-16384(%rsp), %r11
.LPSRL0:
	subq	$4096, %rsp
	orq	$0, (%rsp)
	cmpq	%r11, %rsp
	jne	.LPSRL0
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movslq	32(%rdi), %rax
	movq	%rdi, %r15
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2393
	cmpl	$3, 128(%rdi)
	jne	.L2354
	cmpb	$0, 460(%rdi)
	jne	.L2354
.L2359:
	cmpq	$0, 88(%r15)
	je	.L2400
	call	ERR_set_mark@PLT
	leaq	-16448(%rbp), %rax
	movq	88(%r15), %rdi
	movq	%rax, -16504(%rbp)
	leaq	-16496(%rbp), %rax
	movq	%rax, -16512(%rbp)
.L2368:
	movq	-16504(%rbp), %rsi
	movl	$16384, %edx
	call	SSL_read@PLT
	movq	16(%r15), %rdx
	movl	%eax, -16496(%rbp)
	movl	%eax, %ebx
	movslq	32(%r15), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2395
.L2362:
	testl	%ebx, %ebx
	jle	.L2363
	movq	-16504(%rbp), %r12
	leaq	-16480(%rbp), %r13
	jmp	.L2369
	.p2align 4,,10
	.p2align 3
.L2365:
	movl	-16496(%rbp), %eax
	addq	%r14, %r12
	subl	%ebx, %eax
	movl	%eax, -16496(%rbp)
	movl	%eax, %ebx
	testl	%eax, %eax
	jle	.L2368
.L2369:
	movq	256(%r15), %rdi
	movslq	%ebx, %rsi
	movq	(%rdi), %rax
	call	*16(%rax)
	movq	%r12, %rsi
	cmpl	%edx, %ebx
	movq	%rdx, -16472(%rbp)
	movq	%rax, %rdi
	cmovge	%edx, %ebx
	movq	%rax, -16480(%rbp)
	movslq	%ebx, %r14
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	256(%r15), %rdi
	testq	%r14, %r14
	jle	.L2364
	addq	%r14, 264(%r15)
.L2364:
	movq	(%rdi), %rax
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	*24(%rax)
	movq	88(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L2365
	movq	16(%r15), %rdx
	movslq	32(%r15), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2396
.L2367:
	call	ERR_pop_to_mark@PLT
.L2354:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2401
	addq	$16472, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2400:
	.cfi_restore_state
	movq	16(%r15), %rdx
	movslq	32(%r15), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2354
	jmp	.L2394
	.p2align 4,,10
	.p2align 3
.L2363:
	movq	88(%r15), %rdi
	call	SSL_get_shutdown@PLT
	cmpb	$0, 460(%r15)
	jne	.L2370
	testb	$2, %al
	jne	.L2402
.L2370:
	movl	-16496(%rbp), %eax
	testl	%eax, %eax
	jle	.L2403
.L2371:
	call	ERR_pop_to_mark@PLT
	jmp	.L2354
.L2403:
	movq	16(%r15), %rax
	leaq	-16480(%rbp), %r13
	leaq	-16492(%rbp), %r12
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	-16496(%rbp), %esi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	cmpl	$6, -16492(%rbp)
	movq	%rax, -16488(%rbp)
	je	.L2404
.L2372:
	cmpq	$0, -16488(%rbp)
	je	.L2373
	movq	16(%r15), %rdx
	movslq	32(%r15), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2397
.L2374:
	movq	352(%r15), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$10, %esi
	call	BIO_ctrl@PLT
	testl	%eax, %eax
	jne	.L2405
.L2375:
	movq	16(%r15), %rax
	movq	8(%r15), %rdi
	movq	360(%rax), %rdx
	movq	1160(%rdx), %r12
	testq	%rdi, %rdi
	je	.L2376
	movzbl	11(%rdi), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L2406
.L2376:
	movq	3280(%rax), %rsi
	movq	%r12, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2373
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L2407
.L2373:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2371
.L2402:
	movb	$1, 460(%r15)
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	256(%r15), %rdi
	movq	$-4095, %rsi
	movq	%rax, -16480(%rbp)
	movq	(%rdi), %rax
	movq	%rdx, -16472(%rbp)
	leaq	-16480(%rbp), %rdx
	call	*24(%rax)
	jmp	.L2370
.L2404:
	cmpb	$0, 460(%r15)
	je	.L2372
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L2367
.L2405:
	movq	%r15, %rdi
	call	_ZN4node7TLSWrap6EncOutEv
	jmp	.L2375
.L2406:
	movq	352(%rax), %r9
	movq	(%rdi), %rsi
	movq	%r9, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	16(%r15), %rax
	jmp	.L2376
.L2407:
	leaq	-16488(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L2373
.L2401:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap8ClearOutEv.cold, @function
_ZN4node7TLSWrap8ClearOutEv.cold:
.LFSB8780:
.L2396:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	leaq	.LC113(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2367
.L2394:
	leaq	.LC111(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2354
.L2397:
	movq	%r12, %rdx
	leaq	.LC114(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2374
.L2395:
	movq	-16512(%rbp), %rdx
	leaq	.LC112(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	movl	-16496(%rbp), %ebx
	jmp	.L2362
.L2393:
	leaq	.LC108(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	cmpl	$3, 128(%r15)
	je	.L2356
	movq	16(%r15), %rdx
	movslq	32(%r15), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2354
	leaq	.LC109(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2354
.L2356:
	cmpb	$0, 460(%r15)
	je	.L2359
	movq	16(%r15), %rdx
	movslq	32(%r15), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2354
	leaq	.LC110(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2354
	.cfi_endproc
.LFE8780:
	.text
	.size	_ZN4node7TLSWrap8ClearOutEv, .-_ZN4node7TLSWrap8ClearOutEv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap8ClearOutEv.cold, .-_ZN4node7TLSWrap8ClearOutEv.cold
.LCOLDE115:
	.text
.LHOTE115:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB8762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L2418
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L2416
	cmpw	$1040, %cx
	jne	.L2410
.L2416:
	movq	23(%rdx), %r12
.L2412:
	testq	%r12, %r12
	je	.L2408
	cmpb	$0, 417(%r12)
	jne	.L2419
	movb	$1, 417(%r12)
	movl	72(%r12), %eax
	testl	%eax, %eax
	jne	.L2420
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap8ClearOutEv
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node7TLSWrap6EncOutEv
	.p2align 4,,10
	.p2align 3
.L2410:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L2412
	.p2align 4,,10
	.p2align 3
.L2408:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2418:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2419:
	leaq	_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2420:
	leaq	_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8762:
	.size	_ZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC116:
	.string	"DoShutdown()"
	.section	.text.unlikely
	.align 2
.LCOLDB117:
	.text
.LHOTB117:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE, @function
_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE:
.LFB8800:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	16(%rdi), %rdx
	movq	%rdi, %rbx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2428
.L2422:
	call	ERR_set_mark@PLT
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2424
	call	SSL_shutdown@PLT
	testl	%eax, %eax
	je	.L2430
.L2424:
	movb	$1, 419(%rbx)
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap6EncOutEv
	movq	320(%rbx), %rdi
	leaq	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2426
	movq	%r12, %rsi
	subq	$248, %rdi
	call	.LTHUNK20
	movl	%eax, %r12d
	call	ERR_pop_to_mark@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2426:
	.cfi_restore_state
	movq	%r12, %rsi
	call	*%rax
	movl	%eax, %r12d
	call	ERR_pop_to_mark@PLT
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2430:
	.cfi_restore_state
	movq	88(%rbx), %rdi
	call	SSL_shutdown@PLT
	jmp	.L2424
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold, @function
_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold:
.LFSB8800:
.L2428:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -32
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	.LC116(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2422
	.cfi_endproc
.LFE8800:
	.text
	.size	_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE, .-_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold, .-_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold
.LCOLDE117:
	.text
.LHOTE117:
	.set	.LTHUNK20,_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE
	.section	.rodata.str1.1
.LC118:
	.string	"OnClientHelloParseEnd()"
	.section	.text.unlikely
	.align 2
.LCOLDB119:
	.text
.LHOTB119:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap21OnClientHelloParseEndEPv
	.type	_ZN4node7TLSWrap21OnClientHelloParseEndEPv, @function
_ZN4node7TLSWrap21OnClientHelloParseEndEPv:
.LFB8810:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2436
.L2432:
	movl	456(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 456(%rbx)
	testl	%eax, %eax
	jne	.L2431
	.p2align 4,,10
	.p2align 3
.L2434:
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap7ClearInEv
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap8ClearOutEv
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap6EncOutEv
	movl	456(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 456(%rbx)
	testl	%eax, %eax
	jg	.L2434
.L2431:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap21OnClientHelloParseEndEPv.cold, @function
_ZN4node7TLSWrap21OnClientHelloParseEndEPv.cold:
.LFSB8810:
.L2436:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	.LC118(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2432
	.cfi_endproc
.LFE8810:
	.text
	.size	_ZN4node7TLSWrap21OnClientHelloParseEndEPv, .-_ZN4node7TLSWrap21OnClientHelloParseEndEPv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap21OnClientHelloParseEndEPv.cold, .-_ZN4node7TLSWrap21OnClientHelloParseEndEPv.cold
.LCOLDE119:
	.text
.LHOTE119:
	.section	.rodata.str1.1
.LC120:
	.string	"NewSessionDoneCb()"
	.section	.text.unlikely
	.align 2
.LCOLDB121:
	.text
.LHOTB121:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap16NewSessionDoneCbEv
	.type	_ZN4node7TLSWrap16NewSessionDoneCbEv, @function
_ZN4node7TLSWrap16NewSessionDoneCbEv:
.LFB8758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %rdx
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2443
.L2439:
	movl	456(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 456(%rbx)
	testl	%eax, %eax
	jne	.L2438
	.p2align 4,,10
	.p2align 3
.L2441:
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap7ClearInEv
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap8ClearOutEv
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap6EncOutEv
	movl	456(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 456(%rbx)
	testl	%eax, %eax
	jg	.L2441
.L2438:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap16NewSessionDoneCbEv.cold, @function
_ZN4node7TLSWrap16NewSessionDoneCbEv.cold:
.LFSB8758:
.L2443:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -24
	.cfi_offset 6, -16
	leaq	.LC120(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2439
	.cfi_endproc
.LFE8758:
	.text
	.size	_ZN4node7TLSWrap16NewSessionDoneCbEv, .-_ZN4node7TLSWrap16NewSessionDoneCbEv
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap16NewSessionDoneCbEv.cold, .-_ZN4node7TLSWrap16NewSessionDoneCbEv.cold
.LCOLDE121:
	.text
.LHOTE121:
	.section	.text.unlikely
.LCOLDB122:
	.text
.LHOTB122:
	.p2align 4
	.globl	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE
	.type	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE, @function
_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE:
.LFB12125:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	-248(%rdi), %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-232(%rdi), %rdx
	movslq	-216(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2452
.L2446:
	call	ERR_set_mark@PLT
	movq	-160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L2448
	call	SSL_shutdown@PLT
	testl	%eax, %eax
	je	.L2454
.L2448:
	movb	$1, 171(%rbx)
	movq	%r13, %rdi
	call	_ZN4node7TLSWrap6EncOutEv
	movq	72(%rbx), %rdi
	leaq	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE(%rip), %rdx
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	je	.L2455
	movq	%r12, %rsi
	call	*%rax
	movl	%eax, %r12d
.L2451:
	call	ERR_pop_to_mark@PLT
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2454:
	.cfi_restore_state
	movq	-160(%rbx), %rdi
	call	SSL_shutdown@PLT
	jmp	.L2448
.L2455:
	movq	%r12, %rsi
	subq	$248, %rdi
	call	.LTHUNK20
	movl	%eax, %r12d
	jmp	.L2451
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold, @function
_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold:
.LFSB12125:
.L2452:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	leaq	.LC116(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2446
	.cfi_endproc
.LFE12125:
	.text
	.size	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE, .-_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE
	.section	.text.unlikely
	.size	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold, .-_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE.cold
.LCOLDE122:
	.text
.LHOTE122:
	.section	.text.unlikely
.LCOLDB123:
	.text
.LHOTB123:
	.p2align 4
	.globl	_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB12126:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	-312(%rdi), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movl	%edx, %r12d
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	%edx, -44(%rbp)
	movslq	-280(%rdi), %rax
	movq	-296(%rdi), %rdx
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2468
	movq	96(%rdi), %r13
	testq	%r13, %r13
	je	.L2471
.L2460:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	$0, 96(%rbx)
	call	*16(%rax)
	movq	0(%r13), %rax
	movl	%r12d, %esi
	movq	%r13, %rdi
	call	*24(%rax)
.L2456:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2472
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2471:
	.cfi_restore_state
	cmpq	$0, -224(%rdi)
	je	.L2463
.L2462:
	movl	-44(%rbp), %esi
	testl	%esi, %esi
	je	.L2465
	cmpb	$0, 107(%rbx)
	je	.L2466
.L2473:
	movq	-296(%rbx), %rdx
	movslq	-280(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2456
	jmp	.L2469
	.p2align 4,,10
	.p2align 3
.L2465:
	movq	40(%rbx), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	72(%rbx), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO4ReadEPcm@PLT
	movq	%r14, %rdi
	call	_ZN4node7TLSWrap7ClearInEv
	movq	$0, 72(%rbx)
	movq	%r14, %rdi
	call	_ZN4node7TLSWrap6EncOutEv
	jmp	.L2456
	.p2align 4,,10
	.p2align 3
.L2463:
	cmpb	$0, 107(%rbx)
	movl	$-125, -44(%rbp)
	movl	$-125, %esi
	jne	.L2473
.L2466:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	jmp	.L2456
.L2472:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold, @function
_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold:
.LFSB12126:
.L2468:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	-44(%rbp), %rdx
	leaq	.LC103(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	96(%rbx), %r13
	testq	%r13, %r13
	je	.L2458
	movq	-296(%rbx), %rdx
	movslq	-280(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2459
	movl	-44(%rbp), %r12d
	jmp	.L2460
.L2469:
	leaq	.LC106(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2456
.L2458:
	cmpq	$0, -224(%rbx)
	jne	.L2462
	movq	-296(%rbx), %rdx
	movslq	-280(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2463
	leaq	.LC105(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2463
.L2459:
	leaq	.LC104(%rip), %rsi
	movq	%r14, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	96(%rbx), %r13
	movl	-44(%rbp), %r12d
	jmp	.L2460
	.cfi_endproc
.LFE12126:
	.text
	.size	_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi
	.section	.text.unlikely
	.size	_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold, .-_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi.cold
.LCOLDE123:
	.text
.LHOTE123:
	.section	.text.unlikely
	.align 2
.LCOLDB124:
	.text
.LHOTB124:
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_:
.LFB11875:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2475
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	movl	$0, -44(%rbp)
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2489
	movq	408(%r12), %r13
	xorl	%r14d, %r14d
	testq	%r13, %r13
	je	.L2492
.L2479:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	$0, 408(%r12)
	call	*16(%rax)
	movq	0(%r13), %rax
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	*24(%rax)
.L2474:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2493
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2492:
	.cfi_restore_state
	cmpq	$0, 88(%r12)
	je	.L2482
.L2481:
	movl	-44(%rbp), %esi
	testl	%esi, %esi
	je	.L2484
	cmpb	$0, 419(%r12)
	je	.L2485
.L2494:
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2474
	jmp	.L2490
	.p2align 4,,10
	.p2align 3
.L2475:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2484:
	movq	352(%r12), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	384(%r12), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO4ReadEPcm@PLT
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap7ClearInEv
	movq	%r12, %rdi
	movq	$0, 384(%r12)
	call	_ZN4node7TLSWrap6EncOutEv
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2485:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	jmp	.L2474
	.p2align 4,,10
	.p2align 3
.L2482:
	cmpb	$0, 419(%r12)
	movl	$-125, -44(%rbp)
	movl	$-125, %esi
	jne	.L2494
	jmp	.L2485
.L2493:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_.cold, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_.cold:
.LFSB11875:
.L2490:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	.LC106(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2474
.L2489:
	leaq	-44(%rbp), %rdx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	408(%r12), %r13
	testq	%r13, %r13
	je	.L2477
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2478
	movl	-44(%rbp), %r14d
	jmp	.L2479
.L2477:
	cmpq	$0, 88(%r12)
	jne	.L2481
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2482
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2482
.L2478:
	leaq	.LC104(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	408(%r12), %r13
	movl	-44(%rbp), %r14d
	jmp	.L2479
	.cfi_endproc
.LFE11875:
	.text
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_
	.section	.text.unlikely
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_.cold, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_.cold
.LCOLDE124:
	.text
.LHOTE124:
	.section	.text.unlikely
	.align 2
.LCOLDB125:
	.text
.LHOTB125:
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_:
.LFB11873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$24, %rsp
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	24(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	(%r12), %rax
	movq	408(%r12), %r13
	movq	192(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L2496
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	movl	$0, -44(%rbp)
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2510
	xorl	%r14d, %r14d
	testq	%r13, %r13
	je	.L2513
.L2500:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	$0, 408(%r12)
	call	*16(%rax)
	movq	0(%r13), %rax
	movl	%r14d, %esi
	movq	%r13, %rdi
	call	*24(%rax)
.L2495:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2514
	addq	$24, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2513:
	.cfi_restore_state
	cmpq	$0, 88(%r12)
	je	.L2503
.L2502:
	movl	-44(%rbp), %esi
	testl	%esi, %esi
	je	.L2505
	cmpb	$0, 419(%r12)
	je	.L2506
.L2515:
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2495
	jmp	.L2511
	.p2align 4,,10
	.p2align 3
.L2496:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2505:
	movq	352(%r12), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	384(%r12), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	_ZN4node6crypto7NodeBIO4ReadEPcm@PLT
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap7ClearInEv
	movq	%r12, %rdi
	movq	$0, 384(%r12)
	call	_ZN4node7TLSWrap6EncOutEv
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2506:
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN4node7TLSWrap12InvokeQueuedEiPKc
	jmp	.L2495
	.p2align 4,,10
	.p2align 3
.L2503:
	cmpb	$0, 419(%r12)
	movl	$-125, -44(%rbp)
	movl	$-125, %esi
	jne	.L2515
	jmp	.L2506
.L2514:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_.cold, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_.cold:
.LFSB11873:
.L2511:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	leaq	.LC106(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2495
.L2510:
	leaq	-44(%rbp), %rdx
	leaq	.LC103(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRiEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	408(%r12), %r13
	testq	%r13, %r13
	je	.L2498
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2499
	movl	-44(%rbp), %r14d
	jmp	.L2500
.L2498:
	cmpq	$0, 88(%r12)
	jne	.L2502
	movq	16(%r12), %rdx
	movslq	32(%r12), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2503
	leaq	.LC105(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2503
.L2499:
	leaq	.LC104(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJEEEvPNS_9AsyncWrapEPKcDpOT_
	movq	408(%r12), %r13
	movl	-44(%rbp), %r14d
	jmp	.L2500
	.cfi_endproc
.LFE11873:
	.text
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_
	.section	.text.unlikely
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_.cold, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_.cold
.LCOLDE125:
	.text
.LHOTE125:
	.section	.text.unlikely._ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB11741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L2517
	leaq	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2517:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC45(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L2518:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L2518
	cmpb	$120, %dl
	jg	.L2519
	cmpb	$99, %dl
	jg	.L2520
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L2521
	cmpb	$88, %dl
	je	.L2522
	jmp	.L2519
.L2520:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L2519
	leaq	.L2524(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L2524:
	.long	.L2525-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2525-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2527-.L2524
	.long	.L2526-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2525-.L2524
	.long	.L2519-.L2524
	.long	.L2525-.L2524
	.long	.L2519-.L2524
	.long	.L2519-.L2524
	.long	.L2523-.L2524
	.section	.text.unlikely._ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L2521:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2528
	call	_ZdlPv@PLT
.L2528:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L2548
	jmp	.L2530
.L2519:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2553
	call	_ZdlPv@PLT
	jmp	.L2553
.L2525:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC9(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z.constprop.0
	jmp	.L2550
.L2527:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L2535:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L2535
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L2550
.L2523:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L2550:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L2547
	jmp	.L2534
.L2522:
	movq	(%r8), %rsi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4ElLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L2538
	call	_ZdlPv@PLT
.L2538:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L2534
.L2547:
	call	_ZdlPv@PLT
	jmp	.L2534
.L2526:
	leaq	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2534:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L2553:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2530
.L2548:
	call	_ZdlPv@PLT
.L2530:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L2516
	call	_ZdlPv@PLT
.L2516:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2542
	call	__stack_chk_fail@PLT
.L2542:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11741:
	.size	_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB11526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2555
	call	__stack_chk_fail@PLT
.L2555:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11526:
	.size	_ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_:
.LFB11330:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRlEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2557
	call	_ZdlPv@PLT
.L2557:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2559
	call	__stack_chk_fail@PLT
.L2559:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11330:
	.size	_ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_,"axG",@progbits,_ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_,comdat
	.weak	_ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_
	.type	_ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_, @function
_ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_:
.LFB10243:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-160(%rbp), %r15
	leaq	-192(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%r14, %rdi
	call	*72(%rax)
	movq	%r14, %rsi
	leaq	.LC47(%rip), %rdx
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	-128(%rbp), %r14
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	leaq	.LC48(%rip), %rdx
	leaq	-96(%rbp), %rdi
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_PKS5_
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	je	.L2562
	movq	-96(%rbp), %rsi
	movq	stderr(%rip), %rdi
	movq	%r12, %rdx
	call	_ZN4node7FPrintFIJRlEEEvP8_IO_FILEPKcDpOT_
.L2562:
	movq	-96(%rbp), %rdi
	leaq	-80(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2563
	call	_ZdlPv@PLT
.L2563:
	movq	-128(%rbp), %rdi
	leaq	-112(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2564
	call	_ZdlPv@PLT
.L2564:
	movq	-160(%rbp), %rdi
	leaq	-144(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2565
	call	_ZdlPv@PLT
.L2565:
	movq	-192(%rbp), %rdi
	leaq	-176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L2561
	call	_ZdlPv@PLT
.L2561:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2567
	call	__stack_chk_fail@PLT
.L2567:
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10243:
	.size	_ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_, .-_ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"Read %zd bytes from underlying stream"
	.align 8
.LC127:
	.string	"Passing %zu bytes to the hello parser"
	.section	.text.unlikely
	.align 2
.LCOLDB128:
	.text
.LHOTB128:
	.align 2
	.p2align 4
	.globl	_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t
	.type	_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t, @function
_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t:
.LFB8798:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rsi, -72(%rbp)
	movq	16(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movslq	32(%rdi), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2583
.L2570:
	cmpq	$0, -72(%rbp)
	js	.L2586
	cmpq	$0, 88(%rbx)
	je	.L2587
	movq	344(%rbx), %rdi
	call	_ZN4node6crypto7NodeBIO7FromBIOEP6bio_st@PLT
	movq	-72(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node6crypto7NodeBIO6CommitEm@PLT
	cmpl	$3, 128(%rbx)
	jne	.L2588
	movl	456(%rbx), %eax
	leal	1(%rax), %edx
	movl	%edx, 456(%rbx)
	testl	%eax, %eax
	jne	.L2569
	.p2align 4,,10
	.p2align 3
.L2580:
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap7ClearInEv
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap8ClearOutEv
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap6EncOutEv
	movl	456(%rbx), %eax
	subl	$1, %eax
	movl	%eax, 456(%rbx)
	testl	%eax, %eax
	jg	.L2580
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2588:
	leaq	-64(%rbp), %r13
	movq	%r12, %rdi
	movq	$0, -64(%rbp)
	movq	%r13, %rsi
	call	_ZN4node6crypto7NodeBIO4PeekEPm@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L2589
.L2578:
	movq	16(%rbx), %rdx
	movslq	32(%rbx), %rax
	cmpb	$0, 2208(%rdx,%rax)
	jne	.L2584
.L2579:
	movq	-64(%rbp), %rdx
	leaq	128(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN4node6crypto17ClientHelloParser5ParseEPKhm@PLT
.L2569:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L2590
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2586:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN4node7TLSWrap8ClearOutEv
	cmpq	$-4095, -72(%rbp)
	jne	.L2572
	cmpb	$0, 460(%rbx)
	jne	.L2569
	movb	$1, 460(%rbx)
.L2572:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	-72(%rbp), %rsi
	movq	256(%rbx), %rdi
	movq	%rax, -64(%rbp)
	movq	%rdx, -56(%rbp)
	testq	%rsi, %rsi
	jle	.L2574
	addq	%rsi, 264(%rbx)
.L2574:
	movq	(%rdi), %rax
	leaq	-64(%rbp), %rdx
	call	*24(%rax)
	jmp	.L2569
	.p2align 4,,10
	.p2align 3
.L2587:
	leaq	_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L2589:
	cmpq	$0, -64(%rbp)
	je	.L2578
	leaq	_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L2590:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t.cold, @function
_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t.cold:
.LFSB8798:
.L2584:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -40
	.cfi_offset 6, -16
	.cfi_offset 12, -32
	.cfi_offset 13, -24
	movq	%r13, %rdx
	leaq	.LC127(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRmEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2579
.L2583:
	leaq	-72(%rbp), %rdx
	leaq	.LC126(%rip), %rsi
	call	_ZN4node27UnconditionalAsyncWrapDebugIJRlEEEvPNS_9AsyncWrapEPKcDpOT_
	jmp	.L2570
	.cfi_endproc
.LFE8798:
	.text
	.size	_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t, .-_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t
	.section	.text.unlikely
	.size	_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t.cold, .-_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t.cold
.LCOLDE128:
	.text
.LHOTE128:
	.set	.LTHUNK18,_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t
	.p2align 4
	.globl	_ZThn312_N4node7TLSWrap12OnStreamReadElRK8uv_buf_t
	.type	_ZThn312_N4node7TLSWrap12OnStreamReadElRK8uv_buf_t, @function
_ZThn312_N4node7TLSWrap12OnStreamReadElRK8uv_buf_t:
.LFB12164:
	.cfi_startproc
	endbr64
	subq	$312, %rdi
	jmp	.LTHUNK18
	.cfi_endproc
.LFE12164:
	.size	_ZThn312_N4node7TLSWrap12OnStreamReadElRK8uv_buf_t, .-_ZThn312_N4node7TLSWrap12OnStreamReadElRK8uv_buf_t
	.weak	_ZTVN4node9StreamReqE
	.section	.data.rel.ro._ZTVN4node9StreamReqE,"awG",@progbits,_ZTVN4node9StreamReqE,comdat
	.align 8
	.type	_ZTVN4node9StreamReqE, @object
	.size	_ZTVN4node9StreamReqE, 48
_ZTVN4node9StreamReqE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE
	.section	.data.rel.ro.local._ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE,"awG",@progbits,_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE,comdat
	.align 8
	.type	_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE, @object
	.size	_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE, 32
_ZTVN4node6crypto7SSLWrapINS_7TLSWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED1Ev
	.quad	_ZN4node6crypto7SSLWrapINS_7TLSWrapEED0Ev
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE
	.section	.data.rel.ro._ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,"awG",@progbits,_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE,comdat
	.align 8
	.type	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, @object
	.size	_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE, 168
_ZTVN4node15SimpleWriteWrapINS_9AsyncWrapEEE:
	.quad	0
	.quad	0
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	-40
	.quad	0
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED1Ev
	.quad	_ZThn40_N4node15SimpleWriteWrapINS_9AsyncWrapEED0Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE14MemoryInfoNameB5cxx11Ev
	.quad	_ZThn40_NK4node15SimpleWriteWrapINS_9AsyncWrapEE8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node7TLSWrapE
	.section	.data.rel.ro._ZTVN4node7TLSWrapE,"awG",@progbits,_ZTVN4node7TLSWrapE,comdat
	.align 8
	.type	_ZTVN4node7TLSWrapE, @object
	.size	_ZTVN4node7TLSWrapE, 488
_ZTVN4node7TLSWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node7TLSWrapD1Ev
	.quad	_ZN4node7TLSWrapD0Ev
	.quad	_ZNK4node7TLSWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node7TLSWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node7TLSWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node7TLSWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7TLSWrap7IsAliveEv
	.quad	_ZN4node7TLSWrap9IsClosingEv
	.quad	_ZN4node7TLSWrap9IsIPCPipeEv
	.quad	_ZN4node7TLSWrap5GetFDEv
	.quad	_ZN4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node7TLSWrap12GetAsyncWrapEv
	.quad	_ZN4node7TLSWrap9ReadStartEv
	.quad	_ZN4node7TLSWrap8ReadStopEv
	.quad	_ZN4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node7TLSWrap5ErrorEv
	.quad	_ZN4node7TLSWrap10ClearErrorEv
	.quad	_ZN4node7TLSWrap13OnStreamAllocEm
	.quad	_ZN4node7TLSWrap12OnStreamReadElRK8uv_buf_t
	.quad	_ZN4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node7TLSWrapD1Ev
	.quad	_ZThn56_N4node7TLSWrapD0Ev
	.quad	-248
	.quad	0
	.quad	_ZThn248_N4node7TLSWrapD1Ev
	.quad	_ZThn248_N4node7TLSWrapD0Ev
	.quad	_ZThn248_N4node7TLSWrap9ReadStartEv
	.quad	_ZThn248_N4node7TLSWrap8ReadStopEv
	.quad	_ZThn248_N4node7TLSWrap10DoShutdownEPNS_12ShutdownWrapE
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	_ZThn248_N4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_s
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZThn248_NK4node7TLSWrap5ErrorEv
	.quad	_ZThn248_N4node7TLSWrap10ClearErrorEv
	.quad	_ZThn248_N4node7TLSWrap7IsAliveEv
	.quad	_ZThn248_N4node7TLSWrap9IsClosingEv
	.quad	_ZThn248_N4node7TLSWrap9IsIPCPipeEv
	.quad	_ZThn248_N4node7TLSWrap5GetFDEv
	.quad	_ZThn248_N4node7TLSWrap18CreateShutdownWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZN4node10StreamBase15CreateWriteWrapEN2v85LocalINS1_6ObjectEEE
	.quad	_ZThn248_N4node7TLSWrap12GetAsyncWrapEv
	.quad	_ZN4node10StreamBase9GetObjectEv
	.quad	-312
	.quad	0
	.quad	_ZThn312_N4node7TLSWrapD1Ev
	.quad	_ZThn312_N4node7TLSWrapD0Ev
	.quad	_ZThn312_N4node7TLSWrap13OnStreamAllocEm
	.quad	_ZThn312_N4node7TLSWrap12OnStreamReadElRK8uv_buf_t
	.quad	_ZThn312_N4node7TLSWrap18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E_E4CallES2_
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap6EncOutEvEUlS2_E0_E4CallES2_
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sEUlS2_E_E4CallES2_
	.weak	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC129:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC131:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.weak	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC132:
	.string	"../src/debug_utils-inl.h:76"
.LC133:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRlJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC131
	.weak	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC134
	.weak	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC134
	.weak	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int; Args = {int&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC135
	.weak	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplImJRiEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC135
	.weak	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC136:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC136
	.weak	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRiJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC136
	.weak	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC137
	.weak	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRmJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC137
	.weak	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = int&; Args = {const char*&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args_0:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC138
	.weak	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRiJRPKcEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES3_OT_DpOT0_E4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC138
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC139:
	.string	"../src/debug_utils-inl.h:113"
.LC140:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = const char*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args_1:
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.weak	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPKcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES2_OT_DpOT0_E4args:
	.quad	.LC132
	.quad	.LC133
	.quad	.LC141
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC142:
	.string	"../src/util-inl.h:495"
.LC143:
	.string	"value->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC144:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Value>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args:
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.weak	_ZZN4node6crypto7SSLWrapINS_7TLSWrapEEC4EPNS_11EnvironmentEPNS0_13SecureContextENS3_4KindEE4args
	.section	.rodata.str1.1
.LC145:
	.string	"../src/node_crypto.h:204"
.LC146:
	.string	"ssl_"
	.section	.rodata.str1.8
	.align 8
.LC147:
	.string	"node::crypto::SSLWrap<Base>::SSLWrap(node::Environment*, node::crypto::SecureContext*, node::crypto::SSLWrap<Base>::Kind) [with Base = node::TLSWrap]"
	.section	.data.rel.ro.local._ZZN4node6crypto7SSLWrapINS_7TLSWrapEEC4EPNS_11EnvironmentEPNS0_13SecureContextENS3_4KindEE4args,"awG",@progbits,_ZZN4node6crypto7SSLWrapINS_7TLSWrapEEC4EPNS_11EnvironmentEPNS0_13SecureContextENS3_4KindEE4args,comdat
	.align 16
	.type	_ZZN4node6crypto7SSLWrapINS_7TLSWrapEEC4EPNS_11EnvironmentEPNS0_13SecureContextENS3_4KindEE4args, @gnu_unique_object
	.size	_ZZN4node6crypto7SSLWrapINS_7TLSWrapEEC4EPNS_11EnvironmentEPNS0_13SecureContextENS3_4KindEE4args, 24
_ZZN4node6crypto7SSLWrapINS_7TLSWrapEEC4EPNS_11EnvironmentEPNS0_13SecureContextENS3_4KindEE4args:
	.quad	.LC145
	.quad	.LC146
	.quad	.LC147
	.section	.rodata.str1.1
.LC148:
	.string	"../src/tls_wrap.cc"
.LC149:
	.string	"tls_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC148
	.quad	0
	.quad	_ZN4node7TLSWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC149
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC150:
	.string	"../src/tls_wrap.cc:1126"
.LC151:
	.string	"(wrap->ssl_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC152:
	.string	"static void node::TLSWrap::EnablePskCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap17EnablePskCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC150
	.quad	.LC151
	.quad	.LC152
	.section	.rodata.str1.1
.LC153:
	.string	"../src/tls_wrap.cc:1114"
.LC154:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC155:
	.string	"static void node::TLSWrap::SetPskIdentityHint(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC153
	.quad	.LC154
	.quad	.LC155
	.section	.rodata.str1.1
.LC156:
	.string	"../src/tls_wrap.cc:1109"
.LC157:
	.string	"(p->ssl_) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap18SetPskIdentityHintERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC156
	.quad	.LC157
	.quad	.LC155
	.section	.rodata.str1.1
.LC158:
	.string	"../src/tls_wrap.cc:1098"
	.section	.rodata.str1.8
	.align 8
.LC159:
	.string	"(SSL_set_SSL_CTX(p->ssl_.get(), sc->ctx_.get())) == (sc->ctx_.get())"
	.align 8
.LC160:
	.string	"static int node::TLSWrap::SelectSNIContextCallback(SSL*, int*, void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args_0, @object
	.size	_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args_0, 24
_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args_0:
	.quad	.LC158
	.quad	.LC159
	.quad	.LC160
	.section	.rodata.str1.1
.LC161:
	.string	"../src/tls_wrap.cc:1094"
.LC162:
	.string	"(sc) != nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args, @object
	.size	_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args, 24
_ZZN4node7TLSWrap24SelectSNIContextCallbackEP6ssl_stPiPvE4args:
	.quad	.LC161
	.quad	.LC162
	.quad	.LC160
	.section	.rodata.str1.1
.LC163:
	.string	"../src/tls_wrap.cc:1047"
	.section	.rodata.str1.8
	.align 8
.LC164:
	.string	"static void node::TLSWrap::SetServername(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC163
	.quad	.LC151
	.quad	.LC164
	.section	.rodata.str1.1
.LC165:
	.string	"../src/tls_wrap.cc:1045"
.LC166:
	.string	"wrap->is_client()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC165
	.quad	.LC166
	.quad	.LC164
	.section	.rodata.str1.1
.LC167:
	.string	"../src/tls_wrap.cc:1044"
.LC168:
	.string	"!wrap->started_"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC167
	.quad	.LC168
	.quad	.LC164
	.section	.rodata.str1.1
.LC169:
	.string	"../src/tls_wrap.cc:1043"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC169
	.quad	.LC154
	.quad	.LC164
	.section	.rodata.str1.1
.LC170:
	.string	"../src/tls_wrap.cc:1042"
.LC171:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap13SetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC170
	.quad	.LC171
	.quad	.LC164
	.section	.rodata.str1.1
.LC172:
	.string	"../src/tls_wrap.cc:1024"
	.section	.rodata.str1.8
	.align 8
.LC173:
	.string	"static void node::TLSWrap::GetServername(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap13GetServernameERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC172
	.quad	.LC151
	.quad	.LC173
	.section	.rodata.str1.1
.LC174:
	.string	"../src/tls_wrap.cc:943"
.LC175:
	.string	"(wrap->sc_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC176:
	.string	"static void node::TLSWrap::EnableKeylogCallback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap20EnableKeylogCallbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC174
	.quad	.LC175
	.quad	.LC176
	.section	.rodata.str1.1
.LC177:
	.string	"../src/tls_wrap.cc:926"
	.section	.rodata.str1.8
	.align 8
.LC178:
	.string	"static void node::TLSWrap::EnableSessionCallbacks(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap22EnableSessionCallbacksERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC177
	.quad	.LC151
	.quad	.LC178
	.section	.rodata.str1.1
.LC179:
	.string	"../src/tls_wrap.cc:896"
	.section	.rodata.str1.8
	.align 8
.LC180:
	.string	"static void node::TLSWrap::SetVerifyMode(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC179
	.quad	.LC151
	.quad	.LC180
	.section	.rodata.str1.1
.LC181:
	.string	"../src/tls_wrap.cc:895"
.LC182:
	.string	"args[1]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC181
	.quad	.LC182
	.quad	.LC180
	.section	.rodata.str1.1
.LC183:
	.string	"../src/tls_wrap.cc:894"
.LC184:
	.string	"args[0]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC183
	.quad	.LC184
	.quad	.LC180
	.section	.rodata.str1.1
.LC185:
	.string	"../src/tls_wrap.cc:893"
.LC186:
	.string	"(args.Length()) == (2)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap13SetVerifyModeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC185
	.quad	.LC186
	.quad	.LC180
	.section	.rodata.str1.1
.LC187:
	.string	"../src/tls_wrap.cc:861"
	.section	.rodata.str1.8
	.align 8
.LC188:
	.string	"!(data == nullptr) || (avail == 0)"
	.align 8
.LC189:
	.string	"virtual void node::TLSWrap::OnStreamRead(ssize_t, const uv_buf_t&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args_0, @object
	.size	_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args_0, 24
_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args_0:
	.quad	.LC187
	.quad	.LC188
	.quad	.LC189
	.section	.rodata.str1.1
.LC190:
	.string	"../src/tls_wrap.cc:847"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args, @object
	.size	_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args, 24
_ZZN4node7TLSWrap12OnStreamReadElRK8uv_buf_tE4args:
	.quad	.LC190
	.quad	.LC146
	.quad	.LC189
	.section	.rodata.str1.1
.LC191:
	.string	"../src/tls_wrap.cc:819"
.LC192:
	.string	"(ssl_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC193:
	.string	"virtual uv_buf_t node::TLSWrap::OnStreamAlloc(size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap13OnStreamAllocEmE4args, @object
	.size	_ZZN4node7TLSWrap13OnStreamAllocEmE4args, 24
_ZZN4node7TLSWrap13OnStreamAllocEmE4args:
	.quad	.LC191
	.quad	.LC192
	.quad	.LC193
	.section	.rodata.str1.1
.LC194:
	.string	"../src/tls_wrap.cc:804"
	.section	.rodata.str1.8
	.align 8
.LC195:
	.string	"(pending_cleartext_input_.size()) == (0)"
	.align 8
.LC196:
	.string	"virtual int node::TLSWrap::DoWrite(node::WriteWrap*, uv_buf_t*, size_t, uv_stream_t*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_3, @object
	.size	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_3, 24
_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_3:
	.quad	.LC194
	.quad	.LC195
	.quad	.LC196
	.section	.rodata.str1.1
.LC197:
	.string	"../src/tls_wrap.cc:788"
	.section	.rodata.str1.8
	.align 8
.LC198:
	.string	"written == -1 || written == static_cast<int>(length)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_2, @object
	.size	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_2, 24
_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_2:
	.quad	.LC197
	.quad	.LC198
	.quad	.LC196
	.section	.rodata.str1.1
.LC199:
	.string	"../src/tls_wrap.cc:744"
.LC200:
	.string	"(current_write_) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_1, @object
	.size	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_1, 24
_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_1:
	.quad	.LC199
	.quad	.LC200
	.quad	.LC196
	.section	.rodata.str1.1
.LC201:
	.string	"../src/tls_wrap.cc:729"
	.section	.rodata.str1.8
	.align 8
.LC202:
	.string	"(current_empty_write_) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_0, @object
	.size	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_0, 24
_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args_0:
	.quad	.LC201
	.quad	.LC202
	.quad	.LC196
	.section	.rodata.str1.1
.LC203:
	.string	"../src/tls_wrap.cc:693"
.LC204:
	.string	"(send_handle) == nullptr"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args, @object
	.size	_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args, 24
_ZZN4node7TLSWrap7DoWriteEPNS_9WriteWrapEP8uv_buf_tmP11uv_stream_sE4args:
	.quad	.LC203
	.quad	.LC204
	.quad	.LC196
	.section	.rodata.str1.1
.LC205:
	.string	"../src/tls_wrap.cc:592"
	.section	.rodata.str1.8
	.align 8
.LC206:
	.string	"written == -1 || written == static_cast<int>(data.size())"
	.section	.rodata.str1.1
.LC207:
	.string	"void node::TLSWrap::ClearIn()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap7ClearInEvE4args, @object
	.size	_ZZN4node7TLSWrap7ClearInEvE4args, 24
_ZZN4node7TLSWrap7ClearInEvE4args:
	.quad	.LC205
	.quad	.LC206
	.quad	.LC207
	.section	.rodata.str1.1
.LC208:
	.string	"../src/tls_wrap.cc:478"
.LC209:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC210:
	.string	"v8::Local<v8::Value> node::TLSWrap::GetSSLError(int, int*, std::string*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, @object
	.size	_ZZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args, 24
_ZZN4node7TLSWrap11GetSSLErrorEiPiPNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEE4args:
	.quad	.LC208
	.quad	.LC209
	.quad	.LC210
	.section	.rodata.str1.1
.LC211:
	.string	"../src/tls_wrap.cc:337"
	.section	.rodata.str1.8
	.align 8
.LC212:
	.string	"write_size_ != 0 && count != 0"
	.section	.rodata.str1.1
.LC213:
	.string	"void node::TLSWrap::EncOut()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap6EncOutEvE4args, @object
	.size	_ZZN4node7TLSWrap6EncOutEvE4args, 24
_ZZN4node7TLSWrap6EncOutEvE4args:
	.quad	.LC211
	.quad	.LC212
	.quad	.LC213
	.section	.rodata.str1.1
.LC214:
	.string	"../src/tls_wrap.cc:260"
.LC215:
	.string	"!SSL_renegotiate_pending(ssl)"
	.section	.rodata.str1.8
	.align 8
.LC216:
	.string	"static void node::TLSWrap::SSLInfoCallback(const SSL*, int, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stiiE4args, @object
	.size	_ZZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stiiE4args, 24
_ZZN4node7TLSWrap15SSLInfoCallbackEPK6ssl_stiiE4args:
	.quad	.LC214
	.quad	.LC215
	.quad	.LC216
	.section	.rodata.str1.1
.LC217:
	.string	"../src/tls_wrap.cc:221"
	.section	.rodata.str1.8
	.align 8
.LC218:
	.string	"static void node::TLSWrap::Start(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC217
	.quad	.LC166
	.quad	.LC218
	.section	.rodata.str1.1
.LC219:
	.string	"../src/tls_wrap.cc:216"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap5StartERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC219
	.quad	.LC168
	.quad	.LC218
	.section	.rodata.str1.1
.LC220:
	.string	"../src/tls_wrap.cc:174"
.LC221:
	.string	"(stream) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC222:
	.string	"static void node::TLSWrap::Wrap(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, @object
	.size	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3, 24
_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_3:
	.quad	.LC220
	.quad	.LC221
	.quad	.LC222
	.section	.rodata.str1.1
.LC223:
	.string	"../src/tls_wrap.cc:167"
.LC224:
	.string	"args[2]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, @object
	.size	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2, 24
_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_2:
	.quad	.LC223
	.quad	.LC224
	.quad	.LC222
	.section	.rodata.str1.1
.LC225:
	.string	"../src/tls_wrap.cc:166"
.LC226:
	.string	"args[1]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, @object
	.size	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1, 24
_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_1:
	.quad	.LC225
	.quad	.LC226
	.quad	.LC222
	.section	.rodata.str1.1
.LC227:
	.string	"../src/tls_wrap.cc:165"
.LC228:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC227
	.quad	.LC228
	.quad	.LC222
	.section	.rodata.str1.1
.LC229:
	.string	"../src/tls_wrap.cc:164"
.LC230:
	.string	"(args.Length()) == (3)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7TLSWrap4WrapERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC229
	.quad	.LC230
	.quad	.LC222
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC231:
	.string	"../src/stream_base-inl.h:103"
.LC232:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC233:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC231
	.quad	.LC232
	.quad	.LC233
	.weak	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC234:
	.string	"../src/stream_base-inl.h:85"
	.section	.rodata.str1.8
	.align 8
.LC235:
	.string	"(listener->stream_) == nullptr"
	.align 8
.LC236:
	.string	"void node::StreamResource::PushStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC234
	.quad	.LC235
	.quad	.LC236
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC237:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC238:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC239:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC237
	.quad	.LC238
	.quad	.LC239
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC240:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC241:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC240
	.quad	.LC238
	.quad	.LC241
	.weak	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC242:
	.string	"../src/stream_base-inl.h:26"
	.section	.rodata.str1.8
	.align 8
.LC243:
	.string	"(req_wrap_obj->GetAlignedPointerFromInternalField( StreamReq::kStreamReqField)) == (nullptr)"
	.align 8
.LC244:
	.string	"void node::StreamReq::AttachToObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node9StreamReq14AttachToObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC242
	.quad	.LC243
	.quad	.LC244
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0
	.section	.rodata.str1.8
	.align 8
.LC245:
	.string	"../src/memory_tracker-inl.h:269"
	.section	.rodata.str1.1
.LC246:
	.string	"(n->size_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC247:
	.string	"void node::MemoryTracker::Track(const node::MemoryRetainer*, const char*)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args_0:
	.quad	.LC245
	.quad	.LC246
	.quad	.LC247
	.weak	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args
	.section	.rodata.str1.8
	.align 8
.LC248:
	.string	"../src/memory_tracker-inl.h:268"
	.section	.rodata.str1.1
.LC249:
	.string	"(CurrentNode()) == (n)"
	.section	.data.rel.ro.local._ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,"awG",@progbits,_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args,comdat
	.align 16
	.type	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, @gnu_unique_object
	.size	_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args, 24
_ZZN4node13MemoryTracker5TrackEPKNS_14MemoryRetainerEPKcE4args:
	.quad	.LC248
	.quad	.LC249
	.quad	.LC247
	.weak	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args
	.section	.rodata.str1.8
	.align 8
.LC250:
	.string	"../src/memory_tracker-inl.h:27"
	.section	.rodata.str1.1
.LC251:
	.string	"(retainer_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC252:
	.string	"node::MemoryRetainerNode::MemoryRetainerNode(node::MemoryTracker*, const node::MemoryRetainer*)"
	.section	.data.rel.ro.local._ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,"awG",@progbits,_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args,comdat
	.align 16
	.type	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, @gnu_unique_object
	.size	_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args, 24
_ZZN4node18MemoryRetainerNodeC4EPNS_13MemoryTrackerEPKNS_14MemoryRetainerEE4args:
	.quad	.LC250
	.quad	.LC251
	.quad	.LC252
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC253:
	.string	"../src/debug_utils-inl.h:67"
.LC254:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC255:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC253
	.quad	.LC254
	.quad	.LC255
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args_0
	.section	.rodata.str1.1
.LC256:
	.string	"../src/base_object-inl.h:197"
	.section	.rodata.str1.8
	.align 8
.LC257:
	.string	"(metadata->strong_ptr_count) > (0)"
	.align 8
.LC258:
	.string	"void node::BaseObject::decrease_refcount()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args_0,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args_0,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args_0, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args_0:
	.quad	.LC256
	.quad	.LC257
	.quad	.LC258
	.weak	_ZZN4node10BaseObject17decrease_refcountEvE4args
	.section	.rodata.str1.1
.LC259:
	.string	"../src/base_object-inl.h:195"
.LC260:
	.string	"has_pointer_data()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject17decrease_refcountEvE4args,"awG",@progbits,_ZZN4node10BaseObject17decrease_refcountEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject17decrease_refcountEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject17decrease_refcountEvE4args, 24
_ZZN4node10BaseObject17decrease_refcountEvE4args:
	.quad	.LC259
	.quad	.LC260
	.quad	.LC258
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC261:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC262:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC263:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC261
	.quad	.LC262
	.quad	.LC263
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC264:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC265:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC266:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC264
	.quad	.LC265
	.quad	.LC266
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC267:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC268:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC269:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC267
	.quad	.LC268
	.quad	.LC269
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC270:
	.string	"../src/env-inl.h:955"
.LC271:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC272:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC270
	.quad	.LC271
	.quad	.LC272
	.weak	_ZZN4node11Environment8AllocateEmE4args
	.section	.rodata.str1.1
.LC273:
	.string	"../src/env-inl.h:889"
.LC274:
	.string	"(ret) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC275:
	.string	"char* node::Environment::Allocate(size_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8AllocateEmE4args,"awG",@progbits,_ZZN4node11Environment8AllocateEmE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8AllocateEmE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8AllocateEmE4args, 24
_ZZN4node11Environment8AllocateEmE4args:
	.quad	.LC273
	.quad	.LC274
	.quad	.LC275
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC276:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC277:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC278:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC276
	.quad	.LC277
	.quad	.LC278
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-1074790400
	.align 8
.LC14:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC29:
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.byte	32
	.align 16
.LC30:
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.byte	-97
	.align 16
.LC31:
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.align 16
.LC32:
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.byte	-32
	.align 16
.LC33:
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.byte	95
	.section	.rodata.cst8
	.align 8
.LC90:
	.long	0
	.long	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
