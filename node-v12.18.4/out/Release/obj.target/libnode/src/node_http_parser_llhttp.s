	.file	"node_http_parser_llhttp.cc"
	.text
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7099:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7099:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZN4node14StreamListener18OnStreamWantsWriteEm,"axG",@progbits,_ZN4node14StreamListener18OnStreamWantsWriteEm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.type	_ZN4node14StreamListener18OnStreamWantsWriteEm, @function
_ZN4node14StreamListener18OnStreamWantsWriteEm:
.LFB7539:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7539:
	.size	_ZN4node14StreamListener18OnStreamWantsWriteEm, .-_ZN4node14StreamListener18OnStreamWantsWriteEm
	.section	.text._ZN4node14StreamListener15OnStreamDestroyEv,"axG",@progbits,_ZN4node14StreamListener15OnStreamDestroyEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener15OnStreamDestroyEv
	.type	_ZN4node14StreamListener15OnStreamDestroyEv, @function
_ZN4node14StreamListener15OnStreamDestroyEv:
.LFB7540:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7540:
	.size	_ZN4node14StreamListener15OnStreamDestroyEv, .-_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.text._ZNK4node14StreamResource13HasWantsWriteEv,"axG",@progbits,_ZNK4node14StreamResource13HasWantsWriteEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node14StreamResource13HasWantsWriteEv
	.type	_ZNK4node14StreamResource13HasWantsWriteEv, @function
_ZNK4node14StreamResource13HasWantsWriteEv:
.LFB7556:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7556:
	.size	_ZNK4node14StreamResource13HasWantsWriteEv, .-_ZNK4node14StreamResource13HasWantsWriteEv
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv, @function
_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv:
.LFB7627:
	.cfi_startproc
	endbr64
	movl	$1824, %eax
	ret
	.cfi_endproc
.LFE7627:
	.size	_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv, .-_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"current_buffer"
	.text
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	1784(%rdi), %rax
	testq	%rax, %rax
	je	.L7
	movq	8(%rsi), %r12
	movq	%rsi, %rbx
	leaq	-48(%rbp), %rsi
	movq	(%r12), %rdx
	movq	%r12, %rdi
	movq	16(%rdx), %r13
	movq	(%rdx), %rdx
	movq	%rax, -48(%rbp)
	call	*%rdx
	movq	64(%rbx), %rcx
	movq	%rax, %rdx
	cmpq	32(%rbx), %rcx
	je	.L12
	cmpq	72(%rbx), %rcx
	je	.L17
.L10:
	movq	-8(%rcx), %rsi
.L9:
	leaq	.LC0(%rip), %rcx
	movq	%r12, %rdi
	call	*%r13
.L7:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L18
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rcx
	addq	$512, %rcx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%esi, %esi
	jmp	.L9
.L18:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7625:
	.size	_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB10018:
	.cfi_startproc
	endbr64
	leaq	40(%rdi), %rax
	ret
	.cfi_endproc
.LFE10018:
	.size	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,"axG",@progbits,_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.type	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, @function
_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv:
.LFB10022:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rax
	ret
	.cfi_endproc
.LFE10022:
	.size	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv, .-_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv
	.section	.text._ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,"axG",@progbits,_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.type	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, @function
_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi:
.LFB7574:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L26
	movq	(%rdi), %rax
	jmp	*40(%rax)
	.p2align 4,,10
	.p2align 3
.L26:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7574:
	.size	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi, .-_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.section	.text._ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,"axG",@progbits,_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.type	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, @function
_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi:
.LFB7575:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L32
	movq	(%rdi), %rax
	jmp	*32(%rax)
	.p2align 4,,10
	.p2align 3
.L32:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7575:
	.size	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi, .-_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L62
	cmpw	$1040, %cx
	jne	.L34
.L62:
	movq	23(%rdx), %r15
.L36:
	cmpl	$2, 16(%rbx)
	jg	.L37
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	16(%rbx), %ecx
	movl	%eax, %r13d
	testl	%ecx, %ecx
	jg	.L39
.L80:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L74
.L41:
	cmpl	$1, 16(%rbx)
	jle	.L75
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L43:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L76
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L45
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L46:
	call	_ZNK2v85Int325ValueEv@PLT
	movl	%eax, %r14d
	leal	-1(%rax), %eax
	cmpl	$1, %eax
	ja	.L77
	movq	(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L78
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L63
	cmpw	$1040, %cx
	jne	.L49
.L63:
	movq	23(%rdx), %r12
.L51:
	testq	%r12, %r12
	je	.L33
	cmpq	%r15, 16(%r12)
	jne	.L79
	xorl	%eax, %eax
	cmpl	$1, %r14d
	setne	%al
	addl	$16, %eax
	cmpl	$1, 16(%rbx)
	movl	%eax, 32(%r12)
	jg	.L55
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
.L56:
	movsd	.LC1(%rip), %xmm0
	xorl	%edx, %edx
	movq	%r12, %rdi
	leaq	80(%r12), %r15
	call	_ZN4node9AsyncWrap10AsyncResetEN2v85LocalINS1_6ObjectEEEdb@PLT
	leaq	_ZN4node12_GLOBAL__N_16Parser8settingsE(%rip), %rdx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	llhttp_init@PLT
	movzbl	%r13b, %esi
	movq	%r15, %rdi
	call	llhttp_set_lenient@PLT
	cmpb	$0, 1720(%r12)
	movq	$0, 1816(%r12)
	je	.L57
	movq	1712(%r12), %rdi
	testq	%rdi, %rdi
	je	.L58
	call	_ZdaPv@PLT
.L58:
	movb	$0, 1720(%r12)
.L57:
	cmpb	$0, 1744(%r12)
	movq	$0, 1712(%r12)
	movq	$0, 1728(%r12)
	je	.L59
	movq	1736(%r12), %rdi
	testq	%rdi, %rdi
	je	.L60
	call	_ZdaPv@PLT
.L60:
	movb	$0, 1744(%r12)
.L59:
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	$0, 1736(%r12)
	movq	$0, 1768(%r12)
	movw	%ax, 1776(%r12)
	movups	%xmm0, 1752(%r12)
.L33:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L37:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdi
	call	_ZNK2v85Value6IsTrueEv@PLT
	movl	16(%rbx), %ecx
	movl	%eax, %r13d
	testl	%ecx, %ecx
	jle	.L80
.L39:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L41
.L74:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	movq	8(%rbx), %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L55:
	movq	8(%rbx), %rsi
	subq	$8, %rsi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7649:
	.size	_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm, @function
_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm:
.LFB7654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rdi), %r13
	cmpb	$0, 2192(%r13)
	jne	.L94
	movq	%rdi, %rbx
	movq	2184(%r13), %rdi
	movb	$1, 2192(%r13)
	testq	%rdi, %rdi
	je	.L95
.L84:
	addq	$8, %rsp
	movl	$65536, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L95:
	.cfi_restore_state
	movl	$65536, %edi
	call	_Znam@PLT
	cmpq	$0, 2184(%r13)
	jne	.L96
	movq	%rax, 2184(%r13)
	movq	16(%rbx), %rax
	movq	2184(%rax), %rdi
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L94:
	testq	%rsi, %rsi
	movl	$1, %r13d
	movq	%rsi, %r12
	cmovne	%rsi, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L97
.L83:
	addq	$8, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	.cfi_restore_state
	leaq	_ZZN4node11Environment22set_http_parser_bufferEPcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%r12, %r12
	je	.L83
	testq	%rax, %rax
	jne	.L83
	leaq	_ZZN4node6MallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7654:
	.size	_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm, .-_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16ParserD2Ev, @function
_ZN4node12_GLOBAL__N_16ParserD2Ev:
.LFB9969:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 1744(%rdi)
	movq	%rax, 56(%rdi)
	je	.L99
	movq	1736(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L99
	call	_ZdaPv@PLT
.L99:
	cmpb	$0, 1720(%r13)
	je	.L100
	movq	1712(%r13), %rdi
	testq	%rdi, %rdi
	je	.L100
	call	_ZdaPv@PLT
.L100:
	leaq	1688(%r13), %r12
	leaq	920(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L102:
	cmpb	$0, 8(%r12)
	je	.L101
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	_ZdaPv@PLT
.L101:
	subq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L102
	leaq	152(%r13), %r12
	.p2align 4,,10
	.p2align 3
.L104:
	cmpb	$0, 8(%rbx)
	je	.L103
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L103
	call	_ZdaPv@PLT
.L103:
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L104
	movq	64(%r13), %rcx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 56(%r13)
	testq	%rcx, %rcx
	je	.L105
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L106
	leaq	56(%r13), %rdx
	cmpq	%rax, %rdx
	jne	.L108
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L134:
	cmpq	%rax, %rdx
	je	.L137
.L108:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L134
.L106:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L136:
	movq	72(%r13), %rax
	movq	%rax, 8(%rcx)
.L105:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
.L137:
	.cfi_restore_state
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L105
	.cfi_endproc
.LFE9969:
	.size	_ZN4node12_GLOBAL__N_16ParserD2Ev, .-_ZN4node12_GLOBAL__N_16ParserD2Ev
	.set	_ZN4node12_GLOBAL__N_16ParserD1Ev,_ZN4node12_GLOBAL__N_16ParserD2Ev
	.align 2
	.p2align 4
	.type	_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv, @function
_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv:
.LFB7626:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movl	$1936875856, 16(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$29285, %edx
	movw	%dx, 20(%rdi)
	movq	$6, 8(%rdi)
	movb	$0, 22(%rdi)
	ret
	.cfi_endproc
.LFE7626:
	.size	_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv, .-_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv
	.section	.rodata.str1.1
.LC2:
	.string	"Paused in callback"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_17on_chunk_completeEvEEE3RawEP18llhttp__internal_s, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_17on_chunk_completeEvEEE3RawEP18llhttp__internal_s:
.LFB10079:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	1728(%rdi), %edx
	leaq	-80(%rdi), %rax
	movq	$0, 1816(%rax)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%edx, %edx
	je	.L144
	xorl	%r8d, %r8d
	cmpb	$0, 1812(%rax)
	je	.L139
	movb	$0, 1812(%rax)
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$21, %r8d
.L139:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10079:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_17on_chunk_completeEvEEE3RawEP18llhttp__internal_s, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_17on_chunk_completeEvEEE3RawEP18llhttp__internal_s
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_15on_chunk_headerEvEEE3RawEP18llhttp__internal_s, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_15on_chunk_headerEvEEE3RawEP18llhttp__internal_s:
.LFB8487:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	1728(%rdi), %edx
	leaq	-80(%rdi), %rax
	movq	$0, 1816(%rax)
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	testl	%edx, %edx
	je	.L150
	xorl	%r8d, %r8d
	cmpb	$0, 1812(%rax)
	je	.L145
	movb	$0, 1812(%rax)
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$21, %r8d
.L145:
	movl	%r8d, %eax
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8487:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_15on_chunk_headerEvEEE3RawEP18llhttp__internal_s, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_15on_chunk_headerEvEEE3RawEP18llhttp__internal_s
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L157
	cmpw	$1040, %cx
	jne	.L152
.L157:
	movq	23(%rdx), %r13
.L154:
	movq	8(%rbx), %r12
	movl	$1824, %edi
	call	_Znwm@PLT
	movq	%r13, %rsi
	addq	$8, %r12
	movq	%rax, %rbx
	movq	%rax, %rdi
	movq	%r12, %rdx
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE@PLT
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	$0, 64(%rbx)
	leaq	176(%rbx), %rdx
	movq	$0, 72(%rbx)
	movq	%rax, (%rbx)
	addq	$112, %rax
	movq	%rax, 56(%rbx)
	leaq	944(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L155:
	movb	$0, 8(%rdx)
	addq	$24, %rdx
	movq	$0, -24(%rdx)
	movq	$0, -8(%rdx)
	cmpq	%rdx, %rax
	jne	.L155
	leaq	1712(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L156:
	movb	$0, 8(%rax)
	addq	$24, %rax
	movq	$0, -24(%rax)
	movq	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L156
	movb	$0, 1720(%rbx)
	movq	$0, 1712(%rbx)
	movq	$0, 1728(%rbx)
	movb	$0, 1744(%rbx)
	movq	$0, 1736(%rbx)
	movq	$0, 1752(%rbx)
	movq	$0, 1784(%rbx)
	movq	$0, 1792(%rbx)
	movq	$0, 1800(%rbx)
	movl	$0, 1808(%rbx)
	movb	$0, 1812(%rbx)
	movq	$0, 1816(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L154
	.cfi_endproc
.LFE7643:
	.size	_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP18llhttp__internal_s, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP18llhttp__internal_s:
.LFB8477:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	leaq	-80(%rdi), %rbx
	subq	$8, %rsp
	cmpb	$0, 1720(%rbx)
	movups	%xmm0, 1760(%rbx)
	je	.L162
	movq	1712(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	_ZdaPv@PLT
.L163:
	movb	$0, 1720(%rbx)
.L162:
	cmpb	$0, 1744(%rbx)
	movq	$0, 1712(%rbx)
	movq	$0, 1728(%rbx)
	je	.L164
	movq	1736(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L165
	call	_ZdaPv@PLT
.L165:
	movb	$0, 1744(%rbx)
.L164:
	movl	1808(%rbx), %eax
	movq	$0, 1736(%rbx)
	movq	$0, 1752(%rbx)
	testl	%eax, %eax
	je	.L176
	xorl	%eax, %eax
	cmpb	$0, 1812(%rbx)
	jne	.L177
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	movb	$0, 1812(%rbx)
	leaq	80(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	addq	$8, %rsp
	movl	$21, %eax
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8477:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP18llhttp__internal_s, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP18llhttp__internal_s
	.section	.rodata.str1.1
.LC3:
	.string	"HPE_JS_EXCEPTION:JS Exception"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP18llhttp__internal_sS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP18llhttp__internal_sS4_m:
.LFB8485:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	movq	%r15, %rdi
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rbx), %rax
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L179
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L195
.L179:
	movq	3280(%rdx), %rsi
	movl	$2, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L196
.L180:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L186
	movq	16(%rbx), %rdx
	movq	1784(%rbx), %rax
	movq	1800(%rbx), %rsi
	movq	352(%rdx), %rdi
	testq	%rax, %rax
	je	.L197
.L184:
	subq	%rsi, %r13
	movq	%rax, -80(%rbp)
	movq	%r13, %rsi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movl	%r14d, %esi
	movq	%rax, -72(%rbp)
	movq	16(%rbx), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	leaq	-80(%rbp), %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	$3, %edx
	movq	%rax, -64(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L198
.L186:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movl	1808(%rbx), %eax
	testl	%eax, %eax
	je	.L199
	xorl	%eax, %eax
	cmpb	$0, 1812(%rbx)
	jne	.L200
.L178:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L201
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	.cfi_restore_state
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L200:
	movb	$0, 1812(%rbx)
	leaq	80(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$21, %eax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L197:
	movq	1792(%rbx), %rdx
	call	_ZN4node6Buffer4CopyEPN2v87IsolateEPKcm@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L202
.L185:
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	16(%rbx), %rdx
	movq	1800(%rbx), %rsi
	movq	%rax, 1784(%rbx)
	movq	352(%rdx), %rdi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L196:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L198:
	movb	$1, 1777(%rbx)
	leaq	80(%rbx), %rdi
	leaq	.LC3(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movl	$23, %eax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L199:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rsi
	jmp	.L185
.L201:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8485:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP18llhttp__internal_sS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP18llhttp__internal_sS4_m
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm, @function
_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm:
.LFB10091:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	-40(%rdi), %r13
	cmpb	$0, 2192(%r13)
	jne	.L216
	movq	%rdi, %rbx
	movq	2184(%r13), %rdi
	movb	$1, 2192(%r13)
	testq	%rdi, %rdi
	je	.L217
.L206:
	addq	$8, %rsp
	movl	$65536, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L217:
	.cfi_restore_state
	movl	$65536, %edi
	call	_Znam@PLT
	cmpq	$0, 2184(%r13)
	jne	.L218
	movq	%rax, 2184(%r13)
	movq	-40(%rbx), %rax
	movq	2184(%rax), %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L216:
	testq	%rsi, %rsi
	movl	$1, %r13d
	movq	%rsi, %r12
	cmovne	%rsi, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L219
.L205:
	addq	$8, %rsp
	movl	%r12d, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_buf_init@PLT
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	leaq	_ZZN4node11Environment22set_http_parser_bufferEPcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L219:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%r12, %r12
	je	.L205
	testq	%rax, %rax
	jne	.L205
	leaq	_ZZN4node6MallocIcEEPT_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE10091:
	.size	_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm, .-_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"HPE_HEADER_OVERFLOW:Header overflow"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP18llhttp__internal_sS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP18llhttp__internal_sS4_m:
.LFB8479:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	subq	$24, %rsp
	movq	1736(%rdi), %rax
	addq	%rdx, %rax
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rdx
	movq	%rax, 1816(%rbx)
	cmpq	120(%rdx), %rax
	jnb	.L221
	movq	1712(%rbx), %rax
	movq	1728(%rbx), %r13
	movq	%rsi, %r14
	testq	%rax, %rax
	je	.L233
	cmpb	$0, 1720(%rbx)
	jne	.L224
	addq	%r13, %rax
	cmpq	%rax, %rsi
	je	.L223
.L224:
	leaq	(%r12,%r13), %rdi
	call	_Znam@PLT
	movq	1712(%rbx), %r8
	movq	1728(%rbx), %r13
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r13, %rdx
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	leaq	(%r15,%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	cmpb	$0, 1720(%rbx)
	movq	-56(%rbp), %r8
	jne	.L234
	movb	$1, 1720(%rbx)
.L226:
	movq	%r15, 1712(%rbx)
.L223:
	movl	1808(%rbx), %eax
	addq	%r13, %r12
	movq	%r12, 1728(%rbx)
	testl	%eax, %eax
	je	.L235
	xorl	%eax, %eax
	cmpb	$0, 1812(%rbx)
	jne	.L236
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	%rsi, 1712(%rbx)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L236:
	movb	$0, 1812(%rbx)
	leaq	80(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	addq	$24, %rsp
	movl	$21, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	1728(%rbx), %r13
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	.LC4(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	addq	$24, %rsp
	movl	$23, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8479:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP18llhttp__internal_sS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP18llhttp__internal_sS4_m
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP18llhttp__internal_sS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP18llhttp__internal_sS4_m:
.LFB8481:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	subq	$24, %rsp
	movq	1736(%rdi), %rax
	addq	%rdx, %rax
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rdx
	movq	%rax, 1816(%rbx)
	cmpq	120(%rdx), %rax
	jnb	.L238
	movq	1736(%rbx), %rax
	movq	1752(%rbx), %r13
	movq	%rsi, %r14
	testq	%rax, %rax
	je	.L250
	cmpb	$0, 1744(%rbx)
	jne	.L241
	addq	%r13, %rax
	cmpq	%rax, %rsi
	je	.L240
.L241:
	leaq	(%r12,%r13), %rdi
	call	_Znam@PLT
	movq	1736(%rbx), %r8
	movq	1752(%rbx), %r13
	movq	%rax, %r15
	movq	%rax, %rdi
	movq	%r8, %rsi
	movq	%r13, %rdx
	movq	%r8, -56(%rbp)
	call	memcpy@PLT
	leaq	(%r15,%r13), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	cmpb	$0, 1744(%rbx)
	movq	-56(%rbp), %r8
	jne	.L251
	movb	$1, 1744(%rbx)
.L243:
	movq	%r15, 1736(%rbx)
.L240:
	movl	1808(%rbx), %eax
	addq	%r13, %r12
	movq	%r12, 1752(%rbx)
	testl	%eax, %eax
	je	.L252
	xorl	%eax, %eax
	cmpb	$0, 1812(%rbx)
	jne	.L253
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	.cfi_restore_state
	movq	%rsi, 1736(%rbx)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L253:
	movb	$0, 1812(%rbx)
	leaq	80(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	addq	$24, %rsp
	movl	$21, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	.cfi_restore_state
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	movq	1752(%rbx), %r13
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	.LC4(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	addq	$24, %rsp
	movl	$23, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8481:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP18llhttp__internal_sS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP18llhttp__internal_sS4_m
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv, @function
_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv:
.LFB7658:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %rdx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-576(%rbp), %r12
	pushq	%rbx
	movq	%r12, %r13
	subq	$552, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rax
	.p2align 4,,10
	.p2align 3
.L255:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L255
	movq	16(%r14), %rax
	movq	1768(%r14), %rdx
	movq	352(%rax), %rdi
	testq	%rdx, %rdx
	je	.L256
	leaq	176(%r14), %rbx
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L264:
	movq	16(%rbx), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L281
.L259:
	movq	%rax, 0(%r13)
	movq	784(%rbx), %rax
	testq	%rax, %rax
	je	.L260
	movq	768(%rbx), %rsi
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%rax, 784(%rbx)
	testq	%rax, %rax
	je	.L260
.L263:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L268
	cmpb	$9, %dl
	je	.L268
	movq	768(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L265
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
.L266:
	movq	%rax, 8(%r13)
	addq	$1, %r15
	addq	$24, %rbx
	addq	$16, %r13
	movq	1768(%r14), %rax
	cmpq	%r15, %rax
	ja	.L264
	leaq	(%rax,%rax), %rdx
.L256:
	movq	%r12, %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	-56(%rbp), %rdi
	xorq	%fs:40, %rdi
	jne	.L282
	addq	$552, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	.cfi_restore_state
	leaq	128(%rdi), %rax
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L281:
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L258
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L259
.L265:
	movq	%rax, -584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-584(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L266
.L258:
	movq	%rax, -584(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-584(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L259
.L282:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7658:
	.size	_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv, .-_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP18llhttp__internal_sS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP18llhttp__internal_sS4_m:
.LFB8483:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	subq	$40, %rsp
	movq	1736(%rdi), %rax
	addq	%rdx, %rax
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rdx
	movq	%rax, 1816(%rbx)
	cmpq	120(%rdx), %rax
	jnb	.L284
	movq	1768(%rbx), %r13
	movq	%rsi, %r15
	cmpq	1760(%rbx), %r13
	je	.L305
	leaq	0(%r13,%r13,2), %rax
	leaq	1(%r13), %r12
	leaq	(%rbx,%rax,8), %rax
	movq	%r12, 1768(%rbx)
	cmpb	$0, 952(%rax)
	jne	.L306
.L288:
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	$0, 944(%rax)
	movq	$0, 960(%rax)
	cmpq	$31, %r12
	ja	.L298
	cmpq	%r12, 1760(%rbx)
	jne	.L307
.L291:
	subq	$1, %r12
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	944(%rax), %rdx
	testq	%rdx, %rdx
	je	.L308
	cmpb	$0, 952(%rax)
	movq	960(%rax), %rdi
	jne	.L294
	addq	%rdi, %rdx
	cmpq	%rdx, %r15
	je	.L293
.L294:
	addq	%r14, %rdi
	call	_Znam@PLT
	movq	%rax, %r13
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %rcx
	movq	%r13, %rdi
	movq	944(%rcx), %r8
	movq	960(%rcx), %rdx
	movq	%rcx, -72(%rbp)
	movq	%r8, %rsi
	movq	%r8, -56(%rbp)
	movq	%rdx, -64(%rbp)
	call	memcpy@PLT
	movq	-64(%rbp), %rdx
	movq	%r15, %rsi
	leaq	0(%r13,%rdx), %rdi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	-72(%rbp), %rcx
	movq	-56(%rbp), %r8
	cmpb	$0, 952(%rcx)
	jne	.L309
	movb	$1, 952(%rcx)
.L296:
	leaq	(%r12,%r12,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%r13, 944(%rax)
	movq	960(%rax), %rdi
.L293:
	leaq	(%r12,%r12,2), %rax
	addq	%r14, %rdi
	movq	%rdi, 960(%rbx,%rax,8)
	movl	1808(%rbx), %eax
	testl	%eax, %eax
	je	.L310
	xorl	%eax, %eax
	cmpb	$0, 1812(%rbx)
	jne	.L311
.L283:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	.cfi_restore_state
	movq	944(%rax), %rdi
	testq	%rdi, %rdi
	je	.L289
	call	_ZdaPv@PLT
	movq	1768(%rbx), %r12
.L289:
	leaq	0(%r13,%r13,2), %rax
	movb	$0, 952(%rbx,%rax,8)
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L308:
	movq	%r15, 944(%rax)
	movq	960(%rax), %rdi
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L305:
	cmpq	$31, %r13
	ja	.L298
	movq	%r13, %r12
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L311:
	movb	$0, 1812(%rbx)
	leaq	80(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$21, %eax
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L307:
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L309:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	.LC4(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	addq	$40, %rsp
	movl	$23, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L298:
	.cfi_restore_state
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L310:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8483:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP18llhttp__internal_sS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP18llhttp__internal_sS4_m
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L320
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L318
	cmpw	$1040, %cx
	jne	.L314
.L318:
	movq	23(%rdx), %r12
.L316:
	testq	%r12, %r12
	je	.L312
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap21EmitTraceEventDestroyEv@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	xorl	%esi, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrap11EmitDestroyEb@PLT
	.p2align 4,,10
	.p2align 3
.L312:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7645:
	.size	_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7652:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L348
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L333
	cmpw	$1040, %cx
	jne	.L323
.L333:
	movq	23(%rdx), %rax
.L325:
	testq	%rax, %rax
	je	.L321
	movq	64(%rax), %rsi
	testq	%rsi, %rsi
	je	.L321
	movq	8(%rsi), %rdx
	leaq	56(%rax), %rcx
	testq	%rdx, %rdx
	je	.L327
	cmpq	%rdx, %rcx
	jne	.L329
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L346:
	cmpq	%rdx, %rcx
	je	.L350
.L329:
	movq	%rdx, %rsi
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L346
.L327:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L350:
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rsi)
.L331:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 64(%rax)
.L321:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L348:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L349:
	movq	72(%rax), %rdx
	movq	%rdx, 8(%rsi)
	jmp	.L331
	.cfi_endproc
.LFE7652:
	.size	_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev, @function
_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev:
.LFB10087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 1688(%rdi)
	movq	%rax, (%rdi)
	je	.L352
	movq	1680(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L352
	call	_ZdaPv@PLT
.L352:
	cmpb	$0, 1664(%r13)
	je	.L353
	movq	1656(%r13), %rdi
	testq	%rdi, %rdi
	je	.L353
	call	_ZdaPv@PLT
.L353:
	leaq	1632(%r13), %r12
	leaq	864(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L355:
	cmpb	$0, 8(%r12)
	je	.L354
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L354
	call	_ZdaPv@PLT
.L354:
	subq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L355
	leaq	96(%r13), %r12
	.p2align 4,,10
	.p2align 3
.L357:
	cmpb	$0, 8(%rbx)
	je	.L356
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L356
	call	_ZdaPv@PLT
.L356:
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L357
	movq	8(%r13), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdx, %rdx
	je	.L358
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L359
	cmpq	%rax, %r13
	jne	.L361
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L387:
	cmpq	%rax, %r13
	je	.L390
.L361:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L387
.L359:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L389:
	movq	16(%r13), %rax
	movq	%rax, 8(%rdx)
.L358:
	subq	$56, %r13
	movq	%r13, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	movl	$1824, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
.L390:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	%rax, 16(%rdx)
	jmp	.L358
	.cfi_endproc
.LFE10087:
	.size	_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev, .-_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16ParserD0Ev, @function
_ZN4node12_GLOBAL__N_16ParserD0Ev:
.LFB9971:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -40
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpb	$0, 1744(%rdi)
	movq	%rax, 56(%rdi)
	je	.L392
	movq	1736(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L392
	call	_ZdaPv@PLT
.L392:
	cmpb	$0, 1720(%r12)
	je	.L393
	movq	1712(%r12), %rdi
	testq	%rdi, %rdi
	je	.L393
	call	_ZdaPv@PLT
.L393:
	leaq	1688(%r12), %r13
	leaq	920(%r12), %rbx
	.p2align 4,,10
	.p2align 3
.L395:
	cmpb	$0, 8(%r13)
	je	.L394
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L394
	call	_ZdaPv@PLT
.L394:
	subq	$24, %r13
	cmpq	%r13, %rbx
	jne	.L395
	leaq	152(%r12), %r13
	.p2align 4,,10
	.p2align 3
.L397:
	cmpb	$0, 8(%rbx)
	je	.L396
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L396
	call	_ZdaPv@PLT
.L396:
	subq	$24, %rbx
	cmpq	%r13, %rbx
	jne	.L397
	movq	64(%r12), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 56(%r12)
	testq	%rdx, %rdx
	je	.L398
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L399
	leaq	56(%r12), %rcx
	cmpq	%rax, %rcx
	jne	.L401
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L427:
	cmpq	%rax, %rcx
	je	.L430
.L401:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L427
.L399:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L429:
	movq	72(%r12), %rax
	movq	%rax, 8(%rdx)
.L398:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1824, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	movq	16(%rcx), %rax
	movq	%rax, 16(%rdx)
	jmp	.L398
	.cfi_endproc
.LFE9971:
	.size	_ZN4node12_GLOBAL__N_16ParserD0Ev, .-_ZN4node12_GLOBAL__N_16ParserD0Ev
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev, @function
_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev:
.LFB10092:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpb	$0, 1688(%rdi)
	movq	%rax, (%rdi)
	je	.L432
	movq	1680(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L432
	call	_ZdaPv@PLT
.L432:
	cmpb	$0, 1664(%r13)
	je	.L433
	movq	1656(%r13), %rdi
	testq	%rdi, %rdi
	je	.L433
	call	_ZdaPv@PLT
.L433:
	leaq	1632(%r13), %r12
	leaq	864(%r13), %rbx
	.p2align 4,,10
	.p2align 3
.L435:
	cmpb	$0, 8(%r12)
	je	.L434
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L434
	call	_ZdaPv@PLT
.L434:
	subq	$24, %r12
	cmpq	%r12, %rbx
	jne	.L435
	leaq	96(%r13), %r12
	.p2align 4,,10
	.p2align 3
.L437:
	cmpb	$0, 8(%rbx)
	je	.L436
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L436
	call	_ZdaPv@PLT
.L436:
	subq	$24, %rbx
	cmpq	%r12, %rbx
	jne	.L437
	movq	8(%r13), %rdx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 0(%r13)
	testq	%rdx, %rdx
	je	.L438
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L439
	cmpq	%rax, %r13
	jne	.L441
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L467:
	cmpq	%rax, %r13
	je	.L470
.L441:
	movq	%rax, %rdx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L467
.L439:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L469:
	movq	16(%r13), %rax
	movq	%rax, 8(%rdx)
.L438:
	addq	$8, %rsp
	leaq	-56(%r13), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
.L470:
	.cfi_restore_state
	movq	16(%r13), %rax
	movq	%rax, 16(%rdx)
	jmp	.L438
	.cfi_endproc
.LFE10092:
	.size	_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev, .-_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8503:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L482
	cmpw	$1040, %cx
	jne	.L472
.L482:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L488
.L475:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L483
	cmpw	$1040, %cx
	jne	.L476
.L483:
	movq	23(%rdx), %rax
.L478:
	testq	%rax, %rax
	je	.L471
	cmpq	%r13, 16(%rax)
	jne	.L489
	movl	1808(%rax), %edx
	testl	%edx, %edx
	je	.L481
	movb	$0, 1812(%rax)
.L471:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	80(%rax), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	llhttp_resume@PLT
	.p2align 4,,10
	.p2align 3
.L472:
	.cfi_restore_state
	movq	%rdi, %rbx
	xorl	%esi, %esi
	leaq	32(%r12), %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L475
	.p2align 4,,10
	.p2align 3
.L488:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L476:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8503:
	.size	_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE:
.LFB8502:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L501
	cmpw	$1040, %cx
	jne	.L491
.L501:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L507
.L494:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L502
	cmpw	$1040, %cx
	jne	.L495
.L502:
	movq	23(%rdx), %rax
.L497:
	testq	%rax, %rax
	je	.L490
	cmpq	%r13, 16(%rax)
	jne	.L508
	movl	1808(%rax), %edx
	testl	%edx, %edx
	je	.L500
	movb	$1, 1812(%rax)
.L490:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L500:
	.cfi_restore_state
	addq	$8, %rsp
	leaq	80(%rax), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	llhttp_pause@PLT
	.p2align 4,,10
	.p2align 3
.L491:
	.cfi_restore_state
	movq	%rdi, %rbx
	xorl	%esi, %esi
	leaq	32(%r12), %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L494
	.p2align 4,,10
	.p2align 3
.L507:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L495:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L508:
	leaq	_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8502:
	.size	_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L521
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L516
	cmpw	$1040, %cx
	jne	.L511
.L516:
	movq	23(%rdx), %rax
.L513:
	testq	%rax, %rax
	je	.L509
	movq	1792(%rax), %rdx
	movq	1800(%rax), %rsi
	movq	16(%rax), %rdi
	call	_ZN4node6Buffer4CopyEPNS_11EnvironmentEPKcm@PLT
	testq	%rax, %rax
	je	.L522
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L509:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L511:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L513
	.p2align 4,,10
	.p2align 3
.L521:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L522:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L509
	.cfi_endproc
.LFE7653:
	.size	_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L559
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L545
	cmpw	$1040, %cx
	jne	.L525
.L545:
	movq	23(%rdx), %r13
.L527:
	testq	%r13, %r13
	je	.L523
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L560
	movq	8(%rbx), %rdi
.L530:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L561
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L532
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L533:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L546
	cmpw	$1040, %cx
	jne	.L534
.L546:
	cmpq	$0, 23(%rdx)
	je	.L537
.L536:
	movq	31(%rdx), %rax
.L541:
	testq	%rax, %rax
	je	.L537
	cmpq	$0, 64(%r13)
	leaq	56(%r13), %rdx
	jne	.L562
	movq	8(%rax), %rcx
	movq	%rax, 64(%r13)
	movq	%rcx, 72(%r13)
	movq	%rdx, 8(%rax)
.L523:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L560:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L532:
	movq	8(%rbx), %r12
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L525:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L527
	.p2align 4,,10
	.p2align 3
.L559:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L561:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L534:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	testq	%rax, %rax
	je	.L537
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L536
	cmpw	$1040, %cx
	je	.L536
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L541
	.p2align 4,,10
	.p2align 3
.L537:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L562:
	leaq	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7651:
	.size	_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L607
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L581
	cmpw	$1040, %cx
	jne	.L565
.L581:
	movq	23(%rdx), %r12
.L567:
	testq	%r12, %r12
	je	.L563
	leaq	16+_ZTVN4node12_GLOBAL__N_16ParserE(%rip), %rax
	movq	%rax, (%r12)
	addq	$112, %rax
	cmpb	$0, 1744(%r12)
	movq	%rax, 56(%r12)
	je	.L569
	movq	1736(%r12), %rdi
	testq	%rdi, %rdi
	je	.L569
	call	_ZdaPv@PLT
.L569:
	cmpb	$0, 1720(%r12)
	leaq	1712(%r12), %r13
	jne	.L608
.L570:
	leaq	944(%r12), %rbx
	.p2align 4,,10
	.p2align 3
.L572:
	subq	$24, %r13
	cmpb	$0, 8(%r13)
	je	.L571
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L571
	call	_ZdaPv@PLT
.L571:
	cmpq	%rbx, %r13
	jne	.L572
	leaq	176(%r12), %r13
	.p2align 4,,10
	.p2align 3
.L574:
	subq	$24, %rbx
	cmpb	$0, 8(%rbx)
	je	.L573
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L573
	call	_ZdaPv@PLT
.L573:
	cmpq	%rbx, %r13
	jne	.L574
	movq	64(%r12), %rcx
	leaq	16+_ZTVN4node14StreamListenerE(%rip), %rax
	movq	%rax, 56(%r12)
	testq	%rcx, %rcx
	je	.L575
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.L576
	leaq	56(%r12), %rdx
	cmpq	%rax, %rdx
	jne	.L578
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L605:
	cmpq	%rax, %rdx
	je	.L610
.L578:
	movq	%rax, %rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L605
.L576:
	leaq	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L608:
	movq	1712(%r12), %rdi
	testq	%rdi, %rdi
	je	.L570
	call	_ZdaPv@PLT
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L609:
	movq	72(%r12), %rax
	movq	%rax, 8(%rcx)
.L575:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1824, %esi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L563:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L565:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L607:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L610:
	movq	16(%rdx), %rax
	movq	%rax, 16(%rcx)
	jmp	.L575
	.cfi_endproc
.LFE7644:
	.size	_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7097:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L612
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L612:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L622
.L613:
	movq	(%r12), %rax
	leaq	_ZN4node10BaseObject11OnGCCollectEv(%rip), %rcx
	movq	64(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L614
	movq	8(%rax), %rax
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L622:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L613
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7097:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP18llhttp__internal_sS4_m, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP18llhttp__internal_sS4_m:
.LFB8482:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	-80(%rdi), %rbx
	subq	$104, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	1736(%rdi), %rax
	addq	%rdx, %rax
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rdx
	movq	%rax, 1816(%rbx)
	cmpq	120(%rdx), %rax
	jnb	.L624
	movq	1760(%rbx), %r14
	movq	%rsi, %r15
	cmpq	1768(%rbx), %r14
	je	.L661
.L626:
	cmpq	$31, %r14
	ja	.L662
	movq	1768(%rbx), %r13
	leaq	1(%r13), %rax
	cmpq	%r14, %rax
	jne	.L663
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	176(%rax), %rdx
	testq	%rdx, %rdx
	je	.L664
	cmpb	$0, 184(%rax)
	movq	192(%rax), %rdi
	jne	.L644
	addq	%rdi, %rdx
	cmpq	%rdx, %r15
	je	.L643
.L644:
	addq	%r12, %rdi
	call	_Znam@PLT
	movq	%rax, %r14
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rbx,%rax,8), %rcx
	movq	%r14, %rdi
	movq	176(%rcx), %r8
	movq	192(%rcx), %rdx
	movq	%rcx, -136(%rbp)
	movq	%r8, %rsi
	movq	%r8, -120(%rbp)
	movq	%rdx, -128(%rbp)
	call	memcpy@PLT
	movq	-128(%rbp), %rdx
	movq	%r15, %rsi
	leaq	(%r14,%rdx), %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
	movq	-136(%rbp), %rcx
	movq	-120(%rbp), %r8
	cmpb	$0, 184(%rcx)
	jne	.L665
	movb	$1, 184(%rcx)
.L646:
	leaq	0(%r13,%r13,2), %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%r14, 176(%rax)
	movq	192(%rax), %rdi
.L643:
	leaq	0(%r13,%r13,2), %rax
	addq	%rdi, %r12
	movq	%r12, 192(%rbx,%rax,8)
	movl	1808(%rbx), %eax
	testl	%eax, %eax
	je	.L666
	xorl	%eax, %eax
	cmpb	$0, 1812(%rbx)
	jne	.L667
.L623:
	movq	-56(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L668
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L664:
	.cfi_restore_state
	movq	%r15, 176(%rax)
	movq	192(%rax), %rdi
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L661:
	leaq	1(%r14), %rax
	movq	%rax, 1760(%rbx)
	cmpq	$32, %rax
	je	.L669
.L628:
	leaq	(%r14,%r14,2), %rdx
	leaq	(%rbx,%rdx,8), %rdx
	cmpb	$0, 184(%rdx)
	je	.L638
	movq	176(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L639
	call	_ZdaPv@PLT
	movq	1760(%rbx), %rax
.L639:
	leaq	(%r14,%r14,2), %rdx
	movb	$0, 184(%rbx,%rdx,8)
.L638:
	leaq	(%r14,%r14,2), %rdx
	movq	%rax, %r14
	leaq	(%rbx,%rdx,8), %rdx
	movq	$0, 176(%rdx)
	movq	$0, 192(%rdx)
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L667:
	movb	$0, 1812(%rbx)
	leaq	80(%rbx), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$21, %eax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L665:
	movq	%r8, %rdi
	call	_ZdaPv@PLT
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L624:
	leaq	.LC4(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$23, %eax
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L662:
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L663:
	leaq	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L669:
	movq	16(%rbx), %rax
	leaq	-112(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L629
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	jne	.L629
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
.L629:
	movq	3280(%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L670
.L630:
	movq	%r13, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L660
	movq	%rbx, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser13CreateHeadersEv
	movq	1728(%rbx), %rcx
	movq	%rax, -80(%rbp)
	movq	16(%rbx), %rax
	testq	%rcx, %rcx
	jne	.L671
	movq	352(%rax), %rax
	subq	$-128, %rax
.L634:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, -72(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L672
.L635:
	cmpb	$0, 1720(%rbx)
	je	.L636
	movq	1712(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L637
	call	_ZdaPv@PLT
.L637:
	movb	$0, 1720(%rbx)
.L636:
	movq	$0, 1712(%rbx)
	movq	$0, 1728(%rbx)
	movb	$1, 1776(%rbx)
.L660:
	movq	%r14, %rdi
	xorl	%r14d, %r14d
	call	_ZN2v811HandleScopeD1Ev@PLT
	movdqa	.LC5(%rip), %xmm0
	movl	$1, %eax
	movups	%xmm0, 1760(%rbx)
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L666:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L671:
	movq	1712(%rbx), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L634
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rax
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L672:
	movb	$1, 1777(%rbx)
	jmp	.L635
.L670:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L630
.L668:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8482:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP18llhttp__internal_sS4_m, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP18llhttp__internal_sS4_m
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP18llhttp__internal_s, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP18llhttp__internal_s:
.LFB8484:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rdi), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$712, %rsp
	movq	8(%r14), %rdi
	movq	16(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movaps	%xmm0, -656(%rbp)
	movq	$0, 1816(%r14)
	movq	$0, -592(%rbp)
	movaps	%xmm0, -640(%rbp)
	movaps	%xmm0, -624(%rbp)
	movaps	%xmm0, -608(%rbp)
	testq	%rdi, %rdi
	je	.L674
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L770
.L674:
	movq	3280(%rdx), %rsi
	movl	$1, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L771
.L675:
	movq	%r12, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L677
	movq	16(%r14), %rax
	cmpb	$0, 1776(%r14)
	movq	352(%rax), %rdi
	leaq	88(%rdi), %rax
	movq	%rax, %xmm0
	movq	%rax, -592(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movaps	%xmm0, -656(%rbp)
	movaps	%xmm0, -640(%rbp)
	movaps	%xmm0, -624(%rbp)
	movaps	%xmm0, -608(%rbp)
	je	.L678
	leaq	-704(%rbp), %r13
	movq	%rdi, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r14), %rdi
	movq	16(%r14), %rdx
	testq	%rdi, %rdi
	je	.L679
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L772
.L679:
	movq	3280(%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, -736(%rbp)
	testq	%rax, %rax
	je	.L773
.L680:
	movq	-736(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L769
	leaq	-576(%rbp), %rax
	leaq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, -720(%rbp)
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L683:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L683
	movq	16(%r14), %rax
	movq	1768(%r14), %rdx
	movq	352(%rax), %rdi
	testq	%rdx, %rdx
	je	.L684
	xorl	%r8d, %r8d
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L692:
	movq	112(%rbx,%r8), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L774
.L687:
	movq	%rax, (%r10)
	movq	960(%r14,%r8), %rax
	testq	%rax, %rax
	je	.L688
	movq	944(%r14,%r8), %rsi
	jmp	.L691
	.p2align 4,,10
	.p2align 3
.L734:
	movq	%rax, 960(%r14,%r8)
	testq	%rax, %rax
	je	.L688
.L691:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L734
	cmpb	$9, %dl
	je	.L734
	movq	864(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -728(%rbp)
	movq	%r8, -712(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-712(%rbp), %r8
	movq	-728(%rbp), %r10
	testq	%rax, %rax
	je	.L728
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
.L729:
	addq	$1, %r15
	movq	%rax, 8(%r10)
	addq	$24, %r8
	addq	$16, %r10
	movq	1768(%r14), %rdx
	cmpq	%r15, %rdx
	ja	.L692
	addq	%rdx, %rdx
.L684:
	movq	-720(%rbp), %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	1728(%r14), %rcx
	movq	%rax, -576(%rbp)
	movq	16(%r14), %rax
	testq	%rcx, %rcx
	jne	.L775
	movq	352(%rax), %rbx
	leaq	128(%rbx), %rax
.L694:
	movq	-720(%rbp), %rcx
	movq	-736(%rbp), %rsi
	movl	$2, %edx
	movq	%r14, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L776
.L695:
	cmpb	$0, 1720(%r14)
	je	.L696
	movq	1712(%r14), %rdi
	testq	%rdi, %rdi
	je	.L697
	call	_ZdaPv@PLT
.L697:
	movb	$0, 1720(%r14)
.L696:
	movq	$0, 1712(%r14)
	movq	$0, 1728(%r14)
	movb	$1, 1776(%r14)
.L769:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	16(%r14), %rax
	movzbl	152(%r14), %edx
	movq	352(%rax), %rdi
.L682:
	pxor	%xmm0, %xmm0
	movups	%xmm0, 1760(%r14)
	cmpb	$1, %dl
	je	.L713
.L709:
	cmpb	$2, %dl
	je	.L777
.L714:
	movzbl	154(%r14), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movzbl	155(%r14), %esi
	movq	%rax, -656(%rbp)
	movq	16(%r14), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	leaq	80(%r14), %rdi
	movq	%rax, -648(%rbp)
	call	llhttp_should_keep_alive@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	%eax, %r8d
	movq	16(%r14), %rax
	movq	352(%rax), %rax
	leaq	112(%rax), %rcx
	addq	$120, %rax
	testl	%r8d, %r8d
	movq	%rcx, %rdx
	cmove	%rax, %rdx
	cmpb	$0, 160(%r14)
	cmove	%rax, %rcx
	movq	%rdx, -592(%rbp)
	movl	$2, %edx
	movq	%rcx, -600(%rbp)
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%r14), %rdx
	movq	16(%r14), %rcx
	testq	%rdx, %rdx
	je	.L722
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L778
.L722:
	movq	3280(%rcx), %rsi
	movq	%r12, %rdi
	leaq	-656(%rbp), %r8
	movl	$9, %ecx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L779
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movq	16(%r14), %rax
	movq	%r12, %rdi
	movq	3280(%rax), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L724
	movl	%edx, %eax
	testl	%edx, %edx
	je	.L677
.L673:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L780
	addq	$712, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L677:
	.cfi_restore_state
	movl	1808(%r14), %eax
	testl	%eax, %eax
	je	.L781
	xorl	%eax, %eax
	cmpb	$0, 1812(%r14)
	je	.L673
	movb	$0, 1812(%r14)
	leaq	80(%r14), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$21, %eax
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L678:
	leaq	-576(%rbp), %rax
	leaq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, -720(%rbp)
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L698:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L698
	movq	1768(%r14), %rdx
	testq	%rdx, %rdx
	je	.L699
	movq	$0, -712(%rbp)
	addq	$96, %rbx
	leaq	944(%r14), %r15
	.p2align 4,,10
	.p2align 3
.L707:
	movq	16(%rbx), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L782
.L702:
	movq	%rax, 0(%r13)
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.L703
	movq	(%r15), %rsi
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L735:
	movq	%rax, 16(%r15)
	testq	%rax, %rax
	je	.L703
.L706:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L735
	cmpb	$9, %dl
	je	.L735
	movq	768(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L731
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
.L730:
	movq	%rax, 8(%r13)
	addq	$24, %rbx
	addq	$16, %r13
	addq	$24, %r15
	addq	$1, -712(%rbp)
	movq	1768(%r14), %rdx
	movq	-712(%rbp), %rax
	cmpq	%rax, %rdx
	ja	.L707
	addq	%rdx, %rdx
.L699:
	movq	-720(%rbp), %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movzbl	152(%r14), %edx
	movq	%rax, -640(%rbp)
	cmpb	$1, %dl
	je	.L708
	movq	16(%r14), %rax
	pxor	%xmm0, %xmm0
	leaq	-704(%rbp), %r13
	movq	352(%rax), %rdi
	movups	%xmm0, 1760(%r14)
	cmpb	$2, %dl
	jne	.L714
.L777:
	movzwl	162(%r14), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	1752(%r14), %rcx
	movq	%rax, -616(%rbp)
	movq	16(%r14), %rax
	testq	%rcx, %rcx
	jne	.L783
	movq	352(%rax), %rdi
	leaq	128(%rdi), %rax
.L717:
	movq	%rax, -608(%rbp)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L703:
	leaq	128(%rdi), %rax
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L782:
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L701
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L770:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %rdi
	jmp	.L674
	.p2align 4,,10
	.p2align 3
.L772:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rdx
	movq	%rax, %rdi
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L775:
	movq	1712(%r14), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L694
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-712(%rbp), %rax
	jmp	.L694
	.p2align 4,,10
	.p2align 3
.L688:
	leaq	128(%rdi), %rax
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L774:
	movq	96(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -728(%rbp)
	movq	%r8, -712(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-712(%rbp), %r8
	movq	-728(%rbp), %r10
	testq	%rax, %rax
	je	.L686
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%r13, %rdi
	movb	$1, -670(%rbp)
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
.L724:
	movb	$1, 1777(%r14)
	movl	$-1, %eax
	jmp	.L673
	.p2align 4,,10
	.p2align 3
.L778:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r14), %rcx
	movq	%rax, %rdx
	jmp	.L722
	.p2align 4,,10
	.p2align 3
.L708:
	movq	16(%r14), %rax
	movq	1728(%r14), %rcx
	movq	352(%rax), %rdi
	testq	%rcx, %rcx
	jne	.L784
	leaq	128(%rdi), %rax
	pxor	%xmm0, %xmm0
	leaq	-704(%rbp), %r13
	movq	%rax, -624(%rbp)
	movups	%xmm0, 1760(%r14)
.L713:
	movzbl	153(%r14), %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movzbl	152(%r14), %edx
	movq	%rax, -632(%rbp)
	movq	16(%r14), %rax
	movq	352(%rax), %rdi
	jmp	.L709
	.p2align 4,,10
	.p2align 3
.L771:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L731:
	movq	%rax, -728(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-728(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L730
	.p2align 4,,10
	.p2align 3
.L701:
	movq	%rax, -728(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-728(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L776:
	movb	$1, 1777(%r14)
	jmp	.L695
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%r8, -744(%rbp)
	movq	%r10, -728(%rbp)
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-744(%rbp), %r8
	movq	-728(%rbp), %r10
	movq	-712(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L687
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%r8, -744(%rbp)
	movq	%r10, -728(%rbp)
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-744(%rbp), %r8
	movq	-728(%rbp), %r10
	movq	-712(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L729
	.p2align 4,,10
	.p2align 3
.L783:
	movq	1736(%r14), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L716
	movq	16(%r14), %rdx
	movq	352(%rdx), %rdi
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L784:
	movq	1712(%r14), %rsi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	je	.L711
	movq	16(%r14), %rcx
	movzbl	152(%r14), %edx
	movq	352(%rcx), %rdi
.L712:
	movq	%rax, -624(%rbp)
	leaq	-704(%rbp), %r13
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L781:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L773:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L680
.L716:
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rdx
	movq	-712(%rbp), %rax
	movq	352(%rdx), %rdi
	jmp	.L717
.L711:
	movq	%rax, -712(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r14), %rcx
	movzbl	152(%r14), %edx
	movq	-712(%rbp), %rax
	movq	352(%rcx), %rdi
	jmp	.L712
.L780:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8484:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP18llhttp__internal_s, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP18llhttp__internal_s
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP18llhttp__internal_s, @function
_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP18llhttp__internal_s:
.LFB8486:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	-80(%rdi), %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	leaq	-656(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%r12, %rdi
	subq	$664, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%r15), %rax
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	cmpq	$0, 1760(%r15)
	jne	.L843
.L786:
	movq	8(%r15), %rdi
	movq	16(%r15), %rdx
	testq	%rdi, %rdi
	je	.L805
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L844
.L805:
	movq	3280(%rdx), %rsi
	movl	$3, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L845
.L806:
	movq	%r14, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L807
.L842:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	1808(%r15), %eax
	testl	%eax, %eax
	je	.L846
	xorl	%eax, %eax
	cmpb	$0, 1812(%r15)
	jne	.L847
.L785:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L848
	addq	$664, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L807:
	.cfi_restore_state
	leaq	-624(%rbp), %r13
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi@PLT
	movq	8(%r15), %rdx
	movq	16(%r15), %rcx
	testq	%rdx, %rdx
	je	.L809
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L849
.L809:
	movq	3280(%rcx), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	je	.L850
	movq	%r13, %rdi
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L843:
	movq	16(%r15), %rax
	leaq	-624(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	8(%r15), %rdi
	movq	16(%r15), %rdx
	testq	%rdi, %rdi
	je	.L787
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L851
.L787:
	movq	3280(%rdx), %rsi
	xorl	%edx, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, -680(%rbp)
	testq	%rax, %rax
	je	.L852
.L788:
	movq	-680(%rbp), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L841
	leaq	-576(%rbp), %rax
	leaq	-64(%rbp), %rdx
	pxor	%xmm0, %xmm0
	movq	%rax, -688(%rbp)
	movq	%rax, %r10
	.p2align 4,,10
	.p2align 3
.L790:
	movaps	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rax, %rdx
	jne	.L790
	movq	16(%r15), %rax
	movq	1768(%r15), %rdx
	movq	352(%rax), %rdi
	testq	%rdx, %rdx
	je	.L791
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L799:
	movq	112(%rbx,%r8), %rcx
	leaq	128(%rdi), %rax
	testq	%rcx, %rcx
	jne	.L853
.L794:
	movq	%rax, (%r10)
	movq	960(%r15,%r8), %rax
	testq	%rax, %rax
	je	.L795
	movq	944(%r15,%r8), %rsi
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L817:
	movq	%rax, 960(%r15,%r8)
	testq	%rax, %rax
	je	.L795
.L798:
	movq	%rax, %rcx
	subq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	$32, %dl
	je	.L817
	cmpb	$9, %dl
	je	.L817
	movq	864(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-664(%rbp), %r8
	movq	-672(%rbp), %r10
	testq	%rax, %rax
	je	.L813
	movq	16(%r15), %rdx
	movq	352(%rdx), %rdi
.L814:
	addq	$1, %r14
	movq	%rax, 8(%r10)
	addq	$24, %r8
	addq	$16, %r10
	movq	1768(%r15), %rdx
	cmpq	%r14, %rdx
	ja	.L799
	addq	%rdx, %rdx
.L791:
	movq	-688(%rbp), %rsi
	call	_ZN2v85Array3NewEPNS_7IsolateEPNS_5LocalINS_5ValueEEEm@PLT
	movq	1728(%r15), %rcx
	movq	%rax, -576(%rbp)
	movq	16(%r15), %rax
	testq	%rcx, %rcx
	jne	.L854
	movq	352(%rax), %rbx
	leaq	128(%rbx), %rax
.L801:
	movq	-688(%rbp), %rcx
	movq	-680(%rbp), %rsi
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%rax, -568(%rbp)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L855
.L802:
	cmpb	$0, 1720(%r15)
	je	.L803
	movq	1712(%r15), %rdi
	testq	%rdi, %rdi
	je	.L804
	call	_ZdaPv@PLT
.L804:
	movb	$0, 1720(%r15)
.L803:
	movq	$0, 1712(%r15)
	movq	$0, 1728(%r15)
	movb	$1, 1776(%r15)
.L841:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L844:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r15), %rdx
	movq	%rax, %rdi
	jmp	.L805
	.p2align 4,,10
	.p2align 3
.L847:
	movb	$0, 1812(%r15)
	leaq	80(%r15), %rdi
	leaq	.LC2(%rip), %rsi
	call	llhttp_set_error_reason@PLT
	movl	$21, %eax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L851:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r15), %rdx
	movq	%rax, %rdi
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L854:
	movq	1712(%r15), %rsi
	movq	352(%rax), %rdi
	xorl	%edx, %edx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	testq	%rax, %rax
	jne	.L801
	movq	%rax, -664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-664(%rbp), %rax
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L795:
	leaq	128(%rdi), %rax
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L853:
	movq	96(%rbx,%r8), %rsi
	xorl	%edx, %edx
	movq	%r10, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-664(%rbp), %r8
	movq	-672(%rbp), %r10
	testq	%rax, %rax
	je	.L793
	movq	16(%r15), %rdx
	movq	352(%rdx), %rdi
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L850:
	movq	%r13, %rdi
	movb	$1, -590(%rbp)
	call	_ZN4node21InternalCallbackScopeD1Ev@PLT
	movb	$1, 1777(%r15)
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movl	$-1, %eax
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L849:
	movq	352(%rcx), %rdi
	movq	(%rdx), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r15), %rcx
	movq	%rax, %rdx
	jmp	.L809
	.p2align 4,,10
	.p2align 3
.L845:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L855:
	movb	$1, 1777(%r15)
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L793:
	movq	%r10, -696(%rbp)
	movq	%rax, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r15), %rdx
	movq	-696(%rbp), %r10
	movq	-672(%rbp), %rax
	movq	-664(%rbp), %r8
	movq	352(%rdx), %rdi
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%r10, -696(%rbp)
	movq	%rax, -672(%rbp)
	movq	%r8, -664(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	16(%r15), %rdx
	movq	-696(%rbp), %r10
	movq	-672(%rbp), %rax
	movq	-664(%rbp), %r8
	movq	352(%rdx), %rdi
	jmp	.L814
	.p2align 4,,10
	.p2align 3
.L846:
	leaq	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L852:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L788
.L848:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8486:
	.size	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP18llhttp__internal_s, .-_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP18llhttp__internal_s
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L896
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L881
	cmpw	$1040, %cx
	jne	.L858
.L881:
	movq	23(%rdx), %r15
.L860:
	testq	%r15, %r15
	je	.L856
	cmpq	$0, 1784(%r15)
	jne	.L897
	movq	16(%r15), %rax
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movl	1808(%r15), %eax
	movq	$0, 1792(%r15)
	movq	$0, 1800(%r15)
	movb	$0, 1777(%r15)
	testl	%eax, %eax
	jne	.L898
	movl	$1, 1808(%r15)
	leaq	80(%r15), %r14
	movq	%r14, %rdi
	call	llhttp_finish@PLT
	subl	$1, 1808(%r15)
	xorl	%esi, %esi
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L899
.L864:
	cmpb	$0, 1812(%r15)
	jne	.L900
.L865:
	cmpb	$0, 1777(%r15)
	movq	$0, 1784(%r15)
	movq	$0, 1792(%r15)
	movq	$0, 1800(%r15)
	je	.L866
.L868:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L867:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	testq	%r12, %r12
	je	.L856
	movq	(%r12), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L856:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L901
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L866:
	.cfi_restore_state
	movq	16(%r15), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	cmpb	$0, 160(%r15)
	jne	.L868
	testl	%r12d, %r12d
	je	.L868
	movq	%rax, -120(%rbp)
	movq	16(%r15), %rax
	movq	360(%rax), %rax
	movq	1304(%rax), %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, -112(%rbp)
	movq	16(%r15), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-112(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-120(%rbp), %rcx
	testq	%rax, %rax
	movq	%rax, -104(%rbp)
	je	.L902
.L869:
	movq	16(%r15), %rax
	movq	-104(%rbp), %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	240(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L903
.L870:
	movq	%r14, %rdi
	call	llhttp_get_error_reason@PLT
	movq	%rax, %r14
	cmpl	$23, %r12d
	jne	.L871
	movl	$58, %esi
	movq	%rax, %rdi
	call	strchr@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L904
	movq	%rax, %rcx
	movq	16(%r15), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	subq	%r14, %rcx
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L905
.L873:
	movq	16(%r15), %rax
	movq	%r9, -120(%rbp)
	leaq	1(%r12), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rax), %rdi
.L895:
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-120(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L906
.L874:
	movq	16(%r15), %rax
	movq	-104(%rbp), %rdi
	movq	%r9, %rcx
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	328(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L907
.L876:
	movq	16(%r15), %rax
	movq	-104(%rbp), %rdi
	movq	%r12, %rcx
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1480(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L908
.L877:
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L867
	.p2align 4,,10
	.p2align 3
.L900:
	movb	$0, 1812(%r15)
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	llhttp_pause@PLT
	movq	-104(%rbp), %rsi
	jmp	.L865
	.p2align 4,,10
	.p2align 3
.L899:
	movq	%r14, %rdi
	call	llhttp_get_error_pos@PLT
	movq	%rax, %rsi
	cmpl	$22, %r12d
	jne	.L864
	movq	%r14, %rdi
	movq	%rax, -104(%rbp)
	xorl	%r12d, %r12d
	call	llhttp_resume_after_upgrade@PLT
	movq	-104(%rbp), %rsi
	jmp	.L864
	.p2align 4,,10
	.p2align 3
.L858:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L896:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L897:
	leaq	_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L898:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L871:
	movl	%r12d, %edi
	call	llhttp_errno_name@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	16(%r15), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L909
.L875:
	movq	16(%r15), %rax
	movq	%r9, -120(%rbp)
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	352(%rax), %rdi
	jmp	.L895
.L903:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L870
.L902:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rcx
	jmp	.L869
.L908:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L877
.L907:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L876
.L906:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %r9
	jmp	.L874
.L904:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L909:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %r9
	jmp	.L875
.L905:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %r9
	jmp	.L873
.L901:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7648:
	.size	_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.1
.LC6:
	.string	"HTTPParser"
.LC7:
	.string	"REQUEST"
.LC8:
	.string	"RESPONSE"
.LC9:
	.string	"kOnHeaders"
.LC10:
	.string	"kOnHeadersComplete"
.LC11:
	.string	"kOnBody"
.LC12:
	.string	"kOnMessageComplete"
.LC13:
	.string	"kOnExecute"
.LC14:
	.string	"DELETE"
.LC15:
	.string	"GET"
.LC16:
	.string	"HEAD"
.LC17:
	.string	"POST"
.LC18:
	.string	"PUT"
.LC19:
	.string	"CONNECT"
.LC20:
	.string	"OPTIONS"
.LC21:
	.string	"TRACE"
.LC22:
	.string	"COPY"
.LC23:
	.string	"LOCK"
.LC24:
	.string	"MKCOL"
.LC25:
	.string	"MOVE"
.LC26:
	.string	"PROPFIND"
.LC27:
	.string	"PROPPATCH"
.LC28:
	.string	"SEARCH"
.LC29:
	.string	"UNLOCK"
.LC30:
	.string	"BIND"
.LC31:
	.string	"REBIND"
.LC32:
	.string	"UNBIND"
.LC33:
	.string	"ACL"
.LC34:
	.string	"REPORT"
.LC35:
	.string	"MKACTIVITY"
.LC36:
	.string	"CHECKOUT"
.LC37:
	.string	"MERGE"
.LC38:
	.string	"M-SEARCH"
.LC39:
	.string	"NOTIFY"
.LC40:
	.string	"SUBSCRIBE"
.LC41:
	.string	"UNSUBSCRIBE"
.LC42:
	.string	"PATCH"
.LC43:
	.string	"PURGE"
.LC44:
	.string	"MKCALENDAR"
.LC45:
	.string	"LINK"
.LC46:
	.string	"UNLINK"
.LC47:
	.string	"SOURCE"
.LC48:
	.string	"PRI"
.LC49:
	.string	"methods"
.LC50:
	.string	"close"
.LC51:
	.string	"free"
.LC52:
	.string	"execute"
.LC53:
	.string	"finish"
.LC54:
	.string	"initialize"
.LC55:
	.string	"pause"
.LC56:
	.string	"resume"
.LC57:
	.string	"consume"
.LC58:
	.string	"unconsume"
.LC59:
	.string	"getCurrentBuffer"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB60:
	.text
.LHOTB60:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L911
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L911
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L911
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node12_GLOBAL__N_16Parser3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	leaq	.LC6(%rip), %rsi
	movl	$10, %ecx
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L1009
.L912:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1010
.L913:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	leaq	.LC8(%rip), %rsi
	xorl	%edx, %edx
	movl	$8, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1011
.L914:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1012
.L915:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$1, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1013
.L916:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$2, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	xorl	%edx, %edx
	movl	$7, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1014
.L917:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$3, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC12(%rip), %rsi
	xorl	%edx, %edx
	movl	$18, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1015
.L918:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	movl	$4, %esi
	call	_ZN2v87Integer15NewFromUnsignedEPNS_7IsolateEj@PLT
	movq	352(%rbx), %rdi
	leaq	.LC13(%rip), %rsi
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1016
.L919:
	movq	%r12, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	352(%rbx), %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$6, %ecx
	leaq	.LC14(%rip), %rsi
	movq	%rax, %r12
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1017
.L920:
	movq	3280(%rbx), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1018
.L921:
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1019
.L922:
	movq	3280(%rbx), %rsi
	movl	$1, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1020
.L923:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1021
.L924:
	movq	3280(%rbx), %rsi
	movl	$2, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1022
.L925:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1023
.L926:
	movq	3280(%rbx), %rsi
	movl	$3, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1024
.L927:
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1025
.L928:
	movq	3280(%rbx), %rsi
	movl	$4, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1026
.L929:
	movq	352(%rbx), %rdi
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1027
.L930:
	movq	3280(%rbx), %rsi
	movl	$5, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1028
.L931:
	movq	352(%rbx), %rdi
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1029
.L932:
	movq	3280(%rbx), %rsi
	movl	$6, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1030
.L933:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1031
.L934:
	movq	3280(%rbx), %rsi
	movl	$7, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1032
.L935:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC22(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1033
.L936:
	movq	3280(%rbx), %rsi
	movl	$8, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1034
.L937:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC23(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1035
.L938:
	movq	3280(%rbx), %rsi
	movl	$9, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1036
.L939:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC24(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1037
.L940:
	movq	3280(%rbx), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1038
.L941:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC25(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1039
.L942:
	movq	3280(%rbx), %rsi
	movl	$11, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1040
.L943:
	movq	352(%rbx), %rdi
	movl	$8, %ecx
	xorl	%edx, %edx
	leaq	.LC26(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1041
.L944:
	movq	3280(%rbx), %rsi
	movl	$12, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1042
.L945:
	movq	352(%rbx), %rdi
	movl	$9, %ecx
	xorl	%edx, %edx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1043
.L946:
	movq	3280(%rbx), %rsi
	movl	$13, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1044
.L947:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC28(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1045
.L948:
	movq	3280(%rbx), %rsi
	movl	$14, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1046
.L949:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC29(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1047
.L950:
	movq	3280(%rbx), %rsi
	movl	$15, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1048
.L951:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC30(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1049
.L952:
	movq	3280(%rbx), %rsi
	movl	$16, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1050
.L953:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC31(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1051
.L954:
	movq	3280(%rbx), %rsi
	movl	$17, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1052
.L955:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC32(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1053
.L956:
	movq	3280(%rbx), %rsi
	movl	$18, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1054
.L957:
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC33(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1055
.L958:
	movq	3280(%rbx), %rsi
	movl	$19, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1056
.L959:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC34(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1057
.L960:
	movq	3280(%rbx), %rsi
	movl	$20, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1058
.L961:
	movq	352(%rbx), %rdi
	movl	$10, %ecx
	xorl	%edx, %edx
	leaq	.LC35(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1059
.L962:
	movq	3280(%rbx), %rsi
	movl	$21, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1060
.L963:
	movq	352(%rbx), %rdi
	movl	$8, %ecx
	xorl	%edx, %edx
	leaq	.LC36(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1061
.L964:
	movq	3280(%rbx), %rsi
	movl	$22, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1062
.L965:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC37(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1063
.L966:
	movq	3280(%rbx), %rsi
	movl	$23, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1064
.L967:
	movq	352(%rbx), %rdi
	movl	$8, %ecx
	xorl	%edx, %edx
	leaq	.LC38(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1065
.L968:
	movq	3280(%rbx), %rsi
	movl	$24, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1066
.L969:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC39(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1067
.L970:
	movq	3280(%rbx), %rsi
	movl	$25, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1068
.L971:
	movq	352(%rbx), %rdi
	movl	$9, %ecx
	xorl	%edx, %edx
	leaq	.LC40(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1069
.L972:
	movq	3280(%rbx), %rsi
	movl	$26, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1070
.L973:
	movq	352(%rbx), %rdi
	movl	$11, %ecx
	xorl	%edx, %edx
	leaq	.LC41(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1071
.L974:
	movq	3280(%rbx), %rsi
	movl	$27, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1072
.L975:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC42(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1073
.L976:
	movq	3280(%rbx), %rsi
	movl	$28, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1074
.L977:
	movq	352(%rbx), %rdi
	movl	$5, %ecx
	xorl	%edx, %edx
	leaq	.LC43(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1075
.L978:
	movq	3280(%rbx), %rsi
	movl	$29, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1076
.L979:
	movq	352(%rbx), %rdi
	movl	$10, %ecx
	xorl	%edx, %edx
	leaq	.LC44(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1077
.L980:
	movq	3280(%rbx), %rsi
	movl	$30, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1078
.L981:
	movq	352(%rbx), %rdi
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC45(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1079
.L982:
	movq	3280(%rbx), %rsi
	movl	$31, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1080
.L983:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC46(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1081
.L984:
	movq	3280(%rbx), %rsi
	movl	$32, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1082
.L985:
	movq	352(%rbx), %rdi
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC47(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1083
.L986:
	movq	3280(%rbx), %rsi
	movl	$33, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1084
.L987:
	movq	352(%rbx), %rdi
	movl	$3, %ecx
	xorl	%edx, %edx
	leaq	.LC48(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1085
.L988:
	movq	3280(%rbx), %rsi
	movl	$34, %edx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1086
.L989:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$7, %ecx
	leaq	.LC49(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1087
.L990:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1088
.L991:
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser5CloseERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC50(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r12
	popq	%rax
	testq	%r12, %r12
	je	.L1089
.L992:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser4FreeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC51(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1090
.L993:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC52(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1091
.L994:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC53(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1092
.L995:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC54(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L1093
.L996:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC55(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r11
	movq	%rax, %r12
	popq	%rax
	testq	%r12, %r12
	je	.L1094
.L997:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC56(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1095
.L998:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	movl	$1, %edx
	leaq	.LC57(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1096
.L999:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser9UnconsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC58(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1097
.L1000:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r15, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r13, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node12_GLOBAL__N_16Parser16GetCurrentBufferERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC59(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L1098
.L1001:
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r13, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L1099
.L1002:
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$10, %ecx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L1100
.L1003:
	movq	3280(%rbx), %rsi
	movq	%r12, %rcx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1101
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1009:
	.cfi_restore_state
	movq	%rsi, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L912
	.p2align 4,,10
	.p2align 3
.L1010:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L913
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L914
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L1013:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L916
	.p2align 4,,10
	.p2align 3
.L1014:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L917
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rsi
	jmp	.L919
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L1018:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1020:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L924
	.p2align 4,,10
	.p2align 3
.L1022:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L1023:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L1024:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1025:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L1026:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L929
	.p2align 4,,10
	.p2align 3
.L1027:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L1028:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L1029:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L1030:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1031:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L1032:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L935
	.p2align 4,,10
	.p2align 3
.L1033:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L936
	.p2align 4,,10
	.p2align 3
.L1034:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L937
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L938
	.p2align 4,,10
	.p2align 3
.L1036:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L1037:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L1038:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L1039:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1040:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L1041:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1042:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L1043:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L946
	.p2align 4,,10
	.p2align 3
.L1044:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L1045:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L948
	.p2align 4,,10
	.p2align 3
.L1046:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1047:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L1048:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L951
	.p2align 4,,10
	.p2align 3
.L1049:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L1050:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L953
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L954
	.p2align 4,,10
	.p2align 3
.L1052:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L1054:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L957
	.p2align 4,,10
	.p2align 3
.L1055:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L958
	.p2align 4,,10
	.p2align 3
.L1056:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L959
	.p2align 4,,10
	.p2align 3
.L1057:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L960
	.p2align 4,,10
	.p2align 3
.L1058:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L1059:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L962
	.p2align 4,,10
	.p2align 3
.L1060:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L963
	.p2align 4,,10
	.p2align 3
.L1061:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L964
	.p2align 4,,10
	.p2align 3
.L1062:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L965
	.p2align 4,,10
	.p2align 3
.L1063:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L966
	.p2align 4,,10
	.p2align 3
.L1064:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L968
	.p2align 4,,10
	.p2align 3
.L1066:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L969
	.p2align 4,,10
	.p2align 3
.L1067:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L970
	.p2align 4,,10
	.p2align 3
.L1068:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L971
	.p2align 4,,10
	.p2align 3
.L1069:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L1070:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L1071:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L974
	.p2align 4,,10
	.p2align 3
.L1072:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L975
	.p2align 4,,10
	.p2align 3
.L1073:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L976
	.p2align 4,,10
	.p2align 3
.L1074:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L977
	.p2align 4,,10
	.p2align 3
.L1075:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L978
	.p2align 4,,10
	.p2align 3
.L1076:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L979
	.p2align 4,,10
	.p2align 3
.L1077:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L980
	.p2align 4,,10
	.p2align 3
.L1078:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L981
	.p2align 4,,10
	.p2align 3
.L1079:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L982
	.p2align 4,,10
	.p2align 3
.L1080:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1082:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L985
	.p2align 4,,10
	.p2align 3
.L1083:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L986
	.p2align 4,,10
	.p2align 3
.L1084:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1085:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L988
	.p2align 4,,10
	.p2align 3
.L1086:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L989
	.p2align 4,,10
	.p2align 3
.L1087:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L990
	.p2align 4,,10
	.p2align 3
.L1088:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L991
	.p2align 4,,10
	.p2align 3
.L1089:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1090:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L993
	.p2align 4,,10
	.p2align 3
.L1091:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L994
	.p2align 4,,10
	.p2align 3
.L1092:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L995
	.p2align 4,,10
	.p2align 3
.L1093:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L996
	.p2align 4,,10
	.p2align 3
.L1094:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L997
	.p2align 4,,10
	.p2align 3
.L1095:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L998
	.p2align 4,,10
	.p2align 3
.L1096:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1097:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1098:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1001
	.p2align 4,,10
	.p2align 3
.L1099:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1100:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rdx
	jmp	.L1003
	.p2align 4,,10
	.p2align 3
.L1101:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7665:
.L911:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7665:
	.text
	.size	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE60:
	.text
.LHOTE60:
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm, @function
_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm:
.LFB7657:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$88, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movl	1808(%r13), %ecx
	movq	%r12, 1792(%r13)
	movq	%rbx, 1800(%r13)
	movb	$0, 1777(%r13)
	testl	%ecx, %ecx
	jne	.L1163
	movl	$1, 1808(%r13)
	leaq	80(%r13), %r15
	testq	%rbx, %rbx
	je	.L1164
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	llhttp_execute@PLT
	cmpb	$0, 1720(%r13)
	movl	%eax, -104(%rbp)
	jne	.L1106
	movq	1728(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L1165
.L1106:
	cmpb	$0, 1744(%r13)
	je	.L1166
.L1107:
	movq	1760(%r13), %rax
	leaq	176(%r13), %rcx
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L1112
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1110:
	addq	$1, %r9
	addq	$24, %rcx
	cmpq	%rax, %r9
	jnb	.L1111
.L1112:
	cmpb	$0, 8(%rcx)
	jne	.L1110
	movq	16(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1110
	movq	%r9, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_Znam@PLT
	movq	-112(%rbp), %rcx
	movq	%rax, %rdi
	movq	(%rcx), %rsi
	movq	16(%rcx), %rdx
	call	memcpy@PLT
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	%rax, (%rcx)
	addq	$1, %r9
	addq	$24, %rcx
	movb	$1, -16(%rcx)
	movq	1760(%r13), %rax
	cmpq	%rax, %r9
	jb	.L1112
	.p2align 4,,10
	.p2align 3
.L1111:
	movq	1768(%r13), %rax
	leaq	944(%r13), %rcx
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L1114
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1113:
	addq	$1, %r9
	addq	$24, %rcx
	cmpq	%rax, %r9
	jnb	.L1105
.L1114:
	cmpb	$0, 8(%rcx)
	jne	.L1113
	movq	16(%rcx), %rdi
	testq	%rdi, %rdi
	je	.L1113
	movq	%r9, -120(%rbp)
	movq	%rcx, -112(%rbp)
	call	_Znam@PLT
	movq	-112(%rbp), %rcx
	movq	%rax, %rdi
	movq	(%rcx), %rsi
	movq	16(%rcx), %rdx
	call	memcpy@PLT
	movq	-112(%rbp), %rcx
	movq	-120(%rbp), %r9
	movq	%rax, (%rcx)
	addq	$1, %r9
	addq	$24, %rcx
	movb	$1, -16(%rcx)
	movq	1768(%r13), %rax
	cmpq	%rax, %r9
	jb	.L1114
	.p2align 4,,10
	.p2align 3
.L1105:
	movl	-104(%rbp), %edx
	subl	$1, 1808(%r13)
	testl	%edx, %edx
	jne	.L1167
.L1115:
	cmpb	$0, 1812(%r13)
	jne	.L1168
.L1116:
	cmpb	$0, 1777(%r13)
	movq	$0, 1784(%r13)
	movq	$0, 1792(%r13)
	movq	$0, 1800(%r13)
	je	.L1117
.L1160:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
.L1118:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1169
	addq	$88, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1117:
	.cfi_restore_state
	movq	16(%r13), %rax
	movl	%r12d, %esi
	movq	352(%rax), %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	cmpb	$0, 160(%r13)
	movq	%rax, %r9
	jne	.L1119
	movl	-104(%rbp), %eax
	testl	%eax, %eax
	jne	.L1170
.L1119:
	testq	%rbx, %rbx
	je	.L1160
	movq	%r9, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	1752(%r13), %rdi
	testq	%rdi, %rdi
	je	.L1107
	call	_Znam@PLT
	movq	1736(%r13), %rsi
	movq	1752(%r13), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movb	$1, 1744(%r13)
	movq	%rax, 1736(%r13)
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1167:
	movq	%r15, %rdi
	call	llhttp_get_error_pos@PLT
	subq	%rbx, %rax
	cmpl	$22, -104(%rbp)
	movq	%rax, %r12
	jne	.L1115
	movq	%r15, %rdi
	call	llhttp_resume_after_upgrade@PLT
	movl	$0, -104(%rbp)
	jmp	.L1115
	.p2align 4,,10
	.p2align 3
.L1168:
	movb	$0, 1812(%r13)
	movq	%r15, %rdi
	call	llhttp_pause@PLT
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1164:
	movq	%r15, %rdi
	call	llhttp_finish@PLT
	movl	%eax, -104(%rbp)
	jmp	.L1105
	.p2align 4,,10
	.p2align 3
.L1165:
	call	_Znam@PLT
	movq	1712(%r13), %rsi
	movq	1728(%r13), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	cmpb	$0, 1744(%r13)
	movb	$1, 1720(%r13)
	movq	%rax, 1712(%r13)
	jne	.L1107
	jmp	.L1166
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	16(%r13), %rax
	movq	%r9, -112(%rbp)
	movq	360(%rax), %rax
	movq	1304(%rax), %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%rax, %rbx
	movq	16(%r13), %rax
	movq	352(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rbx, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	-112(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1171
.L1120:
	movq	16(%r13), %rax
	movq	%r9, %rcx
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	240(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1172
.L1121:
	movq	%r15, %rdi
	call	llhttp_get_error_reason@PLT
	cmpl	$23, -104(%rbp)
	movq	%rax, %r15
	jne	.L1122
	movl	$58, %esi
	movq	%rax, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L1173
	movq	%rax, -104(%rbp)
	movq	%rax, %rcx
	movq	16(%r13), %rax
	xorl	%edx, %edx
	subq	%r15, %rcx
	movq	%r15, %rsi
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L1174
.L1124:
	movq	16(%r13), %rax
	movq	%r9, -104(%rbp)
	leaq	1(%r8), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	352(%rax), %rdi
.L1162:
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1175
.L1125:
	movq	16(%r13), %rax
	movq	%r9, %rcx
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	328(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1176
.L1127:
	movq	16(%r13), %rax
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	360(%rax), %rdx
	movq	3280(%rax), %rsi
	movq	1480(%rdx), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L1177
.L1128:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%rax, %r12
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1163:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1122:
	movl	-104(%rbp), %edi
	call	llhttp_errno_name@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	16(%r13), %rax
	movq	352(%rax), %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L1178
.L1126:
	movq	16(%r13), %rax
	movq	%r9, -104(%rbp)
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	352(%rax), %rdi
	jmp	.L1162
.L1172:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1121
.L1171:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %r9
	jmp	.L1120
.L1177:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1128
.L1176:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1127
.L1175:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %r9
	jmp	.L1125
.L1173:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1178:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %r9
	jmp	.L1126
.L1174:
	movq	%r8, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %r8
	movq	-104(%rbp), %r9
	jmp	.L1124
.L1169:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7657:
	.size	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm, .-_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	.align 2
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, @function
_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	16(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	js	.L1204
	movq	0(%r13), %rsi
	jne	.L1205
	movq	16(%rbx), %rax
	cmpq	2184(%rax), %rsi
	je	.L1201
.L1191:
	movq	%rsi, %rdi
	call	free@PLT
.L1190:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1206
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1205:
	.cfi_restore_state
	movq	$0, 1784(%rbx)
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L1203
	movq	8(%rbx), %rdi
	movq	16(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L1186
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1207
.L1186:
	movq	3280(%rdx), %rsi
	movl	$4, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1208
.L1187:
	movq	%r15, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L1188
.L1203:
	movq	0(%r13), %rsi
.L1210:
	movq	16(%rbx), %rax
	cmpq	2184(%rax), %rsi
	jne	.L1191
.L1201:
	movb	$0, 2192(%rax)
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1188:
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	leaq	-96(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, 1792(%rbx)
	movq	%r15, %rsi
	movq	%rax, 1800(%rbx)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	16(%rbx), %rax
	movq	0(%r13), %rdi
	movq	$0, 1792(%rbx)
	movq	$0, 1800(%rbx)
	cmpq	2184(%rax), %rdi
	je	.L1201
	call	free@PLT
	jmp	.L1190
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L1186
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	72(%rbx), %r15
	testq	%r15, %r15
	je	.L1209
	movq	(%r15), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rax), %rcx
	movq	%rcx, -104(%rbp)
	call	uv_buf_init@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	leaq	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t(%rip), %rax
	movq	%rdx, -88(%rbp)
	leaq	-96(%rbp), %rdx
	cmpq	%rax, %rcx
	jne	.L1182
	movq	%r12, %rsi
	leaq	-56(%r15), %rdi
	call	.LTHUNK1
	movq	0(%r13), %rsi
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1208:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L1187
	.p2align 4,,10
	.p2align 3
.L1182:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rcx
	jmp	.L1203
	.p2align 4,,10
	.p2align 3
.L1209:
	leaq	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1206:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7655:
	.size	_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, .-_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.set	.LTHUNK1,_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1233
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1231
	cmpw	$1040, %cx
	jne	.L1213
.L1231:
	movq	23(%rdx), %r12
.L1215:
	testq	%r12, %r12
	je	.L1211
	cmpq	$0, 1784(%r12)
	jne	.L1234
	cmpq	$0, 1792(%r12)
	jne	.L1235
	cmpq	$0, 1800(%r12)
	jne	.L1236
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L1237
	movq	8(%rbx), %r13
.L1221:
	movq	%r13, %rdi
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	_ZNK2v85Value17IsArrayBufferViewEv@PLT
	testb	%al, %al
	je	.L1238
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteLengthEv@PLT
	movq	%rax, -56(%rbp)
	cmpq	$64, %rax
	ja	.L1225
	movq	%r13, %rdi
	call	_ZNK2v815ArrayBufferView9HasBufferEv@PLT
	testb	%al, %al
	je	.L1239
.L1225:
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView6BufferEv@PLT
	leaq	-192(%rbp), %rdi
	movq	%rax, %rsi
	call	_ZN2v811ArrayBuffer11GetContentsEv@PLT
	movq	-192(%rbp), %r14
	movq	%r13, %rdi
	call	_ZN2v815ArrayBufferView10ByteOffsetEv@PLT
	leaq	(%r14,%rax), %rsi
	movq	%rsi, -64(%rbp)
.L1224:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L1226
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L1227:
	movq	%rax, 1784(%r12)
	movq	-56(%rbp), %rdx
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	testq	%rax, %rax
	je	.L1211
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L1211:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1240
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1237:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1226:
	movq	8(%rbx), %rax
	jmp	.L1227
	.p2align 4,,10
	.p2align 3
.L1213:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1215
	.p2align 4,,10
	.p2align 3
.L1239:
	leaq	-128(%rbp), %r14
	movl	$64, %edx
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	_ZN2v815ArrayBufferView12CopyContentsEPvm@PLT
	movq	%r14, -64(%rbp)
	movq	%r14, %rsi
	jmp	.L1224
	.p2align 4,,10
	.p2align 3
.L1233:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1234:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1235:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1236:
	leaq	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1238:
	leaq	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1240:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7647:
	.size	_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, @function
_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t:
.LFB10093:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-40(%rdi), %rax
	movq	%r14, %rdi
	movq	352(%rax), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	testq	%r12, %r12
	js	.L1266
	movq	0(%r13), %rsi
	jne	.L1267
	movq	-40(%rbx), %rax
	cmpq	2184(%rax), %rsi
	je	.L1263
.L1253:
	movq	%rsi, %rdi
	call	free@PLT
.L1252:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1268
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1267:
	.cfi_restore_state
	movq	$0, 1728(%rbx)
	leaq	-56(%rbx), %r15
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN4node12_GLOBAL__N_16Parser7ExecuteEPKcm
	movq	%rax, -96(%rbp)
	testq	%rax, %rax
	je	.L1265
	movq	-48(%rbx), %rdi
	movq	-40(%rbx), %rdx
	testq	%rdi, %rdi
	je	.L1248
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1269
.L1248:
	movq	3280(%rdx), %rsi
	movl	$4, %edx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L1270
.L1249:
	movq	%rsi, %rdi
	movq	%rsi, -104(%rbp)
	call	_ZNK2v85Value10IsFunctionEv@PLT
	movq	-104(%rbp), %rsi
	testb	%al, %al
	jne	.L1250
.L1265:
	movq	0(%r13), %rsi
.L1273:
	movq	-40(%rbx), %rax
	cmpq	2184(%rax), %rsi
	jne	.L1253
.L1263:
	movb	$0, 2192(%rax)
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1250:
	movq	0(%r13), %rax
	movq	%r15, %rdi
	leaq	-96(%rbp), %rcx
	movl	$1, %edx
	movq	%r12, 1736(%rbx)
	movq	%rax, 1744(%rbx)
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	movq	-40(%rbx), %rax
	movq	0(%r13), %rdi
	movq	$0, 1736(%rbx)
	movq	$0, 1744(%rbx)
	cmpq	2184(%rax), %rdi
	je	.L1263
	call	free@PLT
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	-40(%rbx), %rdx
	movq	%rax, %rdi
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1266:
	movq	16(%rbx), %r15
	testq	%r15, %r15
	je	.L1271
	movq	(%r15), %rax
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	24(%rax), %rcx
	movq	%rcx, -104(%rbp)
	call	uv_buf_init@PLT
	movq	-104(%rbp), %rcx
	movq	%rax, -96(%rbp)
	leaq	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t(%rip), %rax
	movq	%rdx, -88(%rbp)
	leaq	-96(%rbp), %rdx
	cmpq	%rax, %rcx
	je	.L1272
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*%rcx
	movq	0(%r13), %rsi
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1270:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rsi
	jmp	.L1249
	.p2align 4,,10
	.p2align 3
.L1271:
	leaq	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1268:
	call	__stack_chk_fail@PLT
.L1272:
	leaq	-56(%r15), %rdi
	movq	%r12, %rsi
	call	.LTHUNK1
	jmp	.L1265
	.cfi_endproc
.LFE10093:
	.size	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t, .-_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.p2align 4
	.globl	_Z28_register_http_parser_llhttpv
	.type	_Z28_register_http_parser_llhttpv, @function
_Z28_register_http_parser_llhttpv:
.LFB7716:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7716:
	.size	_Z28_register_http_parser_llhttpv, .-_Z28_register_http_parser_llhttpv
	.section	.text._ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_,"axG",@progbits,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC5EPS1_,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.type	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, @function
_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_:
.LFB8414:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testq	%rsi, %rsi
	je	.L1292
	movq	%rsi, (%r12)
	movq	24(%rsi), %rax
	movq	%rsi, %rbx
	testq	%rax, %rax
	je	.L1293
.L1278:
	movl	(%rax), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%rax)
	testl	%edx, %edx
	je	.L1282
.L1275:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1293:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	$0, (%rax)
	movw	%cx, 8(%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1283
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1279:
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rbx
	movb	%dl, 8(%rax)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	jne	.L1278
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1284
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1280:
	movb	%dl, 8(%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, 24(%rbx)
	movl	$1, (%rax)
.L1282:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1275
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V89ClearWeakEPm@PLT
	.p2align 4,,10
	.p2align 3
.L1292:
	.cfi_restore_state
	movq	$0, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1283:
	.cfi_restore_state
	xorl	%edx, %edx
	jmp	.L1279
	.p2align 4,,10
	.p2align 3
.L1284:
	xorl	%edx, %edx
	jmp	.L1280
	.cfi_endproc
.LFE8414:
	.size	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_, .-_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.weak	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	.set	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_,_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC2EPS1_
	.section	.text._ZN4node12ShutdownWrap6OnDoneEi,"axG",@progbits,_ZN4node12ShutdownWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12ShutdownWrap6OnDoneEi
	.type	_ZN4node12ShutdownWrap6OnDoneEi, @function
_ZN4node12ShutdownWrap6OnDoneEi:
.LFB7605:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1295
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1298
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1297
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1298
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1295
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1298
	movq	(%rdi), %rcx
	movq	40(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1297
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1298
	movq	(%rdi), %rax
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1295
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1298
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*40(%rax)
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1295:
	movq	%r12, %rsi
	call	*%rax
.L1302:
	movq	(%r12), %rax
	leaq	_ZN4node18SimpleShutdownWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1303
	leaq	16(%r12), %rsi
.L1304:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1305
	addq	$16, %r12
.L1306:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1307
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1335
.L1307:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1336
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1311
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L1294
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L1294:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1337
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1298:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1297:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L1302
	.p2align 4,,10
	.p2align 3
.L1335:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L1307
	.p2align 4,,10
	.p2align 3
.L1305:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1304
	.p2align 4,,10
	.p2align 3
.L1336:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1314
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1309:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1311:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1314:
	xorl	%edx, %edx
	jmp	.L1309
.L1337:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7605:
	.size	_ZN4node12ShutdownWrap6OnDoneEi, .-_ZN4node12ShutdownWrap6OnDoneEi
	.section	.text._ZN4node9WriteWrap6OnDoneEi,"axG",@progbits,_ZN4node9WriteWrap6OnDoneEi,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9WriteWrap6OnDoneEi
	.type	_ZN4node9WriteWrap6OnDoneEi, @function
_ZN4node9WriteWrap6OnDoneEi:
.LFB7607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi(%rip), %rcx
	movl	%esi, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	.cfi_offset 3, -32
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	8(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1339
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1342
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1341
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1342
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1339
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1342
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.L1341
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1342
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L1339
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1342
	movq	(%rdi), %rax
	movq	%r12, %rsi
	call	*32(%rax)
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	%r12, %rsi
	call	*%rax
.L1346:
	movq	(%r12), %rax
	leaq	_ZN4node15SimpleWriteWrapINS_9AsyncWrapEE12GetAsyncWrapEv(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1347
	leaq	40(%r12), %rsi
.L1348:
	leaq	-32(%rbp), %rdi
	call	_ZN4node17BaseObjectPtrImplINS_9AsyncWrapELb0EEC1EPS1_
	movq	(%r12), %rax
	movq	16(%rax), %rax
	cmpq	%rbx, %rax
	jne	.L1349
	addq	$40, %r12
.L1350:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L1351
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L1379
.L1351:
	xorl	%edx, %edx
	movl	$1, %esi
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	-32(%rbp), %r12
	movq	24(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1380
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L1355
	subl	$1, %eax
	movb	$1, 9(%rdx)
	movl	%eax, (%rdx)
	jne	.L1338
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
.L1338:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1381
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1342:
	.cfi_restore_state
	leaq	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1341:
	movq	%r12, %rsi
	call	*%rcx
	jmp	.L1346
	.p2align 4,,10
	.p2align 3
.L1379:
	movq	16(%r12), %rax
	movq	(%rdi), %rsi
	movq	352(%rax), %r8
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1349:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1350
	.p2align 4,,10
	.p2align 3
.L1347:
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rsi
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1380:
	movl	$24, %edi
	call	_Znwm@PLT
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
	movq	8(%r12), %rdx
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	testq	%rdx, %rdx
	je	.L1358
	movzbl	11(%rdx), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	sete	%dl
.L1353:
	movb	%dl, 8(%rax)
	movq	%r12, 16(%rax)
	movq	%rax, 24(%r12)
.L1355:
	leaq	_ZZN4node10BaseObject6DetachEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1358:
	xorl	%edx, %edx
	jmp	.L1353
.L1381:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7607:
	.size	_ZN4node9WriteWrap6OnDoneEi, .-_ZN4node9WriteWrap6OnDoneEi
	.weak	_ZTVN4node14StreamListenerE
	.section	.data.rel.ro._ZTVN4node14StreamListenerE,"awG",@progbits,_ZTVN4node14StreamListenerE,comdat
	.align 8
	.type	_ZTVN4node14StreamListenerE, @object
	.size	_ZTVN4node14StreamListenerE, 80
_ZTVN4node14StreamListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.weak	_ZTVN4node14StreamResourceE
	.section	.data.rel.ro._ZTVN4node14StreamResourceE,"awG",@progbits,_ZTVN4node14StreamResourceE,comdat
	.align 8
	.type	_ZTVN4node14StreamResourceE, @object
	.size	_ZTVN4node14StreamResourceE, 96
_ZTVN4node14StreamResourceE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node14StreamResource10DoTryWriteEPP8uv_buf_tPm
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node14StreamResource13HasWantsWriteEv
	.quad	_ZNK4node14StreamResource5ErrorEv
	.quad	_ZN4node14StreamResource10ClearErrorEv
	.weak	_ZTVN4node12ShutdownWrapE
	.section	.data.rel.ro._ZTVN4node12ShutdownWrapE,"awG",@progbits,_ZTVN4node12ShutdownWrapE,comdat
	.align 8
	.type	_ZTVN4node12ShutdownWrapE, @object
	.size	_ZTVN4node12ShutdownWrapE, 48
_ZTVN4node12ShutdownWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node12ShutdownWrap6OnDoneEi
	.weak	_ZTVN4node9WriteWrapE
	.section	.data.rel.ro._ZTVN4node9WriteWrapE,"awG",@progbits,_ZTVN4node9WriteWrapE,comdat
	.align 8
	.type	_ZTVN4node9WriteWrapE, @object
	.size	_ZTVN4node9WriteWrapE, 48
_ZTVN4node9WriteWrapE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZN4node9WriteWrap6OnDoneEi
	.section	.data.rel.ro,"aw"
	.align 8
	.type	_ZTVN4node12_GLOBAL__N_16ParserE, @object
	.size	_ZTVN4node12_GLOBAL__N_16ParserE, 192
_ZTVN4node12_GLOBAL__N_16ParserE:
	.quad	0
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_16ParserD1Ev
	.quad	_ZN4node12_GLOBAL__N_16ParserD0Ev
	.quad	_ZNK4node12_GLOBAL__N_16Parser10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node12_GLOBAL__N_16Parser14MemoryInfoNameEv
	.quad	_ZNK4node12_GLOBAL__N_16Parser8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.quad	_ZN4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node12_GLOBAL__N_16ParserD1Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_16ParserD0Ev
	.quad	_ZThn56_N4node12_GLOBAL__N_16Parser13OnStreamAllocEm
	.quad	_ZThn56_N4node12_GLOBAL__N_16Parser12OnStreamReadElRK8uv_buf_t
	.quad	_ZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEi
	.quad	_ZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEi
	.quad	_ZN4node14StreamListener18OnStreamWantsWriteEm
	.quad	_ZN4node14StreamListener15OnStreamDestroyEv
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"../src/node_http_parser_impl.h:549"
	.section	.rodata.str1.1
.LC62:
	.string	"(env) == (parser->env())"
	.section	.rodata.str1.8
	.align 8
.LC63:
	.string	"static void node::{anonymous}::Parser::Pause(const v8::FunctionCallbackInfo<v8::Value>&) [with bool should_pause = false]"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser5PauseILb0EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.section	.rodata.str1.8
	.align 8
.LC64:
	.string	"static void node::{anonymous}::Parser::Pause(const v8::FunctionCallbackInfo<v8::Value>&) [with bool should_pause = true]"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser5PauseILb1EEEvRKN2v820FunctionCallbackInfoINS3_5ValueEEEE4args:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC64
	.weak	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC65:
	.string	"../src/util-inl.h:495"
.LC66:
	.string	"value->IsArrayBufferView()"
	.section	.rodata.str1.8
	.align 8
.LC67:
	.string	"node::ArrayBufferViewContents<T, kStackStorageSize>::ArrayBufferViewContents(v8::Local<v8::Value>) [with T = char; long unsigned int kStackStorageSize = 64]"
	.section	.data.rel.ro.local._ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args, 24
_ZZN4node23ArrayBufferViewContentsIcLm64EEC4EN2v85LocalINS2_5ValueEEEE4args:
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.weak	_ZZN4node6MallocIcEEPT_mE4args
	.section	.rodata.str1.1
.LC68:
	.string	"../src/util-inl.h:381"
.LC69:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"T* node::Malloc(size_t) [with T = char; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node6MallocIcEEPT_mE4args,"awG",@progbits,_ZZN4node6MallocIcEEPT_mE4args,comdat
	.align 16
	.type	_ZZN4node6MallocIcEEPT_mE4args, @gnu_unique_object
	.size	_ZZN4node6MallocIcEEPT_mE4args, 24
_ZZN4node6MallocIcEEPT_mE4args:
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"../src/node_http_parser_llhttp.cc"
	.section	.rodata.str1.1
.LC72:
	.string	"http_parser_llhttp"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC71
	.quad	0
	.quad	_ZN4node12_GLOBAL__N_120InitializeHttpParserEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC72
	.quad	0
	.quad	0
	.globl	_ZN4node11per_process14llhttp_versionE
	.section	.rodata.str1.1
.LC73:
	.string	"2.1.2"
	.section	.data.rel.ro.local
	.align 8
	.type	_ZN4node11per_process14llhttp_versionE, @object
	.size	_ZN4node11per_process14llhttp_versionE, 8
_ZN4node11per_process14llhttp_versionE:
	.quad	.LC73
	.align 32
	.type	_ZN4node12_GLOBAL__N_16Parser8settingsE, @object
	.size	_ZN4node12_GLOBAL__N_16Parser8settingsE, 80
_ZN4node12_GLOBAL__N_16Parser8settingsE:
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_16on_message_beginEvEEE3RawEP18llhttp__internal_s
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_6on_urlES4_mEEE3RawEP18llhttp__internal_sS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_9on_statusES4_mEEE3RawEP18llhttp__internal_sS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_fieldES4_mEEE3RawEP18llhttp__internal_sS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_15on_header_valueES4_mEEE3RawEP18llhttp__internal_sS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_headers_completeEvEEE3RawEP18llhttp__internal_s
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FiPKcmEXadL_ZNS1_7on_bodyES4_mEEE3RawEP18llhttp__internal_sS4_m
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_19on_message_completeEvEEE3RawEP18llhttp__internal_s
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_15on_chunk_headerEvEEE3RawEP18llhttp__internal_s
	.quad	_ZN4node12_GLOBAL__N_16Parser5ProxyIMS1_FivEXadL_ZNS1_17on_chunk_completeEvEEE3RawEP18llhttp__internal_s
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"../src/node_http_parser_impl.h:856"
	.section	.rodata.str1.1
.LC75:
	.string	"(execute_depth_) != (0)"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"int node::{anonymous}::Parser::MaybePause()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args, 24
_ZZN4node12_GLOBAL__N_16Parser10MaybePauseEvE4args:
	.quad	.LC74
	.quad	.LC75
	.quad	.LC76
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"../src/node_http_parser_impl.h:757"
	.section	.rodata.str1.1
.LC78:
	.string	"(colon) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"v8::Local<v8::Value> node::{anonymous}::Parser::Execute(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args_0:
	.quad	.LC77
	.quad	.LC78
	.quad	.LC79
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"../src/node_http_parser_impl.h:676"
	.section	.rodata.str1.1
.LC81:
	.string	"(execute_depth_) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteEPKcmE4args:
	.quad	.LC80
	.quad	.LC81
	.quad	.LC79
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"../src/node_http_parser_impl.h:573"
	.section	.rodata.str1.1
.LC83:
	.string	"(stream) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"static void node::{anonymous}::Parser::Consume(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC82
	.quad	.LC83
	.quad	.LC84
	.section	.rodata.str1.8
	.align 8
.LC85:
	.string	"../src/node_http_parser_impl.h:571"
	.section	.rodata.str1.1
.LC86:
	.string	"args[0]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser7ConsumeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC85
	.quad	.LC86
	.quad	.LC84
	.section	.rodata.str1.8
	.align 8
.LC87:
	.string	"../src/node_http_parser_impl.h:531"
	.align 8
.LC88:
	.string	"static void node::{anonymous}::Parser::Initialize(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_2:
	.quad	.LC87
	.quad	.LC62
	.quad	.LC88
	.section	.rodata.str1.8
	.align 8
.LC89:
	.string	"../src/node_http_parser_impl.h:527"
	.align 8
.LC90:
	.string	"type == HTTP_REQUEST || type == HTTP_RESPONSE"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC89
	.quad	.LC90
	.quad	.LC88
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"../src/node_http_parser_impl.h:522"
	.section	.rodata.str1.1
.LC92:
	.string	"args[1]->IsObject()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC91
	.quad	.LC92
	.quad	.LC88
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"../src/node_http_parser_impl.h:521"
	.section	.rodata.str1.1
.LC94:
	.string	"args[0]->IsInt32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser10InitializeERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC93
	.quad	.LC94
	.quad	.LC88
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"../src/node_http_parser_impl.h:509"
	.align 8
.LC96:
	.string	"parser->current_buffer_.IsEmpty()"
	.align 8
.LC97:
	.string	"static void node::{anonymous}::Parser::Finish(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser6FinishERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC95
	.quad	.LC96
	.quad	.LC97
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"../src/node_http_parser_impl.h:489"
	.align 8
.LC99:
	.string	"(parser->current_buffer_data_) == nullptr"
	.align 8
.LC100:
	.string	"static void node::{anonymous}::Parser::Execute(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_1:
	.quad	.LC98
	.quad	.LC99
	.quad	.LC100
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"../src/node_http_parser_impl.h:488"
	.align 8
.LC102:
	.string	"(parser->current_buffer_len_) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC101
	.quad	.LC102
	.quad	.LC100
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"../src/node_http_parser_impl.h:487"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node12_GLOBAL__N_16Parser7ExecuteERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC103
	.quad	.LC96
	.quad	.LC100
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"../src/node_http_parser_impl.h:254"
	.align 8
.LC105:
	.string	"(num_values_) == (num_fields_)"
	.align 8
.LC106:
	.string	"int node::{anonymous}::Parser::on_header_value(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args_0:
	.quad	.LC104
	.quad	.LC105
	.quad	.LC106
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"../src/node_http_parser_impl.h:253"
	.align 8
.LC108:
	.string	"(num_values_) < (arraysize(values_))"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_valueEPKcmE4args:
	.quad	.LC107
	.quad	.LC108
	.quad	.LC106
	.section	.rodata.str1.8
	.align 8
.LC109:
	.string	"../src/node_http_parser_impl.h:233"
	.align 8
.LC110:
	.string	"(num_fields_) == (num_values_ + 1)"
	.align 8
.LC111:
	.string	"int node::{anonymous}::Parser::on_header_field(const char*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args_0:
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"../src/node_http_parser_impl.h:232"
	.align 8
.LC113:
	.string	"(num_fields_) < (kMaxHeaderFieldsCount)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args, @object
	.size	_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args, 24
_ZZN4node12_GLOBAL__N_16Parser15on_header_fieldEPKcmE4args:
	.quad	.LC112
	.quad	.LC113
	.quad	.LC111
	.weak	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC114:
	.string	"../src/stream_base-inl.h:103"
.LC115:
	.string	"(current) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"void node::StreamResource::RemoveStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource20RemoveStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC114
	.quad	.LC115
	.quad	.LC116
	.weak	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0
	.section	.rodata.str1.1
.LC117:
	.string	"../src/stream_base-inl.h:85"
	.section	.rodata.str1.8
	.align 8
.LC118:
	.string	"(listener->stream_) == nullptr"
	.align 8
.LC119:
	.string	"void node::StreamResource::PushStreamListener(node::StreamListener*)"
	.section	.data.rel.ro.local._ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,"awG",@progbits,_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0,comdat
	.align 16
	.type	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, @gnu_unique_object
	.size	_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0, 24
_ZZN4node14StreamResource18PushStreamListenerEPNS_14StreamListenerEE4args_0:
	.quad	.LC117
	.quad	.LC118
	.quad	.LC119
	.weak	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args
	.section	.rodata.str1.1
.LC120:
	.string	"../src/stream_base-inl.h:66"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"(previous_listener_) != nullptr"
	.align 8
.LC122:
	.string	"virtual void node::StreamListener::OnStreamAfterWrite(node::WriteWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args, 24
_ZZN4node14StreamListener18OnStreamAfterWriteEPNS_9WriteWrapEiE4args:
	.quad	.LC120
	.quad	.LC121
	.quad	.LC122
	.weak	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args
	.section	.rodata.str1.1
.LC123:
	.string	"../src/stream_base-inl.h:61"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"virtual void node::StreamListener::OnStreamAfterShutdown(node::ShutdownWrap*, int)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,"awG",@progbits,_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args, 24
_ZZN4node14StreamListener21OnStreamAfterShutdownEPNS_12ShutdownWrapEiE4args:
	.quad	.LC123
	.quad	.LC121
	.quad	.LC124
	.weak	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args
	.section	.rodata.str1.1
.LC125:
	.string	"../src/stream_base-inl.h:56"
	.section	.rodata.str1.8
	.align 8
.LC126:
	.string	"void node::StreamListener::PassReadErrorToPreviousListener(ssize_t)"
	.section	.data.rel.ro.local._ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args,"awG",@progbits,_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args,comdat
	.align 16
	.type	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args, @gnu_unique_object
	.size	_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args, 24
_ZZN4node14StreamListener31PassReadErrorToPreviousListenerElE4args:
	.quad	.LC125
	.quad	.LC121
	.quad	.LC126
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC127:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC129:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC130:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC132:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.weak	_ZZN4node10BaseObject6DetachEvE4args
	.section	.rodata.str1.1
.LC133:
	.string	"../src/base_object-inl.h:77"
	.section	.rodata.str1.8
	.align 8
.LC134:
	.string	"(pointer_data()->strong_ptr_count) > (0)"
	.align 8
.LC135:
	.string	"void node::BaseObject::Detach()"
	.section	.data.rel.ro.local._ZZN4node10BaseObject6DetachEvE4args,"awG",@progbits,_ZZN4node10BaseObject6DetachEvE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject6DetachEvE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject6DetachEvE4args, 24
_ZZN4node10BaseObject6DetachEvE4args:
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.weak	_ZZN4node11Environment22set_http_parser_bufferEPcE4args
	.section	.rodata.str1.1
.LC136:
	.string	"../src/env-inl.h:578"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"(http_parser_buffer_) == nullptr"
	.align 8
.LC138:
	.string	"void node::Environment::set_http_parser_buffer(char*)"
	.section	.data.rel.ro.local._ZZN4node11Environment22set_http_parser_bufferEPcE4args,"awG",@progbits,_ZZN4node11Environment22set_http_parser_bufferEPcE4args,comdat
	.align 16
	.type	_ZZN4node11Environment22set_http_parser_bufferEPcE4args, @gnu_unique_object
	.size	_ZZN4node11Environment22set_http_parser_bufferEPcE4args, 24
_ZZN4node11Environment22set_http_parser_bufferEPcE4args:
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.quad	1
	.quad	0
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
