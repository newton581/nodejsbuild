	.file	"hooks.cc"
	.text
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4391:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4391:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4392:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4392:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.text
	.p2align 4
	.globl	_ZN4node9RunAtExitEPNS_11EnvironmentE
	.type	_ZN4node9RunAtExitEPNS_11EnvironmentE, @function
_ZN4node9RunAtExitEPNS_11EnvironmentE:
.LFB7463:
	.cfi_startproc
	endbr64
	jmp	_ZN4node11Environment18RunAtExitCallbacksEv@PLT
	.cfi_endproc
.LFE7463:
	.size	_ZN4node9RunAtExitEPNS_11EnvironmentE, .-_ZN4node9RunAtExitEPNS_11EnvironmentE
	.p2align 4
	.globl	_ZN4node6AtExitEPFvPvES0_
	.type	_ZN4node6AtExitEPFvPvES0_, @function
_ZN4node6AtExitEPFvPvES0_:
.LFB7464:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	leaq	_ZN4node11Environment16thread_local_envE(%rip), %rdi
	call	uv_key_get@PLT
	testq	%rax, %rax
	je	.L8
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11Environment6AtExitEPFvPvES1_@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	leaq	_ZZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7464:
	.size	_ZN4node6AtExitEPFvPvES0_, .-_ZN4node6AtExitEPFvPvES0_
	.p2align 4
	.globl	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_
	.type	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_, @function
_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_:
.LFB7465:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L14
	jmp	_ZN4node11Environment6AtExitEPFvPvES1_@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_E4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7465:
	.size	_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_, .-_ZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"node,node.environment"
.LC1:
	.string	"BeforeExit"
.LC2:
	.string	"beforeExit"
	.text
	.p2align 4
	.globl	_ZN4node14EmitBeforeExitEPNS_11EnvironmentE
	.type	_ZN4node14EmitBeforeExitEPNS_11EnvironmentE, @function
_ZN4node14EmitBeforeExitEPNS_11EnvironmentE:
.LFB7466:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip), %r12
	testq	%r12, %r12
	je	.L110
.L17:
	testb	$5, (%r12)
	jne	.L111
.L19:
	movq	1416(%rbx), %rax
	cmpq	%rax, 1424(%rbx)
	je	.L23
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap23DestroyAsyncIdsCallbackEPNS_11EnvironmentE@PLT
.L23:
	movq	352(%rbx), %rsi
	leaq	-96(%rbp), %r13
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r12
	movq	%r12, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	360(%rbx), %rax
	movq	2968(%rbx), %rdi
	movq	3280(%rbx), %rsi
	movq	664(%rax), %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L41
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value9ToIntegerENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L41
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L112
.L41:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip), %r12
	testq	%r12, %r12
	je	.L113
.L35:
	testb	$5, (%r12)
	jne	.L114
.L15:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	.cfi_restore_state
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L20
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L116
.L20:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L21
	movq	(%rdi), %rax
	call	*8(%rax)
.L21:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L19
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L110:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L117
.L18:
	movq	%r12, _ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378(%rip)
	mfence
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L114:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L38
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L118
.L38:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L39
	movq	(%rdi), %rax
	call	*8(%rax)
.L39:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L36
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L119
.L36:
	movq	%r12, _ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381(%rip)
	mfence
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L119:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L117:
	leaq	.LC0(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L116:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC1(%rip), %rcx
	movl	$98, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L118:
	subq	$8, %rsp
	leaq	-64(%rbp), %rdx
	movq	%rbx, %r9
	xorl	%r8d, %r8d
	pushq	$6
	leaq	.LC1(%rip), %rcx
	movl	$101, %esi
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L112:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L41
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7466:
	.size	_ZN4node14EmitBeforeExitEPNS_11EnvironmentE, .-_ZN4node14EmitBeforeExitEPNS_11EnvironmentE
	.section	.rodata.str1.1
.LC3:
	.string	"_exiting"
.LC4:
	.string	"exit"
	.text
	.p2align 4
	.globl	_ZN4node8EmitExitEPNS_11EnvironmentE
	.type	_ZN4node8EmitExitEPNS_11EnvironmentE, @function
_ZN4node8EmitExitEPNS_11EnvironmentE:
.LFB7467:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	leaq	-80(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	352(%rdi), %rsi
	movq	%r13, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC3(%rip), %rsi
	movq	2968(%rbx), %r12
	leaq	112(%rdi), %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L129
.L121:
	movq	3280(%rbx), %rsi
	movq	%r15, %rcx
	movq	%r12, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L130
.L122:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	664(%rax), %r15
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L131
.L123:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L132
.L124:
	movq	352(%rbx), %rdi
	sarq	$32, %rsi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	call	_ZN4node11ProcessEmitEPNS_11EnvironmentEPKcN2v85LocalINS4_5ValueEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	movq	%r15, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L133
.L125:
	movq	3280(%rbx), %rsi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L134
.L126:
	movq	%r14, %rdi
	sarq	$32, %rbx
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L135
	addq	$56, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	.cfi_restore_state
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L130:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-88(%rbp), %rsi
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rdi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L134:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L126
.L135:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7467:
	.size	_ZN4node8EmitExitEPNS_11EnvironmentE, .-_ZN4node8EmitExitEPNS_11EnvironmentE
	.p2align 4
	.globl	_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_
	.type	_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_, @function
_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_:
.LFB7468:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L138
	leaq	-80(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L140
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L140
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L140
	movq	271(%rax), %rbx
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%rbx, %rbx
	je	.L138
	movq	2640(%rbx), %r15
	movl	$40, %edi
	leaq	1(%r15), %rax
	movq	%rax, 2640(%rbx)
	call	_Znwm@PLT
	xorl	%edx, %edx
	movq	%r15, 24(%rax)
	movq	%rax, %r12
	movq	%r14, 8(%rax)
	movq	%r13, 16(%rax)
	movq	2592(%rbx), %rsi
	movq	$0, (%rax)
	movq	%r13, %rax
	divq	%rsi
	movq	2584(%rbx), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r9
	leaq	0(,%rdx,8), %r15
	testq	%rdi, %rdi
	je	.L142
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L143:
	movq	(%rax), %r8
	testq	%r8, %r8
	je	.L142
	movq	32(%r8), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r9
	jne	.L142
	movq	%r8, %rax
.L145:
	cmpq	%rcx, %r13
	jne	.L143
	cmpq	8(%rax), %r14
	jne	.L143
	cmpq	16(%rax), %r13
	jne	.L143
	cmpq	$0, (%rdi)
	je	.L142
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L142:
	movq	2608(%rbx), %rdx
	leaq	2616(%rbx), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L146
	movq	2584(%rbx), %r8
	movq	%r13, 32(%r12)
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.L156
.L182:
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	(%r15), %rax
	movq	%r12, (%rax)
.L157:
	addq	$1, 2608(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L179
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L138:
	leaq	_ZZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L146:
	cmpq	$1, %rdx
	je	.L180
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L181
	leaq	0(,%rdx,8), %r15
	movq	%r15, %rdi
	call	_Znwm@PLT
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	2632(%rbx), %r10
	movq	%rax, %r8
.L149:
	movq	2600(%rbx), %rsi
	movq	$0, 2600(%rbx)
	testq	%rsi, %rsi
	je	.L151
	xorl	%edi, %edi
	leaq	2600(%rbx), %r9
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L153:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L154:
	testq	%rsi, %rsi
	je	.L151
.L152:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%r14
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L153
	movq	2600(%rbx), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 2600(%rbx)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L161
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L152
	.p2align 4,,10
	.p2align 3
.L151:
	movq	2584(%rbx), %rdi
	cmpq	%r10, %rdi
	je	.L155
	movq	%r8, -88(%rbp)
	call	_ZdlPv@PLT
	movq	-88(%rbp), %r8
.L155:
	movq	%r13, %rax
	xorl	%edx, %edx
	movq	%r14, 2592(%rbx)
	divq	%r14
	movq	%r8, 2584(%rbx)
	movq	%r13, 32(%r12)
	leaq	0(,%rdx,8), %r15
	addq	%r8, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	jne	.L182
.L156:
	movq	2600(%rbx), %rax
	movq	%rax, (%r12)
	movq	%r12, 2600(%rbx)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L158
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%rbx)
	movq	%r12, (%r8,%rdx,8)
.L158:
	leaq	2600(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L161:
	movq	%rdx, %rdi
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	2632(%rbx), %r8
	movq	$0, 2632(%rbx)
	movq	%r8, %r10
	jmp	.L149
.L179:
	call	__stack_chk_fail@PLT
.L181:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7468:
	.size	_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_, .-_ZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_
	.p2align 4
	.globl	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_
	.type	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_, @function
_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_:
.LFB7469:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L185
	leaq	-80(%rbp), %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L187
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L187
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L187
	movq	271(%rax), %r12
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r12, %r12
	je	.L185
	movq	2592(%r12), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	movq	2584(%r12), %r14
	divq	%rcx
	leaq	(%r14,%rdx,8), %r15
	movq	%rdx, %r11
	movq	(%r15), %r8
	testq	%r8, %r8
	je	.L183
	movq	(%r8), %rdi
	movq	%r8, %r10
	movq	32(%rdi), %rsi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L190:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L183
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L183
	movq	%r9, %rdi
.L192:
	cmpq	%rsi, %rbx
	jne	.L190
	cmpq	8(%rdi), %r13
	jne	.L190
	cmpq	16(%rdi), %rbx
	jne	.L190
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L211
	testq	%rsi, %rsi
	je	.L194
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L194
	movq	%r10, (%r14,%rdx,8)
	movq	(%rdi), %rsi
.L194:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L183:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L212
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L187:
	.cfi_restore_state
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L185:
	leaq	_ZZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L211:
	testq	%rsi, %rsi
	je	.L199
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L194
	movq	%r10, (%r14,%rdx,8)
	movq	(%r15), %rax
.L193:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L213
.L195:
	movq	$0, (%r15)
	movq	(%rdi), %rsi
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%r10, %rax
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L213:
	movq	%rsi, 2600(%r12)
	jmp	.L195
.L212:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7469:
	.size	_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_, .-_ZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_
	.p2align 4
	.globl	_ZN4node29AsyncHooksGetExecutionAsyncIdEPN2v87IsolateE
	.type	_ZN4node29AsyncHooksGetExecutionAsyncIdEPN2v87IsolateE, @function
_ZN4node29AsyncHooksGetExecutionAsyncIdEPN2v87IsolateE:
.LFB7470:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L220
	leaq	-48(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L218
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L218
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L218
	movq	271(%rax), %r12
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r12, %r12
	je	.L220
	movq	1256(%r12), %rax
	movsd	(%rax), %xmm0
.L214:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L222
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movsd	.LC5(%rip), %xmm0
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L220:
	movsd	.LC5(%rip), %xmm0
	jmp	.L214
.L222:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7470:
	.size	_ZN4node29AsyncHooksGetExecutionAsyncIdEPN2v87IsolateE, .-_ZN4node29AsyncHooksGetExecutionAsyncIdEPN2v87IsolateE
	.p2align 4
	.globl	_ZN4node27AsyncHooksGetTriggerAsyncIdEPN2v87IsolateE
	.type	_ZN4node27AsyncHooksGetTriggerAsyncIdEPN2v87IsolateE, @function
_ZN4node27AsyncHooksGetTriggerAsyncIdEPN2v87IsolateE:
.LFB7471:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L229
	leaq	-48(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L227
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L227
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L227
	movq	271(%rax), %r12
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r12, %r12
	je	.L229
	movq	1256(%r12), %rax
	movsd	8(%rax), %xmm0
.L223:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L231
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movsd	.LC5(%rip), %xmm0
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L229:
	movsd	.LC5(%rip), %xmm0
	jmp	.L223
.L231:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7471:
	.size	_ZN4node27AsyncHooksGetTriggerAsyncIdEPN2v87IsolateE, .-_ZN4node27AsyncHooksGetTriggerAsyncIdEPN2v87IsolateE
	.p2align 4
	.globl	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKcd
	.type	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKcd, @function
_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKcd:
.LFB7472:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-112(%rbp), %r15
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movsd	%xmm0, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, %rsi
	movl	$-1, %ecx
	movq	%r12, %rdi
	movl	$1, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L246
.L233:
	movq	%r12, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L235
	leaq	-80(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L237
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L237
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L237
	movq	271(%rax), %r12
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r12, %r12
	je	.L235
	movsd	-120(%rbp), %xmm2
	ucomisd	.LC5(%rip), %xmm2
	movq	1256(%r12), %rax
	jp	.L239
	jne	.L239
	movsd	24(%rax), %xmm3
	pxor	%xmm0, %xmm0
	comisd	%xmm3, %xmm0
	movsd	%xmm3, -120(%rbp)
	ja	.L247
.L239:
	movsd	-120(%rbp), %xmm1
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	movsd	.LC7(%rip), %xmm0
	addsd	16(%rax), %xmm0
	movsd	%xmm0, 16(%rax)
	movsd	%xmm0, -128(%rbp)
	call	_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movsd	-128(%rbp), %xmm0
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L248
	movsd	-120(%rbp), %xmm1
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L237:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L235:
	leaq	_ZZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L247:
	movsd	(%rax), %xmm4
	movsd	%xmm4, -120(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L246:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L233
.L248:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7472:
	.size	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKcd, .-_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKcd
	.p2align 4
	.globl	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEd
	.type	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEd, @function
_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEd:
.LFB7473:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	subq	$48, %rsp
	movsd	%xmm0, -72(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L251
	leaq	-64(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L253
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L253
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L253
	movq	271(%rax), %r12
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r12, %r12
	je	.L251
	movsd	-72(%rbp), %xmm2
	ucomisd	.LC5(%rip), %xmm2
	movq	1256(%r12), %rax
	jp	.L255
	jne	.L255
	movsd	24(%rax), %xmm3
	pxor	%xmm0, %xmm0
	comisd	%xmm3, %xmm0
	movsd	%xmm3, -72(%rbp)
	ja	.L262
.L255:
	movsd	-72(%rbp), %xmm1
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	.LC7(%rip), %xmm0
	addsd	16(%rax), %xmm0
	movsd	%xmm0, 16(%rax)
	movsd	%xmm0, -80(%rbp)
	call	_ZN4node9AsyncWrap13EmitAsyncInitEPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS4_INS3_6StringEEEdd@PLT
	movsd	-80(%rbp), %xmm0
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L263
	movsd	-72(%rbp), %xmm1
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L251:
	leaq	_ZZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L262:
	movsd	(%rax), %xmm4
	movsd	%xmm4, -72(%rbp)
	jmp	.L255
.L263:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7473:
	.size	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEd, .-_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEd
	.p2align 4
	.globl	_ZN4node16EmitAsyncDestroyEPN2v87IsolateENS_13async_contextE
	.type	_ZN4node16EmitAsyncDestroyEPN2v87IsolateENS_13async_contextE, @function
_ZN4node16EmitAsyncDestroyEPN2v87IsolateENS_13async_contextE:
.LFB7474:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$48, %rsp
	movsd	%xmm0, -56(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L270
	leaq	-48(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L268
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L268
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L268
	movq	271(%rax), %r12
.L267:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L265:
	movsd	-56(%rbp), %xmm0
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L272
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L270:
	xorl	%r12d, %r12d
	jmp	.L265
.L272:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7474:
	.size	_ZN4node16EmitAsyncDestroyEPN2v87IsolateENS_13async_contextE, .-_ZN4node16EmitAsyncDestroyEPN2v87IsolateENS_13async_contextE
	.p2align 4
	.globl	_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE
	.type	_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE, @function
_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE:
.LFB7475:
	.cfi_startproc
	endbr64
	jmp	_ZN4node9AsyncWrap11EmitDestroyEPNS_11EnvironmentEd@PLT
	.cfi_endproc
.LFE7475:
	.size	_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE, .-_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE
	.section	.rodata.str1.1
.LC8:
	.string	"../src/api/hooks.cc:122"
.LC9:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC10:
	.string	"node::async_context node::EmitAsyncInit(v8::Isolate*, v8::Local<v8::Object>, v8::Local<v8::String>, node::async_id)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEdE4args, @object
	.size	_ZZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEdE4args, 24
_ZZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEdE4args:
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.section	.rodata.str1.1
.LC11:
	.string	"../src/api/hooks.cc:88"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"void node::RemoveEnvironmentCleanupHook(v8::Isolate*, void (*)(void*), void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args, @object
	.size	_ZZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args, 24
_ZZN4node28RemoveEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args:
	.quad	.LC11
	.quad	.LC9
	.quad	.LC12
	.section	.rodata.str1.1
.LC13:
	.string	"../src/api/hooks.cc:80"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"void node::AddEnvironmentCleanupHook(v8::Isolate*, void (*)(void*), void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args, @object
	.size	_ZZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args, 24
_ZZN4node25AddEnvironmentCleanupHookEPN2v87IsolateEPFvPvES3_E4args:
	.quad	.LC13
	.quad	.LC9
	.quad	.LC14
	.section	.rodata.str1.1
.LC15:
	.string	"../src/api/hooks.cc:28"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"void node::AtExit(node::Environment*, void (*)(void*), void*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_E4args, @object
	.size	_ZZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_E4args, 24
_ZZN4node6AtExitEPNS_11EnvironmentEPFvPvES2_E4args:
	.quad	.LC15
	.quad	.LC9
	.quad	.LC16
	.weak	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381
	.section	.bss._ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381,"awG",@nobits,_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381,comdat
	.align 8
	.type	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381, @gnu_unique_object
	.size	_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381, 8
_ZZN4node15TraceEventScopeD4EvE28trace_event_unique_atomic381:
	.zero	8
	.weak	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378
	.section	.bss._ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378,"awG",@nobits,_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378,comdat
	.align 8
	.type	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378, @gnu_unique_object
	.size	_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378, 8
_ZZN4node15TraceEventScopeC4EPKcS2_PvE28trace_event_unique_atomic378:
	.zero	8
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC17:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC19:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC5:
	.long	0
	.long	-1074790400
	.align 8
.LC7:
	.long	0
	.long	1072693248
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
