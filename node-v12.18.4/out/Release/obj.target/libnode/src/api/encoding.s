	.file	"encoding.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"utf8"
.LC1:
	.string	"utf-8"
.LC2:
	.string	"ascii"
.LC3:
	.string	"base64"
.LC4:
	.string	"ucs2"
.LC5:
	.string	"ucs-2"
.LC6:
	.string	"utf16le"
.LC7:
	.string	"utf-16le"
.LC8:
	.string	"latin1"
.LC9:
	.string	"binary"
.LC10:
	.string	"buffer"
.LC11:
	.string	"hex"
.LC12:
	.string	"16le"
.LC13:
	.string	"tin1"
.LC14:
	.string	"nary"
.LC15:
	.string	"ffer"
	.text
	.p2align 4
	.globl	_ZN4node13ParseEncodingEPKcNS_8encodingE
	.type	_ZN4node13ParseEncodingEPKcNS_8encodingE, @function
_ZN4node13ParseEncodingEPKcNS_8encodingE:
.LFB7082:
	.cfi_startproc
	endbr64
	movzbl	(%rdi), %eax
	movq	%rdi, %r8
	movl	%esi, %r9d
	cmpb	$108, %al
	je	.L2
	jg	.L3
	movl	%esi, %r10d
	testb	%al, %al
	je	.L1
	cmpb	$98, %al
	jne	.L6
	movzbl	1(%rdi), %eax
	cmpb	$105, %al
	je	.L125
	cmpb	$117, %al
	je	.L126
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r8, %rsi
	movl	$117, %edx
	movl	$117, %edi
	leaq	.LC0(%rip), %rcx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L128:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L93
.L16:
	movzbl	(%rcx), %edi
	movl	%edi, %edx
.L18:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L127
	testb	%dl, %dl
	je	.L93
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rsi
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rcx
	cmpb	$25, %dil
	jbe	.L128
	cmpb	%dl, %al
	je	.L16
.L93:
	movq	%r8, %rsi
	movl	$117, %edx
	movl	$117, %edi
	leaq	.LC1(%rip), %rcx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L130:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L95
.L23:
	movzbl	(%rcx), %edi
	movl	%edi, %edx
.L17:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L129
	testb	%dl, %dl
	je	.L95
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rsi
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rcx
	cmpb	$25, %dil
	jbe	.L130
	cmpb	%dl, %al
	je	.L23
.L95:
	movq	%r8, %rsi
	movl	$97, %edx
	movl	$97, %edi
	leaq	.LC2(%rip), %rcx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L132:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L97
.L29:
	movzbl	(%rcx), %edi
	movl	%edi, %edx
.L24:
	movzbl	(%rsi), %eax
	testb	%al, %al
	je	.L131
	testb	%dl, %dl
	je	.L97
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rsi
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rcx
	cmpb	$25, %dil
	jbe	.L132
	cmpb	%dl, %al
	je	.L29
.L97:
	movq	%r8, %rcx
	movl	$98, %edx
	movl	$98, %edi
	leaq	.LC3(%rip), %rsi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L134:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L99
.L35:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L30:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L133
	testb	%dl, %dl
	je	.L99
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L134
	cmpb	%dl, %al
	je	.L35
.L99:
	movq	%r8, %rcx
	movl	$117, %edx
	movl	$117, %edi
	leaq	.LC4(%rip), %rsi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L136:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L101
.L41:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L36:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L135
	testb	%dl, %dl
	je	.L101
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L136
	cmpb	%dl, %al
	je	.L41
.L101:
	movq	%r8, %rcx
	movl	$117, %edx
	movl	$117, %edi
	leaq	.LC5(%rip), %rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L138:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L103
.L47:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L42:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L137
	testb	%dl, %dl
	je	.L103
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L138
	cmpb	%al, %dl
	je	.L47
.L103:
	movq	%r8, %rcx
	movl	$117, %edx
	movl	$117, %edi
	leaq	.LC6(%rip), %rsi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L140:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L105
.L53:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L48:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L139
	testb	%dl, %dl
	je	.L105
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L140
	cmpb	%dl, %al
	je	.L53
.L105:
	movq	%r8, %rcx
	movl	$117, %edx
	movl	$117, %edi
	leaq	.LC7(%rip), %rsi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L142:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L107
.L59:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L54:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L141
	testb	%dl, %dl
	je	.L107
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L142
	cmpb	%dl, %al
	je	.L59
.L107:
	movq	%r8, %rcx
	movl	$108, %edx
	movl	$108, %edi
	leaq	.LC8(%rip), %rsi
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L144:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L109
.L65:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L60:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L143
	testb	%dl, %dl
	je	.L109
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L144
	cmpb	%dl, %al
	je	.L65
.L109:
	movq	%r8, %rcx
	movl	$98, %edx
	movl	$98, %edi
	leaq	.LC9(%rip), %rsi
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L146:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L111
.L71:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L66:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L145
	testb	%dl, %dl
	je	.L111
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L146
	cmpb	%dl, %al
	je	.L71
.L111:
	movq	%r8, %rcx
	movl	$98, %edx
	movl	$98, %edi
	leaq	.LC10(%rip), %rsi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L148:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L113
.L77:
	movzbl	(%rsi), %edi
	movl	%edi, %edx
.L72:
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L147
	testb	%dl, %dl
	je	.L113
	leal	-65(%rax), %r10d
	leal	32(%rax), %edi
	addq	$1, %rcx
	cmpb	$26, %r10b
	cmovb	%edi, %eax
	leal	-65(%rdx), %edi
	addq	$1, %rsi
	cmpb	$25, %dil
	jbe	.L148
	cmpb	%dl, %al
	je	.L77
.L113:
	movl	$104, %edx
	movl	$104, %esi
	leaq	.LC11(%rip), %rcx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L150:
	addl	$32, %edx
	cmpb	%al, %dl
	jne	.L116
.L82:
	movzbl	(%rcx), %esi
	movl	%esi, %edx
.L78:
	movzbl	(%r8), %eax
	testb	%al, %al
	je	.L149
	testb	%dl, %dl
	je	.L116
	leal	-65(%rax), %edi
	leal	32(%rax), %esi
	addq	$1, %r8
	cmpb	$26, %dil
	cmovb	%esi, %eax
	leal	-65(%rdx), %esi
	addq	$1, %rcx
	cmpb	$25, %sil
	jbe	.L150
	cmpb	%dl, %al
	je	.L82
.L116:
	movl	%r9d, %r10d
.L1:
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpb	$117, %al
	jne	.L6
	movzbl	1(%rdi), %eax
	cmpb	$116, %al
	je	.L151
	cmpb	$99, %al
	jne	.L6
	cmpb	$115, 2(%rdi)
	jne	.L6
	xorl	%eax, %eax
	cmpb	$45, 3(%rdi)
	sete	%al
	leaq	3(%rdi,%rax), %r8
	cmpb	$50, (%r8)
	jne	.L6
	cmpb	$0, 1(%r8)
	movl	$3, %r10d
	jne	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L127:
	testb	%dil, %dil
	jne	.L93
.L94:
	movl	$1, %r10d
	movl	%r10d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpb	$97, 1(%r8)
	leaq	2(%rdi), %rsi
	movl	$4, %ecx
	leaq	.LC13(%rip), %rdi
	jne	.L6
.L123:
	repz cmpsb
	movl	$4, %r10d
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	2(%rdi), %rsi
	movl	$4, %ecx
	leaq	.LC14(%rip), %rdi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L151:
	cmpb	$102, 2(%rdi)
	jne	.L6
	xorl	%eax, %eax
	cmpb	$45, 3(%rdi)
	sete	%al
	leaq	3(%rdi,%rax), %r8
	cmpb	$56, (%r8)
	je	.L152
.L9:
	movl	$4, %ecx
	leaq	.LC12(%rip), %rdi
	movq	%r8, %rsi
	movl	$3, %r10d
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L129:
	testb	%dil, %dil
	je	.L94
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L131:
	testb	%dil, %dil
	jne	.L97
	xorl	%r10d, %r10d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	2(%rdi), %rsi
	movl	$4, %ecx
	movl	$6, %r10d
	leaq	.LC15(%rip), %rdi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L133:
	testb	%dil, %dil
	jne	.L99
	movl	$2, %r10d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L135:
	testb	%dil, %dil
	jne	.L101
.L106:
	movl	$3, %r10d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L137:
	testb	%dil, %dil
	je	.L106
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L139:
	testb	%dil, %dil
	je	.L106
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L152:
	cmpb	$0, 1(%r8)
	movl	$1, %r10d
	jne	.L9
	jmp	.L1
.L141:
	testb	%dil, %dil
	je	.L106
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L143:
	testb	%dil, %dil
	jne	.L109
.L110:
	movl	$4, %r10d
	jmp	.L1
.L145:
	testb	%dil, %dil
	je	.L110
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L147:
	testb	%dil, %dil
	jne	.L113
	movl	$6, %r10d
	jmp	.L1
.L149:
	testb	%sil, %sil
	movl	$5, %r10d
	cmovne	%r9d, %r10d
	jmp	.L1
	.cfi_endproc
.LFE7082:
	.size	_ZN4node13ParseEncodingEPKcNS_8encodingE, .-_ZN4node13ParseEncodingEPKcNS_8encodingE
	.p2align 4
	.globl	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE
	.type	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE, @function
_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE:
.LFB7083:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$1064, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L164
	movq	(%rsi), %rax
	movl	%edx, %r12d
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	je	.L165
.L155:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L166
	addq	$1064, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	.cfi_restore_state
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L155
	leaq	-1072(%rbp), %r8
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r8, %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %r11
	movl	%r12d, %esi
	movq	%r11, %rdi
	call	_ZN4node13ParseEncodingEPKcNS_8encodingE
	movl	%eax, %r12d
	testq	%r11, %r11
	je	.L155
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %r11
	je	.L155
	movq	%r11, %rdi
	call	free@PLT
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L164:
	leaq	_ZZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L166:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7083:
	.size	_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE, .-_ZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE
	.p2align 4
	.globl	_ZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingE
	.type	_ZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingE, @function
_ZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingE:
.LFB7087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %ecx
	je	.L172
	leaq	-16(%rbp), %r8
	movq	$0, -16(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKcmNS_8encodingEPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L173
.L169:
	movq	-8(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L174
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	.cfi_restore_state
	leaq	_ZZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rax
	jmp	.L169
.L174:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7087:
	.size	_ZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingE, .-_ZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingE
	.p2align 4
	.globl	_ZN4node6EncodeEPN2v87IsolateEPKtm
	.type	_ZN4node6EncodeEPN2v87IsolateEPKtm, @function
_ZN4node6EncodeEPN2v87IsolateEPKtm:
.LFB7088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-16(%rbp), %rcx
	movq	$0, -16(%rbp)
	call	_ZN4node11StringBytes6EncodeEPN2v87IsolateEPKtmPNS1_5LocalINS1_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L179
.L176:
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L180
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L179:
	.cfi_restore_state
	movq	%rax, -24(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-24(%rbp), %rax
	jmp	.L176
.L180:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7088:
	.size	_ZN4node6EncodeEPN2v87IsolateEPKtm, .-_ZN4node6EncodeEPN2v87IsolateEPKtm
	.p2align 4
	.globl	_ZN4node11DecodeBytesEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE
	.type	_ZN4node11DecodeBytesEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE, @function
_ZN4node11DecodeBytesEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE:
.LFB7089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-64(%rbp), %r15
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	%r14d, %edx
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node11StringBytes4SizeEPN2v87IsolateENS1_5LocalINS1_5ValueEEENS_8encodingE@PLT
	movq	$-1, %r12
	movq	%r15, %rdi
	testb	%al, %al
	cmovne	%rdx, %r12
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	addq	$32, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L186:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7089:
	.size	_ZN4node11DecodeBytesEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE, .-_ZN4node11DecodeBytesEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingE
	.p2align 4
	.globl	_ZN4node11DecodeWriteEPN2v87IsolateEPcmNS0_5LocalINS0_5ValueEEENS_8encodingE
	.type	_ZN4node11DecodeWriteEPN2v87IsolateEPcmNS0_5LocalINS0_5ValueEEENS_8encodingE, @function
_ZN4node11DecodeWriteEPN2v87IsolateEPcmNS0_5LocalINS0_5ValueEEENS_8encodingE:
.LFB7090:
	.cfi_startproc
	endbr64
	xorl	%r9d, %r9d
	jmp	_ZN4node11StringBytes5WriteEPN2v87IsolateEPcmNS1_5LocalINS1_5ValueEEENS_8encodingEPi@PLT
	.cfi_endproc
.LFE7090:
	.size	_ZN4node11DecodeWriteEPN2v87IsolateEPcmNS0_5LocalINS0_5ValueEEENS_8encodingE, .-_ZN4node11DecodeWriteEPN2v87IsolateEPcmNS0_5LocalINS0_5ValueEEENS_8encodingE
	.section	.rodata.str1.1
.LC16:
	.string	"../src/api/encoding.cc:105"
.LC17:
	.string	"(encoding) != (UCS2)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"v8::Local<v8::Value> node::Encode(v8::Isolate*, const char*, size_t, node::encoding)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingEE4args, @object
	.size	_ZZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingEE4args, 24
_ZZN4node6EncodeEPN2v87IsolateEPKcmNS_8encodingEE4args:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"../src/api/encoding.cc:91"
.LC20:
	.string	"!encoding_v.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"node::encoding node::ParseEncoding(v8::Isolate*, v8::Local<v8::Value>, node::encoding)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingEE4args, @object
	.size	_ZZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingEE4args, 24
_ZZN4node13ParseEncodingEPN2v87IsolateENS0_5LocalINS0_5ValueEEENS_8encodingEE4args:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
