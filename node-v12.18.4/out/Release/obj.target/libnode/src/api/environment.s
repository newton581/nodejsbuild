	.file	"environment.cc"
	.text
	.section	.text._ZN4node24NodeArrayBufferAllocator7GetImplEv,"axG",@progbits,_ZN4node24NodeArrayBufferAllocator7GetImplEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocator7GetImplEv
	.type	_ZN4node24NodeArrayBufferAllocator7GetImplEv, @function
_ZN4node24NodeArrayBufferAllocator7GetImplEv:
.LFB6534:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE6534:
	.size	_ZN4node24NodeArrayBufferAllocator7GetImplEv, .-_ZN4node24NodeArrayBufferAllocator7GetImplEv
	.section	.text._ZN4node24NodeArrayBufferAllocatorD2Ev,"axG",@progbits,_ZN4node24NodeArrayBufferAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocatorD2Ev
	.type	_ZN4node24NodeArrayBufferAllocatorD2Ev, @function
_ZN4node24NodeArrayBufferAllocatorD2Ev:
.LFB7708:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7708:
	.size	_ZN4node24NodeArrayBufferAllocatorD2Ev, .-_ZN4node24NodeArrayBufferAllocatorD2Ev
	.weak	_ZN4node24NodeArrayBufferAllocatorD1Ev
	.set	_ZN4node24NodeArrayBufferAllocatorD1Ev,_ZN4node24NodeArrayBufferAllocatorD2Ev
	.section	.text._ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm,"axG",@progbits,_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm
	.type	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm, @function
_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm:
.LFB6532:
	.cfi_startproc
	endbr64
	lock addq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE6532:
	.size	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm, .-_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node24NodeArrayBufferAllocator8AllocateEm
	.type	_ZN4node24NodeArrayBufferAllocator8AllocateEm, @function
_ZN4node24NodeArrayBufferAllocator8AllocateEm:
.LFB7694:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	8(%rdi), %eax
	testq	%rsi, %rsi
	cmovne	%rsi, %r13
	testl	%eax, %eax
	jne	.L6
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpb	$0, 136(%rax)
	je	.L7
.L6:
	movl	$1, %esi
	movq	%r13, %rdi
	call	calloc@PLT
.L8:
	testq	%rax, %rax
	je	.L5
.L9:
	lock addq	%rbx, 16(%r12)
.L5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L9
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	jmp	.L8
	.cfi_endproc
.LFE7694:
	.size	_ZN4node24NodeArrayBufferAllocator8AllocateEm, .-_ZN4node24NodeArrayBufferAllocator8AllocateEm
	.align 2
	.p2align 4
	.globl	_ZN4node24NodeArrayBufferAllocator4FreeEPvm
	.type	_ZN4node24NodeArrayBufferAllocator4FreeEPvm, @function
_ZN4node24NodeArrayBufferAllocator4FreeEPvm:
.LFB7697:
	.cfi_startproc
	endbr64
	lock subq	%rdx, 16(%rdi)
	movq	%rsi, %rdi
	jmp	free@PLT
	.cfi_endproc
.LFE7697:
	.size	_ZN4node24NodeArrayBufferAllocator4FreeEPvm, .-_ZN4node24NodeArrayBufferAllocator4FreeEPvm
	.section	.text._ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm,"axG",@progbits,_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm
	.type	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm, @function
_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm:
.LFB6533:
	.cfi_startproc
	endbr64
	lock subq	%rdx, 16(%rdi)
	ret
	.cfi_endproc
.LFE6533:
	.size	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm, .-_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocatorD2Ev
	.type	_ZN4node29DebuggingArrayBufferAllocatorD2Ev, @function
_ZN4node29DebuggingArrayBufferAllocatorD2Ev:
.LFB7714:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node29DebuggingArrayBufferAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 88(%rdi)
	movq	%rax, (%rdi)
	jne	.L27
	movq	80(%rdi), %r12
	movq	%rdi, %rbx
	testq	%r12, %r12
	je	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r12, %rdi
	movq	(%r12), %r12
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L20
.L19:
	movq	72(%rbx), %rax
	movq	64(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	64(%rbx), %rdi
	leaq	112(%rbx), %rax
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	cmpq	%rax, %rdi
	je	.L21
	call	_ZdlPv@PLT
.L21:
	leaq	24(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	leaq	_ZZN4node29DebuggingArrayBufferAllocatorD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7714:
	.size	_ZN4node29DebuggingArrayBufferAllocatorD2Ev, .-_ZN4node29DebuggingArrayBufferAllocatorD2Ev
	.globl	_ZN4node29DebuggingArrayBufferAllocatorD1Ev
	.set	_ZN4node29DebuggingArrayBufferAllocatorD1Ev,_ZN4node29DebuggingArrayBufferAllocatorD2Ev
	.section	.text._ZN4node24NodeArrayBufferAllocatorD0Ev,"axG",@progbits,_ZN4node24NodeArrayBufferAllocatorD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node24NodeArrayBufferAllocatorD0Ev
	.type	_ZN4node24NodeArrayBufferAllocatorD0Ev, @function
_ZN4node24NodeArrayBufferAllocatorD0Ev:
.LFB7710:
	.cfi_startproc
	endbr64
	movl	$24, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7710:
	.size	_ZN4node24NodeArrayBufferAllocatorD0Ev, .-_ZN4node24NodeArrayBufferAllocatorD0Ev
	.text
	.p2align 4
	.type	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE, @function
_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE:
.LFB7691:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L42
	movq	%rdx, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L42
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L42
	movq	271(%rax), %rbx
	testq	%rbx, %rbx
	je	.L42
	movq	2960(%rbx), %r15
	testq	%r15, %r15
	je	.L42
	movq	%r12, %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%r13, -72(%rbp)
	movq	352(%rbx), %rsi
	leaq	-144(%rbp), %r13
	movq	%r13, %rdi
	movq	%rax, -80(%rbp)
	movq	%r14, -64(%rbp)
	call	_ZN2v88TryCatchC2EPNS_7IsolateE@PLT
	movq	%r12, %rsi
	movq	%r15, %rdi
	leaq	-80(%rbp), %r8
	movq	352(%rbx), %rdx
	movl	$3, %ecx
	movq	%rbx, -96(%rbp)
	movl	$0, -88(%rbp)
	addq	$88, %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	testb	%al, %al
	jne	.L43
.L35:
	movq	%r13, %rdi
	call	_ZN4node6errors13TryCatchScopeD1Ev@PLT
.L31:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L44
	addq	$104, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZNK2v85Value8ToStringENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r13, %rdi
	call	_ZNK2v88TryCatch13HasTerminatedEv@PLT
	testb	%al, %al
	jne	.L35
	movq	%r13, %rdi
	call	_ZN2v88TryCatch7ReThrowEv@PLT
	jmp	.L35
.L44:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7691:
	.size	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE, .-_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE
	.p2align 4
	.type	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE, @function
_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE:
.LFB7689:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	(%rdi), %rdi
	movq	55(%rdi), %rax
	movq	287(%rax), %r12
	call	_ZN2v88internal35IsolateFromNeverReadOnlySpaceObjectEm@PLT
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdi
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L46
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L49
.L46:
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZNK2v85Value6IsTrueEv@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L46
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7689:
	.size	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE, .-_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE
	.align 2
	.p2align 4
	.globl	_ZN4node24NodeArrayBufferAllocator21AllocateUninitializedEm
	.type	_ZN4node24NodeArrayBufferAllocator21AllocateUninitializedEm, @function
_ZN4node24NodeArrayBufferAllocator21AllocateUninitializedEm:
.LFB7695:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	$1, %r13d
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	cmovne	%rsi, %r13
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L58
.L51:
	lock addq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L51
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7695:
	.size	_ZN4node24NodeArrayBufferAllocator21AllocateUninitializedEm, .-_ZN4node24NodeArrayBufferAllocator21AllocateUninitializedEm
	.align 2
	.p2align 4
	.globl	_ZN4node24NodeArrayBufferAllocator10ReallocateEPvmm
	.type	_ZN4node24NodeArrayBufferAllocator10ReallocateEPvmm, @function
_ZN4node24NodeArrayBufferAllocator10ReallocateEPvmm:
.LFB7696:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rcx, %rbx
	testq	%rcx, %rcx
	je	.L67
	movq	%rcx, %rsi
	movq	%r14, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L68
.L61:
	subq	%r13, %rbx
	lock addq	%rbx, 16(%r12)
.L59:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	.cfi_restore_state
	movq	%rsi, %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L68:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L61
	jmp	.L59
	.cfi_endproc
.LFE7696:
	.size	_ZN4node24NodeArrayBufferAllocator10ReallocateEPvmm, .-_ZN4node24NodeArrayBufferAllocator10ReallocateEPvmm
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocatorD0Ev
	.type	_ZN4node29DebuggingArrayBufferAllocatorD0Ev, @function
_ZN4node29DebuggingArrayBufferAllocatorD0Ev:
.LFB7716:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node29DebuggingArrayBufferAllocatorE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	cmpq	$0, 88(%rdi)
	movq	%rax, (%rdi)
	jne	.L79
	movq	80(%rdi), %rbx
	movq	%rdi, %r12
	testq	%rbx, %rbx
	je	.L71
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rbx, %rdi
	movq	(%rbx), %rbx
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L72
.L71:
	movq	72(%r12), %rax
	movq	64(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	64(%r12), %rdi
	leaq	112(%r12), %rax
	movq	$0, 88(%r12)
	movq	$0, 80(%r12)
	cmpq	%rax, %rdi
	je	.L73
	call	_ZdlPv@PLT
.L73:
	leaq	24(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$120, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	leaq	_ZZN4node29DebuggingArrayBufferAllocatorD4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7716:
	.size	_ZN4node29DebuggingArrayBufferAllocatorD0Ev, .-_ZN4node29DebuggingArrayBufferAllocatorD0Ev
	.section	.text._ZN4node12NodePlatformD0Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD0Ev
	.type	_ZN4node12NodePlatformD0Ev, @function
_ZN4node12NodePlatformD0Ev:
.LFB10797:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	120(%rdi), %r13
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L82
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L83
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
	cmpl	$1, %eax
	je	.L109
	.p2align 4,,10
	.p2align 3
.L82:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L88
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L94
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L110
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L88
.L89:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L96
.L110:
	lock subl	$1, 8(%r13)
	jne	.L96
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L96
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L89
	.p2align 4,,10
	.p2align 3
.L88:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L98
	call	_ZdlPv@PLT
.L98:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L93:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L92
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L88
.L94:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L92
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L92
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L83:
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L82
.L109:
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L86
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L87:
	cmpl	$1, %eax
	jne	.L82
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L86:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L87
	.cfi_endproc
.LFE10797:
	.size	_ZN4node12NodePlatformD0Ev, .-_ZN4node12NodePlatformD0Ev
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ERR_PROTO_ACCESS"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Accessing Object.prototype.__proto__ has been disallowed with --disable-proto=throw"
	.section	.rodata.str1.1
.LC2:
	.string	"code"
	.text
	.p2align 4
	.globl	_ZN4node12ProtoThrowerERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.type	_ZN4node12ProtoThrowerERKN2v820FunctionCallbackInfoINS0_5ValueEEE, @function
_ZN4node12ProtoThrowerERKN2v820FunctionCallbackInfoINS0_5ValueEEE:
.LFB7756:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	subq	$16, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	(%rdi), %rax
	movq	8(%rax), %r12
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L118
.L112:
	movq	%r12, %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC1(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L119
.L113:
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L120
.L114:
	movl	$-1, %ecx
	xorl	%edx, %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L121
.L115:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L122
.L116:
	addq	$16, %rsp
	movq	%r13, %rsi
	movq	%r12, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate14ThrowExceptionENS_5LocalINS_5ValueEEE@PLT
	.p2align 4,,10
	.p2align 3
.L118:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L119:
	movq	%rax, -40(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-40(%rbp), %rdi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L120:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L121:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L122:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L116
	.cfi_endproc
.LFE7756:
	.size	_ZN4node12ProtoThrowerERKN2v820FunctionCallbackInfoINS0_5ValueEEE, .-_ZN4node12ProtoThrowerERKN2v820FunctionCallbackInfoINS0_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"cannot create std::deque larger than max_size()"
	.text
	.p2align 4
	.type	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE, @function
_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE:
.LFB7693:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L146
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$40, %rsp
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L123
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L123
	movq	271(%rax), %rbx
	testq	%rbx, %rbx
	je	.L123
	movq	2064(%rbx), %rax
	movq	2048(%rbx), %r14
	movq	%r13, %r12
	movq	352(%rbx), %r8
	subq	$8, %rax
	cmpq	%rax, %r14
	je	.L127
	testq	%r13, %r13
	je	.L138
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %r12
	movq	2048(%rbx), %rax
.L128:
	addq	$8, %rax
	movq	%r12, (%r14)
	movq	%rax, 2048(%rbx)
.L129:
	addq	$40, %rsp
	leaq	1000(%rbx), %rdi
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%r15
	.cfi_restore 15
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	uv_async_send@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	.cfi_restore_state
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	.cfi_restore_state
	movq	%r14, %rax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L127:
	movq	2072(%rbx), %r15
	movq	2040(%rbx), %rsi
	movabsq	$1152921504606846975, %r9
	subq	2056(%rbx), %r14
	movq	%r15, %rcx
	sarq	$3, %r14
	subq	%rsi, %rcx
	movq	%rcx, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rax
	salq	$6, %rax
	addq	%rax, %r14
	movq	2032(%rbx), %rax
	subq	2016(%rbx), %rax
	sarq	$3, %rax
	addq	%r14, %rax
	cmpq	%r9, %rax
	je	.L149
	movq	2000(%rbx), %r10
	movq	2008(%rbx), %rdi
	movq	%r15, %rax
	subq	%r10, %rax
	movq	%rdi, %r11
	sarq	$3, %rax
	subq	%rax, %r11
	cmpq	$1, %r11
	jbe	.L150
.L131:
	movl	$512, %edi
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	%rax, 8(%r15)
	movq	2048(%rbx), %r14
	testq	%r13, %r13
	je	.L137
	movq	-56(%rbp), %r8
	movq	%r13, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, %r12
.L137:
	movq	2072(%rbx), %rdx
	movq	%r12, (%r14)
	movq	8(%rdx), %rax
	addq	$8, %rdx
	movq	%rdx, %xmm1
	movq	%rax, %xmm0
	addq	$512, %rax
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, 2048(%rbx)
	movq	%rax, %xmm0
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, 2064(%rbx)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L150:
	addq	$2, %rdx
	leaq	(%rdx,%rdx), %rax
	cmpq	%rax, %rdi
	jbe	.L132
	subq	%rdx, %rdi
	addq	$8, %r15
	shrq	%rdi
	movq	%r15, %rdx
	leaq	(%r10,%rdi,8), %r9
	subq	%rsi, %rdx
	cmpq	%r9, %rsi
	jbe	.L133
	cmpq	%rsi, %r15
	je	.L134
	movq	%r9, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	movq	%rax, %r9
	.p2align 4,,10
	.p2align 3
.L134:
	movq	(%r9), %rax
	movq	(%r9), %xmm0
	leaq	(%r9,%rcx), %r15
	movq	%r9, 2040(%rbx)
	movq	%r15, 2072(%rbx)
	addq	$512, %rax
	movq	%rax, %xmm2
	punpcklqdq	%xmm2, %xmm0
	movups	%xmm0, 2024(%rbx)
	movq	(%r15), %rax
	movq	%rax, 2056(%rbx)
	addq	$512, %rax
	movq	%rax, 2064(%rbx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L146:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	testq	%rdi, %rdi
	movl	$1, %eax
	cmovne	%rdi, %rax
	leaq	2(%rdi,%rax), %r14
	cmpq	%r9, %r14
	ja	.L151
	leaq	0(,%r14,8), %rdi
	movq	%rdx, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_Znwm@PLT
	movq	-72(%rbp), %rdx
	movq	2040(%rbx), %rsi
	movq	%rax, %r15
	movq	%r14, %rax
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	subq	%rdx, %rax
	shrq	%rax
	leaq	(%r15,%rax,8), %r9
	movq	2072(%rbx), %rax
	leaq	8(%rax), %rdx
	cmpq	%rsi, %rdx
	je	.L136
	movq	%r9, %rdi
	subq	%rsi, %rdx
	call	memmove@PLT
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	movq	%rax, %r9
.L136:
	movq	2000(%rbx), %rdi
	movq	%r9, -72(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	%r15, 2000(%rbx)
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	-56(%rbp), %r8
	movq	%r14, 2008(%rbx)
	jmp	.L134
.L133:
	cmpq	%rsi, %r15
	je	.L134
	leaq	8(%rcx), %rdi
	movq	%r8, -72(%rbp)
	subq	%rdx, %rdi
	movq	%rcx, -64(%rbp)
	addq	%r9, %rdi
	movq	%r9, -56(%rbp)
	call	memmove@PLT
	movq	-56(%rbp), %r9
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %r8
	jmp	.L134
.L151:
	call	_ZSt17__throw_bad_allocv@PLT
.L149:
	leaq	.LC3(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
	.cfi_endproc
.LFE7693:
	.size	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE, .-_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE
	.p2align 4
	.type	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE, @function
_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE:
.LFB7690:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	subq	$32, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L152
	leaq	-48(%rbp), %r13
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L156
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L156
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L156
	movq	271(%rax), %r12
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%r12, %r12
	je	.L160
	testb	$1, 1932(%r12)
	je	.L158
.L161:
	movq	1792(%r12), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L160
	movl	1808(%r12), %eax
	testl	%eax, %eax
	setle	%al
.L152:
	movq	-24(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L167
	addq	$32, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	.cfi_restore_state
	movzbl	2664(%r12), %eax
	testb	%al, %al
	je	.L161
.L160:
	xorl	%eax, %eax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	xorl	%eax, %eax
	jmp	.L152
.L167:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7690:
	.size	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE, .-_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE
	.align 2
	.p2align 4
	.globl	_ZN4node20ArrayBufferAllocator6CreateEb
	.type	_ZN4node20ArrayBufferAllocator6CreateEb, @function
_ZN4node20ArrayBufferAllocator6CreateEb:
.LFB7725:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	testb	%sil, %sil
	jne	.L169
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpb	$0, 137(%rax)
	je	.L170
.L169:
	movl	$120, %edi
	call	_Znwm@PLT
	movl	$15, %ecx
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	rep stosq
	leaq	16+_ZTVN4node29DebuggingArrayBufferAllocatorE(%rip), %rax
	leaq	24(%rbx), %rdi
	movl	$1, 8(%rbx)
	movq	%rax, (%rbx)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L174
	leaq	112(%rbx), %rax
	movq	%rbx, (%r12)
	movq	%rax, 64(%rbx)
	movq	%r12, %rax
	movq	$1, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movl	$0x3f800000, 96(%rbx)
	movq	$0, 104(%rbx)
	movq	$0, 112(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	16+_ZTVN4node24NodeArrayBufferAllocatorE(%rip), %rdx
	movq	%rdx, (%rax)
	movl	$1, 8(%rax)
	movq	$0, 16(%rax)
	popq	%rbx
	movq	%rax, (%r12)
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7725:
	.size	_ZN4node20ArrayBufferAllocator6CreateEb, .-_ZN4node20ArrayBufferAllocator6CreateEb
	.p2align 4
	.globl	_ZN4node26CreateArrayBufferAllocatorEv
	.type	_ZN4node26CreateArrayBufferAllocatorEv, @function
_ZN4node26CreateArrayBufferAllocatorEv:
.LFB7730:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpb	$0, 137(%rax)
	je	.L176
	movl	$120, %edi
	call	_Znwm@PLT
	movl	$15, %ecx
	movq	%rax, %r12
	xorl	%eax, %eax
	movq	%r12, %rdi
	rep stosq
	leaq	16+_ZTVN4node29DebuggingArrayBufferAllocatorE(%rip), %rax
	leaq	24(%r12), %rdi
	movl	$1, 8(%r12)
	movq	%rax, (%r12)
	call	uv_mutex_init@PLT
	testl	%eax, %eax
	jne	.L180
	movq	$1, 72(%r12)
	leaq	112(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%r12, %rax
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	movl	$0x3f800000, 96(%r12)
	movq	$0, 104(%r12)
	movq	$0, 112(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	.cfi_restore_state
	movl	$24, %edi
	call	_Znwm@PLT
	movq	%rax, %r12
	leaq	16+_ZTVN4node24NodeArrayBufferAllocatorE(%rip), %rax
	movq	%rax, (%r12)
	movq	%r12, %rax
	movl	$1, 8(%r12)
	movq	$0, 16(%r12)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	leaq	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7730:
	.size	_ZN4node26CreateArrayBufferAllocatorEv, .-_ZN4node26CreateArrayBufferAllocatorEv
	.p2align 4
	.globl	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE
	.type	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE, @function
_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE:
.LFB7731:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L181
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L181:
	ret
	.cfi_endproc
.LFE7731:
	.size	_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE, .-_ZN4node24FreeArrayBufferAllocatorEPNS_20ArrayBufferAllocatorE
	.p2align 4
	.globl	_ZN4node29SetIsolateCreateParamsForNodeEPN2v87Isolate12CreateParamsE
	.type	_ZN4node29SetIsolateCreateParamsForNodeEPN2v87Isolate12CreateParamsE, @function
_ZN4node29SetIsolateCreateParamsForNodeEPN2v87Isolate12CreateParamsE:
.LFB7732:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	call	uv_get_constrained_memory@PLT
	testq	%rax, %rax
	je	.L184
	movq	%rax, %r12
	call	uv_get_total_memory@PLT
	cmpq	%rax, %r12
	jb	.L185
	testq	%rax, %rax
	jne	.L189
.L183:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	call	uv_get_total_memory@PLT
	testq	%rax, %rax
	je	.L183
.L189:
	movq	%rax, %r12
.L185:
	leaq	8(%rbx), %rdi
	movq	%r12, %rsi
	popq	%rbx
	xorl	%edx, %edx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v819ResourceConstraints17ConfigureDefaultsEmm@PLT
	.cfi_endproc
.LFE7732:
	.size	_ZN4node29SetIsolateCreateParamsForNodeEPN2v87Isolate12CreateParamsE, .-_ZN4node29SetIsolateCreateParamsForNodeEPN2v87Isolate12CreateParamsE
	.p2align 4
	.globl	_ZN4node23SetIsolateErrorHandlersEPN2v87IsolateERKNS_15IsolateSettingsE
	.type	_ZN4node23SetIsolateErrorHandlersEPN2v87IsolateERKNS_15IsolateSettingsE, @function
_ZN4node23SetIsolateErrorHandlersEPN2v87IsolateERKNS_15IsolateSettingsE:
.LFB7733:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testb	$1, (%rsi)
	jne	.L202
.L191:
	movq	16(%rbx), %rsi
	leaq	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE(%rip), %rax
	movq	%r12, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	_ZN2v87Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPS0_E@PLT
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L203
.L193:
	movq	%r12, %rdi
	call	_ZN2v87Isolate20SetFatalErrorHandlerEPFvPKcS2_E@PLT
	movq	32(%rbx), %rsi
	leaq	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE(%rip), %rax
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	_ZN2v87Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS4_IS2_EENS4_INS_5ArrayEEEE@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	.cfi_restore_state
	movq	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE@GOTPCREL(%rip), %rsi
	xorl	%ecx, %ecx
	movl	$24, %edx
	call	_ZN2v87Isolate32AddMessageListenerWithErrorLevelEPFvNS_5LocalINS_7MessageEEENS1_INS_5ValueEEEEiS5_@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L203:
	movq	_ZN4node12OnFatalErrorEPKcS1_@GOTPCREL(%rip), %rsi
	jmp	.L193
	.cfi_endproc
.LFE7733:
	.size	_ZN4node23SetIsolateErrorHandlersEPN2v87IsolateERKNS_15IsolateSettingsE, .-_ZN4node23SetIsolateErrorHandlersEPN2v87IsolateERKNS_15IsolateSettingsE
	.p2align 4
	.globl	_ZN4node22SetIsolateMiscHandlersEPN2v87IsolateERKNS_15IsolateSettingsE
	.type	_ZN4node22SetIsolateMiscHandlersEPN2v87IsolateERKNS_15IsolateSettingsE, @function
_ZN4node22SetIsolateMiscHandlersEPN2v87IsolateERKNS_15IsolateSettingsE:
.LFB7734:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movl	8(%rsi), %esi
	call	_ZN2v87Isolate19SetMicrotasksPolicyENS_16MicrotasksPolicyE@PLT
	movq	48(%rbx), %rsi
	leaq	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE(%rip), %rax
	movq	%r12, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L213
.L206:
	movq	%r12, %rdi
	call	_ZN2v87Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE@PLT
	movq	56(%rbx), %rsi
	leaq	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE(%rip), %rax
	movq	%r12, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	_ZN2v87Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS1_INS_17FinalizationGroupEEEE@PLT
	testb	$2, (%rbx)
	jne	.L214
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	.cfi_restore_state
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v811CpuProfiler38UseDetailedSourcePositionsForProfilingEPNS_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L213:
	.cfi_restore_state
	movq	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE@GOTPCREL(%rip), %rsi
	jmp	.L206
	.cfi_endproc
.LFE7734:
	.size	_ZN4node22SetIsolateMiscHandlersEPN2v87IsolateERKNS_15IsolateSettingsE, .-_ZN4node22SetIsolateMiscHandlersEPN2v87IsolateERKNS_15IsolateSettingsE
	.p2align 4
	.globl	_ZN4node19SetIsolateUpForNodeEPN2v87IsolateERKNS_15IsolateSettingsE
	.type	_ZN4node19SetIsolateUpForNodeEPN2v87IsolateERKNS_15IsolateSettingsE, @function
_ZN4node19SetIsolateUpForNodeEPN2v87IsolateERKNS_15IsolateSettingsE:
.LFB7735:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	testb	$1, (%rsi)
	jne	.L234
.L216:
	movq	16(%rbx), %rsi
	leaq	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE(%rip), %rax
	movq	%r12, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	_ZN2v87Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPS0_E@PLT
	movq	24(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L235
.L218:
	movq	%r12, %rdi
	call	_ZN2v87Isolate20SetFatalErrorHandlerEPFvPKcS2_E@PLT
	movq	32(%rbx), %rsi
	leaq	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE(%rip), %rax
	movq	%r12, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	_ZN2v87Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS4_IS2_EENS4_INS_5ArrayEEEE@PLT
	movl	8(%rbx), %esi
	movq	%r12, %rdi
	call	_ZN2v87Isolate19SetMicrotasksPolicyENS_16MicrotasksPolicyE@PLT
	movq	48(%rbx), %rsi
	leaq	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE(%rip), %rax
	movq	%r12, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	40(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L236
.L221:
	movq	%r12, %rdi
	call	_ZN2v87Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE@PLT
	movq	56(%rbx), %rsi
	leaq	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE(%rip), %rax
	movq	%r12, %rdi
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	call	_ZN2v87Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS1_INS_17FinalizationGroupEEEE@PLT
	testb	$2, (%rbx)
	jne	.L237
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movq	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE@GOTPCREL(%rip), %rsi
	xorl	%ecx, %ecx
	movl	$24, %edx
	call	_ZN2v87Isolate32AddMessageListenerWithErrorLevelEPFvNS_5LocalINS_7MessageEEENS1_INS_5ValueEEEEiS5_@PLT
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L237:
	popq	%rbx
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v811CpuProfiler38UseDetailedSourcePositionsForProfilingEPNS_7IsolateE@PLT
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	movq	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE@GOTPCREL(%rip), %rsi
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L235:
	movq	_ZN4node12OnFatalErrorEPKcS1_@GOTPCREL(%rip), %rsi
	jmp	.L218
	.cfi_endproc
.LFE7735:
	.size	_ZN4node19SetIsolateUpForNodeEPN2v87IsolateERKNS_15IsolateSettingsE, .-_ZN4node19SetIsolateUpForNodeEPN2v87IsolateERKNS_15IsolateSettingsE
	.p2align 4
	.globl	_ZN4node19SetIsolateUpForNodeEPN2v87IsolateE
	.type	_ZN4node19SetIsolateUpForNodeEPN2v87IsolateE, @function
_ZN4node19SetIsolateUpForNodeEPN2v87IsolateE:
.LFB7736:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	movl	$24, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE@GOTPCREL(%rip), %rsi
	call	_ZN2v87Isolate32AddMessageListenerWithErrorLevelEPFvNS_5LocalINS_7MessageEEENS1_INS_5ValueEEEEiS5_@PLT
	movq	%r12, %rdi
	leaq	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE(%rip), %rsi
	call	_ZN2v87Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPS0_E@PLT
	movq	_ZN4node12OnFatalErrorEPKcS1_@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate20SetFatalErrorHandlerEPFvPKcS2_E@PLT
	movq	%r12, %rdi
	leaq	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE(%rip), %rsi
	call	_ZN2v87Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS4_IS2_EENS4_INS_5ArrayEEEE@PLT
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v87Isolate19SetMicrotasksPolicyENS_16MicrotasksPolicyE@PLT
	movq	%r12, %rdi
	leaq	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE(%rip), %rsi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE@PLT
	movq	%r12, %rdi
	leaq	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE(%rip), %rsi
	call	_ZN2v87Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS1_INS_17FinalizationGroupEEEE@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v811CpuProfiler38UseDetailedSourcePositionsForProfilingEPNS_7IsolateE@PLT
	.cfi_endproc
.LFE7736:
	.size	_ZN4node19SetIsolateUpForNodeEPN2v87IsolateE, .-_ZN4node19SetIsolateUpForNodeEPN2v87IsolateE
	.p2align 4
	.globl	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_s
	.type	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_s, @function
_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_s:
.LFB7740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-136(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$112, %rsp
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %r15
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -144(%rbp)
	call	_ZN2v819ResourceConstraintsC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movw	%ax, -48(%rbp)
	movups	%xmm0, -88(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%r12, %r12
	je	.L241
	movq	%r12, -64(%rbp)
.L241:
	call	_ZN2v87Isolate8AllocateEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L240
	movq	(%r15), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	*192(%rax)
	call	uv_get_constrained_memory@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L258
	call	uv_get_total_memory@PLT
.L245:
	testq	%rax, %rax
	jne	.L259
.L246:
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate10InitializeEPS0_RKNS0_12CreateParamsE@PLT
	xorl	%ecx, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE@GOTPCREL(%rip), %rsi
	call	_ZN2v87Isolate32AddMessageListenerWithErrorLevelEPFvNS_5LocalINS_7MessageEEENS1_INS_5ValueEEEEiS5_@PLT
	leaq	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPS0_E@PLT
	movq	_ZN4node12OnFatalErrorEPKcS1_@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate20SetFatalErrorHandlerEPFvPKcS2_E@PLT
	leaq	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS4_IS2_EENS4_INS_5ArrayEEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Isolate19SetMicrotasksPolicyENS_16MicrotasksPolicyE@PLT
	leaq	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE@PLT
	movq	%r12, %rdi
	leaq	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE(%rip), %rsi
	call	_ZN2v87Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS1_INS_17FinalizationGroupEEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811CpuProfiler38UseDetailedSourcePositionsForProfilingEPNS_7IsolateE@PLT
.L240:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L260
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	.cfi_restore_state
	call	uv_get_total_memory@PLT
	cmpq	%rax, %r13
	jnb	.L245
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v819ResourceConstraints17ConfigureDefaultsEmm@PLT
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%rax, %r13
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v819ResourceConstraints17ConfigureDefaultsEmm@PLT
	jmp	.L246
.L260:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7740:
	.size	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_s, .-_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_s
	.p2align 4
	.globl	_ZN4node10NewIsolateEPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformE
	.type	_ZN4node10NewIsolateEPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformE, @function
_ZN4node10NewIsolateEPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformE:
.LFB7741:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	call	_ZN2v87Isolate8AllocateEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L261
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	*192(%rax)
	call	uv_get_constrained_memory@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L274
	call	uv_get_total_memory@PLT
.L265:
	testq	%rax, %rax
	jne	.L275
.L266:
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate10InitializeEPS0_RKNS0_12CreateParamsE@PLT
	xorl	%ecx, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE@GOTPCREL(%rip), %rsi
	call	_ZN2v87Isolate32AddMessageListenerWithErrorLevelEPFvNS_5LocalINS_7MessageEEENS1_INS_5ValueEEEEiS5_@PLT
	leaq	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPS0_E@PLT
	movq	_ZN4node12OnFatalErrorEPKcS1_@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate20SetFatalErrorHandlerEPFvPKcS2_E@PLT
	leaq	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS4_IS2_EENS4_INS_5ArrayEEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Isolate19SetMicrotasksPolicyENS_16MicrotasksPolicyE@PLT
	leaq	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE@PLT
	movq	%r12, %rdi
	leaq	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE(%rip), %rsi
	call	_ZN2v87Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS1_INS_17FinalizationGroupEEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811CpuProfiler38UseDetailedSourcePositionsForProfilingEPNS_7IsolateE@PLT
.L261:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	.cfi_restore_state
	call	uv_get_total_memory@PLT
	cmpq	%rax, %r13
	jnb	.L265
	leaq	8(%r14), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v819ResourceConstraints17ConfigureDefaultsEmm@PLT
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rax, %r13
	leaq	8(%r14), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	call	_ZN2v819ResourceConstraints17ConfigureDefaultsEmm@PLT
	jmp	.L266
	.cfi_endproc
.LFE7741:
	.size	_ZN4node10NewIsolateEPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformE, .-_ZN4node10NewIsolateEPN2v87Isolate12CreateParamsEP9uv_loop_sPNS_20MultiIsolatePlatformE
	.p2align 4
	.globl	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE
	.type	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE, @function
_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE:
.LFB7742:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-136(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	subq	$112, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	$0, -144(%rbp)
	call	_ZN2v819ResourceConstraintsC1Ev@PLT
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movq	$0, -96(%rbp)
	movq	$0, -72(%rbp)
	movw	%ax, -48(%rbp)
	movups	%xmm0, -88(%rbp)
	movaps	%xmm0, -64(%rbp)
	testq	%r12, %r12
	je	.L277
	movq	%r12, -64(%rbp)
.L277:
	call	_ZN2v87Isolate8AllocateEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L276
	movq	0(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	*192(%rax)
	call	uv_get_constrained_memory@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	jne	.L294
	call	uv_get_total_memory@PLT
.L281:
	testq	%rax, %rax
	jne	.L295
.L282:
	leaq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate10InitializeEPS0_RKNS0_12CreateParamsE@PLT
	xorl	%ecx, %ecx
	movl	$24, %edx
	movq	%r12, %rdi
	movq	_ZN4node6errors25PerIsolateMessageListenerEN2v85LocalINS1_7MessageEEENS2_INS1_5ValueEEE@GOTPCREL(%rip), %rsi
	call	_ZN2v87Isolate32AddMessageListenerWithErrorLevelEPFvNS_5LocalINS_7MessageEEENS1_INS_5ValueEEEEiS5_@PLT
	leaq	_ZN4nodeL30ShouldAbortOnUncaughtExceptionEPN2v87IsolateE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate35SetAbortOnUncaughtExceptionCallbackEPFbPS0_E@PLT
	movq	_ZN4node12OnFatalErrorEPKcS1_@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate20SetFatalErrorHandlerEPFvPKcS2_E@PLT
	leaq	_ZN4nodeL25PrepareStackTraceCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_5ValueEEENS1_INS0_5ArrayEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate28SetPrepareStackTraceCallbackEPFNS_10MaybeLocalINS_5ValueEEENS_5LocalINS_7ContextEEENS4_IS2_EENS4_INS_5ArrayEEEE@PLT
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v87Isolate19SetMicrotasksPolicyENS_16MicrotasksPolicyE@PLT
	leaq	_ZN4nodeL31AllowWasmCodeGenerationCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_6StringEEE(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate34SetAllowWasmCodeGenerationCallbackEPFbNS_5LocalINS_7ContextEEENS1_INS_6StringEEEE@PLT
	movq	_ZN4node10task_queue21PromiseRejectCallbackEN2v820PromiseRejectMessageE@GOTPCREL(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v87Isolate24SetPromiseRejectCallbackEPFvNS_20PromiseRejectMessageEE@PLT
	movq	%r12, %rdi
	leaq	_ZN4nodeL36HostCleanupFinalizationGroupCallbackEN2v85LocalINS0_7ContextEEENS1_INS0_17FinalizationGroupEEE(%rip), %rsi
	call	_ZN2v87Isolate39SetHostCleanupFinalizationGroupCallbackEPFvNS_5LocalINS_7ContextEEENS1_INS_17FinalizationGroupEEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v811CpuProfiler38UseDetailedSourcePositionsForProfilingEPNS_7IsolateE@PLT
.L276:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L296
	addq	$112, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L294:
	.cfi_restore_state
	call	uv_get_total_memory@PLT
	cmpq	%rax, %r13
	jnb	.L281
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v819ResourceConstraints17ConfigureDefaultsEmm@PLT
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rax, %r13
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	_ZN2v819ResourceConstraints17ConfigureDefaultsEmm@PLT
	jmp	.L282
.L296:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7742:
	.size	_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE, .-_ZN4node10NewIsolateEPNS_20ArrayBufferAllocatorEP9uv_loop_sPNS_20MultiIsolatePlatformE
	.p2align 4
	.globl	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE
	.type	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE, @function
_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE:
.LFB7743:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movl	$2416, %edi
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$8, %rsp
	call	_Znwm@PLT
	movq	%rbx, %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	xorl	%r9d, %r9d
	movq	%rax, %r12
	call	_ZN4node11IsolateDataC1EPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorEPKSt6vectorImSaImEE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7743:
	.size	_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE, .-_ZN4node17CreateIsolateDataEPN2v87IsolateEP9uv_loop_sPNS_20MultiIsolatePlatformEPNS_20ArrayBufferAllocatorE
	.p2align 4
	.globl	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE
	.type	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE, @function
_ZN4node15FreeIsolateDataEPNS_11IsolateDataE:
.LFB7744:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L299
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L299:
	ret
	.cfi_endproc
.LFE7744:
	.size	_ZN4node15FreeIsolateDataEPNS_11IsolateDataE, .-_ZN4node15FreeIsolateDataEPNS_11IsolateDataE
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"cannot create std::vector larger than max_size()"
	.align 8
.LC6:
	.string	"basic_string::_M_construct null not valid"
	.text
	.p2align 4
	.globl	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_
	.type	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_, @function
_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_:
.LFB7745:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-144(%rbp), %r15
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rcx, %rbx
	subq	$168, %rsp
	movl	%r8d, -188(%rbp)
	movq	%rdi, -200(%rbp)
	movq	%rsi, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movslq	%r14d, %rdx
	pxor	%xmm0, %xmm0
	movq	$0, -96(%rbp)
	salq	$3, %rdx
	movaps	%xmm0, -112(%rbp)
	leaq	(%rbx,%rdx), %rax
	sarq	$3, %rdx
	movq	%rax, -168(%rbp)
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %rdx
	ja	.L311
	movq	%rdx, %rsi
	xorl	%r14d, %r14d
	salq	$5, %rsi
	testq	%rdx, %rdx
	je	.L303
	movq	%rsi, %rdi
	movq	%rsi, -176(%rbp)
	call	_Znwm@PLT
	movq	-176(%rbp), %rsi
	movq	%rax, %r14
.L303:
	addq	%r14, %rsi
	movq	%r14, -112(%rbp)
	movq	%rsi, -96(%rbp)
	cmpq	%rbx, -168(%rbp)
	je	.L304
	leaq	-80(%rbp), %rax
	movq	%rax, -208(%rbp)
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L306:
	cmpq	$1, %rax
	jne	.L308
	movzbl	(%r10), %edx
	movb	%dl, 16(%r14)
.L309:
	movq	%rax, 8(%r14)
	addq	$8, %rbx
	addq	$32, %r14
	movb	$0, (%r8,%rax)
	cmpq	%rbx, -168(%rbp)
	je	.L304
.L310:
	movq	(%rbx), %r10
	leaq	16(%r14), %r8
	movq	%r8, (%r14)
	testq	%r10, %r10
	je	.L314
	movq	%r10, %rdi
	movq	%r8, -184(%rbp)
	movq	%r10, -176(%rbp)
	call	strlen@PLT
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -80(%rbp)
	movq	%rax, %r9
	jbe	.L306
	movq	-208(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%rax, -184(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r9
	movq	%rax, (%r14)
	movq	%rax, %r8
	movq	-80(%rbp), %rax
	movq	%rax, 16(%r14)
.L307:
	movq	%r8, %rdi
	movq	%r9, %rdx
	movq	%r10, %rsi
	call	memcpy@PLT
	movq	-80(%rbp), %rax
	movq	(%r14), %r8
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L314:
	leaq	.LC6(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L308:
	testq	%rax, %rax
	je	.L309
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L304:
	movslq	-188(%rbp), %r8
	pxor	%xmm0, %xmm0
	movq	%r14, -104(%rbp)
	movq	$0, -64(%rbp)
	salq	$3, %r8
	movaps	%xmm0, -80(%rbp)
	leaq	(%r12,%r8), %rax
	sarq	$3, %r8
	movq	%rax, -168(%rbp)
	movabsq	$288230376151711743, %rax
	cmpq	%rax, %r8
	ja	.L311
	movq	%r8, %r14
	xorl	%ebx, %ebx
	salq	$5, %r14
	testq	%r8, %r8
	je	.L312
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%rax, %rbx
.L312:
	addq	%rbx, %r14
	movq	%rbx, -80(%rbp)
	movq	%r14, -64(%rbp)
	cmpq	%r12, -168(%rbp)
	je	.L313
	leaq	-152(%rbp), %rax
	movq	%rax, -184(%rbp)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L357:
	movzbl	(%r14), %edx
	movb	%dl, 16(%rbx)
.L318:
	movq	%rax, 8(%rbx)
	addq	$8, %r12
	addq	$32, %rbx
	movb	$0, (%r8,%rax)
	cmpq	%r12, -168(%rbp)
	je	.L313
.L319:
	movq	(%r12), %r14
	leaq	16(%rbx), %r8
	movq	%r8, (%rbx)
	movq	%r8, -176(%rbp)
	testq	%r14, %r14
	je	.L314
	movq	%r14, %rdi
	call	strlen@PLT
	movq	-176(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -152(%rbp)
	movq	%rax, %r9
	ja	.L356
	cmpq	$1, %rax
	je	.L357
	testq	%rax, %rax
	je	.L318
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L356:
	movq	-184(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-176(%rbp), %r9
	movq	%rax, (%rbx)
	movq	%rax, %r8
	movq	-152(%rbp), %rax
	movq	%rax, 16(%rbx)
.L316:
	movq	%r8, %rdi
	movq	%r9, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-152(%rbp), %rax
	movq	(%rbx), %r8
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L313:
	movl	$3288, %edi
	movq	%rbx, -72(%rbp)
	call	_Znwm@PLT
	subq	$8, %rsp
	leaq	-112(%rbp), %rcx
	movq	%r13, %rdx
	pushq	$-1
	movq	-200(%rbp), %rsi
	movq	%rax, %rdi
	leaq	-80(%rbp), %r8
	movl	$7, %r9d
	movq	%rax, %r14
	call	_ZN4node11EnvironmentC1EPNS_11IsolateDataEN2v85LocalINS3_7ContextEEERKSt6vectorINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESaISD_EESH_NS0_5FlagsEm@PLT
	movzbl	_ZN4node11per_process15v8_is_profilingE(%rip), %esi
	movq	%r14, %rdi
	call	_ZN4node11Environment15InitializeLibuvEb@PLT
	movq	%r14, %rdi
	call	_ZN4node11Environment16RunBootstrappingEv@PLT
	movq	-72(%rbp), %rbx
	movq	-80(%rbp), %r12
	testq	%rax, %rax
	movl	$0, %eax
	popq	%rdx
	popq	%rcx
	cmove	%rax, %r14
	cmpq	%r12, %rbx
	je	.L321
	.p2align 4,,10
	.p2align 3
.L325:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L322
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L325
.L323:
	movq	-80(%rbp), %r12
.L321:
	testq	%r12, %r12
	je	.L326
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L326:
	movq	-104(%rbp), %rbx
	movq	-112(%rbp), %r12
	cmpq	%r12, %rbx
	je	.L327
	.p2align 4,,10
	.p2align 3
.L331:
	movq	(%r12), %rdi
	leaq	16(%r12), %rax
	cmpq	%rax, %rdi
	je	.L328
	call	_ZdlPv@PLT
	addq	$32, %r12
	cmpq	%rbx, %r12
	jne	.L331
.L329:
	movq	-112(%rbp), %r12
.L327:
	testq	%r12, %r12
	je	.L332
	movq	%r12, %rdi
	call	_ZdlPv@PLT
.L332:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L358
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	.cfi_restore_state
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L325
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L328:
	addq	$32, %r12
	cmpq	%r12, %rbx
	jne	.L331
	jmp	.L329
.L311:
	leaq	.LC5(%rip), %rdi
	call	_ZSt20__throw_length_errorPKc@PLT
.L358:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7745:
	.size	_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_, .-_ZN4node17CreateEnvironmentEPNS_11IsolateDataEN2v85LocalINS2_7ContextEEEiPKPKciS9_
	.p2align 4
	.globl	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE
	.type	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE, @function
_ZN4node15FreeEnvironmentEPNS_11EnvironmentE:
.LFB7746:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN4node11Environment10RunCleanupEv@PLT
	testq	%r12, %r12
	je	.L359
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L359:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7746:
	.size	_ZN4node15FreeEnvironmentEPNS_11EnvironmentE, .-_ZN4node15FreeEnvironmentEPNS_11EnvironmentE
	.p2align 4
	.globl	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE
	.type	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE, @function
_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE:
.LFB7747:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L369
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L365
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L365
	movq	271(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	.cfi_restore_state
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L369:
	.cfi_restore 3
	.cfi_restore 6
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7747:
	.size	_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE, .-_ZN4node21GetCurrentEnvironmentEN2v85LocalINS0_7ContextEEE
	.p2align 4
	.globl	_ZN4node33GetMainThreadMultiIsolatePlatformEv
	.type	_ZN4node33GetMainThreadMultiIsolatePlatformEv, @function
_ZN4node33GetMainThreadMultiIsolatePlatformEv:
.LFB7748:
	.cfi_startproc
	endbr64
	movq	32+_ZN4node11per_process11v8_platformE(%rip), %rax
	ret
	.cfi_endproc
.LFE7748:
	.size	_ZN4node33GetMainThreadMultiIsolatePlatformEv, .-_ZN4node33GetMainThreadMultiIsolatePlatformEv
	.p2align 4
	.globl	_ZN4node14CreatePlatformEiPNS_7tracing17TracingControllerE
	.type	_ZN4node14CreatePlatformEiPNS_7tracing17TracingControllerE, @function
_ZN4node14CreatePlatformEiPNS_7tracing17TracingControllerE:
.LFB7749:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edi, %r13d
	movl	$128, %edi
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -40
	call	_Znwm@PLT
	movq	%r14, %rdx
	movl	%r13d, %esi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN4node12NodePlatformC1EiPNS_7tracing17TracingControllerE@PLT
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7749:
	.size	_ZN4node14CreatePlatformEiPNS_7tracing17TracingControllerE, .-_ZN4node14CreatePlatformEiPNS_7tracing17TracingControllerE
	.p2align 4
	.globl	_ZN4node12FreePlatformEPNS_20MultiIsolatePlatformE
	.type	_ZN4node12FreePlatformEPNS_20MultiIsolatePlatformE, @function
_ZN4node12FreePlatformEPNS_20MultiIsolatePlatformE:
.LFB7750:
	.cfi_startproc
	endbr64
	testq	%rdi, %rdi
	je	.L373
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node12NodePlatformD0Ev(%rip), %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L375
	movq	120(%rdi), %r13
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rax, (%rdi)
	testq	%r13, %r13
	je	.L377
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L378
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r13)
.L379:
	cmpl	$1, %eax
	jne	.L377
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	testq	%rbx, %rbx
	je	.L381
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r13)
.L382:
	cmpl	$1, %eax
	je	.L405
	.p2align 4,,10
	.p2align 3
.L377:
	movq	64(%r12), %rbx
	testq	%rbx, %rbx
	je	.L383
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	jne	.L384
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L391:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L383
.L384:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L391
	lock subl	$1, 8(%r13)
	jne	.L391
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L391
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L384
	.p2align 4,,10
	.p2align 3
.L383:
	movq	56(%r12), %rax
	movq	48(%r12), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%r12), %rdi
	leaq	96(%r12), %rax
	movq	$0, 72(%r12)
	movq	$0, 64(%r12)
	cmpq	%rax, %rdi
	je	.L393
	call	_ZdlPv@PLT
.L393:
	leaq	8(%r12), %rdi
	call	uv_mutex_destroy@PLT
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	movq	%r12, %rdi
	movl	$128, %esi
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L388:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L387
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L387:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L383
.L389:
	movq	%rbx, %r14
	movq	(%rbx), %rbx
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L387
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L387
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L373:
	.cfi_def_cfa 7, 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L375:
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L405:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L381:
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	jmp	.L382
	.cfi_endproc
.LFE7750:
	.size	_ZN4node12FreePlatformEPNS_20MultiIsolatePlatformE, .-_ZN4node12FreePlatformEPNS_20MultiIsolatePlatformE
	.section	.rodata.str1.1
.LC7:
	.string	"Intl"
.LC8:
	.string	"v8BreakIterator"
.LC9:
	.string	"Atomics"
.LC10:
	.string	"wake"
.LC11:
	.string	"Object"
.LC12:
	.string	"prototype"
.LC13:
	.string	"__proto__"
.LC14:
	.string	"delete"
.LC15:
	.string	"throw"
.LC16:
	.string	""
.LC17:
	.string	"invalid --disable-proto mode"
.LC18:
	.string	"InitializeContextRuntime()"
	.text
	.p2align 4
	.globl	_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE
	.type	_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE, @function
_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE:
.LFB7757:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %r13
	movq	%rax, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	$4, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L443
.L407:
	movl	$15, %ecx
	xorl	%edx, %edx
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L444
.L408:
	movq	%r12, %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L413
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L445
.L413:
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC9(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L446
.L414:
	movl	$4, %ecx
	xorl	%edx, %edx
	leaq	.LC10(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L447
.L415:
	movq	%r12, %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L417
	movq	%rax, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	jne	.L448
.L417:
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC11(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L449
.L419:
	movl	$9, %ecx
	xorl	%edx, %edx
	leaq	.LC12(%rip), %rsi
	movq	%r13, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L450
.L420:
	movq	%r12, %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L451
.L421:
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L452
.L422:
	movq	%r13, %rdi
	movl	$9, %ecx
	xorl	%edx, %edx
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L453
.L423:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	.LC14(%rip), %rsi
	leaq	144(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L424
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L454
.L426:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L455
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L424:
	.cfi_restore_state
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	.LC15(%rip), %rsi
	leaq	144(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L427
	leaq	_ZN4node12ProtoThrowerERKN2v820FunctionCallbackInfoINS0_5ValueEEE(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$1, %r8d
	movq	%r12, %rdi
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L456
.L428:
	leaq	-88(%rbp), %rbx
	movq	%rsi, %rdx
	movq	%rbx, %rdi
	call	_ZN2v818PropertyDescriptorC1ENS_5LocalINS_5ValueEEES3_@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_ZN2v818PropertyDescriptor14set_enumerableEb@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_ZN2v818PropertyDescriptor16set_configurableEb@PLT
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object14DefinePropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEERNS_18PropertyDescriptorE@PLT
	testb	%al, %al
	je	.L457
.L429:
	movq	%rbx, %rdi
	call	_ZN2v818PropertyDescriptorD1Ev@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L427:
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	leaq	.LC16(%rip), %rsi
	leaq	144(%rax), %rdi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	je	.L426
	leaq	.LC17(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	call	_ZN4node10FatalErrorEPKcS1_@PLT
	.p2align 4,,10
	.p2align 3
.L448:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L417
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L445:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L413
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L454:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L451:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rdi
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L452:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L422
	.p2align 4,,10
	.p2align 3
.L443:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L444:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L453:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L446:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L447:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L449:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L419
	.p2align 4,,10
	.p2align 3
.L450:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L457:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L456:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rsi
	jmp	.L428
.L455:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7757:
	.size	_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE, .-_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE
	.section	.rodata.str1.1
.LC19:
	.string	"primordials"
.LC20:
	.string	"global"
.LC21:
	.string	"exports"
	.text
	.p2align 4
	.globl	_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE
	.type	_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE, @function
_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE:
.LFB7759:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r12, %rdi
	movq	%rax, %rbx
	call	_ZN2v87Context5EnterEv@PLT
	movl	$11, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L501
.L459:
	movl	$6, %ecx
	xorl	%edx, %edx
	leaq	.LC20(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -160(%rbp)
	testq	%rax, %rax
	je	.L502
.L460:
	movl	$7, %ecx
	xorl	%edx, %edx
	leaq	.LC21(%rip), %rsi
	movq	%rbx, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -136(%rbp)
	testq	%rax, %rax
	je	.L503
.L461:
	movq	%rbx, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	leaq	104(%rbx), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	_ZN2v86Object12SetPrototypeENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L504
.L462:
	shrw	$8, %ax
	jne	.L463
.L473:
	movb	$0, -124(%rbp)
.L464:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L505
	movzbl	-124(%rbp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	.cfi_restore_state
	movq	%r12, %rdi
	call	_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE
	movq	%rax, -120(%rbp)
	testq	%rax, %rax
	je	.L473
	movq	%rax, %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L506
.L475:
	movzbl	%ah, %eax
	movb	%al, -124(%rbp)
	testb	%al, %al
	je	.L473
	movq	-160(%rbp), %xmm2
	leaq	-112(%rbp), %rax
	cmpq	$0, _ZZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEEE13context_files(%rip)
	leaq	_ZZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEEE13context_files(%rip), %r13
	movhps	-136(%rbp), %xmm2
	movq	%rax, -136(%rbp)
	movaps	%xmm2, -160(%rbp)
	je	.L464
	.p2align 4,,10
	.p2align 3
.L467:
	pxor	%xmm0, %xmm0
	movl	$24, %edi
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	_Znwm@PLT
	movdqa	-160(%rbp), %xmm1
	movq	%r12, %rdi
	movq	%r15, 16(%rax)
	leaq	24(%rax), %rdx
	movups	%xmm1, (%rax)
	movq	%rdx, -96(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rax, -112(%rbp)
	call	_ZN2v87Context6GlobalEv@PLT
	movq	0(%r13), %rsi
	movq	%r12, %rdi
	xorl	%ecx, %ecx
	movq	%rax, -80(%rbp)
	movq	-136(%rbp), %rdx
	movq	-120(%rbp), %rax
	movq	%r14, -64(%rbp)
	movq	%rax, -72(%rbp)
	call	_ZN4node13native_module15NativeModuleEnv16LookupAndCompileEN2v85LocalINS2_7ContextEEEPKcPSt6vectorINS3_INS2_6StringEEESaISA_EEPNS_11EnvironmentE@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L468
	leaq	88(%rbx), %rdx
	leaq	-80(%rbp), %r8
	movl	$3, %ecx
	movq	%r12, %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	je	.L468
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L470
	call	_ZdlPv@PLT
	addq	$8, %r13
	cmpq	$0, 0(%r13)
	jne	.L467
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L470:
	addq	$8, %r13
	cmpq	$0, 0(%r13)
	jne	.L467
	jmp	.L464
	.p2align 4,,10
	.p2align 3
.L468:
	movq	-112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L473
	call	_ZdlPv@PLT
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L504:
	movl	%eax, -120(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-120(%rbp), %eax
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L501:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L502:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L503:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L506:
	movl	%eax, -124(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movl	-124(%rbp), %eax
	jmp	.L475
.L505:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7759:
	.size	_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE, .-_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"node:per_context_binding_exports"
	.text
	.p2align 4
	.globl	_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE
	.type	_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE, @function
_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE:
.LFB7751:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r15
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	movq	%rax, %r13
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Context6GlobalEv@PLT
	movl	$32, %ecx
	xorl	%edx, %edx
	movq	%r13, %rdi
	leaq	.LC22(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L525
.L508:
	movq	%r13, %rdi
	call	_ZN2v87Private6ForApiEPNS_7IsolateENS_5LocalINS_6StringEEE@PLT
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	%rax, %rbx
	call	_ZN2v86Object10GetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEE@PLT
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%r14, %r14
	je	.L511
	movq	%r14, %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L526
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
.L511:
	movq	%r15, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	movq	-104(%rbp), %rax
	jne	.L527
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L526:
	.cfi_restore_state
	movq	%r13, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%r12, %rdi
	movq	%rax, %r13
	call	_ZN2v87Context6GlobalEv@PLT
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v86Object10SetPrivateENS_5LocalINS_7ContextEEENS1_INS_7PrivateEEENS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L512
.L513:
	xorl	%eax, %eax
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L512:
	movq	%r12, %rdi
	call	_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE
	testb	%al, %al
	je	.L513
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L525:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rsi
	jmp	.L508
.L527:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7751:
	.size	_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE, .-_ZN4node20GetPerContextExportsEN2v85LocalINS0_7ContextEEE
	.p2align 4
	.globl	_ZN4node28InitializeContextForSnapshotEN2v85LocalINS0_7ContextEEE
	.type	_ZN4node28InitializeContextForSnapshotEN2v85LocalINS0_7ContextEEE, @function
_ZN4node28InitializeContextForSnapshotEN2v85LocalINS0_7ContextEEE:
.LFB7758:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	112(%rbx), %rdx
	movl	$34, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context15SetEmbedderDataEiNS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L531
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L531:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7758:
	.size	_ZN4node28InitializeContextForSnapshotEN2v85LocalINS0_7ContextEEE, .-_ZN4node28InitializeContextForSnapshotEN2v85LocalINS0_7ContextEEE
	.p2align 4
	.globl	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE
	.type	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE, @function
_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE:
.LFB7755:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$48, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	pushq	$0
	call	_ZN2v87Context3NewEPNS_7IsolateEPNS_22ExtensionConfigurationENS_10MaybeLocalINS_14ObjectTemplateEEENS5_INS_5ValueEEENS_33DeserializeInternalFieldsCallbackEPNS_14MicrotaskQueueE@PLT
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	je	.L538
	movq	%rax, %rdi
	movq	%rax, %rbx
	leaq	-64(%rbp), %r13
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	movq	%rax, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	112(%r12), %rdx
	movl	$34, %esi
	movq	%rbx, %rdi
	call	_ZN2v87Context15SetEmbedderDataEiNS_5LocalINS_5ValueEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE
	movq	%r13, %rdi
	movl	%eax, %r12d
	call	_ZN2v811HandleScopeD1Ev@PLT
	testb	%r12b, %r12b
	jne	.L535
.L538:
	xorl	%eax, %eax
.L534:
	movq	-40(%rbp), %rcx
	xorq	%fs:40, %rcx
	jne	.L539
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L535:
	.cfi_restore_state
	movq	%rbx, %rdi
	call	_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE
	movq	%rbx, %rax
	jmp	.L534
.L539:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7755:
	.size	_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE, .-_ZN4node10NewContextEPN2v87IsolateENS0_5LocalINS0_14ObjectTemplateEEE
	.p2align 4
	.globl	_ZN4node17InitializeContextEN2v85LocalINS0_7ContextEEE
	.type	_ZN4node17InitializeContextEN2v85LocalINS0_7ContextEEE, @function
_ZN4node17InitializeContextEN2v85LocalINS0_7ContextEEE:
.LFB7760:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	leaq	-64(%rbp), %r14
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	subq	$32, %rsp
	.cfi_offset 3, -48
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Context10GetIsolateEv@PLT
	movq	%r14, %rdi
	movq	%rax, %rbx
	movq	%rax, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	leaq	112(%rbx), %rdx
	movl	$34, %esi
	movq	%r12, %rdi
	call	_ZN2v87Context15SetEmbedderDataEiNS_5LocalINS_5ValueEEE@PLT
	movq	%r12, %rdi
	call	_ZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEE
	movq	%r14, %rdi
	movl	%eax, %r13d
	call	_ZN2v811HandleScopeD1Ev@PLT
	testb	%r13b, %r13b
	je	.L540
	movq	%r12, %rdi
	call	_ZN4node24InitializeContextRuntimeEN2v85LocalINS0_7ContextEEE
.L540:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L547
	addq	$32, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L547:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7760:
	.size	_ZN4node17InitializeContextEN2v85LocalINS0_7ContextEEE, .-_ZN4node17InitializeContextEN2v85LocalINS0_7ContextEEE
	.p2align 4
	.globl	_ZN4node19GetCurrentEventLoopEPN2v87IsolateE
	.type	_ZN4node19GetCurrentEventLoopEPN2v87IsolateE, @function
_ZN4node19GetCurrentEventLoopEPN2v87IsolateE:
.LFB7761:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	pushq	%rbx
	movq	%r13, %rdi
	subq	$40, %rsp
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	testq	%rax, %rax
	je	.L552
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L552
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rdx
	movq	55(%rax), %rax
	cmpq	%rdx, 295(%rax)
	jne	.L552
	movq	271(%rax), %rax
	testq	%rax, %rax
	je	.L552
	movq	360(%rax), %rax
	movq	2360(%rax), %r12
.L550:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L559
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L552:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L550
.L559:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7761:
	.size	_ZN4node19GetCurrentEventLoopEPN2v87IsolateE, .-_ZN4node19GetCurrentEventLoopEPN2v87IsolateE
	.p2align 4
	.globl	_ZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleE
	.type	_ZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleE, @function
_ZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleE:
.LFB7762:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdi, %rdi
	je	.L565
	leaq	2400(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%r13, %rdi
	leaq	2376(%rbx), %r14
	call	uv_mutex_lock@PLT
	cmpq	$0, 2392(%rbx)
	je	.L562
	movl	$80, %edi
	movq	2376(%rbx), %r15
	call	_Znwm@PLT
	movdqu	(%r12), %xmm0
	movq	%r14, %rsi
	movdqu	16(%r12), %xmm1
	movdqu	32(%r12), %xmm2
	movdqu	48(%r12), %xmm3
	movq	%rax, %rdi
	movups	%xmm0, 16(%rax)
	movups	%xmm1, 32(%rax)
	movups	%xmm2, 48(%rax)
	movups	%xmm3, 64(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	2384(%rbx), %rax
	addq	$1, 2392(%rbx)
	addq	$16, %rax
	movq	%rax, 72(%r15)
.L563:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L562:
	.cfi_restore_state
	movl	$80, %edi
	call	_Znwm@PLT
	movdqu	(%r12), %xmm4
	movq	%r14, %rsi
	movdqu	16(%r12), %xmm5
	movdqu	32(%r12), %xmm6
	movdqu	48(%r12), %xmm7
	movq	%rax, %rdi
	movups	%xmm4, 16(%rax)
	movups	%xmm5, 32(%rax)
	movups	%xmm6, 48(%rax)
	movups	%xmm7, 64(%rax)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2392(%rbx)
	jmp	.L563
	.p2align 4,,10
	.p2align 3
.L565:
	leaq	_ZZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7762:
	.size	_ZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleE, .-_ZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleE
	.p2align 4
	.globl	_ZN4node16AddLinkedBindingEPNS_11EnvironmentEPKcPFvN2v85LocalINS4_6ObjectEEENS5_INS4_5ValueEEENS5_INS4_7ContextEEEPvESC_
	.type	_ZN4node16AddLinkedBindingEPNS_11EnvironmentEPKcPFvN2v85LocalINS4_6ObjectEEENS5_INS4_5ValueEEENS5_INS4_7ContextEEEPvESC_, @function
_ZN4node16AddLinkedBindingEPNS_11EnvironmentEPKcPFvN2v85LocalINS4_6ObjectEEENS5_INS4_5ValueEEENS5_INS4_7ContextEEEPvESC_:
.LFB7763:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, -128(%rbp)
	movq	%rcx, -136(%rbp)
	movq	$0, -56(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	testq	%rdi, %rdi
	je	.L571
	leaq	2400(%rdi), %r13
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	%r13, %rdi
	leaq	2376(%rbx), %r14
	call	uv_mutex_lock@PLT
	movq	-128(%rbp), %xmm0
	cmpq	$0, 2392(%rbx)
	movhps	-136(%rbp), %xmm0
	movaps	%xmm0, -128(%rbp)
	je	.L568
	movl	$80, %edi
	movq	2376(%rbx), %r15
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm0
	movq	%r12, -80(%rbp)
	movq	%r14, %rsi
	movq	%rax, %rdi
	movdqa	-96(%rbp), %xmm2
	movabsq	$8589934664, %rax
	movq	%rax, -112(%rbp)
	movdqa	-112(%rbp), %xmm1
	movups	%xmm0, -72(%rbp)
	movdqa	-80(%rbp), %xmm3
	movdqa	-64(%rbp), %xmm4
	movups	%xmm1, 16(%rdi)
	movups	%xmm2, 32(%rdi)
	movups	%xmm3, 48(%rdi)
	movups	%xmm4, 64(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	movq	2384(%rbx), %rax
	addq	$1, 2392(%rbx)
	addq	$16, %rax
	movq	%rax, 72(%r15)
.L569:
	addq	$104, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L568:
	.cfi_restore_state
	movl	$80, %edi
	call	_Znwm@PLT
	movdqa	-128(%rbp), %xmm0
	movq	%r12, -80(%rbp)
	movq	%r14, %rsi
	movq	%rax, %rdi
	movdqa	-96(%rbp), %xmm6
	movabsq	$8589934664, %rax
	movq	%rax, -112(%rbp)
	movdqa	-112(%rbp), %xmm5
	movups	%xmm0, -72(%rbp)
	movdqa	-80(%rbp), %xmm7
	movups	%xmm5, 16(%rdi)
	movdqa	-64(%rbp), %xmm5
	movups	%xmm6, 32(%rdi)
	movups	%xmm7, 48(%rdi)
	movups	%xmm5, 64(%rdi)
	call	_ZNSt8__detail15_List_node_base7_M_hookEPS0_@PLT
	addq	$1, 2392(%rbx)
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	_ZZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7763:
	.size	_ZN4node16AddLinkedBindingEPNS_11EnvironmentEPKcPFvN2v85LocalINS4_6ObjectEEENS5_INS4_5ValueEEENS5_INS4_7ContextEEEPvESC_, .-_ZN4node16AddLinkedBindingEPNS_11EnvironmentEPKcPFvN2v85LocalINS4_6ObjectEEENS5_INS4_5ValueEEENS5_INS4_7ContextEEEPvESC_
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE, @function
_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE:
.LFB9631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	8(%rdi), %rax
	movq	8(%rbx), %rsi
	movq	(%rbx), %r8
	divq	%rsi
	leaq	(%r8,%rdx,8), %r9
	movq	%rdx, %r10
	movq	(%r9), %rax
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%rdx, %rcx
	movq	(%rdx), %rdx
	cmpq	%rdx, %rdi
	jne	.L573
	movq	(%rdi), %r12
	cmpq	%rcx, %rax
	je	.L584
	testq	%r12, %r12
	je	.L576
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L576
	movq	%rcx, (%r8,%rdx,8)
	movq	(%rdi), %r12
.L576:
	movq	%r12, (%rcx)
	call	_ZdlPv@PLT
	movq	%r12, %rax
	subq	$1, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L578
	movq	8(%r12), %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rdx, %r10
	je	.L576
	movq	%rcx, (%r8,%rdx,8)
	movq	(%r9), %rax
.L575:
	leaq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L585
.L577:
	movq	$0, (%r9)
	movq	(%rdi), %r12
	jmp	.L576
.L578:
	movq	%rcx, %rax
	jmp	.L575
.L585:
	movq	%r12, 16(%rbx)
	jmp	.L577
	.cfi_endproc
.LFE9631:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE, .-_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm
	.type	_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm, @function
_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm:
.LFB7723:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L586
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r10
	movq	%rsi, %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	64(%rdi), %r12
	subq	$8, %rsp
	movq	72(%rdi), %r9
	divq	%r9
	movq	64(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r11
	testq	%rax, %rax
	je	.L588
	movq	(%rax), %r8
	movq	8(%r8), %rcx
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L603:
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.L588
	movq	8(%r8), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r9
	cmpq	%rdx, %r11
	jne	.L588
.L590:
	cmpq	%rsi, %rcx
	jne	.L603
	testq	%r10, %r10
	je	.L591
	cmpq	%r10, 16(%r8)
	jne	.L604
.L591:
	addq	$8, %rsp
	movq	%r12, %rdi
	movq	%r8, %rsi
	popq	%r12
	.cfi_restore 12
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
	jmp	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE
	.p2align 4,,10
	.p2align 3
.L586:
	ret
	.p2align 4,,10
	.p2align 3
.L588:
	.cfi_def_cfa 6, 16
	.cfi_offset 6, -16
	.cfi_offset 12, -24
	leaq	_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L604:
	leaq	_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7723:
	.size	_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm, .-_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator4FreeEPvm
	.type	_ZN4node29DebuggingArrayBufferAllocator4FreeEPvm, @function
_ZN4node29DebuggingArrayBufferAllocator4FreeEPvm:
.LFB7719:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	24(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	%r14, %rdi
	call	uv_mutex_lock@PLT
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm
	lock subq	%r13, 16(%rbx)
	movq	%r12, %rdi
	call	free@PLT
	popq	%rbx
	movq	%r14, %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE7719:
	.size	_ZN4node29DebuggingArrayBufferAllocator4FreeEPvm, .-_ZN4node29DebuggingArrayBufferAllocator4FreeEPvm
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator17UnregisterPointerEPvm
	.type	_ZN4node29DebuggingArrayBufferAllocator17UnregisterPointerEPvm, @function
_ZN4node29DebuggingArrayBufferAllocator17UnregisterPointerEPvm:
.LFB7722:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	lock subq	%r13, 16(%r12)
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm
	popq	%r12
	movq	%r15, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE7722:
	.size	_ZN4node29DebuggingArrayBufferAllocator17UnregisterPointerEPvm, .-_ZN4node29DebuggingArrayBufferAllocator17UnregisterPointerEPvm
	.section	.text._ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,"axG",@progbits,_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.type	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, @function
_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm:
.LFB9665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rcx, %r13
	movq	%r8, %rcx
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	addq	$32, %rdi
	subq	$24, %rsp
	movq	%rdx, -56(%rbp)
	movq	-24(%rdi), %rsi
	movq	-8(%rdi), %rdx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	testb	%al, %al
	jne	.L610
	movq	(%rbx), %r15
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L620
.L636:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%rcx), %rax
	movq	%r13, (%rax)
.L621:
	addq	$1, 24(%rbx)
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L610:
	.cfi_restore_state
	movq	%rdx, %r12
	cmpq	$1, %rdx
	je	.L634
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L635
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	48(%rbx), %r9
.L613:
	movq	16(%rbx), %rsi
	movq	$0, 16(%rbx)
	testq	%rsi, %rsi
	je	.L615
	xorl	%edi, %edi
	leaq	16(%rbx), %r8
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L617:
	movq	(%r10), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L618:
	testq	%rsi, %rsi
	je	.L615
.L616:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	8(%rcx), %rax
	divq	%r12
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %r10
	testq	%r10, %r10
	jne	.L617
	movq	16(%rbx), %r10
	movq	%r10, (%rcx)
	movq	%rcx, 16(%rbx)
	movq	%r8, (%rax)
	cmpq	$0, (%rcx)
	je	.L623
	movq	%rcx, (%r15,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L616
	.p2align 4,,10
	.p2align 3
.L615:
	movq	(%rbx), %rdi
	cmpq	%rdi, %r9
	je	.L619
	call	_ZdlPv@PLT
.L619:
	movq	-56(%rbp), %rax
	xorl	%edx, %edx
	movq	%r12, 8(%rbx)
	movq	%r15, (%rbx)
	divq	%r12
	movq	%rdx, %r14
	leaq	(%r15,%r14,8), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.L636
.L620:
	movq	16(%rbx), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 16(%rbx)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L622
	movq	8(%rax), %rax
	xorl	%edx, %edx
	divq	8(%rbx)
	movq	%r13, (%r15,%rdx,8)
.L622:
	leaq	16(%rbx), %rax
	movq	%rax, (%rcx)
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L623:
	movq	%rdx, %rdi
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L634:
	leaq	48(%rbx), %r15
	movq	$0, 48(%rbx)
	movq	%r15, %r9
	jmp	.L613
.L635:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE9665:
	.size	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm, .-_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm
	.type	_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm, @function
_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm:
.LFB7724:
	.cfi_startproc
	endbr64
	testq	%rsi, %rsi
	je	.L666
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	movq	%rdx, %r13
	xorl	%edx, %edx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	72(%rdi), %rsi
	movq	%r12, %rax
	movq	%rdi, %rbx
	divq	%rsi
	movq	64(%rdi), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r14
	testq	%rax, %rax
	je	.L639
	movq	(%rax), %rcx
	xorl	%r10d, %r10d
	movq	8(%rcx), %r8
	movq	%rcx, %rdi
	movq	%r8, %r9
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L640:
	testq	%r10, %r10
	jne	.L642
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L643
.L669:
	movq	8(%rdi), %r9
	xorl	%edx, %edx
	movq	%r9, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L643
.L644:
	cmpq	%r12, %r9
	jne	.L640
	movq	(%rdi), %rdi
	addq	$1, %r10
	testq	%rdi, %rdi
	jne	.L669
	.p2align 4,,10
	.p2align 3
.L643:
	testq	%r10, %r10
	je	.L645
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L670:
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.L639
	movq	8(%rcx), %r8
	xorl	%edx, %edx
	movq	%r8, %rax
	divq	%rsi
	cmpq	%rdx, %r14
	jne	.L639
.L645:
	cmpq	%r12, %r8
	jne	.L670
	movq	%r13, 16(%rcx)
	addq	$16, %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L666:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	ret
	.p2align 4,,10
	.p2align 3
.L639:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -48
	.cfi_offset 6, -16
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	movl	$24, %edi
	call	_Znwm@PLT
	leaq	64(%rbx), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r12, 8(%rax)
	movq	%rax, %rcx
	movl	$1, %r8d
	movq	$0, (%rax)
	movq	$0, 16(%rax)
	call	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE21_M_insert_unique_nodeEmmPNS5_10_Hash_nodeIS3_Lb0EEEm
	movq	%r13, 16(%rax)
	leaq	16(%rax), %rcx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L642:
	.cfi_restore_state
	leaq	_ZZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7724:
	.size	_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm, .-_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator8AllocateEm
	.type	_ZN4node29DebuggingArrayBufferAllocator8AllocateEm, @function
_ZN4node29DebuggingArrayBufferAllocator8AllocateEm:
.LFB7717:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	subq	$16, %rsp
	call	uv_mutex_lock@PLT
	movl	8(%r12), %eax
	testq	%r13, %r13
	movl	$1, %edi
	cmovne	%r13, %rdi
	testl	%eax, %eax
	jne	.L672
	movq	_ZN4node11per_process11cli_optionsE(%rip), %rax
	cmpb	$0, 136(%rax)
	je	.L673
.L672:
	movl	$1, %esi
	call	calloc@PLT
	movq	%rax, %r14
.L674:
	testq	%r14, %r14
	je	.L676
.L675:
	lock addq	%r13, 16(%r12)
.L676:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L673:
	.cfi_restore_state
	movq	%rdi, -40(%rbp)
	call	malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L675
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-40(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %r14
	jmp	.L674
	.cfi_endproc
.LFE7717:
	.size	_ZN4node29DebuggingArrayBufferAllocator8AllocateEm, .-_ZN4node29DebuggingArrayBufferAllocator8AllocateEm
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator21AllocateUninitializedEm
	.type	_ZN4node29DebuggingArrayBufferAllocator21AllocateUninitializedEm, @function
_ZN4node29DebuggingArrayBufferAllocator21AllocateUninitializedEm:
.LFB7718:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	$1, %r15d
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	leaq	24(%rdi), %rbx
	movq	%rbx, %rdi
	subq	$8, %rsp
	call	uv_mutex_lock@PLT
	testq	%r12, %r12
	cmovne	%r12, %r15
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L689
.L682:
	lock addq	%r12, 16(%r13)
.L683:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm
	movq	%rbx, %rdi
	call	uv_mutex_unlock@PLT
	addq	$8, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L689:
	.cfi_restore_state
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r15, %rdi
	call	malloc@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L683
	jmp	.L682
	.cfi_endproc
.LFE7718:
	.size	_ZN4node29DebuggingArrayBufferAllocator21AllocateUninitializedEm, .-_ZN4node29DebuggingArrayBufferAllocator21AllocateUninitializedEm
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmm
	.type	_ZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmm, @function
_ZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmm:
.LFB7720:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -56(%rbp)
	call	uv_mutex_lock@PLT
	testq	%r14, %r14
	je	.L708
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	realloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L709
.L693:
	movq	%r14, %rax
	subq	-56(%rbp), %rax
	lock addq	%rax, 16(%r12)
	testq	%r13, %r13
	je	.L697
	movq	72(%r12), %rdi
	movq	%r13, %rax
	xorl	%edx, %edx
	leaq	64(%r12), %r10
	divq	%rdi
	movq	64(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	testq	%rax, %rax
	je	.L694
	movq	(%rax), %rsi
	movq	8(%rsi), %rcx
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L710:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.L694
	movq	8(%rsi), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rdi
	cmpq	%rdx, %r9
	jne	.L694
.L696:
	cmpq	%rcx, %r13
	jne	.L710
	movq	%r10, %rdi
	call	_ZNSt10_HashtableIPvSt4pairIKS0_mESaIS3_ENSt8__detail10_Select1stESt8equal_toIS0_ESt4hashIS0_ENS5_18_Mod_range_hashingENS5_20_Default_ranged_hashENS5_20_Prime_rehash_policyENS5_17_Hashtable_traitsILb0ELb0ELb1EEEE5eraseENS5_20_Node_const_iteratorIS3_Lb0ELb0EEE
.L697:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm
.L692:
	movq	%r15, %rdi
	call	uv_mutex_unlock@PLT
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L708:
	.cfi_restore_state
	movq	%r13, %rdi
	call	free@PLT
	movq	-56(%rbp), %rdx
	movq	%rdx, %rax
	negq	%rax
	lock addq	%rax, 16(%r12)
	xorl	%ebx, %ebx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvm
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L709:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	realloc@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	jne	.L693
	jmp	.L692
	.p2align 4,,10
	.p2align 3
.L694:
	leaq	_ZZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7720:
	.size	_ZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmm, .-_ZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmm
	.align 2
	.p2align 4
	.globl	_ZN4node29DebuggingArrayBufferAllocator15RegisterPointerEPvm
	.type	_ZN4node29DebuggingArrayBufferAllocator15RegisterPointerEPvm, @function
_ZN4node29DebuggingArrayBufferAllocator15RegisterPointerEPvm:
.LFB7721:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	leaq	24(%rdi), %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r15, %rdi
	call	uv_mutex_lock@PLT
	lock addq	%r13, 16(%r12)
	movq	%r12, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	_ZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvm
	popq	%r12
	movq	%r15, %rdi
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_unlock@PLT
	.cfi_endproc
.LFE7721:
	.size	_ZN4node29DebuggingArrayBufferAllocator15RegisterPointerEPvm, .-_ZN4node29DebuggingArrayBufferAllocator15RegisterPointerEPvm
	.section	.text._ZN4node12NodePlatformD2Ev,"axG",@progbits,_ZN4node12NodePlatformD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node12NodePlatformD2Ev
	.type	_ZN4node12NodePlatformD2Ev, @function
_ZN4node12NodePlatformD2Ev:
.LFB10795:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node12NodePlatformE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	120(%rdi), %r12
	movq	%rdi, %rbx
	movq	%rax, (%rdi)
	testq	%r12, %r12
	je	.L715
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L716
	movl	$-1, %eax
	lock xaddl	%eax, 8(%r12)
	cmpl	$1, %eax
	je	.L742
	.p2align 4,,10
	.p2align 3
.L715:
	movq	64(%rbx), %r12
	testq	%r12, %r12
	je	.L721
	cmpq	$0, _ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip)
	je	.L727
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	jne	.L743
	.p2align 4,,10
	.p2align 3
.L729:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L721
.L722:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L729
.L743:
	lock subl	$1, 8(%r13)
	jne	.L729
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	lock subl	$1, 12(%r13)
	jne	.L729
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	jne	.L722
	.p2align 4,,10
	.p2align 3
.L721:
	movq	56(%rbx), %rax
	movq	48(%rbx), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	48(%rbx), %rdi
	leaq	96(%rbx), %rax
	movq	$0, 72(%rbx)
	movq	$0, 64(%rbx)
	cmpq	%rax, %rdi
	je	.L731
	call	_ZdlPv@PLT
.L731:
	leaq	8(%rbx), %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	uv_mutex_destroy@PLT
	.p2align 4,,10
	.p2align 3
.L726:
	.cfi_restore_state
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*16(%rax)
	movl	12(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r13)
	cmpl	$1, %eax
	jne	.L725
	movq	0(%r13), %rax
	movq	%r13, %rdi
	call	*24(%rax)
	.p2align 4,,10
	.p2align 3
.L725:
	movq	%r14, %rdi
	call	_ZdlPv@PLT
	testq	%r12, %r12
	je	.L721
.L727:
	movq	%r12, %r14
	movq	(%r12), %r12
	movq	24(%r14), %r13
	testq	%r13, %r13
	je	.L725
	movl	8(%r13), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r13)
	cmpl	$1, %eax
	jne	.L725
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L716:
	movl	8(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r12)
	cmpl	$1, %eax
	jne	.L715
.L742:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*16(%rax)
	testq	%r13, %r13
	je	.L719
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r12)
.L720:
	cmpl	$1, %eax
	jne	.L715
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*24(%rax)
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L719:
	movl	12(%r12), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r12)
	jmp	.L720
	.cfi_endproc
.LFE10795:
	.size	_ZN4node12NodePlatformD2Ev, .-_ZN4node12NodePlatformD2Ev
	.weak	_ZN4node12NodePlatformD1Ev
	.set	_ZN4node12NodePlatformD1Ev,_ZN4node12NodePlatformD2Ev
	.weak	_ZTVN4node24NodeArrayBufferAllocatorE
	.section	.data.rel.ro.local._ZTVN4node24NodeArrayBufferAllocatorE,"awG",@progbits,_ZTVN4node24NodeArrayBufferAllocatorE,comdat
	.align 8
	.type	_ZTVN4node24NodeArrayBufferAllocatorE, @object
	.size	_ZTVN4node24NodeArrayBufferAllocatorE, 88
_ZTVN4node24NodeArrayBufferAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN4node24NodeArrayBufferAllocatorD1Ev
	.quad	_ZN4node24NodeArrayBufferAllocatorD0Ev
	.quad	_ZN4node24NodeArrayBufferAllocator8AllocateEm
	.quad	_ZN4node24NodeArrayBufferAllocator21AllocateUninitializedEm
	.quad	_ZN4node24NodeArrayBufferAllocator4FreeEPvm
	.quad	_ZN4node24NodeArrayBufferAllocator7GetImplEv
	.quad	_ZN4node24NodeArrayBufferAllocator10ReallocateEPvmm
	.quad	_ZN4node24NodeArrayBufferAllocator15RegisterPointerEPvm
	.quad	_ZN4node24NodeArrayBufferAllocator17UnregisterPointerEPvm
	.weak	_ZTVN4node29DebuggingArrayBufferAllocatorE
	.section	.data.rel.ro.local._ZTVN4node29DebuggingArrayBufferAllocatorE,"awG",@progbits,_ZTVN4node29DebuggingArrayBufferAllocatorE,comdat
	.align 8
	.type	_ZTVN4node29DebuggingArrayBufferAllocatorE, @object
	.size	_ZTVN4node29DebuggingArrayBufferAllocatorE, 88
_ZTVN4node29DebuggingArrayBufferAllocatorE:
	.quad	0
	.quad	0
	.quad	_ZN4node29DebuggingArrayBufferAllocatorD1Ev
	.quad	_ZN4node29DebuggingArrayBufferAllocatorD0Ev
	.quad	_ZN4node29DebuggingArrayBufferAllocator8AllocateEm
	.quad	_ZN4node29DebuggingArrayBufferAllocator21AllocateUninitializedEm
	.quad	_ZN4node29DebuggingArrayBufferAllocator4FreeEPvm
	.quad	_ZN4node24NodeArrayBufferAllocator7GetImplEv
	.quad	_ZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmm
	.quad	_ZN4node29DebuggingArrayBufferAllocator15RegisterPointerEPvm
	.quad	_ZN4node29DebuggingArrayBufferAllocator17UnregisterPointerEPvm
	.weak	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args
	.section	.rodata.str1.1
.LC23:
	.string	"../src/node_mutex.h:199"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"(0) == (Traits::mutex_init(&mutex_))"
	.align 8
.LC25:
	.string	"node::MutexBase<Traits>::MutexBase() [with Traits = node::LibuvMutexTraits]"
	.section	.data.rel.ro.local._ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,"awG",@progbits,_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args,comdat
	.align 16
	.type	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, @gnu_unique_object
	.size	_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args, 24
_ZZN4node9MutexBaseINS_16LibuvMutexTraitsEEC4EvE4args:
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.section	.rodata.str1.1
.LC26:
	.string	"../src/api/environment.cc:536"
.LC27:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"void node::AddLinkedBinding(node::Environment*, const node::node_module&)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleEE4args, @object
	.size	_ZZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleEE4args, 24
_ZZN4node16AddLinkedBindingEPNS_11EnvironmentERKNS_11node_moduleEE4args:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"internal/per_context/primordials"
	.align 8
.LC30:
	.string	"internal/per_context/domexception"
	.align 8
.LC31:
	.string	"internal/per_context/messageport"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEEE13context_files, @object
	.size	_ZZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEEE13context_files, 32
_ZZN4node21InitializePrimordialsEN2v85LocalINS0_7ContextEEEE13context_files:
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	0
	.section	.rodata.str1.1
.LC32:
	.string	"../src/api/environment.cc:188"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"(allocations_.count(data)) == (0)"
	.align 8
.LC34:
	.string	"void node::DebuggingArrayBufferAllocator::RegisterPointerInternal(void*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvmE4args, @object
	.size	_ZZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvmE4args, 24
_ZZN4node29DebuggingArrayBufferAllocator23RegisterPointerInternalEPvmE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.section	.rodata.str1.1
.LC35:
	.string	"../src/api/environment.cc:180"
.LC36:
	.string	"(it->second) == (size)"
	.section	.rodata.str1.8
	.align 8
.LC37:
	.string	"void node::DebuggingArrayBufferAllocator::UnregisterPointerInternal(void*, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args_0, @object
	.size	_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args_0, 24
_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args_0:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.section	.rodata.str1.1
.LC38:
	.string	"../src/api/environment.cc:176"
.LC39:
	.string	"(it) != (allocations_.end())"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args, @object
	.size	_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args, 24
_ZZN4node29DebuggingArrayBufferAllocator25UnregisterPointerInternalEPvmE4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC37
	.section	.rodata.str1.1
.LC40:
	.string	"../src/api/environment.cc:152"
	.section	.rodata.str1.8
	.align 8
.LC41:
	.string	"virtual void* node::DebuggingArrayBufferAllocator::Reallocate(void*, size_t, size_t)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmmE4args, @object
	.size	_ZZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmmE4args, 24
_ZZN4node29DebuggingArrayBufferAllocator10ReallocateEPvmmE4args:
	.quad	.LC40
	.quad	.LC39
	.quad	.LC41
	.section	.rodata.str1.1
.LC42:
	.string	"../src/api/environment.cc:116"
.LC43:
	.string	"allocations_.empty()"
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"virtual node::DebuggingArrayBufferAllocator::~DebuggingArrayBufferAllocator()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node29DebuggingArrayBufferAllocatorD4EvE4args, @object
	.size	_ZZN4node29DebuggingArrayBufferAllocatorD4EvE4args, 24
_ZZN4node29DebuggingArrayBufferAllocatorD4EvE4args:
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
