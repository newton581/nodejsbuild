	.file	"async_resource.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResourceD2Ev
	.type	_ZN4node13AsyncResourceD2Ev, @function
_ZN4node13AsyncResourceD2Ev:
.LFB7084:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13AsyncResourceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movsd	24(%rdi), %xmm0
	movsd	32(%rbx), %xmm1
	movq	8(%rdi), %rdi
	call	_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE@PLT
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V813DisposeGlobalEPm@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7084:
	.size	_ZN4node13AsyncResourceD2Ev, .-_ZN4node13AsyncResourceD2Ev
	.globl	_ZN4node13AsyncResourceD1Ev
	.set	_ZN4node13AsyncResourceD1Ev,_ZN4node13AsyncResourceD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResourceD0Ev
	.type	_ZN4node13AsyncResourceD0Ev, @function
_ZN4node13AsyncResourceD0Ev:
.LFB7086:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13AsyncResourceE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movsd	24(%rdi), %xmm0
	movsd	32(%r12), %xmm1
	movq	8(%rdi), %rdi
	call	_ZN4node16EmitAsyncDestroyEPNS_11EnvironmentENS_13async_contextE@PLT
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	_ZN2v82V813DisposeGlobalEPm@PLT
.L6:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$40, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7086:
	.size	_ZN4node13AsyncResourceD0Ev, .-_ZN4node13AsyncResourceD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd
	.type	_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd, @function
_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd:
.LFB7081:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$56, %rsp
	movq	%rcx, -88(%rbp)
	movsd	%xmm0, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	16+_ZTVN4node13AsyncResourceE(%rip), %rax
	movq	%rax, (%rdi)
	movq	%rsi, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L27
	leaq	-80(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L17
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L17
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L17
	movq	271(%rax), %r14
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	%r13, %rax
	movq	%r14, 8(%rbx)
	testq	%r13, %r13
	je	.L18
.L13:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	8(%rbx), %r14
.L18:
	movq	%rax, 16(%rbx)
	testq	%r14, %r14
	je	.L14
	movsd	-96(%rbp), %xmm0
	movq	-88(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node13EmitAsyncInitEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKcd@PLT
	movsd	%xmm0, 24(%rbx)
	movsd	%xmm1, 32(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	.cfi_restore_state
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L27:
	movq	$0, 8(%rbx)
	testq	%r13, %r13
	jne	.L13
	movq	$0, 16(%rbx)
.L14:
	leaq	_ZZN4node13AsyncResourceC4EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7081:
	.size	_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd, .-_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd
	.globl	_ZN4node13AsyncResourceC1EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd
	.set	_ZN4node13AsyncResourceC1EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd,_ZN4node13AsyncResourceC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcd
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE
	.type	_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE, @function
_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE:
.LFB7087:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	16(%rbx), %rsi
	movq	352(%rax), %rdi
	testq	%rsi, %rsi
	je	.L30
	movq	(%rsi), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	movq	8(%rbx), %rax
	movq	352(%rax), %rdi
.L30:
	movsd	24(%rbx), %xmm0
	movsd	32(%rbx), %xmm1
	movq	%r14, %r8
	movl	%r13d, %ecx
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	.cfi_endproc
.LFE7087:
	.size	_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE, .-_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResource12MakeCallbackEPKciPN2v85LocalINS3_5ValueEEE
	.type	_ZN4node13AsyncResource12MakeCallbackEPKciPN2v85LocalINS3_5ValueEEE, @function
_ZN4node13AsyncResource12MakeCallbackEPKciPN2v85LocalINS3_5ValueEEE:
.LFB7088:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	16(%rbx), %rsi
	movq	352(%rax), %rdi
	testq	%rsi, %rsi
	je	.L36
	movq	(%rsi), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	movq	8(%rbx), %rax
	movq	352(%rax), %rdi
.L36:
	movsd	24(%rbx), %xmm0
	movsd	32(%rbx), %xmm1
	movq	%r14, %r8
	movl	%r13d, %ecx
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	.cfi_endproc
.LFE7088:
	.size	_ZN4node13AsyncResource12MakeCallbackEPKciPN2v85LocalINS3_5ValueEEE, .-_ZN4node13AsyncResource12MakeCallbackEPKciPN2v85LocalINS3_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_6StringEEEiPNS2_INS1_5ValueEEE
	.type	_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_6StringEEEiPNS2_INS1_5ValueEEE, @function
_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_6StringEEEiPNS2_INS1_5ValueEEE:
.LFB7089:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%edx, %r13d
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	16(%rbx), %rsi
	movq	352(%rax), %rdi
	testq	%rsi, %rsi
	je	.L42
	movq	(%rsi), %rsi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rsi
	movq	8(%rbx), %rax
	movq	352(%rax), %rdi
.L42:
	movsd	24(%rbx), %xmm0
	movsd	32(%rbx), %xmm1
	movq	%r14, %r8
	movl	%r13d, %ecx
	popq	%rbx
	movq	%r12, %rdx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	.cfi_endproc
.LFE7089:
	.size	_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_6StringEEEiPNS2_INS1_5ValueEEE, .-_ZN4node13AsyncResource12MakeCallbackEN2v85LocalINS1_6StringEEEiPNS2_INS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResource12get_resourceEv
	.type	_ZN4node13AsyncResource12get_resourceEv, @function
_ZN4node13AsyncResource12get_resourceEv:
.LFB7090:
	.cfi_startproc
	endbr64
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L53
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	8(%rdi), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore 6
	ret
	.cfi_endproc
.LFE7090:
	.size	_ZN4node13AsyncResource12get_resourceEv, .-_ZN4node13AsyncResource12get_resourceEv
	.align 2
	.p2align 4
	.globl	_ZNK4node13AsyncResource12get_async_idEv
	.type	_ZNK4node13AsyncResource12get_async_idEv, @function
_ZNK4node13AsyncResource12get_async_idEv:
.LFB7091:
	.cfi_startproc
	endbr64
	movsd	24(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE7091:
	.size	_ZNK4node13AsyncResource12get_async_idEv, .-_ZNK4node13AsyncResource12get_async_idEv
	.align 2
	.p2align 4
	.globl	_ZNK4node13AsyncResource20get_trigger_async_idEv
	.type	_ZNK4node13AsyncResource20get_trigger_async_idEv, @function
_ZNK4node13AsyncResource20get_trigger_async_idEv:
.LFB7092:
	.cfi_startproc
	endbr64
	movsd	32(%rdi), %xmm0
	ret
	.cfi_endproc
.LFE7092:
	.size	_ZNK4node13AsyncResource20get_trigger_async_idEv, .-_ZNK4node13AsyncResource20get_trigger_async_idEv
	.align 2
	.p2align 4
	.globl	_ZN4node13AsyncResource13CallbackScopeC2EPS0_
	.type	_ZN4node13AsyncResource13CallbackScopeC2EPS0_, @function
_ZN4node13AsyncResource13CallbackScopeC2EPS0_:
.LFB7094:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	8(%rsi), %rax
	movq	%rsi, %rbx
	movq	16(%rsi), %rdx
	movq	352(%rax), %r8
	testq	%rdx, %rdx
	je	.L59
	movq	(%rdx), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %rdx
	movq	8(%rbx), %rax
	movq	352(%rax), %r8
.L59:
	movsd	24(%rbx), %xmm0
	movsd	32(%rbx), %xmm1
	movq	%r12, %rdi
	movq	%r8, %rsi
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node13CallbackScopeC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE@PLT
	.cfi_endproc
.LFE7094:
	.size	_ZN4node13AsyncResource13CallbackScopeC2EPS0_, .-_ZN4node13AsyncResource13CallbackScopeC2EPS0_
	.globl	_ZN4node13AsyncResource13CallbackScopeC1EPS0_
	.set	_ZN4node13AsyncResource13CallbackScopeC1EPS0_,_ZN4node13AsyncResource13CallbackScopeC2EPS0_
	.weak	_ZTVN4node13AsyncResourceE
	.section	.data.rel.ro.local._ZTVN4node13AsyncResourceE,"awG",@progbits,_ZTVN4node13AsyncResourceE,comdat
	.align 8
	.type	_ZTVN4node13AsyncResourceE, @object
	.size	_ZTVN4node13AsyncResourceE, 32
_ZTVN4node13AsyncResourceE:
	.quad	0
	.quad	0
	.quad	_ZN4node13AsyncResourceD1Ev
	.quad	_ZN4node13AsyncResourceD0Ev
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../src/api/async_resource.cc:20"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"node::AsyncResource::AsyncResource(v8::Isolate*, v8::Local<v8::Object>, const char*, node::async_id)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node13AsyncResourceC4EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcdE4args, @object
	.size	_ZZN4node13AsyncResourceC4EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcdE4args, 24
_ZZN4node13AsyncResourceC4EPN2v87IsolateENS1_5LocalINS1_6ObjectEEEPKcdE4args:
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
