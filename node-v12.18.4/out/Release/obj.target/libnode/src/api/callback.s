	.file	"callback.cc"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node21InternalCallbackScopeC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	.type	_ZN4node21InternalCallbackScopeC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi, @function
_ZN4node21InternalCallbackScopeC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi:
.LFB7534:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -120(%rbp)
	movdqu	(%rcx), %xmm1
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movb	%r8b, 32(%rdi)
	shrl	%r8d
	andl	$1, %r8d
	andb	$1, 32(%rdi)
	movb	%r8b, 33(%rdi)
	xorl	%r8d, %r8d
	movq	%rsi, (%rdi)
	movq	%rdx, 24(%rdi)
	movw	%r8w, 34(%rdi)
	movb	$0, 36(%rdi)
	movups	%xmm1, 8(%rdi)
	testq	%rsi, %rsi
	je	.L26
	addq	$1, 1408(%rsi)
	movq	%rsi, %r14
	movzbl	1930(%rsi), %eax
	testb	%al, %al
	jne	.L27
.L3:
	movb	$1, 34(%r12)
.L1:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L28
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	.cfi_restore_state
	movzbl	2664(%rsi), %eax
	testb	%al, %al
	jne	.L3
	leaq	-112(%rbp), %rax
	movq	352(%rsi), %rsi
	movq	%rcx, %rbx
	movq	%rax, %rdi
	movq	%rax, -144(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	352(%r14), %r15
	movq	%r15, %rdi
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L14
	leaq	-80(%rbp), %r13
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L7
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L7
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L7
	movq	271(%rax), %r15
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	cmpq	%r15, %r14
	jne	.L14
	movsd	16(%r12), %xmm3
	movsd	8(%r12), %xmm2
	movq	%r13, %rdi
	leaq	1144(%r14), %r15
	movq	352(%r14), %rsi
	movsd	%xmm3, -136(%rbp)
	movsd	%xmm2, -128(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	1216(%r14), %rax
	movl	24(%rax), %edx
	testl	%edx, %edx
	jne	.L29
.L9:
	movl	28(%rax), %r8d
	leal	(%r8,%r8), %r11d
	movl	%r8d, %edx
	movq	%r11, %rsi
	cmpq	1160(%r14), %r11
	jnb	.L30
.L11:
	movq	1256(%r14), %rcx
	movq	1176(%r14), %rdi
	addl	$1, %esi
	addl	$1, %edx
	movsd	(%rcx), %xmm0
	movsd	%xmm0, (%rdi,%r11,8)
	movsd	8(%rcx), %xmm0
	movsd	%xmm0, (%rdi,%rsi,8)
	movsd	-128(%rbp), %xmm0
	movq	1272(%r14), %rdi
	movl	%edx, 28(%rax)
	movq	3280(%r14), %rsi
	movl	%r8d, %edx
	movhpd	-136(%rbp), %xmm0
	movups	%xmm0, (%rcx)
	movq	-120(%rbp), %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movsd	(%rbx), %xmm0
	movb	$1, 35(%r12)
	ucomisd	.LC1(%rip), %xmm0
	jp	.L17
	jne	.L17
.L12:
	movq	-144(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	movsd	.LC0(%rip), %xmm0
	movsd	-128(%rbp), %xmm2
	comisd	%xmm0, %xmm2
	jb	.L31
	movsd	-136(%rbp), %xmm4
	comisd	%xmm0, %xmm4
	jnb	.L9
	leaq	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	cmpb	$0, 32(%r12)
	jne	.L12
	movq	%r14, %rdi
	call	_ZN4node9AsyncWrap10EmitBeforeEPNS_11EnvironmentEd@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r15, %rdi
	movq	%r11, -160(%rbp)
	movl	%r11d, -152(%rbp)
	movl	%r8d, -148(%rbp)
	call	_ZN4node10AsyncHooks20grow_async_ids_stackEv@PLT
	movq	1216(%r14), %rax
	movq	-160(%rbp), %r11
	movl	-152(%rbp), %esi
	movl	-148(%rbp), %r8d
	movl	28(%rax), %edx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r13, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L14:
	leaq	_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L28:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7534:
	.size	_ZN4node21InternalCallbackScopeC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi, .-_ZN4node21InternalCallbackScopeC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	.globl	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	.set	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi,_ZN4node21InternalCallbackScopeC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	.align 2
	.p2align 4
	.globl	_ZN4node13CallbackScopeC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE
	.type	_ZN4node13CallbackScopeC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE, @function
_ZN4node13CallbackScopeC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE:
.LFB7525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$72, %rsp
	movq	%xmm0, -96(%rbp)
	movq	%xmm1, -88(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L38
	leaq	-80(%rbp), %r15
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r13, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L36
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L36
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rcx
	movq	55(%rax), %rax
	cmpq	%rcx, 295(%rax)
	jne	.L36
	movq	271(%rax), %rax
	movq	%rax, -104(%rbp)
.L35:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L33:
	movl	$40, %edi
	call	_Znwm@PLT
	movq	-104(%rbp), %rsi
	xorl	%r8d, %r8d
	leaq	-96(%rbp), %rcx
	movq	%rax, %r12
	movq	%rax, %rdi
	movq	%r14, %rdx
	call	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	movq	%r12, (%rbx)
	leaq	8(%rbx), %r12
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN2v88TryCatchC1EPNS_7IsolateE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	_ZN2v88TryCatch10SetVerboseEb@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	.cfi_restore_state
	movq	$0, -104(%rbp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L38:
	movq	$0, -104(%rbp)
	jmp	.L33
.L40:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7525:
	.size	_ZN4node13CallbackScopeC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE, .-_ZN4node13CallbackScopeC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE
	.globl	_ZN4node13CallbackScopeC1EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE
	.set	_ZN4node13CallbackScopeC1EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE,_ZN4node13CallbackScopeC2EPN2v87IsolateENS1_5LocalINS1_6ObjectEEENS_13async_contextE
	.align 2
	.p2align 4
	.globl	_ZN4node21InternalCallbackScopeC2EPNS_9AsyncWrapEi
	.type	_ZN4node21InternalCallbackScopeC2EPNS_9AsyncWrapEi, @function
_ZN4node21InternalCallbackScopeC2EPNS_9AsyncWrapEi:
.LFB7531:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	%edx, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	subq	$48, %rsp
	movupd	40(%rsi), %xmm0
	movq	8(%rbx), %rdx
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	16(%rsi), %rsi
	movaps	%xmm0, -48(%rbp)
	testq	%rdx, %rdx
	je	.L42
	movzbl	11(%rdx), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L48
.L42:
	leaq	-48(%rbp), %rcx
	movq	%r12, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L49
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore_state
	movq	352(%rsi), %rdi
	movq	(%rdx), %rsi
	movl	%r8d, -52(%rbp)
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%rbx), %rsi
	movl	-52(%rbp), %r8d
	movq	%rax, %rdx
	jmp	.L42
.L49:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7531:
	.size	_ZN4node21InternalCallbackScopeC2EPNS_9AsyncWrapEi, .-_ZN4node21InternalCallbackScopeC2EPNS_9AsyncWrapEi
	.globl	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi
	.set	_ZN4node21InternalCallbackScopeC1EPNS_9AsyncWrapEi,_ZN4node21InternalCallbackScopeC2EPNS_9AsyncWrapEi
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"Error: async hook stack has become corrupted (actual: %.f, expected: %.f)\n"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node21InternalCallbackScopeD2Ev
	.type	_ZN4node21InternalCallbackScopeD2Ev, @function
_ZN4node21InternalCallbackScopeD2Ev:
.LFB7537:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 36(%rdi)
	je	.L51
.L138:
	movq	1408(%r12), %rax
.L86:
	subq	$1, %rax
	movq	%rax, 1408(%r12)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L139
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
	movb	$1, 36(%rdi)
	movq	%rdi, %rbx
	movzbl	1930(%r12), %eax
	testb	%al, %al
	jne	.L53
.L137:
	movq	(%rbx), %r12
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L53:
	movzbl	2664(%r12), %eax
	testb	%al, %al
	jne	.L137
	movq	(%rdi), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L140
.L55:
	cmpb	$0, 34(%rbx)
	movq	(%rbx), %r12
	jne	.L58
	movsd	8(%rbx), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	jp	.L95
	je	.L59
.L95:
	cmpb	$0, 32(%rbx)
	je	.L141
.L59:
	cmpb	$0, 35(%rbx)
	jne	.L91
.L93:
	movq	1408(%r12), %rax
.L92:
	cmpq	$1, %rax
	ja	.L86
	cmpb	$0, 33(%rbx)
	jne	.L86
	movzbl	1930(%r12), %eax
	testb	%al, %al
	je	.L137
	movzbl	2664(%r12), %eax
	testb	%al, %al
	jne	.L137
	movq	1360(%r12), %rdx
	movq	(%rbx), %rax
	cmpb	$1, (%rdx)
	je	.L69
	movq	352(%rax), %rdi
	call	_ZN2v815MicrotasksScope17PerformCheckpointEPNS_7IsolateE@PLT
	movq	(%rbx), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L142
.L70:
	movq	(%rbx), %rax
	movq	1216(%rax), %rdx
	movl	20(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L87
.L73:
	movq	1360(%r12), %rdx
	cmpb	$1, (%rdx)
	je	.L77
	cmpb	$1, 1(%rdx)
	je	.L77
.L78:
	movq	(%rbx), %rdi
	call	_ZN4node11Environment17RunWeakRefCleanupEv@PLT
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L58:
	cmpb	$0, 35(%rbx)
	je	.L138
	movq	1216(%r12), %rax
	movsd	8(%rbx), %xmm0
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L138
.L90:
	movl	24(%rax), %esi
	movq	1256(%r12), %rcx
	testl	%esi, %esi
	je	.L63
	movsd	(%rcx), %xmm2
	ucomisd	%xmm0, %xmm2
	jp	.L96
	jne	.L96
.L63:
	movq	1176(%r12), %rsi
	subl	$1, %edx
	leal	(%rdx,%rdx), %r8d
	movsd	(%rsi,%r8,8), %xmm0
	movq	%r8, %rdi
	addl	$1, %edi
	movsd	%xmm0, (%rcx)
	movsd	(%rsi,%rdi,8), %xmm0
	movq	1272(%r12), %rdi
	movq	3280(%r12), %rsi
	movsd	%xmm0, 8(%rcx)
	movl	%edx, 28(%rax)
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEEj@PLT
	movq	(%rbx), %r12
.L62:
	cmpb	$0, 34(%rbx)
	movq	1408(%r12), %rax
	je	.L92
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L140:
	movq	(%rdi), %r12
	movb	$1, 34(%rdi)
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%r12), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%r12), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L56
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 1272(%r12)
.L56:
	testq	%r13, %r13
	je	.L57
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%r12)
.L57:
	movq	1256(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	movq	1216(%r12), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap9EmitAfterEPNS_11EnvironmentEd@PLT
	cmpb	$0, 35(%rbx)
	movq	(%rbx), %r12
	je	.L62
	movq	1216(%r12), %rax
	movsd	8(%rbx), %xmm0
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L62
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	movq	1216(%r12), %rax
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L93
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L77:
	movq	352(%rax), %rsi
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	2968(%rax), %r9
	movzbl	1930(%rax), %edx
	testb	%dl, %dl
	jne	.L143
.L79:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L69:
	movq	1216(%rax), %rdx
	movl	20(%rdx), %edx
	testl	%edx, %edx
	je	.L77
.L87:
	movq	1256(%rax), %rdx
	pxor	%xmm0, %xmm0
	ucomisd	(%rdx), %xmm0
	jp	.L97
	jne	.L97
	ucomisd	8(%rdx), %xmm0
	jp	.L98
	je	.L73
.L98:
	leaq	_ZZN4node21InternalCallbackScope5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L96:
	movq	stderr(%rip), %rdi
	movapd	%xmm0, %xmm1
	movl	$1, %esi
	movapd	%xmm2, %xmm0
	leaq	.LC2(%rip), %rdx
	movl	$2, %eax
	call	__fprintf_chk@PLT
	movq	stderr(%rip), %rdi
	call	_ZN4node13DumpBacktraceEP8_IO_FILE@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movq	1640(%r12), %rax
	cmpb	$0, 8(%rax)
	jne	.L144
	movl	$1, %edi
	call	exit@PLT
.L143:
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L79
	movq	(%rbx), %rax
	movq	3008(%rax), %rdi
	testq	%rdi, %rdi
	je	.L145
	movq	3280(%rax), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	je	.L146
.L82:
	movq	(%rbx), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L147
.L83:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	(%rbx), %rdi
	call	_ZN4node11Environment17RunWeakRefCleanupEv@PLT
	movq	(%rbx), %r12
	movq	1408(%r12), %rax
	jmp	.L86
.L142:
	movq	(%rbx), %r13
	movb	$1, 34(%rbx)
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%r13), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L71
	movq	%rax, -120(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-120(%rbp), %rsi
	movq	$0, 1272(%r13)
.L71:
	testq	%rsi, %rsi
	je	.L72
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%r13)
.L72:
	movq	1256(%r13), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	movq	1216(%r13), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L70
.L147:
	movq	(%rbx), %r13
	movb	$1, 34(%rbx)
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%r13), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L84
	movq	%rax, -120(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-120(%rbp), %rsi
	movq	$0, 1272(%r13)
.L84:
	testq	%rsi, %rsi
	je	.L85
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%r13)
.L85:
	movq	1256(%r13), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	movq	1216(%r13), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L83
.L146:
	movb	$1, 34(%rbx)
	jmp	.L82
.L97:
	leaq	_ZZN4node21InternalCallbackScope5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L145:
	leaq	_ZZN4node21InternalCallbackScope5CloseEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L139:
	call	__stack_chk_fail@PLT
.L144:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	call	fputc@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	call	abort@PLT
	.cfi_endproc
.LFE7537:
	.size	_ZN4node21InternalCallbackScopeD2Ev, .-_ZN4node21InternalCallbackScopeD2Ev
	.globl	_ZN4node21InternalCallbackScopeD1Ev
	.set	_ZN4node21InternalCallbackScopeD1Ev,_ZN4node21InternalCallbackScopeD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node21InternalCallbackScope5CloseEv
	.type	_ZN4node21InternalCallbackScope5CloseEv, @function
_ZN4node21InternalCallbackScope5CloseEv:
.LFB7539:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 36(%rdi)
	je	.L234
.L148:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L235
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	.cfi_restore_state
	movb	$1, 36(%rdi)
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movzbl	1930(%rax), %edx
	testb	%dl, %dl
	je	.L148
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L148
	movq	(%rdi), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L236
.L152:
	cmpb	$0, 34(%rbx)
	jne	.L155
	movsd	8(%rbx), %xmm0
	ucomisd	.LC1(%rip), %xmm0
	movq	(%rbx), %r12
	jp	.L186
	je	.L156
.L186:
	cmpb	$0, 32(%rbx)
	je	.L237
.L156:
	cmpb	$0, 35(%rbx)
	jne	.L183
.L184:
	cmpq	$1, 1408(%r12)
	ja	.L148
	cmpb	$0, 33(%rbx)
	jne	.L148
	movzbl	1930(%r12), %eax
	testb	%al, %al
	je	.L148
	movzbl	2664(%r12), %eax
	testb	%al, %al
	jne	.L148
	movq	1360(%r12), %rdx
	movq	(%rbx), %rax
	cmpb	$1, (%rdx)
	je	.L164
	movq	352(%rax), %rdi
	call	_ZN2v815MicrotasksScope17PerformCheckpointEPNS_7IsolateE@PLT
	movq	(%rbx), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L238
.L165:
	movq	(%rbx), %rax
	movq	1216(%rax), %rdx
	movl	20(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L182
.L168:
	movq	1360(%r12), %rdx
	cmpb	$1, (%rdx)
	je	.L172
	cmpb	$1, 1(%rdx)
	je	.L172
.L173:
	movq	(%rbx), %rdi
	call	_ZN4node11Environment17RunWeakRefCleanupEv@PLT
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L155:
	cmpb	$0, 35(%rbx)
	je	.L148
.L233:
	movsd	8(%rbx), %xmm0
	movq	(%rbx), %r12
.L183:
	movq	1216(%r12), %rax
	movl	28(%rax), %edx
	testl	%edx, %edx
	je	.L163
	movl	24(%rax), %esi
	movq	1256(%r12), %rcx
	testl	%esi, %esi
	je	.L159
	movsd	(%rcx), %xmm2
	ucomisd	%xmm0, %xmm2
	jp	.L187
	jne	.L187
.L159:
	movq	1176(%r12), %rsi
	subl	$1, %edx
	leal	(%rdx,%rdx), %r8d
	movsd	(%rsi,%r8,8), %xmm0
	movq	%r8, %rdi
	addl	$1, %edi
	movsd	%xmm0, (%rcx)
	movsd	(%rsi,%rdi,8), %xmm0
	movq	1272(%r12), %rdi
	movq	3280(%r12), %rsi
	movsd	%xmm0, 8(%rcx)
	movl	%edx, 28(%rax)
	call	_ZN2v86Object6DeleteENS_5LocalINS_7ContextEEEj@PLT
.L163:
	cmpb	$0, 34(%rbx)
	jne	.L148
	movq	(%rbx), %r12
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L236:
	movq	(%rdi), %r12
	movb	$1, 34(%rdi)
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%r12), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rdi
	xorl	%esi, %esi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%r12), %rdi
	movq	%rax, %r13
	testq	%rdi, %rdi
	je	.L153
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 1272(%r12)
.L153:
	testq	%r13, %r13
	je	.L154
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%r12)
.L154:
	movq	1256(%r12), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	movq	1216(%r12), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap9EmitAfterEPNS_11EnvironmentEd@PLT
	cmpb	$0, 35(%rbx)
	je	.L163
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L172:
	movq	352(%rax), %rsi
	leaq	-112(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	(%rbx), %rax
	movq	2968(%rax), %r9
	movzbl	1930(%rax), %edx
	testb	%dl, %dl
	jne	.L239
.L174:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L164:
	movq	1216(%rax), %rdx
	movl	20(%rdx), %edx
	testl	%edx, %edx
	je	.L172
.L182:
	movq	1256(%rax), %rdx
	pxor	%xmm0, %xmm0
	ucomisd	(%rdx), %xmm0
	jp	.L188
	jne	.L188
	ucomisd	8(%rdx), %xmm0
	jp	.L189
	je	.L168
.L189:
	leaq	_ZZN4node21InternalCallbackScope5CloseEvE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L187:
	movq	stderr(%rip), %rdi
	movapd	%xmm0, %xmm1
	movl	$1, %esi
	movapd	%xmm2, %xmm0
	leaq	.LC2(%rip), %rdx
	movl	$2, %eax
	call	__fprintf_chk@PLT
	movq	stderr(%rip), %rdi
	call	_ZN4node13DumpBacktraceEP8_IO_FILE@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	movq	1640(%r12), %rax
	cmpb	$0, 8(%rax)
	jne	.L240
	movl	$1, %edi
	call	exit@PLT
.L239:
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L174
	movq	(%rbx), %rax
	movq	3008(%rax), %rdi
	testq	%rdi, %rdi
	je	.L241
	movq	3280(%rax), %rsi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	testq	%rax, %rax
	je	.L242
.L177:
	movq	(%rbx), %rax
	movzbl	2664(%rax), %eax
	testb	%al, %al
	jne	.L243
.L178:
	movq	%r12, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	(%rbx), %rdi
	call	_ZN4node11Environment17RunWeakRefCleanupEv@PLT
	jmp	.L148
.L238:
	movq	(%rbx), %r13
	movb	$1, 34(%rbx)
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%r13), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L166
	movq	%rax, -120(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-120(%rbp), %rsi
	movq	$0, 1272(%r13)
.L166:
	testq	%rsi, %rsi
	je	.L167
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%r13)
.L167:
	movq	1256(%r13), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	movq	1216(%r13), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L165
.L243:
	movq	(%rbx), %r13
	movb	$1, 34(%rbx)
	leaq	-80(%rbp), %r14
	movq	%r14, %rdi
	movq	352(%r13), %r15
	movq	%r15, %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZN2v85Array3NewEPNS_7IsolateEi@PLT
	movq	1272(%r13), %rdi
	movq	%rax, %rsi
	testq	%rdi, %rdi
	je	.L179
	movq	%rax, -120(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-120(%rbp), %rsi
	movq	$0, 1272(%r13)
.L179:
	testq	%rsi, %rsi
	je	.L180
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 1272(%r13)
.L180:
	movq	1256(%r13), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, %rdi
	movups	%xmm0, (%rax)
	movq	1216(%r13), %rax
	movl	$0, 28(%rax)
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L178
.L242:
	movb	$1, 34(%rbx)
	jmp	.L177
.L188:
	leaq	_ZZN4node21InternalCallbackScope5CloseEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L241:
	leaq	_ZZN4node21InternalCallbackScope5CloseEvE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L235:
	call	__stack_chk_fail@PLT
.L240:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	call	fputc@PLT
	movq	stderr(%rip), %rdi
	call	fflush@PLT
	call	abort@PLT
	.cfi_endproc
.LFE7539:
	.size	_ZN4node21InternalCallbackScope5CloseEv, .-_ZN4node21InternalCallbackScope5CloseEv
	.align 2
	.p2align 4
	.globl	_ZN4node13CallbackScopeD2Ev
	.type	_ZN4node13CallbackScopeD2Ev, @function
_ZN4node13CallbackScopeD2Ev:
.LFB7528:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	leaq	8(%rdi), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	movq	%r13, %rdi
	subq	$8, %rsp
	call	_ZNK2v88TryCatch9HasCaughtEv@PLT
	movq	(%rbx), %r12
	testb	%al, %al
	je	.L245
	movb	$1, 34(%r12)
.L246:
	movq	%r12, %rdi
	call	_ZN4node21InternalCallbackScope5CloseEv
	movq	(%r12), %rax
	movl	$40, %esi
	movq	%r12, %rdi
	subq	$1, 1408(%rax)
	call	_ZdlPvm@PLT
.L247:
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v88TryCatchD1Ev@PLT
	.p2align 4,,10
	.p2align 3
.L245:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L247
	jmp	.L246
	.cfi_endproc
.LFE7528:
	.size	_ZN4node13CallbackScopeD2Ev, .-_ZN4node13CallbackScopeD2Ev
	.globl	_ZN4node13CallbackScopeD1Ev
	.set	_ZN4node13CallbackScopeD1Ev,_ZN4node13CallbackScopeD2Ev
	.p2align 4
	.globl	_ZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextE
	.type	_ZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextE, @function
_ZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextE:
.LFB7542:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rcx, -288(%rbp)
	movl	%r8d, -276(%rbp)
	movq	%xmm0, -272(%rbp)
	movq	%xmm1, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L298
	movq	2704(%rdi), %r15
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	%r9, %r13
	testq	%r15, %r15
	je	.L254
	movq	1216(%rdi), %rax
	leaq	-256(%rbp), %r14
	leaq	-272(%rbp), %rcx
	movl	$1, %r8d
	movl	4(%rax), %edx
	movl	8(%rax), %eax
	movl	%edx, -304(%rbp)
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	%r14, %rdi
	movl	%eax, -296(%rbp)
	call	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	cmpb	$0, -222(%rbp)
	je	.L255
.L277:
	xorl	%r12d, %r12d
.L256:
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScope5CloseEv
	movq	-256(%rbp), %rax
	subq	$1, 1408(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L299
	addq	$280, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	.cfi_restore_state
	movl	-296(%rbp), %eax
	addl	-304(%rbp), %eax
	je	.L257
	movl	-276(%rbp), %eax
	movdqa	.LC3(%rip), %xmm0
	leal	2(%rax), %ecx
	movaps	%xmm0, -208(%rbp)
	pxor	%xmm0, %xmm0
	leaq	-184(%rbp), %rax
	movslq	%ecx, %rcx
	movups	%xmm0, -184(%rbp)
	movq	%rax, -296(%rbp)
	movq	%rax, -192(%rbp)
	movq	$0, -184(%rbp)
	movups	%xmm0, -168(%rbp)
	movups	%xmm0, -152(%rbp)
	movups	%xmm0, -136(%rbp)
	movups	%xmm0, -120(%rbp)
	movups	%xmm0, -104(%rbp)
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	cmpq	$16, %rcx
	jbe	.L259
	movabsq	$2305843009213693951, %rax
	leaq	0(,%rcx,8), %rdi
	andq	%rcx, %rax
	cmpq	%rax, %rcx
	jne	.L300
	movq	%rcx, -304(%rbp)
	testq	%rdi, %rdi
	je	.L261
	movq	%rdi, -312(%rbp)
	call	malloc@PLT
	movq	-304(%rbp), %rcx
	testq	%rax, %rax
	je	.L301
	movq	%rax, -192(%rbp)
	movq	%rcx, -200(%rbp)
.L259:
	movq	352(%rbx), %rdi
	movsd	-272(%rbp), %xmm0
	movq	%rcx, -208(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-208(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L266
	movq	-192(%rbp), %r8
	movq	%rax, (%r8)
	cmpq	$1, %rcx
	jbe	.L266
	movq	-288(%rbp), %rax
	movq	%rax, 8(%r8)
	movl	-276(%rbp), %eax
	testl	%eax, %eax
	jle	.L273
	cmpq	$2, %rcx
	je	.L266
	movl	-276(%rbp), %eax
	leal	-1(%rax), %esi
	movl	$2, %eax
	addq	$2, %rsi
	.p2align 4,,10
	.p2align 3
.L272:
	movq	-16(%r13,%rax,8), %rdx
	movq	%rdx, (%r8,%rax,8)
	cmpq	%rsi, %rax
	je	.L273
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L272
.L266:
	leaq	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	-256(%rbp), %r14
	xorl	%r8d, %r8d
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	leaq	-272(%rbp), %rcx
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScopeC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEi
	cmpb	$0, -222(%rbp)
	jne	.L277
.L257:
	movq	3280(%rbx), %rsi
	movl	-276(%rbp), %ecx
	movq	%r12, %rdx
	movq	%r13, %r8
	movq	-288(%rbp), %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r12
.L275:
	testq	%r12, %r12
	je	.L302
	movq	%r14, %rdi
	call	_ZN4node21InternalCallbackScope5CloseEv
	cmpb	$0, -222(%rbp)
	jne	.L277
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L302:
	movb	$1, -222(%rbp)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L298:
	leaq	_ZZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L273:
	movq	3280(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	-192(%rbp), %rdi
	movq	%rax, %r12
	testq	%rdi, %rdi
	je	.L275
	cmpq	-296(%rbp), %rdi
	je	.L275
	call	free@PLT
	jmp	.L275
.L261:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L301:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-312(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L261
	movq	-304(%rbp), %rcx
	movq	-208(%rbp), %rax
	movq	%rdi, -192(%rbp)
	movq	%rcx, -200(%rbp)
	testq	%rax, %rax
	je	.L259
	movq	-296(%rbp), %rsi
	leaq	0(,%rax,8), %rdx
	movq	%rcx, -304(%rbp)
	call	memcpy@PLT
	movq	-304(%rbp), %rcx
	jmp	.L259
.L300:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L299:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7542:
	.size	_ZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextE, .-_ZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextE
	.p2align 4
	.globl	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	.type	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE, @function
_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE:
.LFB7545:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 3, -56
	movl	%ecx, -52(%rbp)
	movsd	%xmm0, -64(%rbp)
	movsd	%xmm1, -72(%rbp)
	call	_ZN2v86Object15CreationContextEv@PLT
	testq	%rax, %rax
	je	.L305
	movq	%rax, %rdi
	movq	%rax, %r15
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L305
	movq	(%r15), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L305
	movq	271(%rax), %rbx
	testq	%rbx, %rbx
	je	.L305
	movq	3280(%rbx), %r15
	movq	%r15, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movsd	-64(%rbp), %xmm0
	movq	%r14, %r9
	movq	%r12, %rcx
	movsd	-72(%rbp), %xmm1
	movl	-52(%rbp), %r8d
	movq	%r13, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextE
	testq	%rax, %rax
	je	.L312
.L306:
	movq	%rax, %r12
.L307:
	movq	%r15, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	.cfi_restore_state
	leaq	_ZZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L312:
	cmpq	$0, 1408(%rbx)
	jne	.L306
	movq	352(%rbx), %rax
	leaq	88(%rax), %r12
	jmp	.L307
	.cfi_endproc
.LFE7545:
	.size	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE, .-_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	.p2align 4
	.globl	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	.type	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEENS_13async_contextE, @function
_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEENS_13async_contextE:
.LFB7544:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -56(%rbp)
	movsd	%xmm1, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	testq	%rax, %rax
	je	.L320
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L316
.L318:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L316:
	movsd	-56(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	movq	%rbx, %r8
	movl	%r15d, %ecx
	addq	$24, %rsp
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	.cfi_endproc
.LFE7544:
	.size	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEENS_13async_contextE, .-_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	.p2align 4
	.globl	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEENS_13async_contextE
	.type	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEENS_13async_contextE, @function
_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEENS_13async_contextE:
.LFB7543:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%ecx, %r15d
	movl	$-1, %ecx
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdx, %rsi
	xorl	%edx, %edx
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$24, %rsp
	movsd	%xmm0, -56(%rbp)
	movsd	%xmm1, -64(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L328
.L322:
	movq	%r13, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L329
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L325
	xorl	%r12d, %r12d
.L326:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L329:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L325:
	movsd	-56(%rbp), %xmm0
	movsd	-64(%rbp), %xmm1
	movq	%r12, %rdx
	movq	%rbx, %r8
	movl	%r15d, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	movq	%rax, %r12
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L328:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L322
	.cfi_endproc
.LFE7543:
	.size	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEENS_13async_contextE, .-_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEENS_13async_contextE
	.p2align 4
	.globl	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEE
	.type	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEE, @function
_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEE:
.LFB7546:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r15, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L338
.L331:
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L339
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L334
	xorl	%r15d, %r15d
.L333:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L340
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L334:
	xorl	%eax, %eax
	movl	-100(%rbp), %ecx
	movq	%r15, %rdx
	movq	%rbx, %r8
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	movq	%rax, %r15
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L338:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L331
.L340:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7546:
	.size	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEE, .-_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEEPKciPNS3_INS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEE
	.type	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEE, @function
_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEE:
.LFB7550:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	leaq	-96(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	movq	%rdi, %rsi
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%r8, %rbx
	subq	$72, %rsp
	movl	%ecx, -100(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdx
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L348
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L344
	xorl	%r15d, %r15d
.L343:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L349
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L348:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L344:
	xorl	%eax, %eax
	movl	-100(%rbp), %ecx
	movq	%r15, %rdx
	movq	%rbx, %r8
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	movq	%rax, %r15
	jmp	.L343
.L349:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7550:
	.size	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEE, .-_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_6StringEEEiPNS3_INS0_5ValueEEE
	.p2align 4
	.globl	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEE
	.type	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEE, @function
_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEE:
.LFB7551:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movq	%rdi, %rsi
	pushq	%r13
	.cfi_offset 13, -40
	leaq	-96(%rbp), %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r13, %rdi
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$72, %rsp
	movq	%r8, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v820EscapableHandleScopeC1EPNS_7IsolateE@PLT
	xorl	%eax, %eax
	movq	-104(%rbp), %r8
	movl	%ebx, %ecx
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movq	%r15, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	_ZN2v820EscapableHandleScope6EscapeEPm@PLT
	movq	%r13, %rdi
	movq	%rax, %r12
	call	_ZN2v811HandleScopeD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L353
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L353:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7551:
	.size	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEE, .-_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEE
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC4:
	.string	"../src/util-inl.h:374"
.LC5:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC7:
	.string	"../src/util-inl.h:325"
.LC8:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.weak	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args
	.section	.rodata.str1.1
.LC10:
	.string	"../src/util.h:352"
.LC11:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = v8::Local<v8::Value>; long unsigned int kStackStorageSize = 16; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args, 24
_ZZN4node16MaybeStackBufferIN2v85LocalINS1_5ValueEEELm16EEixEmE4args:
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.section	.rodata.str1.1
.LC13:
	.string	"../src/api/callback.cc:245"
.LC14:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"v8::MaybeLocal<v8::Value> node::MakeCallback(v8::Isolate*, v8::Local<v8::Object>, v8::Local<v8::Function>, int, v8::Local<v8::Value>*, node::async_context)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextEE4args, @object
	.size	_ZZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextEE4args, 24
_ZZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextEE4args:
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.section	.rodata.str1.1
.LC16:
	.string	"../src/api/callback.cc:155"
.LC17:
	.string	"!recv.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"v8::MaybeLocal<v8::Value> node::InternalMakeCallback(node::Environment*, v8::Local<v8::Object>, v8::Local<v8::Object>, v8::Local<v8::Function>, int, v8::Local<v8::Value>*, node::async_context)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextEE4args, @object
	.size	_ZZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextEE4args, 24
_ZZN4node20InternalMakeCallbackEPNS_11EnvironmentEN2v85LocalINS2_6ObjectEEES5_NS3_INS2_8FunctionEEEiPNS3_INS2_5ValueEEENS_13async_contextEE4args:
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.section	.rodata.str1.1
.LC19:
	.string	"../src/api/callback.cc:140"
.LC20:
	.string	"!tick_callback.IsEmpty()"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"void node::InternalCallbackScope::Close()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node21InternalCallbackScope5CloseEvE4args_1, @object
	.size	_ZZN4node21InternalCallbackScope5CloseEvE4args_1, 24
_ZZN4node21InternalCallbackScope5CloseEvE4args_1:
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.section	.rodata.str1.1
.LC22:
	.string	"../src/api/callback.cc:124"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"(env_->trigger_async_id()) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node21InternalCallbackScope5CloseEvE4args_0, @object
	.size	_ZZN4node21InternalCallbackScope5CloseEvE4args_0, 24
_ZZN4node21InternalCallbackScope5CloseEvE4args_0:
	.quad	.LC22
	.quad	.LC23
	.quad	.LC21
	.section	.rodata.str1.1
.LC24:
	.string	"../src/api/callback.cc:123"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"(env_->execution_async_id()) == (0)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node21InternalCallbackScope5CloseEvE4args, @object
	.size	_ZZN4node21InternalCallbackScope5CloseEvE4args, 24
_ZZN4node21InternalCallbackScope5CloseEvE4args:
	.quad	.LC24
	.quad	.LC25
	.quad	.LC21
	.section	.rodata.str1.1
.LC26:
	.string	"../src/api/callback.cc:63"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"(Environment::GetCurrent(env->isolate())) == (env)"
	.align 8
.LC28:
	.string	"node::InternalCallbackScope::InternalCallbackScope(node::Environment*, v8::Local<v8::Object>, const node::async_context&, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args_0, @object
	.size	_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args_0, 24
_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args_0:
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.section	.rodata.str1.1
.LC29:
	.string	"../src/api/callback.cc:53"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args, @object
	.size	_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args, 24
_ZZN4node21InternalCallbackScopeC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEERKNS_13async_contextEiE4args:
	.quad	.LC29
	.quad	.LC14
	.quad	.LC28
	.weak	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0
	.section	.rodata.str1.1
.LC30:
	.string	"../src/env-inl.h:133"
.LC31:
	.string	"(trigger_async_id) >= (-1)"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"void node::AsyncHooks::push_async_context(double, double, v8::Local<v8::Value>)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0,"awG",@progbits,_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0, 24
_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args_0:
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.weak	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args
	.section	.rodata.str1.1
.LC33:
	.string	"../src/env-inl.h:132"
.LC34:
	.string	"(async_id) >= (-1)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args,"awG",@progbits,_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args, 24
_ZZN4node10AsyncHooks18push_async_contextEddN2v85LocalINS1_5ValueEEEE4args:
	.quad	.LC33
	.quad	.LC34
	.quad	.LC32
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.align 8
.LC1:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.quad	0
	.quad	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
