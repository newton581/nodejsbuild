	.file	"exceptions.cc"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	", "
.LC1:
	.string	"'"
.LC2:
	.string	" '"
	.text
	.p2align 4
	.globl	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_
	.type	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_, @function
_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_:
.LFB7227:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%esi, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r8, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L3
	leaq	-80(%rbp), %r8
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-88(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L5
	movq	%rax, %rdi
	movq	%r8, -88(%rbp)
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	movq	-88(%rbp), %r8
	cmpl	$35, %eax
	jbe	.L5
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L5
	movq	271(%rax), %rbx
	movq	%r8, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%rbx, %rbx
	je	.L3
	movl	%r15d, %edi
	call	_ZN4node6errors12errno_stringEi@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -88(%rbp)
	testq	%rax, %rax
	je	.L40
.L7:
	testq	%r14, %r14
	je	.L8
	cmpb	$0, (%r14)
	je	.L8
.L9:
	movq	%r14, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L41
.L10:
	xorl	%edx, %edx
	movl	$2, %ecx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L42
.L11:
	movq	-88(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%rax, %r9
	testq	%r13, %r13
	je	.L12
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$-1, %ecx
	movq	%r12, %rdi
	movq	%rax, -104(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L43
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%r12, %rdi
	movq	%r9, -104(%rbp)
	leaq	.LC2(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L44
.L23:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, -104(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-104(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L45
.L14:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%rax, %r9
.L12:
	movq	%r9, %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L46
.L15:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	movq	-88(%rbp), %rcx
	movq	328(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L47
.L16:
	testq	%r13, %r13
	je	.L17
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r14, %rdi
	movq	1320(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L48
.L17:
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L19
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L49
.L20:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r14, %rdi
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L50
.L19:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L51
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	.cfi_restore_state
	movl	%r15d, %edi
	call	strerror@PLT
	movq	%rax, %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r8, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L3:
	leaq	_ZZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L46:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L47:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L40:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L48:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L50:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r9, -112(%rbp)
	movq	%rax, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rdx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L43:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-104(%rbp), %r9
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r9, -112(%rbp)
	movq	%rdx, -104(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-112(%rbp), %r9
	movq	-104(%rbp), %rdx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rax, -88(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-88(%rbp), %rcx
	jmp	.L20
.L51:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7227:
	.size	_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_, .-_ZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_
	.section	.rodata.str1.1
.LC3:
	.string	": "
.LC4:
	.string	" -> '"
	.text
	.p2align 4
	.globl	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_
	.type	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_, @function
_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_:
.LFB7229:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r8, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%r9, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 3, -56
	movl	%esi, -84(%rbp)
	movq	%rdx, -104(%rbp)
	movq	%rcx, -96(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v87Isolate9InContextEv@PLT
	testb	%al, %al
	je	.L54
	leaq	-80(%rbp), %r15
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	%r12, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L56
	movq	%rax, %rdi
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L56
	movq	(%rbx), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L56
	movq	271(%rax), %rbx
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	testq	%rbx, %rbx
	je	.L54
	movq	-96(%rbp), %rax
	testq	%rax, %rax
	je	.L58
	cmpb	$0, (%rax)
	je	.L58
.L59:
	movl	-84(%rbp), %edi
	call	uv_err_name@PLT
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -112(%rbp)
	testq	%rax, %rax
	je	.L98
.L60:
	movq	-104(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, -104(%rbp)
	testq	%rax, %rax
	je	.L99
.L61:
	xorl	%edx, %edx
	movl	$2, %ecx
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L100
.L62:
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	-96(%rbp), %rsi
	xorl	%edx, %edx
	movl	$-1, %ecx
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L101
.L63:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	%r12, %rdi
	leaq	.LC0(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L102
.L64:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	-104(%rbp), %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L65
	movq	%r14, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L103
.L66:
	xorl	%edx, %edx
	movl	$2, %ecx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L104
.L67:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%r14, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L105
.L68:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%rax, %r15
.L65:
	testq	%r13, %r13
	je	.L69
	movq	%r13, %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L106
.L70:
	xorl	%edx, %edx
	movl	$5, %ecx
	leaq	.LC4(%rip), %rsi
	movq	%r12, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L107
.L71:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%r13, %rdx
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	xorl	%edx, %edx
	movl	$1, %ecx
	movq	%r12, %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rax, %r15
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L108
.L72:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v86String6ConcatEPNS_7IsolateENS_5LocalIS0_EES4_@PLT
	movq	%rax, %r15
.L69:
	movq	%r15, %rdi
	call	_ZN2v89Exception5ErrorENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	%r15, %rdi
	movq	%rax, %rsi
	call	_ZNK2v85Value8ToObjectENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L109
.L73:
	movl	-84(%rbp), %esi
	movq	%r12, %rdi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	640(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L110
.L74:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	-112(%rbp), %rcx
	movq	328(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L111
.L75:
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r15, %rdi
	movq	-104(%rbp), %rcx
	movq	1704(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L112
.L76:
	testq	%r14, %r14
	je	.L77
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r14, %rcx
	movq	%r15, %rdi
	movq	1320(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L113
.L77:
	testq	%r13, %r13
	je	.L79
	movq	360(%rbx), %rax
	movq	3280(%rbx), %rsi
	movq	%r13, %rcx
	movq	%r15, %rdi
	movq	448(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L114
.L79:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L115
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	.cfi_restore_state
	movl	-84(%rbp), %edi
	call	uv_strerror@PLT
	movq	%rax, -96(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
.L54:
	leaq	_ZZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L99:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rax, -120(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-120(%rbp), %rdx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rdx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rdx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L109:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L110:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L111:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L112:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L98:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L113:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L114:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rdx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L103:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rdx
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L106:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rdx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rax, -96(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-96(%rbp), %rdx
	jmp	.L72
.L115:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7229:
	.size	_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_, .-_ZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_
	.p2align 4
	.globl	_ZN4node14FatalExceptionEPN2v87IsolateERKNS0_8TryCatchE
	.type	_ZN4node14FatalExceptionEPN2v87IsolateERKNS0_8TryCatchE, @function
_ZN4node14FatalExceptionEPN2v87IsolateERKNS0_8TryCatchE:
.LFB7230:
	.cfi_startproc
	endbr64
	jmp	_ZN4node6errors24TriggerUncaughtExceptionEPN2v87IsolateERKNS1_8TryCatchE@PLT
	.cfi_endproc
.LFE7230:
	.size	_ZN4node14FatalExceptionEPN2v87IsolateERKNS0_8TryCatchE, .-_ZN4node14FatalExceptionEPN2v87IsolateERKNS0_8TryCatchE
	.section	.rodata.str1.1
.LC5:
	.string	"../src/api/exceptions.cc:101"
.LC6:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"v8::Local<v8::Value> node::UVException(v8::Isolate*, int, const char*, const char*, const char*, const char*)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_E4args, @object
	.size	_ZZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_E4args, 24
_ZZN4node11UVExceptionEPN2v87IsolateEiPKcS4_S4_S4_E4args:
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.section	.rodata.str1.1
.LC8:
	.string	"../src/api/exceptions.cc:29"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"v8::Local<v8::Value> node::ErrnoException(v8::Isolate*, int, const char*, const char*, const char*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_E4args, @object
	.size	_ZZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_E4args, 24
_ZZN4node14ErrnoExceptionEPN2v87IsolateEiPKcS4_S4_E4args:
	.quad	.LC8
	.quad	.LC6
	.quad	.LC9
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
