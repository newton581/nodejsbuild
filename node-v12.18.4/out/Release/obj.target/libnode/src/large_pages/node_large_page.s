	.file	"node_large_page.cc"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"/sys/kernel/mm/transparent_hugepage/enabled"
	.align 8
.LC1:
	.string	"could not open /sys/kernel/mm/transparent_hugepage/enabled"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"Hugepages WARNING: %s\n"
.LC3:
	.string	"[always]"
.LC4:
	.string	"[madvise]"
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_129IsTransparentHugePagesEnabledEv, @function
_ZN4node12_GLOBAL__N_129IsTransparentHugePagesEnabledEv:
.LFB6181:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-320(%rbp), %r15
	pushq	%r13
	movq	%r15, %rdi
	.cfi_offset 13, -40
	leaq	-560(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	leaq	-576(%rbp), %rbx
	subq	$632, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZNSt8ios_baseC2Ev@PLT
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	xorl	%esi, %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -320(%rbp)
	xorl	%eax, %eax
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rcx
	movw	%ax, -96(%rbp)
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movups	%xmm0, -88(%rbp)
	movups	%xmm0, -72(%rbp)
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	$0, -104(%rbp)
	movq	%rcx, -576(%rbp,%rax)
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	$0, -568(%rbp)
	movq	-24(%rax), %rdi
	addq	%rbx, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEEC1Ev@PLT
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_E@PLT
	movq	%r13, %rdi
	movl	$8, %edx
	leaq	.LC0(%rip), %rsi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE4openEPKcSt13_Ios_Openmode@PLT
	movq	%rbx, %rdi
	testq	%rax, %rax
	movq	-576(%rbp), %rax
	je	.L22
	addq	-24(%rax), %rdi
	xorl	%esi, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	testb	$5, -288(%rbp)
	jne	.L23
.L4:
	leaq	-624(%rbp), %rax
	movq	$0, -632(%rbp)
	leaq	-640(%rbp), %r12
	movq	%rax, -664(%rbp)
	movq	%rax, -640(%rbp)
	leaq	-592(%rbp), %rax
	movq	%rax, -656(%rbp)
	movq	%rax, -608(%rbp)
	leaq	-456(%rbp), %rax
	movq	%rax, %rdi
	movb	$0, -624(%rbp)
	movq	$0, -600(%rbp)
	movb	$0, -592(%rbp)
	movq	%rax, -648(%rbp)
	call	_ZNKSt12__basic_fileIcE7is_openEv@PLT
	testb	%al, %al
	jne	.L24
	movq	%r13, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	testq	%rax, %rax
	je	.L25
.L8:
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %r14d
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	jne	.L26
.L9:
	movq	-608(%rbp), %rdi
	cmpq	-656(%rbp), %rdi
	je	.L10
	call	_ZdlPv@PLT
.L10:
	movq	-640(%rbp), %rdi
	cmpq	-664(%rbp), %rdi
	je	.L5
	call	_ZdlPv@PLT
.L5:
	leaq	24+_ZTVSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	addq	$40, %rax
	movq	%rax, -320(%rbp)
	leaq	16+_ZTVSt13basic_filebufIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -560(%rbp)
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	movq	-648(%rbp), %rdi
	call	_ZNSt12__basic_fileIcED1Ev@PLT
	leaq	16+_ZTVSt15basic_streambufIcSt11char_traitsIcEE(%rip), %rax
	leaq	-504(%rbp), %rdi
	movq	%rax, -560(%rbp)
	call	_ZNSt6localeD1Ev@PLT
	movq	8+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rax
	movq	16+_ZTTSt14basic_ifstreamIcSt11char_traitsIcEE(%rip), %rcx
	movq	%r15, %rdi
	movq	%rax, -576(%rbp)
	movq	-24(%rax), %rax
	movq	%rcx, -576(%rbp,%rax)
	leaq	16+_ZTVSt9basic_iosIcSt11char_traitsIcEE(%rip), %rax
	movq	%rax, -320(%rbp)
	movq	$0, -568(%rbp)
	call	_ZNSt8ios_baseD2Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L27
	addq	$632, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	.cfi_restore_state
	leaq	-608(%rbp), %r14
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE@PLT
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	_ZStrsIcSt11char_traitsIcESaIcEERSt13basic_istreamIT_T0_ES7_RNSt7__cxx1112basic_stringIS4_S5_T1_EE@PLT
	movq	(%rax), %rdx
	movq	-24(%rdx), %rdx
	testb	$5, 32(%rax,%rdx)
	je	.L7
	movq	%r13, %rdi
	call	_ZNSt13basic_filebufIcSt11char_traitsIcEE5closeEv@PLT
	testq	%rax, %rax
	jne	.L8
.L25:
	movq	-576(%rbp), %rax
	addq	-24(%rax), %rbx
	movl	32(%rbx), %esi
	movq	%rbx, %rdi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L22:
	addq	-24(%rax), %rdi
	movl	32(%rdi), %esi
	orl	$4, %esi
	call	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate@PLT
	testb	$5, -288(%rbp)
	je	.L4
.L23:
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	xorl	%eax, %eax
	xorl	%r14d, %r14d
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rdx
	call	__fprintf_chk@PLT
	leaq	-456(%rbp), %rax
	movq	%rax, -648(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	-608(%rbp), %rdi
	leaq	.LC4(%rip), %rsi
	call	_ZNKSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE7compareEPKc@PLT
	testl	%eax, %eax
	sete	%r14b
	jmp	.L9
.L27:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6181:
	.size	_ZN4node12_GLOBAL__N_129IsTransparentHugePagesEnabledEv, .-_ZN4node12_GLOBAL__N_129IsTransparentHugePagesEnabledEv
	.section	lpstub,"ax",@progbits
	.align 2097152
	.type	_ZN4node26MoveTextRegionToLargePagesERKNS_12_GLOBAL__N_111text_regionE.isra.0, @function
_ZN4node26MoveTextRegionToLargePagesERKNS_12_GLOBAL__N_111text_regionE.isra.0:
.LFB7535:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%r9d, %r9d
	movl	$-1, %r8d
	movl	$34, %ecx
	movl	$3, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	(%rdi), %r15
	xorl	%edi, %edi
	subq	%r15, %rsi
	movq	%rsi, %r12
	call	mmap64@PLT
	cmpq	$-1, %rax
	je	.L29
	movq	(%r14), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r13
	call	memcpy@PLT
	xorl	%r9d, %r9d
	movl	$50, %ecx
	movq	%r12, %rsi
	movl	$-1, %r8d
	movl	$7, %edx
	movq	%r15, %rdi
	call	mmap64@PLT
	movq	%rax, %r14
	cmpq	$-1, %rax
	je	.L30
	movl	$14, %edx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	madvise@PLT
	cmpl	$-1, %eax
	je	.L31
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	memcpy@PLT
	movl	$5, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	mprotect@PLT
	cmpl	$-1, %eax
	je	.L31
	xorl	%r14d, %r14d
.L32:
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	munmap@PLT
	testl	%eax, %eax
	jne	.L54
.L28:
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L30:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
.L53:
	call	strerror@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC2(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
.L34:
	movl	$-1, %r14d
	cmpq	$-1, %r13
	je	.L28
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L54:
	call	__errno_location@PLT
	movl	(%rax), %edi
	call	strerror@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC2(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	popq	%r12
	movl	%r14d, %eax
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	.cfi_restore_state
	call	__errno_location@PLT
	movl	(%rax), %edi
	movq	%rax, %r15
	call	strerror@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC2(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	testq	%r14, %r14
	je	.L34
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	munmap@PLT
	testl	%eax, %eax
	je	.L34
	movl	(%r15), %edi
	jmp	.L53
.L29:
	call	__errno_location@PLT
	movl	$-1, %r14d
	movl	(%rax), %edi
	call	strerror@PLT
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	leaq	.LC2(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fprintf_chk@PLT
	jmp	.L28
	.cfi_endproc
.LFE7535:
	.size	_ZN4node26MoveTextRegionToLargePagesERKNS_12_GLOBAL__N_111text_regionE.isra.0, .-_ZN4node26MoveTextRegionToLargePagesERKNS_12_GLOBAL__N_111text_regionE.isra.0
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"basic_string::_M_construct null not valid"
	.section	.text.unlikely,"ax",@progbits
	.align 2
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0:
.LFB7553:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	leaq	16(%rdi), %rax
	movq	%rax, (%rdi)
	testq	%rsi, %rsi
	je	.L56
	movq	%rdi, %r12
	orq	$-1, %rcx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	repnz scasb
	movq	%rsi, %r13
	notq	%rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, -48(%rbp)
	cmpq	$15, %rbx
	jbe	.L57
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L57:
	movq	(%r12), %rax
	cmpq	$1, %rbx
	jne	.L58
	movb	0(%r13), %dl
	movb	%dl, (%rax)
	jmp	.L59
.L58:
	testq	%rbx, %rbx
	je	.L59
	movq	%rax, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rcx
	rep movsb
.L59:
	movq	-48(%rbp), %rax
	movq	(%r12), %rdx
	movq	%rax, 8(%r12)
	movb	$0, (%rdx,%rax)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L60
	call	__stack_chk_fail@PLT
.L56:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L60:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7553:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	.section	.rodata._ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0.str1.1,"aMS",@progbits,1
.LC6:
	.string	"0123456789abcdef"
	.section	.text.unlikely._ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0,"axG",@progbits,_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.type	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, @function
_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0:
.LFB7569:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	leaq	-25(%rbp), %r8
	subq	$40, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movb	$0, -25(%rbp)
	leaq	.LC6(%rip), %rax
.L67:
	movq	%rsi, %rdx
	decq	%r8
	andl	$15, %edx
	shrq	$4, %rsi
	movb	(%rax,%rdx), %dl
	movb	%dl, (%r8)
	jne	.L67
	movq	%r8, %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L68
	call	__stack_chk_fail@PLT
.L68:
	addq	$40, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7569:
	.size	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0, .-_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	.text
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_111FindMappingEP12dl_phdr_infomPv, @function
_ZN4node12_GLOBAL__N_111FindMappingEP12dl_phdr_infomPv:
.LFB6170:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$88, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	8(%rdi), %r15
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	leaq	-96(%rbp), %rax
	movq	%r14, -96(%rbp)
	movq	%rax, -120(%rbp)
	testq	%r15, %r15
	je	.L106
	movq	%rdi, %rbx
	movq	%r15, %rdi
	movq	%rdx, %r12
	call	strlen@PLT
	movq	%rax, -104(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L107
	cmpq	$1, %rax
	jne	.L75
	movzbl	(%r15), %edx
	movb	%dl, -80(%rbp)
	movq	%r14, %rdx
.L76:
	movq	%rax, -88(%rbp)
	movb	$0, (%rdx,%rax)
	movq	32(%r12), %rdx
	movq	-96(%rbp), %r13
	cmpq	-88(%rbp), %rdx
	je	.L108
.L79:
	cmpq	%r14, %r13
	je	.L104
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	xorl	%eax, %eax
.L71:
	movq	-56(%rbp), %rbx
	xorq	%fs:40, %rbx
	jne	.L109
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	testq	%rax, %rax
	jne	.L110
	movq	%r14, %rdx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%eax, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L108:
	testq	%rdx, %rdx
	je	.L78
	movq	24(%r12), %rdi
	movq	%r13, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L79
.L78:
	cmpq	%r14, %r13
	je	.L80
	movq	%r13, %rdi
	call	_ZdlPv@PLT
.L80:
	movzwl	24(%rbx), %edx
	testw	%dx, %dx
	je	.L104
	leal	-1(%rdx), %ecx
	movq	16(%rbx), %rax
	movabsq	$8589934591, %rsi
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	movabsq	$4294967297, %rcx
	leaq	56(%rax,%rdx,8), %rdi
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$56, %rax
	cmpq	%rdi, %rax
	je	.L104
.L83:
	movq	(%rax), %rdx
	andq	%rsi, %rdx
	cmpq	%rcx, %rdx
	jne	.L82
	movq	40(%rax), %r9
	movq	16(%r12), %r8
	movq	16(%rax), %rdx
	addq	(%rbx), %rdx
	addq	%rdx, %r9
	cmpq	%r8, %rdx
	ja	.L82
	cmpq	%r8, %r9
	jb	.L82
	movq	%rdx, %xmm0
	movq	%r9, %xmm1
	movl	$1, %eax
	punpcklqdq	%xmm1, %xmm0
	movups	%xmm0, (%r12)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-120(%rbp), %rdi
	leaq	-104(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -96(%rbp)
	movq	%rax, %rdi
	movq	-104(%rbp), %rax
	movq	%rax, -80(%rbp)
.L74:
	movq	%r13, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L109:
	call	__stack_chk_fail@PLT
.L110:
	movq	%r14, %rdi
	jmp	.L74
	.cfi_endproc
.LFE6170:
	.size	_ZN4node12_GLOBAL__N_111FindMappingEP12dl_phdr_infomPv, .-_ZN4node12_GLOBAL__N_111FindMappingEP12dl_phdr_infomPv
	.section	.text._ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,"axG",@progbits,_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE,comdat
	.p2align 4
	.weak	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.type	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, @function
_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE:
.LFB4107:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16(%rdi), %rax
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rsi, %rbx
	movq	8(%rsi), %rsi
	movq	%rax, (%rdi)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc@PLT
	cmpq	$0, 8(%rbx)
	je	.L111
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L115:
	movq	(%rbx), %rcx
	movq	(%r12), %rsi
	movzbl	(%rcx,%rdx), %ecx
	addq	%rdx, %rsi
	leal	-97(%rcx), %r8d
	cmpb	$25, %r8b
	ja	.L113
	subl	$32, %ecx
	addq	$1, %rdx
	movb	%cl, (%rsi)
	cmpq	%rdx, 8(%rbx)
	ja	.L115
.L111:
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	.cfi_restore_state
	movb	%cl, (%rsi)
	addq	$1, %rdx
	cmpq	8(%rbx), %rdx
	jb	.L115
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4107:
	.size	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE, .-_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	.section	.text._ZN4node11SPrintFImplB5cxx11EPKc,"axG",@progbits,_ZN4node11SPrintFImplB5cxx11EPKc,comdat
	.p2align 4
	.weak	_ZN4node11SPrintFImplB5cxx11EPKc
	.type	_ZN4node11SPrintFImplB5cxx11EPKc, @function
_ZN4node11SPrintFImplB5cxx11EPKc:
.LFB6032:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	movl	$37, %esi
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	movq	%r14, %rdi
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L118
	leaq	16(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, (%r12)
	call	strlen@PLT
	movq	%rax, -136(%rbp)
	movq	%rax, %r13
	cmpq	$15, %rax
	ja	.L147
	cmpq	$1, %rax
	jne	.L121
	movzbl	(%r14), %edx
	movb	%dl, 16(%r12)
.L122:
	movq	%rax, 8(%r12)
	movb	$0, (%r15,%rax)
.L117:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L148
	addq	$120, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	.cfi_restore_state
	testq	%rax, %rax
	je	.L122
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L147:
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %r15
	movq	-136(%rbp), %rax
	movq	%rax, 16(%r12)
.L120:
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-136(%rbp), %rax
	movq	(%r12), %r15
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L118:
	cmpb	$37, 1(%rax)
	jne	.L149
	leaq	-96(%rbp), %r15
	leaq	2(%rax), %rsi
	movq	%rax, -152(%rbp)
	movq	%r15, %rdi
	leaq	-112(%rbp), %rbx
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	-152(%rbp), %rax
	movq	%rbx, -128(%rbp)
	leaq	-128(%rbp), %r9
	leaq	1(%rax), %r13
	subq	%r14, %r13
	movq	%r13, -136(%rbp)
	cmpq	$15, %r13
	ja	.L150
	cmpq	$1, %r13
	jne	.L127
	movzbl	(%r14), %eax
	movb	%al, -112(%rbp)
	movq	%rbx, %rax
.L128:
	movq	%r13, -120(%rbp)
	movb	$0, (%rax,%r13)
	movq	-128(%rbp), %r10
	movl	$15, %eax
	leaq	-80(%rbp), %r13
	movq	-120(%rbp), %r8
	movq	-88(%rbp), %rdx
	movq	%rax, %rdi
	cmpq	%rbx, %r10
	cmovne	-112(%rbp), %rdi
	movq	-96(%rbp), %rsi
	leaq	(%r8,%rdx), %rcx
	cmpq	%rdi, %rcx
	jbe	.L130
	cmpq	%r13, %rsi
	cmovne	-80(%rbp), %rax
	cmpq	%rax, %rcx
	jbe	.L151
.L130:
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L132:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L152
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L134:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	-128(%rbp), %rdi
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	cmpq	%rbx, %rdi
	je	.L135
	call	_ZdlPv@PLT
.L135:
	movq	-96(%rbp), %rdi
	cmpq	%r13, %rdi
	je	.L117
	call	_ZdlPv@PLT
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L127:
	testq	%r13, %r13
	jne	.L153
	movq	%rbx, %rax
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L150:
	movq	%r9, %rdi
	leaq	-136(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -152(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-152(%rbp), %r9
	movq	%rax, -128(%rbp)
	movq	%rax, %rdi
	movq	-136(%rbp), %rax
	movq	%rax, -112(%rbp)
.L126:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r9, -152(%rbp)
	call	memcpy@PLT
	movq	-136(%rbp), %r13
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r9
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L152:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%r10, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L132
.L149:
	leaq	_ZZN4node11SPrintFImplB5cxx11EPKcE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L148:
	call	__stack_chk_fail@PLT
.L153:
	movq	%rbx, %rdi
	jmp	.L126
	.cfi_endproc
.LFE6032:
	.size	_ZN4node11SPrintFImplB5cxx11EPKc, .-_ZN4node11SPrintFImplB5cxx11EPKc
	.section	.rodata.str1.1
.LC7:
	.string	"Unknown error"
.LC8:
	.string	"OK"
.LC9:
	.string	"failed to find text region"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"Mapping code to large pages failed. Reverting to default page size."
	.section	.rodata.str1.1
.LC11:
	.string	"Large pages are not enabled."
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"Mapping to large pages is not supported."
	.text
	.p2align 4
	.globl	_ZN4node15LargePagesErrorEi
	.type	_ZN4node15LargePagesErrorEi, @function
_ZN4node15LargePagesErrorEi:
.LFB6194:
	.cfi_startproc
	endbr64
	leaq	.LC9(%rip), %rax
	cmpl	$2, %edi
	je	.L154
	jle	.L165
	leaq	.LC11(%rip), %rax
	cmpl	$13, %edi
	je	.L154
	cmpl	$95, %edi
	leaq	.LC7(%rip), %rax
	leaq	.LC12(%rip), %rdx
	cmove	%rdx, %rax
.L154:
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	.LC10(%rip), %rax
	cmpl	$-1, %edi
	je	.L154
	testl	%edi, %edi
	leaq	.LC8(%rip), %rdx
	leaq	.LC7(%rip), %rax
	cmove	%rdx, %rax
	ret
	.cfi_endproc
.LFE6194:
	.size	_ZN4node15LargePagesErrorEi, .-_ZN4node15LargePagesErrorEi
	.section	.text._ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,"axG",@progbits,_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z,comdat
	.p2align 4
	.weak	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.type	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, @function
_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z:
.LFB6268:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r10
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	subq	$232, %rsp
	movq	%r8, -176(%rbp)
	movq	%r9, -168(%rbp)
	testb	%al, %al
	je	.L167
	movaps	%xmm0, -160(%rbp)
	movaps	%xmm1, -144(%rbp)
	movaps	%xmm2, -128(%rbp)
	movaps	%xmm3, -112(%rbp)
	movaps	%xmm4, -96(%rbp)
	movaps	%xmm5, -80(%rbp)
	movaps	%xmm6, -64(%rbp)
	movaps	%xmm7, -48(%rbp)
.L167:
	movq	%fs:40, %rax
	movq	%rax, -216(%rbp)
	xorl	%eax, %eax
	leaq	23(%rsi), %rax
	movq	%rsp, %rdi
	movq	%rax, %rcx
	andq	$-4096, %rax
	subq	%rax, %rdi
	andq	$-16, %rcx
	movq	%rdi, %rax
	cmpq	%rax, %rsp
	je	.L169
.L183:
	subq	$4096, %rsp
	orq	$0, 4088(%rsp)
	cmpq	%rax, %rsp
	jne	.L183
.L169:
	andl	$4095, %ecx
	subq	%rcx, %rsp
	testq	%rcx, %rcx
	jne	.L184
.L170:
	leaq	16(%rbp), %rax
	leaq	15(%rsp), %r14
	movl	$32, -240(%rbp)
	movq	%rax, -232(%rbp)
	andq	$-16, %r14
	leaq	-208(%rbp), %rax
	leaq	-240(%rbp), %rcx
	movq	%r14, %rdi
	movq	%rax, -224(%rbp)
	movl	$48, -236(%rbp)
	call	*%r10
	leaq	16(%r12), %rdi
	movslq	%eax, %r13
	movq	%rdi, (%r12)
	movq	%r13, -248(%rbp)
	cmpq	$15, %r13
	ja	.L185
	cmpq	$1, %r13
	jne	.L173
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L174:
	movq	%r13, 8(%r12)
	movb	$0, (%rdi,%r13)
	movq	-216(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L186
	leaq	-24(%rbp), %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L173:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L174
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%r12, %rdi
	leaq	-248(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-248(%rbp), %rax
	movq	%rax, 16(%r12)
.L172:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-248(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L184:
	orq	$0, -8(%rsp,%rcx)
	jmp	.L170
.L186:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6268:
	.size	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z, .-_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_:
.LFB6590:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdx, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rsi, %rdi
	addq	$16, %rsi
	subq	$8, %rsp
	movq	-8(%rsi), %r8
	movq	8(%rdx), %rdx
	movq	-16(%rsi), %rcx
	leaq	(%r8,%rdx), %rax
	cmpq	%rsi, %rcx
	je	.L194
	movq	16(%rdi), %r10
.L188:
	movq	(%r9), %rsi
	cmpq	%r10, %rax
	jbe	.L189
	leaq	16(%r9), %r10
	cmpq	%r10, %rsi
	je	.L195
	movq	16(%r9), %r10
.L190:
	cmpq	%r10, %rax
	jbe	.L197
.L189:
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
.L191:
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L198
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L193:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	.cfi_restore_state
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L198:
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$15, %r10d
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L195:
	movl	$15, %r10d
	jmp	.L190
	.cfi_endproc
.LFE6590:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	.section	.text._ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,"axG",@progbits,_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.type	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, @function
_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag:
.LFB7223:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L200
	testq	%rsi, %rsi
	je	.L216
.L200:
	subq	%r13, %r12
	movq	%r12, -48(%rbp)
	cmpq	$15, %r12
	ja	.L217
	movq	(%rbx), %rdi
	cmpq	$1, %r12
	jne	.L203
	movzbl	0(%r13), %eax
	movb	%al, (%rdi)
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
.L204:
	movq	%r12, 8(%rbx)
	movb	$0, (%rdi,%r12)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L218
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	.cfi_restore_state
	testq	%r12, %r12
	je	.L204
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%rbx, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%rbx)
.L202:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r12
	movq	(%rbx), %rdi
	jmp	.L204
.L216:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L218:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7223:
	.size	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag, .-_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_:
.LFB7305:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rdi, %r12
	addq	$16, %rdi
	pushq	%rbx
	.cfi_offset 3, -48
	movl	%edx, %ebx
	subq	$16, %rsp
	movq	8(%rsi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%rdi, (%r12)
	movq	(%rsi), %r14
	movq	%r14, %rax
	addq	%r13, %rax
	je	.L220
	testq	%r14, %r14
	je	.L236
.L220:
	movq	%r13, -48(%rbp)
	cmpq	$15, %r13
	ja	.L237
	cmpq	$1, %r13
	jne	.L223
	movzbl	(%r14), %eax
	movb	%al, 16(%r12)
.L224:
	movq	%r13, 8(%r12)
	xorl	%edx, %edx
	movsbl	%bl, %r8d
	movl	$1, %ecx
	movb	$0, (%rdi,%r13)
	movq	8(%r12), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE14_M_replace_auxEmmmc@PLT
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L238
	addq	$16, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L223:
	.cfi_restore_state
	testq	%r13, %r13
	je	.L224
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L237:
	movq	%r12, %rdi
	leaq	-48(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, (%r12)
	movq	%rax, %rdi
	movq	-48(%rbp), %rax
	movq	%rax, 16(%r12)
.L222:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-48(%rbp), %r13
	movq	(%r12), %rdi
	jmp	.L224
.L236:
	leaq	.LC5(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
.L238:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7305:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	.section	.text._ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,"axG",@progbits,_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_,comdat
	.p2align 4
	.weak	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.type	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, @function
_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_:
.LFB7311:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%rdx, %rdi
	xorl	%edx, %edx
	subq	$8, %rsp
	movq	(%rsi), %rcx
	movq	8(%rsi), %r8
	xorl	%esi, %esi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	leaq	16(%r12), %rdx
	movq	%rdx, (%r12)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L243
	movq	%rcx, (%r12)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%r12)
.L241:
	movq	8(%rax), %rcx
	movq	%rdx, (%rax)
	movq	$0, 8(%rax)
	movq	%rcx, 8(%r12)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	.cfi_restore_state
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L241
	.cfi_endproc
.LFE7311:
	.size	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_, .-_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	.section	.rodata._ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC13:
	.string	"true"
.LC14:
	.string	"false"
.LC15:
	.string	"lz"
.LC16:
	.string	"%p"
	.section	.text.unlikely._ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB7411:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L245
	leaq	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L245:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC15(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-184(%rbp), %r9
.L246:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L246
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L247
	cmpb	$99, %dl
	jg	.L248
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L249
	cmpb	$88, %dl
	je	.L250
	jmp	.L247
.L248:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L247
	leaq	.L252(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L252:
	.long	.L251-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L251-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L251-.L252
	.long	.L254-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L251-.L252
	.long	.L247-.L252
	.long	.L251-.L252
	.long	.L247-.L252
	.long	.L247-.L252
	.long	.L251-.L252
	.section	.text.unlikely._ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L249:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L289
	jmp	.L287
.L247:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L287
.L289:
	call	_ZdlPv@PLT
	jmp	.L287
.L251:
	cmpq	$0, (%rbx)
	leaq	.LC14(%rip), %rax
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L282
	jmp	.L263
.L250:
	cmpq	$0, (%rbx)
	leaq	.LC14(%rip), %rax
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	leaq	.LC13(%rip), %rsi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L269
	call	_ZdlPv@PLT
.L269:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L263
.L282:
	call	_ZdlPv@PLT
	jmp	.L263
.L254:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC16(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L271
	leaq	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L271:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L263:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L287:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L258
	call	_ZdlPv@PLT
.L258:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L244
	call	_ZdlPv@PLT
.L244:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L274
	call	__stack_chk_fail@PLT
.L274:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7411:
	.size	_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB7313:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L291
	call	__stack_chk_fail@PLT
.L291:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7313:
	.size	_ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_:
.LFB7204:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJPvEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L293
	call	_ZdlPv@PLT
.L293:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L295
	call	__stack_chk_fail@PLT
.L295:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7204:
	.size	_ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_
	.section	.rodata._ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC17:
	.string	"%lu"
	.section	.text.unlikely._ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB7413:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rsi, %r15
	movl	$37, %esi
	pushq	%r14
	pushq	%r13
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	movq	%r15, %rdi
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L298
	leaq	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L298:
	movq	%rax, %r9
	leaq	-176(%rbp), %r12
	leaq	-160(%rbp), %rax
	movq	%r15, %rsi
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC15(%rip), %r15
	movq	%r9, -192(%rbp)
	movq	%rax, -184(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-192(%rbp), %r9
.L299:
	movq	%r9, %rbx
	movq	%r15, %rdi
	leaq	1(%r9), %r9
	movq	%r8, -208(%rbp)
	movsbl	1(%rbx), %esi
	movq	%r9, -200(%rbp)
	movb	%sil, -192(%rbp)
	call	strchr@PLT
	movb	-192(%rbp), %dl
	movq	-200(%rbp), %r9
	testq	%rax, %rax
	movq	-208(%rbp), %r8
	jne	.L299
	cmpb	$120, %dl
	jg	.L300
	cmpb	$99, %dl
	jg	.L301
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-112(%rbp), %r15
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	leaq	-144(%rbp), %r14
	movq	%rax, -200(%rbp)
	je	.L302
	cmpb	$88, %dl
	je	.L303
	jmp	.L300
.L301:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L300
	leaq	.L305(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L305:
	.long	.L306-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L306-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L308-.L305
	.long	.L307-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L306-.L305
	.long	.L300-.L305
	.long	.L306-.L305
	.long	.L300-.L305
	.long	.L300-.L305
	.long	.L304-.L305
	.section	.text.unlikely._ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L302:
	movq	%r8, %rdx
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L309
	call	_ZdlPv@PLT
.L309:
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L329
	jmp	.L311
.L300:
	leaq	-112(%rbp), %r14
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r14, %rdi
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L334
	call	_ZdlPv@PLT
	jmp	.L334
.L306:
	movq	(%r8), %r8
	movq	vsnprintf@GOTPCREL(%rip), %rsi
	leaq	-112(%rbp), %rdi
	xorl	%eax, %eax
	leaq	.LC17(%rip), %rcx
	movl	$32, %edx
	call	_ZN9__gnu_cxx12__to_xstringINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEcEET_PFiPT0_mPKS8_P13__va_list_tagEmSB_z
	jmp	.L331
.L308:
	movb	$0, -57(%rbp)
	movq	(%r8), %rax
	leaq	-57(%rbp), %rsi
.L316:
	movl	%eax, %edx
	decq	%rsi
	andl	$7, %edx
	addl	$48, %edx
	shrq	$3, %rax
	movb	%dl, (%rsi)
	jne	.L316
	leaq	-112(%rbp), %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	jmp	.L331
.L304:
	movq	(%r8), %rsi
	leaq	-112(%rbp), %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
.L331:
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L328
	jmp	.L315
.L303:
	movq	(%r8), %rsi
	movq	%r14, %rdi
	call	_ZN4node14ToStringHelper11BaseConvertILj4EmLi0EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEERKT0_.isra.0
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L319
	call	_ZdlPv@PLT
.L319:
	movq	-144(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L315
.L328:
	call	_ZdlPv@PLT
	jmp	.L315
.L307:
	leaq	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L315:
	leaq	-112(%rbp), %r15
	leaq	2(%rbx), %rsi
	movq	%r15, %rdi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L334:
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L311
.L329:
	call	_ZdlPv@PLT
.L311:
	movq	-176(%rbp), %rdi
	cmpq	-184(%rbp), %rdi
	je	.L297
	call	_ZdlPv@PLT
.L297:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L323
	call	__stack_chk_fail@PLT
.L323:
	addq	$168, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7413:
	.size	_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB7315:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L336
	call	__stack_chk_fail@PLT
.L336:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7315:
	.size	_ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_:
.LFB7206:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJmEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L338
	call	_ZdlPv@PLT
.L338:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L340
	call	__stack_chk_fail@PLT
.L340:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7206:
	.size	_ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely._ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB7459:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L343
	leaq	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L343:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC15(%rip), %rbx
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %r9
.L344:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -216(%rbp)
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	movq	-216(%rbp), %r8
	jne	.L344
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L345
	cmpb	$99, %dl
	jg	.L346
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L347
	cmpb	$88, %dl
	je	.L348
	jmp	.L345
.L346:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L345
	leaq	.L350(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L350:
	.long	.L349-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L349-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L349-.L350
	.long	.L352-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L349-.L350
	.long	.L345-.L350
	.long	.L349-.L350
	.long	.L345-.L350
	.long	.L345-.L350
	.long	.L349-.L350
	.section	.text.unlikely._ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L347:
	movq	-184(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L385
	jmp	.L357
.L345:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	leaq	-144(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L357
.L385:
	call	_ZdlPv@PLT
.L357:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L381
	jmp	.L356
.L349:
	cmpq	$0, (%r8)
	leaq	.LC14(%rip), %rax
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L380
	jmp	.L361
.L348:
	cmpq	$0, (%r8)
	leaq	.LC14(%rip), %rax
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	leaq	.LC13(%rip), %rsi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L367
	call	_ZdlPv@PLT
.L367:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L361
.L380:
	call	_ZdlPv@PLT
	jmp	.L361
.L352:
	movq	(%r8), %r9
	leaq	-80(%rbp), %rbx
	xorl	%eax, %eax
	leaq	.LC16(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%rbx, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L369
	leaq	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L369:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L361:
	movq	-184(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L356
.L381:
	call	_ZdlPv@PLT
.L356:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L342
	call	_ZdlPv@PLT
.L342:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L372
	call	__stack_chk_fail@PLT
.L372:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7459:
	.size	_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB7410:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%rcx, -192(%rbp)
	movq	%r8, -200(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r9
	testq	%rax, %rax
	jne	.L387
	leaq	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L387:
	movq	%rax, %r10
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r10, %rdx
	movq	%r13, %rdi
	movq	%r9, -216(%rbp)
	leaq	.LC15(%rip), %rbx
	movq	%r10, -184(%rbp)
	movq	%rax, -208(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-216(%rbp), %r9
	movq	-184(%rbp), %r10
	movq	%r13, -216(%rbp)
	movq	%r10, %r15
	movq	%r9, %r12
.L388:
	movq	%r15, %rax
	movq	%rbx, %rdi
	movq	%r15, -184(%rbp)
	leaq	1(%r15), %r15
	movsbl	1(%rax), %esi
	movl	%esi, %r13d
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L388
	movl	%r13d, %edx
	movq	%r12, %r9
	movq	-216(%rbp), %r13
	movq	%r15, %r10
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L389
	cmpb	$99, %dl
	jg	.L390
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r15
	movq	%rax, -216(%rbp)
	je	.L391
	cmpb	$88, %dl
	je	.L392
	jmp	.L389
.L390:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L389
	leaq	.L394(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L394:
	.long	.L393-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L393-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L393-.L394
	.long	.L396-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L393-.L394
	.long	.L389-.L394
	.long	.L393-.L394
	.long	.L389-.L394
	.long	.L389-.L394
	.long	.L393-.L394
	.section	.text.unlikely._ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L391:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	jne	.L429
	jmp	.L401
.L389:
	movq	-200(%rbp), %r8
	movq	%r9, %rdx
	movq	%r10, %rsi
	movq	%r12, %rdi
	movq	-192(%rbp), %rcx
	leaq	-144(%rbp), %r15
	call	_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L401
.L429:
	call	_ZdlPv@PLT
.L401:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L425
	jmp	.L400
.L393:
	cmpq	$0, (%r9)
	leaq	.LC14(%rip), %rax
	leaq	.LC13(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L424
	jmp	.L405
.L392:
	cmpq	$0, (%r9)
	leaq	.LC14(%rip), %rax
	leaq	.LC13(%rip), %rsi
	movq	%r15, %rdi
	cmove	%rax, %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L411
	call	_ZdlPv@PLT
.L411:
	movq	-144(%rbp), %rdi
	cmpq	-216(%rbp), %rdi
	je	.L405
.L424:
	call	_ZdlPv@PLT
	jmp	.L405
.L396:
	leaq	-80(%rbp), %r15
	movq	(%r9), %r9
	xorl	%eax, %eax
	leaq	.LC16(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r15, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L413
	leaq	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L413:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L405:
	movq	-184(%rbp), %rsi
	movq	-200(%rbp), %rcx
	movq	%r12, %rdi
	movq	-192(%rbp), %rdx
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L400
.L425:
	call	_ZdlPv@PLT
.L400:
	movq	-176(%rbp), %rdi
	cmpq	-208(%rbp), %rdi
	je	.L386
	call	_ZdlPv@PLT
.L386:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L416
	call	__stack_chk_fail@PLT
.L416:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7410:
	.size	_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB7312:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L431
	call	__stack_chk_fail@PLT
.L431:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7312:
	.size	_ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_:
.LFB7203:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJPvS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L433
	call	_ZdlPv@PLT
.L433:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L435
	call	__stack_chk_fail@PLT
.L435:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7203:
	.size	_ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_
	.section	.rodata._ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_.str1.1,"aMS",@progbits,1
.LC18:
	.string	"(null)"
	.section	.text.unlikely._ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB7463:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rdi, %r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$168, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L438
	leaq	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L438:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	movq	%r12, %rsi
	leaq	-160(%rbp), %rax
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	leaq	.LC15(%rip), %r12
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-184(%rbp), %r9
.L439:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%r12, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	jne	.L439
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %r14
	jg	.L440
	cmpb	$99, %dl
	jg	.L441
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L442
	cmpb	$88, %dl
	je	.L443
	jmp	.L440
.L441:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L440
	leaq	.L445(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L445:
	.long	.L444-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L444-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L444-.L445
	.long	.L447-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L444-.L445
	.long	.L440-.L445
	.long	.L444-.L445
	.long	.L440-.L445
	.long	.L440-.L445
	.long	.L444-.L445
	.section	.text.unlikely._ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L442:
	movq	-184(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L479
	jmp	.L476
.L440:
	movq	%r9, %rsi
	movq	%rbx, %rdx
	leaq	-144(%rbp), %rbx
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L476
.L479:
	call	_ZdlPv@PLT
	jmp	.L476
.L444:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L459
	leaq	.LC18(%rip), %rsi
.L459:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	jne	.L471
	jmp	.L456
.L443:
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L461
	leaq	.LC18(%rip), %rsi
.L461:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L462
	call	_ZdlPv@PLT
.L462:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L456
.L471:
	call	_ZdlPv@PLT
	jmp	.L456
.L447:
	leaq	-80(%rbp), %r10
	movq	(%rbx), %r9
	xorl	%eax, %eax
	leaq	.LC16(%rip), %r8
	movq	%r10, %rdi
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%r10, -200(%rbp)
	call	__snprintf_chk@PLT
	movq	-200(%rbp), %r10
	testl	%eax, %eax
	jns	.L464
	leaq	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L464:
	movq	%r10, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L456:
	movq	-184(%rbp), %rsi
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplB5cxx11EPKc
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
.L476:
	movq	-112(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L451
	call	_ZdlPv@PLT
.L451:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L437
	call	_ZdlPv@PLT
.L437:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L467
	call	__stack_chk_fail@PLT
.L467:
	addq	$168, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7463:
	.size	_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.weak	_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.type	_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, @function
_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_:
.LFB7412:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movq	%rcx, %r15
	pushq	%r14
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	movl	$37, %esi
	pushq	%rbx
	movq	%r12, %rdi
	subq	$184, %rsp
	.cfi_offset 3, -56
	movq	%rdx, -184(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	strchr@PLT
	movq	-184(%rbp), %r8
	testq	%rax, %rax
	jne	.L481
	leaq	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L481:
	movq	%rax, %r9
	leaq	-176(%rbp), %r13
	leaq	-160(%rbp), %rax
	movq	%r12, %rsi
	movq	%r9, %rdx
	movq	%r13, %rdi
	movq	%r8, -200(%rbp)
	leaq	.LC15(%rip), %rbx
	movq	%r9, -184(%rbp)
	movq	%rax, -192(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructIPKcEEvT_S8_St20forward_iterator_tag
	movq	-200(%rbp), %r8
	movq	-184(%rbp), %r9
.L482:
	movq	%r9, %rax
	movq	%r9, -184(%rbp)
	movq	%rbx, %rdi
	leaq	1(%r9), %r9
	movsbl	1(%rax), %esi
	movq	%r8, -216(%rbp)
	movq	%r9, -208(%rbp)
	movb	%sil, -200(%rbp)
	call	strchr@PLT
	movb	-200(%rbp), %dl
	movq	-208(%rbp), %r9
	testq	%rax, %rax
	movq	-216(%rbp), %r8
	jne	.L482
	cmpb	$120, %dl
	leaq	-112(%rbp), %r12
	leaq	-96(%rbp), %rbx
	jg	.L483
	cmpb	$99, %dl
	jg	.L484
	leaq	-128(%rbp), %rax
	cmpb	$37, %dl
	leaq	-144(%rbp), %r10
	movq	%rax, -200(%rbp)
	je	.L485
	cmpb	$88, %dl
	je	.L486
	jmp	.L483
.L484:
	subl	$100, %edx
	cmpb	$20, %dl
	ja	.L483
	leaq	.L488(%rip), %rcx
	movzbl	%dl, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	notrack jmp	*%rax
	.section	.rodata._ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"aG",@progbits,_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
	.align 4
	.align 4
.L488:
	.long	.L487-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L487-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L487-.L488
	.long	.L490-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L487-.L488
	.long	.L483-.L488
	.long	.L487-.L488
	.long	.L483-.L488
	.long	.L483-.L488
	.long	.L487-.L488
	.section	.text.unlikely._ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,"axG",@progbits,_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_,comdat
.L485:
	movq	-184(%rbp), %rsi
	movq	%r8, %rdx
	movq	%r15, %rcx
	movq	%r12, %rdi
	movq	%r10, -208(%rbp)
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-208(%rbp), %r10
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r10, %rdi
	movq	%r10, -184(%rbp)
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	-184(%rbp), %r10
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r10, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	jne	.L519
	jmp	.L495
.L483:
	movq	%r15, %rcx
	movq	%r8, %rdx
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	leaq	-144(%rbp), %r15
	movl	$37, %edx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_S5_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EEOS8_S9_
	movq	-144(%rbp), %rdi
	leaq	-128(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L495
.L519:
	call	_ZdlPv@PLT
.L495:
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L515
	jmp	.L494
.L487:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L502
	leaq	.LC18(%rip), %rsi
.L502:
	movq	%r12, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	jne	.L514
	jmp	.L499
.L486:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L504
	leaq	.LC18(%rip), %rsi
.L504:
	movq	%r10, %rdi
	movq	%r10, -208(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC2EPKcRKS3_.isra.0
	movq	-208(%rbp), %r10
	movq	%r12, %rdi
	movq	%r10, %rsi
	call	_ZN4node7ToUpperERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE
	movq	-104(%rbp), %rdx
	movq	-112(%rbp), %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	-112(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L505
	call	_ZdlPv@PLT
.L505:
	movq	-144(%rbp), %rdi
	cmpq	-200(%rbp), %rdi
	je	.L499
.L514:
	call	_ZdlPv@PLT
	jmp	.L499
.L490:
	movq	(%r8), %r9
	leaq	-80(%rbp), %rbx
	xorl	%eax, %eax
	leaq	.LC16(%rip), %r8
	movl	$20, %ecx
	movl	$1, %edx
	movl	$20, %esi
	movq	%rbx, %rdi
	call	__snprintf_chk@PLT
	testl	%eax, %eax
	jns	.L507
	leaq	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L507:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE6appendEPKc@PLT
.L499:
	movq	-184(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	addq	$2, %rsi
	call	_ZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	_ZStplIcSt11char_traitsIcESaIcEENSt7__cxx1112basic_stringIT_T0_T1_EERKS8_OS8_
	movq	-112(%rbp), %rdi
	leaq	-96(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L494
.L515:
	call	_ZdlPv@PLT
.L494:
	movq	-176(%rbp), %rdi
	cmpq	-192(%rbp), %rdi
	je	.L480
	call	_ZdlPv@PLT
.L480:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L510
	call	__stack_chk_fail@PLT
.L510:
	addq	$184, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7412:
	.size	_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_, .-_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	.section	.text.unlikely._ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,"axG",@progbits,_ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_,comdat
	.weak	_ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.type	_ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, @function
_ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_:
.LFB7314:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L522
	call	__stack_chk_fail@PLT
.L522:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7314:
	.size	_ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_, .-_ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	.section	.text.unlikely._ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_,"axG",@progbits,_ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_,comdat
	.weak	_ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_
	.type	_ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_, @function
_ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_:
.LFB7205:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	leaq	-64(%rbp), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	subq	$48, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node7SPrintFIJRPcS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcDpOT_
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	_ZN4node6FWriteEP8_IO_FILERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEE@PLT
	movq	-64(%rbp), %rdi
	leaq	-48(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L524
	call	_ZdlPv@PLT
.L524:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L526
	call	__stack_chk_fail@PLT
.L526:
	addq	$48, %rsp
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7205:
	.size	_ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_, .-_ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_
	.section	.text.unlikely
.LCOLDB24:
	.text
.LHOTB24:
	.p2align 4
	.type	_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv, @function
_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv:
.LFB6171:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pxor	%xmm0, %xmm0
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	leaq	-112(%rbp), %rsi
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$232, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	__node_text_start@GOTPCREL(%rip), %rax
	movb	$0, 16(%rdi)
	movups	%xmm0, (%rdi)
	leaq	_ZN4node12_GLOBAL__N_111FindMappingEP12dl_phdr_infomPv(%rip), %rdi
	movq	%rax, -96(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -256(%rbp)
	movq	%rax, -88(%rbp)
	movq	$0, -80(%rbp)
	movb	$0, -72(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	dl_iterate_phdr@PLT
	cmpl	$1, %eax
	je	.L529
	leaq	-160(%rbp), %rax
	leaq	-208(%rbp), %r11
	movq	%rax, -248(%rbp)
	leaq	-192(%rbp), %rbx
	leaq	-128(%rbp), %r14
	leaq	-216(%rbp), %r13
	leaq	-176(%rbp), %r15
.L544:
	xorl	%eax, %eax
.L530:
	movq	%r11, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rax, -224(%rbp)
	movq	%rbx, -208(%rbp)
	movq	$20, -216(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	movdqa	.LC23(%rip), %xmm0
	movq	%rax, -208(%rbp)
	movq	%rdx, -192(%rbp)
	movups	%xmm0, (%rax)
	movl	$175334759, 16(%rax)
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rax, -200(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-248(%rbp), %rax
	xorl	%edx, %edx
	movq	$16, -216(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movdqa	.LC20(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	movq	%rdx, -160(%rbp)
	movups	%xmm0, (%rax)
	movq	-216(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r14, -144(%rbp)
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L567
	movq	%rcx, -144(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -128(%rbp)
.L553:
	movq	8(%rax), %rcx
	movq	%rcx, -136(%rbp)
	movb	$0, 16(%rax)
	cmpb	$0, 47+_ZN4node11per_process18enabled_debug_listE(%rip)
	movq	%rdx, (%rax)
	movq	-144(%rbp), %rdi
	movq	$0, 8(%rax)
	jne	.L565
.L554:
	cmpq	%r14, %rdi
	je	.L555
	call	_ZdlPv@PLT
.L555:
	movq	-176(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L556
	call	_ZdlPv@PLT
.L556:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L557
	call	_ZdlPv@PLT
.L557:
	movq	-88(%rbp), %rdi
	cmpq	-256(%rbp), %rdi
	je	.L528
	call	_ZdlPv@PLT
.L528:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L568
	addq	$232, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L567:
	.cfi_restore_state
	movdqu	16(%rax), %xmm1
	movaps	%xmm1, -128(%rbp)
	jmp	.L553
	.p2align 4,,10
	.p2align 3
.L529:
	movq	-104(%rbp), %rax
	leaq	-208(%rbp), %r11
	xorl	%edx, %edx
	leaq	-128(%rbp), %r14
	leaq	-216(%rbp), %r13
	movq	%r11, %rdi
	leaq	-192(%rbp), %rbx
	movq	%r11, -264(%rbp)
	movq	%rax, -224(%rbp)
	movq	-96(%rbp), %rax
	movq	%r13, %rsi
	leaq	-176(%rbp), %r15
	movq	%rbx, -208(%rbp)
	movq	%rax, -232(%rbp)
	movq	-112(%rbp), %rax
	movq	$30, -216(%rbp)
	movq	%rax, -240(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movdqa	.LC19(%rip), %xmm0
	movq	%r15, %rdi
	movabsq	$7954799695009686816, %rsi
	movq	%rax, -208(%rbp)
	movq	%rdx, -192(%rbp)
	movl	$2672, %edx
	movq	%rsi, 16(%rax)
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	movl	$622869092, 24(%rax)
	movw	%dx, 28(%rax)
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rax, -200(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-160(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -248(%rbp)
	movq	%rax, -176(%rbp)
	movq	$16, -216(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movdqa	.LC20(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	movq	%rdx, -160(%rbp)
	movups	%xmm0, (%rax)
	movq	-216(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r14, -144(%rbp)
	movq	-264(%rbp), %r11
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L569
	movq	%rcx, -144(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -128(%rbp)
.L532:
	movq	8(%rax), %rcx
	movq	%rcx, -136(%rbp)
	movb	$0, 16(%rax)
	cmpb	$0, 47+_ZN4node11per_process18enabled_debug_listE(%rip)
	movq	%rdx, (%rax)
	movq	-144(%rbp), %rdi
	movq	$0, 8(%rax)
	jne	.L562
.L533:
	cmpq	%r14, %rdi
	je	.L534
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
.L534:
	movq	-176(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L535
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
.L535:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L536
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
.L536:
	movq	-96(%rbp), %rax
	leaq	__start_lpstub(%rip), %rcx
	movq	-104(%rbp), %rdx
	movq	%rax, -112(%rbp)
	cmpq	%rcx, %rax
	jnb	.L537
	cmpq	%rdx, %rcx
	jbe	.L570
.L537:
	cmpq	%rdx, %rax
	jnb	.L544
	andq	$-2097152, %rdx
	addq	$2097151, %rax
	movq	%r11, %rdi
	movq	%r13, %rsi
	andq	$-2097152, %rax
	movq	%rdx, -224(%rbp)
	xorl	%edx, %edx
	movq	%r11, -264(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rbx, -208(%rbp)
	movq	$25, -216(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movdqa	.LC22(%rip), %xmm0
	movq	%r15, %rdi
	movabsq	$8080900484576060704, %rsi
	movq	%rax, -208(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rsi, 16(%rax)
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	movb	$10, 24(%rax)
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rax, -200(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-248(%rbp), %rax
	xorl	%edx, %edx
	movq	$16, -216(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movdqa	.LC20(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	movq	%rdx, -160(%rbp)
	movups	%xmm0, (%rax)
	movq	-216(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r14, -144(%rbp)
	movq	-264(%rbp), %r11
	movq	(%rax), %rcx
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	je	.L571
	movq	%rcx, -144(%rbp)
	movq	16(%rax), %rcx
	movq	%rcx, -128(%rbp)
.L546:
	movq	8(%rax), %rcx
	movq	%rcx, -136(%rbp)
	movb	$0, 16(%rax)
	cmpb	$0, 47+_ZN4node11per_process18enabled_debug_listE(%rip)
	movq	%rdx, (%rax)
	movq	-144(%rbp), %rdi
	movq	$0, 8(%rax)
	jne	.L564
.L547:
	cmpq	%r14, %rdi
	je	.L548
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
.L548:
	movq	-176(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L549
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
.L549:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L550
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
.L550:
	movq	-232(%rbp), %rdx
	movq	-224(%rbp), %rcx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jnb	.L530
	movq	%rcx, %rsi
	subq	%rdx, %rsi
	cmpq	$2097151, %rsi
	jbe	.L530
	movq	%rdx, %xmm0
	movq	%rcx, %xmm4
	movq	%rsi, %rax
	movb	$1, 16(%r12)
	punpcklqdq	%xmm4, %xmm0
	shrq	$21, %rax
	movups	%xmm0, (%r12)
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L569:
	movdqu	16(%rax), %xmm2
	movaps	%xmm2, -128(%rbp)
	jmp	.L532
	.p2align 4,,10
	.p2align 3
.L571:
	movdqu	16(%rax), %xmm3
	movaps	%xmm3, -128(%rbp)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L570:
	movq	%r11, %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rcx, -224(%rbp)
	movq	%r11, -264(%rbp)
	movq	%rbx, -208(%rbp)
	movq	$28, -216(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movdqa	.LC21(%rip), %xmm0
	movq	%r15, %rdi
	movabsq	$4207054144930868256, %rsi
	movq	%rax, -208(%rbp)
	movq	%rdx, -192(%rbp)
	movq	%rsi, 16(%rax)
	movq	%r13, %rsi
	movups	%xmm0, (%rax)
	movl	$175121696, 24(%rax)
	movq	-216(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rax, -200(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-248(%rbp), %rax
	xorl	%edx, %edx
	movq	$16, -216(%rbp)
	movq	%rax, -176(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-216(%rbp), %rdx
	movdqa	.LC20(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rax, -176(%rbp)
	movq	%rdx, -160(%rbp)
	movups	%xmm0, (%rax)
	movq	-216(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	%rax, -168(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-200(%rbp), %rdx
	movq	-208(%rbp), %rsi
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_appendEPKcm@PLT
	movq	%r14, -144(%rbp)
	movq	-264(%rbp), %r11
	leaq	__start_lpstub(%rip), %rcx
	movq	(%rax), %rsi
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L572
	movq	%rsi, -144(%rbp)
	movq	16(%rax), %rsi
	movq	%rsi, -128(%rbp)
.L539:
	movq	8(%rax), %rsi
	movq	%rsi, -136(%rbp)
	movb	$0, 16(%rax)
	cmpb	$0, 47+_ZN4node11per_process18enabled_debug_listE(%rip)
	movq	%rdx, (%rax)
	movq	-144(%rbp), %rdi
	movq	$0, 8(%rax)
	jne	.L563
.L540:
	cmpq	%r14, %rdi
	je	.L541
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
	leaq	__start_lpstub(%rip), %rcx
.L541:
	movq	-176(%rbp), %rdi
	cmpq	-248(%rbp), %rdi
	je	.L542
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
	leaq	__start_lpstub(%rip), %rcx
.L542:
	movq	-208(%rbp), %rdi
	cmpq	%rbx, %rdi
	je	.L543
	movq	%r11, -264(%rbp)
	call	_ZdlPv@PLT
	movq	-264(%rbp), %r11
	leaq	__start_lpstub(%rip), %rcx
.L543:
	movq	%rcx, -104(%rbp)
	movq	-112(%rbp), %rax
	leaq	__start_lpstub(%rip), %rdx
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L572:
	movdqu	16(%rax), %xmm5
	movaps	%xmm5, -128(%rbp)
	jmp	.L539
.L568:
	call	__stack_chk_fail@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv.cold, @function
_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv.cold:
.LFSB6171:
.L563:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	%rdi, %rsi
	movq	stderr(%rip), %rdi
	leaq	-224(%rbp), %rdx
	movq	%r11, -264(%rbp)
	call	_ZN4node7FPrintFIJPvEEEvP8_IO_FILEPKcDpOT_
	movq	-144(%rbp), %rdi
	movq	-264(%rbp), %r11
	leaq	__start_lpstub(%rip), %rcx
	jmp	.L540
.L565:
	movq	%rdi, %rsi
	movq	stderr(%rip), %rdi
	leaq	-224(%rbp), %rdx
	call	_ZN4node7FPrintFIJmEEEvP8_IO_FILEPKcDpOT_
	movq	-144(%rbp), %rdi
	jmp	.L554
.L564:
	movq	%rdi, %rsi
	movq	stderr(%rip), %rdi
	leaq	-224(%rbp), %rcx
	leaq	-232(%rbp), %rdx
	movq	%r11, -264(%rbp)
	call	_ZN4node7FPrintFIJRPcS2_EEEvP8_IO_FILEPKcDpOT_
	movq	-144(%rbp), %rdi
	movq	-264(%rbp), %r11
	jmp	.L547
.L562:
	movq	%rdi, %rsi
	movq	stderr(%rip), %rdi
	leaq	-232(%rbp), %rcx
	leaq	-240(%rbp), %rdx
	leaq	-224(%rbp), %r8
	movq	%r11, -264(%rbp)
	call	_ZN4node7FPrintFIJPvS1_S1_EEEvP8_IO_FILEPKcDpOT_
	movq	-144(%rbp), %rdi
	movq	-264(%rbp), %r11
	jmp	.L533
	.cfi_endproc
.LFE6171:
	.text
	.size	_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv, .-_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv
	.section	.text.unlikely
	.size	_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv.cold, .-_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv.cold
.LCOLDE24:
	.text
.LHOTE24:
	.p2align 4
	.globl	_ZN4node25MapStaticCodeToLargePagesEv
	.type	_ZN4node25MapStaticCodeToLargePagesEv, @function
_ZN4node25MapStaticCodeToLargePagesEv:
.LFB6193:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$40, %rsp
	.cfi_offset 12, -24
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node12_GLOBAL__N_129IsTransparentHugePagesEnabledEv
	movl	%eax, %r8d
	movl	$13, %eax
	testb	%r8b, %r8b
	je	.L573
	leaq	-48(%rbp), %r12
	movq	%r12, %rdi
	call	_ZN4node12_GLOBAL__N_118FindNodeTextRegionEv
	cmpb	$0, -32(%rbp)
	movl	$2, %eax
	je	.L573
	movq	-40(%rbp), %rsi
	movq	%r12, %rdi
	call	_ZN4node26MoveTextRegionToLargePagesERKNS_12_GLOBAL__N_111text_regionE.isra.0
.L573:
	movq	-24(%rbp), %rdx
	xorq	%fs:40, %rdx
	jne	.L580
	addq	$40, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L580:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE6193:
	.size	_ZN4node25MapStaticCodeToLargePagesEv, .-_ZN4node25MapStaticCodeToLargePagesEv
	.weak	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.1
.LC25:
	.string	"../src/debug_utils-inl.h:113"
.LC26:
	.string	"(n) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = char*&; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.weak	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.rodata.str1.1
.LC28:
	.string	"../src/debug_utils-inl.h:76"
.LC29:
	.string	"(p) != nullptr"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPcJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC27
	.weak	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC30:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = void*; Args = {void*}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC30
	.weak	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPvJS1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.weak	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0
	.section	.rodata.str1.1
.LC31:
	.string	"../src/debug_utils-inl.h:107"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"std::is_pointer<typename std::remove_reference<Arg>::type>::value"
	.align 8
.LC33:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = long unsigned int; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,"awG",@progbits,_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0, 24
_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_0:
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.weak	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplImJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC33
	.weak	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = char*&; Args = {char*&}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC34
	.weak	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIRPcJS2_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC34
	.weak	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = void*; Args = {}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC35
	.weak	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPvJEEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC35
	.weak	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"std::string node::SPrintFImpl(const char*, Arg&&, Args&& ...) [with Arg = void*; Args = {void*, void*}; std::string = std::__cxx11::basic_string<char>]"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,"awG",@progbits,_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1, 24
_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args_1:
	.quad	.LC25
	.quad	.LC26
	.quad	.LC36
	.weak	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,"awG",@progbits,_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args, 24
_ZZN4node11SPrintFImplIPvJS1_S1_EEENSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEEPKcOT_DpOT0_E4args:
	.quad	.LC28
	.quad	.LC29
	.quad	.LC36
	.weak	_ZZN4node11SPrintFImplB5cxx11EPKcE4args
	.section	.rodata.str1.1
.LC37:
	.string	"../src/debug_utils-inl.h:67"
.LC38:
	.string	"(p[1]) == ('%')"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"std::string node::SPrintFImpl(const char*)"
	.section	.data.rel.ro.local._ZZN4node11SPrintFImplB5cxx11EPKcE4args,"awG",@progbits,_ZZN4node11SPrintFImplB5cxx11EPKcE4args,comdat
	.align 16
	.type	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, @gnu_unique_object
	.size	_ZZN4node11SPrintFImplB5cxx11EPKcE4args, 24
_ZZN4node11SPrintFImplB5cxx11EPKcE4args:
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC19:
	.quad	2675202450467681395
	.quad	4210154762029310064
	.align 16
.LC20:
	.quad	7306916055797429576
	.quad	2322291043592642675
	.align 16
.LC21:
	.quad	7453010352052728404
	.quad	8245921732048610592
	.align 16
.LC22:
	.quad	2334102031740529729
	.quad	8316213806815404402
	.align 16
.LC23:
	.quad	7216209593653096262
	.quad	7021147438970136608
	.weak	__node_text_start
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
