	.file	"udp_wrap.cc"
	.text
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB4624:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4624:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZN4node11UDPListener11OnAfterBindEv,"axG",@progbits,_ZN4node11UDPListener11OnAfterBindEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11UDPListener11OnAfterBindEv
	.type	_ZN4node11UDPListener11OnAfterBindEv, @function
_ZN4node11UDPListener11OnAfterBindEv:
.LFB4662:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4662:
	.size	_ZN4node11UDPListener11OnAfterBindEv, .-_ZN4node11UDPListener11OnAfterBindEv
	.section	.text._ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB4664:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE4664:
	.size	_ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node7UDPWrap8SelfSizeEv,"axG",@progbits,_ZNK4node7UDPWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7UDPWrap8SelfSizeEv
	.type	_ZNK4node7UDPWrap8SelfSizeEv, @function
_ZNK4node7UDPWrap8SelfSizeEv:
.LFB4666:
	.cfi_startproc
	endbr64
	movl	$352, %eax
	ret
	.cfi_endproc
.LFE4666:
	.size	_ZNK4node7UDPWrap8SelfSizeEv, .-_ZNK4node7UDPWrap8SelfSizeEv
	.section	.text._ZN4node10BaseObject11OnGCCollectEv,"axG",@progbits,_ZN4node10BaseObject11OnGCCollectEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10BaseObject11OnGCCollectEv
	.type	_ZN4node10BaseObject11OnGCCollectEv, @function
_ZN4node10BaseObject11OnGCCollectEv:
.LFB7524:
	.cfi_startproc
	endbr64
	movq	(%rdi), %rax
	jmp	*8(%rax)
	.cfi_endproc
.LFE7524:
	.size	_ZN4node10BaseObject11OnGCCollectEv, .-_ZN4node10BaseObject11OnGCCollectEv
	.section	.text._ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7584:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE7584:
	.size	_ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZNK4node8SendWrap8SelfSizeEv,"axG",@progbits,_ZNK4node8SendWrap8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8SendWrap8SelfSizeEv
	.type	_ZNK4node8SendWrap8SelfSizeEv, @function
_ZNK4node8SendWrap8SelfSizeEv:
.LFB7586:
	.cfi_startproc
	endbr64
	movl	$424, %eax
	ret
	.cfi_endproc
.LFE7586:
	.size	_ZNK4node8SendWrap8SelfSizeEv, .-_ZNK4node8SendWrap8SelfSizeEv
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap12GetAsyncWrapEv
	.type	_ZN4node7UDPWrap12GetAsyncWrapEv, @function
_ZN4node7UDPWrap12GetAsyncWrapEv:
.LFB7652:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE7652:
	.size	_ZN4node7UDPWrap12GetAsyncWrapEv, .-_ZN4node7UDPWrap12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.type	_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv, @function
_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv:
.LFB9776:
	.cfi_startproc
	endbr64
	movq	%rdi, %rax
	ret
	.cfi_endproc
.LFE9776:
	.size	_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv, .-_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.type	_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv, @function
_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv:
.LFB9775:
	.cfi_startproc
	endbr64
	cmpq	%rdi, 88(%rdi)
	je	.L13
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$88, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9775:
	.size	_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv, .-_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.section	.text._ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i,"axG",@progbits,_ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i,comdat
	.p2align 4
	.weak	_ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i
	.type	_ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i, @function
_ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i:
.LFB9120:
	.cfi_startproc
	endbr64
	movq	-72(%rdi), %rdx
	leaq	-88(%rdi), %rcx
	subl	$1, 2156(%rdx)
	js	.L19
	jmp	*80(%rcx)
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9120:
	.size	_ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i, .-_ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i
	.section	.text._ZN4node7UDPWrapD2Ev,"axG",@progbits,_ZN4node7UDPWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node7UDPWrapD2Ev
	.type	_ZN4node7UDPWrapD2Ev, @function
_ZN4node7UDPWrapD2Ev:
.LFB9738:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$192, %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, 104(%rdi)
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L21
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L22
	movq	$0, 8(%rdx)
.L22:
	movq	$0, 8(%rax)
.L21:
	movq	96(%rdi), %rax
	testq	%rax, %rax
	je	.L23
	movq	$0, 8(%rax)
.L23:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%rdi), %rdx
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9738:
	.size	_ZN4node7UDPWrapD2Ev, .-_ZN4node7UDPWrapD2Ev
	.weak	_ZN4node7UDPWrapD1Ev
	.set	_ZN4node7UDPWrapD1Ev,_ZN4node7UDPWrapD2Ev
	.section	.text._ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,"axG",@progbits,_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.type	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, @function
_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_:
.LFB7528:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	addq	$8, %rdi
	jmp	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	.cfi_endproc
.LFE7528:
	.size	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_, .-_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap14CreateSendWrapEm
	.type	_ZN4node7UDPWrap14CreateSendWrapEm, @function
_ZN4node7UDPWrap14CreateSendWrapEm:
.LFB7649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	16(%rdi), %r13
	movq	344(%rdi), %r15
	movl	$424, %edi
	call	_Znwm@PLT
	movsd	.LC0(%rip), %xmm0
	movl	$34, %ecx
	movzbl	336(%rbx), %ebx
	movq	%rax, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r13)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L37
	movq	2112(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r13)
	leaq	16+_ZTVN4node8SendWrapE(%rip), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%r13), %rdx
	movq	%rax, (%r12)
	addq	$112, %rax
	movq	%rax, 56(%r12)
	movq	%r12, %rax
	movb	%bl, 416(%r12)
	movq	%r14, 408(%r12)
	movq	%rdx, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	.cfi_restore_state
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7649:
	.size	_ZN4node7UDPWrap14CreateSendWrapEm, .-_ZN4node7UDPWrap14CreateSendWrapEm
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap11GetPeerNameEv
	.type	_ZN4node7UDPWrap11GetPeerNameEv, @function
_ZN4node7UDPWrap11GetPeerNameEv:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$120, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13SocketAddress12FromPeerNameERK8uv_udp_s@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L41
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L41:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7653:
	.size	_ZN4node7UDPWrap11GetPeerNameEv, .-_ZN4node7UDPWrap11GetPeerNameEv
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap11GetSockNameEv
	.type	_ZN4node7UDPWrap11GetSockNameEv, @function
_ZN4node7UDPWrap11GetSockNameEv:
.LFB7654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$120, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13SocketAddress12FromSockNameERK8uv_udp_s@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L45
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L45:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7654:
	.size	_ZN4node7UDPWrap11GetSockNameEv, .-_ZN4node7UDPWrap11GetSockNameEv
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap9RecvStartEv
	.type	_ZN4node7UDPWrap9RecvStartEv, @function
_ZN4node7UDPWrap9RecvStartEv:
.LFB7656:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L48
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$120, %rdi
	leaq	_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj(%rip), %rdx
	leaq	_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_udp_recv_start@PLT
	movl	$0, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$-114, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	.cfi_restore 6
	movl	$-9, %eax
	ret
	.cfi_endproc
.LFE7656:
	.size	_ZN4node7UDPWrap9RecvStartEv, .-_ZN4node7UDPWrap9RecvStartEv
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap8RecvStopEv
	.type	_ZN4node7UDPWrap8RecvStopEv, @function
_ZN4node7UDPWrap8RecvStopEv:
.LFB7658:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L54
	addq	$120, %rdi
	jmp	uv_udp_recv_stop@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$-9, %eax
	ret
	.cfi_endproc
.LFE7658:
	.size	_ZN4node7UDPWrap8RecvStopEv, .-_ZN4node7UDPWrap8RecvStopEv
	.section	.text._ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.type	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv, @function
_ZThn56_N4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv:
.LFB9845:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	ret
	.cfi_endproc
.LFE9845:
	.size	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv, .-_ZThn56_N4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.text
	.p2align 4
	.globl	_ZThn88_N4node7UDPWrap12GetAsyncWrapEv
	.type	_ZThn88_N4node7UDPWrap12GetAsyncWrapEv, @function
_ZThn88_N4node7UDPWrap12GetAsyncWrapEv:
.LFB9846:
	.cfi_startproc
	endbr64
	leaq	-88(%rdi), %rax
	ret
	.cfi_endproc
.LFE9846:
	.size	_ZThn88_N4node7UDPWrap12GetAsyncWrapEv, .-_ZThn88_N4node7UDPWrap12GetAsyncWrapEv
	.section	.text._ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv,"axG",@progbits,_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv,comdat
	.p2align 4
	.weak	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.type	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE6CancelEv, @function
_ZThn56_N4node7ReqWrapI13uv_udp_send_sE6CancelEv:
.LFB9848:
	.cfi_startproc
	endbr64
	leaq	-56(%rdi), %rax
	cmpq	%rax, 32(%rdi)
	je	.L59
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$32, %rdi
	jmp	uv_cancel@PLT
	.cfi_endproc
.LFE9848:
	.size	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE6CancelEv, .-_ZThn56_N4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.section	.text._ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev:
.LFB7585:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movq	$8, 8(%rdi)
	movq	%rdi, %rax
	movabsq	$8097879324661540179, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movb	$0, 24(%rdi)
	ret
	.cfi_endproc
.LFE7585:
	.size	_ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev
	.section	.text._ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev:
.LFB4665:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$112, 22(%rdi)
	movq	%rdi, %rax
	movq	%rdx, (%rdi)
	movl	$24946, %edx
	movl	$1464878165, 16(%rdi)
	movw	%dx, 20(%rdi)
	movq	$7, 8(%rdi)
	movb	$0, 23(%rdi)
	ret
	.cfi_endproc
.LFE4665:
	.size	_ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev, .-_ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev
	.text
	.p2align 4
	.globl	_ZThn88_N4node7UDPWrap11GetPeerNameEv
	.type	_ZThn88_N4node7UDPWrap11GetPeerNameEv, @function
_ZThn88_N4node7UDPWrap11GetPeerNameEv:
.LFB9853:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13SocketAddress12FromPeerNameERK8uv_udp_s@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L65
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L65:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9853:
	.size	_ZThn88_N4node7UDPWrap11GetPeerNameEv, .-_ZThn88_N4node7UDPWrap11GetPeerNameEv
	.p2align 4
	.globl	_ZThn88_N4node7UDPWrap11GetSockNameEv
	.type	_ZThn88_N4node7UDPWrap11GetSockNameEv, @function
_ZThn88_N4node7UDPWrap11GetSockNameEv:
.LFB9854:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$24, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node13SocketAddress12FromSockNameERK8uv_udp_s@PLT
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L69
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L69:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9854:
	.size	_ZThn88_N4node7UDPWrap11GetSockNameEv, .-_ZThn88_N4node7UDPWrap11GetSockNameEv
	.p2align 4
	.globl	_ZThn88_N4node7UDPWrap8RecvStopEv
	.type	_ZThn88_N4node7UDPWrap8RecvStopEv, @function
_ZThn88_N4node7UDPWrap8RecvStopEv:
.LFB9855:
	.cfi_startproc
	endbr64
	movl	-16(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L71
	addq	$32, %rdi
	jmp	uv_udp_recv_stop@PLT
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$-9, %eax
	ret
	.cfi_endproc
.LFE9855:
	.size	_ZThn88_N4node7UDPWrap8RecvStopEv, .-_ZThn88_N4node7UDPWrap8RecvStopEv
	.section	.text._ZN4node8SendWrapD0Ev,"axG",@progbits,_ZN4node8SendWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node8SendWrapD0Ev
	.type	_ZThn56_N4node8SendWrapD0Ev, @function
_ZThn56_N4node8SendWrapD0Ev:
.LFB9850:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI13uv_udp_send_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L75
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r12
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$424, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9850:
	.size	_ZThn56_N4node8SendWrapD0Ev, .-_ZThn56_N4node8SendWrapD0Ev
	.section	.text._ZN4node8SendWrapD2Ev,"axG",@progbits,_ZN4node8SendWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn56_N4node8SendWrapD1Ev
	.type	_ZThn56_N4node8SendWrapD1Ev, @function
_ZThn56_N4node8SendWrapD1Ev:
.LFB9847:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI13uv_udp_send_sEE(%rip), %rax
	movq	%rax, -56(%rdi)
	addq	$112, %rax
	cmpq	$0, -48(%rdi)
	movq	%rax, (%rdi)
	je	.L81
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rax
	leaq	-56(%rdi), %r8
	movq	%r8, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9847:
	.size	_ZThn56_N4node8SendWrapD1Ev, .-_ZThn56_N4node8SendWrapD1Ev
	.align 2
	.p2align 4
	.weak	_ZN4node8SendWrapD2Ev
	.type	_ZN4node8SendWrapD2Ev, @function
_ZN4node8SendWrapD2Ev:
.LFB8854:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7ReqWrapI13uv_udp_send_sEE(%rip), %rax
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L87
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8854:
	.size	_ZN4node8SendWrapD2Ev, .-_ZN4node8SendWrapD2Ev
	.weak	_ZN4node8SendWrapD1Ev
	.set	_ZN4node8SendWrapD1Ev,_ZN4node8SendWrapD2Ev
	.section	.text._ZN4node8SendWrapD0Ev,"axG",@progbits,_ZN4node8SendWrapD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node8SendWrapD0Ev
	.type	_ZN4node8SendWrapD0Ev, @function
_ZN4node8SendWrapD0Ev:
.LFB8856:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7ReqWrapI13uv_udp_send_sEE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	%rax, (%rdi)
	addq	$112, %rax
	cmpq	$0, 8(%rdi)
	movq	%rax, 56(%rdi)
	je	.L91
	movq	64(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdi, %r12
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$424, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	.cfi_restore_state
	leaq	_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE8856:
	.size	_ZN4node8SendWrapD0Ev, .-_ZN4node8SendWrapD0Ev
	.text
	.p2align 4
	.globl	_ZThn88_N4node7UDPWrap9RecvStartEv
	.type	_ZThn88_N4node7UDPWrap9RecvStartEv, @function
_ZThn88_N4node7UDPWrap9RecvStartEv:
.LFB9858:
	.cfi_startproc
	endbr64
	movl	-16(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L94
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	addq	$32, %rdi
	leaq	_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj(%rip), %rdx
	leaq	_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_udp_recv_start@PLT
	movl	$0, %edx
	popq	%rbp
	.cfi_def_cfa 7, 8
	cmpl	$-114, %eax
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	.cfi_restore 6
	movl	$-9, %eax
	ret
	.cfi_endproc
.LFE9858:
	.size	_ZThn88_N4node7UDPWrap9RecvStartEv, .-_ZThn88_N4node7UDPWrap9RecvStartEv
	.section	.text._ZN4node7UDPWrapD0Ev,"axG",@progbits,_ZN4node7UDPWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node7UDPWrapD0Ev
	.type	_ZThn88_N4node7UDPWrapD0Ev, @function
_ZThn88_N4node7UDPWrapD0Ev:
.LFB9851:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-88(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	addq	$192, %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, 16(%rdi)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L100
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L101
	movq	$0, 8(%rdx)
.L101:
	movq	$0, 8(%rax)
.L100:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L102
	movq	$0, 8(%rax)
.L102:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%rdi), %rdx
	movq	%rax, -88(%rdi)
	movq	-24(%rdi), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$352, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9851:
	.size	_ZThn88_N4node7UDPWrapD0Ev, .-_ZThn88_N4node7UDPWrapD0Ev
	.p2align 4
	.weak	_ZThn104_N4node7UDPWrapD0Ev
	.type	_ZThn104_N4node7UDPWrapD0Ev, @function
_ZThn104_N4node7UDPWrapD0Ev:
.LFB9852:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-104(%rdi), %r12
	subq	$8, %rsp
	movq	%rax, -104(%rdi)
	addq	$192, %rax
	movq	%rax, -16(%rdi)
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L114
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L115
	movq	$0, 8(%rdx)
.L115:
	movq	$0, 8(%rax)
.L114:
	movq	-8(%rdi), %rax
	testq	%rax, %rax
	je	.L116
	movq	$0, 8(%rax)
.L116:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-48(%rdi), %rdx
	movq	%rax, -104(%rdi)
	movq	-40(%rdi), %rax
	movq	%r12, %rdi
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$352, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9852:
	.size	_ZThn104_N4node7UDPWrapD0Ev, .-_ZThn104_N4node7UDPWrapD0Ev
	.align 2
	.p2align 4
	.weak	_ZN4node7UDPWrapD0Ev
	.type	_ZN4node7UDPWrapD0Ev, @function
_ZN4node7UDPWrapD0Ev:
.LFB9740:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	addq	$192, %rax
	movq	%rax, 88(%rdi)
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, 104(%rdi)
	movq	112(%rdi), %rax
	testq	%rax, %rax
	je	.L128
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L129
	movq	$0, 8(%rdx)
.L129:
	movq	$0, 8(%rax)
.L128:
	movq	96(%r12), %rax
	testq	%rax, %rax
	je	.L130
	movq	$0, 8(%rax)
.L130:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$352, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9740:
	.size	_ZN4node7UDPWrapD0Ev, .-_ZN4node7UDPWrapD0Ev
	.section	.text._ZN4node7UDPWrapD2Ev,"axG",@progbits,_ZN4node7UDPWrapD5Ev,comdat
	.p2align 4
	.weak	_ZThn104_N4node7UDPWrapD1Ev
	.type	_ZThn104_N4node7UDPWrapD1Ev, @function
_ZThn104_N4node7UDPWrapD1Ev:
.LFB9859:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	%rax, -104(%rdi)
	addq	$192, %rax
	movq	%rax, -16(%rdi)
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L142
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L143
	movq	$0, 8(%rdx)
.L143:
	movq	$0, 8(%rax)
.L142:
	movq	-8(%rdi), %rax
	testq	%rax, %rax
	je	.L144
	movq	$0, 8(%rax)
.L144:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-48(%rdi), %rdx
	subq	$104, %rdi
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9859:
	.size	_ZThn104_N4node7UDPWrapD1Ev, .-_ZThn104_N4node7UDPWrapD1Ev
	.p2align 4
	.weak	_ZThn88_N4node7UDPWrapD1Ev
	.type	_ZThn88_N4node7UDPWrapD1Ev, @function
_ZThn88_N4node7UDPWrapD1Ev:
.LFB9860:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	%rax, -88(%rdi)
	addq	$192, %rax
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, 16(%rdi)
	movq	24(%rdi), %rax
	testq	%rax, %rax
	je	.L155
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L156
	movq	$0, 8(%rdx)
.L156:
	movq	$0, 8(%rax)
.L155:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L157
	movq	$0, 8(%rax)
.L157:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%rdi), %rdx
	subq	$88, %rdi
	movq	%rax, (%rdi)
	movq	64(%rdi), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE9860:
	.size	_ZThn88_N4node7UDPWrapD1Ev, .-_ZThn88_N4node7UDPWrapD1Ev
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap7OnAllocEm
	.type	_ZN4node7UDPWrap7OnAllocEm, @function
_ZN4node7UDPWrap7OnAllocEm:
.LFB7663:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rsi, %rbx
	subq	$40, %rsp
	movq	16(%rdi), %r13
	movq	360(%r13), %rax
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*24(%rax)
	testq	%rax, %rax
	je	.L174
	movl	%ebx, %esi
	movq	%rax, %rdi
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rax, %r15
	movq	%rdx, %r14
	call	uv_buf_init@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rdx, %rbx
	movq	%rax, %r12
	call	uv_buf_init@PLT
	movq	%rax, -72(%rbp)
	movq	%rdx, -64(%rbp)
	testq	%r12, %r12
	je	.L169
	movq	360(%r13), %rax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L169:
	addq	$40, %rsp
	movq	%r15, %rax
	movq	%r14, %rdx
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	.cfi_restore_state
	leaq	_ZZN4node11Environment8AllocateEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7663:
	.size	_ZN4node7UDPWrap7OnAllocEm, .-_ZN4node7UDPWrap7OnAllocEm
	.set	.LTHUNK8,_ZN4node7UDPWrap7OnAllocEm
	.p2align 4
	.globl	_ZThn104_N4node7UDPWrap7OnAllocEm
	.type	_ZThn104_N4node7UDPWrap7OnAllocEm, @function
_ZThn104_N4node7UDPWrap7OnAllocEm:
.LFB9874:
	.cfi_startproc
	endbr64
	subq	$104, %rdi
	jmp	.LTHUNK8
	.cfi_endproc
.LFE9874:
	.size	_ZThn104_N4node7UDPWrap7OnAllocEm, .-_ZThn104_N4node7UDPWrap7OnAllocEm
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t
	.type	_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t, @function
_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t:
.LFB7662:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	-24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L180
	movq	(%rdi), %rax
	movq	%rdx, %rbx
	leaq	_ZThn104_N4node7UDPWrap7OnAllocEm(%rip), %rdx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L177
	subq	$104, %rdi
	call	.LTHUNK8
	movq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	call	*%rax
	movq	%rax, (%rbx)
	movq	%rdx, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	.cfi_restore_state
	leaq	_ZZNK4node11UDPWrapBase8listenerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7662:
	.size	_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t, .-_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t
	.p2align 4
	.globl	_ZThn104_N4node7UDPWrap14CreateSendWrapEm
	.type	_ZThn104_N4node7UDPWrap14CreateSendWrapEm, @function
_ZThn104_N4node7UDPWrap14CreateSendWrapEm:
.LFB9861:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	-88(%rdi), %r13
	movq	240(%rdi), %r15
	movl	$424, %edi
	call	_Znwm@PLT
	movsd	.LC0(%rip), %xmm0
	movl	$34, %ecx
	movzbl	232(%rbx), %ebx
	movq	%rax, %rdi
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rax, %r12
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r13)
	movq	%rax, 56(%r12)
	leaq	64(%r12), %rax
	movq	%rax, 64(%r12)
	movq	%rax, 72(%r12)
	je	.L184
	movq	2112(%r13), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r13)
	leaq	16+_ZTVN4node8SendWrapE(%rip), %rax
	movq	%rdx, 64(%r12)
	leaq	2112(%r13), %rdx
	movq	%rax, (%r12)
	addq	$112, %rax
	movq	%rax, 56(%r12)
	movq	%r12, %rax
	movb	%bl, 416(%r12)
	movq	%r14, 408(%r12)
	movq	%rdx, 72(%r12)
	movq	$0, 80(%r12)
	movq	$0, 88(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	.cfi_restore_state
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE9861:
	.size	_ZThn104_N4node7UDPWrap14CreateSendWrapEm, .-_ZThn104_N4node7UDPWrap14CreateSendWrapEm
	.align 2
	.p2align 4
	.type	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr.part.0, @function
_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr.part.0:
.LFB9828:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rdi, %r9
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rcx, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdx, %rbx
	subq	$56, %rsp
	testq	%rdx, %rdx
	je	.L218
	leaq	-1(%rdx), %rdx
	cmpq	$4, %rdx
	jbe	.L219
	movq	%rdx, %rcx
	movq	%rsi, %rax
	pxor	%xmm1, %xmm1
	shrq	%rcx
	salq	$5, %rcx
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L189:
	movdqu	8(%rax), %xmm0
	movdqu	24(%rax), %xmm2
	addq	$32, %rax
	punpcklqdq	%xmm2, %xmm0
	paddq	%xmm0, %xmm1
	cmpq	%rcx, %rax
	jne	.L189
	movdqa	%xmm1, %xmm0
	movq	%rdx, %rax
	psrldq	$8, %xmm0
	andq	$-2, %rax
	paddq	%xmm0, %xmm1
	movq	%xmm1, %r12
.L187:
	movq	%rax, %rdx
	salq	$4, %rdx
	addq	8(%r13,%rdx), %r12
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L186
	salq	$4, %rdx
	addq	8(%r13,%rdx), %r12
	leaq	2(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L186
	salq	$4, %rdx
	addq	8(%r13,%rdx), %r12
	leaq	3(%rax), %rdx
	cmpq	%rdx, %rbx
	jbe	.L186
	salq	$4, %rdx
	addq	$4, %rax
	addq	8(%r13,%rdx), %r12
	cmpq	%rax, %rbx
	jbe	.L186
	salq	$4, %rax
	addq	8(%r13,%rax), %r12
.L186:
	movq	16(%r9), %rax
	movq	1648(%rax), %r15
	movq	1640(%rax), %rdx
	testq	%r15, %r15
	je	.L191
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rsi
	leaq	8(%r15), %rax
	testq	%rsi, %rsi
	je	.L192
	lock addl	$1, (%rax)
	movzbl	464(%rdx), %ecx
	testq	%rsi, %rsi
	je	.L237
.L216:
	movl	$-1, %edx
	lock xaddl	%edx, (%rax)
	movl	%edx, %eax
	cmpl	$1, %eax
	je	.L238
.L195:
	testb	%cl, %cl
	jne	.L215
.L240:
	leaq	120(%r9), %rdi
	movq	%r14, %rcx
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%r9, -56(%rbp)
	call	uv_udp_try_send@PLT
	movq	-56(%rbp), %r9
	cmpl	$-38, %eax
	movl	%eax, %r15d
	je	.L215
	cmpl	$-11, %eax
	je	.L215
	cltq
	testl	%r15d, %r15d
	js	.L185
	testq	%rbx, %rbx
	je	.L200
	movq	%rax, %rcx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L239:
	subq	%rdx, %rcx
	addq	$16, %r13
	subq	$1, %rbx
	je	.L200
.L202:
	movq	8(%r13), %rdx
	cmpq	%rcx, %rdx
	jbe	.L239
	subq	%rcx, %rdx
	addq	%rcx, 0(%r13)
	movq	%rdx, 8(%r13)
	testl	%r15d, %r15d
	je	.L215
.L185:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	.cfi_restore_state
	addl	$1, 8(%r15)
	movzbl	464(%rdx), %ecx
	testq	%rsi, %rsi
	jne	.L216
.L237:
	movl	8(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 8(%r15)
	cmpl	$1, %eax
	jne	.L195
.L238:
	movq	(%r15), %rax
	movq	%r9, -64(%rbp)
	movq	%r15, %rdi
	movb	%cl, -56(%rbp)
	call	*16(%rax)
	movq	_ZL28__gthrw___pthread_key_createPjPFvPvE@GOTPCREL(%rip), %rsi
	movzbl	-56(%rbp), %ecx
	movq	-64(%rbp), %r9
	testq	%rsi, %rsi
	je	.L196
	movl	$-1, %eax
	lock xaddl	%eax, 12(%r15)
.L197:
	cmpl	$1, %eax
	jne	.L195
	movq	(%r15), %rax
	movq	%r9, -64(%rbp)
	movq	%r15, %rdi
	movb	%cl, -56(%rbp)
	call	*24(%rax)
	movq	-64(%rbp), %r9
	movzbl	-56(%rbp), %ecx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L191:
	movzbl	464(%rdx), %ecx
	testb	%cl, %cl
	je	.L240
	.p2align 4,,10
	.p2align 3
.L215:
	movq	16(%r9), %r11
	movsd	40(%r9), %xmm0
	movq	1216(%r11), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L241
.L204:
	movq	1256(%r11), %rax
	movq	96(%r9), %r15
	movsd	24(%rax), %xmm3
	movsd	%xmm0, 24(%rax)
	movsd	%xmm3, -56(%rbp)
	testq	%r15, %r15
	je	.L242
	movq	(%r15), %rax
	leaq	_ZThn104_N4node7UDPWrap14CreateSendWrapEm(%rip), %rdx
	movq	32(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L206
	movq	-88(%r15), %rsi
	movq	240(%r15), %rdx
	movl	$424, %edi
	movq	%r9, -88(%rbp)
	movq	%r11, -80(%rbp)
	movq	%rsi, -72(%rbp)
	movq	%rdx, -64(%rbp)
	call	_Znwm@PLT
	movq	-64(%rbp), %rdx
	movq	-72(%rbp), %rsi
	movl	$34, %ecx
	movsd	.LC0(%rip), %xmm0
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	movzbl	232(%r15), %r15d
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %rsi
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %r9
	movq	%rax, 56(%r10)
	cmpb	$0, 1928(%rsi)
	leaq	64(%r10), %rax
	movq	%rax, 64(%r10)
	movq	%rax, 72(%r10)
	je	.L243
	movq	2112(%rsi), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%rsi)
	leaq	16+_ZTVN4node8SendWrapE(%rip), %rax
	movq	%rdx, 64(%r10)
	leaq	2112(%rsi), %rdx
	leaq	120(%r9), %rsi
	movq	%rax, (%r10)
	addq	$112, %rax
	movq	%rdx, 72(%r10)
	movq	$0, 80(%r10)
	movq	%rax, 56(%r10)
	movb	%r15b, 416(%r10)
	movq	%r12, 408(%r10)
	movq	%r10, 88(%r10)
.L208:
	leaq	_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrENUlP13uv_udp_send_siE_4_FUNES7_i(%rip), %rax
	leaq	88(%r10), %rdi
	movq	%r14, %r8
	movl	%ebx, %ecx
	movq	%rax, 80(%r10)
	leaq	_ZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE7WrapperES2_i(%rip), %r9
	movq	%r13, %rdx
	movq	%r11, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	uv_udp_send@PLT
	movq	-64(%rbp), %r10
	movq	-72(%rbp), %r11
	testl	%eax, %eax
	movl	%eax, %r15d
	js	.L212
	movq	16(%r10), %rax
	addl	$1, 2156(%rax)
	testl	%r15d, %r15d
	jne	.L212
.L211:
	movq	1256(%r11), %rax
	movsd	-56(%rbp), %xmm4
	movsd	%xmm4, 24(%rax)
	addq	$56, %rsp
	movslq	%r15d, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	.cfi_restore_state
	comisd	.LC1(%rip), %xmm0
	jnb	.L204
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L200:
	cmpq	%rax, %r12
	jne	.L244
	addq	$56, %rsp
	leaq	1(%r12), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	.cfi_restore_state
	xorl	%r12d, %r12d
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L212:
	movq	(%r10), %rax
	movq	%r11, -64(%rbp)
	movq	%r10, %rdi
	call	*8(%rax)
	movq	-64(%rbp), %r11
	jmp	.L211
.L219:
	xorl	%r12d, %r12d
	xorl	%eax, %eax
	jmp	.L187
.L196:
	movl	12(%r15), %eax
	leal	-1(%rax), %edx
	movl	%edx, 12(%r15)
	jmp	.L197
.L206:
	movq	%r9, -72(%rbp)
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%r11, -64(%rbp)
	call	*%rax
	movq	-64(%rbp), %r11
	movq	-72(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L245
	cmpq	$0, 80(%rax)
	movq	%rax, 88(%rax)
	leaq	120(%r9), %rsi
	je	.L208
	leaq	_ZZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L242:
	leaq	_ZZNK4node11UDPWrapBase8listenerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L243:
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L244:
	leaq	_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L245:
	movq	1256(%r11), %rax
	movsd	-56(%rbp), %xmm5
	movsd	%xmm5, 24(%rax)
	movq	$-38, %rax
	jmp	.L185
	.cfi_endproc
.LFE9828:
	.size	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr.part.0, .-_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr.part.0
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr
	.type	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr, @function
_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr:
.LFB7644:
	.cfi_startproc
	endbr64
	movl	72(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L247
	jmp	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr.part.0
.L247:
	movq	$-9, %rax
	ret
	.cfi_endproc
.LFE7644:
	.size	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr, .-_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr
	.p2align 4
	.globl	_ZThn88_N4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr
	.type	_ZThn88_N4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr, @function
_ZThn88_N4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr:
.LFB9857:
	.cfi_startproc
	endbr64
	movl	-16(%rdi), %eax
	leaq	-88(%rdi), %r8
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L249
	movq	%r8, %rdi
	jmp	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr.part.0
.L249:
	movq	$-9, %rax
	ret
	.cfi_endproc
.LFE9857:
	.size	_ZThn88_N4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr, .-_ZThn88_N4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7617:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L251
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L266
.L251:
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L259
	cmpw	$1040, %cx
	jne	.L252
.L259:
	movq	23(%rdx), %r13
.L254:
	movq	8(%rbx), %r12
	movl	$352, %edi
	call	_Znwm@PLT
	movl	$35, %r8d
	movq	%r13, %rsi
	leaq	120(%rax), %r14
	addq	$8, %r12
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%r14, %rcx
	movq	%r12, %rdx
	leaq	88(%rbx), %r15
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	$0, 96(%rbx)
	movq	%rax, (%rbx)
	addq	$192, %rax
	movq	%rax, 88(%rbx)
	addq	$80, %rax
	movq	%rax, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 344(%rbx)
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	360(%r13), %rax
	movq	%r14, %rsi
	movq	2360(%rax), %rdi
	call	uv_udp_init@PLT
	testl	%eax, %eax
	jne	.L267
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L256
	movq	$0, 8(%rax)
.L256:
	leaq	104(%rbx), %rax
	cmpq	$0, 112(%rbx)
	movq	%rax, 96(%rbx)
	jne	.L268
	movq	%r15, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	jne	.L251
	leaq	_ZZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L252:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	_ZZN4node7UDPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	_ZZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7617:
	.size	_ZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPWrapBase8RecvStopERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11UDPWrapBase8RecvStopERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11UDPWrapBase8RecvStopERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7657:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	cmpl	$1, %eax
	jle	.L281
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L278
	cmpw	$1040, %cx
	jne	.L271
.L278:
	movq	31(%rdx), %rdi
.L273:
	movabsq	$-38654705664, %rax
	movq	(%rbx), %rbx
	testq	%rdi, %rdi
	je	.L274
	movq	(%rdi), %rdx
	leaq	_ZThn88_N4node7UDPWrap8RecvStopEv(%rip), %rcx
	movq	24(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L275
	movl	-16(%rdi), %esi
	leal	-1(%rsi), %edx
	cmpl	$1, %edx
	jbe	.L274
	addq	$32, %rdi
	call	uv_udp_recv_stop@PLT
	salq	$32, %rax
.L274:
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L271:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L275:
	call	*%rdx
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	.cfi_restore_state
	leaq	_ZZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7657:
	.size	_ZN4node11UDPWrapBase8RecvStopERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11UDPWrapBase8RecvStopERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPWrapBase9RecvStartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11UDPWrapBase9RecvStartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11UDPWrapBase9RecvStartERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	cmpl	$1, %eax
	jle	.L295
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L292
	cmpw	$1040, %cx
	jne	.L284
.L292:
	movq	31(%rdx), %rdi
.L286:
	movabsq	$-38654705664, %rax
	movq	(%rbx), %rbx
	testq	%rdi, %rdi
	je	.L287
	movq	(%rdi), %rdx
	leaq	_ZThn88_N4node7UDPWrap9RecvStartEv(%rip), %rcx
	movq	16(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L288
	movl	-16(%rdi), %esi
	leal	-1(%rsi), %edx
	cmpl	$1, %edx
	jbe	.L287
	addq	$32, %rdi
	leaq	_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj(%rip), %rdx
	leaq	_ZN4node7UDPWrap7OnAllocEP11uv_handle_smP8uv_buf_t(%rip), %rsi
	call	uv_udp_recv_start@PLT
	movl	%eax, %edx
	movq	%rdx, %rax
	salq	$32, %rax
	cmpl	$-114, %edx
	movl	$0, %edx
	cmove	%rdx, %rax
.L287:
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	.cfi_restore_state
	movq	%r12, %rdi
	movl	$1, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rdi
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L288:
	call	*%rdx
	salq	$32, %rax
	movq	%rax, 24(%rbx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	.cfi_restore_state
	leaq	_ZZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7655:
	.size	_ZN4node11UDPWrapBase9RecvStartERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11UDPWrapBase9RecvStartERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi
	.type	_ZN4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi, @function
_ZN4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi:
.LFB7659:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rsi)
	jne	.L313
.L297:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L314
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L313:
	.cfi_restore_state
	movq	16(%rsi), %rbx
	leaq	-112(%rbp), %r15
	movl	%edx, %r13d
	movq	%r15, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	movl	408(%r12), %esi
	movq	%rax, -80(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -72(%rbp)
	movq	360(%rbx), %rax
	movq	1136(%rax), %r13
	testq	%rdi, %rdi
	je	.L298
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L315
.L298:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L301
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L316
.L301:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L315:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L316:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L301
.L314:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7659:
	.size	_ZN4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi, .-_ZN4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi
	.set	.LTHUNK7,_ZN4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi
	.p2align 4
	.type	_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrENUlP13uv_udp_send_siE_4_FUNES7_i, @function
_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrENUlP13uv_udp_send_siE_4_FUNES7_i:
.LFB7647:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	movq	-24(%rax), %r8
	testq	%r8, %r8
	je	.L323
	movq	(%r8), %rax
	leaq	_ZThn104_N4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi(%rip), %rcx
	movl	%esi, %edx
	leaq	-88(%rdi), %rsi
	movq	40(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L319
	leaq	-104(%r8), %rdi
	jmp	.LTHUNK7
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%r8, %rdi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L323:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node11UDPWrapBase8listenerEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7647:
	.size	_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrENUlP13uv_udp_send_siE_4_FUNES7_i, .-_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrENUlP13uv_udp_send_siE_4_FUNES7_i
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj
	.type	_ZN4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj, @function
_ZN4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj:
.LFB7665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	16(%rdi), %rbx
	movq	(%rdx), %r14
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	8(%rdx), %rax
	movq	%rax, -168(%rbp)
	movq	%rcx, %rax
	orq	%rsi, %rax
	jne	.L367
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	testq	%r14, %r14
	je	.L324
	testq	%rbx, %rbx
	je	.L368
.L331:
	movq	360(%rbx), %rax
	movq	-168(%rbp), %rdx
	movq	%r14, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
.L324:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L369
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L367:
	.cfi_restore_state
	leaq	-128(%rbp), %rax
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	%rcx, %r13
	movq	352(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %rax
	movq	%rax, %rdi
	movq	%rax, -176(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rdi
	movl	%r15d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	%rax, -96(%rbp)
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L326
	movzbl	11(%rax), %edx
	andl	$7, %edx
	cmpb	$2, %dl
	je	.L370
.L326:
	movq	%rax, -88(%rbp)
	movq	352(%rbx), %rax
	addq	$88, %rax
	movq	%rax, -80(%rbp)
	movq	%rax, -72(%rbp)
	testq	%r15, %r15
	js	.L371
	movl	$1, %ecx
	movq	-168(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	cmovne	%r15, %rcx
	call	_ZN4node11Environment10ReallocateEPcmm@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L372
	movl	%r15d, %esi
	call	uv_buf_init@PLT
	xorl	%ecx, %ecx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	movq	%rax, -152(%rbp)
	movq	%rax, %r14
	movq	%rdi, -144(%rbp)
	movq	%rdi, %rdx
	movq	%rdi, %r15
	movq	%rbx, %rdi
	call	_ZN4node6Buffer3NewEPNS_11EnvironmentEPcmb@PLT
	movq	%rax, -168(%rbp)
	testq	%rax, %rax
	je	.L334
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	uv_buf_init@PLT
	movq	-168(%rbp), %rcx
	movq	%rdx, %rdi
	movq	%rax, -152(%rbp)
	movq	%rax, %r14
	movq	%rdi, -144(%rbp)
	movq	%rdi, %r15
.L344:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -80(%rbp)
	call	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE@PLT
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -72(%rbp)
	movq	360(%rbx), %rax
	movq	1200(%rax), %r13
	testq	%rdi, %rdi
	je	.L335
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L373
.L335:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L338
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L374
.L338:
	movq	-176(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-184(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	testq	%r14, %r14
	je	.L324
	movq	360(%rbx), %rax
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	2368(%rax), %rdi
	movq	(%rdi), %rax
	call	*32(%rax)
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L370:
	movq	16(%r12), %rdx
	movq	(%rax), %rsi
	movq	352(%rdx), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L371:
	movq	360(%rbx), %rax
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	1200(%rax), %r13
	testq	%rdi, %rdi
	je	.L328
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L375
.L328:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L340
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L376
.L340:
	movq	-176(%rbp), %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-184(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	uv_buf_init@PLT
	movq	%rax, -152(%rbp)
	movq	%rdx, -144(%rbp)
	testq	%r14, %r14
	jne	.L331
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L373:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L374:
	leaq	-96(%rbp), %rcx
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L334:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-168(%rbp), %rcx
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L376:
	leaq	-96(%rbp), %rcx
	movl	$4, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L368:
	leaq	_ZZN4node15AllocatedBuffer5clearEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L372:
	leaq	_ZZN4node15AllocatedBuffer6ResizeEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L375:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L328
.L369:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7665:
	.size	_ZN4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj, .-_ZN4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj
	.set	.LTHUNK9,_ZN4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj
	.p2align 4
	.globl	_ZThn104_N4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj
	.type	_ZThn104_N4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj, @function
_ZThn104_N4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj:
.LFB9877:
	.cfi_startproc
	endbr64
	subq	$104, %rdi
	jmp	.LTHUNK9
	.cfi_endproc
.LFE9877:
	.size	_ZThn104_N4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj, .-_ZThn104_N4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj
	.type	_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj, @function
_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj:
.LFB7664:
	.cfi_startproc
	endbr64
	movq	-24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L383
	movq	(%rdi), %rax
	leaq	_ZThn104_N4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj(%rip), %r9
	movq	24(%rax), %rax
	cmpq	%r9, %rax
	jne	.L379
	subq	$104, %rdi
	jmp	.LTHUNK9
	.p2align 4,,10
	.p2align 3
.L379:
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L383:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node11UDPWrapBase8listenerEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7664:
	.size	_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj, .-_ZN4node7UDPWrap6OnRecvEP8uv_udp_slPK8uv_buf_tPK8sockaddrj
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movl	$-9, -44(%rbp)
	leaq	8(%r12), %r13
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L396
	movq	8(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L391
	cmpw	$1040, %cx
	jne	.L386
.L391:
	movq	23(%rdx), %rax
.L388:
	testq	%rax, %rax
	je	.L389
	leaq	-44(%rbp), %rsi
	leaq	120(%rax), %rdi
	call	uv_fileno@PLT
.L389:
	movslq	-44(%rbp), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L397
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L388
	.p2align 4,,10
	.p2align 3
.L396:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L397:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7618:
	.size	_ZN4node7UDPWrap5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7631:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L408
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L406
	cmpw	$1040, %cx
	jne	.L400
.L406:
	movq	23(%rdx), %rax
.L402:
	testq	%rax, %rax
	je	.L409
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jne	.L410
	leaq	120(%rax), %rdi
	xorl	%esi, %esi
	call	uv_udp_connect@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L400:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L409:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	_ZZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7631:
	.size	_ZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7632:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L423
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L420
	cmpw	$1040, %cx
	jne	.L413
.L420:
	movq	23(%rdx), %r12
.L415:
	cmpl	$1, 16(%rbx)
	movq	16(%r12), %rax
	je	.L424
	leaq	_ZZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L424:
	movq	3280(%rax), %rsi
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L411
	sarq	$32, %rsi
	leaq	120(%r12), %rdi
	call	uv_udp_set_ttl@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L411:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L413:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L423:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7632:
	.size	_ZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7634:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L437
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L434
	cmpw	$1040, %cx
	jne	.L427
.L434:
	movq	23(%rdx), %r12
.L429:
	cmpl	$1, 16(%rbx)
	movq	16(%r12), %rax
	je	.L438
	leaq	_ZZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L438:
	movq	3280(%rax), %rsi
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L425
	sarq	$32, %rsi
	leaq	120(%r12), %rdi
	call	uv_udp_set_multicast_ttl@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L425:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L427:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7634:
	.size	_ZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L451
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L448
	cmpw	$1040, %cx
	jne	.L441
.L448:
	movq	23(%rdx), %r12
.L443:
	cmpl	$1, 16(%rbx)
	movq	16(%r12), %rax
	je	.L452
	leaq	_ZZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L452:
	movq	3280(%rax), %rsi
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L439
	sarq	$32, %rsi
	leaq	120(%r12), %rdi
	call	uv_udp_set_multicast_loop@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L439:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L441:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L451:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7635:
	.size	_ZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7633:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L465
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L462
	cmpw	$1040, %cx
	jne	.L455
.L462:
	movq	23(%rdx), %r12
.L457:
	cmpl	$1, 16(%rbx)
	movq	16(%r12), %rax
	je	.L466
	leaq	_ZZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L466:
	movq	3280(%rax), %rsi
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rsi
	testb	%al, %al
	je	.L453
	sarq	$32, %rsi
	leaq	120(%r12), %rdi
	call	uv_udp_set_broadcast@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L453:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	.cfi_restore_state
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L465:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7633:
	.size	_ZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7625:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L481
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L479
	cmpw	$1040, %cx
	jne	.L469
.L479:
	movq	23(%rdx), %r12
.L471:
	testq	%r12, %r12
	je	.L482
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L483
	movq	8(%rbx), %rdi
.L475:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L484
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L477
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L478:
	call	_ZNK2v87Integer5ValueEv@PLT
	leaq	120(%r12), %rdi
	movq	%rax, %rsi
	call	uv_udp_open@PLT
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L483:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L477:
	movq	8(%rbx), %rdi
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L469:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L482:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L481:
	.cfi_restore_state
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L484:
	leaq	_ZZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7625:
	.size	_ZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8388:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L501
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L499
	cmpw	$1040, %cx
	jne	.L487
.L499:
	movq	23(%rdx), %r13
.L489:
	testq	%r13, %r13
	je	.L502
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L503
	movq	8(%rbx), %rdi
.L493:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L504
	leaq	-176(%rbp), %r14
	leaq	120(%r13), %rdi
	movl	$128, -180(%rbp)
	leaq	-180(%rbp), %rdx
	movq	%r14, %rsi
	call	uv_udp_getpeername@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L495
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L505
	movq	8(%rbx), %rdx
.L497:
	movq	16(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE@PLT
.L495:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
.L485:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L506
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L503:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L493
	.p2align 4,,10
	.p2align 3
.L505:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L497
	.p2align 4,,10
	.p2align 3
.L487:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L502:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L501:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L506:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8388:
	.size	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.text._ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,"axG",@progbits,_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE,comdat
	.p2align 4
	.weak	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB8389:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$160, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L523
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L521
	cmpw	$1040, %cx
	jne	.L509
.L521:
	movq	23(%rdx), %r13
.L511:
	testq	%r13, %r13
	je	.L524
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L525
	movq	8(%rbx), %rdi
.L515:
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L526
	leaq	-176(%rbp), %r14
	leaq	120(%r13), %rdi
	movl	$128, -180(%rbp)
	leaq	-180(%rbp), %rdx
	movq	%r14, %rsi
	call	uv_udp_getsockname@PLT
	movl	%eax, %r12d
	testl	%eax, %eax
	jne	.L517
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L527
	movq	8(%rbx), %rdx
.L519:
	movq	16(%r13), %rdi
	movq	%r14, %rsi
	call	_ZN4node11AddressToJSEPNS_11EnvironmentEPK8sockaddrN2v85LocalINS5_6ObjectEEE@PLT
.L517:
	movq	(%rbx), %rax
	salq	$32, %r12
	movq	%r12, 24(%rax)
.L507:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L528
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L527:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L509:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L524:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L523:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L526:
	leaq	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L528:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE8389:
	.size	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.text
	.p2align 4
	.globl	_ZThn104_N4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi
	.type	_ZThn104_N4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi, @function
_ZThn104_N4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi:
.LFB9862:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$72, %rsp
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	cmpb	$0, 416(%rsi)
	jne	.L546
.L530:
	movq	(%r12), %rax
	movq	%r12, %rdi
	call	*8(%rax)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L547
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	.cfi_restore_state
	movq	16(%rsi), %rbx
	leaq	-112(%rbp), %r15
	movl	%edx, %r13d
	movq	%r15, %rdi
	movq	352(%rbx), %rsi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %r14
	movq	%r14, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	movq	352(%rbx), %rdi
	movl	%r13d, %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	352(%rbx), %rdi
	movl	408(%r12), %esi
	movq	%rax, -80(%rbp)
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	8(%r12), %rdi
	movq	16(%r12), %rdx
	movq	%rax, -72(%rbp)
	movq	360(%rbx), %rax
	movq	1136(%rax), %r13
	testq	%rdi, %rdi
	je	.L531
	movzbl	11(%rdi), %eax
	andl	$7, %eax
	cmpb	$2, %al
	je	.L548
.L531:
	movq	3280(%rdx), %rsi
	movq	%r13, %rdx
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEE@PLT
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L534
	movq	%rax, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L549
.L534:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L548:
	movq	352(%rdx), %r8
	movq	(%rdi), %rsi
	movq	%r8, %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	16(%r12), %rdx
	movq	%rax, %rdi
	jmp	.L531
	.p2align 4,,10
	.p2align 3
.L549:
	leaq	-80(%rbp), %rcx
	movl	$2, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_ZN4node9AsyncWrap12MakeCallbackEN2v85LocalINS1_8FunctionEEEiPNS2_INS1_5ValueEEE@PLT
	jmp	.L534
.L547:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE9862:
	.size	_ZThn104_N4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi, .-_ZThn104_N4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$1056, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L570
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L562
	cmpw	$1040, %cx
	jne	.L552
.L562:
	movq	23(%rdx), %r12
.L554:
	testq	%r12, %r12
	je	.L571
	cmpl	$1, 16(%rbx)
	jne	.L572
	movq	8(%rbx), %rdx
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L558
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L558
	movq	(%rbx), %rax
	leaq	-1072(%rbp), %rdi
	movq	8(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1056(%rbp), %rsi
	leaq	120(%r12), %rdi
	call	uv_udp_set_multicast_interface@PLT
	movq	(%rbx), %rdx
	movq	-1056(%rbp), %rdi
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L550
	testq	%rdi, %rdi
	je	.L550
	call	free@PLT
.L550:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L573
	addq	$1056, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	.cfi_restore_state
	leaq	_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L554
	.p2align 4,,10
	.p2align 3
.L571:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L570:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L573:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7636:
	.size	_ZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"uv_recv_buffer_size"
.LC3:
	.string	"uv_send_buffer_size"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L608
	cmpw	$1040, %cx
	jne	.L575
.L608:
	movq	%r12, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L612
.L578:
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L609
	cmpw	$1040, %cx
	jne	.L579
.L609:
	movq	23(%rdx), %r12
.L581:
	testq	%r12, %r12
	je	.L613
	movl	16(%rbx), %edi
	testl	%edi, %edi
	jle	.L614
	movq	8(%rbx), %rdi
.L585:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L615
	cmpl	$1, 16(%rbx)
	jg	.L587
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L588:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L616
	cmpl	$1, 16(%rbx)
	jg	.L590
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L591:
	call	_ZNK2v87Boolean5ValueEv@PLT
	movl	16(%rbx), %esi
	leaq	.LC2(%rip), %r15
	testb	%al, %al
	movl	%eax, %r14d
	leaq	.LC3(%rip), %rax
	cmove	%rax, %r15
	testl	%esi, %esi
	jg	.L593
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	je	.L617
.L595:
	movl	16(%rbx), %ecx
	addq	$120, %r12
	testl	%ecx, %ecx
	jle	.L618
	movq	8(%rbx), %rdi
.L599:
	call	_ZNK2v86Uint325ValueEv@PLT
	leaq	-60(%rbp), %rsi
	movq	%r12, %rdi
	movl	%eax, -60(%rbp)
	testb	%r14b, %r14b
	je	.L600
	call	uv_recv_buffer_size@PLT
	testl	%eax, %eax
	jne	.L619
.L602:
	movslq	-60(%rbp), %rax
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	%rax, 24(%rdx)
.L574:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L620
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L587:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L590:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L593:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value7IsInt32Ev@PLT
	testb	%al, %al
	jne	.L595
.L617:
	cmpl	$2, 16(%rbx)
	jle	.L621
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rsi
.L597:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	pushq	$0
	movl	$-22, %edx
.L611:
	movq	%r13, %rdi
	call	_ZN4node11Environment22CollectUVExceptionInfoEN2v85LocalINS1_5ValueEEEiPKcS6_S6_S6_@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	movq	88(%rdx), %rdx
	movq	%rdx, 24(%rax)
	popq	%rax
	popq	%rdx
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L618:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L621:
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	addq	$88, %rsi
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L575:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L578
	.p2align 4,,10
	.p2align 3
.L612:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L579:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L600:
	call	uv_send_buffer_size@PLT
	testl	%eax, %eax
	je	.L602
.L619:
	cmpl	$2, 16(%rbx)
	jg	.L603
	movq	(%rbx), %rdx
	movq	8(%rdx), %rsi
	addq	$88, %rsi
.L604:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	pushq	$0
	movl	%eax, %edx
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L613:
	movabsq	$-38654705664, %rsi
	movq	(%rbx), %rax
	movq	%rsi, 24(%rax)
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L615:
	leaq	_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L603:
	movq	8(%rbx), %rsi
	subq	$16, %rsi
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L616:
	leaq	_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L620:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7628:
	.size	_ZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1
.LC4:
	.string	"UDP"
.LC5:
	.string	"recvStart"
.LC6:
	.string	"recvStop"
.LC7:
	.string	"open"
.LC8:
	.string	"bind"
.LC9:
	.string	"connect"
.LC10:
	.string	"send"
.LC11:
	.string	"bind6"
.LC12:
	.string	"connect6"
.LC13:
	.string	"send6"
.LC14:
	.string	"disconnect"
.LC15:
	.string	"getpeername"
.LC16:
	.string	"getsockname"
.LC17:
	.string	"addMembership"
.LC18:
	.string	"dropMembership"
.LC19:
	.string	"addSourceSpecificMembership"
.LC20:
	.string	"dropSourceSpecificMembership"
.LC21:
	.string	"setMulticastInterface"
.LC22:
	.string	"setMulticastTTL"
.LC23:
	.string	"setMulticastLoopback"
.LC24:
	.string	"setBroadcast"
.LC25:
	.string	"setTTL"
.LC26:
	.string	"bufferSize"
.LC27:
	.string	"SendWrap"
.LC28:
	.string	"UV_UDP_IPV6ONLY"
	.section	.text.unlikely,"ax",@progbits
	.align 2
.LCOLDB30:
	.text
.LHOTB30:
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7616:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L623
	movq	%rdi, %r14
	movq	%rdx, %rdi
	movq	%rdx, %r13
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L623
	movq	0(%r13), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L623
	movq	271(%rax), %rbx
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %r9d
	leaq	_ZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$2, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$3, %ecx
	leaq	.LC4(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L670
.L624:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	movl	$1, %r9d
	leaq	_ZN4node7UDPWrap5GetFDERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rdi
	movq	%rax, -56(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	movq	360(%rbx), %rax
	movl	$5, %r8d
	movq	728(%rax), %rsi
	call	_ZN2v88Template19SetAccessorPropertyENS_5LocalINS_4NameEEENS1_INS_16FunctionTemplateEEES5_NS_17PropertyAttributeENS_13AccessControlE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node11UDPWrapBase9RecvStartERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L671
.L625:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11UDPWrapBase8RecvStopERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L672
.L626:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L673
.L627:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC8(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L674
.L628:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC9(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L675
.L629:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap4SendERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L676
.L630:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L677
.L631:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC12(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L678
.L632:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap5Send6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L679
.L633:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC14(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L680
.L634:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC15(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L681
.L635:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC16(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L682
.L636:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap13AddMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC17(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L683
.L637:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap14DropMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC18(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L684
.L638:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap27AddSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC19(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L685
.L639:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap28DropSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC20(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L686
.L640:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC21(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L687
.L641:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC22(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L688
.L642:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC23(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L689
.L643:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC24(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L690
.L644:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC25(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L691
.L645:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC26(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L692
.L646:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%rbx, %rdi
	call	_ZN4node10HandleWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L693
.L647:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L694
.L648:
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L695
	movq	3040(%rbx), %rdi
	movq	352(%rbx), %r15
	testq	%rdi, %rdi
	je	.L658
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3040(%rbx)
.L658:
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 3040(%rbx)
.L650:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZZN4node10BaseObject31MakeLazilyInitializedJSTemplateEPNS_11EnvironmentEENUlRKN2v820FunctionCallbackInfoINS3_5ValueEEEE_4_FUNES8_(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	%rbx, %rdi
	call	_ZN4node9AsyncWrap22GetConstructorTemplateEPNS_11EnvironmentE@PLT
	movq	%r12, %rdi
	movq	%rax, %rsi
	call	_ZN2v816FunctionTemplate7InheritENS_5LocalIS0_EE@PLT
	movq	352(%rbx), %rdi
	xorl	%edx, %edx
	movl	$8, %ecx
	leaq	.LC27(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	popq	%rax
	popq	%rdx
	testq	%r15, %r15
	je	.L696
.L652:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L697
.L653:
	movq	3280(%rbx), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L698
.L654:
	movq	352(%rbx), %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %r15
	movq	%rax, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r15, %rdi
	leaq	.LC28(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L699
.L655:
	movsd	.LC29(%rip), %xmm0
	movq	%r15, %rdi
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L700
.L656:
	movq	360(%rbx), %rax
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r14, %rdi
	movq	352(%rax), %rdx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L701
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L670:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L672:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L673:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L674:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L675:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L676:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L677:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L680:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L681:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L682:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L683:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L685:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L687:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L688:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L689:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L690:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L644
	.p2align 4,,10
	.p2align 3
.L691:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L692:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L694:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L695:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	3040(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L650
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 3040(%rbx)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L696:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L698:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L700:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L701:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7616:
.L623:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	2680, %rax
	ud2
	.cfi_endproc
.LFE7616:
	.text
	.size	_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE30:
	.text
.LHOTE30:
	.align 2
	.p2align 4
	.globl	_ZN4node8SendWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.type	_ZN4node8SendWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb, @function
_ZN4node8SendWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb:
.LFB7588:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%ecx, %r13d
	movl	$34, %ecx
	pushq	%r12
	.cfi_offset 12, -32
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$8, %rsp
	movsd	.LC0(%rip), %xmm0
	call	_ZN4node9AsyncWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEENS0_12ProviderTypeEd@PLT
	leaq	16+_ZTVN4node11ReqWrapBaseE(%rip), %rax
	cmpb	$0, 1928(%r12)
	movq	%rax, 56(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 64(%rbx)
	movq	%rax, 72(%rbx)
	je	.L705
	movq	2112(%r12), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 2112(%r12)
	leaq	16+_ZTVN4node8SendWrapE(%rip), %rax
	movq	%rdx, 64(%rbx)
	leaq	2112(%r12), %rdx
	movq	%rax, (%rbx)
	addq	$112, %rax
	movb	%r13b, 416(%rbx)
	movq	%rdx, 72(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 88(%rbx)
	movq	%rax, 56(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L705:
	.cfi_restore_state
	leaq	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7588:
	.size	_ZN4node8SendWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb, .-_ZN4node8SendWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.globl	_ZN4node8SendWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.set	_ZN4node8SendWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb,_ZN4node8SendWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEb
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPListenerD2Ev
	.type	_ZN4node11UDPListenerD2Ev, @function
_ZN4node11UDPListenerD2Ev:
.LFB7592:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L706
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L708
	movq	$0, 8(%rdx)
.L708:
	movq	$0, 8(%rax)
.L706:
	ret
	.cfi_endproc
.LFE7592:
	.size	_ZN4node11UDPListenerD2Ev, .-_ZN4node11UDPListenerD2Ev
	.globl	_ZN4node11UDPListenerD1Ev
	.set	_ZN4node11UDPListenerD1Ev,_ZN4node11UDPListenerD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPListenerD0Ev
	.type	_ZN4node11UDPListenerD0Ev, @function
_ZN4node11UDPListenerD0Ev:
.LFB7594:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node11UDPListenerE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L716
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L717
	movq	$0, 8(%rdx)
.L717:
	movq	$0, 8(%rax)
.L716:
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7594:
	.size	_ZN4node11UDPListenerD0Ev, .-_ZN4node11UDPListenerD0Ev
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPWrapBaseD2Ev
	.type	_ZN4node11UDPWrapBaseD2Ev, @function
_ZN4node11UDPWrapBaseD2Ev:
.LFB7596:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L724
	movq	$0, 8(%rax)
.L724:
	ret
	.cfi_endproc
.LFE7596:
	.size	_ZN4node11UDPWrapBaseD2Ev, .-_ZN4node11UDPWrapBaseD2Ev
	.globl	_ZN4node11UDPWrapBaseD1Ev
	.set	_ZN4node11UDPWrapBaseD1Ev,_ZN4node11UDPWrapBaseD2Ev
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPWrapBaseD0Ev
	.type	_ZN4node11UDPWrapBaseD0Ev, @function
_ZN4node11UDPWrapBaseD0Ev:
.LFB7598:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L730
	movq	$0, 8(%rax)
.L730:
	movl	$16, %esi
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7598:
	.size	_ZN4node11UDPWrapBaseD0Ev, .-_ZN4node11UDPWrapBaseD0Ev
	.align 2
	.p2align 4
	.globl	_ZNK4node11UDPWrapBase8listenerEv
	.type	_ZNK4node11UDPWrapBase8listenerEv, @function
_ZNK4node11UDPWrapBase8listenerEv:
.LFB7599:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L739
	ret
	.p2align 4,,10
	.p2align 3
.L739:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZNK4node11UDPWrapBase8listenerEvE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7599:
	.size	_ZNK4node11UDPWrapBase8listenerEv, .-_ZNK4node11UDPWrapBase8listenerEv
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerE
	.type	_ZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerE, @function
_ZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerE:
.LFB7600:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L741
	movq	$0, 8(%rax)
.L741:
	movq	%rsi, 8(%rdi)
	testq	%rsi, %rsi
	je	.L740
	cmpq	$0, 8(%rsi)
	jne	.L753
	movq	%rdi, 8(%rsi)
.L740:
	ret
	.p2align 4,,10
	.p2align 3
.L753:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerEE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7600:
	.size	_ZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerE, .-_ZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerE
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEE
	.type	_ZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEE, @function
_ZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEE:
.LFB7601:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	cmpl	$1, %eax
	jle	.L762
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L759
	cmpw	$1040, %cx
	jne	.L756
.L759:
	movq	31(%rdx), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L756:
	.cfi_restore_state
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$1, %esi
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	.p2align 4,,10
	.p2align 3
.L762:
	.cfi_restore_state
	leaq	_ZZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7601:
	.size	_ZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEE, .-_ZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11UDPWrapBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE
	.type	_ZN4node11UDPWrapBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE, @function
_ZN4node11UDPWrapBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE:
.LFB7602:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	352(%rdi), %rdi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node11UDPWrapBase9RecvStartERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r13
	testq	%rax, %rax
	je	.L767
.L764:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	movq	2680(%rbx), %rdx
	movq	%rax, %rcx
	leaq	_ZN4node11UDPWrapBase8RecvStopERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	352(%rbx), %rdi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rax, %r14
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r13
	popq	%rax
	popq	%rdx
	testq	%r13, %r13
	je	.L768
.L765:
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	%r13, %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	leaq	-32(%rbp), %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	.p2align 4,,10
	.p2align 3
.L767:
	.cfi_restore_state
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L764
	.p2align 4,,10
	.p2align 3
.L768:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L765
	.cfi_endproc
.LFE7602:
	.size	_ZN4node11UDPWrapBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE, .-_ZN4node11UDPWrapBase10AddMethodsEPNS_11EnvironmentEN2v85LocalINS3_16FunctionTemplateEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.type	_ZN4node7UDPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, @function
_ZN4node7UDPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE:
.LFB7614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$35, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	120(%rdi), %r14
	pushq	%r13
	movq	%r14, %rcx
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	leaq	88(%rbx), %r15
	subq	$8, %rsp
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%r15, %rdx
	leaq	16+_ZTVN4node7UDPWrapE(%rip), %rax
	movq	$0, 96(%rbx)
	movq	%rax, (%rbx)
	addq	$192, %rax
	movq	%rax, 88(%rbx)
	addq	$80, %rax
	movq	%rax, 104(%rbx)
	movq	$0, 112(%rbx)
	movq	$0, 344(%rbx)
	call	_ZN2v86Object32SetAlignedPointerInInternalFieldEiPv@PLT
	movq	360(%r12), %rax
	movq	%r14, %rsi
	movq	2360(%rax), %rdi
	call	uv_udp_init@PLT
	testl	%eax, %eax
	jne	.L780
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L771
	movq	$0, 8(%rax)
.L771:
	leaq	104(%rbx), %rax
	cmpq	$0, 112(%rbx)
	movq	%rax, 96(%rbx)
	jne	.L781
	movq	%r15, 112(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L780:
	.cfi_restore_state
	leaq	_ZZN4node7UDPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L781:
	leaq	_ZZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7614:
	.size	_ZN4node7UDPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE, .-_ZN4node7UDPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.globl	_ZN4node7UDPWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.set	_ZN4node7UDPWrapC1EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE,_ZN4node7UDPWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEE
	.p2align 4
	.globl	_ZN4node19sockaddr_for_familyEiPKctP16sockaddr_storage
	.type	_ZN4node19sockaddr_for_familyEiPKctP16sockaddr_storage, @function
_ZN4node19sockaddr_for_familyEiPKctP16sockaddr_storage:
.LFB7619:
	.cfi_startproc
	endbr64
	movq	%rsi, %r8
	cmpl	$2, %edi
	je	.L783
	cmpl	$10, %edi
	jne	.L790
	movzwl	%dx, %esi
	movq	%r8, %rdi
	movq	%rcx, %rdx
	jmp	uv_ip6_addr@PLT
	.p2align 4,,10
	.p2align 3
.L783:
	movzwl	%dx, %esi
	movq	%r8, %rdi
	movq	%rcx, %rdx
	jmp	uv_ip4_addr@PLT
.L790:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZZN4node19sockaddr_for_familyEiPKctP16sockaddr_storageE4args(%rip), %rdi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7619:
	.size	_ZN4node19sockaddr_for_familyEiPKctP16sockaddr_storage, .-_ZN4node19sockaddr_for_familyEiPKctP16sockaddr_storage
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB7620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1208, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L831
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L817
	cmpw	$1040, %cx
	jne	.L793
.L817:
	movq	23(%rdx), %r14
.L795:
	testq	%r14, %r14
	je	.L832
	cmpl	$3, 16(%rbx)
	jne	.L833
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	-1104(%rbp), %rdi
	movq	8(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	cmpl	$1, 16(%rbx)
	movq	%rax, %r13
	jle	.L834
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L800:
	movq	%r13, %rsi
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L801
	cmpl	$2, 16(%rbx)
	jg	.L802
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L803:
	shrq	$32, %rax
	movq	%r13, %rsi
	movq	%rax, %r15
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L801
	shrq	$32, %rax
	movq	-1088(%rbp), %rdi
	movzwl	%r15w, %esi
	movq	%rax, %r13
	cmpl	$2, %r12d
	je	.L804
	cmpl	$10, %r12d
	jne	.L835
	leaq	-1232(%rbp), %r12
	movq	%r12, %rdx
	call	uv_ip6_addr@PLT
.L810:
	testl	%eax, %eax
	je	.L811
.L829:
	salq	$32, %rax
.L812:
	movq	(%rbx), %rdx
	movq	-1088(%rbp), %rdi
	movq	%rax, 24(%rdx)
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L791
	testq	%rdi, %rdi
	jne	.L830
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L834:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L800
	.p2align 4,,10
	.p2align 3
.L801:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L791
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L791
.L830:
	call	free@PLT
.L791:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L836
	addq	$1208, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L802:
	.cfi_restore_state
	movq	8(%rbx), %rcx
	leaq	-16(%rcx), %rdi
	jmp	.L803
	.p2align 4,,10
	.p2align 3
.L804:
	leaq	-1232(%rbp), %r12
	movq	%r12, %rdx
	call	uv_ip4_addr@PLT
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L793:
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L795
	.p2align 4,,10
	.p2align 3
.L811:
	leaq	120(%r14), %rdi
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	uv_udp_bind@PLT
	testl	%eax, %eax
	jne	.L829
	movq	96(%r14), %rdi
	testq	%rdi, %rdi
	je	.L837
	movq	(%rdi), %rax
	leaq	_ZN4node11UDPListener11OnAfterBindEv(%rip), %rcx
	movq	48(%rax), %rdx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	je	.L812
	movq	%rax, -1240(%rbp)
	call	*%rdx
	movq	-1240(%rbp), %rax
	jmp	.L812
	.p2align 4,,10
	.p2align 3
.L832:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L791
	.p2align 4,,10
	.p2align 3
.L831:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L833:
	leaq	_ZZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L837:
	leaq	_ZZNK4node11UDPWrapBase8listenerEvE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L836:
	call	__stack_chk_fail@PLT
.L835:
	leaq	_ZZN4node19sockaddr_for_familyEiPKctP16sockaddr_storageE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7620:
	.size	_ZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7626:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.cfi_endproc
.LFE7626:
	.size	_ZN4node7UDPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap4BindERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7627:
	.cfi_startproc
	endbr64
	movl	$10, %esi
	jmp	_ZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.cfi_endproc
.LFE7627:
	.size	_ZN4node7UDPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap5Bind6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB7624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movl	%esi, %r12d
	pushq	%rbx
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$1192, %rsp
	movq	(%rdi), %r13
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L873
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L862
	cmpw	$1040, %cx
	jne	.L842
.L862:
	movq	23(%rdx), %r13
.L844:
	testq	%r13, %r13
	je	.L874
	cmpl	$2, 16(%rbx)
	jne	.L875
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	-1088(%rbp), %rdi
	movq	8(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	cmpl	$1, 16(%rbx)
	movq	%rax, %rsi
	jle	.L876
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L849:
	call	_ZNK2v85Value11Uint32ValueENS_5LocalINS_7ContextEEE@PLT
	testb	%al, %al
	je	.L850
	shrq	$32, %rax
	movq	-1072(%rbp), %rdi
	movzwl	%ax, %esi
	cmpl	$2, %r12d
	jne	.L877
	leaq	-1216(%rbp), %r12
	movq	%r12, %rdx
	call	uv_ip4_addr@PLT
	testl	%eax, %eax
	je	.L878
.L855:
	movq	(%rbx), %rdx
	salq	$32, %rax
	movq	-1072(%rbp), %rdi
	movq	%rax, 24(%rdx)
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L840
.L872:
	testq	%rdi, %rdi
	je	.L840
	call	free@PLT
.L840:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L879
	addq	$1192, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L876:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L877:
	cmpl	$10, %r12d
	jne	.L880
	leaq	-1216(%rbp), %r12
	movq	%r12, %rdx
	call	uv_ip6_addr@PLT
	testl	%eax, %eax
	jne	.L855
.L878:
	leaq	120(%r13), %rdi
	movq	%r12, %rsi
	call	uv_udp_connect@PLT
	jmp	.L855
	.p2align 4,,10
	.p2align 3
.L850:
	movq	-1072(%rbp), %rdi
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L872
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L842:
	movq	%r13, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L844
	.p2align 4,,10
	.p2align 3
.L874:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L873:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L875:
	leaq	_ZZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L879:
	call	__stack_chk_fail@PLT
.L880:
	leaq	_ZZN4node19sockaddr_for_familyEiPKctP16sockaddr_storageE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7624:
	.size	_ZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7629:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.cfi_endproc
.LFE7629:
	.size	_ZN4node7UDPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap7ConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7630:
	.cfi_startproc
	endbr64
	movl	$10, %esi
	jmp	_ZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.cfi_endproc
.LFE7630:
	.size	_ZN4node7UDPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap8Connect6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.type	_ZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership, @function
_ZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership:
.LFB7637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$2120, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L915
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L901
	cmpw	$1040, %cx
	jne	.L885
.L901:
	movq	23(%rdx), %r12
.L887:
	testq	%r12, %r12
	je	.L916
	cmpl	$2, 16(%rbx)
	jne	.L917
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	-2144(%rbp), %rdi
	movq	8(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jle	.L918
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
.L892:
	leaq	-1088(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	movq	-1072(%rbp), %rdx
	jg	.L893
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L894:
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L895
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L919
.L895:
	movq	-2128(%rbp), %rsi
	leaq	120(%r12), %rdi
	movl	%r13d, %ecx
	call	uv_udp_set_membership@PLT
	movq	(%rbx), %rdx
	movq	-1072(%rbp), %rdi
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L897
	testq	%rdi, %rdi
	je	.L897
	call	free@PLT
.L897:
	movq	-2128(%rbp), %rdi
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L883
	testq	%rdi, %rdi
	je	.L883
	call	free@PLT
.L883:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L920
	addq	$2120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L918:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	leaq	88(%rsi), %rdx
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L893:
	movq	8(%rbx), %rax
	subq	$8, %rax
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L919:
	movslq	43(%rax), %rax
	cmpq	$5, %rax
	jne	.L896
	xorl	%edx, %edx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L885:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L887
	.p2align 4,,10
	.p2align 3
.L916:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L883
	.p2align 4,,10
	.p2align 3
.L896:
	cmpq	$3, %rax
	movl	$0, %eax
	cmove	%rax, %rdx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L915:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L917:
	leaq	_ZZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L920:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7637:
	.size	_ZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership, .-_ZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap13AddMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap13AddMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap13AddMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7638:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.cfi_endproc
.LFE7638:
	.size	_ZN4node7UDPWrap13AddMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap13AddMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap14DropMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap14DropMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap14DropMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7639:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.cfi_endproc
.LFE7639:
	.size	_ZN4node7UDPWrap14DropMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap14DropMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.type	_ZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership, @function
_ZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership:
.LFB7640:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$3176, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L983
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L946
	cmpw	$1040, %cx
	jne	.L925
.L946:
	movq	23(%rdx), %r12
.L927:
	testq	%r12, %r12
	je	.L984
	cmpl	$3, 16(%rbx)
	jne	.L985
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	leaq	-3200(%rbp), %rdi
	movq	8(%rax), %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jle	.L986
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdx
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
.L932:
	leaq	-2144(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$2, 16(%rbx)
	jg	.L933
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	leaq	88(%rsi), %rdx
.L934:
	leaq	-1088(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1072(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L987
	cmpl	$2, 16(%rbx)
	jg	.L938
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
.L939:
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L940
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L988
.L940:
	movq	-3184(%rbp), %rcx
	movq	-2128(%rbp), %rsi
	leaq	120(%r12), %rdi
	movl	%r13d, %r8d
	call	uv_udp_set_source_membership@PLT
	movq	(%rbx), %rdx
	movq	-1072(%rbp), %rdi
	salq	$32, %rax
	movq	%rax, 24(%rdx)
	leaq	-1064(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L942
	testq	%rdi, %rdi
	je	.L942
	call	free@PLT
.L942:
	movq	-2128(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L943
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L943
.L982:
	call	free@PLT
.L943:
	movq	-3184(%rbp), %rdi
	leaq	-3176(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L923
	testq	%rdi, %rdi
	je	.L923
	call	free@PLT
.L923:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L989
	addq	$3176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L986:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	leaq	88(%rsi), %rdx
	jmp	.L932
	.p2align 4,,10
	.p2align 3
.L933:
	movq	8(%rbx), %rax
	leaq	-16(%rax), %rdx
	movq	(%rbx), %rax
	movq	8(%rax), %rsi
	jmp	.L934
	.p2align 4,,10
	.p2align 3
.L938:
	movq	8(%rbx), %rax
	subq	$16, %rax
	jmp	.L939
	.p2align 4,,10
	.p2align 3
.L988:
	movslq	43(%rax), %rax
	cmpq	$5, %rax
	jne	.L941
	xorl	%edx, %edx
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L925:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L984:
	movabsq	$-38654705664, %rcx
	movq	(%rbx), %rax
	movq	%rcx, 24(%rax)
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L987:
	movq	-2128(%rbp), %rdi
	leaq	-2120(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L943
	testq	%rdi, %rdi
	jne	.L982
	jmp	.L943
	.p2align 4,,10
	.p2align 3
.L941:
	cmpq	$3, %rax
	movl	$0, %eax
	cmove	%rax, %rdx
	jmp	.L940
	.p2align 4,,10
	.p2align 3
.L983:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L985:
	leaq	_ZZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L989:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7640:
	.size	_ZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership, .-_ZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap27AddSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap27AddSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap27AddSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7641:
	.cfi_startproc
	endbr64
	movl	$1, %esi
	jmp	_ZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.cfi_endproc
.LFE7641:
	.size	_ZN4node7UDPWrap27AddSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap27AddSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap28DropSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap28DropSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap28DropSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7642:
	.cfi_startproc
	endbr64
	xorl	%esi, %esi
	jmp	_ZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membership
	.cfi_endproc
.LFE7642:
	.size	_ZN4node7UDPWrap28DropSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap28DropSourceSpecificMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.type	_ZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, @function
_ZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEi:
.LFB7643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1528, %rsp
	.cfi_offset 3, -56
	movq	(%rdi), %r14
	movl	%esi, -1552(%rbp)
	movq	32(%r14), %rdx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1079
	cmpw	$1040, %cx
	jne	.L993
.L1079:
	movq	%r14, %rdi
	movq	23(%rdx), %r13
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L1108
.L996:
	movq	(%r14), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1080
	cmpw	$1040, %cx
	jne	.L997
.L1080:
	movq	23(%rdx), %rax
	movq	%rax, -1544(%rbp)
.L999:
	cmpq	$0, -1544(%rbp)
	je	.L1109
	movl	16(%r12), %eax
	andl	$-3, %eax
	cmpl	$4, %eax
	jne	.L1110
	movq	8(%r12), %rdi
	call	_ZNK2v85Value8IsObjectEv@PLT
	testb	%al, %al
	je	.L1111
	cmpl	$1, 16(%r12)
	jle	.L1112
	movq	8(%r12), %rax
	leaq	-8(%rax), %rdi
.L1005:
	call	_ZNK2v85Value7IsArrayEv@PLT
	testb	%al, %al
	je	.L1113
	cmpl	$2, 16(%r12)
	jg	.L1007
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1008:
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1114
	movl	16(%r12), %eax
	movl	%eax, -1548(%rbp)
	cmpl	$6, %eax
	je	.L1115
	cmpl	$3, -1548(%rbp)
	jg	.L1019
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1020:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	je	.L1116
.L1018:
	movl	16(%r12), %eax
	cmpl	$1, %eax
	jg	.L1021
	movq	(%r12), %rax
	movq	8(%rax), %r15
	addq	$88, %r15
	movq	%r15, %rdi
.L1024:
	call	_ZNK2v86Uint325ValueEv@PLT
	movdqa	.LC31(%rip), %xmm0
	movq	$0, -1368(%rbp)
	movq	$0, -1360(%rbp)
	movl	%eax, %ecx
	movq	%rcx, -1536(%rbp)
	leaq	-1368(%rbp), %rcx
	movq	%rcx, -1560(%rbp)
	movq	%rcx, -1376(%rbp)
	movaps	%xmm0, -1392(%rbp)
	cmpl	$16, %eax
	ja	.L1117
	movq	-1536(%rbp), %rax
	movq	%rax, -1392(%rbp)
	testq	%rax, %rax
	je	.L1032
.L1027:
	xorl	%ebx, %ebx
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1092:
	movq	%rax, %rdi
	call	_ZN4node6Buffer6LengthEN2v85LocalINS1_5ValueEEE@PLT
	movq	%r14, %rdi
	movq	%rax, -1528(%rbp)
	call	_ZN4node6Buffer4DataEN2v85LocalINS1_5ValueEEE@PLT
	cmpq	%rbx, -1392(%rbp)
	movq	-1528(%rbp), %rsi
	movq	%rax, %rdi
	jbe	.L1118
	call	uv_buf_init@PLT
	movq	%rdx, %rdi
	movq	%rbx, %rdx
	movq	%rax, %rsi
	addq	$1, %rbx
	salq	$4, %rdx
	addq	-1376(%rbp), %rdx
	movq	%rsi, (%rdx)
	movq	%rdi, 8(%rdx)
	cmpq	-1536(%rbp), %rbx
	jnb	.L1032
.L1033:
	movq	3280(%r13), %rsi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	_ZN2v86Object3GetENS_5LocalINS_7ContextEEEj@PLT
	movq	%rax, %r14
	testq	%rax, %rax
	jne	.L1092
	movq	-1376(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L992
	cmpq	-1560(%rbp), %rdi
	je	.L992
.L1107:
	call	free@PLT
.L992:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1119
	addq	$1528, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1112:
	.cfi_restore_state
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1007:
	movq	8(%r12), %rax
	leaq	-16(%rax), %rdi
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1021:
	movq	8(%r12), %rdi
	leaq	-8(%rdi), %r15
	cmpl	$2, %eax
	je	.L1120
	subq	$16, %rdi
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	8(%r12), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1118:
	leaq	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L993:
	leaq	32(%r14), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%r12), %r14
	movq	%rax, %r13
	movq	%r14, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jg	.L996
	.p2align 4,,10
	.p2align 3
.L1108:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L997:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, -1544(%rbp)
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1032:
	cmpl	$6, -1548(%rbp)
	movl	16(%r12), %eax
	je	.L1121
	testl	%eax, %eax
	jg	.L1122
	movq	(%r12), %rdx
	movq	-1544(%rbp), %rcx
	movq	8(%rdx), %rax
	addq	$88, %rax
	movq	%rax, 344(%rcx)
.L1044:
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1047:
	call	_ZNK2v85Value6IsTrueEv@PLT
	xorl	%ecx, %ecx
.L1046:
	movq	-1544(%rbp), %rdi
	movq	-1376(%rbp), %rsi
	movb	%al, 336(%rdi)
	movl	72(%rdi), %eax
	movl	%eax, -1528(%rbp)
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1078
	movq	-1536(%rbp), %rdx
	call	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr.part.0
	movq	-1376(%rbp), %rsi
	movq	%rax, %rbx
.L1048:
	movq	-1544(%rbp), %rax
	movq	$0, 344(%rax)
	movb	$0, 336(%rax)
.L1054:
	movq	(%r12), %rax
	salq	$32, %rbx
	movq	%rbx, 24(%rax)
	testq	%rsi, %rsi
	je	.L992
	cmpq	-1560(%rbp), %rsi
	je	.L992
	movq	%rsi, %rdi
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1117:
	movq	-1536(%rbp), %r14
	movq	%r14, %rbx
	salq	$4, %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L1062
	movq	%rax, -1376(%rbp)
	movq	%r14, -1384(%rbp)
.L1026:
	movq	-1536(%rbp), %rax
	movq	%rax, -1392(%rbp)
	jmp	.L1027
	.p2align 4,,10
	.p2align 3
.L1109:
	movabsq	$-38654705664, %rcx
	movq	(%r12), %rax
	movq	%rcx, 24(%rax)
	jmp	.L992
	.p2align 4,,10
	.p2align 3
.L1115:
	movq	8(%r12), %rax
	leaq	-24(%rax), %rdi
	call	_ZNK2v85Value8IsUint32Ev@PLT
	testb	%al, %al
	je	.L1123
	movl	16(%r12), %edx
	cmpl	$4, %edx
	jle	.L1124
	movq	8(%r12), %rax
	subq	$32, %rax
.L1013:
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	je	.L1125
.L1014:
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_4(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1111:
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1113:
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_1(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1114:
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_2(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1124:
	movq	(%r12), %rax
	movq	8(%rax), %rax
	addq	$88, %rax
	jmp	.L1013
.L1122:
	movq	8(%r12), %rdi
	movq	-1544(%rbp), %rcx
	movq	%rdi, 344(%rcx)
	cmpl	$3, %eax
	jle	.L1126
	subq	$24, %rdi
	jmp	.L1047
	.p2align 4,,10
	.p2align 3
.L1125:
	movq	-1(%rax), %rax
	cmpw	$63, 11(%rax)
	ja	.L1014
	cmpl	$5, %edx
	jg	.L1016
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1017:
	call	_ZNK2v85Value9IsBooleanEv@PLT
	testb	%al, %al
	jne	.L1018
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_5(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	8(%r12), %rax
	leaq	-40(%rax), %rdi
	jmp	.L1017
	.p2align 4,,10
	.p2align 3
.L1116:
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_6(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1062:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	%rbx, %rdi
	call	malloc@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1127
	movq	%rax, -1376(%rbp)
	movq	-1392(%rbp), %rdx
	movq	-1536(%rbp), %rax
	movq	%rax, -1384(%rbp)
	testq	%rdx, %rdx
	je	.L1026
	movq	-1560(%rbp), %rsi
	salq	$4, %rdx
	call	memcpy@PLT
	jmp	.L1026
.L1121:
	cmpl	$3, %eax
	jg	.L1034
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L1035:
	call	_ZNK2v86Uint325ValueEv@PLT
	cmpl	$4, 16(%r12)
	movl	%eax, %ebx
	jg	.L1036
	movq	(%r12), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L1037:
	movq	352(%r13), %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movl	-1552(%rbp), %eax
	movq	-1088(%rbp), %rdi
	movzwl	%bx, %esi
	cmpl	$2, %eax
	jne	.L1128
	leaq	-1520(%rbp), %r13
	movq	%r13, %rdx
	call	uv_ip4_addr@PLT
	movl	%eax, %ebx
.L1041:
	movq	-1088(%rbp), %rdi
	testl	%ebx, %ebx
	jne	.L1129
	testq	%rdi, %rdi
	je	.L1106
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1106
	call	free@PLT
.L1106:
	movl	16(%r12), %eax
	testl	%eax, %eax
	jg	.L1130
	movq	(%r12), %rdx
	movq	-1544(%rbp), %rcx
	movq	8(%rdx), %rax
	addq	$88, %rax
	movq	%rax, 344(%rcx)
.L1072:
	movq	8(%rdx), %rdi
	addq	$88, %rdi
.L1045:
	call	_ZNK2v85Value6IsTrueEv@PLT
	movq	%r13, %rcx
	jmp	.L1046
.L1128:
	cmpl	$10, %eax
	jne	.L1131
	leaq	-1520(%rbp), %r13
	movq	%r13, %rdx
	call	uv_ip6_addr@PLT
	movl	%eax, %ebx
	jmp	.L1041
.L1127:
	leaq	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1129:
	testq	%rdi, %rdi
	je	.L1105
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1105
	call	free@PLT
.L1105:
	movq	-1376(%rbp), %rsi
	jmp	.L1054
.L1036:
	movq	8(%r12), %rax
	leaq	-32(%rax), %rdx
	jmp	.L1037
.L1034:
	movq	8(%r12), %rax
	leaq	-24(%rax), %rdi
	jmp	.L1035
.L1130:
	movq	8(%r12), %rdi
	movq	-1544(%rbp), %rcx
	movq	%rdi, 344(%rcx)
	cmpl	$5, %eax
	jle	.L1132
	subq	$40, %rdi
	jmp	.L1045
.L1123:
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_3(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1110:
	leaq	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1078:
	movq	$-9, %rbx
	jmp	.L1048
.L1131:
	leaq	_ZZN4node19sockaddr_for_familyEiPKctP16sockaddr_storageE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1126:
	movq	(%r12), %rdx
	jmp	.L1044
.L1132:
	movq	(%r12), %rdx
	jmp	.L1072
.L1119:
	call	__stack_chk_fail@PLT
.L1120:
	movq	(%r12), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L1024
	.cfi_endproc
.LFE7643:
	.size	_ZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEi, .-_ZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap4SendERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap4SendERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap4SendERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7650:
	.cfi_startproc
	endbr64
	movl	$2, %esi
	jmp	_ZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.cfi_endproc
.LFE7650:
	.size	_ZN4node7UDPWrap4SendERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap4SendERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap5Send6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node7UDPWrap5Send6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node7UDPWrap5Send6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7651:
	.cfi_startproc
	endbr64
	movl	$10, %esi
	jmp	_ZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEi
	.cfi_endproc
.LFE7651:
	.size	_ZN4node7UDPWrap5Send6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node7UDPWrap5Send6ERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE
	.type	_ZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE, @function
_ZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE:
.LFB7667:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -24
	movq	16(%rsi), %rbx
	movsd	40(%rsi), %xmm0
	movq	1216(%rbx), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	je	.L1136
	comisd	.LC1(%rip), %xmm0
	jb	.L1139
.L1136:
	movq	1256(%rbx), %rax
	movq	3040(%rdi), %r8
	movsd	24(%rax), %xmm1
	movsd	%xmm0, 24(%rax)
	testq	%r8, %r8
	je	.L1140
	movq	3280(%rdi), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r8, %rdi
	movsd	%xmm1, -24(%rbp)
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	1256(%rbx), %rdx
	movsd	-24(%rbp), %xmm1
	movsd	%xmm1, 24(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1139:
	.cfi_restore_state
	leaq	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1140:
	leaq	_ZZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7667:
	.size	_ZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE, .-_ZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeE
	.p2align 4
	.globl	_Z18_register_udp_wrapv
	.type	_Z18_register_udp_wrapv, @function
_Z18_register_udp_wrapv:
.LFB7668:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7668:
	.size	_Z18_register_udp_wrapv, .-_Z18_register_udp_wrapv
	.weak	_ZTVN4node11ReqWrapBaseE
	.section	.data.rel.ro._ZTVN4node11ReqWrapBaseE,"awG",@progbits,_ZTVN4node11ReqWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11ReqWrapBaseE, @object
	.size	_ZTVN4node11ReqWrapBaseE, 48
_ZTVN4node11ReqWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node7ReqWrapI13uv_udp_send_sEE
	.section	.data.rel.ro._ZTVN4node7ReqWrapI13uv_udp_send_sEE,"awG",@progbits,_ZTVN4node7ReqWrapI13uv_udp_send_sEE,comdat
	.align 8
	.type	_ZTVN4node7ReqWrapI13uv_udp_send_sEE, @object
	.size	_ZTVN4node7ReqWrapI13uv_udp_send_sEE, 160
_ZTVN4node7ReqWrapI13uv_udp_send_sEE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node9AsyncWrap14MemoryInfoNameB5cxx11Ev
	.quad	__cxa_pure_virtual
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.quad	_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	0
	.quad	0
	.quad	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.weak	_ZTVN4node8SendWrapE
	.section	.data.rel.ro._ZTVN4node8SendWrapE,"awG",@progbits,_ZTVN4node8SendWrapE,comdat
	.align 8
	.type	_ZTVN4node8SendWrapE, @object
	.size	_ZTVN4node8SendWrapE, 160
_ZTVN4node8SendWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node8SendWrapD1Ev
	.quad	_ZN4node8SendWrapD0Ev
	.quad	_ZNK4node8SendWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node8SendWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node8SendWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10BaseObject11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.quad	_ZN4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.quad	-56
	.quad	0
	.quad	_ZThn56_N4node8SendWrapD1Ev
	.quad	_ZThn56_N4node8SendWrapD0Ev
	.quad	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE6CancelEv
	.quad	_ZThn56_N4node7ReqWrapI13uv_udp_send_sE12GetAsyncWrapEv
	.weak	_ZTVN4node11UDPListenerE
	.section	.data.rel.ro._ZTVN4node11UDPListenerE,"awG",@progbits,_ZTVN4node11UDPListenerE,comdat
	.align 8
	.type	_ZTVN4node11UDPListenerE, @object
	.size	_ZTVN4node11UDPListenerE, 72
_ZTVN4node11UDPListenerE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN4node11UDPListener11OnAfterBindEv
	.weak	_ZTVN4node11UDPWrapBaseE
	.section	.data.rel.ro._ZTVN4node11UDPWrapBaseE,"awG",@progbits,_ZTVN4node11UDPWrapBaseE,comdat
	.align 8
	.type	_ZTVN4node11UDPWrapBaseE, @object
	.size	_ZTVN4node11UDPWrapBaseE, 80
_ZTVN4node11UDPWrapBaseE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.weak	_ZTVN4node7UDPWrapE
	.section	.data.rel.ro._ZTVN4node7UDPWrapE,"awG",@progbits,_ZTVN4node7UDPWrapE,comdat
	.align 8
	.type	_ZTVN4node7UDPWrapE, @object
	.size	_ZTVN4node7UDPWrapE, 344
_ZTVN4node7UDPWrapE:
	.quad	0
	.quad	0
	.quad	_ZN4node7UDPWrapD1Ev
	.quad	_ZN4node7UDPWrapD0Ev
	.quad	_ZNK4node7UDPWrap10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node7UDPWrap14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node7UDPWrap8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	_ZN4node7UDPWrap7OnAllocEm
	.quad	_ZN4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj
	.quad	_ZN4node7UDPWrap14CreateSendWrapEm
	.quad	_ZN4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi
	.quad	_ZN4node7UDPWrap9RecvStartEv
	.quad	_ZN4node7UDPWrap8RecvStopEv
	.quad	_ZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr
	.quad	_ZN4node7UDPWrap11GetPeerNameEv
	.quad	_ZN4node7UDPWrap11GetSockNameEv
	.quad	_ZN4node7UDPWrap12GetAsyncWrapEv
	.quad	-88
	.quad	0
	.quad	_ZThn88_N4node7UDPWrapD1Ev
	.quad	_ZThn88_N4node7UDPWrapD0Ev
	.quad	_ZThn88_N4node7UDPWrap9RecvStartEv
	.quad	_ZThn88_N4node7UDPWrap8RecvStopEv
	.quad	_ZThn88_N4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddr
	.quad	_ZThn88_N4node7UDPWrap11GetPeerNameEv
	.quad	_ZThn88_N4node7UDPWrap11GetSockNameEv
	.quad	_ZThn88_N4node7UDPWrap12GetAsyncWrapEv
	.quad	-104
	.quad	0
	.quad	_ZThn104_N4node7UDPWrapD1Ev
	.quad	_ZThn104_N4node7UDPWrapD0Ev
	.quad	_ZThn104_N4node7UDPWrap7OnAllocEm
	.quad	_ZThn104_N4node7UDPWrap6OnRecvElRK8uv_buf_tPK8sockaddrj
	.quad	_ZThn104_N4node7UDPWrap14CreateSendWrapEm
	.quad	_ZThn104_N4node7UDPWrap10OnSendDoneEPNS_7ReqWrapI13uv_udp_send_sEEi
	.quad	_ZN4node11UDPListener11OnAfterBindEv
	.weak	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args
	.section	.rodata.str1.1
.LC32:
	.string	"../src/util-inl.h:374"
.LC33:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC34:
	.string	"T* node::Realloc(T*, size_t) [with T = uv_buf_t; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args,"awG",@progbits,_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args, 24
_ZZN4node7ReallocI8uv_buf_tEEPT_S3_mE4args:
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.weak	_ZZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args
	.section	.rodata.str1.1
.LC35:
	.string	"../src/req_wrap-inl.h:130"
	.section	.rodata.str1.8
	.align 8
.LC36:
	.string	"(req_wrap->original_callback_) == nullptr"
	.align 8
.LC37:
	.ascii	"static void (* node::MakeLibuvRequestCallback<ReqT, void (*)"
	.ascii	"(ReqT*, Args ."
	.string	"..)>::For(node::ReqWrap<T>*, node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F))(ReqT*, Args ...) [with ReqT = uv_udp_send_s; Args = {int}; node::MakeLibuvRequestCallback<ReqT, void (*)(ReqT*, Args ...)>::F = void (*)(uv_udp_send_s*, int)]"
	.section	.data.rel.ro.local._ZZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,"awG",@progbits,_ZZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args,comdat
	.align 16
	.type	_ZZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, @gnu_unique_object
	.size	_ZZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args, 24
_ZZN4node24MakeLibuvRequestCallbackI13uv_udp_send_sPFvPS1_iEE3ForEPNS_7ReqWrapIS1_EES4_E4args:
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.weak	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args
	.section	.rodata.str1.1
.LC38:
	.string	"../src/util.h:352"
.LC39:
	.string	"(index) < (length())"
	.section	.rodata.str1.8
	.align 8
.LC40:
	.string	"T& node::MaybeStackBuffer<T, kStackStorageSize>::operator[](size_t) [with T = uv_buf_t; long unsigned int kStackStorageSize = 16; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args,"awG",@progbits,_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args,comdat
	.align 16
	.type	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args, @gnu_unique_object
	.size	_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args, 24
_ZZN4node16MaybeStackBufferI8uv_buf_tLm16EEixEmE4args:
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.weak	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args
	.section	.rodata.str1.1
.LC41:
	.string	"../src/node_internals.h:78"
.LC42:
	.string	"args[0]->IsObject()"
	.section	.rodata.str1.8
	.align 8
.LC43:
	.string	"void node::GetSockOrPeerName(const v8::FunctionCallbackInfo<v8::Value>&) [with T = node::UDPWrap; int (* F)(const typename T::HandleType*, sockaddr*, int*) = uv_udp_getsockname]"
	.section	.data.rel.ro.local._ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getsocknameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.weak	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args
	.section	.rodata.str1.8
	.align 8
.LC44:
	.string	"void node::GetSockOrPeerName(const v8::FunctionCallbackInfo<v8::Value>&) [with T = node::UDPWrap; int (* F)(const typename T::HandleType*, sockaddr*, int*) = uv_udp_getpeername]"
	.section	.data.rel.ro.local._ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,"awG",@progbits,_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args,comdat
	.align 16
	.type	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @gnu_unique_object
	.size	_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node17GetSockOrPeerNameINS_7UDPWrapEXadL_Z18uv_udp_getpeernameEEEEvRKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC41
	.quad	.LC42
	.quad	.LC44
	.weak	_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args
	.section	.rodata.str1.1
.LC45:
	.string	"../src/req_wrap-inl.h:28"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"(false) == (persistent().IsEmpty())"
	.align 8
.LC47:
	.string	"node::ReqWrap<T>::~ReqWrap() [with T = uv_udp_send_s]"
	.section	.data.rel.ro.local._ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args,"awG",@progbits,_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args,comdat
	.align 16
	.type	_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args, @gnu_unique_object
	.size	_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args, 24
_ZZN4node7ReqWrapI13uv_udp_send_sED4EvE4args:
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.section	.rodata.str1.1
.LC48:
	.string	"../src/udp_wrap.cc"
.LC49:
	.string	"udp_wrap"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC48
	.quad	0
	.quad	_ZN4node7UDPWrap10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC49
	.quad	0
	.quad	0
	.section	.rodata.str1.1
.LC50:
	.string	"../src/udp_wrap.cc:740"
	.section	.rodata.str1.8
	.align 8
.LC51:
	.string	"(env->udp_constructor_function().IsEmpty()) == (false)"
	.align 8
.LC52:
	.string	"static v8::MaybeLocal<v8::Object> node::UDPWrap::Instantiate(node::Environment*, node::AsyncWrap*, node::UDPWrap::SocketType)"
	.section	.data.rel.ro.local,"aw"
	.align 16
	.type	_ZZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args, @object
	.size	_ZZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args, 24
_ZZN4node7UDPWrap11InstantiateEPNS_11EnvironmentEPNS_9AsyncWrapENS0_10SocketTypeEE4args:
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.section	.rodata.str1.1
.LC53:
	.string	"../src/udp_wrap.cc:578"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"(static_cast<size_t>(err)) == (msg_size)"
	.align 8
.LC55:
	.string	"virtual ssize_t node::UDPWrap::Send(uv_buf_t*, size_t, const sockaddr*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrE4args_0, @object
	.size	_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrE4args_0, 24
_ZZN4node7UDPWrap4SendEP8uv_buf_tmPK8sockaddrE4args_0:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.section	.rodata.str1.1
.LC56:
	.string	"../src/udp_wrap.cc:507"
.LC57:
	.string	"args[3]->IsBoolean()"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"static void node::UDPWrap::DoSend(const v8::FunctionCallbackInfo<v8::Value>&, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_6, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_6, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_6:
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.section	.rodata.str1.1
.LC59:
	.string	"../src/udp_wrap.cc:504"
.LC60:
	.string	"args[5]->IsBoolean()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_5, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_5, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_5:
	.quad	.LC59
	.quad	.LC60
	.quad	.LC58
	.section	.rodata.str1.1
.LC61:
	.string	"../src/udp_wrap.cc:503"
.LC62:
	.string	"args[4]->IsString()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_4, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_4, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_4:
	.quad	.LC61
	.quad	.LC62
	.quad	.LC58
	.section	.rodata.str1.1
.LC63:
	.string	"../src/udp_wrap.cc:502"
.LC64:
	.string	"args[3]->IsUint32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_3, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_3, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_3:
	.quad	.LC63
	.quad	.LC64
	.quad	.LC58
	.section	.rodata.str1.1
.LC65:
	.string	"../src/udp_wrap.cc:497"
.LC66:
	.string	"args[2]->IsUint32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_2, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_2, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_2:
	.quad	.LC65
	.quad	.LC66
	.quad	.LC58
	.section	.rodata.str1.1
.LC67:
	.string	"../src/udp_wrap.cc:496"
.LC68:
	.string	"args[1]->IsArray()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_1, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_1, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_1:
	.quad	.LC67
	.quad	.LC68
	.quad	.LC58
	.section	.rodata.str1.1
.LC69:
	.string	"../src/udp_wrap.cc:495"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_0, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_0, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args_0:
	.quad	.LC69
	.quad	.LC42
	.quad	.LC58
	.section	.rodata.str1.1
.LC70:
	.string	"../src/udp_wrap.cc:494"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"args.Length() == 4 || args.Length() == 6"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args, @object
	.size	_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args, 24
_ZZN4node7UDPWrap6DoSendERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args:
	.quad	.LC70
	.quad	.LC71
	.quad	.LC58
	.section	.rodata.str1.1
.LC72:
	.string	"../src/udp_wrap.cc:454"
.LC73:
	.string	"(args.Length()) == (3)"
	.section	.rodata.str1.8
	.align 8
.LC74:
	.string	"static void node::UDPWrap::SetSourceMembership(const v8::FunctionCallbackInfo<v8::Value>&, uv_membership)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args, @object
	.size	_ZZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args, 24
_ZZN4node7UDPWrap19SetSourceMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args:
	.quad	.LC72
	.quad	.LC73
	.quad	.LC74
	.section	.rodata.str1.1
.LC75:
	.string	"../src/udp_wrap.cc:420"
.LC76:
	.string	"(args.Length()) == (2)"
	.section	.rodata.str1.8
	.align 8
.LC77:
	.string	"static void node::UDPWrap::SetMembership(const v8::FunctionCallbackInfo<v8::Value>&, uv_membership)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args, @object
	.size	_ZZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args, 24
_ZZN4node7UDPWrap13SetMembershipERKN2v820FunctionCallbackInfoINS1_5ValueEEE13uv_membershipE4args:
	.quad	.LC75
	.quad	.LC76
	.quad	.LC77
	.section	.rodata.str1.1
.LC78:
	.string	"../src/udp_wrap.cc:403"
.LC79:
	.string	"args[0]->IsString()"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"static void node::UDPWrap::SetMulticastInterface(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC78
	.quad	.LC79
	.quad	.LC80
	.section	.rodata.str1.1
.LC81:
	.string	"../src/udp_wrap.cc:402"
.LC82:
	.string	"(args.Length()) == (1)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap21SetMulticastInterfaceERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC81
	.quad	.LC82
	.quad	.LC80
	.section	.rodata.str1.1
.LC83:
	.string	"../src/udp_wrap.cc:392"
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"static void node::UDPWrap::SetMulticastLoopback(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap20SetMulticastLoopbackERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC83
	.quad	.LC82
	.quad	.LC84
	.section	.rodata.str1.1
.LC85:
	.string	"../src/udp_wrap.cc:391"
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"static void node::UDPWrap::SetMulticastTTL(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap15SetMulticastTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC85
	.quad	.LC82
	.quad	.LC86
	.section	.rodata.str1.1
.LC87:
	.string	"../src/udp_wrap.cc:390"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"static void node::UDPWrap::SetBroadcast(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap12SetBroadcastERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC87
	.quad	.LC82
	.quad	.LC88
	.section	.rodata.str1.1
.LC89:
	.string	"../src/udp_wrap.cc:389"
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"static void node::UDPWrap::SetTTL(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap6SetTTLERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC89
	.quad	.LC82
	.quad	.LC90
	.section	.rodata.str1.1
.LC91:
	.string	"../src/udp_wrap.cc:369"
.LC92:
	.string	"(args.Length()) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"static void node::UDPWrap::Disconnect(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap10DisconnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC91
	.quad	.LC92
	.quad	.LC93
	.section	.rodata.str1.1
.LC94:
	.string	"../src/udp_wrap.cc:325"
.LC95:
	.string	"args[1]->IsBoolean()"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"static void node::UDPWrap::BufferSize(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.section	.rodata.str1.1
.LC97:
	.string	"../src/udp_wrap.cc:324"
.LC98:
	.string	"args[0]->IsUint32()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap10BufferSizeERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC96
	.section	.rodata.str1.1
.LC99:
	.string	"../src/udp_wrap.cc:299"
.LC100:
	.string	"args[0]->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"static void node::UDPWrap::Open(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap4OpenERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.section	.rodata.str1.1
.LC102:
	.string	"../src/udp_wrap.cc:276"
	.section	.rodata.str1.8
	.align 8
.LC103:
	.string	"static void node::UDPWrap::DoConnect(const v8::FunctionCallbackInfo<v8::Value>&, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args, @object
	.size	_ZZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args, 24
_ZZN4node7UDPWrap9DoConnectERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args:
	.quad	.LC102
	.quad	.LC76
	.quad	.LC103
	.section	.rodata.str1.1
.LC104:
	.string	"../src/udp_wrap.cc:247"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"static void node::UDPWrap::DoBind(const v8::FunctionCallbackInfo<v8::Value>&, int)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args, @object
	.size	_ZZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args, 24
_ZZN4node7UDPWrap6DoBindERKN2v820FunctionCallbackInfoINS1_5ValueEEEiE4args:
	.quad	.LC104
	.quad	.LC73
	.quad	.LC105
	.section	.rodata.str1.1
.LC106:
	.string	"../src/udp_wrap.cc:236"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"0 && \"unexpected address family\""
	.align 8
.LC108:
	.string	"int node::sockaddr_for_family(int, const char*, short unsigned int, sockaddr_storage*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node19sockaddr_for_familyEiPKctP16sockaddr_storageE4args, @object
	.size	_ZZN4node19sockaddr_for_familyEiPKctP16sockaddr_storageE4args, 24
_ZZN4node19sockaddr_for_familyEiPKctP16sockaddr_storageE4args:
	.quad	.LC106
	.quad	.LC107
	.quad	.LC108
	.section	.rodata.str1.1
.LC109:
	.string	"../src/udp_wrap.cc:210"
.LC110:
	.string	"args.IsConstructCall()"
	.section	.rodata.str1.8
	.align 8
.LC111:
	.string	"static void node::UDPWrap::New(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node7UDPWrap3NewERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC109
	.quad	.LC110
	.quad	.LC111
	.section	.rodata.str1.1
.LC112:
	.string	"../src/udp_wrap.cc:121"
.LC113:
	.string	"(r) == (0)"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"node::UDPWrap::UDPWrap(node::Environment*, v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node7UDPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, @object
	.size	_ZZN4node7UDPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args, 24
_ZZN4node7UDPWrapC4EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEE4args:
	.quad	.LC112
	.quad	.LC113
	.quad	.LC114
	.section	.rodata.str1.1
.LC115:
	.string	"../src/udp_wrap.cc:102"
	.section	.rodata.str1.8
	.align 8
.LC116:
	.string	"(obj->InternalFieldCount()) > (UDPWrapBase::kUDPWrapBaseField)"
	.align 8
.LC117:
	.string	"static node::UDPWrapBase* node::UDPWrapBase::FromObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEEE4args, @object
	.size	_ZZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node11UDPWrapBase10FromObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.section	.rodata.str1.1
.LC118:
	.string	"../src/udp_wrap.cc:96"
.LC119:
	.string	"(listener_->wrap_) == nullptr"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"void node::UDPWrapBase::set_listener(node::UDPListener*)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerEE4args, @object
	.size	_ZZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerEE4args, 24
_ZZN4node11UDPWrapBase12set_listenerEPNS_11UDPListenerEE4args:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.section	.rodata.str1.1
.LC121:
	.string	"../src/udp_wrap.cc:87"
.LC122:
	.string	"(listener_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC123:
	.string	"node::UDPListener* node::UDPWrapBase::listener() const"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZNK4node11UDPWrapBase8listenerEvE4args, @object
	.size	_ZZNK4node11UDPWrapBase8listenerEvE4args, 24
_ZZNK4node11UDPWrapBase8listenerEvE4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC123
	.weak	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args
	.section	.rodata.str1.1
.LC124:
	.string	"../src/req_wrap-inl.h:13"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"env->has_run_bootstrapping_code()"
	.align 8
.LC126:
	.string	"node::ReqWrapBase::ReqWrapBase(node::Environment*)"
	.section	.data.rel.ro.local._ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,"awG",@progbits,_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args,comdat
	.align 16
	.type	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, @gnu_unique_object
	.size	_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args, 24
_ZZN4node11ReqWrapBaseC4EPNS_11EnvironmentEE4args:
	.quad	.LC124
	.quad	.LC125
	.quad	.LC126
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC127:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC129:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC127
	.quad	.LC128
	.quad	.LC129
	.weak	_ZZN4node15AllocatedBuffer5clearEvE4args
	.section	.rodata.str1.1
.LC130:
	.string	"../src/env-inl.h:955"
.LC131:
	.string	"(env_) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC132:
	.string	"void node::AllocatedBuffer::clear()"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer5clearEvE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer5clearEvE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer5clearEvE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer5clearEvE4args, 24
_ZZN4node15AllocatedBuffer5clearEvE4args:
	.quad	.LC130
	.quad	.LC131
	.quad	.LC132
	.weak	_ZZN4node15AllocatedBuffer6ResizeEmE4args
	.section	.rodata.str1.1
.LC133:
	.string	"../src/env-inl.h:911"
.LC134:
	.string	"(new_data) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC135:
	.string	"void node::AllocatedBuffer::Resize(size_t)"
	.section	.data.rel.ro.local._ZZN4node15AllocatedBuffer6ResizeEmE4args,"awG",@progbits,_ZZN4node15AllocatedBuffer6ResizeEmE4args,comdat
	.align 16
	.type	_ZZN4node15AllocatedBuffer6ResizeEmE4args, @gnu_unique_object
	.size	_ZZN4node15AllocatedBuffer6ResizeEmE4args, 24
_ZZN4node15AllocatedBuffer6ResizeEmE4args:
	.quad	.LC133
	.quad	.LC134
	.quad	.LC135
	.weak	_ZZN4node11Environment8AllocateEmE4args
	.section	.rodata.str1.1
.LC136:
	.string	"../src/env-inl.h:889"
.LC137:
	.string	"(ret) != (nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC138:
	.string	"char* node::Environment::Allocate(size_t)"
	.section	.data.rel.ro.local._ZZN4node11Environment8AllocateEmE4args,"awG",@progbits,_ZZN4node11Environment8AllocateEmE4args,comdat
	.align 16
	.type	_ZZN4node11Environment8AllocateEmE4args, @gnu_unique_object
	.size	_ZZN4node11Environment8AllocateEmE4args, 24
_ZZN4node11Environment8AllocateEmE4args:
	.quad	.LC136
	.quad	.LC137
	.quad	.LC138
	.weak	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args
	.section	.rodata.str1.1
.LC139:
	.string	"../src/env-inl.h:399"
.LC140:
	.string	"(request_waiting_) >= (0)"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"void node::Environment::DecreaseWaitingRequestCounter()"
	.section	.data.rel.ro.local._ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,"awG",@progbits,_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args,comdat
	.align 16
	.type	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, @gnu_unique_object
	.size	_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args, 24
_ZZN4node11Environment29DecreaseWaitingRequestCounterEvE4args:
	.quad	.LC139
	.quad	.LC140
	.quad	.LC141
	.weak	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args
	.section	.rodata.str1.1
.LC142:
	.string	"../src/env-inl.h:203"
	.section	.rodata.str1.8
	.align 8
.LC143:
	.string	"(default_trigger_async_id) >= (0)"
	.align 8
.LC144:
	.string	"node::AsyncHooks::DefaultTriggerAsyncIdScope::DefaultTriggerAsyncIdScope(node::Environment*, double)"
	.section	.data.rel.ro.local._ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,"awG",@progbits,_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args,comdat
	.align 16
	.type	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, @gnu_unique_object
	.size	_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args, 24
_ZZN4node10AsyncHooks26DefaultTriggerAsyncIdScopeC4EPNS_11EnvironmentEdE4args:
	.quad	.LC142
	.quad	.LC143
	.quad	.LC144
	.weakref	_ZL28__gthrw___pthread_key_createPjPFvPvE,__pthread_key_create
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.align 8
.LC1:
	.long	0
	.long	0
	.align 8
.LC29:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC31:
	.quad	0
	.quad	16
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
