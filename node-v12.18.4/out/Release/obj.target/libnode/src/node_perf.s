	.file	"node_perf.cc"
	.text
	.section	.text._ZN2v813EmbedderGraph4Node11WrapperNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node11WrapperNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.type	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, @function
_ZN2v813EmbedderGraph4Node11WrapperNodeEv:
.LFB4286:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4286:
	.size	_ZN2v813EmbedderGraph4Node11WrapperNodeEv, .-_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.section	.text._ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,"axG",@progbits,_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.type	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, @function
_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv:
.LFB4288:
	.cfi_startproc
	endbr64
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE4288:
	.size	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv, .-_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.section	.text._ZN4node18MemoryRetainerNode4NameEv,"axG",@progbits,_ZN4node18MemoryRetainerNode4NameEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode4NameEv
	.type	_ZN4node18MemoryRetainerNode4NameEv, @function
_ZN4node18MemoryRetainerNode4NameEv:
.LFB4637:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %rax
	ret
	.cfi_endproc
.LFE4637:
	.size	_ZN4node18MemoryRetainerNode4NameEv, .-_ZN4node18MemoryRetainerNode4NameEv
	.section	.rodata._ZN4node18MemoryRetainerNode10NamePrefixEv.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Node /"
	.section	.text._ZN4node18MemoryRetainerNode10NamePrefixEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10NamePrefixEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.type	_ZN4node18MemoryRetainerNode10NamePrefixEv, @function
_ZN4node18MemoryRetainerNode10NamePrefixEv:
.LFB4638:
	.cfi_startproc
	endbr64
	leaq	.LC0(%rip), %rax
	ret
	.cfi_endproc
.LFE4638:
	.size	_ZN4node18MemoryRetainerNode10NamePrefixEv, .-_ZN4node18MemoryRetainerNode10NamePrefixEv
	.section	.text._ZN4node18MemoryRetainerNode11SizeInBytesEv,"axG",@progbits,_ZN4node18MemoryRetainerNode11SizeInBytesEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.type	_ZN4node18MemoryRetainerNode11SizeInBytesEv, @function
_ZN4node18MemoryRetainerNode11SizeInBytesEv:
.LFB4639:
	.cfi_startproc
	endbr64
	movq	64(%rdi), %rax
	ret
	.cfi_endproc
.LFE4639:
	.size	_ZN4node18MemoryRetainerNode11SizeInBytesEv, .-_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.section	.text._ZN4node18MemoryRetainerNode10IsRootNodeEv,"axG",@progbits,_ZN4node18MemoryRetainerNode10IsRootNodeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.type	_ZN4node18MemoryRetainerNode10IsRootNodeEv, @function
_ZN4node18MemoryRetainerNode10IsRootNodeEv:
.LFB4641:
	.cfi_startproc
	endbr64
	movq	8(%rdi), %r8
	testq	%r8, %r8
	je	.L8
	movq	(%r8), %rax
	movq	%r8, %rdi
	movq	48(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	24(%rdi), %eax
	ret
	.cfi_endproc
.LFE4641:
	.size	_ZN4node18MemoryRetainerNode10IsRootNodeEv, .-_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.section	.text._ZN2v817TracingController23GetCategoryGroupEnabledEPKc,"axG",@progbits,_ZN2v817TracingController23GetCategoryGroupEnabledEPKc,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.type	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, @function
_ZN2v817TracingController23GetCategoryGroupEnabledEPKc:
.LFB4679:
	.cfi_startproc
	endbr64
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rax
	ret
	.cfi_endproc
.LFE4679:
	.size	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc, .-_ZN2v817TracingController23GetCategoryGroupEnabledEPKc
	.section	.text._ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,"axG",@progbits,_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.type	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, @function
_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj:
.LFB4680:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4680:
	.size	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj, .-_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj
	.section	.text._ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl,"axG",@progbits,_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl,comdat
	.align 2
	.p2align 4
	.weak	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl
	.type	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl, @function
_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl:
.LFB4681:
	.cfi_startproc
	endbr64
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE4681:
	.size	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl, .-_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl
	.section	.text._ZN4node10HandleWrap7OnCloseEv,"axG",@progbits,_ZN4node10HandleWrap7OnCloseEv,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node10HandleWrap7OnCloseEv
	.type	_ZN4node10HandleWrap7OnCloseEv, @function
_ZN4node10HandleWrap7OnCloseEv:
.LFB5843:
	.cfi_startproc
	endbr64
	ret
	.cfi_endproc
.LFE5843:
	.size	_ZN4node10HandleWrap7OnCloseEv, .-_ZN4node10HandleWrap7OnCloseEv
	.section	.text._ZNK4node11performance12ELDHistogram8SelfSizeEv,"axG",@progbits,_ZNK4node11performance12ELDHistogram8SelfSizeEv,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11performance12ELDHistogram8SelfSizeEv
	.type	_ZNK4node11performance12ELDHistogram8SelfSizeEv, @function
_ZNK4node11performance12ELDHistogram8SelfSizeEv:
.LFB7609:
	.cfi_startproc
	endbr64
	movl	$280, %eax
	ret
	.cfi_endproc
.LFE7609:
	.size	_ZNK4node11performance12ELDHistogram8SelfSizeEv, .-_ZNK4node11performance12ELDHistogram8SelfSizeEv
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED2Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED2Ev:
.LFB9933:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	movq	(%rdi), %rax
	call	*8(%rax)
.L15:
	movq	16(%rbx), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%rbx)
	testq	%rdi, %rdi
	je	.L14
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9933:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED2Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED2Ev
	.set	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED1Ev,_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED2Ev
	.p2align 4
	.globl	_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv
	.type	_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv, @function
_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv:
.LFB7628:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -24
	movq	1864(%rcx), %rbx
	call	uv_hrtime@PLT
	movq	%rax, 120(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7628:
	.size	_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv, .-_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv
	.section	.text._ZN4node11performance16PerformanceEntryD2Ev,"axG",@progbits,_ZN4node11performance16PerformanceEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11performance16PerformanceEntryD2Ev
	.type	_ZN4node11performance16PerformanceEntryD2Ev, @function
_ZN4node11performance16PerformanceEntryD2Ev:
.LFB7598:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L24
	call	_ZdlPv@PLT
.L24:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L23
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7598:
	.size	_ZN4node11performance16PerformanceEntryD2Ev, .-_ZN4node11performance16PerformanceEntryD2Ev
	.weak	_ZN4node11performance16PerformanceEntryD1Ev
	.set	_ZN4node11performance16PerformanceEntryD1Ev,_ZN4node11performance16PerformanceEntryD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD2Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD2Ev
	.type	_ZN4node18MemoryRetainerNodeD2Ev, @function
_ZN4node18MemoryRetainerNodeD2Ev:
.LFB10344:
	.cfi_startproc
	endbr64
	movq	32(%rdi), %r8
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	addq	$48, %rdi
	movq	%rax, -48(%rdi)
	cmpq	%rdi, %r8
	je	.L27
	movq	%r8, %rdi
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	ret
	.cfi_endproc
.LFE10344:
	.size	_ZN4node18MemoryRetainerNodeD2Ev, .-_ZN4node18MemoryRetainerNodeD2Ev
	.weak	_ZN4node18MemoryRetainerNodeD1Ev
	.set	_ZN4node18MemoryRetainerNodeD1Ev,_ZN4node18MemoryRetainerNodeD2Ev
	.section	.text._ZN4node18MemoryRetainerNodeD0Ev,"axG",@progbits,_ZN4node18MemoryRetainerNodeD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node18MemoryRetainerNodeD0Ev
	.type	_ZN4node18MemoryRetainerNodeD0Ev, @function
_ZN4node18MemoryRetainerNodeD0Ev:
.LFB10346:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	32(%rdi), %rdi
	leaq	48(%r12), %rax
	cmpq	%rax, %rdi
	je	.L30
	call	_ZdlPv@PLT
.L30:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$72, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10346:
	.size	_ZN4node18MemoryRetainerNodeD0Ev, .-_ZN4node18MemoryRetainerNodeD0Ev
	.section	.text._ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,"axG",@progbits,_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_,comdat
	.p2align 4
	.weak	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.type	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, @function
_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_:
.LFB7529:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movq	8(%rdi), %r12
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L33
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 8(%r12)
.L33:
	movq	24(%r12), %rax
	testq	%rax, %rax
	jne	.L42
.L34:
	movq	(%r12), %rax
	movq	%r12, %rdi
	movq	64(%rax), %rax
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L34
	leaq	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7529:
	.size	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_, .-_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_
	.text
	.p2align 4
	.globl	_ZN4node11performance28GarbageCollectionCleanupHookEPv
	.type	_ZN4node11performance28GarbageCollectionCleanupHookEPv, @function
_ZN4node11performance28GarbageCollectionCleanupHookEPv:
.LFB7635:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	movq	%r12, %rdx
	subq	$8, %rsp
	movq	352(%rdi), %rdi
	call	_ZN2v87Isolate24RemoveGCPrologueCallbackEPFvPS0_NS_6GCTypeENS_15GCCallbackFlagsEPvES4_@PLT
	movq	352(%r12), %rdi
	addq	$8, %rsp
	movq	%r12, %rdx
	leaq	_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv(%rip), %rsi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate24RemoveGCEpilogueCallbackEPFvPS0_NS_6GCTypeENS_15GCCallbackFlagsEPvES4_@PLT
	.cfi_endproc
.LFE7635:
	.size	_ZN4node11performance28GarbageCollectionCleanupHookEPv, .-_ZN4node11performance28GarbageCollectionCleanupHookEPv
	.section	.text._ZN4node9HistogramD2Ev,"axG",@progbits,_ZN4node9HistogramD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9HistogramD2Ev
	.type	_ZN4node9HistogramD2Ev, @function
_ZN4node9HistogramD2Ev:
.LFB7663:
	.cfi_startproc
	endbr64
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L45
	jmp	hdr_close@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	ret
	.cfi_endproc
.LFE7663:
	.size	_ZN4node9HistogramD2Ev, .-_ZN4node9HistogramD2Ev
	.weak	_ZN4node9HistogramD1Ev
	.set	_ZN4node9HistogramD1Ev,_ZN4node9HistogramD2Ev
	.section	.text._ZN4node9HistogramD0Ev,"axG",@progbits,_ZN4node9HistogramD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node9HistogramD0Ev
	.type	_ZN4node9HistogramD0Ev, @function
_ZN4node9HistogramD0Ev:
.LFB7665:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L48
	call	hdr_close@PLT
.L48:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$16, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7665:
	.size	_ZN4node9HistogramD0Ev, .-_ZN4node9HistogramD0Ev
	.section	.text._ZN4node11performance12ELDHistogramD2Ev,"axG",@progbits,_ZN4node11performance12ELDHistogramD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11performance12ELDHistogramD2Ev
	.type	_ZN4node11performance12ELDHistogramD2Ev, @function
_ZN4node11performance12ELDHistogramD2Ev:
.LFB10332:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance12ELDHistogramE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, 88(%rdi)
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L54
	call	hdr_close@PLT
.L54:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE10332:
	.size	_ZN4node11performance12ELDHistogramD2Ev, .-_ZN4node11performance12ELDHistogramD2Ev
	.weak	_ZN4node11performance12ELDHistogramD1Ev
	.set	_ZN4node11performance12ELDHistogramD1Ev,_ZN4node11performance12ELDHistogramD2Ev
	.text
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7650:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L78
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L73
	cmpw	$1040, %cx
	jne	.L61
.L73:
	movq	23(%rdx), %r12
.L63:
	testq	%r12, %r12
	je	.L59
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L79
	movq	8(%rbx), %rdi
.L66:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L80
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L68
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
.L69:
	call	_ZNK2v86Number5ValueEv@PLT
	comisd	.LC1(%rip), %xmm0
	jbe	.L81
	movsd	.LC2(%rip), %xmm1
	comisd	%xmm0, %xmm1
	jb	.L82
	movq	96(%r12), %rdi
	movq	(%rbx), %rbx
	call	hdr_value_at_percentile@PLT
	pxor	%xmm0, %xmm0
	movq	8(%rbx), %rdi
	cvtsi2sdq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L83
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L59:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L68:
	movq	8(%rbx), %rdi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L61:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	_ZZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	_ZZN4node9Histogram10PercentileEdE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	_ZZN4node9Histogram10PercentileEdE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L59
	.cfi_endproc
.LFE7650:
	.size	_ZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata._ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE.str1.1,"aMS",@progbits,1
.LC3:
	.string	"histogram"
	.section	.text._ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE,"axG",@progbits,_ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE
	.type	_ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE, @function
_ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE:
.LFB7607:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	96(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	call	hdr_get_memory_size@PLT
	testq	%rax, %rax
	je	.L84
	movl	$72, %edi
	movq	%rax, %r13
	call	_Znwm@PLT
	movl	$9, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %r12
	leaq	16+_ZTVN4node18MemoryRetainerNodeE(%rip), %rax
	leaq	.LC3(%rip), %rcx
	movq	%rax, (%r12)
	leaq	32(%r12), %rdi
	leaq	48(%r12), %rax
	movq	$0, 8(%r12)
	movq	$0, 16(%r12)
	movb	$0, 24(%r12)
	movq	%rax, 32(%r12)
	movq	$0, 40(%r12)
	movb	$0, 48(%r12)
	movq	$0, 64(%r12)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE10_M_replaceEmmPKcm@PLT
	movq	8(%rbx), %rdi
	movq	%r13, 64(%r12)
	leaq	-48(%rbp), %rsi
	movb	$0, 24(%r12)
	movq	(%rdi), %rax
	movq	%r12, -48(%rbp)
	call	*8(%rax)
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L87
	movq	(%rdi), %rax
	call	*8(%rax)
.L87:
	movq	64(%rbx), %rax
	cmpq	32(%rbx), %rax
	je	.L84
	cmpq	72(%rbx), %rax
	je	.L98
.L89:
	movq	-8(%rax), %rsi
	testq	%rsi, %rsi
	je	.L84
	movq	8(%rbx), %rdi
	leaq	.LC3(%rip), %rcx
	movq	%r12, %rdx
	movq	(%rdi), %rax
	call	*16(%rax)
.L84:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L99
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	.cfi_restore_state
	movq	88(%rbx), %rax
	movq	-8(%rax), %rax
	addq	$512, %rax
	jmp	.L89
.L99:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7607:
	.size	_ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE, .-_ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE
	.section	.text._ZN4node11performance12ELDHistogramD0Ev,"axG",@progbits,_ZN4node11performance12ELDHistogramD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node11performance12ELDHistogramD0Ev
	.type	_ZThn88_N4node11performance12ELDHistogramD0Ev, @function
_ZThn88_N4node11performance12ELDHistogramD0Ev:
.LFB10525:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance12ELDHistogramE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	-88(%rdi), %r12
	pushq	%rbx
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	hdr_close@PLT
.L101:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rax, -88(%rbx)
	movq	-24(%rbx), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	popq	%rbx
	movq	%r12, %rdi
	movl	$280, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10525:
	.size	_ZThn88_N4node11performance12ELDHistogramD0Ev, .-_ZThn88_N4node11performance12ELDHistogramD0Ev
	.align 2
	.p2align 4
	.weak	_ZN4node11performance12ELDHistogramD0Ev
	.type	_ZN4node11performance12ELDHistogramD0Ev, @function
_ZN4node11performance12ELDHistogramD0Ev:
.LFB10334:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance12ELDHistogramE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, 88(%rdi)
	movq	96(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	hdr_close@PLT
.L107:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	56(%r12), %rdx
	movq	%r12, %rdi
	movq	%rax, (%r12)
	movq	64(%r12), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	call	_ZN4node9AsyncWrapD2Ev@PLT
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$280, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE10334:
	.size	_ZN4node11performance12ELDHistogramD0Ev, .-_ZN4node11performance12ELDHistogramD0Ev
	.section	.text._ZN4node11performance12ELDHistogramD2Ev,"axG",@progbits,_ZN4node11performance12ELDHistogramD5Ev,comdat
	.p2align 4
	.weak	_ZThn88_N4node11performance12ELDHistogramD1Ev
	.type	_ZThn88_N4node11performance12ELDHistogramD1Ev, @function
_ZThn88_N4node11performance12ELDHistogramD1Ev:
.LFB10526:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance12ELDHistogramE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, -88(%rdi)
	leaq	16+_ZTVN4node9HistogramE(%rip), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L113
	call	hdr_close@PLT
.L113:
	leaq	16+_ZTVN4node10HandleWrapE(%rip), %rax
	movq	-32(%rbx), %rdx
	leaq	-88(%rbx), %rdi
	movq	%rax, -88(%rbx)
	movq	-24(%rbx), %rax
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZN4node9AsyncWrapD2Ev@PLT
	.cfi_endproc
.LFE10526:
	.size	_ZThn88_N4node11performance12ELDHistogramD1Ev, .-_ZThn88_N4node11performance12ELDHistogramD1Ev
	.text
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED0Ev, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED0Ev:
.LFB9935:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_EE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L119
	movq	(%rdi), %rax
	call	*8(%rax)
.L119:
	movq	16(%r12), %rdi
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE(%rip), %rax
	movq	%rax, (%r12)
	testq	%rdi, %rdi
	je	.L120
	movq	(%rdi), %rax
	call	*8(%rax)
.L120:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$32, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE9935:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED0Ev, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED0Ev
	.section	.text._ZN4node11performance18GCPerformanceEntryD2Ev,"axG",@progbits,_ZN4node11performance18GCPerformanceEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11performance18GCPerformanceEntryD2Ev
	.type	_ZN4node11performance18GCPerformanceEntryD2Ev, @function
_ZN4node11performance18GCPerformanceEntryD2Ev:
.LFB8896:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L129
	call	_ZdlPv@PLT
.L129:
	movq	16(%rbx), %rdi
	addq	$32, %rbx
	cmpq	%rbx, %rdi
	je	.L128
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZdlPv@PLT
	.p2align 4,,10
	.p2align 3
.L128:
	.cfi_restore_state
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8896:
	.size	_ZN4node11performance18GCPerformanceEntryD2Ev, .-_ZN4node11performance18GCPerformanceEntryD2Ev
	.weak	_ZN4node11performance18GCPerformanceEntryD1Ev
	.set	_ZN4node11performance18GCPerformanceEntryD1Ev,_ZN4node11performance18GCPerformanceEntryD2Ev
	.section	.text._ZN4node11performance16PerformanceEntryD0Ev,"axG",@progbits,_ZN4node11performance16PerformanceEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11performance16PerformanceEntryD0Ev
	.type	_ZN4node11performance16PerformanceEntryD0Ev, @function
_ZN4node11performance16PerformanceEntryD0Ev:
.LFB7600:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L133
	call	_ZdlPv@PLT
.L133:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L134
	call	_ZdlPv@PLT
.L134:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$96, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE7600:
	.size	_ZN4node11performance16PerformanceEntryD0Ev, .-_ZN4node11performance16PerformanceEntryD0Ev
	.section	.text._ZN4node11performance18GCPerformanceEntryD0Ev,"axG",@progbits,_ZN4node11performance18GCPerformanceEntryD5Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZN4node11performance18GCPerformanceEntryD0Ev
	.type	_ZN4node11performance18GCPerformanceEntryD0Ev, @function
_ZN4node11performance18GCPerformanceEntryD0Ev:
.LFB8898:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	%rax, (%rdi)
	movq	48(%rdi), %rdi
	leaq	64(%r12), %rax
	cmpq	%rax, %rdi
	je	.L137
	call	_ZdlPv@PLT
.L137:
	movq	16(%r12), %rdi
	leaq	32(%r12), %rax
	cmpq	%rax, %rdi
	je	.L138
	call	_ZdlPv@PLT
.L138:
	addq	$8, %rsp
	movq	%r12, %rdi
	movl	$104, %esi
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZdlPvm@PLT
	.cfi_endproc
.LFE8898:
	.size	_ZN4node11performance18GCPerformanceEntryD0Ev, .-_ZN4node11performance18GCPerformanceEntryD0Ev
	.section	.text._ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev,"axG",@progbits,_ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev,comdat
	.align 2
	.p2align 4
	.weak	_ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev
	.type	_ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev, @function
_ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev:
.LFB7608:
	.cfi_startproc
	endbr64
	leaq	16(%rdi), %rdx
	movb	$0, 28(%rdi)
	movq	%rdi, %rax
	movabsq	$8031170931529632837, %rcx
	movq	%rdx, (%rdi)
	movq	%rcx, 16(%rdi)
	movl	$1835102823, 24(%rdi)
	movq	$12, 8(%rdi)
	ret
	.cfi_endproc
.LFE7608:
	.size	_ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev, .-_ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev
	.text
	.p2align 4
	.type	_ZN4node11performanceL31RemoveGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performanceL31RemoveGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7637:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L155
	cmpw	$1040, %cx
	jne	.L142
.L155:
	movq	23(%rdx), %r12
.L144:
	movq	2592(%r12), %rcx
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	2584(%r12), %rbx
	divq	%rcx
	leaq	(%rbx,%rdx,8), %r13
	movq	%rdx, %r11
	movq	0(%r13), %r8
	testq	%r8, %r8
	je	.L145
	movq	(%r8), %rdi
	movq	%r8, %r10
	leaq	_ZN4node11performance28GarbageCollectionCleanupHookEPv(%rip), %r14
	movq	32(%rdi), %rsi
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%rdi), %r9
	testq	%r9, %r9
	je	.L145
	movq	32(%r9), %rsi
	xorl	%edx, %edx
	movq	%rdi, %r10
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r11
	jne	.L145
	movq	%r9, %rdi
.L148:
	cmpq	%rsi, %r12
	jne	.L146
	cmpq	%r14, 8(%rdi)
	jne	.L146
	cmpq	16(%rdi), %r12
	jne	.L146
	movq	(%rdi), %rsi
	cmpq	%r10, %r8
	je	.L167
	testq	%rsi, %rsi
	je	.L150
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L150
	movq	%r10, (%rbx,%rdx,8)
	movq	(%rdi), %rsi
.L150:
	movq	%rsi, (%r10)
	call	_ZdlPv@PLT
	subq	$1, 2608(%r12)
.L145:
	movq	352(%r12), %rdi
	movq	%r12, %rdx
	leaq	_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv(%rip), %rsi
	call	_ZN2v87Isolate24RemoveGCPrologueCallbackEPFvPS0_NS_6GCTypeENS_15GCCallbackFlagsEPvES4_@PLT
	popq	%rbx
	movq	%r12, %rdx
	leaq	_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv(%rip), %rsi
	movq	352(%r12), %rdi
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v87Isolate24RemoveGCEpilogueCallbackEPFvPS0_NS_6GCTypeENS_15GCCallbackFlagsEPvES4_@PLT
	.p2align 4,,10
	.p2align 3
.L167:
	.cfi_restore_state
	testq	%rsi, %rsi
	je	.L154
	movq	32(%rsi), %rax
	xorl	%edx, %edx
	divq	%rcx
	cmpq	%rdx, %r11
	je	.L150
	movq	%r10, (%rbx,%rdx,8)
	movq	0(%r13), %rax
.L149:
	leaq	2600(%r12), %rdx
	cmpq	%rdx, %rax
	je	.L168
.L151:
	movq	$0, 0(%r13)
	movq	(%rdi), %rsi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%r10, %rax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%rsi, 2600(%r12)
	jmp	.L151
	.cfi_endproc
.LFE7637:
	.size	_ZN4node11performanceL31RemoveGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performanceL31RemoveGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11performanceL32InstallGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performanceL32InstallGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7636:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L192
	cmpw	$1040, %cx
	jne	.L170
.L192:
	movq	23(%rdx), %r12
.L172:
	movq	352(%r12), %rdi
	movl	$15, %ecx
	movq	%r12, %rdx
	leaq	_ZN4node11performance26MarkGarbageCollectionStartEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv(%rip), %rsi
	call	_ZN2v87Isolate21AddGCPrologueCallbackEPFvPS0_NS_6GCTypeENS_15GCCallbackFlagsEPvES4_S2_@PLT
	movq	352(%r12), %rdi
	movq	%r12, %rdx
	leaq	_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv(%rip), %rsi
	movl	$15, %ecx
	call	_ZN2v87Isolate21AddGCEpilogueCallbackEPFvPS0_NS_6GCTypeENS_15GCCallbackFlagsEPvES4_S2_@PLT
	movq	2640(%r12), %rbx
	movl	$40, %edi
	leaq	1(%rbx), %rax
	movq	%rax, 2640(%r12)
	call	_Znwm@PLT
	leaq	_ZN4node11performance28GarbageCollectionCleanupHookEPv(%rip), %r10
	xorl	%edx, %edx
	movq	%r10, 8(%rax)
	movq	%rax, %r13
	movq	%r12, 16(%rax)
	movq	%rbx, 24(%rax)
	movq	2592(%r12), %rsi
	movq	$0, (%rax)
	movq	%r12, %rax
	divq	%rsi
	movq	2584(%r12), %rax
	movq	(%rax,%rdx,8), %rdi
	movq	%rdx, %r8
	leaq	0(,%rdx,8), %r14
	testq	%rdi, %rdi
	je	.L173
	movq	(%rdi), %rax
	movq	32(%rax), %rcx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L174:
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L173
	movq	32(%r9), %rcx
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%rsi
	cmpq	%rdx, %r8
	jne	.L173
	movq	%r9, %rax
.L176:
	cmpq	%rcx, %r12
	jne	.L174
	cmpq	%r10, 8(%rax)
	jne	.L174
	cmpq	16(%rax), %r12
	jne	.L174
	cmpq	$0, (%rdi)
	je	.L173
	movq	%r13, %rdi
	call	_ZdlPv@PLT
	leaq	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L173:
	movq	2608(%r12), %rdx
	leaq	2616(%r12), %rdi
	movl	$1, %ecx
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %rbx
	testb	%al, %al
	jne	.L177
	movq	2584(%r12), %r15
	movq	%r12, 32(%r13)
	addq	%r15, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L187
.L212:
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	movq	(%r14), %rax
	movq	%r13, (%rax)
.L188:
	addq	$1, 2608(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L210
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L211
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r15
	call	memset@PLT
	leaq	2632(%r12), %r10
.L180:
	movq	2600(%r12), %rsi
	movq	$0, 2600(%r12)
	testq	%rsi, %rsi
	je	.L182
	xorl	%r8d, %r8d
	leaq	2600(%r12), %r9
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L184:
	movq	(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L185:
	testq	%rsi, %rsi
	je	.L182
.L183:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	32(%rcx), %rax
	divq	%rbx
	leaq	(%r15,%rdx,8), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L184
	movq	2600(%r12), %rdi
	movq	%rdi, (%rcx)
	movq	%rcx, 2600(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L191
	movq	%rcx, (%r15,%r8,8)
	movq	%rdx, %r8
	testq	%rsi, %rsi
	jne	.L183
	.p2align 4,,10
	.p2align 3
.L182:
	movq	2584(%r12), %rdi
	cmpq	%rdi, %r10
	je	.L186
	call	_ZdlPv@PLT
.L186:
	movq	%r12, %rax
	xorl	%edx, %edx
	movq	%rbx, 2592(%r12)
	divq	%rbx
	movq	%r15, 2584(%r12)
	movq	%r12, 32(%r13)
	leaq	0(,%rdx,8), %r14
	addq	%r15, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L212
.L187:
	movq	2600(%r12), %rax
	movq	%rax, 0(%r13)
	movq	%r13, 2600(%r12)
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L189
	movq	32(%rax), %rax
	xorl	%edx, %edx
	divq	2592(%r12)
	movq	%r13, (%r15,%rdx,8)
.L189:
	leaq	2600(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L170:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rdx, %r8
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	2632(%r12), %r15
	movq	$0, 2632(%r12)
	movq	%r15, %r10
	jmp	.L180
.L211:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE7636:
	.size	_ZN4node11performanceL32InstallGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performanceL32InstallGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.globl	_ZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7624:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L224
	cmpw	$1040, %cx
	jne	.L214
.L224:
	movq	23(%rdx), %r12
.L216:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L217
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L232
.L219:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L233
	movq	8(%rbx), %r13
.L221:
	movq	2944(%r12), %rdi
	movq	352(%r12), %r14
	testq	%rdi, %rdi
	je	.L222
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	$0, 2944(%r12)
.L222:
	testq	%r13, %r13
	je	.L213
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2944(%r12)
.L213:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L233:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %r13
	addq	$88, %r13
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L217:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L219
.L232:
	leaq	_ZZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L214:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L216
	.cfi_endproc
.LFE7624:
	.size	_ZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L17ELDHistogramResetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L17ELDHistogramResetERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7655:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L245
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L240
	cmpw	$1040, %cx
	jne	.L236
.L240:
	movq	23(%rdx), %rbx
.L238:
	testq	%rbx, %rbx
	je	.L234
	movq	96(%rbx), %rdi
	call	hdr_reset@PLT
	movq	$0, 112(%rbx)
	movq	$0, 120(%rbx)
.L234:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L236:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %rbx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L245:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7655:
	.size	_ZN4node11performance12_GLOBAL__N_1L17ELDHistogramResetERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L17ELDHistogramResetERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.globl	_ZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7643:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L263
	cmpw	$1040, %cx
	jne	.L247
.L263:
	movq	23(%rdx), %rax
.L249:
	movq	3280(%rax), %r13
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L250
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	je	.L265
.L252:
	cmpl	$1, 16(%rbx)
	jle	.L266
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdi
.L254:
	call	_ZNK2v85Value8IsNumberEv@PLT
	testb	%al, %al
	je	.L267
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L256
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
	movq	%r12, %rdi
.L259:
	movq	%r13, %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %rcx
	testb	%al, %al
	je	.L268
.L260:
	xorl	%r9d, %r9d
	movl	$1, %r8d
	movq	%r12, %rdx
	movq	%r13, %rdi
	leaq	_ZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v88Function3NewENS_5LocalINS_7ContextEEEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS1_IS5_EEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	testq	%rax, %rax
	je	.L269
	movq	(%rax), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 24(%rax)
.L246:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L250:
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10IsFunctionEv@PLT
	testb	%al, %al
	jne	.L252
.L265:
	leaq	_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L256:
	movq	8(%rbx), %r12
	cmpl	$1, %eax
	je	.L270
	leaq	-8(%r12), %rdi
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L247:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L268:
	movq	%rdx, -40(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-40(%rbp), %rcx
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L269:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	(%rbx), %rax
	movq	16(%rax), %rdx
	movq	%rdx, 24(%rax)
	jmp	.L246
.L270:
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L259
	.cfi_endproc
.LFE7643:
	.size	_ZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramExceedsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramExceedsERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7648:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L283
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L278
	cmpw	$1040, %cx
	jne	.L273
.L278:
	movq	23(%rdx), %rax
.L275:
	testq	%rax, %rax
	je	.L271
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rbx
	cvtsi2sdq	112(%rax), %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L284
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L271:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L284:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L271
	.cfi_endproc
.LFE7648:
	.size	_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramExceedsERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramExceedsERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramStddevERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramStddevERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7649:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L297
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L292
	cmpw	$1040, %cx
	jne	.L287
.L292:
	movq	23(%rdx), %rax
.L289:
	testq	%rax, %rax
	je	.L285
	movq	(%rbx), %rbx
	movq	96(%rax), %rdi
	call	hdr_stddev@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L298
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L285:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L297:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L298:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L285
	.cfi_endproc
.LFE7649:
	.size	_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramStddevERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramStddevERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L16ELDHistogramMeanERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L16ELDHistogramMeanERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7647:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L311
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L306
	cmpw	$1040, %cx
	jne	.L301
.L306:
	movq	23(%rdx), %rax
.L303:
	testq	%rax, %rax
	je	.L299
	movq	(%rbx), %rbx
	movq	96(%rax), %rdi
	call	hdr_mean@PLT
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L312
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L299:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L311:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L312:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L299
	.cfi_endproc
.LFE7647:
	.size	_ZN4node11performance12_GLOBAL__N_1L16ELDHistogramMeanERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L16ELDHistogramMeanERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramDisableERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramDisableERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7654:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L327
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L322
	cmpw	$1040, %cx
	jne	.L315
.L322:
	movq	23(%rdx), %rax
.L317:
	testq	%rax, %rax
	je	.L313
	cmpb	$0, 104(%rax)
	movq	(%rbx), %rbx
	movl	$64, %ecx
	je	.L319
	movl	72(%rax), %esi
	leal	-1(%rsi), %edx
	cmpl	$1, %edx
	jbe	.L319
	movb	$0, 104(%rax)
	leaq	128(%rax), %rdi
	call	uv_timer_stop@PLT
	movl	$56, %ecx
.L319:
	movq	8(%rbx), %rax
	movq	56(%rax,%rcx), %rax
	movq	%rax, 24(%rbx)
.L313:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L327:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7654:
	.size	_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramDisableERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramDisableERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.globl	_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv
	.type	_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv, @function
_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv:
.LFB7629:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	1864(%rcx), %rbx
	movq	104(%rbx), %rax
	movl	12(%rax), %edi
	testl	%edi, %edi
	jne	.L340
.L328:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	.cfi_restore_state
	movq	%rcx, %r12
	movl	%esi, %r15d
	movl	%edx, %r13d
	call	uv_hrtime@PLT
	movl	$104, %edi
	movq	120(%rbx), %r14
	movq	%rax, -56(%rbp)
	call	_Znwm@PLT
	movl	$25447, %edx
	movq	%r14, %xmm0
	movl	$32, %edi
	movq	%r12, 8(%rax)
	movq	%rax, %rbx
	leaq	32(%rax), %rax
	movhps	-56(%rbp), %xmm0
	movq	%rax, 16(%rbx)
	movl	$25447, %eax
	movw	%ax, 32(%rbx)
	leaq	64(%rbx), %rax
	movq	%rax, 48(%rbx)
	leaq	16+_ZTVN4node11performance18GCPerformanceEntryE(%rip), %rax
	movw	%dx, 64(%rbx)
	movq	$2, 24(%rbx)
	movb	$0, 34(%rbx)
	movq	$2, 56(%rbx)
	movb	$0, 66(%rbx)
	movq	%rax, (%rbx)
	movl	%r15d, 96(%rbx)
	movl	%r13d, 100(%rbx)
	movups	%xmm0, 80(%rbx)
	call	_Znwm@PLT
	movq	2480(%r12), %rdx
	leaq	16+_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_EE(%rip), %rdi
	movb	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	%rdi, (%rax)
	movq	%rbx, 24(%rax)
	lock addq	$1, 2464(%r12)
	movq	%rax, 2480(%r12)
	testq	%rdx, %rdx
	je	.L331
	movq	16(%rdx), %rdi
	movq	%rax, 16(%rdx)
	testq	%rdi, %rdi
	je	.L328
.L339:
	movq	(%rdi), %rax
	movq	8(%rax), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L331:
	.cfi_restore_state
	movq	2472(%r12), %rdi
	movq	%rax, 2472(%r12)
	testq	%rdi, %rdi
	jne	.L339
	jmp	.L328
	.cfi_endproc
.LFE7629:
	.size	_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv, .-_ZN4node11performance24MarkGarbageCollectionEndEPN2v87IsolateENS1_6GCTypeENS1_15GCCallbackFlagsEPv
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMinERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMinERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7645:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L353
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L348
	cmpw	$1040, %cx
	jne	.L343
.L348:
	movq	23(%rdx), %rax
.L345:
	testq	%rax, %rax
	je	.L341
	movq	96(%rax), %rdi
	call	hdr_min@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rbx
	cvtsi2sdq	%rax, %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L354
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L341:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L343:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L345
	.p2align 4,,10
	.p2align 3
.L353:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L341
	.cfi_endproc
.LFE7645:
	.size	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMinERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMinERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMaxERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMaxERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7646:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L367
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L362
	cmpw	$1040, %cx
	jne	.L357
.L362:
	movq	23(%rdx), %rax
.L359:
	testq	%rax, %rax
	je	.L355
	movq	96(%rax), %rdi
	call	hdr_max@PLT
	pxor	%xmm0, %xmm0
	movq	(%rbx), %rbx
	cvtsi2sdq	%rax, %xmm0
	movq	8(%rbx), %rdi
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	testq	%rax, %rax
	je	.L368
	movq	(%rax), %rax
	movq	%rax, 24(%rbx)
.L355:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L357:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L367:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L368:
	movq	16(%rbx), %rax
	movq	%rax, 24(%rbx)
	jmp	.L355
	.cfi_endproc
.LFE7646:
	.size	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMaxERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMaxERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramEnableERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramEnableERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7653:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	(%rdi), %r13
	movq	%rdi, %r12
	movq	%r13, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L383
	movq	0(%r13), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L378
	cmpw	$1040, %cx
	jne	.L371
.L378:
	movq	23(%rdx), %rax
.L373:
	testq	%rax, %rax
	je	.L369
	cmpb	$0, 104(%rax)
	movq	(%r12), %r13
	movl	$64, %ecx
	jne	.L375
	movl	72(%rax), %esi
	leal	-1(%rsi), %edx
	cmpl	$1, %edx
	jbe	.L375
	movslq	108(%rax), %rdx
	movb	$1, 104(%rax)
	leaq	128(%rax), %r12
	leaq	_ZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_s(%rip), %rsi
	movq	$0, 120(%rax)
	movq	%r12, %rdi
	movq	%rdx, %rcx
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	movl	$56, %ecx
.L375:
	movq	8(%r13), %rax
	movq	56(%rax,%rcx), %rax
	movq	%rax, 24(%r13)
.L369:
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L371:
	.cfi_restore_state
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L383:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7653:
	.size	_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramEnableERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramEnableERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7651:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	subq	$144, %rsp
	movq	(%rdi), %r12
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	movq	32(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L402
	cmpw	$1040, %cx
	jne	.L385
.L402:
	movq	23(%rdx), %r13
.L387:
	movq	%r12, %rdi
	call	_ZN2v86Object18InternalFieldCountEv@PLT
	testl	%eax, %eax
	jle	.L408
	movq	(%r12), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L403
	cmpw	$1040, %cx
	jne	.L389
.L403:
	movq	23(%rdx), %r14
.L391:
	testq	%r14, %r14
	je	.L384
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L409
	movq	8(%rbx), %rdi
.L395:
	call	_ZNK2v85Value5IsMapEv@PLT
	testb	%al, %al
	je	.L410
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L397
	movq	(%rbx), %rax
	movq	8(%rax), %r12
	addq	$88, %r12
.L398:
	leaq	-160(%rbp), %rbx
	movq	96(%r14), %rsi
	movl	$1, %edx
	movq	%rbx, %rdi
	call	hdr_iter_percentile_init@PLT
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L411:
	pxor	%xmm0, %xmm0
	movsd	-64(%rbp), %xmm1
	movq	352(%r13), %rdi
	cvtsi2sdq	-128(%rbp), %xmm0
	movsd	%xmm1, -168(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movsd	-168(%rbp), %xmm1
	movq	352(%r13), %rdi
	movq	%rax, %r14
	movapd	%xmm1, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	3280(%r13), %rsi
	movq	%r14, %rcx
	movq	%r12, %rdi
	movq	%rax, %rdx
	call	_ZN2v83Map3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
.L400:
	movq	%rbx, %rdi
	call	hdr_iter_next@PLT
	testb	%al, %al
	jne	.L411
.L384:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L412
	addq	$144, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L409:
	.cfi_restore_state
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L397:
	movq	8(%rbx), %r12
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L385:
	leaq	32(%r12), %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %r12
	movq	%rax, %r13
	jmp	.L387
	.p2align 4,,10
	.p2align 3
.L389:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r14
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L408:
	leaq	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L410:
	leaq	_ZZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L412:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7651:
	.size	_ZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"basic_string::_M_construct null not valid"
	.text
	.p2align 4
	.globl	_ZN4node11performance9ClearMarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance9ClearMarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance9ClearMarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7620:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$1176, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L451
	cmpw	$1040, %cx
	jne	.L414
.L451:
	movq	23(%rdx), %r13
.L416:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	je	.L491
	jle	.L492
	movq	8(%rbx), %rdx
.L424:
	movq	352(%r13), %rsi
	leaq	-1104(%rbp), %rdi
	leaq	-1120(%rbp), %r14
	leaq	-1136(%rbp), %r12
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r15
	movq	%r14, -1136(%rbp)
	testq	%r15, %r15
	jne	.L493
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L492:
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -1144(%rbp)
	movq	%rax, %rbx
	cmpq	$15, %rax
	ja	.L494
	cmpq	$1, %rax
	jne	.L426
	movzbl	(%r15), %edx
	movb	%dl, -1120(%rbp)
	movq	%r14, %rdx
.L427:
	movq	%rax, -1128(%rbp)
	movb	$0, (%rdx,%rax)
	movq	-1128(%rbp), %rsi
	movl	$3339675911, %edx
	movq	-1136(%rbp), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	1880(%r13), %rbx
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%rbx
	movq	1872(%r13), %rax
	movq	%rax, -1160(%rbp)
	leaq	(%rax,%rdx,8), %rax
	movq	%rdx, %r9
	movq	(%rax), %r10
	movq	%rax, -1168(%rbp)
	testq	%r10, %r10
	je	.L489
	movq	(%r10), %r12
	movq	-1128(%rbp), %r11
	movq	%r10, %r8
	movq	-1136(%rbp), %rdi
	movq	48(%r12), %rcx
	testq	%r11, %r11
	je	.L430
	cmpq	%rcx, %r15
	je	.L495
	.p2align 4,,10
	.p2align 3
.L431:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L429
	movq	48(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%rcx, %rax
	divq	%rbx
	cmpq	%rdx, %r9
	jne	.L429
	movq	%rsi, %r12
	cmpq	%rcx, %r15
	jne	.L431
.L495:
	cmpq	16(%r12), %r11
	jne	.L431
	movq	8(%r12), %rsi
	movq	%r11, %rdx
	movq	%r9, -1208(%rbp)
	movq	%r8, -1200(%rbp)
	movq	%r10, -1192(%rbp)
	movq	%r11, -1184(%rbp)
	movq	%rdi, -1176(%rbp)
	call	memcmp@PLT
	movq	-1176(%rbp), %rdi
	movq	-1184(%rbp), %r11
	testl	%eax, %eax
	movq	-1192(%rbp), %r10
	movq	-1200(%rbp), %r8
	movq	-1208(%rbp), %r9
	jne	.L431
	movq	(%r12), %rcx
	cmpq	%r8, %r10
	je	.L496
.L477:
	testq	%rcx, %rcx
	je	.L436
	movq	48(%rcx), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%rdx, %r9
	je	.L436
	movq	-1160(%rbp), %rax
	movq	%r8, (%rax,%rdx,8)
	movq	(%r12), %rcx
.L436:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	%rcx, (%r8)
	cmpq	%rax, %rdi
	je	.L438
	call	_ZdlPv@PLT
.L438:
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	subq	$1, 1896(%r13)
.L489:
	movq	-1136(%rbp), %rdi
.L429:
	cmpq	%r14, %rdi
	je	.L439
	call	_ZdlPv@PLT
.L439:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L413
	testq	%rdi, %rdi
	je	.L413
	call	free@PLT
.L413:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L497
	addq	$1176, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L430:
	.cfi_restore_state
	cmpq	%rcx, %r15
	je	.L498
	.p2align 4,,10
	.p2align 3
.L434:
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L429
	movq	48(%rsi), %rcx
	xorl	%edx, %edx
	movq	%r12, %r8
	movq	%rcx, %rax
	divq	%rbx
	cmpq	%rdx, %r9
	jne	.L429
	movq	%rsi, %r12
	cmpq	%rcx, %r15
	jne	.L434
.L498:
	cmpq	$0, 16(%r12)
	jne	.L434
	movq	(%r12), %rcx
	cmpq	%r8, %r10
	jne	.L477
.L496:
	testq	%rcx, %rcx
	je	.L450
	movq	48(%rcx), %rax
	xorl	%edx, %edx
	divq	%rbx
	cmpq	%rdx, %r9
	je	.L436
	movq	-1160(%rbp), %rax
	movq	%r8, (%rax,%rdx,8)
	movq	-1168(%rbp), %rax
	movq	(%rax), %rax
.L435:
	leaq	1888(%r13), %rdx
	cmpq	%rdx, %rax
	je	.L499
.L437:
	movq	-1168(%rbp), %rax
	movq	$0, (%rax)
	movq	(%r12), %rcx
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L491:
	movq	1888(%r13), %r12
	testq	%r12, %r12
	jne	.L418
	jmp	.L421
	.p2align 4,,10
	.p2align 3
.L500:
	call	_ZdlPv@PLT
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	je	.L421
.L422:
	movq	%rbx, %r12
.L418:
	movq	8(%r12), %rdi
	leaq	24(%r12), %rax
	movq	(%r12), %rbx
	cmpq	%rax, %rdi
	jne	.L500
	movq	%r12, %rdi
	call	_ZdlPv@PLT
	testq	%rbx, %rbx
	jne	.L422
.L421:
	movq	1880(%r13), %rax
	movq	1872(%r13), %rdi
	xorl	%esi, %esi
	leaq	0(,%rax,8), %rdx
	call	memset@PLT
	movq	$0, 1896(%r13)
	movq	$0, 1888(%r13)
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L426:
	testq	%rbx, %rbx
	jne	.L501
	movq	%r14, %rdx
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L494:
	movq	%r12, %rdi
	leaq	-1144(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1136(%rbp)
	movq	%rax, %rdi
	movq	-1144(%rbp), %rax
	movq	%rax, -1120(%rbp)
.L425:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-1144(%rbp), %rax
	movq	-1136(%rbp), %rdx
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L414:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L450:
	movq	%r8, %rax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L499:
	movq	%rcx, 1888(%r13)
	jmp	.L437
.L497:
	call	__stack_chk_fail@PLT
.L501:
	movq	%r14, %rdi
	jmp	.L425
	.cfi_endproc
.LFE7620:
	.size	_ZN4node11performance9ClearMarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance9ClearMarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"node"
.LC6:
	.string	"mark"
.LC7:
	.string	"measure"
.LC8:
	.string	"function"
.LC9:
	.string	"http2"
.LC10:
	.string	"http"
	.text
	.p2align 4
	.globl	_ZN4node11performance6NotifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance6NotifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance6NotifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7644:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	subq	$1072, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L521
	cmpw	$1040, %cx
	jne	.L503
.L521:
	movq	23(%rdx), %r12
.L505:
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jg	.L506
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L507:
	movq	352(%r12), %rsi
	leaq	-1072(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jg	.L508
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L509:
	movq	%rdx, -1080(%rbp)
	movq	-1056(%rbp), %r8
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L530
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L512
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L513
	cmpb	$103, (%r8)
	jne	.L522
	cmpb	$99, 1(%r8)
	jne	.L522
	cmpb	$0, 2(%r8)
	jne	.L522
	movq	1864(%r12), %rdx
	movl	$12, %eax
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L512:
	movq	1864(%r12), %rdx
	movl	$4, %eax
.L511:
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	jne	.L531
.L518:
	leaq	-1048(%rbp), %rax
	cmpq	%rax, %r8
	je	.L502
	testq	%r8, %r8
	je	.L502
	movq	%r8, %rdi
	call	free@PLT
.L502:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L532
	addq	$1072, %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	.cfi_restore_state
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L516
	movl	$6, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L517
	movl	$5, %ecx
	leaq	.LC10(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L518
	movq	1864(%r12), %rdx
	movl	$24, %eax
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L506:
	movq	8(%rbx), %rdx
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L508:
	movq	8(%rbx), %rdx
	subq	$8, %rdx
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L530:
	movq	1864(%r12), %rdx
	xorl	%eax, %eax
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	je	.L518
.L531:
	movq	352(%r12), %rax
	movq	2944(%r12), %rdi
	leaq	-1080(%rbp), %r8
	movl	$1, %ecx
	movq	3280(%r12), %rsi
	leaq	88(%rax), %rdx
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	-1056(%rbp), %r8
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L503:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L513:
	movq	1864(%r12), %rdx
	movl	$8, %eax
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L516:
	movq	1864(%r12), %rdx
	movl	$16, %eax
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L517:
	movq	1864(%r12), %rdx
	movl	$20, %eax
	jmp	.L511
.L532:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7644:
	.size	_ZN4node11performance6NotifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance6NotifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZNK4node11performance16PerformanceEntry8ToObjectEv
	.type	_ZNK4node11performance16PerformanceEntry8ToObjectEv, @function
_ZNK4node11performance16PerformanceEntry8ToObjectEv:
.LFB7613:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	movq	2952(%rax), %rdi
	movq	3280(%rax), %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.L548
	movq	8(%rbx), %r13
	movq	16(%rbx), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%r13), %r14
	movq	3280(%r13), %r15
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L552
.L547:
	movq	360(%r13), %rax
	movl	$5, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	1056(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L553
.L536:
	movq	48(%rbx), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L554
.L537:
	movq	360(%r13), %rax
	movl	$5, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	616(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L555
.L538:
	movq	80(%rbx), %rax
	subq	_ZN4node11performance10timeOriginE(%rip), %rax
	js	.L539
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L540:
	movq	%r14, %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$5, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%r13), %rax
	movq	1664(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L556
.L541:
	movq	88(%rbx), %rax
	subq	80(%rbx), %rax
	js	.L542
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L543:
	movq	%r14, %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$5, %r8d
	movq	%r15, %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%r13), %rax
	movq	568(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L557
.L544:
	movq	%r12, %rax
.L548:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L539:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L540
	.p2align 4,,10
	.p2align 3
.L542:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L555:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L538
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%rcx, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L547
	.p2align 4,,10
	.p2align 3
.L553:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L536
	.p2align 4,,10
	.p2align 3
.L554:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L537
	.p2align 4,,10
	.p2align 3
.L557:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L544
	.p2align 4,,10
	.p2align 3
.L556:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L541
	.cfi_endproc
.LFE7613:
	.size	_ZNK4node11performance16PerformanceEntry8ToObjectEv, .-_ZNK4node11performance16PerformanceEntry8ToObjectEv
	.section	.rodata.str1.1
.LC12:
	.string	"observerCounts"
.LC13:
	.string	"milestones"
.LC14:
	.string	"PerformanceEntry"
.LC15:
	.string	"clearMark"
.LC16:
	.string	"markMilestone"
.LC17:
	.string	"setupObservers"
.LC18:
	.string	"timerify"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"installGarbageCollectionTracking"
	.align 8
.LC20:
	.string	"removeGarbageCollectionTracking"
	.section	.rodata.str1.1
.LC21:
	.string	"notify"
.LC22:
	.string	"NODE_PERFORMANCE_GC_MAJOR"
.LC24:
	.string	"NODE_PERFORMANCE_GC_MINOR"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"NODE_PERFORMANCE_GC_INCREMENTAL"
	.section	.rodata.str1.1
.LC28:
	.string	"NODE_PERFORMANCE_GC_WEAKCB"
.LC30:
	.string	"NODE_PERFORMANCE_GC_FLAGS_NO"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"NODE_PERFORMANCE_GC_FLAGS_CONSTRUCT_RETAINED"
	.align 8
.LC32:
	.string	"NODE_PERFORMANCE_GC_FLAGS_FORCED"
	.align 8
.LC33:
	.string	"NODE_PERFORMANCE_GC_FLAGS_SYNCHRONOUS_PHANTOM_PROCESSING"
	.align 8
.LC34:
	.string	"NODE_PERFORMANCE_GC_FLAGS_ALL_AVAILABLE_GARBAGE"
	.align 8
.LC36:
	.string	"NODE_PERFORMANCE_GC_FLAGS_ALL_EXTERNAL_MEMORY"
	.align 8
.LC38:
	.string	"NODE_PERFORMANCE_GC_FLAGS_SCHEDULE_IDLE"
	.align 8
.LC40:
	.string	"NODE_PERFORMANCE_ENTRY_TYPE_NODE"
	.align 8
.LC41:
	.string	"NODE_PERFORMANCE_ENTRY_TYPE_MARK"
	.align 8
.LC42:
	.string	"NODE_PERFORMANCE_ENTRY_TYPE_MEASURE"
	.align 8
.LC43:
	.string	"NODE_PERFORMANCE_ENTRY_TYPE_GC"
	.align 8
.LC45:
	.string	"NODE_PERFORMANCE_ENTRY_TYPE_FUNCTION"
	.align 8
.LC46:
	.string	"NODE_PERFORMANCE_ENTRY_TYPE_HTTP2"
	.align 8
.LC48:
	.string	"NODE_PERFORMANCE_ENTRY_TYPE_HTTP"
	.align 8
.LC50:
	.string	"NODE_PERFORMANCE_MILESTONE_ENVIRONMENT"
	.align 8
.LC51:
	.string	"NODE_PERFORMANCE_MILESTONE_NODE_START"
	.align 8
.LC52:
	.string	"NODE_PERFORMANCE_MILESTONE_V8_START"
	.align 8
.LC53:
	.string	"NODE_PERFORMANCE_MILESTONE_LOOP_START"
	.align 8
.LC54:
	.string	"NODE_PERFORMANCE_MILESTONE_LOOP_EXIT"
	.align 8
.LC55:
	.string	"NODE_PERFORMANCE_MILESTONE_BOOTSTRAP_COMPLETE"
	.section	.rodata.str1.1
.LC56:
	.string	"timeOrigin"
.LC58:
	.string	"timeOriginTimestamp"
.LC59:
	.string	"ELDHistogram"
.LC60:
	.string	"exceeds"
.LC61:
	.string	"min"
.LC62:
	.string	"max"
.LC63:
	.string	"mean"
.LC64:
	.string	"stddev"
.LC65:
	.string	"percentile"
.LC66:
	.string	"percentiles"
.LC67:
	.string	"enable"
.LC68:
	.string	"disable"
.LC69:
	.string	"reset"
	.section	.text.unlikely,"ax",@progbits
.LCOLDB70:
	.text
.LHOTB70:
	.p2align 4
	.globl	_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.type	_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, @function
_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv:
.LFB7673:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	testq	%rdx, %rdx
	je	.L559
	movq	%rdi, %r13
	movq	%rdx, %rdi
	movq	%rdx, %r14
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L559
	movq	(%r14), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rbx
	movq	55(%rax), %rax
	cmpq	%rbx, 295(%rax)
	jne	.L559
	movq	271(%rax), %rbx
	movq	1864(%rbx), %r12
	movq	352(%rbx), %r15
	movq	112(%r12), %r8
	testq	%r8, %r8
	je	.L560
	movq	(%r8), %rsi
	movq	80(%r12), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r8
.L560:
	xorl	%edx, %edx
	movl	$14, %ecx
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	leaq	.LC12(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L682
.L561:
	movq	%r8, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L683
.L562:
	movq	72(%r12), %r8
	testq	%r8, %r8
	je	.L563
	movq	(%r8), %rsi
	movq	40(%r12), %rdi
	call	_ZN2v811HandleScope12CreateHandleEPNS_8internal7IsolateEm@PLT
	movq	%rax, %r8
.L563:
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r15, %rdi
	movq	%r8, -56(%rbp)
	leaq	.LC13(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r8
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L684
.L564:
	movq	%r8, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L685
.L565:
	movl	$16, %ecx
	xorl	%edx, %edx
	leaq	.LC14(%rip), %rsi
	movq	%r15, %rdi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L686
.L566:
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$0
	movl	$1, %r9d
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%r10, -56(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	-56(%rbp), %r10
	movq	%rax, %r12
	movq	%rax, %rdi
	movq	%r10, %rsi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	movq	-56(%rbp), %r10
	testq	%rax, %rax
	movq	%rax, %r12
	popq	%r8
	je	.L687
.L567:
	movq	%r12, %rcx
	movq	%r10, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L688
.L568:
	movq	2952(%rbx), %rdi
	movq	352(%rbx), %r8
	testq	%rdi, %rdi
	je	.L569
	movq	%r8, -56(%rbp)
	call	_ZN2v82V813DisposeGlobalEPm@PLT
	movq	-56(%rbp), %r8
	movq	$0, 2952(%rbx)
.L569:
	testq	%r12, %r12
	je	.L570
	movq	%r12, %rsi
	movq	%r8, %rdi
	call	_ZN2v82V818GlobalizeReferenceEPNS_8internal7IsolateEPm@PLT
	movq	%rax, 2952(%rbx)
.L570:
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r12
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performance9ClearMarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L689
.L571:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC15(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L690
.L572:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L691
.L573:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L692
.L574:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC6(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L693
.L575:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L694
.L576:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r11
	movq	%rax, %r12
	popq	%rax
	testq	%r12, %r12
	je	.L695
.L577:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC7(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L696
.L578:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L697
.L579:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performance13MarkMilestoneERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r9
	popq	%r10
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L698
.L580:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC16(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L699
.L581:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L700
.L582:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rdi
	popq	%r8
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L701
.L583:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC17(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L702
.L584:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L703
.L585:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	movq	%rax, %r12
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%rcx
	popq	%rsi
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L704
.L586:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC18(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L705
.L587:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L706
.L588:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performanceL32InstallGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %r12
	popq	%rax
	popq	%rdx
	testq	%r12, %r12
	je	.L707
.L589:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC19(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L708
.L590:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L709
.L591:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performanceL31RemoveGarbageCollectionTrackingERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L710
.L592:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC20(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L711
.L593:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L712
.L594:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %r12
	movq	%rax, -56(%rbp)
	movq	352(%rbx), %rdi
	pushq	$0
	leaq	_ZN4node11performance6NotifyERKN2v820FunctionCallbackInfoINS1_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L713
.L595:
	movq	352(%rbx), %rdi
	movl	$-1, %ecx
	movl	$1, %edx
	leaq	.LC21(%rip), %rsi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L714
.L596:
	movq	-56(%rbp), %rsi
	movq	%r9, %rdx
	movq	%r12, %rcx
	movq	%r13, %rdi
	movq	%r9, -64(%rbp)
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	movq	-64(%rbp), %r9
	testb	%al, %al
	je	.L715
.L597:
	movq	%r9, %rsi
	movq	%r12, %rdi
	call	_ZN2v88Function7SetNameENS_5LocalINS_6StringEEE@PLT
	movq	%r15, %rdi
	call	_ZN2v86Object3NewEPNS_7IsolateE@PLT
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC22(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L716
.L598:
	movq	.LC23(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L717
.L599:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC24(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L718
.L600:
	movq	.LC25(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L719
.L601:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC26(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L720
.L602:
	movq	.LC27(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L721
.L603:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC28(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L722
.L604:
	movq	.LC29(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L723
.L605:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC30(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L724
.L606:
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L725
.L607:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC31(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L726
.L608:
	movq	.LC23(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L727
.L609:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC32(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L728
.L610:
	movq	.LC27(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L729
.L611:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC33(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L730
.L612:
	movq	.LC29(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L731
.L613:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC34(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L732
.L614:
	movsd	.LC35(%rip), %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L733
.L615:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC36(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L734
.L616:
	movsd	.LC37(%rip), %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L735
.L617:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC38(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L736
.L618:
	movsd	.LC39(%rip), %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$5, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L737
.L619:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC40(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L738
.L620:
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L739
.L621:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC41(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L740
.L622:
	movq	.LC25(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L741
.L623:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC42(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L742
.L624:
	movq	.LC23(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L743
.L625:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC43(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L744
.L626:
	movq	.LC44(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L745
.L627:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC45(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L746
.L628:
	movq	.LC27(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L747
.L629:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC46(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L748
.L630:
	movq	.LC47(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L749
.L631:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC48(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L750
.L632:
	movsd	.LC49(%rip), %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L751
.L633:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC50(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L752
.L634:
	pxor	%xmm0, %xmm0
	movq	%rdx, -64(%rbp)
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L753
.L635:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC51(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L754
.L636:
	movq	.LC25(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L755
.L637:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC52(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L756
.L638:
	movq	.LC23(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L757
.L639:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC53(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L758
.L640:
	movq	.LC44(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L759
.L641:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC54(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L760
.L642:
	movq	.LC27(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L761
.L643:
	movq	%r12, %rdi
	call	_ZN2v86Object10GetIsolateEv@PLT
	movq	%rax, %rdi
	movq	%rax, -64(%rbp)
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	movq	-64(%rbp), %rdi
	movl	$1, %edx
	movl	$-1, %ecx
	leaq	.LC55(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	-64(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L762
.L644:
	movq	.LC47(%rip), %rax
	movq	%rdx, -64(%rbp)
	movq	%rax, %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movq	%r12, %rdi
	movq	%rax, %rcx
	movl	$7, %r8d
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L763
.L645:
	movq	_ZN4node11performance10timeOriginE(%rip), %rax
	testq	%rax, %rax
	js	.L646
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L647:
	movq	%r15, %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	xorl	%edx, %edx
	movl	$10, %ecx
	movq	%r15, %rdi
	leaq	.LC56(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L764
.L648:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L765
.L649:
	movq	%r15, %rdi
	movsd	_ZN4node11performanceL19timeOriginTimestampE(%rip), %xmm0
	divsd	.LC57(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	xorl	%edx, %edx
	movl	$19, %ecx
	movq	%r15, %rdi
	leaq	.LC58(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L766
.L650:
	movl	$5, %r8d
	movq	%r9, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L767
.L651:
	movq	360(%rbx), %rax
	movq	%r12, %rcx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$5, %r8d
	movq	352(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L768
.L652:
	movq	%r15, %rdi
	movl	$12, %ecx
	xorl	%edx, %edx
	leaq	.LC59(%rip), %rsi
	call	_ZN2v86String14NewFromOneByteEPNS_7IsolateEPKhNS_13NewStringTypeEi@PLT
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L769
.L653:
	subq	$8, %rsp
	movq	2680(%rbx), %rdx
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	352(%rbx), %rdi
	pushq	$0
	movl	$1, %r9d
	leaq	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movq	%r15, %rsi
	movq	%rax, %r12
	movq	%rax, %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate16InstanceTemplateEv@PLT
	movl	$1, %esi
	movq	%rax, %rdi
	call	_ZN2v814ObjectTemplate21SetInternalFieldCountEi@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	movq	2680(%rbx), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	352(%rbx), %rdi
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramExceedsERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	movl	$0, (%rsp)
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC60(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L770
.L654:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMinERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC61(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L771
.L655:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramMaxERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC62(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L772
.L656:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L16ELDHistogramMeanERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC63(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L773
.L657:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramStddevERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC64(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L774
.L658:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC65(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L775
.L659:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC66(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r10
	popq	%r11
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L776
.L660:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L18ELDHistogramEnableERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC67(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%r8
	popq	%r9
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L777
.L661:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L19ELDHistogramDisableERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$-1, %ecx
	movl	$1, %edx
	movq	352(%rbx), %rdi
	leaq	.LC68(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	popq	%rcx
	popq	%rdi
	movq	%rax, %rsi
	testq	%rax, %rax
	je	.L778
.L662:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	352(%rbx), %rdi
	movq	%r12, %rsi
	call	_ZN2v89Signature3NewEPNS_7IsolateENS_5LocalINS_16FunctionTemplateEEE@PLT
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	2680(%rbx), %rdx
	movq	352(%rbx), %rdi
	pushq	$0
	movq	%rax, %rcx
	leaq	_ZN4node11performance12_GLOBAL__N_1L17ELDHistogramResetERKN2v820FunctionCallbackInfoINS2_5ValueEEE(%rip), %rsi
	call	_ZN2v816FunctionTemplate3NewEPNS_7IsolateEPFvRKNS_20FunctionCallbackInfoINS_5ValueEEEENS_5LocalIS4_EENSA_INS_9SignatureEEEiNS_19ConstructorBehaviorENS_14SideEffectTypeE@PLT
	movl	$1, %edx
	movl	$-1, %ecx
	movq	352(%rbx), %rdi
	leaq	.LC69(%rip), %rsi
	movq	%rax, -56(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	je	.L779
.L663:
	movq	%r12, %rdi
	movq	%rsi, -64(%rbp)
	call	_ZN2v816FunctionTemplate17PrototypeTemplateEv@PLT
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_ZN2v88Template3SetENS_5LocalINS_4NameEEENS1_INS_4DataEEENS_17PropertyAttributeE@PLT
	movq	-64(%rbp), %rsi
	movq	-56(%rbp), %rdi
	call	_ZN2v816FunctionTemplate12SetClassNameENS_5LocalINS_6StringEEE@PLT
	movq	3280(%rbx), %rsi
	movq	%r12, %rdi
	call	_ZN2v816FunctionTemplate11GetFunctionENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L780
.L664:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEENS1_INS_5ValueEEES5_@PLT
	testb	%al, %al
	je	.L781
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L646:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L763:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L645
	.p2align 4,,10
	.p2align 3
.L764:
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rdx
	jmp	.L648
	.p2align 4,,10
	.p2align 3
.L765:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L649
	.p2align 4,,10
	.p2align 3
.L766:
	movq	%r9, -64(%rbp)
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	movq	-56(%rbp), %rdx
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L767:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L651
	.p2align 4,,10
	.p2align 3
.L768:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L652
	.p2align 4,,10
	.p2align 3
.L769:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L770:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L654
	.p2align 4,,10
	.p2align 3
.L771:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L655
	.p2align 4,,10
	.p2align 3
.L772:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L773:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L658
	.p2align 4,,10
	.p2align 3
.L775:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L776:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L660
	.p2align 4,,10
	.p2align 3
.L777:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L661
	.p2align 4,,10
	.p2align 3
.L778:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L779:
	movq	%rsi, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rsi
	jmp	.L663
	.p2align 4,,10
	.p2align 3
.L780:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %rcx
	jmp	.L664
	.p2align 4,,10
	.p2align 3
.L781:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN2v82V817FromJustIsNothingEv@PLT
	.p2align 4,,10
	.p2align 3
.L682:
	.cfi_restore_state
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %r8
	jmp	.L561
	.p2align 4,,10
	.p2align 3
.L683:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L684:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %r8
	jmp	.L564
	.p2align 4,,10
	.p2align 3
.L685:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L686:
	movq	%rax, -56(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r10
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L687:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-56(%rbp), %r10
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L688:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L568
	.p2align 4,,10
	.p2align 3
.L689:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L690:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L691:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L692:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L693:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L694:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L576
	.p2align 4,,10
	.p2align 3
.L695:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L697:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L698:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L581
	.p2align 4,,10
	.p2align 3
.L700:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L701:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L702:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L703:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L704:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L705:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L706:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L707:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L590
	.p2align 4,,10
	.p2align 3
.L709:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L591
	.p2align 4,,10
	.p2align 3
.L710:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L711:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L712:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L713:
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	jmp	.L595
	.p2align 4,,10
	.p2align 3
.L714:
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-64(%rbp), %r9
	jmp	.L596
	.p2align 4,,10
	.p2align 3
.L715:
	movq	%r9, -56(%rbp)
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	movq	-56(%rbp), %r9
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L716:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L717:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L718:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L600
	.p2align 4,,10
	.p2align 3
.L719:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L601
	.p2align 4,,10
	.p2align 3
.L720:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L602
	.p2align 4,,10
	.p2align 3
.L721:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L722:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L723:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L724:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L725:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L607
	.p2align 4,,10
	.p2align 3
.L726:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L727:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L728:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L610
	.p2align 4,,10
	.p2align 3
.L729:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L611
	.p2align 4,,10
	.p2align 3
.L730:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L612
	.p2align 4,,10
	.p2align 3
.L731:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L613
	.p2align 4,,10
	.p2align 3
.L732:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L614
	.p2align 4,,10
	.p2align 3
.L733:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L615
	.p2align 4,,10
	.p2align 3
.L734:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L735:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L736:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L618
	.p2align 4,,10
	.p2align 3
.L737:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L619
	.p2align 4,,10
	.p2align 3
.L738:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L739:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L740:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L741:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L742:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L743:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L625
	.p2align 4,,10
	.p2align 3
.L744:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L626
	.p2align 4,,10
	.p2align 3
.L745:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L627
	.p2align 4,,10
	.p2align 3
.L746:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L747:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L748:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L749:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L631
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L632
	.p2align 4,,10
	.p2align 3
.L751:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L752:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L634
	.p2align 4,,10
	.p2align 3
.L753:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L635
	.p2align 4,,10
	.p2align 3
.L754:
	movq	%rdi, -72(%rbp)
	movq	%rax, -64(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdi
	movq	-64(%rbp), %rdx
	jmp	.L636
	.p2align 4,,10
	.p2align 3
.L755:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L637
	.p2align 4,,10
	.p2align 3
.L756:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L638
	.p2align 4,,10
	.p2align 3
.L757:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L639
	.p2align 4,,10
	.p2align 3
.L758:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L759:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L641
	.p2align 4,,10
	.p2align 3
.L760:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L642
	.p2align 4,,10
	.p2align 3
.L761:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L762:
	movq	%rax, -72(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-72(%rbp), %rdx
	movq	-64(%rbp), %rdi
	jmp	.L644
	.cfi_endproc
	.section	.text.unlikely
	.cfi_startproc
	.type	_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, @function
_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold:
.LFSB7673:
.L559:
	.cfi_def_cfa 6, 16
	.cfi_offset 3, -56
	.cfi_offset 6, -16
	.cfi_offset 12, -48
	.cfi_offset 13, -40
	.cfi_offset 14, -32
	.cfi_offset 15, -24
	movq	352, %rax
	ud2
	.cfi_endproc
.LFE7673:
	.text
	.size	_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv, .-_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.section	.text.unlikely
	.size	_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold, .-_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv.cold
.LCOLDE70:
	.text
.LHOTE70:
	.section	.rodata.str1.1
.LC71:
	.string	"bootstrapComplete"
.LC72:
	.string	"environment"
.LC73:
	.string	"v8Start"
.LC74:
	.string	"loopStart"
.LC75:
	.string	"loopExit"
.LC76:
	.string	"nodeStart"
.LC77:
	.string	"node,node.bootstrap"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm
	.type	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm, @function
_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm:
.LFB7611:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	movq	%r14, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdx, %rbx
	subq	$32, %rsp
	movq	64(%rdi), %rdx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	js	.L783
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rbx, %xmm0
.L784:
	movsd	%xmm0, (%rdx,%r14,8)
	movq	_ZZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEmE27trace_event_unique_atomic47(%rip), %r13
	testq	%r13, %r13
	je	.L818
.L786:
	testb	$5, 0(%r13)
	jne	.L819
.L782:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L820
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L819:
	.cfi_restore_state
	cmpl	$5, %r12d
	ja	.L789
	leaq	.L791(%rip), %rax
	movslq	(%rax,%r14,4), %rdx
	addq	%rax, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L791:
	.long	.L796-.L791
	.long	.L795-.L791
	.long	.L803-.L791
	.long	.L793-.L791
	.long	.L792-.L791
	.long	.L790-.L791
	.text
.L803:
	leaq	.LC73(%rip), %r12
.L794:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -64(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L797
	movq	(%rax), %rax
	movq	32(%rax), %r10
	leaq	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl(%rip), %rax
	cmpq	%rax, %r10
	jne	.L821
.L797:
	movq	-56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L798
	movq	(%rdi), %rax
	call	*8(%rax)
.L798:
	movq	-64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L782
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L782
.L795:
	leaq	.LC76(%rip), %r12
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L783:
	movq	%rbx, %rax
	movq	%rbx, %rcx
	pxor	%xmm0, %xmm0
	shrq	%rax
	andl	$1, %ecx
	orq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L818:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r13
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L787
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r13
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L822
.L787:
	movq	%r13, _ZZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEmE27trace_event_unique_atomic47(%rip)
	mfence
	jmp	.L786
.L792:
	leaq	.LC75(%rip), %r12
	jmp	.L794
.L790:
	leaq	.LC71(%rip), %r12
	jmp	.L794
.L796:
	leaq	.LC72(%rip), %r12
	jmp	.L794
.L793:
	leaq	.LC74(%rip), %r12
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L822:
	leaq	.LC77(%rip), %rsi
	call	*%rax
	movq	%rax, %r13
	jmp	.L787
	.p2align 4,,10
	.p2align 3
.L821:
	shrq	$3, %rbx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r12, %rcx
	movq	%rbx, %rax
	movl	$73, %esi
	movabsq	$2361183241434822607, %rdx
	mulq	%rdx
	leaq	-64(%rbp), %rax
	shrq	$4, %rdx
	pushq	%rdx
	movq	%r13, %rdx
	pushq	$16
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L797
.L820:
	call	__stack_chk_fail@PLT
.L789:
	leaq	_ZZN4node11performanceL27GetPerformanceMilestoneNameENS0_20PerformanceMilestoneEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.cfi_endproc
.LFE7611:
	.size	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm, .-_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm
	.p2align 4
	.globl	_ZN4node11performance13MarkMilestoneERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance13MarkMilestoneERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance13MarkMilestoneERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7623:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L831
	cmpw	$1040, %cx
	jne	.L824
.L831:
	movq	23(%rdx), %r12
.L826:
	movl	16(%rbx), %eax
	movq	3280(%r12), %rsi
	testl	%eax, %eax
	jg	.L827
	movq	(%rbx), %rax
	movq	8(%rax), %rdi
	addq	$88, %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	je	.L833
.L829:
	sarq	$32, %rbx
	cmpl	$6, %ebx
	jne	.L834
.L823:
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L827:
	.cfi_restore_state
	movq	8(%rbx), %rdi
	call	_ZNK2v85Value10Int32ValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rax, %rbx
	testb	%al, %al
	jne	.L829
.L833:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	sarq	$32, %rbx
	cmpl	$6, %ebx
	je	.L823
.L834:
	movq	1864(%r12), %r12
	call	uv_hrtime@PLT
	movl	%ebx, %esi
	popq	%rbx
	movq	%r12, %rdi
	movq	%rax, %rdx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEm
	.p2align 4,,10
	.p2align 3
.L824:
	.cfi_restore_state
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L826
	.cfi_endproc
.LFE7623:
	.size	_ZN4node11performance13MarkMilestoneERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance13MarkMilestoneERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11performance16PerformanceEntry3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.type	_ZN4node11performance16PerformanceEntry3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance16PerformanceEntry3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7614:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$2264, %rsp
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L882
	cmpw	$1040, %cx
	jne	.L836
.L882:
	movq	23(%rdx), %r13
.L838:
	movl	16(%rbx), %edx
	movq	352(%r13), %r12
	testl	%edx, %edx
	jg	.L839
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L840:
	leaq	-2160(%rbp), %rdi
	movq	%r12, %rsi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	cmpl	$1, 16(%rbx)
	jg	.L841
	movq	(%rbx), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L842:
	movq	%r12, %rsi
	leaq	-1104(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	call	uv_hrtime@PLT
	movq	-2144(%rbp), %r15
	movq	%r13, -2248(%rbp)
	movq	%rax, -2296(%rbp)
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	-1088(%rbp), %r12
	movq	%rax, -2256(%rbp)
	leaq	-2224(%rbp), %rax
	movq	%rax, -2280(%rbp)
	movq	%rax, -2240(%rbp)
	testq	%r15, %r15
	je	.L878
	movq	%r15, %rdi
	call	strlen@PLT
	movq	%rax, -2264(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L898
	cmpq	$1, %rax
	jne	.L846
	movzbl	(%r15), %edx
	movb	%dl, -2224(%rbp)
	movq	-2280(%rbp), %rdx
.L847:
	movq	%rax, -2232(%rbp)
	movb	$0, (%rdx,%rax)
	leaq	-2192(%rbp), %rax
	movq	%rax, -2288(%rbp)
	movq	%rax, -2208(%rbp)
	testq	%r12, %r12
	je	.L878
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, -2264(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L899
	cmpq	$1, %rax
	jne	.L849
	movzbl	(%r12), %edx
	movb	%dl, -2192(%rbp)
	movq	-2288(%rbp), %rdx
.L850:
	movq	%rax, -2200(%rbp)
	movl	$-1, %ecx
	movq	-2296(%rbp), %xmm0
	movb	$0, (%rdx,%rax)
	movq	-2248(%rbp), %r12
	xorl	%edx, %edx
	movq	-2240(%rbp), %rsi
	movq	8(%rbx), %rbx
	punpcklqdq	%xmm0, %xmm0
	movq	352(%r12), %r15
	movq	3280(%r12), %r14
	movaps	%xmm0, -2176(%rbp)
	addq	$8, %rbx
	movq	%r15, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L900
.L851:
	movq	360(%r12), %rax
	movl	$5, %r8d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	1056(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L901
.L852:
	movq	-2208(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L902
.L853:
	movq	360(%r12), %rax
	movl	$5, %r8d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	616(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L903
.L854:
	movq	-2176(%rbp), %rax
	subq	_ZN4node11performance10timeOriginE(%rip), %rax
	js	.L855
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L856:
	movq	%r15, %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$5, %r8d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	360(%r12), %rax
	movq	1664(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L904
.L857:
	movq	-2168(%rbp), %rax
	subq	-2176(%rbp), %rax
	js	.L858
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L859:
	movq	%r15, %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$5, %r8d
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	360(%r12), %rax
	movq	568(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L905
.L860:
	movq	-2208(%rbp), %rax
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	3280(%r13), %r12
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L861
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L862
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L863
	cmpb	$103, (%rax)
	jne	.L883
	cmpb	$99, 1(%rax)
	jne	.L883
	cmpb	$0, 2(%rax)
	jne	.L883
	movq	%r12, %rdi
	movq	%rbx, -2264(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r13), %rdx
	movl	$12, %eax
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L878:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L846:
	testq	%rax, %rax
	jne	.L906
	movq	-2280(%rbp), %rdx
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L883:
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L866
	movl	$6, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L867
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	movl	$5, %ecx
	repz cmpsb
	movq	%rbx, -2264(%rbp)
	movq	%r12, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L868
	call	_ZN2v87Context5EnterEv@PLT
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L839:
	movq	8(%rbx), %rdx
	jmp	.L840
	.p2align 4,,10
	.p2align 3
.L841:
	movq	8(%rbx), %rax
	leaq	-8(%rax), %rdx
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L858:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L855:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L856
	.p2align 4,,10
	.p2align 3
.L861:
	movq	%r12, %rdi
	movq	%rbx, -2264(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r13), %rdx
	xorl	%eax, %eax
.L870:
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	jne	.L907
.L869:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	-2208(%rbp), %rdi
	movq	%rax, -2256(%rbp)
	cmpq	-2288(%rbp), %rdi
	je	.L871
	call	_ZdlPv@PLT
.L871:
	movq	-2240(%rbp), %rdi
	cmpq	-2280(%rbp), %rdi
	je	.L872
	call	_ZdlPv@PLT
.L872:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L873
	testq	%rdi, %rdi
	je	.L873
	call	free@PLT
.L873:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L835
	testq	%rdi, %rdi
	je	.L835
	call	free@PLT
.L835:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L908
	addq	$2264, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L849:
	.cfi_restore_state
	testq	%r14, %r14
	jne	.L909
	movq	-2288(%rbp), %rdx
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L898:
	leaq	-2240(%rbp), %rdi
	leaq	-2264(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -2240(%rbp)
	movq	%rax, %rdi
	movq	-2264(%rbp), %rax
	movq	%rax, -2224(%rbp)
.L845:
	movq	%r14, %rdx
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	-2264(%rbp), %rax
	movq	-2240(%rbp), %rdx
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L899:
	leaq	-2208(%rbp), %rdi
	leaq	-2264(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -2208(%rbp)
	movq	%rax, %rdi
	movq	-2264(%rbp), %rax
	movq	%rax, -2192(%rbp)
.L848:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-2264(%rbp), %rax
	movq	-2208(%rbp), %rdx
	jmp	.L850
	.p2align 4,,10
	.p2align 3
.L862:
	movq	%r12, %rdi
	movq	%rbx, -2264(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r13), %rdx
	movl	$4, %eax
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	je	.L869
.L907:
	xorl	%eax, %eax
	movq	2944(%r13), %rdx
	movq	352(%r13), %rdi
	leaq	-2264(%rbp), %r8
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movl	$1, %ecx
	movq	-2264(%rbp), %rsi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	jmp	.L869
	.p2align 4,,10
	.p2align 3
.L836:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r13
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L904:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L903:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L854
	.p2align 4,,10
	.p2align 3
.L902:
	movq	%rax, -2296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2296(%rbp), %rcx
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L901:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L852
	.p2align 4,,10
	.p2align 3
.L905:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L860
	.p2align 4,,10
	.p2align 3
.L900:
	movq	%rax, -2296(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-2296(%rbp), %rcx
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L863:
	movq	%r12, %rdi
	movq	%rbx, -2264(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r13), %rdx
	movl	$8, %eax
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%r12, %rdi
	movq	%rbx, -2264(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r13), %rdx
	movl	$16, %eax
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L868:
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r13), %rdx
	movl	$24, %eax
	jmp	.L870
	.p2align 4,,10
	.p2align 3
.L867:
	movq	%r12, %rdi
	movq	%rbx, -2264(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r13), %rdx
	movl	$20, %eax
	jmp	.L870
.L908:
	call	__stack_chk_fail@PLT
.L906:
	movq	-2280(%rbp), %rdi
	jmp	.L845
.L909:
	movq	-2288(%rbp), %rdi
	jmp	.L848
	.cfi_endproc
.LFE7614:
	.size	_ZN4node11performance16PerformanceEntry3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance16PerformanceEntry3NewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11performance16PerformanceEntry6NotifyEPNS_11EnvironmentENS0_20PerformanceEntryTypeEN2v85LocalINS5_5ValueEEE
	.type	_ZN4node11performance16PerformanceEntry6NotifyEPNS_11EnvironmentENS0_20PerformanceEntryTypeEN2v85LocalINS5_5ValueEEE, @function
_ZN4node11performance16PerformanceEntry6NotifyEPNS_11EnvironmentENS0_20PerformanceEntryTypeEN2v85LocalINS5_5ValueEEE:
.LFB7618:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -40
	movl	%esi, %ebx
	subq	$24, %rsp
	movq	3280(%rdi), %r13
	movq	%rdx, -40(%rbp)
	movq	%r13, %rdi
	call	_ZN2v87Context5EnterEv@PLT
	cmpl	$7, %ebx
	je	.L911
	movq	1864(%r12), %rax
	movq	104(%rax), %rax
	movl	(%rax,%rbx,4), %eax
	testl	%eax, %eax
	jne	.L916
.L911:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L916:
	.cfi_restore_state
	xorl	%eax, %eax
	movq	-40(%rbp), %rsi
	leaq	-40(%rbp), %r8
	movl	$1, %ecx
	movq	2944(%r12), %rdx
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movq	352(%r12), %rdi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	jmp	.L911
	.cfi_endproc
.LFE7618:
	.size	_ZN4node11performance16PerformanceEntry6NotifyEPNS_11EnvironmentENS0_20PerformanceEntryTypeEN2v85LocalINS5_5ValueEEE, .-_ZN4node11performance16PerformanceEntry6NotifyEPNS_11EnvironmentENS0_20PerformanceEntryTypeEN2v85LocalINS5_5ValueEEE
	.p2align 4
	.globl	_ZN4node11performance21PerformanceGCCallbackEPNS_11EnvironmentESt10unique_ptrINS0_18GCPerformanceEntryESt14default_deleteIS4_EE
	.type	_ZN4node11performance21PerformanceGCCallbackEPNS_11EnvironmentESt10unique_ptrINS0_18GCPerformanceEntryESt14default_deleteIS4_EE, @function
_ZN4node11performance21PerformanceGCCallbackEPNS_11EnvironmentESt10unique_ptrINS0_18GCPerformanceEntryESt14default_deleteIS4_EE:
.LFB7627:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	leaq	-80(%rbp), %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	352(%rdi), %rsi
	movq	%r14, %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	3280(%rbx), %rax
	movq	%rax, -104(%rbp)
	movq	1864(%rbx), %rax
	movq	104(%rax), %rax
	movl	12(%rax), %edx
	testl	%edx, %edx
	je	.L918
	movq	0(%r13), %r15
	leaq	_ZNK4node11performance16PerformanceEntry8ToObjectEv(%rip), %rdx
	movq	(%r15), %rax
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L919
	movq	8(%r15), %rax
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	2952(%rax), %rdi
	movq	3280(%rax), %rsi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L918
	movq	8(%r15), %rax
	movq	16(%r15), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%rax), %rdi
	movq	%rax, -112(%rbp)
	movq	3280(%rax), %rax
	movq	%rdi, -128(%rbp)
	movq	%rax, -120(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L958
.L945:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rsi
	movl	$5, %r8d
	movq	%r12, %rdi
	movq	360(%rax), %rax
	movq	1056(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L959
.L921:
	movq	48(%r15), %rsi
	movq	-128(%rbp), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L960
.L922:
	movq	-112(%rbp), %rax
	movq	-120(%rbp), %rsi
	movl	$5, %r8d
	movq	%r12, %rdi
	movq	360(%rax), %rax
	movq	616(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L961
.L923:
	movq	80(%r15), %rax
	subq	_ZN4node11performance10timeOriginE(%rip), %rax
	js	.L924
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L925:
	movq	-128(%rbp), %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-120(%rbp), %rsi
	movl	$5, %r8d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	-112(%rbp), %rax
	movq	360(%rax), %rax
	movq	1664(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L962
.L926:
	movq	88(%r15), %rax
	subq	80(%r15), %rax
	js	.L927
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L928:
	movq	-128(%rbp), %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-120(%rbp), %rsi
	movl	$5, %r8d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	-112(%rbp), %rax
	movq	360(%rax), %rax
	movq	568(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L963
.L930:
	movq	0(%r13), %rax
	movq	352(%rbx), %rdi
	movl	96(%rax), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-104(%rbp), %rsi
	movl	$5, %r8d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	976(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L964
.L942:
	movq	0(%r13), %rax
	movq	352(%rbx), %rdi
	movl	100(%rax), %esi
	call	_ZN2v87Integer3NewEPNS_7IsolateEi@PLT
	movq	-104(%rbp), %rsi
	movl	$5, %r8d
	movq	%r12, %rdi
	movq	%rax, %rcx
	movq	360(%rbx), %rax
	movq	768(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L965
.L931:
	movq	0(%r13), %rax
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	3280(%rbx), %r13
	movq	48(%rax), %rax
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L966
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L934
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L935
	cmpb	$103, (%rax)
	jne	.L947
	cmpb	$99, 1(%rax)
	je	.L967
.L947:
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L938
	movl	$6, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L939
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	movl	$5, %ecx
	repz cmpsb
	movq	%r12, -88(%rbp)
	movq	%r13, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L940
	call	_ZN2v87Context5EnterEv@PLT
	.p2align 4,,10
	.p2align 3
.L941:
	movq	%r13, %rdi
	call	_ZN2v87Context4ExitEv@PLT
.L918:
	movq	%r14, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L968
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L924:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L925
	.p2align 4,,10
	.p2align 3
.L966:
	movq	%r13, %rdi
	movq	%r12, -88(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%rbx), %rdx
	xorl	%eax, %eax
.L933:
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	je	.L941
	xorl	%eax, %eax
	movq	-88(%rbp), %rsi
	leaq	-88(%rbp), %r8
	movl	$1, %ecx
	movq	2944(%rbx), %rdx
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movq	352(%rbx), %rdi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L919:
	movq	%r15, %rdi
	call	*%rax
	movq	%rax, %r12
	testq	%rax, %rax
	jne	.L930
	jmp	.L918
	.p2align 4,,10
	.p2align 3
.L934:
	movq	%r13, %rdi
	movq	%r12, -88(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%rbx), %rdx
	movl	$4, %eax
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L927:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L928
	.p2align 4,,10
	.p2align 3
.L964:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L965:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L935:
	movq	%r13, %rdi
	movq	%r12, -88(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%rbx), %rdx
	movl	$8, %eax
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L967:
	cmpb	$0, 2(%rax)
	jne	.L947
	movq	%r13, %rdi
	movq	%r12, -88(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%rbx), %rdx
	movl	$12, %eax
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L938:
	movq	%r13, %rdi
	movq	%r12, -88(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%rbx), %rdx
	movl	$16, %eax
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L940:
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%rbx), %rdx
	movl	$24, %eax
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L961:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L923
	.p2align 4,,10
	.p2align 3
.L963:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L930
	.p2align 4,,10
	.p2align 3
.L962:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L958:
	movq	%rcx, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rcx
	jmp	.L945
	.p2align 4,,10
	.p2align 3
.L959:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L921
	.p2align 4,,10
	.p2align 3
.L960:
	movq	%rax, -136(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-136(%rbp), %rcx
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L939:
	movq	%r13, %rdi
	movq	%r12, -88(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%rbx), %rdx
	movl	$20, %eax
	jmp	.L933
.L968:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7627:
	.size	_ZN4node11performance21PerformanceGCCallbackEPNS_11EnvironmentESt10unique_ptrINS0_18GCPerformanceEntryESt14default_deleteIS4_EE, .-_ZN4node11performance21PerformanceGCCallbackEPNS_11EnvironmentESt10unique_ptrINS0_18GCPerformanceEntryESt14default_deleteIS4_EE
	.align 2
	.p2align 4
	.type	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_E4CallES2_, @function
_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_E4CallES2_:
.LFB10370:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsi, %r8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	24(%rdi), %rax
	movq	$0, 24(%rdi)
	leaq	-16(%rbp), %rsi
	movq	%r8, %rdi
	movq	%rax, -16(%rbp)
	call	_ZN4node11performance21PerformanceGCCallbackEPNS_11EnvironmentESt10unique_ptrINS0_18GCPerformanceEntryESt14default_deleteIS4_EE
	movq	-16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L969
	movq	(%rdi), %rax
	call	*8(%rax)
.L969:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L976
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L976:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE10370:
	.size	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_E4CallES2_, .-_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_E4CallES2_
	.section	.text._ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE,"axG",@progbits,_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE,comdat
	.p2align 4
	.weak	_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE
	.type	_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE, @function
_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE:
.LFB7638:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -32
	call	_ZNK2v88Function12GetDebugNameEv@PLT
	movq	%rax, %r12
	testq	%rax, %rax
	je	.L978
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L979
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	jne	.L979
	cmpl	$5, 43(%rax)
	jne	.L979
.L978:
	movq	%r13, %rdi
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L979
	movq	(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L980
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L991
.L980:
	call	_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE
	movq	%rax, %r12
.L979:
	movq	%r12, %rax
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L991:
	.cfi_restore_state
	cmpl	$5, 43(%rax)
	je	.L979
	jmp	.L980
	.cfi_endproc
.LFE7638:
	.size	_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE, .-_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE
	.section	.rodata.str1.8
	.align 8
.LC79:
	.string	"node,node.perf,node.perf.timerify"
	.text
	.p2align 4
	.globl	_ZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7639:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$4096, %rsp
	orq	$0, (%rsp)
	subq	$1256, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	(%rdi), %rax
	movq	%rdi, %rbx
	movq	8(%rax), %r14
	movq	%r14, %rdi
	call	_ZN2v87Isolate17GetCurrentContextEv@PLT
	testq	%rax, %rax
	je	.L994
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_ZN2v87Context29GetNumberOfEmbedderDataFieldsEv@PLT
	cmpl	$35, %eax
	jbe	.L994
	movq	(%r12), %rax
	movq	_ZN4node11Environment18kNodeContextTagPtrE(%rip), %rsi
	movq	55(%rax), %rax
	cmpq	%rsi, 295(%rax)
	jne	.L994
	movq	271(%rax), %rax
	movq	%rax, -9448(%rbp)
	testq	%rax, %rax
	je	.L994
	leaq	-8248(%rbp), %rax
	movq	(%rbx), %r15
	movslq	16(%rbx), %r13
	leaq	-56(%rbp), %rdx
	movdqa	.LC78(%rip), %xmm0
	movq	%rax, -9456(%rbp)
	movq	%rax, -8256(%rbp)
	movaps	%xmm0, -8272(%rbp)
	pxor	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L995:
	movups	%xmm0, (%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L995
	movq	$0, -8248(%rbp)
	testq	%r13, %r13
	jne	.L996
.L1007:
	addq	$32, %r15
	movq	%r15, %rdi
	call	_ZNK2v88Function12GetDebugNameEv@PLT
	movq	%rax, %rdx
	testq	%rax, %rax
	je	.L997
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1009
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1009
	cmpl	$5, 43(%rax)
	jne	.L1009
.L997:
	movq	%r15, %rdi
	movq	%rdx, -9464(%rbp)
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	-9464(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1009
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1010
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	je	.L1186
.L1010:
	movq	%rdi, -9464(%rbp)
	call	_ZNK2v88Function12GetDebugNameEv@PLT
	movq	-9464(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L1011
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1009
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1009
	cmpl	$5, 43(%rax)
	jne	.L1009
.L1011:
	movq	%rdx, -9464(%rbp)
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	-9464(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1009
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1013
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1013
	cmpl	$5, 43(%rax)
	je	.L1009
.L1013:
	movq	%rdi, -9464(%rbp)
	call	_ZNK2v88Function12GetDebugNameEv@PLT
	movq	-9464(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L1014
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1009
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1009
	cmpl	$5, 43(%rax)
	jne	.L1009
.L1014:
	movq	%rdx, -9464(%rbp)
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	-9464(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1009
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1016
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1016
	cmpl	$5, 43(%rax)
	je	.L1009
.L1016:
	movq	%rdi, -9464(%rbp)
	call	_ZNK2v88Function12GetDebugNameEv@PLT
	movq	-9464(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L1017
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1009
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1009
	cmpl	$5, 43(%rax)
	jne	.L1009
.L1017:
	movq	%rdx, -9464(%rbp)
	call	_ZNK2v88Function16GetBoundFunctionEv@PLT
	movq	-9464(%rbp), %rdx
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1009
	movq	(%rax), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	subq	$1, %rcx
	jne	.L1019
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1019
	cmpl	$5, 43(%rax)
	je	.L1009
.L1019:
	call	_ZN4node11performance7GetNameEN2v85LocalINS1_8FunctionEEE
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L1009:
	movq	%r14, %rsi
	leaq	-9328(%rbp), %rdi
	xorl	%r14d, %r14d
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	(%rbx), %rax
	movq	40(%rax), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1020
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1187
.L1020:
	call	uv_hrtime@PLT
	movq	%rax, -9464(%rbp)
	movq	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic340(%rip), %rax
	movq	%rax, %r10
	testq	%rax, %rax
	je	.L1188
.L1022:
	testb	$5, (%r10)
	jne	.L1189
.L1024:
	movq	-8256(%rbp), %r8
	movq	-8272(%rbp), %rax
	testb	%r14b, %r14b
	jne	.L1028
	movq	%r8, %rcx
	movl	%eax, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %r14
.L1029:
	call	uv_hrtime@PLT
	movq	%rax, -9472(%rbp)
	movq	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic353(%rip), %rax
	movq	%rax, %r15
	testq	%rax, %rax
	je	.L1190
.L1031:
	movq	-9312(%rbp), %r9
	testb	$5, (%r15)
	jne	.L1191
.L1033:
	testq	%r14, %r14
	je	.L1037
	movq	(%r14), %rdx
	movq	(%rbx), %rax
	movq	-9448(%rbp), %rsi
	movq	%rdx, 24(%rax)
	movq	1864(%rsi), %rax
	movq	104(%rax), %rax
	movl	16(%rax), %edx
	testl	%edx, %edx
	je	.L1037
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%rsi, -9416(%rbp)
	movq	%rax, -9424(%rbp)
	leaq	-9392(%rbp), %rax
	movq	%rax, -9480(%rbp)
	movq	%rax, -9408(%rbp)
	testq	%r9, %r9
	je	.L1192
	movq	%r9, %rdi
	movq	%r9, -9488(%rbp)
	call	strlen@PLT
	movq	-9488(%rbp), %r9
	cmpq	$15, %rax
	movq	%rax, -9432(%rbp)
	movq	%rax, %r14
	ja	.L1193
	cmpq	$1, %rax
	jne	.L1041
	movzbl	(%r9), %edx
	movb	%dl, -9392(%rbp)
	movq	-9480(%rbp), %rdx
.L1042:
	movq	%rax, -9400(%rbp)
	leaq	-9424(%rbp), %rdi
	movq	-9464(%rbp), %xmm0
	movb	$0, (%rdx,%rax)
	leaq	-9360(%rbp), %rax
	movq	%rax, -9488(%rbp)
	movhps	-9472(%rbp), %xmm0
	movq	%rax, -9376(%rbp)
	movabsq	$7957695015192261990, %rax
	movq	%rax, -9360(%rbp)
	movq	$8, -9368(%rbp)
	movb	$0, -9352(%rbp)
	movaps	%xmm0, -9344(%rbp)
	call	_ZNK4node11performance16PerformanceEntry8ToObjectEv
	movq	%rax, %r14
	testq	%rax, %rax
	je	.L1194
	testq	%r13, %r13
	je	.L1050
	xorl	%r15d, %r15d
	jmp	.L1052
	.p2align 4,,10
	.p2align 3
.L1196:
	cmpl	%r15d, 16(%rbx)
	jle	.L1046
	movq	8(%rbx), %rcx
	movslq	%r15d, %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	salq	$3, %rax
	subq	%rax, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	je	.L1195
.L1049:
	addq	$1, %r15
	cmpq	%r15, %r13
	je	.L1050
.L1052:
	movl	%r15d, %edx
	testl	%r15d, %r15d
	jns	.L1196
.L1046:
	movq	(%rbx), %rax
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	8(%rax), %rcx
	addq	$88, %rcx
	call	_ZN2v86Object3SetENS_5LocalINS_7ContextEEEjNS1_INS_5ValueEEE@PLT
	testb	%al, %al
	jne	.L1049
.L1195:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	addq	$1, %r15
	cmpq	%r15, %r13
	jne	.L1052
	.p2align 4,,10
	.p2align 3
.L1050:
	movq	-9376(%rbp), %rax
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	movq	-9448(%rbp), %rsi
	movq	3280(%rsi), %r12
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1053
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1054
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1055
	cmpb	$103, (%rax)
	jne	.L1082
	cmpb	$99, 1(%rax)
	jne	.L1082
	cmpb	$0, 2(%rax)
	jne	.L1082
	movq	%r12, %rdi
	movq	%r14, -9432(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-9448(%rbp), %rax
	movq	1864(%rax), %rdx
	movl	$12, %eax
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L996:
	cmpq	$1024, %r13
	ja	.L1197
	movq	-9456(%rbp), %rcx
.L999:
	movq	%r13, -8272(%rbp)
	xorl	%eax, %eax
	jmp	.L1008
	.p2align 4,,10
	.p2align 3
.L1198:
	cmpl	%eax, 16(%rbx)
	jle	.L1004
	movq	8(%rbx), %rsi
	salq	$3, %rdx
	subq	%rdx, %rsi
	movq	%rsi, %rdx
.L1006:
	movq	%rdx, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r13
	je	.L1007
.L1008:
	movslq	%eax, %rdx
	testl	%eax, %eax
	jns	.L1198
.L1004:
	movq	(%rbx), %rdx
	movq	8(%rdx), %rdx
	addq	$88, %rdx
	jmp	.L1006
	.p2align 4,,10
	.p2align 3
.L1194:
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	-9376(%rbp), %rdi
	movq	%rax, -9424(%rbp)
	cmpq	-9488(%rbp), %rdi
	je	.L1072
	call	_ZdlPv@PLT
.L1072:
	movq	-9408(%rbp), %rdi
	cmpq	-9480(%rbp), %rdi
	je	.L1045
	call	_ZdlPv@PLT
.L1045:
	movq	-9312(%rbp), %r9
	.p2align 4,,10
	.p2align 3
.L1037:
	testq	%r9, %r9
	je	.L1069
	leaq	-9304(%rbp), %rax
	cmpq	%rax, %r9
	je	.L1069
	movq	%r9, %rdi
	call	free@PLT
.L1069:
	movq	-8256(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L992
	cmpq	-9456(%rbp), %rdi
	je	.L992
.L1185:
	call	free@PLT
.L992:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1199
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1028:
	.cfi_restore_state
	movq	8(%rbx), %rsi
	movl	%eax, %ecx
	movq	%r15, %rdi
	leaq	8(%rsi), %rdx
	movq	%r12, %rsi
	call	_ZN2v88Function4CallENS_5LocalINS_7ContextEEENS1_INS_5ValueEEEiPS5_@PLT
	movq	%rax, %r14
	jmp	.L1029
	.p2align 4,,10
	.p2align 3
.L994:
	leaq	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1197:
	movabsq	$2305843009213693951, %rax
	leaq	0(,%r13,8), %rdi
	andq	%r13, %rax
	cmpq	%rax, %r13
	jne	.L1200
	testq	%rdi, %rdi
	je	.L1001
	movq	%rdi, -9464(%rbp)
	call	malloc@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1201
	movq	%rax, -8256(%rbp)
	movq	%r13, -8264(%rbp)
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1187:
	cmpl	$5, 43(%rax)
	sete	%r14b
	jmp	.L1020
	.p2align 4,,10
	.p2align 3
.L1193:
	leaq	-9408(%rbp), %rdi
	leaq	-9432(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-9488(%rbp), %r9
	movq	%rax, -9408(%rbp)
	movq	%rax, %rdi
	movq	-9432(%rbp), %rax
	movq	%rax, -9392(%rbp)
.L1040:
	movq	%r14, %rdx
	movq	%r9, %rsi
	call	memcpy@PLT
	movq	-9432(%rbp), %rax
	movq	-9408(%rbp), %rdx
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1192:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1082:
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1058
	movl	$6, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1059
	leaq	.LC10(%rip), %rdi
	movq	%rax, %rsi
	movl	$5, %ecx
	repz cmpsb
	movq	%r14, -9432(%rbp)
	movq	%r12, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1060
	call	_ZN2v87Context5EnterEv@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1191:
	pxor	%xmm0, %xmm0
	movq	%r9, -9480(%rbp)
	movaps	%xmm0, -9424(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1034
	movq	(%rax), %rax
	movq	-9480(%rbp), %r9
	movq	32(%rax), %r10
	leaq	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1202
.L1034:
	movq	-9416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1035
	movq	(%rdi), %rax
	call	*8(%rax)
.L1035:
	movq	-9424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1036
	movq	(%rdi), %rax
	call	*8(%rax)
.L1036:
	movq	-9312(%rbp), %r9
	jmp	.L1033
	.p2align 4,,10
	.p2align 3
.L1190:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r15
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1032
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r15
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1203
.L1032:
	movq	%r15, _ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic353(%rip)
	mfence
	jmp	.L1031
	.p2align 4,,10
	.p2align 3
.L1189:
	movq	-9312(%rbp), %rax
	pxor	%xmm0, %xmm0
	movq	%r10, -9472(%rbp)
	movaps	%xmm0, -9424(%rbp)
	movq	%rax, -9480(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1025
	movq	(%rax), %rax
	movq	-9472(%rbp), %r10
	movq	32(%rax), %r11
	leaq	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl(%rip), %rax
	cmpq	%rax, %r11
	jne	.L1204
.L1025:
	movq	-9416(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1026
	movq	(%rdi), %rax
	call	*8(%rax)
.L1026:
	movq	-9424(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1024
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1024
	.p2align 4,,10
	.p2align 3
.L1188:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r10
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1023
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r10
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1205
.L1023:
	movq	%r10, _ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic340(%rip)
	mfence
	jmp	.L1022
	.p2align 4,,10
	.p2align 3
.L1041:
	testq	%rax, %rax
	jne	.L1206
	movq	-9480(%rbp), %rdx
	jmp	.L1042
	.p2align 4,,10
	.p2align 3
.L1053:
	movq	%r12, %rdi
	movq	%r14, -9432(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-9448(%rbp), %rax
	movq	1864(%rax), %rdx
	xorl	%eax, %eax
.L1062:
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	jne	.L1207
.L1061:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	-9376(%rbp), %rdi
	movq	%rax, -9424(%rbp)
	cmpq	-9488(%rbp), %rdi
	je	.L1063
	call	_ZdlPv@PLT
.L1063:
	movq	-9408(%rbp), %rdi
	cmpq	-9480(%rbp), %rdi
	je	.L1064
	call	_ZdlPv@PLT
.L1064:
	movq	-9312(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1065
	leaq	-9304(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1065
	call	free@PLT
.L1065:
	movq	-8256(%rbp), %rdi
	cmpq	-9456(%rbp), %rdi
	je	.L992
	testq	%rdi, %rdi
	jne	.L1185
	jmp	.L992
.L1186:
	cmpl	$5, 43(%rax)
	je	.L1009
	jmp	.L1010
.L1058:
	movq	%r12, %rdi
	movq	%r14, -9432(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-9448(%rbp), %rax
	movq	1864(%rax), %rdx
	movl	$16, %eax
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1054:
	movq	%r12, %rdi
	movq	%r14, -9432(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-9448(%rbp), %rax
	movq	1864(%rax), %rdx
	movl	$4, %eax
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1207:
	movq	-9448(%rbp), %rsi
	xorl	%eax, %eax
	movl	$1, %ecx
	leaq	-9432(%rbp), %r8
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movq	2944(%rsi), %rdx
	movq	352(%rsi), %rdi
	movq	-9432(%rbp), %rsi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	jmp	.L1061
	.p2align 4,,10
	.p2align 3
.L1205:
	leaq	.LC79(%rip), %rsi
	call	*%rax
	movq	%rax, %r10
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1203:
	leaq	.LC79(%rip), %rsi
	call	*%rax
	movq	%rax, %r15
	jmp	.L1032
	.p2align 4,,10
	.p2align 3
.L1202:
	movq	-9472(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$101, %esi
	movabsq	$2361183241434822607, %rcx
	shrq	$3, %rdx
	movq	%rdx, %rax
	mulq	%rcx
	leaq	-9424(%rbp), %rax
	movq	%r9, %rcx
	shrq	$4, %rdx
	pushq	%rdx
	movq	%r15, %rdx
	pushq	$7
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1204:
	movq	-9464(%rbp), %rdx
	xorl	%r8d, %r8d
	movl	$98, %esi
	movabsq	$2361183241434822607, %rcx
	shrq	$3, %rdx
	movq	%rdx, %rax
	mulq	%rcx
	leaq	-9424(%rbp), %rax
	movq	-9480(%rbp), %rcx
	movq	%rcx, %r9
	shrq	$4, %rdx
	pushq	%rdx
	movq	%r10, %rdx
	pushq	$7
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r11
	addq	$64, %rsp
	jmp	.L1025
	.p2align 4,,10
	.p2align 3
.L1001:
	leaq	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1201:
	call	_ZN4node21LowMemoryNotificationEv@PLT
	movq	-9464(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1001
	movq	-8272(%rbp), %rax
	movq	%rcx, -8256(%rbp)
	movq	%r13, -8264(%rbp)
	testq	%rax, %rax
	je	.L999
	movq	-9456(%rbp), %rsi
	movq	%rcx, %rdi
	leaq	0(,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	jmp	.L999
.L1055:
	movq	%r12, %rdi
	movq	%r14, -9432(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-9448(%rbp), %rax
	movq	1864(%rax), %rdx
	movl	$8, %eax
	jmp	.L1062
.L1200:
	leaq	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
.L1060:
	call	_ZN2v87Context5EnterEv@PLT
	movq	-9448(%rbp), %rax
	movq	1864(%rax), %rdx
	movl	$24, %eax
	jmp	.L1062
.L1059:
	movq	%r12, %rdi
	movq	%r14, -9432(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	-9448(%rbp), %rax
	movq	1864(%rax), %rdx
	movl	$20, %eax
	jmp	.L1062
.L1199:
	call	__stack_chk_fail@PLT
.L1206:
	movq	-9480(%rbp), %rdi
	jmp	.L1040
	.cfi_endproc
.LFE7639:
	.size	_ZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.align 2
	.p2align 4
	.globl	_ZN4node11performance12ELDHistogramC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.type	_ZN4node11performance12ELDHistogramC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi, @function
_ZN4node11performance12ELDHistogramC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi:
.LFB7666:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3, %r8d
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	leaq	128(%rdi), %r14
	pushq	%r13
	.cfi_offset 13, -32
	movl	%ecx, %r13d
	movq	%r14, %rcx
	pushq	%r12
	.cfi_offset 12, -40
	movq	%rdi, %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rsi, %rbx
	call	_ZN4node10HandleWrapC2EPNS_11EnvironmentEN2v85LocalINS3_6ObjectEEEP11uv_handle_sNS_9AsyncWrap12ProviderTypeE@PLT
	leaq	88(%r12), %rdi
	movl	$3, %ecx
	movabsq	$3600000000000, %rdx
	movl	$1, %esi
	call	_ZN4node9HistogramC2Elli@PLT
	leaq	16+_ZTVN4node11performance12ELDHistogramE(%rip), %rax
	movb	$0, 104(%r12)
	movq	%rax, (%r12)
	addq	$112, %rax
	movq	%rax, 88(%r12)
	movq	24(%r12), %rax
	movl	%r13d, 108(%r12)
	movq	$0, 112(%r12)
	movq	$0, 120(%r12)
	testq	%rax, %rax
	je	.L1211
	movb	$1, 8(%rax)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L1210
.L1211:
	movq	8(%r12), %rdi
	xorl	%ecx, %ecx
	leaq	_ZZN4node10BaseObject8MakeWeakEvENUlRKN2v816WeakCallbackInfoIS0_EEE_4_FUNES5_(%rip), %rdx
	movq	%r12, %rsi
	call	_ZN2v82V88MakeWeakEPmPvPFvRKNS_16WeakCallbackInfoIvEEENS_16WeakCallbackTypeE@PLT
.L1210:
	movq	360(%rbx), %rax
	movq	%r14, %rsi
	popq	%rbx
	popq	%r12
	popq	%r13
	movq	2360(%rax), %rdi
	popq	%r14
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	uv_timer_init@PLT
	.cfi_endproc
.LFE7666:
	.size	_ZN4node11performance12ELDHistogramC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi, .-_ZN4node11performance12ELDHistogramC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.globl	_ZN4node11performance12ELDHistogramC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.set	_ZN4node11performance12ELDHistogramC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi,_ZN4node11performance12ELDHistogramC2EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.p2align 4
	.type	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, @function
_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEE:
.LFB7656:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	.cfi_offset 3, -48
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1222
	cmpw	$1040, %cx
	jne	.L1214
.L1222:
	movq	23(%rdx), %r14
.L1216:
	movq	40(%rdi), %rax
	movq	%rax, %rdx
	andl	$3, %edx
	cmpq	$1, %rdx
	jne	.L1217
	movq	-1(%rax), %rdx
	cmpw	$67, 11(%rdx)
	je	.L1224
.L1217:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jle	.L1225
	movq	8(%rbx), %rdi
.L1219:
	movq	3280(%r14), %rsi
	call	_ZNK2v85Value12IntegerValueENS_5LocalINS_7ContextEEE@PLT
	movq	%rdx, %r12
	testb	%al, %al
	je	.L1226
.L1220:
	testl	%r12d, %r12d
	jle	.L1227
	movq	8(%rbx), %r13
	movl	$280, %edi
	call	_Znwm@PLT
	popq	%rbx
	movl	%r12d, %ecx
	movq	%r14, %rsi
	addq	$8, %r13
	popq	%r12
	movq	%rax, %rdi
	movq	%r13, %rdx
	popq	%r13
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	jmp	_ZN4node11performance12ELDHistogramC1EPNS_11EnvironmentEN2v85LocalINS4_6ObjectEEEi
	.p2align 4,,10
	.p2align 3
.L1225:
	.cfi_restore_state
	movq	8(%rdi), %rdi
	addq	$88, %rdi
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1224:
	cmpl	$5, 43(%rax)
	jne	.L1217
	leaq	_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1214:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	(%rbx), %rdi
	movq	%rax, %r14
	jmp	.L1216
	.p2align 4,,10
	.p2align 3
.L1227:
	leaq	_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0(%rip), %rdi
	call	_ZN4node6AssertERKNS_13AssertionInfoE@PLT
	.p2align 4,,10
	.p2align 3
.L1226:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1220
	.cfi_endproc
.LFE7656:
	.size	_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEE, .-_ZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEE
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"node,node.perf,node.perf.event_loop"
	.section	.rodata.str1.1
.LC81:
	.string	"value"
.LC82:
	.string	"delay"
	.section	.rodata.str1.8
	.align 8
.LC83:
	.string	"Event loop delay exceeded 1 hour: %ld nanoseconds"
	.text
	.align 2
	.p2align 4
	.globl	_ZN4node11performance12ELDHistogram11RecordDeltaEv
	.type	_ZN4node11performance12ELDHistogram11RecordDeltaEv, @function
_ZN4node11performance12ELDHistogram11RecordDeltaEv:
.LFB7670:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	call	uv_hrtime@PLT
	movq	%rax, %r12
	movq	120(%rbx), %rax
	testq	%rax, %rax
	je	.L1231
	movq	%r12, %r15
	subq	%rax, %r15
	testq	%r15, %r15
	jg	.L1257
.L1231:
	movl	$1, %r13d
.L1230:
	movq	%r12, 120(%rbx)
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1258
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	.cfi_restore_state
	movq	96(%rbx), %rdi
	movq	%r15, %rsi
	call	hdr_record_value@PLT
	movl	%eax, %r13d
	movq	_ZZN4node11performance12ELDHistogram11RecordDeltaEvE28trace_event_unique_atomic517(%rip), %r14
	testq	%r14, %r14
	je	.L1259
.L1233:
	testb	$5, (%r14)
	jne	.L1260
.L1235:
	testb	%r13b, %r13b
	jne	.L1231
	movq	112(%rbx), %rax
	movl	$4294967294, %edx
	cmpq	%rdx, %rax
	jg	.L1239
	addq	$1, %rax
	movq	%rax, 112(%rbx)
.L1239:
	movq	16(%rbx), %rdi
	movq	%r15, %rdx
	leaq	.LC83(%rip), %rsi
	xorl	%eax, %eax
	call	_ZN4node18ProcessEmitWarningEPNS_11EnvironmentEPKcz@PLT
	jmp	.L1230
	.p2align 4,,10
	.p2align 3
.L1260:
	leaq	.LC81(%rip), %rax
	pxor	%xmm0, %xmm0
	movb	$3, -97(%rbp)
	movq	%rax, -96(%rbp)
	movslq	%r15d, %rax
	movq	%rax, -88(%rbp)
	movaps	%xmm0, -80(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1236
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1261
.L1236:
	movq	-72(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1237
	movq	(%rdi), %rax
	call	*8(%rax)
.L1237:
	movq	-80(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1235
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1235
	.p2align 4,,10
	.p2align 3
.L1259:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1234
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1262
.L1234:
	movq	%r14, _ZZN4node11performance12ELDHistogram11RecordDeltaEvE28trace_event_unique_atomic517(%rip)
	mfence
	jmp	.L1233
	.p2align 4,,10
	.p2align 3
.L1262:
	leaq	.LC80(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L1234
	.p2align 4,,10
	.p2align 3
.L1261:
	subq	$8, %rsp
	leaq	-80(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC82(%rip), %rcx
	movl	$67, %esi
	pushq	%rdx
	leaq	-88(%rbp), %rdx
	pushq	%rdx
	leaq	-97(%rbp), %rdx
	pushq	%rdx
	leaq	-96(%rbp), %rdx
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1236
.L1258:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7670:
	.size	_ZN4node11performance12ELDHistogram11RecordDeltaEv, .-_ZN4node11performance12ELDHistogram11RecordDeltaEv
	.align 2
	.p2align 4
	.globl	_ZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_s
	.type	_ZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_s, @function
_ZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_s:
.LFB7668:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	leaq	-128(%rdi), %rbx
	movq	%rbx, %rdi
	subq	$64, %rsp
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	call	_ZN4node11performance12ELDHistogram11RecordDeltaEv
	movq	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic500(%rip), %r12
	testq	%r12, %r12
	je	.L1354
	testb	$5, (%r12)
	jne	.L1355
.L1267:
	movq	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic502(%rip), %r12
	testq	%r12, %r12
	je	.L1356
.L1272:
	testb	$5, (%r12)
	jne	.L1357
.L1274:
	movq	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic504(%rip), %r12
	testq	%r12, %r12
	je	.L1358
.L1279:
	testb	$5, (%r12)
	jne	.L1359
.L1281:
	movq	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic506(%rip), %r12
	testq	%r12, %r12
	je	.L1360
.L1286:
	testb	$5, (%r12)
	jne	.L1361
.L1263:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1362
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1354:
	.cfi_restore_state
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1266
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1363
.L1266:
	movq	%r12, _ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic500(%rip)
	mfence
	testb	$5, (%r12)
	je	.L1267
.L1355:
	movq	96(%rbx), %rdi
	call	hdr_min@PLT
	leaq	.LC81(%rip), %rcx
	pxor	%xmm0, %xmm0
	movb	$3, -65(%rbp)
	cltq
	movq	%rcx, -64(%rbp)
	movq	%rax, -56(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1268
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1364
.L1268:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1269
	movq	(%rdi), %rax
	call	*8(%rax)
.L1269:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1267
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic502(%rip), %r12
	testq	%r12, %r12
	jne	.L1272
.L1356:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1273
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1365
.L1273:
	movq	%r12, _ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic502(%rip)
	mfence
	testb	$5, (%r12)
	je	.L1274
.L1357:
	movq	96(%rbx), %rdi
	call	hdr_max@PLT
	leaq	.LC81(%rip), %rsi
	pxor	%xmm0, %xmm0
	movb	$3, -65(%rbp)
	cltq
	movq	%rsi, -64(%rbp)
	movq	%rax, -56(%rbp)
	movaps	%xmm0, -48(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1275
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1366
.L1275:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1276
	movq	(%rdi), %rax
	call	*8(%rax)
.L1276:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1274
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic504(%rip), %r12
	testq	%r12, %r12
	jne	.L1279
.L1358:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1280
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1367
.L1280:
	movq	%r12, _ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic504(%rip)
	mfence
	testb	$5, (%r12)
	je	.L1281
.L1359:
	movq	96(%rbx), %rdi
	call	hdr_mean@PLT
	leaq	.LC81(%rip), %rax
	movb	$3, -65(%rbp)
	movq	%rax, -64(%rbp)
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	cltq
	movq	%rax, -56(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1282
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1368
.L1282:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1283
	movq	(%rdi), %rax
	call	*8(%rax)
.L1283:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1281
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic506(%rip), %r12
	testq	%r12, %r12
	jne	.L1286
.L1360:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r12
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1287
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r12
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1369
.L1287:
	movq	%r12, _ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic506(%rip)
	mfence
	testb	$5, (%r12)
	je	.L1263
.L1361:
	movq	96(%rbx), %rdi
	call	hdr_stddev@PLT
	leaq	.LC81(%rip), %rax
	movb	$3, -65(%rbp)
	movq	%rax, -64(%rbp)
	cvttsd2sil	%xmm0, %eax
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -48(%rbp)
	cltq
	movq	%rax, -56(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1289
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController13AddTraceEventEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEj(%rip), %rdx
	movq	24(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1370
.L1289:
	movq	-40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1290
	movq	(%rdi), %rax
	call	*8(%rax)
.L1290:
	movq	-48(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1263
	movq	(%rdi), %rax
	call	*8(%rax)
	jmp	.L1263
	.p2align 4,,10
	.p2align 3
.L1363:
	leaq	.LC80(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1266
	.p2align 4,,10
	.p2align 3
.L1365:
	leaq	.LC80(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1273
	.p2align 4,,10
	.p2align 3
.L1367:
	leaq	.LC80(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1280
	.p2align 4,,10
	.p2align 3
.L1369:
	leaq	.LC80(%rip), %rsi
	call	*%rax
	movq	%rax, %r12
	jmp	.L1287
	.p2align 4,,10
	.p2align 3
.L1370:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC64(%rip), %rcx
	movl	$67, %esi
	pushq	%rdx
	leaq	-56(%rbp), %rdx
	pushq	%rdx
	leaq	-65(%rbp), %rdx
	pushq	%rdx
	leaq	-64(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1289
	.p2align 4,,10
	.p2align 3
.L1364:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC61(%rip), %rcx
	movl	$67, %esi
	pushq	%rdx
	leaq	-56(%rbp), %rdx
	pushq	%rdx
	leaq	-65(%rbp), %rdx
	pushq	%rdx
	leaq	-64(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1366:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC62(%rip), %rcx
	movl	$67, %esi
	pushq	%rdx
	leaq	-56(%rbp), %rdx
	pushq	%rdx
	leaq	-65(%rbp), %rdx
	pushq	%rdx
	leaq	-64(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1275
	.p2align 4,,10
	.p2align 3
.L1368:
	subq	$8, %rsp
	leaq	-48(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	leaq	.LC63(%rip), %rcx
	movl	$67, %esi
	pushq	%rdx
	leaq	-56(%rbp), %rdx
	pushq	%rdx
	leaq	-65(%rbp), %rdx
	pushq	%rdx
	leaq	-64(%rbp), %rdx
	pushq	%rdx
	movq	%r12, %rdx
	pushq	$1
	pushq	$0
	call	*%rax
	addq	$64, %rsp
	jmp	.L1282
.L1362:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE7668:
	.size	_ZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_s, .-_ZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_s
	.align 2
	.p2align 4
	.globl	_ZN4node11performance12ELDHistogram6EnableEv
	.type	_ZN4node11performance12ELDHistogram6EnableEv, @function
_ZN4node11performance12ELDHistogram6EnableEv:
.LFB7671:
	.cfi_startproc
	endbr64
	movzbl	104(%rdi), %eax
	testb	%al, %al
	jne	.L1373
	movl	72(%rdi), %ecx
	leal	-1(%rcx), %edx
	cmpl	$1, %edx
	jbe	.L1375
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	leaq	_ZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_s(%rip), %rsi
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	.cfi_offset 12, -24
	leaq	128(%rdi), %r12
	subq	$8, %rsp
	movb	$1, 104(%rdi)
	movslq	108(%rdi), %rdx
	movq	$0, 120(%rdi)
	movq	%r12, %rdi
	movq	%rdx, %rcx
	call	uv_timer_start@PLT
	movq	%r12, %rdi
	call	uv_unref@PLT
	addq	$8, %rsp
	movl	$1, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1375:
	.cfi_restore 6
	.cfi_restore 12
	ret
	.p2align 4,,10
	.p2align 3
.L1373:
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE7671:
	.size	_ZN4node11performance12ELDHistogram6EnableEv, .-_ZN4node11performance12ELDHistogram6EnableEv
	.align 2
	.p2align 4
	.globl	_ZN4node11performance12ELDHistogram7DisableEv
	.type	_ZN4node11performance12ELDHistogram7DisableEv, @function
_ZN4node11performance12ELDHistogram7DisableEv:
.LFB7672:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	subq	$8, %rsp
	.cfi_offset 12, -24
	movzbl	104(%rdi), %r12d
	testb	%r12b, %r12b
	je	.L1378
	movl	72(%rdi), %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L1380
	movb	$0, 104(%rdi)
	subq	$-128, %rdi
	call	uv_timer_stop@PLT
.L1378:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1380:
	.cfi_restore_state
	xorl	%r12d, %r12d
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%r12
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7672:
	.size	_ZN4node11performance12ELDHistogram7DisableEv, .-_ZN4node11performance12ELDHistogram7DisableEv
	.p2align 4
	.globl	_Z21_register_performancev
	.type	_Z21_register_performancev, @function
_Z21_register_performancev:
.LFB7674:
	.cfi_startproc
	endbr64
	leaq	_ZL7_module(%rip), %rdi
	jmp	node_module_register@PLT
	.cfi_endproc
.LFE7674:
	.size	_Z21_register_performancev, .-_Z21_register_performancev
	.section	.text._ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,"axG",@progbits,_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.type	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, @function
_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_:
.LFB8873:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rdi, %r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 3, -56
	movq	8(%rsi), %rsi
	movq	0(%r13), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%r12), %rcx
	xorl	%edx, %edx
	movq	%rax, %r15
	divq	%rcx
	movq	(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %r14
	testq	%rax, %rax
	je	.L1387
	movq	(%rax), %rbx
	movq	%rdx, %r8
	movq	48(%rbx), %rsi
.L1390:
	cmpq	%rsi, %r15
	je	.L1431
.L1388:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1387
	movq	48(%rbx), %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rcx
	cmpq	%rdx, %r8
	je	.L1390
.L1387:
	movl	$56, %edi
	call	_Znwm@PLT
	movq	0(%r13), %rdx
	movq	$0, (%rax)
	movq	%rax, %rbx
	leaq	24(%rax), %rax
	movq	%rax, 8(%rbx)
	leaq	16(%r13), %rax
	cmpq	%rax, %rdx
	je	.L1432
	movq	%rdx, 8(%rbx)
	movq	16(%r13), %rdx
	movq	%rdx, 24(%rbx)
.L1392:
	movq	8(%r13), %rdx
	movb	$0, 16(%r13)
	leaq	32(%r12), %rdi
	movl	$1, %ecx
	movq	$0, 8(%r13)
	movq	8(%r12), %rsi
	movq	%rdx, 16(%rbx)
	movq	24(%r12), %rdx
	movq	%rax, 0(%r13)
	movq	$0, 40(%rbx)
	call	_ZNKSt8__detail20_Prime_rehash_policy14_M_need_rehashEmmm@PLT
	movq	%rdx, %r13
	testb	%al, %al
	jne	.L1393
	movq	(%r12), %r8
	movq	%r15, 48(%rbx)
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L1403
.L1435:
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	(%r14), %rax
	movq	%rbx, (%rax)
.L1404:
	addq	$1, 24(%r12)
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1431:
	.cfi_restore_state
	movq	8(%r13), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L1388
	movq	%r8, -64(%rbp)
	movq	%rcx, -56(%rbp)
	testq	%rdx, %rdx
	je	.L1389
	movq	8(%rbx), %rsi
	movq	0(%r13), %rdi
	call	memcmp@PLT
	movq	-56(%rbp), %rcx
	movq	-64(%rbp), %r8
	testl	%eax, %eax
	jne	.L1388
.L1389:
	addq	$24, %rsp
	leaq	40(%rbx), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1393:
	.cfi_restore_state
	cmpq	$1, %rdx
	je	.L1433
	movabsq	$1152921504606846975, %rax
	cmpq	%rax, %rdx
	ja	.L1434
	leaq	0(,%rdx,8), %r14
	movq	%r14, %rdi
	call	_Znwm@PLT
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	memset@PLT
	leaq	48(%r12), %r10
	movq	%rax, %r8
.L1396:
	movq	16(%r12), %rsi
	movq	$0, 16(%r12)
	testq	%rsi, %rsi
	je	.L1398
	xorl	%edi, %edi
	leaq	16(%r12), %r9
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1400:
	movq	(%r11), %rdx
	movq	%rdx, (%rcx)
	movq	(%rax), %rax
	movq	%rcx, (%rax)
.L1401:
	testq	%rsi, %rsi
	je	.L1398
.L1399:
	movq	%rsi, %rcx
	xorl	%edx, %edx
	movq	(%rsi), %rsi
	movq	48(%rcx), %rax
	divq	%r13
	leaq	(%r8,%rdx,8), %rax
	movq	(%rax), %r11
	testq	%r11, %r11
	jne	.L1400
	movq	16(%r12), %r11
	movq	%r11, (%rcx)
	movq	%rcx, 16(%r12)
	movq	%r9, (%rax)
	cmpq	$0, (%rcx)
	je	.L1407
	movq	%rcx, (%r8,%rdi,8)
	movq	%rdx, %rdi
	testq	%rsi, %rsi
	jne	.L1399
	.p2align 4,,10
	.p2align 3
.L1398:
	movq	(%r12), %rdi
	cmpq	%rdi, %r10
	je	.L1402
	movq	%r8, -56(%rbp)
	call	_ZdlPv@PLT
	movq	-56(%rbp), %r8
.L1402:
	movq	%r15, %rax
	xorl	%edx, %edx
	movq	%r13, 8(%r12)
	divq	%r13
	movq	%r8, (%r12)
	movq	%r15, 48(%rbx)
	leaq	0(,%rdx,8), %r14
	addq	%r8, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L1435
.L1403:
	movq	16(%r12), %rax
	movq	%rbx, 16(%r12)
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.L1405
	movq	48(%rax), %rax
	xorl	%edx, %edx
	divq	8(%r12)
	movq	%rbx, (%r8,%rdx,8)
.L1405:
	leaq	16(%r12), %rax
	movq	%rax, (%r14)
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1432:
	movdqu	16(%r13), %xmm0
	movups	%xmm0, 24(%rbx)
	jmp	.L1392
	.p2align 4,,10
	.p2align 3
.L1407:
	movq	%rdx, %rdi
	jmp	.L1401
	.p2align 4,,10
	.p2align 3
.L1433:
	movq	$0, 48(%r12)
	leaq	48(%r12), %r8
	movq	%r8, %r10
	jmp	.L1396
.L1434:
	call	_ZSt17__throw_bad_allocv@PLT
	.cfi_endproc
.LFE8873:
	.size	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_, .-_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"node,node.perf,node.perf.usertiming"
	.text
	.p2align 4
	.globl	_ZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7619:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$1288, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	(%rdi), %rdi
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	32(%rdi), %rdx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1501
	cmpw	$1040, %cx
	jne	.L1437
.L1501:
	movq	23(%rdx), %r12
.L1439:
	movq	352(%r12), %rsi
	leaq	-1264(%rbp), %r15
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movl	16(%r13), %edx
	testl	%edx, %edx
	jg	.L1440
	movq	0(%r13), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L1441:
	movq	352(%r12), %rsi
	leaq	-1104(%rbp), %rdi
	leaq	-1216(%rbp), %rbx
	leaq	-1232(%rbp), %r14
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	call	uv_hrtime@PLT
	movq	-1088(%rbp), %r8
	movq	%rbx, -1232(%rbp)
	movq	%rax, -1288(%rbp)
	testq	%r8, %r8
	jne	.L1532
.L1492:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1532:
	movq	%r8, %rdi
	movq	%r8, -1296(%rbp)
	call	strlen@PLT
	movq	-1296(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -1272(%rbp)
	movq	%rax, %r9
	ja	.L1533
	cmpq	$1, %rax
	jne	.L1443
	movzbl	(%r8), %edx
	movb	%dl, -1216(%rbp)
	movq	%rbx, %rdx
.L1444:
	movq	%rax, -1224(%rbp)
	leaq	1872(%r12), %rdi
	movq	%r14, %rsi
	movb	$0, (%rdx,%rax)
	call	_ZNSt8__detail9_Map_baseINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS6_mESaIS9_ENS_10_Select1stESt8equal_toIS6_ESt4hashIS6_ENS_18_Mod_range_hashingENS_20_Default_ranged_hashENS_20_Prime_rehash_policyENS_17_Hashtable_traitsILb1ELb0ELb1EEELb1EEixEOS6_
	movq	-1288(%rbp), %rcx
	movq	-1232(%rbp), %rdi
	movq	%rcx, (%rax)
	cmpq	%rbx, %rdi
	je	.L1445
	call	_ZdlPv@PLT
.L1445:
	movq	_ZZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic136(%rip), %rbx
	testq	%rbx, %rbx
	je	.L1534
.L1447:
	movq	-1088(%rbp), %r14
	testb	$5, (%rbx)
	jne	.L1535
.L1449:
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%r12, -1192(%rbp)
	movq	%rax, -1200(%rbp)
	leaq	-1168(%rbp), %rax
	movq	%rax, -1296(%rbp)
	movq	%rax, -1184(%rbp)
	testq	%r14, %r14
	je	.L1492
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%rax, -1272(%rbp)
	movq	%rax, %rbx
	cmpq	$15, %rax
	ja	.L1536
	cmpq	$1, %rax
	jne	.L1454
	movzbl	(%r14), %edx
	movb	%dl, -1168(%rbp)
	movq	-1296(%rbp), %rdx
.L1455:
	movq	%rax, -1176(%rbp)
	xorl	%ecx, %ecx
	movq	-1288(%rbp), %xmm0
	movb	$0, (%rdx,%rax)
	leaq	-1136(%rbp), %rax
	xorl	%edx, %edx
	movq	%rax, -1304(%rbp)
	punpcklqdq	%xmm0, %xmm0
	movq	%rax, -1152(%rbp)
	movq	-1192(%rbp), %rax
	movl	$1802658157, -1136(%rbp)
	movb	$0, -1132(%rbp)
	movq	2952(%rax), %rdi
	movq	3280(%rax), %rsi
	movaps	%xmm0, -1120(%rbp)
	movq	$4, -1144(%rbp)
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1537
	movq	-1192(%rbp), %r14
	movq	-1184(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%r14), %rdi
	movq	3280(%r14), %rax
	movq	%rdi, -1312(%rbp)
	movq	%rax, -1288(%rbp)
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1538
.L1488:
	movq	360(%r14), %rax
	movq	-1288(%rbp), %rsi
	movl	$5, %r8d
	movq	%rbx, %rdi
	movq	1056(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1539
.L1458:
	movq	-1152(%rbp), %rsi
	movq	-1312(%rbp), %rdi
	movl	$-1, %ecx
	xorl	%edx, %edx
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1540
.L1459:
	movq	360(%r14), %rax
	movq	-1288(%rbp), %rsi
	movl	$5, %r8d
	movq	%rbx, %rdi
	movq	616(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1541
.L1460:
	movq	-1120(%rbp), %rax
	subq	_ZN4node11performance10timeOriginE(%rip), %rax
	js	.L1461
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1462:
	movq	-1312(%rbp), %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-1288(%rbp), %rsi
	movl	$5, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	360(%r14), %rax
	movq	1664(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1542
.L1463:
	movq	-1112(%rbp), %rax
	subq	-1120(%rbp), %rax
	js	.L1464
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1465:
	movq	-1312(%rbp), %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movq	-1288(%rbp), %rsi
	movl	$5, %r8d
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	360(%r14), %rax
	movq	568(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1543
.L1466:
	movq	-1152(%rbp), %rdx
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	3280(%r12), %r14
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1544
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1473
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1474
	cmpb	$103, (%rdx)
	jne	.L1502
	cmpb	$99, 1(%rdx)
	jne	.L1502
	cmpb	$0, 2(%rdx)
	jne	.L1502
	movq	%r14, %rdi
	movq	%rbx, -1272(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r12), %rdx
	movl	$12, %eax
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1443:
	testq	%r9, %r9
	jne	.L1545
	movq	%rbx, %rdx
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1502:
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1477
	movl	$6, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1478
	leaq	.LC10(%rip), %rdi
	movl	$5, %ecx
	movq	%rdx, %rsi
	repz cmpsb
	movq	%rbx, -1272(%rbp)
	movq	%r14, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1479
	call	_ZN2v87Context5EnterEv@PLT
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1440:
	movq	8(%r13), %rdx
	jmp	.L1441
	.p2align 4,,10
	.p2align 3
.L1537:
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	-1152(%rbp), %rdi
	movq	%rax, -1200(%rbp)
	cmpq	-1304(%rbp), %rdi
	je	.L1486
	call	_ZdlPv@PLT
.L1486:
	movq	-1184(%rbp), %rdi
	cmpq	-1296(%rbp), %rdi
	je	.L1469
	call	_ZdlPv@PLT
.L1469:
	movq	-1088(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1472
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	jne	.L1531
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1454:
	testq	%rbx, %rbx
	jne	.L1546
	movq	-1296(%rbp), %rdx
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1535:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1200(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1450
	movq	(%rax), %rax
	movq	32(%rax), %r10
	leaq	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1547
.L1450:
	movq	-1192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1451
	movq	(%rdi), %rax
	call	*8(%rax)
.L1451:
	movq	-1200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1452
	movq	(%rdi), %rax
	call	*8(%rax)
.L1452:
	movq	-1088(%rbp), %r14
	jmp	.L1449
	.p2align 4,,10
	.p2align 3
.L1534:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %rbx
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1448
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %rbx
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1548
.L1448:
	movq	%rbx, _ZZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic136(%rip)
	mfence
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1533:
	movq	%r14, %rdi
	leaq	-1272(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -1304(%rbp)
	movq	%r8, -1296(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-1296(%rbp), %r8
	movq	-1304(%rbp), %r9
	movq	%rax, -1232(%rbp)
	movq	%rax, %rdi
	movq	-1272(%rbp), %rax
	movq	%rax, -1216(%rbp)
.L1442:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-1272(%rbp), %rax
	movq	-1232(%rbp), %rdx
	jmp	.L1444
	.p2align 4,,10
	.p2align 3
.L1536:
	leaq	-1184(%rbp), %rdi
	leaq	-1272(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -1184(%rbp)
	movq	%rax, %rdi
	movq	-1272(%rbp), %rax
	movq	%rax, -1168(%rbp)
.L1453:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	movq	-1272(%rbp), %rax
	movq	-1184(%rbp), %rdx
	jmp	.L1455
	.p2align 4,,10
	.p2align 3
.L1544:
	movq	%r14, %rdi
	movq	%rbx, -1272(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r12), %rdx
	xorl	%eax, %eax
.L1481:
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	jne	.L1549
.L1480:
	movq	%r14, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	(%rbx), %rdx
	movq	0(%r13), %rax
	movq	-1152(%rbp), %rdi
	movq	%rdx, 24(%rax)
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%rax, -1200(%rbp)
	cmpq	-1304(%rbp), %rdi
	je	.L1482
	call	_ZdlPv@PLT
.L1482:
	movq	-1184(%rbp), %rdi
	cmpq	-1296(%rbp), %rdi
	je	.L1483
	call	_ZdlPv@PLT
.L1483:
	movq	-1088(%rbp), %rdi
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1472
	testq	%rdi, %rdi
	je	.L1472
.L1531:
	call	free@PLT
.L1472:
	movq	%r15, %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1550
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1461:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1462
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1465
	.p2align 4,,10
	.p2align 3
.L1437:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r12
	jmp	.L1439
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	%r14, %rdi
	movq	%rbx, -1272(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r12), %rdx
	movl	$4, %eax
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	je	.L1480
.L1549:
	xorl	%eax, %eax
	movq	2944(%r12), %rdx
	movq	352(%r12), %rdi
	leaq	-1272(%rbp), %r8
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movl	$1, %ecx
	movq	-1272(%rbp), %rsi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	jmp	.L1480
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	%r14, %rdi
	movq	%rbx, -1272(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r12), %rdx
	movl	$8, %eax
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1548:
	leaq	.LC84(%rip), %rsi
	call	*%rax
	movq	%rax, %rbx
	jmp	.L1448
	.p2align 4,,10
	.p2align 3
.L1547:
	movq	-1288(%rbp), %rdx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$82, %esi
	movabsq	$2361183241434822607, %rcx
	shrq	$3, %rdx
	movq	%rdx, %rax
	mulq	%rcx
	leaq	-1200(%rbp), %rax
	movq	%r14, %rcx
	shrq	$4, %rdx
	pushq	%rdx
	movq	%rbx, %rdx
	pushq	$1
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1450
	.p2align 4,,10
	.p2align 3
.L1543:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1466
	.p2align 4,,10
	.p2align 3
.L1542:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1463
	.p2align 4,,10
	.p2align 3
.L1541:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1540:
	movq	%rax, -1320(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1320(%rbp), %rcx
	jmp	.L1459
	.p2align 4,,10
	.p2align 3
.L1539:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1458
	.p2align 4,,10
	.p2align 3
.L1538:
	movq	%rcx, -1320(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-1320(%rbp), %rcx
	jmp	.L1488
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	%r14, %rdi
	movq	%rbx, -1272(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r12), %rdx
	movl	$16, %eax
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1479:
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r12), %rdx
	movl	$24, %eax
	jmp	.L1481
	.p2align 4,,10
	.p2align 3
.L1478:
	movq	%r14, %rdi
	movq	%rbx, -1272(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r12), %rdx
	movl	$20, %eax
	jmp	.L1481
.L1550:
	call	__stack_chk_fail@PLT
.L1545:
	movq	%rbx, %rdi
	jmp	.L1442
.L1546:
	movq	-1296(%rbp), %rdi
	jmp	.L1453
	.cfi_endproc
.LFE7619:
	.size	_ZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text._ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,"axG",@progbits,_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_,comdat
	.align 2
	.p2align 4
	.weak	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.type	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, @function
_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_:
.LFB8882:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movl	$3339675911, %edx
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	movq	(%r12), %rdi
	call	_ZSt11_Hash_bytesPKvmm@PLT
	movq	8(%rbx), %r14
	xorl	%edx, %edx
	movq	%rax, %r13
	divq	%r14
	movq	(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L1552
	movq	(%rax), %rbx
	movq	%rdx, %r15
	movq	48(%rbx), %rcx
.L1555:
	cmpq	%rcx, %r13
	je	.L1572
.L1553:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L1552
	movq	48(%rbx), %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r14
	cmpq	%rdx, %r15
	je	.L1555
.L1552:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1572:
	.cfi_restore_state
	movq	8(%r12), %rdx
	cmpq	16(%rbx), %rdx
	jne	.L1553
	testq	%rdx, %rdx
	je	.L1554
	movq	8(%rbx), %rsi
	movq	(%r12), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L1553
.L1554:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8882:
	.size	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_, .-_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	.text
	.p2align 4
	.globl	_ZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.type	_ZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEE, @function
_ZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEE:
.LFB7622:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$3432, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	%rdi, -3432(%rbp)
	movq	(%rdi), %rdi
	movq	32(%rdi), %rdx
	movq	%fs:40, %rbx
	movq	%rbx, -56(%rbp)
	xorl	%ebx, %ebx
	movq	-1(%rdx), %rax
	movzwl	11(%rax), %eax
	movl	%eax, %ecx
	subl	$1056, %eax
	cmpl	$1, %eax
	jbe	.L1690
	cmpw	$1040, %cx
	jne	.L1574
.L1690:
	movq	23(%rdx), %r15
.L1576:
	leaq	-3408(%rbp), %rax
	movq	352(%r15), %rsi
	movq	%rax, %rdi
	movq	%rax, -3456(%rbp)
	call	_ZN2v811HandleScopeC1EPNS_7IsolateE@PLT
	movq	-3432(%rbp), %rax
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L1577
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L1578:
	movq	352(%r15), %rsi
	leaq	-3216(%rbp), %rdi
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-3432(%rbp), %rax
	cmpl	$1, 16(%rax)
	jle	.L1757
	movq	8(%rax), %rax
	movq	%rax, -3440(%rbp)
	leaq	-8(%rax), %rdx
.L1580:
	movq	352(%r15), %rsi
	leaq	-2160(%rbp), %rdi
	leaq	-3360(%rbp), %r12
	leaq	-3376(%rbp), %r14
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	1864(%r15), %rax
	movq	-2144(%rbp), %r13
	movq	%r12, -3376(%rbp)
	movq	%rax, -3448(%rbp)
	movq	_ZN4node11performance10timeOriginE(%rip), %rax
	movq	%rax, -3440(%rbp)
	testq	%r13, %r13
	jne	.L1758
.L1660:
	leaq	.LC4(%rip), %rdi
	call	_ZSt19__throw_logic_errorPKc@PLT
	.p2align 4,,10
	.p2align 3
.L1757:
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1758:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%rax, -3416(%rbp)
	movq	%rax, %rbx
	cmpq	$15, %rax
	ja	.L1759
	cmpq	$1, %rax
	jne	.L1582
	movzbl	0(%r13), %edx
	movb	%dl, -3360(%rbp)
	movq	%r12, %rdx
.L1583:
	movq	%rax, -3368(%rbp)
	leaq	1872(%r15), %r13
	movq	%r14, %rsi
	movb	$0, (%rdx,%rax)
	movq	%r13, %rdi
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L1584
	movq	-3376(%rbp), %rdi
	movq	40(%rax), %rbx
	cmpq	%r12, %rdi
	je	.L1585
	call	_ZdlPv@PLT
.L1585:
	testq	%rbx, %rbx
	je	.L1665
.L1586:
	movq	-3432(%rbp), %rax
	cmpl	$2, 16(%rax)
	jg	.L1590
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	addq	$88, %rdx
.L1591:
	movq	(%rdx), %rax
	movq	%rax, %rcx
	andl	$3, %ecx
	cmpq	$1, %rcx
	jne	.L1592
	movq	-1(%rax), %rcx
	cmpw	$67, 11(%rcx)
	jne	.L1592
	cmpl	$5, 43(%rax)
	je	.L1760
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	352(%r15), %rsi
	leaq	-1104(%rbp), %rdi
	leaq	-3328(%rbp), %r12
	leaq	-3344(%rbp), %r14
	call	_ZN4node9Utf8ValueC1EPN2v87IsolateENS1_5LocalINS1_5ValueEEE@PLT
	movq	-1088(%rbp), %r8
	movq	%r12, -3344(%rbp)
	testq	%r8, %r8
	je	.L1660
	movq	%r8, %rdi
	movq	%r8, -3440(%rbp)
	call	strlen@PLT
	movq	-3440(%rbp), %r8
	cmpq	$15, %rax
	movq	%rax, -3416(%rbp)
	movq	%rax, %r9
	ja	.L1761
	cmpq	$1, %rax
	jne	.L1595
	movzbl	(%r8), %edx
	movb	%dl, -3328(%rbp)
	movq	%r12, %rdx
.L1596:
	movq	%rax, -3336(%rbp)
	movq	%r14, %rsi
	movq	%r13, %rdi
	movb	$0, (%rdx,%rax)
	call	_ZNSt10_HashtableINSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEESt4pairIKS5_mESaIS8_ENSt8__detail10_Select1stESt8equal_toIS5_ESt4hashIS5_ENSA_18_Mod_range_hashingENSA_20_Default_ranged_hashENSA_20_Prime_rehash_policyENSA_17_Hashtable_traitsILb1ELb0ELb1EEEE4findERS7_
	testq	%rax, %rax
	je	.L1597
	movq	-3344(%rbp), %rdi
	movq	40(%rax), %r13
	cmpq	%r12, %rdi
	je	.L1598
	call	_ZdlPv@PLT
.L1598:
	movq	-1088(%rbp), %r8
	testq	%r13, %r13
	je	.L1599
	cmpq	%r13, %rbx
	cmovnb	%rbx, %r13
.L1600:
	testq	%r8, %r8
	je	.L1593
	leaq	-1080(%rbp), %rax
	cmpq	%rax, %r8
	je	.L1593
	movq	%r8, %rdi
	call	free@PLT
.L1593:
	movq	_ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic201(%rip), %r14
	testq	%r14, %r14
	je	.L1762
.L1606:
	testb	$5, (%r14)
	jne	.L1763
.L1608:
	movq	_ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic204(%rip), %r14
	testq	%r14, %r14
	je	.L1764
.L1613:
	movq	-3200(%rbp), %r12
	testb	$5, (%r14)
	jne	.L1765
.L1615:
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%r15, -3304(%rbp)
	movq	%rax, -3312(%rbp)
	leaq	-3280(%rbp), %rax
	movq	%rax, -3440(%rbp)
	movq	%rax, -3296(%rbp)
	testq	%r12, %r12
	je	.L1660
	movq	%r12, %rdi
	call	strlen@PLT
	movq	%rax, -3416(%rbp)
	movq	%rax, %r14
	cmpq	$15, %rax
	ja	.L1766
	cmpq	$1, %rax
	jne	.L1620
	movzbl	(%r12), %edx
	movb	%dl, -3280(%rbp)
	movq	-3440(%rbp), %rdx
.L1621:
	movq	%rax, -3288(%rbp)
	movq	%rbx, %xmm0
	movq	%r13, %xmm2
	xorl	%ecx, %ecx
	movb	$0, (%rdx,%rax)
	leaq	-3248(%rbp), %rax
	movl	$29301, %edx
	punpcklqdq	%xmm2, %xmm0
	movq	%rax, -3448(%rbp)
	movq	%rax, -3264(%rbp)
	movq	-3304(%rbp), %rax
	movw	%dx, -3244(%rbp)
	xorl	%edx, %edx
	movq	2952(%rax), %rdi
	movq	3280(%rax), %rsi
	movl	$1935762797, -3248(%rbp)
	movb	$101, -3242(%rbp)
	movq	$7, -3256(%rbp)
	movb	$0, -3241(%rbp)
	movaps	%xmm0, -3232(%rbp)
	call	_ZNK2v88Function11NewInstanceENS_5LocalINS_7ContextEEEiPNS1_INS_5ValueEEE@PLT
	movq	%rax, %rbx
	testq	%rax, %rax
	je	.L1767
	movq	-3304(%rbp), %r12
	movq	-3296(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	352(%r12), %r14
	movq	3280(%r12), %r13
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1768
.L1656:
	movq	360(%r12), %rax
	movl	$5, %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	1056(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1769
.L1624:
	movq	-3264(%rbp), %rsi
	movl	$-1, %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_ZN2v86String11NewFromUtf8EPNS_7IsolateEPKcNS_13NewStringTypeEi@PLT
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L1770
.L1625:
	movq	360(%r12), %rax
	movl	$5, %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	616(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1771
.L1626:
	movq	-3232(%rbp), %rax
	subq	_ZN4node11performance10timeOriginE(%rip), %rax
	js	.L1627
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1628:
	movq	%r14, %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$5, %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	360(%r12), %rax
	movq	1664(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1772
.L1629:
	movq	-3224(%rbp), %rax
	subq	-3232(%rbp), %rax
	js	.L1630
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
.L1631:
	movq	%r14, %rdi
	divsd	.LC11(%rip), %xmm0
	call	_ZN2v86Number3NewEPNS_7IsolateEd@PLT
	movl	$5, %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rcx
	movq	360(%r12), %rax
	movq	568(%rax), %rdx
	call	_ZN2v86Object17DefineOwnPropertyENS_5LocalINS_7ContextEEENS1_INS_4NameEEENS1_INS_5ValueEEENS_17PropertyAttributeE@PLT
	testb	%al, %al
	je	.L1773
.L1632:
	movq	-3264(%rbp), %rdx
	movl	$5, %ecx
	leaq	.LC5(%rip), %rdi
	movq	3280(%r15), %r12
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1774
	movl	$5, %ecx
	leaq	.LC6(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1640
	movl	$8, %ecx
	leaq	.LC7(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1641
	cmpb	$103, (%rdx)
	jne	.L1691
	cmpb	$99, 1(%rdx)
	jne	.L1691
	cmpb	$0, 2(%rdx)
	jne	.L1691
	movq	%r12, %rdi
	movq	%rbx, -3416(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r15), %rdx
	movl	$12, %eax
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1691:
	movl	$9, %ecx
	leaq	.LC8(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1644
	movl	$6, %ecx
	leaq	.LC9(%rip), %rdi
	movq	%rdx, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1645
	leaq	.LC10(%rip), %rdi
	movl	$5, %ecx
	movq	%rdx, %rsi
	repz cmpsb
	movq	%rbx, -3416(%rbp)
	movq	%r12, %rdi
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1646
	call	_ZN2v87Context5EnterEv@PLT
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1577:
	movq	8(%rax), %rdx
	jmp	.L1578
	.p2align 4,,10
	.p2align 3
.L1590:
	movq	8(%rax), %rax
	movq	%rax, -3440(%rbp)
	leaq	-16(%rax), %rdx
	jmp	.L1591
	.p2align 4,,10
	.p2align 3
.L1584:
	movq	-3376(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1665
	call	_ZdlPv@PLT
.L1665:
	movq	-2144(%rbp), %rax
	movl	$12, %ecx
	leaq	.LC72(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1672
	movl	$10, %ecx
	leaq	.LC76(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1673
	movl	$8, %ecx
	leaq	.LC73(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1674
	movl	$10, %ecx
	leaq	.LC74(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1675
	movl	$9, %ecx
	leaq	.LC75(%rip), %rdi
	movq	%rax, %rsi
	repz cmpsb
	seta	%dl
	sbbb	$0, %dl
	testb	%dl, %dl
	je	.L1676
	movq	%rax, %rsi
	movl	$18, %ecx
	leaq	.LC71(%rip), %rdi
	movq	-3440(%rbp), %rbx
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L1586
	movl	$40, %eax
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1767:
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	-3264(%rbp), %rdi
	movq	%rax, -3312(%rbp)
	cmpq	-3448(%rbp), %rdi
	je	.L1654
	call	_ZdlPv@PLT
.L1654:
	movq	-3296(%rbp), %rdi
	cmpq	-3440(%rbp), %rdi
	je	.L1635
	call	_ZdlPv@PLT
.L1635:
	movq	-2144(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1636
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1636
	call	free@PLT
.L1636:
	movq	-3200(%rbp), %rdi
	leaq	-3192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1639
	testq	%rdi, %rdi
	jne	.L1756
	jmp	.L1639
	.p2align 4,,10
	.p2align 3
.L1597:
	movq	-3344(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L1755
	call	_ZdlPv@PLT
.L1755:
	movq	-1088(%rbp), %r8
.L1599:
	movl	$12, %ecx
	leaq	.LC72(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1679
	movl	$10, %ecx
	leaq	.LC76(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1680
	movl	$8, %ecx
	leaq	.LC73(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1681
	movl	$10, %ecx
	leaq	.LC74(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1682
	movl	$9, %ecx
	leaq	.LC75(%rip), %rdi
	movq	%r8, %rsi
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	je	.L1683
	movl	$18, %ecx
	leaq	.LC71(%rip), %rdi
	movq	%r8, %rsi
	movq	%rbx, %r13
	repz cmpsb
	seta	%al
	sbbb	$0, %al
	testb	%al, %al
	jne	.L1600
	movl	$40, %eax
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1582:
	testq	%rbx, %rbx
	jne	.L1775
	movq	%r12, %rdx
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1620:
	testq	%r14, %r14
	jne	.L1776
	movq	-3440(%rbp), %rdx
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1759:
	movq	%r14, %rdi
	leaq	-3416(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -3376(%rbp)
	movq	%rax, %rdi
	movq	-3416(%rbp), %rax
	movq	%rax, -3360(%rbp)
.L1581:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-3416(%rbp), %rax
	movq	-3376(%rbp), %rdx
	jmp	.L1583
	.p2align 4,,10
	.p2align 3
.L1763:
	pxor	%xmm0, %xmm0
	movq	-3200(%rbp), %r12
	movaps	%xmm0, -1104(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1609
	movq	(%rax), %rax
	movq	32(%rax), %r10
	leaq	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1777
.L1609:
	movq	-1096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1610
	movq	(%rdi), %rax
	call	*8(%rax)
.L1610:
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1608
	movq	(%rdi), %rax
	call	*8(%rax)
	movq	_ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic204(%rip), %r14
	testq	%r14, %r14
	jne	.L1613
.L1764:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1614
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1778
.L1614:
	movq	%r14, _ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic204(%rip)
	mfence
	movq	-3200(%rbp), %r12
	testb	$5, (%r14)
	je	.L1615
.L1765:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, -1104(%rbp)
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1616
	movq	(%rax), %rax
	movq	32(%rax), %r10
	leaq	_ZN2v817TracingController26AddTraceEventWithTimestampEcPKhPKcS4_mmiPS4_S2_PKmPSt10unique_ptrINS_24ConvertableToTraceFormatESt14default_deleteIS9_EEjl(%rip), %rax
	cmpq	%rax, %r10
	jne	.L1779
.L1616:
	movq	-1096(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1617
	movq	(%rdi), %rax
	call	*8(%rax)
.L1617:
	movq	-1104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1618
	movq	(%rdi), %rax
	call	*8(%rax)
.L1618:
	movq	-3200(%rbp), %r12
	jmp	.L1615
	.p2align 4,,10
	.p2align 3
.L1762:
	call	_ZN4node7tracing16TraceEventHelper20GetTracingControllerEv@PLT
	leaq	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled(%rip), %r14
	movq	%rax, %rdi
	testq	%rax, %rax
	je	.L1607
	movq	(%rax), %rax
	leaq	_ZN2v817TracingController23GetCategoryGroupEnabledEPKc(%rip), %rdx
	leaq	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no(%rip), %r14
	movq	16(%rax), %rax
	cmpq	%rdx, %rax
	jne	.L1780
.L1607:
	movq	%r14, _ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic201(%rip)
	mfence
	jmp	.L1606
	.p2align 4,,10
	.p2align 3
.L1766:
	leaq	-3296(%rbp), %rdi
	leaq	-3416(%rbp), %rsi
	xorl	%edx, %edx
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	%rax, -3296(%rbp)
	movq	%rax, %rdi
	movq	-3416(%rbp), %rax
	movq	%rax, -3280(%rbp)
.L1619:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	-3416(%rbp), %rax
	movq	-3296(%rbp), %rdx
	jmp	.L1621
	.p2align 4,,10
	.p2align 3
.L1595:
	testq	%r9, %r9
	jne	.L1781
	movq	%r12, %rdx
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1761:
	movq	%r14, %rdi
	leaq	-3416(%rbp), %rsi
	xorl	%edx, %edx
	movq	%r9, -3464(%rbp)
	movq	%r8, -3440(%rbp)
	call	_ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE9_M_createERmm@PLT
	movq	-3440(%rbp), %r8
	movq	-3464(%rbp), %r9
	movq	%rax, -3344(%rbp)
	movq	%rax, %rdi
	movq	-3416(%rbp), %rax
	movq	%rax, -3328(%rbp)
.L1594:
	movq	%r9, %rdx
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	-3416(%rbp), %rax
	movq	-3344(%rbp), %rdx
	jmp	.L1596
	.p2align 4,,10
	.p2align 3
.L1774:
	movq	%r12, %rdi
	movq	%rbx, -3416(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r15), %rdx
	xorl	%eax, %eax
.L1648:
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	jne	.L1782
.L1647:
	movq	%r12, %rdi
	call	_ZN2v87Context4ExitEv@PLT
	movq	-3432(%rbp), %rax
	movq	(%rbx), %rdx
	movq	-3264(%rbp), %rdi
	movq	(%rax), %rax
	movq	%rdx, 24(%rax)
	leaq	16+_ZTVN4node11performance16PerformanceEntryE(%rip), %rax
	movq	%rax, -3312(%rbp)
	cmpq	-3448(%rbp), %rdi
	je	.L1649
	call	_ZdlPv@PLT
.L1649:
	movq	-3296(%rbp), %rdi
	cmpq	-3440(%rbp), %rdi
	je	.L1650
	call	_ZdlPv@PLT
.L1650:
	movq	-2144(%rbp), %rdi
	leaq	-2136(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1651
	testq	%rdi, %rdi
	je	.L1651
	call	free@PLT
.L1651:
	movq	-3200(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L1639
	leaq	-3192(%rbp), %rax
	cmpq	%rax, %rdi
	je	.L1639
.L1756:
	call	free@PLT
.L1639:
	movq	-3456(%rbp), %rdi
	call	_ZN2v811HandleScopeD1Ev@PLT
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L1783
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L1630:
	.cfi_restore_state
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1631
	.p2align 4,,10
	.p2align 3
.L1627:
	movq	%rax, %rdx
	andl	$1, %eax
	pxor	%xmm0, %xmm0
	shrq	%rdx
	orq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	addsd	%xmm0, %xmm0
	jmp	.L1628
	.p2align 4,,10
	.p2align 3
.L1680:
	movl	$8, %eax
.L1601:
	movq	-3448(%rbp), %rcx
	movsd	.LC85(%rip), %xmm1
	movq	64(%rcx), %rdx
	movsd	(%rdx,%rax), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L1602
	cvttsd2siq	%xmm0, %r13
.L1603:
	cmpq	%rbx, %r13
	cmovb	%rbx, %r13
	jmp	.L1600
	.p2align 4,,10
	.p2align 3
.L1574:
	addq	$32, %rdi
	xorl	%esi, %esi
	call	_ZN2v86Object38SlowGetAlignedPointerFromInternalFieldEi@PLT
	movq	%rax, %r15
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1760:
	call	uv_hrtime@PLT
	cmpq	%rax, %rbx
	cmovnb	%rbx, %rax
	movq	%rax, %r13
	jmp	.L1593
	.p2align 4,,10
	.p2align 3
.L1640:
	movq	%r12, %rdi
	movq	%rbx, -3416(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r15), %rdx
	movl	$4, %eax
	movq	104(%rdx), %rdx
	movl	(%rdx,%rax), %eax
	testl	%eax, %eax
	je	.L1647
.L1782:
	xorl	%eax, %eax
	movq	2944(%r15), %rdx
	movq	352(%r15), %rdi
	leaq	-3416(%rbp), %r8
	movq	%rax, %xmm1
	movq	%rax, %xmm0
	movl	$1, %ecx
	movq	-3416(%rbp), %rsi
	call	_ZN4node12MakeCallbackEPN2v87IsolateENS0_5LocalINS0_6ObjectEEENS3_INS0_8FunctionEEEiPNS3_INS0_5ValueEEENS_13async_contextE@PLT
	jmp	.L1647
	.p2align 4,,10
	.p2align 3
.L1672:
	xorl	%eax, %eax
.L1587:
	movq	-3448(%rbp), %rbx
	movsd	.LC85(%rip), %xmm1
	movq	64(%rbx), %rdx
	movsd	(%rdx,%rax), %xmm0
	comisd	%xmm1, %xmm0
	jnb	.L1588
	cvttsd2siq	%xmm0, %rbx
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1588:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rbx
	btcq	$63, %rbx
	jmp	.L1586
	.p2align 4,,10
	.p2align 3
.L1679:
	xorl	%eax, %eax
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1602:
	subsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %r13
	btcq	$63, %r13
	jmp	.L1603
	.p2align 4,,10
	.p2align 3
.L1673:
	movl	$8, %eax
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1641:
	movq	%r12, %rdi
	movq	%rbx, -3416(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r15), %rdx
	movl	$8, %eax
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1778:
	leaq	.LC84(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L1614
	.p2align 4,,10
	.p2align 3
.L1780:
	leaq	.LC84(%rip), %rsi
	call	*%rax
	movq	%rax, %r14
	jmp	.L1607
	.p2align 4,,10
	.p2align 3
.L1779:
	movq	%r13, %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movl	$101, %esi
	movabsq	$2361183241434822607, %rcx
	shrq	$3, %rdx
	movq	%rdx, %rax
	mulq	%rcx
	leaq	-1104(%rbp), %rax
	movq	%r12, %rcx
	shrq	$4, %rdx
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$7
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1616
	.p2align 4,,10
	.p2align 3
.L1777:
	movq	%rbx, %rdx
	movq	%r12, %r9
	xorl	%r8d, %r8d
	movl	$98, %esi
	movabsq	$2361183241434822607, %rcx
	shrq	$3, %rdx
	movq	%rdx, %rax
	mulq	%rcx
	leaq	-1104(%rbp), %rax
	movq	%r12, %rcx
	shrq	$4, %rdx
	pushq	%rdx
	movq	%r14, %rdx
	pushq	$7
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	pushq	$0
	call	*%r10
	addq	$64, %rsp
	jmp	.L1609
	.p2align 4,,10
	.p2align 3
.L1770:
	movq	%rax, -3464(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3464(%rbp), %rcx
	jmp	.L1625
	.p2align 4,,10
	.p2align 3
.L1769:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1624
	.p2align 4,,10
	.p2align 3
.L1768:
	movq	%rcx, -3464(%rbp)
	call	_ZN2v82V812ToLocalEmptyEv@PLT
	movq	-3464(%rbp), %rcx
	jmp	.L1656
	.p2align 4,,10
	.p2align 3
.L1773:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1632
	.p2align 4,,10
	.p2align 3
.L1772:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1629
	.p2align 4,,10
	.p2align 3
.L1771:
	call	_ZN2v82V817FromJustIsNothingEv@PLT
	jmp	.L1626
	.p2align 4,,10
	.p2align 3
.L1644:
	movq	%r12, %rdi
	movq	%rbx, -3416(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r15), %rdx
	movl	$16, %eax
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1674:
	movl	$16, %eax
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1646:
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r15), %rdx
	movl	$24, %eax
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1681:
	movl	$16, %eax
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1645:
	movq	%r12, %rdi
	movq	%rbx, -3416(%rbp)
	call	_ZN2v87Context5EnterEv@PLT
	movq	1864(%r15), %rdx
	movl	$20, %eax
	jmp	.L1648
	.p2align 4,,10
	.p2align 3
.L1675:
	movl	$24, %eax
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1682:
	movl	$24, %eax
	jmp	.L1601
	.p2align 4,,10
	.p2align 3
.L1676:
	movl	$32, %eax
	jmp	.L1587
	.p2align 4,,10
	.p2align 3
.L1683:
	movl	$32, %eax
	jmp	.L1601
.L1783:
	call	__stack_chk_fail@PLT
.L1775:
	movq	%r12, %rdi
	jmp	.L1581
.L1776:
	movq	-3440(%rbp), %rdi
	jmp	.L1619
.L1781:
	movq	%r12, %rdi
	jmp	.L1594
	.cfi_endproc
.LFE7622:
	.size	_ZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEE, .-_ZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEE
	.section	.text.startup,"ax",@progbits
	.p2align 4
	.type	_GLOBAL__sub_I__ZN4node11performance10timeOriginE, @function
_GLOBAL__sub_I__ZN4node11performance10timeOriginE:
.LFB10372:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	call	uv_hrtime@PLT
	movq	%rax, _ZN4node11performance10timeOriginE(%rip)
	call	_ZN4node28GetCurrentTimeInMicrosecondsEv@PLT
	popq	%rbp
	.cfi_def_cfa 7, 8
	movsd	%xmm0, _ZN4node11performanceL19timeOriginTimestampE(%rip)
	ret
	.cfi_endproc
.LFE10372:
	.size	_GLOBAL__sub_I__ZN4node11performance10timeOriginE, .-_GLOBAL__sub_I__ZN4node11performance10timeOriginE
	.section	.init_array,"aw"
	.align 8
	.quad	_GLOBAL__sub_I__ZN4node11performance10timeOriginE
	.weak	_ZTVN4node18MemoryRetainerNodeE
	.section	.data.rel.ro.local._ZTVN4node18MemoryRetainerNodeE,"awG",@progbits,_ZTVN4node18MemoryRetainerNodeE,comdat
	.align 8
	.type	_ZTVN4node18MemoryRetainerNodeE, @object
	.size	_ZTVN4node18MemoryRetainerNodeE, 80
_ZTVN4node18MemoryRetainerNodeE:
	.quad	0
	.quad	0
	.quad	_ZN4node18MemoryRetainerNodeD1Ev
	.quad	_ZN4node18MemoryRetainerNodeD0Ev
	.quad	_ZN4node18MemoryRetainerNode4NameEv
	.quad	_ZN4node18MemoryRetainerNode11SizeInBytesEv
	.quad	_ZN2v813EmbedderGraph4Node11WrapperNodeEv
	.quad	_ZN4node18MemoryRetainerNode10IsRootNodeEv
	.quad	_ZN2v813EmbedderGraph4Node14IsEmbedderNodeEv
	.quad	_ZN4node18MemoryRetainerNode10NamePrefixEv
	.weak	_ZTVN4node9HistogramE
	.section	.data.rel.ro.local._ZTVN4node9HistogramE,"awG",@progbits,_ZTVN4node9HistogramE,comdat
	.align 8
	.type	_ZTVN4node9HistogramE, @object
	.size	_ZTVN4node9HistogramE, 32
_ZTVN4node9HistogramE:
	.quad	0
	.quad	0
	.quad	_ZN4node9HistogramD1Ev
	.quad	_ZN4node9HistogramD0Ev
	.weak	_ZTVN4node11performance18GCPerformanceEntryE
	.section	.data.rel.ro.local._ZTVN4node11performance18GCPerformanceEntryE,"awG",@progbits,_ZTVN4node11performance18GCPerformanceEntryE,comdat
	.align 8
	.type	_ZTVN4node11performance18GCPerformanceEntryE, @object
	.size	_ZTVN4node11performance18GCPerformanceEntryE, 40
_ZTVN4node11performance18GCPerformanceEntryE:
	.quad	0
	.quad	0
	.quad	_ZN4node11performance18GCPerformanceEntryD1Ev
	.quad	_ZN4node11performance18GCPerformanceEntryD0Ev
	.quad	_ZNK4node11performance16PerformanceEntry8ToObjectEv
	.weak	_ZTVN4node11performance12ELDHistogramE
	.section	.data.rel.ro._ZTVN4node11performance12ELDHistogramE,"awG",@progbits,_ZTVN4node11performance12ELDHistogramE,comdat
	.align 8
	.type	_ZTVN4node11performance12ELDHistogramE, @object
	.size	_ZTVN4node11performance12ELDHistogramE, 144
_ZTVN4node11performance12ELDHistogramE:
	.quad	0
	.quad	0
	.quad	_ZN4node11performance12ELDHistogramD1Ev
	.quad	_ZN4node11performance12ELDHistogramD0Ev
	.quad	_ZNK4node11performance12ELDHistogram10MemoryInfoEPNS_13MemoryTrackerE
	.quad	_ZNK4node11performance12ELDHistogram14MemoryInfoNameB5cxx11Ev
	.quad	_ZNK4node11performance12ELDHistogram8SelfSizeEv
	.quad	_ZNK4node10BaseObject13WrappedObjectEv
	.quad	_ZNK4node10BaseObject10IsRootNodeEv
	.quad	_ZNK4node9AsyncWrap18IsDoneInitializingEv
	.quad	_ZN4node10HandleWrap11OnGCCollectEv
	.quad	_ZNK4node9AsyncWrap15diagnostic_nameB5cxx11Ev
	.quad	_ZN4node10HandleWrap5CloseEN2v85LocalINS1_5ValueEEE
	.quad	_ZN4node10HandleWrap7OnCloseEv
	.quad	-88
	.quad	0
	.quad	_ZThn88_N4node11performance12ELDHistogramD1Ev
	.quad	_ZThn88_N4node11performance12ELDHistogramD0Ev
	.weak	_ZTVN4node11performance16PerformanceEntryE
	.section	.data.rel.ro.local._ZTVN4node11performance16PerformanceEntryE,"awG",@progbits,_ZTVN4node11performance16PerformanceEntryE,comdat
	.align 8
	.type	_ZTVN4node11performance16PerformanceEntryE, @object
	.size	_ZTVN4node11performance16PerformanceEntryE, 40
_ZTVN4node11performance16PerformanceEntryE:
	.quad	0
	.quad	0
	.quad	_ZN4node11performance16PerformanceEntryD1Ev
	.quad	_ZN4node11performance16PerformanceEntryD0Ev
	.quad	_ZNK4node11performance16PerformanceEntry8ToObjectEv
	.weak	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE
	.section	.data.rel.ro._ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,"awG",@progbits,_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE,comdat
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE8CallbackE:
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	__cxa_pure_virtual
	.section	.data.rel.ro.local,"aw"
	.align 8
	.type	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_EE, @object
	.size	_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_EE, 40
_ZTVN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_EE:
	.quad	0
	.quad	0
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED1Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_ED0Ev
	.quad	_ZN4node13CallbackQueueIvJPNS_11EnvironmentEEE12CallbackImplIZNS_11performance24MarkGarbageCollectionEndEPN2v87IsolateENS6_6GCTypeENS6_15GCCallbackFlagsEPvEUlS2_E_E4CallES2_
	.weak	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args
	.section	.rodata.str1.1
.LC86:
	.string	"../src/util-inl.h:374"
.LC87:
	.string	"!(n > 0) || (ret != nullptr)"
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"T* node::Realloc(T*, size_t) [with T = v8::Local<v8::Value>; size_t = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,"awG",@progbits,_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args,comdat
	.align 16
	.type	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, @gnu_unique_object
	.size	_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args, 24
_ZZN4node7ReallocIN2v85LocalINS1_5ValueEEEEEPT_S6_mE4args:
	.quad	.LC86
	.quad	.LC87
	.quad	.LC88
	.weak	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args
	.section	.rodata.str1.1
.LC89:
	.string	"../src/util-inl.h:325"
.LC90:
	.string	"(b) == (ret / a)"
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"T node::MultiplyWithOverflowCheck(T, T) [with T = long unsigned int]"
	.section	.data.rel.ro.local._ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,"awG",@progbits,_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args,comdat
	.align 16
	.type	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, @gnu_unique_object
	.size	_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args, 24
_ZZN4node25MultiplyWithOverflowCheckImEET_S1_S1_E4args:
	.quad	.LC89
	.quad	.LC90
	.quad	.LC91
	.section	.rodata.str1.1
.LC92:
	.string	"../src/node_perf.cc"
.LC93:
	.string	"performance"
	.section	.data.rel.local,"aw"
	.align 32
	.type	_ZL7_module, @object
	.size	_ZL7_module, 64
_ZL7_module:
	.long	72
	.long	4
	.quad	0
	.quad	.LC92
	.quad	0
	.quad	_ZN4node11performance10InitializeEN2v85LocalINS1_6ObjectEEENS2_INS1_5ValueEEENS2_INS1_7ContextEEEPv
	.quad	.LC93
	.quad	0
	.quad	0
	.local	_ZZN4node11performance12ELDHistogram11RecordDeltaEvE28trace_event_unique_atomic517
	.comm	_ZZN4node11performance12ELDHistogram11RecordDeltaEvE28trace_event_unique_atomic517,8,8
	.local	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic506
	.comm	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic506,8,8
	.local	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic504
	.comm	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic504,8,8
	.local	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic502
	.comm	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic502,8,8
	.local	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic500
	.comm	_ZZN4node11performance12ELDHistogram21DelayIntervalCallbackEP10uv_timer_sE28trace_event_unique_atomic500,8,8
	.section	.rodata.str1.1
.LC94:
	.string	"../src/node_perf.cc:479"
.LC95:
	.string	"(resolution) > (0)"
	.section	.rodata.str1.8
	.align 8
.LC96:
	.string	"void node::performance::{anonymous}::ELDHistogramNew(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, @object
	.size	_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0, 24
_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args_0:
	.quad	.LC94
	.quad	.LC95
	.quad	.LC96
	.section	.rodata.str1.1
.LC97:
	.string	"../src/node_perf.cc:477"
.LC98:
	.string	"args.IsConstructCall()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node11performance12_GLOBAL__N_1L15ELDHistogramNewERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC97
	.quad	.LC98
	.quad	.LC96
	.section	.rodata.str1.1
.LC99:
	.string	"../src/node_perf.cc:448"
.LC100:
	.string	"args[0]->IsMap()"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"void node::performance::{anonymous}::ELDHistogramPercentiles(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node11performance12_GLOBAL__N_1L23ELDHistogramPercentilesERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC99
	.quad	.LC100
	.quad	.LC101
	.section	.rodata.str1.1
.LC102:
	.string	"../src/node_perf.cc:439"
.LC103:
	.string	"args[0]->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC104:
	.string	"void node::performance::{anonymous}::ELDHistogramPercentile(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, @object
	.size	_ZZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args, 24
_ZZN4node11performance12_GLOBAL__N_1L22ELDHistogramPercentileERKN2v820FunctionCallbackInfoINS2_5ValueEEEE4args:
	.quad	.LC102
	.quad	.LC103
	.quad	.LC104
	.section	.rodata.str1.1
.LC105:
	.string	"../src/node_perf.cc:378"
.LC106:
	.string	"args[1]->IsNumber()"
	.section	.rodata.str1.8
	.align 8
.LC107:
	.string	"void node::performance::Timerify(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, @object
	.size	_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0, 24
_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args_0:
	.quad	.LC105
	.quad	.LC106
	.quad	.LC107
	.section	.rodata.str1.1
.LC108:
	.string	"../src/node_perf.cc:377"
.LC109:
	.string	"args[0]->IsFunction()"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11performance8TimerifyERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC108
	.quad	.LC109
	.quad	.LC107
	.local	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic353
	.comm	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic353,8,8
	.local	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic340
	.comm	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic340,8,8
	.section	.rodata.str1.1
.LC110:
	.string	"../src/node_perf.cc:331"
.LC111:
	.string	"(env) != nullptr"
	.section	.rodata.str1.8
	.align 8
.LC112:
	.string	"void node::performance::TimerFunctionCall(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11performance17TimerFunctionCallERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC110
	.quad	.LC111
	.quad	.LC112
	.section	.rodata.str1.1
.LC113:
	.string	"../src/node_perf.cc:229"
	.section	.rodata.str1.8
	.align 8
.LC114:
	.string	"void node::performance::SetupPerformanceObservers(const v8::FunctionCallbackInfo<v8::Value>&)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, @object
	.size	_ZZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args, 24
_ZZN4node11performance25SetupPerformanceObserversERKN2v820FunctionCallbackInfoINS1_5ValueEEEE4args:
	.quad	.LC113
	.quad	.LC109
	.quad	.LC114
	.local	_ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic204
	.comm	_ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic204,8,8
	.local	_ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic201
	.comm	_ZZN4node11performance7MeasureERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic201,8,8
	.local	_ZZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic136
	.comm	_ZZN4node11performance4MarkERKN2v820FunctionCallbackInfoINS1_5ValueEEEE28trace_event_unique_atomic136,8,8
	.local	_ZZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEmE27trace_event_unique_atomic47
	.comm	_ZZN4node11performance16PerformanceState4MarkENS0_20PerformanceMilestoneEmE27trace_event_unique_atomic47,8,8
	.globl	_ZN4node11performance20performance_v8_startE
	.bss
	.align 8
	.type	_ZN4node11performance20performance_v8_startE, @object
	.size	_ZN4node11performance20performance_v8_startE, 8
_ZN4node11performance20performance_v8_startE:
	.zero	8
	.local	_ZN4node11performanceL19timeOriginTimestampE
	.comm	_ZN4node11performanceL19timeOriginTimestampE,8,8
	.globl	_ZN4node11performance10timeOriginE
	.align 8
	.type	_ZN4node11performance10timeOriginE, @object
	.size	_ZN4node11performance10timeOriginE, 8
_ZN4node11performance10timeOriginE:
	.zero	8
	.section	.rodata.str1.1
.LC115:
	.string	"../src/node_perf.h:31"
.LC116:
	.string	"\"Unreachable code reached\""
	.section	.rodata.str1.8
	.align 8
.LC117:
	.string	"const char* node::performance::GetPerformanceMilestoneName(node::performance::PerformanceMilestone)"
	.section	.data.rel.ro.local
	.align 16
	.type	_ZZN4node11performanceL27GetPerformanceMilestoneNameENS0_20PerformanceMilestoneEE4args, @object
	.size	_ZZN4node11performanceL27GetPerformanceMilestoneNameENS0_20PerformanceMilestoneEE4args, 24
_ZZN4node11performanceL27GetPerformanceMilestoneNameENS0_20PerformanceMilestoneEE4args:
	.quad	.LC115
	.quad	.LC116
	.quad	.LC117
	.weak	_ZZN4node9Histogram10PercentileEdE4args_0
	.section	.rodata.str1.1
.LC118:
	.string	"../src/histogram-inl.h:38"
.LC119:
	.string	"(percentile) <= (100)"
	.section	.rodata.str1.8
	.align 8
.LC120:
	.string	"double node::Histogram::Percentile(double)"
	.section	.data.rel.ro.local._ZZN4node9Histogram10PercentileEdE4args_0,"awG",@progbits,_ZZN4node9Histogram10PercentileEdE4args_0,comdat
	.align 16
	.type	_ZZN4node9Histogram10PercentileEdE4args_0, @gnu_unique_object
	.size	_ZZN4node9Histogram10PercentileEdE4args_0, 24
_ZZN4node9Histogram10PercentileEdE4args_0:
	.quad	.LC118
	.quad	.LC119
	.quad	.LC120
	.weak	_ZZN4node9Histogram10PercentileEdE4args
	.section	.rodata.str1.1
.LC121:
	.string	"../src/histogram-inl.h:37"
.LC122:
	.string	"(percentile) > (0)"
	.section	.data.rel.ro.local._ZZN4node9Histogram10PercentileEdE4args,"awG",@progbits,_ZZN4node9Histogram10PercentileEdE4args,comdat
	.align 16
	.type	_ZZN4node9Histogram10PercentileEdE4args, @gnu_unique_object
	.size	_ZZN4node9Histogram10PercentileEdE4args, 24
_ZZN4node9Histogram10PercentileEdE4args:
	.quad	.LC121
	.quad	.LC122
	.quad	.LC120
	.weak	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args
	.section	.rodata.str1.1
.LC123:
	.string	"../src/base_object-inl.h:131"
	.section	.rodata.str1.8
	.align 8
.LC124:
	.string	"!(obj->has_pointer_data()) || (obj->pointer_data()->strong_ptr_count == 0)"
	.align 8
.LC125:
	.string	"node::BaseObject::MakeWeak()::<lambda(const v8::WeakCallbackInfo<node::BaseObject>&)>"
	.section	.data.rel.ro.local._ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,"awG",@progbits,_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args,comdat
	.align 16
	.type	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, @gnu_unique_object
	.size	_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args, 24
_ZZZN4node10BaseObject8MakeWeakEvENKUlRKN2v816WeakCallbackInfoIS0_EEE_clES5_E4args:
	.quad	.LC123
	.quad	.LC124
	.quad	.LC125
	.weak	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args
	.section	.rodata.str1.1
.LC126:
	.string	"../src/base_object-inl.h:104"
	.section	.rodata.str1.8
	.align 8
.LC127:
	.string	"(obj->InternalFieldCount()) > (0)"
	.align 8
.LC128:
	.string	"static node::BaseObject* node::BaseObject::FromJSObject(v8::Local<v8::Object>)"
	.section	.data.rel.ro.local._ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,"awG",@progbits,_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args,comdat
	.align 16
	.type	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, @gnu_unique_object
	.size	_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args, 24
_ZZN4node10BaseObject12FromJSObjectEN2v85LocalINS1_6ObjectEEEE4args:
	.quad	.LC126
	.quad	.LC127
	.quad	.LC128
	.weak	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args
	.section	.rodata.str1.1
.LC129:
	.string	"../src/env-inl.h:1118"
	.section	.rodata.str1.8
	.align 8
.LC130:
	.string	"(insertion_info.second) == (true)"
	.align 8
.LC131:
	.string	"void node::Environment::AddCleanupHook(void (*)(void*), void*)"
	.section	.data.rel.ro.local._ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,"awG",@progbits,_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args,comdat
	.align 16
	.type	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, @gnu_unique_object
	.size	_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args, 24
_ZZN4node11Environment14AddCleanupHookEPFvPvES1_E4args:
	.quad	.LC129
	.quad	.LC130
	.quad	.LC131
	.weak	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled
	.section	.rodata._ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,"aG",@progbits,_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled,comdat
	.type	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, @gnu_unique_object
	.size	_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled, 1
_ZZN4node7tracing16TraceEventHelper23GetCategoryGroupEnabledEPKcE8disabled:
	.zero	1
	.weak	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no
	.section	.bss._ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,"awG",@nobits,_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no,comdat
	.type	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, @gnu_unique_object
	.size	_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no, 1
_ZZN2v817TracingController23GetCategoryGroupEnabledEPKcE2no:
	.zero	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	0
	.align 8
.LC2:
	.long	0
	.long	1079574528
	.align 8
.LC11:
	.long	0
	.long	1093567616
	.align 8
.LC23:
	.long	0
	.long	1073741824
	.align 8
.LC25:
	.long	0
	.long	1072693248
	.align 8
.LC27:
	.long	0
	.long	1074790400
	.align 8
.LC29:
	.long	0
	.long	1075838976
	.align 8
.LC35:
	.long	0
	.long	1076887552
	.align 8
.LC37:
	.long	0
	.long	1077936128
	.align 8
.LC39:
	.long	0
	.long	1078984704
	.align 8
.LC44:
	.long	0
	.long	1074266112
	.align 8
.LC47:
	.long	0
	.long	1075052544
	.align 8
.LC49:
	.long	0
	.long	1075314688
	.align 8
.LC57:
	.long	0
	.long	1083129856
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC78:
	.quad	0
	.quad	1024
	.section	.rodata.cst8
	.align 8
.LC85:
	.long	0
	.long	1138753536
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
