	.file	"ares_data.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares_free_data
	.type	ares_free_data, @function
ares_free_data:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares_data.c"
	.loc 1 42 1 view -0
	.cfi_startproc
	.loc 1 42 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 43 3 is_stmt 1 view .LVU2
	.loc 1 43 9 view .LVU3
	testq	%rdi, %rdi
	je	.L52
	.loc 1 42 1 is_stmt 0 view .LVU4
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 14, -24
	.cfi_offset 13, -32
	.cfi_offset 12, -40
	leaq	.L5(%rip), %r12
	pushq	%rbx
	.cfi_offset 3, -48
	movq	%rdi, %rbx
.LVL1:
.L21:
.LBB6:
	.loc 1 44 5 is_stmt 1 view .LVU5
	.loc 1 45 5 view .LVU6
	.loc 1 53 5 view .LVU7
	.loc 1 59 8 is_stmt 0 view .LVU8
	cmpl	$48813, -4(%rbx)
	.loc 1 53 9 view .LVU9
	leaq	-8(%rbx), %r13
.LVL2:
	.loc 1 59 5 is_stmt 1 view .LVU10
	.loc 1 59 8 is_stmt 0 view .LVU11
	jne	.L1
	.loc 1 62 5 is_stmt 1 view .LVU12
	cmpl	$9, -8(%rbx)
	ja	.L1
	movl	-8(%rbx), %eax
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	notrack jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L1-.L5
	.long	.L1-.L5
	.long	.L10-.L5
	.long	.L10-.L5
	.long	.L10-.L5
	.long	.L4-.L5
	.long	.L10-.L5
	.long	.L7-.L5
	.long	.L6-.L5
	.long	.L4-.L5
	.text
	.p2align 4,,10
	.p2align 3
.L10:
	.loc 1 83 11 view .LVU13
.LVL3:
	.loc 1 85 11 view .LVU14
	.loc 1 85 34 is_stmt 0 view .LVU15
	movq	8(%rbx), %rdi
	movq	ares_free(%rip), %rax
	movq	(%rbx), %rbx
.LVL4:
	.loc 1 85 14 view .LVU16
	testq	%rdi, %rdi
	je	.L12
.L56:
	.loc 1 112 13 is_stmt 1 view .LVU17
	call	*%rax
.LVL5:
	movq	ares_free(%rip), %rax
.L12:
	.loc 1 126 5 view .LVU18
	movq	%r13, %rdi
	call	*%rax
.LVL6:
	.loc 1 127 5 view .LVU19
	.loc 1 127 5 is_stmt 0 view .LVU20
.LBE6:
	.loc 1 43 9 is_stmt 1 view .LVU21
	testq	%rbx, %rbx
	jne	.L21
.LVL7:
.L1:
	.loc 1 129 1 is_stmt 0 view .LVU22
	popq	%rbx
.LVL8:
	.loc 1 129 1 view .LVU23
	popq	%r12
	popq	%r13
.LVL9:
	.loc 1 129 1 view .LVU24
	popq	%r14
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL10:
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
.LBB7:
	.loc 1 97 11 is_stmt 1 view .LVU25
	.loc 1 97 39 is_stmt 0 view .LVU26
	movq	(%rbx), %rbx
.LVL11:
	.loc 1 97 14 view .LVU27
	testq	%rbx, %rbx
	je	.L49
.LVL12:
	.loc 1 126 5 is_stmt 1 view .LVU28
	movq	%r13, %rdi
	call	*ares_free(%rip)
.LVL13:
	.loc 1 127 5 view .LVU29
	.loc 1 127 5 is_stmt 0 view .LVU30
.LBE7:
	.loc 1 43 9 is_stmt 1 view .LVU31
	.loc 1 43 9 is_stmt 0 view .LVU32
	jmp	.L21
.LVL14:
	.p2align 4,,10
	.p2align 3
.L7:
.LBB8:
	.loc 1 103 11 is_stmt 1 view .LVU33
	.loc 1 105 36 is_stmt 0 view .LVU34
	movq	8(%rbx), %rdi
	.loc 1 103 36 view .LVU35
	movq	(%rbx), %r14
.LVL15:
	.loc 1 105 11 is_stmt 1 view .LVU36
	movq	ares_free(%rip), %rax
	.loc 1 105 14 is_stmt 0 view .LVU37
	testq	%rdi, %rdi
	je	.L16
	.loc 1 106 13 is_stmt 1 view .LVU38
	call	*%rax
.LVL16:
	movq	ares_free(%rip), %rax
.L16:
	.loc 1 107 11 view .LVU39
	.loc 1 107 36 is_stmt 0 view .LVU40
	movq	16(%rbx), %rdi
	.loc 1 107 14 view .LVU41
	testq	%rdi, %rdi
	je	.L17
	.loc 1 108 13 is_stmt 1 view .LVU42
	call	*%rax
.LVL17:
	movq	ares_free(%rip), %rax
.L17:
	.loc 1 109 11 view .LVU43
	.loc 1 109 36 is_stmt 0 view .LVU44
	movq	24(%rbx), %rdi
	.loc 1 109 14 view .LVU45
	testq	%rdi, %rdi
	je	.L18
	.loc 1 110 13 is_stmt 1 view .LVU46
	call	*%rax
.LVL18:
	movq	ares_free(%rip), %rax
.L18:
	.loc 1 111 11 view .LVU47
	.loc 1 111 36 is_stmt 0 view .LVU48
	movq	32(%rbx), %rdi
	movq	%r14, %rbx
.LVL19:
	.loc 1 111 14 view .LVU49
	testq	%rdi, %rdi
	jne	.L56
.LVL20:
	.loc 1 111 14 view .LVU50
	jmp	.L12
.LVL21:
	.p2align 4,,10
	.p2align 3
.L6:
	.loc 1 116 11 is_stmt 1 view .LVU51
	.loc 1 116 34 is_stmt 0 view .LVU52
	movq	(%rbx), %rdi
	movq	ares_free(%rip), %rax
	.loc 1 116 14 view .LVU53
	testq	%rdi, %rdi
	je	.L19
	.loc 1 117 13 is_stmt 1 view .LVU54
	call	*%rax
.LVL22:
	movq	ares_free(%rip), %rax
.L19:
	.loc 1 118 11 view .LVU55
	.loc 1 118 34 is_stmt 0 view .LVU56
	movq	8(%rbx), %rdi
	.loc 1 118 14 view .LVU57
	testq	%rdi, %rdi
	je	.L20
	.loc 1 119 13 is_stmt 1 view .LVU58
	call	*%rax
.LVL23:
	.loc 1 126 5 view .LVU59
.L49:
	.loc 1 126 5 view .LVU60
.LBE8:
	.loc 1 129 1 is_stmt 0 view .LVU61
	popq	%rbx
	.cfi_remember_state
	.cfi_restore 3
.LBB9:
	.loc 1 126 5 view .LVU62
	movq	%r13, %rdi
.LBE9:
	.loc 1 129 1 view .LVU63
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
.LVL24:
	.loc 1 129 1 view .LVU64
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
.LBB10:
	.loc 1 126 5 view .LVU65
	jmp	*ares_free(%rip)
.LVL25:
	.p2align 4,,10
	.p2align 3
.L20:
	.cfi_restore_state
	.loc 1 126 5 is_stmt 1 view .LVU66
.LBE10:
	.loc 1 129 1 is_stmt 0 view .LVU67
	popq	%rbx
	.cfi_restore 3
.LVL26:
.LBB11:
	.loc 1 126 5 view .LVU68
	movq	%r13, %rdi
.LBE11:
	.loc 1 129 1 view .LVU69
	popq	%r12
	.cfi_restore 12
	popq	%r13
	.cfi_restore 13
.LVL27:
	.loc 1 129 1 view .LVU70
	popq	%r14
	.cfi_restore 14
	popq	%rbp
	.cfi_restore 6
	.cfi_def_cfa 7, 8
.LBB12:
	.loc 1 126 5 view .LVU71
	jmp	*%rax
.LVL28:
.L52:
	.loc 1 126 5 view .LVU72
	ret
.LBE12:
	.cfi_endproc
.LFE87:
	.size	ares_free_data, .-ares_free_data
	.p2align 4
	.globl	ares_malloc_data
	.type	ares_malloc_data, @function
ares_malloc_data:
.LVL29:
.LFB88:
	.loc 1 144 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 144 1 is_stmt 0 view .LVU74
	endbr64
	.loc 1 145 3 is_stmt 1 view .LVU75
	.loc 1 147 3 view .LVU76
	.loc 1 144 1 is_stmt 0 view .LVU77
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	.cfi_offset 3, -24
	movl	%edi, %ebx
	.loc 1 147 9 view .LVU78
	movl	$56, %edi
.LVL30:
	.loc 1 144 1 view .LVU79
	subq	$8, %rsp
	.loc 1 147 9 view .LVU80
	call	*ares_malloc(%rip)
.LVL31:
	.loc 1 148 3 is_stmt 1 view .LVU81
	.loc 1 148 6 is_stmt 0 view .LVU82
	testq	%rax, %rax
	je	.L61
	.loc 1 151 3 is_stmt 1 view .LVU83
	cmpl	$9, %ebx
	ja	.L63
	leaq	.L65(%rip), %rsi
	movl	%ebx, %ecx
	movslq	(%rsi,%rcx,4), %rdx
	addq	%rsi, %rdx
	notrack jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L65:
	.long	.L63-.L65
	.long	.L63-.L65
	.long	.L72-.L65
	.long	.L71-.L65
	.long	.L70-.L65
	.long	.L69-.L65
	.long	.L68-.L65
	.long	.L67-.L65
	.long	.L66-.L65
	.long	.L64-.L65
	.text
.L70:
	.loc 1 168 9 view .LVU84
	.loc 1 168 40 is_stmt 0 view .LVU85
	movb	$0, 32(%rax)
.L71:
	.loc 1 172 9 is_stmt 1 view .LVU86
	.loc 1 173 9 view .LVU87
	.loc 1 174 36 is_stmt 0 view .LVU88
	movq	$0, 24(%rax)
	.loc 1 172 34 view .LVU89
	pxor	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	.loc 1 174 9 is_stmt 1 view .LVU90
	.loc 1 175 9 view .LVU91
.L73:
	.loc 1 218 3 view .LVU92
	.loc 1 218 13 is_stmt 0 view .LVU93
	movl	$48813, 4(%rax)
	.loc 1 219 3 is_stmt 1 view .LVU94
	.loc 1 221 10 is_stmt 0 view .LVU95
	addq	$8, %rax
.LVL32:
	.loc 1 219 13 view .LVU96
	movl	%ebx, -8(%rax)
	.loc 1 221 3 is_stmt 1 view .LVU97
.LVL33:
.L61:
	.loc 1 222 1 is_stmt 0 view .LVU98
	addq	$8, %rsp
	popq	%rbx
.LVL34:
	.loc 1 222 1 view .LVU99
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL35:
.L72:
	.cfi_restore_state
	.loc 1 160 9 is_stmt 1 view .LVU100
	.loc 1 161 9 view .LVU101
	.loc 1 162 9 view .LVU102
	.loc 1 163 9 view .LVU103
	.loc 1 164 9 view .LVU104
	.loc 1 160 34 is_stmt 0 view .LVU105
	pxor	%xmm0, %xmm0
	.loc 1 164 34 view .LVU106
	xorl	%edx, %edx
	.loc 1 162 38 view .LVU107
	movl	$0, 24(%rax)
	.loc 1 164 34 view .LVU108
	movw	%dx, 28(%rax)
	.loc 1 165 9 is_stmt 1 view .LVU109
	.loc 1 160 34 is_stmt 0 view .LVU110
	movups	%xmm0, 8(%rax)
	.loc 1 165 9 view .LVU111
	jmp	.L73
.L69:
	.loc 1 178 9 is_stmt 1 view .LVU112
.LBB13:
.LBB14:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 71 10 is_stmt 0 view .LVU113
	pxor	%xmm0, %xmm0
.LBE14:
.LBE13:
	.loc 1 178 34 view .LVU114
	movq	$0, 8(%rax)
	.loc 1 179 9 is_stmt 1 view .LVU115
	.loc 1 179 36 is_stmt 0 view .LVU116
	movl	$0, 16(%rax)
	.loc 1 180 9 is_stmt 1 view .LVU117
.LVL36:
.LBB16:
.LBI13:
	.loc 2 59 42 view .LVU118
.LBB15:
	.loc 2 71 3 view .LVU119
	.loc 2 71 10 is_stmt 0 view .LVU120
	movups	%xmm0, 20(%rax)
	jmp	.L73
.LVL37:
.L68:
	.loc 2 71 10 view .LVU121
.LBE15:
.LBE16:
	.loc 1 154 9 is_stmt 1 view .LVU122
	.loc 1 155 9 view .LVU123
	.loc 1 154 33 is_stmt 0 view .LVU124
	pxor	%xmm0, %xmm0
	.loc 1 156 37 view .LVU125
	xorl	%ecx, %ecx
	movw	%cx, 24(%rax)
	.loc 1 154 33 view .LVU126
	movups	%xmm0, 8(%rax)
	.loc 1 156 9 is_stmt 1 view .LVU127
	.loc 1 157 9 view .LVU128
	jmp	.L73
.L67:
	.loc 1 194 9 view .LVU129
	.loc 1 195 9 view .LVU130
	.loc 1 196 9 view .LVU131
	.loc 1 197 9 view .LVU132
	.loc 1 198 9 view .LVU133
	.loc 1 199 9 view .LVU134
	.loc 1 200 9 view .LVU135
	.loc 1 194 36 is_stmt 0 view .LVU136
	pxor	%xmm0, %xmm0
	.loc 1 198 43 view .LVU137
	movq	$0, 40(%rax)
	.loc 1 199 37 view .LVU138
	movl	$0, 48(%rax)
	.loc 1 201 9 is_stmt 1 view .LVU139
	.loc 1 194 36 is_stmt 0 view .LVU140
	movups	%xmm0, 8(%rax)
	movups	%xmm0, 24(%rax)
	.loc 1 201 9 view .LVU141
	jmp	.L73
.L66:
	.loc 1 204 9 is_stmt 1 view .LVU142
	.loc 1 205 9 view .LVU143
	.loc 1 204 36 is_stmt 0 view .LVU144
	pxor	%xmm0, %xmm0
	.loc 1 210 36 view .LVU145
	movl	$0, 40(%rax)
	.loc 1 204 36 view .LVU146
	movups	%xmm0, 8(%rax)
	.loc 1 206 9 is_stmt 1 view .LVU147
	.loc 1 207 9 view .LVU148
	.loc 1 208 9 view .LVU149
	.loc 1 209 9 view .LVU150
	.loc 1 206 36 is_stmt 0 view .LVU151
	pxor	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	.loc 1 210 9 is_stmt 1 view .LVU152
	.loc 1 211 2 view .LVU153
	jmp	.L73
.L64:
	.loc 1 185 9 view .LVU154
.LBB17:
.LBB18:
	.loc 2 71 10 is_stmt 0 view .LVU155
	pxor	%xmm0, %xmm0
.LBE18:
.LBE17:
	.loc 1 185 39 view .LVU156
	movq	$0, 8(%rax)
	.loc 1 186 9 is_stmt 1 view .LVU157
	.loc 1 186 41 is_stmt 0 view .LVU158
	movl	$0, 16(%rax)
	.loc 1 187 9 is_stmt 1 view .LVU159
	.loc 1 188 9 view .LVU160
	.loc 1 187 43 is_stmt 0 view .LVU161
	movq	$0, 36(%rax)
	.loc 1 189 9 is_stmt 1 view .LVU162
.LVL38:
.LBB20:
.LBI17:
	.loc 2 59 42 view .LVU163
.LBB19:
	.loc 2 71 3 view .LVU164
	.loc 2 71 10 is_stmt 0 view .LVU165
	movups	%xmm0, 20(%rax)
	jmp	.L73
.LVL39:
.L63:
	.loc 2 71 10 view .LVU166
.LBE19:
.LBE20:
	.loc 1 214 9 is_stmt 1 view .LVU167
	movq	%rax, %rdi
	call	*ares_free(%rip)
.LVL40:
	.loc 1 215 9 view .LVU168
	.loc 1 222 1 is_stmt 0 view .LVU169
	addq	$8, %rsp
	.loc 1 215 15 view .LVU170
	xorl	%eax, %eax
	.loc 1 222 1 view .LVU171
	popq	%rbx
.LVL41:
	.loc 1 222 1 view .LVU172
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE88:
	.size	ares_malloc_data, .-ares_malloc_data
.Letext0:
	.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 7 "/usr/include/netinet/in.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/errno.h"
	.file 13 "/usr/include/time.h"
	.file 14 "/usr/include/unistd.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 17 "../deps/cares/include/ares.h"
	.file 18 "../deps/cares/src/ares_data.h"
	.file 19 "../deps/cares/src/ares_ipv6.h"
	.file 20 "../deps/cares/src/ares_private.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xda2
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF180
	.byte	0x1
	.long	.LASF181
	.long	.LASF182
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x3
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x3
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x3
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x3
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x3
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0xa9
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x7
	.long	0xa9
	.uleb128 0x3
	.long	.LASF13
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF14
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF15
	.uleb128 0x3
	.long	.LASF16
	.byte	0x5
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF23
	.byte	0x10
	.byte	0x6
	.byte	0xb2
	.byte	0x8
	.long	0x103
	.uleb128 0x9
	.long	.LASF17
	.byte	0x6
	.byte	0xb4
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF18
	.byte	0x6
	.byte	0xb5
	.byte	0xa
	.long	0x108
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0xdb
	.uleb128 0xa
	.long	0xa9
	.long	0x118
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xdb
	.uleb128 0xc
	.long	0x118
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x7
	.long	0x123
	.uleb128 0x6
	.byte	0x8
	.long	0x123
	.uleb128 0xc
	.long	0x12d
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x7
	.long	0x138
	.uleb128 0x6
	.byte	0x8
	.long	0x138
	.uleb128 0xc
	.long	0x142
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x7
	.long	0x14d
	.uleb128 0x6
	.byte	0x8
	.long	0x14d
	.uleb128 0xc
	.long	0x157
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x7
	.long	0x162
	.uleb128 0x6
	.byte	0x8
	.long	0x162
	.uleb128 0xc
	.long	0x16c
	.uleb128 0x8
	.long	.LASF24
	.byte	0x10
	.byte	0x7
	.byte	0xee
	.byte	0x8
	.long	0x1b9
	.uleb128 0x9
	.long	.LASF25
	.byte	0x7
	.byte	0xf0
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF26
	.byte	0x7
	.byte	0xf1
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0x9
	.long	.LASF27
	.byte	0x7
	.byte	0xf2
	.byte	0x14
	.long	0x693
	.byte	0x4
	.uleb128 0x9
	.long	.LASF28
	.byte	0x7
	.byte	0xf5
	.byte	0x13
	.long	0x750
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	0x177
	.uleb128 0x6
	.byte	0x8
	.long	0x177
	.uleb128 0xc
	.long	0x1be
	.uleb128 0x8
	.long	.LASF29
	.byte	0x1c
	.byte	0x7
	.byte	0xfd
	.byte	0x8
	.long	0x21c
	.uleb128 0x9
	.long	.LASF30
	.byte	0x7
	.byte	0xff
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0xe
	.long	.LASF31
	.byte	0x7
	.value	0x100
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0xe
	.long	.LASF32
	.byte	0x7
	.value	0x101
	.byte	0xe
	.long	0x67b
	.byte	0x4
	.uleb128 0xe
	.long	.LASF33
	.byte	0x7
	.value	0x102
	.byte	0x15
	.long	0x718
	.byte	0x8
	.uleb128 0xe
	.long	.LASF34
	.byte	0x7
	.value	0x103
	.byte	0xe
	.long	0x67b
	.byte	0x18
	.byte	0
	.uleb128 0x7
	.long	0x1c9
	.uleb128 0x6
	.byte	0x8
	.long	0x1c9
	.uleb128 0xc
	.long	0x221
	.uleb128 0xd
	.long	.LASF35
	.uleb128 0x7
	.long	0x22c
	.uleb128 0x6
	.byte	0x8
	.long	0x22c
	.uleb128 0xc
	.long	0x236
	.uleb128 0xd
	.long	.LASF36
	.uleb128 0x7
	.long	0x241
	.uleb128 0x6
	.byte	0x8
	.long	0x241
	.uleb128 0xc
	.long	0x24b
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x7
	.long	0x256
	.uleb128 0x6
	.byte	0x8
	.long	0x256
	.uleb128 0xc
	.long	0x260
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x7
	.long	0x26b
	.uleb128 0x6
	.byte	0x8
	.long	0x26b
	.uleb128 0xc
	.long	0x275
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x7
	.long	0x280
	.uleb128 0x6
	.byte	0x8
	.long	0x280
	.uleb128 0xc
	.long	0x28a
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x7
	.long	0x295
	.uleb128 0x6
	.byte	0x8
	.long	0x295
	.uleb128 0xc
	.long	0x29f
	.uleb128 0x6
	.byte	0x8
	.long	0x103
	.uleb128 0xc
	.long	0x2aa
	.uleb128 0x6
	.byte	0x8
	.long	0x128
	.uleb128 0xc
	.long	0x2b5
	.uleb128 0x6
	.byte	0x8
	.long	0x13d
	.uleb128 0xc
	.long	0x2c0
	.uleb128 0x6
	.byte	0x8
	.long	0x152
	.uleb128 0xc
	.long	0x2cb
	.uleb128 0x6
	.byte	0x8
	.long	0x167
	.uleb128 0xc
	.long	0x2d6
	.uleb128 0x6
	.byte	0x8
	.long	0x1b9
	.uleb128 0xc
	.long	0x2e1
	.uleb128 0x6
	.byte	0x8
	.long	0x21c
	.uleb128 0xc
	.long	0x2ec
	.uleb128 0x6
	.byte	0x8
	.long	0x231
	.uleb128 0xc
	.long	0x2f7
	.uleb128 0x6
	.byte	0x8
	.long	0x246
	.uleb128 0xc
	.long	0x302
	.uleb128 0x6
	.byte	0x8
	.long	0x25b
	.uleb128 0xc
	.long	0x30d
	.uleb128 0x6
	.byte	0x8
	.long	0x270
	.uleb128 0xc
	.long	0x318
	.uleb128 0x6
	.byte	0x8
	.long	0x285
	.uleb128 0xc
	.long	0x323
	.uleb128 0x6
	.byte	0x8
	.long	0x29a
	.uleb128 0xc
	.long	0x32e
	.uleb128 0xa
	.long	0xa9
	.long	0x349
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF41
	.byte	0xd8
	.byte	0x8
	.byte	0x31
	.byte	0x8
	.long	0x4d0
	.uleb128 0x9
	.long	.LASF42
	.byte	0x8
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0x9
	.long	.LASF43
	.byte	0x8
	.byte	0x36
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0x9
	.long	.LASF44
	.byte	0x8
	.byte	0x37
	.byte	0x9
	.long	0xa3
	.byte	0x10
	.uleb128 0x9
	.long	.LASF45
	.byte	0x8
	.byte	0x38
	.byte	0x9
	.long	0xa3
	.byte	0x18
	.uleb128 0x9
	.long	.LASF46
	.byte	0x8
	.byte	0x39
	.byte	0x9
	.long	0xa3
	.byte	0x20
	.uleb128 0x9
	.long	.LASF47
	.byte	0x8
	.byte	0x3a
	.byte	0x9
	.long	0xa3
	.byte	0x28
	.uleb128 0x9
	.long	.LASF48
	.byte	0x8
	.byte	0x3b
	.byte	0x9
	.long	0xa3
	.byte	0x30
	.uleb128 0x9
	.long	.LASF49
	.byte	0x8
	.byte	0x3c
	.byte	0x9
	.long	0xa3
	.byte	0x38
	.uleb128 0x9
	.long	.LASF50
	.byte	0x8
	.byte	0x3d
	.byte	0x9
	.long	0xa3
	.byte	0x40
	.uleb128 0x9
	.long	.LASF51
	.byte	0x8
	.byte	0x40
	.byte	0x9
	.long	0xa3
	.byte	0x48
	.uleb128 0x9
	.long	.LASF52
	.byte	0x8
	.byte	0x41
	.byte	0x9
	.long	0xa3
	.byte	0x50
	.uleb128 0x9
	.long	.LASF53
	.byte	0x8
	.byte	0x42
	.byte	0x9
	.long	0xa3
	.byte	0x58
	.uleb128 0x9
	.long	.LASF54
	.byte	0x8
	.byte	0x44
	.byte	0x16
	.long	0x4e9
	.byte	0x60
	.uleb128 0x9
	.long	.LASF55
	.byte	0x8
	.byte	0x46
	.byte	0x14
	.long	0x4ef
	.byte	0x68
	.uleb128 0x9
	.long	.LASF56
	.byte	0x8
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0x9
	.long	.LASF57
	.byte	0x8
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0x9
	.long	.LASF58
	.byte	0x8
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0x9
	.long	.LASF59
	.byte	0x8
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF60
	.byte	0x8
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF61
	.byte	0x8
	.byte	0x4f
	.byte	0x8
	.long	0x339
	.byte	0x83
	.uleb128 0x9
	.long	.LASF62
	.byte	0x8
	.byte	0x51
	.byte	0xf
	.long	0x4f5
	.byte	0x88
	.uleb128 0x9
	.long	.LASF63
	.byte	0x8
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0x9
	.long	.LASF64
	.byte	0x8
	.byte	0x5b
	.byte	0x17
	.long	0x500
	.byte	0x98
	.uleb128 0x9
	.long	.LASF65
	.byte	0x8
	.byte	0x5c
	.byte	0x19
	.long	0x50b
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF66
	.byte	0x8
	.byte	0x5d
	.byte	0x14
	.long	0x4ef
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF67
	.byte	0x8
	.byte	0x5e
	.byte	0x9
	.long	0xa1
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF68
	.byte	0x8
	.byte	0x5f
	.byte	0xa
	.long	0xb5
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF69
	.byte	0x8
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF70
	.byte	0x8
	.byte	0x62
	.byte	0x8
	.long	0x511
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF71
	.byte	0x9
	.byte	0x7
	.byte	0x19
	.long	0x349
	.uleb128 0xf
	.long	.LASF183
	.byte	0x8
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF72
	.uleb128 0x6
	.byte	0x8
	.long	0x4e4
	.uleb128 0x6
	.byte	0x8
	.long	0x349
	.uleb128 0x6
	.byte	0x8
	.long	0x4dc
	.uleb128 0xd
	.long	.LASF73
	.uleb128 0x6
	.byte	0x8
	.long	0x4fb
	.uleb128 0xd
	.long	.LASF74
	.uleb128 0x6
	.byte	0x8
	.long	0x506
	.uleb128 0xa
	.long	0xa9
	.long	0x521
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb0
	.uleb128 0x7
	.long	0x521
	.uleb128 0x10
	.long	.LASF75
	.byte	0xa
	.byte	0x89
	.byte	0xe
	.long	0x538
	.uleb128 0x6
	.byte	0x8
	.long	0x4d0
	.uleb128 0x10
	.long	.LASF76
	.byte	0xa
	.byte	0x8a
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF77
	.byte	0xa
	.byte	0x8b
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF78
	.byte	0xb
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xa
	.long	0x527
	.long	0x56d
	.uleb128 0x11
	.byte	0
	.uleb128 0x7
	.long	0x562
	.uleb128 0x10
	.long	.LASF79
	.byte	0xb
	.byte	0x1b
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF80
	.byte	0xb
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF81
	.byte	0xb
	.byte	0x1f
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF82
	.byte	0xc
	.byte	0x2d
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF83
	.byte	0xc
	.byte	0x2e
	.byte	0xe
	.long	0xa3
	.uleb128 0xa
	.long	0xa3
	.long	0x5be
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF84
	.byte	0xd
	.byte	0x9f
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF85
	.byte	0xd
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF86
	.byte	0xd
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF87
	.byte	0xd
	.byte	0xa6
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF88
	.byte	0xd
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF89
	.byte	0xd
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x12
	.long	.LASF90
	.byte	0xd
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x12
	.long	.LASF91
	.byte	0xe
	.value	0x21f
	.byte	0xf
	.long	0x620
	.uleb128 0x6
	.byte	0x8
	.long	0xa3
	.uleb128 0x12
	.long	.LASF92
	.byte	0xe
	.value	0x221
	.byte	0xf
	.long	0x620
	.uleb128 0x10
	.long	.LASF93
	.byte	0xf
	.byte	0x24
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF94
	.byte	0xf
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF95
	.byte	0xf
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF96
	.byte	0xf
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF97
	.byte	0x10
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF98
	.byte	0x10
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF99
	.byte	0x10
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF100
	.byte	0x7
	.byte	0x1e
	.byte	0x12
	.long	0x67b
	.uleb128 0x8
	.long	.LASF101
	.byte	0x4
	.byte	0x7
	.byte	0x1f
	.byte	0x8
	.long	0x6ae
	.uleb128 0x9
	.long	.LASF102
	.byte	0x7
	.byte	0x21
	.byte	0xf
	.long	0x687
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF103
	.byte	0x7
	.byte	0x77
	.byte	0x12
	.long	0x66f
	.uleb128 0x13
	.byte	0x10
	.byte	0x7
	.byte	0xd6
	.byte	0x5
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF104
	.byte	0x7
	.byte	0xd8
	.byte	0xa
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF105
	.byte	0x7
	.byte	0xd9
	.byte	0xb
	.long	0x6f8
	.uleb128 0x14
	.long	.LASF106
	.byte	0x7
	.byte	0xda
	.byte	0xb
	.long	0x708
	.byte	0
	.uleb128 0xa
	.long	0x663
	.long	0x6f8
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x66f
	.long	0x708
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x67b
	.long	0x718
	.uleb128 0xb
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.long	.LASF107
	.byte	0x10
	.byte	0x7
	.byte	0xd4
	.byte	0x8
	.long	0x733
	.uleb128 0x9
	.long	.LASF108
	.byte	0x7
	.byte	0xdb
	.byte	0x9
	.long	0x6ba
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x718
	.uleb128 0x10
	.long	.LASF109
	.byte	0x7
	.byte	0xe4
	.byte	0x1e
	.long	0x733
	.uleb128 0x10
	.long	.LASF110
	.byte	0x7
	.byte	0xe5
	.byte	0x1e
	.long	0x733
	.uleb128 0xa
	.long	0x2d
	.long	0x760
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x2d
	.uleb128 0x15
	.byte	0x10
	.byte	0x11
	.value	0x204
	.byte	0x3
	.long	0x77e
	.uleb128 0x16
	.long	.LASF111
	.byte	0x11
	.value	0x205
	.byte	0x13
	.long	0x77e
	.byte	0
	.uleb128 0xa
	.long	0x2d
	.long	0x78e
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x17
	.long	.LASF112
	.byte	0x10
	.byte	0x11
	.value	0x203
	.byte	0x8
	.long	0x7ab
	.uleb128 0xe
	.long	.LASF113
	.byte	0x11
	.value	0x206
	.byte	0x5
	.long	0x766
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x78e
	.uleb128 0x17
	.long	.LASF114
	.byte	0x18
	.byte	0x11
	.value	0x213
	.byte	0x8
	.long	0x805
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x214
	.byte	0x1a
	.long	0x805
	.byte	0
	.uleb128 0xe
	.long	.LASF116
	.byte	0x11
	.value	0x215
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0xe
	.long	.LASF117
	.byte	0x11
	.value	0x216
	.byte	0x12
	.long	0x34
	.byte	0x10
	.uleb128 0xe
	.long	.LASF118
	.byte	0x11
	.value	0x217
	.byte	0x12
	.long	0x34
	.byte	0x12
	.uleb128 0xe
	.long	.LASF119
	.byte	0x11
	.value	0x218
	.byte	0x12
	.long	0x34
	.byte	0x14
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x7b0
	.uleb128 0x17
	.long	.LASF120
	.byte	0x18
	.byte	0x11
	.value	0x21b
	.byte	0x8
	.long	0x844
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x21c
	.byte	0x19
	.long	0x844
	.byte	0
	.uleb128 0xe
	.long	.LASF116
	.byte	0x11
	.value	0x21d
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0xe
	.long	.LASF117
	.byte	0x11
	.value	0x21e
	.byte	0x12
	.long	0x34
	.byte	0x10
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x80b
	.uleb128 0x17
	.long	.LASF121
	.byte	0x18
	.byte	0x11
	.value	0x221
	.byte	0x8
	.long	0x883
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x222
	.byte	0x1a
	.long	0x883
	.byte	0
	.uleb128 0x18
	.string	"txt"
	.byte	0x11
	.value	0x223
	.byte	0x12
	.long	0x760
	.byte	0x8
	.uleb128 0xe
	.long	.LASF122
	.byte	0x11
	.value	0x224
	.byte	0xa
	.long	0xb5
	.byte	0x10
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x84a
	.uleb128 0x17
	.long	.LASF123
	.byte	0x20
	.byte	0x11
	.value	0x229
	.byte	0x8
	.long	0x8d0
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x22a
	.byte	0x18
	.long	0x8d0
	.byte	0
	.uleb128 0x18
	.string	"txt"
	.byte	0x11
	.value	0x22b
	.byte	0x12
	.long	0x760
	.byte	0x8
	.uleb128 0xe
	.long	.LASF122
	.byte	0x11
	.value	0x22c
	.byte	0xa
	.long	0xb5
	.byte	0x10
	.uleb128 0xe
	.long	.LASF124
	.byte	0x11
	.value	0x22f
	.byte	0x11
	.long	0x2d
	.byte	0x18
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x889
	.uleb128 0x17
	.long	.LASF125
	.byte	0x30
	.byte	0x11
	.value	0x232
	.byte	0x8
	.long	0x947
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x233
	.byte	0x1c
	.long	0x947
	.byte	0
	.uleb128 0xe
	.long	.LASF126
	.byte	0x11
	.value	0x234
	.byte	0x12
	.long	0x760
	.byte	0x8
	.uleb128 0xe
	.long	.LASF127
	.byte	0x11
	.value	0x235
	.byte	0x12
	.long	0x760
	.byte	0x10
	.uleb128 0xe
	.long	.LASF128
	.byte	0x11
	.value	0x236
	.byte	0x12
	.long	0x760
	.byte	0x18
	.uleb128 0xe
	.long	.LASF129
	.byte	0x11
	.value	0x237
	.byte	0x9
	.long	0xa3
	.byte	0x20
	.uleb128 0xe
	.long	.LASF130
	.byte	0x11
	.value	0x238
	.byte	0x12
	.long	0x34
	.byte	0x28
	.uleb128 0xe
	.long	.LASF131
	.byte	0x11
	.value	0x239
	.byte	0x12
	.long	0x34
	.byte	0x2a
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x8d6
	.uleb128 0x17
	.long	.LASF132
	.byte	0x28
	.byte	0x11
	.value	0x23c
	.byte	0x8
	.long	0x9be
	.uleb128 0xe
	.long	.LASF133
	.byte	0x11
	.value	0x23d
	.byte	0x9
	.long	0xa3
	.byte	0
	.uleb128 0xe
	.long	.LASF134
	.byte	0x11
	.value	0x23e
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0xe
	.long	.LASF135
	.byte	0x11
	.value	0x23f
	.byte	0x10
	.long	0x3b
	.byte	0x10
	.uleb128 0xe
	.long	.LASF136
	.byte	0x11
	.value	0x240
	.byte	0x10
	.long	0x3b
	.byte	0x14
	.uleb128 0xe
	.long	.LASF137
	.byte	0x11
	.value	0x241
	.byte	0x10
	.long	0x3b
	.byte	0x18
	.uleb128 0xe
	.long	.LASF138
	.byte	0x11
	.value	0x242
	.byte	0x10
	.long	0x3b
	.byte	0x1c
	.uleb128 0xe
	.long	.LASF139
	.byte	0x11
	.value	0x243
	.byte	0x10
	.long	0x3b
	.byte	0x20
	.byte	0
	.uleb128 0x15
	.byte	0x10
	.byte	0x11
	.value	0x2ae
	.byte	0x3
	.long	0x9e3
	.uleb128 0x16
	.long	.LASF140
	.byte	0x11
	.value	0x2af
	.byte	0x14
	.long	0x693
	.uleb128 0x16
	.long	.LASF141
	.byte	0x11
	.value	0x2b0
	.byte	0x1a
	.long	0x78e
	.byte	0
	.uleb128 0x17
	.long	.LASF142
	.byte	0x20
	.byte	0x11
	.value	0x2ab
	.byte	0x8
	.long	0xa1c
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x2ac
	.byte	0x1a
	.long	0xa1c
	.byte	0
	.uleb128 0xe
	.long	.LASF143
	.byte	0x11
	.value	0x2ad
	.byte	0x7
	.long	0x6f
	.byte	0x8
	.uleb128 0xe
	.long	.LASF144
	.byte	0x11
	.value	0x2b1
	.byte	0x5
	.long	0x9be
	.byte	0xc
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x9e3
	.uleb128 0x15
	.byte	0x10
	.byte	0x11
	.value	0x2b7
	.byte	0x3
	.long	0xa47
	.uleb128 0x16
	.long	.LASF140
	.byte	0x11
	.value	0x2b8
	.byte	0x14
	.long	0x693
	.uleb128 0x16
	.long	.LASF141
	.byte	0x11
	.value	0x2b9
	.byte	0x1a
	.long	0x78e
	.byte	0
	.uleb128 0x17
	.long	.LASF145
	.byte	0x28
	.byte	0x11
	.value	0x2b4
	.byte	0x8
	.long	0xa9c
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x2b5
	.byte	0x1f
	.long	0xa9c
	.byte	0
	.uleb128 0xe
	.long	.LASF143
	.byte	0x11
	.value	0x2b6
	.byte	0x7
	.long	0x6f
	.byte	0x8
	.uleb128 0xe
	.long	.LASF144
	.byte	0x11
	.value	0x2ba
	.byte	0x5
	.long	0xa22
	.byte	0xc
	.uleb128 0xe
	.long	.LASF146
	.byte	0x11
	.value	0x2bb
	.byte	0x7
	.long	0x6f
	.byte	0x1c
	.uleb128 0xe
	.long	.LASF147
	.byte	0x11
	.value	0x2bc
	.byte	0x7
	.long	0x6f
	.byte	0x20
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xa47
	.uleb128 0x19
	.byte	0x7
	.byte	0x4
	.long	0x3b
	.byte	0x12
	.byte	0x11
	.byte	0xe
	.long	0xaed
	.uleb128 0x1a
	.long	.LASF148
	.byte	0x1
	.uleb128 0x1a
	.long	.LASF149
	.byte	0x2
	.uleb128 0x1a
	.long	.LASF150
	.byte	0x3
	.uleb128 0x1a
	.long	.LASF151
	.byte	0x4
	.uleb128 0x1a
	.long	.LASF152
	.byte	0x5
	.uleb128 0x1a
	.long	.LASF153
	.byte	0x6
	.uleb128 0x1a
	.long	.LASF154
	.byte	0x7
	.uleb128 0x1a
	.long	.LASF155
	.byte	0x8
	.uleb128 0x1a
	.long	.LASF156
	.byte	0x9
	.uleb128 0x1a
	.long	.LASF157
	.byte	0xa
	.byte	0
	.uleb128 0x3
	.long	.LASF158
	.byte	0x12
	.byte	0x22
	.byte	0x3
	.long	0xaa2
	.uleb128 0x13
	.byte	0x30
	.byte	0x12
	.byte	0x3b
	.byte	0x3
	.long	0xb63
	.uleb128 0x14
	.long	.LASF159
	.byte	0x12
	.byte	0x3c
	.byte	0x1b
	.long	0x84a
	.uleb128 0x14
	.long	.LASF160
	.byte	0x12
	.byte	0x3d
	.byte	0x19
	.long	0x889
	.uleb128 0x14
	.long	.LASF161
	.byte	0x12
	.byte	0x3e
	.byte	0x1b
	.long	0x7b0
	.uleb128 0x14
	.long	.LASF162
	.byte	0x12
	.byte	0x3f
	.byte	0x1b
	.long	0x9e3
	.uleb128 0x14
	.long	.LASF163
	.byte	0x12
	.byte	0x40
	.byte	0x20
	.long	0xa47
	.uleb128 0x14
	.long	.LASF164
	.byte	0x12
	.byte	0x41
	.byte	0x1a
	.long	0x80b
	.uleb128 0x14
	.long	.LASF165
	.byte	0x12
	.byte	0x42
	.byte	0x1d
	.long	0x8d6
	.uleb128 0x14
	.long	.LASF166
	.byte	0x12
	.byte	0x43
	.byte	0x1b
	.long	0x94d
	.byte	0
	.uleb128 0x8
	.long	.LASF167
	.byte	0x38
	.byte	0x12
	.byte	0x38
	.byte	0x8
	.long	0xb98
	.uleb128 0x9
	.long	.LASF168
	.byte	0x12
	.byte	0x39
	.byte	0x11
	.long	0xaed
	.byte	0
	.uleb128 0x9
	.long	.LASF169
	.byte	0x12
	.byte	0x3a
	.byte	0x10
	.long	0x3b
	.byte	0x4
	.uleb128 0x9
	.long	.LASF170
	.byte	0x12
	.byte	0x44
	.byte	0x5
	.long	0xaf9
	.byte	0x8
	.byte	0
	.uleb128 0x10
	.long	.LASF171
	.byte	0x13
	.byte	0x52
	.byte	0x23
	.long	0x7ab
	.uleb128 0x1b
	.long	0xa1
	.long	0xbb3
	.uleb128 0x1c
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF172
	.byte	0x14
	.value	0x151
	.byte	0x10
	.long	0xbc0
	.uleb128 0x6
	.byte	0x8
	.long	0xba4
	.uleb128 0x1b
	.long	0xa1
	.long	0xbda
	.uleb128 0x1c
	.long	0xa1
	.uleb128 0x1c
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF173
	.byte	0x14
	.value	0x152
	.byte	0x10
	.long	0xbe7
	.uleb128 0x6
	.byte	0x8
	.long	0xbc6
	.uleb128 0x1d
	.long	0xbf8
	.uleb128 0x1c
	.long	0xa1
	.byte	0
	.uleb128 0x12
	.long	.LASF174
	.byte	0x14
	.value	0x153
	.byte	0xf
	.long	0xc05
	.uleb128 0x6
	.byte	0x8
	.long	0xbed
	.uleb128 0x1e
	.long	.LASF184
	.byte	0x1
	.byte	0x8f
	.byte	0x7
	.long	0xa1
	.quad	.LFB88
	.quad	.LFE88-.LFB88
	.uleb128 0x1
	.byte	0x9c
	.long	0xce8
	.uleb128 0x1f
	.long	.LASF168
	.byte	0x1
	.byte	0x8f
	.byte	0x26
	.long	0xaed
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x20
	.string	"ptr"
	.byte	0x1
	.byte	0x91
	.byte	0x15
	.long	0xce8
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x21
	.long	0xd73
	.quad	.LBI13
	.byte	.LVU118
	.long	.Ldebug_ranges0+0x80
	.byte	0x1
	.byte	0xb4
	.byte	0x9
	.long	0xc96
	.uleb128 0x22
	.long	0xd98
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x22
	.long	0xd8c
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x22
	.long	0xd80
	.long	.LLST7
	.long	.LVUS7
	.byte	0
	.uleb128 0x21
	.long	0xd73
	.quad	.LBI17
	.byte	.LVU163
	.long	.Ldebug_ranges0+0xb0
	.byte	0x1
	.byte	0xbd
	.byte	0x9
	.long	0xcd7
	.uleb128 0x22
	.long	0xd98
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x22
	.long	0xd8c
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x22
	.long	0xd80
	.long	.LLST10
	.long	.LVUS10
	.byte	0
	.uleb128 0x23
	.quad	.LVL31
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8
	.byte	0x38
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb63
	.uleb128 0x25
	.long	.LASF185
	.byte	0x1
	.byte	0x29
	.byte	0x6
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0xd73
	.uleb128 0x1f
	.long	.LASF175
	.byte	0x1
	.byte	0x29
	.byte	0x1b
	.long	0xa1
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x26
	.long	.Ldebug_ranges0+0
	.uleb128 0x20
	.string	"ptr"
	.byte	0x1
	.byte	0x2c
	.byte	0x17
	.long	0xce8
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x27
	.long	.LASF176
	.byte	0x1
	.byte	0x2d
	.byte	0xb
	.long	0xa1
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x28
	.quad	.LVL6
	.long	0xd61
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.uleb128 0x23
	.quad	.LVL13
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x29
	.long	.LASF186
	.byte	0x2
	.byte	0x3b
	.byte	0x2a
	.long	0xa1
	.byte	0x3
	.uleb128 0x2a
	.long	.LASF177
	.byte	0x2
	.byte	0x3b
	.byte	0x38
	.long	0xa1
	.uleb128 0x2a
	.long	.LASF178
	.byte	0x2
	.byte	0x3b
	.byte	0x44
	.long	0x6f
	.uleb128 0x2a
	.long	.LASF179
	.byte	0x2
	.byte	0x3b
	.byte	0x51
	.long	0xb5
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS3:
	.uleb128 0
	.uleb128 .LVU79
	.uleb128 .LVU79
	.uleb128 .LVU99
	.uleb128 .LVU99
	.uleb128 .LVU100
	.uleb128 .LVU100
	.uleb128 .LVU172
	.uleb128 .LVU172
	.uleb128 0
.LLST3:
	.quad	.LVL29-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL30-.Ltext0
	.quad	.LVL34-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL34-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL35-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL41-.Ltext0
	.quad	.LFE88-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU81
	.uleb128 .LVU96
	.uleb128 .LVU96
	.uleb128 .LVU98
	.uleb128 .LVU100
	.uleb128 .LVU168
.LLST4:
	.quad	.LVL31-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL32-.Ltext0
	.quad	.LVL33-.Ltext0
	.value	0x3
	.byte	0x70
	.sleb128 -8
	.byte	0x9f
	.quad	.LVL35-.Ltext0
	.quad	.LVL40-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU118
	.uleb128 .LVU121
.LLST5:
	.quad	.LVL36-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x2
	.byte	0x40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU118
	.uleb128 .LVU121
.LLST6:
	.quad	.LVL36-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU118
	.uleb128 .LVU121
.LLST7:
	.quad	.LVL36-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x3
	.byte	0x70
	.sleb128 20
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU163
	.uleb128 .LVU166
.LLST8:
	.quad	.LVL38-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x2
	.byte	0x40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU163
	.uleb128 .LVU166
.LLST9:
	.quad	.LVL38-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU163
	.uleb128 .LVU166
.LLST10:
	.quad	.LVL38-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x3
	.byte	0x70
	.sleb128 20
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 0
	.uleb128 .LVU5
	.uleb128 .LVU5
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU20
	.uleb128 .LVU20
	.uleb128 .LVU23
	.uleb128 .LVU25
	.uleb128 .LVU27
	.uleb128 .LVU27
	.uleb128 .LVU30
	.uleb128 .LVU30
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU51
	.uleb128 .LVU51
	.uleb128 .LVU60
	.uleb128 .LVU60
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU68
	.uleb128 .LVU68
	.uleb128 .LVU70
	.uleb128 .LVU70
	.uleb128 .LVU72
	.uleb128 .LVU72
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL1-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL4-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x3
	.byte	0x7d
	.sleb128 8
	.byte	0x9f
	.quad	.LVL6-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL10-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL11-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x3
	.byte	0x7d
	.sleb128 8
	.byte	0x9f
	.quad	.LVL13-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL19-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x3
	.byte	0x7d
	.sleb128 8
	.byte	0x9f
	.quad	.LVL21-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL23-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x3
	.byte	0x7d
	.sleb128 8
	.byte	0x9f
	.quad	.LVL24-.Ltext0
	.quad	.LVL25-1-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 8
	.byte	0x9f
	.quad	.LVL25-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL26-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x3
	.byte	0x7d
	.sleb128 8
	.byte	0x9f
	.quad	.LVL27-.Ltext0
	.quad	.LVL28-1-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 8
	.byte	0x9f
	.quad	.LVL28-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 .LVU10
	.uleb128 .LVU24
	.uleb128 .LVU25
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU70
	.uleb128 .LVU70
	.uleb128 .LVU72
.LLST1:
	.quad	.LVL2-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL10-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL24-.Ltext0
	.quad	.LVL25-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL25-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL27-.Ltext0
	.quad	.LVL28-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 .LVU7
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU22
	.uleb128 .LVU25
	.uleb128 .LVU28
	.uleb128 .LVU28
	.uleb128 .LVU33
	.uleb128 .LVU33
	.uleb128 .LVU36
	.uleb128 .LVU36
	.uleb128 .LVU50
	.uleb128 .LVU50
	.uleb128 .LVU51
	.uleb128 .LVU51
	.uleb128 .LVU72
.LLST2:
	.quad	.LVL1-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL3-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x2
	.byte	0x73
	.sleb128 0
	.quad	.LVL4-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL14-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL15-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL20-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL21-.Ltext0
	.quad	.LVL28-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB6-.Ltext0
	.quad	.LBE6-.Ltext0
	.quad	.LBB7-.Ltext0
	.quad	.LBE7-.Ltext0
	.quad	.LBB8-.Ltext0
	.quad	.LBE8-.Ltext0
	.quad	.LBB9-.Ltext0
	.quad	.LBE9-.Ltext0
	.quad	.LBB10-.Ltext0
	.quad	.LBE10-.Ltext0
	.quad	.LBB11-.Ltext0
	.quad	.LBE11-.Ltext0
	.quad	.LBB12-.Ltext0
	.quad	.LBE12-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB13-.Ltext0
	.quad	.LBE13-.Ltext0
	.quad	.LBB16-.Ltext0
	.quad	.LBE16-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB17-.Ltext0
	.quad	.LBE17-.Ltext0
	.quad	.LBB20-.Ltext0
	.quad	.LBE20-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF43:
	.string	"_IO_read_ptr"
.LASF55:
	.string	"_chain"
.LASF127:
	.string	"service"
.LASF33:
	.string	"sin6_addr"
.LASF108:
	.string	"__in6_u"
.LASF13:
	.string	"size_t"
.LASF61:
	.string	"_shortbuf"
.LASF112:
	.string	"ares_in6_addr"
.LASF161:
	.string	"srv_reply"
.LASF139:
	.string	"minttl"
.LASF6:
	.string	"__uint8_t"
.LASF168:
	.string	"type"
.LASF178:
	.string	"__ch"
.LASF49:
	.string	"_IO_buf_base"
.LASF14:
	.string	"long long unsigned int"
.LASF100:
	.string	"in_addr_t"
.LASF144:
	.string	"addr"
.LASF130:
	.string	"order"
.LASF154:
	.string	"ARES_DATATYPE_NAPTR_REPLY"
.LASF115:
	.string	"next"
.LASF64:
	.string	"_codecvt"
.LASF86:
	.string	"__timezone"
.LASF148:
	.string	"ARES_DATATYPE_UNKNOWN"
.LASF15:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF169:
	.string	"mark"
.LASF35:
	.string	"sockaddr_inarp"
.LASF56:
	.string	"_fileno"
.LASF44:
	.string	"_IO_read_end"
.LASF174:
	.string	"ares_free"
.LASF105:
	.string	"__u6_addr16"
.LASF9:
	.string	"long int"
.LASF106:
	.string	"__u6_addr32"
.LASF42:
	.string	"_flags"
.LASF65:
	.string	"_wide_data"
.LASF121:
	.string	"ares_txt_reply"
.LASF59:
	.string	"_cur_column"
.LASF83:
	.string	"program_invocation_short_name"
.LASF73:
	.string	"_IO_codecvt"
.LASF21:
	.string	"sockaddr_dl"
.LASF31:
	.string	"sin6_port"
.LASF136:
	.string	"refresh"
.LASF81:
	.string	"_sys_errlist"
.LASF82:
	.string	"program_invocation_name"
.LASF58:
	.string	"_old_offset"
.LASF63:
	.string	"_offset"
.LASF110:
	.string	"in6addr_loopback"
.LASF133:
	.string	"nsname"
.LASF40:
	.string	"sockaddr_x25"
.LASF36:
	.string	"sockaddr_ipx"
.LASF8:
	.string	"__uint32_t"
.LASF89:
	.string	"timezone"
.LASF164:
	.string	"mx_reply"
.LASF28:
	.string	"sin_zero"
.LASF68:
	.string	"__pad5"
.LASF119:
	.string	"port"
.LASF150:
	.string	"ARES_DATATYPE_TXT_REPLY"
.LASF72:
	.string	"_IO_marker"
.LASF75:
	.string	"stdin"
.LASF2:
	.string	"unsigned int"
.LASF102:
	.string	"s_addr"
.LASF181:
	.string	"../deps/cares/src/ares_data.c"
.LASF98:
	.string	"uint16_t"
.LASF3:
	.string	"long unsigned int"
.LASF146:
	.string	"udp_port"
.LASF47:
	.string	"_IO_write_ptr"
.LASF172:
	.string	"ares_malloc"
.LASF88:
	.string	"daylight"
.LASF78:
	.string	"sys_nerr"
.LASF124:
	.string	"record_start"
.LASF170:
	.string	"data"
.LASF1:
	.string	"short unsigned int"
.LASF27:
	.string	"sin_addr"
.LASF138:
	.string	"expire"
.LASF113:
	.string	"_S6_un"
.LASF132:
	.string	"ares_soa_reply"
.LASF182:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF51:
	.string	"_IO_save_base"
.LASF165:
	.string	"naptr_reply"
.LASF134:
	.string	"hostmaster"
.LASF156:
	.string	"ARES_DATATYPE_ADDR_PORT_NODE"
.LASF117:
	.string	"priority"
.LASF92:
	.string	"environ"
.LASF140:
	.string	"addr4"
.LASF141:
	.string	"addr6"
.LASF62:
	.string	"_lock"
.LASF184:
	.string	"ares_malloc_data"
.LASF167:
	.string	"ares_data"
.LASF57:
	.string	"_flags2"
.LASF69:
	.string	"_mode"
.LASF76:
	.string	"stdout"
.LASF131:
	.string	"preference"
.LASF39:
	.string	"sockaddr_un"
.LASF25:
	.string	"sin_family"
.LASF151:
	.string	"ARES_DATATYPE_TXT_EXT"
.LASF93:
	.string	"optarg"
.LASF155:
	.string	"ARES_DATATYPE_SOA_REPLY"
.LASF90:
	.string	"getdate_err"
.LASF30:
	.string	"sin6_family"
.LASF94:
	.string	"optind"
.LASF48:
	.string	"_IO_write_end"
.LASF125:
	.string	"ares_naptr_reply"
.LASF177:
	.string	"__dest"
.LASF137:
	.string	"retry"
.LASF183:
	.string	"_IO_lock_t"
.LASF109:
	.string	"in6addr_any"
.LASF41:
	.string	"_IO_FILE"
.LASF176:
	.string	"next_data"
.LASF142:
	.string	"ares_addr_node"
.LASF91:
	.string	"__environ"
.LASF85:
	.string	"__daylight"
.LASF77:
	.string	"stderr"
.LASF38:
	.string	"sockaddr_ns"
.LASF26:
	.string	"sin_port"
.LASF17:
	.string	"sa_family"
.LASF79:
	.string	"sys_errlist"
.LASF114:
	.string	"ares_srv_reply"
.LASF54:
	.string	"_markers"
.LASF120:
	.string	"ares_mx_reply"
.LASF153:
	.string	"ARES_DATATYPE_MX_REPLY"
.LASF34:
	.string	"sin6_scope_id"
.LASF143:
	.string	"family"
.LASF0:
	.string	"unsigned char"
.LASF37:
	.string	"sockaddr_iso"
.LASF50:
	.string	"_IO_buf_end"
.LASF5:
	.string	"short int"
.LASF186:
	.string	"memset"
.LASF179:
	.string	"__len"
.LASF80:
	.string	"_sys_nerr"
.LASF60:
	.string	"_vtable_offset"
.LASF87:
	.string	"tzname"
.LASF20:
	.string	"sockaddr_ax25"
.LASF71:
	.string	"FILE"
.LASF162:
	.string	"addr_node"
.LASF107:
	.string	"in6_addr"
.LASF96:
	.string	"optopt"
.LASF99:
	.string	"uint32_t"
.LASF157:
	.string	"ARES_DATATYPE_LAST"
.LASF163:
	.string	"addr_port_node"
.LASF147:
	.string	"tcp_port"
.LASF122:
	.string	"length"
.LASF12:
	.string	"char"
.LASF128:
	.string	"regexp"
.LASF175:
	.string	"dataptr"
.LASF32:
	.string	"sin6_flowinfo"
.LASF7:
	.string	"__uint16_t"
.LASF116:
	.string	"host"
.LASF104:
	.string	"__u6_addr8"
.LASF158:
	.string	"ares_datatype"
.LASF95:
	.string	"opterr"
.LASF11:
	.string	"__off64_t"
.LASF45:
	.string	"_IO_read_base"
.LASF53:
	.string	"_IO_save_end"
.LASF185:
	.string	"ares_free_data"
.LASF159:
	.string	"txt_reply"
.LASF22:
	.string	"sockaddr_eon"
.LASF166:
	.string	"soa_reply"
.LASF19:
	.string	"sockaddr_at"
.LASF173:
	.string	"ares_realloc"
.LASF16:
	.string	"sa_family_t"
.LASF70:
	.string	"_unused2"
.LASF111:
	.string	"_S6_u8"
.LASF123:
	.string	"ares_txt_ext"
.LASF29:
	.string	"sockaddr_in6"
.LASF23:
	.string	"sockaddr"
.LASF24:
	.string	"sockaddr_in"
.LASF67:
	.string	"_freeres_buf"
.LASF97:
	.string	"uint8_t"
.LASF52:
	.string	"_IO_backup_base"
.LASF126:
	.string	"flags"
.LASF135:
	.string	"serial"
.LASF145:
	.string	"ares_addr_port_node"
.LASF152:
	.string	"ARES_DATATYPE_ADDR_NODE"
.LASF149:
	.string	"ARES_DATATYPE_SRV_REPLY"
.LASF18:
	.string	"sa_data"
.LASF66:
	.string	"_freeres_list"
.LASF180:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF74:
	.string	"_IO_wide_data"
.LASF160:
	.string	"txt_ext"
.LASF171:
	.string	"ares_in6addr_any"
.LASF84:
	.string	"__tzname"
.LASF46:
	.string	"_IO_write_base"
.LASF103:
	.string	"in_port_t"
.LASF129:
	.string	"replacement"
.LASF101:
	.string	"in_addr"
.LASF118:
	.string	"weight"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
