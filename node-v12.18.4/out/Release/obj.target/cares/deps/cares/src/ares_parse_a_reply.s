	.file	"ares_parse_a_reply.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares_parse_a_reply
	.type	ares_parse_a_reply, @function
ares_parse_a_reply:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares_parse_a_reply.c"
	.loc 1 53 1 view -0
	.cfi_startproc
	.loc 1 53 1 is_stmt 0 view .LVU1
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
.LBB8:
.LBB9:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 71 10 view .LVU2
	pxor	%xmm0, %xmm0
.LBE9:
.LBE8:
	.loc 1 53 1 view .LVU3
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 53 1 view .LVU4
	movq	%rdx, -136(%rbp)
	.loc 1 67 12 view .LVU5
	leaq	-88(%rbp), %rdx
.LVL1:
	.loc 1 53 1 view .LVU6
	movq	%rcx, -144(%rbp)
.LBB12:
.LBB10:
	.loc 2 71 10 view .LVU7
	leaq	-80(%rbp), %rcx
.LVL2:
	.loc 2 71 10 view .LVU8
.LBE10:
.LBE12:
	.loc 1 53 1 view .LVU9
	movq	%r8, -128(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	.loc 1 54 3 is_stmt 1 view .LVU10
	.loc 1 55 3 view .LVU11
	.loc 1 56 3 view .LVU12
	.loc 1 57 3 view .LVU13
.LVL3:
	.loc 1 58 3 view .LVU14
	.loc 1 58 9 is_stmt 0 view .LVU15
	movq	$0, -88(%rbp)
	.loc 1 59 3 is_stmt 1 view .LVU16
.LVL4:
	.loc 1 60 3 view .LVU17
	.loc 1 61 3 view .LVU18
	.loc 1 62 3 view .LVU19
	.loc 1 63 3 view .LVU20
	.loc 1 65 3 view .LVU21
.LBB13:
.LBI8:
	.loc 2 59 42 view .LVU22
.LBB11:
	.loc 2 71 3 view .LVU23
	.loc 2 71 10 is_stmt 0 view .LVU24
	movaps	%xmm0, -80(%rbp)
.LVL5:
	.loc 2 71 10 view .LVU25
.LBE11:
.LBE13:
	.loc 1 67 3 is_stmt 1 view .LVU26
	.loc 1 67 12 is_stmt 0 view .LVU27
	call	ares__parse_into_addrinfo2@PLT
.LVL6:
	.loc 1 67 12 view .LVU28
	movl	%eax, -116(%rbp)
.LVL7:
	.loc 1 68 3 is_stmt 1 view .LVU29
	.loc 1 68 6 is_stmt 0 view .LVU30
	testl	%eax, %eax
	jne	.L78
	.loc 1 80 3 is_stmt 1 view .LVU31
	.loc 1 80 13 is_stmt 0 view .LVU32
	movl	$32, %edi
	call	*ares_malloc(%rip)
.LVL8:
	.loc 1 80 13 view .LVU33
	movq	%rax, %r12
.LVL9:
	.loc 1 81 3 is_stmt 1 view .LVU34
	.loc 1 81 6 is_stmt 0 view .LVU35
	testq	%rax, %rax
	je	.L4
	.loc 1 86 3 is_stmt 1 view .LVU36
	.loc 1 86 8 is_stmt 0 view .LVU37
	movq	-72(%rbp), %rax
.LVL10:
	.loc 1 87 3 is_stmt 1 view .LVU38
	.loc 1 87 9 view .LVU39
	.loc 1 61 21 is_stmt 0 view .LVU40
	xorl	%r13d, %r13d
	.loc 1 87 9 view .LVU41
	testq	%rax, %rax
	je	.L5
.LVL11:
	.p2align 4,,10
	.p2align 3
.L7:
	.loc 1 89 7 is_stmt 1 view .LVU42
	.loc 1 91 11 is_stmt 0 view .LVU43
	xorl	%edx, %edx
	cmpl	$2, 8(%rax)
	.loc 1 93 12 view .LVU44
	movq	32(%rax), %rax
.LVL12:
	.loc 1 91 11 view .LVU45
	sete	%dl
	addl	%edx, %r13d
.LVL13:
	.loc 1 93 7 is_stmt 1 view .LVU46
	.loc 1 87 9 view .LVU47
	testq	%rax, %rax
	jne	.L7
.LVL14:
.L5:
	.loc 1 96 3 view .LVU48
	.loc 1 96 14 is_stmt 0 view .LVU49
	movq	-80(%rbp), %rax
.LVL15:
	.loc 1 97 3 is_stmt 1 view .LVU50
	.loc 1 97 9 view .LVU51
	movq	ares_malloc(%rip), %rsi
	testq	%rax, %rax
	je	.L8
	.loc 1 61 7 is_stmt 0 view .LVU52
	xorl	%r15d, %r15d
.LVL16:
	.p2align 4,,10
	.p2align 3
.L10:
	.loc 1 99 7 is_stmt 1 view .LVU53
	leal	1(%r15), %edx
	.loc 1 99 9 is_stmt 0 view .LVU54
	cmpq	$0, 8(%rax)
	movl	%edx, %edi
	je	.L9
	.loc 1 100 9 is_stmt 1 view .LVU55
.LVL17:
	.loc 1 100 9 is_stmt 0 view .LVU56
	leal	2(%r15), %edi
	.loc 1 99 9 view .LVU57
	movl	%edx, %r15d
.LVL18:
.L9:
	.loc 1 101 7 is_stmt 1 view .LVU58
	.loc 1 101 18 is_stmt 0 view .LVU59
	movq	24(%rax), %rax
.LVL19:
	.loc 1 97 9 is_stmt 1 view .LVU60
	testq	%rax, %rax
	jne	.L10
	.loc 1 104 3 view .LVU61
	.loc 1 104 35 is_stmt 0 view .LVU62
	movslq	%edi, %rdi
	.loc 1 104 13 view .LVU63
	salq	$3, %rdi
	call	*%rsi
.LVL20:
	.loc 1 104 13 view .LVU64
	movq	%rax, %rbx
.LVL21:
	.loc 1 105 3 is_stmt 1 view .LVU65
	.loc 1 105 6 is_stmt 0 view .LVU66
	testq	%rax, %rax
	je	.L4
	.loc 1 110 3 is_stmt 1 view .LVU67
	.loc 1 110 6 is_stmt 0 view .LVU68
	testl	%r15d, %r15d
	je	.L39
	.loc 1 112 7 is_stmt 1 view .LVU69
	.loc 1 112 18 is_stmt 0 view .LVU70
	movq	-80(%rbp), %r15
.LVL22:
	.loc 1 113 7 is_stmt 1 view .LVU71
	.loc 1 113 13 view .LVU72
	testq	%r15, %r15
	je	.L39
	.loc 1 61 33 is_stmt 0 view .LVU73
	xorl	%r10d, %r10d
	.loc 1 62 7 view .LVU74
	movl	$2147483647, %r14d
	xorl	%eax, %eax
.LVL23:
	.p2align 4,,10
	.p2align 3
.L14:
	.loc 1 115 11 is_stmt 1 view .LVU75
	.loc 1 115 24 is_stmt 0 view .LVU76
	movq	8(%r15), %rdi
	leaq	0(,%rax,8), %rdx
	movq	%rdx, -104(%rbp)
	leaq	(%rbx,%rdx), %rsi
	.loc 1 115 13 view .LVU77
	testq	%rdi, %rdi
	je	.L12
	.loc 1 116 26 view .LVU78
	addl	$1, %r10d
.LVL24:
	.loc 1 116 26 view .LVU79
	movq	%rsi, -112(%rbp)
	.loc 1 116 13 is_stmt 1 view .LVU80
.LVL25:
	.loc 1 116 26 is_stmt 0 view .LVU81
	movl	%r10d, -120(%rbp)
	.loc 1 116 32 view .LVU82
	call	strdup@PLT
.LVL26:
	.loc 1 116 30 view .LVU83
	movq	-112(%rbp), %rsi
	movq	-104(%rbp), %rdx
	movq	%rax, (%rsi)
	movl	(%r15), %eax
	leaq	8(%rbx,%rdx), %rsi
	.loc 1 117 11 is_stmt 1 view .LVU84
	.loc 1 119 22 is_stmt 0 view .LVU85
	movq	24(%r15), %r15
.LVL27:
	.loc 1 119 22 view .LVU86
	cmpl	%eax, %r14d
	cmovg	%eax, %r14d
.LVL28:
	.loc 1 119 11 is_stmt 1 view .LVU87
	.loc 1 113 13 view .LVU88
	testq	%r15, %r15
	je	.L11
	movslq	-120(%rbp), %rax
	movq	%rax, %r10
	jmp	.L14
.LVL29:
	.p2align 4,,10
	.p2align 3
.L12:
	.loc 1 117 11 view .LVU89
	movl	(%r15), %edx
	.loc 1 119 22 is_stmt 0 view .LVU90
	movq	24(%r15), %r15
.LVL30:
	.loc 1 119 22 view .LVU91
	cmpl	%edx, %r14d
	cmovg	%edx, %r14d
.LVL31:
	.loc 1 119 11 is_stmt 1 view .LVU92
	.loc 1 113 13 view .LVU93
	testq	%r15, %r15
	jne	.L14
.LVL32:
.L11:
	.loc 1 123 3 view .LVU94
	.loc 1 125 46 is_stmt 0 view .LVU95
	leal	1(%r13), %edi
	.loc 1 123 18 view .LVU96
	movq	$0, (%rsi)
	.loc 1 125 3 is_stmt 1 view .LVU97
	.loc 1 125 46 is_stmt 0 view .LVU98
	movslq	%edi, %rdi
	.loc 1 125 26 view .LVU99
	salq	$3, %rdi
	call	*ares_malloc(%rip)
.LVL33:
	.loc 1 125 24 view .LVU100
	movq	%rax, 24(%r12)
	.loc 1 126 3 is_stmt 1 view .LVU101
	.loc 1 125 26 is_stmt 0 view .LVU102
	movq	%rax, %rdi
	.loc 1 126 6 view .LVU103
	testq	%rax, %rax
	je	.L15
	.loc 1 133 31 view .LVU104
	movslq	%r13d, %r15
	xorl	%esi, %esi
	leaq	8(,%r15,8), %rdx
	call	memset@PLT
.LVL34:
	.loc 1 136 3 is_stmt 1 view .LVU105
	.loc 1 136 9 is_stmt 0 view .LVU106
	movq	-80(%rbp), %rax
	movq	-88(%rbp), %r10
	.loc 1 136 6 view .LVU107
	testq	%rax, %rax
	je	.L16
	.loc 1 138 25 view .LVU108
	movq	16(%rax), %rdi
	movq	%r10, -104(%rbp)
	.loc 1 138 7 is_stmt 1 view .LVU109
	.loc 1 138 25 is_stmt 0 view .LVU110
	call	strdup@PLT
.LVL35:
	.loc 1 139 7 view .LVU111
	movq	-104(%rbp), %r10
	.loc 1 138 23 view .LVU112
	movq	%rax, (%r12)
	.loc 1 139 7 is_stmt 1 view .LVU113
	movq	%r10, %rdi
	call	*ares_free(%rip)
.LVL36:
.L17:
	.loc 1 146 3 view .LVU114
	.loc 1 147 23 is_stmt 0 view .LVU115
	movabsq	$17179869186, %rax
	.loc 1 146 22 view .LVU116
	movq	%rbx, 8(%r12)
	.loc 1 147 3 is_stmt 1 view .LVU117
	.loc 1 148 3 view .LVU118
	.loc 1 147 23 is_stmt 0 view .LVU119
	movq	%rax, 16(%r12)
	.loc 1 150 3 is_stmt 1 view .LVU120
	.loc 1 150 6 is_stmt 0 view .LVU121
	testl	%r13d, %r13d
	je	.L19
	.loc 1 152 7 is_stmt 1 view .LVU122
	.loc 1 152 15 is_stmt 0 view .LVU123
	leaq	0(,%r15,4), %rdi
	call	*ares_malloc(%rip)
.LVL37:
	movq	%rax, %rdi
.LVL38:
	.loc 1 153 7 is_stmt 1 view .LVU124
	.loc 1 153 10 is_stmt 0 view .LVU125
	testq	%rax, %rax
	je	.L15
	.loc 1 158 7 is_stmt 1 view .LVU126
.LVL39:
	.loc 1 159 7 view .LVU127
	.loc 1 159 12 is_stmt 0 view .LVU128
	movq	-72(%rbp), %rdx
.LVL40:
	.loc 1 160 7 is_stmt 1 view .LVU129
	.loc 1 160 13 view .LVU130
	testq	%rdx, %rdx
	je	.L31
	.loc 1 158 9 is_stmt 0 view .LVU131
	xorl	%esi, %esi
	cmpq	$0, -128(%rbp)
	jne	.L21
	jmp	.L26
.LVL41:
	.p2align 4,,10
	.p2align 3
.L27:
	.loc 1 181 11 is_stmt 1 view .LVU132
	.loc 1 181 16 is_stmt 0 view .LVU133
	movq	32(%rdx), %rdx
.LVL42:
	.loc 1 160 13 is_stmt 1 view .LVU134
	testq	%rdx, %rdx
	je	.L25
.LVL43:
.L21:
	.loc 1 162 11 view .LVU135
	.loc 1 162 14 is_stmt 0 view .LVU136
	cmpl	$2, 8(%rdx)
	jne	.L27
	.loc 1 164 15 is_stmt 1 view .LVU137
	.loc 1 164 55 is_stmt 0 view .LVU138
	movslq	%esi, %rax
	.loc 1 164 39 view .LVU139
	movq	24(%r12), %r11
	.loc 1 164 49 view .LVU140
	leaq	(%rdi,%rax,4), %r9
	.loc 1 164 35 view .LVU141
	leaq	0(,%rax,8), %r10
	.loc 1 164 39 view .LVU142
	movq	%r9, (%r11,%rax,8)
	.loc 1 165 15 is_stmt 1 view .LVU143
.LVL44:
.LBB14:
.LBI14:
	.loc 2 31 42 view .LVU144
.LBB15:
	.loc 2 34 3 view .LVU145
	movq	24(%rdx), %rax
	movl	4(%rax), %eax
	.loc 2 34 10 is_stmt 0 view .LVU146
	movl	%eax, (%r9)
.LVL45:
	.loc 2 34 10 view .LVU147
.LBE15:
.LBE14:
	.loc 1 168 15 is_stmt 1 view .LVU148
	.loc 1 168 29 is_stmt 0 view .LVU149
	movq	-128(%rbp), %rax
	cmpl	%esi, (%rax)
	jle	.L28
	.loc 1 170 19 is_stmt 1 view .LVU150
	.loc 1 170 27 is_stmt 0 view .LVU151
	movl	(%rdx), %eax
	addq	-144(%rbp), %r10
	.loc 1 171 37 view .LVU152
	cmpl	%r14d, %eax
	cmovg	%r14d, %eax
	movl	%eax, 4(%r10)
	.loc 1 175 19 is_stmt 1 view .LVU153
.LVL46:
.LBB17:
.LBI17:
	.loc 2 31 42 view .LVU154
.LBB18:
	.loc 2 34 3 view .LVU155
	movq	24(%rdx), %rax
	movl	4(%rax), %eax
	.loc 2 34 10 is_stmt 0 view .LVU156
	movl	%eax, (%r10)
.LVL47:
.L28:
	.loc 2 34 10 view .LVU157
.LBE18:
.LBE17:
	.loc 1 179 15 is_stmt 1 view .LVU158
	.loc 1 181 16 is_stmt 0 view .LVU159
	movq	32(%rdx), %rdx
.LVL48:
	.loc 1 179 15 view .LVU160
	addl	$1, %esi
.LVL49:
	.loc 1 181 11 is_stmt 1 view .LVU161
	.loc 1 160 13 view .LVU162
	testq	%rdx, %rdx
	jne	.L21
	.p2align 4,,10
	.p2align 3
.L25:
	.loc 1 183 7 view .LVU163
	.loc 1 183 10 is_stmt 0 view .LVU164
	testl	%esi, %esi
	jne	.L19
.LVL50:
.L31:
	.loc 1 185 11 is_stmt 1 view .LVU165
	call	*ares_free(%rip)
.LVL51:
.L19:
	.loc 1 189 3 view .LVU166
	.loc 1 189 6 is_stmt 0 view .LVU167
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L32
	.loc 1 191 7 is_stmt 1 view .LVU168
	.loc 1 191 13 is_stmt 0 view .LVU169
	movq	%r12, (%rax)
.L33:
	.loc 1 198 3 is_stmt 1 view .LVU170
	.loc 1 198 6 is_stmt 0 view .LVU171
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L34
	.loc 1 200 7 is_stmt 1 view .LVU172
	.loc 1 200 18 is_stmt 0 view .LVU173
	movl	%r13d, (%rax)
.L34:
	.loc 1 203 3 is_stmt 1 view .LVU174
	movq	-80(%rbp), %rdi
	call	ares__freeaddrinfo_cnames@PLT
.LVL52:
	.loc 1 204 3 view .LVU175
	movq	-72(%rbp), %rdi
	call	ares__freeaddrinfo_nodes@PLT
.LVL53:
	.loc 1 205 3 view .LVU176
.L1:
	.loc 1 214 1 is_stmt 0 view .LVU177
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L79
	movl	-116(%rbp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
.LVL54:
	.loc 1 214 1 view .LVU178
	ret
.LVL55:
	.p2align 4,,10
	.p2align 3
.L78:
	.cfi_restore_state
	.loc 1 70 7 is_stmt 1 view .LVU179
	movq	-88(%rbp), %rdi
	call	*ares_free(%rip)
.LVL56:
	.loc 1 72 7 view .LVU180
	.loc 1 72 10 is_stmt 0 view .LVU181
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L1
	.loc 1 74 11 is_stmt 1 view .LVU182
	.loc 1 74 22 is_stmt 0 view .LVU183
	movl	$0, (%rax)
	jmp	.L1
.LVL57:
	.p2align 4,,10
	.p2align 3
.L8:
	.loc 1 104 3 is_stmt 1 view .LVU184
	.loc 1 104 13 is_stmt 0 view .LVU185
	movl	$8, %edi
	call	*%rsi
.LVL58:
	.loc 1 104 13 view .LVU186
	movq	%rax, %rbx
.LVL59:
	.loc 1 105 3 is_stmt 1 view .LVU187
	.loc 1 105 6 is_stmt 0 view .LVU188
	testq	%rax, %rax
	je	.L4
.LVL60:
	.p2align 4,,10
	.p2align 3
.L39:
	.loc 1 113 13 view .LVU189
	movq	%rbx, %rsi
	.loc 1 62 7 view .LVU190
	movl	$2147483647, %r14d
	jmp	.L11
.LVL61:
	.p2align 4,,10
	.p2align 3
.L16:
	.loc 1 143 7 is_stmt 1 view .LVU191
	.loc 1 143 23 is_stmt 0 view .LVU192
	movq	%r10, (%r12)
	jmp	.L17
.LVL62:
	.p2align 4,,10
	.p2align 3
.L4:
	.loc 1 57 10 view .LVU193
	xorl	%ebx, %ebx
.LVL63:
.L15:
	.loc 1 208 3 is_stmt 1 view .LVU194
	movq	%rbx, %rdi
	call	*ares_free(%rip)
.LVL64:
	.loc 1 209 3 view .LVU195
	movq	%r12, %rdi
	call	*ares_free(%rip)
.LVL65:
	.loc 1 210 3 view .LVU196
	movq	-80(%rbp), %rdi
	call	ares__freeaddrinfo_cnames@PLT
.LVL66:
	.loc 1 211 3 view .LVU197
	movq	-72(%rbp), %rdi
	call	ares__freeaddrinfo_nodes@PLT
.LVL67:
	.loc 1 212 3 view .LVU198
	movq	-88(%rbp), %rdi
	call	*ares_free(%rip)
.LVL68:
	.loc 1 213 3 view .LVU199
	.loc 1 213 10 is_stmt 0 view .LVU200
	movl	$15, -116(%rbp)
.LVL69:
	.loc 1 213 10 view .LVU201
	jmp	.L1
.LVL70:
	.p2align 4,,10
	.p2align 3
.L80:
	.loc 1 164 15 is_stmt 1 view .LVU202
	.loc 1 164 55 is_stmt 0 view .LVU203
	movslq	%esi, %r8
	.loc 1 164 39 view .LVU204
	movq	24(%r12), %r9
	.loc 1 179 15 view .LVU205
	addl	$1, %esi
.LVL71:
	.loc 1 164 49 view .LVU206
	leaq	(%rdi,%r8,4), %rax
	.loc 1 164 39 view .LVU207
	movq	%rax, (%r9,%r8,8)
	.loc 1 165 15 is_stmt 1 view .LVU208
.LVL72:
.LBB19:
	.loc 2 31 42 view .LVU209
.LBB16:
	.loc 2 34 3 view .LVU210
	movq	24(%rdx), %r8
	movl	4(%r8), %r8d
	.loc 2 34 10 is_stmt 0 view .LVU211
	movl	%r8d, (%rax)
.LVL73:
	.loc 2 34 10 view .LVU212
.LBE16:
.LBE19:
	.loc 1 168 15 is_stmt 1 view .LVU213
	.loc 1 179 15 view .LVU214
	.loc 1 181 11 view .LVU215
	.loc 1 181 16 is_stmt 0 view .LVU216
	movq	32(%rdx), %rdx
.LVL74:
	.loc 1 160 13 is_stmt 1 view .LVU217
	testq	%rdx, %rdx
	je	.L19
.LVL75:
	.p2align 4,,10
	.p2align 3
.L26:
	.loc 1 162 11 view .LVU218
	.loc 1 162 14 is_stmt 0 view .LVU219
	cmpl	$2, 8(%rdx)
	je	.L80
	.loc 1 181 11 is_stmt 1 view .LVU220
	.loc 1 181 16 is_stmt 0 view .LVU221
	movq	32(%rdx), %rdx
.LVL76:
	.loc 1 160 13 is_stmt 1 view .LVU222
	testq	%rdx, %rdx
	jne	.L26
	jmp	.L25
.LVL77:
	.p2align 4,,10
	.p2align 3
.L32:
	.loc 1 195 7 view .LVU223
	movq	%r12, %rdi
	call	ares_free_hostent@PLT
.LVL78:
	jmp	.L33
.LVL79:
.L79:
	.loc 1 214 1 is_stmt 0 view .LVU224
	call	__stack_chk_fail@PLT
.LVL80:
	.cfi_endproc
.LFE87:
	.size	ares_parse_a_reply, .-ares_parse_a_reply
.Letext0:
	.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 7 "/usr/include/netinet/in.h"
	.file 8 "../deps/cares/include/ares_build.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 11 "/usr/include/stdio.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 13 "/usr/include/errno.h"
	.file 14 "/usr/include/time.h"
	.file 15 "/usr/include/unistd.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 17 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 18 "/usr/include/netdb.h"
	.file 19 "/usr/include/signal.h"
	.file 20 "/usr/include/arpa/nameser.h"
	.file 21 "../deps/cares/include/ares.h"
	.file 22 "../deps/cares/src/ares_ipv6.h"
	.file 23 "../deps/cares/src/ares_private.h"
	.file 24 "/usr/include/string.h"
	.file 25 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xedf
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF174
	.byte	0x1
	.long	.LASF175
	.long	.LASF176
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x3
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x4
	.long	.LASF6
	.byte	0x3
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x4
	.long	.LASF7
	.byte	0x3
	.byte	0x28
	.byte	0x1c
	.long	0x39
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.long	.LASF8
	.byte	0x3
	.byte	0x2a
	.byte	0x16
	.long	0x40
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x4
	.long	.LASF10
	.byte	0x3
	.byte	0x98
	.byte	0x12
	.long	0x87
	.uleb128 0x4
	.long	.LASF11
	.byte	0x3
	.byte	0x99
	.byte	0x12
	.long	0x87
	.uleb128 0x6
	.byte	0x8
	.uleb128 0x7
	.long	0xa6
	.uleb128 0x8
	.byte	0x8
	.long	0xb3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x3
	.long	0xb3
	.uleb128 0x4
	.long	.LASF13
	.byte	0x3
	.byte	0xd1
	.byte	0x16
	.long	0x40
	.uleb128 0x4
	.long	.LASF14
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x47
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF15
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF16
	.uleb128 0x4
	.long	.LASF17
	.byte	0x5
	.byte	0x21
	.byte	0x15
	.long	0xbf
	.uleb128 0x4
	.long	.LASF18
	.byte	0x6
	.byte	0x1c
	.byte	0x1c
	.long	0x39
	.uleb128 0x9
	.long	.LASF25
	.byte	0x10
	.byte	0x5
	.byte	0xb2
	.byte	0x8
	.long	0x125
	.uleb128 0xa
	.long	.LASF19
	.byte	0x5
	.byte	0xb4
	.byte	0x11
	.long	0xf1
	.byte	0
	.uleb128 0xa
	.long	.LASF20
	.byte	0x5
	.byte	0xb5
	.byte	0xa
	.long	0x12a
	.byte	0x2
	.byte	0
	.uleb128 0x3
	.long	0xfd
	.uleb128 0xb
	.long	0xb3
	.long	0x13a
	.uleb128 0xc
	.long	0x47
	.byte	0xd
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.long	0xfd
	.uleb128 0x7
	.long	0x13a
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x3
	.long	0x145
	.uleb128 0x8
	.byte	0x8
	.long	0x145
	.uleb128 0x7
	.long	0x14f
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x3
	.long	0x15a
	.uleb128 0x8
	.byte	0x8
	.long	0x15a
	.uleb128 0x7
	.long	0x164
	.uleb128 0xd
	.long	.LASF23
	.uleb128 0x3
	.long	0x16f
	.uleb128 0x8
	.byte	0x8
	.long	0x16f
	.uleb128 0x7
	.long	0x179
	.uleb128 0xd
	.long	.LASF24
	.uleb128 0x3
	.long	0x184
	.uleb128 0x8
	.byte	0x8
	.long	0x184
	.uleb128 0x7
	.long	0x18e
	.uleb128 0x9
	.long	.LASF26
	.byte	0x10
	.byte	0x7
	.byte	0xee
	.byte	0x8
	.long	0x1db
	.uleb128 0xa
	.long	.LASF27
	.byte	0x7
	.byte	0xf0
	.byte	0x11
	.long	0xf1
	.byte	0
	.uleb128 0xa
	.long	.LASF28
	.byte	0x7
	.byte	0xf1
	.byte	0xf
	.long	0x6e8
	.byte	0x2
	.uleb128 0xa
	.long	.LASF29
	.byte	0x7
	.byte	0xf2
	.byte	0x14
	.long	0x6cd
	.byte	0x4
	.uleb128 0xa
	.long	.LASF30
	.byte	0x7
	.byte	0xf5
	.byte	0x13
	.long	0x78a
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.long	0x199
	.uleb128 0x8
	.byte	0x8
	.long	0x199
	.uleb128 0x7
	.long	0x1e0
	.uleb128 0x9
	.long	.LASF31
	.byte	0x1c
	.byte	0x7
	.byte	0xfd
	.byte	0x8
	.long	0x23e
	.uleb128 0xa
	.long	.LASF32
	.byte	0x7
	.byte	0xff
	.byte	0x11
	.long	0xf1
	.byte	0
	.uleb128 0xe
	.long	.LASF33
	.byte	0x7
	.value	0x100
	.byte	0xf
	.long	0x6e8
	.byte	0x2
	.uleb128 0xe
	.long	.LASF34
	.byte	0x7
	.value	0x101
	.byte	0xe
	.long	0x6b5
	.byte	0x4
	.uleb128 0xe
	.long	.LASF35
	.byte	0x7
	.value	0x102
	.byte	0x15
	.long	0x752
	.byte	0x8
	.uleb128 0xe
	.long	.LASF36
	.byte	0x7
	.value	0x103
	.byte	0xe
	.long	0x6b5
	.byte	0x18
	.byte	0
	.uleb128 0x3
	.long	0x1eb
	.uleb128 0x8
	.byte	0x8
	.long	0x1eb
	.uleb128 0x7
	.long	0x243
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x3
	.long	0x24e
	.uleb128 0x8
	.byte	0x8
	.long	0x24e
	.uleb128 0x7
	.long	0x258
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x3
	.long	0x263
	.uleb128 0x8
	.byte	0x8
	.long	0x263
	.uleb128 0x7
	.long	0x26d
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x3
	.long	0x278
	.uleb128 0x8
	.byte	0x8
	.long	0x278
	.uleb128 0x7
	.long	0x282
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x3
	.long	0x28d
	.uleb128 0x8
	.byte	0x8
	.long	0x28d
	.uleb128 0x7
	.long	0x297
	.uleb128 0xd
	.long	.LASF41
	.uleb128 0x3
	.long	0x2a2
	.uleb128 0x8
	.byte	0x8
	.long	0x2a2
	.uleb128 0x7
	.long	0x2ac
	.uleb128 0xd
	.long	.LASF42
	.uleb128 0x3
	.long	0x2b7
	.uleb128 0x8
	.byte	0x8
	.long	0x2b7
	.uleb128 0x7
	.long	0x2c1
	.uleb128 0x8
	.byte	0x8
	.long	0x125
	.uleb128 0x7
	.long	0x2cc
	.uleb128 0x8
	.byte	0x8
	.long	0x14a
	.uleb128 0x7
	.long	0x2d7
	.uleb128 0x8
	.byte	0x8
	.long	0x15f
	.uleb128 0x7
	.long	0x2e2
	.uleb128 0x8
	.byte	0x8
	.long	0x174
	.uleb128 0x7
	.long	0x2ed
	.uleb128 0x8
	.byte	0x8
	.long	0x189
	.uleb128 0x7
	.long	0x2f8
	.uleb128 0x8
	.byte	0x8
	.long	0x1db
	.uleb128 0x7
	.long	0x303
	.uleb128 0x8
	.byte	0x8
	.long	0x23e
	.uleb128 0x7
	.long	0x30e
	.uleb128 0x8
	.byte	0x8
	.long	0x253
	.uleb128 0x7
	.long	0x319
	.uleb128 0x8
	.byte	0x8
	.long	0x268
	.uleb128 0x7
	.long	0x324
	.uleb128 0x8
	.byte	0x8
	.long	0x27d
	.uleb128 0x7
	.long	0x32f
	.uleb128 0x8
	.byte	0x8
	.long	0x292
	.uleb128 0x7
	.long	0x33a
	.uleb128 0x8
	.byte	0x8
	.long	0x2a7
	.uleb128 0x7
	.long	0x345
	.uleb128 0x8
	.byte	0x8
	.long	0x2bc
	.uleb128 0x7
	.long	0x350
	.uleb128 0x4
	.long	.LASF43
	.byte	0x8
	.byte	0xbf
	.byte	0x14
	.long	0xe5
	.uleb128 0xb
	.long	0xb3
	.long	0x377
	.uleb128 0xc
	.long	0x47
	.byte	0
	.byte	0
	.uleb128 0x9
	.long	.LASF44
	.byte	0xd8
	.byte	0x9
	.byte	0x31
	.byte	0x8
	.long	0x4fe
	.uleb128 0xa
	.long	.LASF45
	.byte	0x9
	.byte	0x33
	.byte	0x7
	.long	0x74
	.byte	0
	.uleb128 0xa
	.long	.LASF46
	.byte	0x9
	.byte	0x36
	.byte	0x9
	.long	0xad
	.byte	0x8
	.uleb128 0xa
	.long	.LASF47
	.byte	0x9
	.byte	0x37
	.byte	0x9
	.long	0xad
	.byte	0x10
	.uleb128 0xa
	.long	.LASF48
	.byte	0x9
	.byte	0x38
	.byte	0x9
	.long	0xad
	.byte	0x18
	.uleb128 0xa
	.long	.LASF49
	.byte	0x9
	.byte	0x39
	.byte	0x9
	.long	0xad
	.byte	0x20
	.uleb128 0xa
	.long	.LASF50
	.byte	0x9
	.byte	0x3a
	.byte	0x9
	.long	0xad
	.byte	0x28
	.uleb128 0xa
	.long	.LASF51
	.byte	0x9
	.byte	0x3b
	.byte	0x9
	.long	0xad
	.byte	0x30
	.uleb128 0xa
	.long	.LASF52
	.byte	0x9
	.byte	0x3c
	.byte	0x9
	.long	0xad
	.byte	0x38
	.uleb128 0xa
	.long	.LASF53
	.byte	0x9
	.byte	0x3d
	.byte	0x9
	.long	0xad
	.byte	0x40
	.uleb128 0xa
	.long	.LASF54
	.byte	0x9
	.byte	0x40
	.byte	0x9
	.long	0xad
	.byte	0x48
	.uleb128 0xa
	.long	.LASF55
	.byte	0x9
	.byte	0x41
	.byte	0x9
	.long	0xad
	.byte	0x50
	.uleb128 0xa
	.long	.LASF56
	.byte	0x9
	.byte	0x42
	.byte	0x9
	.long	0xad
	.byte	0x58
	.uleb128 0xa
	.long	.LASF57
	.byte	0x9
	.byte	0x44
	.byte	0x16
	.long	0x517
	.byte	0x60
	.uleb128 0xa
	.long	.LASF58
	.byte	0x9
	.byte	0x46
	.byte	0x14
	.long	0x51d
	.byte	0x68
	.uleb128 0xa
	.long	.LASF59
	.byte	0x9
	.byte	0x48
	.byte	0x7
	.long	0x74
	.byte	0x70
	.uleb128 0xa
	.long	.LASF60
	.byte	0x9
	.byte	0x49
	.byte	0x7
	.long	0x74
	.byte	0x74
	.uleb128 0xa
	.long	.LASF61
	.byte	0x9
	.byte	0x4a
	.byte	0xb
	.long	0x8e
	.byte	0x78
	.uleb128 0xa
	.long	.LASF62
	.byte	0x9
	.byte	0x4d
	.byte	0x12
	.long	0x39
	.byte	0x80
	.uleb128 0xa
	.long	.LASF63
	.byte	0x9
	.byte	0x4e
	.byte	0xf
	.long	0x4e
	.byte	0x82
	.uleb128 0xa
	.long	.LASF64
	.byte	0x9
	.byte	0x4f
	.byte	0x8
	.long	0x367
	.byte	0x83
	.uleb128 0xa
	.long	.LASF65
	.byte	0x9
	.byte	0x51
	.byte	0xf
	.long	0x523
	.byte	0x88
	.uleb128 0xa
	.long	.LASF66
	.byte	0x9
	.byte	0x59
	.byte	0xd
	.long	0x9a
	.byte	0x90
	.uleb128 0xa
	.long	.LASF67
	.byte	0x9
	.byte	0x5b
	.byte	0x17
	.long	0x52e
	.byte	0x98
	.uleb128 0xa
	.long	.LASF68
	.byte	0x9
	.byte	0x5c
	.byte	0x19
	.long	0x539
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF69
	.byte	0x9
	.byte	0x5d
	.byte	0x14
	.long	0x51d
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF70
	.byte	0x9
	.byte	0x5e
	.byte	0x9
	.long	0xa6
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF71
	.byte	0x9
	.byte	0x5f
	.byte	0xa
	.long	0xcb
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF72
	.byte	0x9
	.byte	0x60
	.byte	0x7
	.long	0x74
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF73
	.byte	0x9
	.byte	0x62
	.byte	0x8
	.long	0x53f
	.byte	0xc4
	.byte	0
	.uleb128 0x4
	.long	.LASF74
	.byte	0xa
	.byte	0x7
	.byte	0x19
	.long	0x377
	.uleb128 0xf
	.long	.LASF177
	.byte	0x9
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF75
	.uleb128 0x8
	.byte	0x8
	.long	0x512
	.uleb128 0x8
	.byte	0x8
	.long	0x377
	.uleb128 0x8
	.byte	0x8
	.long	0x50a
	.uleb128 0xd
	.long	.LASF76
	.uleb128 0x8
	.byte	0x8
	.long	0x529
	.uleb128 0xd
	.long	.LASF77
	.uleb128 0x8
	.byte	0x8
	.long	0x534
	.uleb128 0xb
	.long	0xb3
	.long	0x54f
	.uleb128 0xc
	.long	0x47
	.byte	0x13
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.long	0xba
	.uleb128 0x3
	.long	0x54f
	.uleb128 0x10
	.long	.LASF78
	.byte	0xb
	.byte	0x89
	.byte	0xe
	.long	0x566
	.uleb128 0x8
	.byte	0x8
	.long	0x4fe
	.uleb128 0x10
	.long	.LASF79
	.byte	0xb
	.byte	0x8a
	.byte	0xe
	.long	0x566
	.uleb128 0x10
	.long	.LASF80
	.byte	0xb
	.byte	0x8b
	.byte	0xe
	.long	0x566
	.uleb128 0x10
	.long	.LASF81
	.byte	0xc
	.byte	0x1a
	.byte	0xc
	.long	0x74
	.uleb128 0xb
	.long	0x555
	.long	0x59b
	.uleb128 0x11
	.byte	0
	.uleb128 0x3
	.long	0x590
	.uleb128 0x10
	.long	.LASF82
	.byte	0xc
	.byte	0x1b
	.byte	0x1a
	.long	0x59b
	.uleb128 0x10
	.long	.LASF83
	.byte	0xc
	.byte	0x1e
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF84
	.byte	0xc
	.byte	0x1f
	.byte	0x1a
	.long	0x59b
	.uleb128 0x8
	.byte	0x8
	.long	0x5cf
	.uleb128 0x7
	.long	0x5c4
	.uleb128 0x12
	.uleb128 0x10
	.long	.LASF85
	.byte	0xd
	.byte	0x2d
	.byte	0xe
	.long	0xad
	.uleb128 0x10
	.long	.LASF86
	.byte	0xd
	.byte	0x2e
	.byte	0xe
	.long	0xad
	.uleb128 0xb
	.long	0xad
	.long	0x5f8
	.uleb128 0xc
	.long	0x47
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF87
	.byte	0xe
	.byte	0x9f
	.byte	0xe
	.long	0x5e8
	.uleb128 0x10
	.long	.LASF88
	.byte	0xe
	.byte	0xa0
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF89
	.byte	0xe
	.byte	0xa1
	.byte	0x11
	.long	0x87
	.uleb128 0x10
	.long	.LASF90
	.byte	0xe
	.byte	0xa6
	.byte	0xe
	.long	0x5e8
	.uleb128 0x10
	.long	.LASF91
	.byte	0xe
	.byte	0xae
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF92
	.byte	0xe
	.byte	0xaf
	.byte	0x11
	.long	0x87
	.uleb128 0x13
	.long	.LASF93
	.byte	0xe
	.value	0x112
	.byte	0xc
	.long	0x74
	.uleb128 0x13
	.long	.LASF94
	.byte	0xf
	.value	0x21f
	.byte	0xf
	.long	0x65a
	.uleb128 0x8
	.byte	0x8
	.long	0xad
	.uleb128 0x13
	.long	.LASF95
	.byte	0xf
	.value	0x221
	.byte	0xf
	.long	0x65a
	.uleb128 0x10
	.long	.LASF96
	.byte	0x10
	.byte	0x24
	.byte	0xe
	.long	0xad
	.uleb128 0x10
	.long	.LASF97
	.byte	0x10
	.byte	0x32
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF98
	.byte	0x10
	.byte	0x37
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF99
	.byte	0x10
	.byte	0x3b
	.byte	0xc
	.long	0x74
	.uleb128 0x4
	.long	.LASF100
	.byte	0x11
	.byte	0x18
	.byte	0x13
	.long	0x55
	.uleb128 0x4
	.long	.LASF101
	.byte	0x11
	.byte	0x19
	.byte	0x14
	.long	0x68
	.uleb128 0x4
	.long	.LASF102
	.byte	0x11
	.byte	0x1a
	.byte	0x14
	.long	0x7b
	.uleb128 0x4
	.long	.LASF103
	.byte	0x7
	.byte	0x1e
	.byte	0x12
	.long	0x6b5
	.uleb128 0x9
	.long	.LASF104
	.byte	0x4
	.byte	0x7
	.byte	0x1f
	.byte	0x8
	.long	0x6e8
	.uleb128 0xa
	.long	.LASF105
	.byte	0x7
	.byte	0x21
	.byte	0xf
	.long	0x6c1
	.byte	0
	.byte	0
	.uleb128 0x4
	.long	.LASF106
	.byte	0x7
	.byte	0x77
	.byte	0x12
	.long	0x6a9
	.uleb128 0x14
	.byte	0x10
	.byte	0x7
	.byte	0xd6
	.byte	0x5
	.long	0x722
	.uleb128 0x15
	.long	.LASF107
	.byte	0x7
	.byte	0xd8
	.byte	0xa
	.long	0x722
	.uleb128 0x15
	.long	.LASF108
	.byte	0x7
	.byte	0xd9
	.byte	0xb
	.long	0x732
	.uleb128 0x15
	.long	.LASF109
	.byte	0x7
	.byte	0xda
	.byte	0xb
	.long	0x742
	.byte	0
	.uleb128 0xb
	.long	0x69d
	.long	0x732
	.uleb128 0xc
	.long	0x47
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.long	0x6a9
	.long	0x742
	.uleb128 0xc
	.long	0x47
	.byte	0x7
	.byte	0
	.uleb128 0xb
	.long	0x6b5
	.long	0x752
	.uleb128 0xc
	.long	0x47
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF110
	.byte	0x10
	.byte	0x7
	.byte	0xd4
	.byte	0x8
	.long	0x76d
	.uleb128 0xa
	.long	.LASF111
	.byte	0x7
	.byte	0xdb
	.byte	0x9
	.long	0x6f4
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	0x752
	.uleb128 0x10
	.long	.LASF112
	.byte	0x7
	.byte	0xe4
	.byte	0x1e
	.long	0x76d
	.uleb128 0x10
	.long	.LASF113
	.byte	0x7
	.byte	0xe5
	.byte	0x1e
	.long	0x76d
	.uleb128 0xb
	.long	0x2d
	.long	0x79a
	.uleb128 0xc
	.long	0x47
	.byte	0x7
	.byte	0
	.uleb128 0x9
	.long	.LASF114
	.byte	0x20
	.byte	0x12
	.byte	0x62
	.byte	0x8
	.long	0x7e9
	.uleb128 0xa
	.long	.LASF115
	.byte	0x12
	.byte	0x64
	.byte	0x9
	.long	0xad
	.byte	0
	.uleb128 0xa
	.long	.LASF116
	.byte	0x12
	.byte	0x65
	.byte	0xa
	.long	0x65a
	.byte	0x8
	.uleb128 0xa
	.long	.LASF117
	.byte	0x12
	.byte	0x66
	.byte	0x7
	.long	0x74
	.byte	0x10
	.uleb128 0xa
	.long	.LASF118
	.byte	0x12
	.byte	0x67
	.byte	0x7
	.long	0x74
	.byte	0x14
	.uleb128 0xa
	.long	.LASF119
	.byte	0x12
	.byte	0x68
	.byte	0xa
	.long	0x65a
	.byte	0x18
	.byte	0
	.uleb128 0xb
	.long	0x555
	.long	0x7f9
	.uleb128 0xc
	.long	0x47
	.byte	0x40
	.byte	0
	.uleb128 0x3
	.long	0x7e9
	.uleb128 0x13
	.long	.LASF120
	.byte	0x13
	.value	0x11e
	.byte	0x1a
	.long	0x7f9
	.uleb128 0x13
	.long	.LASF121
	.byte	0x13
	.value	0x11f
	.byte	0x1a
	.long	0x7f9
	.uleb128 0x8
	.byte	0x8
	.long	0x34
	.uleb128 0x9
	.long	.LASF122
	.byte	0x8
	.byte	0x14
	.byte	0x67
	.byte	0x8
	.long	0x846
	.uleb128 0xa
	.long	.LASF123
	.byte	0x14
	.byte	0x67
	.byte	0x1b
	.long	0x74
	.byte	0
	.uleb128 0xa
	.long	.LASF124
	.byte	0x14
	.byte	0x67
	.byte	0x21
	.long	0x74
	.byte	0x4
	.byte	0
	.uleb128 0x3
	.long	0x81e
	.uleb128 0xb
	.long	0x846
	.long	0x856
	.uleb128 0x11
	.byte	0
	.uleb128 0x3
	.long	0x84b
	.uleb128 0x10
	.long	.LASF122
	.byte	0x14
	.byte	0x68
	.byte	0x22
	.long	0x856
	.uleb128 0x8
	.byte	0x8
	.long	0x6cd
	.uleb128 0x8
	.byte	0x8
	.long	0x79a
	.uleb128 0x16
	.long	.LASF125
	.byte	0x10
	.byte	0x15
	.value	0x260
	.byte	0x8
	.long	0x89e
	.uleb128 0xe
	.long	.LASF126
	.byte	0x15
	.value	0x261
	.byte	0x1f
	.long	0x9df
	.byte	0
	.uleb128 0xe
	.long	.LASF127
	.byte	0x15
	.value	0x262
	.byte	0x1e
	.long	0x992
	.byte	0x8
	.byte	0
	.uleb128 0x17
	.byte	0x10
	.byte	0x15
	.value	0x204
	.byte	0x3
	.long	0x8b6
	.uleb128 0x18
	.long	.LASF128
	.byte	0x15
	.value	0x205
	.byte	0x13
	.long	0x8b6
	.byte	0
	.uleb128 0xb
	.long	0x2d
	.long	0x8c6
	.uleb128 0xc
	.long	0x47
	.byte	0xf
	.byte	0
	.uleb128 0x16
	.long	.LASF129
	.byte	0x10
	.byte	0x15
	.value	0x203
	.byte	0x8
	.long	0x8e3
	.uleb128 0xe
	.long	.LASF130
	.byte	0x15
	.value	0x206
	.byte	0x5
	.long	0x89e
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	0x8c6
	.uleb128 0x16
	.long	.LASF131
	.byte	0x8
	.byte	0x15
	.value	0x209
	.byte	0x8
	.long	0x913
	.uleb128 0xe
	.long	.LASF132
	.byte	0x15
	.value	0x20a
	.byte	0x12
	.long	0x6cd
	.byte	0
	.uleb128 0x19
	.string	"ttl"
	.byte	0x15
	.value	0x20b
	.byte	0x7
	.long	0x74
	.byte	0x4
	.byte	0
	.uleb128 0x16
	.long	.LASF133
	.byte	0x28
	.byte	0x15
	.value	0x249
	.byte	0x8
	.long	0x992
	.uleb128 0xe
	.long	.LASF134
	.byte	0x15
	.value	0x24a
	.byte	0x7
	.long	0x74
	.byte	0
	.uleb128 0xe
	.long	.LASF135
	.byte	0x15
	.value	0x24b
	.byte	0x7
	.long	0x74
	.byte	0x4
	.uleb128 0xe
	.long	.LASF136
	.byte	0x15
	.value	0x24c
	.byte	0x7
	.long	0x74
	.byte	0x8
	.uleb128 0xe
	.long	.LASF137
	.byte	0x15
	.value	0x24d
	.byte	0x7
	.long	0x74
	.byte	0xc
	.uleb128 0xe
	.long	.LASF138
	.byte	0x15
	.value	0x24e
	.byte	0x7
	.long	0x74
	.byte	0x10
	.uleb128 0xe
	.long	.LASF139
	.byte	0x15
	.value	0x24f
	.byte	0x12
	.long	0x35b
	.byte	0x14
	.uleb128 0xe
	.long	.LASF140
	.byte	0x15
	.value	0x250
	.byte	0x14
	.long	0x13a
	.byte	0x18
	.uleb128 0xe
	.long	.LASF141
	.byte	0x15
	.value	0x251
	.byte	0x1e
	.long	0x992
	.byte	0x20
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.long	0x913
	.uleb128 0x16
	.long	.LASF142
	.byte	0x20
	.byte	0x15
	.value	0x259
	.byte	0x8
	.long	0x9df
	.uleb128 0x19
	.string	"ttl"
	.byte	0x15
	.value	0x25a
	.byte	0x7
	.long	0x74
	.byte	0
	.uleb128 0xe
	.long	.LASF143
	.byte	0x15
	.value	0x25b
	.byte	0x9
	.long	0xad
	.byte	0x8
	.uleb128 0xe
	.long	.LASF144
	.byte	0x15
	.value	0x25c
	.byte	0x9
	.long	0xad
	.byte	0x10
	.uleb128 0xe
	.long	.LASF145
	.byte	0x15
	.value	0x25d
	.byte	0x1f
	.long	0x9df
	.byte	0x18
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.long	0x998
	.uleb128 0x10
	.long	.LASF146
	.byte	0x16
	.byte	0x52
	.byte	0x23
	.long	0x8e3
	.uleb128 0x1a
	.long	0xa6
	.long	0xa00
	.uleb128 0x1b
	.long	0xcb
	.byte	0
	.uleb128 0x13
	.long	.LASF147
	.byte	0x17
	.value	0x151
	.byte	0x10
	.long	0xa0d
	.uleb128 0x8
	.byte	0x8
	.long	0x9f1
	.uleb128 0x1a
	.long	0xa6
	.long	0xa27
	.uleb128 0x1b
	.long	0xa6
	.uleb128 0x1b
	.long	0xcb
	.byte	0
	.uleb128 0x13
	.long	.LASF148
	.byte	0x17
	.value	0x152
	.byte	0x10
	.long	0xa34
	.uleb128 0x8
	.byte	0x8
	.long	0xa13
	.uleb128 0x1c
	.long	0xa45
	.uleb128 0x1b
	.long	0xa6
	.byte	0
	.uleb128 0x13
	.long	.LASF149
	.byte	0x17
	.value	0x153
	.byte	0xf
	.long	0xa52
	.uleb128 0x8
	.byte	0x8
	.long	0xa3a
	.uleb128 0x1d
	.long	.LASF178
	.byte	0x1
	.byte	0x32
	.byte	0x5
	.long	0x74
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0xe10
	.uleb128 0x1e
	.long	.LASF150
	.byte	0x1
	.byte	0x32
	.byte	0x2d
	.long	0x818
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x1e
	.long	.LASF151
	.byte	0x1
	.byte	0x32
	.byte	0x37
	.long	0x74
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x1e
	.long	.LASF152
	.byte	0x1
	.byte	0x33
	.byte	0x29
	.long	0xe10
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x1e
	.long	.LASF153
	.byte	0x1
	.byte	0x34
	.byte	0x2d
	.long	0xe16
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x1e
	.long	.LASF154
	.byte	0x1
	.byte	0x34
	.byte	0x3c
	.long	0xe1c
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x1f
	.string	"ai"
	.byte	0x1
	.byte	0x36
	.byte	0x18
	.long	0x873
	.uleb128 0x3
	.byte	0x91
	.sleb128 -96
	.uleb128 0x20
	.long	.LASF145
	.byte	0x1
	.byte	0x37
	.byte	0x1e
	.long	0x992
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x20
	.long	.LASF155
	.byte	0x1
	.byte	0x38
	.byte	0x1f
	.long	0x9df
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x20
	.long	.LASF156
	.byte	0x1
	.byte	0x39
	.byte	0xa
	.long	0x65a
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x21
	.long	.LASF157
	.byte	0x1
	.byte	0x3a
	.byte	0x9
	.long	0xad
	.uleb128 0x3
	.byte	0x91
	.sleb128 -104
	.uleb128 0x20
	.long	.LASF114
	.byte	0x1
	.byte	0x3b
	.byte	0x13
	.long	0x86d
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x20
	.long	.LASF158
	.byte	0x1
	.byte	0x3c
	.byte	0x13
	.long	0x867
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x20
	.long	.LASF159
	.byte	0x1
	.byte	0x3d
	.byte	0x7
	.long	0x74
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x20
	.long	.LASF160
	.byte	0x1
	.byte	0x3d
	.byte	0x15
	.long	0x74
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x20
	.long	.LASF143
	.byte	0x1
	.byte	0x3d
	.byte	0x21
	.long	0x74
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x22
	.string	"i"
	.byte	0x1
	.byte	0x3d
	.byte	0x2c
	.long	0x74
	.long	.LLST13
	.long	.LVUS13
	.uleb128 0x20
	.long	.LASF161
	.byte	0x1
	.byte	0x3e
	.byte	0x7
	.long	0x74
	.long	.LLST14
	.long	.LVUS14
	.uleb128 0x20
	.long	.LASF162
	.byte	0x1
	.byte	0x3f
	.byte	0x7
	.long	0x74
	.long	.LLST15
	.long	.LVUS15
	.uleb128 0x23
	.long	.LASF179
	.byte	0x1
	.byte	0xcf
	.byte	0x1
	.quad	.L15
	.uleb128 0x24
	.long	0xe22
	.quad	.LBI8
	.byte	.LVU22
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x41
	.byte	0x3
	.long	0xc28
	.uleb128 0x25
	.long	0xe4b
	.long	.LLST16
	.long	.LVUS16
	.uleb128 0x25
	.long	0xe3f
	.long	.LLST17
	.long	.LVUS17
	.uleb128 0x25
	.long	0xe33
	.long	.LLST18
	.long	.LVUS18
	.byte	0
	.uleb128 0x24
	.long	0xe58
	.quad	.LBI14
	.byte	.LVU144
	.long	.Ldebug_ranges0+0x40
	.byte	0x1
	.byte	0xa5
	.byte	0xf
	.long	0xc69
	.uleb128 0x25
	.long	0xe81
	.long	.LLST19
	.long	.LVUS19
	.uleb128 0x25
	.long	0xe75
	.long	.LLST20
	.long	.LVUS20
	.uleb128 0x25
	.long	0xe69
	.long	.LLST21
	.long	.LVUS21
	.byte	0
	.uleb128 0x26
	.long	0xe58
	.quad	.LBI17
	.byte	.LVU154
	.quad	.LBB17
	.quad	.LBE17-.LBB17
	.byte	0x1
	.byte	0xaf
	.byte	0x13
	.long	0xcb6
	.uleb128 0x25
	.long	0xe81
	.long	.LLST22
	.long	.LVUS22
	.uleb128 0x25
	.long	0xe75
	.long	.LLST23
	.long	.LVUS23
	.uleb128 0x25
	.long	0xe69
	.long	.LLST24
	.long	.LVUS24
	.byte	0
	.uleb128 0x27
	.quad	.LVL6
	.long	0xe8e
	.long	0xce4
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0x76
	.sleb128 -88
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x3
	.byte	0x76
	.sleb128 -80
	.byte	0
	.uleb128 0x29
	.quad	.LVL8
	.long	0xcf8
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8
	.byte	0x20
	.byte	0
	.uleb128 0x2a
	.quad	.LVL26
	.long	0xe9b
	.uleb128 0x29
	.quad	.LVL33
	.long	0xd21
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0xa
	.byte	0x7d
	.sleb128 1
	.byte	0x8
	.byte	0x20
	.byte	0x24
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0x33
	.byte	0x24
	.byte	0
	.uleb128 0x27
	.quad	.LVL34
	.long	0xea7
	.long	0xd42
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x6
	.byte	0x7f
	.sleb128 0
	.byte	0x33
	.byte	0x24
	.byte	0x23
	.uleb128 0x8
	.byte	0
	.uleb128 0x2a
	.quad	.LVL35
	.long	0xe9b
	.uleb128 0x29
	.quad	.LVL36
	.long	0xd65
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x4
	.byte	0x76
	.sleb128 -104
	.byte	0x6
	.byte	0
	.uleb128 0x29
	.quad	.LVL37
	.long	0xd7b
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x4
	.byte	0x7f
	.sleb128 0
	.byte	0x32
	.byte	0x24
	.byte	0
	.uleb128 0x2a
	.quad	.LVL52
	.long	0xeb2
	.uleb128 0x2a
	.quad	.LVL53
	.long	0xebf
	.uleb128 0x29
	.quad	.LVL58
	.long	0xda8
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x38
	.byte	0
	.uleb128 0x29
	.quad	.LVL64
	.long	0xdbc
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.byte	0
	.uleb128 0x29
	.quad	.LVL65
	.long	0xdd0
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x2a
	.quad	.LVL66
	.long	0xeb2
	.uleb128 0x2a
	.quad	.LVL67
	.long	0xebf
	.uleb128 0x27
	.quad	.LVL78
	.long	0xecc
	.long	0xe02
	.uleb128 0x28
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x2a
	.quad	.LVL80
	.long	0xed9
	.byte	0
	.uleb128 0x8
	.byte	0x8
	.long	0x86d
	.uleb128 0x8
	.byte	0x8
	.long	0x8e8
	.uleb128 0x8
	.byte	0x8
	.long	0x74
	.uleb128 0x2b
	.long	.LASF166
	.byte	0x2
	.byte	0x3b
	.byte	0x2a
	.long	0xa6
	.byte	0x3
	.long	0xe58
	.uleb128 0x2c
	.long	.LASF163
	.byte	0x2
	.byte	0x3b
	.byte	0x38
	.long	0xa6
	.uleb128 0x2c
	.long	.LASF164
	.byte	0x2
	.byte	0x3b
	.byte	0x44
	.long	0x74
	.uleb128 0x2c
	.long	.LASF165
	.byte	0x2
	.byte	0x3b
	.byte	0x51
	.long	0xcb
	.byte	0
	.uleb128 0x2b
	.long	.LASF167
	.byte	0x2
	.byte	0x1f
	.byte	0x2a
	.long	0xa6
	.byte	0x3
	.long	0xe8e
	.uleb128 0x2c
	.long	.LASF163
	.byte	0x2
	.byte	0x1f
	.byte	0x43
	.long	0xa8
	.uleb128 0x2c
	.long	.LASF168
	.byte	0x2
	.byte	0x1f
	.byte	0x62
	.long	0x5ca
	.uleb128 0x2c
	.long	.LASF165
	.byte	0x2
	.byte	0x1f
	.byte	0x70
	.long	0xcb
	.byte	0
	.uleb128 0x2d
	.long	.LASF169
	.long	.LASF169
	.byte	0x17
	.value	0x183
	.byte	0x5
	.uleb128 0x2e
	.long	.LASF170
	.long	.LASF170
	.byte	0x18
	.byte	0xa7
	.byte	0xe
	.uleb128 0x2f
	.long	.LASF166
	.long	.LASF180
	.byte	0x19
	.byte	0
	.uleb128 0x2d
	.long	.LASF171
	.long	.LASF171
	.byte	0x17
	.value	0x178
	.byte	0x6
	.uleb128 0x2d
	.long	.LASF172
	.long	.LASF172
	.byte	0x17
	.value	0x171
	.byte	0x6
	.uleb128 0x2d
	.long	.LASF173
	.long	.LASF173
	.byte	0x15
	.value	0x2a5
	.byte	0x7
	.uleb128 0x30
	.long	.LASF181
	.long	.LASF181
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0xa
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU28
	.uleb128 .LVU28
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL6-1-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU28
	.uleb128 .LVU28
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL6-1-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU6
	.uleb128 .LVU6
	.uleb128 .LVU178
	.uleb128 .LVU178
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL1-.Ltext0
	.quad	.LVL54-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -136
	.quad	.LVL54-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -152
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 0
	.uleb128 .LVU8
	.uleb128 .LVU8
	.uleb128 .LVU178
	.uleb128 .LVU178
	.uleb128 0
.LLST3:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL2-.Ltext0
	.quad	.LVL54-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -144
	.quad	.LVL54-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -160
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 0
	.uleb128 .LVU28
	.uleb128 .LVU28
	.uleb128 .LVU178
	.uleb128 .LVU178
	.uleb128 0
.LLST4:
	.quad	.LVL0-.Ltext0
	.quad	.LVL6-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL6-1-.Ltext0
	.quad	.LVL54-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -128
	.quad	.LVL54-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -144
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU38
	.uleb128 .LVU45
	.uleb128 .LVU47
	.uleb128 .LVU50
	.uleb128 .LVU129
	.uleb128 .LVU160
	.uleb128 .LVU162
	.uleb128 .LVU166
	.uleb128 .LVU202
	.uleb128 .LVU223
.LLST5:
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL13-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL40-.Ltext0
	.quad	.LVL48-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL49-.Ltext0
	.quad	.LVL51-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL70-.Ltext0
	.quad	.LVL77-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU50
	.uleb128 .LVU64
	.uleb128 .LVU71
	.uleb128 .LVU86
	.uleb128 .LVU88
	.uleb128 .LVU91
	.uleb128 .LVU93
	.uleb128 .LVU94
	.uleb128 .LVU184
	.uleb128 .LVU186
.LLST6:
	.quad	.LVL15-.Ltext0
	.quad	.LVL20-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL22-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL28-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL31-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL57-.Ltext0
	.quad	.LVL58-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU14
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU75
	.uleb128 .LVU75
	.uleb128 .LVU177
	.uleb128 .LVU179
	.uleb128 .LVU187
	.uleb128 .LVU187
	.uleb128 .LVU191
	.uleb128 .LVU191
	.uleb128 .LVU193
	.uleb128 .LVU194
	.uleb128 .LVU224
.LLST7:
	.quad	.LVL3-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL21-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL23-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL55-.Ltext0
	.quad	.LVL59-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL59-.Ltext0
	.quad	.LVL61-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL61-.Ltext0
	.quad	.LVL62-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL63-.Ltext0
	.quad	.LVL79-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU17
	.uleb128 .LVU34
	.uleb128 .LVU34
	.uleb128 .LVU38
	.uleb128 .LVU38
	.uleb128 .LVU177
	.uleb128 .LVU179
	.uleb128 .LVU184
	.uleb128 .LVU184
	.uleb128 .LVU224
.LLST8:
	.quad	.LVL4-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL10-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL55-.Ltext0
	.quad	.LVL57-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL57-.Ltext0
	.quad	.LVL79-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU18
	.uleb128 .LVU124
	.uleb128 .LVU124
	.uleb128 .LVU132
	.uleb128 .LVU132
	.uleb128 .LVU166
	.uleb128 .LVU179
	.uleb128 .LVU194
	.uleb128 .LVU202
	.uleb128 .LVU223
.LLST9:
	.quad	.LVL4-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL38-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL41-.Ltext0
	.quad	.LVL51-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL55-.Ltext0
	.quad	.LVL63-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL70-.Ltext0
	.quad	.LVL77-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU19
	.uleb128 .LVU53
	.uleb128 .LVU53
	.uleb128 .LVU56
	.uleb128 .LVU56
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU71
	.uleb128 .LVU179
	.uleb128 .LVU189
.LLST10:
	.quad	.LVL4-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL16-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL18-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL55-.Ltext0
	.quad	.LVL60-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 .LVU19
	.uleb128 .LVU42
	.uleb128 .LVU42
	.uleb128 .LVU48
	.uleb128 .LVU179
	.uleb128 .LVU184
.LLST11:
	.quad	.LVL4-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL11-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL55-.Ltext0
	.quad	.LVL57-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 .LVU19
	.uleb128 .LVU75
	.uleb128 .LVU75
	.uleb128 .LVU79
	.uleb128 .LVU79
	.uleb128 .LVU81
	.uleb128 .LVU81
	.uleb128 .LVU83
	.uleb128 .LVU83
	.uleb128 .LVU89
	.uleb128 .LVU89
	.uleb128 .LVU94
	.uleb128 .LVU179
	.uleb128 .LVU191
	.uleb128 .LVU193
	.uleb128 .LVU194
.LLST12:
	.quad	.LVL4-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x5a
	.quad	.LVL24-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x3
	.byte	0x7a
	.sleb128 -1
	.byte	0x9f
	.quad	.LVL25-.Ltext0
	.quad	.LVL26-1-.Ltext0
	.value	0x1
	.byte	0x5a
	.quad	.LVL26-1-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -120
	.quad	.LVL29-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x1
	.byte	0x5a
	.quad	.LVL55-.Ltext0
	.quad	.LVL61-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL62-.Ltext0
	.quad	.LVL63-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS13:
	.uleb128 .LVU127
	.uleb128 .LVU132
	.uleb128 .LVU132
	.uleb128 .LVU165
	.uleb128 .LVU202
	.uleb128 .LVU206
	.uleb128 .LVU206
	.uleb128 .LVU215
	.uleb128 .LVU215
	.uleb128 .LVU223
.LLST13:
	.quad	.LVL39-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL41-.Ltext0
	.quad	.LVL50-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL70-.Ltext0
	.quad	.LVL71-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL71-.Ltext0
	.quad	.LVL73-.Ltext0
	.value	0x3
	.byte	0x74
	.sleb128 -1
	.byte	0x9f
	.quad	.LVL73-.Ltext0
	.quad	.LVL77-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LVUS14:
	.uleb128 .LVU20
	.uleb128 .LVU75
	.uleb128 .LVU75
	.uleb128 .LVU177
	.uleb128 .LVU179
	.uleb128 .LVU191
	.uleb128 .LVU191
	.uleb128 .LVU193
	.uleb128 .LVU193
	.uleb128 .LVU194
	.uleb128 .LVU202
	.uleb128 .LVU224
.LLST14:
	.quad	.LVL4-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x6
	.byte	0xc
	.long	0x7fffffff
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL55-.Ltext0
	.quad	.LVL61-.Ltext0
	.value	0x6
	.byte	0xc
	.long	0x7fffffff
	.byte	0x9f
	.quad	.LVL61-.Ltext0
	.quad	.LVL62-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL62-.Ltext0
	.quad	.LVL63-.Ltext0
	.value	0x6
	.byte	0xc
	.long	0x7fffffff
	.byte	0x9f
	.quad	.LVL70-.Ltext0
	.quad	.LVL79-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS15:
	.uleb128 .LVU29
	.uleb128 .LVU33
	.uleb128 .LVU33
	.uleb128 .LVU177
	.uleb128 .LVU179
	.uleb128 .LVU180
	.uleb128 .LVU180
	.uleb128 .LVU201
	.uleb128 .LVU202
	.uleb128 .LVU224
.LLST15:
	.quad	.LVL7-.Ltext0
	.quad	.LVL8-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL8-1-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -116
	.quad	.LVL55-.Ltext0
	.quad	.LVL56-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL56-1-.Ltext0
	.quad	.LVL69-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -132
	.quad	.LVL70-.Ltext0
	.quad	.LVL79-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -132
	.quad	0
	.quad	0
.LVUS16:
	.uleb128 .LVU22
	.uleb128 .LVU25
.LLST16:
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x2
	.byte	0x40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS17:
	.uleb128 .LVU22
	.uleb128 .LVU25
.LLST17:
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS18:
	.uleb128 .LVU22
	.uleb128 .LVU25
.LLST18:
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS19:
	.uleb128 .LVU144
	.uleb128 .LVU147
	.uleb128 .LVU209
	.uleb128 .LVU212
.LLST19:
	.quad	.LVL44-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x2
	.byte	0x34
	.byte	0x9f
	.quad	.LVL72-.Ltext0
	.quad	.LVL73-.Ltext0
	.value	0x2
	.byte	0x34
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS20:
	.uleb128 .LVU144
	.uleb128 .LVU147
	.uleb128 .LVU209
	.uleb128 .LVU212
.LLST20:
	.quad	.LVL44-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x6
	.byte	0x71
	.sleb128 24
	.byte	0x6
	.byte	0x23
	.uleb128 0x4
	.byte	0x9f
	.quad	.LVL72-.Ltext0
	.quad	.LVL73-.Ltext0
	.value	0x6
	.byte	0x71
	.sleb128 24
	.byte	0x6
	.byte	0x23
	.uleb128 0x4
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS21:
	.uleb128 .LVU144
	.uleb128 .LVU147
	.uleb128 .LVU209
	.uleb128 .LVU212
.LLST21:
	.quad	.LVL44-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL72-.Ltext0
	.quad	.LVL73-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS22:
	.uleb128 .LVU154
	.uleb128 .LVU157
.LLST22:
	.quad	.LVL46-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x2
	.byte	0x34
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS23:
	.uleb128 .LVU154
	.uleb128 .LVU157
.LLST23:
	.quad	.LVL46-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x6
	.byte	0x71
	.sleb128 24
	.byte	0x6
	.byte	0x23
	.uleb128 0x4
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS24:
	.uleb128 .LVU154
	.uleb128 .LVU157
.LLST24:
	.quad	.LVL46-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x10
	.byte	0x74
	.sleb128 0
	.byte	0x8
	.byte	0x20
	.byte	0x24
	.byte	0x8
	.byte	0x20
	.byte	0x26
	.byte	0x33
	.byte	0x24
	.byte	0x76
	.sleb128 -144
	.byte	0x6
	.byte	0x22
	.byte	0x9f
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB8-.Ltext0
	.quad	.LBE8-.Ltext0
	.quad	.LBB12-.Ltext0
	.quad	.LBE12-.Ltext0
	.quad	.LBB13-.Ltext0
	.quad	.LBE13-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB14-.Ltext0
	.quad	.LBE14-.Ltext0
	.quad	.LBB19-.Ltext0
	.quad	.LBE19-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF5:
	.string	"short int"
.LASF46:
	.string	"_IO_read_ptr"
.LASF58:
	.string	"_chain"
.LASF35:
	.string	"sin6_addr"
.LASF111:
	.string	"__in6_u"
.LASF14:
	.string	"size_t"
.LASF156:
	.string	"aliases"
.LASF64:
	.string	"_shortbuf"
.LASF129:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF164:
	.string	"__ch"
.LASF52:
	.string	"_IO_buf_base"
.LASF180:
	.string	"__builtin_memset"
.LASF15:
	.string	"long long unsigned int"
.LASF103:
	.string	"in_addr_t"
.LASF142:
	.string	"ares_addrinfo_cname"
.LASF147:
	.string	"ares_malloc"
.LASF154:
	.string	"naddrttls"
.LASF168:
	.string	"__src"
.LASF17:
	.string	"socklen_t"
.LASF38:
	.string	"sockaddr_ipx"
.LASF145:
	.string	"next"
.LASF67:
	.string	"_codecvt"
.LASF60:
	.string	"_flags2"
.LASF131:
	.string	"ares_addrttl"
.LASF89:
	.string	"__timezone"
.LASF16:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF171:
	.string	"ares__freeaddrinfo_cnames"
.LASF37:
	.string	"sockaddr_inarp"
.LASF21:
	.string	"sockaddr_at"
.LASF59:
	.string	"_fileno"
.LASF126:
	.string	"cnames"
.LASF47:
	.string	"_IO_read_end"
.LASF177:
	.string	"_IO_lock_t"
.LASF83:
	.string	"_sys_nerr"
.LASF108:
	.string	"__u6_addr16"
.LASF121:
	.string	"sys_siglist"
.LASF9:
	.string	"long int"
.LASF45:
	.string	"_flags"
.LASF68:
	.string	"_wide_data"
.LASF53:
	.string	"_IO_buf_end"
.LASF167:
	.string	"memcpy"
.LASF86:
	.string	"program_invocation_short_name"
.LASF114:
	.string	"hostent"
.LASF23:
	.string	"sockaddr_dl"
.LASF33:
	.string	"sin6_port"
.LASF101:
	.string	"uint16_t"
.LASF84:
	.string	"_sys_errlist"
.LASF85:
	.string	"program_invocation_name"
.LASF61:
	.string	"_old_offset"
.LASF66:
	.string	"_offset"
.LASF149:
	.string	"ares_free"
.LASF124:
	.string	"shift"
.LASF113:
	.string	"in6addr_loopback"
.LASF42:
	.string	"sockaddr_x25"
.LASF7:
	.string	"__uint16_t"
.LASF135:
	.string	"ai_flags"
.LASF8:
	.string	"__uint32_t"
.LASF123:
	.string	"mask"
.LASF128:
	.string	"_S6_u8"
.LASF30:
	.string	"sin_zero"
.LASF75:
	.string	"_IO_marker"
.LASF152:
	.string	"host"
.LASF2:
	.string	"unsigned int"
.LASF105:
	.string	"s_addr"
.LASF70:
	.string	"_freeres_buf"
.LASF137:
	.string	"ai_socktype"
.LASF115:
	.string	"h_name"
.LASF122:
	.string	"_ns_flagdata"
.LASF50:
	.string	"_IO_write_ptr"
.LASF144:
	.string	"name"
.LASF125:
	.string	"ares_addrinfo"
.LASF81:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF29:
	.string	"sin_addr"
.LASF130:
	.string	"_S6_un"
.LASF176:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF54:
	.string	"_IO_save_base"
.LASF78:
	.string	"stdin"
.LASF117:
	.string	"h_addrtype"
.LASF95:
	.string	"environ"
.LASF65:
	.string	"_lock"
.LASF109:
	.string	"__u6_addr32"
.LASF106:
	.string	"in_port_t"
.LASF79:
	.string	"stdout"
.LASF161:
	.string	"cname_ttl"
.LASF41:
	.string	"sockaddr_un"
.LASF132:
	.string	"ipaddr"
.LASF27:
	.string	"sin_family"
.LASF134:
	.string	"ai_ttl"
.LASF96:
	.string	"optarg"
.LASF93:
	.string	"getdate_err"
.LASF32:
	.string	"sin6_family"
.LASF97:
	.string	"optind"
.LASF169:
	.string	"ares__parse_into_addrinfo2"
.LASF143:
	.string	"alias"
.LASF51:
	.string	"_IO_write_end"
.LASF138:
	.string	"ai_protocol"
.LASF163:
	.string	"__dest"
.LASF153:
	.string	"addrttls"
.LASF158:
	.string	"addrs"
.LASF40:
	.string	"sockaddr_ns"
.LASF112:
	.string	"in6addr_any"
.LASF44:
	.string	"_IO_FILE"
.LASF173:
	.string	"ares_free_hostent"
.LASF94:
	.string	"__environ"
.LASF88:
	.string	"__daylight"
.LASF139:
	.string	"ai_addrlen"
.LASF72:
	.string	"_mode"
.LASF116:
	.string	"h_aliases"
.LASF28:
	.string	"sin_port"
.LASF19:
	.string	"sa_family"
.LASF82:
	.string	"sys_errlist"
.LASF118:
	.string	"h_length"
.LASF119:
	.string	"h_addr_list"
.LASF36:
	.string	"sin6_scope_id"
.LASF0:
	.string	"unsigned char"
.LASF39:
	.string	"sockaddr_iso"
.LASF140:
	.string	"ai_addr"
.LASF179:
	.string	"enomem"
.LASF77:
	.string	"_IO_wide_data"
.LASF150:
	.string	"abuf"
.LASF165:
	.string	"__len"
.LASF133:
	.string	"ares_addrinfo_node"
.LASF141:
	.string	"ai_next"
.LASF63:
	.string	"_vtable_offset"
.LASF90:
	.string	"tzname"
.LASF22:
	.string	"sockaddr_ax25"
.LASF74:
	.string	"FILE"
.LASF57:
	.string	"_markers"
.LASF181:
	.string	"__stack_chk_fail"
.LASF110:
	.string	"in6_addr"
.LASF157:
	.string	"question_hostname"
.LASF99:
	.string	"optopt"
.LASF91:
	.string	"daylight"
.LASF178:
	.string	"ares_parse_a_reply"
.LASF3:
	.string	"long unsigned int"
.LASF12:
	.string	"char"
.LASF13:
	.string	"__socklen_t"
.LASF34:
	.string	"sin6_flowinfo"
.LASF151:
	.string	"alen"
.LASF155:
	.string	"next_cname"
.LASF107:
	.string	"__u6_addr8"
.LASF136:
	.string	"ai_family"
.LASF98:
	.string	"opterr"
.LASF76:
	.string	"_IO_codecvt"
.LASF127:
	.string	"nodes"
.LASF11:
	.string	"__off64_t"
.LASF62:
	.string	"_cur_column"
.LASF48:
	.string	"_IO_read_base"
.LASF56:
	.string	"_IO_save_end"
.LASF120:
	.string	"_sys_siglist"
.LASF175:
	.string	"../deps/cares/src/ares_parse_a_reply.c"
.LASF92:
	.string	"timezone"
.LASF24:
	.string	"sockaddr_eon"
.LASF71:
	.string	"__pad5"
.LASF148:
	.string	"ares_realloc"
.LASF18:
	.string	"sa_family_t"
.LASF73:
	.string	"_unused2"
.LASF80:
	.string	"stderr"
.LASF166:
	.string	"memset"
.LASF31:
	.string	"sockaddr_in6"
.LASF162:
	.string	"status"
.LASF25:
	.string	"sockaddr"
.LASF160:
	.string	"naddrs"
.LASF26:
	.string	"sockaddr_in"
.LASF170:
	.string	"strdup"
.LASF100:
	.string	"uint8_t"
.LASF172:
	.string	"ares__freeaddrinfo_nodes"
.LASF55:
	.string	"_IO_backup_base"
.LASF20:
	.string	"sa_data"
.LASF69:
	.string	"_freeres_list"
.LASF174:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF159:
	.string	"naliases"
.LASF146:
	.string	"ares_in6addr_any"
.LASF87:
	.string	"__tzname"
.LASF49:
	.string	"_IO_write_base"
.LASF43:
	.string	"ares_socklen_t"
.LASF102:
	.string	"uint32_t"
.LASF104:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
