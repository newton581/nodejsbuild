	.file	"ares_freeaddrinfo.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares__freeaddrinfo_cnames
	.type	ares__freeaddrinfo_cnames, @function
ares__freeaddrinfo_cnames:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares_freeaddrinfo.c"
	.loc 1 28 1 view -0
	.cfi_startproc
	.loc 1 28 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 29 3 is_stmt 1 view .LVU2
	.loc 1 30 3 view .LVU3
	.loc 1 30 9 view .LVU4
	testq	%rdi, %rdi
	je	.L9
	.loc 1 28 1 is_stmt 0 view .LVU5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
.LVL1:
	.p2align 4,,10
	.p2align 3
.L3:
	.loc 1 32 7 is_stmt 1 view .LVU6
	.loc 1 33 7 view .LVU7
	movq	%rbx, %r12
	.loc 1 33 12 is_stmt 0 view .LVU8
	movq	24(%rbx), %rbx
.LVL2:
	.loc 1 34 7 is_stmt 1 view .LVU9
	movq	8(%r12), %rdi
	call	*ares_free(%rip)
.LVL3:
	.loc 1 35 7 view .LVU10
	movq	16(%r12), %rdi
	call	*ares_free(%rip)
.LVL4:
	.loc 1 36 7 view .LVU11
	movq	%r12, %rdi
	call	*ares_free(%rip)
.LVL5:
	.loc 1 30 9 view .LVU12
	testq	%rbx, %rbx
	jne	.L3
	.loc 1 38 1 is_stmt 0 view .LVU13
	popq	%rbx
.LVL6:
	.loc 1 38 1 view .LVU14
	popq	%r12
.LVL7:
	.loc 1 38 1 view .LVU15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.LVL8:
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.loc 1 38 1 view .LVU16
	ret
	.cfi_endproc
.LFE87:
	.size	ares__freeaddrinfo_cnames, .-ares__freeaddrinfo_cnames
	.p2align 4
	.globl	ares__freeaddrinfo_nodes
	.type	ares__freeaddrinfo_nodes, @function
ares__freeaddrinfo_nodes:
.LVL9:
.LFB88:
	.loc 1 41 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 41 1 is_stmt 0 view .LVU18
	endbr64
	.loc 1 42 3 is_stmt 1 view .LVU19
	.loc 1 43 3 view .LVU20
	.loc 1 43 9 view .LVU21
	testq	%rdi, %rdi
	je	.L21
	.loc 1 41 1 is_stmt 0 view .LVU22
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r12
	pushq	%rbx
	.cfi_offset 12, -24
	.cfi_offset 3, -32
	movq	%rdi, %rbx
.LVL10:
	.p2align 4,,10
	.p2align 3
.L15:
	.loc 1 45 7 is_stmt 1 view .LVU23
	.loc 1 46 7 view .LVU24
	movq	%rbx, %r12
	.loc 1 46 12 is_stmt 0 view .LVU25
	movq	32(%rbx), %rbx
.LVL11:
	.loc 1 47 7 is_stmt 1 view .LVU26
	movq	24(%r12), %rdi
	call	*ares_free(%rip)
.LVL12:
	.loc 1 48 7 view .LVU27
	movq	%r12, %rdi
	call	*ares_free(%rip)
.LVL13:
	.loc 1 43 9 view .LVU28
	testq	%rbx, %rbx
	jne	.L15
	.loc 1 50 1 is_stmt 0 view .LVU29
	popq	%rbx
.LVL14:
	.loc 1 50 1 view .LVU30
	popq	%r12
.LVL15:
	.loc 1 50 1 view .LVU31
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.LVL16:
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.loc 1 50 1 view .LVU32
	ret
	.cfi_endproc
.LFE88:
	.size	ares__freeaddrinfo_nodes, .-ares__freeaddrinfo_nodes
	.p2align 4
	.globl	ares_freeaddrinfo
	.type	ares_freeaddrinfo, @function
ares_freeaddrinfo:
.LVL17:
.LFB89:
	.loc 1 53 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 53 1 is_stmt 0 view .LVU34
	endbr64
	.loc 1 54 3 is_stmt 1 view .LVU35
	.loc 1 53 1 is_stmt 0 view .LVU36
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	.cfi_offset 13, -24
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	.loc 1 54 3 view .LVU37
	movq	(%rdi), %rbx
.LVL18:
.LBB8:
.LBI8:
	.loc 1 27 6 is_stmt 1 view .LVU38
.LBB9:
	.loc 1 30 9 view .LVU39
	testq	%rbx, %rbx
	je	.L25
.LVL19:
	.p2align 4,,10
	.p2align 3
.L26:
	.loc 1 32 7 view .LVU40
	.loc 1 33 7 view .LVU41
	movq	%rbx, %r12
	.loc 1 33 12 is_stmt 0 view .LVU42
	movq	24(%rbx), %rbx
.LVL20:
	.loc 1 34 7 is_stmt 1 view .LVU43
	movq	8(%r12), %rdi
	call	*ares_free(%rip)
.LVL21:
	.loc 1 35 7 view .LVU44
	movq	16(%r12), %rdi
	call	*ares_free(%rip)
.LVL22:
	.loc 1 36 7 view .LVU45
	movq	%r12, %rdi
	call	*ares_free(%rip)
.LVL23:
	.loc 1 30 9 view .LVU46
	testq	%rbx, %rbx
	jne	.L26
.LVL24:
.L25:
	.loc 1 30 9 is_stmt 0 view .LVU47
.LBE9:
.LBE8:
	.loc 1 55 3 is_stmt 1 view .LVU48
	movq	8(%r13), %rbx
.LVL25:
.LBB10:
.LBI10:
	.loc 1 40 6 view .LVU49
.LBB11:
	.loc 1 42 3 view .LVU50
	.loc 1 43 3 view .LVU51
	.loc 1 43 9 view .LVU52
	testq	%rbx, %rbx
	je	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	.loc 1 45 7 view .LVU53
.LVL26:
	.loc 1 46 7 view .LVU54
	movq	%rbx, %r12
	.loc 1 46 12 is_stmt 0 view .LVU55
	movq	32(%rbx), %rbx
.LVL27:
	.loc 1 47 7 is_stmt 1 view .LVU56
	movq	24(%r12), %rdi
	call	*ares_free(%rip)
.LVL28:
	.loc 1 48 7 view .LVU57
	movq	%r12, %rdi
	call	*ares_free(%rip)
.LVL29:
	.loc 1 43 9 view .LVU58
	testq	%rbx, %rbx
	jne	.L28
.LVL30:
.L27:
	.loc 1 43 9 is_stmt 0 view .LVU59
.LBE11:
.LBE10:
	.loc 1 56 3 is_stmt 1 view .LVU60
	.loc 1 57 1 is_stmt 0 view .LVU61
	addq	$8, %rsp
	.loc 1 56 3 view .LVU62
	movq	%r13, %rdi
	.loc 1 57 1 view .LVU63
	popq	%rbx
	popq	%r12
	popq	%r13
.LVL31:
	.loc 1 57 1 view .LVU64
	popq	%rbp
	.cfi_def_cfa 7, 8
	.loc 1 56 3 view .LVU65
	jmp	*ares_free(%rip)
.LVL32:
	.loc 1 56 3 view .LVU66
	.cfi_endproc
.LFE89:
	.size	ares_freeaddrinfo, .-ares_freeaddrinfo
.Letext0:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 6 "/usr/include/netinet/in.h"
	.file 7 "../deps/cares/include/ares_build.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/errno.h"
	.file 13 "/usr/include/time.h"
	.file 14 "/usr/include/unistd.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 17 "../deps/cares/include/ares.h"
	.file 18 "../deps/cares/src/ares_ipv6.h"
	.file 19 "../deps/cares/src/ares_private.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xaf8
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF141
	.byte	0x1
	.long	.LASF142
	.long	.LASF143
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x2
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x2
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x2
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x2
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x2
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0xa9
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x7
	.long	0xa9
	.uleb128 0x3
	.long	.LASF13
	.byte	0x2
	.byte	0xd1
	.byte	0x16
	.long	0x3b
	.uleb128 0x3
	.long	.LASF14
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF15
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF16
	.uleb128 0x3
	.long	.LASF17
	.byte	0x4
	.byte	0x21
	.byte	0x15
	.long	0xb5
	.uleb128 0x3
	.long	.LASF18
	.byte	0x5
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF25
	.byte	0x10
	.byte	0x4
	.byte	0xb2
	.byte	0x8
	.long	0x11b
	.uleb128 0x9
	.long	.LASF19
	.byte	0x4
	.byte	0xb4
	.byte	0x11
	.long	0xe7
	.byte	0
	.uleb128 0x9
	.long	.LASF20
	.byte	0x4
	.byte	0xb5
	.byte	0xa
	.long	0x120
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0xf3
	.uleb128 0xa
	.long	0xa9
	.long	0x130
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xf3
	.uleb128 0xc
	.long	0x130
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x7
	.long	0x13b
	.uleb128 0x6
	.byte	0x8
	.long	0x13b
	.uleb128 0xc
	.long	0x145
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x7
	.long	0x150
	.uleb128 0x6
	.byte	0x8
	.long	0x150
	.uleb128 0xc
	.long	0x15a
	.uleb128 0xd
	.long	.LASF23
	.uleb128 0x7
	.long	0x165
	.uleb128 0x6
	.byte	0x8
	.long	0x165
	.uleb128 0xc
	.long	0x16f
	.uleb128 0xd
	.long	.LASF24
	.uleb128 0x7
	.long	0x17a
	.uleb128 0x6
	.byte	0x8
	.long	0x17a
	.uleb128 0xc
	.long	0x184
	.uleb128 0x8
	.long	.LASF26
	.byte	0x10
	.byte	0x6
	.byte	0xee
	.byte	0x8
	.long	0x1d1
	.uleb128 0x9
	.long	.LASF27
	.byte	0x6
	.byte	0xf0
	.byte	0x11
	.long	0xe7
	.byte	0
	.uleb128 0x9
	.long	.LASF28
	.byte	0x6
	.byte	0xf1
	.byte	0xf
	.long	0x6d2
	.byte	0x2
	.uleb128 0x9
	.long	.LASF29
	.byte	0x6
	.byte	0xf2
	.byte	0x14
	.long	0x6b7
	.byte	0x4
	.uleb128 0x9
	.long	.LASF30
	.byte	0x6
	.byte	0xf5
	.byte	0x13
	.long	0x774
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	0x18f
	.uleb128 0x6
	.byte	0x8
	.long	0x18f
	.uleb128 0xc
	.long	0x1d6
	.uleb128 0x8
	.long	.LASF31
	.byte	0x1c
	.byte	0x6
	.byte	0xfd
	.byte	0x8
	.long	0x234
	.uleb128 0x9
	.long	.LASF32
	.byte	0x6
	.byte	0xff
	.byte	0x11
	.long	0xe7
	.byte	0
	.uleb128 0xe
	.long	.LASF33
	.byte	0x6
	.value	0x100
	.byte	0xf
	.long	0x6d2
	.byte	0x2
	.uleb128 0xe
	.long	.LASF34
	.byte	0x6
	.value	0x101
	.byte	0xe
	.long	0x69f
	.byte	0x4
	.uleb128 0xe
	.long	.LASF35
	.byte	0x6
	.value	0x102
	.byte	0x15
	.long	0x73c
	.byte	0x8
	.uleb128 0xe
	.long	.LASF36
	.byte	0x6
	.value	0x103
	.byte	0xe
	.long	0x69f
	.byte	0x18
	.byte	0
	.uleb128 0x7
	.long	0x1e1
	.uleb128 0x6
	.byte	0x8
	.long	0x1e1
	.uleb128 0xc
	.long	0x239
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x7
	.long	0x244
	.uleb128 0x6
	.byte	0x8
	.long	0x244
	.uleb128 0xc
	.long	0x24e
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x7
	.long	0x259
	.uleb128 0x6
	.byte	0x8
	.long	0x259
	.uleb128 0xc
	.long	0x263
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x7
	.long	0x26e
	.uleb128 0x6
	.byte	0x8
	.long	0x26e
	.uleb128 0xc
	.long	0x278
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x7
	.long	0x283
	.uleb128 0x6
	.byte	0x8
	.long	0x283
	.uleb128 0xc
	.long	0x28d
	.uleb128 0xd
	.long	.LASF41
	.uleb128 0x7
	.long	0x298
	.uleb128 0x6
	.byte	0x8
	.long	0x298
	.uleb128 0xc
	.long	0x2a2
	.uleb128 0xd
	.long	.LASF42
	.uleb128 0x7
	.long	0x2ad
	.uleb128 0x6
	.byte	0x8
	.long	0x2ad
	.uleb128 0xc
	.long	0x2b7
	.uleb128 0x6
	.byte	0x8
	.long	0x11b
	.uleb128 0xc
	.long	0x2c2
	.uleb128 0x6
	.byte	0x8
	.long	0x140
	.uleb128 0xc
	.long	0x2cd
	.uleb128 0x6
	.byte	0x8
	.long	0x155
	.uleb128 0xc
	.long	0x2d8
	.uleb128 0x6
	.byte	0x8
	.long	0x16a
	.uleb128 0xc
	.long	0x2e3
	.uleb128 0x6
	.byte	0x8
	.long	0x17f
	.uleb128 0xc
	.long	0x2ee
	.uleb128 0x6
	.byte	0x8
	.long	0x1d1
	.uleb128 0xc
	.long	0x2f9
	.uleb128 0x6
	.byte	0x8
	.long	0x234
	.uleb128 0xc
	.long	0x304
	.uleb128 0x6
	.byte	0x8
	.long	0x249
	.uleb128 0xc
	.long	0x30f
	.uleb128 0x6
	.byte	0x8
	.long	0x25e
	.uleb128 0xc
	.long	0x31a
	.uleb128 0x6
	.byte	0x8
	.long	0x273
	.uleb128 0xc
	.long	0x325
	.uleb128 0x6
	.byte	0x8
	.long	0x288
	.uleb128 0xc
	.long	0x330
	.uleb128 0x6
	.byte	0x8
	.long	0x29d
	.uleb128 0xc
	.long	0x33b
	.uleb128 0x6
	.byte	0x8
	.long	0x2b2
	.uleb128 0xc
	.long	0x346
	.uleb128 0x3
	.long	.LASF43
	.byte	0x7
	.byte	0xbf
	.byte	0x14
	.long	0xdb
	.uleb128 0xa
	.long	0xa9
	.long	0x36d
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF44
	.byte	0xd8
	.byte	0x8
	.byte	0x31
	.byte	0x8
	.long	0x4f4
	.uleb128 0x9
	.long	.LASF45
	.byte	0x8
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0x9
	.long	.LASF46
	.byte	0x8
	.byte	0x36
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0x9
	.long	.LASF47
	.byte	0x8
	.byte	0x37
	.byte	0x9
	.long	0xa3
	.byte	0x10
	.uleb128 0x9
	.long	.LASF48
	.byte	0x8
	.byte	0x38
	.byte	0x9
	.long	0xa3
	.byte	0x18
	.uleb128 0x9
	.long	.LASF49
	.byte	0x8
	.byte	0x39
	.byte	0x9
	.long	0xa3
	.byte	0x20
	.uleb128 0x9
	.long	.LASF50
	.byte	0x8
	.byte	0x3a
	.byte	0x9
	.long	0xa3
	.byte	0x28
	.uleb128 0x9
	.long	.LASF51
	.byte	0x8
	.byte	0x3b
	.byte	0x9
	.long	0xa3
	.byte	0x30
	.uleb128 0x9
	.long	.LASF52
	.byte	0x8
	.byte	0x3c
	.byte	0x9
	.long	0xa3
	.byte	0x38
	.uleb128 0x9
	.long	.LASF53
	.byte	0x8
	.byte	0x3d
	.byte	0x9
	.long	0xa3
	.byte	0x40
	.uleb128 0x9
	.long	.LASF54
	.byte	0x8
	.byte	0x40
	.byte	0x9
	.long	0xa3
	.byte	0x48
	.uleb128 0x9
	.long	.LASF55
	.byte	0x8
	.byte	0x41
	.byte	0x9
	.long	0xa3
	.byte	0x50
	.uleb128 0x9
	.long	.LASF56
	.byte	0x8
	.byte	0x42
	.byte	0x9
	.long	0xa3
	.byte	0x58
	.uleb128 0x9
	.long	.LASF57
	.byte	0x8
	.byte	0x44
	.byte	0x16
	.long	0x50d
	.byte	0x60
	.uleb128 0x9
	.long	.LASF58
	.byte	0x8
	.byte	0x46
	.byte	0x14
	.long	0x513
	.byte	0x68
	.uleb128 0x9
	.long	.LASF59
	.byte	0x8
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0x9
	.long	.LASF60
	.byte	0x8
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0x9
	.long	.LASF61
	.byte	0x8
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0x9
	.long	.LASF62
	.byte	0x8
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF63
	.byte	0x8
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF64
	.byte	0x8
	.byte	0x4f
	.byte	0x8
	.long	0x35d
	.byte	0x83
	.uleb128 0x9
	.long	.LASF65
	.byte	0x8
	.byte	0x51
	.byte	0xf
	.long	0x519
	.byte	0x88
	.uleb128 0x9
	.long	.LASF66
	.byte	0x8
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0x9
	.long	.LASF67
	.byte	0x8
	.byte	0x5b
	.byte	0x17
	.long	0x524
	.byte	0x98
	.uleb128 0x9
	.long	.LASF68
	.byte	0x8
	.byte	0x5c
	.byte	0x19
	.long	0x52f
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF69
	.byte	0x8
	.byte	0x5d
	.byte	0x14
	.long	0x513
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF70
	.byte	0x8
	.byte	0x5e
	.byte	0x9
	.long	0xa1
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF71
	.byte	0x8
	.byte	0x5f
	.byte	0xa
	.long	0xc1
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF72
	.byte	0x8
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF73
	.byte	0x8
	.byte	0x62
	.byte	0x8
	.long	0x535
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF74
	.byte	0x9
	.byte	0x7
	.byte	0x19
	.long	0x36d
	.uleb128 0xf
	.long	.LASF144
	.byte	0x8
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF75
	.uleb128 0x6
	.byte	0x8
	.long	0x508
	.uleb128 0x6
	.byte	0x8
	.long	0x36d
	.uleb128 0x6
	.byte	0x8
	.long	0x500
	.uleb128 0xd
	.long	.LASF76
	.uleb128 0x6
	.byte	0x8
	.long	0x51f
	.uleb128 0xd
	.long	.LASF77
	.uleb128 0x6
	.byte	0x8
	.long	0x52a
	.uleb128 0xa
	.long	0xa9
	.long	0x545
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb0
	.uleb128 0x7
	.long	0x545
	.uleb128 0x10
	.long	.LASF78
	.byte	0xa
	.byte	0x89
	.byte	0xe
	.long	0x55c
	.uleb128 0x6
	.byte	0x8
	.long	0x4f4
	.uleb128 0x10
	.long	.LASF79
	.byte	0xa
	.byte	0x8a
	.byte	0xe
	.long	0x55c
	.uleb128 0x10
	.long	.LASF80
	.byte	0xa
	.byte	0x8b
	.byte	0xe
	.long	0x55c
	.uleb128 0x10
	.long	.LASF81
	.byte	0xb
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xa
	.long	0x54b
	.long	0x591
	.uleb128 0x11
	.byte	0
	.uleb128 0x7
	.long	0x586
	.uleb128 0x10
	.long	.LASF82
	.byte	0xb
	.byte	0x1b
	.byte	0x1a
	.long	0x591
	.uleb128 0x10
	.long	.LASF83
	.byte	0xb
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF84
	.byte	0xb
	.byte	0x1f
	.byte	0x1a
	.long	0x591
	.uleb128 0x10
	.long	.LASF85
	.byte	0xc
	.byte	0x2d
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF86
	.byte	0xc
	.byte	0x2e
	.byte	0xe
	.long	0xa3
	.uleb128 0xa
	.long	0xa3
	.long	0x5e2
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF87
	.byte	0xd
	.byte	0x9f
	.byte	0xe
	.long	0x5d2
	.uleb128 0x10
	.long	.LASF88
	.byte	0xd
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF89
	.byte	0xd
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF90
	.byte	0xd
	.byte	0xa6
	.byte	0xe
	.long	0x5d2
	.uleb128 0x10
	.long	.LASF91
	.byte	0xd
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF92
	.byte	0xd
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x12
	.long	.LASF93
	.byte	0xd
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x12
	.long	.LASF94
	.byte	0xe
	.value	0x21f
	.byte	0xf
	.long	0x644
	.uleb128 0x6
	.byte	0x8
	.long	0xa3
	.uleb128 0x12
	.long	.LASF95
	.byte	0xe
	.value	0x221
	.byte	0xf
	.long	0x644
	.uleb128 0x10
	.long	.LASF96
	.byte	0xf
	.byte	0x24
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF97
	.byte	0xf
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF98
	.byte	0xf
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF99
	.byte	0xf
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF100
	.byte	0x10
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF101
	.byte	0x10
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF102
	.byte	0x10
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF103
	.byte	0x6
	.byte	0x1e
	.byte	0x12
	.long	0x69f
	.uleb128 0x8
	.long	.LASF104
	.byte	0x4
	.byte	0x6
	.byte	0x1f
	.byte	0x8
	.long	0x6d2
	.uleb128 0x9
	.long	.LASF105
	.byte	0x6
	.byte	0x21
	.byte	0xf
	.long	0x6ab
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF106
	.byte	0x6
	.byte	0x77
	.byte	0x12
	.long	0x693
	.uleb128 0x13
	.byte	0x10
	.byte	0x6
	.byte	0xd6
	.byte	0x5
	.long	0x70c
	.uleb128 0x14
	.long	.LASF107
	.byte	0x6
	.byte	0xd8
	.byte	0xa
	.long	0x70c
	.uleb128 0x14
	.long	.LASF108
	.byte	0x6
	.byte	0xd9
	.byte	0xb
	.long	0x71c
	.uleb128 0x14
	.long	.LASF109
	.byte	0x6
	.byte	0xda
	.byte	0xb
	.long	0x72c
	.byte	0
	.uleb128 0xa
	.long	0x687
	.long	0x71c
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x693
	.long	0x72c
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x69f
	.long	0x73c
	.uleb128 0xb
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.long	.LASF110
	.byte	0x10
	.byte	0x6
	.byte	0xd4
	.byte	0x8
	.long	0x757
	.uleb128 0x9
	.long	.LASF111
	.byte	0x6
	.byte	0xdb
	.byte	0x9
	.long	0x6de
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x73c
	.uleb128 0x10
	.long	.LASF112
	.byte	0x6
	.byte	0xe4
	.byte	0x1e
	.long	0x757
	.uleb128 0x10
	.long	.LASF113
	.byte	0x6
	.byte	0xe5
	.byte	0x1e
	.long	0x757
	.uleb128 0xa
	.long	0x2d
	.long	0x784
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x78a
	.uleb128 0x15
	.long	.LASF114
	.byte	0x10
	.byte	0x11
	.value	0x260
	.byte	0x8
	.long	0x7b5
	.uleb128 0xe
	.long	.LASF115
	.byte	0x11
	.value	0x261
	.byte	0x1f
	.long	0x8cb
	.byte	0
	.uleb128 0xe
	.long	.LASF116
	.byte	0x11
	.value	0x262
	.byte	0x1e
	.long	0x87e
	.byte	0x8
	.byte	0
	.uleb128 0x16
	.byte	0x10
	.byte	0x11
	.value	0x204
	.byte	0x3
	.long	0x7cd
	.uleb128 0x17
	.long	.LASF117
	.byte	0x11
	.value	0x205
	.byte	0x13
	.long	0x7cd
	.byte	0
	.uleb128 0xa
	.long	0x2d
	.long	0x7dd
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x15
	.long	.LASF118
	.byte	0x10
	.byte	0x11
	.value	0x203
	.byte	0x8
	.long	0x7fa
	.uleb128 0xe
	.long	.LASF119
	.byte	0x11
	.value	0x206
	.byte	0x5
	.long	0x7b5
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x7dd
	.uleb128 0x15
	.long	.LASF120
	.byte	0x28
	.byte	0x11
	.value	0x249
	.byte	0x8
	.long	0x87e
	.uleb128 0xe
	.long	.LASF121
	.byte	0x11
	.value	0x24a
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0xe
	.long	.LASF122
	.byte	0x11
	.value	0x24b
	.byte	0x7
	.long	0x6f
	.byte	0x4
	.uleb128 0xe
	.long	.LASF123
	.byte	0x11
	.value	0x24c
	.byte	0x7
	.long	0x6f
	.byte	0x8
	.uleb128 0xe
	.long	.LASF124
	.byte	0x11
	.value	0x24d
	.byte	0x7
	.long	0x6f
	.byte	0xc
	.uleb128 0xe
	.long	.LASF125
	.byte	0x11
	.value	0x24e
	.byte	0x7
	.long	0x6f
	.byte	0x10
	.uleb128 0xe
	.long	.LASF126
	.byte	0x11
	.value	0x24f
	.byte	0x12
	.long	0x351
	.byte	0x14
	.uleb128 0xe
	.long	.LASF127
	.byte	0x11
	.value	0x250
	.byte	0x14
	.long	0x130
	.byte	0x18
	.uleb128 0xe
	.long	.LASF128
	.byte	0x11
	.value	0x251
	.byte	0x1e
	.long	0x87e
	.byte	0x20
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x7ff
	.uleb128 0x15
	.long	.LASF129
	.byte	0x20
	.byte	0x11
	.value	0x259
	.byte	0x8
	.long	0x8cb
	.uleb128 0x18
	.string	"ttl"
	.byte	0x11
	.value	0x25a
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0xe
	.long	.LASF130
	.byte	0x11
	.value	0x25b
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0xe
	.long	.LASF131
	.byte	0x11
	.value	0x25c
	.byte	0x9
	.long	0xa3
	.byte	0x10
	.uleb128 0xe
	.long	.LASF132
	.byte	0x11
	.value	0x25d
	.byte	0x1f
	.long	0x8cb
	.byte	0x18
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x884
	.uleb128 0x10
	.long	.LASF133
	.byte	0x12
	.byte	0x52
	.byte	0x23
	.long	0x7fa
	.uleb128 0x19
	.long	0xa1
	.long	0x8ec
	.uleb128 0x1a
	.long	0xc1
	.byte	0
	.uleb128 0x12
	.long	.LASF134
	.byte	0x13
	.value	0x151
	.byte	0x10
	.long	0x8f9
	.uleb128 0x6
	.byte	0x8
	.long	0x8dd
	.uleb128 0x19
	.long	0xa1
	.long	0x913
	.uleb128 0x1a
	.long	0xa1
	.uleb128 0x1a
	.long	0xc1
	.byte	0
	.uleb128 0x12
	.long	.LASF135
	.byte	0x13
	.value	0x152
	.byte	0x10
	.long	0x920
	.uleb128 0x6
	.byte	0x8
	.long	0x8ff
	.uleb128 0x1b
	.long	0x931
	.uleb128 0x1a
	.long	0xa1
	.byte	0
	.uleb128 0x12
	.long	.LASF136
	.byte	0x13
	.value	0x153
	.byte	0xf
	.long	0x93e
	.uleb128 0x6
	.byte	0x8
	.long	0x926
	.uleb128 0x1c
	.long	.LASF145
	.byte	0x1
	.byte	0x34
	.byte	0x6
	.quad	.LFB89
	.quad	.LFE89-.LFB89
	.uleb128 0x1
	.byte	0x9c
	.long	0xa27
	.uleb128 0x1d
	.string	"ai"
	.byte	0x1
	.byte	0x34
	.byte	0x2e
	.long	0x784
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x1e
	.long	0xa4d
	.quad	.LBI8
	.byte	.LVU38
	.quad	.LBB8
	.quad	.LBE8-.LBB8
	.byte	0x1
	.byte	0x36
	.byte	0x3
	.long	0x9c5
	.uleb128 0x1f
	.long	0xa5a
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x20
	.long	0xa66
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x21
	.quad	.LVL23
	.uleb128 0x22
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x1e
	.long	0xa27
	.quad	.LBI10
	.byte	.LVU49
	.quad	.LBB10
	.quad	.LBE10-.LBB10
	.byte	0x1
	.byte	0x37
	.byte	0x3
	.long	0xa15
	.uleb128 0x1f
	.long	0xa34
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x20
	.long	0xa40
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x21
	.quad	.LVL29
	.uleb128 0x22
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x23
	.quad	.LVL32
	.uleb128 0x22
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.byte	0
	.uleb128 0x24
	.long	.LASF137
	.byte	0x1
	.byte	0x28
	.byte	0x6
	.byte	0x1
	.long	0xa4d
	.uleb128 0x25
	.long	.LASF139
	.byte	0x1
	.byte	0x28
	.byte	0x3a
	.long	0x87e
	.uleb128 0x26
	.long	.LASF140
	.byte	0x1
	.byte	0x2a
	.byte	0x1e
	.long	0x87e
	.byte	0
	.uleb128 0x24
	.long	.LASF138
	.byte	0x1
	.byte	0x1b
	.byte	0x6
	.byte	0x1
	.long	0xa73
	.uleb128 0x25
	.long	.LASF139
	.byte	0x1
	.byte	0x1b
	.byte	0x3c
	.long	0x8cb
	.uleb128 0x26
	.long	.LASF140
	.byte	0x1
	.byte	0x1d
	.byte	0x1f
	.long	0x8cb
	.byte	0
	.uleb128 0x27
	.long	0xa4d
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0xab9
	.uleb128 0x1f
	.long	0xa5a
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x20
	.long	0xa66
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x21
	.quad	.LVL5
	.uleb128 0x22
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x28
	.long	0xa27
	.quad	.LFB88
	.quad	.LFE88-.LFB88
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x1f
	.long	0xa34
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x20
	.long	0xa40
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x21
	.quad	.LVL13
	.uleb128 0x22
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x2115
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS4:
	.uleb128 0
	.uleb128 .LVU40
	.uleb128 .LVU40
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 0
.LLST4:
	.quad	.LVL17-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL19-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL31-.Ltext0
	.quad	.LVL32-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL32-1-.Ltext0
	.quad	.LFE89-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU38
	.uleb128 .LVU47
.LLST5:
	.quad	.LVL18-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU41
	.uleb128 .LVU43
	.uleb128 .LVU43
	.uleb128 .LVU47
.LLST6:
	.quad	.LVL19-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL20-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU49
	.uleb128 .LVU59
.LLST7:
	.quad	.LVL25-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU54
	.uleb128 .LVU56
	.uleb128 .LVU56
	.uleb128 .LVU59
.LLST8:
	.quad	.LVL26-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL27-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS0:
	.uleb128 0
	.uleb128 .LVU6
	.uleb128 .LVU6
	.uleb128 .LVU14
	.uleb128 .LVU16
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL1-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL8-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 .LVU7
	.uleb128 .LVU9
	.uleb128 .LVU9
	.uleb128 .LVU15
.LLST1:
	.quad	.LVL1-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL2-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU23
	.uleb128 .LVU23
	.uleb128 .LVU30
	.uleb128 .LVU32
	.uleb128 0
.LLST2:
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL10-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL16-.Ltext0
	.quad	.LFE88-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU24
	.uleb128 .LVU26
	.uleb128 .LVU26
	.uleb128 .LVU31
.LLST3:
	.quad	.LVL10-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL11-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF46:
	.string	"_IO_read_ptr"
.LASF58:
	.string	"_chain"
.LASF122:
	.string	"ai_flags"
.LASF111:
	.string	"__in6_u"
.LASF14:
	.string	"size_t"
.LASF64:
	.string	"_shortbuf"
.LASF118:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF52:
	.string	"_IO_buf_base"
.LASF15:
	.string	"long long unsigned int"
.LASF103:
	.string	"in_addr_t"
.LASF129:
	.string	"ares_addrinfo_cname"
.LASF134:
	.string	"ares_malloc"
.LASF17:
	.string	"socklen_t"
.LASF132:
	.string	"next"
.LASF67:
	.string	"_codecvt"
.LASF89:
	.string	"__timezone"
.LASF16:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF138:
	.string	"ares__freeaddrinfo_cnames"
.LASF37:
	.string	"sockaddr_inarp"
.LASF59:
	.string	"_fileno"
.LASF47:
	.string	"_IO_read_end"
.LASF136:
	.string	"ares_free"
.LASF108:
	.string	"__u6_addr16"
.LASF9:
	.string	"long int"
.LASF109:
	.string	"__u6_addr32"
.LASF45:
	.string	"_flags"
.LASF68:
	.string	"_wide_data"
.LASF53:
	.string	"_IO_buf_end"
.LASF62:
	.string	"_cur_column"
.LASF86:
	.string	"program_invocation_short_name"
.LASF76:
	.string	"_IO_codecvt"
.LASF23:
	.string	"sockaddr_dl"
.LASF33:
	.string	"sin6_port"
.LASF101:
	.string	"uint16_t"
.LASF84:
	.string	"_sys_errlist"
.LASF85:
	.string	"program_invocation_name"
.LASF61:
	.string	"_old_offset"
.LASF66:
	.string	"_offset"
.LASF145:
	.string	"ares_freeaddrinfo"
.LASF113:
	.string	"in6addr_loopback"
.LASF42:
	.string	"sockaddr_x25"
.LASF38:
	.string	"sockaddr_ipx"
.LASF8:
	.string	"__uint32_t"
.LASF92:
	.string	"timezone"
.LASF30:
	.string	"sin_zero"
.LASF71:
	.string	"__pad5"
.LASF75:
	.string	"_IO_marker"
.LASF78:
	.string	"stdin"
.LASF2:
	.string	"unsigned int"
.LASF105:
	.string	"s_addr"
.LASF70:
	.string	"_freeres_buf"
.LASF142:
	.string	"../deps/cares/src/ares_freeaddrinfo.c"
.LASF124:
	.string	"ai_socktype"
.LASF3:
	.string	"long unsigned int"
.LASF50:
	.string	"_IO_write_ptr"
.LASF131:
	.string	"name"
.LASF114:
	.string	"ares_addrinfo"
.LASF91:
	.string	"daylight"
.LASF81:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF29:
	.string	"sin_addr"
.LASF119:
	.string	"_S6_un"
.LASF143:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF54:
	.string	"_IO_save_base"
.LASF95:
	.string	"environ"
.LASF65:
	.string	"_lock"
.LASF60:
	.string	"_flags2"
.LASF72:
	.string	"_mode"
.LASF79:
	.string	"stdout"
.LASF41:
	.string	"sockaddr_un"
.LASF27:
	.string	"sin_family"
.LASF121:
	.string	"ai_ttl"
.LASF96:
	.string	"optarg"
.LASF93:
	.string	"getdate_err"
.LASF32:
	.string	"sin6_family"
.LASF97:
	.string	"optind"
.LASF130:
	.string	"alias"
.LASF51:
	.string	"_IO_write_end"
.LASF125:
	.string	"ai_protocol"
.LASF144:
	.string	"_IO_lock_t"
.LASF112:
	.string	"in6addr_any"
.LASF44:
	.string	"_IO_FILE"
.LASF94:
	.string	"__environ"
.LASF88:
	.string	"__daylight"
.LASF80:
	.string	"stderr"
.LASF126:
	.string	"ai_addrlen"
.LASF40:
	.string	"sockaddr_ns"
.LASF28:
	.string	"sin_port"
.LASF19:
	.string	"sa_family"
.LASF82:
	.string	"sys_errlist"
.LASF57:
	.string	"_markers"
.LASF36:
	.string	"sin6_scope_id"
.LASF0:
	.string	"unsigned char"
.LASF39:
	.string	"sockaddr_iso"
.LASF127:
	.string	"ai_addr"
.LASF5:
	.string	"short int"
.LASF83:
	.string	"_sys_nerr"
.LASF128:
	.string	"ai_next"
.LASF63:
	.string	"_vtable_offset"
.LASF90:
	.string	"tzname"
.LASF22:
	.string	"sockaddr_ax25"
.LASF74:
	.string	"FILE"
.LASF140:
	.string	"current"
.LASF110:
	.string	"in6_addr"
.LASF139:
	.string	"head"
.LASF99:
	.string	"optopt"
.LASF102:
	.string	"uint32_t"
.LASF12:
	.string	"char"
.LASF13:
	.string	"__socklen_t"
.LASF34:
	.string	"sin6_flowinfo"
.LASF7:
	.string	"__uint16_t"
.LASF107:
	.string	"__u6_addr8"
.LASF123:
	.string	"ai_family"
.LASF98:
	.string	"opterr"
.LASF115:
	.string	"cnames"
.LASF116:
	.string	"nodes"
.LASF11:
	.string	"__off64_t"
.LASF48:
	.string	"_IO_read_base"
.LASF56:
	.string	"_IO_save_end"
.LASF24:
	.string	"sockaddr_eon"
.LASF21:
	.string	"sockaddr_at"
.LASF135:
	.string	"ares_realloc"
.LASF18:
	.string	"sa_family_t"
.LASF73:
	.string	"_unused2"
.LASF117:
	.string	"_S6_u8"
.LASF31:
	.string	"sockaddr_in6"
.LASF120:
	.string	"ares_addrinfo_node"
.LASF26:
	.string	"sockaddr_in"
.LASF100:
	.string	"uint8_t"
.LASF137:
	.string	"ares__freeaddrinfo_nodes"
.LASF55:
	.string	"_IO_backup_base"
.LASF20:
	.string	"sa_data"
.LASF69:
	.string	"_freeres_list"
.LASF141:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF77:
	.string	"_IO_wide_data"
.LASF35:
	.string	"sin6_addr"
.LASF133:
	.string	"ares_in6addr_any"
.LASF87:
	.string	"__tzname"
.LASF49:
	.string	"_IO_write_base"
.LASF43:
	.string	"ares_socklen_t"
.LASF106:
	.string	"in_port_t"
.LASF25:
	.string	"sockaddr"
.LASF104:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
