	.file	"bitncmp.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares__bitncmp
	.type	ares__bitncmp, @function
ares__bitncmp:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/bitncmp.c"
	.loc 1 37 1 view -0
	.cfi_startproc
	.loc 1 37 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 38 3 is_stmt 1 view .LVU2
	.loc 1 39 3 view .LVU3
	.loc 1 41 3 view .LVU4
.LVL1:
	.loc 1 42 3 view .LVU5
	.loc 1 37 1 is_stmt 0 view .LVU6
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 41 5 view .LVU7
	testl	%edx, %edx
	.loc 1 37 1 view .LVU8
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r14
	.cfi_offset 14, -24
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -32
	movq	%rsi, %r13
	pushq	%r12
	.cfi_offset 12, -40
	.loc 1 41 5 view .LVU9
	leal	7(%rdx), %r12d
	cmovns	%edx, %r12d
	.loc 1 37 1 view .LVU10
	pushq	%rbx
	.cfi_offset 3, -48
	.loc 1 37 1 view .LVU11
	movl	%edx, %ebx
	.loc 1 41 5 view .LVU12
	sarl	$3, %r12d
	.loc 1 42 7 view .LVU13
	movslq	%r12d, %r12
	movq	%r12, %rdx
.LVL2:
	.loc 1 42 7 view .LVU14
	call	memcmp@PLT
.LVL3:
	.loc 1 43 3 is_stmt 1 view .LVU15
	.loc 1 43 6 is_stmt 0 view .LVU16
	testl	%eax, %eax
	jne	.L1
	.loc 1 43 9 discriminator 1 view .LVU17
	testb	$7, %bl
	je	.L1
	.loc 1 46 3 is_stmt 1 view .LVU18
	.loc 1 48 10 is_stmt 0 view .LVU19
	movl	%ebx, %ecx
	.loc 1 46 34 view .LVU20
	movzbl	(%r14,%r12), %esi
	.loc 1 47 34 view .LVU21
	movzbl	0(%r13,%r12), %edx
	.loc 1 48 10 view .LVU22
	sarl	$31, %ecx
	shrl	$29, %ecx
	.loc 1 46 34 view .LVU23
	movl	%esi, %r8d
.LVL4:
	.loc 1 47 3 is_stmt 1 view .LVU24
	.loc 1 48 3 view .LVU25
	.loc 1 48 10 is_stmt 0 view .LVU26
	addl	%ecx, %ebx
.LVL5:
	.loc 1 48 10 view .LVU27
	andl	$7, %ebx
	subl	%ecx, %ebx
.LVL6:
	.loc 1 48 19 is_stmt 1 view .LVU28
	.loc 1 48 3 is_stmt 0 view .LVU29
	testl	%ebx, %ebx
	jle	.L1
	.loc 1 49 5 is_stmt 1 view .LVU30
	.loc 1 49 8 is_stmt 0 view .LVU31
	xorb	%dl, %r8b
	js	.L3
	.loc 1 54 5 is_stmt 1 view .LVU32
	.loc 1 54 8 is_stmt 0 view .LVU33
	leal	(%rsi,%rsi), %edi
.LVL7:
	.loc 1 55 5 is_stmt 1 view .LVU34
	.loc 1 55 8 is_stmt 0 view .LVU35
	leal	(%rdx,%rdx), %ecx
.LVL8:
	.loc 1 48 26 is_stmt 1 view .LVU36
	.loc 1 48 19 view .LVU37
	.loc 1 48 3 is_stmt 0 view .LVU38
	cmpl	$1, %ebx
	je	.L1
	.loc 1 49 5 is_stmt 1 view .LVU39
	.loc 1 49 21 is_stmt 0 view .LVU40
	xorl	%edi, %ecx
	.loc 1 49 8 view .LVU41
	andl	$128, %ecx
	jne	.L16
	.loc 1 54 5 is_stmt 1 view .LVU42
	.loc 1 54 8 is_stmt 0 view .LVU43
	leal	0(,%rsi,4), %edi
.LVL9:
	.loc 1 55 5 is_stmt 1 view .LVU44
	.loc 1 55 8 is_stmt 0 view .LVU45
	leal	0(,%rdx,4), %ecx
.LVL10:
	.loc 1 48 26 is_stmt 1 view .LVU46
	.loc 1 48 19 view .LVU47
	.loc 1 48 3 is_stmt 0 view .LVU48
	cmpl	$2, %ebx
	je	.L1
	.loc 1 49 5 is_stmt 1 view .LVU49
	.loc 1 49 21 is_stmt 0 view .LVU50
	xorl	%edi, %ecx
	.loc 1 49 8 view .LVU51
	andl	$128, %ecx
	jne	.L16
	.loc 1 54 5 is_stmt 1 view .LVU52
	.loc 1 54 8 is_stmt 0 view .LVU53
	leal	0(,%rsi,8), %edi
.LVL11:
	.loc 1 55 5 is_stmt 1 view .LVU54
	.loc 1 55 8 is_stmt 0 view .LVU55
	leal	0(,%rdx,8), %ecx
.LVL12:
	.loc 1 48 26 is_stmt 1 view .LVU56
	.loc 1 48 19 view .LVU57
	.loc 1 48 3 is_stmt 0 view .LVU58
	cmpl	$3, %ebx
	je	.L1
	.loc 1 49 5 is_stmt 1 view .LVU59
	.loc 1 49 21 is_stmt 0 view .LVU60
	xorl	%edi, %ecx
	.loc 1 49 8 view .LVU61
	andl	$128, %ecx
	jne	.L16
	.loc 1 54 5 is_stmt 1 view .LVU62
	.loc 1 54 8 is_stmt 0 view .LVU63
	movl	%esi, %edi
	.loc 1 55 8 view .LVU64
	movl	%edx, %ecx
	.loc 1 54 8 view .LVU65
	sall	$4, %edi
.LVL13:
	.loc 1 55 5 is_stmt 1 view .LVU66
	.loc 1 55 8 is_stmt 0 view .LVU67
	sall	$4, %ecx
.LVL14:
	.loc 1 48 26 is_stmt 1 view .LVU68
	.loc 1 48 19 view .LVU69
	.loc 1 48 3 is_stmt 0 view .LVU70
	cmpl	$4, %ebx
	je	.L15
	.loc 1 49 5 is_stmt 1 view .LVU71
	.loc 1 49 21 is_stmt 0 view .LVU72
	xorl	%edi, %ecx
.LVL15:
	.loc 1 49 8 view .LVU73
	andl	$128, %ecx
	jne	.L16
	.loc 1 54 5 is_stmt 1 view .LVU74
	.loc 1 54 8 is_stmt 0 view .LVU75
	movl	%esi, %edi
.LVL16:
	.loc 1 55 8 view .LVU76
	movl	%edx, %ecx
	.loc 1 54 8 view .LVU77
	sall	$5, %edi
.LVL17:
	.loc 1 55 5 is_stmt 1 view .LVU78
	.loc 1 55 8 is_stmt 0 view .LVU79
	sall	$5, %ecx
.LVL18:
	.loc 1 48 26 is_stmt 1 view .LVU80
	.loc 1 48 19 view .LVU81
	.loc 1 48 3 is_stmt 0 view .LVU82
	cmpl	$5, %ebx
	je	.L15
	.loc 1 49 5 is_stmt 1 view .LVU83
	.loc 1 49 21 is_stmt 0 view .LVU84
	xorl	%edi, %ecx
.LVL19:
	.loc 1 49 8 view .LVU85
	andl	$128, %ecx
	jne	.L16
	.loc 1 54 5 is_stmt 1 view .LVU86
	.loc 1 54 8 is_stmt 0 view .LVU87
	sall	$6, %esi
.LVL20:
	.loc 1 55 5 is_stmt 1 view .LVU88
	.loc 1 55 8 is_stmt 0 view .LVU89
	sall	$6, %edx
.LVL21:
	.loc 1 48 26 is_stmt 1 view .LVU90
	.loc 1 48 19 view .LVU91
	.loc 1 48 3 is_stmt 0 view .LVU92
	cmpl	$7, %ebx
	jne	.L1
	.loc 1 49 5 is_stmt 1 view .LVU93
	.loc 1 49 21 is_stmt 0 view .LVU94
	xorl	%esi, %edx
.LVL22:
	.loc 1 49 8 view .LVU95
	andl	$128, %edx
	jne	.L3
.LVL23:
.L1:
	.loc 1 58 1 view .LVU96
	popq	%rbx
	popq	%r12
	popq	%r13
.LVL24:
	.loc 1 58 1 view .LVU97
	popq	%r14
.LVL25:
	.loc 1 58 1 view .LVU98
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL26:
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
	.loc 1 54 8 view .LVU99
	movl	%edi, %esi
.L3:
	.loc 1 50 7 is_stmt 1 view .LVU100
	.loc 1 50 14 is_stmt 0 view .LVU101
	andl	$128, %esi
	.loc 1 58 1 view .LVU102
	popq	%rbx
	popq	%r12
	.loc 1 52 14 view .LVU103
	cmpl	$1, %esi
	.loc 1 58 1 view .LVU104
	popq	%r13
.LVL27:
	.loc 1 58 1 view .LVU105
	popq	%r14
.LVL28:
	.loc 1 52 14 view .LVU106
	sbbl	%eax, %eax
.LVL29:
	.loc 1 58 1 view .LVU107
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	.loc 1 52 14 view .LVU108
	orl	$1, %eax
	.loc 1 58 1 view .LVU109
	ret
.LVL30:
.L15:
	.cfi_restore_state
	.loc 1 48 3 view .LVU110
	xorl	%eax, %eax
.LVL31:
	.loc 1 48 3 view .LVU111
	jmp	.L1
	.cfi_endproc
.LFE87:
	.size	ares__bitncmp, .-ares__bitncmp
.Letext0:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 8 "/usr/include/stdio.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 10 "/usr/include/errno.h"
	.file 11 "/usr/include/time.h"
	.file 12 "/usr/include/unistd.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 14 "/usr/include/string.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x687
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF85
	.byte	0x1
	.long	.LASF86
	.long	.LASF87
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF6
	.uleb128 0x4
	.long	.LASF7
	.byte	0x2
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x4
	.long	.LASF8
	.byte	0x2
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0x85
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF9
	.uleb128 0x7
	.long	0x85
	.uleb128 0x4
	.long	.LASF10
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF11
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF12
	.uleb128 0x4
	.long	.LASF13
	.byte	0x4
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF28
	.byte	0x10
	.byte	0x5
	.byte	0xb2
	.byte	0x8
	.long	0xdf
	.uleb128 0x9
	.long	.LASF14
	.byte	0x5
	.byte	0xb4
	.byte	0x11
	.long	0xab
	.byte	0
	.uleb128 0x9
	.long	.LASF15
	.byte	0x5
	.byte	0xb5
	.byte	0xa
	.long	0xe4
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0xb7
	.uleb128 0xa
	.long	0x85
	.long	0xf4
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb7
	.uleb128 0xc
	.long	0xf4
	.uleb128 0xd
	.long	.LASF16
	.uleb128 0x7
	.long	0xff
	.uleb128 0x6
	.byte	0x8
	.long	0xff
	.uleb128 0xc
	.long	0x109
	.uleb128 0xd
	.long	.LASF17
	.uleb128 0x7
	.long	0x114
	.uleb128 0x6
	.byte	0x8
	.long	0x114
	.uleb128 0xc
	.long	0x11e
	.uleb128 0xd
	.long	.LASF18
	.uleb128 0x7
	.long	0x129
	.uleb128 0x6
	.byte	0x8
	.long	0x129
	.uleb128 0xc
	.long	0x133
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x7
	.long	0x13e
	.uleb128 0x6
	.byte	0x8
	.long	0x13e
	.uleb128 0xc
	.long	0x148
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x7
	.long	0x153
	.uleb128 0x6
	.byte	0x8
	.long	0x153
	.uleb128 0xc
	.long	0x15d
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x7
	.long	0x168
	.uleb128 0x6
	.byte	0x8
	.long	0x168
	.uleb128 0xc
	.long	0x172
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x7
	.long	0x17d
	.uleb128 0x6
	.byte	0x8
	.long	0x17d
	.uleb128 0xc
	.long	0x187
	.uleb128 0xd
	.long	.LASF23
	.uleb128 0x7
	.long	0x192
	.uleb128 0x6
	.byte	0x8
	.long	0x192
	.uleb128 0xc
	.long	0x19c
	.uleb128 0xd
	.long	.LASF24
	.uleb128 0x7
	.long	0x1a7
	.uleb128 0x6
	.byte	0x8
	.long	0x1a7
	.uleb128 0xc
	.long	0x1b1
	.uleb128 0xd
	.long	.LASF25
	.uleb128 0x7
	.long	0x1bc
	.uleb128 0x6
	.byte	0x8
	.long	0x1bc
	.uleb128 0xc
	.long	0x1c6
	.uleb128 0xd
	.long	.LASF26
	.uleb128 0x7
	.long	0x1d1
	.uleb128 0x6
	.byte	0x8
	.long	0x1d1
	.uleb128 0xc
	.long	0x1db
	.uleb128 0xd
	.long	.LASF27
	.uleb128 0x7
	.long	0x1e6
	.uleb128 0x6
	.byte	0x8
	.long	0x1e6
	.uleb128 0xc
	.long	0x1f0
	.uleb128 0x6
	.byte	0x8
	.long	0xdf
	.uleb128 0xc
	.long	0x1fb
	.uleb128 0x6
	.byte	0x8
	.long	0x104
	.uleb128 0xc
	.long	0x206
	.uleb128 0x6
	.byte	0x8
	.long	0x119
	.uleb128 0xc
	.long	0x211
	.uleb128 0x6
	.byte	0x8
	.long	0x12e
	.uleb128 0xc
	.long	0x21c
	.uleb128 0x6
	.byte	0x8
	.long	0x143
	.uleb128 0xc
	.long	0x227
	.uleb128 0x6
	.byte	0x8
	.long	0x158
	.uleb128 0xc
	.long	0x232
	.uleb128 0x6
	.byte	0x8
	.long	0x16d
	.uleb128 0xc
	.long	0x23d
	.uleb128 0x6
	.byte	0x8
	.long	0x182
	.uleb128 0xc
	.long	0x248
	.uleb128 0x6
	.byte	0x8
	.long	0x197
	.uleb128 0xc
	.long	0x253
	.uleb128 0x6
	.byte	0x8
	.long	0x1ac
	.uleb128 0xc
	.long	0x25e
	.uleb128 0x6
	.byte	0x8
	.long	0x1c1
	.uleb128 0xc
	.long	0x269
	.uleb128 0x6
	.byte	0x8
	.long	0x1d6
	.uleb128 0xc
	.long	0x274
	.uleb128 0x6
	.byte	0x8
	.long	0x1eb
	.uleb128 0xc
	.long	0x27f
	.uleb128 0xa
	.long	0x85
	.long	0x29a
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF29
	.byte	0xd8
	.byte	0x6
	.byte	0x31
	.byte	0x8
	.long	0x421
	.uleb128 0x9
	.long	.LASF30
	.byte	0x6
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0x9
	.long	.LASF31
	.byte	0x6
	.byte	0x36
	.byte	0x9
	.long	0x7f
	.byte	0x8
	.uleb128 0x9
	.long	.LASF32
	.byte	0x6
	.byte	0x37
	.byte	0x9
	.long	0x7f
	.byte	0x10
	.uleb128 0x9
	.long	.LASF33
	.byte	0x6
	.byte	0x38
	.byte	0x9
	.long	0x7f
	.byte	0x18
	.uleb128 0x9
	.long	.LASF34
	.byte	0x6
	.byte	0x39
	.byte	0x9
	.long	0x7f
	.byte	0x20
	.uleb128 0x9
	.long	.LASF35
	.byte	0x6
	.byte	0x3a
	.byte	0x9
	.long	0x7f
	.byte	0x28
	.uleb128 0x9
	.long	.LASF36
	.byte	0x6
	.byte	0x3b
	.byte	0x9
	.long	0x7f
	.byte	0x30
	.uleb128 0x9
	.long	.LASF37
	.byte	0x6
	.byte	0x3c
	.byte	0x9
	.long	0x7f
	.byte	0x38
	.uleb128 0x9
	.long	.LASF38
	.byte	0x6
	.byte	0x3d
	.byte	0x9
	.long	0x7f
	.byte	0x40
	.uleb128 0x9
	.long	.LASF39
	.byte	0x6
	.byte	0x40
	.byte	0x9
	.long	0x7f
	.byte	0x48
	.uleb128 0x9
	.long	.LASF40
	.byte	0x6
	.byte	0x41
	.byte	0x9
	.long	0x7f
	.byte	0x50
	.uleb128 0x9
	.long	.LASF41
	.byte	0x6
	.byte	0x42
	.byte	0x9
	.long	0x7f
	.byte	0x58
	.uleb128 0x9
	.long	.LASF42
	.byte	0x6
	.byte	0x44
	.byte	0x16
	.long	0x43a
	.byte	0x60
	.uleb128 0x9
	.long	.LASF43
	.byte	0x6
	.byte	0x46
	.byte	0x14
	.long	0x440
	.byte	0x68
	.uleb128 0x9
	.long	.LASF44
	.byte	0x6
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0x9
	.long	.LASF45
	.byte	0x6
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0x9
	.long	.LASF46
	.byte	0x6
	.byte	0x4a
	.byte	0xb
	.long	0x65
	.byte	0x78
	.uleb128 0x9
	.long	.LASF47
	.byte	0x6
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF48
	.byte	0x6
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF49
	.byte	0x6
	.byte	0x4f
	.byte	0x8
	.long	0x28a
	.byte	0x83
	.uleb128 0x9
	.long	.LASF50
	.byte	0x6
	.byte	0x51
	.byte	0xf
	.long	0x446
	.byte	0x88
	.uleb128 0x9
	.long	.LASF51
	.byte	0x6
	.byte	0x59
	.byte	0xd
	.long	0x71
	.byte	0x90
	.uleb128 0x9
	.long	.LASF52
	.byte	0x6
	.byte	0x5b
	.byte	0x17
	.long	0x451
	.byte	0x98
	.uleb128 0x9
	.long	.LASF53
	.byte	0x6
	.byte	0x5c
	.byte	0x19
	.long	0x45c
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF54
	.byte	0x6
	.byte	0x5d
	.byte	0x14
	.long	0x440
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF55
	.byte	0x6
	.byte	0x5e
	.byte	0x9
	.long	0x7d
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF56
	.byte	0x6
	.byte	0x5f
	.byte	0xa
	.long	0x91
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF57
	.byte	0x6
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF58
	.byte	0x6
	.byte	0x62
	.byte	0x8
	.long	0x462
	.byte	0xc4
	.byte	0
	.uleb128 0x4
	.long	.LASF59
	.byte	0x7
	.byte	0x7
	.byte	0x19
	.long	0x29a
	.uleb128 0xe
	.long	.LASF88
	.byte	0x6
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF60
	.uleb128 0x6
	.byte	0x8
	.long	0x435
	.uleb128 0x6
	.byte	0x8
	.long	0x29a
	.uleb128 0x6
	.byte	0x8
	.long	0x42d
	.uleb128 0xd
	.long	.LASF61
	.uleb128 0x6
	.byte	0x8
	.long	0x44c
	.uleb128 0xd
	.long	.LASF62
	.uleb128 0x6
	.byte	0x8
	.long	0x457
	.uleb128 0xa
	.long	0x85
	.long	0x472
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x8c
	.uleb128 0x7
	.long	0x472
	.uleb128 0xf
	.long	.LASF63
	.byte	0x8
	.byte	0x89
	.byte	0xe
	.long	0x489
	.uleb128 0x6
	.byte	0x8
	.long	0x421
	.uleb128 0xf
	.long	.LASF64
	.byte	0x8
	.byte	0x8a
	.byte	0xe
	.long	0x489
	.uleb128 0xf
	.long	.LASF65
	.byte	0x8
	.byte	0x8b
	.byte	0xe
	.long	0x489
	.uleb128 0xf
	.long	.LASF66
	.byte	0x9
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xa
	.long	0x478
	.long	0x4be
	.uleb128 0x10
	.byte	0
	.uleb128 0x7
	.long	0x4b3
	.uleb128 0xf
	.long	.LASF67
	.byte	0x9
	.byte	0x1b
	.byte	0x1a
	.long	0x4be
	.uleb128 0xf
	.long	.LASF68
	.byte	0x9
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF69
	.byte	0x9
	.byte	0x1f
	.byte	0x1a
	.long	0x4be
	.uleb128 0x6
	.byte	0x8
	.long	0x4ed
	.uleb128 0x11
	.uleb128 0xf
	.long	.LASF70
	.byte	0xa
	.byte	0x2d
	.byte	0xe
	.long	0x7f
	.uleb128 0xf
	.long	.LASF71
	.byte	0xa
	.byte	0x2e
	.byte	0xe
	.long	0x7f
	.uleb128 0xa
	.long	0x7f
	.long	0x516
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0xf
	.long	.LASF72
	.byte	0xb
	.byte	0x9f
	.byte	0xe
	.long	0x506
	.uleb128 0xf
	.long	.LASF73
	.byte	0xb
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF74
	.byte	0xb
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0xf
	.long	.LASF75
	.byte	0xb
	.byte	0xa6
	.byte	0xe
	.long	0x506
	.uleb128 0xf
	.long	.LASF76
	.byte	0xb
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF77
	.byte	0xb
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x12
	.long	.LASF78
	.byte	0xb
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x12
	.long	.LASF79
	.byte	0xc
	.value	0x21f
	.byte	0xf
	.long	0x578
	.uleb128 0x6
	.byte	0x8
	.long	0x7f
	.uleb128 0x12
	.long	.LASF80
	.byte	0xc
	.value	0x221
	.byte	0xf
	.long	0x578
	.uleb128 0xf
	.long	.LASF81
	.byte	0xd
	.byte	0x24
	.byte	0xe
	.long	0x7f
	.uleb128 0xf
	.long	.LASF82
	.byte	0xd
	.byte	0x32
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF83
	.byte	0xd
	.byte	0x37
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF84
	.byte	0xd
	.byte	0x3b
	.byte	0xc
	.long	0x57
	.uleb128 0x13
	.long	.LASF89
	.byte	0x1
	.byte	0x24
	.byte	0x5
	.long	0x57
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0x67e
	.uleb128 0x14
	.string	"l"
	.byte	0x1
	.byte	0x24
	.byte	0x1f
	.long	0x4e7
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x14
	.string	"r"
	.byte	0x1
	.byte	0x24
	.byte	0x2e
	.long	0x4e7
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x14
	.string	"n"
	.byte	0x1
	.byte	0x24
	.byte	0x35
	.long	0x57
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x15
	.string	"lb"
	.byte	0x1
	.byte	0x26
	.byte	0x10
	.long	0x3b
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x15
	.string	"rb"
	.byte	0x1
	.byte	0x26
	.byte	0x14
	.long	0x3b
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x15
	.string	"x"
	.byte	0x1
	.byte	0x27
	.byte	0x7
	.long	0x57
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x15
	.string	"b"
	.byte	0x1
	.byte	0x27
	.byte	0xa
	.long	0x57
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x16
	.quad	.LVL3
	.long	0x67e
	.uleb128 0x17
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.uleb128 0x17
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.uleb128 0x17
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x18
	.long	.LASF90
	.long	.LASF90
	.byte	0xe
	.byte	0x40
	.byte	0xc
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU15
	.uleb128 .LVU15
	.uleb128 .LVU98
	.uleb128 .LVU98
	.uleb128 .LVU99
	.uleb128 .LVU99
	.uleb128 .LVU106
	.uleb128 .LVU106
	.uleb128 .LVU110
	.uleb128 .LVU110
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL3-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL3-1-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL25-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL26-.Ltext0
	.quad	.LVL28-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL28-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL30-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU15
	.uleb128 .LVU15
	.uleb128 .LVU97
	.uleb128 .LVU97
	.uleb128 .LVU99
	.uleb128 .LVU99
	.uleb128 .LVU105
	.uleb128 .LVU105
	.uleb128 .LVU110
	.uleb128 .LVU110
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL3-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL3-1-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL24-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL26-.Ltext0
	.quad	.LVL27-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL27-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL30-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU27
	.uleb128 .LVU27
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL2-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL5-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU24
	.uleb128 .LVU34
	.uleb128 .LVU34
	.uleb128 .LVU44
	.uleb128 .LVU44
	.uleb128 .LVU54
	.uleb128 .LVU54
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU76
	.uleb128 .LVU76
	.uleb128 .LVU78
	.uleb128 .LVU78
	.uleb128 .LVU88
	.uleb128 .LVU88
	.uleb128 .LVU96
.LLST3:
	.quad	.LVL4-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL7-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x5
	.byte	0x74
	.sleb128 0
	.byte	0x31
	.byte	0x24
	.byte	0x9f
	.quad	.LVL9-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x5
	.byte	0x74
	.sleb128 0
	.byte	0x32
	.byte	0x24
	.byte	0x9f
	.quad	.LVL11-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x5
	.byte	0x74
	.sleb128 0
	.byte	0x33
	.byte	0x24
	.byte	0x9f
	.quad	.LVL13-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL16-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x5
	.byte	0x74
	.sleb128 0
	.byte	0x34
	.byte	0x24
	.byte	0x9f
	.quad	.LVL17-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL20-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU25
	.uleb128 .LVU36
	.uleb128 .LVU36
	.uleb128 .LVU46
	.uleb128 .LVU46
	.uleb128 .LVU56
	.uleb128 .LVU56
	.uleb128 .LVU68
	.uleb128 .LVU68
	.uleb128 .LVU73
	.uleb128 .LVU73
	.uleb128 .LVU80
	.uleb128 .LVU80
	.uleb128 .LVU85
	.uleb128 .LVU85
	.uleb128 .LVU90
	.uleb128 .LVU90
	.uleb128 .LVU95
	.uleb128 .LVU95
	.uleb128 .LVU96
.LLST4:
	.quad	.LVL4-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL8-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x5
	.byte	0x71
	.sleb128 0
	.byte	0x31
	.byte	0x24
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x5
	.byte	0x71
	.sleb128 0
	.byte	0x32
	.byte	0x24
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x5
	.byte	0x71
	.sleb128 0
	.byte	0x33
	.byte	0x24
	.byte	0x9f
	.quad	.LVL14-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL15-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x5
	.byte	0x71
	.sleb128 0
	.byte	0x34
	.byte	0x24
	.byte	0x9f
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL19-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x5
	.byte	0x71
	.sleb128 0
	.byte	0x35
	.byte	0x24
	.byte	0x9f
	.quad	.LVL21-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL22-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0xd
	.byte	0x7d
	.sleb128 0
	.byte	0x7c
	.sleb128 0
	.byte	0x22
	.byte	0x94
	.byte	0x1
	.byte	0x8
	.byte	0xff
	.byte	0x1a
	.byte	0x36
	.byte	0x24
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU15
	.uleb128 .LVU96
	.uleb128 .LVU99
	.uleb128 .LVU107
	.uleb128 .LVU110
	.uleb128 .LVU111
.LLST5:
	.quad	.LVL3-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL26-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL30-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU5
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU27
	.uleb128 .LVU27
	.uleb128 .LVU28
	.uleb128 .LVU28
	.uleb128 .LVU37
	.uleb128 .LVU37
	.uleb128 .LVU47
	.uleb128 .LVU47
	.uleb128 .LVU57
	.uleb128 .LVU57
	.uleb128 .LVU69
	.uleb128 .LVU69
	.uleb128 .LVU81
	.uleb128 .LVU81
	.uleb128 .LVU91
	.uleb128 .LVU91
	.uleb128 .LVU96
.LLST6:
	.quad	.LVL1-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x5
	.byte	0x71
	.sleb128 0
	.byte	0x38
	.byte	0x1b
	.byte	0x9f
	.quad	.LVL2-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x5
	.byte	0x73
	.sleb128 0
	.byte	0x38
	.byte	0x1b
	.byte	0x9f
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x6
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x38
	.byte	0x1b
	.byte	0x9f
	.quad	.LVL6-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL8-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x3
	.byte	0x73
	.sleb128 -1
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x3
	.byte	0x73
	.sleb128 -2
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x3
	.byte	0x73
	.sleb128 -3
	.byte	0x9f
	.quad	.LVL14-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x3
	.byte	0x73
	.sleb128 -4
	.byte	0x9f
	.quad	.LVL18-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x3
	.byte	0x73
	.sleb128 -5
	.byte	0x9f
	.quad	.LVL21-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x3
	.byte	0x73
	.sleb128 -6
	.byte	0x9f
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF82:
	.string	"optind"
.LASF81:
	.string	"optarg"
.LASF46:
	.string	"_old_offset"
.LASF33:
	.string	"_IO_read_base"
.LASF66:
	.string	"sys_nerr"
.LASF41:
	.string	"_IO_save_end"
.LASF24:
	.string	"sockaddr_iso"
.LASF10:
	.string	"size_t"
.LASF22:
	.string	"sockaddr_inarp"
.LASF51:
	.string	"_offset"
.LASF72:
	.string	"__tzname"
.LASF35:
	.string	"_IO_write_ptr"
.LASF12:
	.string	"long long int"
.LASF26:
	.string	"sockaddr_un"
.LASF37:
	.string	"_IO_buf_base"
.LASF55:
	.string	"_freeres_buf"
.LASF79:
	.string	"__environ"
.LASF42:
	.string	"_markers"
.LASF86:
	.string	"../deps/cares/src/bitncmp.c"
.LASF77:
	.string	"timezone"
.LASF76:
	.string	"daylight"
.LASF88:
	.string	"_IO_lock_t"
.LASF83:
	.string	"opterr"
.LASF13:
	.string	"sa_family_t"
.LASF5:
	.string	"short int"
.LASF69:
	.string	"_sys_errlist"
.LASF89:
	.string	"ares__bitncmp"
.LASF90:
	.string	"memcmp"
.LASF25:
	.string	"sockaddr_ns"
.LASF23:
	.string	"sockaddr_ipx"
.LASF74:
	.string	"__timezone"
.LASF50:
	.string	"_lock"
.LASF73:
	.string	"__daylight"
.LASF6:
	.string	"long int"
.LASF78:
	.string	"getdate_err"
.LASF47:
	.string	"_cur_column"
.LASF68:
	.string	"_sys_nerr"
.LASF29:
	.string	"_IO_FILE"
.LASF70:
	.string	"program_invocation_name"
.LASF0:
	.string	"unsigned char"
.LASF17:
	.string	"sockaddr_ax25"
.LASF4:
	.string	"signed char"
.LASF52:
	.string	"_codecvt"
.LASF11:
	.string	"long long unsigned int"
.LASF21:
	.string	"sockaddr_in6"
.LASF80:
	.string	"environ"
.LASF60:
	.string	"_IO_marker"
.LASF49:
	.string	"_shortbuf"
.LASF32:
	.string	"_IO_read_end"
.LASF34:
	.string	"_IO_write_base"
.LASF58:
	.string	"_unused2"
.LASF31:
	.string	"_IO_read_ptr"
.LASF15:
	.string	"sa_data"
.LASF38:
	.string	"_IO_buf_end"
.LASF18:
	.string	"sockaddr_dl"
.LASF9:
	.string	"char"
.LASF53:
	.string	"_wide_data"
.LASF54:
	.string	"_freeres_list"
.LASF75:
	.string	"tzname"
.LASF56:
	.string	"__pad5"
.LASF87:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF19:
	.string	"sockaddr_eon"
.LASF1:
	.string	"short unsigned int"
.LASF65:
	.string	"stderr"
.LASF71:
	.string	"program_invocation_short_name"
.LASF3:
	.string	"long unsigned int"
.LASF36:
	.string	"_IO_write_end"
.LASF8:
	.string	"__off64_t"
.LASF27:
	.string	"sockaddr_x25"
.LASF7:
	.string	"__off_t"
.LASF43:
	.string	"_chain"
.LASF85:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF62:
	.string	"_IO_wide_data"
.LASF40:
	.string	"_IO_backup_base"
.LASF63:
	.string	"stdin"
.LASF45:
	.string	"_flags2"
.LASF14:
	.string	"sa_family"
.LASF57:
	.string	"_mode"
.LASF16:
	.string	"sockaddr_at"
.LASF48:
	.string	"_vtable_offset"
.LASF39:
	.string	"_IO_save_base"
.LASF67:
	.string	"sys_errlist"
.LASF84:
	.string	"optopt"
.LASF44:
	.string	"_fileno"
.LASF59:
	.string	"FILE"
.LASF2:
	.string	"unsigned int"
.LASF30:
	.string	"_flags"
.LASF20:
	.string	"sockaddr_in"
.LASF64:
	.string	"stdout"
.LASF28:
	.string	"sockaddr"
.LASF61:
	.string	"_IO_codecvt"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
