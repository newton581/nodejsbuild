	.file	"ares_getopt.c"
	.text
.Ltext0:
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../deps/cares/src/ares_getopt.c"
	.section	.rodata.str1.1
.LC2:
	.string	"%s: illegal option -- %c\n"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"%s: option requires an argument -- %c\n"
	.text
	.p2align 4
	.globl	ares_getopt
	.type	ares_getopt, @function
ares_getopt:
.LVL0:
.LFB59:
	.file 1 "../deps/cares/src/ares_getopt.c"
	.loc 1 67 1 view -0
	.cfi_startproc
	.loc 1 67 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 68 5 is_stmt 1 view .LVU2
	.loc 1 69 5 view .LVU3
	.loc 1 71 5 view .LVU4
	.loc 1 67 1 is_stmt 0 view .LVU5
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rsi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 71 8 view .LVU6
	movl	ares_optreset(%rip), %esi
.LVL1:
	.loc 1 71 8 view .LVU7
	testl	%esi, %esi
	jne	.L2
	.loc 1 71 27 discriminator 1 view .LVU8
	movq	place.3724(%rip), %r15
	movsbl	(%r15), %r12d
	.loc 1 71 23 discriminator 1 view .LVU9
	testb	%r12b, %r12b
	jne	.L3
.L2:
	.loc 1 72 9 is_stmt 1 view .LVU10
	.loc 1 72 23 is_stmt 0 view .LVU11
	movl	$0, ares_optreset(%rip)
	.loc 1 73 9 is_stmt 1 view .LVU12
	.loc 1 73 25 is_stmt 0 view .LVU13
	movl	ares_optind(%rip), %eax
	.loc 1 73 12 view .LVU14
	cmpl	%r13d, %eax
	jge	.L29
	.loc 1 73 52 discriminator 1 view .LVU15
	movslq	%eax, %rcx
	movq	(%r14,%rcx,8), %rbx
	.loc 1 73 34 discriminator 1 view .LVU16
	cmpb	$45, (%rbx)
	.loc 1 73 45 discriminator 1 view .LVU17
	movq	%rbx, place.3724(%rip)
	.loc 1 73 34 discriminator 1 view .LVU18
	jne	.L29
	.loc 1 77 9 is_stmt 1 view .LVU19
	.loc 1 77 18 is_stmt 0 view .LVU20
	movsbl	1(%rbx), %r12d
	leaq	1(%rbx), %r15
	.loc 1 77 12 view .LVU21
	testb	%r12b, %r12b
	je	.L7
	.loc 1 77 22 discriminator 1 view .LVU22
	cmpb	$45, %r12b
	je	.L8
	leaq	2(%rbx), %rcx
.L9:
	.loc 1 83 5 is_stmt 1 view .LVU23
	.loc 1 83 35 is_stmt 0 view .LVU24
	movq	%rcx, place.3724(%rip)
	movq	%rcx, -56(%rbp)
	.loc 1 83 22 view .LVU25
	movl	%r12d, ares_optopt(%rip)
	.loc 1 83 8 view .LVU26
	cmpl	$58, %r12d
	jne	.L34
.LVL2:
.L10:
	.loc 1 91 9 is_stmt 1 view .LVU27
	.loc 1 91 12 is_stmt 0 view .LVU28
	cmpb	$0, 1(%r15)
	jne	.L12
	.loc 1 92 13 is_stmt 1 view .LVU29
	addl	$1, ares_optind(%rip)
.L12:
	.loc 1 93 9 view .LVU30
	.loc 1 93 12 is_stmt 0 view .LVU31
	movl	ares_opterr(%rip), %ecx
	testl	%ecx, %ecx
	je	.L31
	.loc 1 93 25 discriminator 1 view .LVU32
	cmpb	$58, (%rdx)
	jne	.L35
.L31:
	.loc 1 114 20 view .LVU33
	movl	$63, %r12d
.LVL3:
.L1:
	.loc 1 122 1 view .LVU34
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
.LVL4:
	.loc 1 122 1 view .LVU35
	popq	%r14
.LVL5:
	.loc 1 122 1 view .LVU36
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL6:
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	.loc 1 83 5 is_stmt 1 view .LVU37
	.loc 1 84 16 is_stmt 0 view .LVU38
	movl	$45, %esi
	movq	%rdx, %rdi
.LVL7:
	.loc 1 83 35 view .LVU39
	movq	%r15, place.3724(%rip)
	.loc 1 83 22 view .LVU40
	movl	$45, ares_optopt(%rip)
	.loc 1 84 16 view .LVU41
	movq	%rdx, -56(%rbp)
	call	strchr@PLT
.LVL8:
	.loc 1 83 51 view .LVU42
	testq	%rax, %rax
	je	.L30
	.loc 1 83 51 view .LVU43
	movq	-56(%rbp), %rdx
	.loc 1 83 24 view .LVU44
	movl	$45, %r12d
.LVL9:
.L11:
	.loc 1 98 5 is_stmt 1 view .LVU45
	.loc 1 98 8 is_stmt 0 view .LVU46
	cmpb	$58, 1(%rax)
	je	.L15
	.loc 1 99 9 is_stmt 1 view .LVU47
	.loc 1 99 21 is_stmt 0 view .LVU48
	movq	$0, ares_optarg(%rip)
	.loc 1 100 9 is_stmt 1 view .LVU49
	.loc 1 100 12 is_stmt 0 view .LVU50
	cmpb	$0, 1(%rbx)
	jne	.L1
	.loc 1 101 13 is_stmt 1 view .LVU51
	addl	$1, ares_optind(%rip)
	jmp	.L1
.LVL10:
	.p2align 4,,10
	.p2align 3
.L3:
	.loc 1 101 13 is_stmt 0 view .LVU52
	leaq	1(%r15), %rcx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L34:
	.loc 1 84 16 discriminator 1 view .LVU53
	movq	%rdx, %rdi
.LVL11:
	.loc 1 84 16 discriminator 1 view .LVU54
	movl	%r12d, %esi
	movq	%rdx, -64(%rbp)
	call	strchr@PLT
.LVL12:
	.loc 1 83 51 discriminator 1 view .LVU55
	movq	-64(%rbp), %rdx
	movq	-56(%rbp), %rcx
	testq	%rax, %rax
	jne	.L20
	.loc 1 89 9 is_stmt 1 view .LVU56
	.loc 1 89 12 is_stmt 0 view .LVU57
	cmpl	$45, %r12d
	jne	.L10
.LVL13:
	.p2align 4,,10
	.p2align 3
.L30:
	.loc 1 80 13 is_stmt 1 view .LVU58
	.loc 1 80 20 is_stmt 0 view .LVU59
	movl	$-1, %r12d
	jmp	.L1
.LVL14:
	.p2align 4,,10
	.p2align 3
.L35:
	.loc 1 94 13 is_stmt 1 view .LVU60
.LBB6:
.LBI6:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/stdio2.h"
	.loc 2 98 1 view .LVU61
.LBB7:
	.loc 2 100 3 view .LVU62
	.loc 2 100 10 is_stmt 0 view .LVU63
	movl	%r12d, %r8d
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rdx
.LVL15:
.L32:
	.loc 2 100 10 view .LVU64
.LBE7:
.LBE6:
.LBB8:
.LBB9:
	movq	stderr(%rip), %rdi
	movl	$1, %esi
	xorl	%eax, %eax
.LBE9:
.LBE8:
	.loc 1 114 20 view .LVU65
	movl	$63, %r12d
.LBB12:
.LBB10:
	.loc 2 100 10 view .LVU66
	call	__fprintf_chk@PLT
.LVL16:
	jmp	.L1
.LVL17:
	.p2align 4,,10
	.p2align 3
.L20:
	.loc 2 100 10 view .LVU67
.LBE10:
.LBE12:
	movq	%r15, %rbx
	movq	%rcx, %r15
	jmp	.L11
.LVL18:
	.p2align 4,,10
	.p2align 3
.L15:
	.loc 1 104 9 is_stmt 1 view .LVU68
	movl	ares_optind(%rip), %ecx
	.loc 1 104 12 is_stmt 0 view .LVU69
	cmpb	$0, 1(%rbx)
	leal	1(%rcx), %eax
.LVL19:
	.loc 1 104 12 view .LVU70
	je	.L16
	.loc 1 105 13 is_stmt 1 view .LVU71
	.loc 1 105 25 is_stmt 0 view .LVU72
	movq	%r15, ares_optarg(%rip)
.L17:
	.loc 1 118 9 is_stmt 1 view .LVU73
	.loc 1 118 15 is_stmt 0 view .LVU74
	leaq	.LC0(%rip), %rsi
	.loc 1 119 9 view .LVU75
	movl	%eax, ares_optind(%rip)
	.loc 1 118 15 view .LVU76
	movq	%rsi, place.3724(%rip)
	.loc 1 119 9 is_stmt 1 view .LVU77
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	.loc 1 106 14 view .LVU78
	.loc 1 106 17 is_stmt 0 view .LVU79
	movl	%eax, ares_optind(%rip)
	cmpl	%eax, %r13d
	jg	.L18
	.loc 1 107 13 is_stmt 1 view .LVU80
	.loc 1 107 19 is_stmt 0 view .LVU81
	leaq	.LC0(%rip), %rax
	.loc 1 108 16 view .LVU82
	cmpb	$58, (%rdx)
	.loc 1 107 19 view .LVU83
	movq	%rax, place.3724(%rip)
	.loc 1 108 13 is_stmt 1 view .LVU84
	.loc 1 108 16 is_stmt 0 view .LVU85
	je	.L21
	.loc 1 110 13 is_stmt 1 view .LVU86
	.loc 1 110 16 is_stmt 0 view .LVU87
	movl	ares_opterr(%rip), %eax
	testl	%eax, %eax
	je	.L31
	.loc 1 111 17 is_stmt 1 view .LVU88
.LVL20:
.LBB13:
.LBI8:
	.loc 2 98 1 view .LVU89
.LBB11:
	.loc 2 100 3 view .LVU90
	.loc 2 100 10 is_stmt 0 view .LVU91
	movl	%r12d, %r8d
	leaq	.LC1(%rip), %rcx
	leaq	.LC3(%rip), %rdx
.LVL21:
	.loc 2 100 10 view .LVU92
	jmp	.L32
.LVL22:
	.p2align 4,,10
	.p2align 3
.L18:
	.loc 2 100 10 view .LVU93
.LBE11:
.LBE13:
	.loc 1 117 13 is_stmt 1 view .LVU94
	.loc 1 117 32 is_stmt 0 view .LVU95
	cltq
	movq	(%r14,%rax,8), %rax
	.loc 1 117 25 view .LVU96
	movq	%rax, ares_optarg(%rip)
	leal	2(%rcx), %eax
	jmp	.L17
.LVL23:
.L8:
	.loc 1 78 13 is_stmt 1 view .LVU97
	addl	$1, %eax
	movl	%eax, ares_optind(%rip)
	.p2align 4,,10
	.p2align 3
.L29:
	.loc 1 79 13 view .LVU98
	.loc 1 79 19 is_stmt 0 view .LVU99
	leaq	.LC0(%rip), %rax
	movq	%rax, place.3724(%rip)
	jmp	.L30
.LVL24:
	.p2align 4,,10
	.p2align 3
.L21:
	.loc 1 109 24 view .LVU100
	movl	$58, %r12d
	jmp	.L1
	.cfi_endproc
.LFE59:
	.size	ares_getopt, .-ares_getopt
	.section	.data.rel.local,"aw"
	.align 8
	.type	place.3724, @object
	.size	place.3724, 8
place.3724:
	.quad	.LC0
	.comm	ares_optarg,8,8
	.local	ares_optreset
	.comm	ares_optreset,4,4
	.globl	ares_optopt
	.bss
	.align 4
	.type	ares_optopt, @object
	.size	ares_optopt, 4
ares_optopt:
	.zero	4
	.globl	ares_optind
	.data
	.align 4
	.type	ares_optind, @object
	.size	ares_optind, 4
ares_optind:
	.long	1
	.globl	ares_opterr
	.align 4
	.type	ares_opterr, @object
	.size	ares_opterr, 4
ares_opterr:
	.long	1
	.text
.Letext0:
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 7 "/usr/include/stdio.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 9 "../deps/cares/src/ares_getopt.h"
	.file 10 "/usr/include/string.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x525
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF66
	.byte	0x1
	.long	.LASF67
	.long	.LASF68
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.long	.LASF7
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x39
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF0
	.uleb128 0x3
	.byte	0x4
	.byte	0x7
	.long	.LASF1
	.uleb128 0x4
	.byte	0x8
	.uleb128 0x3
	.byte	0x1
	.byte	0x8
	.long	.LASF2
	.uleb128 0x3
	.byte	0x2
	.byte	0x7
	.long	.LASF3
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF6
	.uleb128 0x2
	.long	.LASF8
	.byte	0x4
	.byte	0x98
	.byte	0x12
	.long	0x6c
	.uleb128 0x2
	.long	.LASF9
	.byte	0x4
	.byte	0x99
	.byte	0x12
	.long	0x6c
	.uleb128 0x6
	.byte	0x8
	.long	0x96
	.uleb128 0x7
	.long	0x8b
	.uleb128 0x3
	.byte	0x1
	.byte	0x6
	.long	.LASF10
	.uleb128 0x7
	.long	0x96
	.uleb128 0x8
	.long	.LASF69
	.byte	0xd8
	.byte	0x5
	.byte	0x31
	.byte	0x8
	.long	0x229
	.uleb128 0x9
	.long	.LASF11
	.byte	0x5
	.byte	0x33
	.byte	0x7
	.long	0x65
	.byte	0
	.uleb128 0x9
	.long	.LASF12
	.byte	0x5
	.byte	0x36
	.byte	0x9
	.long	0x8b
	.byte	0x8
	.uleb128 0x9
	.long	.LASF13
	.byte	0x5
	.byte	0x37
	.byte	0x9
	.long	0x8b
	.byte	0x10
	.uleb128 0x9
	.long	.LASF14
	.byte	0x5
	.byte	0x38
	.byte	0x9
	.long	0x8b
	.byte	0x18
	.uleb128 0x9
	.long	.LASF15
	.byte	0x5
	.byte	0x39
	.byte	0x9
	.long	0x8b
	.byte	0x20
	.uleb128 0x9
	.long	.LASF16
	.byte	0x5
	.byte	0x3a
	.byte	0x9
	.long	0x8b
	.byte	0x28
	.uleb128 0x9
	.long	.LASF17
	.byte	0x5
	.byte	0x3b
	.byte	0x9
	.long	0x8b
	.byte	0x30
	.uleb128 0x9
	.long	.LASF18
	.byte	0x5
	.byte	0x3c
	.byte	0x9
	.long	0x8b
	.byte	0x38
	.uleb128 0x9
	.long	.LASF19
	.byte	0x5
	.byte	0x3d
	.byte	0x9
	.long	0x8b
	.byte	0x40
	.uleb128 0x9
	.long	.LASF20
	.byte	0x5
	.byte	0x40
	.byte	0x9
	.long	0x8b
	.byte	0x48
	.uleb128 0x9
	.long	.LASF21
	.byte	0x5
	.byte	0x41
	.byte	0x9
	.long	0x8b
	.byte	0x50
	.uleb128 0x9
	.long	.LASF22
	.byte	0x5
	.byte	0x42
	.byte	0x9
	.long	0x8b
	.byte	0x58
	.uleb128 0x9
	.long	.LASF23
	.byte	0x5
	.byte	0x44
	.byte	0x16
	.long	0x242
	.byte	0x60
	.uleb128 0x9
	.long	.LASF24
	.byte	0x5
	.byte	0x46
	.byte	0x14
	.long	0x248
	.byte	0x68
	.uleb128 0x9
	.long	.LASF25
	.byte	0x5
	.byte	0x48
	.byte	0x7
	.long	0x65
	.byte	0x70
	.uleb128 0x9
	.long	.LASF26
	.byte	0x5
	.byte	0x49
	.byte	0x7
	.long	0x65
	.byte	0x74
	.uleb128 0x9
	.long	.LASF27
	.byte	0x5
	.byte	0x4a
	.byte	0xb
	.long	0x73
	.byte	0x78
	.uleb128 0x9
	.long	.LASF28
	.byte	0x5
	.byte	0x4d
	.byte	0x12
	.long	0x50
	.byte	0x80
	.uleb128 0x9
	.long	.LASF29
	.byte	0x5
	.byte	0x4e
	.byte	0xf
	.long	0x57
	.byte	0x82
	.uleb128 0x9
	.long	.LASF30
	.byte	0x5
	.byte	0x4f
	.byte	0x8
	.long	0x24e
	.byte	0x83
	.uleb128 0x9
	.long	.LASF31
	.byte	0x5
	.byte	0x51
	.byte	0xf
	.long	0x25e
	.byte	0x88
	.uleb128 0x9
	.long	.LASF32
	.byte	0x5
	.byte	0x59
	.byte	0xd
	.long	0x7f
	.byte	0x90
	.uleb128 0x9
	.long	.LASF33
	.byte	0x5
	.byte	0x5b
	.byte	0x17
	.long	0x269
	.byte	0x98
	.uleb128 0x9
	.long	.LASF34
	.byte	0x5
	.byte	0x5c
	.byte	0x19
	.long	0x274
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF35
	.byte	0x5
	.byte	0x5d
	.byte	0x14
	.long	0x248
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF36
	.byte	0x5
	.byte	0x5e
	.byte	0x9
	.long	0x47
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF37
	.byte	0x5
	.byte	0x5f
	.byte	0xa
	.long	0x2d
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF38
	.byte	0x5
	.byte	0x60
	.byte	0x7
	.long	0x65
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF39
	.byte	0x5
	.byte	0x62
	.byte	0x8
	.long	0x27a
	.byte	0xc4
	.byte	0
	.uleb128 0x2
	.long	.LASF40
	.byte	0x6
	.byte	0x7
	.byte	0x19
	.long	0xa2
	.uleb128 0xa
	.long	.LASF70
	.byte	0x5
	.byte	0x2b
	.byte	0xe
	.uleb128 0xb
	.long	.LASF41
	.uleb128 0x6
	.byte	0x8
	.long	0x23d
	.uleb128 0x6
	.byte	0x8
	.long	0xa2
	.uleb128 0xc
	.long	0x96
	.long	0x25e
	.uleb128 0xd
	.long	0x39
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x235
	.uleb128 0xb
	.long	.LASF42
	.uleb128 0x6
	.byte	0x8
	.long	0x264
	.uleb128 0xb
	.long	.LASF43
	.uleb128 0x6
	.byte	0x8
	.long	0x26f
	.uleb128 0xc
	.long	0x96
	.long	0x28a
	.uleb128 0xd
	.long	0x39
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x9d
	.uleb128 0x7
	.long	0x28a
	.uleb128 0xe
	.long	0x28a
	.uleb128 0xf
	.long	.LASF44
	.byte	0x7
	.byte	0x89
	.byte	0xe
	.long	0x2a6
	.uleb128 0x6
	.byte	0x8
	.long	0x229
	.uleb128 0xe
	.long	0x2a6
	.uleb128 0xf
	.long	.LASF45
	.byte	0x7
	.byte	0x8a
	.byte	0xe
	.long	0x2a6
	.uleb128 0xf
	.long	.LASF46
	.byte	0x7
	.byte	0x8b
	.byte	0xe
	.long	0x2a6
	.uleb128 0xf
	.long	.LASF47
	.byte	0x8
	.byte	0x1a
	.byte	0xc
	.long	0x65
	.uleb128 0xc
	.long	0x290
	.long	0x2e0
	.uleb128 0x10
	.byte	0
	.uleb128 0x7
	.long	0x2d5
	.uleb128 0xf
	.long	.LASF48
	.byte	0x8
	.byte	0x1b
	.byte	0x1a
	.long	0x2e0
	.uleb128 0xf
	.long	.LASF49
	.byte	0x8
	.byte	0x1e
	.byte	0xc
	.long	0x65
	.uleb128 0xf
	.long	.LASF50
	.byte	0x8
	.byte	0x1f
	.byte	0x1a
	.long	0x2e0
	.uleb128 0x3
	.byte	0x8
	.byte	0x5
	.long	.LASF51
	.uleb128 0x3
	.byte	0x8
	.byte	0x7
	.long	.LASF52
	.uleb128 0xf
	.long	.LASF53
	.byte	0x9
	.byte	0x30
	.byte	0xe
	.long	0x8b
	.uleb128 0xf
	.long	.LASF54
	.byte	0x9
	.byte	0x31
	.byte	0xc
	.long	0x65
	.uleb128 0xf
	.long	.LASF55
	.byte	0x9
	.byte	0x32
	.byte	0xc
	.long	0x65
	.uleb128 0xf
	.long	.LASF56
	.byte	0x9
	.byte	0x33
	.byte	0xc
	.long	0x65
	.uleb128 0x11
	.long	0x32f
	.byte	0x1
	.byte	0x33
	.byte	0x5
	.uleb128 0x9
	.byte	0x3
	.quad	ares_opterr
	.uleb128 0x11
	.long	0x323
	.byte	0x1
	.byte	0x34
	.byte	0x7
	.uleb128 0x9
	.byte	0x3
	.quad	ares_optind
	.uleb128 0x11
	.long	0x33b
	.byte	0x1
	.byte	0x35
	.byte	0x5
	.uleb128 0x9
	.byte	0x3
	.quad	ares_optopt
	.uleb128 0x12
	.long	.LASF60
	.byte	0x1
	.byte	0x36
	.byte	0xc
	.long	0x65
	.uleb128 0x9
	.byte	0x3
	.quad	ares_optreset
	.uleb128 0x11
	.long	0x317
	.byte	0x1
	.byte	0x37
	.byte	0x7
	.uleb128 0x9
	.byte	0x3
	.quad	ares_optarg
	.uleb128 0x13
	.long	.LASF71
	.byte	0x1
	.byte	0x42
	.byte	0x1
	.long	0x65
	.quad	.LFB59
	.quad	.LFE59-.LFB59
	.uleb128 0x1
	.byte	0x9c
	.long	0x4df
	.uleb128 0x14
	.long	.LASF57
	.byte	0x1
	.byte	0x42
	.byte	0x11
	.long	0x65
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x14
	.long	.LASF58
	.byte	0x1
	.byte	0x42
	.byte	0x25
	.long	0x4df
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x14
	.long	.LASF59
	.byte	0x1
	.byte	0x42
	.byte	0x3a
	.long	0x28a
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x12
	.long	.LASF61
	.byte	0x1
	.byte	0x44
	.byte	0x12
	.long	0x8b
	.uleb128 0x9
	.byte	0x3
	.quad	place.3724
	.uleb128 0x15
	.string	"oli"
	.byte	0x1
	.byte	0x45
	.byte	0xb
	.long	0x8b
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x16
	.long	0x4e5
	.quad	.LBI6
	.byte	.LVU61
	.quad	.LBB6
	.quad	.LBE6-.LBB6
	.byte	0x1
	.byte	0x5e
	.byte	0x13
	.long	0x465
	.uleb128 0x17
	.long	0x502
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x18
	.long	0x4f6
	.byte	0
	.uleb128 0x19
	.long	0x4e5
	.quad	.LBI8
	.byte	.LVU89
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x6f
	.byte	0x17
	.long	0x4a4
	.uleb128 0x17
	.long	0x502
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x18
	.long	0x4f6
	.uleb128 0x1a
	.quad	.LVL16
	.long	0x510
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x31
	.byte	0
	.byte	0
	.uleb128 0x1c
	.quad	.LVL8
	.long	0x51c
	.long	0x4c3
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0x76
	.sleb128 -56
	.byte	0x6
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x8
	.byte	0x2d
	.byte	0
	.uleb128 0x1a
	.quad	.LVL12
	.long	0x51c
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x3
	.byte	0x76
	.sleb128 -64
	.byte	0x6
	.uleb128 0x1b
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x91
	.uleb128 0x1d
	.long	.LASF72
	.byte	0x2
	.byte	0x62
	.byte	0x1
	.long	0x65
	.byte	0x3
	.long	0x510
	.uleb128 0x1e
	.long	.LASF62
	.byte	0x2
	.byte	0x62
	.byte	0x1b
	.long	0x2ac
	.uleb128 0x1e
	.long	.LASF63
	.byte	0x2
	.byte	0x62
	.byte	0x3c
	.long	0x295
	.uleb128 0x1f
	.byte	0
	.uleb128 0x20
	.long	.LASF64
	.long	.LASF64
	.byte	0x2
	.byte	0x58
	.byte	0xc
	.uleb128 0x20
	.long	.LASF65
	.long	.LASF65
	.byte	0xa
	.byte	0xe2
	.byte	0xe
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x47
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU27
	.uleb128 .LVU27
	.uleb128 .LVU35
	.uleb128 .LVU35
	.uleb128 .LVU37
	.uleb128 .LVU37
	.uleb128 .LVU39
	.uleb128 .LVU39
	.uleb128 .LVU52
	.uleb128 .LVU52
	.uleb128 .LVU54
	.uleb128 .LVU54
	.uleb128 .LVU97
	.uleb128 .LVU97
	.uleb128 .LVU100
	.uleb128 .LVU100
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL2-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL4-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL7-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL10-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL11-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL23-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL24-.Ltext0
	.quad	.LFE59-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU7
	.uleb128 .LVU7
	.uleb128 .LVU36
	.uleb128 .LVU36
	.uleb128 .LVU37
	.uleb128 .LVU37
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL1-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL6-.Ltext0
	.quad	.LFE59-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU34
	.uleb128 .LVU34
	.uleb128 .LVU37
	.uleb128 .LVU37
	.uleb128 .LVU42
	.uleb128 .LVU42
	.uleb128 .LVU45
	.uleb128 .LVU45
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU60
	.uleb128 .LVU60
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU67
	.uleb128 .LVU67
	.uleb128 .LVU92
	.uleb128 .LVU92
	.uleb128 .LVU93
	.uleb128 .LVU93
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL3-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL6-.Ltext0
	.quad	.LVL8-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL8-1-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x2
	.byte	0x76
	.sleb128 -56
	.quad	.LVL9-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL13-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL14-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL15-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL17-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL21-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL22-.Ltext0
	.quad	.LFE59-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU42
	.uleb128 .LVU46
	.uleb128 .LVU46
	.uleb128 .LVU52
	.uleb128 .LVU55
	.uleb128 .LVU58
	.uleb128 .LVU67
	.uleb128 .LVU68
	.uleb128 .LVU68
	.uleb128 .LVU70
.LLST3:
	.quad	.LVL8-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x3
	.byte	0x70
	.sleb128 1
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x3
	.byte	0x70
	.sleb128 1
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU61
	.uleb128 .LVU64
.LLST4:
	.quad	.LVL14-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0xa
	.byte	0x3
	.quad	.LC2
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU89
	.uleb128 .LVU93
.LLST5:
	.quad	.LVL20-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0xa
	.byte	0x3
	.quad	.LC3
	.byte	0x9f
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB8-.Ltext0
	.quad	.LBE8-.Ltext0
	.quad	.LBB12-.Ltext0
	.quad	.LBE12-.Ltext0
	.quad	.LBB13-.Ltext0
	.quad	.LBE13-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF8:
	.string	"__off_t"
.LASF12:
	.string	"_IO_read_ptr"
.LASF24:
	.string	"_chain"
.LASF7:
	.string	"size_t"
.LASF30:
	.string	"_shortbuf"
.LASF57:
	.string	"nargc"
.LASF18:
	.string	"_IO_buf_base"
.LASF58:
	.string	"nargv"
.LASF52:
	.string	"long long unsigned int"
.LASF33:
	.string	"_codecvt"
.LASF51:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF55:
	.string	"ares_opterr"
.LASF25:
	.string	"_fileno"
.LASF13:
	.string	"_IO_read_end"
.LASF6:
	.string	"long int"
.LASF11:
	.string	"_flags"
.LASF19:
	.string	"_IO_buf_end"
.LASF28:
	.string	"_cur_column"
.LASF42:
	.string	"_IO_codecvt"
.LASF50:
	.string	"_sys_errlist"
.LASF27:
	.string	"_old_offset"
.LASF32:
	.string	"_offset"
.LASF71:
	.string	"ares_getopt"
.LASF41:
	.string	"_IO_marker"
.LASF44:
	.string	"stdin"
.LASF1:
	.string	"unsigned int"
.LASF36:
	.string	"_freeres_buf"
.LASF72:
	.string	"fprintf"
.LASF62:
	.string	"__stream"
.LASF0:
	.string	"long unsigned int"
.LASF16:
	.string	"_IO_write_ptr"
.LASF65:
	.string	"strchr"
.LASF47:
	.string	"sys_nerr"
.LASF67:
	.string	"../deps/cares/src/ares_getopt.c"
.LASF3:
	.string	"short unsigned int"
.LASF68:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF20:
	.string	"_IO_save_base"
.LASF31:
	.string	"_lock"
.LASF26:
	.string	"_flags2"
.LASF38:
	.string	"_mode"
.LASF45:
	.string	"stdout"
.LASF17:
	.string	"_IO_write_end"
.LASF61:
	.string	"place"
.LASF70:
	.string	"_IO_lock_t"
.LASF69:
	.string	"_IO_FILE"
.LASF48:
	.string	"sys_errlist"
.LASF23:
	.string	"_markers"
.LASF2:
	.string	"unsigned char"
.LASF5:
	.string	"short int"
.LASF43:
	.string	"_IO_wide_data"
.LASF49:
	.string	"_sys_nerr"
.LASF29:
	.string	"_vtable_offset"
.LASF40:
	.string	"FILE"
.LASF64:
	.string	"__fprintf_chk"
.LASF56:
	.string	"ares_optopt"
.LASF10:
	.string	"char"
.LASF60:
	.string	"ares_optreset"
.LASF9:
	.string	"__off64_t"
.LASF14:
	.string	"_IO_read_base"
.LASF22:
	.string	"_IO_save_end"
.LASF63:
	.string	"__fmt"
.LASF37:
	.string	"__pad5"
.LASF53:
	.string	"ares_optarg"
.LASF39:
	.string	"_unused2"
.LASF46:
	.string	"stderr"
.LASF54:
	.string	"ares_optind"
.LASF21:
	.string	"_IO_backup_base"
.LASF35:
	.string	"_freeres_list"
.LASF66:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF34:
	.string	"_wide_data"
.LASF59:
	.string	"ostr"
.LASF15:
	.string	"_IO_write_base"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
