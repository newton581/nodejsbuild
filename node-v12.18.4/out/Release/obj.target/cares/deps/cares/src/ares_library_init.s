	.file	"ares_library_init.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares_library_init
	.type	ares_library_init, @function
ares_library_init:
.LVL0:
.LFB89:
	.file 1 "../deps/cares/src/ares_library_init.c"
	.loc 1 129 1 view -0
	.cfi_startproc
	.loc 1 129 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 130 3 is_stmt 1 view .LVU2
	.loc 1 132 3 view .LVU3
	.loc 1 134 23 is_stmt 0 view .LVU4
	addl	$1, ares_initialized(%rip)
	.loc 1 149 1 view .LVU5
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE89:
	.size	ares_library_init, .-ares_library_init
	.p2align 4
	.globl	ares_library_init_mem
	.type	ares_library_init_mem, @function
ares_library_init_mem:
.LVL1:
.LFB90:
	.loc 1 155 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 155 1 is_stmt 0 view .LVU7
	endbr64
	.loc 1 156 3 is_stmt 1 view .LVU8
	.loc 1 156 6 is_stmt 0 view .LVU9
	testq	%rsi, %rsi
	je	.L4
	.loc 1 157 5 is_stmt 1 view .LVU10
	.loc 1 157 17 is_stmt 0 view .LVU11
	movq	%rsi, ares_malloc(%rip)
.L4:
	.loc 1 158 3 is_stmt 1 view .LVU12
	.loc 1 158 6 is_stmt 0 view .LVU13
	testq	%rcx, %rcx
	je	.L5
	.loc 1 159 5 is_stmt 1 view .LVU14
	.loc 1 159 18 is_stmt 0 view .LVU15
	movq	%rcx, ares_realloc(%rip)
.L5:
	.loc 1 160 3 is_stmt 1 view .LVU16
	.loc 1 160 6 is_stmt 0 view .LVU17
	testq	%rdx, %rdx
	je	.L6
	.loc 1 161 5 is_stmt 1 view .LVU18
	.loc 1 161 15 is_stmt 0 view .LVU19
	movq	%rdx, ares_free(%rip)
.L6:
	.loc 1 162 3 is_stmt 1 view .LVU20
.LVL2:
.LBB8:
.LBI8:
	.loc 1 128 5 view .LVU21
.LBB9:
	.loc 1 130 3 view .LVU22
	.loc 1 132 3 view .LVU23
	.loc 1 134 23 is_stmt 0 view .LVU24
	addl	$1, ares_initialized(%rip)
.LVL3:
	.loc 1 134 23 view .LVU25
.LBE9:
.LBE8:
	.loc 1 163 1 view .LVU26
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE90:
	.size	ares_library_init_mem, .-ares_library_init_mem
	.p2align 4
	.globl	ares_library_cleanup
	.type	ares_library_cleanup, @function
ares_library_cleanup:
.LFB91:
	.loc 1 167 1 is_stmt 1 view -0
	.cfi_startproc
	endbr64
	.loc 1 168 3 view .LVU28
	.loc 1 168 7 is_stmt 0 view .LVU29
	movl	ares_initialized(%rip), %eax
	.loc 1 168 6 view .LVU30
	testl	%eax, %eax
	je	.L16
	.loc 1 170 3 is_stmt 1 view .LVU31
	.loc 1 170 19 is_stmt 0 view .LVU32
	subl	$1, %eax
	movl	%eax, ares_initialized(%rip)
	.loc 1 171 3 is_stmt 1 view .LVU33
	.loc 1 171 6 is_stmt 0 view .LVU34
	jne	.L16
	.loc 1 174 3 is_stmt 1 view .LVU35
	.loc 1 181 3 view .LVU36
	.loc 1 182 3 view .LVU37
	.loc 1 182 15 is_stmt 0 view .LVU38
	movq	malloc@GOTPCREL(%rip), %rax
	movq	%rax, ares_malloc(%rip)
	.loc 1 183 3 is_stmt 1 view .LVU39
	.loc 1 183 16 is_stmt 0 view .LVU40
	movq	realloc@GOTPCREL(%rip), %rax
	movq	%rax, ares_realloc(%rip)
	.loc 1 184 3 is_stmt 1 view .LVU41
	.loc 1 184 13 is_stmt 0 view .LVU42
	movq	free@GOTPCREL(%rip), %rax
	movq	%rax, ares_free(%rip)
.L16:
	.loc 1 185 1 view .LVU43
	ret
	.cfi_endproc
.LFE91:
	.size	ares_library_cleanup, .-ares_library_cleanup
	.p2align 4
	.globl	ares_library_initialized
	.type	ares_library_initialized, @function
ares_library_initialized:
.LFB92:
	.loc 1 189 1 is_stmt 1 view -0
	.cfi_startproc
	endbr64
	.loc 1 194 3 view .LVU45
	.loc 1 195 1 is_stmt 0 view .LVU46
	xorl	%eax, %eax
	ret
	.cfi_endproc
.LFE92:
	.size	ares_library_initialized, .-ares_library_initialized
	.globl	ares_free
	.section	.data.rel,"aw"
	.align 8
	.type	ares_free, @object
	.size	ares_free, 8
ares_free:
	.quad	free
	.globl	ares_realloc
	.align 8
	.type	ares_realloc, @object
	.size	ares_realloc, 8
ares_realloc:
	.quad	realloc
	.globl	ares_malloc
	.align 8
	.type	ares_malloc, @object
	.size	ares_malloc, 8
ares_malloc:
	.quad	malloc
	.local	ares_initialized
	.comm	ares_initialized,4,4
	.text
.Letext0:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 6 "/usr/include/netinet/in.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 9 "/usr/include/stdio.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 11 "/usr/include/errno.h"
	.file 12 "/usr/include/time.h"
	.file 13 "/usr/include/unistd.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 16 "../deps/cares/include/ares.h"
	.file 17 "../deps/cares/src/ares_ipv6.h"
	.file 18 "../deps/cares/src/ares_private.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x99d
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF123
	.byte	0x1
	.long	.LASF124
	.long	.LASF125
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x2
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x2
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x2
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x2
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x2
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0xa9
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x7
	.long	0xa9
	.uleb128 0x3
	.long	.LASF13
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF14
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF15
	.uleb128 0x3
	.long	.LASF16
	.byte	0x4
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF23
	.byte	0x10
	.byte	0x5
	.byte	0xb2
	.byte	0x8
	.long	0x103
	.uleb128 0x9
	.long	.LASF17
	.byte	0x5
	.byte	0xb4
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF18
	.byte	0x5
	.byte	0xb5
	.byte	0xa
	.long	0x108
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0xdb
	.uleb128 0xa
	.long	0xa9
	.long	0x118
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xdb
	.uleb128 0xc
	.long	0x118
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x7
	.long	0x123
	.uleb128 0x6
	.byte	0x8
	.long	0x123
	.uleb128 0xc
	.long	0x12d
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x7
	.long	0x138
	.uleb128 0x6
	.byte	0x8
	.long	0x138
	.uleb128 0xc
	.long	0x142
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x7
	.long	0x14d
	.uleb128 0x6
	.byte	0x8
	.long	0x14d
	.uleb128 0xc
	.long	0x157
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x7
	.long	0x162
	.uleb128 0x6
	.byte	0x8
	.long	0x162
	.uleb128 0xc
	.long	0x16c
	.uleb128 0x8
	.long	.LASF24
	.byte	0x10
	.byte	0x6
	.byte	0xee
	.byte	0x8
	.long	0x1b9
	.uleb128 0x9
	.long	.LASF25
	.byte	0x6
	.byte	0xf0
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF26
	.byte	0x6
	.byte	0xf1
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0x9
	.long	.LASF27
	.byte	0x6
	.byte	0xf2
	.byte	0x14
	.long	0x693
	.byte	0x4
	.uleb128 0x9
	.long	.LASF28
	.byte	0x6
	.byte	0xf5
	.byte	0x13
	.long	0x750
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	0x177
	.uleb128 0x6
	.byte	0x8
	.long	0x177
	.uleb128 0xc
	.long	0x1be
	.uleb128 0x8
	.long	.LASF29
	.byte	0x1c
	.byte	0x6
	.byte	0xfd
	.byte	0x8
	.long	0x21c
	.uleb128 0x9
	.long	.LASF30
	.byte	0x6
	.byte	0xff
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0xe
	.long	.LASF31
	.byte	0x6
	.value	0x100
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0xe
	.long	.LASF32
	.byte	0x6
	.value	0x101
	.byte	0xe
	.long	0x67b
	.byte	0x4
	.uleb128 0xe
	.long	.LASF33
	.byte	0x6
	.value	0x102
	.byte	0x15
	.long	0x718
	.byte	0x8
	.uleb128 0xe
	.long	.LASF34
	.byte	0x6
	.value	0x103
	.byte	0xe
	.long	0x67b
	.byte	0x18
	.byte	0
	.uleb128 0x7
	.long	0x1c9
	.uleb128 0x6
	.byte	0x8
	.long	0x1c9
	.uleb128 0xc
	.long	0x221
	.uleb128 0xd
	.long	.LASF35
	.uleb128 0x7
	.long	0x22c
	.uleb128 0x6
	.byte	0x8
	.long	0x22c
	.uleb128 0xc
	.long	0x236
	.uleb128 0xd
	.long	.LASF36
	.uleb128 0x7
	.long	0x241
	.uleb128 0x6
	.byte	0x8
	.long	0x241
	.uleb128 0xc
	.long	0x24b
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x7
	.long	0x256
	.uleb128 0x6
	.byte	0x8
	.long	0x256
	.uleb128 0xc
	.long	0x260
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x7
	.long	0x26b
	.uleb128 0x6
	.byte	0x8
	.long	0x26b
	.uleb128 0xc
	.long	0x275
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x7
	.long	0x280
	.uleb128 0x6
	.byte	0x8
	.long	0x280
	.uleb128 0xc
	.long	0x28a
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x7
	.long	0x295
	.uleb128 0x6
	.byte	0x8
	.long	0x295
	.uleb128 0xc
	.long	0x29f
	.uleb128 0x6
	.byte	0x8
	.long	0x103
	.uleb128 0xc
	.long	0x2aa
	.uleb128 0x6
	.byte	0x8
	.long	0x128
	.uleb128 0xc
	.long	0x2b5
	.uleb128 0x6
	.byte	0x8
	.long	0x13d
	.uleb128 0xc
	.long	0x2c0
	.uleb128 0x6
	.byte	0x8
	.long	0x152
	.uleb128 0xc
	.long	0x2cb
	.uleb128 0x6
	.byte	0x8
	.long	0x167
	.uleb128 0xc
	.long	0x2d6
	.uleb128 0x6
	.byte	0x8
	.long	0x1b9
	.uleb128 0xc
	.long	0x2e1
	.uleb128 0x6
	.byte	0x8
	.long	0x21c
	.uleb128 0xc
	.long	0x2ec
	.uleb128 0x6
	.byte	0x8
	.long	0x231
	.uleb128 0xc
	.long	0x2f7
	.uleb128 0x6
	.byte	0x8
	.long	0x246
	.uleb128 0xc
	.long	0x302
	.uleb128 0x6
	.byte	0x8
	.long	0x25b
	.uleb128 0xc
	.long	0x30d
	.uleb128 0x6
	.byte	0x8
	.long	0x270
	.uleb128 0xc
	.long	0x318
	.uleb128 0x6
	.byte	0x8
	.long	0x285
	.uleb128 0xc
	.long	0x323
	.uleb128 0x6
	.byte	0x8
	.long	0x29a
	.uleb128 0xc
	.long	0x32e
	.uleb128 0xa
	.long	0xa9
	.long	0x349
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF41
	.byte	0xd8
	.byte	0x7
	.byte	0x31
	.byte	0x8
	.long	0x4d0
	.uleb128 0x9
	.long	.LASF42
	.byte	0x7
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0x9
	.long	.LASF43
	.byte	0x7
	.byte	0x36
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0x9
	.long	.LASF44
	.byte	0x7
	.byte	0x37
	.byte	0x9
	.long	0xa3
	.byte	0x10
	.uleb128 0x9
	.long	.LASF45
	.byte	0x7
	.byte	0x38
	.byte	0x9
	.long	0xa3
	.byte	0x18
	.uleb128 0x9
	.long	.LASF46
	.byte	0x7
	.byte	0x39
	.byte	0x9
	.long	0xa3
	.byte	0x20
	.uleb128 0x9
	.long	.LASF47
	.byte	0x7
	.byte	0x3a
	.byte	0x9
	.long	0xa3
	.byte	0x28
	.uleb128 0x9
	.long	.LASF48
	.byte	0x7
	.byte	0x3b
	.byte	0x9
	.long	0xa3
	.byte	0x30
	.uleb128 0x9
	.long	.LASF49
	.byte	0x7
	.byte	0x3c
	.byte	0x9
	.long	0xa3
	.byte	0x38
	.uleb128 0x9
	.long	.LASF50
	.byte	0x7
	.byte	0x3d
	.byte	0x9
	.long	0xa3
	.byte	0x40
	.uleb128 0x9
	.long	.LASF51
	.byte	0x7
	.byte	0x40
	.byte	0x9
	.long	0xa3
	.byte	0x48
	.uleb128 0x9
	.long	.LASF52
	.byte	0x7
	.byte	0x41
	.byte	0x9
	.long	0xa3
	.byte	0x50
	.uleb128 0x9
	.long	.LASF53
	.byte	0x7
	.byte	0x42
	.byte	0x9
	.long	0xa3
	.byte	0x58
	.uleb128 0x9
	.long	.LASF54
	.byte	0x7
	.byte	0x44
	.byte	0x16
	.long	0x4e9
	.byte	0x60
	.uleb128 0x9
	.long	.LASF55
	.byte	0x7
	.byte	0x46
	.byte	0x14
	.long	0x4ef
	.byte	0x68
	.uleb128 0x9
	.long	.LASF56
	.byte	0x7
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0x9
	.long	.LASF57
	.byte	0x7
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0x9
	.long	.LASF58
	.byte	0x7
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0x9
	.long	.LASF59
	.byte	0x7
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF60
	.byte	0x7
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF61
	.byte	0x7
	.byte	0x4f
	.byte	0x8
	.long	0x339
	.byte	0x83
	.uleb128 0x9
	.long	.LASF62
	.byte	0x7
	.byte	0x51
	.byte	0xf
	.long	0x4f5
	.byte	0x88
	.uleb128 0x9
	.long	.LASF63
	.byte	0x7
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0x9
	.long	.LASF64
	.byte	0x7
	.byte	0x5b
	.byte	0x17
	.long	0x500
	.byte	0x98
	.uleb128 0x9
	.long	.LASF65
	.byte	0x7
	.byte	0x5c
	.byte	0x19
	.long	0x50b
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF66
	.byte	0x7
	.byte	0x5d
	.byte	0x14
	.long	0x4ef
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF67
	.byte	0x7
	.byte	0x5e
	.byte	0x9
	.long	0xa1
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF68
	.byte	0x7
	.byte	0x5f
	.byte	0xa
	.long	0xb5
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF69
	.byte	0x7
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF70
	.byte	0x7
	.byte	0x62
	.byte	0x8
	.long	0x511
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF71
	.byte	0x8
	.byte	0x7
	.byte	0x19
	.long	0x349
	.uleb128 0xf
	.long	.LASF126
	.byte	0x7
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF72
	.uleb128 0x6
	.byte	0x8
	.long	0x4e4
	.uleb128 0x6
	.byte	0x8
	.long	0x349
	.uleb128 0x6
	.byte	0x8
	.long	0x4dc
	.uleb128 0xd
	.long	.LASF73
	.uleb128 0x6
	.byte	0x8
	.long	0x4fb
	.uleb128 0xd
	.long	.LASF74
	.uleb128 0x6
	.byte	0x8
	.long	0x506
	.uleb128 0xa
	.long	0xa9
	.long	0x521
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb0
	.uleb128 0x7
	.long	0x521
	.uleb128 0x10
	.long	.LASF75
	.byte	0x9
	.byte	0x89
	.byte	0xe
	.long	0x538
	.uleb128 0x6
	.byte	0x8
	.long	0x4d0
	.uleb128 0x10
	.long	.LASF76
	.byte	0x9
	.byte	0x8a
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF77
	.byte	0x9
	.byte	0x8b
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF78
	.byte	0xa
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xa
	.long	0x527
	.long	0x56d
	.uleb128 0x11
	.byte	0
	.uleb128 0x7
	.long	0x562
	.uleb128 0x10
	.long	.LASF79
	.byte	0xa
	.byte	0x1b
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF80
	.byte	0xa
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF81
	.byte	0xa
	.byte	0x1f
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF82
	.byte	0xb
	.byte	0x2d
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF83
	.byte	0xb
	.byte	0x2e
	.byte	0xe
	.long	0xa3
	.uleb128 0xa
	.long	0xa3
	.long	0x5be
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF84
	.byte	0xc
	.byte	0x9f
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF85
	.byte	0xc
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF86
	.byte	0xc
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF87
	.byte	0xc
	.byte	0xa6
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF88
	.byte	0xc
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF89
	.byte	0xc
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x12
	.long	.LASF90
	.byte	0xc
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x12
	.long	.LASF91
	.byte	0xd
	.value	0x21f
	.byte	0xf
	.long	0x620
	.uleb128 0x6
	.byte	0x8
	.long	0xa3
	.uleb128 0x12
	.long	.LASF92
	.byte	0xd
	.value	0x221
	.byte	0xf
	.long	0x620
	.uleb128 0x10
	.long	.LASF93
	.byte	0xe
	.byte	0x24
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF94
	.byte	0xe
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF95
	.byte	0xe
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF96
	.byte	0xe
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF97
	.byte	0xf
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF98
	.byte	0xf
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF99
	.byte	0xf
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF100
	.byte	0x6
	.byte	0x1e
	.byte	0x12
	.long	0x67b
	.uleb128 0x8
	.long	.LASF101
	.byte	0x4
	.byte	0x6
	.byte	0x1f
	.byte	0x8
	.long	0x6ae
	.uleb128 0x9
	.long	.LASF102
	.byte	0x6
	.byte	0x21
	.byte	0xf
	.long	0x687
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF103
	.byte	0x6
	.byte	0x77
	.byte	0x12
	.long	0x66f
	.uleb128 0x13
	.byte	0x10
	.byte	0x6
	.byte	0xd6
	.byte	0x5
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF104
	.byte	0x6
	.byte	0xd8
	.byte	0xa
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF105
	.byte	0x6
	.byte	0xd9
	.byte	0xb
	.long	0x6f8
	.uleb128 0x14
	.long	.LASF106
	.byte	0x6
	.byte	0xda
	.byte	0xb
	.long	0x708
	.byte	0
	.uleb128 0xa
	.long	0x663
	.long	0x6f8
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x66f
	.long	0x708
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x67b
	.long	0x718
	.uleb128 0xb
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.long	.LASF107
	.byte	0x10
	.byte	0x6
	.byte	0xd4
	.byte	0x8
	.long	0x733
	.uleb128 0x9
	.long	.LASF108
	.byte	0x6
	.byte	0xdb
	.byte	0x9
	.long	0x6ba
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x718
	.uleb128 0x10
	.long	.LASF109
	.byte	0x6
	.byte	0xe4
	.byte	0x1e
	.long	0x733
	.uleb128 0x10
	.long	.LASF110
	.byte	0x6
	.byte	0xe5
	.byte	0x1e
	.long	0x733
	.uleb128 0xa
	.long	0x2d
	.long	0x760
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0x15
	.byte	0x10
	.byte	0x10
	.value	0x204
	.byte	0x3
	.long	0x778
	.uleb128 0x16
	.long	.LASF111
	.byte	0x10
	.value	0x205
	.byte	0x13
	.long	0x778
	.byte	0
	.uleb128 0xa
	.long	0x2d
	.long	0x788
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x17
	.long	.LASF112
	.byte	0x10
	.byte	0x10
	.value	0x203
	.byte	0x8
	.long	0x7a5
	.uleb128 0xe
	.long	.LASF113
	.byte	0x10
	.value	0x206
	.byte	0x5
	.long	0x760
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x788
	.uleb128 0x10
	.long	.LASF114
	.byte	0x11
	.byte	0x52
	.byte	0x23
	.long	0x7a5
	.uleb128 0x18
	.long	0xa1
	.long	0x7c5
	.uleb128 0x19
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF115
	.byte	0x12
	.value	0x151
	.byte	0x10
	.long	0x7d2
	.uleb128 0x6
	.byte	0x8
	.long	0x7b6
	.uleb128 0x18
	.long	0xa1
	.long	0x7ec
	.uleb128 0x19
	.long	0xa1
	.uleb128 0x19
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF116
	.byte	0x12
	.value	0x152
	.byte	0x10
	.long	0x7f9
	.uleb128 0x6
	.byte	0x8
	.long	0x7d8
	.uleb128 0x1a
	.long	0x80a
	.uleb128 0x19
	.long	0xa1
	.byte	0
	.uleb128 0x12
	.long	.LASF117
	.byte	0x12
	.value	0x153
	.byte	0xf
	.long	0x817
	.uleb128 0x6
	.byte	0x8
	.long	0x7ff
	.uleb128 0x1b
	.long	.LASF127
	.byte	0x1
	.byte	0x27
	.byte	0x15
	.long	0x3b
	.uleb128 0x9
	.byte	0x3
	.quad	ares_initialized
	.uleb128 0x1c
	.long	.LASF122
	.byte	0x1
	.byte	0x28
	.byte	0xc
	.long	0x6f
	.uleb128 0x1d
	.long	0x7c5
	.byte	0x1
	.byte	0x35
	.byte	0x9
	.uleb128 0x9
	.byte	0x3
	.quad	ares_malloc
	.uleb128 0x1d
	.long	0x7ec
	.byte	0x1
	.byte	0x36
	.byte	0x9
	.uleb128 0x9
	.byte	0x3
	.quad	ares_realloc
	.uleb128 0x1d
	.long	0x80a
	.byte	0x1
	.byte	0x37
	.byte	0x8
	.uleb128 0x9
	.byte	0x3
	.quad	ares_free
	.uleb128 0x1e
	.long	.LASF128
	.byte	0x1
	.byte	0xbc
	.byte	0x5
	.long	0x6f
	.quad	.LFB92
	.quad	.LFE92-.LFB92
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x1f
	.long	.LASF129
	.byte	0x1
	.byte	0xa6
	.byte	0x6
	.quad	.LFB91
	.quad	.LFE91-.LFB91
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x20
	.long	.LASF130
	.byte	0x1
	.byte	0x97
	.byte	0x5
	.long	0x6f
	.quad	.LFB90
	.quad	.LFE90-.LFB90
	.uleb128 0x1
	.byte	0x9c
	.long	0x93c
	.uleb128 0x21
	.long	.LASF118
	.byte	0x1
	.byte	0x97
	.byte	0x1f
	.long	0x6f
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x21
	.long	.LASF119
	.byte	0x1
	.byte	0x98
	.byte	0x23
	.long	0x7d2
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x21
	.long	.LASF120
	.byte	0x1
	.byte	0x99
	.byte	0x22
	.long	0x817
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x21
	.long	.LASF121
	.byte	0x1
	.byte	0x9a
	.byte	0x23
	.long	0x7f9
	.uleb128 0x1
	.byte	0x52
	.uleb128 0x22
	.long	0x93c
	.quad	.LBI8
	.byte	.LVU21
	.quad	.LBB8
	.quad	.LBE8-.LBB8
	.byte	0x1
	.byte	0xa2
	.byte	0xa
	.uleb128 0x23
	.long	0x94d
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x24
	.long	0x959
	.byte	0
	.byte	0
	.uleb128 0x25
	.long	.LASF131
	.byte	0x1
	.byte	0x80
	.byte	0x5
	.long	0x6f
	.byte	0x1
	.long	0x966
	.uleb128 0x26
	.long	.LASF118
	.byte	0x1
	.byte	0x80
	.byte	0x1b
	.long	0x6f
	.uleb128 0x27
	.string	"res"
	.byte	0x1
	.byte	0x82
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0x28
	.long	.LASF132
	.byte	0x1
	.byte	0x75
	.byte	0xd
	.byte	0x1
	.uleb128 0x29
	.long	.LASF133
	.byte	0x1
	.byte	0x3f
	.byte	0xc
	.long	0x6f
	.byte	0x1
	.uleb128 0x2a
	.long	0x93c
	.quad	.LFB89
	.quad	.LFE89-.LFB89
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x2b
	.long	0x94d
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x24
	.long	0x959
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x47
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 .LVU21
	.uleb128 .LVU25
.LLST0:
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF43:
	.string	"_IO_read_ptr"
.LASF55:
	.string	"_chain"
.LASF33:
	.string	"sin6_addr"
.LASF108:
	.string	"__in6_u"
.LASF13:
	.string	"size_t"
.LASF61:
	.string	"_shortbuf"
.LASF112:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF120:
	.string	"afree"
.LASF129:
	.string	"ares_library_cleanup"
.LASF49:
	.string	"_IO_buf_base"
.LASF14:
	.string	"long long unsigned int"
.LASF133:
	.string	"ares_win32_init"
.LASF100:
	.string	"in_addr_t"
.LASF128:
	.string	"ares_library_initialized"
.LASF64:
	.string	"_codecvt"
.LASF86:
	.string	"__timezone"
.LASF122:
	.string	"ares_init_flags"
.LASF15:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF35:
	.string	"sockaddr_inarp"
.LASF127:
	.string	"ares_initialized"
.LASF56:
	.string	"_fileno"
.LASF44:
	.string	"_IO_read_end"
.LASF121:
	.string	"arealloc"
.LASF105:
	.string	"__u6_addr16"
.LASF9:
	.string	"long int"
.LASF106:
	.string	"__u6_addr32"
.LASF42:
	.string	"_flags"
.LASF65:
	.string	"_wide_data"
.LASF117:
	.string	"ares_free"
.LASF59:
	.string	"_cur_column"
.LASF83:
	.string	"program_invocation_short_name"
.LASF73:
	.string	"_IO_codecvt"
.LASF21:
	.string	"sockaddr_dl"
.LASF31:
	.string	"sin6_port"
.LASF98:
	.string	"uint16_t"
.LASF81:
	.string	"_sys_errlist"
.LASF82:
	.string	"program_invocation_name"
.LASF58:
	.string	"_old_offset"
.LASF63:
	.string	"_offset"
.LASF110:
	.string	"in6addr_loopback"
.LASF40:
	.string	"sockaddr_x25"
.LASF36:
	.string	"sockaddr_ipx"
.LASF8:
	.string	"__uint32_t"
.LASF89:
	.string	"timezone"
.LASF28:
	.string	"sin_zero"
.LASF68:
	.string	"__pad5"
.LASF72:
	.string	"_IO_marker"
.LASF75:
	.string	"stdin"
.LASF2:
	.string	"unsigned int"
.LASF102:
	.string	"s_addr"
.LASF67:
	.string	"_freeres_buf"
.LASF3:
	.string	"long unsigned int"
.LASF32:
	.string	"sin6_flowinfo"
.LASF47:
	.string	"_IO_write_ptr"
.LASF115:
	.string	"ares_malloc"
.LASF88:
	.string	"daylight"
.LASF78:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF27:
	.string	"sin_addr"
.LASF113:
	.string	"_S6_un"
.LASF125:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF51:
	.string	"_IO_save_base"
.LASF92:
	.string	"environ"
.LASF62:
	.string	"_lock"
.LASF57:
	.string	"_flags2"
.LASF69:
	.string	"_mode"
.LASF76:
	.string	"stdout"
.LASF39:
	.string	"sockaddr_un"
.LASF119:
	.string	"amalloc"
.LASF25:
	.string	"sin_family"
.LASF93:
	.string	"optarg"
.LASF90:
	.string	"getdate_err"
.LASF30:
	.string	"sin6_family"
.LASF94:
	.string	"optind"
.LASF48:
	.string	"_IO_write_end"
.LASF126:
	.string	"_IO_lock_t"
.LASF109:
	.string	"in6addr_any"
.LASF41:
	.string	"_IO_FILE"
.LASF91:
	.string	"__environ"
.LASF85:
	.string	"__daylight"
.LASF77:
	.string	"stderr"
.LASF38:
	.string	"sockaddr_ns"
.LASF26:
	.string	"sin_port"
.LASF17:
	.string	"sa_family"
.LASF79:
	.string	"sys_errlist"
.LASF54:
	.string	"_markers"
.LASF34:
	.string	"sin6_scope_id"
.LASF0:
	.string	"unsigned char"
.LASF37:
	.string	"sockaddr_iso"
.LASF50:
	.string	"_IO_buf_end"
.LASF5:
	.string	"short int"
.LASF80:
	.string	"_sys_nerr"
.LASF60:
	.string	"_vtable_offset"
.LASF87:
	.string	"tzname"
.LASF20:
	.string	"sockaddr_ax25"
.LASF71:
	.string	"FILE"
.LASF107:
	.string	"in6_addr"
.LASF96:
	.string	"optopt"
.LASF99:
	.string	"uint32_t"
.LASF12:
	.string	"char"
.LASF131:
	.string	"ares_library_init"
.LASF7:
	.string	"__uint16_t"
.LASF104:
	.string	"__u6_addr8"
.LASF95:
	.string	"opterr"
.LASF11:
	.string	"__off64_t"
.LASF45:
	.string	"_IO_read_base"
.LASF53:
	.string	"_IO_save_end"
.LASF22:
	.string	"sockaddr_eon"
.LASF19:
	.string	"sockaddr_at"
.LASF116:
	.string	"ares_realloc"
.LASF16:
	.string	"sa_family_t"
.LASF70:
	.string	"_unused2"
.LASF111:
	.string	"_S6_u8"
.LASF29:
	.string	"sockaddr_in6"
.LASF132:
	.string	"ares_win32_cleanup"
.LASF24:
	.string	"sockaddr_in"
.LASF97:
	.string	"uint8_t"
.LASF52:
	.string	"_IO_backup_base"
.LASF118:
	.string	"flags"
.LASF18:
	.string	"sa_data"
.LASF66:
	.string	"_freeres_list"
.LASF124:
	.string	"../deps/cares/src/ares_library_init.c"
.LASF123:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF74:
	.string	"_IO_wide_data"
.LASF114:
	.string	"ares_in6addr_any"
.LASF84:
	.string	"__tzname"
.LASF46:
	.string	"_IO_write_base"
.LASF103:
	.string	"in_port_t"
.LASF130:
	.string	"ares_library_init_mem"
.LASF23:
	.string	"sockaddr"
.LASF101:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
