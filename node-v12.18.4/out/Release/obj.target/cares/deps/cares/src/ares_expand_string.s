	.file	"ares_expand_string.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares_expand_string
	.type	ares_expand_string, @function
ares_expand_string:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares_expand_string.c"
	.loc 1 41 1 view -0
	.cfi_startproc
	.loc 1 41 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 42 3 is_stmt 1 view .LVU2
	.loc 1 43 3 view .LVU3
	.loc 1 48 3 view .LVU4
	.loc 1 48 22 is_stmt 0 view .LVU5
	movslq	%edx, %rdx
	.loc 1 48 22 view .LVU6
	addq	%rdx, %rsi
.LVL1:
	.loc 1 48 6 view .LVU7
	cmpq	%rdi, %rsi
	je	.L3
	.loc 1 51 3 is_stmt 1 view .LVU8
	.loc 1 41 1 is_stmt 0 view .LVU9
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 49 12 view .LVU10
	movl	$17, %eax
	.loc 1 41 1 view .LVU11
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 51 14 view .LVU12
	movzbl	(%rdi), %r15d
.LVL2:
	.loc 1 52 3 is_stmt 1 view .LVU13
	.loc 1 52 23 is_stmt 0 view .LVU14
	leaq	1(%r15), %r14
	leaq	(%rdi,%r14), %rdx
.LVL3:
	.loc 1 52 6 view .LVU15
	cmpq	%rdx, %rsi
	jb	.L1
	movq	%rcx, %rbx
	movq	%r8, %r13
	.loc 1 55 3 is_stmt 1 view .LVU16
	.loc 1 55 10 is_stmt 0 view .LVU17
	leaq	1(%rdi), %r12
.LVL4:
	.loc 1 57 3 is_stmt 1 view .LVU18
	.loc 1 57 8 is_stmt 0 view .LVU19
	movq	%r14, %rdi
	call	*ares_malloc(%rip)
.LVL5:
	.loc 1 57 6 view .LVU20
	movq	%rax, (%rbx)
	.loc 1 58 3 is_stmt 1 view .LVU21
	.loc 1 58 6 is_stmt 0 view .LVU22
	testq	%rax, %rax
	je	.L5
	.loc 1 60 3 is_stmt 1 view .LVU23
.LVL6:
	.loc 1 61 3 view .LVU24
.LBB4:
.LBI4:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 103 42 view .LVU25
.LBB5:
	.loc 2 106 3 view .LVU26
	.loc 2 106 10 is_stmt 0 view .LVU27
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	strncpy@PLT
.LVL7:
	.loc 2 106 10 view .LVU28
.LBE5:
.LBE4:
	.loc 1 62 3 is_stmt 1 view .LVU29
	.loc 1 62 15 is_stmt 0 view .LVU30
	movb	$0, (%rax,%r15)
	.loc 1 64 3 is_stmt 1 view .LVU31
	.loc 1 64 6 is_stmt 0 view .LVU32
	movq	%rax, (%rbx)
	.loc 1 66 3 is_stmt 1 view .LVU33
	.loc 1 68 10 is_stmt 0 view .LVU34
	xorl	%eax, %eax
	.loc 1 66 28 view .LVU35
	movq	%r14, 0(%r13)
	.loc 1 68 3 is_stmt 1 view .LVU36
.LVL8:
.L1:
	.loc 1 69 1 is_stmt 0 view .LVU37
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
.LVL9:
	.loc 1 69 1 view .LVU38
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL10:
	.p2align 4,,10
	.p2align 3
.L5:
	.cfi_restore_state
	.loc 1 69 1 view .LVU39
	addq	$8, %rsp
	.loc 1 59 12 view .LVU40
	movl	$15, %eax
.LVL11:
	.loc 1 69 1 view .LVU41
	popq	%rbx
.LVL12:
	.loc 1 69 1 view .LVU42
	popq	%r12
.LVL13:
	.loc 1 69 1 view .LVU43
	popq	%r13
.LVL14:
	.loc 1 69 1 view .LVU44
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
.LVL15:
	.p2align 4,,10
	.p2align 3
.L3:
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	.loc 1 49 12 view .LVU45
	movl	$17, %eax
	.loc 1 69 1 view .LVU46
	ret
	.cfi_endproc
.LFE87:
	.size	ares_expand_string, .-ares_expand_string
.Letext0:
	.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 4 "/usr/include/x86_64-linux-gnu/sys/types.h"
	.file 5 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 8 "/usr/include/netinet/in.h"
	.file 9 "../deps/cares/include/ares_build.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 12 "/usr/include/stdio.h"
	.file 13 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 14 "/usr/include/errno.h"
	.file 15 "/usr/include/time.h"
	.file 16 "/usr/include/unistd.h"
	.file 17 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 18 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 19 "/usr/include/signal.h"
	.file 20 "/usr/include/arpa/nameser.h"
	.file 21 "../deps/cares/include/ares.h"
	.file 22 "../deps/cares/src/ares_ipv6.h"
	.file 23 "../deps/cares/src/ares_private.h"
	.file 24 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xa62
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF134
	.byte	0x1
	.long	.LASF135
	.long	.LASF136
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x3
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x4
	.long	.LASF6
	.byte	0x3
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x4
	.long	.LASF7
	.byte	0x3
	.byte	0x28
	.byte	0x1c
	.long	0x39
	.uleb128 0x5
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x4
	.long	.LASF8
	.byte	0x3
	.byte	0x2a
	.byte	0x16
	.long	0x40
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x4
	.long	.LASF10
	.byte	0x3
	.byte	0x98
	.byte	0x12
	.long	0x87
	.uleb128 0x4
	.long	.LASF11
	.byte	0x3
	.byte	0x99
	.byte	0x12
	.long	0x87
	.uleb128 0x6
	.byte	0x8
	.uleb128 0x4
	.long	.LASF12
	.byte	0x3
	.byte	0xc1
	.byte	0x12
	.long	0x87
	.uleb128 0x7
	.byte	0x8
	.long	0xbf
	.uleb128 0x8
	.long	0xb4
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF13
	.uleb128 0x3
	.long	0xbf
	.uleb128 0x4
	.long	.LASF14
	.byte	0x4
	.byte	0x6c
	.byte	0x13
	.long	0xa8
	.uleb128 0x4
	.long	.LASF15
	.byte	0x5
	.byte	0xd1
	.byte	0x1b
	.long	0x47
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF16
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF17
	.uleb128 0x4
	.long	.LASF18
	.byte	0x6
	.byte	0x1c
	.byte	0x1c
	.long	0x39
	.uleb128 0x9
	.long	.LASF25
	.byte	0x10
	.byte	0x7
	.byte	0xb2
	.byte	0x8
	.long	0x125
	.uleb128 0xa
	.long	.LASF19
	.byte	0x7
	.byte	0xb4
	.byte	0x11
	.long	0xf1
	.byte	0
	.uleb128 0xa
	.long	.LASF20
	.byte	0x7
	.byte	0xb5
	.byte	0xa
	.long	0x12a
	.byte	0x2
	.byte	0
	.uleb128 0x3
	.long	0xfd
	.uleb128 0xb
	.long	0xbf
	.long	0x13a
	.uleb128 0xc
	.long	0x47
	.byte	0xd
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.long	0xfd
	.uleb128 0x8
	.long	0x13a
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x3
	.long	0x145
	.uleb128 0x7
	.byte	0x8
	.long	0x145
	.uleb128 0x8
	.long	0x14f
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x3
	.long	0x15a
	.uleb128 0x7
	.byte	0x8
	.long	0x15a
	.uleb128 0x8
	.long	0x164
	.uleb128 0xd
	.long	.LASF23
	.uleb128 0x3
	.long	0x16f
	.uleb128 0x7
	.byte	0x8
	.long	0x16f
	.uleb128 0x8
	.long	0x179
	.uleb128 0xd
	.long	.LASF24
	.uleb128 0x3
	.long	0x184
	.uleb128 0x7
	.byte	0x8
	.long	0x184
	.uleb128 0x8
	.long	0x18e
	.uleb128 0x9
	.long	.LASF26
	.byte	0x10
	.byte	0x8
	.byte	0xee
	.byte	0x8
	.long	0x1db
	.uleb128 0xa
	.long	.LASF27
	.byte	0x8
	.byte	0xf0
	.byte	0x11
	.long	0xf1
	.byte	0
	.uleb128 0xa
	.long	.LASF28
	.byte	0x8
	.byte	0xf1
	.byte	0xf
	.long	0x6e1
	.byte	0x2
	.uleb128 0xa
	.long	.LASF29
	.byte	0x8
	.byte	0xf2
	.byte	0x14
	.long	0x6c6
	.byte	0x4
	.uleb128 0xa
	.long	.LASF30
	.byte	0x8
	.byte	0xf5
	.byte	0x13
	.long	0x783
	.byte	0x8
	.byte	0
	.uleb128 0x3
	.long	0x199
	.uleb128 0x7
	.byte	0x8
	.long	0x199
	.uleb128 0x8
	.long	0x1e0
	.uleb128 0x9
	.long	.LASF31
	.byte	0x1c
	.byte	0x8
	.byte	0xfd
	.byte	0x8
	.long	0x23e
	.uleb128 0xa
	.long	.LASF32
	.byte	0x8
	.byte	0xff
	.byte	0x11
	.long	0xf1
	.byte	0
	.uleb128 0xe
	.long	.LASF33
	.byte	0x8
	.value	0x100
	.byte	0xf
	.long	0x6e1
	.byte	0x2
	.uleb128 0xe
	.long	.LASF34
	.byte	0x8
	.value	0x101
	.byte	0xe
	.long	0x6ae
	.byte	0x4
	.uleb128 0xe
	.long	.LASF35
	.byte	0x8
	.value	0x102
	.byte	0x15
	.long	0x74b
	.byte	0x8
	.uleb128 0xe
	.long	.LASF36
	.byte	0x8
	.value	0x103
	.byte	0xe
	.long	0x6ae
	.byte	0x18
	.byte	0
	.uleb128 0x3
	.long	0x1eb
	.uleb128 0x7
	.byte	0x8
	.long	0x1eb
	.uleb128 0x8
	.long	0x243
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x3
	.long	0x24e
	.uleb128 0x7
	.byte	0x8
	.long	0x24e
	.uleb128 0x8
	.long	0x258
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x3
	.long	0x263
	.uleb128 0x7
	.byte	0x8
	.long	0x263
	.uleb128 0x8
	.long	0x26d
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x3
	.long	0x278
	.uleb128 0x7
	.byte	0x8
	.long	0x278
	.uleb128 0x8
	.long	0x282
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x3
	.long	0x28d
	.uleb128 0x7
	.byte	0x8
	.long	0x28d
	.uleb128 0x8
	.long	0x297
	.uleb128 0xd
	.long	.LASF41
	.uleb128 0x3
	.long	0x2a2
	.uleb128 0x7
	.byte	0x8
	.long	0x2a2
	.uleb128 0x8
	.long	0x2ac
	.uleb128 0xd
	.long	.LASF42
	.uleb128 0x3
	.long	0x2b7
	.uleb128 0x7
	.byte	0x8
	.long	0x2b7
	.uleb128 0x8
	.long	0x2c1
	.uleb128 0x7
	.byte	0x8
	.long	0x125
	.uleb128 0x8
	.long	0x2cc
	.uleb128 0x7
	.byte	0x8
	.long	0x14a
	.uleb128 0x8
	.long	0x2d7
	.uleb128 0x7
	.byte	0x8
	.long	0x15f
	.uleb128 0x8
	.long	0x2e2
	.uleb128 0x7
	.byte	0x8
	.long	0x174
	.uleb128 0x8
	.long	0x2ed
	.uleb128 0x7
	.byte	0x8
	.long	0x189
	.uleb128 0x8
	.long	0x2f8
	.uleb128 0x7
	.byte	0x8
	.long	0x1db
	.uleb128 0x8
	.long	0x303
	.uleb128 0x7
	.byte	0x8
	.long	0x23e
	.uleb128 0x8
	.long	0x30e
	.uleb128 0x7
	.byte	0x8
	.long	0x253
	.uleb128 0x8
	.long	0x319
	.uleb128 0x7
	.byte	0x8
	.long	0x268
	.uleb128 0x8
	.long	0x324
	.uleb128 0x7
	.byte	0x8
	.long	0x27d
	.uleb128 0x8
	.long	0x32f
	.uleb128 0x7
	.byte	0x8
	.long	0x292
	.uleb128 0x8
	.long	0x33a
	.uleb128 0x7
	.byte	0x8
	.long	0x2a7
	.uleb128 0x8
	.long	0x345
	.uleb128 0x7
	.byte	0x8
	.long	0x2bc
	.uleb128 0x8
	.long	0x350
	.uleb128 0x4
	.long	.LASF43
	.byte	0x9
	.byte	0xcd
	.byte	0x11
	.long	0xcb
	.uleb128 0xb
	.long	0xbf
	.long	0x377
	.uleb128 0xc
	.long	0x47
	.byte	0
	.byte	0
	.uleb128 0x9
	.long	.LASF44
	.byte	0xd8
	.byte	0xa
	.byte	0x31
	.byte	0x8
	.long	0x4fe
	.uleb128 0xa
	.long	.LASF45
	.byte	0xa
	.byte	0x33
	.byte	0x7
	.long	0x74
	.byte	0
	.uleb128 0xa
	.long	.LASF46
	.byte	0xa
	.byte	0x36
	.byte	0x9
	.long	0xb4
	.byte	0x8
	.uleb128 0xa
	.long	.LASF47
	.byte	0xa
	.byte	0x37
	.byte	0x9
	.long	0xb4
	.byte	0x10
	.uleb128 0xa
	.long	.LASF48
	.byte	0xa
	.byte	0x38
	.byte	0x9
	.long	0xb4
	.byte	0x18
	.uleb128 0xa
	.long	.LASF49
	.byte	0xa
	.byte	0x39
	.byte	0x9
	.long	0xb4
	.byte	0x20
	.uleb128 0xa
	.long	.LASF50
	.byte	0xa
	.byte	0x3a
	.byte	0x9
	.long	0xb4
	.byte	0x28
	.uleb128 0xa
	.long	.LASF51
	.byte	0xa
	.byte	0x3b
	.byte	0x9
	.long	0xb4
	.byte	0x30
	.uleb128 0xa
	.long	.LASF52
	.byte	0xa
	.byte	0x3c
	.byte	0x9
	.long	0xb4
	.byte	0x38
	.uleb128 0xa
	.long	.LASF53
	.byte	0xa
	.byte	0x3d
	.byte	0x9
	.long	0xb4
	.byte	0x40
	.uleb128 0xa
	.long	.LASF54
	.byte	0xa
	.byte	0x40
	.byte	0x9
	.long	0xb4
	.byte	0x48
	.uleb128 0xa
	.long	.LASF55
	.byte	0xa
	.byte	0x41
	.byte	0x9
	.long	0xb4
	.byte	0x50
	.uleb128 0xa
	.long	.LASF56
	.byte	0xa
	.byte	0x42
	.byte	0x9
	.long	0xb4
	.byte	0x58
	.uleb128 0xa
	.long	.LASF57
	.byte	0xa
	.byte	0x44
	.byte	0x16
	.long	0x517
	.byte	0x60
	.uleb128 0xa
	.long	.LASF58
	.byte	0xa
	.byte	0x46
	.byte	0x14
	.long	0x51d
	.byte	0x68
	.uleb128 0xa
	.long	.LASF59
	.byte	0xa
	.byte	0x48
	.byte	0x7
	.long	0x74
	.byte	0x70
	.uleb128 0xa
	.long	.LASF60
	.byte	0xa
	.byte	0x49
	.byte	0x7
	.long	0x74
	.byte	0x74
	.uleb128 0xa
	.long	.LASF61
	.byte	0xa
	.byte	0x4a
	.byte	0xb
	.long	0x8e
	.byte	0x78
	.uleb128 0xa
	.long	.LASF62
	.byte	0xa
	.byte	0x4d
	.byte	0x12
	.long	0x39
	.byte	0x80
	.uleb128 0xa
	.long	.LASF63
	.byte	0xa
	.byte	0x4e
	.byte	0xf
	.long	0x4e
	.byte	0x82
	.uleb128 0xa
	.long	.LASF64
	.byte	0xa
	.byte	0x4f
	.byte	0x8
	.long	0x367
	.byte	0x83
	.uleb128 0xa
	.long	.LASF65
	.byte	0xa
	.byte	0x51
	.byte	0xf
	.long	0x523
	.byte	0x88
	.uleb128 0xa
	.long	.LASF66
	.byte	0xa
	.byte	0x59
	.byte	0xd
	.long	0x9a
	.byte	0x90
	.uleb128 0xa
	.long	.LASF67
	.byte	0xa
	.byte	0x5b
	.byte	0x17
	.long	0x52e
	.byte	0x98
	.uleb128 0xa
	.long	.LASF68
	.byte	0xa
	.byte	0x5c
	.byte	0x19
	.long	0x539
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF69
	.byte	0xa
	.byte	0x5d
	.byte	0x14
	.long	0x51d
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF70
	.byte	0xa
	.byte	0x5e
	.byte	0x9
	.long	0xa6
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF71
	.byte	0xa
	.byte	0x5f
	.byte	0xa
	.long	0xd7
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF72
	.byte	0xa
	.byte	0x60
	.byte	0x7
	.long	0x74
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF73
	.byte	0xa
	.byte	0x62
	.byte	0x8
	.long	0x53f
	.byte	0xc4
	.byte	0
	.uleb128 0x4
	.long	.LASF74
	.byte	0xb
	.byte	0x7
	.byte	0x19
	.long	0x377
	.uleb128 0xf
	.long	.LASF137
	.byte	0xa
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF75
	.uleb128 0x7
	.byte	0x8
	.long	0x512
	.uleb128 0x7
	.byte	0x8
	.long	0x377
	.uleb128 0x7
	.byte	0x8
	.long	0x50a
	.uleb128 0xd
	.long	.LASF76
	.uleb128 0x7
	.byte	0x8
	.long	0x529
	.uleb128 0xd
	.long	.LASF77
	.uleb128 0x7
	.byte	0x8
	.long	0x534
	.uleb128 0xb
	.long	0xbf
	.long	0x54f
	.uleb128 0xc
	.long	0x47
	.byte	0x13
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.long	0xc6
	.uleb128 0x3
	.long	0x54f
	.uleb128 0x8
	.long	0x54f
	.uleb128 0x10
	.long	.LASF78
	.byte	0xc
	.byte	0x89
	.byte	0xe
	.long	0x56b
	.uleb128 0x7
	.byte	0x8
	.long	0x4fe
	.uleb128 0x10
	.long	.LASF79
	.byte	0xc
	.byte	0x8a
	.byte	0xe
	.long	0x56b
	.uleb128 0x10
	.long	.LASF80
	.byte	0xc
	.byte	0x8b
	.byte	0xe
	.long	0x56b
	.uleb128 0x10
	.long	.LASF81
	.byte	0xd
	.byte	0x1a
	.byte	0xc
	.long	0x74
	.uleb128 0xb
	.long	0x555
	.long	0x5a0
	.uleb128 0x11
	.byte	0
	.uleb128 0x3
	.long	0x595
	.uleb128 0x10
	.long	.LASF82
	.byte	0xd
	.byte	0x1b
	.byte	0x1a
	.long	0x5a0
	.uleb128 0x10
	.long	.LASF83
	.byte	0xd
	.byte	0x1e
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF84
	.byte	0xd
	.byte	0x1f
	.byte	0x1a
	.long	0x5a0
	.uleb128 0x10
	.long	.LASF85
	.byte	0xe
	.byte	0x2d
	.byte	0xe
	.long	0xb4
	.uleb128 0x10
	.long	.LASF86
	.byte	0xe
	.byte	0x2e
	.byte	0xe
	.long	0xb4
	.uleb128 0xb
	.long	0xb4
	.long	0x5f1
	.uleb128 0xc
	.long	0x47
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF87
	.byte	0xf
	.byte	0x9f
	.byte	0xe
	.long	0x5e1
	.uleb128 0x10
	.long	.LASF88
	.byte	0xf
	.byte	0xa0
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF89
	.byte	0xf
	.byte	0xa1
	.byte	0x11
	.long	0x87
	.uleb128 0x10
	.long	.LASF90
	.byte	0xf
	.byte	0xa6
	.byte	0xe
	.long	0x5e1
	.uleb128 0x10
	.long	.LASF91
	.byte	0xf
	.byte	0xae
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF92
	.byte	0xf
	.byte	0xaf
	.byte	0x11
	.long	0x87
	.uleb128 0x12
	.long	.LASF93
	.byte	0xf
	.value	0x112
	.byte	0xc
	.long	0x74
	.uleb128 0x12
	.long	.LASF94
	.byte	0x10
	.value	0x21f
	.byte	0xf
	.long	0x653
	.uleb128 0x7
	.byte	0x8
	.long	0xb4
	.uleb128 0x12
	.long	.LASF95
	.byte	0x10
	.value	0x221
	.byte	0xf
	.long	0x653
	.uleb128 0x10
	.long	.LASF96
	.byte	0x11
	.byte	0x24
	.byte	0xe
	.long	0xb4
	.uleb128 0x10
	.long	.LASF97
	.byte	0x11
	.byte	0x32
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF98
	.byte	0x11
	.byte	0x37
	.byte	0xc
	.long	0x74
	.uleb128 0x10
	.long	.LASF99
	.byte	0x11
	.byte	0x3b
	.byte	0xc
	.long	0x74
	.uleb128 0x4
	.long	.LASF100
	.byte	0x12
	.byte	0x18
	.byte	0x13
	.long	0x55
	.uleb128 0x4
	.long	.LASF101
	.byte	0x12
	.byte	0x19
	.byte	0x14
	.long	0x68
	.uleb128 0x4
	.long	.LASF102
	.byte	0x12
	.byte	0x1a
	.byte	0x14
	.long	0x7b
	.uleb128 0x4
	.long	.LASF103
	.byte	0x8
	.byte	0x1e
	.byte	0x12
	.long	0x6ae
	.uleb128 0x9
	.long	.LASF104
	.byte	0x4
	.byte	0x8
	.byte	0x1f
	.byte	0x8
	.long	0x6e1
	.uleb128 0xa
	.long	.LASF105
	.byte	0x8
	.byte	0x21
	.byte	0xf
	.long	0x6ba
	.byte	0
	.byte	0
	.uleb128 0x4
	.long	.LASF106
	.byte	0x8
	.byte	0x77
	.byte	0x12
	.long	0x6a2
	.uleb128 0x13
	.byte	0x10
	.byte	0x8
	.byte	0xd6
	.byte	0x5
	.long	0x71b
	.uleb128 0x14
	.long	.LASF107
	.byte	0x8
	.byte	0xd8
	.byte	0xa
	.long	0x71b
	.uleb128 0x14
	.long	.LASF108
	.byte	0x8
	.byte	0xd9
	.byte	0xb
	.long	0x72b
	.uleb128 0x14
	.long	.LASF109
	.byte	0x8
	.byte	0xda
	.byte	0xb
	.long	0x73b
	.byte	0
	.uleb128 0xb
	.long	0x696
	.long	0x72b
	.uleb128 0xc
	.long	0x47
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.long	0x6a2
	.long	0x73b
	.uleb128 0xc
	.long	0x47
	.byte	0x7
	.byte	0
	.uleb128 0xb
	.long	0x6ae
	.long	0x74b
	.uleb128 0xc
	.long	0x47
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF110
	.byte	0x10
	.byte	0x8
	.byte	0xd4
	.byte	0x8
	.long	0x766
	.uleb128 0xa
	.long	.LASF111
	.byte	0x8
	.byte	0xdb
	.byte	0x9
	.long	0x6ed
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	0x74b
	.uleb128 0x10
	.long	.LASF112
	.byte	0x8
	.byte	0xe4
	.byte	0x1e
	.long	0x766
	.uleb128 0x10
	.long	.LASF113
	.byte	0x8
	.byte	0xe5
	.byte	0x1e
	.long	0x766
	.uleb128 0xb
	.long	0x2d
	.long	0x793
	.uleb128 0xc
	.long	0x47
	.byte	0x7
	.byte	0
	.uleb128 0xb
	.long	0x555
	.long	0x7a3
	.uleb128 0xc
	.long	0x47
	.byte	0x40
	.byte	0
	.uleb128 0x3
	.long	0x793
	.uleb128 0x12
	.long	.LASF114
	.byte	0x13
	.value	0x11e
	.byte	0x1a
	.long	0x7a3
	.uleb128 0x12
	.long	.LASF115
	.byte	0x13
	.value	0x11f
	.byte	0x1a
	.long	0x7a3
	.uleb128 0x7
	.byte	0x8
	.long	0x34
	.uleb128 0x9
	.long	.LASF116
	.byte	0x8
	.byte	0x14
	.byte	0x67
	.byte	0x8
	.long	0x7f0
	.uleb128 0xa
	.long	.LASF117
	.byte	0x14
	.byte	0x67
	.byte	0x1b
	.long	0x74
	.byte	0
	.uleb128 0xa
	.long	.LASF118
	.byte	0x14
	.byte	0x67
	.byte	0x21
	.long	0x74
	.byte	0x4
	.byte	0
	.uleb128 0x3
	.long	0x7c8
	.uleb128 0xb
	.long	0x7f0
	.long	0x800
	.uleb128 0x11
	.byte	0
	.uleb128 0x3
	.long	0x7f5
	.uleb128 0x10
	.long	.LASF116
	.byte	0x14
	.byte	0x68
	.byte	0x22
	.long	0x800
	.uleb128 0x7
	.byte	0x8
	.long	0x2d
	.uleb128 0x15
	.byte	0x10
	.byte	0x15
	.value	0x204
	.byte	0x3
	.long	0x82f
	.uleb128 0x16
	.long	.LASF119
	.byte	0x15
	.value	0x205
	.byte	0x13
	.long	0x82f
	.byte	0
	.uleb128 0xb
	.long	0x2d
	.long	0x83f
	.uleb128 0xc
	.long	0x47
	.byte	0xf
	.byte	0
	.uleb128 0x17
	.long	.LASF120
	.byte	0x10
	.byte	0x15
	.value	0x203
	.byte	0x8
	.long	0x85c
	.uleb128 0xe
	.long	.LASF121
	.byte	0x15
	.value	0x206
	.byte	0x5
	.long	0x817
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	0x83f
	.uleb128 0x10
	.long	.LASF122
	.byte	0x16
	.byte	0x52
	.byte	0x23
	.long	0x85c
	.uleb128 0x18
	.long	0xa6
	.long	0x87c
	.uleb128 0x19
	.long	0xd7
	.byte	0
	.uleb128 0x12
	.long	.LASF123
	.byte	0x17
	.value	0x151
	.byte	0x10
	.long	0x889
	.uleb128 0x7
	.byte	0x8
	.long	0x86d
	.uleb128 0x18
	.long	0xa6
	.long	0x8a3
	.uleb128 0x19
	.long	0xa6
	.uleb128 0x19
	.long	0xd7
	.byte	0
	.uleb128 0x12
	.long	.LASF124
	.byte	0x17
	.value	0x152
	.byte	0x10
	.long	0x8b0
	.uleb128 0x7
	.byte	0x8
	.long	0x88f
	.uleb128 0x1a
	.long	0x8c1
	.uleb128 0x19
	.long	0xa6
	.byte	0
	.uleb128 0x12
	.long	.LASF125
	.byte	0x17
	.value	0x153
	.byte	0xf
	.long	0x8ce
	.uleb128 0x7
	.byte	0x8
	.long	0x8b6
	.uleb128 0x1b
	.long	.LASF138
	.byte	0x1
	.byte	0x24
	.byte	0x5
	.long	0x74
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0xa18
	.uleb128 0x1c
	.long	.LASF126
	.byte	0x1
	.byte	0x24
	.byte	0x2d
	.long	0x7c2
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x1c
	.long	.LASF127
	.byte	0x1
	.byte	0x25
	.byte	0x2d
	.long	0x7c2
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x1c
	.long	.LASF128
	.byte	0x1
	.byte	0x26
	.byte	0x1c
	.long	0x74
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x1d
	.string	"s"
	.byte	0x1
	.byte	0x27
	.byte	0x28
	.long	0xa18
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x1c
	.long	.LASF129
	.byte	0x1
	.byte	0x28
	.byte	0x1e
	.long	0xa1e
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x1e
	.string	"q"
	.byte	0x1
	.byte	0x2a
	.byte	0x12
	.long	0x811
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x13
	.byte	0x8
	.byte	0x1
	.byte	0x2b
	.byte	0x3
	.long	0x98c
	.uleb128 0x1f
	.string	"sig"
	.byte	0x1
	.byte	0x2c
	.byte	0x12
	.long	0x35b
	.uleb128 0x1f
	.string	"uns"
	.byte	0x1
	.byte	0x2d
	.byte	0xd
	.long	0xd7
	.byte	0
	.uleb128 0x20
	.long	.LASF130
	.byte	0x1
	.byte	0x2e
	.byte	0x5
	.long	0x96a
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x21
	.long	0xa24
	.quad	.LBI4
	.byte	.LVU25
	.quad	.LBB4
	.quad	.LBE4-.LBB4
	.byte	0x1
	.byte	0x3d
	.byte	0x3
	.long	0xa07
	.uleb128 0x22
	.long	0xa4d
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x22
	.long	0xa41
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x22
	.long	0xa35
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x23
	.quad	.LVL7
	.long	0xa5a
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x25
	.quad	.LVL5
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.long	0x811
	.uleb128 0x7
	.byte	0x8
	.long	0x87
	.uleb128 0x26
	.long	.LASF139
	.byte	0x2
	.byte	0x67
	.byte	0x2a
	.long	0xb4
	.byte	0x3
	.long	0xa5a
	.uleb128 0x27
	.long	.LASF131
	.byte	0x2
	.byte	0x67
	.byte	0x44
	.long	0xba
	.uleb128 0x27
	.long	.LASF132
	.byte	0x2
	.byte	0x67
	.byte	0x63
	.long	0x55a
	.uleb128 0x27
	.long	.LASF133
	.byte	0x2
	.byte	0x67
	.byte	0x71
	.long	0xd7
	.byte	0
	.uleb128 0x28
	.long	.LASF139
	.long	.LASF140
	.byte	0x18
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU18
	.uleb128 .LVU18
	.uleb128 .LVU37
	.uleb128 .LVU39
	.uleb128 .LVU43
	.uleb128 .LVU43
	.uleb128 .LVU45
	.uleb128 .LVU45
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL4-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL10-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL13-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x6
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x23
	.uleb128 0x1
	.byte	0x9f
	.quad	.LVL15-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU7
	.uleb128 .LVU7
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL1-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU15
	.uleb128 .LVU15
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL3-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 0
	.uleb128 .LVU20
	.uleb128 .LVU20
	.uleb128 .LVU37
	.uleb128 .LVU37
	.uleb128 .LVU39
	.uleb128 .LVU39
	.uleb128 .LVU42
	.uleb128 .LVU42
	.uleb128 .LVU45
	.uleb128 .LVU45
	.uleb128 0
.LLST3:
	.quad	.LVL0-.Ltext0
	.quad	.LVL5-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL5-1-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL8-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL12-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	.LVL15-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 0
	.uleb128 .LVU20
	.uleb128 .LVU20
	.uleb128 .LVU37
	.uleb128 .LVU37
	.uleb128 .LVU39
	.uleb128 .LVU39
	.uleb128 .LVU44
	.uleb128 .LVU44
	.uleb128 .LVU45
	.uleb128 .LVU45
	.uleb128 0
.LLST4:
	.quad	.LVL0-.Ltext0
	.quad	.LVL5-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL5-1-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL8-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x58
	.byte	0x9f
	.quad	.LVL10-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL14-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x58
	.byte	0x9f
	.quad	.LVL15-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU24
	.uleb128 .LVU28
.LLST5:
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU13
	.uleb128 .LVU38
	.uleb128 .LVU39
	.uleb128 .LVU41
.LLST6:
	.quad	.LVL2-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x3
	.byte	0x5f
	.byte	0x93
	.uleb128 0x8
	.quad	.LVL10-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x3
	.byte	0x5f
	.byte	0x93
	.uleb128 0x8
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU25
	.uleb128 .LVU28
.LLST7:
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU25
	.uleb128 .LVU28
.LLST8:
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU25
	.uleb128 .LVU28
.LLST9:
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF46:
	.string	"_IO_read_ptr"
.LASF58:
	.string	"_chain"
.LASF126:
	.string	"encoded"
.LASF35:
	.string	"sin6_addr"
.LASF111:
	.string	"__in6_u"
.LASF15:
	.string	"size_t"
.LASF64:
	.string	"_shortbuf"
.LASF120:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF14:
	.string	"ssize_t"
.LASF139:
	.string	"strncpy"
.LASF52:
	.string	"_IO_buf_base"
.LASF16:
	.string	"long long unsigned int"
.LASF103:
	.string	"in_addr_t"
.LASF132:
	.string	"__src"
.LASF38:
	.string	"sockaddr_ipx"
.LASF67:
	.string	"_codecvt"
.LASF89:
	.string	"__timezone"
.LASF86:
	.string	"program_invocation_short_name"
.LASF17:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF37:
	.string	"sockaddr_inarp"
.LASF21:
	.string	"sockaddr_at"
.LASF59:
	.string	"_fileno"
.LASF138:
	.string	"ares_expand_string"
.LASF47:
	.string	"_IO_read_end"
.LASF137:
	.string	"_IO_lock_t"
.LASF108:
	.string	"__u6_addr16"
.LASF115:
	.string	"sys_siglist"
.LASF9:
	.string	"long int"
.LASF43:
	.string	"ares_ssize_t"
.LASF45:
	.string	"_flags"
.LASF12:
	.string	"__ssize_t"
.LASF53:
	.string	"_IO_buf_end"
.LASF78:
	.string	"stdin"
.LASF140:
	.string	"__builtin_strncpy"
.LASF76:
	.string	"_IO_codecvt"
.LASF23:
	.string	"sockaddr_dl"
.LASF33:
	.string	"sin6_port"
.LASF101:
	.string	"uint16_t"
.LASF84:
	.string	"_sys_errlist"
.LASF85:
	.string	"program_invocation_name"
.LASF61:
	.string	"_old_offset"
.LASF66:
	.string	"_offset"
.LASF125:
	.string	"ares_free"
.LASF118:
	.string	"shift"
.LASF113:
	.string	"in6addr_loopback"
.LASF42:
	.string	"sockaddr_x25"
.LASF7:
	.string	"__uint16_t"
.LASF8:
	.string	"__uint32_t"
.LASF117:
	.string	"mask"
.LASF119:
	.string	"_S6_u8"
.LASF30:
	.string	"sin_zero"
.LASF129:
	.string	"enclen"
.LASF75:
	.string	"_IO_marker"
.LASF2:
	.string	"unsigned int"
.LASF105:
	.string	"s_addr"
.LASF70:
	.string	"_freeres_buf"
.LASF130:
	.string	"elen"
.LASF3:
	.string	"long unsigned int"
.LASF116:
	.string	"_ns_flagdata"
.LASF50:
	.string	"_IO_write_ptr"
.LASF123:
	.string	"ares_malloc"
.LASF81:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF29:
	.string	"sin_addr"
.LASF121:
	.string	"_S6_un"
.LASF136:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF54:
	.string	"_IO_save_base"
.LASF95:
	.string	"environ"
.LASF65:
	.string	"_lock"
.LASF109:
	.string	"__u6_addr32"
.LASF106:
	.string	"in_port_t"
.LASF79:
	.string	"stdout"
.LASF41:
	.string	"sockaddr_un"
.LASF27:
	.string	"sin_family"
.LASF96:
	.string	"optarg"
.LASF93:
	.string	"getdate_err"
.LASF32:
	.string	"sin6_family"
.LASF97:
	.string	"optind"
.LASF51:
	.string	"_IO_write_end"
.LASF131:
	.string	"__dest"
.LASF40:
	.string	"sockaddr_ns"
.LASF112:
	.string	"in6addr_any"
.LASF44:
	.string	"_IO_FILE"
.LASF94:
	.string	"__environ"
.LASF88:
	.string	"__daylight"
.LASF72:
	.string	"_mode"
.LASF28:
	.string	"sin_port"
.LASF19:
	.string	"sa_family"
.LASF82:
	.string	"sys_errlist"
.LASF57:
	.string	"_markers"
.LASF135:
	.string	"../deps/cares/src/ares_expand_string.c"
.LASF36:
	.string	"sin6_scope_id"
.LASF0:
	.string	"unsigned char"
.LASF39:
	.string	"sockaddr_iso"
.LASF5:
	.string	"short int"
.LASF77:
	.string	"_IO_wide_data"
.LASF127:
	.string	"abuf"
.LASF133:
	.string	"__len"
.LASF83:
	.string	"_sys_nerr"
.LASF63:
	.string	"_vtable_offset"
.LASF90:
	.string	"tzname"
.LASF22:
	.string	"sockaddr_ax25"
.LASF74:
	.string	"FILE"
.LASF110:
	.string	"in6_addr"
.LASF99:
	.string	"optopt"
.LASF91:
	.string	"daylight"
.LASF13:
	.string	"char"
.LASF34:
	.string	"sin6_flowinfo"
.LASF128:
	.string	"alen"
.LASF107:
	.string	"__u6_addr8"
.LASF98:
	.string	"opterr"
.LASF11:
	.string	"__off64_t"
.LASF62:
	.string	"_cur_column"
.LASF48:
	.string	"_IO_read_base"
.LASF56:
	.string	"_IO_save_end"
.LASF114:
	.string	"_sys_siglist"
.LASF92:
	.string	"timezone"
.LASF24:
	.string	"sockaddr_eon"
.LASF71:
	.string	"__pad5"
.LASF124:
	.string	"ares_realloc"
.LASF18:
	.string	"sa_family_t"
.LASF73:
	.string	"_unused2"
.LASF80:
	.string	"stderr"
.LASF31:
	.string	"sockaddr_in6"
.LASF25:
	.string	"sockaddr"
.LASF26:
	.string	"sockaddr_in"
.LASF100:
	.string	"uint8_t"
.LASF60:
	.string	"_flags2"
.LASF55:
	.string	"_IO_backup_base"
.LASF20:
	.string	"sa_data"
.LASF69:
	.string	"_freeres_list"
.LASF134:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF68:
	.string	"_wide_data"
.LASF122:
	.string	"ares_in6addr_any"
.LASF87:
	.string	"__tzname"
.LASF49:
	.string	"_IO_write_base"
.LASF102:
	.string	"uint32_t"
.LASF104:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
