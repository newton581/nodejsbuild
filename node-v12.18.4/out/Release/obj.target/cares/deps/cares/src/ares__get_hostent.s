	.file	"ares__get_hostent.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares__get_hostent
	.type	ares__get_hostent, @function
ares__get_hostent:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares__get_hostent.c"
	.loc 1 35 1 view -0
	.cfi_startproc
	.loc 1 35 1 is_stmt 0 view .LVU1
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	.loc 1 35 1 view .LVU2
	movq	%rdx, -136(%rbp)
	movq	%fs:40, %rsi
	movq	%rsi, -56(%rbp)
	xorl	%esi, %esi
.LVL1:
	.loc 1 36 3 is_stmt 1 view .LVU3
	.loc 1 36 9 is_stmt 0 view .LVU4
	movq	$0, -112(%rbp)
	.loc 1 37 3 is_stmt 1 view .LVU5
	.loc 1 38 3 view .LVU6
	.loc 1 39 3 view .LVU7
	.loc 1 40 3 view .LVU8
	.loc 1 41 3 view .LVU9
.LVL2:
	.loc 1 43 3 view .LVU10
	.loc 1 43 9 is_stmt 0 view .LVU11
	movq	$0, (%rdx)
	.loc 1 46 3 is_stmt 1 view .LVU12
	cmpl	$10, %r13d
	ja	.L72
	movl	$1, %eax
	movl	%r13d, %ecx
	.loc 1 43 9 is_stmt 0 view .LVU13
	movl	$9, %r15d
	salq	%cl, %rax
	testl	$1029, %eax
	jne	.L187
.LVL3:
.L1:
	.loc 1 260 1 view .LVU14
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L188
	addq	$152, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
.LVL4:
	.loc 1 260 1 view .LVU15
	ret
.LVL5:
.L187:
	.cfi_restore_state
	.loc 1 139 41 view .LVU16
	movl	%r13d, %eax
	movq	%rdi, %rbx
	andl	$-3, %eax
	cmpl	$1, %eax
	movl	%eax, -144(%rbp)
	sbbq	%rax, %rax
	andl	$4, %eax
	movq	%rax, -168(%rbp)
	movq	%rax, -160(%rbp)
	leaq	-104(%rbp), %rax
	movq	%rax, -128(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -120(%rbp)
.LVL6:
	.p2align 4,,10
	.p2align 3
.L3:
	.loc 1 55 9 is_stmt 1 discriminator 1 view .LVU17
	.loc 1 55 20 is_stmt 0 discriminator 1 view .LVU18
	movq	-128(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movq	%rbx, %rdi
	call	ares__read_line@PLT
.LVL7:
	movl	%eax, %r15d
.LVL8:
	.loc 1 55 9 discriminator 1 view .LVU19
	testl	%eax, %eax
	jne	.L189
	.loc 1 59 7 is_stmt 1 view .LVU20
	.loc 1 59 9 is_stmt 0 view .LVU21
	movq	-112(%rbp), %rax
.LVL9:
	.loc 1 60 7 is_stmt 1 view .LVU22
	.loc 1 60 13 view .LVU23
	.loc 1 60 14 is_stmt 0 view .LVU24
	movzbl	(%rax), %edx
	.loc 1 60 13 view .LVU25
	cmpb	$35, %dl
	je	.L4
	testb	%dl, %dl
	je	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	.loc 1 61 9 is_stmt 1 view .LVU26
	movq	%rax, %r14
.LVL10:
	.loc 1 60 14 is_stmt 0 view .LVU27
	movzbl	1(%rax), %edx
	.loc 1 61 10 view .LVU28
	addq	$1, %rax
.LVL11:
	.loc 1 60 13 is_stmt 1 view .LVU29
	testb	%dl, %dl
	je	.L7
	cmpb	$35, %dl
	jne	.L5
.L7:
	.loc 1 62 7 view .LVU30
	.loc 1 62 10 is_stmt 0 view .LVU31
	movb	$0, (%rax)
	.loc 1 65 7 is_stmt 1 view .LVU32
.LVL12:
	.loc 1 66 7 view .LVU33
	.loc 1 66 13 view .LVU34
	.loc 1 66 17 is_stmt 0 view .LVU35
	movq	-112(%rbp), %r12
	.loc 1 66 13 view .LVU36
	cmpq	%r14, %r12
	ja	.L8
	.loc 1 66 31 view .LVU37
	call	__ctype_b_loc@PLT
.LVL13:
	.loc 1 66 30 view .LVU38
	movq	(%rax), %rcx
	jmp	.L9
.LVL14:
	.p2align 4,,10
	.p2align 3
.L10:
	.loc 1 67 9 is_stmt 1 view .LVU39
	.loc 1 67 10 is_stmt 0 view .LVU40
	subq	$1, %r14
.LVL15:
	.loc 1 66 13 is_stmt 1 view .LVU41
	cmpq	%r12, %r14
	jb	.L8
.LVL16:
.L9:
	.loc 1 66 49 is_stmt 0 discriminator 1 view .LVU42
	movzbl	(%r14), %eax
	.loc 1 66 26 discriminator 1 view .LVU43
	testb	$32, 1(%rcx,%rax,2)
	jne	.L10
.LVL17:
.L8:
	.loc 1 68 7 is_stmt 1 view .LVU44
	.loc 1 68 12 is_stmt 0 view .LVU45
	movb	$0, 1(%r14)
	.loc 1 71 7 is_stmt 1 view .LVU46
	.loc 1 71 9 is_stmt 0 view .LVU47
	movq	-112(%rbp), %r12
.LVL18:
	.loc 1 72 7 is_stmt 1 view .LVU48
	.loc 1 72 13 view .LVU49
	.loc 1 72 14 is_stmt 0 view .LVU50
	movzbl	(%r12), %r14d
.LVL19:
	.loc 1 72 13 view .LVU51
	testb	%r14b, %r14b
	je	.L3
	.loc 1 72 22 view .LVU52
	call	__ctype_b_loc@PLT
.LVL20:
	.loc 1 72 21 view .LVU53
	movq	(%rax), %rdx
	.loc 1 72 22 view .LVU54
	movq	%rax, %r10
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	.loc 1 73 9 is_stmt 1 view .LVU55
	.loc 1 72 14 is_stmt 0 view .LVU56
	movzbl	1(%r12), %r14d
	.loc 1 73 10 view .LVU57
	addq	$1, %r12
.LVL21:
	.loc 1 72 13 is_stmt 1 view .LVU58
	testb	%r14b, %r14b
	je	.L3
.L12:
	.loc 1 72 17 is_stmt 0 discriminator 1 view .LVU59
	testb	$32, 1(%rdx,%r14,2)
	jne	.L13
	.loc 1 82 13 is_stmt 1 view .LVU60
	.loc 1 82 14 is_stmt 0 view .LVU61
	movzbl	(%r12), %eax
	.loc 1 82 13 view .LVU62
	movq	%r12, %r14
	testb	%al, %al
	jne	.L14
	jmp	.L3
.LVL22:
	.p2align 4,,10
	.p2align 3
.L15:
	.loc 1 83 9 is_stmt 1 view .LVU63
	.loc 1 82 13 view .LVU64
	testb	%al, %al
	je	.L3
.LVL23:
.L14:
	.loc 1 82 41 is_stmt 0 discriminator 1 view .LVU65
	movzbl	%al, %ecx
	movq	%r14, %rdi
	movzbl	1(%r14), %eax
	addq	$1, %r14
.LVL24:
	.loc 1 82 17 discriminator 1 view .LVU66
	testb	$32, 1(%rdx,%rcx,2)
	je	.L15
	.loc 1 84 7 is_stmt 1 view .LVU67
	.loc 1 89 7 view .LVU68
	.loc 1 89 10 is_stmt 0 view .LVU69
	movb	$0, (%rdi)
	.loc 1 92 7 is_stmt 1 view .LVU70
.LVL25:
	.loc 1 93 7 view .LVU71
	.loc 1 93 13 view .LVU72
	testb	%al, %al
	je	.L3
	.loc 1 93 21 is_stmt 0 view .LVU73
	movq	(%r10), %rdx
	jmp	.L65
.LVL26:
	.p2align 4,,10
	.p2align 3
.L17:
	.loc 1 94 9 is_stmt 1 view .LVU74
	.loc 1 93 14 is_stmt 0 view .LVU75
	movzbl	1(%r14), %eax
	.loc 1 94 10 view .LVU76
	addq	$1, %r14
.LVL27:
	.loc 1 93 13 is_stmt 1 view .LVU77
	testb	%al, %al
	je	.L3
.LVL28:
.L65:
	.loc 1 93 17 is_stmt 0 discriminator 1 view .LVU78
	testb	$32, 1(%rdx,%rax,2)
	jne	.L17
	.loc 1 103 13 is_stmt 1 view .LVU79
	.loc 1 103 14 is_stmt 0 view .LVU80
	movzbl	(%r14), %eax
	.loc 1 103 13 view .LVU81
	movq	%r14, %r11
	testb	%al, %al
	jne	.L18
	jmp	.L19
.LVL29:
	.p2align 4,,10
	.p2align 3
.L20:
	.loc 1 104 9 is_stmt 1 view .LVU82
	.loc 1 103 13 view .LVU83
	testb	%al, %al
	je	.L19
.LVL30:
.L18:
	.loc 1 103 41 is_stmt 0 discriminator 1 view .LVU84
	movzbl	%al, %ecx
	movq	%r11, %rdi
	movzbl	1(%r11), %eax
	addq	$1, %r11
.LVL31:
	.loc 1 103 17 discriminator 1 view .LVU85
	testb	$32, 1(%rdx,%rcx,2)
	je	.L20
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L191:
.LVL32:
	.loc 1 111 21 discriminator 1 view .LVU86
	testb	$32, 1(%rdx,%rax,2)
	je	.L190
	.loc 1 112 13 is_stmt 1 view .LVU87
	.loc 1 111 18 is_stmt 0 view .LVU88
	movzbl	1(%r11), %eax
	.loc 1 112 14 view .LVU89
	addq	$1, %r11
.LVL33:
	.loc 1 111 17 is_stmt 1 view .LVU90
.L180:
	.loc 1 111 17 is_stmt 0 view .LVU91
	testb	%al, %al
	jne	.L191
	.loc 1 113 11 is_stmt 1 view .LVU92
.LVL34:
	.loc 1 118 7 view .LVU93
	.loc 1 118 10 is_stmt 0 view .LVU94
	movb	$0, (%rdi)
	.loc 1 121 7 is_stmt 1 view .LVU95
.LVL35:
	.loc 1 122 7 view .LVU96
	.loc 1 107 16 is_stmt 0 view .LVU97
	xorl	%r11d, %r11d
	.loc 1 121 16 view .LVU98
	movq	$0, -152(%rbp)
	jmp	.L63
.LVL36:
	.p2align 4,,10
	.p2align 3
.L4:
	.loc 1 121 16 view .LVU99
	leaq	-1(%rax), %r14
	jmp	.L7
.LVL37:
.L19:
	.loc 1 107 7 is_stmt 1 view .LVU100
	.loc 1 108 7 view .LVU101
	.loc 1 118 7 view .LVU102
	.loc 1 118 10 is_stmt 0 view .LVU103
	movb	$0, (%r11)
	.loc 1 121 7 is_stmt 1 view .LVU104
.LVL38:
	.loc 1 122 7 view .LVU105
	.loc 1 107 16 is_stmt 0 view .LVU106
	xorl	%r11d, %r11d
.LVL39:
.L181:
	.loc 1 121 16 view .LVU107
	movq	$0, -152(%rbp)
.LVL40:
.L63:
	.loc 1 136 7 is_stmt 1 view .LVU108
	.loc 1 137 7 view .LVU109
	.loc 1 138 7 view .LVU110
	.loc 1 137 19 is_stmt 0 view .LVU111
	movabsq	$-4294967296, %rax
	movq	%rax, -96(%rbp)
	.loc 1 139 7 is_stmt 1 view .LVU112
	.loc 1 139 10 is_stmt 0 view .LVU113
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	je	.L192
	.loc 1 148 7 is_stmt 1 view .LVU114
	.loc 1 148 10 is_stmt 0 view .LVU115
	cmpl	$10, %r13d
	jne	.L3
	.loc 1 150 15 view .LVU116
	leaq	-92(%rbp), %rdx
	movq	%r12, %rsi
	movl	$10, %edi
	movq	%r11, -184(%rbp)
	movq	%r10, -176(%rbp)
	.loc 1 150 11 is_stmt 1 view .LVU117
	.loc 1 150 15 is_stmt 0 view .LVU118
	call	ares_inet_pton@PLT
.LVL41:
	.loc 1 150 14 view .LVU119
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r11
	testl	%eax, %eax
	jg	.L176
.LVL42:
	.loc 1 157 7 is_stmt 1 view .LVU120
	.loc 1 157 10 is_stmt 0 view .LVU121
	cmpq	$0, -168(%rbp)
	je	.L3
	movq	%r14, %r8
.LVL43:
.L32:
	.loc 1 157 10 view .LVU122
	movq	%r11, -144(%rbp)
	.loc 1 166 17 view .LVU123
	movl	$32, %edi
	movq	%r8, -120(%rbp)
	movq	%r10, -128(%rbp)
	.loc 1 166 7 is_stmt 1 view .LVU124
	.loc 1 166 17 is_stmt 0 view .LVU125
	call	*ares_malloc(%rip)
.LVL44:
	movq	%rax, %r12
.LVL45:
	.loc 1 167 7 is_stmt 1 view .LVU126
	.loc 1 167 10 is_stmt 0 view .LVU127
	testq	%rax, %rax
	je	.L35
	.loc 1 171 7 is_stmt 1 view .LVU128
	.loc 1 175 25 is_stmt 0 view .LVU129
	movq	-120(%rbp), %r8
	.loc 1 171 26 view .LVU130
	movq	$0, 8(%rax)
	.loc 1 172 7 is_stmt 1 view .LVU131
	.loc 1 172 28 is_stmt 0 view .LVU132
	movq	$0, 24(%rax)
	.loc 1 175 7 is_stmt 1 view .LVU133
	.loc 1 175 25 is_stmt 0 view .LVU134
	movq	%r8, %rdi
	call	ares_strdup@PLT
.LVL46:
	.loc 1 175 23 view .LVU135
	movq	%rax, (%r12)
	.loc 1 176 7 is_stmt 1 view .LVU136
	.loc 1 176 10 is_stmt 0 view .LVU137
	testq	%rax, %rax
	je	.L36
	.loc 1 180 7 is_stmt 1 view .LVU138
	.loc 1 180 30 is_stmt 0 view .LVU139
	movl	$16, %edi
	call	*ares_malloc(%rip)
.LVL47:
	.loc 1 180 28 view .LVU140
	movq	%rax, 24(%r12)
	.loc 1 181 7 is_stmt 1 view .LVU141
	.loc 1 180 30 is_stmt 0 view .LVU142
	movq	%rax, %rbx
.LVL48:
	.loc 1 181 10 view .LVU143
	testq	%rax, %rax
	je	.L37
	.loc 1 183 7 is_stmt 1 view .LVU144
	.loc 1 183 31 is_stmt 0 view .LVU145
	movq	$0, 8(%rax)
	.loc 1 184 7 is_stmt 1 view .LVU146
	.loc 1 184 33 is_stmt 0 view .LVU147
	movq	-160(%rbp), %rdi
	call	*ares_malloc(%rip)
.LVL49:
	.loc 1 184 31 view .LVU148
	movq	%rax, (%rbx)
	.loc 1 185 7 is_stmt 1 view .LVU149
	.loc 1 185 32 is_stmt 0 view .LVU150
	movq	24(%r12), %rax
	movq	(%rax), %rax
	.loc 1 185 10 view .LVU151
	testq	%rax, %rax
	je	.L37
	.loc 1 187 7 is_stmt 1 view .LVU152
	.loc 1 187 10 is_stmt 0 view .LVU153
	cmpl	$2, -96(%rbp)
	movq	-128(%rbp), %r10
	movq	-144(%rbp), %r11
	je	.L193
	.loc 1 190 9 is_stmt 1 view .LVU154
.LVL50:
.LBB6:
.LBI6:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 31 42 view .LVU155
.LBB7:
	.loc 2 34 3 view .LVU156
	.loc 2 34 10 is_stmt 0 view .LVU157
	movdqu	-92(%rbp), %xmm0
	movups	%xmm0, (%rax)
.LVL51:
.L39:
	.loc 2 34 10 view .LVU158
.LBE7:
.LBE6:
	.loc 1 193 28 view .LVU159
	movq	-152(%rbp), %r14
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	.loc 1 193 7 is_stmt 1 view .LVU160
	.loc 1 193 28 is_stmt 0 view .LVU161
	leaq	8(,%r14,8), %r13
.LVL52:
	.loc 1 193 28 view .LVU162
	movq	%r13, %rdi
	call	*ares_malloc(%rip)
.LVL53:
	.loc 1 193 26 view .LVU163
	movq	%rax, 8(%r12)
	.loc 1 194 7 is_stmt 1 view .LVU164
	.loc 1 193 28 is_stmt 0 view .LVU165
	movq	%rax, %rbx
	.loc 1 194 10 view .LVU166
	testq	%rax, %rax
	je	.L37
.LVL54:
	.loc 1 197 13 is_stmt 1 view .LVU167
	testq	%r14, %r14
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r11
	je	.L42
	.loc 1 198 31 is_stmt 0 view .LVU168
	leaq	-8(%r13), %rdx
	leaq	8(%rax), %rdi
	xorl	%esi, %esi
	movq	%r11, -128(%rbp)
	movq	%r10, -120(%rbp)
	call	memset@PLT
.LVL55:
	.loc 1 198 31 view .LVU169
	movq	-120(%rbp), %r10
	movq	-128(%rbp), %r11
.L42:
	.loc 1 199 7 is_stmt 1 view .LVU170
	.loc 1 199 14 is_stmt 0 view .LVU171
	movq	$0, (%rbx)
	.loc 1 200 7 is_stmt 1 view .LVU172
	movq	%r10, %r13
	movq	%r11, %rdi
.L41:
.LVL56:
	.loc 1 200 13 view .LVU173
	testq	%rdi, %rdi
	je	.L48
.LVL57:
	.loc 1 203 17 view .LVU174
	.loc 1 203 18 is_stmt 0 view .LVU175
	movzbl	(%rdi), %eax
	.loc 1 203 17 view .LVU176
	testb	%al, %al
	je	.L78
	.loc 1 203 26 view .LVU177
	movq	0(%r13), %rcx
	movq	%rdi, %rdx
	jmp	.L49
.LVL58:
	.p2align 4,,10
	.p2align 3
.L44:
	.loc 1 204 13 is_stmt 1 view .LVU178
	.loc 1 203 18 is_stmt 0 view .LVU179
	movzbl	1(%rdx), %eax
	.loc 1 204 14 view .LVU180
	addq	$1, %rdx
.LVL59:
	.loc 1 203 17 is_stmt 1 view .LVU181
	testb	%al, %al
	je	.L43
.L49:
	.loc 1 203 21 is_stmt 0 discriminator 1 view .LVU182
	testb	$32, 1(%rcx,%rax,2)
	je	.L44
.LVL60:
	.loc 1 206 17 is_stmt 1 view .LVU183
	.loc 1 206 18 is_stmt 0 view .LVU184
	movzbl	(%rdx), %eax
	.loc 1 206 17 view .LVU185
	movq	%rdx, %r14
	testb	%al, %al
	jne	.L45
	jmp	.L46
.LVL61:
	.p2align 4,,10
	.p2align 3
.L47:
	.loc 1 207 13 is_stmt 1 view .LVU186
	.loc 1 206 18 is_stmt 0 view .LVU187
	movzbl	1(%r14), %eax
	.loc 1 207 14 view .LVU188
	addq	$1, %r14
.LVL62:
	.loc 1 206 17 is_stmt 1 view .LVU189
	testb	%al, %al
	je	.L46
.L45:
	.loc 1 206 21 is_stmt 0 discriminator 1 view .LVU190
	testb	$32, 1(%rcx,%rax,2)
	jne	.L47
.L46:
	.loc 1 208 11 is_stmt 1 view .LVU191
	.loc 1 208 14 is_stmt 0 view .LVU192
	movb	$0, (%rdx)
	.loc 1 209 11 is_stmt 1 view .LVU193
	.loc 1 209 25 is_stmt 0 view .LVU194
	call	ares_strdup@PLT
.LVL63:
	.loc 1 209 23 view .LVU195
	movq	%rax, (%rbx)
	.loc 1 209 14 view .LVU196
	testq	%rax, %rax
	je	.L37
	.loc 1 211 11 is_stmt 1 view .LVU197
	.loc 1 211 16 is_stmt 0 view .LVU198
	addq	$8, %rbx
.LVL64:
	.loc 1 212 11 is_stmt 1 view .LVU199
	.loc 1 212 29 is_stmt 0 view .LVU200
	cmpb	$0, (%r14)
	je	.L48
	.loc 1 212 29 view .LVU201
	movq	%r14, %rdi
	jmp	.L41
.LVL65:
.L189:
	.loc 1 232 3 is_stmt 1 view .LVU202
	.loc 1 232 7 is_stmt 0 view .LVU203
	movq	-112(%rbp), %rdi
	.loc 1 232 6 view .LVU204
	testq	%rdi, %rdi
	je	.L1
	.loc 1 233 5 is_stmt 1 view .LVU205
	call	*ares_free(%rip)
.LVL66:
	.loc 1 235 3 view .LVU206
	jmp	.L1
.LVL67:
.L72:
	.loc 1 43 9 is_stmt 0 view .LVU207
	movl	$9, %r15d
	jmp	.L1
.LVL68:
.L36:
	.loc 1 232 3 is_stmt 1 view .LVU208
	.loc 1 232 7 is_stmt 0 view .LVU209
	movq	-112(%rbp), %rdi
	.loc 1 232 6 view .LVU210
	testq	%rdi, %rdi
	je	.L182
.LVL69:
.L66:
	.loc 1 233 5 is_stmt 1 view .LVU211
	call	*ares_free(%rip)
.LVL70:
	.loc 1 235 3 view .LVU212
	.loc 1 238 7 view .LVU213
.L53:
	.loc 1 240 11 view .LVU214
	.loc 1 240 22 is_stmt 0 view .LVU215
	movq	(%r12), %rdi
	movq	ares_free(%rip), %rax
	.loc 1 240 14 view .LVU216
	testq	%rdi, %rdi
	je	.L54
	.loc 1 241 13 is_stmt 1 view .LVU217
	call	*%rax
.LVL71:
.L182:
	.loc 1 241 13 is_stmt 0 view .LVU218
	movq	ares_free(%rip), %rax
.L54:
	.loc 1 242 11 is_stmt 1 view .LVU219
	.loc 1 242 22 is_stmt 0 view .LVU220
	movq	8(%r12), %r13
	.loc 1 242 14 view .LVU221
	testq	%r13, %r13
	je	.L55
.LVL72:
	.loc 1 244 48 is_stmt 1 view .LVU222
	movq	0(%r13), %rdi
	.loc 1 244 15 is_stmt 0 view .LVU223
	testq	%rdi, %rdi
	je	.L56
.L58:
	.loc 1 245 17 is_stmt 1 discriminator 3 view .LVU224
	call	*%rax
.LVL73:
	.loc 1 244 56 discriminator 3 view .LVU225
	.loc 1 244 48 is_stmt 0 discriminator 3 view .LVU226
	movq	8(%r13), %rdi
	.loc 1 244 61 discriminator 3 view .LVU227
	addq	$8, %r13
.LVL74:
	.loc 1 244 48 is_stmt 1 discriminator 3 view .LVU228
	movq	ares_free(%rip), %rax
	.loc 1 244 15 is_stmt 0 discriminator 3 view .LVU229
	testq	%rdi, %rdi
	jne	.L58
	movq	8(%r12), %r13
.LVL75:
.L56:
	.loc 1 246 15 is_stmt 1 view .LVU230
	movq	%r13, %rdi
	call	*%rax
.LVL76:
	movq	ares_free(%rip), %rax
.L55:
	.loc 1 248 11 view .LVU231
	.loc 1 248 22 is_stmt 0 view .LVU232
	movq	24(%r12), %rdi
	.loc 1 248 14 view .LVU233
	testq	%rdi, %rdi
	je	.L59
	.loc 1 250 15 is_stmt 1 view .LVU234
	.loc 1 250 39 is_stmt 0 view .LVU235
	movq	(%rdi), %r8
	.loc 1 250 18 view .LVU236
	testq	%r8, %r8
	je	.L60
	.loc 1 251 17 is_stmt 1 view .LVU237
	movq	%r8, %rdi
	call	*%rax
.LVL77:
	movq	ares_free(%rip), %rax
	movq	24(%r12), %rdi
.L60:
	.loc 1 252 15 view .LVU238
	call	*%rax
.LVL78:
	movq	ares_free(%rip), %rax
.L59:
	.loc 1 254 11 view .LVU239
	movq	%r12, %rdi
	call	*%rax
.LVL79:
.L183:
	.loc 1 256 14 is_stmt 0 view .LVU240
	movl	$15, %r15d
.LVL80:
	.loc 1 256 14 view .LVU241
	jmp	.L1
.LVL81:
.L192:
	.loc 1 141 15 view .LVU242
	leaq	-92(%rbp), %rdx
	movq	%r12, %rsi
	movl	$2, %edi
	movq	%r11, -192(%rbp)
	movq	%r10, -184(%rbp)
	.loc 1 141 11 is_stmt 1 view .LVU243
	.loc 1 141 15 is_stmt 0 view .LVU244
	movq	%rdx, -176(%rbp)
	call	ares_inet_pton@PLT
.LVL82:
	.loc 1 141 14 view .LVU245
	movq	-176(%rbp), %rdx
	movq	-184(%rbp), %r10
	testl	%eax, %eax
	movq	-192(%rbp), %r11
	jle	.L31
	.loc 1 144 27 view .LVU246
	movl	$2, -96(%rbp)
	movq	%r14, %r8
	.loc 1 144 15 is_stmt 1 view .LVU247
	.loc 1 145 15 view .LVU248
.LVL83:
	.loc 1 148 7 view .LVU249
	.loc 1 145 23 is_stmt 0 view .LVU250
	movq	$4, -160(%rbp)
	jmp	.L32
.LVL84:
.L31:
	.loc 1 148 7 is_stmt 1 discriminator 1 view .LVU251
	.loc 1 148 31 is_stmt 0 discriminator 1 view .LVU252
	testl	%r13d, %r13d
	jne	.L3
	.loc 1 150 15 view .LVU253
	movq	%r12, %rsi
	movl	$10, %edi
	movq	%r11, -184(%rbp)
	movq	%r10, -176(%rbp)
	.loc 1 150 11 is_stmt 1 view .LVU254
	.loc 1 150 15 is_stmt 0 view .LVU255
	call	ares_inet_pton@PLT
.LVL85:
	.loc 1 150 14 view .LVU256
	movq	-176(%rbp), %r10
	movq	-184(%rbp), %r11
	testl	%eax, %eax
	jle	.L3
.L176:
	.loc 1 153 27 view .LVU257
	movl	$10, -96(%rbp)
	movq	%r14, %r8
	.loc 1 153 15 is_stmt 1 view .LVU258
	.loc 1 154 15 view .LVU259
.LVL86:
	.loc 1 157 7 view .LVU260
	.loc 1 154 23 is_stmt 0 view .LVU261
	movq	$16, -160(%rbp)
	jmp	.L32
.LVL87:
.L35:
	.loc 1 232 3 is_stmt 1 view .LVU262
	.loc 1 232 7 is_stmt 0 view .LVU263
	movq	-112(%rbp), %rdi
	.loc 1 232 6 view .LVU264
	testq	%rdi, %rdi
	je	.L183
	.loc 1 233 5 is_stmt 1 view .LVU265
	call	*ares_free(%rip)
.LVL88:
	.loc 1 235 3 view .LVU266
	.loc 1 238 7 view .LVU267
	jmp	.L183
.LVL89:
.L37:
	.loc 1 232 3 view .LVU268
	.loc 1 232 7 is_stmt 0 view .LVU269
	movq	-112(%rbp), %rdi
	.loc 1 232 6 view .LVU270
	testq	%rdi, %rdi
	jne	.L66
	jmp	.L53
.LVL90:
.L193:
	.loc 1 188 9 is_stmt 1 view .LVU271
.LBB8:
.LBI8:
	.loc 2 31 42 view .LVU272
.LBB9:
	.loc 2 34 3 view .LVU273
	movl	-92(%rbp), %edx
	.loc 2 34 10 is_stmt 0 view .LVU274
	movl	%edx, (%rax)
	jmp	.L39
.LVL91:
.L48:
	.loc 2 34 10 view .LVU275
.LBE9:
.LBE8:
	.loc 1 214 7 is_stmt 1 view .LVU276
	.loc 1 219 7 view .LVU277
	.loc 1 219 29 is_stmt 0 view .LVU278
	movl	-96(%rbp), %edi
	call	aresx_sitoss@PLT
.LVL92:
	.loc 1 220 27 view .LVU279
	movq	-160(%rbp), %rdi
	.loc 1 219 29 view .LVU280
	cwtl
	movl	%eax, 16(%r12)
	.loc 1 220 7 is_stmt 1 view .LVU281
	.loc 1 220 27 is_stmt 0 view .LVU282
	call	aresx_uztoss@PLT
.LVL93:
	.loc 1 223 7 view .LVU283
	movq	-112(%rbp), %rdi
	.loc 1 220 27 view .LVU284
	cwtl
	movl	%eax, 20(%r12)
	.loc 1 223 7 is_stmt 1 view .LVU285
	call	*ares_free(%rip)
.LVL94:
	.loc 1 226 7 view .LVU286
	.loc 1 226 13 is_stmt 0 view .LVU287
	movq	-136(%rbp), %rax
	movq	%r12, (%rax)
	.loc 1 227 7 is_stmt 1 view .LVU288
	.loc 1 227 14 is_stmt 0 view .LVU289
	jmp	.L1
.LVL95:
.L190:
	.loc 1 113 11 is_stmt 1 view .LVU290
	.loc 1 114 13 view .LVU291
	.loc 1 118 7 view .LVU292
	.loc 1 118 10 is_stmt 0 view .LVU293
	movb	$0, (%rdi)
	.loc 1 121 7 is_stmt 1 view .LVU294
.LVL96:
	.loc 1 122 7 view .LVU295
	.loc 1 125 17 view .LVU296
	.loc 1 125 18 is_stmt 0 view .LVU297
	movzbl	(%r11), %edx
	.loc 1 125 17 view .LVU298
	testb	%dl, %dl
	je	.L181
	.loc 1 127 21 is_stmt 1 view .LVU299
	.loc 1 127 30 is_stmt 0 view .LVU300
	movq	(%r10), %rcx
	movq	%r11, %rax
	.loc 1 121 16 view .LVU301
	xorl	%esi, %esi
	jmp	.L69
.LVL97:
	.p2align 4,,10
	.p2align 3
.L26:
	.loc 1 128 17 is_stmt 1 view .LVU302
	.loc 1 127 22 is_stmt 0 view .LVU303
	movzbl	1(%rax), %edx
	.loc 1 128 18 view .LVU304
	leaq	1(%rax), %rdi
.LVL98:
	.loc 1 127 21 is_stmt 1 view .LVU305
	testb	%dl, %dl
	je	.L175
.L29:
.LVL99:
	.loc 1 127 21 is_stmt 0 view .LVU306
	movq	%rdi, %rax
.L69:
.LVL100:
	.loc 1 127 25 discriminator 1 view .LVU307
	testb	$32, 1(%rcx,%rdx,2)
	je	.L26
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L28:
	.loc 1 130 17 is_stmt 1 view .LVU308
	.loc 1 130 18 is_stmt 0 view .LVU309
	addq	$1, %rax
.LVL101:
.L185:
	.loc 1 129 21 is_stmt 1 view .LVU310
	.loc 1 129 22 is_stmt 0 view .LVU311
	movzbl	(%rax), %edx
	.loc 1 129 21 view .LVU312
	testb	%dl, %dl
	je	.L175
	.loc 1 129 25 discriminator 1 view .LVU313
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L28
	.loc 1 131 15 is_stmt 1 view .LVU314
	.loc 1 125 18 is_stmt 0 view .LVU315
	movzbl	(%rax), %edx
	.loc 1 131 23 view .LVU316
	addq	$1, %rsi
.LVL102:
	.loc 1 125 17 is_stmt 1 view .LVU317
	testb	%dl, %dl
	je	.L194
	movq	%rax, %rdi
	jmp	.L29
.LVL103:
.L175:
	.loc 1 125 17 is_stmt 0 view .LVU318
	movq	%rsi, -152(%rbp)
	.loc 1 131 15 is_stmt 1 view .LVU319
.LVL104:
	.loc 1 125 17 view .LVU320
	.loc 1 131 23 is_stmt 0 view .LVU321
	addq	$1, -152(%rbp)
	jmp	.L63
.LVL105:
.L78:
	.loc 1 203 17 view .LVU322
	movq	%rdi, %rdx
.LVL106:
.L43:
	.loc 1 206 17 is_stmt 1 view .LVU323
	movq	%rdx, %r14
	jmp	.L46
.LVL107:
.L188:
	.loc 1 260 1 is_stmt 0 view .LVU324
	call	__stack_chk_fail@PLT
.LVL108:
.L194:
	.loc 1 260 1 view .LVU325
	movq	%rsi, -152(%rbp)
	jmp	.L63
	.cfi_endproc
.LFE87:
	.size	ares__get_hostent, .-ares__get_hostent
.Letext0:
	.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 7 "/usr/include/netinet/in.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/errno.h"
	.file 13 "/usr/include/time.h"
	.file 14 "/usr/include/unistd.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 17 "/usr/include/netdb.h"
	.file 18 "../deps/cares/include/ares.h"
	.file 19 "../deps/cares/src/ares_ipv6.h"
	.file 20 "../deps/cares/src/ares_private.h"
	.file 21 "/usr/include/ctype.h"
	.file 22 "../deps/cares/src/ares_strdup.h"
	.file 23 "../deps/cares/src/ares_nowarn.h"
	.file 24 "<built-in>"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xd5a
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF162
	.byte	0x1
	.long	.LASF163
	.long	.LASF164
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x3
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x3
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x3
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x3
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x3
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.long	0xa1
	.uleb128 0x7
	.byte	0x8
	.long	0xae
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x8
	.long	0xae
	.uleb128 0x3
	.long	.LASF13
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF14
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF15
	.uleb128 0x3
	.long	.LASF16
	.byte	0x5
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x9
	.long	.LASF23
	.byte	0x10
	.byte	0x6
	.byte	0xb2
	.byte	0x8
	.long	0x108
	.uleb128 0xa
	.long	.LASF17
	.byte	0x6
	.byte	0xb4
	.byte	0x11
	.long	0xd4
	.byte	0
	.uleb128 0xa
	.long	.LASF18
	.byte	0x6
	.byte	0xb5
	.byte	0xa
	.long	0x10d
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.long	0xe0
	.uleb128 0xb
	.long	0xae
	.long	0x11d
	.uleb128 0xc
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.long	0xe0
	.uleb128 0x6
	.long	0x11d
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x8
	.long	0x128
	.uleb128 0x7
	.byte	0x8
	.long	0x128
	.uleb128 0x6
	.long	0x132
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x8
	.long	0x13d
	.uleb128 0x7
	.byte	0x8
	.long	0x13d
	.uleb128 0x6
	.long	0x147
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x8
	.long	0x152
	.uleb128 0x7
	.byte	0x8
	.long	0x152
	.uleb128 0x6
	.long	0x15c
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x8
	.long	0x167
	.uleb128 0x7
	.byte	0x8
	.long	0x167
	.uleb128 0x6
	.long	0x171
	.uleb128 0x9
	.long	.LASF24
	.byte	0x10
	.byte	0x7
	.byte	0xee
	.byte	0x8
	.long	0x1be
	.uleb128 0xa
	.long	.LASF25
	.byte	0x7
	.byte	0xf0
	.byte	0x11
	.long	0xd4
	.byte	0
	.uleb128 0xa
	.long	.LASF26
	.byte	0x7
	.byte	0xf1
	.byte	0xf
	.long	0x71e
	.byte	0x2
	.uleb128 0xa
	.long	.LASF27
	.byte	0x7
	.byte	0xf2
	.byte	0x14
	.long	0x703
	.byte	0x4
	.uleb128 0xa
	.long	.LASF28
	.byte	0x7
	.byte	0xf5
	.byte	0x13
	.long	0x7c0
	.byte	0x8
	.byte	0
	.uleb128 0x8
	.long	0x17c
	.uleb128 0x7
	.byte	0x8
	.long	0x17c
	.uleb128 0x6
	.long	0x1c3
	.uleb128 0x9
	.long	.LASF29
	.byte	0x1c
	.byte	0x7
	.byte	0xfd
	.byte	0x8
	.long	0x221
	.uleb128 0xa
	.long	.LASF30
	.byte	0x7
	.byte	0xff
	.byte	0x11
	.long	0xd4
	.byte	0
	.uleb128 0xe
	.long	.LASF31
	.byte	0x7
	.value	0x100
	.byte	0xf
	.long	0x71e
	.byte	0x2
	.uleb128 0xe
	.long	.LASF32
	.byte	0x7
	.value	0x101
	.byte	0xe
	.long	0x6eb
	.byte	0x4
	.uleb128 0xe
	.long	.LASF33
	.byte	0x7
	.value	0x102
	.byte	0x15
	.long	0x788
	.byte	0x8
	.uleb128 0xe
	.long	.LASF34
	.byte	0x7
	.value	0x103
	.byte	0xe
	.long	0x6eb
	.byte	0x18
	.byte	0
	.uleb128 0x8
	.long	0x1ce
	.uleb128 0x7
	.byte	0x8
	.long	0x1ce
	.uleb128 0x6
	.long	0x226
	.uleb128 0xd
	.long	.LASF35
	.uleb128 0x8
	.long	0x231
	.uleb128 0x7
	.byte	0x8
	.long	0x231
	.uleb128 0x6
	.long	0x23b
	.uleb128 0xd
	.long	.LASF36
	.uleb128 0x8
	.long	0x246
	.uleb128 0x7
	.byte	0x8
	.long	0x246
	.uleb128 0x6
	.long	0x250
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x8
	.long	0x25b
	.uleb128 0x7
	.byte	0x8
	.long	0x25b
	.uleb128 0x6
	.long	0x265
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x8
	.long	0x270
	.uleb128 0x7
	.byte	0x8
	.long	0x270
	.uleb128 0x6
	.long	0x27a
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x8
	.long	0x285
	.uleb128 0x7
	.byte	0x8
	.long	0x285
	.uleb128 0x6
	.long	0x28f
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x8
	.long	0x29a
	.uleb128 0x7
	.byte	0x8
	.long	0x29a
	.uleb128 0x6
	.long	0x2a4
	.uleb128 0x7
	.byte	0x8
	.long	0x108
	.uleb128 0x6
	.long	0x2af
	.uleb128 0x7
	.byte	0x8
	.long	0x12d
	.uleb128 0x6
	.long	0x2ba
	.uleb128 0x7
	.byte	0x8
	.long	0x142
	.uleb128 0x6
	.long	0x2c5
	.uleb128 0x7
	.byte	0x8
	.long	0x157
	.uleb128 0x6
	.long	0x2d0
	.uleb128 0x7
	.byte	0x8
	.long	0x16c
	.uleb128 0x6
	.long	0x2db
	.uleb128 0x7
	.byte	0x8
	.long	0x1be
	.uleb128 0x6
	.long	0x2e6
	.uleb128 0x7
	.byte	0x8
	.long	0x221
	.uleb128 0x6
	.long	0x2f1
	.uleb128 0x7
	.byte	0x8
	.long	0x236
	.uleb128 0x6
	.long	0x2fc
	.uleb128 0x7
	.byte	0x8
	.long	0x24b
	.uleb128 0x6
	.long	0x307
	.uleb128 0x7
	.byte	0x8
	.long	0x260
	.uleb128 0x6
	.long	0x312
	.uleb128 0x7
	.byte	0x8
	.long	0x275
	.uleb128 0x6
	.long	0x31d
	.uleb128 0x7
	.byte	0x8
	.long	0x28a
	.uleb128 0x6
	.long	0x328
	.uleb128 0x7
	.byte	0x8
	.long	0x29f
	.uleb128 0x6
	.long	0x333
	.uleb128 0xb
	.long	0xae
	.long	0x34e
	.uleb128 0xc
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x9
	.long	.LASF41
	.byte	0xd8
	.byte	0x8
	.byte	0x31
	.byte	0x8
	.long	0x4d5
	.uleb128 0xa
	.long	.LASF42
	.byte	0x8
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0xa
	.long	.LASF43
	.byte	0x8
	.byte	0x36
	.byte	0x9
	.long	0xa8
	.byte	0x8
	.uleb128 0xa
	.long	.LASF44
	.byte	0x8
	.byte	0x37
	.byte	0x9
	.long	0xa8
	.byte	0x10
	.uleb128 0xa
	.long	.LASF45
	.byte	0x8
	.byte	0x38
	.byte	0x9
	.long	0xa8
	.byte	0x18
	.uleb128 0xa
	.long	.LASF46
	.byte	0x8
	.byte	0x39
	.byte	0x9
	.long	0xa8
	.byte	0x20
	.uleb128 0xa
	.long	.LASF47
	.byte	0x8
	.byte	0x3a
	.byte	0x9
	.long	0xa8
	.byte	0x28
	.uleb128 0xa
	.long	.LASF48
	.byte	0x8
	.byte	0x3b
	.byte	0x9
	.long	0xa8
	.byte	0x30
	.uleb128 0xa
	.long	.LASF49
	.byte	0x8
	.byte	0x3c
	.byte	0x9
	.long	0xa8
	.byte	0x38
	.uleb128 0xa
	.long	.LASF50
	.byte	0x8
	.byte	0x3d
	.byte	0x9
	.long	0xa8
	.byte	0x40
	.uleb128 0xa
	.long	.LASF51
	.byte	0x8
	.byte	0x40
	.byte	0x9
	.long	0xa8
	.byte	0x48
	.uleb128 0xa
	.long	.LASF52
	.byte	0x8
	.byte	0x41
	.byte	0x9
	.long	0xa8
	.byte	0x50
	.uleb128 0xa
	.long	.LASF53
	.byte	0x8
	.byte	0x42
	.byte	0x9
	.long	0xa8
	.byte	0x58
	.uleb128 0xa
	.long	.LASF54
	.byte	0x8
	.byte	0x44
	.byte	0x16
	.long	0x4ee
	.byte	0x60
	.uleb128 0xa
	.long	.LASF55
	.byte	0x8
	.byte	0x46
	.byte	0x14
	.long	0x4f4
	.byte	0x68
	.uleb128 0xa
	.long	.LASF56
	.byte	0x8
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0xa
	.long	.LASF57
	.byte	0x8
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0xa
	.long	.LASF58
	.byte	0x8
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0xa
	.long	.LASF59
	.byte	0x8
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0xa
	.long	.LASF60
	.byte	0x8
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0xa
	.long	.LASF61
	.byte	0x8
	.byte	0x4f
	.byte	0x8
	.long	0x33e
	.byte	0x83
	.uleb128 0xa
	.long	.LASF62
	.byte	0x8
	.byte	0x51
	.byte	0xf
	.long	0x4fa
	.byte	0x88
	.uleb128 0xa
	.long	.LASF63
	.byte	0x8
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0xa
	.long	.LASF64
	.byte	0x8
	.byte	0x5b
	.byte	0x17
	.long	0x505
	.byte	0x98
	.uleb128 0xa
	.long	.LASF65
	.byte	0x8
	.byte	0x5c
	.byte	0x19
	.long	0x510
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF66
	.byte	0x8
	.byte	0x5d
	.byte	0x14
	.long	0x4f4
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF67
	.byte	0x8
	.byte	0x5e
	.byte	0x9
	.long	0xa1
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF68
	.byte	0x8
	.byte	0x5f
	.byte	0xa
	.long	0xba
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF69
	.byte	0x8
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF70
	.byte	0x8
	.byte	0x62
	.byte	0x8
	.long	0x516
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF71
	.byte	0x9
	.byte	0x7
	.byte	0x19
	.long	0x34e
	.uleb128 0xf
	.long	.LASF165
	.byte	0x8
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF72
	.uleb128 0x7
	.byte	0x8
	.long	0x4e9
	.uleb128 0x7
	.byte	0x8
	.long	0x34e
	.uleb128 0x7
	.byte	0x8
	.long	0x4e1
	.uleb128 0xd
	.long	.LASF73
	.uleb128 0x7
	.byte	0x8
	.long	0x500
	.uleb128 0xd
	.long	.LASF74
	.uleb128 0x7
	.byte	0x8
	.long	0x50b
	.uleb128 0xb
	.long	0xae
	.long	0x526
	.uleb128 0xc
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.long	0xb5
	.uleb128 0x8
	.long	0x526
	.uleb128 0x10
	.long	.LASF75
	.byte	0xa
	.byte	0x89
	.byte	0xe
	.long	0x53d
	.uleb128 0x7
	.byte	0x8
	.long	0x4d5
	.uleb128 0x10
	.long	.LASF76
	.byte	0xa
	.byte	0x8a
	.byte	0xe
	.long	0x53d
	.uleb128 0x10
	.long	.LASF77
	.byte	0xa
	.byte	0x8b
	.byte	0xe
	.long	0x53d
	.uleb128 0x10
	.long	.LASF78
	.byte	0xb
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xb
	.long	0x52c
	.long	0x572
	.uleb128 0x11
	.byte	0
	.uleb128 0x8
	.long	0x567
	.uleb128 0x10
	.long	.LASF79
	.byte	0xb
	.byte	0x1b
	.byte	0x1a
	.long	0x572
	.uleb128 0x10
	.long	.LASF80
	.byte	0xb
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF81
	.byte	0xb
	.byte	0x1f
	.byte	0x1a
	.long	0x572
	.uleb128 0x7
	.byte	0x8
	.long	0x5a6
	.uleb128 0x6
	.long	0x59b
	.uleb128 0x12
	.uleb128 0x13
	.byte	0x7
	.byte	0x4
	.long	0x3b
	.byte	0x15
	.byte	0x2f
	.byte	0x1
	.long	0x606
	.uleb128 0x14
	.long	.LASF82
	.value	0x100
	.uleb128 0x14
	.long	.LASF83
	.value	0x200
	.uleb128 0x14
	.long	.LASF84
	.value	0x400
	.uleb128 0x14
	.long	.LASF85
	.value	0x800
	.uleb128 0x14
	.long	.LASF86
	.value	0x1000
	.uleb128 0x14
	.long	.LASF87
	.value	0x2000
	.uleb128 0x14
	.long	.LASF88
	.value	0x4000
	.uleb128 0x14
	.long	.LASF89
	.value	0x8000
	.uleb128 0x15
	.long	.LASF90
	.byte	0x1
	.uleb128 0x15
	.long	.LASF91
	.byte	0x2
	.uleb128 0x15
	.long	.LASF92
	.byte	0x4
	.uleb128 0x15
	.long	.LASF93
	.byte	0x8
	.byte	0
	.uleb128 0x10
	.long	.LASF94
	.byte	0xc
	.byte	0x2d
	.byte	0xe
	.long	0xa8
	.uleb128 0x10
	.long	.LASF95
	.byte	0xc
	.byte	0x2e
	.byte	0xe
	.long	0xa8
	.uleb128 0xb
	.long	0xa8
	.long	0x62e
	.uleb128 0xc
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF96
	.byte	0xd
	.byte	0x9f
	.byte	0xe
	.long	0x61e
	.uleb128 0x10
	.long	.LASF97
	.byte	0xd
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF98
	.byte	0xd
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF99
	.byte	0xd
	.byte	0xa6
	.byte	0xe
	.long	0x61e
	.uleb128 0x10
	.long	.LASF100
	.byte	0xd
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF101
	.byte	0xd
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x16
	.long	.LASF102
	.byte	0xd
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x16
	.long	.LASF103
	.byte	0xe
	.value	0x21f
	.byte	0xf
	.long	0x690
	.uleb128 0x7
	.byte	0x8
	.long	0xa8
	.uleb128 0x16
	.long	.LASF104
	.byte	0xe
	.value	0x221
	.byte	0xf
	.long	0x690
	.uleb128 0x10
	.long	.LASF105
	.byte	0xf
	.byte	0x24
	.byte	0xe
	.long	0xa8
	.uleb128 0x10
	.long	.LASF106
	.byte	0xf
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF107
	.byte	0xf
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF108
	.byte	0xf
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF109
	.byte	0x10
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF110
	.byte	0x10
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF111
	.byte	0x10
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF112
	.byte	0x7
	.byte	0x1e
	.byte	0x12
	.long	0x6eb
	.uleb128 0x9
	.long	.LASF113
	.byte	0x4
	.byte	0x7
	.byte	0x1f
	.byte	0x8
	.long	0x71e
	.uleb128 0xa
	.long	.LASF114
	.byte	0x7
	.byte	0x21
	.byte	0xf
	.long	0x6f7
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF115
	.byte	0x7
	.byte	0x77
	.byte	0x12
	.long	0x6df
	.uleb128 0x17
	.byte	0x10
	.byte	0x7
	.byte	0xd6
	.byte	0x5
	.long	0x758
	.uleb128 0x18
	.long	.LASF116
	.byte	0x7
	.byte	0xd8
	.byte	0xa
	.long	0x758
	.uleb128 0x18
	.long	.LASF117
	.byte	0x7
	.byte	0xd9
	.byte	0xb
	.long	0x768
	.uleb128 0x18
	.long	.LASF118
	.byte	0x7
	.byte	0xda
	.byte	0xb
	.long	0x778
	.byte	0
	.uleb128 0xb
	.long	0x6d3
	.long	0x768
	.uleb128 0xc
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.long	0x6df
	.long	0x778
	.uleb128 0xc
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xb
	.long	0x6eb
	.long	0x788
	.uleb128 0xc
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF119
	.byte	0x10
	.byte	0x7
	.byte	0xd4
	.byte	0x8
	.long	0x7a3
	.uleb128 0xa
	.long	.LASF120
	.byte	0x7
	.byte	0xdb
	.byte	0x9
	.long	0x72a
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	0x788
	.uleb128 0x10
	.long	.LASF121
	.byte	0x7
	.byte	0xe4
	.byte	0x1e
	.long	0x7a3
	.uleb128 0x10
	.long	.LASF122
	.byte	0x7
	.byte	0xe5
	.byte	0x1e
	.long	0x7a3
	.uleb128 0xb
	.long	0x2d
	.long	0x7d0
	.uleb128 0xc
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0x9
	.long	.LASF123
	.byte	0x20
	.byte	0x11
	.byte	0x62
	.byte	0x8
	.long	0x81f
	.uleb128 0xa
	.long	.LASF124
	.byte	0x11
	.byte	0x64
	.byte	0x9
	.long	0xa8
	.byte	0
	.uleb128 0xa
	.long	.LASF125
	.byte	0x11
	.byte	0x65
	.byte	0xa
	.long	0x690
	.byte	0x8
	.uleb128 0xa
	.long	.LASF126
	.byte	0x11
	.byte	0x66
	.byte	0x7
	.long	0x6f
	.byte	0x10
	.uleb128 0xa
	.long	.LASF127
	.byte	0x11
	.byte	0x67
	.byte	0x7
	.long	0x6f
	.byte	0x14
	.uleb128 0xa
	.long	.LASF128
	.byte	0x11
	.byte	0x68
	.byte	0xa
	.long	0x690
	.byte	0x18
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.long	0x7d0
	.uleb128 0x19
	.byte	0x10
	.byte	0x12
	.value	0x204
	.byte	0x3
	.long	0x83d
	.uleb128 0x1a
	.long	.LASF129
	.byte	0x12
	.value	0x205
	.byte	0x13
	.long	0x83d
	.byte	0
	.uleb128 0xb
	.long	0x2d
	.long	0x84d
	.uleb128 0xc
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x1b
	.long	.LASF130
	.byte	0x10
	.byte	0x12
	.value	0x203
	.byte	0x8
	.long	0x86a
	.uleb128 0xe
	.long	.LASF131
	.byte	0x12
	.value	0x206
	.byte	0x5
	.long	0x825
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	0x84d
	.uleb128 0x10
	.long	.LASF132
	.byte	0x13
	.byte	0x52
	.byte	0x23
	.long	0x86a
	.uleb128 0x17
	.byte	0x10
	.byte	0x14
	.byte	0x82
	.byte	0x3
	.long	0x89d
	.uleb128 0x18
	.long	.LASF133
	.byte	0x14
	.byte	0x83
	.byte	0x14
	.long	0x703
	.uleb128 0x18
	.long	.LASF134
	.byte	0x14
	.byte	0x84
	.byte	0x1a
	.long	0x84d
	.byte	0
	.uleb128 0x9
	.long	.LASF135
	.byte	0x1c
	.byte	0x14
	.byte	0x80
	.byte	0x8
	.long	0x8df
	.uleb128 0xa
	.long	.LASF136
	.byte	0x14
	.byte	0x81
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0xa
	.long	.LASF137
	.byte	0x14
	.byte	0x85
	.byte	0x5
	.long	0x87b
	.byte	0x4
	.uleb128 0xa
	.long	.LASF138
	.byte	0x14
	.byte	0x86
	.byte	0x7
	.long	0x6f
	.byte	0x14
	.uleb128 0xa
	.long	.LASF139
	.byte	0x14
	.byte	0x87
	.byte	0x7
	.long	0x6f
	.byte	0x18
	.byte	0
	.uleb128 0x1c
	.long	0xa1
	.long	0x8ee
	.uleb128 0x1d
	.long	0xba
	.byte	0
	.uleb128 0x16
	.long	.LASF140
	.byte	0x14
	.value	0x151
	.byte	0x10
	.long	0x8fb
	.uleb128 0x7
	.byte	0x8
	.long	0x8df
	.uleb128 0x1c
	.long	0xa1
	.long	0x915
	.uleb128 0x1d
	.long	0xa1
	.uleb128 0x1d
	.long	0xba
	.byte	0
	.uleb128 0x16
	.long	.LASF141
	.byte	0x14
	.value	0x152
	.byte	0x10
	.long	0x922
	.uleb128 0x7
	.byte	0x8
	.long	0x901
	.uleb128 0x1e
	.long	0x933
	.uleb128 0x1d
	.long	0xa1
	.byte	0
	.uleb128 0x16
	.long	.LASF142
	.byte	0x14
	.value	0x153
	.byte	0xf
	.long	0x940
	.uleb128 0x7
	.byte	0x8
	.long	0x928
	.uleb128 0x1f
	.long	.LASF166
	.byte	0x1
	.byte	0x22
	.byte	0x5
	.long	0x6f
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0xcc3
	.uleb128 0x20
	.string	"fp"
	.byte	0x1
	.byte	0x22
	.byte	0x1d
	.long	0x53d
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x21
	.long	.LASF136
	.byte	0x1
	.byte	0x22
	.byte	0x25
	.long	0x6f
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x21
	.long	.LASF143
	.byte	0x1
	.byte	0x22
	.byte	0x3e
	.long	0xcc3
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x22
	.long	.LASF150
	.byte	0x1
	.byte	0x24
	.byte	0x9
	.long	0xa8
	.uleb128 0x3
	.byte	0x91
	.sleb128 -128
	.uleb128 0x23
	.string	"p"
	.byte	0x1
	.byte	0x24
	.byte	0x16
	.long	0xa8
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x23
	.string	"q"
	.byte	0x1
	.byte	0x24
	.byte	0x1a
	.long	0xa8
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x24
	.long	.LASF144
	.byte	0x1
	.byte	0x24
	.byte	0x1f
	.long	0x690
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x25
	.long	.LASF145
	.byte	0x1
	.byte	0x25
	.byte	0x9
	.long	0xa8
	.uleb128 0x25
	.long	.LASF146
	.byte	0x1
	.byte	0x25
	.byte	0x13
	.long	0xa8
	.uleb128 0x24
	.long	.LASF147
	.byte	0x1
	.byte	0x25
	.byte	0x1d
	.long	0xa8
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x24
	.long	.LASF148
	.byte	0x1
	.byte	0x26
	.byte	0x7
	.long	0x6f
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x24
	.long	.LASF149
	.byte	0x1
	.byte	0x27
	.byte	0xa
	.long	0xba
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x22
	.long	.LASF151
	.byte	0x1
	.byte	0x27
	.byte	0x13
	.long	0xba
	.uleb128 0x3
	.byte	0x91
	.sleb128 -120
	.uleb128 0x24
	.long	.LASF152
	.byte	0x1
	.byte	0x27
	.byte	0x1d
	.long	0xba
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x22
	.long	.LASF137
	.byte	0x1
	.byte	0x28
	.byte	0x14
	.long	0x89d
	.uleb128 0x3
	.byte	0x91
	.sleb128 -112
	.uleb128 0x24
	.long	.LASF123
	.byte	0x1
	.byte	0x29
	.byte	0x13
	.long	0x81f
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x26
	.long	0xcc9
	.quad	.LBI6
	.value	.LVU155
	.quad	.LBB6
	.quad	.LBE6-.LBB6
	.byte	0x1
	.byte	0xbe
	.byte	0x9
	.long	0xad5
	.uleb128 0x27
	.long	0xcf2
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x27
	.long	0xce6
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x27
	.long	0xcda
	.long	.LLST13
	.long	.LVUS13
	.byte	0
	.uleb128 0x26
	.long	0xcc9
	.quad	.LBI8
	.value	.LVU272
	.quad	.LBB8
	.quad	.LBE8-.LBB8
	.byte	0x1
	.byte	0xbc
	.byte	0x9
	.long	0xb23
	.uleb128 0x27
	.long	0xcf2
	.long	.LLST14
	.long	.LVUS14
	.uleb128 0x27
	.long	0xce6
	.long	.LLST15
	.long	.LVUS15
	.uleb128 0x27
	.long	0xcda
	.long	.LLST16
	.long	.LVUS16
	.byte	0
	.uleb128 0x28
	.quad	.LVL7
	.long	0xcff
	.long	0xb4b
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 0
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x4
	.byte	0x91
	.sleb128 -136
	.byte	0x6
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x4
	.byte	0x91
	.sleb128 -144
	.byte	0x6
	.byte	0
	.uleb128 0x2a
	.quad	.LVL13
	.long	0xd0c
	.uleb128 0x2a
	.quad	.LVL20
	.long	0xd0c
	.uleb128 0x28
	.quad	.LVL41
	.long	0xd18
	.long	0xb89
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x3a
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x3
	.byte	0x91
	.sleb128 -108
	.byte	0
	.uleb128 0x2b
	.quad	.LVL44
	.long	0xb9d
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8
	.byte	0x20
	.byte	0
	.uleb128 0x28
	.quad	.LVL46
	.long	0xd25
	.long	0xbb7
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x4
	.byte	0x91
	.sleb128 -136
	.byte	0x6
	.byte	0
	.uleb128 0x2b
	.quad	.LVL47
	.long	0xbca
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x40
	.byte	0
	.uleb128 0x2b
	.quad	.LVL49
	.long	0xbe0
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x4
	.byte	0x91
	.sleb128 -176
	.byte	0x6
	.byte	0
	.uleb128 0x2b
	.quad	.LVL53
	.long	0xbf4
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.uleb128 0x28
	.quad	.LVL55
	.long	0xd31
	.long	0xc17
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x73
	.sleb128 8
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7d
	.sleb128 -8
	.byte	0
	.uleb128 0x2a
	.quad	.LVL63
	.long	0xd25
	.uleb128 0x2b
	.quad	.LVL76
	.long	0xc38
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.uleb128 0x2b
	.quad	.LVL79
	.long	0xc4c
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x28
	.quad	.LVL82
	.long	0xd18
	.long	0xc71
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x32
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x4
	.byte	0x91
	.sleb128 -192
	.byte	0x6
	.byte	0
	.uleb128 0x28
	.quad	.LVL85
	.long	0xd18
	.long	0xc8e
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x3a
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x7c
	.sleb128 0
	.byte	0
	.uleb128 0x2a
	.quad	.LVL92
	.long	0xd3c
	.uleb128 0x28
	.quad	.LVL93
	.long	0xd48
	.long	0xcb5
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x4
	.byte	0x91
	.sleb128 -176
	.byte	0x6
	.byte	0
	.uleb128 0x2a
	.quad	.LVL108
	.long	0xd54
	.byte	0
	.uleb128 0x7
	.byte	0x8
	.long	0x81f
	.uleb128 0x2c
	.long	.LASF167
	.byte	0x2
	.byte	0x1f
	.byte	0x2a
	.long	0xa1
	.byte	0x3
	.long	0xcff
	.uleb128 0x2d
	.long	.LASF153
	.byte	0x2
	.byte	0x1f
	.byte	0x43
	.long	0xa3
	.uleb128 0x2d
	.long	.LASF154
	.byte	0x2
	.byte	0x1f
	.byte	0x62
	.long	0x5a1
	.uleb128 0x2d
	.long	.LASF155
	.byte	0x2
	.byte	0x1f
	.byte	0x70
	.long	0xba
	.byte	0
	.uleb128 0x2e
	.long	.LASF156
	.long	.LASF156
	.byte	0x14
	.value	0x15d
	.byte	0x5
	.uleb128 0x2f
	.long	.LASF157
	.long	.LASF157
	.byte	0x15
	.byte	0x4f
	.byte	0x23
	.uleb128 0x2e
	.long	.LASF158
	.long	.LASF158
	.byte	0x12
	.value	0x2d2
	.byte	0x6
	.uleb128 0x2f
	.long	.LASF159
	.long	.LASF159
	.byte	0x16
	.byte	0x16
	.byte	0xe
	.uleb128 0x30
	.long	.LASF168
	.long	.LASF169
	.byte	0x18
	.byte	0
	.uleb128 0x2f
	.long	.LASF160
	.long	.LASF160
	.byte	0x17
	.byte	0x18
	.byte	0x7
	.uleb128 0x2f
	.long	.LASF161
	.long	.LASF161
	.byte	0x17
	.byte	0x16
	.byte	0x7
	.uleb128 0x31
	.long	.LASF170
	.long	.LASF170
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x26
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0x5
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2e
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2f
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x30
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x31
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU17
	.uleb128 .LVU17
	.uleb128 .LVU143
	.uleb128 .LVU143
	.uleb128 .LVU202
	.uleb128 .LVU202
	.uleb128 .LVU207
	.uleb128 .LVU207
	.uleb128 .LVU208
	.uleb128 .LVU208
	.uleb128 .LVU211
	.uleb128 .LVU211
	.uleb128 .LVU242
	.uleb128 .LVU242
	.uleb128 .LVU268
	.uleb128 .LVU268
	.uleb128 .LVU290
	.uleb128 .LVU290
	.uleb128 .LVU322
	.uleb128 .LVU322
	.uleb128 .LVU325
	.uleb128 .LVU325
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL3-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL6-.Ltext0
	.quad	.LVL48-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL48-.Ltext0
	.quad	.LVL65-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL65-.Ltext0
	.quad	.LVL67-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL67-.Ltext0
	.quad	.LVL68-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL68-.Ltext0
	.quad	.LVL69-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL69-.Ltext0
	.quad	.LVL81-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL81-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL89-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL95-.Ltext0
	.quad	.LVL105-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL105-.Ltext0
	.quad	.LVL108-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU3
	.uleb128 .LVU3
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU162
	.uleb128 .LVU162
	.uleb128 .LVU202
	.uleb128 .LVU202
	.uleb128 .LVU211
	.uleb128 .LVU211
	.uleb128 .LVU242
	.uleb128 .LVU242
	.uleb128 .LVU268
	.uleb128 .LVU268
	.uleb128 .LVU271
	.uleb128 .LVU271
	.uleb128 .LVU275
	.uleb128 .LVU275
	.uleb128 .LVU290
	.uleb128 .LVU290
	.uleb128 .LVU322
	.uleb128 .LVU322
	.uleb128 .LVU325
	.uleb128 .LVU325
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL1-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL3-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL5-.Ltext0
	.quad	.LVL52-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL52-.Ltext0
	.quad	.LVL65-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL65-.Ltext0
	.quad	.LVL69-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL69-.Ltext0
	.quad	.LVL81-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL81-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL89-.Ltext0
	.quad	.LVL90-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL90-.Ltext0
	.quad	.LVL91-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL91-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL95-.Ltext0
	.quad	.LVL105-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL105-.Ltext0
	.quad	.LVL108-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU14
	.uleb128 .LVU14
	.uleb128 .LVU15
	.uleb128 .LVU15
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU17
	.uleb128 .LVU17
	.uleb128 .LVU207
	.uleb128 .LVU207
	.uleb128 .LVU208
	.uleb128 .LVU208
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL3-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x3
	.byte	0x76
	.sleb128 -136
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -152
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL6-.Ltext0
	.quad	.LVL67-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -152
	.quad	.LVL67-.Ltext0
	.quad	.LVL68-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL68-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -152
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU22
	.uleb128 .LVU27
	.uleb128 .LVU27
	.uleb128 .LVU29
	.uleb128 .LVU29
	.uleb128 .LVU38
	.uleb128 .LVU48
	.uleb128 .LVU63
	.uleb128 .LVU63
	.uleb128 .LVU64
	.uleb128 .LVU64
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU71
	.uleb128 .LVU71
	.uleb128 .LVU74
	.uleb128 .LVU74
	.uleb128 .LVU82
	.uleb128 .LVU82
	.uleb128 .LVU83
	.uleb128 .LVU83
	.uleb128 .LVU84
	.uleb128 .LVU84
	.uleb128 .LVU85
	.uleb128 .LVU85
	.uleb128 .LVU99
	.uleb128 .LVU99
	.uleb128 .LVU100
	.uleb128 .LVU174
	.uleb128 .LVU178
	.uleb128 .LVU178
	.uleb128 .LVU195
	.uleb128 .LVU290
	.uleb128 .LVU296
	.uleb128 .LVU296
	.uleb128 .LVU302
	.uleb128 .LVU302
	.uleb128 .LVU305
	.uleb128 .LVU305
	.uleb128 .LVU306
	.uleb128 .LVU307
	.uleb128 .LVU318
	.uleb128 .LVU322
	.uleb128 .LVU323
	.uleb128 .LVU323
	.uleb128 .LVU324
	.uleb128 .LVU325
	.uleb128 0
.LLST3:
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL10-.Ltext0
	.quad	.LVL11-.Ltext0
	.value	0x3
	.byte	0x7e
	.sleb128 1
	.byte	0x9f
	.quad	.LVL11-.Ltext0
	.quad	.LVL13-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL18-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL22-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL22-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 1
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL24-.Ltext0
	.quad	.LVL25-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL25-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 1
	.byte	0x9f
	.quad	.LVL26-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL29-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL29-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 1
	.byte	0x9f
	.quad	.LVL30-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	.LVL31-.Ltext0
	.quad	.LVL36-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL36-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL57-.Ltext0
	.quad	.LVL58-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL58-.Ltext0
	.quad	.LVL63-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL95-.Ltext0
	.quad	.LVL96-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL96-.Ltext0
	.quad	.LVL97-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	.LVL97-.Ltext0
	.quad	.LVL98-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL98-.Ltext0
	.quad	.LVL99-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL100-.Ltext0
	.quad	.LVL103-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL105-.Ltext0
	.quad	.LVL106-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL106-.Ltext0
	.quad	.LVL107-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU33
	.uleb128 .LVU38
	.uleb128 .LVU39
	.uleb128 .LVU44
	.uleb128 .LVU45
	.uleb128 .LVU51
	.uleb128 .LVU86
	.uleb128 .LVU91
	.uleb128 .LVU183
	.uleb128 .LVU186
	.uleb128 .LVU186
	.uleb128 .LVU202
	.uleb128 .LVU290
	.uleb128 .LVU322
	.uleb128 .LVU323
	.uleb128 .LVU324
	.uleb128 .LVU325
	.uleb128 0
.LLST4:
	.quad	.LVL12-.Ltext0
	.quad	.LVL13-1-.Ltext0
	.value	0x3
	.byte	0x70
	.sleb128 -1
	.byte	0x9f
	.quad	.LVL14-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL17-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x3
	.byte	0x7e
	.sleb128 1
	.byte	0x9f
	.quad	.LVL32-.Ltext0
	.quad	.LVL33-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	.LVL60-.Ltext0
	.quad	.LVL61-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL61-.Ltext0
	.quad	.LVL65-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL95-.Ltext0
	.quad	.LVL105-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	.LVL106-.Ltext0
	.quad	.LVL107-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU173
	.uleb128 .LVU202
	.uleb128 .LVU222
	.uleb128 .LVU230
	.uleb128 .LVU322
	.uleb128 .LVU324
.LLST5:
	.quad	.LVL56-.Ltext0
	.quad	.LVL65-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL72-.Ltext0
	.quad	.LVL75-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL105-.Ltext0
	.quad	.LVL107-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU93
	.uleb128 .LVU99
	.uleb128 .LVU101
	.uleb128 .LVU107
	.uleb128 .LVU173
	.uleb128 .LVU195
	.uleb128 .LVU292
	.uleb128 .LVU322
	.uleb128 .LVU322
	.uleb128 .LVU324
	.uleb128 .LVU325
	.uleb128 0
.LLST6:
	.quad	.LVL34-.Ltext0
	.quad	.LVL36-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL37-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL56-.Ltext0
	.quad	.LVL63-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL95-.Ltext0
	.quad	.LVL105-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	.LVL105-.Ltext0
	.quad	.LVL107-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU19
	.uleb128 .LVU22
	.uleb128 .LVU22
	.uleb128 .LVU202
	.uleb128 .LVU202
	.uleb128 .LVU206
	.uleb128 .LVU206
	.uleb128 .LVU207
	.uleb128 .LVU208
	.uleb128 .LVU241
	.uleb128 .LVU242
	.uleb128 .LVU324
	.uleb128 .LVU325
	.uleb128 0
.LLST7:
	.quad	.LVL8-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL9-.Ltext0
	.quad	.LVL65-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL65-.Ltext0
	.quad	.LVL66-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL66-1-.Ltext0
	.quad	.LVL67-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL68-.Ltext0
	.quad	.LVL80-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL81-.Ltext0
	.quad	.LVL107-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU109
	.uleb128 .LVU120
	.uleb128 .LVU120
	.uleb128 .LVU122
	.uleb128 .LVU242
	.uleb128 .LVU249
	.uleb128 .LVU249
	.uleb128 .LVU251
	.uleb128 .LVU251
	.uleb128 .LVU260
	.uleb128 .LVU260
	.uleb128 .LVU262
.LLST8:
	.quad	.LVL40-.Ltext0
	.quad	.LVL42-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL42-.Ltext0
	.quad	.LVL43-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -184
	.quad	.LVL81-.Ltext0
	.quad	.LVL83-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL83-.Ltext0
	.quad	.LVL84-.Ltext0
	.value	0x2
	.byte	0x34
	.byte	0x9f
	.quad	.LVL84-.Ltext0
	.quad	.LVL86-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL86-.Ltext0
	.quad	.LVL87-.Ltext0
	.value	0x2
	.byte	0x40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU96
	.uleb128 .LVU99
	.uleb128 .LVU105
	.uleb128 .LVU108
	.uleb128 .LVU108
	.uleb128 .LVU167
	.uleb128 .LVU167
	.uleb128 .LVU169
	.uleb128 .LVU208
	.uleb128 .LVU211
	.uleb128 .LVU242
	.uleb128 .LVU268
	.uleb128 .LVU271
	.uleb128 .LVU275
	.uleb128 .LVU295
	.uleb128 .LVU302
	.uleb128 .LVU317
	.uleb128 .LVU318
	.uleb128 .LVU320
	.uleb128 .LVU322
	.uleb128 .LVU325
	.uleb128 0
.LLST9:
	.quad	.LVL35-.Ltext0
	.quad	.LVL36-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL38-.Ltext0
	.quad	.LVL40-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL40-.Ltext0
	.quad	.LVL54-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -168
	.quad	.LVL54-.Ltext0
	.quad	.LVL55-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL68-.Ltext0
	.quad	.LVL69-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -168
	.quad	.LVL81-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -168
	.quad	.LVL90-.Ltext0
	.quad	.LVL91-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -168
	.quad	.LVL96-.Ltext0
	.quad	.LVL97-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL102-.Ltext0
	.quad	.LVL103-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL104-.Ltext0
	.quad	.LVL105-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU10
	.uleb128 .LVU14
	.uleb128 .LVU16
	.uleb128 .LVU126
	.uleb128 .LVU126
	.uleb128 .LVU135
	.uleb128 .LVU135
	.uleb128 .LVU202
	.uleb128 .LVU202
	.uleb128 .LVU208
	.uleb128 .LVU208
	.uleb128 .LVU240
	.uleb128 .LVU242
	.uleb128 .LVU268
	.uleb128 .LVU268
	.uleb128 .LVU290
	.uleb128 .LVU290
	.uleb128 .LVU322
	.uleb128 .LVU322
	.uleb128 .LVU324
	.uleb128 .LVU325
	.uleb128 0
.LLST10:
	.quad	.LVL2-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL5-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL45-.Ltext0
	.quad	.LVL46-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL46-1-.Ltext0
	.quad	.LVL65-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL65-.Ltext0
	.quad	.LVL68-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL68-.Ltext0
	.quad	.LVL79-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL81-.Ltext0
	.quad	.LVL89-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL89-.Ltext0
	.quad	.LVL95-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL95-.Ltext0
	.quad	.LVL105-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL105-.Ltext0
	.quad	.LVL107-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL108-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 .LVU155
	.uleb128 .LVU158
.LLST11:
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x2
	.byte	0x40
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 .LVU155
	.uleb128 .LVU158
.LLST12:
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x4
	.byte	0x91
	.sleb128 -108
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS13:
	.uleb128 .LVU155
	.uleb128 .LVU158
.LLST13:
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS14:
	.uleb128 .LVU272
	.uleb128 .LVU275
.LLST14:
	.quad	.LVL90-.Ltext0
	.quad	.LVL91-.Ltext0
	.value	0x2
	.byte	0x34
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS15:
	.uleb128 .LVU272
	.uleb128 .LVU275
.LLST15:
	.quad	.LVL90-.Ltext0
	.quad	.LVL91-.Ltext0
	.value	0x4
	.byte	0x91
	.sleb128 -108
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS16:
	.uleb128 .LVU272
	.uleb128 .LVU275
.LLST16:
	.quad	.LVL90-.Ltext0
	.quad	.LVL91-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF89:
	.string	"_ISgraph"
.LASF55:
	.string	"_chain"
.LASF33:
	.string	"sin6_addr"
.LASF120:
	.string	"__in6_u"
.LASF13:
	.string	"size_t"
.LASF61:
	.string	"_shortbuf"
.LASF130:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF87:
	.string	"_ISspace"
.LASF49:
	.string	"_IO_buf_base"
.LASF169:
	.string	"__builtin_memset"
.LASF14:
	.string	"long long unsigned int"
.LASF112:
	.string	"in_addr_t"
.LASF137:
	.string	"addr"
.LASF154:
	.string	"__src"
.LASF84:
	.string	"_ISalpha"
.LASF64:
	.string	"_codecvt"
.LASF85:
	.string	"_ISdigit"
.LASF98:
	.string	"__timezone"
.LASF158:
	.string	"ares_inet_pton"
.LASF15:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF35:
	.string	"sockaddr_inarp"
.LASF135:
	.string	"ares_addr"
.LASF56:
	.string	"_fileno"
.LASF44:
	.string	"_IO_read_end"
.LASF142:
	.string	"ares_free"
.LASF117:
	.string	"__u6_addr16"
.LASF160:
	.string	"aresx_sitoss"
.LASF9:
	.string	"long int"
.LASF163:
	.string	"../deps/cares/src/ares__get_hostent.c"
.LASF147:
	.string	"txtalias"
.LASF42:
	.string	"_flags"
.LASF65:
	.string	"_wide_data"
.LASF93:
	.string	"_ISalnum"
.LASF59:
	.string	"_cur_column"
.LASF95:
	.string	"program_invocation_short_name"
.LASF73:
	.string	"_IO_codecvt"
.LASF21:
	.string	"sockaddr_dl"
.LASF31:
	.string	"sin6_port"
.LASF110:
	.string	"uint16_t"
.LASF81:
	.string	"_sys_errlist"
.LASF94:
	.string	"program_invocation_name"
.LASF58:
	.string	"_old_offset"
.LASF63:
	.string	"_offset"
.LASF122:
	.string	"in6addr_loopback"
.LASF40:
	.string	"sockaddr_x25"
.LASF20:
	.string	"sockaddr_ax25"
.LASF36:
	.string	"sockaddr_ipx"
.LASF150:
	.string	"line"
.LASF8:
	.string	"__uint32_t"
.LASF145:
	.string	"txtaddr"
.LASF101:
	.string	"timezone"
.LASF28:
	.string	"sin_zero"
.LASF68:
	.string	"__pad5"
.LASF92:
	.string	"_ISpunct"
.LASF72:
	.string	"_IO_marker"
.LASF75:
	.string	"stdin"
.LASF2:
	.string	"unsigned int"
.LASF114:
	.string	"s_addr"
.LASF104:
	.string	"environ"
.LASF124:
	.string	"h_name"
.LASF138:
	.string	"udp_port"
.LASF47:
	.string	"_IO_write_ptr"
.LASF140:
	.string	"ares_malloc"
.LASF100:
	.string	"daylight"
.LASF78:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF27:
	.string	"sin_addr"
.LASF131:
	.string	"_S6_un"
.LASF149:
	.string	"addrlen"
.LASF164:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF51:
	.string	"_IO_save_base"
.LASF126:
	.string	"h_addrtype"
.LASF161:
	.string	"aresx_uztoss"
.LASF133:
	.string	"addr4"
.LASF134:
	.string	"addr6"
.LASF62:
	.string	"_lock"
.LASF57:
	.string	"_flags2"
.LASF69:
	.string	"_mode"
.LASF76:
	.string	"stdout"
.LASF39:
	.string	"sockaddr_un"
.LASF25:
	.string	"sin_family"
.LASF105:
	.string	"optarg"
.LASF146:
	.string	"txthost"
.LASF102:
	.string	"getdate_err"
.LASF115:
	.string	"in_port_t"
.LASF30:
	.string	"sin6_family"
.LASF106:
	.string	"optind"
.LASF32:
	.string	"sin6_flowinfo"
.LASF144:
	.string	"alias"
.LASF43:
	.string	"_IO_read_ptr"
.LASF125:
	.string	"h_aliases"
.LASF153:
	.string	"__dest"
.LASF165:
	.string	"_IO_lock_t"
.LASF3:
	.string	"long unsigned int"
.LASF41:
	.string	"_IO_FILE"
.LASF103:
	.string	"__environ"
.LASF77:
	.string	"stderr"
.LASF38:
	.string	"sockaddr_ns"
.LASF26:
	.string	"sin_port"
.LASF17:
	.string	"sa_family"
.LASF79:
	.string	"sys_errlist"
.LASF127:
	.string	"h_length"
.LASF128:
	.string	"h_addr_list"
.LASF34:
	.string	"sin6_scope_id"
.LASF136:
	.string	"family"
.LASF0:
	.string	"unsigned char"
.LASF37:
	.string	"sockaddr_iso"
.LASF159:
	.string	"ares_strdup"
.LASF121:
	.string	"in6addr_any"
.LASF50:
	.string	"_IO_buf_end"
.LASF5:
	.string	"short int"
.LASF155:
	.string	"__len"
.LASF80:
	.string	"_sys_nerr"
.LASF60:
	.string	"_vtable_offset"
.LASF99:
	.string	"tzname"
.LASF90:
	.string	"_ISblank"
.LASF71:
	.string	"FILE"
.LASF54:
	.string	"_markers"
.LASF170:
	.string	"__stack_chk_fail"
.LASF166:
	.string	"ares__get_hostent"
.LASF119:
	.string	"in6_addr"
.LASF97:
	.string	"__daylight"
.LASF108:
	.string	"optopt"
.LASF111:
	.string	"uint32_t"
.LASF139:
	.string	"tcp_port"
.LASF12:
	.string	"char"
.LASF157:
	.string	"__ctype_b_loc"
.LASF91:
	.string	"_IScntrl"
.LASF123:
	.string	"hostent"
.LASF7:
	.string	"__uint16_t"
.LASF86:
	.string	"_ISxdigit"
.LASF143:
	.string	"host"
.LASF156:
	.string	"ares__read_line"
.LASF116:
	.string	"__u6_addr8"
.LASF83:
	.string	"_ISlower"
.LASF107:
	.string	"opterr"
.LASF11:
	.string	"__off64_t"
.LASF45:
	.string	"_IO_read_base"
.LASF167:
	.string	"memcpy"
.LASF53:
	.string	"_IO_save_end"
.LASF22:
	.string	"sockaddr_eon"
.LASF19:
	.string	"sockaddr_at"
.LASF141:
	.string	"ares_realloc"
.LASF48:
	.string	"_IO_write_end"
.LASF16:
	.string	"sa_family_t"
.LASF70:
	.string	"_unused2"
.LASF129:
	.string	"_S6_u8"
.LASF168:
	.string	"memset"
.LASF29:
	.string	"sockaddr_in6"
.LASF148:
	.string	"status"
.LASF23:
	.string	"sockaddr"
.LASF24:
	.string	"sockaddr_in"
.LASF82:
	.string	"_ISupper"
.LASF67:
	.string	"_freeres_buf"
.LASF109:
	.string	"uint8_t"
.LASF52:
	.string	"_IO_backup_base"
.LASF18:
	.string	"sa_data"
.LASF66:
	.string	"_freeres_list"
.LASF152:
	.string	"naliases"
.LASF162:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF118:
	.string	"__u6_addr32"
.LASF74:
	.string	"_IO_wide_data"
.LASF151:
	.string	"linesize"
.LASF132:
	.string	"ares_in6addr_any"
.LASF96:
	.string	"__tzname"
.LASF46:
	.string	"_IO_write_base"
.LASF88:
	.string	"_ISprint"
.LASF113:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
