	.file	"ares_llist.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares__init_list_head
	.type	ares__init_list_head, @function
ares__init_list_head:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares_llist.c"
	.loc 1 27 51 view -0
	.cfi_startproc
	.loc 1 27 51 is_stmt 0 view .LVU1
	endbr64
	.loc 1 28 3 is_stmt 1 view .LVU2
	.loc 1 29 3 view .LVU3
	.loc 1 28 14 is_stmt 0 view .LVU4
	movq	%rdi, %xmm0
	.loc 1 30 14 view .LVU5
	movq	$0, 16(%rdi)
	.loc 1 28 14 view .LVU6
	punpcklqdq	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	.loc 1 30 3 is_stmt 1 view .LVU7
	.loc 1 31 1 is_stmt 0 view .LVU8
	ret
	.cfi_endproc
.LFE87:
	.size	ares__init_list_head, .-ares__init_list_head
	.p2align 4
	.globl	ares__init_list_node
	.type	ares__init_list_node, @function
ares__init_list_node:
.LVL1:
.LFB88:
	.loc 1 34 60 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 34 60 is_stmt 0 view .LVU10
	endbr64
	.loc 1 35 3 is_stmt 1 view .LVU11
	.loc 1 36 3 view .LVU12
	.loc 1 35 14 is_stmt 0 view .LVU13
	pxor	%xmm0, %xmm0
	.loc 1 37 14 view .LVU14
	movq	%rsi, 16(%rdi)
	.loc 1 35 14 view .LVU15
	movups	%xmm0, (%rdi)
	.loc 1 37 3 is_stmt 1 view .LVU16
	.loc 1 38 1 is_stmt 0 view .LVU17
	ret
	.cfi_endproc
.LFE88:
	.size	ares__init_list_node, .-ares__init_list_node
	.p2align 4
	.globl	ares__is_list_empty
	.type	ares__is_list_empty, @function
ares__is_list_empty:
.LVL2:
.LFB89:
	.loc 1 41 49 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 41 49 is_stmt 0 view .LVU19
	endbr64
	.loc 1 42 3 is_stmt 1 view .LVU20
	.loc 1 42 32 is_stmt 0 view .LVU21
	xorl	%eax, %eax
	cmpq	%rdi, 8(%rdi)
	je	.L7
	.loc 1 43 1 view .LVU22
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.loc 1 42 32 discriminator 1 view .LVU23
	xorl	%eax, %eax
	cmpq	%rdi, (%rdi)
	sete	%al
	.loc 1 43 1 discriminator 1 view .LVU24
	ret
	.cfi_endproc
.LFE89:
	.size	ares__is_list_empty, .-ares__is_list_empty
	.p2align 4
	.globl	ares__insert_in_list
	.type	ares__insert_in_list, @function
ares__insert_in_list:
.LVL3:
.LFB90:
	.loc 1 47 55 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 47 55 is_stmt 0 view .LVU26
	endbr64
	.loc 1 48 3 is_stmt 1 view .LVU27
	.loc 1 49 28 is_stmt 0 view .LVU28
	movq	(%rsi), %rax
	.loc 1 48 18 view .LVU29
	movq	%rsi, 8(%rdi)
	.loc 1 49 3 is_stmt 1 view .LVU30
	.loc 1 49 18 is_stmt 0 view .LVU31
	movq	%rax, (%rdi)
	.loc 1 50 3 is_stmt 1 view .LVU32
	.loc 1 50 24 is_stmt 0 view .LVU33
	movq	(%rsi), %rax
	movq	%rdi, 8(%rax)
	.loc 1 51 3 is_stmt 1 view .LVU34
	.loc 1 51 18 is_stmt 0 view .LVU35
	movq	%rdi, (%rsi)
	.loc 1 52 1 view .LVU36
	ret
	.cfi_endproc
.LFE90:
	.size	ares__insert_in_list, .-ares__insert_in_list
	.p2align 4
	.globl	ares__remove_from_list
	.type	ares__remove_from_list, @function
ares__remove_from_list:
.LVL4:
.LFB91:
	.loc 1 55 53 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 55 53 is_stmt 0 view .LVU38
	endbr64
	.loc 1 56 3 is_stmt 1 view .LVU39
	.loc 1 56 11 is_stmt 0 view .LVU40
	movq	8(%rdi), %rax
	.loc 1 56 6 view .LVU41
	testq	%rax, %rax
	je	.L9
	.loc 1 57 5 is_stmt 1 view .LVU42
	.loc 1 57 9 is_stmt 0 view .LVU43
	movq	(%rdi), %rdx
	.loc 1 59 16 view .LVU44
	pxor	%xmm0, %xmm0
	.loc 1 57 22 view .LVU45
	movq	%rax, 8(%rdx)
	.loc 1 58 5 is_stmt 1 view .LVU46
	.loc 1 58 22 is_stmt 0 view .LVU47
	movq	%rdx, (%rax)
	.loc 1 59 5 is_stmt 1 view .LVU48
	.loc 1 60 5 view .LVU49
	.loc 1 59 16 is_stmt 0 view .LVU50
	movups	%xmm0, (%rdi)
.L9:
	.loc 1 62 1 view .LVU51
	ret
	.cfi_endproc
.LFE91:
	.size	ares__remove_from_list, .-ares__remove_from_list
.Letext0:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 6 "/usr/include/netinet/in.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 9 "/usr/include/stdio.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 11 "/usr/include/errno.h"
	.file 12 "/usr/include/time.h"
	.file 13 "/usr/include/unistd.h"
	.file 14 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 16 "../deps/cares/include/ares.h"
	.file 17 "../deps/cares/src/ares_ipv6.h"
	.file 18 "../deps/cares/src/ares_llist.h"
	.file 19 "../deps/cares/src/ares_private.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x950
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF129
	.byte	0x1
	.long	.LASF130
	.long	.LASF131
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x2
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x2
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x2
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x2
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x2
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0xa9
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x7
	.long	0xa9
	.uleb128 0x3
	.long	.LASF13
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF14
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF15
	.uleb128 0x3
	.long	.LASF16
	.byte	0x4
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF23
	.byte	0x10
	.byte	0x5
	.byte	0xb2
	.byte	0x8
	.long	0x103
	.uleb128 0x9
	.long	.LASF17
	.byte	0x5
	.byte	0xb4
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF18
	.byte	0x5
	.byte	0xb5
	.byte	0xa
	.long	0x108
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0xdb
	.uleb128 0xa
	.long	0xa9
	.long	0x118
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xdb
	.uleb128 0xc
	.long	0x118
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x7
	.long	0x123
	.uleb128 0x6
	.byte	0x8
	.long	0x123
	.uleb128 0xc
	.long	0x12d
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x7
	.long	0x138
	.uleb128 0x6
	.byte	0x8
	.long	0x138
	.uleb128 0xc
	.long	0x142
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x7
	.long	0x14d
	.uleb128 0x6
	.byte	0x8
	.long	0x14d
	.uleb128 0xc
	.long	0x157
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x7
	.long	0x162
	.uleb128 0x6
	.byte	0x8
	.long	0x162
	.uleb128 0xc
	.long	0x16c
	.uleb128 0x8
	.long	.LASF24
	.byte	0x10
	.byte	0x6
	.byte	0xee
	.byte	0x8
	.long	0x1b9
	.uleb128 0x9
	.long	.LASF25
	.byte	0x6
	.byte	0xf0
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF26
	.byte	0x6
	.byte	0xf1
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0x9
	.long	.LASF27
	.byte	0x6
	.byte	0xf2
	.byte	0x14
	.long	0x693
	.byte	0x4
	.uleb128 0x9
	.long	.LASF28
	.byte	0x6
	.byte	0xf5
	.byte	0x13
	.long	0x750
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	0x177
	.uleb128 0x6
	.byte	0x8
	.long	0x177
	.uleb128 0xc
	.long	0x1be
	.uleb128 0x8
	.long	.LASF29
	.byte	0x1c
	.byte	0x6
	.byte	0xfd
	.byte	0x8
	.long	0x21c
	.uleb128 0x9
	.long	.LASF30
	.byte	0x6
	.byte	0xff
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0xe
	.long	.LASF31
	.byte	0x6
	.value	0x100
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0xe
	.long	.LASF32
	.byte	0x6
	.value	0x101
	.byte	0xe
	.long	0x67b
	.byte	0x4
	.uleb128 0xe
	.long	.LASF33
	.byte	0x6
	.value	0x102
	.byte	0x15
	.long	0x718
	.byte	0x8
	.uleb128 0xe
	.long	.LASF34
	.byte	0x6
	.value	0x103
	.byte	0xe
	.long	0x67b
	.byte	0x18
	.byte	0
	.uleb128 0x7
	.long	0x1c9
	.uleb128 0x6
	.byte	0x8
	.long	0x1c9
	.uleb128 0xc
	.long	0x221
	.uleb128 0xd
	.long	.LASF35
	.uleb128 0x7
	.long	0x22c
	.uleb128 0x6
	.byte	0x8
	.long	0x22c
	.uleb128 0xc
	.long	0x236
	.uleb128 0xd
	.long	.LASF36
	.uleb128 0x7
	.long	0x241
	.uleb128 0x6
	.byte	0x8
	.long	0x241
	.uleb128 0xc
	.long	0x24b
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x7
	.long	0x256
	.uleb128 0x6
	.byte	0x8
	.long	0x256
	.uleb128 0xc
	.long	0x260
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x7
	.long	0x26b
	.uleb128 0x6
	.byte	0x8
	.long	0x26b
	.uleb128 0xc
	.long	0x275
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x7
	.long	0x280
	.uleb128 0x6
	.byte	0x8
	.long	0x280
	.uleb128 0xc
	.long	0x28a
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x7
	.long	0x295
	.uleb128 0x6
	.byte	0x8
	.long	0x295
	.uleb128 0xc
	.long	0x29f
	.uleb128 0x6
	.byte	0x8
	.long	0x103
	.uleb128 0xc
	.long	0x2aa
	.uleb128 0x6
	.byte	0x8
	.long	0x128
	.uleb128 0xc
	.long	0x2b5
	.uleb128 0x6
	.byte	0x8
	.long	0x13d
	.uleb128 0xc
	.long	0x2c0
	.uleb128 0x6
	.byte	0x8
	.long	0x152
	.uleb128 0xc
	.long	0x2cb
	.uleb128 0x6
	.byte	0x8
	.long	0x167
	.uleb128 0xc
	.long	0x2d6
	.uleb128 0x6
	.byte	0x8
	.long	0x1b9
	.uleb128 0xc
	.long	0x2e1
	.uleb128 0x6
	.byte	0x8
	.long	0x21c
	.uleb128 0xc
	.long	0x2ec
	.uleb128 0x6
	.byte	0x8
	.long	0x231
	.uleb128 0xc
	.long	0x2f7
	.uleb128 0x6
	.byte	0x8
	.long	0x246
	.uleb128 0xc
	.long	0x302
	.uleb128 0x6
	.byte	0x8
	.long	0x25b
	.uleb128 0xc
	.long	0x30d
	.uleb128 0x6
	.byte	0x8
	.long	0x270
	.uleb128 0xc
	.long	0x318
	.uleb128 0x6
	.byte	0x8
	.long	0x285
	.uleb128 0xc
	.long	0x323
	.uleb128 0x6
	.byte	0x8
	.long	0x29a
	.uleb128 0xc
	.long	0x32e
	.uleb128 0xa
	.long	0xa9
	.long	0x349
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF41
	.byte	0xd8
	.byte	0x7
	.byte	0x31
	.byte	0x8
	.long	0x4d0
	.uleb128 0x9
	.long	.LASF42
	.byte	0x7
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0x9
	.long	.LASF43
	.byte	0x7
	.byte	0x36
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0x9
	.long	.LASF44
	.byte	0x7
	.byte	0x37
	.byte	0x9
	.long	0xa3
	.byte	0x10
	.uleb128 0x9
	.long	.LASF45
	.byte	0x7
	.byte	0x38
	.byte	0x9
	.long	0xa3
	.byte	0x18
	.uleb128 0x9
	.long	.LASF46
	.byte	0x7
	.byte	0x39
	.byte	0x9
	.long	0xa3
	.byte	0x20
	.uleb128 0x9
	.long	.LASF47
	.byte	0x7
	.byte	0x3a
	.byte	0x9
	.long	0xa3
	.byte	0x28
	.uleb128 0x9
	.long	.LASF48
	.byte	0x7
	.byte	0x3b
	.byte	0x9
	.long	0xa3
	.byte	0x30
	.uleb128 0x9
	.long	.LASF49
	.byte	0x7
	.byte	0x3c
	.byte	0x9
	.long	0xa3
	.byte	0x38
	.uleb128 0x9
	.long	.LASF50
	.byte	0x7
	.byte	0x3d
	.byte	0x9
	.long	0xa3
	.byte	0x40
	.uleb128 0x9
	.long	.LASF51
	.byte	0x7
	.byte	0x40
	.byte	0x9
	.long	0xa3
	.byte	0x48
	.uleb128 0x9
	.long	.LASF52
	.byte	0x7
	.byte	0x41
	.byte	0x9
	.long	0xa3
	.byte	0x50
	.uleb128 0x9
	.long	.LASF53
	.byte	0x7
	.byte	0x42
	.byte	0x9
	.long	0xa3
	.byte	0x58
	.uleb128 0x9
	.long	.LASF54
	.byte	0x7
	.byte	0x44
	.byte	0x16
	.long	0x4e9
	.byte	0x60
	.uleb128 0x9
	.long	.LASF55
	.byte	0x7
	.byte	0x46
	.byte	0x14
	.long	0x4ef
	.byte	0x68
	.uleb128 0x9
	.long	.LASF56
	.byte	0x7
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0x9
	.long	.LASF57
	.byte	0x7
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0x9
	.long	.LASF58
	.byte	0x7
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0x9
	.long	.LASF59
	.byte	0x7
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF60
	.byte	0x7
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF61
	.byte	0x7
	.byte	0x4f
	.byte	0x8
	.long	0x339
	.byte	0x83
	.uleb128 0x9
	.long	.LASF62
	.byte	0x7
	.byte	0x51
	.byte	0xf
	.long	0x4f5
	.byte	0x88
	.uleb128 0x9
	.long	.LASF63
	.byte	0x7
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0x9
	.long	.LASF64
	.byte	0x7
	.byte	0x5b
	.byte	0x17
	.long	0x500
	.byte	0x98
	.uleb128 0x9
	.long	.LASF65
	.byte	0x7
	.byte	0x5c
	.byte	0x19
	.long	0x50b
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF66
	.byte	0x7
	.byte	0x5d
	.byte	0x14
	.long	0x4ef
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF67
	.byte	0x7
	.byte	0x5e
	.byte	0x9
	.long	0xa1
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF68
	.byte	0x7
	.byte	0x5f
	.byte	0xa
	.long	0xb5
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF69
	.byte	0x7
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF70
	.byte	0x7
	.byte	0x62
	.byte	0x8
	.long	0x511
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF71
	.byte	0x8
	.byte	0x7
	.byte	0x19
	.long	0x349
	.uleb128 0xf
	.long	.LASF132
	.byte	0x7
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF72
	.uleb128 0x6
	.byte	0x8
	.long	0x4e4
	.uleb128 0x6
	.byte	0x8
	.long	0x349
	.uleb128 0x6
	.byte	0x8
	.long	0x4dc
	.uleb128 0xd
	.long	.LASF73
	.uleb128 0x6
	.byte	0x8
	.long	0x4fb
	.uleb128 0xd
	.long	.LASF74
	.uleb128 0x6
	.byte	0x8
	.long	0x506
	.uleb128 0xa
	.long	0xa9
	.long	0x521
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb0
	.uleb128 0x7
	.long	0x521
	.uleb128 0x10
	.long	.LASF75
	.byte	0x9
	.byte	0x89
	.byte	0xe
	.long	0x538
	.uleb128 0x6
	.byte	0x8
	.long	0x4d0
	.uleb128 0x10
	.long	.LASF76
	.byte	0x9
	.byte	0x8a
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF77
	.byte	0x9
	.byte	0x8b
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF78
	.byte	0xa
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xa
	.long	0x527
	.long	0x56d
	.uleb128 0x11
	.byte	0
	.uleb128 0x7
	.long	0x562
	.uleb128 0x10
	.long	.LASF79
	.byte	0xa
	.byte	0x1b
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF80
	.byte	0xa
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF81
	.byte	0xa
	.byte	0x1f
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF82
	.byte	0xb
	.byte	0x2d
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF83
	.byte	0xb
	.byte	0x2e
	.byte	0xe
	.long	0xa3
	.uleb128 0xa
	.long	0xa3
	.long	0x5be
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF84
	.byte	0xc
	.byte	0x9f
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF85
	.byte	0xc
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF86
	.byte	0xc
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF87
	.byte	0xc
	.byte	0xa6
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF88
	.byte	0xc
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF89
	.byte	0xc
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x12
	.long	.LASF90
	.byte	0xc
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x12
	.long	.LASF91
	.byte	0xd
	.value	0x21f
	.byte	0xf
	.long	0x620
	.uleb128 0x6
	.byte	0x8
	.long	0xa3
	.uleb128 0x12
	.long	.LASF92
	.byte	0xd
	.value	0x221
	.byte	0xf
	.long	0x620
	.uleb128 0x10
	.long	.LASF93
	.byte	0xe
	.byte	0x24
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF94
	.byte	0xe
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF95
	.byte	0xe
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF96
	.byte	0xe
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF97
	.byte	0xf
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF98
	.byte	0xf
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF99
	.byte	0xf
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF100
	.byte	0x6
	.byte	0x1e
	.byte	0x12
	.long	0x67b
	.uleb128 0x8
	.long	.LASF101
	.byte	0x4
	.byte	0x6
	.byte	0x1f
	.byte	0x8
	.long	0x6ae
	.uleb128 0x9
	.long	.LASF102
	.byte	0x6
	.byte	0x21
	.byte	0xf
	.long	0x687
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF103
	.byte	0x6
	.byte	0x77
	.byte	0x12
	.long	0x66f
	.uleb128 0x13
	.byte	0x10
	.byte	0x6
	.byte	0xd6
	.byte	0x5
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF104
	.byte	0x6
	.byte	0xd8
	.byte	0xa
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF105
	.byte	0x6
	.byte	0xd9
	.byte	0xb
	.long	0x6f8
	.uleb128 0x14
	.long	.LASF106
	.byte	0x6
	.byte	0xda
	.byte	0xb
	.long	0x708
	.byte	0
	.uleb128 0xa
	.long	0x663
	.long	0x6f8
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x66f
	.long	0x708
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x67b
	.long	0x718
	.uleb128 0xb
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.long	.LASF107
	.byte	0x10
	.byte	0x6
	.byte	0xd4
	.byte	0x8
	.long	0x733
	.uleb128 0x9
	.long	.LASF108
	.byte	0x6
	.byte	0xdb
	.byte	0x9
	.long	0x6ba
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x718
	.uleb128 0x10
	.long	.LASF109
	.byte	0x6
	.byte	0xe4
	.byte	0x1e
	.long	0x733
	.uleb128 0x10
	.long	.LASF110
	.byte	0x6
	.byte	0xe5
	.byte	0x1e
	.long	0x733
	.uleb128 0xa
	.long	0x2d
	.long	0x760
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0x15
	.byte	0x10
	.byte	0x10
	.value	0x204
	.byte	0x3
	.long	0x778
	.uleb128 0x16
	.long	.LASF111
	.byte	0x10
	.value	0x205
	.byte	0x13
	.long	0x778
	.byte	0
	.uleb128 0xa
	.long	0x2d
	.long	0x788
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x17
	.long	.LASF112
	.byte	0x10
	.byte	0x10
	.value	0x203
	.byte	0x8
	.long	0x7a5
	.uleb128 0xe
	.long	.LASF113
	.byte	0x10
	.value	0x206
	.byte	0x5
	.long	0x760
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x788
	.uleb128 0x10
	.long	.LASF114
	.byte	0x11
	.byte	0x52
	.byte	0x23
	.long	0x7a5
	.uleb128 0x8
	.long	.LASF115
	.byte	0x18
	.byte	0x12
	.byte	0x16
	.byte	0x8
	.long	0x7eb
	.uleb128 0x9
	.long	.LASF116
	.byte	0x12
	.byte	0x17
	.byte	0x15
	.long	0x7eb
	.byte	0
	.uleb128 0x9
	.long	.LASF117
	.byte	0x12
	.byte	0x18
	.byte	0x15
	.long	0x7eb
	.byte	0x8
	.uleb128 0x9
	.long	.LASF118
	.byte	0x12
	.byte	0x19
	.byte	0x9
	.long	0xa1
	.byte	0x10
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x7b6
	.uleb128 0x18
	.long	0xa1
	.long	0x800
	.uleb128 0x19
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF119
	.byte	0x13
	.value	0x151
	.byte	0x10
	.long	0x80d
	.uleb128 0x6
	.byte	0x8
	.long	0x7f1
	.uleb128 0x18
	.long	0xa1
	.long	0x827
	.uleb128 0x19
	.long	0xa1
	.uleb128 0x19
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF120
	.byte	0x13
	.value	0x152
	.byte	0x10
	.long	0x834
	.uleb128 0x6
	.byte	0x8
	.long	0x813
	.uleb128 0x1a
	.long	0x845
	.uleb128 0x19
	.long	0xa1
	.byte	0
	.uleb128 0x12
	.long	.LASF121
	.byte	0x13
	.value	0x153
	.byte	0xf
	.long	0x852
	.uleb128 0x6
	.byte	0x8
	.long	0x83a
	.uleb128 0x1b
	.long	.LASF122
	.byte	0x1
	.byte	0x37
	.byte	0x6
	.quad	.LFB91
	.quad	.LFE91-.LFB91
	.uleb128 0x1
	.byte	0x9c
	.long	0x885
	.uleb128 0x1c
	.long	.LASF124
	.byte	0x1
	.byte	0x37
	.byte	0x2f
	.long	0x7eb
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x1b
	.long	.LASF123
	.byte	0x1
	.byte	0x2e
	.byte	0x6
	.quad	.LFB90
	.quad	.LFE90-.LFB90
	.uleb128 0x1
	.byte	0x9c
	.long	0x8c0
	.uleb128 0x1c
	.long	.LASF125
	.byte	0x1
	.byte	0x2e
	.byte	0x2d
	.long	0x7eb
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1c
	.long	.LASF126
	.byte	0x1
	.byte	0x2f
	.byte	0x2d
	.long	0x7eb
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x1d
	.long	.LASF133
	.byte	0x1
	.byte	0x29
	.byte	0x5
	.long	0x6f
	.quad	.LFB89
	.quad	.LFE89-.LFB89
	.uleb128 0x1
	.byte	0x9c
	.long	0x8f1
	.uleb128 0x1c
	.long	.LASF127
	.byte	0x1
	.byte	0x29
	.byte	0x2b
	.long	0x7eb
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x1b
	.long	.LASF128
	.byte	0x1
	.byte	0x22
	.byte	0x6
	.quad	.LFB88
	.quad	.LFE88-.LFB88
	.uleb128 0x1
	.byte	0x9c
	.long	0x92a
	.uleb128 0x1c
	.long	.LASF124
	.byte	0x1
	.byte	0x22
	.byte	0x2d
	.long	0x7eb
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1e
	.string	"d"
	.byte	0x1
	.byte	0x22
	.byte	0x39
	.long	0xa1
	.uleb128 0x1
	.byte	0x54
	.byte	0
	.uleb128 0x1f
	.long	.LASF134
	.byte	0x1
	.byte	0x1b
	.byte	0x6
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x1c
	.long	.LASF127
	.byte	0x1
	.byte	0x1b
	.byte	0x2d
	.long	0x7eb
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF43:
	.string	"_IO_read_ptr"
.LASF55:
	.string	"_chain"
.LASF33:
	.string	"sin6_addr"
.LASF108:
	.string	"__in6_u"
.LASF13:
	.string	"size_t"
.LASF61:
	.string	"_shortbuf"
.LASF112:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF116:
	.string	"prev"
.LASF49:
	.string	"_IO_buf_base"
.LASF130:
	.string	"../deps/cares/src/ares_llist.c"
.LASF14:
	.string	"long long unsigned int"
.LASF100:
	.string	"in_addr_t"
.LASF122:
	.string	"ares__remove_from_list"
.LASF125:
	.string	"new_node"
.LASF117:
	.string	"next"
.LASF64:
	.string	"_codecvt"
.LASF86:
	.string	"__timezone"
.LASF15:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF35:
	.string	"sockaddr_inarp"
.LASF56:
	.string	"_fileno"
.LASF44:
	.string	"_IO_read_end"
.LASF121:
	.string	"ares_free"
.LASF105:
	.string	"__u6_addr16"
.LASF9:
	.string	"long int"
.LASF106:
	.string	"__u6_addr32"
.LASF42:
	.string	"_flags"
.LASF65:
	.string	"_wide_data"
.LASF50:
	.string	"_IO_buf_end"
.LASF59:
	.string	"_cur_column"
.LASF83:
	.string	"program_invocation_short_name"
.LASF73:
	.string	"_IO_codecvt"
.LASF21:
	.string	"sockaddr_dl"
.LASF31:
	.string	"sin6_port"
.LASF98:
	.string	"uint16_t"
.LASF81:
	.string	"_sys_errlist"
.LASF82:
	.string	"program_invocation_name"
.LASF58:
	.string	"_old_offset"
.LASF63:
	.string	"_offset"
.LASF123:
	.string	"ares__insert_in_list"
.LASF110:
	.string	"in6addr_loopback"
.LASF40:
	.string	"sockaddr_x25"
.LASF128:
	.string	"ares__init_list_node"
.LASF36:
	.string	"sockaddr_ipx"
.LASF8:
	.string	"__uint32_t"
.LASF89:
	.string	"timezone"
.LASF28:
	.string	"sin_zero"
.LASF68:
	.string	"__pad5"
.LASF72:
	.string	"_IO_marker"
.LASF75:
	.string	"stdin"
.LASF2:
	.string	"unsigned int"
.LASF102:
	.string	"s_addr"
.LASF67:
	.string	"_freeres_buf"
.LASF134:
	.string	"ares__init_list_head"
.LASF3:
	.string	"long unsigned int"
.LASF47:
	.string	"_IO_write_ptr"
.LASF119:
	.string	"ares_malloc"
.LASF88:
	.string	"daylight"
.LASF78:
	.string	"sys_nerr"
.LASF118:
	.string	"data"
.LASF1:
	.string	"short unsigned int"
.LASF27:
	.string	"sin_addr"
.LASF113:
	.string	"_S6_un"
.LASF131:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF51:
	.string	"_IO_save_base"
.LASF92:
	.string	"environ"
.LASF62:
	.string	"_lock"
.LASF57:
	.string	"_flags2"
.LASF69:
	.string	"_mode"
.LASF76:
	.string	"stdout"
.LASF39:
	.string	"sockaddr_un"
.LASF25:
	.string	"sin_family"
.LASF93:
	.string	"optarg"
.LASF90:
	.string	"getdate_err"
.LASF30:
	.string	"sin6_family"
.LASF94:
	.string	"optind"
.LASF48:
	.string	"_IO_write_end"
.LASF132:
	.string	"_IO_lock_t"
.LASF109:
	.string	"in6addr_any"
.LASF41:
	.string	"_IO_FILE"
.LASF126:
	.string	"old_node"
.LASF91:
	.string	"__environ"
.LASF85:
	.string	"__daylight"
.LASF77:
	.string	"stderr"
.LASF38:
	.string	"sockaddr_ns"
.LASF26:
	.string	"sin_port"
.LASF17:
	.string	"sa_family"
.LASF79:
	.string	"sys_errlist"
.LASF54:
	.string	"_markers"
.LASF133:
	.string	"ares__is_list_empty"
.LASF34:
	.string	"sin6_scope_id"
.LASF0:
	.string	"unsigned char"
.LASF37:
	.string	"sockaddr_iso"
.LASF124:
	.string	"node"
.LASF5:
	.string	"short int"
.LASF80:
	.string	"_sys_nerr"
.LASF60:
	.string	"_vtable_offset"
.LASF87:
	.string	"tzname"
.LASF20:
	.string	"sockaddr_ax25"
.LASF71:
	.string	"FILE"
.LASF115:
	.string	"list_node"
.LASF107:
	.string	"in6_addr"
.LASF127:
	.string	"head"
.LASF96:
	.string	"optopt"
.LASF99:
	.string	"uint32_t"
.LASF12:
	.string	"char"
.LASF32:
	.string	"sin6_flowinfo"
.LASF7:
	.string	"__uint16_t"
.LASF104:
	.string	"__u6_addr8"
.LASF95:
	.string	"opterr"
.LASF11:
	.string	"__off64_t"
.LASF45:
	.string	"_IO_read_base"
.LASF53:
	.string	"_IO_save_end"
.LASF22:
	.string	"sockaddr_eon"
.LASF19:
	.string	"sockaddr_at"
.LASF120:
	.string	"ares_realloc"
.LASF16:
	.string	"sa_family_t"
.LASF70:
	.string	"_unused2"
.LASF111:
	.string	"_S6_u8"
.LASF29:
	.string	"sockaddr_in6"
.LASF23:
	.string	"sockaddr"
.LASF24:
	.string	"sockaddr_in"
.LASF97:
	.string	"uint8_t"
.LASF52:
	.string	"_IO_backup_base"
.LASF18:
	.string	"sa_data"
.LASF66:
	.string	"_freeres_list"
.LASF129:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF74:
	.string	"_IO_wide_data"
.LASF114:
	.string	"ares_in6addr_any"
.LASF84:
	.string	"__tzname"
.LASF46:
	.string	"_IO_write_base"
.LASF103:
	.string	"in_port_t"
.LASF101:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
