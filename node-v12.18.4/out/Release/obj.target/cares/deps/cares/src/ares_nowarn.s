	.file	"ares_nowarn.c"
	.text
.Ltext0:
	.p2align 4
	.globl	aresx_uztosl
	.type	aresx_uztosl, @function
aresx_uztosl:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares_nowarn.c"
	.loc 1 66 1 view -0
	.cfi_startproc
	.loc 1 66 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 72 3 is_stmt 1 view .LVU2
	.loc 1 72 10 is_stmt 0 view .LVU3
	movq	%rdi, %rax
	btrq	$63, %rax
	.loc 1 77 1 view .LVU4
	ret
	.cfi_endproc
.LFE87:
	.size	aresx_uztosl, .-aresx_uztosl
	.p2align 4
	.globl	aresx_uztosi
	.type	aresx_uztosi, @function
aresx_uztosi:
.LVL1:
.LFB88:
	.loc 1 84 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 84 1 is_stmt 0 view .LVU6
	endbr64
	.loc 1 90 3 is_stmt 1 view .LVU7
	.loc 1 90 10 is_stmt 0 view .LVU8
	movl	%edi, %eax
	andl	$2147483647, %eax
	.loc 1 95 1 view .LVU9
	ret
	.cfi_endproc
.LFE88:
	.size	aresx_uztosi, .-aresx_uztosi
	.p2align 4
	.globl	aresx_uztoss
	.type	aresx_uztoss, @function
aresx_uztoss:
.LVL2:
.LFB89:
	.loc 1 102 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 102 1 is_stmt 0 view .LVU11
	endbr64
	.loc 1 108 3 is_stmt 1 view .LVU12
	.loc 1 108 10 is_stmt 0 view .LVU13
	movl	%edi, %eax
	andw	$32767, %ax
	.loc 1 113 1 view .LVU14
	ret
	.cfi_endproc
.LFE89:
	.size	aresx_uztoss, .-aresx_uztoss
	.p2align 4
	.globl	aresx_sitoss
	.type	aresx_sitoss, @function
aresx_sitoss:
.LVL3:
.LFB90:
	.loc 1 120 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 120 1 is_stmt 0 view .LVU16
	endbr64
	.loc 1 126 3 is_stmt 1 view .LVU17
	.loc 1 126 8 view .LVU18
	.loc 1 126 15 view .LVU19
	.loc 1 127 3 view .LVU20
	.loc 1 127 10 is_stmt 0 view .LVU21
	movl	%edi, %eax
	andw	$32767, %ax
	.loc 1 132 1 view .LVU22
	ret
	.cfi_endproc
.LFE90:
	.size	aresx_sitoss, .-aresx_sitoss
	.p2align 4
	.globl	aresx_sltosi
	.type	aresx_sltosi, @function
aresx_sltosi:
.LVL4:
.LFB91:
	.loc 1 139 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 139 1 is_stmt 0 view .LVU24
	endbr64
	.loc 1 145 3 is_stmt 1 view .LVU25
	.loc 1 145 8 view .LVU26
	.loc 1 145 15 view .LVU27
	.loc 1 146 3 view .LVU28
	.loc 1 146 10 is_stmt 0 view .LVU29
	movl	%edi, %eax
	andl	$2147483647, %eax
	.loc 1 151 1 view .LVU30
	ret
	.cfi_endproc
.LFE91:
	.size	aresx_sltosi, .-aresx_sltosi
	.p2align 4
	.globl	aresx_sztosi
	.type	aresx_sztosi, @function
aresx_sztosi:
.LFB96:
	.cfi_startproc
	endbr64
	movl	%edi, %eax
	andl	$2147483647, %eax
	ret
	.cfi_endproc
.LFE96:
	.size	aresx_sztosi, .-aresx_sztosi
	.p2align 4
	.globl	aresx_sztoui
	.type	aresx_sztoui, @function
aresx_sztoui:
.LVL5:
.LFB93:
	.loc 1 177 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 177 1 is_stmt 0 view .LVU32
	endbr64
	.loc 1 183 3 is_stmt 1 view .LVU33
	.loc 1 183 8 view .LVU34
	.loc 1 183 15 view .LVU35
	.loc 1 184 3 view .LVU36
	.loc 1 177 1 is_stmt 0 view .LVU37
	movq	%rdi, %rax
	.loc 1 189 1 view .LVU38
	ret
	.cfi_endproc
.LFE93:
	.size	aresx_sztoui, .-aresx_sztoui
	.p2align 4
	.globl	aresx_sitous
	.type	aresx_sitous, @function
aresx_sitous:
.LVL6:
.LFB94:
	.loc 1 196 1 is_stmt 1 view -0
	.cfi_startproc
	.loc 1 196 1 is_stmt 0 view .LVU40
	endbr64
	.loc 1 202 3 is_stmt 1 view .LVU41
	.loc 1 202 8 view .LVU42
	.loc 1 202 15 view .LVU43
	.loc 1 203 3 view .LVU44
	.loc 1 196 1 is_stmt 0 view .LVU45
	movl	%edi, %eax
	.loc 1 208 1 view .LVU46
	ret
	.cfi_endproc
.LFE94:
	.size	aresx_sitous, .-aresx_sitous
.Letext0:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 3 "/usr/include/x86_64-linux-gnu/sys/types.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 7 "../deps/cares/include/ares_build.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/errno.h"
	.file 13 "/usr/include/time.h"
	.file 14 "/usr/include/unistd.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x755
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF98
	.byte	0x1
	.long	.LASF99
	.long	.LASF100
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF6
	.uleb128 0x4
	.long	.LASF7
	.byte	0x2
	.byte	0x98
	.byte	0x12
	.long	0x5e
	.uleb128 0x4
	.long	.LASF8
	.byte	0x2
	.byte	0x99
	.byte	0x12
	.long	0x5e
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x4
	.long	.LASF9
	.byte	0x2
	.byte	0xc1
	.byte	0x12
	.long	0x5e
	.uleb128 0x6
	.byte	0x8
	.long	0x91
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF10
	.uleb128 0x7
	.long	0x91
	.uleb128 0x4
	.long	.LASF11
	.byte	0x3
	.byte	0x6c
	.byte	0x13
	.long	0x7f
	.uleb128 0x4
	.long	.LASF12
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF13
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF14
	.uleb128 0x4
	.long	.LASF15
	.byte	0x5
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF31
	.byte	0x10
	.byte	0x6
	.byte	0xb2
	.byte	0x8
	.long	0xf7
	.uleb128 0x9
	.long	.LASF16
	.byte	0x6
	.byte	0xb4
	.byte	0x11
	.long	0xc3
	.byte	0
	.uleb128 0x9
	.long	.LASF17
	.byte	0x6
	.byte	0xb5
	.byte	0xa
	.long	0xfc
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0xcf
	.uleb128 0xa
	.long	0x91
	.long	0x10c
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xcf
	.uleb128 0xc
	.long	0x10c
	.uleb128 0xd
	.long	.LASF18
	.uleb128 0x7
	.long	0x117
	.uleb128 0x6
	.byte	0x8
	.long	0x117
	.uleb128 0xc
	.long	0x121
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x7
	.long	0x12c
	.uleb128 0x6
	.byte	0x8
	.long	0x12c
	.uleb128 0xc
	.long	0x136
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x7
	.long	0x141
	.uleb128 0x6
	.byte	0x8
	.long	0x141
	.uleb128 0xc
	.long	0x14b
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x7
	.long	0x156
	.uleb128 0x6
	.byte	0x8
	.long	0x156
	.uleb128 0xc
	.long	0x160
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x7
	.long	0x16b
	.uleb128 0x6
	.byte	0x8
	.long	0x16b
	.uleb128 0xc
	.long	0x175
	.uleb128 0xd
	.long	.LASF23
	.uleb128 0x7
	.long	0x180
	.uleb128 0x6
	.byte	0x8
	.long	0x180
	.uleb128 0xc
	.long	0x18a
	.uleb128 0xd
	.long	.LASF24
	.uleb128 0x7
	.long	0x195
	.uleb128 0x6
	.byte	0x8
	.long	0x195
	.uleb128 0xc
	.long	0x19f
	.uleb128 0xd
	.long	.LASF25
	.uleb128 0x7
	.long	0x1aa
	.uleb128 0x6
	.byte	0x8
	.long	0x1aa
	.uleb128 0xc
	.long	0x1b4
	.uleb128 0xd
	.long	.LASF26
	.uleb128 0x7
	.long	0x1bf
	.uleb128 0x6
	.byte	0x8
	.long	0x1bf
	.uleb128 0xc
	.long	0x1c9
	.uleb128 0xd
	.long	.LASF27
	.uleb128 0x7
	.long	0x1d4
	.uleb128 0x6
	.byte	0x8
	.long	0x1d4
	.uleb128 0xc
	.long	0x1de
	.uleb128 0xd
	.long	.LASF28
	.uleb128 0x7
	.long	0x1e9
	.uleb128 0x6
	.byte	0x8
	.long	0x1e9
	.uleb128 0xc
	.long	0x1f3
	.uleb128 0xd
	.long	.LASF29
	.uleb128 0x7
	.long	0x1fe
	.uleb128 0x6
	.byte	0x8
	.long	0x1fe
	.uleb128 0xc
	.long	0x208
	.uleb128 0x6
	.byte	0x8
	.long	0xf7
	.uleb128 0xc
	.long	0x213
	.uleb128 0x6
	.byte	0x8
	.long	0x11c
	.uleb128 0xc
	.long	0x21e
	.uleb128 0x6
	.byte	0x8
	.long	0x131
	.uleb128 0xc
	.long	0x229
	.uleb128 0x6
	.byte	0x8
	.long	0x146
	.uleb128 0xc
	.long	0x234
	.uleb128 0x6
	.byte	0x8
	.long	0x15b
	.uleb128 0xc
	.long	0x23f
	.uleb128 0x6
	.byte	0x8
	.long	0x170
	.uleb128 0xc
	.long	0x24a
	.uleb128 0x6
	.byte	0x8
	.long	0x185
	.uleb128 0xc
	.long	0x255
	.uleb128 0x6
	.byte	0x8
	.long	0x19a
	.uleb128 0xc
	.long	0x260
	.uleb128 0x6
	.byte	0x8
	.long	0x1af
	.uleb128 0xc
	.long	0x26b
	.uleb128 0x6
	.byte	0x8
	.long	0x1c4
	.uleb128 0xc
	.long	0x276
	.uleb128 0x6
	.byte	0x8
	.long	0x1d9
	.uleb128 0xc
	.long	0x281
	.uleb128 0x6
	.byte	0x8
	.long	0x1ee
	.uleb128 0xc
	.long	0x28c
	.uleb128 0x6
	.byte	0x8
	.long	0x203
	.uleb128 0xc
	.long	0x297
	.uleb128 0x4
	.long	.LASF30
	.byte	0x7
	.byte	0xcd
	.byte	0x11
	.long	0x9d
	.uleb128 0xa
	.long	0x91
	.long	0x2be
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF32
	.byte	0xd8
	.byte	0x8
	.byte	0x31
	.byte	0x8
	.long	0x445
	.uleb128 0x9
	.long	.LASF33
	.byte	0x8
	.byte	0x33
	.byte	0x7
	.long	0x57
	.byte	0
	.uleb128 0x9
	.long	.LASF34
	.byte	0x8
	.byte	0x36
	.byte	0x9
	.long	0x8b
	.byte	0x8
	.uleb128 0x9
	.long	.LASF35
	.byte	0x8
	.byte	0x37
	.byte	0x9
	.long	0x8b
	.byte	0x10
	.uleb128 0x9
	.long	.LASF36
	.byte	0x8
	.byte	0x38
	.byte	0x9
	.long	0x8b
	.byte	0x18
	.uleb128 0x9
	.long	.LASF37
	.byte	0x8
	.byte	0x39
	.byte	0x9
	.long	0x8b
	.byte	0x20
	.uleb128 0x9
	.long	.LASF38
	.byte	0x8
	.byte	0x3a
	.byte	0x9
	.long	0x8b
	.byte	0x28
	.uleb128 0x9
	.long	.LASF39
	.byte	0x8
	.byte	0x3b
	.byte	0x9
	.long	0x8b
	.byte	0x30
	.uleb128 0x9
	.long	.LASF40
	.byte	0x8
	.byte	0x3c
	.byte	0x9
	.long	0x8b
	.byte	0x38
	.uleb128 0x9
	.long	.LASF41
	.byte	0x8
	.byte	0x3d
	.byte	0x9
	.long	0x8b
	.byte	0x40
	.uleb128 0x9
	.long	.LASF42
	.byte	0x8
	.byte	0x40
	.byte	0x9
	.long	0x8b
	.byte	0x48
	.uleb128 0x9
	.long	.LASF43
	.byte	0x8
	.byte	0x41
	.byte	0x9
	.long	0x8b
	.byte	0x50
	.uleb128 0x9
	.long	.LASF44
	.byte	0x8
	.byte	0x42
	.byte	0x9
	.long	0x8b
	.byte	0x58
	.uleb128 0x9
	.long	.LASF45
	.byte	0x8
	.byte	0x44
	.byte	0x16
	.long	0x45e
	.byte	0x60
	.uleb128 0x9
	.long	.LASF46
	.byte	0x8
	.byte	0x46
	.byte	0x14
	.long	0x464
	.byte	0x68
	.uleb128 0x9
	.long	.LASF47
	.byte	0x8
	.byte	0x48
	.byte	0x7
	.long	0x57
	.byte	0x70
	.uleb128 0x9
	.long	.LASF48
	.byte	0x8
	.byte	0x49
	.byte	0x7
	.long	0x57
	.byte	0x74
	.uleb128 0x9
	.long	.LASF49
	.byte	0x8
	.byte	0x4a
	.byte	0xb
	.long	0x65
	.byte	0x78
	.uleb128 0x9
	.long	.LASF50
	.byte	0x8
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF51
	.byte	0x8
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF52
	.byte	0x8
	.byte	0x4f
	.byte	0x8
	.long	0x2ae
	.byte	0x83
	.uleb128 0x9
	.long	.LASF53
	.byte	0x8
	.byte	0x51
	.byte	0xf
	.long	0x46a
	.byte	0x88
	.uleb128 0x9
	.long	.LASF54
	.byte	0x8
	.byte	0x59
	.byte	0xd
	.long	0x71
	.byte	0x90
	.uleb128 0x9
	.long	.LASF55
	.byte	0x8
	.byte	0x5b
	.byte	0x17
	.long	0x475
	.byte	0x98
	.uleb128 0x9
	.long	.LASF56
	.byte	0x8
	.byte	0x5c
	.byte	0x19
	.long	0x480
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF57
	.byte	0x8
	.byte	0x5d
	.byte	0x14
	.long	0x464
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF58
	.byte	0x8
	.byte	0x5e
	.byte	0x9
	.long	0x7d
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF59
	.byte	0x8
	.byte	0x5f
	.byte	0xa
	.long	0xa9
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF60
	.byte	0x8
	.byte	0x60
	.byte	0x7
	.long	0x57
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF61
	.byte	0x8
	.byte	0x62
	.byte	0x8
	.long	0x486
	.byte	0xc4
	.byte	0
	.uleb128 0x4
	.long	.LASF62
	.byte	0x9
	.byte	0x7
	.byte	0x19
	.long	0x2be
	.uleb128 0xe
	.long	.LASF101
	.byte	0x8
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF63
	.uleb128 0x6
	.byte	0x8
	.long	0x459
	.uleb128 0x6
	.byte	0x8
	.long	0x2be
	.uleb128 0x6
	.byte	0x8
	.long	0x451
	.uleb128 0xd
	.long	.LASF64
	.uleb128 0x6
	.byte	0x8
	.long	0x470
	.uleb128 0xd
	.long	.LASF65
	.uleb128 0x6
	.byte	0x8
	.long	0x47b
	.uleb128 0xa
	.long	0x91
	.long	0x496
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x98
	.uleb128 0x7
	.long	0x496
	.uleb128 0xf
	.long	.LASF66
	.byte	0xa
	.byte	0x89
	.byte	0xe
	.long	0x4ad
	.uleb128 0x6
	.byte	0x8
	.long	0x445
	.uleb128 0xf
	.long	.LASF67
	.byte	0xa
	.byte	0x8a
	.byte	0xe
	.long	0x4ad
	.uleb128 0xf
	.long	.LASF68
	.byte	0xa
	.byte	0x8b
	.byte	0xe
	.long	0x4ad
	.uleb128 0xf
	.long	.LASF69
	.byte	0xb
	.byte	0x1a
	.byte	0xc
	.long	0x57
	.uleb128 0xa
	.long	0x49c
	.long	0x4e2
	.uleb128 0x10
	.byte	0
	.uleb128 0x7
	.long	0x4d7
	.uleb128 0xf
	.long	.LASF70
	.byte	0xb
	.byte	0x1b
	.byte	0x1a
	.long	0x4e2
	.uleb128 0xf
	.long	.LASF71
	.byte	0xb
	.byte	0x1e
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF72
	.byte	0xb
	.byte	0x1f
	.byte	0x1a
	.long	0x4e2
	.uleb128 0xf
	.long	.LASF73
	.byte	0xc
	.byte	0x2d
	.byte	0xe
	.long	0x8b
	.uleb128 0xf
	.long	.LASF74
	.byte	0xc
	.byte	0x2e
	.byte	0xe
	.long	0x8b
	.uleb128 0xa
	.long	0x8b
	.long	0x533
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0xf
	.long	.LASF75
	.byte	0xd
	.byte	0x9f
	.byte	0xe
	.long	0x523
	.uleb128 0xf
	.long	.LASF76
	.byte	0xd
	.byte	0xa0
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF77
	.byte	0xd
	.byte	0xa1
	.byte	0x11
	.long	0x5e
	.uleb128 0xf
	.long	.LASF78
	.byte	0xd
	.byte	0xa6
	.byte	0xe
	.long	0x523
	.uleb128 0xf
	.long	.LASF79
	.byte	0xd
	.byte	0xae
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF80
	.byte	0xd
	.byte	0xaf
	.byte	0x11
	.long	0x5e
	.uleb128 0x11
	.long	.LASF81
	.byte	0xd
	.value	0x112
	.byte	0xc
	.long	0x57
	.uleb128 0x11
	.long	.LASF82
	.byte	0xe
	.value	0x21f
	.byte	0xf
	.long	0x595
	.uleb128 0x6
	.byte	0x8
	.long	0x8b
	.uleb128 0x11
	.long	.LASF83
	.byte	0xe
	.value	0x221
	.byte	0xf
	.long	0x595
	.uleb128 0xf
	.long	.LASF84
	.byte	0xf
	.byte	0x24
	.byte	0xe
	.long	0x8b
	.uleb128 0xf
	.long	.LASF85
	.byte	0xf
	.byte	0x32
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF86
	.byte	0xf
	.byte	0x37
	.byte	0xc
	.long	0x57
	.uleb128 0xf
	.long	.LASF87
	.byte	0xf
	.byte	0x3b
	.byte	0xc
	.long	0x57
	.uleb128 0x12
	.long	.LASF88
	.byte	0x1
	.byte	0xc3
	.byte	0x10
	.long	0x34
	.quad	.LFB94
	.quad	.LFE94-.LFB94
	.uleb128 0x1
	.byte	0x9c
	.long	0x609
	.uleb128 0x13
	.long	.LASF90
	.byte	0x1
	.byte	0xc3
	.byte	0x21
	.long	0x57
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x12
	.long	.LASF89
	.byte	0x1
	.byte	0xb0
	.byte	0xe
	.long	0x3b
	.quad	.LFB93
	.quad	.LFE93-.LFB93
	.uleb128 0x1
	.byte	0x9c
	.long	0x63a
	.uleb128 0x13
	.long	.LASF91
	.byte	0x1
	.byte	0xb0
	.byte	0x28
	.long	0x2a2
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x14
	.long	.LASF102
	.byte	0x1
	.byte	0x9d
	.byte	0x5
	.long	0x57
	.long	0x657
	.uleb128 0x15
	.long	.LASF91
	.byte	0x1
	.byte	0x9d
	.byte	0x1f
	.long	0x2a2
	.byte	0
	.uleb128 0x16
	.long	.LASF103
	.byte	0x1
	.byte	0x8a
	.byte	0x5
	.long	0x57
	.byte	0x1
	.long	0x675
	.uleb128 0x15
	.long	.LASF92
	.byte	0x1
	.byte	0x8a
	.byte	0x17
	.long	0x5e
	.byte	0
	.uleb128 0x12
	.long	.LASF93
	.byte	0x1
	.byte	0x77
	.byte	0x7
	.long	0x50
	.quad	.LFB90
	.quad	.LFE90-.LFB90
	.uleb128 0x1
	.byte	0x9c
	.long	0x6a6
	.uleb128 0x13
	.long	.LASF90
	.byte	0x1
	.byte	0x77
	.byte	0x18
	.long	0x57
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x12
	.long	.LASF94
	.byte	0x1
	.byte	0x65
	.byte	0x7
	.long	0x50
	.quad	.LFB89
	.quad	.LFE89-.LFB89
	.uleb128 0x1
	.byte	0x9c
	.long	0x6d7
	.uleb128 0x13
	.long	.LASF95
	.byte	0x1
	.byte	0x65
	.byte	0x1b
	.long	0xa9
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x12
	.long	.LASF96
	.byte	0x1
	.byte	0x53
	.byte	0x5
	.long	0x57
	.quad	.LFB88
	.quad	.LFE88-.LFB88
	.uleb128 0x1
	.byte	0x9c
	.long	0x708
	.uleb128 0x13
	.long	.LASF95
	.byte	0x1
	.byte	0x53
	.byte	0x19
	.long	0xa9
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x12
	.long	.LASF97
	.byte	0x1
	.byte	0x41
	.byte	0x6
	.long	0x5e
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0x739
	.uleb128 0x13
	.long	.LASF95
	.byte	0x1
	.byte	0x41
	.byte	0x1a
	.long	0xa9
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.uleb128 0x17
	.long	0x657
	.quad	.LFB91
	.quad	.LFE91-.LFB91
	.uleb128 0x1
	.byte	0x9c
	.uleb128 0x18
	.long	0x668
	.uleb128 0x1
	.byte	0x55
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF85:
	.string	"optind"
.LASF84:
	.string	"optarg"
.LASF49:
	.string	"_old_offset"
.LASF90:
	.string	"sinum"
.LASF36:
	.string	"_IO_read_base"
.LASF91:
	.string	"sznum"
.LASF69:
	.string	"sys_nerr"
.LASF44:
	.string	"_IO_save_end"
.LASF26:
	.string	"sockaddr_iso"
.LASF12:
	.string	"size_t"
.LASF24:
	.string	"sockaddr_inarp"
.LASF54:
	.string	"_offset"
.LASF75:
	.string	"__tzname"
.LASF38:
	.string	"_IO_write_ptr"
.LASF14:
	.string	"long long int"
.LASF28:
	.string	"sockaddr_un"
.LASF40:
	.string	"_IO_buf_base"
.LASF11:
	.string	"ssize_t"
.LASF58:
	.string	"_freeres_buf"
.LASF82:
	.string	"__environ"
.LASF45:
	.string	"_markers"
.LASF35:
	.string	"_IO_read_end"
.LASF80:
	.string	"timezone"
.LASF79:
	.string	"daylight"
.LASF101:
	.string	"_IO_lock_t"
.LASF86:
	.string	"opterr"
.LASF15:
	.string	"sa_family_t"
.LASF5:
	.string	"short int"
.LASF72:
	.string	"_sys_errlist"
.LASF68:
	.string	"stderr"
.LASF27:
	.string	"sockaddr_ns"
.LASF25:
	.string	"sockaddr_ipx"
.LASF9:
	.string	"__ssize_t"
.LASF53:
	.string	"_lock"
.LASF76:
	.string	"__daylight"
.LASF6:
	.string	"long int"
.LASF89:
	.string	"aresx_sztoui"
.LASF96:
	.string	"aresx_uztosi"
.LASF50:
	.string	"_cur_column"
.LASF97:
	.string	"aresx_uztosl"
.LASF94:
	.string	"aresx_uztoss"
.LASF71:
	.string	"_sys_nerr"
.LASF32:
	.string	"_IO_FILE"
.LASF73:
	.string	"program_invocation_name"
.LASF0:
	.string	"unsigned char"
.LASF30:
	.string	"ares_ssize_t"
.LASF88:
	.string	"aresx_sitous"
.LASF4:
	.string	"signed char"
.LASF55:
	.string	"_codecvt"
.LASF13:
	.string	"long long unsigned int"
.LASF23:
	.string	"sockaddr_in6"
.LASF83:
	.string	"environ"
.LASF63:
	.string	"_IO_marker"
.LASF52:
	.string	"_shortbuf"
.LASF19:
	.string	"sockaddr_ax25"
.LASF37:
	.string	"_IO_write_base"
.LASF61:
	.string	"_unused2"
.LASF34:
	.string	"_IO_read_ptr"
.LASF17:
	.string	"sa_data"
.LASF41:
	.string	"_IO_buf_end"
.LASF20:
	.string	"sockaddr_dl"
.LASF10:
	.string	"char"
.LASF77:
	.string	"__timezone"
.LASF92:
	.string	"slnum"
.LASF56:
	.string	"_wide_data"
.LASF57:
	.string	"_freeres_list"
.LASF78:
	.string	"tzname"
.LASF59:
	.string	"__pad5"
.LASF100:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF21:
	.string	"sockaddr_eon"
.LASF1:
	.string	"short unsigned int"
.LASF74:
	.string	"program_invocation_short_name"
.LASF81:
	.string	"getdate_err"
.LASF3:
	.string	"long unsigned int"
.LASF39:
	.string	"_IO_write_end"
.LASF8:
	.string	"__off64_t"
.LASF31:
	.string	"sockaddr"
.LASF29:
	.string	"sockaddr_x25"
.LASF7:
	.string	"__off_t"
.LASF46:
	.string	"_chain"
.LASF98:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF65:
	.string	"_IO_wide_data"
.LASF43:
	.string	"_IO_backup_base"
.LASF66:
	.string	"stdin"
.LASF95:
	.string	"uznum"
.LASF48:
	.string	"_flags2"
.LASF16:
	.string	"sa_family"
.LASF60:
	.string	"_mode"
.LASF18:
	.string	"sockaddr_at"
.LASF51:
	.string	"_vtable_offset"
.LASF102:
	.string	"aresx_sztosi"
.LASF42:
	.string	"_IO_save_base"
.LASF70:
	.string	"sys_errlist"
.LASF87:
	.string	"optopt"
.LASF47:
	.string	"_fileno"
.LASF62:
	.string	"FILE"
.LASF2:
	.string	"unsigned int"
.LASF99:
	.string	"../deps/cares/src/ares_nowarn.c"
.LASF33:
	.string	"_flags"
.LASF22:
	.string	"sockaddr_in"
.LASF67:
	.string	"stdout"
.LASF93:
	.string	"aresx_sitoss"
.LASF64:
	.string	"_IO_codecvt"
.LASF103:
	.string	"aresx_sltosi"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
