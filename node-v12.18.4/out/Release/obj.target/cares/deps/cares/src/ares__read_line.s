	.file	"ares__read_line.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares__read_line
	.type	ares__read_line, @function
ares__read_line:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares__read_line.c"
	.loc 1 32 1 view -0
	.cfi_startproc
	.loc 1 32 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 33 3 is_stmt 1 view .LVU2
	.loc 1 34 3 view .LVU3
.LVL1:
	.loc 1 35 3 view .LVU4
	.loc 1 37 3 view .LVU5
	.loc 1 32 1 is_stmt 0 view .LVU6
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%rdi, %r14
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdx, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%rsi, %r12
	pushq	%rbx
	subq	$8, %rsp
	.cfi_offset 3, -56
	.loc 1 37 6 view .LVU7
	cmpq	$0, (%rsi)
	je	.L2
	movq	(%rdx), %rdi
.LVL2:
.L3:
	.loc 1 42 16 view .LVU8
	xorl	%ebx, %ebx
.LVL3:
	.p2align 4,,10
	.p2align 3
.L5:
	.loc 1 45 3 is_stmt 1 view .LVU9
.LBB5:
	.loc 1 47 7 view .LVU10
	.loc 1 47 25 is_stmt 0 view .LVU11
	subq	%rbx, %rdi
	call	aresx_uztosi@PLT
.LVL4:
	.loc 1 49 12 view .LVU12
	movq	(%r12), %rdi
.LBB6:
.LBB7:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/stdio2.h"
	.loc 2 265 10 view .LVU13
	movq	%r14, %rdx
.LBE7:
.LBE6:
	.loc 1 47 25 view .LVU14
	movl	%eax, %esi
.LVL5:
	.loc 1 49 7 is_stmt 1 view .LVU15
.LBB10:
.LBI6:
	.loc 2 255 1 view .LVU16
.LBB8:
	.loc 2 257 3 view .LVU17
	.loc 2 265 3 view .LVU18
.LBE8:
.LBE10:
	.loc 1 49 12 is_stmt 0 view .LVU19
	addq	%rbx, %rdi
.LVL6:
.LBB11:
.LBB9:
	.loc 2 265 10 view .LVU20
	call	fgets@PLT
.LVL7:
	.loc 2 265 10 view .LVU21
.LBE9:
.LBE11:
	.loc 1 49 10 view .LVU22
	testq	%rax, %rax
	je	.L15
	.loc 1 51 7 is_stmt 1 view .LVU23
	.loc 1 51 29 is_stmt 0 view .LVU24
	movq	(%r12), %r15
	.loc 1 51 34 view .LVU25
	leaq	(%r15,%rbx), %rdi
	.loc 1 51 22 view .LVU26
	call	strlen@PLT
.LVL8:
	.loc 1 51 11 view .LVU27
	addq	%rax, %rbx
.LVL9:
	.loc 1 52 7 is_stmt 1 view .LVU28
	.loc 1 52 17 is_stmt 0 view .LVU29
	leaq	-1(%r15,%rbx), %rax
	.loc 1 52 10 view .LVU30
	cmpb	$10, (%rax)
	je	.L16
	.loc 1 57 7 is_stmt 1 view .LVU31
.LVL10:
	.loc 1 58 7 view .LVU32
	.loc 1 58 16 is_stmt 0 view .LVU33
	movq	0(%r13), %rdi
	.loc 1 58 25 view .LVU34
	leaq	-1(%rdi), %rax
	.loc 1 58 9 view .LVU35
	cmpq	%rbx, %rax
	ja	.L5
	.loc 1 62 7 is_stmt 1 view .LVU36
	.loc 1 62 16 is_stmt 0 view .LVU37
	leaq	(%rdi,%rdi), %rsi
	movq	%r15, %rdi
	call	*ares_realloc(%rip)
.LVL11:
	.loc 1 63 7 is_stmt 1 view .LVU38
	.loc 1 63 10 is_stmt 0 view .LVU39
	testq	%rax, %rax
	je	.L17
	.loc 1 69 7 is_stmt 1 view .LVU40
	.loc 1 69 12 is_stmt 0 view .LVU41
	movq	%rax, (%r12)
	.loc 1 70 7 is_stmt 1 view .LVU42
	.loc 1 70 16 is_stmt 0 view .LVU43
	movq	0(%r13), %rax
.LVL12:
	.loc 1 70 16 view .LVU44
	leaq	(%rax,%rax), %rdi
	movq	%rdi, 0(%r13)
	jmp	.L5
.LVL13:
	.p2align 4,,10
	.p2align 3
.L15:
	.loc 1 50 9 is_stmt 1 view .LVU45
	.loc 1 50 34 is_stmt 0 view .LVU46
	testq	%rbx, %rbx
	je	.L18
.LVL14:
.L1:
	.loc 1 50 34 view .LVU47
.LBE5:
	.loc 1 73 1 view .LVU48
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
.LVL15:
	.loc 1 73 1 view .LVU49
	popq	%r13
.LVL16:
	.loc 1 73 1 view .LVU50
	popq	%r14
.LVL17:
	.loc 1 73 1 view .LVU51
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL18:
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
.LBB12:
	.loc 1 54 11 is_stmt 1 view .LVU52
	.loc 1 54 27 is_stmt 0 view .LVU53
	movb	$0, (%rax)
	.loc 1 55 11 is_stmt 1 view .LVU54
.LBE12:
	.loc 1 72 3 view .LVU55
	.loc 1 73 1 is_stmt 0 view .LVU56
	addq	$8, %rsp
	.loc 1 72 10 view .LVU57
	xorl	%eax, %eax
	.loc 1 73 1 view .LVU58
	popq	%rbx
.LVL19:
	.loc 1 73 1 view .LVU59
	popq	%r12
.LVL20:
	.loc 1 73 1 view .LVU60
	popq	%r13
.LVL21:
	.loc 1 73 1 view .LVU61
	popq	%r14
.LVL22:
	.loc 1 73 1 view .LVU62
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL23:
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	.loc 1 39 7 is_stmt 1 view .LVU63
	.loc 1 39 14 is_stmt 0 view .LVU64
	movl	$128, %edi
.LVL24:
	.loc 1 39 14 view .LVU65
	call	*ares_malloc(%rip)
.LVL25:
	.loc 1 39 12 view .LVU66
	movq	%rax, (%r12)
	.loc 1 40 7 is_stmt 1 view .LVU67
	.loc 1 40 10 is_stmt 0 view .LVU68
	testq	%rax, %rax
	je	.L10
	.loc 1 42 7 is_stmt 1 view .LVU69
	.loc 1 42 16 is_stmt 0 view .LVU70
	movq	$128, 0(%r13)
	movl	$128, %edi
	jmp	.L3
.LVL26:
	.p2align 4,,10
	.p2align 3
.L17:
.LBB13:
	.loc 1 65 11 is_stmt 1 view .LVU71
	movq	(%r12), %rdi
	call	*ares_free(%rip)
.LVL27:
	.loc 1 66 11 view .LVU72
	.loc 1 66 16 is_stmt 0 view .LVU73
	movq	$0, (%r12)
	.loc 1 67 11 is_stmt 1 view .LVU74
.LBE13:
	.loc 1 73 1 is_stmt 0 view .LVU75
	addq	$8, %rsp
.LBB14:
	.loc 1 67 18 view .LVU76
	movl	$15, %eax
.LBE14:
	.loc 1 73 1 view .LVU77
	popq	%rbx
.LVL28:
	.loc 1 73 1 view .LVU78
	popq	%r12
.LVL29:
	.loc 1 73 1 view .LVU79
	popq	%r13
.LVL30:
	.loc 1 73 1 view .LVU80
	popq	%r14
.LVL31:
	.loc 1 73 1 view .LVU81
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL32:
	.p2align 4,,10
	.p2align 3
.L18:
	.cfi_restore_state
.LBB15:
	.loc 1 50 37 discriminator 1 view .LVU82
	movq	%r14, %rdi
	call	ferror@PLT
.LVL33:
	.loc 1 50 54 discriminator 1 view .LVU83
	testl	%eax, %eax
	setne	%al
.LBE15:
	.loc 1 73 1 discriminator 1 view .LVU84
	addq	$8, %rsp
.LBB16:
	.loc 1 50 54 discriminator 1 view .LVU85
	movzbl	%al, %eax
.LBE16:
	.loc 1 73 1 discriminator 1 view .LVU86
	popq	%rbx
.LVL34:
	.loc 1 73 1 discriminator 1 view .LVU87
	popq	%r12
.LVL35:
.LBB17:
	.loc 1 50 54 discriminator 1 view .LVU88
	addl	$13, %eax
.LBE17:
	.loc 1 73 1 discriminator 1 view .LVU89
	popq	%r13
.LVL36:
	.loc 1 73 1 discriminator 1 view .LVU90
	popq	%r14
.LVL37:
	.loc 1 73 1 discriminator 1 view .LVU91
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL38:
.L10:
	.cfi_restore_state
	.loc 1 41 16 view .LVU92
	movl	$15, %eax
	jmp	.L1
	.cfi_endproc
.LFE87:
	.size	ares__read_line, .-ares__read_line
.Letext0:
	.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 7 "/usr/include/netinet/in.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/errno.h"
	.file 13 "/usr/include/time.h"
	.file 14 "/usr/include/unistd.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 17 "../deps/cares/include/ares.h"
	.file 18 "../deps/cares/src/ares_ipv6.h"
	.file 19 "../deps/cares/src/ares_private.h"
	.file 20 "../deps/cares/src/ares_nowarn.h"
	.file 21 "/usr/include/string.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x9ff
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF128
	.byte	0x1
	.long	.LASF129
	.long	.LASF130
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x3
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x3
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x3
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x3
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x3
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0xae
	.uleb128 0x7
	.long	0xa3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x8
	.long	0xae
	.uleb128 0x3
	.long	.LASF13
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF14
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF15
	.uleb128 0x3
	.long	.LASF16
	.byte	0x5
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x9
	.long	.LASF23
	.byte	0x10
	.byte	0x6
	.byte	0xb2
	.byte	0x8
	.long	0x108
	.uleb128 0xa
	.long	.LASF17
	.byte	0x6
	.byte	0xb4
	.byte	0x11
	.long	0xd4
	.byte	0
	.uleb128 0xa
	.long	.LASF18
	.byte	0x6
	.byte	0xb5
	.byte	0xa
	.long	0x10d
	.byte	0x2
	.byte	0
	.uleb128 0x8
	.long	0xe0
	.uleb128 0xb
	.long	0xae
	.long	0x11d
	.uleb128 0xc
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xe0
	.uleb128 0x7
	.long	0x11d
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x8
	.long	0x128
	.uleb128 0x6
	.byte	0x8
	.long	0x128
	.uleb128 0x7
	.long	0x132
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x8
	.long	0x13d
	.uleb128 0x6
	.byte	0x8
	.long	0x13d
	.uleb128 0x7
	.long	0x147
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x8
	.long	0x152
	.uleb128 0x6
	.byte	0x8
	.long	0x152
	.uleb128 0x7
	.long	0x15c
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x8
	.long	0x167
	.uleb128 0x6
	.byte	0x8
	.long	0x167
	.uleb128 0x7
	.long	0x171
	.uleb128 0x9
	.long	.LASF24
	.byte	0x10
	.byte	0x7
	.byte	0xee
	.byte	0x8
	.long	0x1be
	.uleb128 0xa
	.long	.LASF25
	.byte	0x7
	.byte	0xf0
	.byte	0x11
	.long	0xd4
	.byte	0
	.uleb128 0xa
	.long	.LASF26
	.byte	0x7
	.byte	0xf1
	.byte	0xf
	.long	0x6b8
	.byte	0x2
	.uleb128 0xa
	.long	.LASF27
	.byte	0x7
	.byte	0xf2
	.byte	0x14
	.long	0x69d
	.byte	0x4
	.uleb128 0xa
	.long	.LASF28
	.byte	0x7
	.byte	0xf5
	.byte	0x13
	.long	0x75a
	.byte	0x8
	.byte	0
	.uleb128 0x8
	.long	0x17c
	.uleb128 0x6
	.byte	0x8
	.long	0x17c
	.uleb128 0x7
	.long	0x1c3
	.uleb128 0x9
	.long	.LASF29
	.byte	0x1c
	.byte	0x7
	.byte	0xfd
	.byte	0x8
	.long	0x221
	.uleb128 0xa
	.long	.LASF30
	.byte	0x7
	.byte	0xff
	.byte	0x11
	.long	0xd4
	.byte	0
	.uleb128 0xe
	.long	.LASF31
	.byte	0x7
	.value	0x100
	.byte	0xf
	.long	0x6b8
	.byte	0x2
	.uleb128 0xe
	.long	.LASF32
	.byte	0x7
	.value	0x101
	.byte	0xe
	.long	0x685
	.byte	0x4
	.uleb128 0xe
	.long	.LASF33
	.byte	0x7
	.value	0x102
	.byte	0x15
	.long	0x722
	.byte	0x8
	.uleb128 0xe
	.long	.LASF34
	.byte	0x7
	.value	0x103
	.byte	0xe
	.long	0x685
	.byte	0x18
	.byte	0
	.uleb128 0x8
	.long	0x1ce
	.uleb128 0x6
	.byte	0x8
	.long	0x1ce
	.uleb128 0x7
	.long	0x226
	.uleb128 0xd
	.long	.LASF35
	.uleb128 0x8
	.long	0x231
	.uleb128 0x6
	.byte	0x8
	.long	0x231
	.uleb128 0x7
	.long	0x23b
	.uleb128 0xd
	.long	.LASF36
	.uleb128 0x8
	.long	0x246
	.uleb128 0x6
	.byte	0x8
	.long	0x246
	.uleb128 0x7
	.long	0x250
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x8
	.long	0x25b
	.uleb128 0x6
	.byte	0x8
	.long	0x25b
	.uleb128 0x7
	.long	0x265
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x8
	.long	0x270
	.uleb128 0x6
	.byte	0x8
	.long	0x270
	.uleb128 0x7
	.long	0x27a
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x8
	.long	0x285
	.uleb128 0x6
	.byte	0x8
	.long	0x285
	.uleb128 0x7
	.long	0x28f
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x8
	.long	0x29a
	.uleb128 0x6
	.byte	0x8
	.long	0x29a
	.uleb128 0x7
	.long	0x2a4
	.uleb128 0x6
	.byte	0x8
	.long	0x108
	.uleb128 0x7
	.long	0x2af
	.uleb128 0x6
	.byte	0x8
	.long	0x12d
	.uleb128 0x7
	.long	0x2ba
	.uleb128 0x6
	.byte	0x8
	.long	0x142
	.uleb128 0x7
	.long	0x2c5
	.uleb128 0x6
	.byte	0x8
	.long	0x157
	.uleb128 0x7
	.long	0x2d0
	.uleb128 0x6
	.byte	0x8
	.long	0x16c
	.uleb128 0x7
	.long	0x2db
	.uleb128 0x6
	.byte	0x8
	.long	0x1be
	.uleb128 0x7
	.long	0x2e6
	.uleb128 0x6
	.byte	0x8
	.long	0x221
	.uleb128 0x7
	.long	0x2f1
	.uleb128 0x6
	.byte	0x8
	.long	0x236
	.uleb128 0x7
	.long	0x2fc
	.uleb128 0x6
	.byte	0x8
	.long	0x24b
	.uleb128 0x7
	.long	0x307
	.uleb128 0x6
	.byte	0x8
	.long	0x260
	.uleb128 0x7
	.long	0x312
	.uleb128 0x6
	.byte	0x8
	.long	0x275
	.uleb128 0x7
	.long	0x31d
	.uleb128 0x6
	.byte	0x8
	.long	0x28a
	.uleb128 0x7
	.long	0x328
	.uleb128 0x6
	.byte	0x8
	.long	0x29f
	.uleb128 0x7
	.long	0x333
	.uleb128 0xb
	.long	0xae
	.long	0x34e
	.uleb128 0xc
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x9
	.long	.LASF41
	.byte	0xd8
	.byte	0x8
	.byte	0x31
	.byte	0x8
	.long	0x4d5
	.uleb128 0xa
	.long	.LASF42
	.byte	0x8
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0xa
	.long	.LASF43
	.byte	0x8
	.byte	0x36
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0xa
	.long	.LASF44
	.byte	0x8
	.byte	0x37
	.byte	0x9
	.long	0xa3
	.byte	0x10
	.uleb128 0xa
	.long	.LASF45
	.byte	0x8
	.byte	0x38
	.byte	0x9
	.long	0xa3
	.byte	0x18
	.uleb128 0xa
	.long	.LASF46
	.byte	0x8
	.byte	0x39
	.byte	0x9
	.long	0xa3
	.byte	0x20
	.uleb128 0xa
	.long	.LASF47
	.byte	0x8
	.byte	0x3a
	.byte	0x9
	.long	0xa3
	.byte	0x28
	.uleb128 0xa
	.long	.LASF48
	.byte	0x8
	.byte	0x3b
	.byte	0x9
	.long	0xa3
	.byte	0x30
	.uleb128 0xa
	.long	.LASF49
	.byte	0x8
	.byte	0x3c
	.byte	0x9
	.long	0xa3
	.byte	0x38
	.uleb128 0xa
	.long	.LASF50
	.byte	0x8
	.byte	0x3d
	.byte	0x9
	.long	0xa3
	.byte	0x40
	.uleb128 0xa
	.long	.LASF51
	.byte	0x8
	.byte	0x40
	.byte	0x9
	.long	0xa3
	.byte	0x48
	.uleb128 0xa
	.long	.LASF52
	.byte	0x8
	.byte	0x41
	.byte	0x9
	.long	0xa3
	.byte	0x50
	.uleb128 0xa
	.long	.LASF53
	.byte	0x8
	.byte	0x42
	.byte	0x9
	.long	0xa3
	.byte	0x58
	.uleb128 0xa
	.long	.LASF54
	.byte	0x8
	.byte	0x44
	.byte	0x16
	.long	0x4ee
	.byte	0x60
	.uleb128 0xa
	.long	.LASF55
	.byte	0x8
	.byte	0x46
	.byte	0x14
	.long	0x4f4
	.byte	0x68
	.uleb128 0xa
	.long	.LASF56
	.byte	0x8
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0xa
	.long	.LASF57
	.byte	0x8
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0xa
	.long	.LASF58
	.byte	0x8
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0xa
	.long	.LASF59
	.byte	0x8
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0xa
	.long	.LASF60
	.byte	0x8
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0xa
	.long	.LASF61
	.byte	0x8
	.byte	0x4f
	.byte	0x8
	.long	0x33e
	.byte	0x83
	.uleb128 0xa
	.long	.LASF62
	.byte	0x8
	.byte	0x51
	.byte	0xf
	.long	0x4fa
	.byte	0x88
	.uleb128 0xa
	.long	.LASF63
	.byte	0x8
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0xa
	.long	.LASF64
	.byte	0x8
	.byte	0x5b
	.byte	0x17
	.long	0x505
	.byte	0x98
	.uleb128 0xa
	.long	.LASF65
	.byte	0x8
	.byte	0x5c
	.byte	0x19
	.long	0x510
	.byte	0xa0
	.uleb128 0xa
	.long	.LASF66
	.byte	0x8
	.byte	0x5d
	.byte	0x14
	.long	0x4f4
	.byte	0xa8
	.uleb128 0xa
	.long	.LASF67
	.byte	0x8
	.byte	0x5e
	.byte	0x9
	.long	0xa1
	.byte	0xb0
	.uleb128 0xa
	.long	.LASF68
	.byte	0x8
	.byte	0x5f
	.byte	0xa
	.long	0xba
	.byte	0xb8
	.uleb128 0xa
	.long	.LASF69
	.byte	0x8
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0xa
	.long	.LASF70
	.byte	0x8
	.byte	0x62
	.byte	0x8
	.long	0x516
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF71
	.byte	0x9
	.byte	0x7
	.byte	0x19
	.long	0x34e
	.uleb128 0xf
	.long	.LASF131
	.byte	0x8
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF72
	.uleb128 0x6
	.byte	0x8
	.long	0x4e9
	.uleb128 0x6
	.byte	0x8
	.long	0x34e
	.uleb128 0x6
	.byte	0x8
	.long	0x4e1
	.uleb128 0xd
	.long	.LASF73
	.uleb128 0x6
	.byte	0x8
	.long	0x500
	.uleb128 0xd
	.long	.LASF74
	.uleb128 0x6
	.byte	0x8
	.long	0x50b
	.uleb128 0xb
	.long	0xae
	.long	0x526
	.uleb128 0xc
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb5
	.uleb128 0x8
	.long	0x526
	.uleb128 0x10
	.long	.LASF75
	.byte	0xa
	.byte	0x89
	.byte	0xe
	.long	0x53d
	.uleb128 0x6
	.byte	0x8
	.long	0x4d5
	.uleb128 0x7
	.long	0x53d
	.uleb128 0x10
	.long	.LASF76
	.byte	0xa
	.byte	0x8a
	.byte	0xe
	.long	0x53d
	.uleb128 0x10
	.long	.LASF77
	.byte	0xa
	.byte	0x8b
	.byte	0xe
	.long	0x53d
	.uleb128 0x10
	.long	.LASF78
	.byte	0xb
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xb
	.long	0x52c
	.long	0x577
	.uleb128 0x11
	.byte	0
	.uleb128 0x8
	.long	0x56c
	.uleb128 0x10
	.long	.LASF79
	.byte	0xb
	.byte	0x1b
	.byte	0x1a
	.long	0x577
	.uleb128 0x10
	.long	.LASF80
	.byte	0xb
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF81
	.byte	0xb
	.byte	0x1f
	.byte	0x1a
	.long	0x577
	.uleb128 0x10
	.long	.LASF82
	.byte	0xc
	.byte	0x2d
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF83
	.byte	0xc
	.byte	0x2e
	.byte	0xe
	.long	0xa3
	.uleb128 0xb
	.long	0xa3
	.long	0x5c8
	.uleb128 0xc
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF84
	.byte	0xd
	.byte	0x9f
	.byte	0xe
	.long	0x5b8
	.uleb128 0x10
	.long	.LASF85
	.byte	0xd
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF86
	.byte	0xd
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF87
	.byte	0xd
	.byte	0xa6
	.byte	0xe
	.long	0x5b8
	.uleb128 0x10
	.long	.LASF88
	.byte	0xd
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF89
	.byte	0xd
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x12
	.long	.LASF90
	.byte	0xd
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x12
	.long	.LASF91
	.byte	0xe
	.value	0x21f
	.byte	0xf
	.long	0x62a
	.uleb128 0x6
	.byte	0x8
	.long	0xa3
	.uleb128 0x12
	.long	.LASF92
	.byte	0xe
	.value	0x221
	.byte	0xf
	.long	0x62a
	.uleb128 0x10
	.long	.LASF93
	.byte	0xf
	.byte	0x24
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF94
	.byte	0xf
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF95
	.byte	0xf
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF96
	.byte	0xf
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF97
	.byte	0x10
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF98
	.byte	0x10
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF99
	.byte	0x10
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF100
	.byte	0x7
	.byte	0x1e
	.byte	0x12
	.long	0x685
	.uleb128 0x9
	.long	.LASF101
	.byte	0x4
	.byte	0x7
	.byte	0x1f
	.byte	0x8
	.long	0x6b8
	.uleb128 0xa
	.long	.LASF102
	.byte	0x7
	.byte	0x21
	.byte	0xf
	.long	0x691
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF103
	.byte	0x7
	.byte	0x77
	.byte	0x12
	.long	0x679
	.uleb128 0x13
	.byte	0x10
	.byte	0x7
	.byte	0xd6
	.byte	0x5
	.long	0x6f2
	.uleb128 0x14
	.long	.LASF104
	.byte	0x7
	.byte	0xd8
	.byte	0xa
	.long	0x6f2
	.uleb128 0x14
	.long	.LASF105
	.byte	0x7
	.byte	0xd9
	.byte	0xb
	.long	0x702
	.uleb128 0x14
	.long	.LASF106
	.byte	0x7
	.byte	0xda
	.byte	0xb
	.long	0x712
	.byte	0
	.uleb128 0xb
	.long	0x66d
	.long	0x702
	.uleb128 0xc
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xb
	.long	0x679
	.long	0x712
	.uleb128 0xc
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xb
	.long	0x685
	.long	0x722
	.uleb128 0xc
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x9
	.long	.LASF107
	.byte	0x10
	.byte	0x7
	.byte	0xd4
	.byte	0x8
	.long	0x73d
	.uleb128 0xa
	.long	.LASF108
	.byte	0x7
	.byte	0xdb
	.byte	0x9
	.long	0x6c4
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	0x722
	.uleb128 0x10
	.long	.LASF109
	.byte	0x7
	.byte	0xe4
	.byte	0x1e
	.long	0x73d
	.uleb128 0x10
	.long	.LASF110
	.byte	0x7
	.byte	0xe5
	.byte	0x1e
	.long	0x73d
	.uleb128 0xb
	.long	0x2d
	.long	0x76a
	.uleb128 0xc
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0x15
	.byte	0x10
	.byte	0x11
	.value	0x204
	.byte	0x3
	.long	0x782
	.uleb128 0x16
	.long	.LASF111
	.byte	0x11
	.value	0x205
	.byte	0x13
	.long	0x782
	.byte	0
	.uleb128 0xb
	.long	0x2d
	.long	0x792
	.uleb128 0xc
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x17
	.long	.LASF112
	.byte	0x10
	.byte	0x11
	.value	0x203
	.byte	0x8
	.long	0x7af
	.uleb128 0xe
	.long	.LASF113
	.byte	0x11
	.value	0x206
	.byte	0x5
	.long	0x76a
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	0x792
	.uleb128 0x10
	.long	.LASF114
	.byte	0x12
	.byte	0x52
	.byte	0x23
	.long	0x7af
	.uleb128 0x18
	.long	0xa1
	.long	0x7cf
	.uleb128 0x19
	.long	0xba
	.byte	0
	.uleb128 0x12
	.long	.LASF115
	.byte	0x13
	.value	0x151
	.byte	0x10
	.long	0x7dc
	.uleb128 0x6
	.byte	0x8
	.long	0x7c0
	.uleb128 0x18
	.long	0xa1
	.long	0x7f6
	.uleb128 0x19
	.long	0xa1
	.uleb128 0x19
	.long	0xba
	.byte	0
	.uleb128 0x12
	.long	.LASF116
	.byte	0x13
	.value	0x152
	.byte	0x10
	.long	0x803
	.uleb128 0x6
	.byte	0x8
	.long	0x7e2
	.uleb128 0x1a
	.long	0x814
	.uleb128 0x19
	.long	0xa1
	.byte	0
	.uleb128 0x12
	.long	.LASF117
	.byte	0x13
	.value	0x153
	.byte	0xf
	.long	0x821
	.uleb128 0x6
	.byte	0x8
	.long	0x809
	.uleb128 0x1b
	.long	.LASF132
	.byte	0x1
	.byte	0x1f
	.byte	0x5
	.long	0x6f
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0x994
	.uleb128 0x1c
	.string	"fp"
	.byte	0x1
	.byte	0x1f
	.byte	0x1b
	.long	0x53d
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x1c
	.string	"buf"
	.byte	0x1
	.byte	0x1f
	.byte	0x26
	.long	0x62a
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x1d
	.long	.LASF118
	.byte	0x1
	.byte	0x1f
	.byte	0x33
	.long	0x994
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x1e
	.long	.LASF119
	.byte	0x1
	.byte	0x21
	.byte	0x9
	.long	0xa3
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x1e
	.long	.LASF120
	.byte	0x1
	.byte	0x22
	.byte	0xa
	.long	0xba
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x1f
	.string	"len"
	.byte	0x1
	.byte	0x23
	.byte	0xa
	.long	0xba
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x20
	.long	.Ldebug_ranges0+0
	.long	0x983
	.uleb128 0x1e
	.long	.LASF121
	.byte	0x1
	.byte	0x2f
	.byte	0xb
	.long	0x6f
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x21
	.long	0x99a
	.quad	.LBI6
	.byte	.LVU16
	.long	.Ldebug_ranges0+0x80
	.byte	0x1
	.byte	0x31
	.byte	0xc
	.long	0x932
	.uleb128 0x22
	.long	0x9c3
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x22
	.long	0x9b7
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x22
	.long	0x9ab
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x23
	.quad	.LVL7
	.long	0x9d0
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x51
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x25
	.quad	.LVL4
	.long	0x9dc
	.uleb128 0x26
	.quad	.LVL8
	.long	0x9e8
	.long	0x95a
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x5
	.byte	0x7f
	.sleb128 0
	.byte	0x73
	.sleb128 0
	.byte	0x22
	.byte	0
	.uleb128 0x27
	.quad	.LVL11
	.long	0x96e
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7f
	.sleb128 0
	.byte	0
	.uleb128 0x23
	.quad	.LVL33
	.long	0x9f5
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7e
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x28
	.quad	.LVL25
	.uleb128 0x24
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x8
	.byte	0x80
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xba
	.uleb128 0x29
	.long	.LASF123
	.byte	0x2
	.byte	0xff
	.byte	0x1
	.long	0xa3
	.byte	0x3
	.long	0x9d0
	.uleb128 0x2a
	.string	"__s"
	.byte	0x2
	.byte	0xff
	.byte	0x19
	.long	0xa9
	.uleb128 0x2a
	.string	"__n"
	.byte	0x2
	.byte	0xff
	.byte	0x22
	.long	0x6f
	.uleb128 0x2b
	.long	.LASF122
	.byte	0x2
	.byte	0xff
	.byte	0x38
	.long	0x543
	.byte	0
	.uleb128 0x2c
	.long	.LASF123
	.long	.LASF125
	.byte	0x2
	.byte	0xf5
	.byte	0xe
	.uleb128 0x2c
	.long	.LASF124
	.long	.LASF124
	.byte	0x14
	.byte	0x15
	.byte	0x5
	.uleb128 0x2d
	.long	.LASF126
	.long	.LASF126
	.byte	0x15
	.value	0x181
	.byte	0xf
	.uleb128 0x2d
	.long	.LASF127
	.long	.LASF127
	.byte	0xa
	.value	0x2f9
	.byte	0xc
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU8
	.uleb128 .LVU8
	.uleb128 .LVU51
	.uleb128 .LVU51
	.uleb128 .LVU52
	.uleb128 .LVU52
	.uleb128 .LVU62
	.uleb128 .LVU62
	.uleb128 .LVU63
	.uleb128 .LVU63
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU81
	.uleb128 .LVU81
	.uleb128 .LVU82
	.uleb128 .LVU82
	.uleb128 .LVU91
	.uleb128 .LVU91
	.uleb128 .LVU92
	.uleb128 .LVU92
	.uleb128 0
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL2-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL17-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL18-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL22-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL24-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL31-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL32-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL37-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL38-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU8
	.uleb128 .LVU8
	.uleb128 .LVU49
	.uleb128 .LVU49
	.uleb128 .LVU52
	.uleb128 .LVU52
	.uleb128 .LVU60
	.uleb128 .LVU60
	.uleb128 .LVU63
	.uleb128 .LVU63
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU79
	.uleb128 .LVU79
	.uleb128 .LVU82
	.uleb128 .LVU82
	.uleb128 .LVU88
	.uleb128 .LVU88
	.uleb128 .LVU92
	.uleb128 .LVU92
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL2-.Ltext0
	.quad	.LVL15-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL15-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL18-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL20-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL25-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL25-1-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL29-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL32-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL35-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x54
	.byte	0x9f
	.quad	.LVL38-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU8
	.uleb128 .LVU8
	.uleb128 .LVU50
	.uleb128 .LVU50
	.uleb128 .LVU52
	.uleb128 .LVU52
	.uleb128 .LVU61
	.uleb128 .LVU61
	.uleb128 .LVU63
	.uleb128 .LVU63
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU80
	.uleb128 .LVU80
	.uleb128 .LVU82
	.uleb128 .LVU82
	.uleb128 .LVU90
	.uleb128 .LVU90
	.uleb128 .LVU92
	.uleb128 .LVU92
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL2-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL2-.Ltext0
	.quad	.LVL16-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL16-.Ltext0
	.quad	.LVL18-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL18-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL21-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL25-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL25-1-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL30-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL32-.Ltext0
	.quad	.LVL36-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL36-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL38-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 .LVU38
	.uleb128 .LVU44
	.uleb128 .LVU44
	.uleb128 .LVU45
	.uleb128 .LVU71
	.uleb128 .LVU72
.LLST3:
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL12-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x2
	.byte	0x7c
	.sleb128 0
	.quad	.LVL26-.Ltext0
	.quad	.LVL27-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 .LVU4
	.uleb128 .LVU9
	.uleb128 .LVU9
	.uleb128 .LVU28
	.uleb128 .LVU32
	.uleb128 .LVU47
	.uleb128 .LVU63
	.uleb128 .LVU71
	.uleb128 .LVU71
	.uleb128 .LVU78
	.uleb128 .LVU82
	.uleb128 .LVU87
	.uleb128 .LVU92
	.uleb128 0
.LLST4:
	.quad	.LVL1-.Ltext0
	.quad	.LVL3-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL3-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL10-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL23-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL26-.Ltext0
	.quad	.LVL28-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL32-.Ltext0
	.quad	.LVL34-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL38-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 .LVU28
	.uleb128 .LVU45
	.uleb128 .LVU52
	.uleb128 .LVU59
	.uleb128 .LVU71
	.uleb128 .LVU78
.LLST5:
	.quad	.LVL9-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL18-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	.LVL26-.Ltext0
	.quad	.LVL28-.Ltext0
	.value	0x1
	.byte	0x53
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU15
	.uleb128 .LVU21
.LLST6:
	.quad	.LVL5-.Ltext0
	.quad	.LVL7-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU16
	.uleb128 .LVU21
	.uleb128 .LVU21
	.uleb128 .LVU21
.LLST7:
	.quad	.LVL5-.Ltext0
	.quad	.LVL7-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL7-1-.Ltext0
	.quad	.LVL7-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU16
	.uleb128 .LVU21
.LLST8:
	.quad	.LVL5-.Ltext0
	.quad	.LVL7-1-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU16
	.uleb128 .LVU20
	.uleb128 .LVU20
	.uleb128 .LVU21
.LLST9:
	.quad	.LVL5-.Ltext0
	.quad	.LVL6-.Ltext0
	.value	0x6
	.byte	0x75
	.sleb128 0
	.byte	0x73
	.sleb128 0
	.byte	0x22
	.byte	0x9f
	.quad	.LVL6-.Ltext0
	.quad	.LVL7-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB5-.Ltext0
	.quad	.LBE5-.Ltext0
	.quad	.LBB12-.Ltext0
	.quad	.LBE12-.Ltext0
	.quad	.LBB13-.Ltext0
	.quad	.LBE13-.Ltext0
	.quad	.LBB14-.Ltext0
	.quad	.LBE14-.Ltext0
	.quad	.LBB15-.Ltext0
	.quad	.LBE15-.Ltext0
	.quad	.LBB16-.Ltext0
	.quad	.LBE16-.Ltext0
	.quad	.LBB17-.Ltext0
	.quad	.LBE17-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB6-.Ltext0
	.quad	.LBE6-.Ltext0
	.quad	.LBB10-.Ltext0
	.quad	.LBE10-.Ltext0
	.quad	.LBB11-.Ltext0
	.quad	.LBE11-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF43:
	.string	"_IO_read_ptr"
.LASF55:
	.string	"_chain"
.LASF33:
	.string	"sin6_addr"
.LASF108:
	.string	"__in6_u"
.LASF13:
	.string	"size_t"
.LASF61:
	.string	"_shortbuf"
.LASF112:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF49:
	.string	"_IO_buf_base"
.LASF14:
	.string	"long long unsigned int"
.LASF100:
	.string	"in_addr_t"
.LASF64:
	.string	"_codecvt"
.LASF86:
	.string	"__timezone"
.LASF15:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF35:
	.string	"sockaddr_inarp"
.LASF56:
	.string	"_fileno"
.LASF44:
	.string	"_IO_read_end"
.LASF117:
	.string	"ares_free"
.LASF105:
	.string	"__u6_addr16"
.LASF9:
	.string	"long int"
.LASF106:
	.string	"__u6_addr32"
.LASF42:
	.string	"_flags"
.LASF65:
	.string	"_wide_data"
.LASF50:
	.string	"_IO_buf_end"
.LASF59:
	.string	"_cur_column"
.LASF83:
	.string	"program_invocation_short_name"
.LASF73:
	.string	"_IO_codecvt"
.LASF21:
	.string	"sockaddr_dl"
.LASF31:
	.string	"sin6_port"
.LASF98:
	.string	"uint16_t"
.LASF81:
	.string	"_sys_errlist"
.LASF118:
	.string	"bufsize"
.LASF82:
	.string	"program_invocation_name"
.LASF58:
	.string	"_old_offset"
.LASF63:
	.string	"_offset"
.LASF110:
	.string	"in6addr_loopback"
.LASF40:
	.string	"sockaddr_x25"
.LASF36:
	.string	"sockaddr_ipx"
.LASF8:
	.string	"__uint32_t"
.LASF129:
	.string	"../deps/cares/src/ares__read_line.c"
.LASF89:
	.string	"timezone"
.LASF28:
	.string	"sin_zero"
.LASF68:
	.string	"__pad5"
.LASF72:
	.string	"_IO_marker"
.LASF75:
	.string	"stdin"
.LASF2:
	.string	"unsigned int"
.LASF102:
	.string	"s_addr"
.LASF67:
	.string	"_freeres_buf"
.LASF126:
	.string	"strlen"
.LASF119:
	.string	"newbuf"
.LASF122:
	.string	"__stream"
.LASF3:
	.string	"long unsigned int"
.LASF127:
	.string	"ferror"
.LASF47:
	.string	"_IO_write_ptr"
.LASF115:
	.string	"ares_malloc"
.LASF88:
	.string	"daylight"
.LASF78:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF27:
	.string	"sin_addr"
.LASF113:
	.string	"_S6_un"
.LASF124:
	.string	"aresx_uztosi"
.LASF130:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF51:
	.string	"_IO_save_base"
.LASF92:
	.string	"environ"
.LASF62:
	.string	"_lock"
.LASF57:
	.string	"_flags2"
.LASF69:
	.string	"_mode"
.LASF123:
	.string	"fgets"
.LASF76:
	.string	"stdout"
.LASF39:
	.string	"sockaddr_un"
.LASF25:
	.string	"sin_family"
.LASF93:
	.string	"optarg"
.LASF90:
	.string	"getdate_err"
.LASF30:
	.string	"sin6_family"
.LASF94:
	.string	"optind"
.LASF48:
	.string	"_IO_write_end"
.LASF131:
	.string	"_IO_lock_t"
.LASF109:
	.string	"in6addr_any"
.LASF41:
	.string	"_IO_FILE"
.LASF91:
	.string	"__environ"
.LASF85:
	.string	"__daylight"
.LASF77:
	.string	"stderr"
.LASF38:
	.string	"sockaddr_ns"
.LASF26:
	.string	"sin_port"
.LASF17:
	.string	"sa_family"
.LASF79:
	.string	"sys_errlist"
.LASF54:
	.string	"_markers"
.LASF34:
	.string	"sin6_scope_id"
.LASF121:
	.string	"bytestoread"
.LASF0:
	.string	"unsigned char"
.LASF37:
	.string	"sockaddr_iso"
.LASF5:
	.string	"short int"
.LASF125:
	.string	"__fgets_alias"
.LASF80:
	.string	"_sys_nerr"
.LASF60:
	.string	"_vtable_offset"
.LASF87:
	.string	"tzname"
.LASF20:
	.string	"sockaddr_ax25"
.LASF71:
	.string	"FILE"
.LASF107:
	.string	"in6_addr"
.LASF96:
	.string	"optopt"
.LASF99:
	.string	"uint32_t"
.LASF12:
	.string	"char"
.LASF32:
	.string	"sin6_flowinfo"
.LASF7:
	.string	"__uint16_t"
.LASF132:
	.string	"ares__read_line"
.LASF104:
	.string	"__u6_addr8"
.LASF95:
	.string	"opterr"
.LASF11:
	.string	"__off64_t"
.LASF120:
	.string	"offset"
.LASF45:
	.string	"_IO_read_base"
.LASF53:
	.string	"_IO_save_end"
.LASF22:
	.string	"sockaddr_eon"
.LASF19:
	.string	"sockaddr_at"
.LASF116:
	.string	"ares_realloc"
.LASF16:
	.string	"sa_family_t"
.LASF70:
	.string	"_unused2"
.LASF111:
	.string	"_S6_u8"
.LASF29:
	.string	"sockaddr_in6"
.LASF23:
	.string	"sockaddr"
.LASF24:
	.string	"sockaddr_in"
.LASF97:
	.string	"uint8_t"
.LASF52:
	.string	"_IO_backup_base"
.LASF18:
	.string	"sa_data"
.LASF66:
	.string	"_freeres_list"
.LASF128:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF74:
	.string	"_IO_wide_data"
.LASF114:
	.string	"ares_in6addr_any"
.LASF84:
	.string	"__tzname"
.LASF46:
	.string	"_IO_write_base"
.LASF103:
	.string	"in_port_t"
.LASF101:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
