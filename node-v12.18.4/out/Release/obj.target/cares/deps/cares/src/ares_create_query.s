	.file	"ares_create_query.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares_create_query
	.type	ares_create_query, @function
ares_create_query:
.LVL0:
.LFB87:
	.file 1 "../deps/cares/src/ares_create_query.c"
	.loc 1 90 1 view -0
	.cfi_startproc
	.loc 1 90 1 is_stmt 0 view .LVU1
	endbr64
	.loc 1 91 3 is_stmt 1 view .LVU2
	.loc 1 92 3 view .LVU3
	.loc 1 93 3 view .LVU4
	.loc 1 94 3 view .LVU5
	.loc 1 95 3 view .LVU6
	.loc 1 98 3 view .LVU7
	.loc 1 90 1 is_stmt 0 view .LVU8
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	.cfi_offset 15, -24
	movl	%r8d, %r15d
	pushq	%r14
	.cfi_offset 14, -32
	movl	%edx, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	movq	%r9, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%ecx, %ebx
	subq	$24, %rsp
	.loc 1 98 12 view .LVU9
	movq	16(%rbp), %rax
	.loc 1 90 1 view .LVU10
	movl	%esi, -56(%rbp)
	.loc 1 98 12 view .LVU11
	movl	$0, (%rax)
	.loc 1 99 3 is_stmt 1 view .LVU12
	.loc 1 99 9 is_stmt 0 view .LVU13
	movq	$0, (%r9)
	.loc 1 102 3 is_stmt 1 view .LVU14
	.loc 1 102 7 is_stmt 0 view .LVU15
	call	ares__is_onion_domain@PLT
.LVL1:
	.loc 1 103 12 view .LVU16
	movl	$4, %r8d
	.loc 1 102 6 view .LVU17
	testl	%eax, %eax
	movl	%eax, -52(%rbp)
	jne	.L1
	.loc 1 109 3 is_stmt 1 view .LVU18
	.loc 1 109 9 is_stmt 0 view .LVU19
	movq	%r13, %rdi
	call	strlen@PLT
.LVL2:
	.loc 1 110 24 view .LVU20
	cmpl	$1, 24(%rbp)
	.loc 1 109 9 view .LVU21
	movq	%rax, %r9
	.loc 1 110 24 view .LVU22
	sbbq	%rax, %rax
	notq	%rax
	andl	$11, %eax
.LVL3:
	.loc 1 111 3 is_stmt 1 view .LVU23
	.loc 1 109 7 is_stmt 0 view .LVU24
	leaq	18(%rax,%r9), %rdi
.LVL4:
	.loc 1 111 9 view .LVU25
	call	*ares_malloc(%rip)
.LVL5:
	.loc 1 111 9 view .LVU26
	movq	%rax, %r9
.LVL6:
	.loc 1 112 3 is_stmt 1 view .LVU27
	.loc 1 112 6 is_stmt 0 view .LVU28
	testq	%rax, %rax
	je	.L27
	.loc 1 116 3 is_stmt 1 view .LVU29
.LVL7:
	.loc 1 117 3 view .LVU30
.LBB6:
.LBI6:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/string_fortified.h"
	.loc 2 59 42 view .LVU31
.LBB7:
	.loc 2 71 3 view .LVU32
.LBE7:
.LBE6:
	.loc 1 118 12 is_stmt 0 view .LVU33
	rolw	$8, %bx
	.loc 1 120 6 view .LVU34
	testl	%r15d, %r15d
.LBB10:
.LBB8:
	.loc 2 71 10 view .LVU35
	movq	$0, (%rax)
.LBE8:
.LBE10:
	.loc 1 120 6 view .LVU36
	movl	-52(%rbp), %r8d
.LBB11:
.LBB9:
	.loc 2 71 10 view .LVU37
	movl	$0, 8(%rax)
.LVL8:
	.loc 2 71 10 view .LVU38
.LBE9:
.LBE11:
	.loc 1 118 3 is_stmt 1 view .LVU39
	.loc 1 118 12 is_stmt 0 view .LVU40
	movw	%bx, (%rax)
	.loc 1 119 3 is_stmt 1 view .LVU41
	.loc 1 120 3 view .LVU42
	.loc 1 120 6 is_stmt 0 view .LVU43
	je	.L4
	.loc 1 121 5 is_stmt 1 view .LVU44
	.loc 1 121 13 is_stmt 0 view .LVU45
	movb	$1, 2(%rax)
.L4:
	.loc 1 126 3 is_stmt 1 view .LVU46
	.loc 1 128 6 is_stmt 0 view .LVU47
	movl	24(%rbp), %r11d
	.loc 1 126 18 view .LVU48
	movl	$256, %r10d
	movw	%r10w, 4(%r9)
	.loc 1 128 3 is_stmt 1 view .LVU49
	.loc 1 128 6 is_stmt 0 view .LVU50
	testl	%r11d, %r11d
	je	.L5
	.loc 1 129 7 is_stmt 1 view .LVU51
	.loc 1 129 23 is_stmt 0 view .LVU52
	movl	$256, %edi
	movw	%di, 10(%r9)
.L5:
	.loc 1 133 3 is_stmt 1 view .LVU53
	.loc 1 133 7 is_stmt 0 view .LVU54
	cmpb	$46, 0(%r13)
	je	.L67
.LVL9:
.L7:
	.loc 1 137 3 is_stmt 1 view .LVU55
	.loc 1 138 10 is_stmt 0 view .LVU56
	movzbl	0(%r13), %edx
	.loc 1 137 5 view .LVU57
	leaq	12(%r9), %rdi
.LVL10:
	.loc 1 138 3 is_stmt 1 view .LVU58
	.loc 1 138 9 view .LVU59
	testb	%dl, %dl
	je	.L8
	.loc 1 140 7 view .LVU60
	.loc 1 140 10 is_stmt 0 view .LVU61
	cmpb	$46, %dl
	je	.L64
	.p2align 4,,10
	.p2align 3
.L71:
.LVL11:
	.loc 1 147 22 is_stmt 1 view .LVU62
	.loc 1 147 7 is_stmt 0 view .LVU63
	movq	%r13, %rcx
	.loc 1 146 11 view .LVU64
	xorl	%eax, %eax
	jmp	.L10
.LVL12:
	.p2align 4,,10
	.p2align 3
.L29:
	.loc 1 146 11 view .LVU65
	movq	%r11, %rcx
.LVL13:
	.loc 1 146 11 view .LVU66
	movl	%esi, %edx
.LVL14:
.L12:
	.loc 1 151 11 is_stmt 1 discriminator 2 view .LVU67
	.loc 1 151 14 is_stmt 0 discriminator 2 view .LVU68
	addq	$1, %rax
.LVL15:
	.loc 1 147 39 is_stmt 1 discriminator 2 view .LVU69
	.loc 1 147 22 discriminator 2 view .LVU70
	.loc 1 147 7 is_stmt 0 discriminator 2 view .LVU71
	testb	%dl, %dl
	je	.L24
	cmpb	$46, %dl
	je	.L24
.LVL16:
.L10:
	.loc 1 149 11 is_stmt 1 view .LVU72
	movzbl	1(%rcx), %esi
	leaq	1(%rcx), %r11
	.loc 1 149 14 is_stmt 0 view .LVU73
	cmpb	$92, %dl
	jne	.L29
	.loc 1 149 26 discriminator 1 view .LVU74
	testb	%sil, %sil
	jne	.L68
.LVL17:
	.loc 1 151 11 is_stmt 1 view .LVU75
	.loc 1 151 14 is_stmt 0 view .LVU76
	addq	$1, %rax
.LVL18:
	.loc 1 147 39 is_stmt 1 view .LVU77
	.loc 1 147 22 view .LVU78
.L24:
	.loc 1 153 7 view .LVU79
	.loc 1 153 10 is_stmt 0 view .LVU80
	cmpq	$63, %rax
	ja	.L64
	.loc 1 159 7 is_stmt 1 view .LVU81
	.loc 1 159 12 is_stmt 0 view .LVU82
	movb	%al, (%rdi)
	.loc 1 160 22 view .LVU83
	movzbl	0(%r13), %edx
	.loc 1 159 9 view .LVU84
	leaq	1(%rdi), %rsi
.LVL19:
	.loc 1 160 7 is_stmt 1 view .LVU85
	.loc 1 160 22 view .LVU86
	.loc 1 160 7 is_stmt 0 view .LVU87
	movq	%r13, %rcx
	.loc 1 159 9 view .LVU88
	movq	%rsi, %rdi
	.loc 1 160 7 view .LVU89
	cmpb	$46, %dl
	je	.L16
.LVL20:
	.loc 1 160 7 view .LVU90
	testb	%dl, %dl
	jne	.L18
	jmp	.L16
.LVL21:
	.p2align 4,,10
	.p2align 3
.L32:
	.loc 1 160 7 view .LVU91
	movq	%rsi, %rax
	movq	%rcx, %rsi
	.loc 1 164 16 view .LVU92
	movb	%dl, (%rdi)
	.loc 1 164 13 view .LVU93
	addq	$1, %rdi
.LVL22:
	.loc 1 160 22 view .LVU94
	movzbl	1(%rsi), %edx
	movq	%rax, %rcx
.LVL23:
	.loc 1 164 11 is_stmt 1 view .LVU95
	.loc 1 160 39 view .LVU96
	.loc 1 160 22 view .LVU97
	.loc 1 160 7 is_stmt 0 view .LVU98
	testb	%dl, %dl
	je	.L8
.LVL24:
.L69:
	.loc 1 160 7 discriminator 2 view .LVU99
	cmpb	$46, %dl
	je	.L16
.L18:
.LVL25:
	.loc 1 162 11 is_stmt 1 view .LVU100
	leaq	1(%rcx), %rsi
	.loc 1 162 14 is_stmt 0 view .LVU101
	cmpb	$92, %dl
	jne	.L32
	.loc 1 162 29 discriminator 1 view .LVU102
	movzbl	1(%rcx), %eax
	.loc 1 162 26 discriminator 1 view .LVU103
	testb	%al, %al
	je	.L32
	.loc 1 163 13 is_stmt 1 view .LVU104
.LVL26:
	.loc 1 162 26 is_stmt 0 view .LVU105
	movl	%eax, %edx
	.loc 1 164 11 is_stmt 1 view .LVU106
	.loc 1 164 13 is_stmt 0 view .LVU107
	addq	$1, %rdi
.LVL27:
	.loc 1 164 13 view .LVU108
	addq	$2, %rcx
	.loc 1 164 16 view .LVU109
	movb	%dl, -1(%rdi)
	.loc 1 160 39 is_stmt 1 view .LVU110
.LVL28:
	.loc 1 160 22 view .LVU111
	movzbl	1(%rsi), %edx
	.loc 1 160 7 is_stmt 0 view .LVU112
	testb	%dl, %dl
	jne	.L69
.LVL29:
	.p2align 4,,10
	.p2align 3
.L8:
	.loc 1 174 3 is_stmt 1 view .LVU113
	.loc 1 178 18 is_stmt 0 view .LVU114
	movzwl	-56(%rbp), %r13d
.LVL30:
	.loc 1 180 5 view .LVU115
	leaq	5(%rdi), %rdx
	.loc 1 181 6 view .LVU116
	movl	24(%rbp), %esi
	.loc 1 174 8 view .LVU117
	movb	$0, (%rdi)
	.loc 1 177 3 is_stmt 1 view .LVU118
	.loc 1 177 12 is_stmt 0 view .LVU119
	rolw	$8, %r14w
.LVL31:
	.loc 1 189 15 view .LVU120
	movq	%rdx, %rax
	.loc 1 195 16 view .LVU121
	movl	$271, %ecx
	.loc 1 178 18 view .LVU122
	rolw	$8, %r13w
	.loc 1 177 12 view .LVU123
	movw	%r14w, 1(%rdi)
	.loc 1 178 3 is_stmt 1 view .LVU124
	.loc 1 189 15 is_stmt 0 view .LVU125
	subq	%r9, %rax
	.loc 1 178 18 view .LVU126
	movw	%r13w, 3(%rdi)
	.loc 1 180 3 is_stmt 1 view .LVU127
.LVL32:
	.loc 1 181 3 view .LVU128
	.loc 1 195 3 view .LVU129
	.loc 1 181 6 is_stmt 0 view .LVU130
	testl	%esi, %esi
	jne	.L70
.LVL33:
.L23:
	.loc 1 195 6 discriminator 4 view .LVU131
	cmpq	%rax, %rcx
	jb	.L64
	.loc 1 202 3 is_stmt 1 view .LVU132
	.loc 1 202 14 is_stmt 0 view .LVU133
	movq	16(%rbp), %rbx
	movl	%eax, (%rbx)
	.loc 1 203 3 is_stmt 1 view .LVU134
	.loc 1 203 9 is_stmt 0 view .LVU135
	movq	%r9, (%r12)
	.loc 1 205 3 is_stmt 1 view .LVU136
.LVL34:
.L1:
	.loc 1 206 1 is_stmt 0 view .LVU137
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
.LVL35:
	.loc 1 206 1 view .LVU138
	popq	%r13
	popq	%r14
	popq	%r15
.LVL36:
	.loc 1 206 1 view .LVU139
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.LVL37:
	.p2align 4,,10
	.p2align 3
.L68:
	.cfi_restore_state
	.loc 1 150 13 is_stmt 1 view .LVU140
	.loc 1 150 13 is_stmt 0 view .LVU141
	movzbl	2(%rcx), %edx
	addq	$2, %rcx
.LVL38:
	.loc 1 150 13 view .LVU142
	jmp	.L12
.LVL39:
	.p2align 4,,10
	.p2align 3
.L16:
	.loc 1 168 7 is_stmt 1 view .LVU143
	.loc 1 168 10 is_stmt 0 view .LVU144
	testb	%dl, %dl
	je	.L8
	.loc 1 170 7 is_stmt 1 view .LVU145
	.loc 1 138 10 is_stmt 0 view .LVU146
	movzbl	1(%rcx), %edx
	.loc 1 170 12 view .LVU147
	leaq	1(%rcx), %r13
.LVL40:
	.loc 1 138 9 is_stmt 1 view .LVU148
	testb	%dl, %dl
	je	.L8
	.loc 1 140 7 view .LVU149
	.loc 1 140 10 is_stmt 0 view .LVU150
	cmpb	$46, %dl
	jne	.L71
.LVL41:
.L64:
	.loc 1 197 5 is_stmt 1 view .LVU151
	movq	%r9, %rdi
	call	*ares_free(%rip)
.LVL42:
	.loc 1 198 5 view .LVU152
	.loc 1 198 12 is_stmt 0 view .LVU153
	movl	$8, %r8d
	jmp	.L1
.LVL43:
.L67:
	.loc 1 133 7 view .LVU154
	movzbl	1(%r13), %eax
.LVL44:
	.loc 1 134 9 view .LVU155
	cmpl	$1, %eax
	adcq	$0, %r13
.LVL45:
	.loc 1 134 9 view .LVU156
	jmp	.L7
.LVL46:
.L27:
	.loc 1 113 12 view .LVU157
	movl	$15, %r8d
	jmp	.L1
.LVL47:
.L70:
	.loc 1 183 7 is_stmt 1 view .LVU158
.LBB12:
.LBI12:
	.loc 2 59 42 view .LVU159
.LBB13:
	.loc 2 71 3 view .LVU160
	.loc 2 71 10 is_stmt 0 view .LVU161
	xorl	%eax, %eax
.LVL48:
	.loc 2 71 10 view .LVU162
.LBE13:
.LBE12:
	.loc 1 185 16 view .LVU163
	movl	$10496, %edx
.LVL49:
.LBB17:
.LBB14:
	.loc 2 71 10 view .LVU164
	movb	$0, 15(%rdi)
.LVL50:
	.loc 2 71 10 view .LVU165
.LBE14:
.LBE17:
	.loc 1 184 7 is_stmt 1 view .LVU166
	.loc 1 185 7 view .LVU167
	.loc 1 195 16 is_stmt 0 view .LVU168
	movl	$282, %ecx
.LBB18:
.LBB15:
	.loc 2 71 10 view .LVU169
	movw	%ax, 13(%rdi)
.LBE15:
.LBE18:
	.loc 1 186 22 view .LVU170
	movzwl	24(%rbp), %eax
.LBB19:
.LBB16:
	.loc 2 71 10 view .LVU171
	movq	$0, 5(%rdi)
.LBE16:
.LBE19:
	.loc 1 186 22 view .LVU172
	rolw	$8, %ax
	.loc 1 185 16 view .LVU173
	movw	%dx, 6(%rdi)
	.loc 1 186 7 is_stmt 1 view .LVU174
	.loc 1 186 22 is_stmt 0 view .LVU175
	movw	%ax, 8(%rdi)
	.loc 1 187 7 is_stmt 1 view .LVU176
.LVL51:
	.loc 1 189 3 view .LVU177
	.loc 1 187 9 is_stmt 0 view .LVU178
	leaq	16(%rdi), %rax
.LVL52:
	.loc 1 189 15 view .LVU179
	subq	%r9, %rax
.LVL53:
	.loc 1 195 3 is_stmt 1 view .LVU180
	jmp	.L23
	.cfi_endproc
.LFE87:
	.size	ares_create_query, .-ares_create_query
.Letext0:
	.file 3 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 4 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 7 "/usr/include/netinet/in.h"
	.file 8 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 10 "/usr/include/stdio.h"
	.file 11 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 12 "/usr/include/errno.h"
	.file 13 "/usr/include/time.h"
	.file 14 "/usr/include/unistd.h"
	.file 15 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 17 "/usr/include/signal.h"
	.file 18 "/usr/include/arpa/nameser.h"
	.file 19 "../deps/cares/include/ares.h"
	.file 20 "../deps/cares/src/ares_ipv6.h"
	.file 21 "../deps/cares/src/ares_private.h"
	.file 22 "/usr/include/string.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0xd07
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF229
	.byte	0x1
	.long	.LASF230
	.long	.LASF231
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x3
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x3
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x3
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x3
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x3
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x6
	.byte	0x8
	.long	0xa9
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF12
	.uleb128 0x7
	.long	0xa9
	.uleb128 0x3
	.long	.LASF13
	.byte	0x4
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF14
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF15
	.uleb128 0x3
	.long	.LASF16
	.byte	0x5
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF23
	.byte	0x10
	.byte	0x6
	.byte	0xb2
	.byte	0x8
	.long	0x103
	.uleb128 0x9
	.long	.LASF17
	.byte	0x6
	.byte	0xb4
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF18
	.byte	0x6
	.byte	0xb5
	.byte	0xa
	.long	0x108
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0xdb
	.uleb128 0xa
	.long	0xa9
	.long	0x118
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xdb
	.uleb128 0xc
	.long	0x118
	.uleb128 0xd
	.long	.LASF19
	.uleb128 0x7
	.long	0x123
	.uleb128 0x6
	.byte	0x8
	.long	0x123
	.uleb128 0xc
	.long	0x12d
	.uleb128 0xd
	.long	.LASF20
	.uleb128 0x7
	.long	0x138
	.uleb128 0x6
	.byte	0x8
	.long	0x138
	.uleb128 0xc
	.long	0x142
	.uleb128 0xd
	.long	.LASF21
	.uleb128 0x7
	.long	0x14d
	.uleb128 0x6
	.byte	0x8
	.long	0x14d
	.uleb128 0xc
	.long	0x157
	.uleb128 0xd
	.long	.LASF22
	.uleb128 0x7
	.long	0x162
	.uleb128 0x6
	.byte	0x8
	.long	0x162
	.uleb128 0xc
	.long	0x16c
	.uleb128 0x8
	.long	.LASF24
	.byte	0x10
	.byte	0x7
	.byte	0xee
	.byte	0x8
	.long	0x1b9
	.uleb128 0x9
	.long	.LASF25
	.byte	0x7
	.byte	0xf0
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0x9
	.long	.LASF26
	.byte	0x7
	.byte	0xf1
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0x9
	.long	.LASF27
	.byte	0x7
	.byte	0xf2
	.byte	0x14
	.long	0x693
	.byte	0x4
	.uleb128 0x9
	.long	.LASF28
	.byte	0x7
	.byte	0xf5
	.byte	0x13
	.long	0x750
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	0x177
	.uleb128 0x6
	.byte	0x8
	.long	0x177
	.uleb128 0xc
	.long	0x1be
	.uleb128 0x8
	.long	.LASF29
	.byte	0x1c
	.byte	0x7
	.byte	0xfd
	.byte	0x8
	.long	0x21c
	.uleb128 0x9
	.long	.LASF30
	.byte	0x7
	.byte	0xff
	.byte	0x11
	.long	0xcf
	.byte	0
	.uleb128 0xe
	.long	.LASF31
	.byte	0x7
	.value	0x100
	.byte	0xf
	.long	0x6ae
	.byte	0x2
	.uleb128 0xe
	.long	.LASF32
	.byte	0x7
	.value	0x101
	.byte	0xe
	.long	0x67b
	.byte	0x4
	.uleb128 0xe
	.long	.LASF33
	.byte	0x7
	.value	0x102
	.byte	0x15
	.long	0x718
	.byte	0x8
	.uleb128 0xe
	.long	.LASF34
	.byte	0x7
	.value	0x103
	.byte	0xe
	.long	0x67b
	.byte	0x18
	.byte	0
	.uleb128 0x7
	.long	0x1c9
	.uleb128 0x6
	.byte	0x8
	.long	0x1c9
	.uleb128 0xc
	.long	0x221
	.uleb128 0xd
	.long	.LASF35
	.uleb128 0x7
	.long	0x22c
	.uleb128 0x6
	.byte	0x8
	.long	0x22c
	.uleb128 0xc
	.long	0x236
	.uleb128 0xd
	.long	.LASF36
	.uleb128 0x7
	.long	0x241
	.uleb128 0x6
	.byte	0x8
	.long	0x241
	.uleb128 0xc
	.long	0x24b
	.uleb128 0xd
	.long	.LASF37
	.uleb128 0x7
	.long	0x256
	.uleb128 0x6
	.byte	0x8
	.long	0x256
	.uleb128 0xc
	.long	0x260
	.uleb128 0xd
	.long	.LASF38
	.uleb128 0x7
	.long	0x26b
	.uleb128 0x6
	.byte	0x8
	.long	0x26b
	.uleb128 0xc
	.long	0x275
	.uleb128 0xd
	.long	.LASF39
	.uleb128 0x7
	.long	0x280
	.uleb128 0x6
	.byte	0x8
	.long	0x280
	.uleb128 0xc
	.long	0x28a
	.uleb128 0xd
	.long	.LASF40
	.uleb128 0x7
	.long	0x295
	.uleb128 0x6
	.byte	0x8
	.long	0x295
	.uleb128 0xc
	.long	0x29f
	.uleb128 0x6
	.byte	0x8
	.long	0x103
	.uleb128 0xc
	.long	0x2aa
	.uleb128 0x6
	.byte	0x8
	.long	0x128
	.uleb128 0xc
	.long	0x2b5
	.uleb128 0x6
	.byte	0x8
	.long	0x13d
	.uleb128 0xc
	.long	0x2c0
	.uleb128 0x6
	.byte	0x8
	.long	0x152
	.uleb128 0xc
	.long	0x2cb
	.uleb128 0x6
	.byte	0x8
	.long	0x167
	.uleb128 0xc
	.long	0x2d6
	.uleb128 0x6
	.byte	0x8
	.long	0x1b9
	.uleb128 0xc
	.long	0x2e1
	.uleb128 0x6
	.byte	0x8
	.long	0x21c
	.uleb128 0xc
	.long	0x2ec
	.uleb128 0x6
	.byte	0x8
	.long	0x231
	.uleb128 0xc
	.long	0x2f7
	.uleb128 0x6
	.byte	0x8
	.long	0x246
	.uleb128 0xc
	.long	0x302
	.uleb128 0x6
	.byte	0x8
	.long	0x25b
	.uleb128 0xc
	.long	0x30d
	.uleb128 0x6
	.byte	0x8
	.long	0x270
	.uleb128 0xc
	.long	0x318
	.uleb128 0x6
	.byte	0x8
	.long	0x285
	.uleb128 0xc
	.long	0x323
	.uleb128 0x6
	.byte	0x8
	.long	0x29a
	.uleb128 0xc
	.long	0x32e
	.uleb128 0xa
	.long	0xa9
	.long	0x349
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF41
	.byte	0xd8
	.byte	0x8
	.byte	0x31
	.byte	0x8
	.long	0x4d0
	.uleb128 0x9
	.long	.LASF42
	.byte	0x8
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0x9
	.long	.LASF43
	.byte	0x8
	.byte	0x36
	.byte	0x9
	.long	0xa3
	.byte	0x8
	.uleb128 0x9
	.long	.LASF44
	.byte	0x8
	.byte	0x37
	.byte	0x9
	.long	0xa3
	.byte	0x10
	.uleb128 0x9
	.long	.LASF45
	.byte	0x8
	.byte	0x38
	.byte	0x9
	.long	0xa3
	.byte	0x18
	.uleb128 0x9
	.long	.LASF46
	.byte	0x8
	.byte	0x39
	.byte	0x9
	.long	0xa3
	.byte	0x20
	.uleb128 0x9
	.long	.LASF47
	.byte	0x8
	.byte	0x3a
	.byte	0x9
	.long	0xa3
	.byte	0x28
	.uleb128 0x9
	.long	.LASF48
	.byte	0x8
	.byte	0x3b
	.byte	0x9
	.long	0xa3
	.byte	0x30
	.uleb128 0x9
	.long	.LASF49
	.byte	0x8
	.byte	0x3c
	.byte	0x9
	.long	0xa3
	.byte	0x38
	.uleb128 0x9
	.long	.LASF50
	.byte	0x8
	.byte	0x3d
	.byte	0x9
	.long	0xa3
	.byte	0x40
	.uleb128 0x9
	.long	.LASF51
	.byte	0x8
	.byte	0x40
	.byte	0x9
	.long	0xa3
	.byte	0x48
	.uleb128 0x9
	.long	.LASF52
	.byte	0x8
	.byte	0x41
	.byte	0x9
	.long	0xa3
	.byte	0x50
	.uleb128 0x9
	.long	.LASF53
	.byte	0x8
	.byte	0x42
	.byte	0x9
	.long	0xa3
	.byte	0x58
	.uleb128 0x9
	.long	.LASF54
	.byte	0x8
	.byte	0x44
	.byte	0x16
	.long	0x4e9
	.byte	0x60
	.uleb128 0x9
	.long	.LASF55
	.byte	0x8
	.byte	0x46
	.byte	0x14
	.long	0x4ef
	.byte	0x68
	.uleb128 0x9
	.long	.LASF56
	.byte	0x8
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0x9
	.long	.LASF57
	.byte	0x8
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0x9
	.long	.LASF58
	.byte	0x8
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0x9
	.long	.LASF59
	.byte	0x8
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF60
	.byte	0x8
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF61
	.byte	0x8
	.byte	0x4f
	.byte	0x8
	.long	0x339
	.byte	0x83
	.uleb128 0x9
	.long	.LASF62
	.byte	0x8
	.byte	0x51
	.byte	0xf
	.long	0x4f5
	.byte	0x88
	.uleb128 0x9
	.long	.LASF63
	.byte	0x8
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0x9
	.long	.LASF64
	.byte	0x8
	.byte	0x5b
	.byte	0x17
	.long	0x500
	.byte	0x98
	.uleb128 0x9
	.long	.LASF65
	.byte	0x8
	.byte	0x5c
	.byte	0x19
	.long	0x50b
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF66
	.byte	0x8
	.byte	0x5d
	.byte	0x14
	.long	0x4ef
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF67
	.byte	0x8
	.byte	0x5e
	.byte	0x9
	.long	0xa1
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF68
	.byte	0x8
	.byte	0x5f
	.byte	0xa
	.long	0xb5
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF69
	.byte	0x8
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF70
	.byte	0x8
	.byte	0x62
	.byte	0x8
	.long	0x511
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF71
	.byte	0x9
	.byte	0x7
	.byte	0x19
	.long	0x349
	.uleb128 0xf
	.long	.LASF232
	.byte	0x8
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF72
	.uleb128 0x6
	.byte	0x8
	.long	0x4e4
	.uleb128 0x6
	.byte	0x8
	.long	0x349
	.uleb128 0x6
	.byte	0x8
	.long	0x4dc
	.uleb128 0xd
	.long	.LASF73
	.uleb128 0x6
	.byte	0x8
	.long	0x4fb
	.uleb128 0xd
	.long	.LASF74
	.uleb128 0x6
	.byte	0x8
	.long	0x506
	.uleb128 0xa
	.long	0xa9
	.long	0x521
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xb0
	.uleb128 0x7
	.long	0x521
	.uleb128 0x10
	.long	.LASF75
	.byte	0xa
	.byte	0x89
	.byte	0xe
	.long	0x538
	.uleb128 0x6
	.byte	0x8
	.long	0x4d0
	.uleb128 0x10
	.long	.LASF76
	.byte	0xa
	.byte	0x8a
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF77
	.byte	0xa
	.byte	0x8b
	.byte	0xe
	.long	0x538
	.uleb128 0x10
	.long	.LASF78
	.byte	0xb
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xa
	.long	0x527
	.long	0x56d
	.uleb128 0x11
	.byte	0
	.uleb128 0x7
	.long	0x562
	.uleb128 0x10
	.long	.LASF79
	.byte	0xb
	.byte	0x1b
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF80
	.byte	0xb
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF81
	.byte	0xb
	.byte	0x1f
	.byte	0x1a
	.long	0x56d
	.uleb128 0x10
	.long	.LASF82
	.byte	0xc
	.byte	0x2d
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF83
	.byte	0xc
	.byte	0x2e
	.byte	0xe
	.long	0xa3
	.uleb128 0xa
	.long	0xa3
	.long	0x5be
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF84
	.byte	0xd
	.byte	0x9f
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF85
	.byte	0xd
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF86
	.byte	0xd
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF87
	.byte	0xd
	.byte	0xa6
	.byte	0xe
	.long	0x5ae
	.uleb128 0x10
	.long	.LASF88
	.byte	0xd
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF89
	.byte	0xd
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x12
	.long	.LASF90
	.byte	0xd
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x12
	.long	.LASF91
	.byte	0xe
	.value	0x21f
	.byte	0xf
	.long	0x620
	.uleb128 0x6
	.byte	0x8
	.long	0xa3
	.uleb128 0x12
	.long	.LASF92
	.byte	0xe
	.value	0x221
	.byte	0xf
	.long	0x620
	.uleb128 0x10
	.long	.LASF93
	.byte	0xf
	.byte	0x24
	.byte	0xe
	.long	0xa3
	.uleb128 0x10
	.long	.LASF94
	.byte	0xf
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF95
	.byte	0xf
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF96
	.byte	0xf
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF97
	.byte	0x10
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF98
	.byte	0x10
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF99
	.byte	0x10
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF100
	.byte	0x7
	.byte	0x1e
	.byte	0x12
	.long	0x67b
	.uleb128 0x8
	.long	.LASF101
	.byte	0x4
	.byte	0x7
	.byte	0x1f
	.byte	0x8
	.long	0x6ae
	.uleb128 0x9
	.long	.LASF102
	.byte	0x7
	.byte	0x21
	.byte	0xf
	.long	0x687
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF103
	.byte	0x7
	.byte	0x77
	.byte	0x12
	.long	0x66f
	.uleb128 0x13
	.byte	0x10
	.byte	0x7
	.byte	0xd6
	.byte	0x5
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF104
	.byte	0x7
	.byte	0xd8
	.byte	0xa
	.long	0x6e8
	.uleb128 0x14
	.long	.LASF105
	.byte	0x7
	.byte	0xd9
	.byte	0xb
	.long	0x6f8
	.uleb128 0x14
	.long	.LASF106
	.byte	0x7
	.byte	0xda
	.byte	0xb
	.long	0x708
	.byte	0
	.uleb128 0xa
	.long	0x663
	.long	0x6f8
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x66f
	.long	0x708
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x67b
	.long	0x718
	.uleb128 0xb
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.long	.LASF107
	.byte	0x10
	.byte	0x7
	.byte	0xd4
	.byte	0x8
	.long	0x733
	.uleb128 0x9
	.long	.LASF108
	.byte	0x7
	.byte	0xdb
	.byte	0x9
	.long	0x6ba
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x718
	.uleb128 0x10
	.long	.LASF109
	.byte	0x7
	.byte	0xe4
	.byte	0x1e
	.long	0x733
	.uleb128 0x10
	.long	.LASF110
	.byte	0x7
	.byte	0xe5
	.byte	0x1e
	.long	0x733
	.uleb128 0xa
	.long	0x2d
	.long	0x760
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x527
	.long	0x770
	.uleb128 0xb
	.long	0x42
	.byte	0x40
	.byte	0
	.uleb128 0x7
	.long	0x760
	.uleb128 0x12
	.long	.LASF111
	.byte	0x11
	.value	0x11e
	.byte	0x1a
	.long	0x770
	.uleb128 0x12
	.long	.LASF112
	.byte	0x11
	.value	0x11f
	.byte	0x1a
	.long	0x770
	.uleb128 0x8
	.long	.LASF113
	.byte	0x8
	.byte	0x12
	.byte	0x67
	.byte	0x8
	.long	0x7b7
	.uleb128 0x9
	.long	.LASF114
	.byte	0x12
	.byte	0x67
	.byte	0x1b
	.long	0x6f
	.byte	0
	.uleb128 0x9
	.long	.LASF115
	.byte	0x12
	.byte	0x67
	.byte	0x21
	.long	0x6f
	.byte	0x4
	.byte	0
	.uleb128 0x7
	.long	0x78f
	.uleb128 0xa
	.long	0x7b7
	.long	0x7c7
	.uleb128 0x11
	.byte	0
	.uleb128 0x7
	.long	0x7bc
	.uleb128 0x10
	.long	.LASF113
	.byte	0x12
	.byte	0x68
	.byte	0x22
	.long	0x7c7
	.uleb128 0x15
	.long	.LASF122
	.byte	0x7
	.byte	0x4
	.long	0x3b
	.byte	0x12
	.byte	0x9c
	.byte	0xe
	.long	0x80f
	.uleb128 0x16
	.long	.LASF116
	.byte	0
	.uleb128 0x16
	.long	.LASF117
	.byte	0x1
	.uleb128 0x16
	.long	.LASF118
	.byte	0x2
	.uleb128 0x16
	.long	.LASF119
	.byte	0x4
	.uleb128 0x16
	.long	.LASF120
	.byte	0x5
	.uleb128 0x16
	.long	.LASF121
	.byte	0x6
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x2d
	.uleb128 0x15
	.long	.LASF123
	.byte	0x7
	.byte	0x4
	.long	0x3b
	.byte	0x12
	.byte	0xe7
	.byte	0xe
	.long	0xa34
	.uleb128 0x16
	.long	.LASF124
	.byte	0
	.uleb128 0x16
	.long	.LASF125
	.byte	0x1
	.uleb128 0x16
	.long	.LASF126
	.byte	0x2
	.uleb128 0x16
	.long	.LASF127
	.byte	0x3
	.uleb128 0x16
	.long	.LASF128
	.byte	0x4
	.uleb128 0x16
	.long	.LASF129
	.byte	0x5
	.uleb128 0x16
	.long	.LASF130
	.byte	0x6
	.uleb128 0x16
	.long	.LASF131
	.byte	0x7
	.uleb128 0x16
	.long	.LASF132
	.byte	0x8
	.uleb128 0x16
	.long	.LASF133
	.byte	0x9
	.uleb128 0x16
	.long	.LASF134
	.byte	0xa
	.uleb128 0x16
	.long	.LASF135
	.byte	0xb
	.uleb128 0x16
	.long	.LASF136
	.byte	0xc
	.uleb128 0x16
	.long	.LASF137
	.byte	0xd
	.uleb128 0x16
	.long	.LASF138
	.byte	0xe
	.uleb128 0x16
	.long	.LASF139
	.byte	0xf
	.uleb128 0x16
	.long	.LASF140
	.byte	0x10
	.uleb128 0x16
	.long	.LASF141
	.byte	0x11
	.uleb128 0x16
	.long	.LASF142
	.byte	0x12
	.uleb128 0x16
	.long	.LASF143
	.byte	0x13
	.uleb128 0x16
	.long	.LASF144
	.byte	0x14
	.uleb128 0x16
	.long	.LASF145
	.byte	0x15
	.uleb128 0x16
	.long	.LASF146
	.byte	0x16
	.uleb128 0x16
	.long	.LASF147
	.byte	0x17
	.uleb128 0x16
	.long	.LASF148
	.byte	0x18
	.uleb128 0x16
	.long	.LASF149
	.byte	0x19
	.uleb128 0x16
	.long	.LASF150
	.byte	0x1a
	.uleb128 0x16
	.long	.LASF151
	.byte	0x1b
	.uleb128 0x16
	.long	.LASF152
	.byte	0x1c
	.uleb128 0x16
	.long	.LASF153
	.byte	0x1d
	.uleb128 0x16
	.long	.LASF154
	.byte	0x1e
	.uleb128 0x16
	.long	.LASF155
	.byte	0x1f
	.uleb128 0x16
	.long	.LASF156
	.byte	0x20
	.uleb128 0x16
	.long	.LASF157
	.byte	0x21
	.uleb128 0x16
	.long	.LASF158
	.byte	0x22
	.uleb128 0x16
	.long	.LASF159
	.byte	0x23
	.uleb128 0x16
	.long	.LASF160
	.byte	0x24
	.uleb128 0x16
	.long	.LASF161
	.byte	0x25
	.uleb128 0x16
	.long	.LASF162
	.byte	0x26
	.uleb128 0x16
	.long	.LASF163
	.byte	0x27
	.uleb128 0x16
	.long	.LASF164
	.byte	0x28
	.uleb128 0x16
	.long	.LASF165
	.byte	0x29
	.uleb128 0x16
	.long	.LASF166
	.byte	0x2a
	.uleb128 0x16
	.long	.LASF167
	.byte	0x2b
	.uleb128 0x16
	.long	.LASF168
	.byte	0x2c
	.uleb128 0x16
	.long	.LASF169
	.byte	0x2d
	.uleb128 0x16
	.long	.LASF170
	.byte	0x2e
	.uleb128 0x16
	.long	.LASF171
	.byte	0x2f
	.uleb128 0x16
	.long	.LASF172
	.byte	0x30
	.uleb128 0x16
	.long	.LASF173
	.byte	0x31
	.uleb128 0x16
	.long	.LASF174
	.byte	0x32
	.uleb128 0x16
	.long	.LASF175
	.byte	0x33
	.uleb128 0x16
	.long	.LASF176
	.byte	0x34
	.uleb128 0x16
	.long	.LASF177
	.byte	0x35
	.uleb128 0x16
	.long	.LASF178
	.byte	0x37
	.uleb128 0x16
	.long	.LASF179
	.byte	0x38
	.uleb128 0x16
	.long	.LASF180
	.byte	0x39
	.uleb128 0x16
	.long	.LASF181
	.byte	0x3a
	.uleb128 0x16
	.long	.LASF182
	.byte	0x3b
	.uleb128 0x16
	.long	.LASF183
	.byte	0x3c
	.uleb128 0x16
	.long	.LASF184
	.byte	0x3d
	.uleb128 0x16
	.long	.LASF185
	.byte	0x3e
	.uleb128 0x16
	.long	.LASF186
	.byte	0x63
	.uleb128 0x16
	.long	.LASF187
	.byte	0x64
	.uleb128 0x16
	.long	.LASF188
	.byte	0x65
	.uleb128 0x16
	.long	.LASF189
	.byte	0x66
	.uleb128 0x16
	.long	.LASF190
	.byte	0x67
	.uleb128 0x16
	.long	.LASF191
	.byte	0x68
	.uleb128 0x16
	.long	.LASF192
	.byte	0x69
	.uleb128 0x16
	.long	.LASF193
	.byte	0x6a
	.uleb128 0x16
	.long	.LASF194
	.byte	0x6b
	.uleb128 0x16
	.long	.LASF195
	.byte	0x6c
	.uleb128 0x16
	.long	.LASF196
	.byte	0x6d
	.uleb128 0x16
	.long	.LASF197
	.byte	0xf9
	.uleb128 0x16
	.long	.LASF198
	.byte	0xfa
	.uleb128 0x16
	.long	.LASF199
	.byte	0xfb
	.uleb128 0x16
	.long	.LASF200
	.byte	0xfc
	.uleb128 0x16
	.long	.LASF201
	.byte	0xfd
	.uleb128 0x16
	.long	.LASF202
	.byte	0xfe
	.uleb128 0x16
	.long	.LASF203
	.byte	0xff
	.uleb128 0x17
	.long	.LASF204
	.value	0x100
	.uleb128 0x17
	.long	.LASF205
	.value	0x101
	.uleb128 0x17
	.long	.LASF206
	.value	0x102
	.uleb128 0x17
	.long	.LASF207
	.value	0x8000
	.uleb128 0x17
	.long	.LASF208
	.value	0x8001
	.uleb128 0x18
	.long	.LASF209
	.long	0x10000
	.byte	0
	.uleb128 0x19
	.byte	0x10
	.byte	0x13
	.value	0x204
	.byte	0x3
	.long	0xa4c
	.uleb128 0x1a
	.long	.LASF210
	.byte	0x13
	.value	0x205
	.byte	0x13
	.long	0xa4c
	.byte	0
	.uleb128 0xa
	.long	0x2d
	.long	0xa5c
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x1b
	.long	.LASF211
	.byte	0x10
	.byte	0x13
	.value	0x203
	.byte	0x8
	.long	0xa79
	.uleb128 0xe
	.long	.LASF212
	.byte	0x13
	.value	0x206
	.byte	0x5
	.long	0xa34
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0xa5c
	.uleb128 0x10
	.long	.LASF213
	.byte	0x14
	.byte	0x52
	.byte	0x23
	.long	0xa79
	.uleb128 0x1c
	.long	0xa1
	.long	0xa99
	.uleb128 0x1d
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF214
	.byte	0x15
	.value	0x151
	.byte	0x10
	.long	0xaa6
	.uleb128 0x6
	.byte	0x8
	.long	0xa8a
	.uleb128 0x1c
	.long	0xa1
	.long	0xac0
	.uleb128 0x1d
	.long	0xa1
	.uleb128 0x1d
	.long	0xb5
	.byte	0
	.uleb128 0x12
	.long	.LASF215
	.byte	0x15
	.value	0x152
	.byte	0x10
	.long	0xacd
	.uleb128 0x6
	.byte	0x8
	.long	0xaac
	.uleb128 0x1e
	.long	0xade
	.uleb128 0x1d
	.long	0xa1
	.byte	0
	.uleb128 0x12
	.long	.LASF216
	.byte	0x15
	.value	0x153
	.byte	0xf
	.long	0xaeb
	.uleb128 0x6
	.byte	0x8
	.long	0xad3
	.uleb128 0x1f
	.long	.LASF233
	.byte	0x1
	.byte	0x57
	.byte	0x5
	.long	0x6f
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0xcae
	.uleb128 0x20
	.long	.LASF217
	.byte	0x1
	.byte	0x57
	.byte	0x23
	.long	0x521
	.long	.LLST0
	.long	.LVUS0
	.uleb128 0x20
	.long	.LASF218
	.byte	0x1
	.byte	0x57
	.byte	0x2d
	.long	0x6f
	.long	.LLST1
	.long	.LVUS1
	.uleb128 0x20
	.long	.LASF219
	.byte	0x1
	.byte	0x57
	.byte	0x3b
	.long	0x6f
	.long	.LLST2
	.long	.LVUS2
	.uleb128 0x21
	.string	"id"
	.byte	0x1
	.byte	0x58
	.byte	0x26
	.long	0x34
	.long	.LLST3
	.long	.LVUS3
	.uleb128 0x21
	.string	"rd"
	.byte	0x1
	.byte	0x58
	.byte	0x2e
	.long	0x6f
	.long	.LLST4
	.long	.LVUS4
	.uleb128 0x20
	.long	.LASF220
	.byte	0x1
	.byte	0x58
	.byte	0x42
	.long	0xcae
	.long	.LLST5
	.long	.LVUS5
	.uleb128 0x22
	.long	.LASF221
	.byte	0x1
	.byte	0x59
	.byte	0x1c
	.long	0xcb4
	.uleb128 0x2
	.byte	0x91
	.sleb128 0
	.uleb128 0x22
	.long	.LASF222
	.byte	0x1
	.byte	0x59
	.byte	0x29
	.long	0x6f
	.uleb128 0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0x23
	.string	"len"
	.byte	0x1
	.byte	0x5b
	.byte	0xa
	.long	0xb5
	.long	.LLST6
	.long	.LVUS6
	.uleb128 0x23
	.string	"q"
	.byte	0x1
	.byte	0x5c
	.byte	0x12
	.long	0x80f
	.long	.LLST7
	.long	.LVUS7
	.uleb128 0x23
	.string	"p"
	.byte	0x1
	.byte	0x5d
	.byte	0xf
	.long	0x521
	.long	.LLST8
	.long	.LVUS8
	.uleb128 0x24
	.long	.LASF223
	.byte	0x1
	.byte	0x5e
	.byte	0xa
	.long	0xb5
	.long	.LLST9
	.long	.LVUS9
	.uleb128 0x23
	.string	"buf"
	.byte	0x1
	.byte	0x5f
	.byte	0x12
	.long	0x80f
	.long	.LLST10
	.long	.LVUS10
	.uleb128 0x25
	.long	0xcba
	.quad	.LBI6
	.byte	.LVU31
	.long	.Ldebug_ranges0+0
	.byte	0x1
	.byte	0x75
	.byte	0x3
	.long	0xc48
	.uleb128 0x26
	.long	0xce3
	.long	.LLST11
	.long	.LVUS11
	.uleb128 0x26
	.long	0xcd7
	.long	.LLST12
	.long	.LVUS12
	.uleb128 0x26
	.long	0xccb
	.long	.LLST13
	.long	.LVUS13
	.byte	0
	.uleb128 0x25
	.long	0xcba
	.quad	.LBI12
	.byte	.LVU159
	.long	.Ldebug_ranges0+0x40
	.byte	0x1
	.byte	0xb7
	.byte	0x7
	.long	0xc81
	.uleb128 0x26
	.long	0xce3
	.long	.LLST14
	.long	.LVUS14
	.uleb128 0x26
	.long	0xcd7
	.long	.LLST15
	.long	.LVUS15
	.uleb128 0x27
	.long	0xccb
	.byte	0
	.uleb128 0x28
	.quad	.LVL1
	.long	0xcf0
	.long	0xc99
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.uleb128 0x2a
	.quad	.LVL2
	.long	0xcfd
	.uleb128 0x29
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x7d
	.sleb128 0
	.byte	0
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x80f
	.uleb128 0x6
	.byte	0x8
	.long	0x6f
	.uleb128 0x2b
	.long	.LASF234
	.byte	0x2
	.byte	0x3b
	.byte	0x2a
	.long	0xa1
	.byte	0x3
	.long	0xcf0
	.uleb128 0x2c
	.long	.LASF224
	.byte	0x2
	.byte	0x3b
	.byte	0x38
	.long	0xa1
	.uleb128 0x2c
	.long	.LASF225
	.byte	0x2
	.byte	0x3b
	.byte	0x44
	.long	0x6f
	.uleb128 0x2c
	.long	.LASF226
	.byte	0x2
	.byte	0x3b
	.byte	0x51
	.long	0xb5
	.byte	0
	.uleb128 0x2d
	.long	.LASF227
	.long	.LASF227
	.byte	0x15
	.value	0x14e
	.byte	0x5
	.uleb128 0x2d
	.long	.LASF228
	.long	.LASF228
	.byte	0x16
	.value	0x181
	.byte	0xf
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x5
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1c
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2116
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x23
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x24
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x25
	.uleb128 0x1d
	.byte	0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x52
	.uleb128 0x1
	.uleb128 0x2138
	.uleb128 0xb
	.uleb128 0x55
	.uleb128 0x17
	.uleb128 0x58
	.uleb128 0xb
	.uleb128 0x59
	.uleb128 0xb
	.uleb128 0x57
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x26
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x17
	.uleb128 0x2137
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x27
	.uleb128 0x5
	.byte	0
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x28
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x29
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x2a
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x20
	.uleb128 0xb
	.uleb128 0x34
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2c
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x2d
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_loc,"",@progbits
.Ldebug_loc0:
.LVUS0:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU90
	.uleb128 .LVU90
	.uleb128 .LVU91
	.uleb128 .LVU91
	.uleb128 .LVU115
	.uleb128 .LVU140
	.uleb128 .LVU151
	.uleb128 .LVU154
	.uleb128 .LVU156
	.uleb128 .LVU156
	.uleb128 .LVU157
	.uleb128 .LVU157
	.uleb128 .LVU158
.LLST0:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL1-1-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL20-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL21-.Ltext0
	.quad	.LVL30-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL37-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL43-.Ltext0
	.quad	.LVL45-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL45-.Ltext0
	.quad	.LVL46-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x55
	.byte	0x9f
	.quad	.LVL46-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	0
	.quad	0
.LVUS1:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 0
.LLST1:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL1-1-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x3
	.byte	0x91
	.sleb128 -72
	.quad	0
	.quad	0
.LVUS2:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU120
	.uleb128 .LVU120
	.uleb128 .LVU140
	.uleb128 .LVU140
	.uleb128 .LVU151
	.uleb128 .LVU151
	.uleb128 .LVU154
	.uleb128 .LVU154
	.uleb128 .LVU158
	.uleb128 .LVU158
	.uleb128 0
.LLST2:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL1-1-.Ltext0
	.quad	.LVL31-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL31-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL37-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL41-.Ltext0
	.quad	.LVL43-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	.LVL43-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x1
	.byte	0x5e
	.quad	.LVL47-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x51
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS3:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 0
.LLST3:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL1-1-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x52
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS4:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU139
	.uleb128 .LVU139
	.uleb128 .LVU140
	.uleb128 .LVU140
	.uleb128 0
.LLST4:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x58
	.quad	.LVL1-1-.Ltext0
	.quad	.LVL36-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	.LVL36-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x58
	.byte	0x9f
	.quad	.LVL37-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5f
	.quad	0
	.quad	0
.LVUS5:
	.uleb128 0
	.uleb128 .LVU16
	.uleb128 .LVU16
	.uleb128 .LVU138
	.uleb128 .LVU138
	.uleb128 .LVU140
	.uleb128 .LVU140
	.uleb128 0
.LLST5:
	.quad	.LVL0-.Ltext0
	.quad	.LVL1-1-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL1-1-.Ltext0
	.quad	.LVL35-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	.LVL35-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x4
	.byte	0xf3
	.uleb128 0x1
	.byte	0x59
	.byte	0x9f
	.quad	.LVL37-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x5c
	.quad	0
	.quad	0
.LVUS6:
	.uleb128 .LVU23
	.uleb128 .LVU25
	.uleb128 .LVU25
	.uleb128 .LVU26
	.uleb128 .LVU62
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU91
	.uleb128 .LVU140
	.uleb128 .LVU143
.LLST6:
	.quad	.LVL3-.Ltext0
	.quad	.LVL4-.Ltext0
	.value	0x8
	.byte	0x70
	.sleb128 0
	.byte	0x79
	.sleb128 0
	.byte	0x22
	.byte	0x23
	.uleb128 0x12
	.byte	0x9f
	.quad	.LVL4-.Ltext0
	.quad	.LVL5-1-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	.LVL12-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL37-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS7:
	.uleb128 .LVU30
	.uleb128 .LVU55
	.uleb128 .LVU55
	.uleb128 .LVU58
	.uleb128 .LVU58
	.uleb128 .LVU85
	.uleb128 .LVU85
	.uleb128 .LVU91
	.uleb128 .LVU91
	.uleb128 .LVU94
	.uleb128 .LVU94
	.uleb128 .LVU96
	.uleb128 .LVU96
	.uleb128 .LVU114
	.uleb128 .LVU114
	.uleb128 .LVU128
	.uleb128 .LVU128
	.uleb128 .LVU131
	.uleb128 .LVU140
	.uleb128 .LVU151
	.uleb128 .LVU154
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 .LVU157
	.uleb128 .LVU158
	.uleb128 .LVU164
	.uleb128 .LVU164
	.uleb128 .LVU167
	.uleb128 .LVU167
	.uleb128 .LVU177
	.uleb128 .LVU177
	.uleb128 .LVU179
	.uleb128 .LVU179
	.uleb128 .LVU180
	.uleb128 .LVU180
	.uleb128 0
.LLST7:
	.quad	.LVL7-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL9-.Ltext0
	.quad	.LVL10-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL10-.Ltext0
	.quad	.LVL19-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL19-.Ltext0
	.quad	.LVL21-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL21-.Ltext0
	.quad	.LVL22-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL22-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 -1
	.byte	0x9f
	.quad	.LVL23-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL29-.Ltext0
	.quad	.LVL32-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 1
	.byte	0x9f
	.quad	.LVL32-.Ltext0
	.quad	.LVL33-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL37-.Ltext0
	.quad	.LVL41-.Ltext0
	.value	0x1
	.byte	0x55
	.quad	.LVL43-.Ltext0
	.quad	.LVL44-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL44-.Ltext0
	.quad	.LVL46-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL47-.Ltext0
	.quad	.LVL49-.Ltext0
	.value	0x1
	.byte	0x51
	.quad	.LVL49-.Ltext0
	.quad	.LVL50-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 5
	.byte	0x9f
	.quad	.LVL50-.Ltext0
	.quad	.LVL51-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 6
	.byte	0x9f
	.quad	.LVL51-.Ltext0
	.quad	.LVL52-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 16
	.byte	0x9f
	.quad	.LVL52-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL53-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x3
	.byte	0x75
	.sleb128 16
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS8:
	.uleb128 .LVU62
	.uleb128 .LVU65
	.uleb128 .LVU65
	.uleb128 .LVU66
	.uleb128 .LVU66
	.uleb128 .LVU67
	.uleb128 .LVU72
	.uleb128 .LVU75
	.uleb128 .LVU86
	.uleb128 .LVU90
	.uleb128 .LVU90
	.uleb128 .LVU95
	.uleb128 .LVU95
	.uleb128 .LVU97
	.uleb128 .LVU97
	.uleb128 .LVU99
	.uleb128 .LVU100
	.uleb128 .LVU105
	.uleb128 .LVU105
	.uleb128 .LVU111
	.uleb128 .LVU111
	.uleb128 .LVU113
	.uleb128 .LVU140
	.uleb128 .LVU141
	.uleb128 .LVU141
	.uleb128 .LVU142
	.uleb128 .LVU142
	.uleb128 .LVU143
.LLST8:
	.quad	.LVL11-.Ltext0
	.quad	.LVL12-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL12-.Ltext0
	.quad	.LVL13-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL13-.Ltext0
	.quad	.LVL14-.Ltext0
	.value	0x3
	.byte	0x7b
	.sleb128 -1
	.byte	0x9f
	.quad	.LVL16-.Ltext0
	.quad	.LVL17-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL19-.Ltext0
	.quad	.LVL20-.Ltext0
	.value	0x1
	.byte	0x5d
	.quad	.LVL20-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL23-.Ltext0
	.quad	.LVL23-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL23-.Ltext0
	.quad	.LVL24-.Ltext0
	.value	0x3
	.byte	0x74
	.sleb128 1
	.byte	0x9f
	.quad	.LVL25-.Ltext0
	.quad	.LVL26-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL26-.Ltext0
	.quad	.LVL28-.Ltext0
	.value	0x1
	.byte	0x54
	.quad	.LVL28-.Ltext0
	.quad	.LVL29-.Ltext0
	.value	0x3
	.byte	0x74
	.sleb128 1
	.byte	0x9f
	.quad	.LVL37-.Ltext0
	.quad	.LVL37-.Ltext0
	.value	0x1
	.byte	0x52
	.quad	.LVL37-.Ltext0
	.quad	.LVL38-.Ltext0
	.value	0x3
	.byte	0x72
	.sleb128 1
	.byte	0x9f
	.quad	.LVL38-.Ltext0
	.quad	.LVL39-.Ltext0
	.value	0x1
	.byte	0x5b
	.quad	0
	.quad	0
.LVUS9:
	.uleb128 .LVU129
	.uleb128 .LVU137
	.uleb128 .LVU158
	.uleb128 .LVU162
	.uleb128 .LVU162
	.uleb128 .LVU164
	.uleb128 .LVU164
	.uleb128 .LVU180
	.uleb128 .LVU180
	.uleb128 0
.LLST9:
	.quad	.LVL32-.Ltext0
	.quad	.LVL34-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL47-.Ltext0
	.quad	.LVL48-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL48-.Ltext0
	.quad	.LVL49-.Ltext0
	.value	0x6
	.byte	0x71
	.sleb128 0
	.byte	0x79
	.sleb128 0
	.byte	0x1c
	.byte	0x9f
	.quad	.LVL49-.Ltext0
	.quad	.LVL53-.Ltext0
	.value	0x8
	.byte	0x75
	.sleb128 0
	.byte	0x79
	.sleb128 0
	.byte	0x1c
	.byte	0x23
	.uleb128 0x5
	.byte	0x9f
	.quad	.LVL53-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS10:
	.uleb128 .LVU27
	.uleb128 .LVU55
	.uleb128 .LVU55
	.uleb128 .LVU137
	.uleb128 .LVU140
	.uleb128 .LVU152
	.uleb128 .LVU154
	.uleb128 .LVU155
	.uleb128 .LVU155
	.uleb128 .LVU157
	.uleb128 .LVU157
	.uleb128 .LVU158
	.uleb128 .LVU158
	.uleb128 0
.LLST10:
	.quad	.LVL6-.Ltext0
	.quad	.LVL9-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL9-.Ltext0
	.quad	.LVL34-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL37-.Ltext0
	.quad	.LVL42-1-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL43-.Ltext0
	.quad	.LVL44-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL44-.Ltext0
	.quad	.LVL46-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	.LVL46-.Ltext0
	.quad	.LVL47-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	.LVL47-.Ltext0
	.quad	.LFE87-.Ltext0
	.value	0x1
	.byte	0x59
	.quad	0
	.quad	0
.LVUS11:
	.uleb128 .LVU31
	.uleb128 .LVU38
.LLST11:
	.quad	.LVL7-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x2
	.byte	0x3c
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS12:
	.uleb128 .LVU31
	.uleb128 .LVU38
.LLST12:
	.quad	.LVL7-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS13:
	.uleb128 .LVU31
	.uleb128 .LVU38
.LLST13:
	.quad	.LVL7-.Ltext0
	.quad	.LVL8-.Ltext0
	.value	0x1
	.byte	0x50
	.quad	0
	.quad	0
.LVUS14:
	.uleb128 .LVU159
	.uleb128 .LVU165
.LLST14:
	.quad	.LVL47-.Ltext0
	.quad	.LVL50-.Ltext0
	.value	0x2
	.byte	0x3b
	.byte	0x9f
	.quad	0
	.quad	0
.LVUS15:
	.uleb128 .LVU159
	.uleb128 .LVU165
.LLST15:
	.quad	.LVL47-.Ltext0
	.quad	.LVL50-.Ltext0
	.value	0x2
	.byte	0x30
	.byte	0x9f
	.quad	0
	.quad	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.quad	.LBB6-.Ltext0
	.quad	.LBE6-.Ltext0
	.quad	.LBB10-.Ltext0
	.quad	.LBE10-.Ltext0
	.quad	.LBB11-.Ltext0
	.quad	.LBE11-.Ltext0
	.quad	0
	.quad	0
	.quad	.LBB12-.Ltext0
	.quad	.LBE12-.Ltext0
	.quad	.LBB17-.Ltext0
	.quad	.LBE17-.Ltext0
	.quad	.LBB18-.Ltext0
	.quad	.LBE18-.Ltext0
	.quad	.LBB19-.Ltext0
	.quad	.LBE19-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF5:
	.string	"short int"
.LASF43:
	.string	"_IO_read_ptr"
.LASF88:
	.string	"daylight"
.LASF55:
	.string	"_chain"
.LASF33:
	.string	"sin6_addr"
.LASF108:
	.string	"__in6_u"
.LASF13:
	.string	"size_t"
.LASF165:
	.string	"ns_t_opt"
.LASF61:
	.string	"_shortbuf"
.LASF36:
	.string	"sockaddr_ipx"
.LASF211:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF219:
	.string	"type"
.LASF225:
	.string	"__ch"
.LASF9:
	.string	"long int"
.LASF49:
	.string	"_IO_buf_base"
.LASF162:
	.string	"ns_t_a6"
.LASF158:
	.string	"ns_t_atma"
.LASF14:
	.string	"long long unsigned int"
.LASF100:
	.string	"in_addr_t"
.LASF170:
	.string	"ns_t_rrsig"
.LASF197:
	.string	"ns_t_tkey"
.LASF138:
	.string	"ns_t_minfo"
.LASF147:
	.string	"ns_t_nsap_ptr"
.LASF152:
	.string	"ns_t_aaaa"
.LASF86:
	.string	"__timezone"
.LASF130:
	.string	"ns_t_soa"
.LASF15:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF187:
	.string	"ns_t_uinfo"
.LASF35:
	.string	"sockaddr_inarp"
.LASF163:
	.string	"ns_t_dname"
.LASF119:
	.string	"ns_o_notify"
.LASF82:
	.string	"program_invocation_name"
.LASF56:
	.string	"_fileno"
.LASF220:
	.string	"bufp"
.LASF140:
	.string	"ns_t_txt"
.LASF232:
	.string	"_IO_lock_t"
.LASF150:
	.string	"ns_t_px"
.LASF105:
	.string	"__u6_addr16"
.LASF112:
	.string	"sys_siglist"
.LASF48:
	.string	"_IO_write_end"
.LASF230:
	.string	"../deps/cares/src/ares_create_query.c"
.LASF42:
	.string	"_flags"
.LASF50:
	.string	"_IO_buf_end"
.LASF75:
	.string	"stdin"
.LASF83:
	.string	"program_invocation_short_name"
.LASF73:
	.string	"_IO_codecvt"
.LASF21:
	.string	"sockaddr_dl"
.LASF144:
	.string	"ns_t_isdn"
.LASF98:
	.string	"uint16_t"
.LASF176:
	.string	"ns_t_tlsa"
.LASF81:
	.string	"_sys_errlist"
.LASF196:
	.string	"ns_t_eui64"
.LASF204:
	.string	"ns_t_uri"
.LASF58:
	.string	"_old_offset"
.LASF63:
	.string	"_offset"
.LASF164:
	.string	"ns_t_sink"
.LASF216:
	.string	"ares_free"
.LASF115:
	.string	"shift"
.LASF110:
	.string	"in6addr_loopback"
.LASF181:
	.string	"ns_t_talink"
.LASF191:
	.string	"ns_t_nid"
.LASF120:
	.string	"ns_o_update"
.LASF154:
	.string	"ns_t_nxt"
.LASF87:
	.string	"tzname"
.LASF161:
	.string	"ns_t_cert"
.LASF8:
	.string	"__uint32_t"
.LASF114:
	.string	"mask"
.LASF121:
	.string	"ns_o_max"
.LASF143:
	.string	"ns_t_x25"
.LASF146:
	.string	"ns_t_nsap"
.LASF210:
	.string	"_S6_u8"
.LASF28:
	.string	"sin_zero"
.LASF203:
	.string	"ns_t_any"
.LASF149:
	.string	"ns_t_key"
.LASF153:
	.string	"ns_t_loc"
.LASF2:
	.string	"unsigned int"
.LASF102:
	.string	"s_addr"
.LASF67:
	.string	"_freeres_buf"
.LASF228:
	.string	"strlen"
.LASF3:
	.string	"long unsigned int"
.LASF186:
	.string	"ns_t_spf"
.LASF113:
	.string	"_ns_flagdata"
.LASF148:
	.string	"ns_t_sig"
.LASF47:
	.string	"_IO_write_ptr"
.LASF178:
	.string	"ns_t_hip"
.LASF217:
	.string	"name"
.LASF214:
	.string	"ares_malloc"
.LASF136:
	.string	"ns_t_ptr"
.LASF78:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF27:
	.string	"sin_addr"
.LASF212:
	.string	"_S6_un"
.LASF231:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF51:
	.string	"_IO_save_base"
.LASF192:
	.string	"ns_t_l32"
.LASF92:
	.string	"environ"
.LASF209:
	.string	"ns_t_max"
.LASF141:
	.string	"ns_t_rp"
.LASF62:
	.string	"_lock"
.LASF145:
	.string	"ns_t_rt"
.LASF106:
	.string	"__u6_addr32"
.LASF103:
	.string	"in_port_t"
.LASF151:
	.string	"ns_t_gpos"
.LASF155:
	.string	"ns_t_eid"
.LASF76:
	.string	"stdout"
.LASF190:
	.string	"ns_t_unspec"
.LASF182:
	.string	"ns_t_cds"
.LASF39:
	.string	"sockaddr_un"
.LASF200:
	.string	"ns_t_axfr"
.LASF64:
	.string	"_codecvt"
.LASF25:
	.string	"sin_family"
.LASF44:
	.string	"_IO_read_end"
.LASF19:
	.string	"sockaddr_at"
.LASF169:
	.string	"ns_t_ipseckey"
.LASF208:
	.string	"ns_t_dlv"
.LASF93:
	.string	"optarg"
.LASF90:
	.string	"getdate_err"
.LASF30:
	.string	"sin6_family"
.LASF233:
	.string	"ares_create_query"
.LASF189:
	.string	"ns_t_gid"
.LASF160:
	.string	"ns_t_kx"
.LASF129:
	.string	"ns_t_cname"
.LASF224:
	.string	"__dest"
.LASF166:
	.string	"ns_t_apl"
.LASF156:
	.string	"ns_t_nimloc"
.LASF38:
	.string	"sockaddr_ns"
.LASF109:
	.string	"in6addr_any"
.LASF41:
	.string	"_IO_FILE"
.LASF124:
	.string	"ns_t_invalid"
.LASF157:
	.string	"ns_t_srv"
.LASF91:
	.string	"__environ"
.LASF85:
	.string	"__daylight"
.LASF118:
	.string	"ns_o_status"
.LASF69:
	.string	"_mode"
.LASF72:
	.string	"_IO_marker"
.LASF167:
	.string	"ns_t_ds"
.LASF17:
	.string	"sa_family"
.LASF79:
	.string	"sys_errlist"
.LASF123:
	.string	"__ns_type"
.LASF54:
	.string	"_markers"
.LASF213:
	.string	"ares_in6addr_any"
.LASF125:
	.string	"ns_t_a"
.LASF34:
	.string	"sin6_scope_id"
.LASF0:
	.string	"unsigned char"
.LASF37:
	.string	"sockaddr_iso"
.LASF198:
	.string	"ns_t_tsig"
.LASF202:
	.string	"ns_t_maila"
.LASF201:
	.string	"ns_t_mailb"
.LASF207:
	.string	"ns_t_ta"
.LASF74:
	.string	"_IO_wide_data"
.LASF57:
	.string	"_flags2"
.LASF40:
	.string	"sockaddr_x25"
.LASF226:
	.string	"__len"
.LASF227:
	.string	"ares__is_onion_domain"
.LASF80:
	.string	"_sys_nerr"
.LASF60:
	.string	"_vtable_offset"
.LASF20:
	.string	"sockaddr_ax25"
.LASF71:
	.string	"FILE"
.LASF223:
	.string	"buflen"
.LASF117:
	.string	"ns_o_iquery"
.LASF171:
	.string	"ns_t_nsec"
.LASF142:
	.string	"ns_t_afsdb"
.LASF221:
	.string	"buflenp"
.LASF107:
	.string	"in6_addr"
.LASF31:
	.string	"sin6_port"
.LASF96:
	.string	"optopt"
.LASF183:
	.string	"ns_t_cdnskey"
.LASF159:
	.string	"ns_t_naptr"
.LASF179:
	.string	"ns_t_ninfo"
.LASF131:
	.string	"ns_t_mb"
.LASF127:
	.string	"ns_t_md"
.LASF12:
	.string	"char"
.LASF128:
	.string	"ns_t_mf"
.LASF132:
	.string	"ns_t_mg"
.LASF174:
	.string	"ns_t_nsec3"
.LASF122:
	.string	"__ns_opcode"
.LASF32:
	.string	"sin6_flowinfo"
.LASF185:
	.string	"ns_t_csync"
.LASF7:
	.string	"__uint16_t"
.LASF133:
	.string	"ns_t_mr"
.LASF184:
	.string	"ns_t_openpgpkey"
.LASF104:
	.string	"__u6_addr8"
.LASF139:
	.string	"ns_t_mx"
.LASF95:
	.string	"opterr"
.LASF26:
	.string	"sin_port"
.LASF206:
	.string	"ns_t_avc"
.LASF11:
	.string	"__off64_t"
.LASF193:
	.string	"ns_t_l64"
.LASF59:
	.string	"_cur_column"
.LASF45:
	.string	"_IO_read_base"
.LASF172:
	.string	"ns_t_dnskey"
.LASF53:
	.string	"_IO_save_end"
.LASF111:
	.string	"_sys_siglist"
.LASF89:
	.string	"timezone"
.LASF180:
	.string	"ns_t_rkey"
.LASF22:
	.string	"sockaddr_eon"
.LASF68:
	.string	"__pad5"
.LASF215:
	.string	"ares_realloc"
.LASF205:
	.string	"ns_t_caa"
.LASF16:
	.string	"sa_family_t"
.LASF70:
	.string	"_unused2"
.LASF77:
	.string	"stderr"
.LASF234:
	.string	"memset"
.LASF29:
	.string	"sockaddr_in6"
.LASF168:
	.string	"ns_t_sshfp"
.LASF23:
	.string	"sockaddr"
.LASF175:
	.string	"ns_t_nsec3param"
.LASF199:
	.string	"ns_t_ixfr"
.LASF24:
	.string	"sockaddr_in"
.LASF97:
	.string	"uint8_t"
.LASF52:
	.string	"_IO_backup_base"
.LASF94:
	.string	"optind"
.LASF126:
	.string	"ns_t_ns"
.LASF177:
	.string	"ns_t_smimea"
.LASF135:
	.string	"ns_t_wks"
.LASF173:
	.string	"ns_t_dhcid"
.LASF218:
	.string	"dnsclass"
.LASF18:
	.string	"sa_data"
.LASF66:
	.string	"_freeres_list"
.LASF222:
	.string	"max_udp_size"
.LASF229:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF65:
	.string	"_wide_data"
.LASF116:
	.string	"ns_o_query"
.LASF84:
	.string	"__tzname"
.LASF46:
	.string	"_IO_write_base"
.LASF137:
	.string	"ns_t_hinfo"
.LASF134:
	.string	"ns_t_null"
.LASF194:
	.string	"ns_t_lp"
.LASF195:
	.string	"ns_t_eui48"
.LASF99:
	.string	"uint32_t"
.LASF188:
	.string	"ns_t_uid"
.LASF101:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
