	.file	"ares__timeval.c"
	.text
.Ltext0:
	.p2align 4
	.globl	ares__tvnow
	.type	ares__tvnow, @function
ares__tvnow:
.LFB87:
	.file 1 "../deps/cares/src/ares__timeval.c"
	.loc 1 38 1 view -0
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	.loc 1 48 11 is_stmt 0 view .LVU1
	movl	$1, %edi
	.loc 1 38 1 view .LVU2
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	.loc 1 38 1 view .LVU3
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	.loc 1 46 3 is_stmt 1 view .LVU4
	.loc 1 47 3 view .LVU5
	.loc 1 48 3 view .LVU6
	.loc 1 48 11 is_stmt 0 view .LVU7
	leaq	-32(%rbp), %rsi
	call	clock_gettime@PLT
.LVL0:
	.loc 1 48 5 view .LVU8
	testl	%eax, %eax
	jne	.L2
	.loc 1 49 5 is_stmt 1 view .LVU9
	.loc 1 49 16 is_stmt 0 view .LVU10
	movq	-32(%rbp), %rax
	.loc 1 50 33 view .LVU11
	movq	-24(%rbp), %rcx
	movabsq	$2361183241434822607, %rdx
	.loc 1 49 16 view .LVU12
	movq	%rax, -48(%rbp)
	.loc 1 50 5 is_stmt 1 view .LVU13
	.loc 1 50 33 is_stmt 0 view .LVU14
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rcx, %rdx
	.loc 1 50 17 view .LVU15
	movq	%rdx, -40(%rbp)
.L3:
	.loc 1 66 3 is_stmt 1 view .LVU16
	.loc 1 67 1 is_stmt 0 view .LVU17
	movq	-8(%rbp), %rdi
	xorq	%fs:40, %rdi
	.loc 1 66 10 view .LVU18
	movq	-48(%rbp), %rax
	movq	-40(%rbp), %rdx
	.loc 1 67 1 view .LVU19
	jne	.L7
	leave
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	.cfi_restore_state
	.loc 1 59 5 is_stmt 1 view .LVU20
	.loc 1 59 11 is_stmt 0 view .LVU21
	leaq	-48(%rbp), %rdi
	xorl	%esi, %esi
	call	gettimeofday@PLT
.LVL1:
	jmp	.L3
.L7:
	.loc 1 67 1 view .LVU22
	call	__stack_chk_fail@PLT
.LVL2:
	.cfi_endproc
.LFE87:
	.size	ares__tvnow, .-ares__tvnow
.Letext0:
	.file 2 "/usr/include/x86_64-linux-gnu/bits/types.h"
	.file 3 "/usr/lib/gcc/x86_64-linux-gnu/9/include/stddef.h"
	.file 4 "/usr/include/x86_64-linux-gnu/bits/types/struct_timeval.h"
	.file 5 "/usr/include/x86_64-linux-gnu/bits/types/struct_timespec.h"
	.file 6 "/usr/include/x86_64-linux-gnu/bits/sockaddr.h"
	.file 7 "/usr/include/x86_64-linux-gnu/bits/socket.h"
	.file 8 "/usr/include/netinet/in.h"
	.file 9 "/usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h"
	.file 10 "/usr/include/x86_64-linux-gnu/bits/types/FILE.h"
	.file 11 "/usr/include/stdio.h"
	.file 12 "/usr/include/x86_64-linux-gnu/bits/sys_errlist.h"
	.file 13 "/usr/include/errno.h"
	.file 14 "/usr/include/time.h"
	.file 15 "/usr/include/unistd.h"
	.file 16 "/usr/include/x86_64-linux-gnu/bits/getopt_core.h"
	.file 17 "/usr/include/x86_64-linux-gnu/bits/stdint-uintn.h"
	.file 18 "../deps/cares/include/ares.h"
	.file 19 "../deps/cares/src/ares_ipv6.h"
	.file 20 "../deps/cares/src/ares_private.h"
	.file 21 "/usr/include/x86_64-linux-gnu/sys/time.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.long	0x937
	.value	0x4
	.long	.Ldebug_abbrev0
	.byte	0x8
	.uleb128 0x1
	.long	.LASF129
	.byte	0x1
	.long	.LASF130
	.long	.LASF131
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.long	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.long	.LASF0
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.long	.LASF1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.long	.LASF2
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF3
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF4
	.uleb128 0x3
	.long	.LASF6
	.byte	0x2
	.byte	0x26
	.byte	0x17
	.long	0x2d
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.long	.LASF5
	.uleb128 0x3
	.long	.LASF7
	.byte	0x2
	.byte	0x28
	.byte	0x1c
	.long	0x34
	.uleb128 0x4
	.byte	0x4
	.byte	0x5
	.string	"int"
	.uleb128 0x3
	.long	.LASF8
	.byte	0x2
	.byte	0x2a
	.byte	0x16
	.long	0x3b
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF9
	.uleb128 0x3
	.long	.LASF10
	.byte	0x2
	.byte	0x98
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF11
	.byte	0x2
	.byte	0x99
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF12
	.byte	0x2
	.byte	0xa0
	.byte	0x12
	.long	0x82
	.uleb128 0x3
	.long	.LASF13
	.byte	0x2
	.byte	0xa2
	.byte	0x12
	.long	0x82
	.uleb128 0x5
	.byte	0x8
	.uleb128 0x3
	.long	.LASF14
	.byte	0x2
	.byte	0xc4
	.byte	0x12
	.long	0x82
	.uleb128 0x6
	.byte	0x8
	.long	0xcd
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.long	.LASF15
	.uleb128 0x7
	.long	0xcd
	.uleb128 0x3
	.long	.LASF16
	.byte	0x3
	.byte	0xd1
	.byte	0x1b
	.long	0x42
	.uleb128 0x8
	.long	.LASF19
	.byte	0x10
	.byte	0x4
	.byte	0x8
	.byte	0x8
	.long	0x10d
	.uleb128 0x9
	.long	.LASF17
	.byte	0x4
	.byte	0xa
	.byte	0xc
	.long	0xa1
	.byte	0
	.uleb128 0x9
	.long	.LASF18
	.byte	0x4
	.byte	0xb
	.byte	0x11
	.long	0xad
	.byte	0x8
	.byte	0
	.uleb128 0x8
	.long	.LASF20
	.byte	0x10
	.byte	0x5
	.byte	0xa
	.byte	0x8
	.long	0x135
	.uleb128 0x9
	.long	.LASF17
	.byte	0x5
	.byte	0xc
	.byte	0xc
	.long	0xa1
	.byte	0
	.uleb128 0x9
	.long	.LASF21
	.byte	0x5
	.byte	0x10
	.byte	0x15
	.long	0xbb
	.byte	0x8
	.byte	0
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.long	.LASF22
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.long	.LASF23
	.uleb128 0x3
	.long	.LASF24
	.byte	0x6
	.byte	0x1c
	.byte	0x1c
	.long	0x34
	.uleb128 0x8
	.long	.LASF25
	.byte	0x10
	.byte	0x7
	.byte	0xb2
	.byte	0x8
	.long	0x177
	.uleb128 0x9
	.long	.LASF26
	.byte	0x7
	.byte	0xb4
	.byte	0x11
	.long	0x143
	.byte	0
	.uleb128 0x9
	.long	.LASF27
	.byte	0x7
	.byte	0xb5
	.byte	0xa
	.long	0x17c
	.byte	0x2
	.byte	0
	.uleb128 0x7
	.long	0x14f
	.uleb128 0xa
	.long	0xcd
	.long	0x18c
	.uleb128 0xb
	.long	0x42
	.byte	0xd
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0x14f
	.uleb128 0xc
	.long	0x18c
	.uleb128 0xd
	.long	.LASF28
	.uleb128 0x7
	.long	0x197
	.uleb128 0x6
	.byte	0x8
	.long	0x197
	.uleb128 0xc
	.long	0x1a1
	.uleb128 0xd
	.long	.LASF29
	.uleb128 0x7
	.long	0x1ac
	.uleb128 0x6
	.byte	0x8
	.long	0x1ac
	.uleb128 0xc
	.long	0x1b6
	.uleb128 0xd
	.long	.LASF30
	.uleb128 0x7
	.long	0x1c1
	.uleb128 0x6
	.byte	0x8
	.long	0x1c1
	.uleb128 0xc
	.long	0x1cb
	.uleb128 0xd
	.long	.LASF31
	.uleb128 0x7
	.long	0x1d6
	.uleb128 0x6
	.byte	0x8
	.long	0x1d6
	.uleb128 0xc
	.long	0x1e0
	.uleb128 0x8
	.long	.LASF32
	.byte	0x10
	.byte	0x8
	.byte	0xee
	.byte	0x8
	.long	0x22d
	.uleb128 0x9
	.long	.LASF33
	.byte	0x8
	.byte	0xf0
	.byte	0x11
	.long	0x143
	.byte	0
	.uleb128 0x9
	.long	.LASF34
	.byte	0x8
	.byte	0xf1
	.byte	0xf
	.long	0x722
	.byte	0x2
	.uleb128 0x9
	.long	.LASF35
	.byte	0x8
	.byte	0xf2
	.byte	0x14
	.long	0x707
	.byte	0x4
	.uleb128 0x9
	.long	.LASF36
	.byte	0x8
	.byte	0xf5
	.byte	0x13
	.long	0x7c4
	.byte	0x8
	.byte	0
	.uleb128 0x7
	.long	0x1eb
	.uleb128 0x6
	.byte	0x8
	.long	0x1eb
	.uleb128 0xc
	.long	0x232
	.uleb128 0x8
	.long	.LASF37
	.byte	0x1c
	.byte	0x8
	.byte	0xfd
	.byte	0x8
	.long	0x290
	.uleb128 0x9
	.long	.LASF38
	.byte	0x8
	.byte	0xff
	.byte	0x11
	.long	0x143
	.byte	0
	.uleb128 0xe
	.long	.LASF39
	.byte	0x8
	.value	0x100
	.byte	0xf
	.long	0x722
	.byte	0x2
	.uleb128 0xe
	.long	.LASF40
	.byte	0x8
	.value	0x101
	.byte	0xe
	.long	0x6ef
	.byte	0x4
	.uleb128 0xe
	.long	.LASF41
	.byte	0x8
	.value	0x102
	.byte	0x15
	.long	0x78c
	.byte	0x8
	.uleb128 0xe
	.long	.LASF42
	.byte	0x8
	.value	0x103
	.byte	0xe
	.long	0x6ef
	.byte	0x18
	.byte	0
	.uleb128 0x7
	.long	0x23d
	.uleb128 0x6
	.byte	0x8
	.long	0x23d
	.uleb128 0xc
	.long	0x295
	.uleb128 0xd
	.long	.LASF43
	.uleb128 0x7
	.long	0x2a0
	.uleb128 0x6
	.byte	0x8
	.long	0x2a0
	.uleb128 0xc
	.long	0x2aa
	.uleb128 0xd
	.long	.LASF44
	.uleb128 0x7
	.long	0x2b5
	.uleb128 0x6
	.byte	0x8
	.long	0x2b5
	.uleb128 0xc
	.long	0x2bf
	.uleb128 0xd
	.long	.LASF45
	.uleb128 0x7
	.long	0x2ca
	.uleb128 0x6
	.byte	0x8
	.long	0x2ca
	.uleb128 0xc
	.long	0x2d4
	.uleb128 0xd
	.long	.LASF46
	.uleb128 0x7
	.long	0x2df
	.uleb128 0x6
	.byte	0x8
	.long	0x2df
	.uleb128 0xc
	.long	0x2e9
	.uleb128 0xd
	.long	.LASF47
	.uleb128 0x7
	.long	0x2f4
	.uleb128 0x6
	.byte	0x8
	.long	0x2f4
	.uleb128 0xc
	.long	0x2fe
	.uleb128 0xd
	.long	.LASF48
	.uleb128 0x7
	.long	0x309
	.uleb128 0x6
	.byte	0x8
	.long	0x309
	.uleb128 0xc
	.long	0x313
	.uleb128 0x6
	.byte	0x8
	.long	0x177
	.uleb128 0xc
	.long	0x31e
	.uleb128 0x6
	.byte	0x8
	.long	0x19c
	.uleb128 0xc
	.long	0x329
	.uleb128 0x6
	.byte	0x8
	.long	0x1b1
	.uleb128 0xc
	.long	0x334
	.uleb128 0x6
	.byte	0x8
	.long	0x1c6
	.uleb128 0xc
	.long	0x33f
	.uleb128 0x6
	.byte	0x8
	.long	0x1db
	.uleb128 0xc
	.long	0x34a
	.uleb128 0x6
	.byte	0x8
	.long	0x22d
	.uleb128 0xc
	.long	0x355
	.uleb128 0x6
	.byte	0x8
	.long	0x290
	.uleb128 0xc
	.long	0x360
	.uleb128 0x6
	.byte	0x8
	.long	0x2a5
	.uleb128 0xc
	.long	0x36b
	.uleb128 0x6
	.byte	0x8
	.long	0x2ba
	.uleb128 0xc
	.long	0x376
	.uleb128 0x6
	.byte	0x8
	.long	0x2cf
	.uleb128 0xc
	.long	0x381
	.uleb128 0x6
	.byte	0x8
	.long	0x2e4
	.uleb128 0xc
	.long	0x38c
	.uleb128 0x6
	.byte	0x8
	.long	0x2f9
	.uleb128 0xc
	.long	0x397
	.uleb128 0x6
	.byte	0x8
	.long	0x30e
	.uleb128 0xc
	.long	0x3a2
	.uleb128 0xa
	.long	0xcd
	.long	0x3bd
	.uleb128 0xb
	.long	0x42
	.byte	0
	.byte	0
	.uleb128 0x8
	.long	.LASF49
	.byte	0xd8
	.byte	0x9
	.byte	0x31
	.byte	0x8
	.long	0x544
	.uleb128 0x9
	.long	.LASF50
	.byte	0x9
	.byte	0x33
	.byte	0x7
	.long	0x6f
	.byte	0
	.uleb128 0x9
	.long	.LASF51
	.byte	0x9
	.byte	0x36
	.byte	0x9
	.long	0xc7
	.byte	0x8
	.uleb128 0x9
	.long	.LASF52
	.byte	0x9
	.byte	0x37
	.byte	0x9
	.long	0xc7
	.byte	0x10
	.uleb128 0x9
	.long	.LASF53
	.byte	0x9
	.byte	0x38
	.byte	0x9
	.long	0xc7
	.byte	0x18
	.uleb128 0x9
	.long	.LASF54
	.byte	0x9
	.byte	0x39
	.byte	0x9
	.long	0xc7
	.byte	0x20
	.uleb128 0x9
	.long	.LASF55
	.byte	0x9
	.byte	0x3a
	.byte	0x9
	.long	0xc7
	.byte	0x28
	.uleb128 0x9
	.long	.LASF56
	.byte	0x9
	.byte	0x3b
	.byte	0x9
	.long	0xc7
	.byte	0x30
	.uleb128 0x9
	.long	.LASF57
	.byte	0x9
	.byte	0x3c
	.byte	0x9
	.long	0xc7
	.byte	0x38
	.uleb128 0x9
	.long	.LASF58
	.byte	0x9
	.byte	0x3d
	.byte	0x9
	.long	0xc7
	.byte	0x40
	.uleb128 0x9
	.long	.LASF59
	.byte	0x9
	.byte	0x40
	.byte	0x9
	.long	0xc7
	.byte	0x48
	.uleb128 0x9
	.long	.LASF60
	.byte	0x9
	.byte	0x41
	.byte	0x9
	.long	0xc7
	.byte	0x50
	.uleb128 0x9
	.long	.LASF61
	.byte	0x9
	.byte	0x42
	.byte	0x9
	.long	0xc7
	.byte	0x58
	.uleb128 0x9
	.long	.LASF62
	.byte	0x9
	.byte	0x44
	.byte	0x16
	.long	0x55d
	.byte	0x60
	.uleb128 0x9
	.long	.LASF63
	.byte	0x9
	.byte	0x46
	.byte	0x14
	.long	0x563
	.byte	0x68
	.uleb128 0x9
	.long	.LASF64
	.byte	0x9
	.byte	0x48
	.byte	0x7
	.long	0x6f
	.byte	0x70
	.uleb128 0x9
	.long	.LASF65
	.byte	0x9
	.byte	0x49
	.byte	0x7
	.long	0x6f
	.byte	0x74
	.uleb128 0x9
	.long	.LASF66
	.byte	0x9
	.byte	0x4a
	.byte	0xb
	.long	0x89
	.byte	0x78
	.uleb128 0x9
	.long	.LASF67
	.byte	0x9
	.byte	0x4d
	.byte	0x12
	.long	0x34
	.byte	0x80
	.uleb128 0x9
	.long	.LASF68
	.byte	0x9
	.byte	0x4e
	.byte	0xf
	.long	0x49
	.byte	0x82
	.uleb128 0x9
	.long	.LASF69
	.byte	0x9
	.byte	0x4f
	.byte	0x8
	.long	0x3ad
	.byte	0x83
	.uleb128 0x9
	.long	.LASF70
	.byte	0x9
	.byte	0x51
	.byte	0xf
	.long	0x569
	.byte	0x88
	.uleb128 0x9
	.long	.LASF71
	.byte	0x9
	.byte	0x59
	.byte	0xd
	.long	0x95
	.byte	0x90
	.uleb128 0x9
	.long	.LASF72
	.byte	0x9
	.byte	0x5b
	.byte	0x17
	.long	0x574
	.byte	0x98
	.uleb128 0x9
	.long	.LASF73
	.byte	0x9
	.byte	0x5c
	.byte	0x19
	.long	0x57f
	.byte	0xa0
	.uleb128 0x9
	.long	.LASF74
	.byte	0x9
	.byte	0x5d
	.byte	0x14
	.long	0x563
	.byte	0xa8
	.uleb128 0x9
	.long	.LASF75
	.byte	0x9
	.byte	0x5e
	.byte	0x9
	.long	0xb9
	.byte	0xb0
	.uleb128 0x9
	.long	.LASF76
	.byte	0x9
	.byte	0x5f
	.byte	0xa
	.long	0xd9
	.byte	0xb8
	.uleb128 0x9
	.long	.LASF77
	.byte	0x9
	.byte	0x60
	.byte	0x7
	.long	0x6f
	.byte	0xc0
	.uleb128 0x9
	.long	.LASF78
	.byte	0x9
	.byte	0x62
	.byte	0x8
	.long	0x585
	.byte	0xc4
	.byte	0
	.uleb128 0x3
	.long	.LASF79
	.byte	0xa
	.byte	0x7
	.byte	0x19
	.long	0x3bd
	.uleb128 0xf
	.long	.LASF132
	.byte	0x9
	.byte	0x2b
	.byte	0xe
	.uleb128 0xd
	.long	.LASF80
	.uleb128 0x6
	.byte	0x8
	.long	0x558
	.uleb128 0x6
	.byte	0x8
	.long	0x3bd
	.uleb128 0x6
	.byte	0x8
	.long	0x550
	.uleb128 0xd
	.long	.LASF81
	.uleb128 0x6
	.byte	0x8
	.long	0x56f
	.uleb128 0xd
	.long	.LASF82
	.uleb128 0x6
	.byte	0x8
	.long	0x57a
	.uleb128 0xa
	.long	0xcd
	.long	0x595
	.uleb128 0xb
	.long	0x42
	.byte	0x13
	.byte	0
	.uleb128 0x6
	.byte	0x8
	.long	0xd4
	.uleb128 0x7
	.long	0x595
	.uleb128 0x10
	.long	.LASF83
	.byte	0xb
	.byte	0x89
	.byte	0xe
	.long	0x5ac
	.uleb128 0x6
	.byte	0x8
	.long	0x544
	.uleb128 0x10
	.long	.LASF84
	.byte	0xb
	.byte	0x8a
	.byte	0xe
	.long	0x5ac
	.uleb128 0x10
	.long	.LASF85
	.byte	0xb
	.byte	0x8b
	.byte	0xe
	.long	0x5ac
	.uleb128 0x10
	.long	.LASF86
	.byte	0xc
	.byte	0x1a
	.byte	0xc
	.long	0x6f
	.uleb128 0xa
	.long	0x59b
	.long	0x5e1
	.uleb128 0x11
	.byte	0
	.uleb128 0x7
	.long	0x5d6
	.uleb128 0x10
	.long	.LASF87
	.byte	0xc
	.byte	0x1b
	.byte	0x1a
	.long	0x5e1
	.uleb128 0x10
	.long	.LASF88
	.byte	0xc
	.byte	0x1e
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF89
	.byte	0xc
	.byte	0x1f
	.byte	0x1a
	.long	0x5e1
	.uleb128 0x10
	.long	.LASF90
	.byte	0xd
	.byte	0x2d
	.byte	0xe
	.long	0xc7
	.uleb128 0x10
	.long	.LASF91
	.byte	0xd
	.byte	0x2e
	.byte	0xe
	.long	0xc7
	.uleb128 0xa
	.long	0xc7
	.long	0x632
	.uleb128 0xb
	.long	0x42
	.byte	0x1
	.byte	0
	.uleb128 0x10
	.long	.LASF92
	.byte	0xe
	.byte	0x9f
	.byte	0xe
	.long	0x622
	.uleb128 0x10
	.long	.LASF93
	.byte	0xe
	.byte	0xa0
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF94
	.byte	0xe
	.byte	0xa1
	.byte	0x11
	.long	0x82
	.uleb128 0x10
	.long	.LASF95
	.byte	0xe
	.byte	0xa6
	.byte	0xe
	.long	0x622
	.uleb128 0x10
	.long	.LASF96
	.byte	0xe
	.byte	0xae
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF97
	.byte	0xe
	.byte	0xaf
	.byte	0x11
	.long	0x82
	.uleb128 0x12
	.long	.LASF98
	.byte	0xe
	.value	0x112
	.byte	0xc
	.long	0x6f
	.uleb128 0x12
	.long	.LASF99
	.byte	0xf
	.value	0x21f
	.byte	0xf
	.long	0x694
	.uleb128 0x6
	.byte	0x8
	.long	0xc7
	.uleb128 0x12
	.long	.LASF100
	.byte	0xf
	.value	0x221
	.byte	0xf
	.long	0x694
	.uleb128 0x10
	.long	.LASF101
	.byte	0x10
	.byte	0x24
	.byte	0xe
	.long	0xc7
	.uleb128 0x10
	.long	.LASF102
	.byte	0x10
	.byte	0x32
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF103
	.byte	0x10
	.byte	0x37
	.byte	0xc
	.long	0x6f
	.uleb128 0x10
	.long	.LASF104
	.byte	0x10
	.byte	0x3b
	.byte	0xc
	.long	0x6f
	.uleb128 0x3
	.long	.LASF105
	.byte	0x11
	.byte	0x18
	.byte	0x13
	.long	0x50
	.uleb128 0x3
	.long	.LASF106
	.byte	0x11
	.byte	0x19
	.byte	0x14
	.long	0x63
	.uleb128 0x3
	.long	.LASF107
	.byte	0x11
	.byte	0x1a
	.byte	0x14
	.long	0x76
	.uleb128 0x3
	.long	.LASF108
	.byte	0x8
	.byte	0x1e
	.byte	0x12
	.long	0x6ef
	.uleb128 0x8
	.long	.LASF109
	.byte	0x4
	.byte	0x8
	.byte	0x1f
	.byte	0x8
	.long	0x722
	.uleb128 0x9
	.long	.LASF110
	.byte	0x8
	.byte	0x21
	.byte	0xf
	.long	0x6fb
	.byte	0
	.byte	0
	.uleb128 0x3
	.long	.LASF111
	.byte	0x8
	.byte	0x77
	.byte	0x12
	.long	0x6e3
	.uleb128 0x13
	.byte	0x10
	.byte	0x8
	.byte	0xd6
	.byte	0x5
	.long	0x75c
	.uleb128 0x14
	.long	.LASF112
	.byte	0x8
	.byte	0xd8
	.byte	0xa
	.long	0x75c
	.uleb128 0x14
	.long	.LASF113
	.byte	0x8
	.byte	0xd9
	.byte	0xb
	.long	0x76c
	.uleb128 0x14
	.long	.LASF114
	.byte	0x8
	.byte	0xda
	.byte	0xb
	.long	0x77c
	.byte	0
	.uleb128 0xa
	.long	0x6d7
	.long	0x76c
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0xa
	.long	0x6e3
	.long	0x77c
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0xa
	.long	0x6ef
	.long	0x78c
	.uleb128 0xb
	.long	0x42
	.byte	0x3
	.byte	0
	.uleb128 0x8
	.long	.LASF115
	.byte	0x10
	.byte	0x8
	.byte	0xd4
	.byte	0x8
	.long	0x7a7
	.uleb128 0x9
	.long	.LASF116
	.byte	0x8
	.byte	0xdb
	.byte	0x9
	.long	0x72e
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x78c
	.uleb128 0x10
	.long	.LASF117
	.byte	0x8
	.byte	0xe4
	.byte	0x1e
	.long	0x7a7
	.uleb128 0x10
	.long	.LASF118
	.byte	0x8
	.byte	0xe5
	.byte	0x1e
	.long	0x7a7
	.uleb128 0xa
	.long	0x2d
	.long	0x7d4
	.uleb128 0xb
	.long	0x42
	.byte	0x7
	.byte	0
	.uleb128 0x15
	.byte	0x10
	.byte	0x12
	.value	0x204
	.byte	0x3
	.long	0x7ec
	.uleb128 0x16
	.long	.LASF119
	.byte	0x12
	.value	0x205
	.byte	0x13
	.long	0x7ec
	.byte	0
	.uleb128 0xa
	.long	0x2d
	.long	0x7fc
	.uleb128 0xb
	.long	0x42
	.byte	0xf
	.byte	0
	.uleb128 0x17
	.long	.LASF120
	.byte	0x10
	.byte	0x12
	.value	0x203
	.byte	0x8
	.long	0x819
	.uleb128 0xe
	.long	.LASF121
	.byte	0x12
	.value	0x206
	.byte	0x5
	.long	0x7d4
	.byte	0
	.byte	0
	.uleb128 0x7
	.long	0x7fc
	.uleb128 0x10
	.long	.LASF122
	.byte	0x13
	.byte	0x52
	.byte	0x23
	.long	0x819
	.uleb128 0x18
	.long	0xb9
	.long	0x839
	.uleb128 0x19
	.long	0xd9
	.byte	0
	.uleb128 0x12
	.long	.LASF123
	.byte	0x14
	.value	0x151
	.byte	0x10
	.long	0x846
	.uleb128 0x6
	.byte	0x8
	.long	0x82a
	.uleb128 0x18
	.long	0xb9
	.long	0x860
	.uleb128 0x19
	.long	0xb9
	.uleb128 0x19
	.long	0xd9
	.byte	0
	.uleb128 0x12
	.long	.LASF124
	.byte	0x14
	.value	0x152
	.byte	0x10
	.long	0x86d
	.uleb128 0x6
	.byte	0x8
	.long	0x84c
	.uleb128 0x1a
	.long	0x87e
	.uleb128 0x19
	.long	0xb9
	.byte	0
	.uleb128 0x12
	.long	.LASF125
	.byte	0x14
	.value	0x153
	.byte	0xf
	.long	0x88b
	.uleb128 0x6
	.byte	0x8
	.long	0x873
	.uleb128 0x1b
	.long	.LASF133
	.byte	0x1
	.byte	0x25
	.byte	0x10
	.long	0xe5
	.quad	.LFB87
	.quad	.LFE87-.LFB87
	.uleb128 0x1
	.byte	0x9c
	.long	0x919
	.uleb128 0x1c
	.string	"now"
	.byte	0x1
	.byte	0x2e
	.byte	0x12
	.long	0xe5
	.uleb128 0x2
	.byte	0x91
	.sleb128 -64
	.uleb128 0x1d
	.long	.LASF126
	.byte	0x1
	.byte	0x2f
	.byte	0x13
	.long	0x10d
	.uleb128 0x2
	.byte	0x91
	.sleb128 -48
	.uleb128 0x1e
	.quad	.LVL0
	.long	0x919
	.long	0x8ee
	.uleb128 0x1f
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x1
	.byte	0x31
	.uleb128 0x1f
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x2
	.byte	0x76
	.sleb128 -32
	.byte	0
	.uleb128 0x1e
	.quad	.LVL1
	.long	0x925
	.long	0x90b
	.uleb128 0x1f
	.uleb128 0x1
	.byte	0x55
	.uleb128 0x2
	.byte	0x76
	.sleb128 -48
	.uleb128 0x1f
	.uleb128 0x1
	.byte	0x54
	.uleb128 0x1
	.byte	0x30
	.byte	0
	.uleb128 0x20
	.quad	.LVL2
	.long	0x931
	.byte	0
	.uleb128 0x21
	.long	.LASF127
	.long	.LASF127
	.byte	0xe
	.byte	0xd5
	.byte	0xc
	.uleb128 0x21
	.long	.LASF128
	.long	.LASF128
	.byte	0x15
	.byte	0x42
	.byte	0xc
	.uleb128 0x22
	.long	.LASF134
	.long	.LASF134
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x10
	.uleb128 0x17
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0xf
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0x26
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x21
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x37
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x13
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x21
	.byte	0
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x16
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x17
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x18
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x19
	.uleb128 0x5
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1a
	.uleb128 0x15
	.byte	0x1
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1b
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0x19
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x7
	.uleb128 0x40
	.uleb128 0x18
	.uleb128 0x2117
	.uleb128 0x19
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1c
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1d
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x1e
	.uleb128 0x4109
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x1f
	.uleb128 0x410a
	.byte	0
	.uleb128 0x2
	.uleb128 0x18
	.uleb128 0x2111
	.uleb128 0x18
	.byte	0
	.byte	0
	.uleb128 0x20
	.uleb128 0x4109
	.byte	0
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x31
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x21
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x39
	.uleb128 0xb
	.byte	0
	.byte	0
	.uleb128 0x22
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0x19
	.uleb128 0x3c
	.uleb128 0x19
	.uleb128 0x6e
	.uleb128 0xe
	.uleb128 0x3
	.uleb128 0xe
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.long	0x2c
	.value	0x2
	.long	.Ldebug_info0
	.byte	0x8
	.byte	0
	.value	0
	.value	0
	.quad	.Ltext0
	.quad	.Letext0-.Ltext0
	.quad	0
	.quad	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF10:
	.string	"__off_t"
.LASF51:
	.string	"_IO_read_ptr"
.LASF63:
	.string	"_chain"
.LASF41:
	.string	"sin6_addr"
.LASF116:
	.string	"__in6_u"
.LASF16:
	.string	"size_t"
.LASF69:
	.string	"_shortbuf"
.LASF120:
	.string	"ares_in6_addr"
.LASF6:
	.string	"__uint8_t"
.LASF57:
	.string	"_IO_buf_base"
.LASF22:
	.string	"long long unsigned int"
.LASF108:
	.string	"in_addr_t"
.LASF127:
	.string	"clock_gettime"
.LASF72:
	.string	"_codecvt"
.LASF94:
	.string	"__timezone"
.LASF23:
	.string	"long long int"
.LASF4:
	.string	"signed char"
.LASF43:
	.string	"sockaddr_inarp"
.LASF130:
	.string	"../deps/cares/src/ares__timeval.c"
.LASF64:
	.string	"_fileno"
.LASF52:
	.string	"_IO_read_end"
.LASF125:
	.string	"ares_free"
.LASF113:
	.string	"__u6_addr16"
.LASF9:
	.string	"long int"
.LASF114:
	.string	"__u6_addr32"
.LASF50:
	.string	"_flags"
.LASF73:
	.string	"_wide_data"
.LASF58:
	.string	"_IO_buf_end"
.LASF67:
	.string	"_cur_column"
.LASF91:
	.string	"program_invocation_short_name"
.LASF81:
	.string	"_IO_codecvt"
.LASF30:
	.string	"sockaddr_dl"
.LASF39:
	.string	"sin6_port"
.LASF106:
	.string	"uint16_t"
.LASF89:
	.string	"_sys_errlist"
.LASF90:
	.string	"program_invocation_name"
.LASF66:
	.string	"_old_offset"
.LASF71:
	.string	"_offset"
.LASF118:
	.string	"in6addr_loopback"
.LASF48:
	.string	"sockaddr_x25"
.LASF44:
	.string	"sockaddr_ipx"
.LASF8:
	.string	"__uint32_t"
.LASF97:
	.string	"timezone"
.LASF36:
	.string	"sin_zero"
.LASF76:
	.string	"__pad5"
.LASF80:
	.string	"_IO_marker"
.LASF83:
	.string	"stdin"
.LASF2:
	.string	"unsigned int"
.LASF110:
	.string	"s_addr"
.LASF75:
	.string	"_freeres_buf"
.LASF104:
	.string	"optopt"
.LASF3:
	.string	"long unsigned int"
.LASF13:
	.string	"__suseconds_t"
.LASF55:
	.string	"_IO_write_ptr"
.LASF123:
	.string	"ares_malloc"
.LASF96:
	.string	"daylight"
.LASF86:
	.string	"sys_nerr"
.LASF1:
	.string	"short unsigned int"
.LASF35:
	.string	"sin_addr"
.LASF121:
	.string	"_S6_un"
.LASF131:
	.string	"/workspace/nodejsbuild/node-v12.18.4/out"
.LASF59:
	.string	"_IO_save_base"
.LASF128:
	.string	"gettimeofday"
.LASF100:
	.string	"environ"
.LASF70:
	.string	"_lock"
.LASF65:
	.string	"_flags2"
.LASF19:
	.string	"timeval"
.LASF84:
	.string	"stdout"
.LASF47:
	.string	"sockaddr_un"
.LASF33:
	.string	"sin_family"
.LASF101:
	.string	"optarg"
.LASF21:
	.string	"tv_nsec"
.LASF98:
	.string	"getdate_err"
.LASF38:
	.string	"sin6_family"
.LASF102:
	.string	"optind"
.LASF14:
	.string	"__syscall_slong_t"
.LASF56:
	.string	"_IO_write_end"
.LASF132:
	.string	"_IO_lock_t"
.LASF117:
	.string	"in6addr_any"
.LASF49:
	.string	"_IO_FILE"
.LASF99:
	.string	"__environ"
.LASF93:
	.string	"__daylight"
.LASF85:
	.string	"stderr"
.LASF77:
	.string	"_mode"
.LASF46:
	.string	"sockaddr_ns"
.LASF34:
	.string	"sin_port"
.LASF26:
	.string	"sa_family"
.LASF87:
	.string	"sys_errlist"
.LASF62:
	.string	"_markers"
.LASF42:
	.string	"sin6_scope_id"
.LASF0:
	.string	"unsigned char"
.LASF45:
	.string	"sockaddr_iso"
.LASF5:
	.string	"short int"
.LASF17:
	.string	"tv_sec"
.LASF88:
	.string	"_sys_nerr"
.LASF20:
	.string	"timespec"
.LASF68:
	.string	"_vtable_offset"
.LASF95:
	.string	"tzname"
.LASF29:
	.string	"sockaddr_ax25"
.LASF79:
	.string	"FILE"
.LASF134:
	.string	"__stack_chk_fail"
.LASF115:
	.string	"in6_addr"
.LASF18:
	.string	"tv_usec"
.LASF107:
	.string	"uint32_t"
.LASF15:
	.string	"char"
.LASF40:
	.string	"sin6_flowinfo"
.LASF7:
	.string	"__uint16_t"
.LASF112:
	.string	"__u6_addr8"
.LASF103:
	.string	"opterr"
.LASF11:
	.string	"__off64_t"
.LASF53:
	.string	"_IO_read_base"
.LASF61:
	.string	"_IO_save_end"
.LASF31:
	.string	"sockaddr_eon"
.LASF28:
	.string	"sockaddr_at"
.LASF124:
	.string	"ares_realloc"
.LASF12:
	.string	"__time_t"
.LASF24:
	.string	"sa_family_t"
.LASF78:
	.string	"_unused2"
.LASF119:
	.string	"_S6_u8"
.LASF37:
	.string	"sockaddr_in6"
.LASF25:
	.string	"sockaddr"
.LASF32:
	.string	"sockaddr_in"
.LASF105:
	.string	"uint8_t"
.LASF60:
	.string	"_IO_backup_base"
.LASF27:
	.string	"sa_data"
.LASF74:
	.string	"_freeres_list"
.LASF129:
	.string	"GNU C89 9.3.0 -m64 -mtune=generic -march=x86-64 -g -O3 -std=gnu90 -fno-omit-frame-pointer -fasynchronous-unwind-tables -fstack-protector-strong -fstack-clash-protection -fcf-protection"
.LASF82:
	.string	"_IO_wide_data"
.LASF126:
	.string	"tsnow"
.LASF122:
	.string	"ares_in6addr_any"
.LASF92:
	.string	"__tzname"
.LASF54:
	.string	"_IO_write_base"
.LASF133:
	.string	"ares__tvnow"
.LASF111:
	.string	"in_port_t"
.LASF109:
	.string	"in_addr"
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
