	.file	"uarrsort.cpp"
	.text
	.p2align 4
	.globl	uprv_uint16Comparator_67
	.type	uprv_uint16Comparator_67, @function
uprv_uint16Comparator_67:
.LFB2054:
	.cfi_startproc
	endbr64
	movzwl	(%rsi), %eax
	movzwl	(%rdx), %edx
	subl	%edx, %eax
	ret
	.cfi_endproc
.LFE2054:
	.size	uprv_uint16Comparator_67, .-uprv_uint16Comparator_67
	.p2align 4
	.globl	uprv_int32Comparator_67
	.type	uprv_int32Comparator_67, @function
uprv_int32Comparator_67:
.LFB2055:
	.cfi_startproc
	endbr64
	movl	(%rsi), %eax
	subl	(%rdx), %eax
	ret
	.cfi_endproc
.LFE2055:
	.size	uprv_int32Comparator_67, .-uprv_int32Comparator_67
	.p2align 4
	.globl	uprv_uint32Comparator_67
	.type	uprv_uint32Comparator_67, @function
uprv_uint32Comparator_67:
.LFB2056:
	.cfi_startproc
	endbr64
	movl	(%rdx), %ecx
	movl	$-1, %eax
	cmpl	%ecx, (%rsi)
	jb	.L4
	setne	%al
	movzbl	%al, %eax
.L4:
	ret
	.cfi_endproc
.LFE2056:
	.size	uprv_uint32Comparator_67, .-uprv_uint32Comparator_67
	.p2align 4
	.globl	uprv_stableBinarySearch_67
	.type	uprv_stableBinarySearch_67, @function
uprv_stableBinarySearch_67:
.LFB2057:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdx, %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movl	%esi, %ebx
	subq	$40, %rsp
	movq	%rdi, -80(%rbp)
	movl	%ecx, -64(%rbp)
	movq	%r8, -56(%rbp)
	cmpl	$8, %esi
	jle	.L8
	movb	$0, -65(%rbp)
	xorl	%r15d, %r15d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	leal	1(%r13), %r15d
	movl	%ebx, %eax
	movb	$1, -65(%rbp)
	subl	%r15d, %eax
	cmpl	$8, %eax
	jle	.L25
.L9:
	leal	(%rbx,%r15), %eax
	movl	-64(%rbp), %edx
	movq	%r12, %rsi
	movq	%r14, %rdi
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	movq	-56(%rbp), %rax
	sarl	%ecx
	imull	%ecx, %edx
	movl	%ecx, %r13d
	movslq	%edx, %rdx
	addq	-80(%rbp), %rdx
	call	*%rax
	testl	%eax, %eax
	je	.L26
	cmovs	%r13d, %ebx
	cmovns	%r13d, %r15d
	movl	%ebx, %eax
	subl	%r15d, %eax
	cmpl	$8, %eax
	jg	.L9
.L25:
	cmpl	%ebx, %r15d
	jge	.L13
.L12:
	movslq	-64(%rbp), %rdi
	movq	-80(%rbp), %r13
	movq	%rdi, %rax
	movq	%rdi, -64(%rbp)
	imull	%r15d, %eax
	movslq	%eax, %rdx
	addq	%rdx, %r13
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	js	.L13
	addl	$1, %r15d
	addq	-64(%rbp), %r13
	cmpl	%r15d, %ebx
	jle	.L13
.L18:
	movq	-56(%rbp), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*%rax
	testl	%eax, %eax
	jne	.L27
	addl	$1, %r15d
	movb	$1, -65(%rbp)
	addq	-64(%rbp), %r13
	cmpl	%r15d, %ebx
	jg	.L18
.L13:
	movl	%r15d, %r8d
	leal	-1(%r15), %eax
	cmpb	$0, -65(%rbp)
	notl	%r8d
	cmove	%r8d, %eax
.L7:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L8:
	.cfi_restore_state
	testl	%esi, %esi
	jle	.L21
	movb	$0, -65(%rbp)
	xorl	%r15d, %r15d
	jmp	.L12
.L21:
	movl	$-1, %eax
	jmp	.L7
	.cfi_endproc
.LFE2057:
	.size	uprv_stableBinarySearch_67, .-uprv_stableBinarySearch_67
	.p2align 4
	.type	_ZL15doInsertionSortPciiPFiPKvS1_S1_ES1_Pv, @function
_ZL15doInsertionSortPciiPFiPKvS1_S1_ES1_Pv:
.LFB2058:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movl	%esi, -52(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -80(%rbp)
	cmpl	$1, %esi
	jle	.L28
	movslq	%edx, %rbx
	movq	%rdi, %r12
	movl	%edx, %r13d
	movl	$1, %r14d
	leaq	(%rdi,%rbx), %r15
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L32:
	addl	$1, %r14d
	addq	%rbx, %r15
	cmpl	%r14d, -52(%rbp)
	je	.L28
.L33:
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r8
	movl	%r13d, %ecx
	movq	%r15, %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	uprv_stableBinarySearch_67
	movl	%eax, %edx
	leal	1(%rax), %ecx
	testl	%eax, %eax
	notl	%edx
	cmovs	%edx, %ecx
	cmpl	%r14d, %ecx
	jge	.L32
	movl	%ecx, %r8d
	movq	-80(%rbp), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	imull	%r13d, %r8d
	movl	%ecx, -56(%rbp)
	addq	%rbx, %r15
	movslq	%r8d, %r8
	addq	%r12, %r8
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movl	-56(%rbp), %ecx
	movl	%r14d, %edx
	movq	-88(%rbp), %r8
	addl	$1, %r14d
	subl	%ecx, %edx
	leaq	(%r8,%rbx), %rdi
	movq	%r8, %rsi
	movslq	%edx, %rdx
	imulq	%rbx, %rdx
	call	memmove@PLT
	movq	-88(%rbp), %r8
	movq	-80(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r8, %rdi
	call	memcpy@PLT
	cmpl	%r14d, -52(%rbp)
	jne	.L33
.L28:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2058:
	.size	_ZL15doInsertionSortPciiPFiPKvS1_S1_ES1_Pv, .-_ZL15doInsertionSortPciiPFiPKvS1_S1_ES1_Pv
	.p2align 4
	.type	_ZL12subQuickSortPciiiPFiPKvS1_S1_ES1_PvS4_, @function
_ZL12subQuickSortPciiiPFiPKvS1_S1_ES1_PvS4_:
.LFB2060:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$104, %rsp
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	.cfi_offset 3, -56
	movq	24(%rbp), %rax
	movl	%ecx, -116(%rbp)
	movq	%rdi, -112(%rbp)
	movq	16(%rbp), %r13
	movq	%rax, -136(%rbp)
	movslq	%ecx, %rax
	negl	%ecx
	movq	%rax, -104(%rbp)
	movslq	%ecx, %rax
	movl	%esi, -120(%rbp)
	movl	%edx, -124(%rbp)
	movq	%r8, -56(%rbp)
	movq	%rax, -72(%rbp)
.L45:
	movl	-120(%rbp), %eax
	addl	$9, %eax
	cmpl	-124(%rbp), %eax
	jge	.L55
	movl	-120(%rbp), %r15d
	movl	-124(%rbp), %eax
	movq	%r13, %rdi
	movq	-104(%rbp), %r12
	movq	-112(%rbp), %rbx
	addl	%r15d, %eax
	movl	%eax, %esi
	movq	%r12, %rdx
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	movslq	%esi, %rsi
	imulq	%r12, %rsi
	addq	%rbx, %rsi
	call	memcpy@PLT
	movl	-116(%rbp), %eax
	movl	%r15d, -64(%rbp)
	imull	%r15d, %eax
	cltq
	addq	%rbx, %rax
	movq	%rax, -80(%rbp)
	movslq	%r15d, %rax
	imulq	%r12, %rax
	addq	%rbx, %rax
	movl	-124(%rbp), %ebx
	movq	%rax, -96(%rbp)
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%r15d, -88(%rbp)
	movq	-80(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r14, %rdi
	movq	-56(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	js	.L56
	movl	-116(%rbp), %edx
	leal	-1(%rbx), %r15d
	movq	-112(%rbp), %rax
	imull	%r15d, %edx
	movslq	%edx, %rdx
	leaq	(%rax,%rdx), %r12
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L57:
	subl	$1, %r15d
.L40:
	movl	%ebx, -60(%rbp)
	movq	%r12, %rdx
	movq	-56(%rbp), %rax
	movq	%r13, %rsi
	movq	%r14, %rdi
	movl	%r15d, %ebx
	call	*%rax
	addq	-72(%rbp), %r12
	testl	%eax, %eax
	js	.L57
	movl	-64(%rbp), %eax
	movl	-60(%rbp), %ecx
	cmpl	%ecx, %eax
	jl	.L58
.L41:
	movl	-124(%rbp), %edx
	movl	-60(%rbp), %ecx
	movl	-120(%rbp), %edi
	leal	-1(%rdx), %eax
	subl	-88(%rbp), %edx
	subl	%edi, %ecx
	cmpl	%edx, %ecx
	jge	.L43
	cmpl	%ebx, %edi
	jl	.L59
	movl	-88(%rbp), %edi
	movl	%edi, -120(%rbp)
.L44:
	cmpl	%eax, -120(%rbp)
	jl	.L45
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
	leal	1(%r15), %eax
.L39:
	movq	-104(%rbp), %rsi
	movl	%eax, -64(%rbp)
	movl	%eax, %r15d
	addq	%rsi, -80(%rbp)
	addq	%rsi, -96(%rbp)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	%eax, %r15d
	jg	.L60
.L42:
	movl	-64(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -88(%rbp)
	cmpl	%eax, %r15d
	jg	.L39
	movl	-60(%rbp), %ebx
	movl	%r15d, -60(%rbp)
	subl	$2, %ebx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L60:
	movq	-104(%rbp), %r12
	movq	-96(%rbp), %rsi
	movq	-136(%rbp), %rdi
	movq	%r12, %rdx
	call	memcpy@PLT
	movslq	%r15d, %r8
	movq	-96(%rbp), %rdi
	movq	%r12, %rdx
	imulq	%r12, %r8
	addq	-112(%rbp), %r8
	movq	%r8, %rsi
	movq	%r8, -88(%rbp)
	call	memcpy@PLT
	movq	-88(%rbp), %r8
	movq	-136(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r8, %rdi
	call	memcpy@PLT
	jmp	.L42
.L43:
	cmpl	-88(%rbp), %eax
	jg	.L61
	movl	-60(%rbp), %edx
	movl	%ebx, %eax
	movl	%edx, -124(%rbp)
	jmp	.L44
.L59:
	pushq	-136(%rbp)
	movl	%edi, %esi
	movl	-116(%rbp), %ecx
	movq	%r14, %r9
	movq	-56(%rbp), %r8
	pushq	%r13
	movl	-60(%rbp), %edx
	movq	-112(%rbp), %rdi
	movl	%eax, -64(%rbp)
	call	_ZL12subQuickSortPciiiPFiPKvS1_S1_ES1_PvS4_
	movl	-88(%rbp), %eax
	popq	%rcx
	popq	%rsi
	movl	%eax, -120(%rbp)
	movl	-64(%rbp), %eax
	jmp	.L44
.L61:
	movl	-124(%rbp), %edx
	movq	-56(%rbp), %r8
	pushq	-136(%rbp)
	movq	%r14, %r9
	movl	-116(%rbp), %ecx
	pushq	%r13
	movl	-88(%rbp), %esi
	movq	-112(%rbp), %rdi
	call	_ZL12subQuickSortPciiiPFiPKvS1_S1_ES1_PvS4_
	popq	%rax
	popq	%rdx
	movl	%ebx, %eax
	movl	-60(%rbp), %edx
	movl	%edx, -124(%rbp)
	jmp	.L44
.L55:
	movl	-120(%rbp), %edi
	movl	-124(%rbp), %esi
	movq	%r13, %r9
	movq	%r14, %r8
	movl	-116(%rbp), %edx
	movq	-56(%rbp), %rcx
	subl	%edi, %esi
	imull	%edx, %edi
	movslq	%edi, %rdi
	addq	-112(%rbp), %rdi
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_def_cfa 7, 8
	jmp	_ZL15doInsertionSortPciiPFiPKvS1_S1_ES1_Pv
	.cfi_endproc
.LFE2060:
	.size	_ZL12subQuickSortPciiiPFiPKvS1_S1_ES1_PvS4_, .-_ZL12subQuickSortPciiiPFiPKvS1_S1_ES1_PvS4_
	.p2align 4
	.type	_ZL9quickSortPciiPFiPKvS1_S1_ES1_P10UErrorCode, @function
_ZL9quickSortPciiPFiPKvS1_S1_ES1_P10UErrorCode:
.LFB2061:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r15
	pushq	%r14
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	movl	%esi, %r14d
	pushq	%r13
	.cfi_offset 13, -40
	movq	%rdi, %r13
	pushq	%r12
	.cfi_offset 12, -48
	leaq	-512(%rbp), %r12
	pushq	%rbx
	.cfi_offset 3, -56
	movslq	%edx, %rbx
	movq	%rbx, %r15
	addq	$31, %rbx
	shrq	$5, %rbx
	subq	$520, %rsp
	movq	%r9, -536(%rbp)
	leal	(%rbx,%rbx), %edx
	movq	%fs:40, %rax
	movq	%rax, -56(%rbp)
	xorl	%eax, %eax
	movq	%r12, -528(%rbp)
	movl	$14, -520(%rbp)
	movb	$0, -516(%rbp)
	cmpl	$14, %edx
	jle	.L63
	movslq	%edx, %rdi
	movq	%r8, -560(%rbp)
	salq	$5, %rdi
	movq	%rcx, -552(%rbp)
	movl	%edx, -544(%rbp)
	call	uprv_malloc_67@PLT
	movq	-536(%rbp), %r9
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L64
	cmpb	$0, -516(%rbp)
	movl	-544(%rbp), %edx
	movq	-552(%rbp), %rcx
	movq	-560(%rbp), %r8
	jne	.L73
.L65:
	movq	%r12, -528(%rbp)
	movl	%edx, -520(%rbp)
	movb	$1, -516(%rbp)
.L63:
	movslq	%ebx, %rbx
	movq	%r8, %r9
	movl	%r14d, %edx
	movq	%rcx, %r8
	salq	$5, %rbx
	xorl	%esi, %esi
	movl	%r15d, %ecx
	movq	%r13, %rdi
	addq	%r12, %rbx
	pushq	%rbx
	pushq	%r12
	call	_ZL12subQuickSortPciiiPFiPKvS1_S1_ES1_PvS4_
	cmpb	$0, -516(%rbp)
	popq	%rax
	popq	%rdx
	jne	.L66
.L62:
	movq	-56(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L74
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L64:
	.cfi_restore_state
	cmpb	$0, -516(%rbp)
	movl	$7, (%r9)
	je	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	movq	-528(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-528(%rbp), %rdi
	movq	%r8, -552(%rbp)
	movq	%rcx, -544(%rbp)
	movl	%edx, -536(%rbp)
	call	uprv_free_67@PLT
	movq	-552(%rbp), %r8
	movq	-544(%rbp), %rcx
	movl	-536(%rbp), %edx
	jmp	.L65
.L74:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2061:
	.size	_ZL9quickSortPciiPFiPKvS1_S1_ES1_P10UErrorCode, .-_ZL9quickSortPciiPFiPKvS1_S1_ES1_P10UErrorCode
	.p2align 4
	.globl	uprv_sortArray_67
	.type	uprv_sortArray_67, @function
uprv_sortArray_67:
.LFB2062:
	.cfi_startproc
	endbr64
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$312, %rsp
	.cfi_offset 13, -24
	.cfi_offset 12, -32
	.cfi_offset 3, -40
	movq	16(%rbp), %rbx
	movq	%fs:40, %rax
	movq	%rax, -40(%rbp)
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L75
	movl	(%rbx), %eax
	testl	%eax, %eax
	jg	.L75
	movq	%rdi, %r12
	testl	%esi, %esi
	jle	.L92
	testq	%rdi, %rdi
	jne	.L92
.L77:
	movl	$1, (%rbx)
.L75:
	movq	-40(%rbp), %rax
	xorq	%fs:40, %rax
	jne	.L101
	addq	$312, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	.cfi_restore_state
	movl	%esi, %eax
	shrl	$31, %eax
	testl	%edx, %edx
	setle	%dil
	orb	%al, %dil
	jne	.L77
	testq	%rcx, %rcx
	je	.L77
	cmpl	$1, %esi
	jle	.L75
	cmpl	$8, %esi
	jle	.L93
	testb	%r9b, %r9b
	jne	.L93
	movq	%rbx, %r9
	movq	%r12, %rdi
	call	_ZL9quickSortPciiPFiPKvS1_S1_ES1_P10UErrorCode
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L93:
	movslq	%edx, %rax
	leaq	-272(%rbp), %r9
	movl	$7, -280(%rbp)
	addq	$31, %rax
	movq	%r9, -288(%rbp)
	movq	%rax, %r13
	movb	$0, -276(%rbp)
	shrq	$5, %r13
	cmpq	$255, %rax
	jbe	.L83
	movq	%r13, %rdi
	movq	%r8, -320(%rbp)
	salq	$5, %rdi
	movq	%rcx, -312(%rbp)
	movl	%edx, -300(%rbp)
	movl	%esi, -296(%rbp)
	call	uprv_malloc_67@PLT
	movq	%rax, %r9
	testq	%rax, %rax
	je	.L84
	cmpb	$0, -276(%rbp)
	movl	-296(%rbp), %esi
	movl	-300(%rbp), %edx
	movq	-312(%rbp), %rcx
	movq	-320(%rbp), %r8
	jne	.L102
.L85:
	movq	%r9, -288(%rbp)
	movl	%r13d, -280(%rbp)
	movb	$1, -276(%rbp)
.L83:
	movq	%r12, %rdi
	call	_ZL15doInsertionSortPciiPFiPKvS1_S1_ES1_Pv
	cmpb	$0, -276(%rbp)
	je	.L75
.L86:
	movq	-288(%rbp), %rdi
	call	uprv_free_67@PLT
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-288(%rbp), %rdi
	movq	%r8, -328(%rbp)
	movq	%rcx, -320(%rbp)
	movl	%edx, -312(%rbp)
	movl	%esi, -300(%rbp)
	movq	%rax, -296(%rbp)
	call	uprv_free_67@PLT
	movq	-328(%rbp), %r8
	movq	-320(%rbp), %rcx
	movl	-312(%rbp), %edx
	movl	-300(%rbp), %esi
	movq	-296(%rbp), %r9
	jmp	.L85
.L84:
	cmpb	$0, -276(%rbp)
	movl	$7, (%rbx)
	je	.L75
	jmp	.L86
.L101:
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE2062:
	.size	uprv_sortArray_67, .-uprv_sortArray_67
	.ident	"GCC: (Ubuntu 9.3.0-10ubuntu2) 9.3.0"
	.section	.note.GNU-stack,"",@progbits
	.section	.note.gnu.property,"a"
	.align 8
	.long	 1f - 0f
	.long	 4f - 1f
	.long	 5
0:
	.string	 "GNU"
1:
	.align 8
	.long	 0xc0000002
	.long	 3f - 2f
2:
	.long	 0x3
3:
	.align 8
4:
